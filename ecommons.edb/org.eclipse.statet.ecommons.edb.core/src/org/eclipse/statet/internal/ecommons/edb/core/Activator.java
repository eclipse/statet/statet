/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.edb.core;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.Plugin;


/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends Plugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.ecommons.edb.core";
	
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	private static Activator instance;
	
	public static Activator getInstance() {
		return instance;
	}
	
	
	/**
	 * The constructor
	 */
	public Activator() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		instance= null;
		super.stop(context);
	}
	
}
