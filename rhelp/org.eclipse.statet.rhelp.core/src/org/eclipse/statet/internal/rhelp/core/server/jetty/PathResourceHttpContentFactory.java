/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.server.jetty;

import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;

import org.eclipse.jetty.http.MimeTypes;
import org.eclipse.jetty.http.content.HttpContent;
import org.eclipse.jetty.http.content.ResourceHttpContent;
import org.eclipse.jetty.util.resource.PathResourceFactory;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.resource.ResourceFactory;
import org.eclipse.jetty.util.resource.Resources;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/** Passes path */
@NonNullByDefault
public class PathResourceHttpContentFactory implements HttpContent.Factory {
	
	private final ResourceFactory factory;
	private final MimeTypes mimeTypes;
	
	public PathResourceHttpContentFactory(final MimeTypes mimeTypes) {
		this.factory= new PathResourceFactory();
		this.mimeTypes = mimeTypes;
	}
	
	@Override
	public @Nullable HttpContent getContent(final String pathInContext) throws IOException {
		try {
			final Resource resource= this.factory.newResource(pathInContext);
			if (resource == null || Resources.missing(resource)) {
				return null;
			}
			return load(pathInContext, resource);
		}
		catch (final Throwable t) {
			final InvalidPathException saferException= new InvalidPathException(pathInContext, "Invalid pathInContext");
			saferException.initCause(t);
			throw saferException;
		}
	}
	
	public @Nullable HttpContent getContent(final Path path, final String pathInContext) throws IOException {
		try {
			final Resource resource= this.factory.newResource(path);
			if (resource == null || Resources.missing(resource)) {
				return null;
			}
			return load(pathInContext, resource);
		}
		catch (final Throwable t) {
			final InvalidPathException saferException= new InvalidPathException(pathInContext, "Invalid path");
			saferException.initCause(t);
			throw saferException;
		}
	}
	
	private @Nullable HttpContent load(final String pathInContext, final Resource resource) {
		return new ResourceHttpContent(resource, this.mimeTypes.getMimeByExtension(pathInContext));
	}
	
}
