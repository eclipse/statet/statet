/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core;

import com.ibm.icu.text.Collator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.rj.renv.core.RPkgUtils;


@NonNullByDefault
public class RHelpCoreInternals {
	
	
	public static final Collator R_NAMES_COLLATOR= RPkgUtils.NAMES_COLLATOR;
	
	
	public static void log(final Status status) {
		CommonsRuntime.log(status);
	}
	
	
	public static final boolean DEBUG= Boolean.getBoolean("org.eclipse.statet.rhelp.debug"); //$NON-NLS-1$
	
	
	private RHelpCoreInternals() {}
	
}
