/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.server.jetty;

import org.eclipse.jetty.http.MimeTypes;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rhelp.core.http.MediaTypeProvider;


@NonNullByDefault
public class ExtMimeTypes extends MimeTypes {
	
	
	private final MediaTypeProvider defaultMediaTypes;
	
	private @Nullable MediaTypeProvider specialMediaTypes;
	
	
	public ExtMimeTypes(final MediaTypeProvider mimeTypes) {
		this.defaultMediaTypes= mimeTypes;
	}
	
	
	public @Nullable MediaTypeProvider getSpecialMediaTypes() {
		return this.specialMediaTypes;
	}
	
	public void setSpecialMediaTypes(final @Nullable MediaTypeProvider specialMediaTypes) {
		this.specialMediaTypes= specialMediaTypes;
	}
	
	
	@Override
	public @Nullable String getMimeByExtension(String filename) {
		final int idx= Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\'));
		if (idx >= 0) {
			filename= filename.substring(idx + 1);
		}
		String type= null;
		{	final var specialMediaTypes= getSpecialMediaTypes();
			if (specialMediaTypes != null) {
				type= specialMediaTypes.getMediaTypeString(filename);
			}
		}
		if (type == null) {
			type= this.defaultMediaTypes.getMediaTypeString(filename);
		}
		if (type == null) {
			type= super.getMimeByExtension(filename);
		}
		return type;
	}
	
}
