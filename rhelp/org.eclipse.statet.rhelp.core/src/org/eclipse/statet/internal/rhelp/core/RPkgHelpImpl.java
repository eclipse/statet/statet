/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rhelp.core.RHelpPage;
import org.eclipse.statet.rhelp.core.RHelpTopicEntry;
import org.eclipse.statet.rhelp.core.RPkgHelp;
import org.eclipse.statet.rj.renv.core.BasicRPkgDescription;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.core.RNumVersion;
import org.eclipse.statet.rj.renv.core.RPkgDescription;
import org.eclipse.statet.rj.renv.core.RPkgUtils;


@NonNullByDefault
public class RPkgHelpImpl implements RPkgHelp {
	
	
	private final String name;
	private final RPkgDescription pkgDescription;
	
	private final REnv rEnv;
	
	private ImList<RHelpPage> pages;
	
	private @Nullable ImList<RHelpTopicEntry> topics;
	
	
	public RPkgHelpImpl(final RPkgDescription pkgDescription, final REnv rEnv) {
		this.name= pkgDescription.getName();
		this.pkgDescription= pkgDescription;
		this.rEnv= rEnv;
	}
	
	public RPkgHelpImpl(final RPkgHelp template, final RLibLocation libLocation) {
		this.name= template.getName();
		this.pkgDescription= new BasicRPkgDescription(template.getPkgDescription(), libLocation );
		this.rEnv= template.getREnv();
		this.pages= template.getPages();
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public RNumVersion getVersion() {
		return this.pkgDescription.getVersion();
	}
	
	@Override
	public String getTitle() {
		return this.pkgDescription.getTitle();
	}
	
	@Override
	public RPkgDescription getPkgDescription() {
		return this.pkgDescription;
	}
	
	@Override
	public REnv getREnv() {
		return this.rEnv;
	}
	
	
	public void setPages(final ImList<RHelpPage> pages) {
		this.pages= pages;
	}
	
	@Override
	public ImList<RHelpPage> getPages() {
		return this.pages;
	}
	
	@Override
	public @Nullable RHelpPage getPage(final String name) {
		if (name != null) {
			for (final RHelpPage page : this.pages) {
				if (page.getName().equals(name)) {
					return page;
				}
			}
		}
		return null;
	}
	
	public int indexOf(final ImList<RHelpTopicEntry> topics, final @Nullable String topic) {
		if (topic == null) {
			return -1;
		}
		int low= 0;
		int high= topics.size() - 1;
		while (low <= high) {
			final int mid= (low + high) >>> 1;
			final int diff= RHelpCoreInternals.R_NAMES_COLLATOR.compare(topics.get(mid).getTopic(), topic);
			if (diff < 0) {
				low= mid + 1;
			}
			else if (diff > 0) {
				high= mid - 1;
			}
			else {
				return mid;
			}
		}
		return -(low + 1);
	}
	
	@Override
	public @Nullable RHelpPage getPageForTopic(final String topic) {
		final ImList<RHelpTopicEntry> topics= getTopics();
		final int idx= indexOf(topics, topic);
		if (idx >= 0) {
			return topics.get(idx).getPage();
		}
		return null;
	}
	
	@Override
	public ImList<RHelpTopicEntry> getTopics() {
		ImList<RHelpTopicEntry> topics= this.topics;
		if (topics == null) {
			final List<RHelpTopicEntry> list= new ArrayList<>(this.pages.size() * 3);
			for (final RHelpPage page : this.pages) {
				for (final String topic : page.getTopics()) {
					list.add(new RHelpTopicEntry(topic, page));
				}
			}
			this.topics= topics= ImCollections.toList(list, null);
		}
		return topics;
	}
	
	
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RPkgHelp other
						&& this.name.equals(other.getName())
						&& this.rEnv.equals(other.getREnv()) ));
	}
	
	@Override
	public int compareTo(final RPkgHelp o) {
		return RPkgUtils.NAMES_COLLATOR.compare(this.name, o.getName());
	}
	
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
