/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.DelegatingAnalyzerWrapper;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
final class QueryAnalyzer extends DelegatingAnalyzerWrapper {
	// see org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper
	
	
	private final Analyzer defaultAnalyzer;
	
	private final Analyzer nameAnalyzers;
	
	
	public QueryAnalyzer() {
		super(PER_FIELD_REUSE_STRATEGY);
		this.defaultAnalyzer= new DefaultAnalyzer();
		this.nameAnalyzers= new WhitespaceAnalyzer();
	}
	
	
	@Override
	protected Analyzer getWrappedAnalyzer(final String fieldName) {
		if (!(fieldName.endsWith(".txt") || fieldName.endsWith(".html"))) { //$NON-NLS-1$ //$NON-NLS-2$
			return this.nameAnalyzers;
		}
		return this.defaultAnalyzer;
	}
	
}
