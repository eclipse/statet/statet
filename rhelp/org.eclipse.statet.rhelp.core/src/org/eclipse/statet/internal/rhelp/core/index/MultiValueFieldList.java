/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import java.util.ArrayList;

import org.apache.lucene.document.Field;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * List creating automatically new elements in {@link #get(int)}.
 */
@NonNullByDefault
@SuppressWarnings("serial")
abstract class MultiValueFieldList<E extends Field> extends ArrayList<E> {
	
	
	public static MultiValueFieldList<NameField> forNameField(final String name) {
		return new MultiValueFieldList<>() {
			@Override
			protected NameField createNewField() {
				return new NameField(name);
			}
		};
	}
	
	public static MultiValueFieldList<KeywordField> forKeywordField(final String name) {
		return new MultiValueFieldList<>() {
			@Override
			protected KeywordField createNewField() {
				return new KeywordField(name);
			}
		};
	}
	
	public static MultiValueFieldList<TxtField> forTxtField(final String name) {
		return new MultiValueFieldList<>() {
			@Override
			protected TxtField createNewField() {
				return new TxtField(name);
			}
		};
	}
	
	
	public MultiValueFieldList() {
	}
	
	
	protected abstract E createNewField();
	
	
	@Override
	public E get(final int index) {
		while (index >= size()) {
			add(createNewField());
		}
		return super.get(index);
	}
	
}
