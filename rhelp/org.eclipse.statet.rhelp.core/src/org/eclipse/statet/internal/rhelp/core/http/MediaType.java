/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.http;
//package org.eclipse.statet.jcommons.io;

import java.util.Map;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface MediaType {
	
	String WILDCARD= "*";
	
	
	String getType();
	
	String getSubtype();
	
	
	ImList<? extends Map.Entry<String, String>> getParameters();
	
	default @Nullable String getParameterValue(final String name) {
		final ImList<? extends Map.Entry<String, String>> parameters= getParameters();
		for (final Map.Entry<String, String> parameter : parameters) {
			if (parameter.getKey().equals(name)) {
				return parameter.getValue();
			}
		}
		return null;
	}
	
}
