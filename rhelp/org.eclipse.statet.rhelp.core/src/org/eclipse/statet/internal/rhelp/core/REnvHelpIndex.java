/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core;

import java.util.List;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rhelp.core.RHelpPage;
import org.eclipse.statet.rhelp.core.RHelpSearchQuery;
import org.eclipse.statet.rhelp.core.RHelpSearchRequestor;
import org.eclipse.statet.rhelp.core.RPkgHelp;


@NonNullByDefault
public interface REnvHelpIndex {
	
	
	int TIMEOUT_ERROR= 1001;
	int CONNECT_ERROR= 1002;
	
	
	void dispose();
	
	
	List<RHelpPage> getPagesForTopic(String topic, Map<String, RPkgHelp> packageMap,
			final int timeout, final @Nullable ProgressMonitor m) throws StatusException;
	
	
	@Nullable String getHtmlPage(RPkgHelp pkgHelp, String pageName, @Nullable String queryString,
			final int timeout, final @Nullable ProgressMonitor m) throws StatusException;
	
	
	void search(RHelpSearchQuery searchQuery, List<RPkgHelp> packageList,
			Map<String, RPkgHelp> packageMap,
			RHelpSearchRequestor requestor) throws StatusException;
	
}
