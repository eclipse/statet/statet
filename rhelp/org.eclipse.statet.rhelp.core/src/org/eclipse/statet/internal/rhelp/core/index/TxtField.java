/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/** 
 * A field that is indexed and tokenized, with term vectors.  For example this would be used on a
 * 'body' field, that contains the bulk of a document's text.
 * 
 * Like {@link org.apache.lucene.document.TextField}, but stores term vector positions and
 * offsets, as required e.g. by vectorhightlighting.
 */
@NonNullByDefault
final class TxtField extends StringDataField {
	
	
	public static final FieldType TYPE;
	public static final FieldType TYPE_OMIT_NORM;
	
	static {
		TYPE= new FieldType(TextField.TYPE_STORED);
		TYPE.setStoreTermVectors(true);
		TYPE.setStoreTermVectorPositions(true);
		TYPE.setStoreTermVectorOffsets(true);
		TYPE.freeze();
		
		TYPE_OMIT_NORM= new FieldType(TYPE);
		TYPE_OMIT_NORM.setOmitNorms(true);
		TYPE_OMIT_NORM.freeze();
	}
	
	
	static final class OmitNorm extends StringDataField {
		
		
		/**
		 * Creates a new field.
		 * 
		 * @param name field name
		 * @throws IllegalArgumentException if the field name.
		 */
		public OmitNorm(final String name) {
			super(name, TYPE_OMIT_NORM);
		}
		
		
	}
	
	
	/**
	 * Creates a new field.
	 * 
	 * @param name field name
	 * @throws IllegalArgumentException if the field name.
	 */
	public TxtField(final String name) {
		super(name, TYPE);
	}
	
}
