/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.http;
// package org.eclipse.statet.jcommons.io;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.ImMapEntry;
import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicMediaType implements MediaType, Immutable {
	
	
	private static String check(final String t) {
		if (t.length() == 1 && t.charAt(0) == '*') {
			return WILDCARD;
		}
		return t;
	}
	
	protected static final int compare(final String t1, final String t2) {
		if (t1 == WILDCARD) {
			if (t2 == WILDCARD) {
				return 0;
			}
			else {
				return 1;
			}
		}
		else if (t2 == WILDCARD) {
			return -1;
		}
		else {
			return t1.compareTo(t2);
		}
	}
	
	
	private final String type;
	private final String subtype;
	private final ImList<? extends ImMapEntry<String, String>> parameters;
	
	
	public BasicMediaType(final String type, final String subtype,
			final ImList<? extends ImMapEntry<String, String>> parameters) {
		this.type= check(type);
		this.subtype= check(subtype);
		this.parameters= parameters;
	}
	
	@Override
	public final String getType() {
		return this.type;
	}
	
	@Override
	public final String getSubtype() {
		return this.subtype;
	}
	
	@Override
	public ImList<? extends ImMapEntry<String, String>> getParameters() {
		return this.parameters;
	}
	
	
//	public boolean includes(final MediaType type) {
//		if (this.type == WILDCARD || this.type.equals(type.getType())) {
//			if (this.subtype == WILDCARD || this.subtype.equals(type.getSubtype())) {
//				return true;
//			}
//		}
//		return false;
//	}
	
	
	@Override
	public int hashCode() {
		return this.type.hashCode() + this.subtype.hashCode() * 31;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final MediaType other
						&& this.type.equals(other.getType())
						&& this.subtype.equals(other.getSubtype())
						&& getParameters().equals(other.getParameters()) ));
	}
	
}
