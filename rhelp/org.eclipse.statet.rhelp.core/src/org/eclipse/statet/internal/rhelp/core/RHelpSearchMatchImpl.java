/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.rhelp.core.index.REnvIndexSchema;
import org.eclipse.statet.rhelp.core.RHelpPage;
import org.eclipse.statet.rhelp.core.RHelpSearchMatch;


@NonNullByDefault
public final class RHelpSearchMatchImpl implements RHelpSearchMatch {
	
	
	public static class Fragment implements MatchFragment {
		
		
		private final String field;
		private final String text;
		
		
		public Fragment(final String field, final String text) {
			this.field= field;
			this.text= text;
		}
		
		
		@Override
		public String getField() {
			return this.field;
		}
		
		@Override
		public @Nullable String getFieldLabel() {
			if (this.field == REnvIndexSchema.ALIAS_FIELD_NAME || this.field == REnvIndexSchema.ALIAS_TXT_FIELD_NAME) {
				return "Topic";
			}
			if (this.field == REnvIndexSchema.TITLE_TXT_FIELD_NAME) {
				return "Title";
			}
			if (this.field == REnvIndexSchema.CONCEPT_TXT_FIELD_NAME) {
				return "Concept";
			}
			if (this.field == REnvIndexSchema.DESCRIPTION_TXT_FIELD_NAME) {
				return "Description";
			}
			if (this.field == REnvIndexSchema.AUTHORS_TXT_FIELD_NAME) {
				return "Author(s)";
			}
			if (this.field == REnvIndexSchema.EXAMPLES_TXT_FIELD_NAME) {
				return "Examples";
			}
			return null;
		}
		
		@Override
		public String getText() {
			return this.text;
		}
		
		
		@Override
		public String toString() {
			final StringBuilder sb= new StringBuilder();
			sb.append(this.field);
			sb.append(": "); //$NON-NLS-1$
			sb.append(this.text);
			return sb.toString();
		}
		
	}
	
	
	private final RHelpPage page;
	private final float score;
	private final int matchCount;
	private final MatchFragment @Nullable [] bestFragments;
	
	
	public RHelpSearchMatchImpl(final RHelpPage page, final float score,
			final int matchCount, final MatchFragment @Nullable [] bestFragments) {
		this.page= page;
		this.score= score;
		this.matchCount= matchCount;
		this.bestFragments= bestFragments;
	}
	
	public RHelpSearchMatchImpl(final RHelpPage page, final float score) {
		this.page= page;
		this.score= score;
		this.matchCount= -1;
		this.bestFragments= null;
	}
	
	
	@Override
	public RHelpPage getPage() {
		return this.page;
	}
	
	@Override
	public float getScore() {
		return this.score;
	}
	
	@Override
	public int getMatchCount() {
		return this.matchCount;
	}
	
	@Override
	public MatchFragment @Nullable [] getBestFragments() {
		return this.bestFragments;
	}
	
	
	@Override
	public int hashCode() {
		return this.page.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj instanceof RHelpSearchMatch
				&& this.page.equals(((RHelpSearchMatch) obj).getPage()) );
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append(getPage());
		sb.append(" (score= ").append(getScore()).append(")"); //$NON-NLS-1$ //$NON-NLS-2$
		return sb.toString();
	}
	
}
