/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.similarities.Similarity;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface REnvIndexSchema {
	
	/**
	 * Lucene field name for exact doc type.
	 */
	String DOCTYPE_FIELD_NAME= "doctype"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for exact package name.
	 */
	String PACKAGE_FIELD_NAME= "pkg"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for exact page name.
	 */
	String PAGE_FIELD_NAME= "page"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for exact page alias.
	 */
	String ALIAS_FIELD_NAME= "alias"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed page alias.
	 */
	String ALIAS_TXT_FIELD_NAME= "alias.txt"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed page title.
	 */
	String TITLE_TXT_FIELD_NAME= "title.txt"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for exact help keyword.
	 */
	String KEYWORD_FIELD_NAME= "keyword"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed help concept.
	 */
	String CONCEPT_TXT_FIELD_NAME= "concept.txt"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed page text.
	 */
	String DOC_TXT_FIELD_NAME= "doc.txt"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed page text.
	 */
	String DOC_HTML_FIELD_NAME= "doc.html"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed description.
	 */
	String DESCRIPTION_TXT_FIELD_NAME= "descr.txt"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed authors.
	 */
	String AUTHORS_TXT_FIELD_NAME= "authors.txt"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed package maintainer.
	 */
	String MAINTAINER_TXT_FIELD_NAME= "maintainer.txt"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed package maintainer.
	 */
	String URL_TXT_FIELD_NAME= "url.txt"; //$NON-NLS-1$
	
	/**
	 * Lucene field name for analyzed authors.
	 */
	String EXAMPLES_TXT_FIELD_NAME= "examples.txt"; //$NON-NLS-1$
	
	
	String PKG_DESCRIPTION_DOCTYPE=     "RPkgDescription"; //$NON-NLS-1$
	
	String PAGE_DOCTYPE=                "RHelpPage"; //$NON-NLS-1$
	
	
	Similarity SIMILARITY= IndexSearcher.getDefaultSimilarity(); // BM25Similarity;
	
}
