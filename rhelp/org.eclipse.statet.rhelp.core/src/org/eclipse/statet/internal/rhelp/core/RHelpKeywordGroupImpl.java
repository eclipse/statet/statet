/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rhelp.core.RHelpKeyword;
import org.eclipse.statet.rhelp.core.RHelpKeywordGroup;


@NonNullByDefault
public final class RHelpKeywordGroupImpl implements RHelpKeywordGroup {
	
	
	private final String label;
	private final String description;
	private List<RHelpKeyword> nested;
	
	
	public RHelpKeywordGroupImpl(final String label, final String description,
			final ImList<RHelpKeyword> keywords) {
		this.label= label;
		this.description= description;
		this.nested= keywords;
	}
	
	public RHelpKeywordGroupImpl(final String label, final String description) {
		this.label= label;
		this.description= description;
		this.nested= new ArrayList<>();
	}
	
	
	@Override
	public String getLabel() {
		return this.label;
	}
	
	@Override
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public List<RHelpKeyword> getNestedKeywords() {
		return this.nested;
	}
	
	@Override
	public @Nullable RHelpKeyword getNestedKeyword(final String keyword) {
		for (int i= 0; i < this.nested.size(); i++) {
			final RHelpKeyword node= this.nested.get(i);
			if (node.getKeyword().equals(keyword)) {
				return node;
			}
		}
		return null;
	}
	
	public void freeze() {
		this.nested= ImCollections.toList(this.nested);
		for (int i= 0; i < this.nested.size(); i++) {
			((RHelpKeywordImpl) this.nested.get(i)).freeze();
		}
	}
	
	
	@Override
	public int hashCode() {
		return this.label.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof RHelpKeywordGroup
						&& this.label.equals(((RHelpKeywordGroup) obj).getLabel()) ));
	}
	
}
