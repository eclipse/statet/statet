/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.server;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rhelp.core.REnvHelpIndex;
import org.eclipse.statet.internal.rhelp.core.RHelpSearchMatchImpl;
import org.eclipse.statet.internal.rhelp.core.RHelpWebapp;
import org.eclipse.statet.internal.rhelp.core.SerUtil;
import org.eclipse.statet.internal.rhelp.core.SerUtil.Controller;
import org.eclipse.statet.internal.rhelp.core.http.HttpHeaderUtils;
import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rhelp.core.RHelpCore;
import org.eclipse.statet.rhelp.core.RHelpPage;
import org.eclipse.statet.rhelp.core.RHelpSearchMatch.MatchFragment;
import org.eclipse.statet.rhelp.core.RHelpSearchQuery;
import org.eclipse.statet.rhelp.core.RHelpSearchRequestor;
import org.eclipse.statet.rhelp.core.RPkgHelp;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;


@NonNullByDefault
public abstract class ServerREnvHelpAccess implements REnvHelpIndex {
	
	
	protected static final @NonNull String[] NO_PARAMS= new @NonNull String[0];
	
	private static String createDSAcceptHeader(final SerUtil serUtil) {
		final HttpHeaderUtils.HeaderBuilder builder= new HttpHeaderUtils.HeaderBuilder();
		int prio= 10;
		for (final int knownVersion : SerUtil.KNOWN_VERSIONS) {
			if (serUtil.canRead(knownVersion, REnvConfiguration.SHARED_SERVER)) {
				builder.newEntry(ServerApi.DS_MEDIA_TYPE_STRING, prio/10.0f);
				builder.addParameter(ServerApi.DS_SER_VERSION, Integer.toString(knownVersion));
				prio--;
			}
		}
		return builder.build();
	}
	
	
	private final URI url;
	
	private final String basePath;
	private final String rEnvId;
	
	private final String path;
	
	private final SerUtil serUtil= new SerUtil();
	private final String dsModestAcceptHeader= ServerApi.DS_MEDIA_TYPE_STRING;
	private final String dsVersionedAcceptHeader= createDSAcceptHeader(this.serUtil);
	
	
	public ServerREnvHelpAccess(final URI uri) {
		this.url= uri;
		
		final String uriPath= nonNullAssert(uri.getPath());
		final int idStart= uriPath.lastIndexOf('/');
		this.basePath= uriPath.substring(0, idStart);
		this.rEnvId= uriPath.substring(idStart + 1);
		this.path= this.basePath + RHelpWebapp.CONTEXT_PATH +
				'/' + ServerApi.API_VERSION_1 +
				'/' + this.rEnvId +
				'/';
	}
	
	
	@Override
	public void dispose() {
	}
	
	
	protected final String getBasePath() {
		return this.basePath;
	}
	
	protected final String createPath(final String s1) {
		final StringBuilder sb= new StringBuilder(this.path.length() + s1.length());
		sb.append(this.path);
		sb.append(s1);
		return sb.toString();
	}
	
	protected final String createPath(final String s1, final String s2) {
		final StringBuilder sb= new StringBuilder(this.path.length() + s1.length() + s2.length() + 1);
		sb.append(this.path);
		sb.append(s1);
		sb.append('/');
		sb.append(s2);
		return sb.toString();
	}
	
	protected final String createPath(final String s1, final String s2, final String s3) {
		final StringBuilder sb= new StringBuilder(this.path.length() + s1.length() + s2.length() +  s3.length() + 2);
		sb.append(this.path);
		sb.append(s1);
		sb.append('/');
		sb.append(s2);
		sb.append('/');
		sb.append(s3);
		return sb.toString();
	}
	
	protected final String createPath(final String s1, final String s2, final String s3, final String s4) {
		final StringBuilder sb= new StringBuilder(this.path.length() + s1.length() + s2.length() +  s3.length() + s4.length() + 3);
		sb.append(this.path);
		sb.append(s1);
		sb.append('/');
		sb.append(s2);
		sb.append('/');
		sb.append(s3);
		sb.append('/');
		sb.append(s4);
		return sb.toString();
	}
	
	protected URI createUrl(final String path) {
		try {
			return new URI(this.url.getScheme(), null,
					this.url.getHost(), this.url.getPort(),
					path, null, null);
		}
		catch (final URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	protected StatusException onCancelled() {
		return new StatusException(Status.CANCEL_STATUS);
	}
	
	protected StatusException onTimeout(final @Nullable Throwable e) {
		return new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID, TIMEOUT_ERROR,
				"R help server access - Timeout.",
				e ));
	}
	
	protected StatusException onConnectError(final @Nullable Throwable e) {
		return new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID, CONNECT_ERROR,
				"R help server access - Failed to connect to server.",
				e ));
	}
	
	protected StatusException onFailed(final @Nullable Throwable e) {
		final Status status= new ErrorStatus(RHelpCore.BUNDLE_ID,
				"R help server access - Failed to perform search.",
				e );
		CommonsRuntime.log(status);
		return new StatusException(status);
	}
	
	
	protected abstract @Nullable InputStream getDataStreamIn(final URI url,
			final @Nullable String eTag,
			final String accept) throws StatusException, ResponseException;
	
	protected abstract InputStream getDataStreamIn(final URI url, final @NonNull String[] params, final byte[] requestData,
			final String accept) throws StatusException, ResponseException;
	
	protected abstract byte[] getDataStreamBytes(final URI url, final @NonNull String[] params,
			final String accept,
			final int timeout, final @Nullable ProgressMonitor m) throws StatusException, ResponseException;
	
	
	public boolean loadREnvHelpData(final REnvHelpConfiguration rEnvConfig,
			final long currentStamp, final Controller saveCheck) throws Exception {
		try (final InputStream in= getDataStreamIn(
					createUrl(createPath(ServerApi.BASIC_DATA)),
					ServerApi.createETag(currentStamp),
					this.dsVersionedAcceptHeader )) {
			if (in == null) {
				return false;
			}
			return this.serUtil.save(rEnvConfig, in, saveCheck);
		}
	}
	
	public <T> @Nullable T loadREnvHelpData(final long currentStamp,
			final Function<@Nullable InputStream, T> reader) throws Exception {
		try (final InputStream in= getDataStreamIn(
					createUrl(createPath(ServerApi.BASIC_DATA)),
					ServerApi.createETag(currentStamp),
					this.dsVersionedAcceptHeader )) {
			return reader.apply(in);
		}
	}
	
	
	@Override
	public List<RHelpPage> getPagesForTopic(final String topic, final Map<String, RPkgHelp> packageMap,
			final int timeout, final @Nullable ProgressMonitor m) throws StatusException {
		try (final DataStream in= DataStream.get(new ByteArrayInputStream(getDataStreamBytes(
					createUrl(createPath(ServerApi.PAGES)), new @NonNull String[] {
						ServerApi.TOPIC_PARAM, topic,
					},
					this.dsModestAcceptHeader,
					timeout, m )))) {
			final int n= in.readInt();
			final List<RHelpPage> pages= new ArrayList<>(n);
			for (int i= 0; i < n; i++) {
				final String pkgName= nonNullAssert(in.readString());
				final String pageName= nonNullAssert(in.readString());
				final RPkgHelp pkgHelp= packageMap.get(pkgName);
				if (pkgHelp != null) {
					final RHelpPage page= pkgHelp.getPage(pageName);
					if (page != null) {
						pages.add(page);
					}
				}
			}
			return pages;
		}
		catch (final StatusException e) {
			throw e;
		}
		catch (final Exception e) {
//			throw handleException(e);
			return ImCollections.emptyList();
		}
	}
	
	@Override
	public @Nullable String getHtmlPage(final RPkgHelp pkgHelp, final String pageName,
			final @Nullable String queryString,
			final int timeout, final @Nullable ProgressMonitor m) throws StatusException {
		try (final DataStream in= DataStream.get(new ByteArrayInputStream(getDataStreamBytes(
					createUrl(createPath(ServerApi.PKGS, pkgHelp.getName(), ServerApi.PAGES, pageName)),
							(queryString != null) ? new @NonNull String[] {
								ServerApi.QUERY_STRING_PARAM, queryString,
							} : NO_PARAMS,
							this.dsModestAcceptHeader,
							timeout, m )))) {
			return in.readString();
		}
		catch (final NotFoundException e) {
			return null;
		}
		catch (final StatusException e) {
			throw e;
		}
		catch (final Exception e) {
			throw onFailed(e);
		}
	}
	
	@Override
	public void search(final RHelpSearchQuery searchQuery, final List<RPkgHelp> packageList,
			final Map<String, RPkgHelp> packageMap,
			final RHelpSearchRequestor requestor) throws StatusException {
		final ByteArrayOutputStream requestData= new ByteArrayOutputStream();
		try (final DataStream out= DataStream.get(requestData)) {
			out.writeInt(searchQuery.getSearchType());
			out.writeString(searchQuery.getSearchString());
			out.writeStringList(searchQuery.getEnabledFields());
			out.writeStringList(searchQuery.getKeywords());
			out.writeStringList(searchQuery.getPackages());
		}
		catch (final Exception e) {
			throw onFailed(e);
		}
		try (
			final var inputStream= getDataStreamIn(createUrl(createPath(ServerApi.SEARCH)), new @NonNull String[] {
						ServerApi.MAX_FRAGMENTS_PARAM, Integer.toString(requestor.getMaxFragments()),
					}, requestData.toByteArray(),
					this.dsModestAcceptHeader );
			final DataStream in= DataStream.get(inputStream) ) {
			while (true) {
				final byte matchType= in.readByte();
				switch (matchType) {
				case ServerApi.END_MATCH:
					return;
				case ServerApi.PAGE_MATCH: {
					final String pkgName= in.readNonNullString();
					final String pageName= in.readNonNullString();
					final float score= in.readFloat();
					final int matchCount= in.readInt();
					final MatchFragment[] fragments;
					if (matchCount >= 0) {
						final int nFragments= in.readInt();
						fragments= new @NonNull MatchFragment[nFragments];
						for (int i= 0; i < nFragments; i++) {
							final String field= in.readNonNullString();
							final String text= in.readNonNullString();
							fragments[i]= new RHelpSearchMatchImpl.Fragment(field, text);
						}
					}
					else {
						fragments= null;
					}
					
					final RPkgHelp pkgHelp= packageMap.get(pkgName);
					if (pkgHelp != null) {
						final RHelpPage page= pkgHelp.getPage(pageName);
						if (page != null) {
							final RHelpSearchMatchImpl match= (matchCount >= 0) ?
									new RHelpSearchMatchImpl(page, score, matchCount, fragments) :
									new RHelpSearchMatchImpl(page, score);
							requestor.matchFound(match);
						}
					}
					continue;
				}
				default:
					throw new IOException("type= " + matchType);
				}
			}
		}
		catch (final StatusException e) {
			throw e;
		}
		catch (final Exception e) {
			throw onFailed(e);
		}
	}
	
}
