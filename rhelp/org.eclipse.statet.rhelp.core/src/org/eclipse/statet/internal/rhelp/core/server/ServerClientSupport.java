/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.server;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rhelp.core.RHelpWebapp;
import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rhelp.core.RHelpCore;
import org.eclipse.statet.rhelp.core.http.RHelpHttpService;


@NonNullByDefault
public abstract class ServerClientSupport {
	
	
	private static @Nullable ServerClientSupport instance;
	
	
	public static synchronized ServerClientSupport getInstance() throws StatusException {
		final ServerClientSupport impl= instance;
		if (impl == null) {
			throw new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID,
					"R-help server support not available." ));
		}
		return impl;
	}
	
	public static synchronized void init(final ServerClientSupport impl) {
		if (instance != null) {
			throw new IllegalStateException();
		}
		instance= impl;
	}
	
	
	public ServerClientSupport() {
	}
	
	
	public URI checkServerUri(final @Nullable URI uri) throws URISyntaxException {
		return checkServerUri(uri, "https");
	}
	
	protected URI checkServerUri(final @Nullable URI uri, final String defaultScheme)
			throws URISyntaxException {
		if (uri == null) {
			throw new UnsupportedOperationException("The uri is missing.");
		}
		String scheme= uri.getScheme();
		if (scheme == null) {
			scheme= defaultScheme;
		}
		final String host= uri.getHost();
		if (host == null) {
			throw new UnsupportedOperationException(
					String.format("The uri '%1$s' is invalid: missing host.", uri));
		}
		final String path= uri.getPath();
		if (path == null || path.length() <= 1) {
			throw new UnsupportedOperationException(
					String.format("The url '%1$s' is invalid: missing R environment id.", uri));
		}
		return new URI(scheme, uri.getUserInfo(), host, uri.getPort(), path, null, null);
	}
	
	public URI toServerBrowseUrl(final REnvHelpConfiguration rEnvConfig, final String path)
			throws URISyntaxException {
		final URI uri= checkServerUri(rEnvConfig.getStateSharedServerUri());
		
		final String uriPath= nonNullAssert(uri.getPath());
		final int idStart= uriPath.lastIndexOf('/');
		
		final var pathBuilder= new StringBuilder(uriPath.length() + path.length() + 13);
		pathBuilder.append(uriPath, 0, idStart);
		pathBuilder.append(RHelpWebapp.CONTEXT_PATH + '/' + RHelpHttpService.BROWSE_TARGET);
		pathBuilder.append(uriPath, idStart, uriPath.length()); // /rEnvId
		pathBuilder.append(path);
		return new URI(uri.getScheme(), null, uri.getHost(), uri.getPort(),
				pathBuilder.toString(), null, null );
	}
	
	
	protected abstract ServerREnvHelpAccess newREnvHelpAccess(final URI url) throws Exception;
	
	public ServerREnvHelpAccess getREnvHelpAccess(final REnvHelpConfiguration rEnvConfig)
			throws StatusException {
		try {
			final URI url= checkServerUri(rEnvConfig.getStateSharedServerUri());
			
			return newREnvHelpAccess(url);
		}
		catch (final Exception e) {
			throw new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID,
					"An error occured when initializing access to R help server",
					e ));
		}
	}
	
	
}
