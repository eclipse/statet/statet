/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.core.index;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rhelp.core.DocResource;


@NonNullByDefault
public class RDocResource extends DocResource {
	
	
	public static final ImList<DocResource> R_MANUALS= ImCollections.newList(
			new RDocResource("manual/R-intro.html", "An Introduction to R"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("manual/R-data.html", "R Data Import/Export"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("manual/R-lang.html", "The R Language Definition"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("manual/R-exts.html", "Writing R Extensions"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("manual/R-admin.html", "R Installation and Administration"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("manual/R-ints.html", "R Internals") ); //$NON-NLS-1$ //$NON-NLS-2$
	
	public static final ImList<DocResource> R_MISCS= ImCollections.newList(
			new RDocResource("html/about.html", "About R"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("COPYING", "License"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("AUTHORS", "Authors"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("THANKS", "Thanks"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("html/resources.html", "Resources"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("manual/R-FAQ.html", "Frequently Asked Questions"), //$NON-NLS-1$ //$NON-NLS-2$
			new RDocResource("html/rw-FAQ.html", "FAQ for Windows port") ); //$NON-NLS-1$ //$NON-NLS-2$
	
	
	private RDocResource(final String url, final String title) {
		super(title, url, (url.endsWith(".html")) ? toPdf(url) : null);
	}
	
	
}
