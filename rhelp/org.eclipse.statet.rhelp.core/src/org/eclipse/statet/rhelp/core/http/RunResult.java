/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.Status;


@NonNullByDefault
public class RunResult {
	
	
	private final Status status;
	
	private final @Nullable String html;
	
	
	public RunResult(final Status status, final @Nullable String html) {
		this.status= status;
		this.html= html;
	}
	
	
	public Status getStatus() {
		return this.status;
	}
	
	public @Nullable String getHtml() {
		return this.html;
	}
	
	
}
