/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http.ee8.jetty;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.ee8.nested.ResourceService;
import org.eclipse.jetty.http.HttpField;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.PreEncodedHttpField;
import org.eclipse.jetty.http.content.HttpContent;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.resource.Resources;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.rhelp.core.server.jetty.ExtMimeTypes;
import org.eclipse.statet.internal.rhelp.core.server.jetty.PathResourceHttpContentFactory;
import org.eclipse.statet.rhelp.core.http.MediaTypeProvider;
import org.eclipse.statet.rhelp.core.http.ee8.ResourceHandler;


@NonNullByDefault
public class JettyResourceHandler extends ResourceService implements ResourceHandler {
	
	
	private final ExtMimeTypes mediaTypes;
	
	
	public JettyResourceHandler(final MediaTypeProvider defaultTypes) {
		this.mediaTypes= new ExtMimeTypes(defaultTypes);
		final PathResourceHttpContentFactory contentFactory= new PathResourceHttpContentFactory(this.mediaTypes);
		setHttpContentFactory(contentFactory);
		setDirAllowed(false);
	}
	
	
	@Override
	public void setSpecialMediaTypes(final MediaTypeProvider types) {
		this.mediaTypes.setSpecialMediaTypes(types);
	}
	
	@Override
	public void setCacheControl(final String value) {
		setCacheControl(new PreEncodedHttpField(HttpHeader.CACHE_CONTROL, value));
	}
	
	@Override
	public void doGet(final Path path,
			final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final HttpContent content= ((PathResourceHttpContentFactory)getHttpContentFactory())
				.getContent(path, path.toString());
		boolean releaseContent= true;
		try {
			{	final Resource resource;
				if (content == null || (resource= content.getResource()) == null
						|| Resources.missing(resource) ) {
					notFound(req, resp);
					return;
				}
				if (resource.isDirectory()) {
					resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
				}
			}
			
			if (!passConditionalHeaders(req, resp, content)) {
				return;
			}
			
			final Enumeration<String> reqRanges= req.getHeaders(HttpHeader.RANGE.asString());
			
			{	final HttpField contentEncoding = content.getContentEncoding();
				if (contentEncoding != null) {
					resp.setHeader(contentEncoding.getName(), contentEncoding.getValue());
				}
			}
			releaseContent= sendData(req, resp, false, content, reqRanges);
		}
		finally {
			if (releaseContent && content != null) {
				content.release();
			}
		}
	}
	
}
