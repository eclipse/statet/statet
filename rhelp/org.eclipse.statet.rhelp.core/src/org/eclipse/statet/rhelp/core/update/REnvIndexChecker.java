/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.update;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rhelp.core.REnvHelpIndexChecker;
import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rhelp.core.RHelpCore;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rj.renv.core.RPkgBuilt;
import org.eclipse.statet.rj.renv.core.RPkgCompilation;
import org.eclipse.statet.rj.renv.runtime.RPkgManager;
import org.eclipse.statet.rj.renv.runtime.RPkgManagerDataset;
import org.eclipse.statet.rj.services.RService;


/**
 * Public interface to check if REnvHelp is up-to-date
 */
@NonNullByDefault
public class REnvIndexChecker {
	
	
	public static final int NOT_AVAILABLE= -1;
	/** the index is up-to-date */
	public static final int UP_TO_DATE= 0;
	/** complete index is missing */
	public static final int COMPLETE= 1;
	/** new or changed packages found in R */
	public static final int PACKAGES= 2;
	
	
	private final REnvHelpIndexChecker index;
	
	private final RPkgManager rPkgManager;
	
	
	public REnvIndexChecker(final REnvHelpConfiguration rEnvConfig,
			final RHelpManager rHelpManager, final RPkgManager rPkgManager) {
		this.index= new REnvHelpIndexChecker(rHelpManager, rEnvConfig);
		this.rPkgManager= rPkgManager;
	}
	
	
	public int check(final RService r, final ProgressMonitor m) throws StatusException {
		Exception errorCause= null;
		try {
			if (!this.index.beginCheck()) {
				return NOT_AVAILABLE;
			}
			if (this.index.needsComplete()) {
				return COMPLETE;
			}
			final RPkgManagerDataset rPkgDataset= this.rPkgManager.getDataset();
			if (rPkgDataset == null) {
				return NOT_AVAILABLE;
			}
			final RPkgCompilation<? extends RPkgBuilt> installed= rPkgDataset.getInstalled();
			this.index.beginPackageCheck();
			for (final String pkgName : installed.getNames()) {
				final RPkgBuilt pkgInfo= installed.getFirst(pkgName);
				if (pkgInfo == null) {
					continue;
				}
				this.index.checkPackage(pkgInfo);
			}
			this.index.endPackageCheck();
			
			if (this.index.needsComplete()) {
				return COMPLETE;
			}
			else if (this.index.hasPackageChanges()) {
				return PACKAGES;
			}
			else {
				return UP_TO_DATE;
			}
		}
//		catch (final CoreException e) {
//			this.index.cancelCheck();
//			if (e.getStatus().getSeverity() == IStatus.CANCEL) {
//				throw e;
//			}
//			errorCause= e;
//		}
		catch (final Exception e) {
			this.index.cancelCheck();
			errorCause= e;
		}
		finally {
			this.index.finalCheck();
		}
		throw new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID,
				"An error occurred when checking the package data.",
				errorCause ));
	}
	
	
	public boolean wasAlreadyReported() {
		return !this.index.hasNewChanges();
	}
	
	public int getNewPackageCount() {
		return this.index.getNewPackageCount();
	}
	
	public int getChangedPackageCount() {
		return this.index.getChangedPackageCount();
	}
	
	
	public void release() {
		this.index.release();
	}
	
}
