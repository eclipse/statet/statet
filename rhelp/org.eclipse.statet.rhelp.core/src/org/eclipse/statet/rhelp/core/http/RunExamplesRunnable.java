/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.InfoStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.rhelp.core.RHelpCore;
import org.eclipse.statet.rhelp.core.RHelpPage;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.ts.core.console.AbstractRConsoleRunnable;
import org.eclipse.statet.rj.ts.core.console.RConsoleService;


@NonNullByDefault
public class RunExamplesRunnable extends AbstractRConsoleRunnable {
	
	
	public static final String RUN_EXAMPLES_TASK_ID= "org.eclipse.statet.r.rhelp/RunExamples"; //$NON-NLS-1$
	
	
	private final RHelpPage helpPage;
	
	private Tool tool;
	
	private @Nullable RunResult result;
	
	
	public RunExamplesRunnable(final RHelpPage helpPage) {
		super(RUN_EXAMPLES_TASK_ID, String.format("Run Examples of '%1$s'", helpPage));
		this.helpPage= helpPage;
	}
	
	
	public RHelpPage getHelpPage() {
		return this.helpPage;
	}
	
	
	public @Nullable RunResult waitForResult() throws InterruptedException {
		synchronized (this) {
			if (this.result == null) {
				wait(60_000);
			}
			notifyAll();
			return this.result;
		}
	}
	
	public void cancel() {
		synchronized (this) {
			this.tool.getQueue().remove(this);
			if (this.result == null) {
				this.result= new RunResult(Status.CANCEL_STATUS, null);
			}
			notifyAll();
		}
	}
	
	
	@Override
	public boolean changed(final int event, final Tool tool) {
		Status status;
		switch (event) {
		case ADDING_TO:
		case MOVING_TO:
			this.tool= tool;
			if (!tool.isTerminated()) {
				return true;
			}
			//$FALL-THROUGH$
		case REMOVING_FROM:
		case BEING_ABANDONED:
		case FINISHING_CANCEL:
			status= Status.CANCEL_STATUS;
			break;
		case FINISHING_OK:
		case FINISHING_ERROR:
			status= null;
			break;
		default:
			return true;
		}
		synchronized (this) {
			if (this.result == null) {
				if (status == null) {
					status= new ErrorStatus(RHelpCore.BUNDLE_ID, "An error occurred when running the examples.");
				}
				this.result= new RunResult(status, null);
			}
		}
		return true;
	}
	
	@Override
	protected void run(final RConsoleService r, final ProgressMonitor m) throws StatusException {
		synchronized (this) {
			if (this.result != null) {
				return;
			}
		}
		
		try {
			final var codelines= RDataUtils.checkRCharVector(r.createFunctionCall("utils::example") //$NON-NLS-1$
					.addChar("topic", this.helpPage.getTopics().getFirst()) //$NON-NLS-1$
					.addChar("package", this.helpPage.getPackage().getName()) //$NON-NLS-1$
					.addLogi("character.only", true) //$NON-NLS-1$
					.addLogi("give.lines", true) //$NON-NLS-1$
					.evalData(m) ).getData();
			
			int startIdx= 0;
			int endIdx= RDataUtils.checkIntLength(codelines);
			while (startIdx < endIdx) {
				if (!codelines.isNA(startIdx)) {
					final String line= codelines.getChar(startIdx);
					if (!(line.isEmpty() || line.startsWith("### "))) { //$NON-NLS-1$
						break;
					}
					if (line.startsWith("### ** Examples")) { //$NON-NLS-1$
						startIdx++;
						break;
					}
				}
				startIdx++;
			}
			while (startIdx < endIdx) {
				if (!codelines.isNA(startIdx)) {
					final String line= codelines.getChar(startIdx);
					if (!line.isEmpty()) {
						break;
					}
				}
				startIdx++;
			}
			while (endIdx > startIdx) {
				if (!codelines.isNA(endIdx - 1)) {
					final String line= codelines.getChar(endIdx - 1);
					if (!line.isEmpty()) {
						break;
					}
				}
				endIdx--;
			}
			
			r.briefAboutToChange();
			r.handleStatus(new InfoStatus(RHelpCore.BUNDLE_ID,
							String.format("Examples of '%1$s'", this.helpPage) ),
					m );
			
			final var isPkgAttachedFCall= r.createFunctionCall("%in%") //$NON-NLS-1$
					.addChar("x", this.helpPage.getPackage().getName()) //$NON-NLS-1$
					.add("table", "base::.packages()"); //$NON-NLS-1$ //$NON-NLS-2$
			if (!RDataUtils.isSingleLogiTrue(isPkgAttachedFCall.evalData(m))) {
				r.submitToConsole("# First load the package", m);
				r.submitToConsole(String.format("library(\"%1$s\")", //$NON-NLS-1$
						this.helpPage.getPackage().getName() ), m);
				if (!RDataUtils.isSingleLogiTrue(isPkgAttachedFCall.evalData(m))) {
					return;
				}
				r.briefChanged(RConsoleService.PACKAGE_CHANGE);
				r.submitToConsole("", m); //$NON-NLS-1$
				r.briefAboutToChange();
			}
			
			for (int idx= startIdx; idx < endIdx; idx++) {
				if (m.isCanceled()) {
					throw new StatusException(Status.CANCEL_STATUS);
				}
				if (!codelines.isNA(idx)) {
					final String line= codelines.getChar(idx);
					r.submitToConsole(line, m);
				}
			}
			
			synchronized (this) {
				this.result= new RunResult(Status.OK_STATUS, null);
				notifyAll();
			}
		}
		catch (final UnexpectedRDataException e) {
			throw new StatusException(new ErrorStatus(RHelpCore.BUNDLE_ID,
					"An error occurred when running examples of R help.",
					e ));
		}
	}
	
}
