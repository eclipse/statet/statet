/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http.ee10;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rhelp.core.http.MediaTypeProvider;


@NonNullByDefault
public class SimpleResourceHandler implements ResourceHandler {
	
	
	private final MediaTypeProvider defaultMediaTypes;
	private @Nullable MediaTypeProvider specialMediaTypes;
	
	private @Nullable String cacheControl;
	
	
	public SimpleResourceHandler(final MediaTypeProvider mediaTypes) {
		this.defaultMediaTypes= nonNullAssert(mediaTypes);
	}
	
	
	@Override
	public void setSpecialMediaTypes(final MediaTypeProvider types) {
		this.specialMediaTypes= types;
	}
	
	private @Nullable String getMediaType(final String fileName) {
		String type= null;
		{	final var specialMediaTypes= this.specialMediaTypes;
			if (specialMediaTypes != null) {
				type= specialMediaTypes.getMediaTypeString(fileName);
			}
		}
		if (type == null) {
			type= this.defaultMediaTypes.getMediaTypeString(fileName);
		}
		return type;
	}
	
	@Override
	public void setCacheControl(final String value) {
		this.cacheControl= value;
	}
	
	@Override
	public void doGet(final Path file,
			final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		try (final var in= Files.newInputStream(file)) {
			in.available();
			
			{	final String mediaType= getMediaType(nonNullAssert(file.getFileName()).toString());
				if (mediaType != null) {
					resp.setContentType(mediaType);
				}
			}
			{	final String cacheControl= this.cacheControl;
				if (cacheControl != null) {
					resp.setHeader("Cache-Control", cacheControl); //$NON-NLS-1$
				}
			}
			
			try (final var out= resp.getOutputStream()) {
				in.transferTo(out);
			}
		}
	}
	
}
