/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public class RHelpTopicLookup {
	
	
	private final REnv rEnv;
	
	private final String topic;
	
	private final ImList<RHelpPage> pages;
	
	
	public RHelpTopicLookup(final REnv rEnv, final String topic, final ImList<RHelpPage> pages) {
		this.rEnv= rEnv;
		this.topic= topic;
		this.pages= pages;
	}
	
	
	public REnv getREnv() {
		return this.rEnv;
	}
	
	public String getTopic() {
		return this.topic;
	}
	
	public ImList<RHelpPage> getPages() {
		return this.pages;
	}
	
}
