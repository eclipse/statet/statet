/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RHelpSearchMatch {
	
	
	String PRE_TAGS_PREFIX= "<SMATCH-"; //$NON-NLS-1$
	String POST_TAGS_PREFIX= "</SMATCH-"; //$NON-NLS-1$
	
	@NonNull String[] PRE_TAGS= new @NonNull String[] {
			"<SMATCH-A>", //$NON-NLS-1$
			"<SMATCH-B>", //$NON-NLS-1$
			"<SMATCH-C>", //$NON-NLS-1$
			"<SMATCH-D>", //$NON-NLS-1$
			"<SMATCH-E>", //$NON-NLS-1$
			"<SMATCH-F>", //$NON-NLS-1$
			"<SMATCH-G>", //$NON-NLS-1$
			"<SMATCH-H>", //$NON-NLS-1$
			"<SMATCH-I>", //$NON-NLS-1$
			"<SMATCH-J>", //$NON-NLS-1$
	};
	@NonNull String[] POST_TAGS= new @NonNull String[] {
			"</SMATCH-A>", //$NON-NLS-1$
			"</SMATCH-B>", //$NON-NLS-1$
			"</SMATCH-C>", //$NON-NLS-1$
			"</SMATCH-D>", //$NON-NLS-1$
			"</SMATCH-E>", //$NON-NLS-1$
			"</SMATCH-F>", //$NON-NLS-1$
			"</SMATCH-G>", //$NON-NLS-1$
			"</SMATCH-H>", //$NON-NLS-1$
			"</SMATCH-I>", //$NON-NLS-1$
			"</SMATCH-J>", //$NON-NLS-1$
	};
	Pattern ALL_TAGS_PATTERN= Pattern.compile("\\<\\/?SMATCH\\-[A-Z]\\>"); //$NON-NLS-1$
	
	interface MatchFragment {
		
		String getField();
		
		@Nullable String getFieldLabel();
		
		String getText();
		
	}
	
	
	RHelpPage getPage();
	
	float getScore();
	
	int getMatchCount();
	
	MatchFragment @Nullable [] getBestFragments();
	
}
