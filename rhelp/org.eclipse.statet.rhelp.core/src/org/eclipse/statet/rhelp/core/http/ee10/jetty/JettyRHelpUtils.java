/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http.ee10.jetty;

import java.util.concurrent.Executor;

import jakarta.servlet.ServletContext;

import org.eclipse.jetty.util.thread.QueuedThreadPool;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rhelp.core.http.MediaTypeProvider;
import org.eclipse.statet.rhelp.core.http.ee10.ResourceHandler;
import org.eclipse.statet.rhelp.core.http.ee10.ServletMediaTypeProvider;


@NonNullByDefault
public class JettyRHelpUtils {
	
	
	public static MediaTypeProvider getMediaTypes(final ServletContext context) {
		return new ServletMediaTypeProvider(context);
	}
	
	public static ResourceHandler newResourceHandler(final ServletContext context) {
		return new JettyResourceHandler(getMediaTypes(context));
	}
	
	
	private static final String JETTY_EXECUTOR_KEY= "org.eclipse.jetty.server.Executor"; //$NON-NLS-1$
	
	private static @Nullable Executor executor;
	
	public synchronized static Executor getExecutor() {
		Executor executor= JettyRHelpUtils.executor;
		if (executor == null) {
			executor= new QueuedThreadPool(132, 2);
			JettyRHelpUtils.executor= executor;
		}
		return executor;
	}
	
	public static void ensureExecutor(final ServletContext context) {
		if (context.getAttribute(JETTY_EXECUTOR_KEY) == null) {
			context.setAttribute(JETTY_EXECUTOR_KEY, getExecutor());
		}
	}
	
	
	private JettyRHelpUtils() {
	}
	
}
