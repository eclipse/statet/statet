/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class TopicDocResource extends DocResource {
	
	
	private final String topic;
	
	private final String sourcePath;
	private final String rPath;
	
	
	public TopicDocResource(final String topic, final String title, final String path,
			final @Nullable String pdfPath, final String sourcePath, final String rPath) {
		super(title, path, pdfPath);
		this.topic= topic;
		this.sourcePath= sourcePath;
		this.rPath= rPath;
	}
	
	
	public String getTopic() {
		return this.topic;
	}
	
	public String getSourcePath() {
		return this.sourcePath;
	}
	
	public String getRPath() {
		return this.rPath;
	}
	
}
