/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.rhelp.core.RHelpCoreInternals;


@NonNullByDefault
public final class RHelpTopicEntry implements Comparable<RHelpTopicEntry> {
	
	
	private final String topic;
	
	private final RHelpPage page;
	
	
	public RHelpTopicEntry(final String alias, final RHelpPage page) {
		this.topic= alias;
		this.page= page;
	}
	
	
	public String getTopic() {
		return this.topic;
	}
	
	public RHelpPage getPage() {
		return this.page;
	}
	
	
	@Override
	public int hashCode() {
		return this.topic.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RHelpTopicEntry other
						&& this.topic.equals(other.topic) && this.page.equals(other.page) ));
	}
	
	@Override
	public int compareTo(final RHelpTopicEntry o) {
		final int diff= RHelpCoreInternals.R_NAMES_COLLATOR.compare(this.topic, o.topic);
		if (diff != 0) {
			return diff;
		}
		return this.page.compareTo(o.page);
	}
	
}
