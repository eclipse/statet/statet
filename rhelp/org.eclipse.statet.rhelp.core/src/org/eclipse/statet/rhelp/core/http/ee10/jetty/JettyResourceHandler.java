/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core.http.ee10.jetty;

import java.io.IOException;
import java.nio.file.Path;

import jakarta.servlet.AsyncContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.eclipse.jetty.ee10.servlet.ServletApiRequest;
import org.eclipse.jetty.ee10.servlet.ServletApiResponse;
import org.eclipse.jetty.ee10.servlet.ServletContextRequest;
import org.eclipse.jetty.http.content.HttpContent;
import org.eclipse.jetty.server.ResourceService;
import org.eclipse.jetty.util.Blocker;
import org.eclipse.jetty.util.Callback;
import org.eclipse.jetty.util.ExceptionUtil;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.resource.Resources;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.rhelp.core.server.jetty.ExtMimeTypes;
import org.eclipse.statet.internal.rhelp.core.server.jetty.PathResourceHttpContentFactory;
import org.eclipse.statet.rhelp.core.http.MediaTypeProvider;
import org.eclipse.statet.rhelp.core.http.ee10.ResourceHandler;


@NonNullByDefault
public class JettyResourceHandler extends ResourceService implements ResourceHandler {
	
	
	private final ExtMimeTypes mediaTypes;
	
	
	public JettyResourceHandler(final MediaTypeProvider defaultTypes) {
		this.mediaTypes= new ExtMimeTypes(defaultTypes);
		final PathResourceHttpContentFactory contentFactory= new PathResourceHttpContentFactory(this.mediaTypes);
		setHttpContentFactory(contentFactory);
		setDirAllowed(false);
	}
	
	
	@Override
	public void setSpecialMediaTypes(final MediaTypeProvider types) {
		this.mediaTypes.setSpecialMediaTypes(types);
	}
	
	@Override
	public void setCacheControl(final String value) {
		super.setCacheControl(value);
	}
	
	@Override
	public void doGet(final Path path,
			final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
		final HttpContent content= ((PathResourceHttpContentFactory)getHttpContentFactory())
				.getContent(path, path.toString());
		boolean releaseContent= true;
		try {
			{	final Resource resource;
				if (content == null || (resource= content.getResource()) == null
						|| Resources.missing(resource) ) {
					notFound(req, resp);
					return;
				}
				if (resource.isDirectory()) {
					resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
				}
			}
			
			final var servletContextRequest= ServletContextRequest.getServletContextRequest(req);
			final var servletContextResponse= servletContextRequest.getServletContextResponse();
			final var servletChannel= servletContextRequest.getServletChannel();
			
			if (!(req instanceof ServletApiRequest)
					|| !(resp instanceof ServletApiResponse)
					|| servletContextResponse.isWriting() || servletContextResponse.isStreaming() ) {
				throw new UnsupportedOperationException();
			}
			final var coreRequest= servletChannel.getRequest();
			final var coreResponse= servletChannel.getResponse();
			
			if (coreResponse.isCommitted()) {
				return;
			}
			
			final long contentLength = content.getContentLengthValue();
			
			if (req.isAsyncSupported()
					&& (contentLength < 0 || contentLength > coreRequest.getConnectionMetaData().getHttpConfiguration().getOutputBufferSize())) {
				final var asyncContext= req.startAsync();
				final Callback callback= new AsyncContextCallback(asyncContext, resp);
				releaseContent= false;
				doGet(coreRequest, coreResponse, callback, content);
			}
			else {
				try (final Blocker.Callback callback= Blocker.callback()) {
					releaseContent= false;
					doGet(coreRequest, coreResponse, callback, content);
					callback.block();
				}
				catch (final Exception e) {
					throw new ServletException(e);
				}
			}
		}
		finally {
			if (releaseContent && content != null) {
				content.release();
			}
		}
	}
	
	
	protected void notFound(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
	
	
	private static class AsyncContextCallback implements Callback {
		
		private final AsyncContext asyncContext;
		private final HttpServletResponse response;
		
		private AsyncContextCallback(final AsyncContext asyncContext, final HttpServletResponse response) {
			this.asyncContext= asyncContext;
			this.response= response;
		}
		
		@Override
		public void succeeded() {
			this.asyncContext.complete();
		}
		
		@Override
		public void failed(final @Nullable Throwable x) {
			try {
				this.response.sendError(-1);
			}
			catch (final IOException e) {
				ExceptionUtil.addSuppressedIfNotAssociated(x, e);
			}
			finally {
				this.asyncContext.complete();
			}
		}
	}
	
}
