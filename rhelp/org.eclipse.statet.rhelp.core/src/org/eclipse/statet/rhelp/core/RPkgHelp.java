/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.RNumVersion;
import org.eclipse.statet.rj.renv.core.RPkg;
import org.eclipse.statet.rj.renv.core.RPkgDescription;


@NonNullByDefault
public interface RPkgHelp extends RPkg, Comparable<RPkgHelp> {
	
	
	@Override
	String getName();
	@Override
	RNumVersion getVersion();
	
	String getTitle();
	RPkgDescription getPkgDescription();
	
	REnv getREnv();
	
	
	ImList<RHelpPage> getPages();
	
	@Nullable RHelpPage getPage(final String name);
	
	ImList<RHelpTopicEntry> getTopics();
	@Nullable RHelpPage getPageForTopic(final String topic);
	
}
