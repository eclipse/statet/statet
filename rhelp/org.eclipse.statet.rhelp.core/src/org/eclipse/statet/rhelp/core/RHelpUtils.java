/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public class RHelpUtils {
	
	
	public static @Nullable REnv getREnv(final Object helpObject) {
		if (helpObject instanceof REnvHelp) {
			return ((REnvHelp) helpObject).getREnv();
		}
		if (helpObject instanceof RPkgHelp) {
			return ((RPkgHelp) helpObject).getREnv();
		}
		if (helpObject instanceof RHelpPage) {
			return ((RHelpPage) helpObject).getPackage().getREnv();
		}
		if (helpObject instanceof RHelpTopicLookup) {
			return ((RHelpTopicLookup) helpObject).getREnv();
		}
		if (helpObject instanceof RHelpTopicEntry) {
			return ((RHelpTopicEntry) helpObject).getPage().getPackage().getREnv();
		}
		if (helpObject instanceof RHelpSearchQuery) {
			return ((RHelpSearchQuery) helpObject).getREnv();
		}
		return null;
	}
	
	
	private RHelpUtils() {
	}
	
}
