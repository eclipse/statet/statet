# Eclipse StatET - R Help Server

  * Note the NOTICE file for project information.
  * See also additional information in the [project wiki][statet wiki r-help-server].

[statet wiki r-help-server]:https://gitlab.eclipse.org/groups/eclipse/statet/-/wikis/Usage/R-Help-Server


## Requirements

  * Java
  * RJSrv

## Configuration

Edit the configuration(s) for the R environments in the folder `renvs`:
  - One configuration folder per R environment
  - The name of the folder is the id of the R environment
  - The properties file `renv.properties` in the folder specifies the R environment.
      - To create the file initialized with the default values of an R installation, run:
        `R -e "rj::.renv.createConfigProperties(out= '<renv-folder>')"`

The server is based on Spring Boot; see [documentation][spring-boot doc] of Spring Boot e.g. how to
configure the webserver.

[spring-boot doc]:https://docs.spring.io/spring-boot/docs/3.4.3/reference/html/


## Run the Server

To run the server just start the jar:

    java -jar org.eclipse.statet.rhelp.server.jar &

### Special modes
  * `-index-and-exit` - creates or updates the help index only


## Access the Server

### URI for API
The URI to access the help of an R environment via IDE is specified as follows:

    http://<name or ip>:<port>/<env id>

For example: `http://serverwithhelp:8080/r35`

The following defaults can be optionally omitted:
  - `<port>` = 80
  - `<env id>` = default

So `http://serverwithhelp/` is equal to `http://serverwithhelp:80/default`.

### Web
The URL to access the help using a webbrowser is:

    http://<name or ip>:<port>/rhelp/browse/<env id>/

