/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server.renv;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.Statuses;

import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnvManager;


@WebServlet(urlPatterns= "/renvs")
@NonNullByDefault
public class REnvsServlet extends HttpServlet {
	
	
	private static final long serialVersionUID= 1L;
	
	
	@Autowired
	private REnvManager rEnvManager;
	
	
	public REnvsServlet() {
	}
	
	
	@Override
	public void init(final ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/html;charset=UTF-8"); //$NON-NLS-1$
		resp.setHeader("Cache-Control", "max-age=30, must-revalidate"); //$NON-NLS-1$ //$NON-NLS-2$
		final PrintWriter writer= resp.getWriter();
		writer.write("<html><body>");
		
		writer.write("<table>");
		writer.write("<tr><th>Id</th><th>Name</th><th>Status</th>");
		for (final REnv rEnv : this.rEnvManager.list()) {
			final REnvConfiguration config= rEnv.get(REnvConfiguration.class);
			writer.write("<tr><td>");
			writer.write(rEnv.getId());
			writer.write("</td><td>");
			writer.write(rEnv.getName());
			writer.write("</td><td>");
			if (config != null) {
				writer.write(Statuses.getSeverityString(config.getValidationStatus().getSeverity()));
			}
			else {
				writer.write("MISSING");
			}
			writer.write("</td></tr>");
		}
		
		writer.write("</html></body>");
		writer.close();
	}
	
}
