/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rhelp.server;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;

import org.eclipse.jetty.util.URIUtil;

import org.springframework.beans.factory.annotation.Autowired;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rhelp.core.http.ee10.RHelpHttpServlet;
import org.eclipse.statet.rhelp.core.http.ee10.jetty.JettyRHelpUtils;


@WebServlet(urlPatterns= "/rhelp/browse/*")
@NonNullByDefault
public class RHelpServerBrowseServlet extends RHelpHttpServlet {
	
	
	private static final long serialVersionUID= 1L;
	
	
	private final RHelpManager rHelpManager;
	
	
	@Autowired
	public RHelpServerBrowseServlet(final RHelpManager rHelpManager) {
		this.rHelpManager= rHelpManager;
	}
	
	
	@Override
	public void init(final ServletConfig config) throws ServletException {
		super.init(config);
		
		init(this.rHelpManager,
				JettyRHelpUtils.newResourceHandler(config.getServletContext()),
				null );
		
//		init(this.appContext.getBean(RHelpManager.class));
	}
	
	
	/** Create relative path */
	@Override
	protected StringBuilder getServletPath(final HttpServletRequest req) {
		final String requestPath= URIUtil.canonicalPath(req.getPathInfo());
		final StringBuilder sb= new StringBuilder();
		for (int i= 1; i < requestPath.length(); ) {
			if (requestPath.charAt(i++) == '/') {
				sb.append("../"); //$NON-NLS-1$
			}
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 1);
		}
		return sb;
	}
	
}
