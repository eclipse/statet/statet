/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rhelp.server;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.jetty.client.HttpClient;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.opentest4j.AssertionFailedError;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.util.StreamUtils;

import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.rhelp.core.REnvHelpImpl;
import org.eclipse.statet.internal.rhelp.core.SerUtil;
import org.eclipse.statet.internal.rhelp.core.server.JettyREnvHelpAccess;
import org.eclipse.statet.rhelp.core.http.ee10.jetty.JettyRHelpUtils;


@NonNullByDefault
public class HttpAccesssTests extends AbstractDefaultAppTest {
	
	
	@LocalServerPort
	private int port;
	
	
	public HttpAccesssTests() {
	}
	
	
	private HttpClient getHttpClient() {
		final HttpClient httpClient= new HttpClient();
		httpClient.setExecutor(JettyRHelpUtils.getExecutor());
		httpClient.setAddressResolutionTimeout(10000);
		httpClient.setConnectTimeout(10000);
		return httpClient;
	}
	
	private URI getHelpUrl(final String envId) throws URISyntaxException {
		return new URI("http", null, "localhost", this.port, "/" + envId, null, null);
	}
	
	
	@Test
	@EnabledIfEnvironmentVariable(named = "STATET_TEST_FILES", matches = ".+")
	public void loadBasicData() throws Exception {
		assumeRHelpAvailable("default");
		
		final JettyREnvHelpAccess clientSupport= new JettyREnvHelpAccess(
				getHelpUrl("default"), getHttpClient() );
		final byte[] basicDataSerialized= clientSupport.loadREnvHelpData(
				REnvHelpImpl.NOT_AVAILABLE_STAMP, (in) -> {
					try {
						assertNotNull(in);
						return StreamUtils.copyToByteArray(in);
					}
					catch (final IOException e) {
						throw new AssertionFailedError("Error when reading data stream.", e);
					}
				});
		
		assertNotNull(basicDataSerialized);
		
		final DataStream in= DataStream.get(new ByteArrayInputStream(basicDataSerialized));
		assertEquals(SerUtil.CURRENT_VERSION, in.readVersion());
	}
	
	
}
