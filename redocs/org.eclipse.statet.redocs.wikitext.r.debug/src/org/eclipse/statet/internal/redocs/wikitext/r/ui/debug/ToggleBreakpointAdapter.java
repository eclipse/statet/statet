/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.debug;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.debug.ui.actions.IToggleBreakpointsTargetExtension;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.debug.ui.actions.RToggleBreakpointAdapter;
import org.eclipse.statet.redocs.wikitext.r.ui.editors.WikidocRweaveEditor;


public class ToggleBreakpointAdapter implements IToggleBreakpointsTargetExtension {
	
	
	private final RToggleBreakpointAdapter rAdapter;
	
	
	public ToggleBreakpointAdapter() {
		this.rAdapter= new RToggleBreakpointAdapter();
	}
	
	
	@Override
	public boolean canToggleLineBreakpoints(final IWorkbenchPart part, final ISelection selection) {
		final WikidocRweaveEditor editor= getREditor(part, selection);
		return (editor != null && editor.getSourceUnit() instanceof WorkspaceSourceUnit
				&& selection instanceof ITextSelection );
	}
	
	@Override
	public void toggleLineBreakpoints(final IWorkbenchPart part, final ISelection selection)
			throws CoreException {
		final WikidocRweaveEditor editor= getREditor(part, selection);
		if (editor != null && editor.getSourceUnit() instanceof WorkspaceSourceUnit
				&& selection instanceof ITextSelection) {
			if (isRChunk(editor, (ITextSelection) selection) ) {
				this.rAdapter.toggleLineBreakpoints(part, selection);
			}
			else {
				this.rAdapter.removeBreakpoints(part, selection, new NullProgressMonitor());
			}
		}
	}
	
	@Override
	public boolean canToggleMethodBreakpoints(final IWorkbenchPart part, final ISelection selection) {
		final WikidocRweaveEditor editor= getREditor(part, selection);
		return (editor != null && editor.getSourceUnit() instanceof WorkspaceSourceUnit
				&& selection instanceof ITextSelection );
	}
	
	@Override
	public void toggleMethodBreakpoints(final IWorkbenchPart part, final ISelection selection) throws CoreException {
		final WikidocRweaveEditor editor= getREditor(part, selection);
		if (editor != null && editor.getSourceUnit() instanceof WorkspaceSourceUnit
				&& selection instanceof ITextSelection) {
			if (isRChunk(editor, (ITextSelection) selection) ) {
				this.rAdapter.toggleMethodBreakpoints(part, selection);
			}
			else {
				this.rAdapter.removeBreakpoints(part, selection, new NullProgressMonitor());
			}
		}
	}
	
	@Override
	public boolean canToggleWatchpoints(final IWorkbenchPart part, final ISelection selection) {
		return false;
	}
	
	@Override
	public void toggleWatchpoints(final IWorkbenchPart part, final ISelection selection)
			throws CoreException {
	}
	
	@Override
	public boolean canToggleBreakpoints(final IWorkbenchPart part, final ISelection selection) {
		final WikidocRweaveEditor editor= getREditor(part, selection);
		return (editor != null && editor.getSourceUnit() instanceof WorkspaceSourceUnit
				&& selection instanceof ITextSelection );
	}
	
	@Override
	public void toggleBreakpoints(final IWorkbenchPart part, final ISelection selection)
			throws CoreException {
		final WikidocRweaveEditor editor= getREditor(part, selection);
		if (editor != null && editor.getSourceUnit() instanceof WorkspaceSourceUnit
				&& selection instanceof ITextSelection) {
			if (isRChunk(editor, (ITextSelection) selection) ) {
				this.rAdapter.toggleBreakpoints(part, selection);
			}
			else {
				this.rAdapter.removeBreakpoints(part, selection, new NullProgressMonitor());
			}
		}
	}
	
	private WikidocRweaveEditor getREditor(final IWorkbenchPart part, final ISelection selection) {
		if (part instanceof WikidocRweaveEditor) {
			return (WikidocRweaveEditor) part;
		}
		final Object adapter= part.getAdapter(SourceEditor.class);
		if (adapter instanceof WikidocRweaveEditor) {
			return (WikidocRweaveEditor) adapter;
		}
		return null;
	}
	
	private boolean isRChunk(final WikidocRweaveEditor editor, final ITextSelection selection) {
		final SourceViewer viewer= editor.getViewer();
		if (viewer != null) {
			try {
				final ITypedRegion partition= ((IDocumentExtension3) viewer.getDocument()).getPartition(
						editor.getDocumentContentInfo().getPartitioning(),
						selection.getOffset(), false );
				return RDocumentConstants.R_ANY_CONTENT_CONSTRAINT.matches(partition.getType());
			}
			catch (final BadLocationException | BadPartitioningException e) {}
		}
		return false;
	}
	
}
