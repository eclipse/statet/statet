/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.commonmark;

import static org.eclipse.statet.internal.redocs.wikitext.r.commonmark.core.RCommonmarkLanguage.COMMONMARK_RWEAVE_LANGUAGE_NAME;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager1;
import org.eclipse.statet.internal.redocs.wikitext.r.commonmark.core.RCommonmarkLanguage;
import org.eclipse.statet.redocs.wikitext.r.core.source.WikitextRweaveTemplateContextType;


public class CommonmarkRweavePlugin extends AbstractUIPlugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.redocs.wikitext.r.commonmark"; //$NON-NLS-1$
	
	
	public static final String DOC_CONTENT_ID= "org.eclipse.statet.redocs.contentTypes.CommonmarkRweave"; //$NON-NLS-1$
	
	public static final IContentType DOC_CONTENT_TYPE;
	
	static {
		final IContentTypeManager contentTypeManager= Platform.getContentTypeManager();
		DOC_CONTENT_TYPE= contentTypeManager.getContentType(DOC_CONTENT_ID);
	}
	
	
	public static final String TEMPLATES_WEAVE_DOCDEFAULT_CONTEXTTYPE= COMMONMARK_RWEAVE_LANGUAGE_NAME +
			WikitextRweaveTemplateContextType.WEAVE_DOCDEFAULT_CONTEXTTYPE_SUFFIX;
	
	public static final String TEMPLATES_QUALIFIER= BUNDLE_ID + "/codegen"; //$NON-NLS-1$
	
	public static final String NEWDOC_TEMPLATE_CATEGORY_ID= "CommonmarkRweave.NewDoc"; //$NON-NLS-1$
	
	
	private static CommonmarkRweavePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static CommonmarkRweavePlugin getInstance() {
		return instance;
	}
	
	public static void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private RCommonmarkLanguage markupLanguage;
	
	
	public CommonmarkRweavePlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		final WikitextMarkupLanguageManager1 markupLanguageManager= WikitextCore.getMarkupLanguageManager();
		
		this.markupLanguage= (RCommonmarkLanguage) markupLanguageManager
				.getLanguage(COMMONMARK_RWEAVE_LANGUAGE_NAME);
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	public RCommonmarkLanguage getMarkupLanguage() {
		return this.markupLanguage;
	}
	
}
