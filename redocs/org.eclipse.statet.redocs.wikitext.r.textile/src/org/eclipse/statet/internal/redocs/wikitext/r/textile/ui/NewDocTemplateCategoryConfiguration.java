/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.textile.ui;

import static org.eclipse.statet.internal.redocs.wikitext.r.textile.core.RTextileLanguage.TEXTILE_RWEAVE_LANGUAGE_NAME;

import org.eclipse.core.resources.IProject;
import org.eclipse.text.templates.ContextTypeRegistry;
import org.eclipse.text.templates.TemplatePersistenceData;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.StringPref;
import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.internal.redocs.wikitext.r.textile.TextileRweavePlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.templates.config.ITemplateCategoryConfiguration;
import org.eclipse.statet.ltk.ui.templates.config.ITemplateContribution;
import org.eclipse.statet.ltk.ui.templates.config.TemplateStoreContribution;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.redocs.wikitext.r.core.source.WikitextRweaveTemplateContextType;
import org.eclipse.statet.redocs.wikitext.r.ui.codegen.CodeGeneration;
import org.eclipse.statet.redocs.wikitext.r.ui.sourceediting.WikidocRweaveTemplateViewerConfigurator;


public class NewDocTemplateCategoryConfiguration
		implements ITemplateCategoryConfiguration {
	
	
	public static final String NEWDOC_DEFAULT_NAME_KEY= "NewDoc.Default.name"; //$NON-NLS-1$
	public static final Preference<String> NEWDOC_DEFAULT_NAME_PREF= new StringPref(
			TextileRweavePlugin.TEMPLATES_QUALIFIER, NEWDOC_DEFAULT_NAME_KEY );
	
	public static final String TEMPLATES_NEWDOC_CONTEXTTYPE= TEXTILE_RWEAVE_LANGUAGE_NAME +
			WikitextRweaveTemplateContextType.NEWDOC_CONTEXTTYPE_SUFFIX;
	
	
	public NewDocTemplateCategoryConfiguration() {
	}
	
	
	@Override
	public ITemplateContribution getTemplates() {
		return new TemplateStoreContribution(CodeGeneration.getDocTemplateStore());
	}
	
	@Override
	public Preference<String> getDefaultPref() {
		return NEWDOC_DEFAULT_NAME_PREF;
	}
	
	@Override
	public ContextTypeRegistry getContextTypeRegistry() {
		return CodeGeneration.getDocContextTypeRegistry();
	}
	
	@Override
	public String getDefaultContextTypeId() {
		return NewDocTemplateCategoryConfiguration.TEMPLATES_NEWDOC_CONTEXTTYPE;
	}
	
	@Override
	public String getViewerConfigId(final TemplatePersistenceData data) {
		return NewDocTemplateCategoryConfiguration.TEMPLATES_NEWDOC_CONTEXTTYPE;
	}
	
	@Override
	public SourceEditorViewerConfigurator createViewerConfiguator(final String viewerConfigId,
			final TemplatePersistenceData data, final TemplateVariableProcessor templateProcessor,
			final IProject project) {
		return new WikidocRweaveTemplateViewerConfigurator(
				new TextileRweaveDocumentSetupParticipant(true),
				WikitextCore.getContextAccess(project), RCore.getContextAccess(project),
				templateProcessor );
	}
	
}
