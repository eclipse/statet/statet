/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.textile.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.TextLineInformation;

import org.eclipse.statet.docmlet.wikitext.core.WikitextProblemReporter;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.internal.redocs.wikitext.r.textile.Messages;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.impl.BasicProblem;


@NonNullByDefault
public class TextileValidator extends WikitextProblemReporter {
	
	
	private static final Pattern BLOCK_START_PATTERN= Pattern.compile("\\A((?:bc|bq|pre|table|p)\\.\\.?+)(.)?"); //$NON-NLS-1$
	
	
	public TextileValidator() {
	}
	
	
	@Override
	public void run(final WikitextSourceUnit su, final WikidocSourceUnitModelInfo model,
			final SourceContent sourceContent,
			final IssueRequestor requestor, final int level) {
		if (sourceContent.getStartOffset() != 0) {
			throw new UnsupportedOperationException();
		}
		
		final Matcher matcher= BLOCK_START_PATTERN.matcher(sourceContent.getString());
		
		final TextLineInformation lines= sourceContent.getStringLines();
		int startOffset= 0;
		for (int line= 0; line < lines.getNumberOfLines(); line++) {
			final int endOffset= lines.getEndOffset(line);
			matcher.region(startOffset, endOffset);
			if (matcher.find()) {
				final String followingCharacter= matcher.group(2);
				if (followingCharacter == null || !followingCharacter.equals(" ")) { //$NON-NLS-1$
					final String matched= matcher.group(1);
					requestor.acceptProblems(new BasicProblem(WikitextModel.WIKIDOC_TYPE_ID,
							Problem.SEVERITY_WARNING, 0,
							NLS.bind(Messages.Validation_BlockWhitespace_message, matched),
							line, startOffset, matcher.end(1) ));
				}
			}
			startOffset= endOffset;
		}
	}
	
}
