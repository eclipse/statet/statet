/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.core.model;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.WikitextBuildParticipant;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocWorkspaceSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.build.RModelIndexUpdate;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.redocs.wikitext.r.core.model.WikidocRweaveSourceUnit;
import org.eclipse.statet.redocs.wikitext.r.core.model.WikitextRweaveModel;


@NonNullByDefault
public class WikitextRweaveBuildParticipant extends WikitextBuildParticipant {
	
	
	private static final ImList<String> WIKITEXT_R_MODEL_TYPES= ImCollections.newList(
			WikitextRweaveModel.WIKIDOC_R_MODEL_TYPE_ID );
	
	
	private RModelIndexUpdate rIndexUpdate= nonNullLateInit();
	
	
	public WikitextRweaveBuildParticipant() {
	}
	
	
	@Override
	public void init() {
		super.init();
		
		final RProject rProject= RProjects.getRProject(getLtkProject().getProject());
		if (rProject != null) {
			setEnabled(true);
			this.rIndexUpdate= new RModelIndexUpdate(rProject, WIKITEXT_R_MODEL_TYPES,
					(getBuildType() == IncrementalProjectBuilder.FULL_BUILD) );
		}
	}
	
	
	@Override
	public void handleSourceUnitUpdated(final WikidocWorkspaceSourceUnit sourceUnit,
			final SubMonitor m) throws CoreException {
		if (sourceUnit instanceof final WikidocRweaveSourceUnit unit) {
			final WikidocRweaveSuModelContainer modelContainer= (WikidocRweaveSuModelContainer)unit
					.getAdapter(WikidocSourceUnitModelContainer.class);
			if (modelContainer != null) {
				this.rIndexUpdate.update(unit,
						RModel.getRModelInfo(modelContainer.getCurrentModel()) );
			}
			else {
				this.rIndexUpdate.remove(unit);
			}
		}
	}
	
	@Override
	public void handleSourceUnitRemoved(final IFile file,
			final SubMonitor m) throws CoreException {
		this.rIndexUpdate.remove(file);
	}
	
	@Override
	public void finish(final SubMonitor m) throws CoreException {
		this.rIndexUpdate.submit(m);
	}
	
}
