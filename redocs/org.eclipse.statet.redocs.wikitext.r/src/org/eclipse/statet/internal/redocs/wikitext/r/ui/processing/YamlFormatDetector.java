/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.processing;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import java.lang.reflect.InvocationTargetException;
import java.util.regex.Matcher;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextLineInformation;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.text.core.input.OffsetStringParserInput;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig;
import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.YamlBlockWeaveParticipant;
import org.eclipse.statet.dsl.core.source.ast.Collection;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.DslAstVisitor;
import org.eclipse.statet.dsl.core.source.ast.KeyValuePair;
import org.eclipse.statet.dsl.core.source.ast.NodeType;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.redocs.wikitext.r.core.WikitextRweaveCore;
import org.eclipse.statet.yaml.core.source.ast.YamlParser;


@NonNullByDefault
public class YamlFormatDetector {
	
	
	private static final String FAIL= "<fail>"; //$NON-NLS-1$
	
	
	private final String modelTypeId;
	
	private @Nullable Matcher extValidator;
	
	
	public YamlFormatDetector(final String modelTypeId) {
		this.modelTypeId= modelTypeId;
	}
	
	
	public String detect(final IFile file, final SubMonitor m) throws CoreException {
		m.beginTask(NLS.bind("Detecting output format of ''{0}''...", file.getName()),
				10 );
		try {
			final SourceUnit sourceUnit= LtkModels.getSourceUnitManager().getSourceUnit(this.modelTypeId,
					Ltk.PERSISTENCE_CONTEXT, file, m.newChild(1) );
			try {
				final SourceContent content= sourceUnit.getContent(m.newChild(1));
				m.worked(0);
				final TextRegion yamlRegion= getYamlBlockRegion(content);
				m.worked(1);
				
				final YamlParser yamlParser= new YamlParser(AstInfo.LEVEL_MODEL_DEFAULT);
				yamlParser.setScalarText(true);
				
				final TextParserInput input= new OffsetStringParserInput(content.getString(), content.getStartOffset());
				
				final String code= content.getString(yamlRegion);
				final SourceComponent block= yamlParser.parseSourceFragment(
						input.init(yamlRegion),
						null, false );
				
				final String format= searchOutputInfo(block, code);
				final String ext= toExtension(format);
				
				if (!isValidExt(ext)) {
					throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
							NLS.bind("Failed to detect file extension for format ''{0}''.",
									format )));
				}
				
				return ext;
			}
			finally {
				sourceUnit.disconnect(m);
			}
		}
		catch (final Exception e) {
			throw new CoreException(new Status(IStatus.ERROR, WikitextRweaveCore.BUNDLE_ID, 0,
					NLS.bind("Failed to detect output format specified in doc (YAML) for ''{0}''.",
							file.getName() ),
					e ));
		}
	}
	
	
	private TextRegion getYamlBlockRegion(final SourceContent sourceContent)
			throws BadLocationException, CoreException {
		final TextLineInformation lines= sourceContent.getStringLines();
		final YamlBlockWeaveParticipant part= new YamlBlockWeaveParticipant();
		part.reset(sourceContent);
		
		int lineEndOffset= lines.getStartOffset(0);
		final int numLines= lines.getNumberOfLines();
		for (int line= 0; line < numLines; line++) {
			int lineOffset= lineEndOffset;
			lineEndOffset= lines.getEndOffset(line);
			if (part.checkStartLine(lineOffset, lineEndOffset)) {
				while (++line < numLines) {
					lineOffset= lineEndOffset;
					lineEndOffset= lines.getEndOffset(line);
					if (part.checkEndLine(lineOffset, lineEndOffset)) {
						return new BasicTextRegion(
								sourceContent.getStartOffset() + part.getStartOffset(),
								sourceContent.getStartOffset() + lineEndOffset );
					}
				}
			}
		}
		throw new CoreException(new Status(IStatus.ERROR, WikitextRweaveCore.BUNDLE_ID,
				"No YAML metadata block found." ));
	}
	
	
	private String searchOutputInfo(final SourceComponent block, final String code)
			throws CoreException, InvocationTargetException {
		
		class Searcher extends DslAstVisitor {
			
			private @Nullable String format;
			private @Nullable String output;
			private @Nullable String outputFormat;
			
			@Override
			public void visit(final Collection node) throws InvocationTargetException {
				if (node.getDslParent().getNodeType() == NodeType.RECORD
						&& node.getNodeType() == NodeType.MAP) {
					node.acceptInDslChildren(this);
				}
			}
			
			@Override
			public void visit(final KeyValuePair node) throws InvocationTargetException {
				if (node.getKey().getNodeType() == NodeType.SCALAR) {
					switch (nonNullElse(node.getKey().getText(), "")) { //$NON-NLS-1$
					case "format": //$NON-NLS-1$
						if (this.format == null) {
							if (node.getValue().getNodeType() == NodeType.SCALAR) {
								this.format= node.getValue().getText();
							}
							if (this.format == null) {
								this.format= FAIL;
							}
						}
						break;
					case "output": //$NON-NLS-1$
						if (this.output == null) {
							switch (node.getValue().getNodeType()) {
							case SCALAR:
								this.output= node.getValue().getText();
								break;
							case SEQ:
							case MAP:
								if (node.getValue().hasChildren()) {
									final DslAstNode firstOutput= node.getValue().getChild(0);
									if (firstOutput.getNodeType() == NodeType.KEY_VALUE_ENTRY) {
										checkOutputEntry((KeyValuePair)firstOutput);
									}
								}
								break;
							default:
								break;
							}
							if (this.output == null) {
								this.output= FAIL;
							}
						}
						break;
					default:
						break;
					}
				}
			}
			
			private void checkOutputEntry(final KeyValuePair outputEntry) {
				if (outputEntry.getKey().getNodeType() == NodeType.SCALAR) {
					this.output= outputEntry.getKey().getText();
				}
				if (outputEntry.getValue().getNodeType() == NodeType.MAP) {
					final var outputConfig= outputEntry.getValue();
					for (int i= 0; i < outputConfig.getChildCount(); i++) {
						final DslAstNode iChild= outputConfig.getChild(i);
						if (iChild.getNodeType() == NodeType.KEY_VALUE_ENTRY) {
							final KeyValuePair outputConfigEntry= (KeyValuePair)iChild;
							if (outputConfigEntry.getKey().getNodeType() == NodeType.SCALAR
									&& "format".equals(outputConfigEntry.getKey().getText()) //$NON-NLS-1$
									&& outputConfigEntry.getValue().getNodeType() == NodeType.SCALAR) {
								this.outputFormat= outputConfigEntry.getValue().getText();
							}
						}
					}
				}
			}
			
		}
		
		final Searcher searcher= new Searcher();
		block.acceptInDsl(searcher);
		
		if (searcher.format != null) {
			if (searcher.format == FAIL) {
				throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
						"Unexpected data for 'format'.\nYAML code:\n" + code ));
			}
			else {
				return searcher.format;
			}
		}
		else if (searcher.output != null) {
			if (searcher.outputFormat != null && isValidExt(toExtension(searcher.outputFormat))) {
				return searcher.outputFormat;
			}
			else if (searcher.output == FAIL) {
				throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
						"Unexpected data for 'output'.\nYAML code:\n" + code ));
			}
			else {
				return searcher.output;
			}
		}
		else {
			throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
					"No 'format' or 'output' entry found.\nYAML code:\n" + code ));
		}
	}
	
	
	private String toExtension(String format) {
		{	final int startIdx= format.lastIndexOf(':') + 1; // no match -> 0
			final int endIdx= format.indexOf('_', startIdx);
			format= format.substring(startIdx, (endIdx >= 0) ? endIdx : format.length());
		}
		return switch (format) {
		case "native" ->
				"hs";
		case "plain" ->
				"txt";
		case "markdown" ->
				"md";
		case "mediawiki" ->
				"mediawiki";
		case "textile" ->
				"textile";
		case "asciidoc" ->
				"asciidoc";
		case "html", "html5" ->
				"html";
		case "pdf", "latex", "beamer" ->
				"pdf";
		case "context" ->
				"tex";
		case "texinfo" ->
				"texi";
		case "docbook" ->
				"dbk";
		case "opendocument", "odt" ->
				"odt";
		case "word", "docx" ->
				"docx";
		case "rtf" ->
				"rtf";
		case "epub", "epub3" ->
				"epub";
		case "slidy", "slideous", "dzslides", "revealjs", "s5" ->
				"html";
		default ->
				format;
		};
	}
	
	private boolean isValidExt(final String ext) {
		var extValidator= this.extValidator;
		if (extValidator == null) {
			extValidator= DocProcessingConfig.VALID_EXT_PATTERN.matcher(ext);
			this.extValidator= extValidator;
		}
		else {
			extValidator.reset(ext);
		}
		return extValidator.matches();
	}
	
}
