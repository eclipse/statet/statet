/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.sourceediting;

import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.content.ITextElementFilter;

import org.eclipse.statet.internal.redocs.wikitext.r.ui.util.WikitextRNameElementFilter;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.ui.sourceediting.QuickOutlineInformationControl;
import org.eclipse.statet.ltk.ui.sourceediting.actions.OpenDeclaration;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.redocs.wikitext.r.core.model.WikitextRweaveModel;


@NonNullByDefault
class DocRQuickOutlineInformationControl extends QuickOutlineInformationControl {
	
	
	private class ContentFilter implements LtkModelElementFilter<SourceStructElement<?, ?>> {
		
		@Override
		public boolean include(final SourceStructElement<?, ?> element) {
			if (element.getModelTypeId() == RModel.R_TYPE_ID) {
				switch (element.getElementType()) {
				case RElement.R_ARGUMENT:
					return false;
				default:
					return true;
				}
			}
			return true;
		}
		
	}
	
	
	private final ContentFilter contentFilter= new ContentFilter();
	
	
	public DocRQuickOutlineInformationControl(final Shell parent, final String modelType,
			final String commandId) {
		super(parent, commandId, 2, new OpenDeclaration());
	}
	
	
	@Override
	public String getModelTypeId(final int page) {
		if (page == 1) {
			return RModel.R_TYPE_ID;
		}
		return WikitextRweaveModel.WIKIDOC_R_MODEL_TYPE_ID;
	}
	
	@Override
	protected int getInitialIterationPage(final SourceElement<?> element) {
//		if (element.getModelTypeId() == RModel.R_TYPE_ID) {
//			return 1;
//		}
		return 0;
	}
	
	@Override
	protected String getDescription(final int iterationPage) {
		if (iterationPage == 1) {
			return "R Outline";
		}
		return super.getDescription(iterationPage);
	}
	
	@Override
	protected LtkModelElementFilter<? super SourceStructElement<?, ?>> getContentFilter() {
		return this.contentFilter;
	}
	
	@Override
	protected ITextElementFilter createNameFilter() {
		return new WikitextRNameElementFilter();
	}
	
}
