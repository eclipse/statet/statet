/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.processing;

import java.util.Collections;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigPresets;
import org.eclipse.statet.ecommons.debug.ui.util.CheckedCommonTab;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfigMainTab;
import org.eclipse.statet.docmlet.base.ui.processing.PreviewTab;
import org.eclipse.statet.redocs.r.ui.processing.RunRConsoleSnippetOperation;


/**
 * Tab group for Wikitext+R output creation toolchain.
 */
@NonNullByDefault
public class WikitextRweaveConfigTabGroup extends AbstractLaunchConfigurationTabGroup {
	
	
	private static final LaunchConfigPresets PRESETS;
	static {
		final LaunchConfigPresets presets= new LaunchConfigPresets(
				WikitextRweaveConfig.TYPE_ID );
		
		{	final ILaunchConfigurationWorkingCopy config= presets.add("PDF using R:knitr + R:pandoc");
			config.setAttribute(WikitextRweaveConfig.WEAVE_ENABLED_ATTR_NAME, true);
			config.setAttribute(WikitextRweaveConfig.WEAVE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.AUTO_WIKITEXT_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"knitr::knit(" +
									"input= \"${resource_loc}\", " +
									"output= \"${resource_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc}\")" ));
			config.setAttribute(WikitextRweaveConfig.PRODUCE_ENABLED_ATTR_NAME, true);
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.EXT_PDF_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"knitr::pandoc(" +
									"input= \"${resource_loc}\", " +
									"format= \"latex\", " +
									"encoding= \"${resource_enc:${source_file_path}}\")" ));
		}
		{	final ILaunchConfigurationWorkingCopy config= presets.add("PDF using R:RMarkdown, two-step");
			config.setAttribute(WikitextRweaveConfig.WEAVE_ENABLED_ATTR_NAME, true);
			config.setAttribute(WikitextRweaveConfig.WEAVE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.AUTO_WIKITEXT_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"rmarkdown::render(" +
									"input= \"${resource_loc}\", " +
									"output_format= \"md_document\", " +
									"output_file= \"${resource_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc}\")" ));
			config.setAttribute(WikitextRweaveConfig.PRODUCE_ENABLED_ATTR_NAME, true);
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.EXT_PDF_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"rmarkdown::render(" +
									"input= \"${resource_loc}\", " +
									"output_format= \"pdf_document\", " +
									"output_file= \"${resource_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc:${source_file_path}}\")" ));
		}
		{	final ILaunchConfigurationWorkingCopy config= presets.add("PDF using R:RMarkdown, single-step");
			config.setAttribute(WikitextRweaveConfig.WEAVE_ENABLED_ATTR_NAME, false);
			config.setAttribute(WikitextRweaveConfig.WEAVE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.AUTO_WIKITEXT_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"rmarkdown::render(" +
									"input= \"${resource_loc}\", " +
									"output_format= \"md_document\", " +
									"output_file= \"${resource_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc}\")" ));
			config.setAttribute(WikitextRweaveConfig.PRODUCE_ENABLED_ATTR_NAME, true);
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.EXT_PDF_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"rmarkdown::render(" +
									"input= \"${resource_loc}\", " +
									"output_format= \"pdf_document\", " +
									"output_file= \"${resource_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc}\")" ));
		}
		{	final ILaunchConfigurationWorkingCopy config= presets.add("Auto (YAML) using R:RMarkdown, two-step");
			config.setAttribute(WikitextRweaveConfig.WEAVE_ENABLED_ATTR_NAME, true);
			config.setAttribute(WikitextRweaveConfig.WEAVE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.AUTO_WIKITEXT_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"rmarkdown::render(" +
									"input= \"${resource_loc}\", " +
									"output_format= \"md_document\", " +
									"output_file= \"${resource_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc}\")" ));
			config.setAttribute(WikitextRweaveConfig.PRODUCE_ENABLED_ATTR_NAME, true);
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.AUTO_YAML_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"rmarkdown::render(" +
									"input= \"${resource_loc}\", " +
									"output_dir= \"${container_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc:${source_file_path}}\")" ));
		}
		{	final ILaunchConfigurationWorkingCopy config= presets.add("Auto (YAML) using R:RMarkdown, single-step");
			config.setAttribute(WikitextRweaveConfig.WEAVE_ENABLED_ATTR_NAME, false);
			config.setAttribute(WikitextRweaveConfig.WEAVE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.AUTO_WIKITEXT_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.WEAVE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"rmarkdown::render(" +
									"input= \"${resource_loc}\", " +
									"output_format= \"md_document\", " +
									"output_file= \"${resource_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc}\")" ));
			config.setAttribute(WikitextRweaveConfig.PRODUCE_ENABLED_ATTR_NAME, true);
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OUTPUT_FORMAT_ATTR_NAME,
					WikitextRweaveConfig.AUTO_YAML_FORMAT_KEY );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(WikitextRweaveConfig.PRODUCE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"rmarkdown::render(" +
									"input= \"${resource_loc}\", " +
									"output_dir= \"${container_loc:${out_file_path}}\", " +
									"encoding= \"${resource_enc}\")" ));
		}
		
		PRESETS= presets;
	}
	
	
	public WikitextRweaveConfigTabGroup() {
	}
	
	@Override
	public void createTabs(final ILaunchConfigurationDialog dialog, final String mode) {
		final DocProcessingConfigMainTab mainTab= new DocProcessingConfigMainTab(PRESETS) {
			@Override
			protected ImList<ILaunchConfigurationTab> getPresetTabs(final ILaunchConfiguration config) {
				return ImCollections.<ILaunchConfigurationTab>newList(getStepTab(1), getStepTab(2) );
			}
		};
		final WeaveTab weaveTab= new WeaveTab(mainTab);
		final ProduceTab produceTab= new ProduceTab(mainTab, weaveTab);
		final PreviewTab previewTab= new PreviewTab(mainTab, produceTab);
		
		final ILaunchConfigurationTab[] tabs= new ILaunchConfigurationTab[] {
				mainTab,
				weaveTab,
				produceTab,
				previewTab,
				new CheckedCommonTab()
		};
		
		setTabs(tabs);
	}
	
}
