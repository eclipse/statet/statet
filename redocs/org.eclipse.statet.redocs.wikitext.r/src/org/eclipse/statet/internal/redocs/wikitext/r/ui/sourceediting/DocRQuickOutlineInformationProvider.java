/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.sourceediting;

import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.sourceediting.QuickInformationProvider;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.redocs.wikitext.r.core.model.WikitextRweaveModel;


@NonNullByDefault
public class DocRQuickOutlineInformationProvider extends QuickInformationProvider {
	
	
	public DocRQuickOutlineInformationProvider(final SourceEditor editor, final int textOperation) {
		super(editor, WikitextRweaveModel.WIKIDOC_R_MODEL_TYPE_ID, textOperation);
	}
	
	
	@Override
	public IInformationControlCreator createInformationPresenterControlCreator() {
		return new IInformationControlCreator() {
			@Override
			public IInformationControl createInformationControl(final Shell parent) {
				return new DocRQuickOutlineInformationControl(parent, getModelTypeId(), getCommandId());
			}
		};
	}
	
}
