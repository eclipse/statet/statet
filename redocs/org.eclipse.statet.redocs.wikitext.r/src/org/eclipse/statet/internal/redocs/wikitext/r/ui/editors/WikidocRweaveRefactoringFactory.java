/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.editors;

import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.refactoring.WikitextRefactoring;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;
import org.eclipse.statet.ltk.refactoring.core.MultiRefactoringFactory;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.refactoring.RRefactoring;


public class WikidocRweaveRefactoringFactory extends MultiRefactoringFactory {
	
	
	private static final WikidocRweaveRefactoringFactory INSTANCE= new WikidocRweaveRefactoringFactory();
	
	public static WikidocRweaveRefactoringFactory getInstance() {
		return INSTANCE;
	}
	
	
	public WikidocRweaveRefactoringFactory() {
		super(WikitextModel.WIKIDOC_TYPE_ID, WikitextRefactoring.getWikidocFactory());
	}
	
	
	@Override
	protected CommonRefactoringFactory createFactory(final String modelTypeId) {
		switch (modelTypeId) {
		case RModel.R_TYPE_ID:
			return RRefactoring.getFactory();
		default:
			return null;
		}
	}
	
}
