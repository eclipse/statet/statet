/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.wikitext.r.ui.editors;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.ui.texteditor.templates.TemplatesView;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;
import org.eclipse.statet.ltk.ui.templates.config.AbstractEditorTemplatesPage;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.ui.editors.templates.REditorTemplateContextType;
import org.eclipse.statet.r.ui.sourceediting.RTemplateSourceViewerConfigurator;
import org.eclipse.statet.redocs.wikitext.r.core.source.doc.WikitextRweaveDocumentConstants;
import org.eclipse.statet.redocs.wikitext.r.ui.editors.WikidocRweaveDocEditor;


/**
 * Page for {@link WikidocRweaveDocEditor} / {@link TemplatesView}
 * 
 * At moment only for R templates.
 */
@NonNullByDefault
public class WikidocRweaveEditorTemplatesPage extends AbstractEditorTemplatesPage {
	
	
	private @Nullable SourceEditorViewerConfigurator rPreviewConfigurator;
	
	
	public WikidocRweaveEditorTemplatesPage(final WikidocRweaveDocEditor editor) {
		super(RUIPlugin.getInstance().getREditorTemplateStore(), editor, editor);
	}
	
	
	@Override
	protected String getPreferencePageId() {
		return "org.eclipse.statet.r.preferencePages.REditorTemplates"; //$NON-NLS-1$
	}
	
	@Override
	protected IPreferenceStore getTemplatePreferenceStore() {
		return RUIPlugin.getInstance().getPreferenceStore();
	}
	
	@Override
	protected String[] getContextTypeIds(final IDocument document, final int offset) {
		try {
			final String contentType= TextUtilities.getContentType(document, WikitextRweaveDocumentConstants.WIKIDOC_R_PARTITIONING, offset, true);
			if (RDocumentConstants.R_ANY_CONTENT_CONSTRAINT.matches(contentType)) {
				return new String[] { REditorTemplateContextType.RCODE_CONTEXTTYPE_ID };
			}
		}
		catch (final BadLocationException e) {
		}
		return new String[0];
	}
	
	@Override
	protected @Nullable TemplateCompletionComputer getComputer(
			final AssistInvocationContext context, final Template template) {
		return (TemplateCompletionComputer)RUIPlugin.getInstance().getREditorContentAssistRegistry()
				.getComputer("org.eclipse.statet.r.contentAssistComputers.RTemplateCompletion"); //$NON-NLS-1$
	}
	
	
	@Override
	protected SourceEditorViewerConfigurator getTemplatePreviewConfig(final Template template, final TemplateVariableProcessor templateProcessor) {
		SourceEditorViewerConfigurator configurator= this.rPreviewConfigurator;
		if (configurator == null) {
			configurator= new RTemplateSourceViewerConfigurator(
					RCore.getWorkbenchAccess(),
					templateProcessor );
			this.rPreviewConfigurator= configurator;
		}
		return configurator;
	}
	
	@Override
	protected SourceEditorViewerConfigurator getTemplateEditConfig(final Template template, final TemplateVariableProcessor templateProcessor) {
		return new RTemplateSourceViewerConfigurator(
				RCore.getWorkbenchAccess(),
				templateProcessor );
	}
	
}
