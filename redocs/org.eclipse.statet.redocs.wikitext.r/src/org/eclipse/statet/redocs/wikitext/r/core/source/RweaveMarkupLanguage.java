/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.wikitext.r.core.source;

import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;
import org.eclipse.statet.redocs.r.core.source.AbstractRChunkPartitionNodeScanner;


@NonNullByDefault
public interface RweaveMarkupLanguage extends WikitextMarkupLanguage {
	
	
	String EMBEDDED_R= "R"; // RModel.R_TYPE_ID //$NON-NLS-1$
	
	byte EMBEDDED_R_CHUNK_DESCR= EmbeddingAstNode.EMBED_CHUNK;
	byte EMBEDDED_R_INLINE_DESCR= EmbeddingAstNode.EMBED_INLINE;
	
	
	List<String> getIndentPrefixes();
	
	Pattern getRChunkStartLinePattern();
	@Nullable Pattern getRChunkRefLinePattern();
	Pattern getRChunkEndLinePattern();
	
	
	AbstractRChunkPartitionNodeScanner getRChunkPartitionScanner();
	
}
