/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.wikitext.r.ui.sourceediting;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;

import org.eclipse.statet.redocs.wikitext.r.ui.WikitextRweaveUI;


@NonNullByDefault
public class WikitextRweaveEditingSettings {
	
	
	public static final String WIKIDOC_EDITOR_NODE= WikitextRweaveUI.BUNDLE_ID + "/editor/Doc"; //$NON-NLS-1$
	
	
	public static final String SPELLCHECK_ENABLED_PREF_KEY= "SpellCheck.enabled"; //$NON-NLS-1$
	public static final BooleanPref WIKIDOC_SPELLCHECK_ENABLED_PREF= new BooleanPref(
			WIKIDOC_EDITOR_NODE, SPELLCHECK_ENABLED_PREF_KEY); 
	
}
