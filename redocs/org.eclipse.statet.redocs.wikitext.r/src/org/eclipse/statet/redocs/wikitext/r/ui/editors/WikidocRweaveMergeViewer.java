/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.wikitext.r.ui.editors;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;
import org.eclipse.statet.ecommons.text.core.PartitionConstraint;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.ltk.ui.compare.CompareMergeTextViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.redocs.wikitext.r.core.source.doc.WikidocRweaveDocumentSetupParticipant;
import org.eclipse.statet.redocs.wikitext.r.ui.sourceediting.WikidocRweaveSourceViewerConfiguration;
import org.eclipse.statet.redocs.wikitext.r.ui.sourceediting.WikidocRweaveSourceViewerConfigurator;


@NonNullByDefault
public class WikidocRweaveMergeViewer extends CompareMergeTextViewer {
	
	
	private final WikidocRweaveDocumentSetupParticipant documentSetup;
	
	
	public WikidocRweaveMergeViewer(final WikidocRweaveDocumentSetupParticipant documentSetup,
			final Composite parent, final CompareConfiguration configuration) {
		super(parent, configuration);
		this.documentSetup= documentSetup;
	}
	
	
	@Override
	protected PartitionerDocumentSetupParticipant createDocumentSetupParticipant() {
		return this.documentSetup;
	}
	
	@Override
	protected @Nullable PartitionConstraint getIgnoreWhitespaceExcludePartitionConstraint() {
		return RDocumentConstants.R_SPACE_SENSITIVE_CONSTRAINT;
	}
	
	@Override
	protected SourceEditorViewerConfigurator createConfigurator(final SourceViewer sourceViewer) {
		return new WikidocRweaveSourceViewerConfigurator(this.documentSetup,
				WikitextCore.getWorkbenchAccess(), RCore.getWorkbenchAccess(),
				new WikidocRweaveSourceViewerConfiguration(SourceEditorViewerConfiguration.COMPARE_MODE) );
	}
	
}
