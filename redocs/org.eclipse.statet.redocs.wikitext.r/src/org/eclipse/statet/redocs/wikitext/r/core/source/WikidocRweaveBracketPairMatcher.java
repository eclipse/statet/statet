/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.wikitext.r.core.source;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;

import org.eclipse.statet.docmlet.wikitext.core.source.MarkupBracketPairMatcher;
import org.eclipse.statet.docmlet.wikitext.core.source.WikitextHeuristicTokenScanner;
import org.eclipse.statet.ltk.text.core.CharPairMatcher;
import org.eclipse.statet.ltk.text.core.MultiContentSectionCharPairMatcher;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.util.RBracketPairMatcher;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;
import org.eclipse.statet.redocs.wikitext.r.core.source.doc.WikidocRweaveDocumentContentInfo;
import org.eclipse.statet.redocs.wikitext.r.core.source.doc.WikitextRweaveDocumentConstants;


@NonNullByDefault
public class WikidocRweaveBracketPairMatcher extends MultiContentSectionCharPairMatcher {
	
	
	private static final PartitionConstraint R_CONTENT_TYPES= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
			return (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE
					|| contentType == WikitextRweaveDocumentConstants.RCHUNK_CONTROL_CONTENT_TYPE );
		}
	};
	
	public static RBracketPairMatcher createRChunkPairMatcher(final RHeuristicTokenScanner scanner) {
		return new RBracketPairMatcher(scanner, R_CONTENT_TYPES);
	}
	
	
	public WikidocRweaveBracketPairMatcher() {
		super(WikidocRweaveDocumentContentInfo.INSTANCE);
	}
	
	
	@Override
	protected @Nullable CharPairMatcher createHandler(final String sectionType) {
		switch (sectionType) {
		case WikidocRweaveDocumentContentInfo.WIKITEXT:
			return new MarkupBracketPairMatcher(
					WikitextHeuristicTokenScanner.create(getSections()) );
		case WikidocRweaveDocumentContentInfo.R:
		case WikidocRweaveDocumentContentInfo.R_CHUNK_CONTROL:
			return createRChunkPairMatcher(
					RHeuristicTokenScanner.create(getSections()) );
		default:
			return null;
		}
	}
	
}
