/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.core;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;
import org.eclipse.statet.docmlet.tex.core.commands.TexEmbedCommand;
import org.eclipse.statet.docmlet.tex.core.parser.CustomScanner;
import org.eclipse.statet.internal.redocs.tex.r.Messages;
import org.eclipse.statet.r.core.model.RModel;


@NonNullByDefault
public interface SweaveLtxCommands {
	
	
	TexCommand SWEAVE_SweaveOpts_COMMANDS= new TexCommand(0,
			"SweaveOpts", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("options", Parameter.REQUIRED, Parameter.NONE) //$NON-NLS-1$
			), Messages.LtxCommand_SweaveOpts_description);
	
	TexEmbedCommand SWEAVE_Sexpr_COMMANDS= new TexEmbedCommand(0, RModel.R_TYPE_ID,
			"Sexpr", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("expression", Parameter.REQUIRED, Parameter.EMBEDDED) //$NON-NLS-1$
			), Messages.LtxCommand_Sexpr_description) {
		
		@Override
		public CustomScanner getArgumentScanner(final int argIdx) {
			return CurlyExpandEmbeddedScanner.INSTANCE;
		}
		
	};
	
}
