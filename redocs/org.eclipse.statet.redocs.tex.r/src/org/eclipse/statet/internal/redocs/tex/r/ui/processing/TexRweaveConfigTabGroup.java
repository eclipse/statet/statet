/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.processing;

import java.util.Collections;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigPresets;
import org.eclipse.statet.ecommons.debug.ui.util.CheckedCommonTab;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfigMainTab;
import org.eclipse.statet.docmlet.base.ui.processing.PreviewTab;
import org.eclipse.statet.redocs.r.ui.processing.RunRConsoleSnippetOperation;


/**
 * Tab group for Sweave (LaTeX+R) output creation toolchain.
 */
@NonNullByDefault
public class TexRweaveConfigTabGroup extends AbstractLaunchConfigurationTabGroup {
	
	
	private static final LaunchConfigPresets PRESETS;
	static {
		final LaunchConfigPresets presets= new LaunchConfigPresets(
				TexRweaveConfig.TYPE_ID );
		
		{	final ILaunchConfigurationWorkingCopy config= presets.add("PDF using R:tools (texi2dvi)");
			config.setAttribute(TexRweaveConfig.WEAVE_ENABLED_ATTR_NAME, true);
			config.setAttribute(TexRweaveConfig.WEAVE_OUTPUT_FORMAT_ATTR_NAME,
					TexRweaveConfig.EXT_LTX_FORMAT_KEY );
			config.setAttribute(TexRweaveConfig.WEAVE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(TexRweaveConfig.WEAVE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"Sweave(" +
									"file= \"${resource_loc}\", " +
									"encoding= \"${resource_enc}\")" ));
			config.setAttribute(TexRweaveConfig.PRODUCE_ENABLED_ATTR_NAME, true);
			config.setAttribute(TexRweaveConfig.PRODUCE_OUTPUT_FORMAT_ATTR_NAME,
					TexRweaveConfig.EXT_PDF_FORMAT_KEY );
			config.setAttribute(TexRweaveConfig.PRODUCE_OPERATION_ID_ATTR_NAME,
					RunRConsoleSnippetOperation.ID );
			config.setAttribute(TexRweaveConfig.PRODUCE_OPERATION_SETTINGS_ATTR_NAME,
					Collections.singletonMap(RunRConsoleSnippetOperation.R_SNIPPET_CODE_ATTR_NAME,
							"tools::texi2dvi(" +
									"file= \"${resource_loc}\", " +
									"pdf= TRUE)" ));
		}
		
		PRESETS= presets;
	}
	
	
	public TexRweaveConfigTabGroup() {
	}
	
	@Override
	public void createTabs(final ILaunchConfigurationDialog dialog, final String mode) {
		final DocProcessingConfigMainTab mainTab= new DocProcessingConfigMainTab(PRESETS) {
			@Override
			protected ImList<ILaunchConfigurationTab> getPresetTabs(final ILaunchConfiguration config) {
				return ImCollections.<ILaunchConfigurationTab>newList(getStepTab(1), getStepTab(2) );
			}
		};
		final TexTab texTab= new TexTab(mainTab);
		final ProduceTab produceTab= new ProduceTab(mainTab, texTab);
		final PreviewTab previewTab= new PreviewTab(mainTab, produceTab);
		
		final ILaunchConfigurationTab[] tabs= new ILaunchConfigurationTab[] {
				mainTab,
				texTab,
				produceTab,
				previewTab,
				new CheckedCommonTab()
		};
		
		setTabs(tabs);
	}
	
}
