/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui;

import org.eclipse.statet.redocs.tex.r.ui.TexRweaveUI;


public class LtxRweaveTemplates {
	
	
	public static final String PREF_QUALIFIER= TexRweaveUI.BUNDLE_ID + "/codegen"; //$NON-NLS-1$
	
	
	public static final String DOC_TEMPLATES_STORE_KEY= "DocTemplates_store"; //$NON-NLS-1$
	
	public static final String NEWDOC_TEMPLATE_CATEGORY_ID= "LtxRweave.NewDoc";
	
}
