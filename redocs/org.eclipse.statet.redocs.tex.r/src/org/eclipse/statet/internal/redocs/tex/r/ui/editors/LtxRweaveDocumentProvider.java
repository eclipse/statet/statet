/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.editors;

import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.source.IAnnotationModel;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.preferences.SettingsChangeNotifier;
import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.ui.editors.TexEditorBuild;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.ui.sourceediting.SourceAnnotationModel;
import org.eclipse.statet.ltk.ui.sourceediting.SourceDocumentProvider;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.ui.editors.REditorBuild;
import org.eclipse.statet.redocs.tex.r.core.model.LtxRweaveSourceUnit;
import org.eclipse.statet.redocs.tex.r.core.model.TexRweaveModel;
import org.eclipse.statet.redocs.tex.r.core.source.doc.LtxRweaveDocumentSetupParticipant;
import org.eclipse.statet.redocs.tex.r.ui.TexRweaveUI;


@NonNullByDefault
public class LtxRweaveDocumentProvider extends SourceDocumentProvider<LtxRweaveSourceUnit>
		implements Disposable {
	
	
	private static final IssueTypeSet ISSUE_TYPE_SET= new IssueTypeSet(TexRweaveUI.BUNDLE_ID,
			TexEditorBuild.LTX_ISSUE_TYPE_SET,
			REditorBuild.R_MODEL_PROBLEM_CATEGORY );
	
	
	private class ThisAnnotationModel extends SourceAnnotationModel {
		
		
		public ThisAnnotationModel(final IResource resource) {
			super(resource, LtxRweaveDocumentProvider.this.getIssueTypeSet());
		}
		
		@Override
		protected boolean isHandlingTemporaryProblems(final IssueTypeSet.ProblemCategory issueCategory) {
			if (issueCategory != null && issueCategory.getId() == RModel.R_TYPE_ID) {
				return LtxRweaveDocumentProvider.this.handleTemporaryRProblems;
			}
			return LtxRweaveDocumentProvider.this.handleTemporaryDocProblems;
		}
		
	}
	
	
	private SettingsChangeNotifier. @Nullable ChangeListener editorPrefListener;
	
	private boolean handleTemporaryDocProblems;
	private boolean handleTemporaryRProblems;
	
	
	public LtxRweaveDocumentProvider() {
		super(TexRweaveModel.LTX_R_MODEL_TYPE_ID, new LtxRweaveDocumentSetupParticipant(),
				ISSUE_TYPE_SET );
		
		this.editorPrefListener= new SettingsChangeNotifier.ChangeListener() {
			@Override
			public void settingsChanged(final Set<String> groupIds) {
				if (groupIds.contains(REditorBuild.GROUP_ID)
						|| groupIds.contains(TexEditorBuild.GROUP_ID)) {
					updateEditorPrefs();
				}
			}
		};
		PreferencesUtil.getSettingsChangeNotifier().addChangeListener(this.editorPrefListener);
		final PreferenceAccess access= EPreferences.getInstancePrefs();
		this.handleTemporaryDocProblems= access.getPreferenceValue(TexEditorBuild.PROBLEMCHECKING_ENABLED_PREF);
		this.handleTemporaryRProblems= access.getPreferenceValue(REditorBuild.PROBLEMCHECKING_ENABLED_PREF);
	}
	
	@Override
	public void dispose() {
		if (this.editorPrefListener != null) {
			PreferencesUtil.getSettingsChangeNotifier().removeChangeListener(this.editorPrefListener);
			this.editorPrefListener= null;
		}
	}
	
	private void updateEditorPrefs() {
		final PreferenceAccess access= EPreferences.getInstancePrefs();
		final boolean newHandleTemporaryTexProblems= access.getPreferenceValue(TexEditorBuild.PROBLEMCHECKING_ENABLED_PREF);
		final boolean newHandleTemporaryRProblems= access.getPreferenceValue(REditorBuild.PROBLEMCHECKING_ENABLED_PREF);
		if (this.handleTemporaryDocProblems != newHandleTemporaryTexProblems
				|| this.handleTemporaryRProblems != newHandleTemporaryRProblems ) {
			this.handleTemporaryDocProblems= newHandleTemporaryRProblems;
			this.handleTemporaryRProblems= newHandleTemporaryRProblems;
			TexModel.getLtxModelManager().refresh(Ltk.EDITOR_CONTEXT);
		}
	}
	
	@Override
	protected IAnnotationModel createAnnotationModel(final IFile file) {
		return new ThisAnnotationModel(file);
	}
	
}
