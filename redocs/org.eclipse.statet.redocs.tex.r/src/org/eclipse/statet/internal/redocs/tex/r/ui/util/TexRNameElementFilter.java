/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.util;

import org.eclipse.ui.dialogs.SearchPattern;

import org.eclipse.statet.ecommons.models.core.util.ElementProxy;
import org.eclipse.statet.ecommons.ui.content.ITextElementFilter;
import org.eclipse.statet.ecommons.ui.content.MultiTextElementFilter;
import org.eclipse.statet.ecommons.ui.content.TextElementFilter;

import org.eclipse.statet.docmlet.tex.ui.util.TexNameSearchPattern;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.ui.util.RNameSearchPattern;


public class TexRNameElementFilter extends MultiTextElementFilter {
	
	
	public TexRNameElementFilter() {
		super(new ITextElementFilter[] {
				new TextElementFilter() {
					@Override
					protected SearchPattern createSearchPattern() {
						return new TexNameSearchPattern();
					}
				},
				new TextElementFilter() {
					@Override
					protected SearchPattern createSearchPattern() {
						return new RNameSearchPattern();
					}
				}
		});
	}
	
	
	@Override
	protected int getIdx(Object element) {
		if (element instanceof ElementProxy) {
			element= ((ElementProxy) element).getElement();
			if (element instanceof LtkModelElement) {
				if (((LtkModelElement) element).getModelTypeId() == RModel.R_TYPE_ID) {
					return 1;
				}
			}
		}
		return 0;
	}
	
}
