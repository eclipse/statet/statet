/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.editors;

import org.eclipse.core.commands.IHandler2;

import org.eclipse.statet.docmlet.tex.ui.actions.LtxOpenDeclarationHandler;
import org.eclipse.statet.ltk.ui.sourceediting.actions.MultiContentSectionHandler;
import org.eclipse.statet.r.ui.sourceediting.ROpenDeclarationHandler;
import org.eclipse.statet.redocs.tex.r.core.source.doc.LtxRweaveDocumentContentInfo;


public class OpenDeclarationHandler extends MultiContentSectionHandler {
	
	
	public OpenDeclarationHandler() {
		super(LtxRweaveDocumentContentInfo.INSTANCE);
	}
	
	
	@Override
	protected IHandler2 createHandler(final String sectionType) {
		switch (sectionType) {
		case LtxRweaveDocumentContentInfo.LTX:
			return new LtxOpenDeclarationHandler();
		case LtxRweaveDocumentContentInfo.R:
			return new ROpenDeclarationHandler();
		default:
			return null;
		}
	}
	
}
