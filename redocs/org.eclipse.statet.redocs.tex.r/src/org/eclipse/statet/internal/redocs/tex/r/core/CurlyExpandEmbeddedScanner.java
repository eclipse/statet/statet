/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.docmlet.tex.core.parser.CustomScanner;
import org.eclipse.statet.docmlet.tex.core.parser.LtxLexer;


/**
 * Scanner supporting expand variables {{ ... }}.
 */
@NonNullByDefault
public class CurlyExpandEmbeddedScanner implements CustomScanner {
	
	
	protected static final CustomScanner INSTANCE= new CurlyExpandEmbeddedScanner();
	
	
	protected CurlyExpandEmbeddedScanner() {
	}
	
	
	@Override
	public byte consume(final LtxLexer lexer) {
		final TextParserInput input= lexer.getInput();
		lexer.consume(true);
		
		int offset= 0;
		int expandVar= 0;
		while (true) {
			final int c= input.get(offset++);
			if (c < 0x20) {
				input.consume(offset - 1);
				lexer.consume(true);
				return 0;
			}
			switch (c) {
			case '{':
				if (input.get(offset) == '{') {
					offset++;
					expandVar++;
					continue;
				}
				continue;
			case '}':
				if (expandVar > 0 && input.get(offset) == '}') {
					offset++;
					expandVar--;
					continue;
				}
				input.consume(offset);
				lexer.consume(true);
				return LtxLexer.CURLY_BRACKET_CLOSE;
			default:
				continue;
			}
		}
	}
	
}
