/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.model;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.model.TexWorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.model.core.impl.GenericResourceSourceUnit2;
import org.eclipse.statet.ltk.model.core.impl.ResourceIssueSupport;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RWorkspaceSourceUnit;
import org.eclipse.statet.redocs.tex.r.core.TexRweaveCore;
import org.eclipse.statet.redocs.tex.r.core.model.LtxRweaveSourceUnit;
import org.eclipse.statet.redocs.tex.r.core.model.TexRweaveModel;
import org.eclipse.statet.redocs.tex.r.core.source.doc.LtxRweaveDocumentContentInfo;


@NonNullByDefault
public class LtxRweaveResourceSourceUnit
		extends GenericResourceSourceUnit2<LtxRweaveSuModelContainer>
		implements LtxRweaveSourceUnit, TexWorkspaceSourceUnit, RWorkspaceSourceUnit {
	
	
	private static final SourceUnitIssueSupport ISSUE_SUPPORT=
			new ResourceIssueSupport(LtxRweaveSuModelContainer.ISSUE_TYPE_SET);
	
	
	public LtxRweaveResourceSourceUnit(final String id, final IFile file) {
		super(id, file);
	}
	
	@Override
	protected LtxRweaveSuModelContainer createModelContainer() {
		return new LtxRweaveSuModelContainer(this, ISSUE_SUPPORT);
	}
	
	
	@Override
	public String getModelTypeId() {
		return TexRweaveModel.LTX_R_MODEL_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return LtxRweaveDocumentContentInfo.INSTANCE;
	}
	
	public RCoreAccess getRCoreAccess() {
		return RCore.getContextAccess(getResource());
	}
	
	public TexCoreAccess getTexCoreAccess() {
		return TexRweaveCore.getContextAccess(getResource());
	}
	
	
	@Override
	protected void register() {
		super.register();
		
		final ModelManager rManager= RModel.getRModelManager();
		if (rManager != null) {
			rManager.deregisterDependentUnit(this);
		}
	}
	
	@Override
	protected void unregister() {
		final ModelManager rManager= RModel.getRModelManager();
		if (rManager != null) {
			rManager.deregisterDependentUnit(this);
		}
		
		super.unregister();
	}
	
	
	@Override
	public @Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int flags,
			final IProgressMonitor monitor) {
		if (type == RModel.R_TYPE_ID) {
			return RModel.getRModelInfo(getModelContainer().getModelInfo(flags, monitor));
		}
		return super.getModelInfo(type, flags, monitor);
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == TexCoreAccess.class) {
			return (T)getTexCoreAccess();
		}
		if (adapterType == PreferenceAccess.class) {
			return (T)getTexCoreAccess().getPrefs();
		}
		if (adapterType == RCoreAccess.class) {
			return (T)getRCoreAccess();
		}
		return super.getAdapter(adapterType);
	}
	
	
}
