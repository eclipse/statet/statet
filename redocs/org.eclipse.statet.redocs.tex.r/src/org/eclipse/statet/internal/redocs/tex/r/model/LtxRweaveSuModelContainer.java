/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitModelContainer;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.project.RIssues;
import org.eclipse.statet.redocs.tex.r.core.TexRweaveCore;
import org.eclipse.statet.redocs.tex.r.core.model.LtxRweaveSourceUnit;
import org.eclipse.statet.redocs.tex.r.core.model.TexRweaveModel;


@NonNullByDefault
public class LtxRweaveSuModelContainer
		extends LtxSourceUnitModelContainer<LtxRweaveSourceUnit> {
	
	
	public static final IssueTypeSet ISSUE_TYPE_SET= new IssueTypeSet(TexRweaveCore.BUNDLE_ID,
			LtxSourceUnitModelContainer.ISSUE_TYPE_SET,
			RIssues.R_MODEL_PROBLEM_CATEGORY );
	
	
	public LtxRweaveSuModelContainer(final LtxRweaveSourceUnit unit,
			final @Nullable SourceUnitIssueSupport issueSupport) {
		super(unit, issueSupport);
	}
	
	
	@Override
	public boolean isContainerFor(final String modelTypeId) {
		return (modelTypeId == TexModel.LTX_TYPE_ID
				|| modelTypeId == TexRweaveModel.LTX_R_MODEL_TYPE_ID );
	}
	
	@Override
	public @Nullable String getNowebType() {
		return RModel.R_TYPE_ID;
	}
	
	
}
