/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui;

import org.eclipse.core.filesystem.IFileStore;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.redocs.tex.r.model.LtxRweaveEditorWorkingCopy;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.core.impl.AbstractEditorSourceUnitFactory;
import org.eclipse.statet.r.core.model.RWorkspaceSourceUnit;


@NonNullByDefault
public final class LtxRweaveEditorUnitFactory extends AbstractEditorSourceUnitFactory {
	
	
	public LtxRweaveEditorUnitFactory() {
	}
	
	
	@Override
	protected SourceUnit createSourceUnit(final String id, final WorkspaceSourceUnit su) {
		return new LtxRweaveEditorWorkingCopy((RWorkspaceSourceUnit)su);
	}
	
	@Override
	protected @Nullable SourceUnit createSourceUnit(final String id, final IFileStore file) {
		return null;
	}
	
}
