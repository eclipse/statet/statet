/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.config;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.internal.redocs.tex.r.ui.LtxRweaveTemplates;
import org.eclipse.statet.internal.redocs.tex.r.ui.NewDocTemplateCategoryConfiguration;
import org.eclipse.statet.redocs.tex.r.ui.sourceediting.TexRweaveEditingOptions;


public class TexRweaveUIPreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public TexRweaveUIPreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IScopeContext scope= DefaultScope.INSTANCE;
		
		{	final IEclipsePreferences node= scope.getNode(TexRweaveEditingOptions.LTX_EDITOR_NODE);
			node.putBoolean(TexRweaveEditingOptions.LTX_SPELLCHECK_ENABLED_PREF_KEY, false);
		}
		{	final IEclipsePreferences node= scope.getNode(NewDocTemplateCategoryConfiguration.PREF_QUALIFIER);
			node.put(NewDocTemplateCategoryConfiguration.NEWDOC_DEFAULT_NAME_KEY,
					LtxRweaveTemplates.NEWDOC_TEMPLATE_CATEGORY_ID + ':' + "Article" ); //$NON-NLS-1$
		}
	}
	
}
