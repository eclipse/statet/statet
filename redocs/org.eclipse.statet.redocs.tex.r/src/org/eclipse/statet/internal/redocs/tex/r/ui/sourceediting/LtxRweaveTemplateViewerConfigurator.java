/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.tex.r.ui.sourceediting;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.redocs.tex.r.core.source.doc.LtxRweaveDocumentSetupParticipant;
import org.eclipse.statet.redocs.tex.r.ui.sourceediting.LtxRweaveSourceViewerConfiguration;
import org.eclipse.statet.redocs.tex.r.ui.sourceediting.LtxRweaveSourceViewerConfigurator;


public class LtxRweaveTemplateViewerConfigurator extends LtxRweaveSourceViewerConfigurator {
	
	
	public LtxRweaveTemplateViewerConfigurator(final TexCoreAccess texCore, final RCoreAccess rCore,
			final TemplateVariableProcessor processor) {
		super(texCore, rCore, new LtxRweaveSourceViewerConfiguration(SourceEditorViewerConfiguration.TEMPLATE_MODE) {
			@Override
			protected TemplateVariableProcessor getTemplateVariableProcessor() {
				return processor;
			}
		});
	}
	
	
	@Override
	public IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new LtxRweaveDocumentSetupParticipant(true);
	}
	
}
