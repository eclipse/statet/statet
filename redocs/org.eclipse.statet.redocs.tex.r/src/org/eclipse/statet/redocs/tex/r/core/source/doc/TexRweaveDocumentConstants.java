/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.tex.r.core.source.doc;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.redocs.r.core.source.RweaveDocumentConstants;


@NonNullByDefault
public interface TexRweaveDocumentConstants extends RweaveDocumentConstants {
	
	
	/**
	 * The id of partitioning of Sweave (LaTeX+R) documents.
	 */
	String LTX_R_PARTITIONING= "org.eclipse.statet.LtxRweave"; //$NON-NLS-1$
	
	
	/**
	 * List with all partition content types of Sweave (LaTeX+R) documents.
	 */
	ImList<String> LTX_R_CONTENT_TYPES= ImCollections.concatList(
			TexDocumentConstants.LTX_CONTENT_TYPES,
			RCHUNK_CONTENT_TYPES,
			RDocumentConstants.R_CONTENT_TYPES );
	
}
