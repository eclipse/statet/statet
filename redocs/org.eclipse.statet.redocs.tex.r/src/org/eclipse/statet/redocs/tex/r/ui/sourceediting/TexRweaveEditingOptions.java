/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.tex.r.ui.sourceediting;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;

import org.eclipse.statet.redocs.tex.r.ui.TexRweaveUI;


@NonNullByDefault
public class TexRweaveEditingOptions {
	
	
	public static final String LTX_EDITOR_NODE= TexRweaveUI.BUNDLE_ID + "/editor/Ltx"; //$NON-NLS-1$
	
	
	public static final String LTX_SPELLCHECK_ENABLED_PREF_KEY= "SpellCheck.enabled"; //$NON-NLS-1$
	public static final BooleanPref LTX_SPELLCHECK_ENABLED_PREF= new BooleanPref(
			LTX_EDITOR_NODE, LTX_SPELLCHECK_ENABLED_PREF_KEY);
	
}
