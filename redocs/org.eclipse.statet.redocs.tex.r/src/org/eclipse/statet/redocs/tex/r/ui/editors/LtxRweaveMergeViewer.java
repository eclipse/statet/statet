/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.tex.r.ui.editors;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;
import org.eclipse.statet.ecommons.text.core.PartitionConstraint;

import org.eclipse.statet.ltk.ui.compare.CompareMergeTextViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.redocs.tex.r.core.TexRweaveCore;
import org.eclipse.statet.redocs.tex.r.core.source.doc.LtxRweaveDocumentSetupParticipant;
import org.eclipse.statet.redocs.tex.r.ui.sourceediting.LtxRweaveSourceViewerConfiguration;
import org.eclipse.statet.redocs.tex.r.ui.sourceediting.LtxRweaveSourceViewerConfigurator;


@NonNullByDefault
public class LtxRweaveMergeViewer extends CompareMergeTextViewer {
	
	
	public LtxRweaveMergeViewer(final Composite parent, final CompareConfiguration configuration) {
		super(parent, configuration);
	}
	
	
	@Override
	protected PartitionerDocumentSetupParticipant createDocumentSetupParticipant() {
		return new LtxRweaveDocumentSetupParticipant();
	}
	
	@Override
	protected @Nullable PartitionConstraint getIgnoreWhitespaceExcludePartitionConstraint() {
		return RDocumentConstants.R_SPACE_SENSITIVE_CONSTRAINT;
	}
	
	@Override
	protected SourceEditorViewerConfigurator createConfigurator(final SourceViewer sourceViewer) {
		return new LtxRweaveSourceViewerConfigurator(
				TexRweaveCore.getWorkbenchAccess(), RCore.getWorkbenchAccess(),
				new LtxRweaveSourceViewerConfiguration(SourceEditorViewerConfiguration.COMPARE_MODE) );
	}
	
}
