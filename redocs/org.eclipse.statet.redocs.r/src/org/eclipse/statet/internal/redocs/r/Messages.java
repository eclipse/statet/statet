/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.redocs.r;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String RChunkLaunch_error_message;
	
	public static String ProcessingAction_Weave_label;
	
	public static String ProcessingOperationContext_RConsole_label;
	public static String ProcessingOperationContext_RConsole_RTask_label;
	public static String ProcessingOperationContext_RConsole_RTask_Canceled_label;
	
	public static String ProcessingOperation_RunRConsoleSnippet_label;
	public static String ProcessingOperation_RunRConsoleSnippetSettings_RCode_label;
	public static String ProcessingOperation_Insert_InFileLocVariable_label;
	public static String ProcessingOperation_Insert_OutFileLocVariable_label;
	public static String ProcessingOperation_RunRConsoleSnippet_task;
	public static String ProcessingOperation_RunRConsoleSnippet_RCode_error_SpecMissing_message;
	public static String ProcessingOperation_RunRConsoleSnippet_RCode_error_SpecInvalid_message;
	public static String ProcessingOperation_RunRConsoleSnippet_error_SetWdFailed_message;
	
	public static String ProcessingOperation_RunRCmdTool_label;
	public static String ProcessingOperation_RunRCmdTool_Wd_error_SpecInvalid_message;
	public static String ProcessingOperation_RunRCmdTool_RCmdResource_error_SpecInvalid_message;
	public static String ProcessingOperation_RunRCmdTool_RCmdOptions_error_SpecInvalid_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
