/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.core.model;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.ast.FCall.Args;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


/**
 * R Chunk
 */
@NonNullByDefault
public class RChunkNode implements AstNode {
	
	
	private static final ImList<Object> NO_ATTACHMENT= ImCollections.emptyList();
	
	
	private final AstNode parent;
	// start/stop control chunk
	@Nullable Args weaveArgs;
	ImList<SourceComponent> rSources;
	
	int startOffset;
	int endOffset;
	
	private volatile ImList<Object> attachments= NO_ATTACHMENT;
	
	
	RChunkNode(final AstNode parent) {
		this.parent= parent;
	}
	
	
	@Override
	public int getStatusCode() {
		return 0;
	}
	
	
	@Override
	public AstNode getParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasChildren() {
		return true;
	}
	
	public @Nullable Args getWeaveArgsChild() {
		return this.weaveArgs;
	}
	
	public ImList<SourceComponent> getRCodeChildren() {
		return this.rSources;
	}
	
	@Override
	public int getChildCount() {
		if (this.weaveArgs != null) {
			return this.rSources.size() + 1;
		}
		else {
			return this.rSources.size();
		}
	}
	
	@Override
	public AstNode getChild(final int index) {
		final Args weaveArgs= this.weaveArgs;
		if (weaveArgs != null) {
			if (index == 0) {
				return weaveArgs;
			}
			return this.rSources.get(index - 1);
		}
		else {
			return this.rSources.get(index);
		}
	}
	
	@Override
	public int getChildIndex(final AstNode element) {
		final Args weaveArgs= this.weaveArgs;
		if (weaveArgs != null) {
			if (weaveArgs == element) {
				return 0;
			}
			for (int i= 0; i < this.rSources.size(); i++) {
				if (this.rSources.get(i) == element) {
					return i + 1;
				}
			}
			return -1;
		}
		else {
			for (int i= 0; i < this.rSources.size(); i++) {
				if (this.rSources.get(i) == element) {
					return i;
				}
			}
			return -1;
		}
	}
	
	
	@Override
	public void accept(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		final Args weaveArgs= this.weaveArgs;
		if (weaveArgs != null) {
			visitor.visit(weaveArgs);
		}
		for (final SourceComponent node : this.rSources) {
			visitor.visit(node);
		}
	}
	
	
	@Override
	public int getStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public int getEndOffset() {
		return this.endOffset;
	}
	
	@Override
	public int getLength() {
		return this.endOffset - this.startOffset;
	}
	
	
	@Override
	public synchronized void addAttachment(final Object data) {
		this.attachments= ImCollections.addElement(this.attachments, data);
	}
	
	@Override
	public synchronized void removeAttachment(final Object data) {
		this.attachments= ImCollections.removeElement(this.attachments, data);
	}
	
	@Override
	public ImList<Object> getAttachments() {
		return this.attachments;
	}
	
}
