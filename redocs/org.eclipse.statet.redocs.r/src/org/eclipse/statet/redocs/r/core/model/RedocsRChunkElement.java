/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.core.model;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.rlang.RChunkSrcStrElement;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public class RedocsRChunkElement extends RChunkSrcStrElement {
	
	
	public RedocsRChunkElement(final SourceStructElement<?, ?> parent, final RChunkNode astNode,
			final RElementName name, final @Nullable TextRegion nameRegion) {
		super(parent, astNode, name, nameRegion);
	}
	
	
	@Override
	protected RChunkNode getNode() {
		return (RChunkNode)super.getNode();
	}
	
	@Override
	protected ImList<SourceComponent> getSourceComponents() {
		return getNode().getRCodeChildren();
	}
	
}
