/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.core.source;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;


@NonNullByDefault
public interface RweaveDocumentConstants {
	
	
	String RCHUNK_BASE_CONTENT_TYPE= "RweaveChunk.Base"; //$NON-NLS-1$
	String RCHUNK_CONTROL_CONTENT_TYPE= "RweaveChunk.Control"; //$NON-NLS-1$
	String RCHUNK_COMMENT_CONTENT_TYPE= "RweaveChunk.Comment"; //$NON-NLS-1$
	
	
	ImList<String> RCHUNK_CONTENT_TYPES= ImCollections.newList(
			RCHUNK_BASE_CONTENT_TYPE,
			RCHUNK_CONTROL_CONTENT_TYPE,
			RCHUNK_COMMENT_CONTENT_TYPE );
	
	
	PartitionConstraint RCHUNK_PARTITION_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
			return (contentType == RCHUNK_BASE_CONTENT_TYPE
					|| contentType == RCHUNK_CONTROL_CONTENT_TYPE
					|| contentType == RCHUNK_COMMENT_CONTENT_TYPE );
		}
	};
	
	
}
