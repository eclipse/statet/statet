/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.ui.processing;

import java.util.Map;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.ecommons.variables.core.VariableText2;
import org.eclipse.statet.ecommons.variables.core.VariableText2.Severities;
import org.eclipse.statet.ecommons.variables.core.VariableUtils;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingOperation;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolConfig.StepConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolOperationContext;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolProcess;
import org.eclipse.statet.internal.redocs.r.Messages;
import org.eclipse.statet.nico.core.runtime.IRequireSynch;
import org.eclipse.statet.r.console.core.IRBasicAdapter;
import org.eclipse.statet.r.console.core.RWorkspace;
import org.eclipse.statet.r.console.core.util.RCodeVariableText;
import org.eclipse.statet.r.core.RUtil;
import org.eclipse.statet.redocs.r.RedocsRweave;


public class RunRConsoleSnippetOperation extends DocProcessingOperation {
	
	
	public static final String ID= "org.eclipse.statet.redocs.docProcessing.RunRConsoleSnippetOperation"; //$NON-NLS-1$
	
	public static final String R_SNIPPET_CODE_ATTR_NAME= ID + '/' + "RSnippet.code"; //$NON-NLS-1$
	
	
	private String rCodeSnippet;
	
	
	public RunRConsoleSnippetOperation() {
	}
	
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_RunRConsoleSnippet_label;
	}
	
	
	@Override
	public void init(final StepConfig stepConfig, final Map<String, String> settings,
			final SubMonitor m) throws CoreException {
		super.init(stepConfig, settings, m);
		
		final String code= settings.get(R_SNIPPET_CODE_ATTR_NAME);
		if (code == null) {
			throw new CoreException(new Status(IStatus.ERROR, RedocsRweave.BUNDLE_ID,
					Messages.ProcessingOperation_RunRConsoleSnippet_RCode_error_SpecMissing_message ));
		}
		try {
			getStepConfig().getVariableResolver().validate(code, Severities.CHECK_SYNTAX, null);
		}
		catch (final CoreException e) {
			throw new CoreException(new Status(IStatus.ERROR, RedocsRweave.BUNDLE_ID,
					NLS.bind(Messages.ProcessingOperation_RunRConsoleSnippet_RCode_error_SpecInvalid_message,
							e.getMessage() )));
		}
		this.rCodeSnippet= code;
	}
	
	
	@Override
	public String getContextId() {
		return RConsoleOperationContext.ID;
	}
	
	@Override
	public DocProcessingToolOperationContext createContext() {
		return new RConsoleOperationContext();
	}
	
	
	@Override
	public IStatus run(final DocProcessingToolProcess toolProcess,
			final SubMonitor m) throws CoreException {
		final RConsoleOperationContext context= (RConsoleOperationContext) toolProcess.getCurrentOperationContext();
		final IRBasicAdapter r= context.getRService();
		final ProgressMonitor rMonitor= context.getRMonitor();
		try {
			m.beginTask(NLS.bind(Messages.ProcessingOperation_RunRConsoleSnippet_label,
							r.getTool().getLabel() ),
					10 );
			
			r.briefAboutToChange();
			final RWorkspace rWorkspace= r.getWorkspace();
			final VariableText2 variableResolver= new RCodeVariableText(rWorkspace,
					getStepConfig().getVariableResolver().getExtraVariables() );
			
			{	// Set wd
				final IFileStore dir= FileUtil.getFileStore(VariableUtils.getValue(
						variableResolver.getVariable(DocProcessingConfig.WD_LOC_VAR_NAME)) );
				IFileStore currentDir= rWorkspace.getWorkspaceDir();
				if (!dir.equals(currentDir)) {
					final String command= variableResolver.performStringSubstitution("setwd(\"${wd_loc}\")", null); //$NON-NLS-1$
					r.submitToConsole(command, rMonitor);
					r.refreshWorkspaceData(0, rMonitor);
					currentDir= rWorkspace.getWorkspaceDir();
					if (!dir.equals(currentDir)) {
						toolProcess.check(new Status(IStatus.ERROR, RedocsRweave.BUNDLE_ID,
								NLS.bind(Messages.ProcessingOperation_RunRConsoleSnippet_error_SetWdFailed_message,
										dir.toString(),
										(currentDir != null) ? currentDir.toString() : "<missing>" ) )); //$NON-NLS-1$
					}
				}
			}
			
			try {
				final String code;
				try {
					code= variableResolver.performStringSubstitution(this.rCodeSnippet, null);
				}
				catch (final CoreException e) {
					throw new CoreException(new Status(IStatus.ERROR, RedocsRweave.BUNDLE_ID,
							NLS.bind(Messages.ProcessingOperation_RunRConsoleSnippet_RCode_error_SpecInvalid_message,
									e.getMessage() )));
				}
				final String[] codeLines= RUtil.LINE_SEPARATOR_PATTERN.split(code);
				for (int i= 0; i < codeLines.length; i++) {
					r.submitToConsole(codeLines[i], rMonitor);
				}
				if (r instanceof IRequireSynch) {
					((IRequireSynch) r).synch(rMonitor);
				}
			}
			finally {
				r.briefChanged(RWorkspace.REFRESH_AUTO);
			}
		}
		catch (final StatusException e) {
			final org.eclipse.statet.jcommons.status.Status status= e.getStatus();
			throw new CoreException(new Status(status.getSeverity(), RedocsRweave.BUNDLE_ID,
					((status.getSeverity() & (IStatus.ERROR | IStatus.WARNING)) != 0) ?
							"An error occurred when running commands in R." : status.getMessage(),
					e ));
		}
		
		return Status.OK_STATUS;
	}
	
}
