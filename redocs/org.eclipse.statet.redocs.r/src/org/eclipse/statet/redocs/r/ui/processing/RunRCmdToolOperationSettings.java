/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.ui.processing;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

import org.eclipse.statet.docmlet.base.ui.processing.operations.AbstractLaunchConfigOperationSettings;
import org.eclipse.statet.internal.redocs.r.Messages;
import org.eclipse.statet.r.cmd.ui.launching.RCmdLaunching;


public class RunRCmdToolOperationSettings extends AbstractLaunchConfigOperationSettings {
	
	
	public RunRCmdToolOperationSettings() {
		super(RCmdLaunching.R_CMD_CONFIGURATION_TYPE_ID);
	}
	
	
	@Override
	public String getId() {
		return RunRCmdToolOperation.ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_RunRCmdTool_label;
	}
	
	
	@Override
	protected boolean includeLaunchConfig(final ILaunchConfiguration config) {
		try {
			final String cmd= config.getAttribute(RCmdLaunching.R_CMD_COMMAND_ATTR_NAME, ""); //$NON-NLS-1$
			return (cmd.equals("CMD Sweave")); //$NON-NLS-1$
		}
		catch (final CoreException e) {
			return false;
		}
	}
	
	@Override
	protected String getNewLaunchConfigName() {
		return "R CMD Sweave"; //$NON-NLS-1$
	}
	
	@Override
	protected void initializeNewLaunchConfig(final ILaunchConfigurationWorkingCopy config) {
		RCmdLaunching.initializeRCmdConfig(config, "CMD Sweave"); //$NON-NLS-1$
	}
	
}
