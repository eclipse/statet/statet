/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.ui.processing;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingManager;
import org.eclipse.statet.internal.redocs.r.Messages;
import org.eclipse.statet.redocs.r.ui.RedocsRUIResources;


@NonNullByDefault
public class RweaveDocProcessingManager extends DocProcessingManager {
	
	
	public RweaveDocProcessingManager() {
	}
	
	
	@Override
	protected @Nullable Image getActionImage(final byte bits) {
		switch (bits) {
		case WEAVE_BIT:
			return RedocsRUIResources.INSTANCE.getImage(RedocsRUIResources.TOOL_RWEAVE_IMAGE_ID);
		case PRODUCE_OUTPUT_BIT:
			return RedocsRUIResources.INSTANCE.getImage(RedocsRUIResources.TOOL_BUILDTEX_IMAGE_ID);
		default:
			return super.getActionImage(bits);
		}
	}
	
	@Override
	protected String getActionLabel(final byte bits) {
		switch (bits) {
		case WEAVE_BIT:
			return Messages.ProcessingAction_Weave_label;
		default:
			return super.getActionLabel(bits);
		}
	}
	
}
