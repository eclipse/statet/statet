/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.ui.sourceediting;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.ui.editors.RSourceEditor;
import org.eclipse.statet.redocs.r.core.source.DocContentSectionsRweaveExtension;


@NonNullByDefault
public interface RweaveEditor extends RSourceEditor {
	
	
	@Override
	DocContentSectionsRweaveExtension getDocumentContentInfo();
	
}
