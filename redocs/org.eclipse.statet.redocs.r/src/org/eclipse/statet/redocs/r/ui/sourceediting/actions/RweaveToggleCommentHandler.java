/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.ui.sourceediting.actions;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextSelection;

import org.eclipse.statet.ltk.ui.EditorUtils;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.actions.ToggleCommentHandler;
import org.eclipse.statet.redocs.r.ui.sourceediting.RweaveEditor;


public abstract class RweaveToggleCommentHandler extends ToggleCommentHandler {
	
	
	public RweaveToggleCommentHandler(final SourceEditor1 editor) {
		super(editor);
		assert (editor instanceof RweaveEditor);
	}
	
	
	@Override
	protected void run(final AbstractDocument document, final ITextSelection selection,
			final int operationCode) {
		try {
			if (isMixed(document, selection)) {
				final IRegion block= EditorUtils.getTextBlockFromSelection(document,
						selection.getOffset(), selection.getLength() );
				switch (operationCode) {
				case ITextOperationTarget.PREFIX:
					doPrefixPrimary(document, block);
					return;
				case ITextOperationTarget.STRIP_PREFIX:
					doStripPrefix(document, block);
					return;
				default:
					throw new IllegalArgumentException("operationCode= " + operationCode); //$NON-NLS-1$
				}
			}
		}
		catch (final BadLocationException | BadPartitioningException e) {
			log(e);
		}
		
		doRunOperation(operationCode);
	}
	
	protected boolean isMixed(final IDocument document, final ITextSelection selection)
			throws BadLocationException, BadPartitioningException {
		final IRegion block= EditorUtils.getTextBlockFromSelection(document,
				selection.getOffset(), selection.getLength() );
		final IRegion rContent= ((RweaveEditor) getEditor()).getDocumentContentInfo()
				.getRChunkContentRegion(document, block.getOffset());
		return (rContent == null || block.getOffset() < rContent.getOffset()
				|| block.getOffset() + block.getLength() > rContent.getOffset() + rContent.getLength() );
	}
	
	
	protected abstract void doPrefixPrimary(AbstractDocument document, IRegion block)
			throws BadLocationException, BadPartitioningException;
	
}
