/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.redocs.r.core.source;

import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;


@NonNullByDefault
public interface DocContentSectionsRweaveExtension extends DocContentSections {
	
	
	@Nullable TreePartitionNode getRChunkRegion(final IDocument document, final int offset)
			throws BadLocationException;
	
	List<TreePartitionNode> getRChunkRegions(final IDocument document,
			final int offset, final int length)
			throws BadLocationException;
	
	@Nullable IRegion getRChunkContentRegion(final IDocument document, final int offset)
			throws BadLocationException;
	
	List<TreePartitionNode> getRChunkCodeRegions(final IDocument document,
			final int offset, final int length)
			throws BadLocationException;
	
	@Nullable TreePartitionNode getRCodeRegion(final IDocument document, final int offset)
			throws BadLocationException;
	
}
