/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.print;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


/**
 * ILayerCommandHandler for handling the PrintCommand.
 * Simply delegates to the 
 */
@NonNullByDefault
public class PrintCommandHandler implements LayerCommandHandler<PrintCommand> {
	
	
	private final Layer layer;
	
	
	/**
	 * @param layer The layer that should be printed. 
	 * 			Usually the top most layer to print, e.g. the GridLayer.
	 */
	public PrintCommandHandler(final Layer layer) {
		this.layer= layer;
	}
	
	
	@Override
	public Class<PrintCommand> getCommandClass() {
		return PrintCommand.class;
	}
	
	@Override
	public boolean executeCommand(final PrintCommand command) {
		new LayerPrinter(this.layer, command.getConfigRegistry())
				.print(command.getShell());
		return true;
	}
	
}
