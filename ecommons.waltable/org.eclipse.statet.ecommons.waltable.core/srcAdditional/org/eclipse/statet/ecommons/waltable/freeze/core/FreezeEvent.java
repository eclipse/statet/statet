/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.events.GeneralStructuralChangeEvent;


@NonNullByDefault
public class FreezeEvent extends GeneralStructuralChangeEvent {
	
	
	public FreezeEvent(final Layer layer) {
		super(layer);
	}
	
	@Override
	public @Nullable FreezeEvent toLayer(final Layer targetLayer) {
		if (targetLayer == getLayer()) {
			return this;
		}
		
		return new FreezeEvent(targetLayer);
	}
	
	
}
