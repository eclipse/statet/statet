/*=============================================================================#
 # Copyright (c) 2013, 2025 Dirk Fauth and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Dirk Fauth <dirk.fauth@gmail.com> - initial API and implementation
 #=============================================================================*/ 
package org.eclipse.statet.ecommons.waltable.export;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;


/**
 * Configuration attributes that are used to configure the export functionality.
 */
public interface ExportConfigAttributes {

	/**
	 * The configuration attribute for specifying the concrete implementation instance of
	 * ILayerExporter that should be used for exporting the NatTable.
	 */
	ConfigAttribute<ILayerExporter> EXPORTER= new ConfigAttribute<>();
	/**
	 * The configuration attribute for specifying a formatter that should be used to
	 * format the values for the export.
	 */
	ConfigAttribute<IExportFormatter> EXPORT_FORMATTER= new ConfigAttribute<>();
	/**
	 * The configuration attribute for specifying the format that should be used for exporting
	 * Calendar or Date objects appropriately.
	 */
	ConfigAttribute<String> DATE_FORMAT= new ConfigAttribute<>();

}
