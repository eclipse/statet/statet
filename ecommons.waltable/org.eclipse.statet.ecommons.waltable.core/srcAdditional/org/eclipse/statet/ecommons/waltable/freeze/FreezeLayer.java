/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionOutOfBoundsException;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.TransformLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.TransformLayerDim;


@NonNullByDefault
public class FreezeLayer extends TransformLayer<TransformLayerDim<FreezeLayer>> {
	
	
	public static final String PERSISTENCE_TOP_LEFT_POSITION= ".freezeTopLeftPosition"; //$NON-NLS-1$
	public static final String PERSISTENCE_BOTTOM_RIGHT_POSITION= ".freezeBottomRightPosition"; //$NON-NLS-1$
	
	
	static class Dim extends TransformLayerDim<FreezeLayer> {
		
		
		private long startPosition;
		private long endPosition;
		
		
		public Dim(final FreezeLayer layer, final LayerDim underlyingDim) {
			super(layer, underlyingDim);
		}
		
		
		public void setFreeze(final long start, final long end) {
			if (start < 0 || start > end) {
				throw new IllegalArgumentException();
			}
			this.startPosition= start;
			this.endPosition= end;
		}
		
		public long getStartPosition() {
			return this.startPosition;
		}
		
		public long getEndPosition() {
			return this.endPosition;
		}
		
		private long getStartPixel() {
			return this.underlyingDim.getPositionStart(this.startPosition);
		}
		
		
		@Override
		public long getPositionCount() {
			return this.endPosition - this.startPosition;
		}
		
		@Override
		public long localToUnderlyingPosition(final long refPosition, final long position) {
			if (refPosition < 0 || refPosition >= getPositionCount()) {
				throw PositionOutOfBoundsException.refPosition(refPosition, getOrientation()); 
			}
			
			return this.startPosition + position;
		}
		
		@Override
		protected long doUnderlyingToLocalPosition(final long underlyingPosition) {
			return underlyingPosition - this.startPosition;
		}
		
		
		@Override
		public long getSize() {
			long size= 0;
			for (long position= this.startPosition; position < this.endPosition; position++) {
				size+= this.underlyingDim.getPositionSize(position, position);
			}
			return size;
		}
		
		@Override
		public long getPreferredSize() {
			return getSize();
		}
		
		@Override
		public long getPositionByPixel(final long pixel) {
			final long underlyingPosition= this.underlyingDim.getPositionByPixel(getStartPixel() + pixel);
			return underlyingToLocalPosition(this.underlyingDim, underlyingPosition);
		}
		
		@Override
		public long getPositionStart(final long refPosition, final long position) {
			return super.getPositionStart(refPosition, position) - getStartPixel();
		}
		
		@Override
		public long getPositionStart(final long position) {
			return super.getPositionStart(position) - getStartPixel();
		}
		
	}
	
	
	public FreezeLayer(final Layer underlyingLayer) {
		super(underlyingLayer);
		
		init();
		
		registerEventHandler(new FreezeEventHandler(this));
	}
	
	@Override
	protected Dim createDim(final Orientation orientation) {
		final Layer underlyingLayer= getUnderlyingLayer();
		return new Dim(this, underlyingLayer.getDim(orientation));
	}
	
	
	final Dim get(final Orientation orientation) {
		return (Dim)getDim(orientation);
	}
	
	
	public boolean isFrozen() {
		return (getColumnCount() > 0 || getRowCount() > 0);
	}
	
	public PositionCoordinate getTopLeftPosition() {
		return new PositionCoordinate(this,
				get(HORIZONTAL).startPosition,
				get(VERTICAL).startPosition );
	}
	
	public PositionCoordinate getBottomRightPosition() {
		return new PositionCoordinate(this,
				get(HORIZONTAL).endPosition - 1,
				get(VERTICAL).endPosition - 1 );
	}
	
	public void setFreeze(final long leftColumnPosition, final long topRowPosition,
			final long rightColumnPosition, final long bottomRowPosition) {
		if (rightColumnPosition < leftColumnPosition || bottomRowPosition < topRowPosition) {
			throw new IllegalArgumentException();
		}
		if (leftColumnPosition < 0) {
			get(HORIZONTAL).setFreeze(0, 0);
		}
		else {
			get(HORIZONTAL).setFreeze(leftColumnPosition, rightColumnPosition + 1);
		}
		if (topRowPosition < 0) {
			get(VERTICAL).setFreeze(0, 0);
		}
		else {
			get(VERTICAL).setFreeze(topRowPosition, bottomRowPosition + 1);
		}
	}
	
}
