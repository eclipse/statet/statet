/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.tickupdate.config;

import org.eclipse.swt.SWT;

import org.eclipse.statet.ecommons.waltable.config.AbstractLayerConfiguration;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;
import org.eclipse.statet.ecommons.waltable.tickupdate.ITickUpdateHandler;
import org.eclipse.statet.ecommons.waltable.tickupdate.TickUpdateCommandHandler;
import org.eclipse.statet.ecommons.waltable.tickupdate.TickUpdateConfigAttributes;
import org.eclipse.statet.ecommons.waltable.tickupdate.ui.action.TickUpdateAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.KeyEventMatcher;


/**
 * The default configuration for tick update handling. Will register the default
 * {@link ITickUpdateHandler#DEFAULT_TICK_UPDATE_HANDLER} to be the update handler
 * for tick updates and key bindings on keypad add and keybadd subtract to call 
 * the corresponding {@link TickUpdateAction}.
 */
public class DefaultTickUpdateConfiguration extends AbstractLayerConfiguration<SelectionLayer> {
	
	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		configRegistry.registerAttribute(
				TickUpdateConfigAttributes.UPDATE_HANDLER, ITickUpdateHandler.DEFAULT_TICK_UPDATE_HANDLER);
	}

	@Override
	public void configureTypedLayer(final SelectionLayer selectionLayer) {
		selectionLayer.registerCommandHandler(new TickUpdateCommandHandler(selectionLayer));
	}
	
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		uiBindingRegistry.registerKeyBinding(
				new KeyEventMatcher(SWT.NONE, SWT.KEYPAD_ADD), 
				new TickUpdateAction(true));

		uiBindingRegistry.registerKeyBinding(
				new KeyEventMatcher(SWT.NONE, SWT.KEYPAD_SUBTRACT), 
				new TickUpdateAction(false));
	}
	
}
