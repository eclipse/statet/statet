/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.export;

import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


@NonNullByDefault
public class ExportCommandHandler implements LayerCommandHandler<ExportCommand> {
	
	
	private final Layer layer;
	
	
	public ExportCommandHandler(final Layer layer) {
		this.layer= layer;
	}
	
	
	@Override
	public Class<ExportCommand> getCommandClass() {
		return ExportCommand.class;
	}
	
	
	@Override
	public boolean executeCommand(final ExportCommand command) {
		final Shell shell= command.getShell();
		final ConfigRegistry configRegistry= command.getConfigRegistry();
		
		new NatExporter(shell).exportSingleLayer(this.layer, configRegistry);
		
		return true;
	}
	
}
