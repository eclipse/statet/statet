/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze.ui.action;

import org.eclipse.swt.SWT;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.config.AbstractUiBindingConfiguration;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.KeyEventMatcher;


@NonNullByDefault
public class DefaultFreezeGridBindings extends AbstractUiBindingConfiguration {
	
	
	public DefaultFreezeGridBindings() {
	}
	
	
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.MOD1 | SWT.MOD2, 'f'),
				new FreezeGridAction() );
		uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.MOD1 | SWT.MOD2, 'u'),
				new UnFreezeGridAction() );
	}
	
}
