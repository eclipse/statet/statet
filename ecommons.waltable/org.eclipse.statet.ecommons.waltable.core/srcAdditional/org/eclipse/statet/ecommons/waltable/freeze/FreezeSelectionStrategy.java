/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;
import org.eclipse.statet.ecommons.waltable.viewport.core.ViewportLayer;


public class FreezeSelectionStrategy implements IFreezeCoordinatesProvider {

	private final FreezeLayer freezeLayer;

	private final ViewportLayer viewportLayer;
	
	private final SelectionLayer selectionLayer;

	public FreezeSelectionStrategy(final FreezeLayer freezeLayer, final ViewportLayer viewportLayer, final SelectionLayer selectionLayer) {
		this.freezeLayer= freezeLayer;
		this.viewportLayer= viewportLayer;
		this.selectionLayer= selectionLayer;
	}

	@Override
	public PositionCoordinate getTopLeftPosition() {
		final PositionCoordinate selectionAnchor= this.selectionLayer.getSelectionAnchor();
		if (selectionAnchor == null) {
			return null;
		}
		
		return new PositionCoordinate(this.freezeLayer,
				checkPosition(HORIZONTAL, selectionAnchor.columnPosition),
				checkPosition(VERTICAL, selectionAnchor.rowPosition) );
	}
	
	private long checkPosition(final Orientation orientation, final long maxPosition) {
		final var viewportDim= this.viewportLayer.getDim(orientation);
		if (viewportDim.getSize() > 0) {
			final long position= viewportDim.getOriginPosition();
			if (position < maxPosition) {
				return position;
			}
		}
		return -1;
	}
	
	@Override
	public PositionCoordinate getBottomRightPosition() {
		final PositionCoordinate selectionAnchor= this.selectionLayer.getSelectionAnchor();
		if (selectionAnchor == null) {
			return null;
		}
		return new PositionCoordinate(this.freezeLayer, selectionAnchor.columnPosition - 1, selectionAnchor.rowPosition - 1);
	}

}
