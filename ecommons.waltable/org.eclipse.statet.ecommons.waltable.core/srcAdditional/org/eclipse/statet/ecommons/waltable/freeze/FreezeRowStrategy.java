/*=============================================================================#
 # Copyright (c) 2013, 2025 Dirk Fauth and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Dirk Fauth <dirk.fauth@gmail.com> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;


public class FreezeRowStrategy implements IFreezeCoordinatesProvider {

	private final FreezeLayer freezeLayer;
	
	private final long rowPosition;

	public FreezeRowStrategy(final FreezeLayer freezeLayer, final long rowPosition) {
		this.freezeLayer= freezeLayer;
		this.rowPosition= rowPosition;
	}

	@Override
	public PositionCoordinate getTopLeftPosition() {
		return new PositionCoordinate(this.freezeLayer, -1, 0);
	}

	@Override
	public PositionCoordinate getBottomRightPosition() {
		return new PositionCoordinate(this.freezeLayer, -1, this.rowPosition);
	}

}
