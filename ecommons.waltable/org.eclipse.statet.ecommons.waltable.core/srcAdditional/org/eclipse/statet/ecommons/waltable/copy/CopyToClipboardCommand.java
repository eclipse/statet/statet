/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.copy;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;


public class CopyToClipboardCommand extends AbstractContextFreeCommand {
	
	
	private final String cellDelimeter;
	private final String rowDelimeter;
	private final ConfigRegistry configRegistry;
	
	
	public CopyToClipboardCommand(final String cellDelimeter, final String rowDelimeter, final ConfigRegistry configRegistry) {
		this.cellDelimeter= cellDelimeter;
		this.rowDelimeter= rowDelimeter;
		this.configRegistry= configRegistry;
	}
	
	
	public String getCellDelimeter() {
		return this.cellDelimeter;
	}
	
	public String getRowDelimeter() {
		return this.rowDelimeter;
	}
	
	public ConfigRegistry getConfigRegistry() {
		return this.configRegistry;
	}
	
}
