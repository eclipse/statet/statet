/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze.swt;

import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.layer.CompositeLayer.CompositeLayerPainter;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.freeze.IFreezeConfigAttributes;
import org.eclipse.statet.ecommons.waltable.freeze.core.CompositeFreezeLayer;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


@NonNullByDefault
public class CompositeFreezeLayerPainter extends CompositeLayerPainter {
	
	
	public CompositeFreezeLayerPainter(final CompositeFreezeLayer layer) {
		super(layer);
	}
	
	
	protected Layer getFreezeLayer() {
		return this.layer.getChildLayerByLayoutCoordinate(0, 0);
	}
	
	@Override
	public void paintLayer(final Layer natLayer, final GC gc, final int xOffset, final int yOffset,
			final Rectangle rectangle, final ConfigRegistry configRegistry) {
		super.paintLayer(natLayer, gc, xOffset, yOffset, rectangle, configRegistry);
		final var freezeLayer= getFreezeLayer();
		
		Color separatorColor= configRegistry.getAttribute(IFreezeConfigAttributes.SEPARATOR_COLOR, DisplayMode.NORMAL);
		if (separatorColor == null) {
			separatorColor= GUIHelper.COLOR_BLUE;
		}
		
		gc.setClipping(rectangle);
		final Color oldFg= gc.getForeground();
		gc.setForeground(separatorColor);
		final long freezeWidth= freezeLayer.getWidth() - 1;
		if (freezeWidth > 0) {
			gc.drawLine(safe(xOffset + freezeWidth), yOffset,
					safe(xOffset + freezeWidth), safe(yOffset + this.layer.getHeight() - 1));
		}
		final long freezeHeight= freezeLayer.getHeight() - 1;
		if (freezeHeight > 0) {
			gc.drawLine(xOffset, safe(yOffset + freezeHeight),
					safe(xOffset + this.layer.getWidth() - 1), safe(yOffset + freezeHeight));
		}
		gc.setForeground(oldFg);
	}
	
}
