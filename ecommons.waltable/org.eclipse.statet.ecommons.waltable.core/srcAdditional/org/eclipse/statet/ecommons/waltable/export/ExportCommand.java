/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.export;

import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;


public class ExportCommand extends AbstractContextFreeCommand {

	private final ConfigRegistry configRegistry;
	private final Shell shell;

	public ExportCommand(final ConfigRegistry configRegistry, final Shell shell) {
		this.configRegistry= configRegistry;
		this.shell= shell;
	}

	public ConfigRegistry getConfigRegistry() {
		return this.configRegistry;
	}

	public Shell getShell() {
		return this.shell;
	}
}
