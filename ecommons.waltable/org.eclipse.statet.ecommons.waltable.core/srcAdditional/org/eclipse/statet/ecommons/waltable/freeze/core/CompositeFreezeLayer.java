/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.freeze.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import java.util.Map;
import java.util.StringTokenizer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.Persistable;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.layer.AbstractLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.CompositeLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.freeze.FreezeCommandHandler;
import org.eclipse.statet.ecommons.waltable.freeze.FreezeHelper;
import org.eclipse.statet.ecommons.waltable.freeze.FreezeLayer;
import org.eclipse.statet.ecommons.waltable.freeze.swt.CompositeFreezeLayerPainter;
import org.eclipse.statet.ecommons.waltable.freeze.ui.action.DefaultFreezeGridBindings;
import org.eclipse.statet.ecommons.waltable.grid.core.ClientAreaResizeCommand;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.DimensionallyDependentLayer;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;
import org.eclipse.statet.ecommons.waltable.viewport.core.ViewportLayer;
import org.eclipse.statet.ecommons.waltable.viewport.core.ViewportSelectDimPositionsCommandHandler;


@NonNullByDefault
public class CompositeFreezeLayer extends CompositeLayer {
	
	
	protected final FreezeLayer freezeLayer;
	
	protected final ViewportLayer viewportLayer;
	
	protected final SelectionLayer selectionLayer;
	
	
	public CompositeFreezeLayer(final FreezeLayer freezeLayer, final ViewportLayer viewportLayer,
			final SelectionLayer selectionLayer) {
		this(freezeLayer, viewportLayer, selectionLayer, true);
	}
	
	public CompositeFreezeLayer(final FreezeLayer freezeLayer, final ViewportLayer viewportLayer,
			final SelectionLayer selectionLayer,
			final boolean useDefaultConfiguration) {
		super(2, 2);
		this.freezeLayer= freezeLayer;
		this.viewportLayer= viewportLayer;
		this.selectionLayer= selectionLayer;
		
		setChildLayer("FROZEN_REGION", freezeLayer, 0, 0); //$NON-NLS-1$
		setChildLayer("FROZEN_ROW_REGION", new DimensionallyDependentLayer( //$NON-NLS-1$
						viewportLayer.getScrollableLayer(), viewportLayer, freezeLayer),
				1, 0);
		setChildLayer("FROZEN_COLUMN_REGION", new DimensionallyDependentLayer( //$NON-NLS-1$
						viewportLayer.getScrollableLayer(), freezeLayer, viewportLayer),
				0, 1);
		setChildLayer("NONFROZEN_REGION", viewportLayer, 1, 1); //$NON-NLS-1$
		
		registerCommandHandlers();
		
		if (useDefaultConfiguration) {
			addConfiguration(new DefaultFreezeGridBindings());
		}
	}
	
	
	@Override
	protected LayerPainter createPainter() {
		return new CompositeFreezeLayerPainter(this);
	}
	
	
	public boolean isFrozen() {
		return this.freezeLayer.isFrozen();
	}
	
	@Override
	protected void registerCommandHandlers() {
		registerCommandHandler(new FreezeCommandHandler(this.freezeLayer, this.viewportLayer, this.selectionLayer));
		
		final AbstractLayer frozenRowLayer= (AbstractLayer) getChildLayerByLayoutCoordinate(1, 0);
		frozenRowLayer.registerCommandHandler(new ViewportSelectDimPositionsCommandHandler(
				frozenRowLayer, VERTICAL ));
		
		final AbstractLayer frozenColumnLayer= (AbstractLayer) getChildLayerByLayoutCoordinate(0, 1);
		frozenColumnLayer.registerCommandHandler(new ViewportSelectDimPositionsCommandHandler(
				frozenRowLayer, HORIZONTAL ));
	}
	
	
	@Override
	public boolean doCommand(final LayerCommand command) {
		//if this layer should handle a ClientAreaResizeCommand we have to ensure that
		//it is only called on the ViewportLayer, as otherwise an undefined behaviour
		//could occur because the ViewportLayer isn't informed about potential refreshes
		if (command instanceof ClientAreaResizeCommand) {
			this.viewportLayer.doCommand(command);
		}
		return super.doCommand(command);
	}
	
	
	// Persistence
	
	@Override
	public void saveState(final String prefix, final Map<String, String> properties) {
		PositionCoordinate coord= this.freezeLayer.getTopLeftPosition();
		properties.put(prefix + FreezeLayer.PERSISTENCE_TOP_LEFT_POSITION, 
				coord.columnPosition + Persistable.VALUE_SEPARATOR + coord.rowPosition);
		
		coord= this.freezeLayer.getBottomRightPosition();
		properties.put(prefix + FreezeLayer.PERSISTENCE_BOTTOM_RIGHT_POSITION, 
				coord.columnPosition + Persistable.VALUE_SEPARATOR + coord.rowPosition);
		
		super.saveState(prefix, properties);
	}
	
	@Override
	public void loadState(final String prefix, final Map<String, String> properties) {
		String property= properties.get(prefix + FreezeLayer.PERSISTENCE_TOP_LEFT_POSITION);
		PositionCoordinate topLeftPosition= null;
		if (property != null) {
			final StringTokenizer tok= new StringTokenizer(property, Persistable.VALUE_SEPARATOR);
			final String columnPosition= tok.nextToken();
			final String rowPosition= tok.nextToken();
			topLeftPosition= new PositionCoordinate(this.freezeLayer, 
					Long.parseLong(columnPosition), Long.parseLong(rowPosition));
		}
		
		property= properties.get(prefix + FreezeLayer.PERSISTENCE_BOTTOM_RIGHT_POSITION);
		PositionCoordinate bottomRightPosition= null;
		if (property != null) {
			final StringTokenizer tok= new StringTokenizer(property, Persistable.VALUE_SEPARATOR);
			final String columnPosition= tok.nextToken();
			final String rowPosition= tok.nextToken();
			bottomRightPosition= new PositionCoordinate(this.freezeLayer, 
					Long.parseLong(columnPosition), Long.parseLong(rowPosition));
		}
		
		//only restore a freeze state if there is one persisted
		if (topLeftPosition != null && bottomRightPosition != null) {
			if (topLeftPosition.columnPosition == -1 && topLeftPosition.rowPosition == -1
					&& bottomRightPosition.columnPosition == -1 && bottomRightPosition.rowPosition == -1) {
				FreezeHelper.unfreeze(this.freezeLayer, this.viewportLayer);
			} else {
				FreezeHelper.freeze(this.freezeLayer, this.viewportLayer, topLeftPosition, bottomRightPosition);
			}
		}
		
		super.loadState(prefix, properties);
	}
	
}
