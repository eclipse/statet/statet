/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.print;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;


/**
 * The viewport picks this up and moved to 0,0 so that
 * the entire grid can be printed.
 */
public class PrintEntireGridCommand extends AbstractContextFreeCommand {

}
