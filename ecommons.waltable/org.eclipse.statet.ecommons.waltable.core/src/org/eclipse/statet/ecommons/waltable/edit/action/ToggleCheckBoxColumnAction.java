/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit.action;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;
import org.eclipse.statet.ecommons.waltable.edit.UpdateDataCommand;
import org.eclipse.statet.ecommons.waltable.painter.cell.ColumnHeaderCheckBoxPainter;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;


public class ToggleCheckBoxColumnAction implements IMouseAction {

	private final ColumnHeaderCheckBoxPainter columnHeaderCheckBoxPainter;
	private final Layer bodyDataLayer;

	public ToggleCheckBoxColumnAction(final ColumnHeaderCheckBoxPainter columnHeaderCheckBoxPainter, final Layer bodyDataLayer) {
		this.columnHeaderCheckBoxPainter= columnHeaderCheckBoxPainter;
		this.bodyDataLayer= bodyDataLayer;
	}
	
	@Override
	public void run(final NatTable natTable, final MouseEvent event) {
		final long sourceColumnPosition= natTable.getColumnPositionByX(event.x);
		final long columnPosition= LayerUtils.convertColumnPosition(natTable, sourceColumnPosition, this.bodyDataLayer);
		
		final long checkedCellsCount= this.columnHeaderCheckBoxPainter.getCheckedCellsCount(columnPosition, natTable.getConfigRegistry());
		final boolean targetState= checkedCellsCount < this.bodyDataLayer.getRowCount();
		
		for (long rowPosition= 0; rowPosition < this.bodyDataLayer.getRowCount(); rowPosition++) {
			this.bodyDataLayer.doCommand(new UpdateDataCommand(this.bodyDataLayer, columnPosition, rowPosition, targetState));
		}
	}

}
