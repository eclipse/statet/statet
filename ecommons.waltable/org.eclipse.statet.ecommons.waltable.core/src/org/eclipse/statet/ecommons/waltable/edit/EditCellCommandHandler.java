/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.waltable.config.IEditableRule;
import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;


/**
 * Command handler for handling {@link EditCellCommand}s.
 * Will first check if putting the cell into edit mode is allowed. If it is
 * allowed it will call the {@link EditController} for activation of the edit
 * mode.
 */
public class EditCellCommandHandler extends AbstractLayerCommandHandler<EditCellCommand> {

	@Override
	public Class<EditCellCommand> getCommandClass() {
		return EditCellCommand.class;
	}
	
	@Override
	public boolean doCommand(final EditCellCommand command) {
		final LayerCell cell= command.getCell();
		final Composite parent= command.getParent();
		final ConfigRegistry configRegistry= command.getConfigRegistry();
		
		//check if the cell is editable
		final IEditableRule rule= configRegistry.getAttribute(
				EditConfigAttributes.CELL_EDITABLE_RULE, 
				DisplayMode.EDIT, cell.getLabels().getLabels());
		
		if (rule.isEditable(cell, configRegistry)) {
			EditController.editCell(cell, parent, cell.getDataValue(0, null), configRegistry);
		}

		//as commands by default are intended to be consumed by the handler, always
		//return true, whether the activation of the edit mode was successfull or not
		return true;
	}

}
