/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.layer.ForwardLayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.top.TopLayerDim;


@NonNullByDefault
public class NatTableLayerDim extends ForwardLayerDim<NatTable> implements TopLayerDim {
	
	
	public NatTableLayerDim(final NatTable layer, final LayerDim underlyingDim) {
		super(layer, underlyingDim);
	}
	
	
	@Override
	public final NatTable getLayer() {
		return this.layer;
	}
	
	@Override
	public void repaintPosition(final long position) {
		if (position == POSITION_NA) {
			return;
		}
		final long start= getPositionStart(position);
		this.layer.repaint(this.orientation, start, getPositionSize(position));
	}
	
	@Override
	public void repaintPositions(final LRange positions) {
		if (positions.size() == 0) {
			return;
		}
		final long start= getPositionStart(positions.start);
		final long end= ((positions.size() == 1) ? start : getPositionStart(positions.end - 1))
				+ getPositionSize(positions.end - 1);
		this.layer.repaint(this.orientation, start, end - start);
	}
	
}
