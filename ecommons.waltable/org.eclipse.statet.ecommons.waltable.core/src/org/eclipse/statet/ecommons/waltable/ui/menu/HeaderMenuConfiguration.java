/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.menu;

import org.eclipse.statet.ecommons.waltable.NatTable;


public class HeaderMenuConfiguration extends AbstractHeaderMenuConfiguration {

	public HeaderMenuConfiguration(final NatTable natTable) {
		super(natTable);
	}

	@Override
	protected PopupMenuBuilder createColumnHeaderMenu(final NatTable natTable) {
		return super.createColumnHeaderMenu(natTable)
//								.withHideColumnMenuItem()
//								.withShowAllColumnsMenuItem()
//								.withCreateColumnGroupsMenuItem()
//								.withUngroupColumnsMenuItem()
								.withAutoResizeSelectedColumnsMenuItem()
								.withColumnStyleEditor();
//								.withClearAllFilters();
	}

	@Override
	protected PopupMenuBuilder createRowHeaderMenu(final NatTable natTable) {
		return super.createRowHeaderMenu(natTable)
								.withAutoResizeSelectedRowsMenuItem();
	}

	@Override
	protected PopupMenuBuilder createCornerMenu(final NatTable natTable) {
		return super.createCornerMenu(natTable);
//								.withShowAllColumnsMenuItem();
	}
}
