/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.persistence;

import static org.eclipse.statet.ecommons.waltable.core.Persistable.DOT;
import static org.eclipse.statet.ecommons.waltable.core.style.CellStyling.BACKGROUND_COLOR;
import static org.eclipse.statet.ecommons.waltable.core.style.CellStyling.BORDER_STYLE;
import static org.eclipse.statet.ecommons.waltable.core.style.CellStyling.FONT;
import static org.eclipse.statet.ecommons.waltable.core.style.CellStyling.FOREGROUND_COLOR;
import static org.eclipse.statet.ecommons.waltable.core.style.CellStyling.HORIZONTAL_ALIGNMENT;
import static org.eclipse.statet.ecommons.waltable.core.style.CellStyling.VERTICAL_ALIGNMENT;

import java.util.Map;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;

import org.eclipse.statet.ecommons.waltable.core.style.BasicStyle;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.HorizontalAlignment;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.core.style.VerticalAlignment;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


/**
 * Saves and loads the following components of a style to a properties object.
 * 	- Foreground color
 * 	- Background color
 * 	- Horizontal alignment
 * 	- Vertical alignment
 * 	- Font
 * 	- Border style
 */
public class StylePersistor {

	// Style prefix constants
	public static final String STYLE_PERSISTENCE_PREFIX= "style"; //$NON-NLS-1$
	public static final String BLUE_COLOR_PREFIX= "blue"; //$NON-NLS-1$
	public static final String GREEN_COLOR_PREFIX= "green"; //$NON-NLS-1$
	public static final String RED_COLOR_PREFIX= "red"; //$NON-NLS-1$
	public static final String V_ALIGNMENT_PREFIX= "verticalAlignment"; //$NON-NLS-1$
	public static final String H_ALIGNMENT_PREFIX= "horizontalAlignment"; //$NON-NLS-1$
	public static final String BG_COLOR_PREFIX= "bg"; //$NON-NLS-1$
	public static final String FG_COLOR_PREFIX= "fg"; //$NON-NLS-1$
	public static final String FONT_PREFIX= "font"; //$NON-NLS-1$
	public static final String BORDER_PREFIX= "border"; //$NON-NLS-1$

	// Save

	public static void saveStyle(String prefix, final Map<String, String> properties, final Style style) {
		prefix= prefix + DOT + STYLE_PERSISTENCE_PREFIX;

		saveColor(prefix + DOT + BG_COLOR_PREFIX, properties, style.getAttributeValue(BACKGROUND_COLOR));
		saveColor(prefix + DOT + FG_COLOR_PREFIX, properties, style.getAttributeValue(FOREGROUND_COLOR));

		saveHAlign(prefix, properties, style.getAttributeValue(HORIZONTAL_ALIGNMENT));
		saveVAlign(prefix, properties, style.getAttributeValue(VERTICAL_ALIGNMENT));

		saveFont(prefix, properties, style.getAttributeValue(FONT));

		saveBorder(prefix, properties, style.getAttributeValue(BORDER_STYLE));
	}

	protected static void saveVAlign(final String prefix, final Map<String, String> properties, final VerticalAlignment vAlign) {
		if (vAlign == null) {
			return;
		}
		properties.put(prefix + DOT + V_ALIGNMENT_PREFIX, vAlign.name());
	}

	protected static void saveHAlign(final String prefix, final Map<String, String> properties, final HorizontalAlignment hAlign) {
		if (hAlign == null) {
			return;
		}
		properties.put(prefix + DOT + H_ALIGNMENT_PREFIX, hAlign.name());
	}

	protected static void saveBorder(final String prefix, final Map<String, String> properties, final BorderStyle borderStyle) {
		if (borderStyle == null) {
			return;
		}
		properties.put(prefix + DOT + BORDER_PREFIX, String.valueOf(borderStyle.toString()));
	}

	protected static void saveFont(final String prefix, final Map<String, String> properties, final Font font) {
		if (font == null) {
			return;
		}
		properties.put(prefix + DOT + FONT_PREFIX, String.valueOf(font.getFontData()[0].toString()));
	}

	protected static void saveColor(final String prefix, final Map<String, String> properties, final Color color) {
		if (color == null) {
			return;
		}
		ColorPersistor.saveColor(prefix, properties, color);
	}

	// Load

	public static BasicStyle loadStyle(String prefix, final Map<String, String> properties) {
		final var style= new BasicStyle();
		prefix= prefix + DOT + STYLE_PERSISTENCE_PREFIX;

		// BG Color
		final String bgColorPrefix= prefix + DOT + BG_COLOR_PREFIX;
		final Color bgColor= loadColor(bgColorPrefix, properties);
		if (bgColor != null) {
			style.setAttributeValue(CellStyling.BACKGROUND_COLOR, bgColor);
		}

		// FG Color
		final String fgColorPrefix= prefix + DOT + FG_COLOR_PREFIX;
		final Color fgColor= loadColor(fgColorPrefix, properties);
		if (fgColor != null) {
			style.setAttributeValue(CellStyling.FOREGROUND_COLOR, fgColor);
		}

		// Alignment
		final String hAlignPrefix= prefix + DOT + H_ALIGNMENT_PREFIX;
		final HorizontalAlignment hAlign= loadHAlignment(hAlignPrefix, properties);
		if (hAlign != null) {
			style.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, hAlign);
		}

		final String vAlignPrefix= prefix + DOT + V_ALIGNMENT_PREFIX;
		final VerticalAlignment vAlign= loadVAlignment(vAlignPrefix, properties);
		if (vAlign != null) {
			style.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, vAlign);
		}

		// Font
		final String fontPrefix= prefix + DOT + FONT_PREFIX;
		final Font font= loadFont(fontPrefix, properties);
		if (font != null) {
			style.setAttributeValue(CellStyling.FONT, font);
		}

		// Border Style
		final String borderPrefix= prefix + DOT + BORDER_PREFIX;
		final BorderStyle borderStyle= loadBorderStyle(borderPrefix, properties);
		if (borderStyle != null) {
			style.setAttributeValue(CellStyling.BORDER_STYLE, borderStyle);
		}

		return style;
	}

	private static BorderStyle loadBorderStyle(final String borderPrefix, final Map<String, String> properties) {
		final String borderStyle= properties.get(borderPrefix);
		if (borderStyle != null) {
			return new BorderStyle(borderStyle);
		}
		return null;
	}

	private static Font loadFont(final String fontPrefix, final Map<String, String> properties) {
		final String fontdata= properties.get(fontPrefix);
		if (fontdata != null) {
			return GUIHelper.getFont(new FontData(fontdata));
		}
		return null;
	}

	private static HorizontalAlignment loadHAlignment(final String hAlignPrefix, final Map<String, String> properties) {
		final String enumName= properties.get(hAlignPrefix);
		if (enumName != null) {
			return HorizontalAlignment.valueOf(enumName);
		}
		return null;
	}

	private static VerticalAlignment loadVAlignment(final String vAlignPrefix, final Map<String, String> properties) {
		final String enumName= properties.get(vAlignPrefix);
		if (enumName != null) {
			return VerticalAlignment.valueOf(enumName);
		}
		return null;
	}

	protected static Color loadColor(final String prefix, final Map<String, String> properties) {
		return ColorPersistor.loadColor(prefix, properties);
	}
}
