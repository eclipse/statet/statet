/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.convert;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;


/**
 * Converts between two different data representations.
 * 
 * The normal data representation is known as the <i>canonical representation</i>
 * The representation displayed on the UI is called the <i>display representation</i>.
 * 
 * For example, the canonical representation might be a Date object,
 * whereas the target representation could be a formatted String.
 */
public interface IDisplayConverter {
	
	
	/**
	 * Convert backing data value to value to be displayed.
	 * 
	 * Typically converted to a String for display.
	 * Use this method for contextual conversion.
	 */
	public Object canonicalToDisplayValue(LayerCell cell, ConfigRegistry configRegistry, Object canonicalValue);
	
	/**
	 * Convert from display value to value in the backing data structure.
	 * 
	 * NOTE: The type the display value is converted to <i>must</i> match the type
	 * in the setter of the backing bean/row object
	 * Use this method for contextual conversion.
	 */
	public Object displayToCanonicalValue(LayerCell cell, ConfigRegistry configRegistry, Object displayValue);
	
}
