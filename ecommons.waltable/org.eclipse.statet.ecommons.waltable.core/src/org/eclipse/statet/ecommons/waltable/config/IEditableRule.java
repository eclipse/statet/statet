/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.config;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;


public interface IEditableRule {
	
	
	IEditableRule ALWAYS_EDITABLE= new IEditableRule() {
		
		@Override
		public boolean isEditable(final LayerCell cell, final ConfigRegistry configRegistry) {
			return true;
		}
		
	};
	
	IEditableRule NEVER_EDITABLE= new IEditableRule() {
		
		@Override
		public boolean isEditable(final LayerCell cell, final ConfigRegistry configRegistry) {
			return false;
		}
		
	};
	
	
	boolean isEditable(LayerCell cell, ConfigRegistry configRegistry);
	
	
}
