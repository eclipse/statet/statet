/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.convert;

import org.eclipse.statet.ecommons.waltable.Messages;


/**
 * Converts the display value to a double and vice versa.
 */
public abstract class NumericDisplayConverter extends DisplayConverter {

	@Override
	public Object canonicalToDisplayValue(final Object canonicalValue) {
		try {
			if (canonicalValue != null) {
				return canonicalValue.toString();
			}
			return null;
		} catch (final Exception e) {
			return canonicalValue;
		}
	}

	@Override
	public Object displayToCanonicalValue(final Object displayValue) {
		try {
			String s;
			if (displayValue != null
					&& (s= displayValue.toString()) != null && !s.isEmpty() ) {
				return convertToNumericValue(s);
			}
			return null;
		} catch (final Exception e) {
			throw new ConversionFailedException(
					Messages.getString("NumericDisplayConverter.failure", //$NON-NLS-1$
							new Object[] {displayValue}), e);
		}
	}
	
	protected abstract Object convertToNumericValue(String value);
}
