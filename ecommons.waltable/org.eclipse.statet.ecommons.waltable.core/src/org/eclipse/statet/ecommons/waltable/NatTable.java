/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;
import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import java.util.ArrayList;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.waltable.command.DisposeResourcesCommand;
import org.eclipse.statet.ecommons.waltable.config.DefaultNatTableStyleConfiguration;
import org.eclipse.statet.ecommons.waltable.config.IConfiguration;
import org.eclipse.statet.ecommons.waltable.conflation.EventConflaterChain;
import org.eclipse.statet.ecommons.waltable.conflation.IEventConflater;
import org.eclipse.statet.ecommons.waltable.conflation.VisualChangeEventConflater;
import org.eclipse.statet.ecommons.waltable.core.Persistable;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PixelOutOfBoundsException;
import org.eclipse.statet.ecommons.waltable.core.layer.ClientAreaProvider;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerListener;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.core.layer.events.VisualChangeEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.top.OverlayPainter;
import org.eclipse.statet.ecommons.waltable.core.layer.top.TopLayer;
import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;
import org.eclipse.statet.ecommons.waltable.core.swt.painters.TableLayerPainter;
import org.eclipse.statet.ecommons.waltable.edit.ActiveCellEditorRegistry;
import org.eclipse.statet.ecommons.waltable.grid.core.ClientAreaResizeCommand;
import org.eclipse.statet.ecommons.waltable.refresh.core.StructuralRefreshCommand;
import org.eclipse.statet.ecommons.waltable.selection.core.CellSelectionEvent;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.mode.ConfigurableModeEventHandler;
import org.eclipse.statet.ecommons.waltable.ui.mode.Mode;
import org.eclipse.statet.ecommons.waltable.ui.mode.ModeSupport;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;
import org.eclipse.statet.ecommons.waltable.viewport.core.RecalculateScrollBarsCommand;
import org.eclipse.statet.internal.ecommons.waltable.WaLTablePlugin;


public class NatTable extends Canvas implements TopLayer, PaintListener, LayerListener, Persistable {
	
	public static final int DEFAULT_STYLE_OPTIONS= SWT.NO_BACKGROUND | SWT.NO_REDRAW_RESIZE | SWT.DOUBLE_BUFFERED  | SWT.V_SCROLL | SWT.H_SCROLL;
	
	
	private final NatTableLayerDim hDim;
	private final NatTableLayerDim vDim;
	
	private ClientAreaProvider clientAreaProvider= new ClientAreaProvider() {
		@Override
		public LRectangle getClientArea() {
			if (!isDisposed()) {
				return SwtUtils.toNatTable(NatTable.this.getClientArea());
			} else {
				return new LRectangle(0, 0, 0, 0);
			}
		}
		
	};
	
	
	private UiBindingRegistry uiBindingRegistry;
	
	private ModeSupport modeSupport;
	
	private final EventConflaterChain conflaterChain= new EventConflaterChain();
	
	private final CopyOnWriteIdentityListSet<OverlayPainter> overlayPainters= new CopyOnWriteIdentityListSet<>();
	
	private final ArrayList<Persistable> persistables= new ArrayList<>();
	
	private Layer underlyingLayer;
	
	private ConfigRegistry configRegistry;
	
	protected final ArrayList<IConfiguration> configurations= new ArrayList<>();
	
	protected String id= GUIHelper.getSequenceNumber();
	
	private LayerPainter layerPainter= new TableLayerPainter(this);
	
	private final boolean autoconfigure;
	
	
	public NatTable(final Composite parent, final Layer layer) {
		this(parent, DEFAULT_STYLE_OPTIONS, layer);
	}
	
	public NatTable(final Composite parent, final Layer layer, final boolean autoconfigure) {
		this(parent, DEFAULT_STYLE_OPTIONS, layer, autoconfigure);
	}
	
	public NatTable(final Composite parent, final int style, final Layer layer) {
		this(parent, style, layer, true);
	}
	
	public NatTable(final Composite parent, final int style, final Layer layer, final boolean autoconfigure) {
		super(parent, style);
		
		this.hDim= new NatTableLayerDim(this, layer.getDim(HORIZONTAL));
		this.vDim= new NatTableLayerDim(this, layer.getDim(VERTICAL));
		
		// Disable scroll bars by default; if a Viewport is available, it will enable the scroll bars
		disableScrollBar(getHorizontalBar());
		disableScrollBar(getVerticalBar());
		
		initInternalListener();
		
		internalSetLayer(layer);
		
		this.autoconfigure= autoconfigure;
		if (autoconfigure) {
			this.configurations.add(new DefaultNatTableStyleConfiguration());
			configure();
		}
		
		this.conflaterChain.add(getVisualChangeEventConflater());
		this.conflaterChain.start();
		
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				doCommand(new DisposeResourcesCommand());
				NatTable.this.conflaterChain.stop();
				ActiveCellEditorRegistry.unregisterActiveCellEditor();
				layer.dispose();
			}
			
		});
	}
	
	
	@Override
	public NatTableLayerDim getDim(final Orientation orientation) {
		if (orientation == null) {
			throw new NullPointerException("orientation"); //$NON-NLS-1$
		}
		
		return (orientation == HORIZONTAL) ? this.hDim : this.vDim;
	}
	
	
	protected IEventConflater getVisualChangeEventConflater() {
		return new VisualChangeEventConflater(this);
	}
	
	private void disableScrollBar(final ScrollBar scrollBar) {
		if (scrollBar != null) {
			scrollBar.setMinimum(0);
			scrollBar.setMaximum(1);
			scrollBar.setThumb(1);
			scrollBar.setEnabled(false);
		}
	}
	
	public Layer getUnderlyingLayer() {
		return this.underlyingLayer;
	}
	
	private void internalSetLayer(final Layer layer) {
		if (layer != null) {
			this.underlyingLayer= layer;
			this.underlyingLayer.setClientAreaProvider(getClientAreaProvider());
			this.underlyingLayer.addLayerListener(this);
		}
	}
	
	/**
	 * Adds a configuration to the table.
	 * <p>
	 * Configurations are processed when the {@link #configure()} method is invoked.
	 * Each configuration object then has a chance to configure the
	 * 	<ol>
	 * 		<li>ILayer</li>
	 * 		<li>ConfigRegistry</li>
	 * 		<li>UiBindingRegistry</li>
	 *  </ol>
	 */
	public void addConfiguration(final IConfiguration configuration) {
		if (this.autoconfigure) {
			throw new IllegalStateException("May only add configurations post construction if autoconfigure is turned off"); //$NON-NLS-1$
		}
		
		this.configurations.add(configuration);
	}
	
	/**
	 * @return {@link ConfigRegistry} used to hold the configuration bindings
	 * 	by Layer, DisplayMode and Config labels.
	 */
	public ConfigRegistry getConfigRegistry() {
		if (this.configRegistry == null) {
			this.configRegistry= new BasicConfigRegistry();
		}
		return this.configRegistry;
	}
	
	public void setConfigRegistry(final ConfigRegistry configRegistry) {
		if (this.autoconfigure) {
			throw new IllegalStateException("May only set config registry post construction if autoconfigure is turned off"); //$NON-NLS-1$
		}
		
		this.configRegistry= configRegistry;
	}
	
	/**
	 * @return Registry holding all the UIBindings contributed by the underlying layers
	 */
	public UiBindingRegistry getUiBindingRegistry() {
		if (this.uiBindingRegistry == null) {
			this.uiBindingRegistry= new UiBindingRegistry(this);
		}
		return this.uiBindingRegistry;
	}
	
	public void setUiBindingRegistry(final UiBindingRegistry uiBindingRegistry) {
		if (this.autoconfigure) {
			throw new IllegalStateException("May only set UI binding registry post construction if autoconfigure is turned off"); //$NON-NLS-1$
		}
		
		this.uiBindingRegistry= uiBindingRegistry;
	}
	
	public String getID() {
		return this.id;
	}
	
	@Override
	protected void checkSubclass() {
	}
	
	protected void initInternalListener() {
		this.modeSupport= new ModeSupport(this);
		this.modeSupport.registerModeEventHandler(Mode.NORMAL_MODE, new ConfigurableModeEventHandler(this.modeSupport, this));
		this.modeSupport.switchMode(Mode.NORMAL_MODE);
		
		addPaintListener(this);
		
		addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(final FocusEvent arg0) {
				redraw();
			}
			
			@Override
			public void focusGained(final FocusEvent arg0) {
				redraw();
			}
			
		});
		
		addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				doCommand(new ClientAreaResizeCommand(NatTable.this));
			}
		});
	}
	
	@Override
	public boolean forceFocus() {
		return super.forceFocus();
	}
	
	// Painting ///////////////////////////////////////////////////////////////
	
	public ImList<OverlayPainter> getOverlayPainters() {
		return this.overlayPainters.toList();
	}
	
	public void addOverlayPainter(final OverlayPainter overlayPainter) {
		this.overlayPainters.add(nonNullAssert(overlayPainter));
	}
	
	public void removeOverlayPainter(final OverlayPainter overlayPainter) {
		this.overlayPainters.remove(overlayPainter);
	}
	
	@Override
	public void paintControl(final PaintEvent event) {
		paintNatTable(event);
	}
	
	private void paintNatTable(final PaintEvent event) {
		final Rectangle eventRectangle= new Rectangle(event.x, event.y, event.width, event.height);
		
		if (!eventRectangle.isEmpty()) {
			getLayerPainter().paintLayer(this, event.gc, 0, 0, eventRectangle, getConfigRegistry());
		}
	}
	
	@Override
	public LayerPainter getLayerPainter() {
		return this.layerPainter;
	}
	
	public void setLayerPainter(final LayerPainter layerPainter) {
		this.layerPainter= layerPainter;
	}
	
	/**
	 * Repaint only a specific column in the grid. This method is optimized so that only the specific column is
	 * repainted and nothing else.
	 *
	 * @param columnPosition column of the grid to repaint
	 */
	public void repaintColumn(final long columnPosition) {
		this.hDim.repaintPosition(columnPosition);
	}
	
	/**
	 * Repaint only a specific row in the grid. This method is optimized so that only the specific row is repainted and
	 * nothing else.
	 *
	 * @param rowPosition row of the grid to repaint
	 */
	public void repaintRow(final long rowPosition) {
		this.vDim.repaintPosition(rowPosition);
	}
	
	protected void repaint(final Orientation orientation, final long start, final long size) {
		if (orientation == HORIZONTAL) {
			redraw(SwtUtils.toSWT(start), 0, SwtUtils.toSWT(size), safe(getHeight()), true);
		}
		else {
			redraw(0, SwtUtils.toSWT(start), safe(getWidth()), SwtUtils.toSWT(size), true);
		}
	}
	
	
	public void updateResize() {
		updateResize(true);
	}
	
	/**
	 * Update the table screen by re-calculating everything again. It should not
	 * be called too frequently.
	 *
	 * @param redraw
	 *            true to redraw the table
	 */
	private void updateResize(final boolean redraw) {
		if (isDisposed()) {
			return;
		}
		doCommand(new RecalculateScrollBarsCommand());
		if (redraw) {
			redraw();
		}
	}
	
	/**
	 * Refreshes the entire NatTable as every layer will be refreshed.
	 */
	public void refresh() {
		doCommand(new StructuralRefreshCommand());
	}
	
	@Override
	public void configure(final ConfigRegistry configRegistry, final UiBindingRegistry uiBindingRegistry) {
		throw new UnsupportedOperationException("Cannot use this method to configure NatTable. Use no-argument configure() instead."); //$NON-NLS-1$
	}
	
	/**
	 * Processes all the registered {@link IConfiguration} (s).
	 * All the underlying layers are walked and given a chance to configure.
	 * Note: all desired configuration tweaks must be done <i>before</i> this method is invoked.
	 */
	public void configure() {
		if (this.underlyingLayer == null) {
			throw new IllegalStateException("Layer must be set before configure is called"); //$NON-NLS-1$
		}
		
		if (this.underlyingLayer != null) {
			this.underlyingLayer.configure(getConfigRegistry(), getUiBindingRegistry());
		}
		
		for (final IConfiguration configuration : this.configurations) {
			configuration.configureLayer(this);
			configuration.configureRegistry(getConfigRegistry());
			configuration.configureUiBindings(getUiBindingRegistry());
		}
	}
	
	// Events /////////////////////////////////////////////////////////////////
	
	@Override
	public void handleLayerEvent(final LayerEvent event) {
		for (final LayerListener layerListener : this.listeners) {
			layerListener.handleLayerEvent(event);
		}
		
		if (event instanceof VisualChangeEvent) {
			this.conflaterChain.addEvent(event);
		}
		
		if (event instanceof CellSelectionEvent) {
			final Event e= new Event();
			e.widget= this;
			try {
				notifyListeners(SWT.Selection, e);
			} catch (final RuntimeException re) {
				WaLTablePlugin.log(new Status(IStatus.ERROR, WaLTablePlugin.BUNDLE_ID,
						"An error occurred when fireing SWT selection event.", re )); //$NON-NLS-1$
			}
		}
	}
	
	
	// ILayer /////////////////////////////////////////////////////////////////
	
	// Persistence
	
	/**
	 * Save the state of the table to the properties object.
	 * {@link Layer#saveState(String, Map)} is invoked on all the underlying layers.
	 * This properties object will be populated with the settings of all underlying layers
	 * and any {@link Persistable} registered with those layers.
	 */
	@Override
	public void saveState(final String prefix, final Map<String, String> properties) {
		BusyIndicator.showWhile(null, new Runnable() {
			
			@Override
			public void run() {
				NatTable.this.underlyingLayer.saveState(prefix, properties);
			}
		});
	}
	
	/**
	 * Restore the state of the underlying layers from the values in the properties object.
	 * @see #saveState(String, Map)
	 */
	@Override
	public void loadState(final String prefix, final Map<String, String> properties) {
		BusyIndicator.showWhile(null, new Runnable() {
			
			@Override
			public void run() {
				NatTable.this.underlyingLayer.loadState(prefix, properties);
			}
		});
	}
	
	/**
	 * @see Layer#registerPersistable(Persistable)
	 */
	@Override
	public void registerPersistable(final Persistable persistable) {
		this.persistables.add(persistable);
	}
	
	@Override
	public void unregisterPersistable(final Persistable persistable) {
		this.persistables.remove(persistable);
	}
	
	// Command
	
	@Override
	public boolean doCommand(final LayerCommand command) {
		return this.underlyingLayer.doCommand(command);
	}
	
	@Override
	public void registerCommandHandler(final LayerCommandHandler<?> commandHandler) {
		this.underlyingLayer.registerCommandHandler(commandHandler);
	}
	
	@Override
	public void unregisterCommandHandler(final Class<? extends LayerCommand> commandClass) {
		this.underlyingLayer.unregisterCommandHandler(commandClass);
	}
	
	// Events
	
	private final CopyOnWriteIdentityListSet<LayerListener> listeners= new CopyOnWriteIdentityListSet<>();
	
	@Override
	public void fireLayerEvent(final LayerEvent event) {
		this.underlyingLayer.fireLayerEvent(event);
	}
	
	@Override
	public void addLayerListener(final LayerListener listener) {
		this.listeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeLayerListener(final LayerListener listener) {
		this.listeners.remove(listener);
	}
	
	// Columns/Horizontal
	
	@Override
	public long getColumnCount() {
		return this.hDim.getPositionCount();
	}
	
	@Override
	public long getWidth() {
		return this.hDim.getSize();
	}
	
	@Override
	public long getColumnPositionByX(final long x) {
		try {
			return this.hDim.getPositionByPixel(x);
		}
		catch (final PixelOutOfBoundsException e) {
			return Long.MIN_VALUE;
		}
	}
	
	
	// Rows/Vertical
	
	@Override
	public long getRowCount() {
		return this.vDim.getPositionCount();
	}
	
	@Override
	public long getHeight() {
		return this.vDim.getSize();
	}
	
	@Override
	public long getRowPositionByY(final long y) {
		try {
			return this.vDim.getPositionByPixel(y);
		}
		catch (final PixelOutOfBoundsException e) {
			return Long.MIN_VALUE;
		}
	}
	
	// Y
	
	// Cell features
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		return this.underlyingLayer.getCellByPosition(columnPosition, rowPosition);
	}
	
	// IRegionResolver
	
	@Override
	public LabelStack getRegionLabelsByXY(final long x, final long y) {
		return this.underlyingLayer.getRegionLabelsByXY(x, y);
	}
	
	@Override
	public Layer getUnderlyingLayerByPosition(final long columnPosition, final long rowPosition) {
		return this.underlyingLayer;
	}
	
	@Override
	public ClientAreaProvider getClientAreaProvider() {
		return this.clientAreaProvider;
	}
	
	@Override
	public void setClientAreaProvider(final ClientAreaProvider clientAreaProvider) {
		this.clientAreaProvider= clientAreaProvider;
		this.underlyingLayer.setClientAreaProvider(clientAreaProvider);
	}
	
	
	// DND /////////////////////////////////////////////////////////////////
	
	/**
	 * Adds support for dragging items out of this control via a user
	 * drag-and-drop operation.
	 *
	 * @param operations
	 *            a bitwise OR of the supported drag and drop operation types (
	 *            <code>DROP_COPY</code>,<code>DROP_LINK</code>, and
	 *            <code>DROP_MOVE</code>)
	 * @param transferTypes
	 *            the transfer types that are supported by the drag operation
	 * @param listener
	 *            the callback that will be invoked to set the drag data and to
	 *            cleanup after the drag and drop operation finishes
	 * @see org.eclipse.swt.dnd.DND
	 */
	public void addDragSupport(final int operations, final Transfer[] transferTypes, final DragSourceListener listener) {
		final DragSource dragSource= new DragSource(this, operations);
		dragSource.setTransfer(transferTypes);
		dragSource.addDragListener(listener);
	}
	
	/**
	 * Adds support for dropping items into this control via a user drag-and-drop
	 * operation.
	 *
	 * @param operations
	 *            a bitwise OR of the supported drag and drop operation types (
	 *            <code>DROP_COPY</code>,<code>DROP_LINK</code>, and
	 *            <code>DROP_MOVE</code>)
	 * @param transferTypes
	 *            the transfer types that are supported by the drop operation
	 * @param listener
	 *            the callback that will be invoked after the drag and drop
	 *            operation finishes
	 * @see org.eclipse.swt.dnd.DND
	 */
	public void addDropSupport(final int operations, final Transfer[] transferTypes, final DropTargetListener listener) {
		final DropTarget dropTarget= new DropTarget(this, operations);
		dropTarget.setTransfer(transferTypes);
		dropTarget.addDropListener(listener);
	}
	
}
