/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.validate;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;


public abstract class ContextualDataValidator implements IDataValidator {
	
	
	@Override
	public boolean validate(final long columnIndex, final long rowIndex, final Object newValue) {
		throw new UnsupportedOperationException(this.getClass().getName() 
				+ " is a ContextualDataValidator and has therefore to be called with context informations."); //$NON-NLS-1$
	}
	
	@Override
	public abstract boolean validate(LayerCell cell, ConfigRegistry configRegistry, Object newValue);
	
}
