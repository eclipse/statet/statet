/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.matcher;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;


public interface IMouseEventMatcher {
	
	
	int NO_BUTTON= 0;
	int LEFT_BUTTON= 1;
	int RIGHT_BUTTON= 3;
	
	
	/**
	 * Figures out if the mouse event occurred in the supplied region.
	 * 
	 * @param event SWT mouse event
	 */
	public boolean matches(NatTable natTable, MouseEvent event, LabelStack regionLabels);
	
}
