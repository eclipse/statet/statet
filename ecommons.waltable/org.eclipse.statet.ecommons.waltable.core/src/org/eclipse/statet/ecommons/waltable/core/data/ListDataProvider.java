/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.data;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Enables the use of a {@link List} containing POJO(s) as a backing data
 * source.
 * 
 * <p>By default a bean at position {@code x} in the list is displayed in row
 * {@code x} in the table. A {@link ColumnAccessor} is used to retrieve column
 * data from the row objects.</p>
 * 
 * @param <T> type of the row objects in the backing list.
 * 
 * @see org.eclipse.statet.ecommons.waltable.util.data.PropertyColumnResolver
 */
@NonNullByDefault
public class ListDataProvider<T> implements DataProvider {
	
	
	protected final List<T> list;
	protected final ColumnAccessor<T> columnAccessor;
	
	
	public ListDataProvider(final List<T> list, final ColumnAccessor<T> columnAccessor) {
		this.list= list;
		this.columnAccessor= columnAccessor;
	}
	
	
	public List<T> getList() {
		return this.list;
	}
	
	
	@Override
	public long getColumnCount() {
		return this.columnAccessor.getColumnCount();
	}
	
	@Override
	public long getRowCount() {
		return this.list.size();
	}
	
	public T getRowObject(final long rowIndex) {
		if (rowIndex >= Integer.MAX_VALUE) {
			throw new IndexOutOfBoundsException();
		}
		return this.list.get((int) rowIndex);
	}
	
	public long indexOfRowObject(final T rowObject) {
		return this.list.indexOf(rowObject);
	}
	
	@Override
	public @Nullable Object getDataValue(final long columnIndex, final long rowIndex,
			final int flags, final @Nullable IProgressMonitor monitor) {
		final var rowObject= getRowObject(rowIndex);
		return this.columnAccessor.getDataValue(rowObject, columnIndex);
	}
	
	@Override
	public void setDataValue(final long columnIndex, final long rowIndex,
			final @Nullable Object newValue) {
		final var rowObject= getRowObject(rowIndex);
		this.columnAccessor.setDataValue(rowObject, columnIndex, newValue);
	}
	
}
