/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.coordinate;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class PixelOutOfBoundsException extends IllegalArgumentException {
	
	
	private static final long serialVersionUID= 1L;
	
	
	public PixelOutOfBoundsException(final long pixel, final Orientation orientation) {
		super("pixel (" + orientation + ")= " + pixel); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
