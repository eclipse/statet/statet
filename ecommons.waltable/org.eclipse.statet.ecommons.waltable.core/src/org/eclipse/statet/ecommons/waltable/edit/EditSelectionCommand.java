/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;


/**
 * Command that will trigger activating the edit mode for the current selection.
 * The corresponding command handler is responsible for determining if activation
 * should proceed because of race conditions like e.g. the cell is configured to 
 * be editable or another editor is active containing an invalid data.
 */
public class EditSelectionCommand extends AbstractContextFreeCommand {
	
	/**
	 * The {@link ConfigRegistry} containing the configuration of the current NatTable 
	 * instance the command should be executed for.
	 * <p>
	 * This is necessary because the edit controllers in the current architecture
	 * are not aware of the instance they are running in.
	 */
	private final ConfigRegistry configRegistry;
	/**
	 * The Character represented by the key that was typed in case this command
	 * should be executed because of typing a letter or digit key. Can be <code>null</code> 
	 * if this command should be executed because of a pressed control key (like F2) or a 
	 * programmatical execution.
	 */
	private final Character character;
	/**
	 * The parent Composite, needed for the creation of the editor control.
	 */
	private final Composite parent;

	/**
	 * @param parent The parent Composite, needed for the creation of the editor control.
	 * @param configRegistry The {@link ConfigRegistry} containing the configuration of the
	 * 			current NatTable instance the command should be executed for.
	 * 			This is necessary because the edit controllers in the current architecture
	 * 			are not aware of the instance they are running in.
	 */
	public EditSelectionCommand(final Composite parent, final ConfigRegistry configRegistry) {
		this(parent, configRegistry, null);
	}

	/**
	 * @param parent The parent Composite, needed for the creation of the editor control.
	 * @param configRegistry The {@link ConfigRegistry} containing the configuration of the
	 * 			current NatTable instance the command should be executed for.
	 * 			This is necessary because the edit controllers in the current architecture
	 * 			are not aware of the instance they are running in.
	 * @param character The Character represented by the key that was typed in case this command
	 * 			should be executed because of typing a letter or digit key. Can be <code>null</code> 
	 * 			if this command should be executed because of a pressed control key (like F2) or a 
	 * 			programmatical execution.
	 */
	public EditSelectionCommand(final Composite parent, final ConfigRegistry configRegistry, final Character character) {
		this.parent= parent;
		this.configRegistry= configRegistry;
		this.character= character;
	}

	/**
	 * @return The {@link ConfigRegistry} containing the configuration of the current NatTable 
	 * 			instance the command should be executed for.
	 */
	public ConfigRegistry getConfigRegistry() {
		return this.configRegistry;
	}
	
	/**
	 * @return The Character represented by the key that was typed in case this command
	 * 			should be executed because of typing a letter or digit key. Can be <code>null</code> 
	 * 			if this command should be executed because of a pressed control key (like F2) or a 
	 * 			programmatical execution.
	 */
	public Character getCharacter() {
		return this.character;
	}
	
	/**
	 * @return The parent Composite, needed for the creation of the editor control.
	 */
	public Composite getParent() {
		return this.parent;
	}
	
}
