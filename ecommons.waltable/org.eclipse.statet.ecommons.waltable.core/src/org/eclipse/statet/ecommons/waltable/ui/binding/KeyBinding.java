/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.binding;

import org.eclipse.statet.ecommons.waltable.ui.action.IKeyAction;
import org.eclipse.statet.ecommons.waltable.ui.matcher.IKeyEventMatcher;


public class KeyBinding {

	private final IKeyEventMatcher keyEventMatcher;
	
	private final IKeyAction action;
	
	public KeyBinding(final IKeyEventMatcher keyEventMatcher, final IKeyAction action) {
		this.keyEventMatcher= keyEventMatcher;
		this.action= action;
	}
	
	public IKeyEventMatcher getKeyEventMatcher() {
		return this.keyEventMatcher;
	}
	
	public IKeyAction getAction() {
		return this.action;
	}
	
}
