/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style;

import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;


public interface SelectionStyleLabels {
    
    public static final String SELECTION_ANCHOR_STYLE= "SELECTION_ANCHOR"; //$NON-NLS-1$
    
    public static final String SELECTION_ANCHOR_GRID_LINE_STYLE= "selectionAnchorGridLine"; //$NON-NLS-1$

    public static final String COLUMN_FULLY_SELECTED_STYLE= GridLabels.COLUMN_HEADER + "_FULL"; //$NON-NLS-1$
    
    public static final String ROW_FULLY_SELECTED_STYLE= GridLabels.ROW_HEADER + "_FULL"; //$NON-NLS-1$

}
