/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.waltable.Messages;
import org.eclipse.statet.ecommons.waltable.core.style.BasicStyle;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


public class ColumnStyleEditorDialog extends AbstractStyleEditorDialog {

	// Tabs in the dialog
	private CellStyleEditorPanel cellStyleEditorPanel;
	private BorderStyleEditorPanel borderStyleEditorPanel;

	// These are populated on OK button press
	protected BasicStyle newColumnCellStyle;
	protected BorderStyle newBorderStyle;

	private final BasicStyle columnStyle;

	public ColumnStyleEditorDialog(final Shell parent, final BasicStyle columnCellStyle) {
		super(parent);
		this.columnStyle= columnCellStyle;

		this.newColumnCellStyle= columnCellStyle;
		if (columnCellStyle != null) {
			this.newBorderStyle= this.columnStyle.getAttributeValue(CellStyling.BORDER_STYLE);
		}
	}

	@Override
	protected void initComponents(final Shell shell) {
		shell.setLayout(new GridLayout());
		shell.setText(Messages.getString("ColumnStyleEditorDialog.shellTitle")); //$NON-NLS-1$

		// Closing the window is the same as canceling the form
		shell.addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(final ShellEvent e) {
				doFormCancel(shell);
			}

		});

		// Tabs panel
		final Composite tabPanel= new Composite(shell, SWT.NONE);
		tabPanel.setLayout(new GridLayout());

		final GridData fillGridData= new GridData();
		fillGridData.grabExcessHorizontalSpace= true;
		fillGridData.horizontalAlignment= GridData.FILL;
		tabPanel.setLayoutData(fillGridData);

		final CTabFolder tabFolder= new CTabFolder(tabPanel, SWT.BORDER);
		tabFolder.setLayout(new GridLayout());
		tabFolder.setLayoutData(fillGridData);

		final CTabItem columnTab= new CTabItem(tabFolder, SWT.NONE);
		columnTab.setText(Messages.getString("ColumnStyleEditorDialog.column")); //$NON-NLS-1$
		columnTab.setImage(GUIHelper.getImage("column")); //$NON-NLS-1$
		columnTab.setControl(createColumnPanel(tabFolder));

		try {
			this.cellStyleEditorPanel.edit(this.columnStyle);
			this.borderStyleEditorPanel.edit(this.columnStyle.getAttributeValue(CellStyling.BORDER_STYLE));
		} catch (final Exception e) {
			e.printStackTrace(System.err);
		}
	}

/*	Grid level styling
 * private Composite createBlotterPanel(Composite parent) {
		Composite blotterPanel= new Composite(parent, SWT.NONE);
		GridLayout panelLayout= new GridLayout();
		blotterPanel.setLayout(panelLayout);

		GridData panelLayoutData= new GridData();
		panelLayoutData.grabExcessHorizontalSpace= true;
		panelLayoutData.grabExcessVerticalSpace= true;
		panelLayoutData.verticalAlignment= GridData.VERTICAL_ALIGN_BEGINNING;
		panelLayoutData.horizontalAlignment= GridData.HORIZONTAL_ALIGN_BEGINNING;
		panelLayoutData.horizontalIndent= 20;
		blotterPanel.setLayoutData(panelLayoutData);

		new SeparatorPanel(blotterPanel, "Styling");
		gridColorsEditorPanel= new GridColorsEditorPanel(blotterPanel, gridStyle);

		return blotterPanel;
	}
*/
	private Composite createColumnPanel(final Composite parent) {
		final Composite columnPanel= new Composite(parent, SWT.NONE);
		columnPanel.setLayout(new GridLayout());

		new SeparatorPanel(columnPanel, Messages.getString("ColumnStyleEditorDialog.styling")); //$NON-NLS-1$
		this.cellStyleEditorPanel= new CellStyleEditorPanel(columnPanel, SWT.NONE);

		new SeparatorPanel(columnPanel, Messages.getString("ColumnStyleEditorDialog.border")); //$NON-NLS-1$
		this.borderStyleEditorPanel= new BorderStyleEditorPanel(columnPanel, SWT.NONE);
		return columnPanel;
	}

	@Override
	protected void doFormOK(final Shell shell) {
		this.newColumnCellStyle= this.cellStyleEditorPanel.getNewValue();
		this.newBorderStyle= this.borderStyleEditorPanel.getNewValue();
		shell.dispose();
	}

	@Override
	protected void doFormClear(final Shell shell) {
		this.newColumnCellStyle= null;
		shell.dispose();
	}

	// Getters for the modified style

	public BasicStyle getNewColumnCellStyle() {
		return this.newColumnCellStyle;
	}

	public BorderStyle getNewColumnBorderStyle() {
		return this.newBorderStyle;
	}
}
