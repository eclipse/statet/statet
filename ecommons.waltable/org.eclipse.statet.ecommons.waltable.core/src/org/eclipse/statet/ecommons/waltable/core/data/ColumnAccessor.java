/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.data;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Maps the properties from the row object to the corresponding columns.
 *  
 * @param <T> type of the bean used as a row object
 */
@NonNullByDefault
public interface ColumnAccessor<T> {
	
	
	long getColumnCount();
	
	
	@Nullable Object getDataValue(@NonNull T rowObject, long columnIndex);
	
	void setDataValue(@NonNull T rowObject, long columnIndex, @Nullable Object newValue);
	
}
