/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class LabelStack {
	
	
	private final List<String> labels= new ArrayList<>(8);
	
	
	public LabelStack() {
	}
	
	public LabelStack(final String label) {
		this.labels.add(label);
	}
	
	
	/**
	 * Adds a label to the bottom of the label stack.
	 * @param label
	 */
	public void addLabel(final String label) {
		if (!this.labels.contains(label)){
			this.labels.add(label);
		}
	}
	
	/**
	 * Adds a label to the top of the label stack.
	 * @param label
	 */
	public void addLabelOnTop(final String label) {
		final int idx= this.labels.indexOf(label);
		if (idx == 0) {
			return;
		}
		else if (idx > 0) {
			this.labels.remove(idx);
		}
		this.labels.add(0, label);
	}
	
	public void removeLabel(final String label) {
		this.labels.remove(label);
	}
	
	
	public List<String> getLabels() {
		return this.labels;
	}
	
	public boolean hasLabel(final String label) {
		return this.labels.contains(label);
	}
	
	
	@Override
	public int hashCode() {
		return this.labels.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final LabelStack other
						&& this.labels.equals(other.labels) ));
	}
	
	
	@Override
	public String toString() {
		return this.labels.toString();
	}
	
}
