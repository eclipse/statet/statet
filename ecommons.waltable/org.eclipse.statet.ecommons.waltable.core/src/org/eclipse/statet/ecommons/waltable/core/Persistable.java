/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core;

import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Instances implementing this interface can save and load their state from
 * a properties map. The state is therefore a collection of key value pairs.
 */
@NonNullByDefault
public interface Persistable {
	
	/**
	 * Separator used for properties. Example: .BODY.columnWidth.resizableByDefault
	 */
	static final String DOT= "."; //$NON-NLS-1$
	
	/**
	 * Separator used for values. Example: 0,1,2,3,4
	 */
	static final String VALUE_SEPARATOR= ","; //$NON-NLS-1$
	
	
	/**
	 * Saves the state to the given Properties using the specified prefix.
	 * 
	 * Note: The prefix must be prepended to the property key to support
	 *     multiple states within one properties store.
	 * 
	 * @param prefix the prefix to use for the state keys. It is also used as
	 *     the state configuration name.
	 * @param properties the properties to save the state to.
	 */
	void saveState(String prefix, Map<String, String> properties);
	
	/**
	 * Restore the state out of the given Properties identified by the
	 * specified prefix.
	 * 
	 * Note: The prefix must be prepended to the property key to support multiple states
	 * within one properties store.
	 * 
	 * @param prefix the prefix to use for the state keys. It is also used as
	 *     the state configuration name.
	 * @param properties the properties to load the state from.
	 */
	void loadState(String prefix, Map<String, String> properties);
	
}
