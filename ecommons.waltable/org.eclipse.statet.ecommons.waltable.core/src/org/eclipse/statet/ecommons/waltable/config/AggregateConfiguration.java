/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.config;

import java.util.ArrayList;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;


/**
 * Aggregates {@link IConfiguration} objects and invokes configure methods on all its members.
 */
@NonNullByDefault
public class AggregateConfiguration implements IConfiguration {
	
	
	private final ArrayList<IConfiguration> configurations= new ArrayList<>();
	
	
	public AggregateConfiguration() {
	}
	
	
	public void addConfiguration(final IConfiguration configuration) {
		this.configurations.add(configuration);
	}
	
	@Override
	public void configureLayer(final Layer layer) {
		for (final IConfiguration configuration : this.configurations) {
			configuration.configureLayer(layer);
		}
	}
	
	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		for (final IConfiguration configuration : this.configurations) {
			configuration.configureRegistry(configRegistry);
		}
	}
	
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		for (final IConfiguration configuration : this.configurations) {
			configuration.configureUiBindings(uiBindingRegistry);
		}
	}
	
}
