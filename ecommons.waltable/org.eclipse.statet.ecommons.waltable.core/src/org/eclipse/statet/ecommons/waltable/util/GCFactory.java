/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.util;

import org.eclipse.swt.graphics.Drawable;
import org.eclipse.swt.graphics.GC;


public class GCFactory {

	private final Drawable drawable;

	public GCFactory(final Drawable drawable) {
		this.drawable= drawable;
	}
	
	public GC createGC() {
		return new GC(this.drawable);
	}
	
}
