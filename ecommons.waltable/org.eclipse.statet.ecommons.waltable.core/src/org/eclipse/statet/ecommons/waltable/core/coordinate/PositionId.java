/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.coordinate;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class PositionId {
	
	
	private static final int CAT_SHIFT= 52;
	
	
	public static final long CAT_MASK= 0x0_7FFL << CAT_SHIFT;
	public static final long NUM_MASK= ~(0x0_FFFL << CAT_SHIFT);
	
	public static final long BODY_CAT= 0x001L << CAT_SHIFT;
	public static final long HEADER_CAT= 0x002L << CAT_SHIFT;
	
	public static final long PLACEHOLDER_CAT= 0x010L << CAT_SHIFT;
	
	
	public static String toString(final long id) {
		return String.format("<%03X:%d>", (id & CAT_MASK) >>> CAT_SHIFT, id & NUM_MASK); //$NON-NLS-1$
	}
	
}
