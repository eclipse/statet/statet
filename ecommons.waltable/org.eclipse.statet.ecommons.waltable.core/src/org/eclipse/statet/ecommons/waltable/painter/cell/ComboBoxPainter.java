/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.ecommons.waltable.painter.cell.decorator.CellPainterDecorator;
import org.eclipse.statet.ecommons.waltable.ui.util.CellEdgeEnum;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


public class ComboBoxPainter extends CellPainterWrapper {

	/**
	 * Create a new {@link ComboBoxPainter} with the default image.
	 */
	public ComboBoxPainter() {
		this(GUIHelper.getImage("down_2")); //$NON-NLS-1$
	}
	
	/**
	 * Create a new {@link ComboBoxPainter} with the given {@link Image} as the image 
	 * marking the cell as a combo control.
	 * @param comboImage The image marking the cell as a combo control
	 */
	public ComboBoxPainter(final Image comboImage) {
		setWrappedPainter(
				new CellPainterDecorator(
						new TextPainter(), CellEdgeEnum.RIGHT, 
						new ImagePainter(comboImage)));
	}
}
