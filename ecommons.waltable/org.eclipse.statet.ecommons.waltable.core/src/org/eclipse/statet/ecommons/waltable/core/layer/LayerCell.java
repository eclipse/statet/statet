/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;


@NonNullByDefault
public interface LayerCell {
	
	
	Layer getLayer();
	
	LayerCellDim getDim(Orientation orientation);
	
	
	long getOriginColumnPosition();
	
	long getOriginRowPosition();
	
	long getColumnPosition();
	
	long getRowPosition();
	
	long getColumnSpan();
	
	long getRowSpan();
	
	boolean isSpannedCell();
	
	
	DisplayMode getDisplayMode();
	
	LRectangle getBounds();
	
	LabelStack getLabels();
	
	
	@Nullable Object getDataValue(int flags, @Nullable IProgressMonitor monitor);
	
}
