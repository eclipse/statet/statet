/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style.editor;

import static org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes.CELL_STYLE;
import static org.eclipse.statet.ecommons.waltable.core.config.DisplayMode.NORMAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.ecommons.waltable.core.Persistable;
import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.style.BasicStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.layer.cell.ColumnOverrideLabelAccumulator;
import org.eclipse.statet.ecommons.waltable.persistence.StylePersistor;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;


/**
 * 
 * 1. Captures a new style using the <code>StyleEditorDialog</code> 
 * 2. Registers style from step 1 in the <code>ConfigRegistry</code> with a new label 
 * 3. Applies the label from step 2 to all cells in the selected column
 * 
 */
public class DisplayColumnStyleEditorCommandHandler extends AbstractLayerCommandHandler<DisplayColumnStyleEditorCommand> implements Persistable {
	
	protected static final String PERSISTENCE_PREFIX= "userDefinedColumnStyle"; //$NON-NLS-1$
	protected static final String USER_EDITED_STYLE_LABEL= "USER_EDITED_STYLE_FOR_INDEX_"; //$NON-NLS-1$

	protected final SelectionLayer selectionLayer;
	protected ColumnOverrideLabelAccumulator columnLabelAccumulator;
	private final ConfigRegistry configRegistry;
	protected ColumnStyleEditorDialog dialog;
	protected final Map<String, BasicStyle> stylesToPersist= new HashMap<>();

	public DisplayColumnStyleEditorCommandHandler(final SelectionLayer selectionLayer, final ColumnOverrideLabelAccumulator labelAccumulator, final ConfigRegistry configRegistry) {
		this.selectionLayer= selectionLayer;
		this.columnLabelAccumulator= labelAccumulator;
		this.configRegistry= configRegistry;
	}

	@Override
	public boolean doCommand(final DisplayColumnStyleEditorCommand command) {
		final long columnIndexOfClick= command.getNattableLayer().getDim(HORIZONTAL)
				.getPositionId(command.columnPosition, command.columnPosition);
		
		final LabelStack configLabels= new LabelStack();
		this.columnLabelAccumulator.addLabels(configLabels, columnIndexOfClick, 0);
		configLabels.addLabel(getConfigLabel(columnIndexOfClick));
		
		// Column style
		final BasicStyle clickedCellStyle= (BasicStyle) this.configRegistry.getAttribute(CELL_STYLE, NORMAL, configLabels.getLabels());
		
		this.dialog= new ColumnStyleEditorDialog(Display.getCurrent().getActiveShell(), clickedCellStyle);
		this.dialog.open();

		if(this.dialog.isCancelPressed()) {
			return true;
		}
		
		applySelectedStyleToColumns(command, this.selectionLayer.getSelectedColumnPositions());
		return true;
	}

	@Override
	public Class<DisplayColumnStyleEditorCommand> getCommandClass() {
		return DisplayColumnStyleEditorCommand.class;
	}

	protected void applySelectedStyleToColumns(final DisplayColumnStyleEditorCommand command,
			final LRangeList columnPositions) {
		for (final var columnIter= columnPositions.values().iterator(); columnIter.hasNext(); ) {
			final long position= columnIter.nextLong();
			final long columnIndex= this.selectionLayer.getDim(HORIZONTAL).getPositionId(position, position);
			// Read the edited styles
			final BasicStyle newColumnCellStyle= this.dialog.getNewColumnCellStyle(); 
			
			final String configLabel= getConfigLabel(columnIndex);
			if (newColumnCellStyle == null) {
				this.stylesToPersist.remove(configLabel);
			} else {
				newColumnCellStyle.setAttributeValue(CellStyling.BORDER_STYLE, this.dialog.getNewColumnBorderStyle());
				this.stylesToPersist.put(configLabel, newColumnCellStyle);
			}
			this.configRegistry.registerAttribute(CELL_STYLE, newColumnCellStyle, NORMAL, configLabel);
			this.columnLabelAccumulator.registerColumnOverridesOnTop(columnIndex, configLabel);
		}
	}

	protected String getConfigLabel(final long columnIndex) {
		return USER_EDITED_STYLE_LABEL + columnIndex;
	}

	@Override
	public void loadState(String prefix, final Map<String, String> properties) {
		prefix= prefix + DOT + PERSISTENCE_PREFIX;
		for (final var entry : properties.entrySet()) {
			final String key= entry.getKey();
			if (key.contains(PERSISTENCE_PREFIX)) {
				final long colIndex= parseColumnIndexFromKey(key);
				// Has the config label been processed
				if (!this.stylesToPersist.keySet().contains(getConfigLabel(colIndex))) {
					final BasicStyle savedStyle= StylePersistor.loadStyle(prefix + DOT + getConfigLabel(colIndex), properties);
					
					this.configRegistry.registerAttribute(CELL_STYLE, savedStyle, NORMAL, getConfigLabel(colIndex));
					this.stylesToPersist.put(getConfigLabel(colIndex), savedStyle);
					this.columnLabelAccumulator.registerColumnOverrides(colIndex, getConfigLabel(colIndex));
				}
			}
		}
	}

	protected long parseColumnIndexFromKey(final String keyString) {
		final int colLabelStartIndex= keyString.indexOf(USER_EDITED_STYLE_LABEL);
		final String columnConfigLabel= keyString.substring(colLabelStartIndex, keyString.indexOf('.', colLabelStartIndex));
		final int lastUnderscoreInLabel= columnConfigLabel.lastIndexOf('_', colLabelStartIndex);

		return Long.parseLong(columnConfigLabel.substring(lastUnderscoreInLabel + 1));
	}

	@Override
	public void saveState(String prefix, final Map<String, String> properties) {
		prefix= prefix + DOT + PERSISTENCE_PREFIX;

		for (final Map.Entry<String, BasicStyle> labelToStyle : this.stylesToPersist.entrySet()) {
			final BasicStyle style= labelToStyle.getValue();
			final String label= labelToStyle.getKey();

			StylePersistor.saveStyle(prefix + DOT + label, properties, style);
		}
	}
}
