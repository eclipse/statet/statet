/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.top;

import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


/**
 * An overlay painter is given a chance to paint the canvas once the layers have finished rendering.
 * 
 * @see NatTable#addOverlayPainter(SwtOverlayPainter)
 */
@NonNullByDefault
public interface OverlayPainter {
	
	
	void paintOverlay(GC gc, Layer layer);
	
}
