/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerEvent;


/**
 * This event is used to activate an editor inline.
 * It is used internally if a single cell should be edited by selection,
 * e.g. on pressing the F2 key on the current selected cell.
 * <p>
 * This event is needed to do the translation of the coordinates and 
 * bounds needed for inline editing. On activating an editor via selection
 * these informations are not known and need to be evaluated through the
 * layer event handlers.
 */
public class InlineCellEditEvent implements LayerEvent {

	/**
	 * The layer the cellCoordinates rely on. The layer will change on
	 * event processing to always match the translated coordinates.
	 */
	private Layer layer;
	/**
	 * The coordinates of the cell to edit for the set layer.
	 */
	private final PositionCoordinate cellCoordinate;
	/**
	 * The parent Composite, needed for the creation of the editor control.
	 */
	private final Composite parent;
	/**
	 * The {@link ConfigRegistry} containing the configuration of the
	 * current NatTable instance the command should be executed for.
	 * This is necessary because the edit controllers in the current architecture
	 * are not aware of the instance they are running in.
	 */
	private final ConfigRegistry configRegistry;
	/**
	 * The value that should be put to the activated editor control.
	 */
	private final Object initialValue;

	/**
	 * 
	 * @param layer The layer the cellCoordinates rely on.
	 * @param cellCoordinate The coordinates of the cell to edit for the set layer.
	 * @param parent The parent Composite, needed for the creation of the editor control.
	 * @param configRegistry The {@link ConfigRegistry} containing the configuration of the
	 * 			current NatTable instance the command should be executed for.
	 * 			This is necessary because the edit controllers in the current architecture
	 * 			are not aware of the instance they are running in.
	 * @param initialValue The value that should be put to the activated editor control. 
	 */
	public InlineCellEditEvent(final Layer layer, final PositionCoordinate cellCoordinate, final Composite parent, 
			final ConfigRegistry configRegistry, final Object initialValue) {
		
		this.layer= layer;
		this.cellCoordinate= cellCoordinate;
		this.parent= parent;
		this.configRegistry= configRegistry;
		this.initialValue= initialValue;
	}
	
	@Override
	public @Nullable InlineCellEditEvent toLayer(final Layer targetLayer) {
		final InlineCellEditEvent event= new InlineCellEditEvent(this.layer,
				new PositionCoordinate(this.cellCoordinate), this.parent, this.configRegistry, this.initialValue);
		return (event.convertToLocal(targetLayer)) ? event : null;
	}
	
	private boolean convertToLocal(final Layer localLayer) {
		this.cellCoordinate.columnPosition= localLayer.getDim(HORIZONTAL).underlyingToLocalPosition(
				this.layer.getDim(HORIZONTAL), this.cellCoordinate.columnPosition );
		if (this.cellCoordinate.columnPosition < 0 || this.cellCoordinate.columnPosition >= localLayer.getColumnCount()) {
			return false;
		}
		
		this.cellCoordinate.rowPosition= localLayer.getDim(VERTICAL).underlyingToLocalPosition(
				this.layer.getDim(VERTICAL), this.cellCoordinate.rowPosition );
		if (this.cellCoordinate.rowPosition < 0 || this.cellCoordinate.rowPosition >= localLayer.getRowCount()) {
			return false;
		}
		
		this.layer= localLayer;
		return true;
	}
	
	
	/**
	 * @return The column position of the cell to edit.
	 */
	public long getColumnPosition() {
		return this.cellCoordinate.columnPosition;
	}
	
	/**
	 * @return The row position of the cell to edit.
	 */
	public long getRowPosition() {
		return this.cellCoordinate.rowPosition;
	}
	
	/**
	 * @return The parent Composite, needed for the creation of the editor control.
	 */
	public Composite getParent() {
		return this.parent;
	}
	
	/**
	 * @return The {@link ConfigRegistry} containing the configuration of the
	 * 			current NatTable instance the command should be executed for.
	 * 			This is necessary because the edit controllers in the current architecture
	 * 			are not aware of the instance they are running in.
	 */
	public ConfigRegistry getConfigRegistry() {
		return this.configRegistry;
	}
	
	/**
	 * @return The value that should be put to the activated editor control.
	 */
	public Object getInitialValue() {
		return this.initialValue;
	}
	
}
