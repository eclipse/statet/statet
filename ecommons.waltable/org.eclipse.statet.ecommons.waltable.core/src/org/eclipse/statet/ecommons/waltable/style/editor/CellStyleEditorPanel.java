/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.ecommons.waltable.Messages;
import org.eclipse.statet.ecommons.waltable.core.style.BasicStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.HorizontalAlignment;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.core.style.VerticalAlignment;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


/**
 * EditorPanel for editing the core style attributes.
 */
public class CellStyleEditorPanel extends AbstractEditorPanel<Style> {

    private static final Color DEFAULT_FG_COLOR= GUIHelper.COLOR_BLACK;
	private static final Color DEFAULT_BG_COLOR= GUIHelper.COLOR_WHITE;
	private ColorPicker backgroundColorPicker;
    private ColorPicker foregroundColorPicker;
    private FontPicker fontPicker;
    private HorizontalAlignmentPicker horizontalAlignmentPicker;
    private VerticalAlignmentPicker verticalAlignmentPicker;
	private Color origBgColor;
	private Color origFgColor;
	private HorizontalAlignment origHAlign;
	private VerticalAlignment origVAlign;

    public CellStyleEditorPanel(final Composite parent, final int style) {
        super(parent, style);
        initComponents();
    }

    private void initComponents() {
        final GridLayout gridLayout= new GridLayout(2, false);
        gridLayout.marginLeft= 10;
		setLayout(gridLayout);

        new Label(this, SWT.NONE).setText(Messages.getString("CellStyleEditorPanel.backgroundColor")); //$NON-NLS-1$
        this.backgroundColorPicker= new ColorPicker(this, DEFAULT_BG_COLOR);

        new Label(this, SWT.NONE).setText(Messages.getString("CellStyleEditorPanel.foregroundColor")); //$NON-NLS-1$
        this.foregroundColorPicker= new ColorPicker(this, DEFAULT_FG_COLOR);

        new Label(this, SWT.NONE).setText(Messages.getString("CellStyleEditorPanel.font")); //$NON-NLS-1$
        this.fontPicker= new FontPicker(this, GUIHelper.DEFAULT_FONT);
        this.fontPicker.setLayoutData(new GridData(80, 20));

        new Label(this, SWT.NONE).setText(Messages.getString("CellStyleEditorPanel.horizontalAlignment")); //$NON-NLS-1$
        this.horizontalAlignmentPicker= new HorizontalAlignmentPicker(this, HorizontalAlignment.CENTER);

        new Label(this, SWT.NONE).setText(Messages.getString("CellStyleEditorPanel.verticalAlignment")); //$NON-NLS-1$
        this.verticalAlignmentPicker= new VerticalAlignmentPicker(this, VerticalAlignment.MIDDLE);
    }

    @Override
    public String getEditorName() {
        return Messages.getString("CellStyleEditorPanel.editorName"); //$NON-NLS-1$
    }

    @Override
    public void edit(final Style style) throws Exception {
        this.origBgColor= style.getAttributeValue(CellStyling.BACKGROUND_COLOR);
        if (this.origBgColor == null) {
        	this.origBgColor= GUIHelper.COLOR_WHITE;
        }
		this.backgroundColorPicker.setSelectedColor(this.origBgColor);

        this.origFgColor= style.getAttributeValue(CellStyling.FOREGROUND_COLOR);
        if (this.origFgColor == null) {
        	this.origFgColor= GUIHelper.COLOR_BLACK;
        }
		this.foregroundColorPicker.setSelectedColor(this.origFgColor);

		this.fontPicker.setOriginalFont(style.getAttributeValue(CellStyling.FONT));

        this.origHAlign= style.getAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT);
        if (this.origHAlign == null) {
        	this.origHAlign= HorizontalAlignment.CENTER;
        }
		this.horizontalAlignmentPicker.setSelectedAlignment(this.origHAlign);

        this.origVAlign= style.getAttributeValue(CellStyling.VERTICAL_ALIGNMENT);
        if (this.origVAlign == null) {
        	this.origVAlign= VerticalAlignment.MIDDLE;
        }
		this.verticalAlignmentPicker.setSelectedAlignment(this.origVAlign);
    }

    @Override
    public BasicStyle getNewValue() {
    	final BasicStyle newStyle= new BasicStyle();
    	
    	final Color bgColor= this.backgroundColorPicker.getSelectedColor();
    	newStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, bgColor);
    	
    	final Color fgColor= this.foregroundColorPicker.getSelectedColor();
    	newStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, fgColor);
    	
    	final Font font= this.fontPicker.getSelectedFont();
		newStyle.setAttributeValue(CellStyling.FONT, font);
		
    	final HorizontalAlignment hAlign= this.horizontalAlignmentPicker.getSelectedAlignment();
    	newStyle.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, hAlign);
    	
    	final VerticalAlignment vAlign= this.verticalAlignmentPicker.getSelectedAlignment();
		newStyle.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, vAlign);
		
    	return newStyle;
    }
	
}
