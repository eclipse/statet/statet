/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.validate;


public class DefaultNumericDataValidator extends DataValidator {

	@Override
	public boolean validate(final long columnIndex, final long rowIndex, final Object newValue) {
		try {
			if (newValue != null) {
				Double.parseDouble(newValue.toString());
			}
		} catch (final Exception e) {
			return false;
		}
		return true;
	}

}
