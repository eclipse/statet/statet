/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerListener;


/**
 * Event handler for handling {@link InlineCellEditEvent}s.
 * Used to activate editors for inline editing.
 * 
 * @see InlineCellEditEvent
 * @see EditSelectionCommandHandler
 */
@NonNullByDefault
public class InlineCellEditEventHandler implements LayerListener {
	
	
	/**
	 * The layer this event handler is associated with. Needed for the conversion of 
	 * cell position coordinates.Usually this is a grid layer because this is the main 
	 * cause for this event handler is needed.
	 */
	private final Layer layer;
	
	
	/**
	 * @param layer The layer this event handler is associated with. Needed for 
	 * 			the conversion of cell position coordinates.
	 */
	public InlineCellEditEventHandler(final Layer layer) {
		this.layer= layer;
	}
	
	
	@Override
	public void handleLayerEvent(final LayerEvent event) {
		if (event instanceof InlineCellEditEvent) {
			final var editEvent= (InlineCellEditEvent)event.toLayer(this.layer);
			if (editEvent != null) {
				final LayerCell cell= this.layer.getCellByPosition(editEvent.getColumnPosition(), editEvent.getRowPosition());
				EditController.editCell(cell, editEvent.getParent(), editEvent.getInitialValue(), editEvent.getConfigRegistry());
			}
		}
	}
	
}
