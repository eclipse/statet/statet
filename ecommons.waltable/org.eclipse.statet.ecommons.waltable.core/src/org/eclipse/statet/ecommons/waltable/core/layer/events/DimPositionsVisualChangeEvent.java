/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.events;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


/**
 * An event that indicates a visible change to one ore more columns/rows in the
 * layer.
 */
@NonNullByDefault
public class DimPositionsVisualChangeEvent implements VisualChangeEvent {
	
	
	/**
	 * The LayerDim to which the given column/row positions match
	 */
	private final LayerDim layerDim;
	
	/**
	 * The column position ranges for the columns/rows that have changed.
	 */
	private final List<LRange> positions;
	
	
	/**
	 * Creates a new ColumnVisualChangeEvent based on the given information.
	 * @param layerDim the layer dim to which the given positions match.
	 * @param positions the position ranges of columns/rows that have changed.
	 */
	public DimPositionsVisualChangeEvent(final LayerDim layerDim, final List<LRange> positions) {
		this.layerDim= layerDim;
		this.positions= positions;
	}
	
	public DimPositionsVisualChangeEvent(final LayerDim layerDim, final LRange positionRanges) {
		this.layerDim= layerDim;
		this.positions= ImCollections.newList(positionRanges);
	}
	
	@Override
	public @Nullable DimPositionsVisualChangeEvent toLayer(final Layer targetLayer) {
		final LayerDim targetLayerDim= targetLayer.getDim(getOrientation());
		if (targetLayerDim == this.layerDim) {
			return this;
		}
		
		final var positionRanges= targetLayerDim.underlyingToLocalPositions(this.layerDim,
				this.positions );
		
		return (positionRanges != null && positionRanges.size() > 0) ?
				toLayer(targetLayerDim, positionRanges) : null;
	}
	
	protected @Nullable DimPositionsVisualChangeEvent toLayer(final LayerDim targetLayerDim,
			final List<LRange> positionRanges) {
		return new DimPositionsVisualChangeEvent(targetLayerDim, positionRanges);
	}
	
	
	public final LayerDim getLayerDim() {
		return this.layerDim;
	}
	
	public final Orientation getOrientation() {
		return this.layerDim.getOrientation();
	}
	
	@Override
	public final Layer getLayer() {
		return this.layerDim.getLayer();
	}
	
	/**
	 * @return The column/row position ranges for the columns that have changed.
	 */
	public List<LRange> getPositionRanges() {
		return this.positions;
	}
	
	@Override
	public ImList<LRectangle> getChangedPositionRectangles() {
		final var columnPositionRanges= getPositionRanges();
		final var positionRectangles= new @NonNull LRectangle[columnPositionRanges.size()];
		
		final long positionCount;
		switch (getOrientation()) {
		case HORIZONTAL:
			positionCount= getLayer().getDim(VERTICAL).getPositionCount();
			for (int i= 0; i < positionRectangles.length; i++) {
				final var range= columnPositionRanges.get(i);
				positionRectangles[i]=
						new LRectangle(range.start, 0, range.end - range.start, positionCount);
			}
			break;
		case VERTICAL:
			positionCount= getLayer().getDim(HORIZONTAL).getPositionCount();
			for (int i= 0; i < positionRectangles.length; i++) {
				final var range= columnPositionRanges.get(i);
				positionRectangles[i]=
						new LRectangle(0, range.start, positionCount, range.end - range.start);
			}
			break;
		default:
			throw new IllegalStateException();
		}
		
		return ImCollections.newList(positionRectangles);
	}
	
	
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
	
}
