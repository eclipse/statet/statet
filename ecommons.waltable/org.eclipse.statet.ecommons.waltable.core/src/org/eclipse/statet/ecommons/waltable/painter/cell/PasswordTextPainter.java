/*=============================================================================#
 # Copyright (c) 2012, 2025 Dirk Fauth and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Dirk Fauth - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.edit.editor.PasswordCellEditor;
import org.eclipse.statet.ecommons.waltable.style.CellStyleUtil;


/**
 * Specialized {@link TextPainter} that will paint the text that should be showed within
 * a cell anonymized. Like in {@link PasswordCellEditor} every character will be replaced 
 * with another echo character. The echo character can be configured by setting the 
 * attribute {@link CellStyling#PASSWORD_ECHO_CHAR} to the cell style to use. 
 * If no echo character is configured, the bullet character will be used as echo character.
 * 
 * @see PasswordCellEditor
 */
public class PasswordTextPainter extends TextPainter {

	/**
	 * The echo character to use for anonymization.
	 * Stored as member variable because getTextToDisplay() has no context information.
	 * Will be set on every paintCell() so changes to the cell style will be taken
	 * into account on runtime.
	 * Default value is the bullet character.
	 */
	private Character echoChar= '\u2022';
	
	public PasswordTextPainter() {
		super();
	}

	public PasswordTextPainter(final boolean wrapText, final boolean paintBg) {
		super(wrapText, paintBg);
	}

	public PasswordTextPainter(final boolean wrapText, final boolean paintBg, final int spacing) {
		super(wrapText, paintBg, spacing);
	}

	public PasswordTextPainter(final boolean wrapText, final boolean paintBg, final boolean calculate) {
		super(wrapText, paintBg, calculate);
	}

	public PasswordTextPainter(final boolean wrapText, final boolean paintBg, final int spacing, final boolean calculate) {
		super(wrapText, paintBg, spacing, calculate);
	}

	
	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle lRectangle, final ConfigRegistry configRegistry) {
		//check for the configuration of a echo character in the corresponding cell style
		final Style cellStyle= CellStyleUtil.getCellStyle(cell, configRegistry);
		final Character configEchoChar= cellStyle.getAttributeValue(CellStyling.PASSWORD_ECHO_CHAR);
		if (configEchoChar != null) {
			this.echoChar= configEchoChar;
		}
		super.paintCell(cell, gc, lRectangle, configRegistry);
	}
	
	@Override
	protected String getTextToDisplay(final LayerCell cell, final GC gc, final long availableLength, final String text) {
		final String result= super.getTextToDisplay(cell, gc, availableLength, text);
		//replace all characters with the configured echo character
		return result.replaceAll(".", this.echoChar.toString()); //$NON-NLS-1$
	}
}
