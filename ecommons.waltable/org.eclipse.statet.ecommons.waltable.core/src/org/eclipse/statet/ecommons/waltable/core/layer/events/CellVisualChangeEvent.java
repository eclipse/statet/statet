/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.events;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LPoint;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


@NonNullByDefault
public class CellVisualChangeEvent implements VisualChangeEvent {
	
	
	protected final Layer layer;
	
	protected final LPoint position;
	
	
	public CellVisualChangeEvent(final Layer layer, final LPoint position) {
		this.layer= layer;
		this.position= position;
	}
	
	public CellVisualChangeEvent(final Layer layer, final long columnPosition, final long rowPosition) {
		this(layer, new LPoint(columnPosition, rowPosition));
	}
	
	@Override
	public @Nullable CellVisualChangeEvent toLayer(final Layer targetLayer) {
		if (targetLayer == this.layer) {
			return this;
		}
		
		final var hTargetDim= targetLayer.getDim(HORIZONTAL);
		final var vTargetDim= targetLayer.getDim(VERTICAL);
		final long hPosition= hTargetDim.underlyingToLocalPosition(this.layer.getDim(HORIZONTAL),
				this.position.x );
		final long vPosition= vTargetDim.underlyingToLocalPosition(this.layer.getDim(VERTICAL),
				this.position.y );
		
		return (hPosition >= 0 && hPosition < hTargetDim.getPositionCount()
						&& vPosition >= 0 && vPosition < vTargetDim.getPositionCount() ) ?
				toLayer(targetLayer, hPosition, vPosition) :
				null;
	}
	
	protected @Nullable CellVisualChangeEvent toLayer(final Layer targetLayer,
			final long columnPosition, final long rowPosition) {
		return new CellVisualChangeEvent(targetLayer, columnPosition, rowPosition);
	}
	
	
	@Override
	public final Layer getLayer() {
		return this.layer;
	}
	
	public LPoint getPosition() {
		return this.position;
	}
	
	public long getColumnPosition() {
		return this.position.x;
	}
	
	public long getRowPosition() {
		return this.position.y;
	}
	
	@Override
	public ImList<LRectangle> getChangedPositionRectangles() {
		return ImCollections.newList(
				new LRectangle(this.position.x, this.position.y, 1, 1) );
	}
	
}
