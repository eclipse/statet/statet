/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.data;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Provide data to the table.
 * 
 * @see ListDataProvider
 * @see org.eclipse.statet.ecommons.waltable.data.core.DataLayer
 */
@NonNullByDefault
public interface DataProvider {
	
	
	int FORCE_SYNC= 1 << 0;
	
	
	long getColumnCount();
	
	long getRowCount();
	
	
	/**
	 * Gets the value at the given column and row index.
	 *
	 * @param columnIndex
	 * @param rowIndex
	 * @param flags
	 * @param monitor
	 * @return the data value associated with the specified cell
	 */
	@Nullable Object getDataValue(long columnIndex, long rowIndex,
			int flags, @Nullable IProgressMonitor monitor);
	
	/**
	 * Sets the value at the given column and row index. Optional operation.
	 * 
	 * The method throws an UnsupportedOperationException if this operation is not supported.
	 *
	 * @param columnIndex
	 * @param rowIndex
	 * @param newValue
	 */
	void setDataValue(long columnIndex, long rowIndex, @Nullable Object newValue);
	
}
