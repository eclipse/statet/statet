/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionOutOfBoundsException;


@NonNullByDefault
public class LayerUtils {
	
	
	/**
	 * Convert column/row position from the source layer to the target layer
	 * @param sourceLayer source layer
	 * @param sourceColumnPosition column position in the source layer
	 * @param targetLayer layer to convert the from position to 
	 * @return converted position, or ILayerDim.POSITION_NA if conversion not possible
	 */
	public static final long convertPosition(final LayerDim sourceDim,
			final long sourceRefPosition, final long sourcePosition,
			final LayerDim targetDim) {
		if (targetDim == sourceDim) {
			return sourcePosition;
		}
		
		try {
			final long id= sourceDim.getPositionId(sourceRefPosition, sourcePosition);
			return targetDim.getPositionById(id);
		}
		catch (final PositionOutOfBoundsException e) {
			return LayerDim.POSITION_NA;
		}
	}
	
	/**
	 * Convert column position from the source layer to the target layer
	 * @param sourceLayer source layer
	 * @param sourceColumnPosition column position in the source layer
	 * @param targetLayer layer to convert the from position to 
	 * @return converted column position, or -1 if conversion not possible
	 */
	public static final long convertColumnPosition(final Layer sourceLayer,
			final long sourceColumnPosition, final Layer targetLayer) {
		return convertPosition(sourceLayer.getDim(HORIZONTAL),
				sourceColumnPosition, sourceColumnPosition,
				targetLayer.getDim(HORIZONTAL) );
	}
	
	/**
	 * Convert row position from the source layer to the target layer
	 * @param sourceLayer source layer
	 * @param sourceRowPosition position in the source layer
	 * @param targetLayer layer to convert the from position to 
	 * @return converted row position, or -1 if conversion not possible
	 */
	public static final long convertRowPosition(final Layer sourceLayer,
			final long sourceRowPosition, final Layer targetLayer) {
		return convertPosition(sourceLayer.getDim(VERTICAL),
				sourceRowPosition, sourceRowPosition,
				targetLayer.getDim(VERTICAL) );
	}
	
}
