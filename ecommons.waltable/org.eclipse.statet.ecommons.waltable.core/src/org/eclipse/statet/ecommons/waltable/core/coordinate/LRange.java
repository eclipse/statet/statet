/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.coordinate;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Range of numbers of type long.
 * 
 * <p>Ranges are inclusive of their start value and exclusive of their end value,
 * start &lt;= x &lt; end.</p>
 */
@NonNullByDefault
public final class LRange implements Comparable<LRange> {
	
	
	public long start;
	public long end;
	
	
	/**
	 * Creates a new range with the specified start and end values.
	 */
	public LRange(final long start, final long end) {
		this.start= start;
		this.end= end;
	}
	
	/**
	 * Creates a new range which contains the specified single value.
	 */
	public LRange(final long value) {
		this.start= value;
		this.end= value + 1;
	}
	
	
	public long size() {
		return this.end - this.start;
	}
	
	
	@Override
	public int compareTo(final LRange other) {
		if (this.start < other.start) {
			return -1;
		}
		if (this.start > other.start) {
			return 1;
		}
		if (this.end < other.end) {
			return -1;
		}
		if (this.end > other.end) {
			return 1;
		}
		return 0;
	}
	
	/**
	 * @return <code>true</code> if the range contains the given row position
	 */
	public boolean contains(final long position) {
		return (position >= this.start && position < this.end);
	}
	
	public boolean overlap(final LRange lRange) {
		return (this.start < this.end &&  // this is non-empty
				lRange.start < lRange.end &&  // parameter is non-empty
				(contains(lRange.start) || contains(lRange.end - 1)
						|| lRange.contains(this.start) || lRange.contains(this.end - 1) ));
	}
	
	
	@Override
	public int hashCode() {
		int h= (int)(this.start ^ (this.start >>> 32));
		h= Integer.rotateRight(h, 15);
		h ^= (int)(this.end ^ (this.end >>> 32));
		return h ^ (h >>> 7);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final LRange other
						&& this.start == other.start && this.end == other.end ));
	}
	
	
	@Override
	public String toString() {
		return "[" + this.start + ", " + this.end + ")L"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
	
}
