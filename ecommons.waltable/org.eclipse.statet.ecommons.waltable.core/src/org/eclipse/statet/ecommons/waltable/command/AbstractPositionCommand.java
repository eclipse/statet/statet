/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.command;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


public abstract class AbstractPositionCommand implements LayerCommand {
	
	
	private PositionCoordinate positionCoordinate;
	
	
	protected AbstractPositionCommand(final Layer layer, final long columnPosition, final long rowPosition) {
		this.positionCoordinate= new PositionCoordinate(layer, columnPosition, rowPosition);
	}
	
	protected AbstractPositionCommand(final AbstractPositionCommand command) {
		this.positionCoordinate= command.positionCoordinate;
	}
	
	
	public long getColumnPosition() {
		return this.positionCoordinate.getColumnPosition();
	}
	
	public long getRowPosition() {
		return this.positionCoordinate.getRowPosition();
	}
	
	@Override
	public boolean convertToTargetLayer(final Layer targetLayer) {
		final PositionCoordinate targetPositionCoordinate= LayerCommandUtil.convertPositionToTargetContext(this.positionCoordinate, targetLayer);
		if (targetPositionCoordinate != null) {
			this.positionCoordinate= targetPositionCoordinate;
			return true;
		} else {
			return false;
		}
	}
	
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " columnPosition=" + this.positionCoordinate.getColumnPosition() + ", rowPosition=" + this.positionCoordinate.getRowPosition(); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
