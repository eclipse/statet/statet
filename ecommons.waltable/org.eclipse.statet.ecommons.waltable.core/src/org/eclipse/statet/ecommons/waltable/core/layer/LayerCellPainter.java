/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.painter.cell.PercentageBarCellPainter;


/**
 * Implementations are responsible for painting a cell.
 * 
 * Custom {@link LayerCellPainter} can be registered in the {@link ConfigRegistry}.
 * This is a mechanism for plugging in custom cell painting.
 * 
 * @see PercentageBarCellPainter
 */
public interface LayerCellPainter {
	
	
	void paintCell(LayerCell cell, GC gc, LRectangle bounds, ConfigRegistry configRegistry);
	
	/**
	 * Get the preferred width of the cell when rendered by this painter. Used for auto-resize.
	 * @param cell
	 * @param gc
	 * @param configRegistry
	 * @return
	 */
	long getPreferredWidth(LayerCell cell, GC gc, ConfigRegistry configRegistry);
	
	/**
	 * Get the preferred height of the cell when rendered by this painter. Used for auto-resize.
	 * @param cell
	 * @param gc
	 * @param configRegistry
	 * @return
	 */
	long getPreferredHeight(LayerCell cell, GC gc, ConfigRegistry configRegistry);
	
	
	LayerCellPainter getCellPainterAt(long x, long y, LayerCell cell, GC gc,
			LRectangle adjustedCellBounds, ConfigRegistry configRegistrys);
	
}
