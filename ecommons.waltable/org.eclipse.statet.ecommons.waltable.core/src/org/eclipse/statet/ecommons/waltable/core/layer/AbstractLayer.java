/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.config.IConfiguration;
import org.eclipse.statet.ecommons.waltable.core.Persistable;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PixelOutOfBoundsException;
import org.eclipse.statet.ecommons.waltable.core.swt.painters.GridLineCellLayerPainter;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;


/**
 * Base layer implementation with common methods for managing listeners and caching, etc.
 * 
 * @param <TLayerDim> the type of the layer dimension
 */
@NonNullByDefault
public abstract class AbstractLayer<TLayerDim extends LayerDim> implements Layer {
	
	
	private TLayerDim hDim= nonNullLateInit();
	private TLayerDim vDim= nonNullLateInit();
	
	private @Nullable LayerPainter layerPainter;
	private ClientAreaProvider clientAreaProvider= ClientAreaProvider.DEFAULT;
	
	private final Map<Class<? extends LayerCommand>, LayerCommandHandler<? extends LayerCommand>> commandHandlers= new LinkedHashMap<>();
	private final CopyOnWriteIdentityListSet<LayerListener> eventHandlers= new CopyOnWriteIdentityListSet<>();
	
	private final CopyOnWriteIdentityListSet<Persistable> persistables= new CopyOnWriteIdentityListSet<>();
	private final CopyOnWriteIdentityListSet<LayerListener> listeners= new CopyOnWriteIdentityListSet<>();
	
	private final Collection<IConfiguration> configurations= new ArrayList<>();
	
	
	protected AbstractLayer() {
	}
	
	protected AbstractLayer(final @Nullable LayerPainter layerPainter) {
		this.layerPainter= layerPainter;
	}
	
	
	protected void init() {
		this.hDim= createDim(HORIZONTAL);
		this.vDim= createDim(VERTICAL);
		if (this.hDim.getOrientation() != HORIZONTAL || this.vDim.getOrientation() != VERTICAL) {
			throw new RuntimeException("Invalid LayerDim");
		}
	}
	
	// Dims
	
	protected abstract TLayerDim createDim(Orientation orientation);
	
	@Override
	public TLayerDim getDim(final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return this.hDim;
		case VERTICAL:
			return this.vDim;
		default:
			throw new IllegalStateException();
		}
	}
	
	
	// Dispose
	
	@Override
	public void dispose() {
	}
	
	
	// Regions
	
	@Override
	public LabelStack getRegionLabelsByXY(final long x, final long y) {
		return new LabelStack();
	}
	
	// Persistence
	
	@Override
	public void saveState(final String prefix, final Map<String, String> properties) {
		for (final Persistable persistable : this.persistables) {
			persistable.saveState(prefix, properties);
		}
	}
	
	@Override
	public void loadState(final String prefix, final Map<String, String> properties) {
		for (final Persistable persistable : this.persistables) {
			persistable.loadState(prefix, properties);
		}
	}
	  
	@Override
	public void registerPersistable(final Persistable persistable){
		this.persistables.add(persistable);
	}

	@Override
	public void unregisterPersistable(final Persistable persistable){
		this.persistables.remove(persistable);
	}
	
	// Configuration
	
	public void addConfiguration(final IConfiguration configuration) {
		this.configurations.add(configuration);
	}

	public void clearConfiguration() {
		this.configurations.clear();
	}
	
	@Override
	public void configure(final ConfigRegistry configRegistry, final UiBindingRegistry uiBindingRegistry) {
		for (final IConfiguration configuration : this.configurations) {
			configuration.configureLayer(this);
			configuration.configureRegistry(configRegistry);
			configuration.configureUiBindings(uiBindingRegistry);
		}
	}
	
	// Commands
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean doCommand(final LayerCommand command) {
		for (final var entry : this.commandHandlers.entrySet()) {
			if (entry.getKey().isInstance(command)) {
				final LayerCommandHandler commandHandler= entry.getValue();
				if (commandHandler.doCommand(this, command.cloneCommand())) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	// Command handlers
	
	/**
	 * Layers should use this method to register their command handlers
	 * and call it from their constructor. This allows easy overriding if 
	 * required of command handlers 
	 */
	protected void registerCommandHandlers() {
		// No op
	}
	
	@Override
	public void registerCommandHandler(final LayerCommandHandler<?> commandHandler) {
		this.commandHandlers.put(commandHandler.getCommandClass(), commandHandler);
	}
	
	@Override
	public void unregisterCommandHandler(final Class<? extends LayerCommand> commandClass) {
		this.commandHandlers.remove(commandClass);
	}
	
	// Events
	
	@Override
	public void addLayerListener(final LayerListener listener) {
		this.listeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeLayerListener(final LayerListener listener) {
		this.listeners.remove(listener);
	}
	
	/**
	 * Handle layer event notification. Convert it to your context
	 * and propagate <i>UP</i>.
	 *  
	 * If you override this method you <strong>MUST NOT FORGET</strong> to raise
	 * the event up the layer stack by calling <code>super.fireLayerEvent(event)</code>
	 * - unless you plan to eat the event yourself.
	 */
	@Override
	public void handleLayerEvent(final LayerEvent event) {
		for (final var handler : this.eventHandlers) {
			handler.handleLayerEvent(event);
		}
		
		final var localEvent= event.toLayer(this);
		// Pass on the event to our parent
		if (localEvent != null) {
			fireLayerEvent(localEvent);
		}
	}
	
	public void registerEventHandler(final LayerListener eventHandler) {
		this.eventHandlers.add(nonNullAssert(eventHandler));
	}
	
	public void unregisterEventHandler(final LayerListener eventHandler) {
		this.eventHandlers.remove(eventHandler);
	}
	
	/**
	 * Pass the event to all the {@link LayerListener} registered on this layer.
	 * A cloned copy is passed to each listener.
	 */
	@Override
	public void fireLayerEvent(final LayerEvent event) {
		for (final var listener : this.listeners) {
			listener.handleLayerEvent(event);
		}
	}
	
	/**
	 * @return {@link LayerPainter}. Defaults to {@link GridLineCellLayerPainter}
	 */
	@Override
	public LayerPainter getLayerPainter() {
		var layerPainter= this.layerPainter;
		if (layerPainter == null) {
			layerPainter= createPainter();
			this.layerPainter= layerPainter;
		}
		return layerPainter;
	}
	
	protected LayerPainter createPainter() {
		return new GridLineCellLayerPainter();
	}
	
	
	// Client area
	
	@Override
	public ClientAreaProvider getClientAreaProvider() {
		return this.clientAreaProvider;
	}
	
	@Override
	public void setClientAreaProvider(final ClientAreaProvider clientAreaProvider) {
		this.clientAreaProvider= clientAreaProvider;
	}
	
	
	@Override
	public final long getColumnCount() {
		return this.hDim.getPositionCount();
	}
	
	@Override
	public final long getWidth() {
		return this.hDim.getSize();
	}
	
	@Override
	public final long getColumnPositionByX(final long x) {
		try {
			return this.hDim.getPositionByPixel(x);
		}
		catch (final PixelOutOfBoundsException e) {
			return Long.MIN_VALUE;
		}
	}
	
	
	@Override
	public final long getRowCount() {
		return this.vDim.getPositionCount();
	}
	
	@Override
	public final long getHeight() {
		return this.vDim.getSize();
	}
	
	@Override
	public final long getRowPositionByY(final long y) {
		try {
			return this.vDim.getPositionByPixel(y);
		}
		catch (final PixelOutOfBoundsException e) {
			return Long.MIN_VALUE;
		}
	}
	
	
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
	
}
