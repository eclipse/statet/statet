/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style.editor;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


public class DisplayColumnStyleEditorCommand extends AbstractContextFreeCommand {

	public final long columnPosition;
	public final long rowPosition;
	private final Layer layer;
	private final ConfigRegistry configRegistry;

	public DisplayColumnStyleEditorCommand(final Layer natLayer, final ConfigRegistry configRegistry, final long columnPosition, final long rowPosition) {
		this.layer= natLayer;
		this.configRegistry= configRegistry;
		this.columnPosition= columnPosition;
		this.rowPosition= rowPosition;
	}
	
	public Layer getNattableLayer() {
		return this.layer;
	}

	public ConfigRegistry getConfigRegistry() {
		return this.configRegistry;
	}
}
