/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.config;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;


public abstract class AbstractRegistryConfiguration implements IConfiguration {
	
	@Override
	public void configureLayer(final Layer layer) {}
	
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {}
	
}
