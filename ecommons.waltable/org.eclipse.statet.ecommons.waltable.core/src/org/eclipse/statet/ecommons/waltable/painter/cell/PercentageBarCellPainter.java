/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle.LineStyle;
import org.eclipse.statet.ecommons.waltable.painter.cell.decorator.LineBorderDecorator;
import org.eclipse.statet.ecommons.waltable.painter.cell.decorator.PaddingDecorator;
import org.eclipse.statet.ecommons.waltable.painter.cell.decorator.PercentageBarDecorator;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


public class PercentageBarCellPainter extends CellPainterWrapper {

    public PercentageBarCellPainter() {
        this(2);
    }

    public PercentageBarCellPainter(final long outerPadding) {
        super(new PaddingDecorator(new LineBorderDecorator(new PercentageBarDecorator(new TextPainter(false, false)),
        		new BorderStyle(1, GUIHelper.COLOR_BLACK, LineStyle.SOLID)),
        		outerPadding));
    }
}
