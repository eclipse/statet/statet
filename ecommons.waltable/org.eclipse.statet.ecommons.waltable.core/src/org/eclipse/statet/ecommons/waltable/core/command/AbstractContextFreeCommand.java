/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.command;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


@NonNullByDefault
public abstract class AbstractContextFreeCommand implements LayerCommand {
	
	
	@Override
	public boolean convertToTargetLayer(final Layer targetLayer) {
		return true;
	}
	
	@Override
	public AbstractContextFreeCommand cloneCommand() {
		return this;
	}
	
}
