/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.menu;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Menu;

import org.eclipse.statet.ecommons.waltable.Messages;
import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.config.AbstractUiBindingConfiguration;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;


public class BodyMenuConfiguration extends AbstractUiBindingConfiguration {

	private final Menu colHeaderMenu;
	
	public BodyMenuConfiguration(final NatTable natTable, final Layer bodyLayer) {
		this.colHeaderMenu= new PopupMenuBuilder(natTable)
								.withColumnStyleEditor(Messages.getString("ColumnStyleEditorDialog.shellTitle")) //$NON-NLS-1$
								.build();
		
		natTable.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				BodyMenuConfiguration.this.colHeaderMenu.dispose();
			}
			
		});
	}
	
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		uiBindingRegistry.registerMouseDownBinding(
				new MouseEventMatcher(SWT.NONE, GridLabels.COLUMN_HEADER, 3), 
				new PopupMenuAction(this.colHeaderMenu));
	}

}
