/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;


public class NatEventData {
	
	private final Object originalEvent;
	private final NatTable natTable;
	private final LabelStack regionLabels;
	long columnPosition;
	long rowPosition;

	public static NatEventData createInstanceFromEvent(final MouseEvent event) {
		final NatTable natTable= (NatTable) event.widget;
		
		final long columnPosition= natTable.getColumnPositionByX(event.x);
		final long rowPosition= natTable.getRowPositionByY(event.y);
        
        return new NatEventData(
				natTable,
				natTable.getRegionLabelsByXY(event.x, event.y),
				columnPosition,
				rowPosition,
				event
		);
	}
	
	public NatEventData(final NatTable natTable, final LabelStack regionLabels, final long columnPosition, final long rowPosition, final Object originalEvent) {
		this.natTable= natTable;
		this.regionLabels= regionLabels;
		this.columnPosition= columnPosition;
		this.rowPosition= rowPosition;
		this.originalEvent= originalEvent;
	}

	public NatTable getNatTable() {
		return this.natTable;
	}

	public LabelStack getRegionLabels() {
		return this.regionLabels;
	}

	public long getColumnPosition() {
		return this.columnPosition;
	}

	public long getRowPosition() {
		return this.rowPosition;
	}
	
	public Object getOriginalEvent() {
		return this.originalEvent;
	}

}
