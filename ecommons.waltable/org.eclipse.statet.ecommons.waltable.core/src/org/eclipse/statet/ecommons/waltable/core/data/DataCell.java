/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class DataCell {

	protected long columnPosition;
	
	protected long rowPosition;
	
	protected long columnSpan;
	
	protected long rowSpan;
	
	public DataCell(final long columnPosition, final long rowPosition) {
		this(columnPosition, rowPosition, 1, 1);
	}	
	
	public DataCell(final long columnPosition, final long rowPosition, final long columnSpan, final long rowSpan) {
		this.columnPosition= columnPosition;
		this.rowPosition= rowPosition;
		this.columnSpan= columnSpan;
		this.rowSpan= rowSpan;
	}
	
	public long getColumnPosition() {
		return this.columnPosition;
	}
	
	public long getRowPosition() {
		return this.rowPosition;
	}
	
	public long getColumnSpan() {
		return this.columnSpan;
	}
	
	public long getRowSpan() {
		return this.rowSpan;
	}
	
	public boolean isSpannedCell() {
		return this.columnSpan > 1 || this.rowSpan > 1;
	}
	
	
	@Override
	public int hashCode() {
		int h= (int) (this.columnPosition ^ (this.columnPosition >>> 32));
		h= Integer.rotateRight(h, 15);
		h ^= (int) (this.rowPosition ^ (this.rowPosition >>> 32));
		h ^= (h >>> 7);
		h ^= 3 * (int) (this.columnSpan ^ (this.columnSpan >>> 32));
		h ^= 31 * (int) (this.rowSpan ^ (this.rowSpan >>> 32));
		return h;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final DataCell other
						&& this.columnPosition == other.columnPosition && this.rowPosition == other.rowPosition
						&& this.columnSpan == other.columnSpan && this.rowSpan == other.rowSpan ));
	}
	
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " (" //$NON-NLS-1$
			+ "\n\tcolumnPosition= " + this.columnPosition //$NON-NLS-1$
			+ "\n\trowPosition= " + this.rowPosition //$NON-NLS-1$
			+ "\n\tcolumnSpan= " + this.columnSpan //$NON-NLS-1$
			+ "\n\trowSpan= " + this.rowSpan //$NON-NLS-1$
			+ "\n)"; //$NON-NLS-1$
	}
	
}
