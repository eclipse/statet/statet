/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.binding;

import org.eclipse.statet.ecommons.waltable.ui.action.IDragMode;
import org.eclipse.statet.ecommons.waltable.ui.matcher.IMouseEventMatcher;


public class DragBinding {
	
	private final IMouseEventMatcher mouseEventMatcher;
	
	private final IDragMode dragMode;
	
	public DragBinding(final IMouseEventMatcher mouseEventMatcher, final IDragMode dragMode) {
		this.mouseEventMatcher= mouseEventMatcher;
		this.dragMode= dragMode;
	}
	
	public IMouseEventMatcher getMouseEventMatcher() {
		return this.mouseEventMatcher;
	}
	
	public IDragMode getDragMode() {
		return this.dragMode;
	}
	
}
