/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.config;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * The various modes the table can be under.
 * <ol>
 *    <li>During normal display a cell is in NORMAL mode.</li>
 *    <li>If the contents of the cell are being edited, its in EDIT mode.</li>
 *    <li>If a cell has been selected, its in SELECTED mode.</li>
 * </ol>
 * 
 * These modes are used to bind different settings to different modes. For
 * example, a different style can be registered for a cell when it is in
 * SELECTED mode.
 */
@NonNullByDefault
public enum DisplayMode {
	
	NORMAL,
	HOVER,
	SELECTED,
	EDIT;
	
	
}
