/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.events;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


/**
 * Event indicating a change in the structure of the columns/rows.
 * 
 * <p>This event carried {@link StructuralDiff} indicating the columns/rows
 * which have changed.</p>
 */
@NonNullByDefault
public abstract class DimPositionsStructuralChangeEvent extends DimPositionsVisualChangeEvent
		implements StructuralChangeEvent {
	
	
	/**
	 * Creates a new ColumnStructuralChangeEvent based on the given information.
	 * @param layerDim the layer dim to which the given positions match.
	 * @param positions the position ranges of columns/rows that have changed.
	 */
	public DimPositionsStructuralChangeEvent(final LayerDim layerDim,
			final List<LRange> positions) {
		super(layerDim, positions);
	}
	
	@Override
	protected @Nullable abstract DimPositionsStructuralChangeEvent toLayer(LayerDim targetLayerDim,
			List<LRange> positions);
	
	
	@Override
	public boolean isStructureChanged(final Orientation orientation) {
		return (getOrientation() == orientation);
	}
	
	
}
