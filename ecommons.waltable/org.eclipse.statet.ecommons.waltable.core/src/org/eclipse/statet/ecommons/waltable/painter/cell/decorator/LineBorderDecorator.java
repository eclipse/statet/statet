/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell.decorator;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;
import org.eclipse.statet.ecommons.waltable.painter.cell.CellPainterWrapper;
import org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils;
import org.eclipse.statet.ecommons.waltable.style.CellStyleUtil;


public class LineBorderDecorator extends CellPainterWrapper {

	private final BorderStyle defaultBorderStyle;

	public LineBorderDecorator(final LayerCellPainter interiorPainter) {
		this(interiorPainter, null);
	}
	
	public LineBorderDecorator(final LayerCellPainter interiorPainter, final BorderStyle defaultBorderStyle) {
		super(interiorPainter);
		this.defaultBorderStyle= defaultBorderStyle;
	}


	@Override
	public long getPreferredWidth(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		final BorderStyle borderStyle= getBorderStyle(cell, configRegistry);
		final long padding= borderStyle != null ? Math.max(borderStyle.getOffset() + borderStyle.getThickness(), 0) : 0;
		
		return super.getPreferredWidth(cell, gc, configRegistry) + (padding * 2);
	}
	
	@Override
	public long getPreferredHeight(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		final BorderStyle borderStyle= getBorderStyle(cell, configRegistry);
		final long padding= borderStyle != null ? Math.max(borderStyle.getOffset() + borderStyle.getThickness(), 0) : 0;
		
		return super.getPreferredHeight(cell, gc, configRegistry) + (padding * 2);
	}

	private BorderStyle getBorderStyle(final LayerCell cell, final ConfigRegistry configRegistry) {
		final Style cellStyle= CellStyleUtil.getCellStyle(cell, configRegistry);
		BorderStyle borderStyle= cellStyle.getAttributeValue(CellStyling.BORDER_STYLE);
		if (borderStyle == null) {
			borderStyle= this.defaultBorderStyle;
		}
		return borderStyle;
	}

	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle lRectangle, final ConfigRegistry configRegistry) {
		final BorderStyle borderStyle= getBorderStyle(cell, configRegistry);
		
		final long padding= borderStyle != null ? Math.max(borderStyle.getOffset() + borderStyle.getThickness(), 0) : 0;
		final LRectangle interiorBounds =
			new LRectangle(
					lRectangle.x + padding,
					lRectangle.y + padding,
					lRectangle.width - (padding * 2),
					lRectangle.height - (padding * 2)
			);
		super.paintCell(cell, gc, interiorBounds, configRegistry);
		
		if (borderStyle == null || borderStyle.getThickness() <= 0) {
			return;
		}
		
		// Save GC settings
		final Color originalForeground= gc.getForeground();
		final int originalLineWidth= gc.getLineWidth();
		final int originalLineStyle= gc.getLineStyle();
		
		final long borderOffset= borderStyle.getOffset();
		final int borderThickness= borderStyle.getThickness();
		final LRectangle borderArea;
		{	long shift= 0;
			long areaShift= 0;
			if ((borderThickness % 2) == 0) {
				shift= borderThickness / 2;
				areaShift= (shift * 2);
			} else {
				shift= borderThickness / 2;
				areaShift= (shift * 2) + 1;
			}
			borderArea= new LRectangle(
						lRectangle.x + borderOffset + shift,
						lRectangle.y + borderOffset + shift,
						lRectangle.width - (borderOffset * 2) - areaShift,
						lRectangle.height - (borderOffset * 2) - areaShift );
		}
		
		gc.setLineWidth(borderThickness);
		gc.setLineStyle(SwtUtils.toSWT(borderStyle.getLineStyle()));
		gc.setForeground(borderStyle.getColor());
		gc.drawRectangle(GraphicsUtils.safe(borderArea));

		// Restore GC settings
		gc.setForeground(originalForeground);
		gc.setLineWidth(originalLineWidth);
		gc.setLineStyle(originalLineStyle);
	}
	
}
