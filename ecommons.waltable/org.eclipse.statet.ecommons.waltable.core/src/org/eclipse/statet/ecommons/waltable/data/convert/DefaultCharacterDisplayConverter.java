/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.convert;

import org.eclipse.statet.ecommons.waltable.Messages;


/**
 * Converts the display value to a {@link Character} and vice versa.
 */
public class DefaultCharacterDisplayConverter extends DisplayConverter {

	@Override
	public Object canonicalToDisplayValue(final Object sourceValue) {
		return sourceValue != null ? sourceValue.toString() : ""; //$NON-NLS-1$
	}

	@Override
	public Object displayToCanonicalValue(final Object displayValue) {
		String s;
		if (displayValue != null
				&& (s= displayValue.toString()) != null && !s.isEmpty() ) {
			if (s.length() > 1) {
				throw new ConversionFailedException(Messages.getString("DefaultCharacterDisplayConverter.failure", //$NON-NLS-1$
						new Object[] {s}));
			} else {
				return s.charAt(0);
			}
		} 
		return null;
	}
}
