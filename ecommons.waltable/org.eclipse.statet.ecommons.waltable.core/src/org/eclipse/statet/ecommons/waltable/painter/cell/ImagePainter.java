/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.style.CellStyleUtil;


/**
 * Paints an image. If no image is provided, it will attempt to look up an image from the cell style.
 */
public class ImagePainter extends BackgroundPainter {

	private final Image image;
	private final boolean paintBg;

	public ImagePainter() {
		this(null);
	}

	public ImagePainter(final Image image) {
		this(image, true);
	}

	public ImagePainter(final Image image, final boolean paintBg) {
		this.image= image;
		this.paintBg= paintBg;
	}

	@Override
	public long getPreferredWidth(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		final Image image= getImage(cell, configRegistry);
		if (image != null) {
			return image.getBounds().width;
		} else {
			return 0;
		}
	}

	@Override
	public long getPreferredHeight(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		final Image image= getImage(cell, configRegistry);
		if (image != null) {
			return image.getBounds().height;
		} else {
			return 0;
		}
	}

	@Override
	public LayerCellPainter getCellPainterAt(final long x, final long y, final LayerCell cell, final GC gc, final LRectangle bounds, final ConfigRegistry configRegistry) {
		final Image image= getImage(cell, configRegistry);
		if (image != null) {
			final Rectangle imageBounds= image.getBounds();
			final Style cellStyle= CellStyleUtil.getCellStyle(cell, configRegistry);
			final long x0= bounds.x + CellStyleUtil.getHorizontalAlignmentPadding(cellStyle, bounds, imageBounds.width);
			final long y0= bounds.y + CellStyleUtil.getVerticalAlignmentPadding(cellStyle, bounds, imageBounds.height);
			if (	x >= x0 &&
					x < x0 + imageBounds.width &&
					y >= y0 &&
					y < y0 + imageBounds.height) {
				return super.getCellPainterAt(x, y, cell, gc, bounds, configRegistry);
			}
		}
		return null;
	}
	
	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle bounds, final ConfigRegistry configRegistry) {
		if (this.paintBg) {
			super.paintCell(cell, gc, bounds, configRegistry);
		}

		final Image image= getImage(cell, configRegistry);
		if (image != null) {
			final Rectangle imageBounds= image.getBounds();
			final Style cellStyle= CellStyleUtil.getCellStyle(cell, configRegistry);
			gc.drawImage(image,
					safe(bounds.x + CellStyleUtil.getHorizontalAlignmentPadding(cellStyle, bounds, imageBounds.width)),
					safe(bounds.y + CellStyleUtil.getVerticalAlignmentPadding(cellStyle, bounds, imageBounds.height)));
		}
	}
	
	protected Image getImage(final LayerCell cell, final ConfigRegistry configRegistry) {
		return this.image != null ? this.image : CellStyleUtil.getCellStyle(cell, configRegistry).getAttributeValue(CellStyling.IMAGE);
	}

}
