/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit.config;

import org.eclipse.statet.ecommons.waltable.config.AbstractLayerConfiguration;
import org.eclipse.statet.ecommons.waltable.config.IEditableRule;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.AbstractLayer;
import org.eclipse.statet.ecommons.waltable.data.validate.DefaultDataValidator;
import org.eclipse.statet.ecommons.waltable.edit.EditCellCommandHandler;
import org.eclipse.statet.ecommons.waltable.edit.EditConfigAttributes;
import org.eclipse.statet.ecommons.waltable.edit.InlineCellEditEventHandler;
import org.eclipse.statet.ecommons.waltable.edit.editor.TextCellEditor;


/**
 * Default configuration for edit behaviour in a NatTable.
 * Will register the {@link EditCellCommandHandler} and the {@link InlineCellEditEventHandler}
 * to the layer this configuration is added to. Usually this configuration is added to 
 * a GridLayer.
 * <p>
 * It also registers default values on top-level for the following {@link EditConfigAttributes}:
 * <ul>
 * <li>{@link EditConfigAttributes#CELL_EDITABLE_RULE} - IEditableRule.NEVER_EDITABLE<br>
 * by default a NatTable is not editable</li>
 * <li>{@link EditConfigAttributes} - {@link TextCellEditor}<br>
 * by default a TextCellEditor will be used for editing cells in a NatTable</li>
 * <li>{@link EditConfigAttributes} - {@link DefaultDataValidator}<br>
 * by default a validator is registered that always returns <code>true</code>, regardless of the 
 * entered value</li>
 * </ul>
 */
public class DefaultEditConfiguration extends AbstractLayerConfiguration<AbstractLayer> {

	@Override
	public void configureTypedLayer(final AbstractLayer layer) {
		layer.registerCommandHandler(new EditCellCommandHandler());
		layer.registerEventHandler(new InlineCellEditEventHandler(layer));
	}
	
	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		configRegistry.registerAttribute(EditConfigAttributes.CELL_EDITABLE_RULE, IEditableRule.NEVER_EDITABLE);
		configRegistry.registerAttribute(EditConfigAttributes.CELL_EDITOR, new TextCellEditor());
		configRegistry.registerAttribute(EditConfigAttributes.DATA_VALIDATOR, new DefaultDataValidator());
	}
	
}
