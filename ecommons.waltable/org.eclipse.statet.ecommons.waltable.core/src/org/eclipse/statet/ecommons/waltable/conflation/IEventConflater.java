/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.conflation;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.LayerEvent;


/**
 * A Conflater queues events and periodically runs a task to 
 * handle those Events. This prevents the table from
 * being overwhelmed by ultra fast updates.
 */
@NonNullByDefault
public interface IEventConflater {
	
	
	public abstract void addEvent(LayerEvent event);
	
	public abstract void clearQueue();
	
	
	/**
	 * @return Number of events currently waiting to be handled
	 */
	public abstract int getCount();
	
	public Runnable getConflaterTask();
	
}
