/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.graphics.Color;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.HorizontalAlignment;
import org.eclipse.statet.ecommons.waltable.core.style.RegistryStyle;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.core.style.VerticalAlignment;
import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;


public class CellStyleUtil {

	public static Style getCellStyle(final LayerCell cell, final ConfigRegistry configRegistry) {
		return new RegistryStyle(configRegistry, CellConfigAttributes.CELL_STYLE, cell.getDisplayMode(), cell.getLabels().getLabels());
	}

	public static int getHorizontalAlignmentSWT(final Style cellStyle, final int swtDefault) {
		final HorizontalAlignment horizontalAlignment= cellStyle.getAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT);
		return (horizontalAlignment != null) ? SwtUtils.toSWT(horizontalAlignment) : swtDefault;
	}

	public static long getHorizontalAlignmentPadding(final Style cellStyle, final LRectangle lRectangle, final long contentWidth) {
		final HorizontalAlignment horizontalAlignment= cellStyle.getAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT);
		return getHorizontalAlignmentPadding(horizontalAlignment, lRectangle.width, contentWidth);
	}
	
	/**
	 * Calculate padding needed at the left to align horizontally. Defaults to CENTER horizontal alignment.
	 */
	public static long getHorizontalAlignmentPadding(HorizontalAlignment horizontalAlignment, final long width, final long contentWidth) {
		if (horizontalAlignment == null) {
			horizontalAlignment= HorizontalAlignment.CENTER;
		}
		
		long padding;
		
		switch (horizontalAlignment) {
		case CENTER:
			padding= (width - contentWidth) / 2;
			break;
		case RIGHT:
			padding= width - contentWidth;
			break;
		default:
			padding= 0;
			break;
		}
		
		if (padding < 0) {
			padding= 0;
		}
		
		return padding;
	}

	public static long getVerticalAlignmentPadding(final Style cellStyle, final LRectangle lRectangle, final long contentHeight) {
		final VerticalAlignment verticalAlignment= cellStyle.getAttributeValue(CellStyling.VERTICAL_ALIGNMENT);
		return getVerticalAlignmentPadding(verticalAlignment, lRectangle.height, contentHeight);
	}
	
	/**
	 * Calculate padding needed at the top to align vertically. Defaults to MIDDLE vertical alignment.
	 */
	public static long getVerticalAlignmentPadding(VerticalAlignment verticalAlignment, final long height, final long contentHeight) {
		if (verticalAlignment == null) {
			verticalAlignment= VerticalAlignment.MIDDLE;
		}
		
		long padding= 0;

		switch (verticalAlignment) {
		case MIDDLE:
			padding= (height - contentHeight) / 2;
			break;
		case BOTTOM:
			padding= height - contentHeight;
			break;
		}
		
		if (padding < 0) {
			padding= 0;
		}

		return padding;
	}
	
	public static List<Color> getAllBackgroundColors(final LayerCell cell, final ConfigRegistry configRegistry,
			final DisplayMode displayMode) {
		
		final List<Color> colors= new ArrayList<>();
		
		for (final String configLabel : cell.getLabels().getLabels()) {
			final Style cellStyle= configRegistry.getSpecificAttribute(CellConfigAttributes.CELL_STYLE, displayMode, configLabel);
			if (cellStyle != null) {
				final Color color= cellStyle.getAttributeValue(CellStyling.BACKGROUND_COLOR);
				
				if (color != null) {
					colors.add(color);
				}
			}
		}
		
		return colors;
	}
	
}
