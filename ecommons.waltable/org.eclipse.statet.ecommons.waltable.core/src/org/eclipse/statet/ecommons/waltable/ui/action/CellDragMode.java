/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.action;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.layer.top.OverlayPainter;


public class CellDragMode implements IDragMode {
	
	private MouseEvent initialEvent;
	private MouseEvent currentEvent;
	
	private int xOffset;
	private int yOffset;
	private Image cellImage;
	protected CellImageOverlayPainter cellImageOverlayPainter= new CellImageOverlayPainter();
	
	@Override
	public void mouseDown(final NatTable natTable, final MouseEvent event) {
		this.initialEvent= event;
		this.currentEvent= this.initialEvent;
		
		setCellImage(natTable);
		
		natTable.forceFocus();

		natTable.addOverlayPainter(this.cellImageOverlayPainter);
	}

	@Override
	public void mouseMove(final NatTable natTable, final MouseEvent event) {
		this.currentEvent= event;
		
		natTable.redraw();
	}

	@Override
	public void mouseUp(final NatTable natTable, final MouseEvent event) {
		natTable.removeOverlayPainter(this.cellImageOverlayPainter);
		this.cellImage.dispose();
		
		natTable.redraw();
	}
	
	protected MouseEvent getInitialEvent() {
		return this.initialEvent;
	}
	
	protected MouseEvent getCurrentEvent() {
		return this.currentEvent;
	}
	
	private void setCellImage(final NatTable natTable) {
		final long columnPosition= natTable.getColumnPositionByX(this.currentEvent.x);
		final long rowPosition= natTable.getRowPositionByY(this.currentEvent.y);
		final LayerCell cell= natTable.getCellByPosition(columnPosition, rowPosition);
		
		final LRectangle cellBounds= cell.getBounds();
		final int width= (int) Math.min(cellBounds.width, 0x1fff);
		final int height= (int) Math.min(cellBounds.height, 0x1fff);
		this.xOffset= (int) Math.max(this.currentEvent.x - cellBounds.x, 0);
		this.yOffset= (int) Math.max(this.currentEvent.y - cellBounds.y, 0);
		
		final ImageData imageData;
		final Image image= new Image(natTable.getDisplay(), width, height);
		try {
			final GC gc= new GC(image);
			try {
				final ConfigRegistry configRegistry= natTable.getConfigRegistry();
				final LayerCellPainter layerCellPainter= configRegistry.getAttribute(CellConfigAttributes.CELL_PAINTER,
						cell.getDisplayMode(), cell.getLabels().getLabels() );
				if (layerCellPainter != null) {
					layerCellPainter.paintCell(cell, gc, new LRectangle(0, 0, width, height), configRegistry);
				}
			}
			finally {
				gc.dispose();
			}
			
			imageData= image.getImageData();
			imageData.alpha= 150;
		}
		finally {
			image.dispose();
		}
		
		this.cellImage= new Image(natTable.getDisplay(), imageData);
	}

	private class CellImageOverlayPainter implements OverlayPainter {

		@Override
		public void paintOverlay(final GC gc, final Layer layer) {
			if (CellDragMode.this.cellImage != null & !CellDragMode.this.cellImage.isDisposed()) {
				gc.drawImage(CellDragMode.this.cellImage, CellDragMode.this.currentEvent.x - CellDragMode.this.xOffset, CellDragMode.this.currentEvent.y - CellDragMode.this.yOffset);
			}
		}

	}

}
