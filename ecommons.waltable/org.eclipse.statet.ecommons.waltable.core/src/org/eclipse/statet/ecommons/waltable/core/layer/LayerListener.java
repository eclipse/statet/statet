/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Object interested in receiving events related to a {@link Layer}.
 */
@NonNullByDefault
public interface LayerListener {
	
	
	/**
	 * Handle an event notification from an {@link Layer}
	 * @param event the event
	 */
	void handleLayerEvent(LayerEvent event);
	
}
