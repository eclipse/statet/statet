/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.coordinate;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public enum Direction {
	
	
	LEFT {
		
		@Override
		public Orientation getOrientation() {
			return HORIZONTAL;
		}
		
		@Override
		public boolean isForward() {
			return false;
		}
		
		@Override
		public boolean isBackward() {
			return true;
		}
		
		@Override
		public Direction getOpposite() {
			return RIGHT;
		}
		
	},
	
	RIGHT {
		
		@Override
		public Orientation getOrientation() {
			return HORIZONTAL;
		}
		
		@Override
		public boolean isForward() {
			return true;
		}
		
		@Override
		public boolean isBackward() {
			return false;
		}
		
		@Override
		public Direction getOpposite() {
			return LEFT;
		}
		
	},
	
	UP {
		
		@Override
		public Orientation getOrientation() {
			return VERTICAL;
		}
		
		@Override
		public boolean isForward() {
			return false;
		}
		
		@Override
		public boolean isBackward() {
			return true;
		}
		
		@Override
		public Direction getOpposite() {
			return DOWN;
		}
		
	},
	
	DOWN {
		
		@Override
		public Orientation getOrientation() {
			return VERTICAL;
		}
		
		@Override
		public boolean isForward() {
			return true;
		}
		
		@Override
		public boolean isBackward() {
			return false;
		}
		
		@Override
		public Direction getOpposite() {
			return UP;
		}
		
	};
	
	
	public static Direction forward(final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return RIGHT;
		case VERTICAL:
			return LEFT;
		default:
			throw new IllegalStateException();
		}
	}
	
	public static Direction backward(final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return LEFT;
		case VERTICAL:
			return UP;
		default:
			throw new IllegalStateException();
		}
	}
	
	
	public abstract Orientation getOrientation();
	
	public abstract boolean isForward();
	
	public abstract boolean isBackward();
	
	public abstract Direction getOpposite();
	
}
