/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.layer.cell;

import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;


/**
 * Adds the Java class name of the cell's data value as a label.   
 */
public class ClassNameConfigLabelAccumulator implements CellLabelContributor {
	
	
	private final DataProvider dataProvider;
	
	
	public ClassNameConfigLabelAccumulator(final DataProvider dataProvider) {
		this.dataProvider= dataProvider;
	}
	
	@Override
	public void addLabels(final LabelStack configLabel, final long columnPosition, final long rowPosition) {
		final Object value= this.dataProvider.getDataValue(columnPosition, rowPosition, 0, null);
		if (value != null) {
			configLabel.addLabel(value.getClass().getName());
		}
	}
	
}
