/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Abstract base class for layers that expose transformed views of an underlying layer.
 * 
 * By default the layer behaves as an identity transform of its underlying layer; that is, it
 * exposes its underlying layer as is without any changes.  Subclasses are expected to override
 * methods in this class to implement specific kinds of layer transformations.
 * 
 * The layer is similar to {@link AbstractLayerTransform}, but is {@link DimBasedLayer dim-based}.
 */
@NonNullByDefault
public abstract class TransformLayer<TLayerDim extends ForwardLayerDim<? extends TransformLayer<TLayerDim>>>
		extends ForwardLayer<TLayerDim> {
	
	
	public TransformLayer(final Layer underlyingLayer,
			final @Nullable LayerPainter layerPainter) {
		super(underlyingLayer, layerPainter);
	}
	
	public TransformLayer(final Layer underlyingLayer) {
		this(underlyingLayer, null);
	}
	
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final LayerCell underlyingCell= getUnderlyingLayer().getCellByPosition(
				getDim(HORIZONTAL).localToUnderlyingPosition(columnPosition, columnPosition),
				getDim(VERTICAL).localToUnderlyingPosition(rowPosition, rowPosition) );
		
		return createCell(
				transformCellDim(underlyingCell.getDim(HORIZONTAL), columnPosition),
				transformCellDim(underlyingCell.getDim(VERTICAL), rowPosition),
				underlyingCell );
	}
	
	protected LayerCellDim transformCellDim(final LayerCellDim underlyingDim, final long position) {
		if (underlyingDim.getPosition() == position) {
			return underlyingDim;
		}
		
		final long originPosition= position -
				(underlyingDim.getPosition() - underlyingDim.getOriginPosition());
		return new BasicLayerCellDim(underlyingDim.getOrientation(), underlyingDim.getId(),
				position, originPosition, underlyingDim.getPositionSpan() );
	}
	
}
