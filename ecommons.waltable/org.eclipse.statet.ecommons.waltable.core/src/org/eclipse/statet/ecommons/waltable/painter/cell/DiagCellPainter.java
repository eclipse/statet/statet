/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


/**
 * Cell painter painting a diagonal line from the top left to the bottom right corner
 */
public class DiagCellPainter extends BackgroundPainter {
	
	
	private final Color color;
	
	
	public DiagCellPainter(final Color color) {
		this.color= color;
	}
	
	
	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle bounds, final ConfigRegistry configRegistry) {
		super.paintCell(cell, gc, bounds, configRegistry);
		
		gc.setForeground(this.color);
		gc.setAntialias(SWT.ON);
		gc.drawLine(safe(bounds.x), safe(bounds.y), safe(bounds.x + bounds.width - 1), safe(bounds.y + bounds.height - 1));
		gc.setAntialias(GUIHelper.DEFAULT_ANTIALIAS);
	}
	
}
