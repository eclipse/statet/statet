/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;


/**
 * Classes implementing this interface are responsible for painting to the relevant {@link Device}.
 * Every layer has a layer painter. A layer can contribute to painting by providing its own painter.
 */
@NonNullByDefault
public interface LayerPainter {
	
	
	/**
	 * This method is used to adjust the cell bounds when painting the layer. This is most often used to reduce the size
	 * of the cell to accommodate grid lines. 
	 */
	LRectangle adjustCellBounds(long columnPosition, long rowPosition, LRectangle cellBounds);
	
	/**
	 * @param natLayer
	 * @param gc GC used for painting
	 * @param xOffset of the layer from the origin of the table
	 * @param yOffset of the layer from the origin of the table
	 * @param rectangle area the layer can paint in
	 * @param configuration in use by NatTable. Useful for looking up associated painters.
	 */
	void paintLayer(Layer natLayer, GC gc, int xOffset, int yOffset,
			Rectangle rectangle, ConfigRegistry configuration);
	
}
