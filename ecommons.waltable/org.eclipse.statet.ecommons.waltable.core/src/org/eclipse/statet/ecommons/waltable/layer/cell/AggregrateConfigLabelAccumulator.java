/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.layer.cell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;


/**
 * An {@link CellLabelContributor} that can aggregate labels from other <code>IConfigLabelAccumulator</code>s. 
 * All the labels provided by the aggregated accumulators are applied to the cell.
 */
public class AggregrateConfigLabelAccumulator implements CellLabelContributor {
    
    private final List<CellLabelContributor> accumulators= new ArrayList<>();
    
    public void add(final CellLabelContributor r) {
        if (r == null)
		 {
			throw new IllegalArgumentException("null"); //$NON-NLS-1$
		}
        this.accumulators.add(r);
    }

    public void add(final CellLabelContributor... r) {
    	if (r == null)
		 {
			throw new IllegalArgumentException("null"); //$NON-NLS-1$
		}
    	this.accumulators.addAll(Arrays.asList(r));
    }

    @Override
	public void addLabels(final LabelStack configLabels, final long columnPosition, final long rowPosition) {
        for (final CellLabelContributor accumulator : this.accumulators) {
        	accumulator.addLabels(configLabels, columnPosition, rowPosition);
        }
    }

}
