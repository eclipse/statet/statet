/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;


/**
 * Specifies the rectangular area available to an {@link Layer}
 * Note: All layers get the client area from {@link NatTable} which implements this interface.
 * 
 * @see Layer#getClientAreaProvider()
 */
@NonNullByDefault
public interface ClientAreaProvider {
	
	
	static final ClientAreaProvider DEFAULT= new ClientAreaProvider() {
		@Override
		public LRectangle getClientArea() {
			return new LRectangle(0, 0, 0, 0);
		}
	};
	
	
	LRectangle getClientArea();
	
}
