/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.style;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;


/**
 * A style backed by a registry.
 */
@NonNullByDefault
public class RegistryStyle implements Style {
	
	
	private final ConfigRegistry configRegistry;
	
	private final ConfigAttribute<Style> styleConfigAttribute;
	private final DisplayMode targetDisplayMode;
	private final List<String> configLabels;
	
	
	public RegistryStyle(final ConfigRegistry configRegistry,
			final ConfigAttribute<Style> styleConfigAttribute,
			final DisplayMode targetDisplayMode, final List<String> configLabels) {
		this.configRegistry= configRegistry;
		this.styleConfigAttribute= styleConfigAttribute;
		this.targetDisplayMode= targetDisplayMode;
		this.configLabels= configLabels;
	}
	
	
	@Override
	public <T> @Nullable T getAttributeValue(final ConfigAttribute<T> configAttribute) {
		@Nullable T configAttributeValue= null;
		for (final DisplayMode displayMode : this.configRegistry.getDisplayModeOrdering()
				.getDisplayModeOrdering(this.targetDisplayMode) ) {
			for (final String configLabel : this.configLabels) {
				final Style style= this.configRegistry
						.getSpecificAttribute(this.styleConfigAttribute, displayMode, configLabel);
				if (style != null) {
					configAttributeValue= style.getAttributeValue(configAttribute);
					if (configAttributeValue != null) {
						return configAttributeValue;
					}
				}
			}
			
			// default
			final Style cellStyle= this.configRegistry
					.getSpecificAttribute(this.styleConfigAttribute, displayMode, null);
			if (cellStyle != null) {
				configAttributeValue= cellStyle.getAttributeValue(configAttribute);
				if (configAttributeValue != null) {
					return configAttributeValue;
				}
			}
		}
		
		return null;
	}
	
	@Override
	public <T> void setAttributeValue(final ConfigAttribute<T> configAttribute, final @Nullable T value) {
		throw new UnsupportedOperationException();
	}
	
}
