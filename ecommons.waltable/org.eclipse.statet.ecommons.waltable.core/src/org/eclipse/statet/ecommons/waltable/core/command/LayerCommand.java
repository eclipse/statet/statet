/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.command;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


/**
 * A command triggers a specific operation on a layer or behaviour of the table.
 * 
 * Commands flow down the layer stack until they are processed by a {@link Layer}
 * or an associated {@link LayerCommandHandler}.
 * Commands can be fired from code by invoking {@link NatTable#doCommand(LayerCommand)}.
 */
@NonNullByDefault
public interface LayerCommand {
	
	
	/**
	 * Convert the row/column coordinates the command might be carrying from the source layer
	 * to the destination (target) layer. If it is not possible to convert the command to the target layer,
	 * then this method will return false and <b>the state of this command object will remain unchanged</b>.
	 * Note: Commands should not be processed if they fail conversion.
	 * 
	 * @param targetLayer the target layer
	 * @return true if the command is valid after conversion, false if the command is no longer valid.
	 */
	boolean convertToTargetLayer(Layer targetLayer);
	
	/**
	 * Same semantics as {@link Object#clone()}
	 * Used to make a copies of the command if has to passed to different layer stacks.
	 * 
	 * @return a cloned instance of the command
	 */
	LayerCommand cloneCommand();
	
}
