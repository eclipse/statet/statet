/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.events;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


/**
 * General event indicating that structures cached by the layers need refreshing.
 * <p>
 * TIP: Consider throwing a more focused event (subclass) if you need to do this.
 */
@NonNullByDefault
public class GeneralStructuralChangeEvent extends GeneralVisualChangeEvent implements StructuralChangeEvent {
	
	
	public GeneralStructuralChangeEvent(final Layer layer) {
		super(layer);
	}
	
	@Override
	public @Nullable GeneralStructuralChangeEvent toLayer(final Layer targetLayer) {
		return new GeneralStructuralChangeEvent(targetLayer);
	}
	
	
	@Override
	public boolean isStructureChanged(final Orientation orientation) {
		return true;
	}
	
	@Override
	public @Nullable List<StructuralDiff> getDiffs(final Orientation orientation) {
		return null;
	}
	
	
}
