/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.style;

import java.util.function.Function;

import org.eclipse.swt.graphics.Color;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


@NonNullByDefault
public class GridStyling {
	
	
	public static final ConfigAttribute<Color> GRID_LINE_COLOR= new ConfigAttribute<>();
	
	
	public static class ConfigGridLineColorSupplier implements Function<ConfigRegistry, Color> {
		
		
		private final ImIdentityList<String> configLabels;
		
		
		public ConfigGridLineColorSupplier(final ImIdentityList<String> configLabels) {
			this.configLabels= configLabels;
		}
		
		public ConfigGridLineColorSupplier(final String configLabel) {
			this(ImCollections.newIdentityList(configLabel));
		}
		
		public ConfigGridLineColorSupplier() {
			this(ImCollections.emptyIdentityList());
		}
		
		
		@Override
		public Color apply(final ConfigRegistry configRegistry) {
			final var value= configRegistry.getAttribute(GridStyling.GRID_LINE_COLOR,
					DisplayMode.NORMAL, this.configLabels );
			if (value != null) {
				return value;
			}
			return GUIHelper.COLOR_GRAY;
		}
		
	}
	
	private GridStyling() {
	}
	
}
