/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit.action;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.edit.EditCellCommand;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseClickAction;


/**
 * Action that will execute an {@link EditCellCommand}.
 * It determines the cell to edit by mouse pointer coordinates
 * instead of using a SelectionLayer. So this action is also
 * working in NatTables that doesn't have a SelectionLayer in
 * its composition of layers.
 */
public class MouseEditAction implements IMouseClickAction {
	
	
	public MouseEditAction() {
	}
	
	
	@Override
	public void run(final NatTable natTable, final MouseEvent event) {
		final long columnPosition= natTable.getColumnPositionByX(event.x);
		final long rowPosition= natTable.getRowPositionByY(event.y);
		
		if (columnPosition < 0 || rowPosition < 0) {
			return;
		}
		
		natTable.doCommand(new EditCellCommand(natTable,
				natTable.getConfigRegistry(),
				natTable.getCellByPosition(columnPosition, rowPosition) ));
	}
	
	@Override
	public boolean isExclusive() {
		return true;
	}
	
}
