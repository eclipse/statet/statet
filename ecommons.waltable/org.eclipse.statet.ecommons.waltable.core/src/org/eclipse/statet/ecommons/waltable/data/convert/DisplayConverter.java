/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.convert;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;


public abstract class DisplayConverter implements IDisplayConverter {

	public abstract Object canonicalToDisplayValue(Object canonicalValue);

	public abstract Object displayToCanonicalValue(Object displayValue);
	
	@Override
	public Object canonicalToDisplayValue(final LayerCell cell, final ConfigRegistry configRegistry, final Object canonicalValue) {
		return canonicalToDisplayValue(canonicalValue);
	}

	@Override
	public Object displayToCanonicalValue(final LayerCell cell, final ConfigRegistry configRegistry, final Object displayValue) {
		return displayToCanonicalValue(displayValue);
	}

}
