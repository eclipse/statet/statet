/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.action;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.ui.mode.AbstractModeEventHandler;
import org.eclipse.statet.ecommons.waltable.ui.mode.Mode;
import org.eclipse.statet.ecommons.waltable.ui.mode.ModeSupport;


public class DragModeEventHandler extends AbstractModeEventHandler {

	private final NatTable natTable;
	
	private final IDragMode dragMode;
	
	public DragModeEventHandler(final ModeSupport modeSupport, final NatTable natTable, final IDragMode dragMode) {
		super(modeSupport);
		
		this.natTable= natTable;
		this.dragMode= dragMode;
	}
	
	@Override
	public void mouseMove(final MouseEvent event) {
		this.dragMode.mouseMove(this.natTable, event);
	}
	
	@Override
	public void mouseUp(final MouseEvent event) {
		this.dragMode.mouseUp(this.natTable, event);
		switchMode(Mode.NORMAL_MODE);
	}
	
}
