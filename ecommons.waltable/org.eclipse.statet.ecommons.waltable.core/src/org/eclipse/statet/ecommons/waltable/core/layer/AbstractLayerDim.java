/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;


/**
 * Abstract implementation of layer dimension.
 * 
 * @param <TLayer> the type of the layer
 */
@NonNullByDefault
public abstract class AbstractLayerDim<TLayer extends Layer> implements LayerDim {
	
	
	protected final TLayer layer;
	
	protected final Orientation orientation;
	
	
	public AbstractLayerDim(final TLayer layer, final Orientation orientation) {
		this.layer= nonNullAssert(layer);
		this.orientation= nonNullAssert(orientation);
	}
	
	
	@Override
	public Layer getLayer() {
		return this.layer;
	}
	
	@Override
	public final Orientation getOrientation() {
		return this.orientation;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder("LayerDim"); //$NON-NLS-1$
		sb.append(" ").append(this.orientation); //$NON-NLS-1$
		sb.append(" of \n").append(this.layer); //$NON-NLS-1$
		return sb.toString();
	}
	
}
