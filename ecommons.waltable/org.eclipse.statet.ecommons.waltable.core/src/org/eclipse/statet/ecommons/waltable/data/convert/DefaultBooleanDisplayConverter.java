/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.convert;


/**
 * Data type converter for a Check Box. 
 * Assumes that the data value is stored as a boolean.
 */
public class DefaultBooleanDisplayConverter extends DisplayConverter {

	@Override
	public Object displayToCanonicalValue(final Object displayValue) {
		return Boolean.valueOf(displayValue.toString());
	}

	@Override
	public Object canonicalToDisplayValue(final Object canonicalValue) {
		if (canonicalValue == null) {
			return null;
		} else {
			return canonicalValue.toString();
		}
	}

}
