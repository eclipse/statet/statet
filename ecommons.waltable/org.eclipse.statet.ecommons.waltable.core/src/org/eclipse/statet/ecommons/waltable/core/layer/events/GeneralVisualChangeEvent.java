/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.events;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


@NonNullByDefault
public class GeneralVisualChangeEvent implements VisualChangeEvent {
	
	
	private final Layer layer;
	
	
	public GeneralVisualChangeEvent(final Layer layer) {
		this.layer= layer;
	}
	
	@Override
	public @Nullable GeneralVisualChangeEvent toLayer(final Layer targetLayer) {
		if (targetLayer == this.layer) {
			return this;
		}
		
		return new GeneralVisualChangeEvent(targetLayer);
	}
	
	
	@Override
	public Layer getLayer() {
		return this.layer;
	}
	
	
	@Override
	public ImList<LRectangle> getChangedPositionRectangles() {
		return ImCollections.newList(
				new LRectangle(0, 0, this.layer.getColumnCount(), this.layer.getRowCount()) );
	}
	
}
