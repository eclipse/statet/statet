/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.layer.config;

import org.eclipse.statet.ecommons.waltable.config.AggregateConfiguration;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.RowHeaderLayer;
import org.eclipse.statet.ecommons.waltable.resize.ui.action.DefaultRowResizeBindings;


/**
 * Default setup for the Row header area. Added by the {@link RowHeaderLayer}
 * Override the methods in this class to customize style / UI bindings.
 * 
 * @see GridLabels
 */
public class DefaultRowHeaderLayerConfiguration extends AggregateConfiguration {

	public DefaultRowHeaderLayerConfiguration() {
		addRowHeaderStyleConfig();
		addRowHeaderUIBindings();
	}

	protected void addRowHeaderStyleConfig() {
		addConfiguration(new DefaultRowHeaderStyleConfiguration());
	}

	protected void addRowHeaderUIBindings() {
		addConfiguration(new DefaultRowResizeBindings());
	}

}
