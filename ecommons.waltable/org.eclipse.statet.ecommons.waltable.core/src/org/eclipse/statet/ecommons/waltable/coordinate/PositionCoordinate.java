/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.coordinate;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


@NonNullByDefault
public final class PositionCoordinate {
	
	
	private final Layer layer;
	
	public long columnPosition;
	public long rowPosition;
	
	
	public PositionCoordinate(final Layer layer, final long columnPosition, final long rowPosition) {
		this.layer= layer;
		this.columnPosition= columnPosition;
		this.rowPosition= rowPosition;
	}
	
	public PositionCoordinate(final PositionCoordinate coordinate) {
		this.layer= coordinate.layer;
		this.columnPosition= coordinate.columnPosition;
		this.rowPosition= coordinate.rowPosition;
	}
	
	
	public Layer getLayer() {
		return this.layer;
	}
	
	public long getColumnPosition() {
		return this.columnPosition;
	}
	
	public long getRowPosition() {
		return this.rowPosition;
	}
	
	public void set(final long rowPosition, final long columnPosition) {
		this.rowPosition= rowPosition;
		this.columnPosition= columnPosition;
	}
	
	@Override
	public int hashCode() {
		int h= (int) (this.columnPosition ^ (this.columnPosition >>> 32));
		h= Integer.rotateRight(h, 15);
		h ^= (int) (this.rowPosition ^ (this.rowPosition));
		return this.layer.hashCode() + (h ^ (h >>> 7));
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final PositionCoordinate other
						&& this.layer == other.layer
						&& this.columnPosition == other.columnPosition && this.rowPosition == other.rowPosition ));
	}
	
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + this.layer + ":" + this.columnPosition + "," + this.rowPosition + "]";
	}
	
}
