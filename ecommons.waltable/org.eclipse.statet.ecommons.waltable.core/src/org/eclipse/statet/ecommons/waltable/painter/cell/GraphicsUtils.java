/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;


/**
 * This class contains utility methods for drawing graphics
 * 
 * @see <a href="http://java-gui.info/Apress-The.Definitive.Guide.to.SWT.and.JFace/8886final/LiB0095.html">GC snippets</a>
 */
public class GraphicsUtils {
	
	
	public static final int safe(final long pixel) {
		return (pixel <= Integer.MIN_VALUE) ? Integer.MIN_VALUE :
			((pixel >= Integer.MAX_VALUE) ? Integer.MAX_VALUE : (int) pixel);
	}
	
	public static final Rectangle safe(final long x, final long y, final long width, final long height) {
		final int sx= safe(x);
		final int sy= safe(y);
		return new Rectangle(sx, sy, safe(x + width) - sx, safe(y + height) - sy);
	}
	
	public static final Rectangle safe(final LRectangle rect) {
		return safe(rect.x, rect.y, rect.width, rect.height);
	}
	
  /**
   * Draws text vertically (rotates plus or minus 90 degrees). Uses the current
   * font, color, and background.
   * <dl>
   * <dt><b>Styles: </b></dt>
   * <dd>UP, DOWN</dd>
   * </dl>
   *
   * @param string the text to draw
   * @param x the x coordinate of the top left corner of the drawing rectangle
   * @param y the y coordinate of the top left corner of the drawing rectangle
   * @param gc the GC on which to draw the text
   * @param style the style (SWT.UP or SWT.DOWN)
   *           <p>
   *           Note: Only one of the style UP or DOWN may be specified.
   *           </p>
   */
  public static void drawVerticalText(final String string, final int x, final int y, final GC gc, final int style) {
	  drawVerticalText(string, x, y, false, false, true, gc, style);
  }
	  
  /**
   * Draws text vertically (rotates plus or minus 90 degrees). Uses the current
   * font, color, and background.
   * <dl>
   * <dt><b>Styles: </b></dt>
   * <dd>UP, DOWN</dd>
   * </dl>
   *
   * @param string the text to draw
   * @param x the x coordinate of the top left corner of the drawing rectangle
   * @param y the y coordinate of the top left corner of the drawing rectangle
   * @param underline set to <code>true</code> to render the text underlined
   * @param strikethrough set to <code>true</code> to render the text strikethrough
   * @param paintBackground set to <code>false</code> to render the background transparent.
   * 			Needed for example to render the background with an image or gradient with another painter
   * 			so the text drawn here should have no background.
   * @param gc the GC on which to draw the text
   * @param style the style (SWT.UP or SWT.DOWN)
   *           <p>
   *           Note: Only one of the style UP or DOWN may be specified.
   *           </p>
   */
	public static void drawVerticalText(final String string, final int x, final int y, 
			final boolean underline, final boolean strikethrough, final boolean paintBackground, 
			final GC gc, final int style) {
		// Get the current display
		final Display display= Display.getCurrent();
		if (display == null) {
			SWT.error(SWT.ERROR_THREAD_INVALID_ACCESS);
		}
		
		// Determine string's dimensions
		final Point pt= gc.textExtent(string.trim());
		
		final Image stringImage= new Image(display, pt.x, pt.y);
		try {
			final GC gc0= new GC(stringImage);
			try {
				// Set attributes from the original GC to the new GC
				gc0.setAntialias(gc.getAntialias());
				gc0.setTextAntialias(gc.getTextAntialias());
				gc0.setForeground(gc.getForeground());
				gc0.setBackground(gc.getBackground());
				gc0.setFont(gc.getFont());
				
				// Fill the image with the specified background color
				// to avoid white spaces if the text does not fill the 
				// whole image (e.g. on new lines)
				gc0.fillRectangle(0, 0, pt.x, pt.y);
				
				// Draw the text onto the image
				gc0.drawText(string, 0, 0);
				
				//draw underline and/or strikethrough
				if (underline || strikethrough) {
					//check and draw underline and strikethrough separately so it is possible to combine both
					if (underline) {
						//y= start y of text + font height 
						// - half of the font descent so the underline is between the baseline and the bottom
						final int underlineY= pt.y - (gc0.getFontMetrics().getDescent() / 2);
						gc0.drawLine(
								0, 
								underlineY, 
								pt.x, 
								underlineY);
					}
					
					if (strikethrough) {
						//y= start y of text + half of font height + ascent so lower case characters are
						//also strikethrough
						final int strikeY= (pt.y / 2) + (gc0.getFontMetrics().getLeading() / 2);
						gc0.drawLine(
								0, 
								strikeY, 
								pt.x, 
								strikeY);
					}
				}
			}
			finally {
				gc0.dispose();
			}
			
			// Draw the image vertically onto the original GC
			drawVerticalImage(stringImage, x, y, paintBackground, gc, style);
		}
		finally {
			stringImage.dispose();
		}
	}
	
  /**
   * Draws an image vertically (rotates plus or minus 90 degrees)
   * <dl>
   * <dt><b>Styles: </b></dt>
   * <dd>UP, DOWN</dd>
   * </dl>
   *
   * @param image the image to draw
   * @param x the x coordinate of the top left corner of the drawing rectangle
   * @param y the y coordinate of the top left corner of the drawing rectangle
   * @param gc the GC on which to draw the image
   * @param style the style (SWT.UP or SWT.DOWN)
   *           <p>
   *           Note: Only one of the style UP or DOWN may be specified.
   *           </p>
   */
  public static void drawVerticalImage(final Image image, final int x, final int y, final GC gc, final int style) {
	  drawVerticalImage(image, x, y, true, gc, style);
  }

  /**
   * Draws an image vertically (rotates plus or minus 90 degrees)
   * <dl>
   * <dt><b>Styles: </b></dt>
   * <dd>UP, DOWN</dd>
   * </dl>
   *
   * @param image the image to draw
   * @param x the x coordinate of the top left corner of the drawing rectangle
   * @param y the y coordinate of the top left corner of the drawing rectangle
   * @param paintBackground set to <code>false</code> to render the background transparent.
   * 			Needed for example to render the background with an image or gradient with another painter
   * 			so the text drawn here should have no background.
   * @param gc the GC on which to draw the image
   * @param style the style (SWT.UP or SWT.DOWN)
   *           <p>
   *           Note: Only one of the style UP or DOWN may be specified.
   *           </p>
   */
  public static void drawVerticalImage(final Image image, final int x, final int y, final boolean paintBackground, final GC gc, final int style) {
    // Get the current display
    final Display display= Display.getCurrent();
    if (display == null) {
		SWT.error(SWT.ERROR_THREAD_INVALID_ACCESS);
	}

    // Use the image's data to create a rotated image's data
    final ImageData sd= image.getImageData();
    final ImageData dd= new ImageData(sd.height, sd.width, sd.depth, sd.palette);
    dd.transparentPixel= sd.transparentPixel;
    
    //set the defined backgroundcolor to be transparent
    if (!paintBackground) {
    	dd.transparentPixel= sd.palette.getPixel(gc.getBackground().getRGB());
    }

    // Determine which way to rotate, depending on up or down
    final boolean up= (style & SWT.UP) == SWT.UP;

    // Run through the horizontal pixels
    for (int sx= 0; sx < sd.width; sx++) {
      // Run through the vertical pixels
      for (int sy= 0; sy < sd.height; sy++) {
        // Determine where to move pixel to in destination image data
        final int dx= up ? sy : sd.height - sy - 1;
        final int dy= up ? sd.width - sx - 1 : sx;
        // Swap the x, y source data to y, x in the destination
        dd.setPixel(dx, dy, sd.getPixel(sx, sy));
      }
    }

    // Create the vertical image
    final Image vertical= new Image(display, dd);

    // Draw the vertical image onto the original GC
    gc.drawImage(vertical, x, y);

    // Dispose the vertical image
    vertical.dispose();
  }

	/**
	 * Creates an image containing the specified text, rotated either plus or minus
	 * 90 degrees.
	 * <dl>
	 * <dt><b>Styles: </b></dt>
	 * <dd>UP, DOWN</dd>
	 * </dl>
	 *
	 * @param text the text to rotate
	 * @param font the font to use
	 * @param foreground the color for the text
	 * @param background the background color
	 * @param style direction to rotate (up or down)
	 * @return Image
	 */
	public static Image createRotatedText(final String text,
			final Font font, final Color foreground, final Color background, final int style) {
		// Get the current display
		final Display display= Display.getCurrent();
		if (display == null) {
			SWT.error(SWT.ERROR_THREAD_INVALID_ACCESS);
		}
		
		// Calculate dimension
		final Point size;
		{	final GC gc= new GC(display);
			try {
				gc.setFont(font);
				
				// Determine string's dimensions
				size= gc.textExtent(text);
			}
			finally {
				gc.dispose();
			}
		}
		
		final Image stringImage= new Image(display, size.x, size.y);
		try {
			final GC gc0= new GC(stringImage);
			try {
				gc0.setFont(font);
				gc0.setForeground(foreground);
				gc0.setBackground(background);
				
				gc0.drawText(text, 0, 0);
			}
			finally {
				gc0.dispose();
			}
			
			// Draw the image vertically onto the original GC
			return createRotatedImage(stringImage, style);
		}
		finally {
			stringImage.dispose();
		}
	}
	
  /**
   * Creates a rotated image (plus or minus 90 degrees)
   * <dl>
   * <dt><b>Styles: </b></dt>
   * <dd>UP, DOWN</dd>
   * </dl>
   *
   * @param image the image to rotate
   * @param style direction to rotate (up or down)
   * @return Image
   *          <p>
   *          Note: Only one of the style UP or DOWN may be specified.
   *          </p>
   */
  public static Image createRotatedImage(final Image image, final int style) {
    // Get the current display
    final Display display= Display.getCurrent();
    if (display == null) {
		SWT.error(SWT.ERROR_THREAD_INVALID_ACCESS);
	}

    // Use the image's data to create a rotated image's data
    final ImageData sd= image.getImageData();
    final ImageData dd= new ImageData(sd.height, sd.width, sd.depth, sd.palette);

    // Determine which way to rotate, depending on up or down
    final boolean up= (style & SWT.UP) == SWT.UP;

    // Run through the horizontal pixels
    for (int sx= 0; sx < sd.width; sx++) {
      // Run through the vertical pixels
      for (int sy= 0; sy < sd.height; sy++) {
        // Determine where to move pixel to in destination image data
        final int dx= up ? sy : sd.height - sy - 1;
        final int dy= up ? sd.width - sx - 1 : sx;

        // Swap the x, y source data to y, x in the destination
        dd.setPixel(dx, dy, sd.getPixel(sx, sy));
      }
    }

    // Create the vertical image
    return new Image(display, dd);
  }
  
}

