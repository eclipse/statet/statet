/*=============================================================================#
 # Copyright (c) 2012, 2025 Dirk Fauth and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Dirk Fauth - initial API and implementation and/or initial documentation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.style;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Text decoration line that can be configured for cell styles.
 */
@NonNullByDefault
public enum TextDecorationLine {
	
	UNDERLINE,
	STRIKETHROUGH;
	
	
}
