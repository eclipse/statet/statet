/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.config;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class DefaultDisplayModeOrdering implements DisplayModeLookupStrategy {
	
	
	private static final ImList<DisplayMode> NORMAL_ORDERING= ImCollections.newList(DisplayMode.NORMAL);
	
	private static final ImList<DisplayMode> HOVER_ORDERING= ImCollections.newList(DisplayMode.HOVER, DisplayMode.NORMAL);
	
	private static final ImList<DisplayMode> SELECT_ORDERING= ImCollections.newList(DisplayMode.SELECTED, DisplayMode.NORMAL);
	
	private static final ImList<DisplayMode> EDIT_ORDERING= ImCollections.newList(DisplayMode.EDIT, DisplayMode.NORMAL);
	
	private static final ImList<DisplayMode> EMPTY_ORDERING= ImCollections.emptyList();
	
	
	@Override
	public ImList<DisplayMode> getDisplayModeOrdering(final DisplayMode targetDisplayMode) {
		switch (targetDisplayMode) {
		case NORMAL:
			return NORMAL_ORDERING;
		case HOVER:
			return HOVER_ORDERING;
		case SELECTED:
			return SELECT_ORDERING;
		case EDIT:
			return EDIT_ORDERING;
		default:
			return EMPTY_ORDERING;
		}
	}
	
}
