/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.binding;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.ui.action.IDragMode;
import org.eclipse.statet.ecommons.waltable.ui.action.IKeyAction;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;


public interface IUiBindingRegistry {
	
	public IKeyAction getKeyEventAction(KeyEvent event);
	
	public IDragMode getDragMode(MouseEvent event);
	
	public IMouseAction getMouseMoveAction(MouseEvent event);
	
	public IMouseAction getMouseDownAction(MouseEvent event);
	
	public IMouseAction getSingleClickAction(MouseEvent event);
	
	public IMouseAction getDoubleClickAction(MouseEvent event);
	
	public IMouseAction getMouseHoverAction(MouseEvent event);
	
	public IMouseAction getMouseEnterAction(MouseEvent event);
	
	public IMouseAction getMouseExitAction(MouseEvent event);

}
