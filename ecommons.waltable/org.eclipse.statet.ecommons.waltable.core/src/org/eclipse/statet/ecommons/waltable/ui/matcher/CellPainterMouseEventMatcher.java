/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.matcher;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;


/**
 * Matches a mouse click on a given cell painter within a cell.
 */
public class CellPainterMouseEventMatcher extends MouseEventMatcher {
	
	
	private LayerCellPainter targetCellPainter;
	private Class<? extends LayerCellPainter> targetCellPainterClass;
	
	
	public CellPainterMouseEventMatcher(final String regionName, final int button, final LayerCellPainter targetCellPainter) {
		super(regionName, button);
		this.targetCellPainter= targetCellPainter;
	}
	
	public CellPainterMouseEventMatcher(final String regionName, final int button, final Class<? extends LayerCellPainter> targetCellPainterClass) {
		super(regionName, button);
		this.targetCellPainterClass= targetCellPainterClass;
	}
	
	
	@Override
	public boolean matches(final NatTable natTable, final MouseEvent event, final LabelStack regionLabels) {
		if (super.matches(natTable, event, regionLabels)) {
			final long columnPosition= natTable.getColumnPositionByX(event.x);
			final long rowPosition= natTable.getRowPositionByY(event.y);
			
			final LayerCell cell= natTable.getCellByPosition(columnPosition, rowPosition);
			
			//Bug 407598: only perform a check if the click in the body region was performed on a cell
			//cell == null can happen if the viewport is quite large and contains not enough cells to fill it.
			if (cell != null) {
				final ConfigRegistry configRegistry= natTable.getConfigRegistry();
				final LayerCellPainter layerCellPainter= configRegistry.getAttribute(CellConfigAttributes.CELL_PAINTER,
						cell.getDisplayMode(), cell.getLabels().getLabels() );
				
				final GC gc= new GC(natTable.getDisplay());
				try {
					final LRectangle adjustedCellBounds= natTable.getLayerPainter().adjustCellBounds(columnPosition, rowPosition, cell.getBounds());
					
					final LayerCellPainter clickedCellPainter= layerCellPainter.getCellPainterAt(event.x, event.y, cell, gc, adjustedCellBounds, configRegistry);
					if (clickedCellPainter != null) {
						if (	(this.targetCellPainter != null && this.targetCellPainter == clickedCellPainter) ||
								(this.targetCellPainterClass != null && this.targetCellPainterClass.isInstance(clickedCellPainter))) {
							return true;
						}
					}
				}
				finally {
					gc.dispose();
				}
			}
		}
		return false;
	}
	
}
