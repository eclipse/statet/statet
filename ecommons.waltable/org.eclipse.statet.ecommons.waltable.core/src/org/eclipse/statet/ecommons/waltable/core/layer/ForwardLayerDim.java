/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;


/**
 * A layer dimension for a {@link ForwardLayer}.
 * 
 * @param <TLayer> the type of the layer
 */
@NonNullByDefault
public class ForwardLayerDim<TLayer extends Layer> extends AbstractLayerDim<TLayer> {
	
	
	protected final LayerDim underlyingDim;
	
	
	public ForwardLayerDim(final TLayer layer, final LayerDim underlyingDim) {
		this(layer, underlyingDim.getOrientation(), underlyingDim);
	}
	
	public ForwardLayerDim(final TLayer layer, final Orientation orientation,
			final LayerDim underlyingDim) {
		super(layer, orientation);
		this.underlyingDim= nonNullAssert(underlyingDim);
	}
	
	
	@Override
	public long getPositionId(final long refPosition, final long position) {
		return this.underlyingDim.getPositionId(refPosition, position);
	}
	
	@Override
	public long getPositionById(final long id) {
		return this.underlyingDim.getPositionById(id);
	}
	
	
	@Override
	public long getPositionCount() {
		return this.underlyingDim.getPositionCount();
	}
	
	@Override
	public long localToUnderlyingPosition(final long refPosition, final long position) {
		return position;
	}
	
	@Override
	public long underlyingToLocalPosition(final LayerDim sourceUnderlyingDim,
			final long underlyingPosition) {
		if (sourceUnderlyingDim != this.underlyingDim) {
			throw new IllegalArgumentException("underlyingLayer"); //$NON-NLS-1$
		}
		
		return underlyingPosition;
	}
	
	@Override
	public List<LRange> underlyingToLocalPositions(final LayerDim sourceUnderlyingDim,
			final Collection<LRange> underlyingPositions) {
		if (sourceUnderlyingDim != this.underlyingDim) {
			throw new IllegalArgumentException("underlyingLayer"); //$NON-NLS-1$
		}
		
		return LRangeList.toRangeList(underlyingPositions);
	}
	
	@Override
	public ImList<LayerDim> getUnderlyingDimsByPosition(final long position) {
		return ImCollections.newList(this.underlyingDim);
	}
	
	
	@Override
	public long getSize() {
		return this.underlyingDim.getSize();
	}
	
	@Override
	public long getPreferredSize() {
		return this.underlyingDim.getPreferredSize();
	}
	
	@Override
	public long getPositionByPixel(final long pixel) {
		return this.underlyingDim.getPositionByPixel(pixel);
	}
	
	@Override
	public long getPositionStart(final long refPosition, final long position) {
		return this.underlyingDim.getPositionStart(refPosition, position);
	}
	
	@Override
	public long getPositionStart(final long position) {
		return this.underlyingDim.getPositionStart(position);
	}
	
	@Override
	public int getPositionSize(final long refPosition, final long position) {
		return this.underlyingDim.getPositionSize(refPosition, position);
	}
	
	@Override
	public int getPositionSize(final long position) {
		return this.underlyingDim.getPositionSize(position);
	}
	
	@Override
	public boolean isPositionResizable(final long position) {
		return this.underlyingDim.isPositionResizable(position);
	}
	
}
