/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style.editor;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.grid.AlternatingRowLabelContributor;


public class GridStyleParameterObject {

	public Font tableFont;
	public Color evenRowColor;
	public Color oddRowColor;
	public Color selectionColor;

	public Style evenRowStyle;
	public Style oddRowStyle;
	public Style selectionStyle;
	public Style tableStyle;

	private final ConfigRegistry configRegistry;

	public GridStyleParameterObject(final ConfigRegistry configRegistry) {
		this.configRegistry= configRegistry;
		init(configRegistry);
	}

	private void init(final ConfigRegistry configRegistry) {
		this.evenRowStyle= configRegistry.getAttribute(
				CellConfigAttributes.CELL_STYLE, 
				DisplayMode.NORMAL, 
				AlternatingRowLabelContributor.EVEN_ROW_CONFIG_TYPE);
		this.evenRowColor= this.evenRowStyle.getAttributeValue(CellStyling.BACKGROUND_COLOR);

		this.oddRowStyle= configRegistry.getAttribute(
				CellConfigAttributes.CELL_STYLE, 
				DisplayMode.NORMAL, 
				AlternatingRowLabelContributor.ODD_ROW_CONFIG_TYPE);
		this.oddRowColor= this.oddRowStyle.getAttributeValue(CellStyling.BACKGROUND_COLOR);

		this.selectionStyle= configRegistry.getAttribute(CellConfigAttributes.CELL_STYLE, DisplayMode.SELECTED);
		this.selectionColor= this.selectionStyle.getAttributeValue(CellStyling.BACKGROUND_COLOR);
		
		this.tableStyle= configRegistry.getAttribute(CellConfigAttributes.CELL_STYLE, DisplayMode.NORMAL);
		this.tableFont= this.tableStyle.getAttributeValue(CellStyling.FONT);
	}
	
	public ConfigRegistry getConfigRegistry() {
		return this.configRegistry;
	}

}
