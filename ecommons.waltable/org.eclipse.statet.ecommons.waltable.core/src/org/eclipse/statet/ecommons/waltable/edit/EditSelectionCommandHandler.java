/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit;

import java.util.Collection;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;


/**
 * Command handler for handling {@link EditSelectionCommand}s.
 * Will first check if all selected cells are editable and if they have the same
 * editor configured. Will call the {@link EditController} for activation of the 
 * edit mode if these checks succeed.
 */
public class EditSelectionCommandHandler extends AbstractLayerCommandHandler<EditSelectionCommand> {

	private final SelectionLayer selectionLayer;
	
	public EditSelectionCommandHandler(final SelectionLayer selectionLayer) {
		this.selectionLayer= selectionLayer;
	}
	
	@Override
	public Class<EditSelectionCommand> getCommandClass() {
		return EditSelectionCommand.class;
	}
	
	@Override
    public boolean doCommand(final EditSelectionCommand command) {
		final Composite parent= command.getParent();
		final ConfigRegistry configRegistry= command.getConfigRegistry();
		final Character initialValue= command.getCharacter();
		
		if (EditUtils.allCellsEditable(this.selectionLayer, configRegistry)
				&& EditUtils.isEditorSame(this.selectionLayer, configRegistry)
				&& EditUtils.isConverterSame(this.selectionLayer, configRegistry)) {
			//check how many cells are selected
			final Collection<LayerCell> selectedCells= this.selectionLayer.getSelectedCells();
			if (selectedCells.size() == 1) {
				//editing is triggered by key for a single cell
				//we need to fire the InlineCellEditEvent here because we don't know the correct bounds
				//of the cell to edit inline corresponding to the NatTable. On firing the event, a
				//translation process is triggered, converting the information to the correct values
				//needed for inline editing
				final LayerCell cell= selectedCells.iterator().next();
				this.selectionLayer.fireLayerEvent(
						new InlineCellEditEvent(
								this.selectionLayer, 
								new PositionCoordinate(this.selectionLayer, cell.getColumnPosition(), cell.getRowPosition()), 
								parent, 
								configRegistry, 
								(initialValue != null ? initialValue : cell.getDataValue(0, null))));
			}
			else if (selectedCells.size() > 1) {
				//determine the initial value
				Object initialEditValue= initialValue;
				if (initialValue == null && EditUtils.isValueSame(this.selectionLayer)) {
					final LayerCell cell= selectedCells.iterator().next();
					initialEditValue= cell.getDataValue(0, null);
				}
				
				EditController.editCells(selectedCells, parent, initialEditValue, configRegistry);
			}
		}

		//as commands by default are intended to be consumed by the handler, always
		//return true, whether the activation of the edit mode was successfull or not
		return true;
	}

}
