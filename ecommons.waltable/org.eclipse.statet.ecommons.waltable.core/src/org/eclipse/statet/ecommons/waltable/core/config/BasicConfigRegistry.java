/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.config;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicConfigRegistry implements ConfigRegistry {
	
	
	private DisplayModeLookupStrategy displayModeLookupStrategy= new DefaultDisplayModeOrdering();
	
	private final Map<ConfigAttribute<?>, Map<DisplayMode, Map<String, ?>>> configRegistry= new HashMap<>();
	
	
	public BasicConfigRegistry() {
	}
	
	
	@Override
	public DisplayModeLookupStrategy getDisplayModeOrdering() {
		return this.displayModeLookupStrategy;
	}
	
	public void setDisplayModeOrdering(final DisplayModeLookupStrategy displayModeLookupStrategy) {
		this.displayModeLookupStrategy= nonNullAssert(displayModeLookupStrategy);
	}
	
	
	@Override
	public <T> @Nullable T getAttribute(final ConfigAttribute<T> configAttribute,
			final DisplayMode targetDisplayMode, final @NonNull String... configLabels) {
		return getAttribute(configAttribute, targetDisplayMode, ImCollections.newList(configLabels));
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAttribute(final ConfigAttribute<T> configAttribute,
			final DisplayMode targetDisplayMode, final List<String> configLabels) {
		@Nullable T attributeValue= null;
		
		final Map<DisplayMode, Map<String, ?>> displayModeAttributeMap= this.configRegistry.get(configAttribute);
		if (displayModeAttributeMap != null) {
			for (final DisplayMode displayMode : this.displayModeLookupStrategy.getDisplayModeOrdering(targetDisplayMode)) {
				final Map<String, T> configAttributeMap= (Map<String, T>)displayModeAttributeMap.get(displayMode);
				if (configAttributeMap != null) {
					for (final String configLabel : configLabels) {
						attributeValue= configAttributeMap.get(configLabel);
						if (attributeValue != null) {
							return attributeValue;
						}
					}
					
					// default config type
					attributeValue= configAttributeMap.get(null);
					if (attributeValue != null) {
						return attributeValue;
					}
				}
			}
		}
		
		return attributeValue;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getSpecificAttribute(final ConfigAttribute<T> configAttribute,
			final DisplayMode displayMode, final @Nullable String configLabel) {
		@Nullable T attributeValue= null;
		
		final Map<DisplayMode, Map<String, ?>> displayModeAttributeMap= this.configRegistry.get(configAttribute);
		if (displayModeAttributeMap != null) {
			final Map<String, T> configAttributeMap= (Map<String, T>)displayModeAttributeMap.get(displayMode);
			if (configAttributeMap != null) {
				attributeValue= configAttributeMap.get(configLabel);
				if (attributeValue != null) {
					return attributeValue;
				}
			}
		}
		
		return attributeValue;
	}
	
	@Override
	public <T> void registerAttribute(final ConfigAttribute<T> configAttribute, final T attributeValue) {
		registerAttribute(configAttribute, attributeValue, DisplayMode.NORMAL);
	}
	
	@Override
	public <T> void registerAttribute(final ConfigAttribute<T> configAttribute, final T attributeValue,
			final DisplayMode displayMode) {
		registerAttribute(configAttribute, attributeValue, displayMode, null);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> void registerAttribute(final ConfigAttribute<T> configAttribute, final T attributeValue,
			final DisplayMode displayMode, final String configLabel) {
		Map<DisplayMode, Map<String, ?>> displayModeAttributeMap= this.configRegistry.get(configAttribute);
		if (displayModeAttributeMap == null) {
			displayModeAttributeMap= new HashMap<>();
			this.configRegistry.put(configAttribute, displayModeAttributeMap);
		}
		
		Map<String, T> configAttributeMap= (Map<String, T>) displayModeAttributeMap.get(displayMode);
		if (configAttributeMap == null) {
			configAttributeMap= new HashMap<>();
			displayModeAttributeMap.put(displayMode, configAttributeMap);
		}
		
		configAttributeMap.put(configLabel, attributeValue);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> void unregisterAttribute(final ConfigAttribute<T> configAttributeType,
			final DisplayMode displayMode, final String configLabel) {
		final Map<DisplayMode, Map<String, ?>> displayModeAttributeMap= this.configRegistry.get(configAttributeType);
		if (displayModeAttributeMap != null) {
			final Map<String, T> configAttributeMap= (Map<String, T>) displayModeAttributeMap.get(displayMode);
			if (configAttributeMap != null) {
				configAttributeMap.remove(configLabel);
			}
		}
	}
	
}
