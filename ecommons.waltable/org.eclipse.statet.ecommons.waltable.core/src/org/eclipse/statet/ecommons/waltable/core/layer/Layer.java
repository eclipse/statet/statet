/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import java.util.Properties;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.Persistable;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.events.VisualChangeEvent;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;


/**
 * <p>
 * A Layer is a rectangular region of grid cells. A layer has methods to access its columns, rows, width and height. A
 * layer can be stacked on top of another layer in order to expose a transformed view of its underlying layer's grid
 * cell structure.
 * </p>
 * <p>
 * Columns and rows in a layer are referenced either by <b>position</b> or <b>id</b>. The position of a column/row in
 * a layer corresponds to the physical location of the column/row in the layer. The id of a column/row in a layer
 * corresponds to the location of the column/row in the lowest level layer in the layer stack. These concepts are
 * illustrated by the following example:
 * </p>
 * <pre>
 * Hide Layer C
 * 0 1 2 3 4 &lt;- column positions
 * 1 0 3 4 5 &lt;- column ids
 * 
 * Reorder Layer B
 * 0 1 2 3 4 5 &lt;- column positions
 * 2 1 0 3 4 5 &lt;- column ids
 * 
 * Data Layer A
 * 0 1 2 3 4 5 &lt;- column positions
 * 0 1 2 3 4 5 &lt;- column ids
 * </pre>
 * <p>
 * In the above example, Hide Layer C is stacked on top of Reorder Layer B, which is in turn stacked on top of Data
 * Layer A. The positions in Data Layer A are the same as its ids, because it is the lowest level layer in the
 * stack. Reorder Layer B reorders column 0 of its underlying layer (Data Layer A) after column 2 of its underlying
 * layer. Hide Layer C hides the first column of its underlying layer (Reorder Layer B).
 * </p>
 * <p>
 * Layers can also be laterally composed into larger layers. For instance, the standard grid layer is composed of a
 * body layer, column header layer, row header layer, and corner layer:
 * </p>
 * <table border=1>
 *   <caption></caption>
 *   <tr><td>corner</td><td>column header</td></tr>
 *   <tr><td>row header</td><td>body</td></tr>
 * </table>
 */
@NonNullByDefault
public interface Layer extends LayerListener, Persistable {
	
	
	// Dispose
	
	void dispose();
	
	
	/**
	 * Returns the layer dimension of this layer for the given orientation
	 * 
	 * @param orientation the orientation
	 * 
	 * @return the layer dimension
	 */
	LayerDim getDim(Orientation orientation);
	
	
	// Persistence
	
	/**
	 * Persistables registered with a layer will have a chance to write their data out to the
	 * {@link Properties} instance when the layer is persisted.
	 * @param persistable the persistable to be registered
	 */
	void registerPersistable(Persistable persistable);
	
	void unregisterPersistable(Persistable persistable);
	
	// Configuration
	
	/**
	 * Every layer gets this call back, starting at the top of the stack. This is triggered
	 * by the {@link NatTable#configure()} method. This is an opportunity to add
	 * any key/mouse bindings and other general configuration.
	 *
	 * @param configRegistry instance owned by {@link NatTable}
	 * @param uiBindingRegistry instance owned by {@link NatTable}
	 */
	void configure(ConfigRegistry configRegistry, UiBindingRegistry uiBindingRegistry);
	
	// Region
	
	/**
	 * Layer can apply its own labels to any cell it wishes.
	 * @param x the x pixel coordinate
	 * @param y the y pixel coordinate
	 * @return a LabelStack containing the region labels for the cell at the given pixel position
	 */
	LabelStack getRegionLabelsByXY(long x, long y);
	
	// Commands
	
	/**
	 * Opportunity to respond to a command as it flows down the stack. If the layer
	 * is not interested in the command it should allow the command to keep traveling
	 * down the stack.
	 *
	 * @param command the command to perform
	 * @return true if the command has been handled, false otherwise
	 */
	boolean doCommand(LayerCommand command);
	
	void registerCommandHandler(LayerCommandHandler<?> commandHandler);
	
	void unregisterCommandHandler(Class<? extends LayerCommand> commandClass);
	
	// Events
	
	/**
	 * Events can be fired to notify other components of the grid.
	 * Events travel <i>up</i> the layer stack and may cause a repaint.
	 * <p>
	 * Example: When the contents of the grid change {@link VisualChangeEvent} can be
	 * fired to notify other layers to refresh their caches etc.
	 * 
	 * @param event the event to fire
	 */
	void fireLayerEvent(LayerEvent event);
	
	void addLayerListener(LayerListener listener);
	
	void removeLayerListener(LayerListener listener);
	
	LayerPainter getLayerPainter();
	
	// Client area
	
	ClientAreaProvider getClientAreaProvider();
	
	void setClientAreaProvider(ClientAreaProvider clientAreaProvider);
	
	
	// Columns
	
	/**
	 * @return the number of columns in this coordinate model
	 */
	long getColumnCount();
	
	/**
	 * Returns the total width in pixels of this layer.
	 * 
	 * @return the width of this layer
	 */
	long getWidth();
	
	long getColumnPositionByX(long x);
	
	
	// Rows
	
	/**
	 * @return the number of rows in this coordinate model
	 */
	long getRowCount();
	
	/**
	 * Returns the total height in pixels of this layer.
	 * 
	 * @return the height of this layer
	 */
	long getHeight();
	
	long getRowPositionByY(long y);
	
	
	// Cells
	
	LayerCell getCellByPosition(long columnPosition, long rowPosition);
	
	@Nullable Layer getUnderlyingLayerByPosition(long columnPosition, long rowPosition);
	
}
