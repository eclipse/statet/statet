/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.config;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.AbstractLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.data.validate.IDataValidator;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;


/**
 * Configurations can be added to NatTable/ILayer to modify default behavior.
 * These will be processed when {@link NatTable#configure()} is invoked.
 * 
 * Default configurations are added to most layers {@link AbstractLayer#addConfiguration(IConfiguration)}.
 * You can turn off default configuration for an {@link Layer} by setting auto configure to false
 * in the constructor.
 */
public interface IConfiguration {

	public void configureLayer(Layer layer);

	/**
	 * Configure NatTable's {@link ConfigRegistry} upon receiving this call back.
	 * A mechanism to plug-in custom {@link LayerCellPainter}, {@link IDataValidator} etc.
	 */
	public void configureRegistry(ConfigRegistry configRegistry);

	/**
	 * Configure NatTable's {@link ConfigRegistry} upon receiving this call back
	 * A mechanism to customize key/mouse bindings.
	 */
	public void configureUiBindings(UiBindingRegistry uiBindingRegistry);

}
