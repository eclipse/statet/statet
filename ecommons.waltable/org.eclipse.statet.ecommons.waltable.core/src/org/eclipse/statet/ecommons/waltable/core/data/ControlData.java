/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ControlData {
	
	
	public static final int ERROR= 1 << 0;
	public static final int NA= 1 << 1;
	public static final int ASYNC= 1 << 2;
	
	
	private final int code;
	
	private final String text;
	
	
	public ControlData(final int code, final String text) {
		this.code= code;
		this.text= text;
	}
	
	
	public int getCode() {
		return this.code;
	}
	
	
	@Override
	public int hashCode() {
		return this.text.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj == this
				|| (obj instanceof ControlData 
						&& this.code == ((ControlData)obj).code ));
	}
	
	
	@Override
	public String toString() {
		return this.text;
	}
	
}
