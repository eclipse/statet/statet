/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer.events;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


@NonNullByDefault
public class DimGeneralStrucuralChangeEvent implements VisualChangeEvent, StructuralChangeEvent {
	
	
	private final LayerDim layerDim;
	
	
	public DimGeneralStrucuralChangeEvent(final LayerDim layerDim) {
		this.layerDim= layerDim;
	}
	
	@Override
	public @Nullable DimGeneralStrucuralChangeEvent toLayer(final Layer targetLayer) {
		final LayerDim targetLayerDim= targetLayer.getDim(this.layerDim.getOrientation());
		if (targetLayerDim == this.layerDim) {
			return this;
		}
		
		return new DimGeneralStrucuralChangeEvent(targetLayerDim);
	}
	
	
	public final LayerDim getLayerDim() {
		return this.layerDim;
	}
	
	public final Orientation getOrientation() {
		return this.layerDim.getOrientation();
	}
	
	@Override
	public final Layer getLayer() {
		return this.layerDim.getLayer();
	}
	
	
	@Override
	public ImList<LRectangle> getChangedPositionRectangles() {
		final var layer= getLayer();
		return ImCollections.newList(
				new LRectangle(0, 0, layer.getColumnCount(), layer.getRowCount()) );
	}
	
	
	@Override
	public boolean isStructureChanged(final Orientation orientation) {
		return (getOrientation() == orientation);
	}
	
	@Override
	public @Nullable List<StructuralDiff> getDiffs(final Orientation orientation) {
		return null;
	}
	
}
