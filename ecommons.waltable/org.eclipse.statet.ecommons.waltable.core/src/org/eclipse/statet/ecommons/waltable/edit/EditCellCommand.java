/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;


/**
 * Command that will trigger activating the edit mode for the specified cell.
 * The corresponding command handler is responsible for determining if activation
 * should proceed because of race conditions like e.g. the cell is configured to 
 * be editable or another editor is active containing an invalid data.
 */
public class EditCellCommand extends AbstractContextFreeCommand {
	
	/**
	 * The {@link ConfigRegistry} containing the configuration of the current NatTable 
	 * instance the command should be executed for.
	 * <p>
	 * This is necessary because the edit controllers in the current architecture
	 * are not aware of the instance they are running in.
	 */
	private final ConfigRegistry configRegistry;
	/**
	 * The parent Composite, needed for the creation of the editor control.
	 */
	private final Composite parent;
	/**
	 * The cell that should be put in edit mode.
	 */
	private final LayerCell cell;

	/**
	 * 
	 * @param parent The parent Composite, needed for the creation of the editor control.
	 * @param configRegistry The {@link ConfigRegistry} containing the configuration of the
	 * 			current NatTable instance the command should be executed for.
	 * 			This is necessary because the edit controllers in the current architecture
	 * 			are not aware of the instance they are running in.
	 * @param cell The cell that should be put into the edit mode.
	 */
	public EditCellCommand(final Composite parent, final ConfigRegistry configRegistry, final LayerCell cell) {
		this.configRegistry= configRegistry;
		this.parent= parent;
		this.cell= cell;
	}

	/**
	 * @return The {@link ConfigRegistry} containing the configuration of the current NatTable 
	 * 			instance the command should be executed for.
	 */
	public ConfigRegistry getConfigRegistry() {
		return this.configRegistry;
	}
	
	/**
	 * @return The parent Composite, needed for the creation of the editor control.
	 */
	public Composite getParent() {
		return this.parent;
	}

	/**
	 * @return The cell that should be put in edit mode.
	 */
	public LayerCell getCell() {
		return this.cell;
	}
	
}
