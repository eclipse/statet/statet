/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.style.CellStyleUtil;


/**
 * Paints the background of the cell using the color from the cell style.
 * If no background color is registered in the {@link BasicConfigRegistry} the painting
 * is skipped.
 * <p>
 * Example: The {@link TextPainter} inherits this and uses the paint method
 * in this class to paint the background of the cell.
 * 
 * Can be used as a cell painter or a decorator.
 */
public class BackgroundPainter extends CellPainterWrapper {

	public BackgroundPainter() {}

	public BackgroundPainter(final LayerCellPainter painter) {
		super(painter);
	}

	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle bounds, final ConfigRegistry configRegistry) {
		final Color backgroundColor= getBackgroundColour(cell, configRegistry);
		if (backgroundColor != null) {
			final Color originalBackground= gc.getBackground();

			gc.setBackground(backgroundColor);
			gc.fillRectangle(safe(bounds));

			gc.setBackground(originalBackground);
		}

		super.paintCell(cell, gc, bounds, configRegistry);
	}
	
	protected Color getBackgroundColour(final LayerCell cell, final ConfigRegistry configRegistry) {
		return CellStyleUtil.getCellStyle(cell, configRegistry).getAttributeValue(CellStyling.BACKGROUND_COLOR);
	}

}
