/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.command;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.ecommons.waltable.coordinate.ColumnPositionCoordinate;
import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.coordinate.RowPositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;


public class LayerCommandUtil {
	
	public static PositionCoordinate convertPositionToTargetContext(final PositionCoordinate positionCoordinate, final Layer targetLayer) {
		final Layer layer= positionCoordinate.getLayer();
		
		if (layer == targetLayer) {
			return positionCoordinate;
		}
		
		final long columnPosition= positionCoordinate.getColumnPosition();
		final long underlyingColumnPosition= layer.getDim(HORIZONTAL).localToUnderlyingPosition(columnPosition, columnPosition);
		if (underlyingColumnPosition == Long.MIN_VALUE) {
			return null;
		}
		
		final long rowPosition= positionCoordinate.getRowPosition();
		final long underlyingRowPosition= layer.getDim(VERTICAL).localToUnderlyingPosition(rowPosition, rowPosition);
		if (underlyingRowPosition == Long.MIN_VALUE) {
			return null;
		}
		
		final Layer underlyingLayer= layer.getUnderlyingLayerByPosition(columnPosition, rowPosition);
		if (underlyingLayer == null) {
			return null;
		}
		
		return convertPositionToTargetContext(new PositionCoordinate(underlyingLayer, underlyingColumnPosition, underlyingRowPosition), targetLayer);
	}
	
	public static ColumnPositionCoordinate convertColumnPositionToTargetContext(final ColumnPositionCoordinate columnPositionCoordinate, final Layer targetLayer) {
		if (columnPositionCoordinate != null) {
			final Layer layer= columnPositionCoordinate.getLayer();
			
			final long targetPosition= LayerUtils.convertPosition(layer.getDim(HORIZONTAL),
					columnPositionCoordinate.columnPosition, columnPositionCoordinate.columnPosition,
					targetLayer.getDim(HORIZONTAL) );
			return (targetPosition != LayerDim.POSITION_NA) ?
					new ColumnPositionCoordinate(targetLayer, targetPosition) :
					null;
		}
		return null;
	}
	
	public static RowPositionCoordinate convertRowPositionToTargetContext(final RowPositionCoordinate rowPositionCoordinate, final Layer targetLayer) {
		if (rowPositionCoordinate != null) {
			final Layer layer= rowPositionCoordinate.getLayer();
			
			final long targetPosition= LayerUtils.convertPosition(layer.getDim(VERTICAL),
					rowPositionCoordinate.rowPosition, rowPositionCoordinate.rowPosition,
					targetLayer.getDim(VERTICAL) );
			return (targetPosition != LayerDim.POSITION_NA) ?
					new RowPositionCoordinate(targetLayer, targetPosition) :
					null;
		}
		return null;
	}
	
}
