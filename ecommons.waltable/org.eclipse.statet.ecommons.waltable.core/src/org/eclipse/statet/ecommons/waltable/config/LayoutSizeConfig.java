/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.config;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;


public class LayoutSizeConfig {
	
	
	public static final ConfigAttribute<LayoutSizeConfig> CONFIG= new ConfigAttribute<>();
	
	
	private final int spaceWidth;
	
	private final int textHeight;
	
	private final int charWidth;
	
	
	public LayoutSizeConfig(final int spaceWidth, final int textHeight, final int charWidth) {
		this.spaceWidth= spaceWidth;
		this.textHeight= textHeight;
		this.charWidth= charWidth;
	}
	
	public int getDefaultSpace() {
		return this.spaceWidth;
	}
	
	public int getTextHeight() {
		return this.textHeight;
	}
	
	public int getCharWidth() {
		return this.charWidth;
	}
	
	public int getRowHeight() {
		return this.textHeight + this.spaceWidth;
	}
	
}
