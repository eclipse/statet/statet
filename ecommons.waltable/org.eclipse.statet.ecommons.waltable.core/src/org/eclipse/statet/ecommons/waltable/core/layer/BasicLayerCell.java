/*=============================================================================#
 # Copyright (c) 2012, 2025 Edwin Park and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Edwin Park - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.data.ControlData;


@NonNullByDefault
public class BasicLayerCell implements LayerCell {
	
	
	private static final ControlData NO_DATA= new ControlData(0, ""); //$NON-NLS-1$
	
	
	private final Layer layer;
	
	private final LayerCellDim h;
	private final LayerCellDim v;
	
	
	public BasicLayerCell(final Layer layer,
			final LayerCellDim horizontalDim, final LayerCellDim verticalDim) {
		this.layer= layer;
		this.h= horizontalDim;
		this.v= verticalDim;
	}
	
	
	@Override
	public final Layer getLayer() {
		return this.layer;
	}
	
	@Override
	public final LayerCellDim getDim(final Orientation orientation) {
		switch (orientation) {
		case HORIZONTAL:
			return this.h;
		case VERTICAL:
			return this.v;
		default:
			throw new IllegalStateException();
		}
	}
	
	
	@Override
	public final long getColumnPosition() {
		return this.h.getPosition();
	}
	
	@Override
	public final long getRowPosition() {
		return this.v.getPosition();
	}
	
	@Override
	public final long getOriginColumnPosition() {
		return this.h.getOriginPosition();
	}
	
	@Override
	public final long getOriginRowPosition() {
		return this.v.getOriginPosition();
	}
	
	@Override
	public final long getColumnSpan() {
		return this.h.getPositionSpan();
	}
	
	@Override
	public final long getRowSpan() {
		return this.v.getPositionSpan();
	}
	
	@Override
	public final boolean isSpannedCell() {
		return (getColumnSpan() > 1 || getRowSpan() > 1);
	}
	
	
	@Override
	public DisplayMode getDisplayMode() {
		return DisplayMode.NORMAL;
	}
	
	@Override
	public LabelStack getLabels() {
		return new LabelStack();
	}
	
	@Override
	public @Nullable Object getDataValue(final int flags,
			final @Nullable IProgressMonitor monitor) {
		return NO_DATA;
	}
	
	
	@Override
	public LRectangle getBounds() {
		final long xOffset;
		final long yOffset;
		final long width;
		final long height;
		{	final LayerCellDim dim= getDim(HORIZONTAL);
			
			final long cellPosition= dim.getPosition();
			final long firstPosition= dim.getOriginPosition();
			final long lastPosition= firstPosition + dim.getPositionSpan() - 1;
			
			final LayerDim layerDim= getLayer().getDim(HORIZONTAL);
			xOffset= layerDim.getPositionStart(cellPosition, firstPosition);
			width= (firstPosition == lastPosition) ?
					layerDim.getPositionSize(cellPosition, lastPosition) :
					layerDim.getPositionStart(cellPosition, lastPosition) - xOffset + layerDim.getPositionSize(cellPosition, lastPosition);
		}
		{	final LayerCellDim dim= getDim(VERTICAL);
			
			final long cellPosition= dim.getPosition();
			final long firstPosition= dim.getOriginPosition();
			final long lastPosition= firstPosition + dim.getPositionSpan() - 1;
			
			final LayerDim layerDim= getLayer().getDim(VERTICAL);
			yOffset= layerDim.getPositionStart(cellPosition, firstPosition);
			height= (firstPosition == lastPosition) ?
					layerDim.getPositionSize(cellPosition, lastPosition) :
					layerDim.getPositionStart(cellPosition, lastPosition) - yOffset + layerDim.getPositionSize(cellPosition, lastPosition);
		}
		
		return new LRectangle(xOffset, yOffset, width, height);
	}
	
	
	// Spanned cells are equal if they have the same origin positions (different positions possible)
	// required in LayerCellPainter
	
	@Override
	public int hashCode() {
		final int h= this.h.hashCode() + this.v.hashCode();
		return h ^ h * getLayer().hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final LayerCell other
						&& this.layer.equals(other.getLayer())
						&& this.h.equals(other.getDim(HORIZONTAL))
						&& this.v.equals(other.getDim(VERTICAL)) ));
	}
	
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " (" //$NON-NLS-1$
			+ "\n\tdata= " + getDataValue(0, null) //$NON-NLS-1$
			+ "\n\tlayer= " + getLayer().getClass().getSimpleName() //$NON-NLS-1$
			+ "\n\thorizontal= " + this.h //$NON-NLS-1$
			+ "\n\tvertical= " + this.v //$NON-NLS-1$
			+ "\n)"; //$NON-NLS-1$
	}
	
}
