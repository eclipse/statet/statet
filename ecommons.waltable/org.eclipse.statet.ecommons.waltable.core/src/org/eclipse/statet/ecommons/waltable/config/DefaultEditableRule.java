/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.config;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;


public class DefaultEditableRule implements IEditableRule {
	
	private final boolean defaultEditable;
	
	public DefaultEditableRule(final boolean defaultEditable) {
		this.defaultEditable= defaultEditable;
	}
	
	@Override
	public boolean isEditable(final LayerCell cell, final ConfigRegistry configRegistry) {
		return this.defaultEditable;
	}
	
}
