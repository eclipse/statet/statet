/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.layer.cell;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.eclipse.statet.ecommons.waltable.core.Persistable;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.edit.editor.ICellEditor;


/**
 * Registers/Adds configuration labels for a given column (by index).
 * Custom {@link ICellEditor}, {@link LayerCellPainter}, {@link Style} can then 
 * be registered in the {@link ConfigRegistry} against these labels.
 * 
 * Also @see {@link RowOverrideLabelAccumulator} 
 */
public class ColumnOverrideLabelAccumulator extends AbstractOverrider implements Persistable {
	
	
	public static final String PERSISTENCE_KEY= ".columnOverrideLabelAccumulator"; //$NON-NLS-1$
	
	
	private final Layer layer;
	
	
	public ColumnOverrideLabelAccumulator(final Layer layer) {
		this.layer= layer;
	}
	
	
	@Override
	public void addLabels(final LabelStack configLabels, final long columnPosition, final long rowPosition) {
		final long columnIndex= this.layer.getDim(HORIZONTAL)
				.getPositionId(columnPosition, columnPosition);
		final List<String> overrides= getOverrides(Long.valueOf(columnIndex));
		if (overrides != null) {
			for (final String configLabel : overrides) {
				configLabels.addLabel(configLabel);
			}
		}
	}

	/**
	 * Register labels to be contributed a column. This label will be applied to
	 * all cells in the column.
	 */
	public void registerColumnOverrides(final long columnIndex, final String... configLabels) {
		super.registerOverrides(Long.valueOf(columnIndex), configLabels);
	}
	
	/**
	 * Register labels to be contributed a column. This label will be applied to
	 * all cells in the column.
	 */
	public void registerColumnOverridesOnTop(final long columnIndex, final String... configLabels) {
		super.registerOverridesOnTop(Long.valueOf(columnIndex), configLabels);
	}
	
	/** 
	 * Save the overrides to a properties file. A line is stored for every column.
	 * 
	 * Example for column 0:
	 * prefix.columnOverrideLabelAccumulator.0= LABEL1,LABEL2
	 */
	@Override
	public void saveState(final String prefix, final Map<String, String> properties) {
		final Map<Serializable, List<String>> overrides= getOverrides();

		for (final Map.Entry<Serializable, List<String>> entry : overrides.entrySet()) {
			final StringBuilder strBuilder= new StringBuilder();
			for (final String columnLabel : entry.getValue()) {
				strBuilder.append(columnLabel);
				strBuilder.append(VALUE_SEPARATOR);
			}
			//Strip the last comma
			String propertyValue= strBuilder.toString();
			if(propertyValue.endsWith(VALUE_SEPARATOR)){
				propertyValue= propertyValue.substring(0, propertyValue.length() - 1);
			}
			final String propertyKey= prefix + PERSISTENCE_KEY + DOT + entry.getKey();
			properties.put(propertyKey, propertyValue);
		}
	}

	/**
	 * Load the overrides state from the given properties file.
	 * @see #saveState(String, Map)
	 */
	@Override
	public void loadState(final String prefix, final Map<String, String> properties) {
		for (final var entry : properties.entrySet()) {
			final var key= entry.getKey();
			if (key.contains(PERSISTENCE_KEY)){
				final String labelsFromPropertyValue= entry.getValue().trim();
				final String columnIndexFromKey= key.substring(key.lastIndexOf(DOT) + 1);
				registerColumnOverrides(Long.parseLong(columnIndexFromKey), labelsFromPropertyValue.split(VALUE_SEPARATOR));
			}
		}
	}	
}
