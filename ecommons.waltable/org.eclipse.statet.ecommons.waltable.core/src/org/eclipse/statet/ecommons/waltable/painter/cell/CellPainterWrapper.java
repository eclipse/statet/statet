/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;


public abstract class CellPainterWrapper extends AbstractCellPainter {

	private LayerCellPainter wrappedPainter;

	public CellPainterWrapper() {}

	public CellPainterWrapper(final LayerCellPainter painter) {
		this.wrappedPainter= painter;
	}

	public void setWrappedPainter(final LayerCellPainter painter) {
		this.wrappedPainter= painter;
	}

	public LayerCellPainter getWrappedPainter() {
		return this.wrappedPainter;
	}

	public LRectangle getWrappedPainterBounds(final LayerCell cell, final GC gc, final LRectangle bounds, final ConfigRegistry configRegistry) {
		return bounds;
	}

	@Override
	public LayerCellPainter getCellPainterAt(final long x, final long y, final LayerCell cell, final GC gc, final LRectangle adjustedCellBounds, final ConfigRegistry configRegistry) {
		final LRectangle wrappedPainterBounds= getWrappedPainterBounds(cell, gc, adjustedCellBounds, configRegistry);
		if (this.wrappedPainter != null && wrappedPainterBounds.contains(x, y)) {
			return this.wrappedPainter.getCellPainterAt(x, y, cell, gc, wrappedPainterBounds, configRegistry);
		} else {
			return super.getCellPainterAt(x, y, cell, gc, adjustedCellBounds, configRegistry);
		}
	}

	@Override
	public long getPreferredWidth(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		return this.wrappedPainter != null ? this.wrappedPainter.getPreferredWidth(cell, gc, configRegistry) : 0;
	}

	@Override
	public long getPreferredHeight(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		return this.wrappedPainter != null ? this.wrappedPainter.getPreferredHeight(cell, gc, configRegistry) : 0;
	}

	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle adjustedCellBounds, final ConfigRegistry configRegistry) {
		if (this.wrappedPainter != null) {
			this.wrappedPainter.paintCell(cell, gc, adjustedCellBounds, configRegistry);
		}
	}

}
