/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Dirk Fauth
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.style.CellStyleUtil;


/**
 * Paints the background of the cell with a gradient sweeping using the style configuration.
 * To configure the gradient sweeping the following style attributes need to be configured
 * in the {@link BasicConfigRegistry}: 
 * <ul>
 * 	<li>{@link CellStyling#GRADIENT_FOREGROUND_COLOR} or {@link CellStyling#FOREGROUND_COLOR}</li>
 * 	<li>{@link CellStyling#GRADIENT_BACKGROUND_COLOR} or {@link CellStyling#BACKGROUND_COLOR}</li>
 * </ul>
 * If none of these values are registered in the {@link BasicConfigRegistry} the painting is skipped.
 * <p>
 * Can be used as a cell painter or a decorator.
 */
public class GradientBackgroundPainter extends CellPainterWrapper {

	/**
	 * @param vertical if <code>true</code> sweeps from top to bottom, else 
	 *        sweeps from left to right. <code>false</code> is default
	 */
	private final boolean vertical;
	
	/**
	 * Creates a {@link GradientBackgroundPainter} with a gradient sweeping from
	 * left to right.
	 */
	public GradientBackgroundPainter() {
		this(false);
	}

	/**
	 * Creates a {@link GradientBackgroundPainter} where the sweeping direction
	 * can be set.
	 * @param vertical if <code>true</code> sweeps from top to bottom, else 
	 *        sweeps from left to right. <code>false</code> is default
	 */
	public GradientBackgroundPainter(final boolean vertical) {
		this.vertical= vertical;
	}

	/**
	 * Creates a {@link GradientBackgroundPainter} as wrapper for the given painter with a gradient sweeping from
	 * left to right.
	 * @param painter The {@link LayerCellPainter} that is wrapped by this {@link GradientBackgroundPainter}
	 */
	public GradientBackgroundPainter(final LayerCellPainter painter) {
		this(painter, false);
	}

	/**
	 * Creates a {@link GradientBackgroundPainter} as wrapper for the given painter where the sweeping direction
	 * can be set.
	 * @param painter The {@link LayerCellPainter} that is wrapped by this {@link GradientBackgroundPainter}
	 * @param vertical if <code>true</code> sweeps from top to bottom, else 
	 *        sweeps from left to right. <code>false</code> is default
	 */
	public GradientBackgroundPainter(final LayerCellPainter painter, final boolean vertical) {
		super(painter);
		this.vertical= vertical;
	}

	@Override
	public void paintCell(final LayerCell cell, final GC gc, final LRectangle bounds, final ConfigRegistry configRegistry) {
		final Color foregroundColor= getForeGroundColour(cell, configRegistry);
		final Color backgroundColor= getBackgroundColour(cell, configRegistry);
		if (backgroundColor != null && foregroundColor != null) {
			final Color originalForeground= gc.getForeground();
			final Color originalBackground= gc.getBackground();

			gc.setForeground(foregroundColor);
			gc.setBackground(backgroundColor);
			final Rectangle rect= GraphicsUtils.safe(bounds);
			gc.fillGradientRectangle(rect.x, rect.y, rect.width, rect.height, this.vertical);

			gc.setForeground(originalForeground);
			gc.setBackground(originalBackground);
		}

		super.paintCell(cell, gc, bounds, configRegistry);
	}
	
	/**
	 * Searches the foreground color to be used for gradient sweeping. First checks the {@link org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry} if there
	 * is a value for the attribute {@link CellStyling#GRADIENT_FOREGROUND_COLOR} is registered. If there is one
	 * this value will be returned, if not it is checked if there is a value registered for {@link CellStyling#FOREGROUND_COLOR}
	 * and returned. If there is no value registered for any of these attributes, <code>null</code> will be returned which
	 * will skip the painting.
	 * @param cell The {@link org.eclipse.statet.ecommons.waltable.core.layer.ForwardLayerCell} for which the style attributes should be retrieved out of the {@link org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry}
	 * @param configRegistry The {@link org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry} to retrieve the attribute values from.
	 * @return The {@link Color} to use as foreground color of the gradient sweeping or <code>null</code> if none was configured.
	 */
	protected Color getForeGroundColour(final LayerCell cell, final ConfigRegistry configRegistry) {
		final Color fgColor= CellStyleUtil.getCellStyle(cell, configRegistry).getAttributeValue(CellStyling.GRADIENT_FOREGROUND_COLOR);
		return fgColor != null ? fgColor : CellStyleUtil.getCellStyle(cell, configRegistry).getAttributeValue(CellStyling.FOREGROUND_COLOR);
	}
	
	/**
	 * Searches the background color to be used for gradient sweeping. First checks the {@link org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry} if there
	 * is a value for the attribute {@link CellStyling#GRADIENT_BACKGROUND_COLOR} is registered. If there is one
	 * this value will be returned, if not it is checked if there is a value registered for {@link CellStyling#BACKGROUND_COLOR}
	 * and returned. If there is no value registered for any of these attributes, <code>null</code> will be returned which
	 * will skip the painting.
	 * @param cell The {@link org.eclipse.statet.ecommons.waltable.core.layer.ForwardLayerCell} for which the style attributes should be retrieved out of the {@link org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry}
	 * @param configRegistry The {@link org.eclipse.statet.ecommons.waltable.core.config.BasicConfigRegistry} to retrieve the attribute values from.
	 * @return The {@link Color} to use as background color of the gradient sweeping or <code>null</code> if none was configured.
	 */
	protected Color getBackgroundColour(final LayerCell cell, final ConfigRegistry configRegistry) {
		final Color bgColor= CellStyleUtil.getCellStyle(cell, configRegistry).getAttributeValue(CellStyling.GRADIENT_BACKGROUND_COLOR);
		return bgColor != null ? bgColor : CellStyleUtil.getCellStyle(cell, configRegistry).getAttributeValue(CellStyling.BACKGROUND_COLOR);
	}

}
