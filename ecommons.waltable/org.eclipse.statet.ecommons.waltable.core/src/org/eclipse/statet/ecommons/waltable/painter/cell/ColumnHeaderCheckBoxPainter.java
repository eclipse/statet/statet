/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.painter.cell;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;
import org.eclipse.statet.ecommons.waltable.data.convert.IDisplayConverter;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


public class ColumnHeaderCheckBoxPainter extends ImagePainter {
	

	private final Image checkedImg;
	private final Image semicheckedImg;
	private final Image uncheckedImg;
	
	private final Layer columnDataLayer;

	public ColumnHeaderCheckBoxPainter(final Layer columnDataLayer) {
		this(
				columnDataLayer,
				GUIHelper.getImage("checked"), //$NON-NLS-1$
				GUIHelper.getImage("semichecked"), //$NON-NLS-1$
				GUIHelper.getImage("unchecked") //$NON-NLS-1$
		);
	}

	public ColumnHeaderCheckBoxPainter(final Layer columnLayer, final Image checkedImg, final Image semicheckedImage, final Image uncheckedImg) {
		this.columnDataLayer= columnLayer;
		this.checkedImg= checkedImg;
		this.semicheckedImg= semicheckedImage;
		this.uncheckedImg= uncheckedImg;
	}

	public long getPreferredWidth(final boolean checked) {
		return checked ? this.checkedImg.getBounds().width : this.uncheckedImg.getBounds().width;
	}

	public long getPreferredHeight(final boolean checked) {
		return checked ? this.checkedImg.getBounds().height : this.uncheckedImg.getBounds().height;
	}

	@Override
	protected Image getImage(final LayerCell cell, final ConfigRegistry configRegistry) {
		final long columnPosition= LayerUtils.convertColumnPosition(cell.getLayer(), cell.getColumnPosition(), this.columnDataLayer);
		
		final long checkedCellsCount= getCheckedCellsCount(columnPosition, configRegistry);
		
		if (checkedCellsCount > 0) {
			if (checkedCellsCount == this.columnDataLayer.getRowCount()) {
				return this.checkedImg;
			} else {
				return this.semicheckedImg;
			}
		} else {
			return this.uncheckedImg;
		}
	}

	public long getCheckedCellsCount(final long columnPosition, final ConfigRegistry configRegistry) {
		long checkedCellsCount= 0;
		
		for (long rowPosition= 0; rowPosition < this.columnDataLayer.getRowCount(); rowPosition++) {
			final LayerCell columnCell= this.columnDataLayer.getCellByPosition(columnPosition, rowPosition);
			if (isChecked(columnCell, configRegistry)) {
				checkedCellsCount++;
			}
		}
		return checkedCellsCount;
	}

	protected boolean isChecked(final LayerCell cell, final ConfigRegistry configRegistry) {
		return convertDataType(cell, configRegistry).booleanValue();
	}

	protected Boolean convertDataType(final LayerCell cell, final ConfigRegistry configRegistry) {
		if (cell.getDataValue(0, null) instanceof Boolean) {
			return (Boolean) cell.getDataValue(0, null);
		}
		final IDisplayConverter displayConverter= configRegistry.getAttribute(CellConfigAttributes.DISPLAY_CONVERTER, cell.getDisplayMode(), cell.getLabels().getLabels());
		Boolean convertedValue= null;
		if (displayConverter != null) {
			try {
				convertedValue= (Boolean) displayConverter.canonicalToDisplayValue(cell, configRegistry, cell.getDataValue(0, null));
			} catch (final Exception e) {
//				log.debug(e);
			}
		}
		if (convertedValue == null) {
			convertedValue= Boolean.FALSE;
		}
		return convertedValue;
	}
	
}
