/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.waltable.Messages;
import org.eclipse.statet.ecommons.waltable.core.style.VerticalAlignment;


/**
 * Component that lets the user select an alignment.
 */
public class VerticalAlignmentPicker extends Composite {

    private final Combo combo;

    public VerticalAlignmentPicker(final Composite parent, final VerticalAlignment alignment) {
        super(parent, SWT.NONE);
        setLayout(new RowLayout());

		this.combo= new Combo(this, SWT.READ_ONLY | SWT.DROP_DOWN);
		this.combo.setItems(
				Messages.getString("VerticalAlignmentPicker.top"), //$NON-NLS-1$
				Messages.getString("VerticalAlignmentPicker.middle"), //$NON-NLS-1$
				Messages.getString("VerticalAlignmentPicker.bottom") ); //$NON-NLS-1$

        update(alignment);
    }

    private void update(final VerticalAlignment alignment) {
        if (alignment.equals(VerticalAlignment.TOP)) {
			this.combo.select(0);
		} else if (alignment.equals(VerticalAlignment.MIDDLE)) {
			this.combo.select(1);
		} else if (alignment.equals(VerticalAlignment.BOTTOM)) {
			this.combo.select(2);
		}
		else {
			throw new IllegalArgumentException("bad alignment: " + alignment); //$NON-NLS-1$
		}
    }

    public VerticalAlignment getSelectedAlignment() {
        final long idx= this.combo.getSelectionIndex();
        if (idx == 0) {
			return VerticalAlignment.TOP;
		} else if (idx == 1) {
			return VerticalAlignment.MIDDLE;
		} else if (idx == 2) {
			return VerticalAlignment.BOTTOM;
		}
		else {
			throw new IllegalStateException("shouldn't happen"); //$NON-NLS-1$
		}
    }

    public void setSelectedAlignment(final VerticalAlignment verticalAlignment) {
        if (verticalAlignment == null)
		 {
			throw new IllegalArgumentException("null"); //$NON-NLS-1$
		}
        update(verticalAlignment);
    }
}
