/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.style;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.swt.graphics.Color;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.persistence.ColorPersistor;


/**
 * This class defines the visual attributes of a border:
 * 
 * <dl>
 *   <dt>Thickness</dt>
 *   <dd>The width of the border in pixel</dd>
 *   <dt>Color</dt>
 *   <dd>The color of the border</dd>
 *   <dt>Line Style</dt>
 *   <dd>The line style of the border, a constant of {@link LineStyle}</dd>
 *   <dt>Offset</dt>
 *   <dd>The offset of the border toward the regular painting area.
 *       If not specified otherwise, the value is 0, meaning no offset. A
 *       value &gt; 0 shifts the border inwards, a value &lt; 0 shifts the
 *       border outwards.</dd>
 * </dl> 
 */
@NonNullByDefault
public class BorderStyle {
	
	
	public static enum LineStyle {
		
		SOLID,
		DASHED,
		DOTTED,
		DASHDOT,
		DASHDOTDOT,
		
		
	}
	
	
	private final int thickness;
	
	private final Color color;
	
	private final LineStyle lineStyle;
	
	private final int offset;
	
	
	/**
	 * Creates a new border style.
	 * 
	 * @param thickness the width of the border in pixel
	 * @param color the color of the border
	 * @param lineStyle the line style of the border
	 * @param offset the offset of the border
	 */
	public BorderStyle(final int thickness, final Color color, final LineStyle lineStyle, final int offset) {
		this.thickness= (thickness > 0) ? thickness : 0;
		this.color= nonNullAssert(color);
		this.lineStyle= nonNullAssert(lineStyle);
		this.offset= offset;
	}
	
	public BorderStyle(final int thickness, final Color color, final LineStyle lineStyle) {
		this(thickness, color, lineStyle, 0);
	}
	
	/**
	 * Reconstruct this instance from the persisted String.
	 * 
	 * @see BorderStyle#toString()
	 */
	public BorderStyle(final String string) {
		final var tokens= string.split("\\|"); //$NON-NLS-1$
		
		this.thickness= Integer.parseInt(tokens[0]);
		this.color= ColorPersistor.asColor(tokens[1]);
		this.lineStyle= LineStyle.valueOf(tokens[2]);
		this.offset= (tokens.length > 3) ? Integer.parseInt(tokens[3]) : 0;
	}
	
	
	public int getThickness() {
		return this.thickness;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public LineStyle getLineStyle() {
		return this.lineStyle;
	}
	
	public int getOffset() {
		return this.offset;
	}
	
	
	@Override
	public int hashCode() {
		return ((this.thickness
				* 13 + this.color.hashCode())
				* 17 + this.lineStyle.hashCode())
				+ this.offset * 31;
	}

	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof BorderStyle) {
			final BorderStyle other= (BorderStyle)obj;
			return (this.thickness == other.thickness
					&& this.color.equals(other.color)
					&& this.lineStyle == other.lineStyle
					&& this.offset == other.offset );
		}
		return false;
	}
	
	/**
	 * @return a human readable representation of the border style. This is
	 *         suitable for constructing an equivalent instance using the
	 *         BorderStyle(String) constructor
	 */
	@Override
	public String toString() {
		return "" + this.thickness + '|' + ColorPersistor.asString(this.color) + '|' + this.lineStyle + '|' + this.offset; //$NON-NLS-1$
	}
	
}
