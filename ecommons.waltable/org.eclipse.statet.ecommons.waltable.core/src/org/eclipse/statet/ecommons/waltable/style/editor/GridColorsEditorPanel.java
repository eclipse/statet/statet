/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.style.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.ecommons.waltable.Messages;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;


public class GridColorsEditorPanel extends AbstractEditorPanel<GridStyleParameterObject> {
	private FontPicker fontPicker;
	private ColorPicker evenRowColorPicker;
	private ColorPicker oddRowColorPicker;
	private ColorPicker selectionColorPicker;
	private ConfigRegistry configRegistry;

	public GridColorsEditorPanel(final Composite parent, final GridStyleParameterObject currentStyle) {
		super(parent, SWT.NONE);
	}

	@Override
	public String getEditorName() {
		return Messages.getString("GridColorsEditorPanel.editorName"); //$NON-NLS-1$
	}

	@Override
	public GridStyleParameterObject getNewValue() {
		final GridStyleParameterObject newStyle= new GridStyleParameterObject(this.configRegistry);
		newStyle.tableFont= this.fontPicker.getSelectedFont();
		newStyle.evenRowColor= this.evenRowColorPicker.getSelectedColor();
		newStyle.oddRowColor= this.oddRowColorPicker.getSelectedColor();
		newStyle.selectionColor= this.selectionColorPicker.getSelectedColor();
		return newStyle;
	}

	@Override
	public void edit(final GridStyleParameterObject currentStyle) throws Exception {
		this.configRegistry= currentStyle.getConfigRegistry();
        final GridLayout layout= new GridLayout(2, false);
        layout.marginLeft= 10;
		setLayout(layout);

		new Label(this, SWT.NONE).setText(Messages.getString("GridColorsEditorPanel.font")); //$NON-NLS-1$
		this.fontPicker= new FontPicker(this, currentStyle.tableFont);
		this.fontPicker.setLayoutData(new GridData(100, 22));

		new Label(this, SWT.NONE).setText(Messages.getString("GridColorsEditorPanel.evenRowColor")); //$NON-NLS-1$
        this.evenRowColorPicker= new ColorPicker(this, currentStyle.evenRowColor);

        new Label(this, SWT.NONE).setText(Messages.getString("GridColorsEditorPanel.oddRowColor")); //$NON-NLS-1$
        this.oddRowColorPicker= new ColorPicker(this, currentStyle.oddRowColor);

        new Label(this, SWT.NONE).setText(Messages.getString("GridColorsEditorPanel.selectionColor")); //$NON-NLS-1$
        this.selectionColorPicker= new ColorPicker(this, currentStyle.selectionColor);
	}

}
