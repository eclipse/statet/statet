/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;


/**
 * A LayerDim represents one dimension (horizontal= columns / vertical= rows) of a layer.
 * This allows to write code which is independent of the orientation.
 * 
 * It is recommend that implementation extends {@link AbstractLayerDim}.
 */
@NonNullByDefault
public interface LayerDim {
	
	
	long POSITION_NA= Long.MIN_VALUE;
	
	
	/**
	 * Returns the layer this dimension belongs to.
	 * 
	 * @return the layer
	 */
	Layer getLayer();
	
	/**
	 * Returns the orientation of this dimension.
	 * 
	 * @return the orientation
	 */
	Orientation getOrientation();
	
	
	// Id
	
	/**
	 * Returns the unique id for the specified position.
	 * 
	 * @param position the local position
	 * 
	 * @return the id
	 */
	long getPositionId(long refPosition, long position);
	
	/**
	 * Returns the position in this layer dimension for the id.
	 * 
	 * @param id the position id
	 * 
	 * @return the local position
	 */
	long getPositionById(long id);
	
	
	// Position= Columns / Rows
	
	/**
	 * Returns the number of positions in this layer dimension.
	 * 
	 * {@link Layer#getColumnCount()} /
	 * {@link Layer#getRowCount()}
	 * 
	 * @return the count of local positions
	 */
	long getPositionCount();
	
	
	/**
	 * Converts the specified position in this layer dimension to the position in the underlying
	 * layer.
	 * 
	 * @param the local position
	 * 
	 * @return the position in the underlying layer
	 */
	long localToUnderlyingPosition(long refPosition, long position);
	
	/**
	 * Converts the specified position in the specified underlying layer to the position in this
	 * layer dimension.
	 * 
	 * @param sourceUnderlyingDim the underlying layer dimension the position refers to
	 * @param underlyingPosition the position in the underlying layer
	 * 
	 * @return the local position
	 */
	long underlyingToLocalPosition(LayerDim sourceUnderlyingDim, long underlyingPosition);
	
	/**
	 * Converts the specified positions in the specified underlying layer to the position in this
	 * layer dimension.
	 * 
	 * {@link Layer#underlyingToLocalColumnPositions(Layer, Collection)} /
	 * {@link Layer#underlyingToLocalRowPositions(Layer, Collection)}
	 * 
	 * @param sourceUnderlyingDim the underlying layer dimension the positions refers to
	 * @param underlyingPositions the positions in the underlying layer
	 * 
	 * @return the local positions
	 */
	List<LRange> underlyingToLocalPositions(LayerDim sourceUnderlyingDim,
			Collection<LRange> underlyingPositions);
	
	/**
	 * Returns all underlying dimensions for the specified position.
	 * 
	 * @param position the local position
	 * 
	 * @return the underlying layer dimensions
	 */
	@Nullable ImList<LayerDim> getUnderlyingDimsByPosition(long position);
	
	
	// Pixel= X / Y, Width / Height
	
	/**
	 * Returns the size of this layer dimension.
	 * 
	 * {@link Layer#getWidth()} /
	 * {@link Layer#getHeight()}
	 * 
	 * @return the size in pixel
	 */
	long getSize();
	
	/**
	 * Returns the preferred size of this layer dimension.
	 * 
	 * @return the preferred size in pixel
	 */
	long getPreferredSize();
	
	/**
	 * Returns the position in this layer dimension for the specified pixel coordinate.
	 * 
	 * {@link Layer#getColumnPositionByX(int)} /
	 * {@link Layer#getRowPositionByY(int)}
	 * 
	 * @param pixel the pixel coordinate
	 * 
	 * @return the local position
	 */
	long getPositionByPixel(long pixel);
	
	/**
	 * Returns the pixel coordinate of the start of the specified position in this layer dimension.
	 * 
	 * @param position the local position
	 * 
	 * @return the pixel coordinate of the start
	 */
	long getPositionStart(long refPosition, long position);
	long getPositionStart(long position);
	
	/**
	 * Returns the size in pixel of the specified position in this layer dimension.
	 * 
	 * @param position the local position
	 * 
	 * @return the size in pixel
	 */
	int getPositionSize(long refPosition, long position);
	int getPositionSize(long position);
	
	/**
	 * Returns if the specified position is resizable.
	 *  
	 * @param position the local position
	 * 
	 * @return <code>true</code> if the position is resizable, otherwise <code>false</code>
	 */
	boolean isPositionResizable(long position);
	
}
