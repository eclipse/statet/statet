/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.coordinate;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Point on the (x, y) coordinate plane.
 * 
 * <p>The coordinate space for rectangles and points is considered to have
 * increasing values downward and to the right from its origin making this
 * the normal, computer graphics oriented notion of (x, y) coordinates rather
 * than the strict mathematical one.</p>
 * 
 * @see LRectangle
 */
@NonNullByDefault
public final class LPoint implements Immutable {
	
	/**
	 * the x coordinate of the point
	 */
	public final long x;
	
	/**
	 * the y coordinate of the point
	 */
	public final long y;
	
	
	/**
	 * Creates a new point with the specified x and y coordinates.
	 *
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	public LPoint(final long x, final long y) {
		this.x= x;
		this.y= y;
	}
	
	
	public long get(final Orientation orientation) {
		return (orientation == HORIZONTAL) ?
				this.x :
				this.y;
	}
	
	
	@Override
	public int hashCode() {
		int h= (int)(this.x ^ (this.x >>> 32));
		h= Integer.rotateRight(h, 15);
		h ^= (int)(this.y ^ (this.y >>> 32));
		return h ^ (h >>> 7);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final LPoint other
						&& this.x == other.x && this.y == other.y ));
	}
	
	
	@Override
	public String toString() {
		return "{" + this.x + ", " + this.y + "}L"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
	
}
