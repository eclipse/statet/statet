/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.swt.painters;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.core.layer.top.OverlayPainter;
import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;
import org.eclipse.statet.internal.ecommons.waltable.WaLTablePlugin;


@NonNullByDefault
public class TableLayerPainter implements LayerPainter {
	
	
	private final NatTable natTable;
	
	
	public TableLayerPainter(final NatTable natTable) {
		this.natTable= natTable;
	}
	
	
	@Override
	public LRectangle adjustCellBounds(final long columnPosition, final long rowPosition,
			final LRectangle cellBounds) {
		final var layerPainter= this.natTable.getUnderlyingLayer().getLayerPainter();
		return layerPainter.adjustCellBounds(columnPosition, rowPosition, cellBounds);
	}
	
	@Override
	public void paintLayer(final Layer natLayer, final GC gc,
			final int xOffset, final int yOffset, final Rectangle pixelRectangle,
			final ConfigRegistry configRegistry) {
		try {
			paintBackground(natLayer, gc, xOffset, yOffset, pixelRectangle, configRegistry);
			
			gc.setForeground(this.natTable.getForeground());
			
			final Rectangle paintRectangle= pixelRectangle.intersection(SwtUtils.toSWT(
					new LRectangle(xOffset, yOffset, natLayer.getWidth(), natLayer.getHeight()) ));
			
			if (!paintRectangle.isEmpty()) {
				final var layerPainter= this.natTable.getUnderlyingLayer().getLayerPainter();
				layerPainter.paintLayer(natLayer, gc, xOffset, yOffset,
						paintRectangle, configRegistry );
			}
			
			paintOverlays(natLayer, gc, xOffset, yOffset, pixelRectangle, configRegistry);
		}
		catch (final Exception e) {
			WaLTablePlugin.log(new Status(IStatus.ERROR, WaLTablePlugin.BUNDLE_ID,
					"An error occurred while painting the table.", e ));
		}
	}
	
	protected void paintBackground(final Layer natLayer, final GC gc, final long xOffset, final long yOffset,
			final Rectangle rectangle, final ConfigRegistry configRegistry) {
		gc.setBackground(this.natTable.getBackground());
		
		// Clean Background
		gc.fillRectangle(rectangle);
	}
	
	protected void paintOverlays(final Layer natLayer, final GC gc, final long xOffset, final long yOffset,
			final Rectangle rectangle, final ConfigRegistry configRegistry) {
		for (final OverlayPainter overlayPainter : this.natTable.getOverlayPainters()) {
			overlayPainter.paintOverlay(gc, this.natTable);
		}
	}
	
}
