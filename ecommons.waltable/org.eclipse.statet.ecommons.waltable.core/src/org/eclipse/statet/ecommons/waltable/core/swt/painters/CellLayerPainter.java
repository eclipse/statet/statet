/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.swt.painters;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;
import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;


@NonNullByDefault
public class CellLayerPainter implements LayerPainter {
	
	
	public static final long getEndPosition(final LayerDim layerDim, final int pixel) {
		final long positionByPixel= layerDim.getPositionByPixel(pixel);
		return ((positionByPixel > 0) ?
						Math.min(layerDim.getPositionCount(), positionByPixel) :
						layerDim.getPositionCount() );
	}
	
	private static Map<Long, Long> calculateDimensionInfo(final LayerDim dim,
			final long startPosition, final long endPosition) {
		final Map<Long, Long> positionToPixelMap= new HashMap<>();
		long start2= (startPosition > 0) ?
				dim.getPositionStart(startPosition - 1)
						+ dim.getPositionSize(startPosition - 1) :
				Long.MIN_VALUE;
		for (long position= startPosition; position < endPosition; position++) {
			final long start1= dim.getPositionStart(position);
			positionToPixelMap.put(position, Math.max(start1, start2));
			start2= start1 + dim.getPositionSize(position);
		}
		if (endPosition < dim.getPositionCount()) {
			final long start1= dim.getPositionStart(endPosition);
			positionToPixelMap.put(endPosition, Math.max(start1, start2));
		}
		return positionToPixelMap;
	}
	
	private static long getPositionStart(final LayerDim dim, final long position,
			final Map<Long, Long> positionToPixelMap) {
		if (position < dim.getPositionCount()) {
			Long start= positionToPixelMap.get(position);
			if (start == null) {
				start= Long.valueOf(dim.getPositionStart(position));
				if (position > 0) {
					final long start2= dim.getPositionStart(position - 1)
							+ dim.getPositionStart(position - 1);
					if (start2 > start.longValue()) {
						start= Long.valueOf(start2);
					}
				}
				positionToPixelMap.put(position, start);
			}
			return start.longValue();
		}
		else {
			return dim.getSize();
		}
	}
	
	
	private Layer natLayer;
	
	private Map<Long, Long> horizontalPositionToPixelMap;
	private Map<Long, Long> verticalPositionToPixelMap;
	
	
	@SuppressWarnings("null")
	public CellLayerPainter() {
	}
	
	
	@Override
	public LRectangle adjustCellBounds(final long columnPosition, final long rowPosition,
			final LRectangle cellBounds) {
		return cellBounds;
	}
	
	@Override
	public void paintLayer(final Layer natLayer, final GC gc, final int xOffset, final int yOffset,
			final Rectangle pixelRectangle, final ConfigRegistry configRegistry) {
		if (pixelRectangle.isEmpty()) {
			return;
		}
		
		this.natLayer= natLayer;
		final LRectangle positionRectangle= getPositionRectangleFromPixelRectangle(natLayer, pixelRectangle);
		
		calculateDimensionInfo(positionRectangle);
		
		final Collection<LayerCell> spannedCells= new HashSet<>();
		
		for (long columnPosition= positionRectangle.x; columnPosition < positionRectangle.x + positionRectangle.width; columnPosition++) {
			for (long rowPosition= positionRectangle.y; rowPosition < positionRectangle.y + positionRectangle.height; rowPosition++) {
				final LayerCell cell= natLayer.getCellByPosition(columnPosition, rowPosition);
				if (cell != null) {
					if (cell.isSpannedCell()) {
						spannedCells.add(cell);
					}
					else {
						paintCell(cell, gc, configRegistry);
					}
				}
			}
		}
		
		for (final LayerCell cell : spannedCells) {
			paintCell(cell, gc, configRegistry);
		}
	}
	
	private void calculateDimensionInfo(final LRectangle positionRectangle) {
		this.horizontalPositionToPixelMap= calculateDimensionInfo(
				this.natLayer.getDim(HORIZONTAL),
				positionRectangle.x, positionRectangle.x + positionRectangle.width);
		this.verticalPositionToPixelMap= calculateDimensionInfo(
				this.natLayer.getDim(VERTICAL),
				positionRectangle.y, positionRectangle.y + positionRectangle.height);
	}
	
	protected LRectangle getPositionRectangleFromPixelRectangle(final Layer natLayer, final Rectangle pixelRectangle) {
		final long columnPositionOffset= natLayer.getColumnPositionByX(pixelRectangle.x);
		final long rowPositionOffset= natLayer.getRowPositionByY(pixelRectangle.y);
		final long numColumns= natLayer.getColumnPositionByX(Math.min(natLayer.getWidth(), pixelRectangle.x + pixelRectangle.width) - 1) - columnPositionOffset + 1;
		final long numRows= natLayer.getRowPositionByY(Math.min(natLayer.getHeight(), pixelRectangle.y + pixelRectangle.height) - 1) - rowPositionOffset + 1;
		
		if (columnPositionOffset < 0 || rowPositionOffset < 0 || numColumns < 0 || numRows < 0) {
//			getPositionRectangleFromPixelRectangle(natLayer, pixelRectangle);
			throw new RuntimeException();
		}
		
		return new LRectangle(columnPositionOffset, rowPositionOffset, numColumns, numRows);
	}
	
	protected void paintCell(final LayerCell cell, final GC gc, final ConfigRegistry configRegistry) {
		final Layer layer= cell.getLayer();
		final long columnPosition= cell.getColumnPosition();
		final long rowPosition= cell.getRowPosition();
		
		final LayerCellPainter layerCellPainter= configRegistry.getAttribute(CellConfigAttributes.CELL_PAINTER,
				cell.getDisplayMode(), cell.getLabels().getLabels() );
		final LRectangle adjustedCellBounds= layer.getLayerPainter().adjustCellBounds(columnPosition, rowPosition, cell.getBounds());
		if (layerCellPainter != null) {
			final Rectangle originalClipping= gc.getClipping();
			
			final long startX= getColumnPositionStart(columnPosition);
			final long startY= getRowPositionStart(rowPosition);
			
			final long endX= getColumnPositionStart(cell.getOriginColumnPosition() + cell.getColumnSpan());
			final long endY= getRowPositionStart(cell.getOriginRowPosition() + cell.getRowSpan());
			
			final LRectangle clipBounds= new LRectangle(startX, startY, endX - startX, endY - startY);
			final LRectangle adjustedClipBounds= clipBounds.intersection(adjustedCellBounds);
//			LRectangle adjustedClipBounds= layer.getLayerPainter().adjustCellBounds(columnPosition, rowPosition, clipBounds);
			gc.setClipping(safe(adjustedClipBounds));
			
			layerCellPainter.paintCell(cell, gc, adjustedCellBounds, configRegistry);
			
			gc.setClipping(originalClipping);
		}
	}
	
	private long getColumnPositionStart(final long columnPosition) {
		return getPositionStart(this.natLayer.getDim(HORIZONTAL), columnPosition,
				this.horizontalPositionToPixelMap );
	}
	
	private long getRowPositionStart(final long rowPosition) {
		return getPositionStart(this.natLayer.getDim(VERTICAL), rowPosition,
				this.verticalPositionToPixelMap );
	}
	
}
