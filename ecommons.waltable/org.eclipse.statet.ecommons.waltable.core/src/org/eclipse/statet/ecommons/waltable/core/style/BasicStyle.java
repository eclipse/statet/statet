/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.style;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigAttribute;


/**
 * A style backed by a map.
 */
@NonNullByDefault
public class BasicStyle implements Style {
	
	
	private final Map<ConfigAttribute<?>, Object> configAttributeValueMap= new HashMap<>();
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAttributeValue(final ConfigAttribute<T> configAttribute) {
		return (T)this.configAttributeValueMap.get(configAttribute);
	}
	
	@Override
	public <T> void setAttributeValue(final ConfigAttribute<T> configAttribute, final @Nullable T value) {
		if (value != null) {
			this.configAttributeValueMap.put(configAttribute, value);
		}
		else {
			this.configAttributeValueMap.remove(configAttribute);
		}
	}
	
	
	@Override
	public String toString() {
		final StringBuilder resultBuilder= new StringBuilder();
		resultBuilder.append(this.getClass().getSimpleName() + ": "); //$NON-NLS-1$
		
		for (final Entry<ConfigAttribute<?>, Object> entry : this.configAttributeValueMap.entrySet()) {
			resultBuilder.append(entry.getKey() + ": " + entry.getValue() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
		return resultBuilder.toString();
	}
	
}
