/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.swt.painters;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;
import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


@NonNullByDefault
public class TableGridLayerPainter extends TableLayerPainter {
	
	
	private final Color gridColor;
	
	
	public TableGridLayerPainter(final NatTable natTable, final Color gridColor) {
		super(natTable);
		this.gridColor= gridColor;
	}
	
	
	@Override
	protected void paintBackground(final Layer natLayer, final GC gc, final long xOffset, final long yOffset,
			final Rectangle rectangle, final ConfigRegistry configRegistry) {
		super.paintBackground(natLayer, gc, xOffset, yOffset, rectangle, configRegistry);
		
		gc.setForeground(this.gridColor);
		drawHorizontalLines(natLayer, gc, rectangle);
		drawVerticalLines(natLayer, gc, rectangle);
	}
	
	private void drawHorizontalLines(final Layer natLayer, final GC gc,
			final Rectangle rectangle) {
		final int startX= rectangle.x;
		final int endX= rectangle.x + rectangle.width;
		
		final LayerDim dim= natLayer.getDim(VERTICAL);
		final long endPosition= CellLayerPainter.getEndPosition(dim, rectangle.y + rectangle.height);
		for (long position= dim.getPositionByPixel(rectangle.y); position < endPosition; position++) {
			final int size= dim.getPositionSize(position);
			if (size > 0) {
				final int y= safe(dim.getPositionStart(position) + size - 1);
				gc.drawLine(startX, y, endX, y);
			}
		}
	}
	
	private void drawVerticalLines(final Layer natLayer, final GC gc,
			final Rectangle rectangle) {
		final int startY= rectangle.y;
		final int endY= rectangle.y + rectangle.height;
		
		final LayerDim dim= natLayer.getDim(HORIZONTAL);
		final long endPosition= CellLayerPainter.getEndPosition(dim, rectangle.x + rectangle.width);
		for (long position= dim.getPositionByPixel(rectangle.x); position < endPosition; position++) {
			final int size= dim.getPositionSize(position);
			if (size > 0) {
				final int x= safe(dim.getPositionStart(position) + size - 1);
				gc.drawLine(x, startY, x, endY);
			}
		}
	}
	
}
