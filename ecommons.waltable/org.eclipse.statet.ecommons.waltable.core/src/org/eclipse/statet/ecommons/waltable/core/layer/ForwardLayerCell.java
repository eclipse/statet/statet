/*=============================================================================#
 # Copyright (c) 2012, 2025 Edwin Park and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Edwin Park - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.layer;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;


@NonNullByDefault
public class ForwardLayerCell extends BasicLayerCell {
	
	
	private final LayerCell underlyingCell;
	
	
	public ForwardLayerCell(final Layer layer,
			final LayerCellDim horizontalDim, final LayerCellDim verticalDim,
			final LayerCell underlyingCell) {
		super(layer, horizontalDim, verticalDim);
		
		this.underlyingCell= nonNullAssert(underlyingCell);
	}
	
	
	public LayerCell getUnderlyingCell() {
		return this.underlyingCell;
	}
	
	@Override
	public DisplayMode getDisplayMode() {
		return this.underlyingCell.getDisplayMode();
	}
	
	@Override
	public LabelStack getLabels() {
		return this.underlyingCell.getLabels();
	}
	
	
	@Override
	public @Nullable Object getDataValue(final int flags,
			final @Nullable IProgressMonitor monitor) {
		return this.underlyingCell.getDataValue(flags, monitor);
	}
	
}
