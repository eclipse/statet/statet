/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.layer.cell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;


@NonNullByDefault
public class SimpleLabelContributor implements CellLabelContributor {
	
	
	private final String configLabel;
	
	
	public SimpleLabelContributor(final String configLabel) {
		this.configLabel= configLabel;
	}
	
	
	@Override
	public void addLabels(final LabelStack configLabels, final long columnPosition, final long rowPosition) {
		configLabels.addLabel(this.configLabel);
	}
	
}
