/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.core.coordinate;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class PositionOutOfBoundsException extends IllegalArgumentException {
	
	
	private static final long serialVersionUID= 1L;
	
	
	public static PositionOutOfBoundsException refPosition(final long position,
			final Orientation orientation) {
		return new PositionOutOfBoundsException("refPosition", position, orientation); //$NON-NLS-1$
	}
	
	
	public PositionOutOfBoundsException(final String message) {
		super(message);
	}
	
	public PositionOutOfBoundsException(final String label, final long position) {
		super(label + ": " + position); //$NON-NLS-1$
	}
	
	public PositionOutOfBoundsException(final String label, final long position,
			final Orientation orientation) {
		super(label + " (" + orientation + "): " + position); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public PositionOutOfBoundsException(final long position) {
		super("position= " + position); //$NON-NLS-1$
	}
	
	public PositionOutOfBoundsException(final long position, final Orientation orientation) {
		super("position (" + orientation + ")= " + position); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
