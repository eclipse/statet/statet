/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.ui.menu;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Menu;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.config.AbstractUiBindingConfiguration;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;


public class DebugMenuConfiguration extends AbstractUiBindingConfiguration {

	private final Menu debugMenu;

	public DebugMenuConfiguration(final NatTable natTable) {
		this.debugMenu= new PopupMenuBuilder(natTable)
								.withInspectLabelsMenuItem()
								.build();

		natTable.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				DebugMenuConfiguration.this.debugMenu.dispose();
			}
		});
	}

	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		uiBindingRegistry.registerMouseDownBinding(
				new MouseEventMatcher(SWT.NONE, null, 3),
				new PopupMenuAction(this.debugMenu));
	}

}
