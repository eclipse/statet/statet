/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.conflation;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.events.VisualChangeEvent;


/**
 * Gathers all the VisualChangeEvents. When its run, it refreshes/repaints the table. 
 * 
 */
public class VisualChangeEventConflater extends AbstractEventConflater { 
	
	
	private final NatTable natTable;
	
	
	public VisualChangeEventConflater(final NatTable ownerLayer) {
		this.natTable= ownerLayer;
	}
	
	@Override
	public void addEvent(final LayerEvent event) {
		if (event instanceof VisualChangeEvent){
			super.addEvent(event);
		}
	}
	
	@Override
	public Runnable getConflaterTask() {
		return new Runnable() {
			
			@Override
			public void run() {
				if (VisualChangeEventConflater.this.queue.size() > 0) {
					VisualChangeEventConflater.this.natTable.getDisplay().asyncExec(new Runnable() {
						@Override
						public void run() {
							VisualChangeEventConflater.this.natTable.updateResize();
						}
					});
					
					clearQueue();
				}
			}
		};
	}

}
