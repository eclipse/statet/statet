/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.edit;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;
import org.eclipse.statet.ecommons.waltable.core.layer.events.CellVisualChangeEvent;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayer;
import org.eclipse.statet.internal.ecommons.waltable.WaLTablePlugin;


/**
 * {@link LayerCommandHandler} that handles {@link UpdateDataCommand}s by updating
 * the data model. It is usually directly registered to the {@link DataLayer} this
 * command handler is associated with.
 */
public class UpdateDataCommandHandler extends AbstractLayerCommandHandler<UpdateDataCommand> {
	
	
	/**
	 * The {@link DataLayer} on which the data model updates should be executed.
	 */
	private final DataLayer dataLayer;
	
	/**
	 * @param dataLayer The {@link DataLayer} on which the data model updates should be executed.
	 */
	public UpdateDataCommandHandler(final DataLayer dataLayer) {
		this.dataLayer= dataLayer;
	}
	
	@Override
	public Class<UpdateDataCommand> getCommandClass() {
		return UpdateDataCommand.class;
	}
	
	@Override
	protected boolean doCommand(final UpdateDataCommand command) {
		try {
			final long columnPosition= command.getColumnPosition();
			final long rowPosition= command.getRowPosition();
			final DataProvider dataProvider= this.dataLayer.getDataProvider();
			final Object oldValue= dataProvider.getDataValue(columnPosition, rowPosition, 0, null);
			final Object newValue= command.getNewValue();
			if ((oldValue != null) ? !oldValue.equals(newValue) : null != newValue)  {
				dataProvider.setDataValue(columnPosition, rowPosition, newValue);
				this.dataLayer.fireLayerEvent(new CellVisualChangeEvent(this.dataLayer, columnPosition, rowPosition));
				
				//TODO implement a new event which is a mix of PropertyUpdateEvent and CellVisualChangeEvent
			}
			return true;
		} catch (final UnsupportedOperationException e) {
			WaLTablePlugin.log(new Status(IStatus.ERROR, WaLTablePlugin.BUNDLE_ID,
					"Failed to update value to: " + command.getNewValue(), e ));
			return false;
		}
	}
	
}
