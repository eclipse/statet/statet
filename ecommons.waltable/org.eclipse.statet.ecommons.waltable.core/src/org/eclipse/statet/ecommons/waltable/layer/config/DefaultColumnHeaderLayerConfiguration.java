/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.layer.config;

import org.eclipse.statet.ecommons.waltable.config.AggregateConfiguration;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.ColumnHeaderLayer;
import org.eclipse.statet.ecommons.waltable.resize.ui.action.DefaultColumnResizeBindings;


/**
 * Sets up Column header styling and resize bindings.
 * Added by the {@link ColumnHeaderLayer}
 */
public class DefaultColumnHeaderLayerConfiguration extends AggregateConfiguration {

	public DefaultColumnHeaderLayerConfiguration() {
		addColumnHeaderStyleConfig();
		addColumnHeaderUIBindings();
	}

	protected void addColumnHeaderUIBindings() {
		addConfiguration(new DefaultColumnResizeBindings());
	}

	protected void addColumnHeaderStyleConfig() {
		addConfiguration(new DefaultColumnHeaderStyleConfiguration());
	}

}
