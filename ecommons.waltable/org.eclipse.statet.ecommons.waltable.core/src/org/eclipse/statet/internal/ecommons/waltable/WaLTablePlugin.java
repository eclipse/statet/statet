/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.waltable;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;


/**
 * The activator class for the plug-in life cycle
 */
public class WaLTablePlugin extends Plugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.ecommons.waltable.core"; //$NON-NLS-1$
	
	
	private static WaLTablePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static WaLTablePlugin getInstance() {
		return instance;
	}
	
	
	public static void log(final IStatus status) {
		final WaLTablePlugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	public WaLTablePlugin() {
		instance= this;
	}
	
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		super.stop(context);
		
		instance= null;
	}
	
}
