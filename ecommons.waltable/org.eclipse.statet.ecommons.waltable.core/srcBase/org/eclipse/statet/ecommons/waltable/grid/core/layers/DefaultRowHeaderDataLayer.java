/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.layers;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionId;
import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayer;


@NonNullByDefault
public class DefaultRowHeaderDataLayer extends DataLayer {
	
	
	public DefaultRowHeaderDataLayer(final DataProvider rowHeaderDataProvider) {
		super(rowHeaderDataProvider,
				PositionId.HEADER_CAT, 40,
				PositionId.BODY_CAT, 40);
	}
	
}
