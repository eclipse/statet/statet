/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.AbstractLayer;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectDimPositionsCommand;


@NonNullByDefault
public class ViewportSelectDimPositionsCommandHandler extends AbstractLayerCommandHandler<ViewportSelectDimPositionsCommand> {
	
	
	private final AbstractLayer viewportLayer;
	
	private final @Nullable Orientation orientation;
	
	
	public ViewportSelectDimPositionsCommandHandler(final AbstractLayer viewportLayer) {
		this(viewportLayer, null);
	}
	
	public ViewportSelectDimPositionsCommandHandler(final AbstractLayer viewportLayer,
			final @Nullable Orientation orientation) {
		this.viewportLayer= viewportLayer;
		this.orientation= orientation;
	}
	
	
	@Override
	public Class<ViewportSelectDimPositionsCommand> getCommandClass() {
		return ViewportSelectDimPositionsCommand.class;
	}
	
	
	@Override
	protected boolean doCommand(final ViewportSelectDimPositionsCommand command) {
		if (this.orientation != null && command.getOrientation() != this.orientation) {
			return false;
		}
		
		this.viewportLayer.doCommand(new SelectDimPositionsCommand(
				this.viewportLayer.getDim(command.getOrientation()),
				command.getRefPosition(), command.getPositions(), 0,
				command.getSelectionFlags(), command.getPositionToReveal() ));
		return true;
	}
	
}
