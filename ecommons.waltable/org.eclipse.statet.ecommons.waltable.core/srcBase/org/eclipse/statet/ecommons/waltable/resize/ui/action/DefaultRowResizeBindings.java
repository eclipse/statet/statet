/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.ui.action;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.ecommons.waltable.config.AbstractUiBindingConfiguration;
import org.eclipse.statet.ecommons.waltable.resize.PositionResizeDragMode;
import org.eclipse.statet.ecommons.waltable.resize.RowResizeEventMatcher;
import org.eclipse.statet.ecommons.waltable.ui.action.ClearCursorAction;
import org.eclipse.statet.ecommons.waltable.ui.action.NoOpMouseAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;


public class DefaultRowResizeBindings extends AbstractUiBindingConfiguration {
	
	
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		// Mouse move - Show resize cursor
		uiBindingRegistry.registerFirstMouseMoveBinding(new RowResizeEventMatcher(0), new RowResizeCursorAction());
		uiBindingRegistry.registerMouseMoveBinding(new MouseEventMatcher(), new ClearCursorAction());
		
		// Row resize
		uiBindingRegistry.registerFirstMouseDragMode(new RowResizeEventMatcher(1), new PositionResizeDragMode(VERTICAL));
		
		uiBindingRegistry.registerDoubleClickBinding(new RowResizeEventMatcher(1), new AutoResizeRowAction());
		uiBindingRegistry.registerSingleClickBinding(new RowResizeEventMatcher(1), new NoOpMouseAction());
	}
	
}
