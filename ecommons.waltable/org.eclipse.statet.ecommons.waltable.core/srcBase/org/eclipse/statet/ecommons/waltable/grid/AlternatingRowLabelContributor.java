/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.grid.config.DefaultRowStyleConfiguration;
import org.eclipse.statet.ecommons.waltable.layer.cell.CellLabelContributor;


/**
 * Applies 'odd'/'even' labels to all the rows. These labels are
 * the used to apply color to alternate rows.
 * 
 * @see DefaultRowStyleConfiguration
 */
@NonNullByDefault
public class AlternatingRowLabelContributor implements CellLabelContributor {
	
	public static final String ODD_ROW_CONFIG_TYPE= "BODY_ODD_ROW"; //$NON-NLS-1$
	
	public static final String EVEN_ROW_CONFIG_TYPE= "BODY_EVEN_ROW"; //$NON-NLS-1$
	
	
	
	public AlternatingRowLabelContributor() {
	}
	
	
	@Override
	public void addLabels(final LabelStack configLabels, final long columnPosition, final long rowPosition) {
		configLabels.addLabel((rowPosition % 2 == 0 ? EVEN_ROW_CONFIG_TYPE : ODD_ROW_CONFIG_TYPE));
	}
	
}
