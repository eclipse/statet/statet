/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractRelativeCommand;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Direction;


@NonNullByDefault
public abstract class AbstractSelectRelativeCommand extends AbstractRelativeCommand {
	
	
	private final int selectionFlags;
	
	
	protected AbstractSelectRelativeCommand(final Direction direction, final long stepCount, final int selectionFlags) {
		super(direction, stepCount);
		
		this.selectionFlags= selectionFlags;
	}
	
	
	public int getSelectionFlags() {
		return this.selectionFlags;
	}
	
}
