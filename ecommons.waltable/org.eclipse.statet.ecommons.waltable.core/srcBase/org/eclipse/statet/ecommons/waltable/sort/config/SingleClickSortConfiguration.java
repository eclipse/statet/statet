/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.config;

import org.eclipse.swt.SWT;

import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.sort.ColumnHeaderClickEventMatcher;
import org.eclipse.statet.ecommons.waltable.sort.ui.action.SortColumnAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;


/**
 * Modifies the default sort configuration to sort on a <i>single left</i>
 * click on the column header.
 */
public class SingleClickSortConfiguration extends DefaultSortConfiguration {

	public SingleClickSortConfiguration() {
		super();
	}
	
	public SingleClickSortConfiguration(final LayerCellPainter layerCellPainter) {
		super(layerCellPainter);
	}
	
	/**
	 * Remove the original key bindings and implement new ones.
	 */
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		// Register new bindings
		uiBindingRegistry.registerFirstSingleClickBinding(
              new ColumnHeaderClickEventMatcher(SWT.NONE, 1), new SortColumnAction(false));

		uiBindingRegistry.registerSingleClickBinding(
             MouseEventMatcher.columnHeaderLeftClick(SWT.ALT), new SortColumnAction(true));
	}

}
