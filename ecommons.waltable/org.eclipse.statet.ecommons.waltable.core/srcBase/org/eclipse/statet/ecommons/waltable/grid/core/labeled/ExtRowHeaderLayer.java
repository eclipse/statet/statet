/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.labeled;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.CompositeLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.PlaceholderLayer;


@NonNullByDefault
public class ExtRowHeaderLayer extends CompositeLayer {
	
	
	public ExtRowHeaderLayer(final Layer rowHeaderLayer) {
		super(2, 1);
		
		setChildLayer(GridLabels.ROW_HEADER, rowHeaderLayer, 0, 0);
		setChildLayer(GridLabels.HEADER_PLACEHOLDER, new PlaceholderLayer(null, rowHeaderLayer,
				false, rowHeaderLayer.getLayerPainter() ), 1, 0);
	}
	
	
	@Override
	protected boolean ignoreRef(final Orientation orientation) {
		return (orientation == HORIZONTAL);
	}
	
	
	public void setSpaceSize(final int pixel) {
		((PlaceholderLayer)getChildByLabel(GridLabels.HEADER_PLACEHOLDER).layer).setSize(pixel);
	}
	
}
