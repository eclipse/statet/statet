/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.ui.action;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LPoint;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectCellCommand;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionFlags;
import org.eclipse.statet.ecommons.waltable.ui.action.IDragMode;


/**
 * Fires commands to select a range of cells when the mouse is dragged in the viewport.
 */
public class CellSelectionDragMode implements IDragMode {
	
	
	private LPoint lastDragInCellPosition= null;
	
	
	public CellSelectionDragMode() {
	}
	
	
	@Override
	public void mouseDown(final NatTable natTable, final MouseEvent event) {
		natTable.forceFocus();
		this.lastDragInCellPosition= new LPoint(natTable.getColumnPositionByX(event.x), natTable.getRowPositionByY(event.y));
	}
	
	@Override
	public void mouseMove(final NatTable natTable, final MouseEvent event) {
		if (event.x > natTable.getWidth()) {
			return;
		}
		final long selectedColumnPosition= natTable.getColumnPositionByX(event.x);
		final long selectedRowPosition= natTable.getRowPositionByY(event.y);
		
		if (selectedColumnPosition > -1 && selectedRowPosition > -1) {
			final LPoint dragInCellPosition= new LPoint(selectedColumnPosition, selectedRowPosition);
			if (this.lastDragInCellPosition == null || !dragInCellPosition.equals(this.lastDragInCellPosition)) {
				this.lastDragInCellPosition= dragInCellPosition;
				
				fireSelectionCommand(natTable, selectedColumnPosition, selectedRowPosition, SelectionFlags.RANGE_SELECTION);
			}
		}
	}
	
	protected void fireSelectionCommand(final NatTable natTable, final long columnPosition,	final long rowPosition, final int selectionFlags) {
		natTable.doCommand(new SelectCellCommand(natTable, columnPosition, rowPosition, selectionFlags ));
	}
	
	@Override
	public void mouseUp(final NatTable natTable, final MouseEvent event) {
		endDrag();
	}
	
	private void endDrag(){
		this.lastDragInCellPosition= null;
	}
	
}
