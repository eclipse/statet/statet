/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.data;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;


@NonNullByDefault
public class DefaultColumnHeaderDataProvider implements DataProvider {
	
	
	private final ImList<String> columnLabels;
	
	
	public DefaultColumnHeaderDataProvider(final ImList<String> columnLabels) {
		this.columnLabels= columnLabels;
	}
	
	
	@Override
	public long getColumnCount() {
		return this.columnLabels.size();
	}
	
	@Override
	public long getRowCount() {
		return 1;
	}
	
	/**
	 * This class does not support multiple rows in the column header layer.
	 */
	@Override
	public Object getDataValue(final long columnIndex, final long rowIndex, final int flags,
			final @Nullable IProgressMonitor monitor) {
		if (columnIndex < 0 || columnIndex >= this.columnLabels.size()) {
			throw new IndexOutOfBoundsException();
		}
		return this.columnLabels.get((int)columnIndex);
	}
	
	@Override
	public void setDataValue(final long columnIndex, final long rowIndex, final @Nullable Object newValue) {
		throw new UnsupportedOperationException();
	}
	
}
