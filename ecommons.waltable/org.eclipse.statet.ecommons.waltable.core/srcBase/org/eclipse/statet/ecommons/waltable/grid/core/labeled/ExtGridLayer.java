/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.labeled;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLayer;


@NonNullByDefault
public class ExtGridLayer extends GridLayer {
	
	
	public static final String EXT_COLUMN_HEADER= "EXT_" + GridLabels.COLUMN_HEADER; //$NON-NLS-1$
	public static final String EXT_ROW_HEADER= "EXT_" + GridLabels.ROW_HEADER; //$NON-NLS-1$
	
	
	public ExtGridLayer(final Layer bodyLayer,
			final Layer columnHeaderLayer, final Layer rowHeaderLayer, final Layer cornerLayer,
			final boolean useDefaultConfiguration) {
		super(bodyLayer, columnHeaderLayer, rowHeaderLayer, cornerLayer, useDefaultConfiguration);
	}
	
	
	@Override
	public void setColumnHeaderLayer(final Layer columnHeaderLayer) {
		setChildLayer(EXT_COLUMN_HEADER, columnHeaderLayer, 1, 0);
	}
	
	@Override
	public void setRowHeaderLayer(final Layer rowHeaderLayer) {
		setChildLayer(EXT_ROW_HEADER, rowHeaderLayer, 0, 1);
	}
	
}
