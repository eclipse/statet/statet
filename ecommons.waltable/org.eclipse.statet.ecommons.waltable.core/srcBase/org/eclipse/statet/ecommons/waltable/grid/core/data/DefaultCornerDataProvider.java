/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.data;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;


@NonNullByDefault
public class DefaultCornerDataProvider implements DataProvider {
	
	
	private final DataProvider columnHeaderDataProvider;
	private final DataProvider rowHeaderDataProvider;
	
	
	public DefaultCornerDataProvider(final DataProvider columnHeaderDataProvider, final DataProvider rowHeaderDataProvider) {
		this.columnHeaderDataProvider= columnHeaderDataProvider;
		this.rowHeaderDataProvider= rowHeaderDataProvider;
	}
	
	
	@Override
	public long getColumnCount() {
		return this.rowHeaderDataProvider.getColumnCount();
	}
	
	@Override
	public long getRowCount() {
		return this.columnHeaderDataProvider.getRowCount();
	}
	
	@Override
	public Object getDataValue(final long columnIndex, final long rowIndex, final int flags, final IProgressMonitor monitor) {
		return ""; //$NON-NLS-1$
	}
	
	@Override
	public void setDataValue(final long columnIndex, final long rowIndex, final Object newValue) {
		throw new UnsupportedOperationException();
	}
	
}
