/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractDimPositionsCommand;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


/**
 * Command indicating that all selected columns have to be auto resized i.e made
 * wide enough to just fit the widest cell. This should also take the column
 * header into account
 * 
 * Note: The {@link InitializeAutoResizeCommand} has to be fired first
 * when autoresizing columns.
 */
@NonNullByDefault
public class AutoResizePositionsCommand extends AbstractDimPositionsCommand {
	
	
	public AutoResizePositionsCommand(final LayerDim layerDim, final LRangeList columnPositions) {
		super(layerDim, columnPositions);
	}
	
	protected AutoResizePositionsCommand(final AutoResizePositionsCommand command) {
		super(command);
	}
	
	@Override
	public LayerCommand cloneCommand() {
		return new AutoResizePositionsCommand(this);
	}
	
}
