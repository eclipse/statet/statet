/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LPoint;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;
import org.eclipse.statet.ecommons.waltable.ui.util.CellEdgeDetectUtil;


public class ColumnResizeEventMatcher extends MouseEventMatcher {
	
	
	public ColumnResizeEventMatcher(final int button, final boolean rowColumn) {
		super(SWT.NONE, GridLabels.COLUMN_HEADER, (rowColumn) ? GridLabels.CORNER : null, button);
	}
	
	public ColumnResizeEventMatcher(final int button) {
		this(SWT.NONE, GridLabels.COLUMN_HEADER, button);
	}
	
	public ColumnResizeEventMatcher(final int stateMask, final String eventRegion, final int button) {
		super(stateMask, eventRegion, button);
	}
	
	
	@Override
	public boolean matches(final NatTable natTable, final MouseEvent event, final LabelStack regionLabels) {
		return super.matches(natTable, event, regionLabels) && isResizable(natTable, event);
	}
	
	private boolean isResizable(final Layer natLayer, final MouseEvent event) {
		final long columnPosition= CellEdgeDetectUtil.getPositionToResize(natLayer,
				new LPoint(event.x, event.y), HORIZONTAL );
		
		return (columnPosition >= 0
				&& natLayer.getDim(HORIZONTAL).isPositionResizable(columnPosition) );
	}
	
}
