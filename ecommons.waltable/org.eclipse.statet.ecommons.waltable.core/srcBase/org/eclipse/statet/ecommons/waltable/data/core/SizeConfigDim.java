/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


@NonNullByDefault
public abstract class SizeConfigDim<TLayer extends Layer> extends DataLayerDim<TLayer> {
	
	
	private final SizeConfig sizeConfig;
	
	
	public SizeConfigDim(final TLayer layer, final Orientation orientation,
			final long idCat, final SizeConfig sizeConfig) {
		super(layer, orientation, idCat);
		
		this.sizeConfig= sizeConfig;
	}
	
	
	public void setResizableByDefault(final boolean resizableByDefault) {
		this.sizeConfig.setResizableByDefault(resizableByDefault);
	}
	
	public void setPositionResizable(final long rowPosition, final boolean resizable) {
		this.sizeConfig.setPositionResizable(rowPosition, resizable);
	}
	
	
	@Override
	public long getSize() {
		return this.sizeConfig.getAggregateSize(getPositionCount());
	}
	
	@Override
	public long getPositionStart(final long position) {
		return this.sizeConfig.getAggregateSize(position);
	}
	
	@Override
	public int getPositionSize(final long position) {
		return this.sizeConfig.getSize(position);
	}
	
	@Override
	public boolean isPositionResizable(final long position) {
		return this.sizeConfig.isPositionResizable(position);
	}
	
}
