/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


@NonNullByDefault
public class ClearSortCommandHandler implements LayerCommandHandler<ClearSortCommand> {
	
	
	private final SortModel sortModel;
	
	
	public ClearSortCommandHandler(final SortModel sortModel) {
		this.sortModel= sortModel;
	}
	
	
	@Override
	public Class<ClearSortCommand> getCommandClass() {
		return ClearSortCommand.class;
	}
	
	@Override
	public boolean doCommand(final Layer targetLayer, final ClearSortCommand command) {
		final LayerDim layerDim= targetLayer.getDim(HORIZONTAL);
		
		final List<Long> sortedIds= this.sortModel.getSortedColumnIds();
		final LRangeList sortedPositions= new LRangeList();
		for (final Long id : sortedIds) {
			final long position= layerDim.getPositionById(id);
			if (position != LayerDim.POSITION_NA) {
				sortedPositions.values().add(position);
			}
		}
		
		this.sortModel.clear();
		
		// Fire event
		final SortColumnEvent sortEvent= new SortColumnEvent(layerDim, sortedPositions);
		layerDim.getLayer().fireLayerEvent(sortEvent);
		
		return true;
	}
	
}
