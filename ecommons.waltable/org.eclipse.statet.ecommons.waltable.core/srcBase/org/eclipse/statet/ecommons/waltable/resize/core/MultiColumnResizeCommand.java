/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.command.LayerCommandUtil;
import org.eclipse.statet.ecommons.waltable.coordinate.ColumnPositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.command.AbstractDimPositionsCommand;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;


@NonNullByDefault
public class MultiColumnResizeCommand extends AbstractDimPositionsCommand {
	
	
	private int commonColumnWidth= -1;
	protected Map<ColumnPositionCoordinate, Integer> colPositionToWidth= new HashMap<>();
	
	
	/**
	 * All columns are being resized to the same width e.g. during a drag resize
	 */
	public MultiColumnResizeCommand(final Layer layer, final Collection<LRange> columnPositions, final int commonColumnWidth) {
		super(layer.getDim(HORIZONTAL), columnPositions);
		this.commonColumnWidth= commonColumnWidth;
	}
	
	/**
	 * Each column is being resized to a different size e.g. during auto resize
	 */
	public MultiColumnResizeCommand(final Layer layer, final long[] columnPositions, final int[] columnWidths) {
		super(layer.getDim(HORIZONTAL), new LRangeList(columnPositions));
		for (int i= 0; i < columnPositions.length; i++) {
			this.colPositionToWidth.put(new ColumnPositionCoordinate(layer, columnPositions[i]), Integer.valueOf(columnWidths[i]));
		}
	}
	
	protected MultiColumnResizeCommand(final MultiColumnResizeCommand command) {
		super(command);
		this.commonColumnWidth= command.commonColumnWidth;
		this.colPositionToWidth= new HashMap<>(command.colPositionToWidth);
	}
	
	@Override
	public MultiColumnResizeCommand cloneCommand() {
		return new MultiColumnResizeCommand(this);
	}
	
	
	public long getCommonColumnWidth() {
		return this.commonColumnWidth;
	}
	
	public int getColumnWidth(final long columnPosition) {
		for (final ColumnPositionCoordinate columnPositionCoordinate : this.colPositionToWidth.keySet()) {
			if (columnPositionCoordinate.getColumnPosition() == columnPosition) {
				return this.colPositionToWidth.get(columnPositionCoordinate).intValue();
			}
		}
		return this.commonColumnWidth;
	}
	
	@Override
	public boolean convertToTargetLayer(final Layer targetLayer) {
		if (super.convertToTargetLayer(targetLayer)) {
			// Ensure that the width associated with the column is now associated with the converted 
			// column position.
			final Map<ColumnPositionCoordinate, Integer> targetColPositionToWidth= new HashMap<>();
			
			for (final ColumnPositionCoordinate columnPositionCoordinate : this.colPositionToWidth.keySet()) {
				final ColumnPositionCoordinate targetColumnPositionCoordinate= LayerCommandUtil.convertColumnPositionToTargetContext(columnPositionCoordinate, targetLayer);
				if (targetColumnPositionCoordinate != null) {
					targetColPositionToWidth.put(targetColumnPositionCoordinate, this.colPositionToWidth.get(columnPositionCoordinate));
				}
			}
			
			this.colPositionToWidth= targetColPositionToWidth;
			return true;
		}
		return false;
	}
	
}
