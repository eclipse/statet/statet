/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core;

import org.eclipse.swt.widgets.Scrollable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;


/**
 * Command that gives the layers access to ClientArea and the Scrollable 
 */
@NonNullByDefault
public class ClientAreaResizeCommand extends AbstractContextFreeCommand {
	
	
	/**
	 * The {@link Scrollable}, normally the NatTable itself.
	 */
	private final Scrollable scrollable;
	
	/**
	 * This is the area within the client area that is used for percentage calculation.
	 * Without using a GridLayer, this will be the client area of the scrollable.
	 * On using a GridLayer this value will be overriden with the body region area.
	 */
	private @Nullable LRectangle calcArea;
	
	
	public ClientAreaResizeCommand(final Scrollable scrollable) {
		super();
		this.scrollable= scrollable;
	}
	
	
	public Scrollable getScrollable() {
		return this.scrollable;
	}
	
	public LRectangle getCalcArea() {
		final LRectangle calcArea= this.calcArea;
		if (calcArea == null) {
			return SwtUtils.toNatTable(this.scrollable.getClientArea());
		}
		return calcArea;
	}
	
	public void setCalcArea(final LRectangle calcArea) {
		this.calcArea= calcArea;
	}
	
}
