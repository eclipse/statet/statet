/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.config;

import org.eclipse.swt.SWT;

import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.config.IConfiguration;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.painter.cell.decorator.BeveledBorderDecorator;
import org.eclipse.statet.ecommons.waltable.sort.swt.SortableHeaderTextPainter;
import org.eclipse.statet.ecommons.waltable.sort.ui.action.SortColumnAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;


public class DefaultSortConfiguration implements IConfiguration {
	
	public static final String SORT_DOWN_CONFIG_TYPE= "SORT_DOWN"; //$NON-NLS-1$
	public static final String SORT_UP_CONFIG_TYPE= "SORT_UP"; //$NON-NLS-1$
	
	/** The sort sequence can be appended to this base */
	public static final String SORT_SEQ_CONFIG_TYPE= "SORT_SEQ_"; //$NON-NLS-1$
	
	private final LayerCellPainter layerCellPainter;
	
	public DefaultSortConfiguration() {
		this(new BeveledBorderDecorator(new SortableHeaderTextPainter()));
	}
	
	public DefaultSortConfiguration(final LayerCellPainter layerCellPainter) {
		this.layerCellPainter= layerCellPainter;
	}
	
	@Override
	public void configureLayer(final Layer layer) {}
	
	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.layerCellPainter, DisplayMode.NORMAL, SORT_DOWN_CONFIG_TYPE);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.layerCellPainter, DisplayMode.NORMAL, SORT_UP_CONFIG_TYPE);
	}
	
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		uiBindingRegistry.registerSingleClickBinding(
				new MouseEventMatcher(SWT.ALT, GridLabels.COLUMN_HEADER.toString(), 1),	new SortColumnAction(false));
		
		uiBindingRegistry.registerSingleClickBinding(
				new MouseEventMatcher(SWT.ALT | SWT.MOD2, GridLabels.COLUMN_HEADER.toString(), 1), new SortColumnAction(true));
	}
	
}
