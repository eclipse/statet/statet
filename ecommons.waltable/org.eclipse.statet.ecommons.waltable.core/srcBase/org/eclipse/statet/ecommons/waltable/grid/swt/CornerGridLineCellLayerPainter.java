/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.swt;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;
import static org.eclipse.statet.ecommons.waltable.painter.cell.GraphicsUtils.safe;

import java.util.function.Function;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.swt.painters.GridLineCellLayerPainter;


@NonNullByDefault
public class CornerGridLineCellLayerPainter extends GridLineCellLayerPainter {
	
	
	public CornerGridLineCellLayerPainter(final Function<ConfigRegistry, Color> gridLineColorSupplier) {
		super(gridLineColorSupplier);
	}
	
	public CornerGridLineCellLayerPainter() {
		super();
	}
	
	
	private Layer getCornerLayer(final Layer layer) {
		return layer.getUnderlyingLayerByPosition(0, 0).getUnderlyingLayerByPosition(0, 0);
	}
	
	
	@Override
	protected void drawHorizontalLines(final Layer natLayer, final GC gc, final Rectangle rectangle) {
		final int startX, endX;
		{	final LayerDim hDim= natLayer.getDim(HORIZONTAL);
			startX= safe(rectangle.x + hDim.getPositionStart(
					getCornerLayer(natLayer).getColumnCount() - 1 ) - 1 );
			endX= safe(rectangle.x + Math.min(hDim.getSize() - 1, rectangle.width));
		}
		
		final LayerDim dim= natLayer.getDim(VERTICAL);
		final long lastPosition= getEndPosition(dim, rectangle.y + rectangle.height) - 1;
		if (startX < endX) {
			for (long position= dim.getPositionByPixel(rectangle.y); position < lastPosition; position++) {
				final int size= dim.getPositionSize(position);
				if (size > 0) {
					final int y= safe(dim.getPositionStart(position) + dim.getPositionSize(position) - 1);
					gc.drawLine(startX, y, endX, y);
				}
			}
		}
		{	final int y= safe(dim.getPositionStart(lastPosition) + dim.getPositionSize(lastPosition) - 1);
			gc.drawLine(safe(rectangle.x), y, endX, y);
		}
	}
	
	@Override
	protected void drawVerticalLines(final Layer natLayer, final GC gc, final Rectangle rectangle) {
		final int startY, endY;
		{	final LayerDim vDim= natLayer.getDim(VERTICAL);
			startY= safe(rectangle.y + vDim.getPositionStart(
					getCornerLayer(natLayer).getRowCount() - 1 ) - 1 );
			endY= safe(rectangle.y + Math.min(vDim.getSize() - 1, rectangle.height));
		}
		
		final LayerDim dim= natLayer.getDim(HORIZONTAL);
		final long lastPosition= getEndPosition(dim, rectangle.x + rectangle.width) - 1;
		if (startY < endY) {
			for (long position= dim.getPositionByPixel(rectangle.x); position < lastPosition; position++) {
				final int size= dim.getPositionSize(position);
				if (size > 0) {
					final int x= safe(dim.getPositionStart(position) + size - 1);
					gc.drawLine(x, startY, x, endY);
				}
			}
		}
		{	final int x= safe(dim.getPositionStart(lastPosition) + dim.getPositionSize(lastPosition) - 1);
			gc.drawLine(x, safe(rectangle.y), x, endY);
		}
	}
	
}
