/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.ui.action;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.statet.ecommons.waltable.config.AbstractUiBindingConfiguration;
import org.eclipse.statet.ecommons.waltable.resize.ColumnResizeEventMatcher;
import org.eclipse.statet.ecommons.waltable.resize.PositionResizeDragMode;
import org.eclipse.statet.ecommons.waltable.ui.action.ClearCursorAction;
import org.eclipse.statet.ecommons.waltable.ui.action.NoOpMouseAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;


public class DefaultColumnResizeBindings extends AbstractUiBindingConfiguration {
	
	
	@Override
	public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
		// Mouse move - Show resize cursor
		uiBindingRegistry.registerFirstMouseMoveBinding(new ColumnResizeEventMatcher(0), new ColumnResizeCursorAction());
		uiBindingRegistry.registerMouseMoveBinding(new MouseEventMatcher(), new ClearCursorAction());
		
		// Column resize
		uiBindingRegistry.registerFirstMouseDragMode(new ColumnResizeEventMatcher(1), new PositionResizeDragMode(HORIZONTAL));
		
		uiBindingRegistry.registerDoubleClickBinding(new ColumnResizeEventMatcher(1), new AutoResizeColumnAction());
		uiBindingRegistry.registerSingleClickBinding(new ColumnResizeEventMatcher(1), new NoOpMouseAction());
	}
	
}
