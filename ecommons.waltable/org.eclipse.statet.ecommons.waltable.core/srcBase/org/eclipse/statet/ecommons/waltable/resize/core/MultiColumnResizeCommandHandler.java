/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList.ValueIterator;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayer;


@NonNullByDefault
public class MultiColumnResizeCommandHandler extends AbstractLayerCommandHandler<MultiColumnResizeCommand> {
	
	
	private final DataLayer dataLayer;
	
	
	public MultiColumnResizeCommandHandler(final DataLayer dataLayer) {
		this.dataLayer= dataLayer;
	}
	
	@Override
	public Class<MultiColumnResizeCommand> getCommandClass() {
		return MultiColumnResizeCommand.class;
	}
	
	@Override
	protected boolean doCommand(final MultiColumnResizeCommand command) {
		for (final var columnIter= new ValueIterator(command.getPositions()); columnIter.hasNext(); ) {
			final long columnPosition= columnIter.nextLong();
			this.dataLayer.setColumnWidthByPosition(columnPosition, command.getColumnWidth(columnPosition));
		}
		return true;
	}
	
}
