/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.util.data;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LPoint;
import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;


public class DummyBodyDataProvider implements DataProvider {
	
	
	private final long columnCount;
	
	private final long rowCount;
	
	private final Map<LPoint, Object> values= new HashMap<>();
	
	
	public DummyBodyDataProvider(final long columnCount, final long rowCount) {
		this.columnCount= columnCount;
		this.rowCount= rowCount;
	}
	
	
	@Override
	public long getColumnCount() {
		return this.columnCount;
	}
	
	@Override
	public long getRowCount() {
		return this.rowCount;
	}
	
	@Override
	public Object getDataValue(final long columnIndex, final long rowIndex, final int flags, final IProgressMonitor monitor) {
		final LPoint lPoint= new LPoint(columnIndex, rowIndex);
		if (this.values.containsKey(lPoint)) {
			return this.values.get(lPoint);
		}
		else {
			return "Col: " + (columnIndex + 1) + ", Row: " + (rowIndex + 1); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
	
	@Override
	public void setDataValue(final long columnIndex, final long rowIndex, final Object newValue) {
		this.values.put(new LPoint(columnIndex, rowIndex), newValue);
	}
	
}
