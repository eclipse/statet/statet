/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.ui.action;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.sort.core.SortDimPositionCommand;
import org.eclipse.statet.ecommons.waltable.ui.NatEventData;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;


public class SortColumnAction implements IMouseAction {
	
	
	private final boolean accumulate;
	
	
	public SortColumnAction(final boolean accumulate) {
		this.accumulate= accumulate;
	}
	
	@Override
	public void run(final NatTable natTable, final MouseEvent event) {
		final long columnPosition= ((NatEventData)event.data).getColumnPosition();
		natTable.doCommand(new SortDimPositionCommand(natTable.getDim(HORIZONTAL),
				columnPosition, this.accumulate ));
	}
	
}
