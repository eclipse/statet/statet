/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.NatTableLayerDim;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LPoint;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.top.OverlayPainter;
import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;
import org.eclipse.statet.ecommons.waltable.resize.core.DimPositionResizeCommand;
import org.eclipse.statet.ecommons.waltable.ui.action.IDragMode;
import org.eclipse.statet.ecommons.waltable.ui.util.CellEdgeDetectUtil;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


/**
 * Drag mode that will implement the column/row resizing process.
 */
public class PositionResizeDragMode implements IDragMode {
	
	
	private static final int DEFAULT_WIDTH_MINIMUM= 25;
	
	private static final int RESIZE_OVERLAY_SIZE= 2;
	
	
	private static void addPositions(final long pixel, final LayerDim dim, final LRangeList positions) {
		final long startPixel= Math.max(pixel - RESIZE_OVERLAY_SIZE / 2, 0);
		final long endPixel= Math.min(pixel + RESIZE_OVERLAY_SIZE / 2, dim.getSize() - 1);
		if (startPixel < endPixel) {
			positions.add(new LRange(dim.getPositionByPixel(startPixel), dim.getPositionByPixel(endPixel) + 1));
		}
	}
	
	
	private class ColumnResizeOverlayPainter implements OverlayPainter {
		
		@Override
		public void paintOverlay(final GC gc, final Layer layer) {
			final Color originalBackgroundColor= gc.getBackground();
			gc.setBackground(GUIHelper.COLOR_DARK_GRAY);
			gc.fillRectangle(PositionResizeDragMode.this.currentPixel - (RESIZE_OVERLAY_SIZE / 2), 0, RESIZE_OVERLAY_SIZE, SwtUtils.toSWT(layer.getHeight()));
			gc.setBackground(originalBackgroundColor);
		}
		
	}
	
	private class RowResizeOverlayPainter implements OverlayPainter {
		
		@Override
		public void paintOverlay(final GC gc, final Layer layer) {
			final Color originalBackgroundColor= gc.getBackground();
			gc.setBackground(GUIHelper.COLOR_DARK_GRAY);
			gc.fillRectangle(0, PositionResizeDragMode.this.currentPixel - (RESIZE_OVERLAY_SIZE / 2), SwtUtils.toSWT(layer.getWidth()), RESIZE_OVERLAY_SIZE);
			gc.setBackground(originalBackgroundColor);
		}
	}
	
	
	private final Orientation orientation;
	
	private long positionToResize;
	
	private int positionStart;
	private int positionSize;
	
	private int startPixel;
	private int currentPixel;
	private int lastPixel= -1;
	
	private final OverlayPainter overlayPainter;
	
	
	public PositionResizeDragMode(final Orientation orientation) {
		this.orientation= orientation;
		
		this.overlayPainter= (orientation == HORIZONTAL) ?
				new ColumnResizeOverlayPainter() :
				new RowResizeOverlayPainter();
	}
	
	
	// XXX: This method must ask the layer what it's minimum width is!
	private int getMinPositionSize() {
		return DEFAULT_WIDTH_MINIMUM;
	}
	
	
	@Override
	public void mouseDown(final NatTable natTable, final MouseEvent event) {
		natTable.forceFocus();
		this.positionToResize= CellEdgeDetectUtil.getPositionToResize(natTable,
				new LPoint(event.x, event.y), this.orientation );
		if (this.positionToResize >= 0) {
			final LayerDim layerDim= natTable.getDim(this.orientation);
			
			this.positionStart= SwtUtils.toSWT(layerDim.getPositionStart(this.positionToResize));
			this.positionSize= layerDim.getPositionSize(this.positionToResize);
			this.startPixel= SwtUtils.getPixel(event, this.orientation);
			
			natTable.addOverlayPainter(this.overlayPainter);
		}
	}
	
	@Override
	public void mouseMove(final NatTable natTable, final MouseEvent event) {
		final NatTableLayerDim layerDim= natTable.getDim(this.orientation);
		
		final int pixel= SwtUtils.getPixel(event, this.orientation);
		if (pixel > layerDim.getSize()) {
			return;
		}
		
		if (pixel < this.positionStart + getMinPositionSize()) {
			this.currentPixel= this.positionStart + getMinPositionSize();
		}
		else {
			this.currentPixel= pixel;
			
			final LRangeList positionsToRepaint= new LRangeList();
			
			addPositions(this.currentPixel, layerDim, positionsToRepaint);
			if (this.lastPixel >= 0) {
				addPositions(this.lastPixel, layerDim, positionsToRepaint);
			}
			for (final LRange positions : positionsToRepaint) {
				layerDim.repaintPositions(positions);
			}
			
			this.lastPixel= this.currentPixel;
		}
	}
	
	@Override
	public void mouseUp(final NatTable natTable, final MouseEvent event) {
		final NatTableLayerDim layerDim= natTable.getDim(this.orientation);
		
		natTable.removeOverlayPainter(this.overlayPainter);
		
		updatePositionSize(layerDim, SwtUtils.getPixel(event, this.orientation));
	}
	
	private void updatePositionSize(final LayerDim layerDim, final int pixel) {
		final int dragSize= pixel - this.startPixel;
		int newSize= this.positionSize + dragSize;
		if (newSize < getMinPositionSize()) {
			newSize= getMinPositionSize();
		}
		
		layerDim.getLayer().doCommand(
				new DimPositionResizeCommand(layerDim, this.positionToResize, newSize) );
	}
	
}
