/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.config;

import org.eclipse.statet.ecommons.waltable.config.AggregateConfiguration;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;
import org.eclipse.statet.ecommons.waltable.tickupdate.config.DefaultTickUpdateConfiguration;


/**
 * Sets up default styling and UI bindings. Override the methods in here to
 * customize behavior. Added by the {@link SelectionLayer}
 */
public class DefaultSelectionLayerConfiguration extends AggregateConfiguration {

	public DefaultSelectionLayerConfiguration() {
		addSelectionStyleConfig();
		addSelectionUIBindings();
		addTickUpdateConfig();
		addMoveSelectionConfig();
	}

	protected void addSelectionStyleConfig() {
		addConfiguration(new DefaultSelectionStyleConfiguration());
	}

	protected void addSelectionUIBindings() {
		addConfiguration(new DefaultSelectionBindings());
	}

	protected void addTickUpdateConfig() {
		addConfiguration(new DefaultTickUpdateConfiguration());
	}

	protected void addMoveSelectionConfig() {
		addConfiguration(new DefaultMoveSelectionConfiguration());
	}
}
