/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionOutOfBoundsException;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.TransformLayerDim;


/**
 * Dimension of a viewport layer.
 */
@NonNullByDefault
public abstract class ViewportLayerDim extends TransformLayerDim<ViewportLayer> {
	
	
	public ViewportLayerDim(final ViewportLayer layer, final LayerDim underlyingDim) {
		super(layer, underlyingDim);
	}
	
	
	/**
	 * Returns the underlying scrollable layer dimension.
	 * 
	 * @return the underlying scrollable layer dimension
	 */
	public abstract LayerDim getScrollable();
	
	
	/**
	 * Returns the minimum origin.
	 * 
	 * @return the pixel coordinate of the minimum origin in terms of the scrollable layer
	 */
	public abstract long getMinimumOriginPixel();
	
	/**
	 * Returns the minimum origin.
	 * 
	 * @return the position of the minimum origin in terms of the scrollable layer
	 */
	public abstract long getMinimumOriginPosition();
	
	/**
	 * Sets the minimum origin.
	 * 
	 * If required, the current origin is adapted to the next valid value; otherwise the origin is
	 * not changed.
	 * 
	 * @param scrollablePosition position of the origin in terms of the scrollable layer
	 */
	public abstract void setMinimumOriginPosition(long scrollablePosition);
	
	
	/**
	 * Returns the current origin.
	 * 
	 * @return the pixel coordinate of the current origin in terms of the scrollable layer
	 */
	public abstract long getOriginPixel();
	
	/**
	 * Returns the current origin.
	 * 
	 * @return the position of the current origin in terms of the scrollable layer
	 */
	public abstract long getOriginPosition();
	
	/**
	 * Sets the current origin.
	 * 
	 * If required, the specified origin is adjusted to the next valid value.
	 * 
	 * @param scrollablePixel pixel coordinate of the origin in terms of the scrollable layer
	 */
	public abstract void setOriginPixel(long scrollablePixel);
	
	/**
	 * Sets the current origin.
	 * 
	 * @param scrollablePixel position of the origin in terms of the scrollable layer
	 * 
	 * @throws PositionOutOfBoundsException if scrollablePosition is not a valid position
	 */
	public abstract void setOriginPosition(long scrollablePosition);
	
	/**
	 * Resets the viewport dimension.
	 * 
	 * Resets all settings, sets the minimum origin position to 0 and sets the origin to the 
	 * specified position.
	 * 
	 * @param scrollablePosition position in terms of the scrollable layer
	 * 
	 * @throws PositionOutOfBoundsException if scrollablePosition is not a valid position
	 */
	public abstract void reset(long scrollablePosition);
	
	
	/**
	 * Scrolls the viewport (if required) so that the specified position is visible.
	 * 
	 * @param scrollablePosition position in terms of the scrollable layer
	 */
	public abstract void movePositionIntoViewport(long scrollablePosition);
	
	
	/**
	 * Scrolls the viewport content backward by one step.
	 */
	public abstract void scrollBackwardByStep();
	
	/**
	 * Scrolls the viewport content forward by one step.
	 */
	public abstract void scrollForwardByStep();
	
	
	/**
	 * Scrolls the viewport content backward by one position.
	 */
	public abstract void scrollBackwardByPosition();
	
	/**
	 * Scrolls the viewport content forward by one position.
	 */
	public abstract void scrollForwardByPosition();
	
	
	/**
	 * Scrolls the viewport content backward by one viewport page.
	 */
	public abstract void scrollBackwardByPage();
	
	/**
	 * Scrolls the viewport content forward by one viewport page.
	 */
	public abstract void scrollForwardByPage();
	
	
	/**
	 * Scrolls the viewport content to the first position (minimum origin).
	 */
	public abstract void scrollBackwardToBound();
	
	/**
	 * Scrolls the viewport content to the last position.
	 */
	public abstract void scrollForwardToBound();
	
}
