/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.config;

import org.eclipse.statet.ecommons.waltable.config.AggregateConfiguration;
import org.eclipse.statet.ecommons.waltable.core.layer.CompositeLayer;
import org.eclipse.statet.ecommons.waltable.edit.config.DefaultEditBindings;
import org.eclipse.statet.ecommons.waltable.edit.config.DefaultEditConfiguration;
import org.eclipse.statet.ecommons.waltable.export.config.DefaultExportBindings;
import org.eclipse.statet.ecommons.waltable.grid.AlternatingRowLabelContributor;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLayer;
import org.eclipse.statet.ecommons.waltable.print.config.DefaultPrintBindings;


/**
 * Sets up features handled at the grid level. Added by {@link GridLayer}
 */
public class DefaultGridLayerConfiguration extends AggregateConfiguration {

	public DefaultGridLayerConfiguration(final CompositeLayer gridLayer) {
		addAlternateRowColoringConfig(gridLayer);
		addEditingHandlerConfig();
		addEditingUIConfig();
		addPrintUIBindings();
		addExcelExportUIBindings();
	}

	protected void addExcelExportUIBindings() {
		addConfiguration(new DefaultExportBindings());
	}

	protected void addPrintUIBindings() {
		addConfiguration(new DefaultPrintBindings());
	}

	protected void addEditingUIConfig() {
		addConfiguration(new DefaultEditBindings());
	}

	protected void addEditingHandlerConfig() {
		addConfiguration(new DefaultEditConfiguration());
	}

	protected void addAlternateRowColoringConfig(final CompositeLayer gridLayer) {
		addConfiguration(new DefaultRowStyleConfiguration());
		gridLayer.addCellLabelContributor(GridLabels.BODY, new AlternatingRowLabelContributor());
	}

}
