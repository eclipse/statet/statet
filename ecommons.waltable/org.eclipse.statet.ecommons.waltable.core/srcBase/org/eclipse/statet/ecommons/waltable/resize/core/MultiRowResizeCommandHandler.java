/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList.ValueIterator;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayer;


@NonNullByDefault
public class MultiRowResizeCommandHandler extends AbstractLayerCommandHandler<MultiRowResizeCommand> {
	
	
	private final DataLayer dataLayer;
	
	
	public MultiRowResizeCommandHandler(final DataLayer dataLayer) {
		this.dataLayer= dataLayer;
	}
	
	@Override
	public Class<MultiRowResizeCommand> getCommandClass() {
		return MultiRowResizeCommand.class;
	}
	
	@Override
	protected boolean doCommand(final MultiRowResizeCommand command) {
		for (final var rowIter= new ValueIterator(command.getPositions()); rowIter.hasNext(); ) {
			final long rowPosition= rowIter.nextLong();
			this.dataLayer.setRowHeightByPosition(rowPosition, command.getRowHeight(rowPosition));
		}
		return true;
	}
	
}
