/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.layers;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.ForwardLayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;


/**
 * Responsible for rendering, event handling etc on the column/row headers.
 */
@NonNullByDefault
public class AbstractPositionHeaderLayer extends DimensionallyDependentLayer {
	
	
	private final Orientation headerOrientation;
	
	private final SelectionLayer selectionLayer;
	private final String fullySelectedLabel;
	
	
	/**
	 * @param baseLayer
	 *            The data provider for this layer
	 * @param contentLayerDependency
	 *            The layer to link the horizontal dimension to, typically the body layer
	 * @param selectionLayer
	 *            The selection layer required to respond to selection events
	 * @param layerPainter
	 *            The painter for this layer or <code>null</code> to use the painter of the base layer
	 */
	public AbstractPositionHeaderLayer(final Layer baseLayer, final Orientation orientation,
			final Layer contentLayerDependency,
			final SelectionLayer selectionLayer, final String fullySelectedLabel,
			final LayerPainter layerPainter) {
		super(baseLayer,
				(orientation == HORIZONTAL) ? contentLayerDependency : baseLayer,
				(orientation == VERTICAL) ? contentLayerDependency : baseLayer,
				layerPainter );
		this.headerOrientation= nonNullAssert(orientation);
		this.selectionLayer= nonNullAssert(selectionLayer);
		this.fullySelectedLabel= nonNullAssert(fullySelectedLabel);
	}
	
	
	@Override
	protected LayerCell createCell(final LayerCellDim hDim, final LayerCellDim vDim, final LayerCell underlyingCell) {
		return new ForwardLayerCell(this, hDim, vDim, underlyingCell) {
			
			@Override
			public DisplayMode getDisplayMode() {
				if (isSelected(getDim(AbstractPositionHeaderLayer.this.headerOrientation))) {
					return DisplayMode.SELECTED;
				}
				return super.getDisplayMode();
			}
			
			@Override
			public LabelStack getLabels() {
				final LabelStack configLabels= super.getLabels();
				
				if (isFullySelected(getDim(AbstractPositionHeaderLayer.this.headerOrientation))) {
					configLabels.addLabel(AbstractPositionHeaderLayer.this.fullySelectedLabel);
				}
				
				return configLabels;
			}
			
		};
	}
	
	protected boolean isSelected(final LayerCellDim dim) {
		final LayerDim layerDim= getDim(this.headerOrientation);
		final long position= dim.getPosition();
		if (this.selectionLayer.isPositionSelected(this.headerOrientation,
				LayerUtils.convertPosition(layerDim, position, position,
						this.selectionLayer.getDim(this.headerOrientation) ) )) {
			return true;
		}
		if (dim.getPositionSpan() > 1) {
			long iPosition= dim.getOriginPosition();
			final long endPosition= iPosition + dim.getPositionSpan();
			for (; iPosition < endPosition; iPosition++) {
				if (iPosition != position
						&& this.selectionLayer.isPositionSelected(this.headerOrientation,
								LayerUtils.convertPosition(layerDim, position, iPosition,
										this.selectionLayer.getDim(this.headerOrientation) ))) {
					return true;
				}
			}
		}
		return false;
	}
	
	protected boolean isFullySelected(final LayerCellDim dim) {
		final LayerDim layerDim= getDim(this.headerOrientation);
		final long position= dim.getPosition();
		if (!this.selectionLayer.isPositionFullySelected(this.headerOrientation,
				LayerUtils.convertPosition(layerDim, position, position,
						this.selectionLayer.getDim(this.headerOrientation)) )) {
			return false;
		}
		if (dim.getPositionSpan() > 1) {
			long iPosition= dim.getOriginPosition();
			final long endPosition= iPosition + dim.getPositionSpan();
			for (; iPosition < endPosition; iPosition++) {
				if (iPosition != position
						&& !this.selectionLayer.isPositionFullySelected(this.headerOrientation,
						LayerUtils.convertPosition(layerDim, position, iPosition,
								this.selectionLayer.getDim(this.headerOrientation) ))) {
					return false;
				}
			}
		}
		return true;
	}
	
}
