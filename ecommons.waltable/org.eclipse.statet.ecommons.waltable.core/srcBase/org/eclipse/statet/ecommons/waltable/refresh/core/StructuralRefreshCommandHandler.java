/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.refresh.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.events.GeneralStructuralChangeEvent;


/**
 * Command handler for handling {@link StructuralRefreshCommand}s.
 * Simply fires a {@link GeneralStructuralChangeEvent}.
 * 
 * Needed to be able to refresh all layers by simply calling a command on the NatTable
 * instance itself (Remember that events are fired bottom up the layer stack while commands
 * are propagated top down). 
 * 
 * To refresh all layers by calling a {@link StructuralRefreshCommand} on the NatTable
 * instance, the {@link StructuralRefreshCommandHandler} should be registered against
 * the DataLayer.
 */
@NonNullByDefault
public class StructuralRefreshCommandHandler implements LayerCommandHandler<StructuralRefreshCommand> {
	
	
	private final Layer layer;
	
	
	public StructuralRefreshCommandHandler(final Layer layer) {
		this.layer= layer;
	}
	
	
	@Override
	public Class<StructuralRefreshCommand> getCommandClass() {
		return StructuralRefreshCommand.class;
	}
	
	@Override
	public boolean executeCommand(final StructuralRefreshCommand command) {
		this.layer.fireLayerEvent(new GeneralStructuralChangeEvent(this.layer));
		return false;
	}
	
}
