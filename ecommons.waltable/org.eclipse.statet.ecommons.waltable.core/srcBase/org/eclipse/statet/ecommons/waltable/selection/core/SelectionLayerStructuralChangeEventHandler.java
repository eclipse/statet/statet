/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerListener;
import org.eclipse.statet.ecommons.waltable.core.layer.events.StructuralChangeEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.events.StructuralDiff;
import org.eclipse.statet.ecommons.waltable.core.layer.events.StructuralDiff.DiffType;


@NonNullByDefault
public class SelectionLayerStructuralChangeEventHandler implements LayerListener {
	
	
	private final SelectionModel selectionModel;
	private final SelectionLayer selectionLayer;
	
	
	public SelectionLayerStructuralChangeEventHandler(final SelectionLayer selectionLayer, final SelectionModel selectionModel) {
		this.selectionLayer= selectionLayer;
		this.selectionModel= selectionModel;
	}
	
	
	@Override
	public void handleLayerEvent(final LayerEvent event) {
		if (event instanceof StructuralChangeEvent) {
			final var changeEvent= (StructuralChangeEvent)event;
			if (changeEvent.isStructureChanged(HORIZONTAL)) {
				// TODO handle column deletion
				this.selectionLayer.clear();
				return;
			}
			
			if (changeEvent.isStructureChanged(VERTICAL)) {
				final List<StructuralDiff> rowDiffs= changeEvent.getDiffs(VERTICAL);
				if (rowDiffs == null) {
					//if there are no row diffs, it is a complete refresh
					final Collection<LRectangle> lRectangles= changeEvent.getChangedPositionRectangles();
					for (final LRectangle lRectangle : lRectangles) {
						final LRange changedRange= new LRange(lRectangle.y, lRectangle.y + lRectangle.height);
						if (selectedRowModified(changedRange)) {
							if (this.selectionLayer.getRowCount() > 0) {
								long columnPosition= this.selectionLayer.getSelectionAnchor().columnPosition;
								if (columnPosition == SelectionLayer.NO_SELECTION) {
									columnPosition= 0;
								}
								this.selectionLayer.setSelectionToCell(columnPosition, 0, false);
							}
							else {
								this.selectionLayer.clear();
							}
							return;
						}
					}
				}
				else {
					//there are row diffs so we try to determine the diffs to process
					for (final StructuralDiff diff : rowDiffs) {
						//DiffTypeEnum.CHANGE is used for resizing and shouldn't result in clearing the selection
						if (diff.getDiffType() != DiffType.CHANGE) {
							if (selectedRowModified(diff.getBeforePositionRange())) {
								this.selectionLayer.clear();
								return;
							}
						}
					}
				}
			}
		}
	}
	
	private boolean selectedRowModified(final LRange changedRange){
		final List<LRange> selectedRows= this.selectionModel.getSelectedRowPositions();
		for (final var range : selectedRows) {
			if (range.overlap(changedRange)){
				return true;
			}
		}
		return false;
	}
	
}
