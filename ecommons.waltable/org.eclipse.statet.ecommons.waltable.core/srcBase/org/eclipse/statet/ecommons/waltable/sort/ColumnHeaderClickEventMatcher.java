/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LPoint;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;
import org.eclipse.statet.ecommons.waltable.ui.util.CellEdgeDetectUtil;
import org.eclipse.statet.ecommons.waltable.ui.util.CellEdgeEnum;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


/**
 * Matches a click on the column header, except if the click is on the column edge.
 */
public class ColumnHeaderClickEventMatcher extends MouseEventMatcher {

	public ColumnHeaderClickEventMatcher(final int stateMask, final int button) {
		super(stateMask, GridLabels.COLUMN_HEADER, button);
	}

	@Override
    public boolean matches(final NatTable natTable, final MouseEvent event, final LabelStack regionLabels) {
	    return super.matches(natTable, event, regionLabels) && isNearTheHeaderEdge(natTable, event) && isNotFilterRegion(regionLabels);
	}

	private boolean isNearTheHeaderEdge(final Layer natLayer, final MouseEvent event) {
		final CellEdgeEnum cellEdge= CellEdgeDetectUtil.getHorizontalCellEdge(
                                                           natLayer, 
                                                           new LPoint(event.x, event.y), 
                                                           GUIHelper.DEFAULT_RESIZE_HANDLE_SIZE);
		return cellEdge == CellEdgeEnum.NONE;
	}

    private boolean isNotFilterRegion(final LabelStack regionLabels) {
        if (regionLabels != null) {
            return !regionLabels.getLabels().contains(GridLabels.FILTER_ROW);
        }
        return true;
    }
}
