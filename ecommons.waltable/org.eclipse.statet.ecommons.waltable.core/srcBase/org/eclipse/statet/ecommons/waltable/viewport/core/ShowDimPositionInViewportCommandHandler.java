/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;


@NonNullByDefault
public class ShowDimPositionInViewportCommandHandler extends AbstractLayerCommandHandler<ShowDimPositionInViewportCommand> {
	
	
	private final ViewportLayer viewportLayer;
	
	
	public ShowDimPositionInViewportCommandHandler(final ViewportLayer viewportLayer) {
		this.viewportLayer= viewportLayer;
	}
	
	
	@Override
	public Class<ShowDimPositionInViewportCommand> getCommandClass() {
		return ShowDimPositionInViewportCommand.class;
	}
	
	@Override
	protected boolean doCommand(final ShowDimPositionInViewportCommand command) {
		final var viewportDim= this.viewportLayer.getDim(command.getOrientation());
		viewportDim.movePositionIntoViewport(command.getPosition());
		return true;
	}
	
}
