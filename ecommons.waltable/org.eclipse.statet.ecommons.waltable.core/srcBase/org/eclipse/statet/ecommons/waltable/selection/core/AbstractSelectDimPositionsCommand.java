/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractDimPositionsCommand;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;


/**
 * Abstract command to select column(s)/row(s).
 */
@NonNullByDefault
public abstract class AbstractSelectDimPositionsCommand extends AbstractDimPositionsCommand {
	
	
	private final int selectionFlags;
	
	private long positionToReveal;
	
	
	public AbstractSelectDimPositionsCommand(final LayerDim layerDim,
			final long position,
			final int selectionFlags) {
		this(layerDim, position,
				Collections.singletonList(new LRange(position)),
				position, selectionFlags );
	}
	
	public AbstractSelectDimPositionsCommand(final LayerDim layerDim,
			final long refPosition, final Collection<LRange> positions,
			final long positionToReveal, final int selectionFlags) {
		super(layerDim, refPosition, positions);
		
		this.positionToReveal= positionToReveal;
		this.selectionFlags= selectionFlags;
	}
	
	protected AbstractSelectDimPositionsCommand(final AbstractSelectDimPositionsCommand command) {
		super(command);
		
		this.positionToReveal= command.positionToReveal;
		this.selectionFlags= command.selectionFlags;
	}
	
	
	public int getSelectionFlags() {
		return this.selectionFlags;
	}
	
	public long getPositionToReveal() {
		return this.positionToReveal;
	}
	
	
	@Override
	protected boolean convertToTargetLayer(final LayerDim dim, final long refPosition,
			final LayerDim targetDim) {
		if (super.convertToTargetLayer(dim, refPosition, targetDim)) {
			if (this.positionToReveal != Long.MIN_VALUE) {
				this.positionToReveal= (this.positionToReveal == refPosition) ?
						getRefPosition() :
						LayerUtils.convertPosition(dim, refPosition, this.positionToReveal,
								targetDim );
			}
			return true;
		}
		return false;
	}
	
}
