/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.ui.action;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectCellCommand;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionFlags;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;


/**
 * Action executed when the user selects any cell in the grid.
 */
public class SelectCellAction implements IMouseAction {
	
	
	public static final int swt2Flags(final int swtMask) {
		int flags= 0;
		if ((swtMask & SWT.MOD2) != 0) {
			flags |= SelectionFlags.RANGE_SELECTION;
		}
		if ((swtMask & SWT.MOD1) != 0) {
			flags |= SelectionFlags.RETAIN_SELECTION;
		}
		return flags;
	}
	
	
	public SelectCellAction() {
	}
	
	
	@Override
	public void run(final NatTable natTable, final MouseEvent event) {
		natTable.doCommand(new SelectCellCommand(natTable,
				natTable.getColumnPositionByX(event.x),
				natTable.getRowPositionByY(event.y),
				SelectCellAction.swt2Flags(event.stateMask),
				true ));
	}
	
	
}
