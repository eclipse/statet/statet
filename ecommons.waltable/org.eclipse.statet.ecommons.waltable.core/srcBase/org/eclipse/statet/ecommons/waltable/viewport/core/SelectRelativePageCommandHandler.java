/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectRelativeCellCommand;


@NonNullByDefault
public class SelectRelativePageCommandHandler implements LayerCommandHandler<SelectRelativePageCommand> {
	
	
	private final Layer viewportLayer;
	
	
	public SelectRelativePageCommandHandler(final Layer viewportLayer) {
		this.viewportLayer= viewportLayer;
	}
	
	
	@Override
	public Class<SelectRelativePageCommand> getCommandClass() {
		return SelectRelativePageCommand.class;
	}
	
	@Override
	public boolean executeCommand(final SelectRelativePageCommand command) {
		long stepCount;
		switch (command.getDirection()) {
		case UP:
		case DOWN:
			stepCount= this.viewportLayer.getRowCount();
			break;
		case LEFT:
		case RIGHT:
			stepCount= this.viewportLayer.getColumnCount();
			break;
		default:
			throw new IllegalStateException();
		}
		this.viewportLayer.doCommand(new SelectRelativeCellCommand(
				command.getDirection(), stepCount, command.getSelectionFlags() ));
		return true;
	}
	
}
