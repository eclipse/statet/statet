/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;


@NonNullByDefault
public class InitializeAutoResizeCommandHandler extends AbstractLayerCommandHandler<InitializeAutoResizeCommand> {
	
	
	private final SelectionLayer selectionLayer;
	
	
	public InitializeAutoResizeCommandHandler(final SelectionLayer selectionLayer) {
		this.selectionLayer= selectionLayer;
	}
	
	@Override
	public Class<InitializeAutoResizeCommand> getCommandClass() {
		return InitializeAutoResizeCommand.class;
	}
	
	
	@Override
	protected boolean doCommand(final InitializeAutoResizeCommand initCommand) {
		final LayerDim layerDim= initCommand.getDim();
		final long position= initCommand.getPosition();
		final LRangeList positions;
		
		final long selectionPosition= LayerUtils.convertPosition(layerDim,
				position, position, this.selectionLayer.getDim(layerDim.getOrientation()) );
		if (selectionPosition != LayerDim.POSITION_NA
				&& this.selectionLayer.isPositionFullySelected(layerDim.getOrientation(), position) ) {
			positions= this.selectionLayer.getFullySelectedPositions(layerDim.getOrientation());
			
			this.selectionLayer.doCommand(new AutoResizePositionsCommand(
					this.selectionLayer.getDim(layerDim.getOrientation()), positions));
		}
		else {
			positions= new LRangeList(position);
			
			layerDim.getLayer().doCommand(new AutoResizePositionsCommand(layerDim, positions));
		}
		
		return true;
	}
	
}
