/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


/**
 * Handle sort commands
 */
@NonNullByDefault
public class SortPositionCommandHandler extends AbstractLayerCommandHandler<SortDimPositionCommand> {
	
	
	private final SortModel sortModel;
	
	
	public SortPositionCommandHandler(final SortModel sortModel) {
		this.sortModel= sortModel;
	}
	
	
	@Override
	public Class<SortDimPositionCommand> getCommandClass() {
		return SortDimPositionCommand.class;
	}
	
	@Override
	protected boolean doCommand(final SortDimPositionCommand command) {
		final LayerDim layerDim= command.getDim();
		final long columnId= layerDim.getPositionId(command.getPosition(), command.getPosition());
		
		SortDirection newSortDirection= command.getDirection();
		if (newSortDirection == null) {
			newSortDirection= this.sortModel.getSortDirection(columnId).getNextSortDirection();
		}
		this.sortModel.sort(columnId, newSortDirection, command.isAccumulate());
		
		// Fire event
		final SortColumnEvent sortEvent= new SortColumnEvent(layerDim,
				new LRange(command.getPosition()) );
		layerDim.getLayer().fireLayerEvent(sortEvent);
		
		return true;
	}
	
}
