/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import static org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer.NO_SELECTION;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.events.DimPositionsVisualChangeEvent;


@NonNullByDefault
public class DimSelectionEvent extends DimPositionsVisualChangeEvent implements SelectionEvent {
	
	
	private final SelectionLayer selectionLayer;
	
	private final long positionToReveal;
	
	
	public DimSelectionEvent(final SelectionLayer selectionLayer, final Orientation orientation,
			final long position, final boolean revealPosition) {
		this(selectionLayer, orientation, new LRangeList(position),
				(revealPosition) ? position : NO_SELECTION );
	}
	
	public DimSelectionEvent(final SelectionLayer selectionLayer, final Orientation orientation,
			final List<LRange> positions, final long positionToReveal) {
		super(selectionLayer.getDim(orientation), positions);
		this.selectionLayer= selectionLayer;
		this.positionToReveal= positionToReveal;
	}
	
	protected DimSelectionEvent(final SelectionLayer selectionLayer, final LayerDim layerDim,
			final List<LRange> positions, final long positionToReveal) {
		super(layerDim, positions);
		this.selectionLayer= selectionLayer;
		this.positionToReveal= positionToReveal;
	}
	
	@Override
	protected @Nullable DimPositionsVisualChangeEvent toLayer(final LayerDim targetLayerDim,
			final List<LRange> positions) {
		final long positionToReveal= (this.positionToReveal != NO_SELECTION) ?
				targetLayerDim.underlyingToLocalPosition(
						getLayerDim(), this.positionToReveal ) :
				NO_SELECTION;
		
		return new DimSelectionEvent(this.selectionLayer, targetLayerDim,
				positions, positionToReveal );
	}
	
	
	@Override
	public SelectionLayer getSelectionLayer() {
		return this.selectionLayer;
	}
	
	public long getPositionToReveal() {
		return this.positionToReveal;
	}
	
}
