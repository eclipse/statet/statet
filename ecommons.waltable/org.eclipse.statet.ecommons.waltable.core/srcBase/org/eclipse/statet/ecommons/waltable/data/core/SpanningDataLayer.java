/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.data.DataCell;
import org.eclipse.statet.ecommons.waltable.core.data.SpanningDataProvider;
import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCellDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


@NonNullByDefault
public class SpanningDataLayer extends DataLayer {
	
	
	public SpanningDataLayer(final SpanningDataProvider dataProvider,
			final long columnIdCat, final int defaultColumnWidth,
			final long rowIdCat, final int defaultRowHeight) {
		super(dataProvider,
				columnIdCat, defaultColumnWidth,
				rowIdCat, defaultRowHeight );
	}
	
	
	@Override
	public SpanningDataProvider getDataProvider() {
		return (SpanningDataProvider)super.getDataProvider();
	}
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final LayerDim hDim= getDim(HORIZONTAL);
		final LayerDim vDim= getDim(VERTICAL);
		final long columnId= hDim.getPositionId(columnPosition, columnPosition);
		final long rowId= vDim.getPositionId(rowPosition, rowPosition);
		
		final DataCell dataCell= getDataProvider().getCellByPosition(columnPosition, rowPosition);
		
		return new DataLayerCell(
				new BasicLayerCellDim(HORIZONTAL, columnId, columnPosition,
						dataCell.getColumnPosition(), dataCell.getColumnSpan() ),
				new BasicLayerCellDim(VERTICAL, rowId, rowPosition,
						dataCell.getRowPosition(), dataCell.getRowSpan() ));
	}
	
}
