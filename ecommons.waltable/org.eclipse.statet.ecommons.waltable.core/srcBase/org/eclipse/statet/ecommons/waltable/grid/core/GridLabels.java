/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * A region is simply an area on the Grid.
 * Diving the table/grid into regions makes it easier to manage areas with similar behavior.
 * 
 * For example all the cells in the column header are painted differently
 * and can respond to sorting actions.
 */
@NonNullByDefault
public interface GridLabels {
	
	
	static final String CORNER= "CORNER"; //$NON-NLS-1$
	static final String COLUMN_HEADER= "COLUMN_HEADER"; //$NON-NLS-1$
	static final String ROW_HEADER= "ROW_HEADER"; //$NON-NLS-1$
	
	/* labeled header */
	static final String COLUMN_HEADER_LABEL= COLUMN_HEADER + "_LABEL"; //$NON-NLS-1$
	static final String ROW_HEADER_LABEL= ROW_HEADER + "_LABEL"; //$NON-NLS-1$
	static final String HEADER_PLACEHOLDER= "HEADER_PLACEHOLDER"; //$NON-NLS-1$
	
	/* grouped column/row */
	static final String COLUMN_GROUP_HEADER= "COLUMN_GROUP_HEADER"; //$NON-NLS-1$
	static final String ROW_GROUP_HEADER= "ROW_GROUP_HEADER"; //$NON-NLS-1$
	
	static final String BODY= "BODY"; //$NON-NLS-1$
	
	static final String FILTER_ROW= "FILTER_ROW"; //$NON-NLS-1$
	
}
