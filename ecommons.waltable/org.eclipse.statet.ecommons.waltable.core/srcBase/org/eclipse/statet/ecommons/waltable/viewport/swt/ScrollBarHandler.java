/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - dim-based implementation prepared for long datasets
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Scrollable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.swt.SwtUtils;
import org.eclipse.statet.ecommons.waltable.edit.EditUtils;
import org.eclipse.statet.ecommons.waltable.viewport.core.ViewportLayerDim;


@NonNullByDefault
public class ScrollBarHandler implements Listener {
	
	
	public static @Nullable ScrollBarHandler create(final ViewportLayerDim layerDim, final Object control) {
		if (control instanceof Scrollable) {
			final ScrollBar scrollBar= SwtUtils.getScrollBar((Scrollable)control, layerDim.getOrientation());
			if (scrollBar != null) {
				return new ScrollBarHandler(layerDim, scrollBar);
			}
		}
		return null;
	}
	
	
	private final ViewportLayerDim dim;
	
	private final ScrollBar scrollBar;
	
	private double factor= 1;
	
	/**
	 * Flag to remember if the scroll bar is moved by dragging.
	 * Needed because if the scroll bar is moved by dragging, there will be 
	 * another event that is handled for releasing the drag mode. 
	 * We only need to handle the dragging once, otherwise if the 
	 * DialogErrorHandling strategy is used, the dialog would be showed
	 * twice.
	 */
	private boolean dragging= false;
	
	
	public ScrollBarHandler(final ViewportLayerDim dim, final ScrollBar scrollBar) {
		this.dim= dim;
		this.scrollBar= scrollBar;
		this.scrollBar.addListener(SWT.Selection, this);
		
		scrollBar.getParent().addListener(SwtUtils.getMouseWheelEventType(this.dim.getOrientation()), this);
	}
	
	
	public void dispose() {
		if (this.scrollBar != null && !this.scrollBar.isDisposed()) {
			this.scrollBar.removeListener(SWT.Selection, this);
			this.scrollBar.removeListener(SwtUtils.getMouseWheelEventType(this.dim.getOrientation()), this);
		}
	}
	
	
	@Override
	public void handleEvent(final Event event) {
		boolean handle= true;
		
		if (!this.dragging) {
			if (!EditUtils.commitAndCloseActiveEditor()) {
				handle= false;
			}
		}
		this.dragging= (event.detail == SWT.DRAG);
		
		if (!handle || !event.doit) {
			adjustScrollBar();
			return;
		}
		
		switch (event.type) {
		case SWT.MouseHorizontalWheel:
		case SWT.MouseVerticalWheel:
			if (event.count > 0) {
				for (; event.count > 0; event.count--) {
					this.dim.scrollBackwardByStep();
				}
			}
			else if (event.count < 0) {
				for (; event.count < 0; event.count++) {
					this.dim.scrollForwardByStep();
				}
			}
			event.doit= false;
			return;
		case SWT.Selection:
			switch (event.detail) {
			case SWT.HOME:
				this.dim.scrollBackwardToBound();
				return;
			case SWT.END:
				this.dim.scrollForwardToBound();
				return;
			case SWT.PAGE_UP:
				this.dim.scrollBackwardByPage();
				return;
			case SWT.PAGE_DOWN:
				this.dim.scrollForwardByPage();
				return;
			case SWT.ARROW_UP:
			case SWT.ARROW_LEFT:
				this.dim.scrollBackwardByStep();
				return;
			case SWT.ARROW_DOWN:
			case SWT.ARROW_RIGHT:
				this.dim.scrollForwardByStep();
				return;
			default:
				this.dim.setOriginPixel(this.dim.getMinimumOriginPixel()
						+ (long) (this.scrollBar.getSelection() / this.factor) );
				return;
			}
		}
	}
	
	public ScrollBar getScrollBar() {
		return this.scrollBar;
	}
	
	public void adjustScrollBar() {
		if (this.scrollBar.isDisposed()) {
			return;
		}
		final long startPixel= this.dim.getOriginPixel() - this.dim.getMinimumOriginPixel();
		
		this.scrollBar.setSelection((int) (this.factor * startPixel));
	}
	
	public void recalculateScrollBarSize() {
		if (this.scrollBar.isDisposed()) {
			return;
		}
		
		final long scrollablePixel= this.dim.getScrollable().getSize() - this.dim.getMinimumOriginPixel();
		final long viewportWindowPixel= this.dim.getSize();
		
		final int max;
		final int viewportWindowSpan;
		if (scrollablePixel <= 0x3fffffff) {
			this.factor= 1.0;
			viewportWindowSpan= (int) viewportWindowPixel;
			max= (int) scrollablePixel;
		}
		else {
			this.factor= ((double) 0x3fffffff) / scrollablePixel;
			final double exactSpan= (this.factor * viewportWindowPixel);
			viewportWindowSpan= (int) Math.ceil(exactSpan);
			max= (int) Math.min(0x3fffffff
						// the thumb will be larger than required, add the diff to adjust this, 
						// so the user can scroll to the end using the mouse
						+ (long) ((viewportWindowSpan - exactSpan) / this.factor),
					Integer.MAX_VALUE ); 
		}
		
		if (this.scrollBar.isDisposed()) {
			return;
		}
		
		this.scrollBar.setMaximum(max);
		this.scrollBar.setPageIncrement(Math.max(viewportWindowSpan / 4, 1));
		
		if (viewportWindowSpan < max && viewportWindowPixel != 0) {
			this.scrollBar.setThumb(viewportWindowSpan);
			this.scrollBar.setEnabled(true);
			this.scrollBar.setVisible(true);
		}
		else {
			this.scrollBar.setThumb(max);
			this.scrollBar.setEnabled(false);
			this.scrollBar.setVisible(false);
		}
		
		adjustScrollBar();
	}
	
}
