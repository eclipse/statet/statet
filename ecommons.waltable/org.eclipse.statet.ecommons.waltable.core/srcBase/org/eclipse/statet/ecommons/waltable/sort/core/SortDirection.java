/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public enum SortDirection {
	
	
	NONE("Unsorted"), //$NON-NLS-1$
	ASC("Ascending"), //$NON-NLS-1$
	DESC("Descending"); //$NON-NLS-1$
	
	
	private final String description;
	
	
	private SortDirection(final String description) {
		this.description= description;
	}
	
	
	public String getDescription() {
		return this.description;
	}
	
	public SortDirection getNextSortDirection() {
		switch (this) {
		case NONE:
			return SortDirection.ASC;
		case ASC:
			return SortDirection.DESC;
		case DESC:
		default:
			return SortDirection.NONE;
		}
	}
	
}
