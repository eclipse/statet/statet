/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.coordinate.PositionCoordinate;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommandHandler;


@NonNullByDefault
public class SelectRelativeCommandHandler implements LayerCommandHandler<SelectRelativeCellCommand> {
	
	
	protected final SelectionLayer selectionLayer;
	
	
	public SelectRelativeCommandHandler(final SelectionLayer selectionLayer) {
		this.selectionLayer= selectionLayer;
	}
	
	
	@Override
	public Class<SelectRelativeCellCommand> getCommandClass() {
		return SelectRelativeCellCommand.class;
	}
	
	
	@Override
	public boolean executeCommand(final SelectRelativeCellCommand command) {
		select(command);
		return true;
	}
	
	protected boolean select(final SelectRelativeCellCommand command) {
		long row;
		long column;
		PositionCoordinate lastSelectedCell= null;
		if ((command.getSelectionFlags() & SelectionFlags.RANGE_SELECTION) == SelectionFlags.RANGE_SELECTION) {
			lastSelectedCell= this.selectionLayer.getLastSelectedCellPosition();
		}
		else {
			lastSelectedCell= this.selectionLayer.getSelectionAnchor();
		}
		
		switch (command.getDirection()) {
		case UP:
			if (command.getStepCount() == SelectionLayer.MOVE_ALL) {
				row= 0;
			}
			else {
				if (lastSelectedCell.rowPosition >= 0) {
					row= lastSelectedCell.rowPosition - command.getStepCount();
					if (row < 0) {
						row= 0;
					}
				}
				else {
					row= 0;
				}
			}
			column= lastSelectedCell.columnPosition;
			if (column < 0) {
				column= 0;
			}
			break;
		case DOWN:
			if (command.getStepCount() == SelectionLayer.MOVE_ALL) {
				row= this.selectionLayer.getRowCount() - 1;
			}
			else {
				if (lastSelectedCell.rowPosition >= 0) {
					row= lastSelectedCell.rowPosition + command.getStepCount();
					if (row >= this.selectionLayer.getRowCount()) {
						row= this.selectionLayer.getRowCount() - 1;
					}
				}
				else {
					row= 0;
				}
			}
			column= lastSelectedCell.columnPosition;
			if (column < 0) {
				column= 0;
			}
			break;
		case LEFT:
			if (command.getStepCount() == SelectionLayer.MOVE_ALL) {
				column= 0;
			}
			else {
				if (lastSelectedCell.columnPosition >= 0) {
					column= lastSelectedCell.columnPosition - command.getStepCount();
					if (column < 0) {
						column= 0;
					}
				}
				else {
					column= 0;
				}
			}
			row= lastSelectedCell.rowPosition;
			if (row < 0) {
				row= 0;
			}
			break;
		case RIGHT:
			if (command.getStepCount() == SelectionLayer.MOVE_ALL) {
				column= this.selectionLayer.getColumnCount() - 1;
			}
			else {
				if (lastSelectedCell.columnPosition >= 0) {
					column= lastSelectedCell.columnPosition + command.getStepCount();
					if (column >= this.selectionLayer.getColumnCount()) {
						column= this.selectionLayer.getColumnCount() - 1;
					}
				}
				else {
					column= 0;
				}
			}
			row= lastSelectedCell.rowPosition;
			if (row < 0) {
				row= 0;
			}
			break;
		default:
			return false;
		}
		
		this.selectionLayer.selectCell(column, row, command.getSelectionFlags());
		this.selectionLayer.fireCellSelectionEvent(column, row, true);
		return true;
	}
	
}
