/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.config;

import org.eclipse.swt.graphics.Color;

import org.eclipse.statet.ecommons.waltable.config.AbstractRegistryConfiguration;
import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.style.BasicStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.grid.AlternatingRowLabelContributor;
import org.eclipse.statet.ecommons.waltable.util.GUIHelper;


/**
 * Sets up alternate row coloring. Applied by {@link DefaultGridLayerConfiguration}
 */
public class DefaultRowStyleConfiguration extends AbstractRegistryConfiguration {

	public Color evenRowBgColor= GUIHelper.COLOR_WIDGET_BACKGROUND;
	public Color oddRowBgColor= GUIHelper.COLOR_WHITE;

	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		configureOddRowStyle(configRegistry);
		configureEvenRowStyle(configRegistry);
	}

	protected void configureOddRowStyle(final ConfigRegistry configRegistry) {
		final BasicStyle cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR,  this.oddRowBgColor);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle, DisplayMode.NORMAL, AlternatingRowLabelContributor.EVEN_ROW_CONFIG_TYPE);
	}

	protected void configureEvenRowStyle(final ConfigRegistry configRegistry) {
		final BasicStyle cellStyle= new BasicStyle();
		cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR,  this.evenRowBgColor);
		configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle, DisplayMode.NORMAL, AlternatingRowLabelContributor.ODD_ROW_CONFIG_TYPE);
	}
}
