/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.ui.action;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LPoint;
import org.eclipse.statet.ecommons.waltable.resize.core.InitializeAutoResizeCommand;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;
import org.eclipse.statet.ecommons.waltable.ui.util.CellEdgeDetectUtil;


public class AutoResizeRowAction implements IMouseAction {
	
	
	public AutoResizeRowAction() {
	}
	
	
	@Override
	public void run(final NatTable natTable, final MouseEvent event) {
		final long position= CellEdgeDetectUtil.getPositionToResize(natTable,
				new LPoint(event.x, event.y), VERTICAL );
		if (position == Long.MIN_VALUE) {
			return;
		}
		
		final InitializeAutoResizeCommand command= new InitializeAutoResizeCommand(
				natTable.getDim(VERTICAL), position );
		natTable.doCommand(command);
	}
	
}
