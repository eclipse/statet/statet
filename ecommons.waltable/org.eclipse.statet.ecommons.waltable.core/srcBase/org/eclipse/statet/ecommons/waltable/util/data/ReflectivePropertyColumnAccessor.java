/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.util.data;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ecommons.waltable.WaLTablePlugin;


/**
 * Convenience class which uses java reflection to get/set property names from the row bean. It
 * looks for getter methods for reading and setter methods for writing according to the Java
 * conventions.
 * 
 * @param <R> type of the row object/bean
 */
@NonNullByDefault
public class ReflectivePropertyColumnAccessor<R> implements PropertyColumnAccessor<R> {
	
	
	private final List<String> propertyNames;
	
	private final Map<Class<?>, Map<String, PropertyDescriptor>> propertyDescriptorMap= new HashMap<>();
	
	
	/**
	 * @param propertyNames of the members of the row bean
	 */
	public ReflectivePropertyColumnAccessor(final @NonNull String... propertyNames) {
		this.propertyNames= ImCollections.newList(propertyNames);
	}
	
	
	@Override
	public long getColumnCount() {
		return this.propertyNames.size();
	}

	@Override
	public @Nullable Object getDataValue(final R rowObj, final long columnIndex) {
		try {
			final PropertyDescriptor propertyDesc= getPropertyDescriptor(rowObj,
					getColumnProperty(columnIndex) );
			final Method readMethod= propertyDesc.getReadMethod();
			return readMethod.invoke(rowObj);
		}
		catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void setDataValue(final @NonNull R rowObj, final long columnIndex, final @Nullable Object newValue) {
		try {
			final PropertyDescriptor propertyDesc= getPropertyDescriptor(rowObj,
					getColumnProperty(columnIndex) );
			final Method writeMethod= propertyDesc.getWriteMethod();
			if (writeMethod == null) {
				throw new RuntimeException("Setter method not found in backing bean for value at column index: " + columnIndex); //$NON-NLS-1$
			}
			writeMethod.invoke(rowObj, newValue);
		}
		catch (final IllegalArgumentException ex) {
			WaLTablePlugin.log(new Status(IStatus.WARNING, WaLTablePlugin.BUNDLE_ID,
					"Data type being set does not match the data type of the setter method in the backing bean", ex )); //$NON-NLS-1$
		}
		catch (final Exception e) {
			WaLTablePlugin.log(new Status(IStatus.ERROR, WaLTablePlugin.BUNDLE_ID,
					"Error while setting data value", e )); //$NON-NLS-1$
			throw new RuntimeException("Error while setting data value"); //$NON-NLS-1$
		}
	}
	
	@Override
	public String getColumnProperty(final long columnIndex) {
		if (columnIndex < 0 || columnIndex >= Integer.MAX_VALUE) {
			throw new IndexOutOfBoundsException();
		}
		return this.propertyNames.get((int)columnIndex);
	}
	
	@Override
	public long getColumnIndex(final String propertyName) {
		return this.propertyNames.indexOf(propertyName);
	}
	
	protected PropertyDescriptor getPropertyDescriptor(final @NonNull R rowObj,
			final String propertyName) throws IntrospectionException {
		synchronized (this.propertyDescriptorMap) {
			var map= this.propertyDescriptorMap.get(rowObj.getClass());
			if (map == null) {
				final PropertyDescriptor[] propertyDescriptors=
						Introspector.getBeanInfo(rowObj.getClass()).getPropertyDescriptors();
				map= new HashMap<>();
				for (final PropertyDescriptor propertyDescriptor : propertyDescriptors) {
					map.put(propertyDescriptor.getName(), propertyDescriptor);
				}
				this.propertyDescriptorMap.put(rowObj.getClass(), map);
			}
			return map.get(propertyName);
		}
	}
	
}
