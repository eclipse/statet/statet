/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.Persistable;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.ForwardLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.ForwardLayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.ForwardLayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellDim;
import org.eclipse.statet.ecommons.waltable.sort.SortStatePersistor;
import org.eclipse.statet.ecommons.waltable.sort.config.DefaultSortConfiguration;


/**
 * Enables sorting of the data. Uses an {@link SortModel} to do/track the sorting.
 * @param <T> Type of the Beans in the backing data source.
 * 
 * @see DefaultSortConfiguration
 * @see SortStatePersistor
 */
@NonNullByDefault
public class SortHeaderLayer<T> extends ForwardLayer<ForwardLayerDim<? extends SortHeaderLayer<T>>> implements Persistable {
	
	
	/** Handles the actual sorting of underlying data */
	private final SortModel sortModel;
	
	
	public SortHeaderLayer(final Layer underlyingLayer, final SortModel sortModel) {
		this(underlyingLayer, sortModel, true);
	}
	
	public SortHeaderLayer(final Layer underlyingLayer, final SortModel sortModel, final boolean useDefaultConfiguration) {
		super(underlyingLayer);
		this.sortModel= sortModel;
		
		init();
		
		registerPersistable(new SortStatePersistor<T>(sortModel));
		
		if (useDefaultConfiguration) {
			addConfiguration(new DefaultSortConfiguration());
		}
	}
	
	@Override
	protected ForwardLayerDim<? extends SortHeaderLayer<T>> createDim(final Orientation orientation) {
		return new ForwardLayerDim<>(this, getUnderlyingLayer().getDim(orientation));
	}
	
	
	/**
	 * @return The ISortModel that is used to handle the sorting of the underlying data.
	 */
	public SortModel getSortModel() {
		return this.sortModel;
	}
	
	
	@Override
	protected LayerCell createCell(final LayerCellDim hDim, final LayerCellDim vDim,
			final LayerCell underlyingCell) {
		return new ForwardLayerCell(this, hDim, vDim, underlyingCell) {
			
			@Override
			public LabelStack getLabels() {
				final LabelStack configLabels= super.getLabels();
				
				final long id= getDim(HORIZONTAL).getId();
				if (SortHeaderLayer.this.sortModel.isSorted(id)) {
					final String sortConfig= DefaultSortConfiguration.SORT_SEQ_CONFIG_TYPE + SortHeaderLayer.this.sortModel.getSortOrder(id);
					configLabels.addLabelOnTop(sortConfig);
					
					final SortDirection sortDirection= SortHeaderLayer.this.sortModel.getSortDirection(id);
					switch (sortDirection) {
					case ASC:
						configLabels.addLabelOnTop(DefaultSortConfiguration.SORT_UP_CONFIG_TYPE);
						break;
					case DESC:
						configLabels.addLabelOnTop(DefaultSortConfiguration.SORT_DOWN_CONFIG_TYPE);
						break;
					default:
						break;
					}
				}
				
				return configLabels;
			}
			
		};
	}
	
}
