/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.labeled;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.data.DataProvider;
import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCellDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LabelStack;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.grid.core.layers.CornerLayer;


@NonNullByDefault
public class LabelCornerLayer extends CornerLayer {
	
	
	private final @Nullable DataProvider columnHeaderLabelProvider;
	private final @Nullable DataProvider rowHeaderLabelProvider;
	
	
	/**
	 * @param baseLayer
	 *            The data provider for this layer
	 * @param horizontalLayerDependency
	 *            The layer to link the horizontal dimension to, typically the row header layer
	 * @param verticalLayerDependency
	 *            The layer to link the vertical dimension to, typically the column header layer
	 * @param useDefaultConfiguration
	 *            If default configuration should be applied to this layer (at moment none)
	 * @param layerPainter
	 *            The painter for this layer or <code>null</code> to use the painter of the base layer
	 */
	public LabelCornerLayer(final Layer baseLayer,
			final Layer horizontalLayerDependency, final Layer verticalLayerDependency,
			final @Nullable DataProvider columnHeaderLabelProvider,
			final @Nullable DataProvider rowHeaderLabelProvider,
			final boolean useDefaultConfiguration,
			final LayerPainter layerPainter) {
		super(baseLayer, horizontalLayerDependency, verticalLayerDependency,
				useDefaultConfiguration, layerPainter );
		
		this.columnHeaderLabelProvider= columnHeaderLabelProvider;
		this.rowHeaderLabelProvider= rowHeaderLabelProvider;
	}
	
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final LayerDim hDim= getDim(HORIZONTAL);
		final LayerDim vDim= getDim(VERTICAL);
		final long columnId= hDim.getPositionId(columnPosition, columnPosition);
		final long rowId= vDim.getPositionId(rowPosition, rowPosition);
		
		final long columnCount= getColumnCount();
		final long rowCount= getRowCount();
		if (rowPosition < rowCount - 1) {
			return new BasicLayerCell(this,
					new BasicLayerCellDim(HORIZONTAL, columnId, columnPosition, 0, columnCount),
					new BasicLayerCellDim(VERTICAL, rowId, rowPosition) ) {
				@Override
				public LabelStack getLabels() {
					return new LabelStack(GridLabels.COLUMN_HEADER_LABEL);
				}
				@Override
				public @Nullable Object getDataValue(final int flags, final @Nullable IProgressMonitor monitor) {
					final var labelProvider= LabelCornerLayer.this.columnHeaderLabelProvider;
					return (labelProvider != null) ? 
							labelProvider.getDataValue(0, getRowPosition(), flags, monitor) :
							""; //$NON-NLS-1$
				}
			};
		}
		else if (columnPosition < columnCount - 1) {
			return new BasicLayerCell(this,
					new BasicLayerCellDim(HORIZONTAL, columnId, columnPosition),
					new BasicLayerCellDim(VERTICAL, rowId, rowPosition) ) {
				@Override
				public LabelStack getLabels() {
					return new LabelStack(GridLabels.ROW_HEADER_LABEL);
				}
				@Override
				public @Nullable Object getDataValue(final int flags, final @Nullable IProgressMonitor monitor) {
					final var labelProvider= LabelCornerLayer.this.rowHeaderLabelProvider;
					return (labelProvider != null) ? 
							labelProvider.getDataValue(getColumnPosition(), 0, flags, monitor) :
							""; //$NON-NLS-1$
				}
			};
		}
		else {
			return new BasicLayerCell(this,
					new BasicLayerCellDim(HORIZONTAL, columnId, columnPosition),
					new BasicLayerCellDim(VERTICAL, rowId, rowPosition) ) {
				@Override
				public LabelStack getLabels() {
					return new LabelStack(GridLabels.HEADER_PLACEHOLDER);
				}
			};
		}
	}
	
}
