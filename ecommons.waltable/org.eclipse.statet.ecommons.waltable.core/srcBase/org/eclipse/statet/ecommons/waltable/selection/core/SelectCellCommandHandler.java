/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRectangle;


@NonNullByDefault
public class SelectCellCommandHandler extends AbstractLayerCommandHandler<SelectCellCommand> {
	
	
	private final SelectionLayer selectionLayer;
	
	
	public SelectCellCommandHandler(final SelectionLayer selectionLayer) {
		this.selectionLayer= selectionLayer;
	}
	
	
	@Override
	public Class<SelectCellCommand> getCommandClass() {
		return SelectCellCommand.class;
	}
	
	@Override
	protected boolean doCommand(final SelectCellCommand command) {
		toggleOrSelectCell(command.getColumnPosition(), command.getRowPosition(),
				command.getSelectionFlags() );
		this.selectionLayer.fireCellSelectionEvent(command.getColumnPosition(), command.getRowPosition(),
				command.getRevealCell() );
		return true;
	}
	
	/**
	 * Toggles the selection state of the given row and column.
	 */
	protected void toggleOrSelectCell(final long columnPosition, final long rowPosition,
			final int selectionFlags) {
		if ((selectionFlags & (SelectionFlags.RETAIN_SELECTION | SelectionFlags.RANGE_SELECTION)) == SelectionFlags.RETAIN_SELECTION) {
			if (this.selectionLayer.isCellPositionSelected(columnPosition, rowPosition)) {
				this.selectionLayer.clearSelection(columnPosition, rowPosition);
				return;
			}
		}
		
		selectCell(columnPosition, rowPosition, selectionFlags);
	}
	
	/**
	 * Selects a cell.
	 * 
	 * @param columnPosition the column position of the cell
	 * @param rowPosition the row position of the cell
	 * @param selectionFlags {@link SelectionFlags flags} how to update the selection
	 */
	protected void selectCell(final long columnPosition, final long rowPosition,
			final int selectionFlags) {
		if ((selectionFlags & (SelectionFlags.RETAIN_SELECTION | SelectionFlags.RANGE_SELECTION)) == 0) {
			this.selectionLayer.clearSelections();
		}
		this.selectionLayer.lastSelectedCell.columnPosition= columnPosition;
		this.selectionLayer.lastSelectedCell.rowPosition= rowPosition;
		
		if (this.selectionLayer.getSelectionModel().isMultipleSelectionAllowed()
				&& (selectionFlags & SelectionFlags.RANGE_SELECTION) != 0 && this.selectionLayer.lastSelectedRegion != null
				&& this.selectionLayer.selectionAnchor.columnPosition >= 0) {
			if ((selectionFlags & SelectionFlags.RETAIN_SELECTION) != 0) {
				this.selectionLayer.lastSelectedRegion= new LRectangle(0, 0, 0, 0);
			}
			
			this.selectionLayer.lastSelectedRegion.x= Math.min(this.selectionLayer.selectionAnchor.columnPosition, columnPosition);
			this.selectionLayer.lastSelectedRegion.width= Math.abs(this.selectionLayer.selectionAnchor.columnPosition - columnPosition) + 1;
			this.selectionLayer.lastSelectedRegion.y= Math.min(this.selectionLayer.selectionAnchor.rowPosition, rowPosition);
			this.selectionLayer.lastSelectedRegion.height= Math.abs(this.selectionLayer.selectionAnchor.rowPosition - rowPosition) + 1;
			
			this.selectionLayer.addSelection(this.selectionLayer.lastSelectedRegion);
		}
		else {
			this.selectionLayer.addSelection(new LRectangle(columnPosition, rowPosition, 1, 1));
		}
	}
	
}
