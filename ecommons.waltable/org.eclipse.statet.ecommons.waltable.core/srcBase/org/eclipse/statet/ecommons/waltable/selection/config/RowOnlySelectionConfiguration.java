/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.config;

import org.eclipse.statet.ecommons.waltable.config.AbstractLayerConfiguration;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;


/**
 * Configure the move selection behavior so that we always move by a row.
 * Add {@link LayerEventHandler} to preserve row selection.
 * 
 * @see DefaultMoveSelectionConfiguration
 */
public class RowOnlySelectionConfiguration<T> extends AbstractLayerConfiguration<SelectionLayer> {

	@Override
	public void configureTypedLayer(final SelectionLayer layer) {
//		layer.registerCommandHandler(new MoveRowSelectionCommandHandler(layer));
	}
	
}
