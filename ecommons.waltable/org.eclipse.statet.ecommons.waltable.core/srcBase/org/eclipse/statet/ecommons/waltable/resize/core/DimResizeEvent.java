/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.events.DimPositionsStructuralChangeEvent;
import org.eclipse.statet.ecommons.waltable.core.layer.events.StructuralDiff;
import org.eclipse.statet.ecommons.waltable.core.layer.events.StructuralDiff.DiffType;


@NonNullByDefault
public class DimResizeEvent extends DimPositionsStructuralChangeEvent {
	
	
	public DimResizeEvent(final LayerDim layerDim, final LRange positions) {
		super(layerDim, ImCollections.newList(positions));
	}
	
	public DimResizeEvent(final LayerDim layerDim, final List<LRange> positions) {
		super(layerDim, positions);
	}
	
	
	@Override
	protected @Nullable DimResizeEvent toLayer(final LayerDim targetLayerDim,
			final List<LRange> positions) {
		return new DimResizeEvent(targetLayerDim, positions);
	}
	
	
	@Override
	public @Nullable List<StructuralDiff> getDiffs(final Orientation orientation) {
		if (orientation == getOrientation()) {
			final var diffs= new ArrayList<StructuralDiff>();
			
			for (final LRange lRange : getPositionRanges()) {
				diffs.add(new StructuralDiff(DiffType.CHANGE, lRange, lRange));
			}
			
			return diffs;
		}
		else {
			return null;
		}
	}
	
}
