/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractDimPositionCommand;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


@NonNullByDefault
public class ShowDimPositionInViewportCommand extends AbstractDimPositionCommand {
	
	
	public ShowDimPositionInViewportCommand(final LayerDim layerDim, final long position) {
		super(layerDim, position);
	}
	
	protected ShowDimPositionInViewportCommand(final ShowDimPositionInViewportCommand command) {
		super(command);
	}
	
	@Override
	public ShowDimPositionInViewportCommand cloneCommand() {
		return new ShowDimPositionInViewportCommand(this);
	}
	
	
}
