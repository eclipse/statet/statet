/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.events.GeneralVisualChangeEvent;


@NonNullByDefault
public class EmptySelectionEvent extends GeneralVisualChangeEvent implements SelectionEvent {
	
	
	private final SelectionLayer selectionLayer;
	
	
	public EmptySelectionEvent(final SelectionLayer selectionLayer) {
		super(selectionLayer);
		this.selectionLayer= selectionLayer;
	}
	
	protected EmptySelectionEvent(final SelectionLayer selectionLayer, final Layer layer) {
		super(layer);
		this.selectionLayer= selectionLayer;
	}
	
	@Override
	public @Nullable GeneralVisualChangeEvent toLayer(final Layer targetLayer) {
		if (targetLayer == getLayer()) {
			return this;
		}
		
		return new EmptySelectionEvent(this.selectionLayer, targetLayer);
	}
	
	
	@Override
	public SelectionLayer getSelectionLayer() {
		return this.selectionLayer;
	}
	
	
}
