/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.layers;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCellDim;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;


/**
 * Layer for the top left header corner of the grid layer
 */
@NonNullByDefault
public class CornerLayer extends DimensionallyDependentLayer {
	
	
	/**
	 * @param baseLayer
	 *            The data provider for this layer
	 * @param horizontalLayerDependency
	 *            The layer to link the horizontal dimension to, typically the row header layer
	 * @param verticalLayerDependency
	 *            The layer to link the vertical dimension to, typically the column header layer
	 * @param useDefaultConfiguration
	 *            If default configuration should be applied to this layer (at moment none)
	 * @param layerPainter
	 *            The painter for this layer or <code>null</code> to use the painter of the base layer
	 */
	public CornerLayer(final Layer baseLayer,
			final Layer horizontalLayerDependency, final Layer verticalLayerDependency,
			final boolean useDefaultConfiguration,
			final LayerPainter layerPainter) {
		super(baseLayer, horizontalLayerDependency, verticalLayerDependency, layerPainter);
	}
	
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final LayerDim hDim= getDim(HORIZONTAL);
		final LayerDim vDim= getDim(VERTICAL);
		final long columnId= hDim.getPositionId(columnPosition, columnPosition);
		final long rowId= vDim.getPositionId(rowPosition, rowPosition);
		
		return new BasicLayerCell(this,
				new BasicLayerCellDim(HORIZONTAL, columnId,
						columnPosition, 0, hDim.getPositionCount() ),
				new BasicLayerCellDim(VERTICAL, rowId,
						rowPosition, 0, getVerticalLayerDependency().getRowCount()) );
	}
	
}
