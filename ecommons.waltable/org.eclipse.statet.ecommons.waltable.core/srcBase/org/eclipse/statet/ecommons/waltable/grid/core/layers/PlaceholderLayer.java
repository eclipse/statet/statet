/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.layers;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionId;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionOutOfBoundsException;
import org.eclipse.statet.ecommons.waltable.core.layer.AbstractLayer;
import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.BasicLayerCellDim;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCell;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.core.swt.painters.CellLayerPainter;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayer;
import org.eclipse.statet.ecommons.waltable.data.core.DataLayerDim;


@NonNullByDefault
public class PlaceholderLayer extends DimensionallyDependentLayer {
	
	
	private static class DummyLayer extends AbstractLayer<DataLayerDim<DummyLayer>> implements Layer {
		
		private static class Dim extends DataLayerDim<DummyLayer> {
			
			
			public Dim(final DummyLayer layer, final Orientation orientation, final long catId) {
				super(layer, orientation, catId);
			}
			
			
			@Override
			public long getPositionCount() {
				return 1;
			}
			
			
			@Override
			public long getSize() {
				return this.layer.size;
			}
			
			@Override
			public long getPositionByPixel(final long pixel) {
				return 0;
			}
			
			@Override
			public long getPositionStart(final long position) {
				if (position != 0) {
					throw new PositionOutOfBoundsException(position, getOrientation());
				}
				return 0;
			}
			
			@Override
			public int getPositionSize(final long position) {
				if (position != 0) {
					throw new PositionOutOfBoundsException(position, getOrientation());
				}
				return this.layer.size;
			}
			
			@Override
			public boolean isPositionResizable(final long position) {
				return false;
			}
			
		}
		
		
		private static long idCounter;
		
		
		private final long id;
		
		private int size= DataLayer.DEFAULT_ROW_HEIGHT;
		
		
		public DummyLayer() {
			this.id= PositionId.PLACEHOLDER_CAT + idCounter++;
			
			init();
		}
		
		@Override
		protected DataLayerDim<DummyLayer> createDim(final Orientation orientation) {
			return new Dim(this, orientation, this.id);
		}
		
		
		@Override
		public @Nullable Layer getUnderlyingLayerByPosition(final long columnPosition, final long rowPosition) {
			return null;
		}
		
		@Override
		public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
			throw new UnsupportedOperationException();
		}
		
	}
	
	
	/**
	 * Creates a corner header layer using the default configuration and painter
	 * 
	 * @param horizontalLayerDependency
	 *            The layer to link the horizontal dimension to, typically the row header layer
	 * @param verticalLayerDependency
	 *            The layer to link the vertical dimension to, typically the column header layer
	 */
	public PlaceholderLayer(final @Nullable Layer horizontalLayerDependency,
			final @Nullable Layer verticalLayerDependency) {
		this(horizontalLayerDependency, verticalLayerDependency, true, new CellLayerPainter());
	}
	
	/**
	 * @param horizontalLayerDependency
	 *            The layer to link the horizontal dimension to, typically the row header layer
	 * @param verticalLayerDependency
	 *            The layer to link the vertical dimension to, typically the column header layer
	 * @param useDefaultConfiguration
	 *            If default configuration should be applied to this layer (at moment none)
	 * @param layerPainter
	 *            The painter for this layer or <code>null</code> to use the painter of the base layer
	 */
	public PlaceholderLayer(final @Nullable Layer horizontalLayerDependency,
			final @Nullable Layer verticalLayerDependency,
			final boolean useDefaultConfiguration, final LayerPainter layerPainter) {
		super(new DummyLayer(),
				layerPainter );
		
		setHorizontalLayerDependency((horizontalLayerDependency != null) ?
				horizontalLayerDependency : getBaseLayer() );
		setVerticalLayerDependency((verticalLayerDependency != null) ?
				verticalLayerDependency : getBaseLayer() );
	}
	
	
	public void setSize(final int size) {
		((DummyLayer) getBaseLayer()).size= size;
	}
	
	@Override
	public LayerCell getCellByPosition(final long columnPosition, final long rowPosition) {
		final LayerDim hDim= getDim(HORIZONTAL);
		final LayerDim vDim= getDim(VERTICAL);
		final long columnId= hDim.getPositionId(columnPosition, columnPosition);
		final long rowId= vDim.getPositionId(rowPosition, rowPosition);
		
		return new BasicLayerCell(this,
				new BasicLayerCellDim(HORIZONTAL, columnId,
						columnPosition, 0, hDim.getPositionCount() ),
				new BasicLayerCellDim(VERTICAL, rowId,
						rowPosition, 0, vDim.getPositionCount() ));
	}
	
}
