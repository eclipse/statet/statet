/*=============================================================================#
 # Copyright (c) 2012, 2025 Edwin Park and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Edwin Park - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractContextFreeCommand;


@NonNullByDefault
public class ViewportDragCommand extends AbstractContextFreeCommand {
	
	
	private final long x;
	private final long y;
	
	
	public ViewportDragCommand(final long x, final long y) {
		this.x= x;
		this.y= y;
	}
	
	public long getX() {
		return this.x;
	}
	
	public long getY() {
		return this.y;
	}
	
}
