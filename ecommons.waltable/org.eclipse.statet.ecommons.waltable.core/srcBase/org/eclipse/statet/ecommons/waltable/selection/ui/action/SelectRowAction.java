/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.ui.action;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.swt.events.MouseEvent;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectDimPositionsCommand;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;


/**
 * Action executed when the user selects any row in the grid.
 */
public class SelectRowAction implements IMouseAction {
	
	
	public SelectRowAction() {
	}
	
	
	@Override
	public void run(final NatTable natTable, final MouseEvent event) {
		natTable.doCommand(new SelectDimPositionsCommand(natTable.getDim(VERTICAL),
				natTable.getRowPositionByY(event.y),
				natTable.getColumnPositionByX(event.x),
				SelectCellAction.swt2Flags(event.stateMask) ));
	}
	
}
