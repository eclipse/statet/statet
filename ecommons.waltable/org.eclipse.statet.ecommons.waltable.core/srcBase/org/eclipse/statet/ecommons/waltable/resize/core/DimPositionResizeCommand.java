/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractDimPositionCommand;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


/**
 * Event indicating that a column has been resized.
 */
@NonNullByDefault
public class DimPositionResizeCommand extends AbstractDimPositionCommand {
	
	
	private final int newSize;
	
	
	public DimPositionResizeCommand(final LayerDim layerDim, final long position, final int newSize) {
		super(layerDim, position);
		
		this.newSize= newSize;
	}
	
	protected DimPositionResizeCommand(final DimPositionResizeCommand command) {
		super(command);
		
		this.newSize= command.newSize;
	}
	
	
	public int getNewSize() {
		return this.newSize;
	}
	
	@Override
	public DimPositionResizeCommand cloneCommand() {
		return new DimPositionResizeCommand(this);
	}
	
}
