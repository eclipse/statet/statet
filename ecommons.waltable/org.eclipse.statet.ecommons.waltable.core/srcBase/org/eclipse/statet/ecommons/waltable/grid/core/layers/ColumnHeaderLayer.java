/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.grid.core.layers;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.core.swt.painters.CellLayerPainter;
import org.eclipse.statet.ecommons.waltable.layer.config.DefaultColumnHeaderLayerConfiguration;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;
import org.eclipse.statet.ecommons.waltable.style.SelectionStyleLabels;


/**
 * Responsible for rendering, event handling etc on the column headers.
 */
@NonNullByDefault
public class ColumnHeaderLayer extends AbstractPositionHeaderLayer {
	
	
	/**
	 * Creates a column header layer using the default configuration and painter
	 * 
	 * @param baseLayer
	 *            The data provider for this layer
	 * @param horizontalLayerDependency
	 *            The layer to link the horizontal dimension to, typically the body layer
	 * @param selectionLayer
	 *            The selection layer required to respond to selection events
	 */
	public ColumnHeaderLayer(final Layer baseLayer, final Layer horizontalLayerDependency, final SelectionLayer selectionLayer) {
		this(baseLayer, horizontalLayerDependency, selectionLayer, true);
	}
	
	public ColumnHeaderLayer(final Layer baseLayer, final Layer horizontalLayerDependency, final SelectionLayer selectionLayer, final boolean useDefaultConfiguration) {
		this(baseLayer, horizontalLayerDependency, selectionLayer, useDefaultConfiguration, new CellLayerPainter());
	}
	
	/**
	 * @param baseLayer
	 *            The data provider for this layer
	 * @param horizontalLayerDependency
	 *            The layer to link the horizontal dimension to, typically the body layer
	 * @param selectionLayer
	 *            The selection layer required to respond to selection events
	 * @param useDefaultConfiguration
	 *            If default configuration should be applied to this layer
	 * @param layerPainter
	 *            The painter for this layer or <code>null</code> to use the painter of the base layer
	 */
	public ColumnHeaderLayer(final Layer baseLayer, final Layer horizontalLayerDependency,
			final SelectionLayer selectionLayer, final boolean useDefaultConfiguration,
			final LayerPainter layerPainter) {
		super(baseLayer, HORIZONTAL, horizontalLayerDependency,
				selectionLayer, SelectionStyleLabels.COLUMN_FULLY_SELECTED_STYLE,
				layerPainter );
		
		registerCommandHandlers();
		
		if (useDefaultConfiguration) {
			addConfiguration(new DefaultColumnHeaderLayerConfiguration());
		}
	}
	
	
}
