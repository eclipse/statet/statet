/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractDimPositionCommand;
import org.eclipse.statet.ecommons.waltable.core.command.LayerCommand;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLayer;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectionLayer;


/**
 * This command triggers the AutoResizeColumms command. It collects the selected
 * columns from the {@link SelectionLayer} and fires the
 * {@link AutoResizePositionsCommand} on the {@link GridLayer}
 */
@NonNullByDefault
public class InitializeAutoResizeCommand extends AbstractDimPositionCommand {
	
	
	public InitializeAutoResizeCommand(final LayerDim layer, final long position) {
		super(layer, position);
	}
	
	protected InitializeAutoResizeCommand(final InitializeAutoResizeCommand command) {
		super(command);
	}
	
	@Override
	public LayerCommand cloneCommand() {
		return new InitializeAutoResizeCommand(this);
	}
	
	
}
