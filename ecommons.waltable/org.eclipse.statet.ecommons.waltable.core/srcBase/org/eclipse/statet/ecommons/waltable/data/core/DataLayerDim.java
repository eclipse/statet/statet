/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.data.core;

import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PixelOutOfBoundsException;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionId;
import org.eclipse.statet.ecommons.waltable.core.coordinate.PositionOutOfBoundsException;
import org.eclipse.statet.ecommons.waltable.core.layer.AbstractLayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


@NonNullByDefault
public abstract class DataLayerDim<TLayer extends Layer> extends AbstractLayerDim<TLayer> {
	
	
	private final long idCat;
	
	
	public DataLayerDim(final TLayer layer, final Orientation orientation,
			final long idCat) {
		super(layer, orientation);
		
		this.idCat= idCat;
	}
	
	
	@Override
	public long getPositionId(final long refPosition, final long position) {
		if (position < 0 || position >= getPositionCount()) {
			throw new PositionOutOfBoundsException(position, getOrientation());
		}
		return (this.idCat | position);
	}
	
	@Override
	public long getPositionById(final long id) {
		if ((id & PositionId.CAT_MASK) == this.idCat) {
			final long position= (id & PositionId.NUM_MASK);
			if (position < getPositionCount()) {
				return position;
			}
		}
		return POSITION_NA;
	}
	
	
	@Override
	public long localToUnderlyingPosition(final long refPosition, final long position) {
		return position;
	}
	
	@Override
	public long underlyingToLocalPosition(final LayerDim sourceUnderlyingDim, final long underlyingPosition) {
		throw new IllegalArgumentException("underlyingLayer"); //$NON-NLS-1$
	}
	
	@Override
	public List<LRange> underlyingToLocalPositions(final LayerDim sourceUnderlyingDim,
			final Collection<LRange> underlyingPositions) {
		throw new IllegalArgumentException("underlyingLayer"); //$NON-NLS-1$
	}
	
	@Override
	public @Nullable ImList<LayerDim> getUnderlyingDimsByPosition(final long position) {
		return null;
	}
	
	
	@Override
	public long getPreferredSize() {
		return getSize();
	}
	
	@Override
	public long getPositionByPixel(final long pixel) {
		long startPixel= 0;
		long endPixel= getSize();
		
		if (pixel < startPixel || pixel >= endPixel) {
			throw new PixelOutOfBoundsException(pixel, getOrientation());
		}
		
		long startPosition= 0;
		long endPosition= getPositionCount();
		
		while (true) {
			final double size= (double) (endPixel - startPixel) / (endPosition - startPosition);
			final long position= startPosition + (long) ((pixel - startPixel) / size);
			
			final long start= getPositionStart(position);
			final long end= start + getPositionSize(position);
			if (pixel < start) {
				endPosition= position;
				endPixel= start;
			}
			else if (pixel >= end) {
				startPosition= position + 1;
				startPixel= end;
			}
			else {
				return position;
			}
		}
	}
	
	@Override
	public long getPositionStart(final long refPosition, final long position) {
		return getPositionStart(position);
	}
	
	@Override
	public int getPositionSize(final long refPosition, final long position) {
		return getPositionSize(position);
	}
	
}
