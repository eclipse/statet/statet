/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.selection.core;

import java.util.Collection;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractDimPositionsCommand;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList;
import org.eclipse.statet.ecommons.waltable.core.layer.Layer;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerUtils;


@NonNullByDefault
public class SelectDimPositionsCommand extends AbstractDimPositionsCommand {
	
	
	private final int selectionFlags;
	
	private long orthogonalPosition;
	
	private long positionToReveal;
	
	
	public SelectDimPositionsCommand(final LayerDim layerDim,
			final long position, final long orthogonalPosition,
			final int selectionFlags) {
		this(layerDim, position, new LRangeList(position), orthogonalPosition, selectionFlags,
				position );
	}
	
	public SelectDimPositionsCommand(final LayerDim layerDim,
			final long refPosition, final Collection<LRange> positions,
			final long orthogonalPosition, final int selectionFlags, final long positionToReveal) {
		super(layerDim, refPosition, positions);
		
		this.orthogonalPosition= orthogonalPosition;
		this.selectionFlags= selectionFlags;
		this.positionToReveal= positionToReveal;
	}
	
	protected SelectDimPositionsCommand(final SelectDimPositionsCommand command) {
		super(command);
		
		this.orthogonalPosition= command.orthogonalPosition;
		this.selectionFlags= command.selectionFlags;
		this.positionToReveal= command.positionToReveal;
	}
	
	@Override
	public SelectDimPositionsCommand cloneCommand() {
		return new SelectDimPositionsCommand(this);
	}
	
	
	@Override
	public boolean convertToTargetLayer(final Layer targetLayer) {
		final LayerDim layerDim= getDim();
		final long targetOrthogonalPosition= LayerUtils.convertPosition(
				layerDim.getLayer().getDim(getOrientation().getOrthogonal()),
				this.orthogonalPosition, this.orthogonalPosition,
				targetLayer.getDim(getOrientation().getOrthogonal()) );
		if (targetOrthogonalPosition != LayerDim.POSITION_NA
				&& super.convertToTargetLayer(targetLayer) ) {
			this.orthogonalPosition= targetOrthogonalPosition;
			this.positionToReveal= LayerUtils.convertPosition(layerDim,
					this.positionToReveal, this.positionToReveal,
					targetLayer.getDim(getOrientation()) );
			return true;
		}
		return false;
	}
	
	
	public long getOrthogonalPosition() {
		return this.orthogonalPosition;
	}
	
	public int getSelectionFlags() {
		return this.selectionFlags;
	}
	
	public long getPositionToReveal() {
		return this.positionToReveal;
	}
	
}
