/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.viewport.core;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;


@NonNullByDefault
public class ShowCellInViewportCommandHandler extends AbstractLayerCommandHandler<ShowCellInViewportCommand> {
	
	
	private final ViewportLayer viewportLayer;
	
	
	public ShowCellInViewportCommandHandler(final ViewportLayer viewportLayer) {
		this.viewportLayer= viewportLayer;
	}
	
	@Override
	public Class<ShowCellInViewportCommand> getCommandClass() {
		return ShowCellInViewportCommand.class;
	}
	
	
	@Override
	protected boolean doCommand(final ShowCellInViewportCommand command) {
		this.viewportLayer.getDim(HORIZONTAL).movePositionIntoViewport(command.getColumnPosition());
		this.viewportLayer.getDim(VERTICAL).movePositionIntoViewport(command.getRowPosition());
		return true;
	}
	
}
