/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractDimPositionCommand;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;


@NonNullByDefault
public class SortDimPositionCommand extends AbstractDimPositionCommand {
	
	
	private final boolean accumulate;
	
	private final @Nullable SortDirection direction;
	
	
	public SortDimPositionCommand(final LayerDim layerDim, final long columnPosition,
			final boolean accumulate) {
		super(layerDim, columnPosition);
		
		this.accumulate= accumulate;
		this.direction= null;
	}
	
	public SortDimPositionCommand(final LayerDim layerDim, final long columnPosition,
			final SortDirection direction, final boolean accumulate) {
		super(layerDim, columnPosition);
		
		this.direction= direction;
		this.accumulate= accumulate;
	}
	
	protected SortDimPositionCommand(final SortDimPositionCommand command) {
		super(command);
		
		this.accumulate= command.accumulate;
		this.direction= command.direction;
	}
	
	@Override
	public SortDimPositionCommand cloneCommand() {
		return new SortDimPositionCommand(this);
	}
	
	
	public boolean isAccumulate() {
		return this.accumulate;
	}
	
	/**
	 * The sort direction, if specified.
	 * 
	 * @return the sort direction or <code>null</code> for automatic iteration
	 */
	public @Nullable SortDirection getDirection() {
		return this.direction;
	}
	
}
