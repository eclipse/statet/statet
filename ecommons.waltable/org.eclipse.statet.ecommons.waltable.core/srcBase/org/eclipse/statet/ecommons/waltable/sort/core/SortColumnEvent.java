/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.sort.core;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.coordinate.LRange;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerDim;
import org.eclipse.statet.ecommons.waltable.core.layer.events.DimPositionsVisualChangeEvent;


@NonNullByDefault
public class SortColumnEvent extends DimPositionsVisualChangeEvent {
	
	
	public SortColumnEvent(final LayerDim layerDim, final List<LRange> positions) {
		super(layerDim, positions);
	}
	
	public SortColumnEvent(final LayerDim layerDim, final LRange positions) {
		super(layerDim, ImCollections.newList(positions));
	}
	
	@Override
	protected @Nullable DimPositionsVisualChangeEvent toLayer(final LayerDim targetLayerDim,
			@NonNull final List<@NonNull LRange> positionRanges) {
		return new SortColumnEvent(targetLayerDim, positionRanges);
	}
	
	
}
