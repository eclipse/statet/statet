/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.util.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Maps between the column property name in the backing data bean
 * and its corresponding column index.
 */
@NonNullByDefault
public interface PropertyColumnResolver {
	
	
	/**
	 * @param columnIndex i.e the order of the column in the backing bean
	 */
	String getColumnProperty(long columnIndex);
	
	/**
	 * @param propertyName i.e the name of the column in the backing bean
	 */
	long getColumnIndex(String propertyName);
	
}
