/*=============================================================================#
 # Copyright (c) 2012, 2025 Original NatTable authors and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Original NatTable authors and others - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.waltable.resize.ui.action;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.ui.action.IMouseAction;


public class ColumnResizeCursorAction implements IMouseAction {

	private Cursor columnResizeCursor;

	@Override
	public void run(final NatTable natTable, final MouseEvent event) {
		if (this.columnResizeCursor == null) {
			this.columnResizeCursor= new Cursor(Display.getDefault(), SWT.CURSOR_SIZEWE);
			
			natTable.addDisposeListener(new DisposeListener() {
				
				@Override
				public void widgetDisposed(final DisposeEvent e) {
					ColumnResizeCursorAction.this.columnResizeCursor.dispose();
				}
				
			});
		}
		
		natTable.setCursor(this.columnResizeCursor);
	}
	
}
