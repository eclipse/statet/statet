/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.core;

import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicBuildpathAttribute implements BuildpathAttribute {
	
	
	private final String name;
	
	private final @Nullable String value;
	
	
	public BasicBuildpathAttribute(final String name, final @Nullable String value) {
		this.name= name;
		this.value= value;
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public @Nullable String getValue() {
		return this.value;
	}
	
	
	@Override
	public int hashCode() {
		return this.name.hashCode() * 17 + Objects.hashCode(this.value);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final BasicBuildpathAttribute other
						&& this.name.equals(other.name)
						&& Objects.equals(this.value, other.value) ));
	}
	
	
	@Override
	public String toString() {
		return this.name + "=" + this.value; //$NON-NLS-1$
	}
	
}
