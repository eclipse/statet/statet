/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.core;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BuildpathElementType {
	
	
	private final String modelTypeId;
	
	private final String name;
	
	private final ImList<String> attributeBuiltinKeys;
	
	
	public BuildpathElementType(final String modelTypeId,
			final String name, final ImList<String> attributeBuiltinKeys) {
		this.modelTypeId= modelTypeId;
		this.name= name;
		this.attributeBuiltinKeys= attributeBuiltinKeys;
	}
	
	
	public String getModelTypeId() {
		return this.modelTypeId;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ImList<String> getAttributeBuiltinKeys() {
		return this.attributeBuiltinKeys;
	}
	
	public boolean isAttributeBuiltin(final String key) {
		return this.attributeBuiltinKeys.contains(key);
	}
	
	
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final BuildpathElementType other
						&& this.name == other.name ));
	}
	
}
