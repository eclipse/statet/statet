/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.TypedRegion;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.InterruptNullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.text.core.util.TextUtils;

import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.CombinedHover;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHover;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHoverDescriptor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHoverRegistry.EffectiveHovers;


/**
 * Wraps an Ltk {@link InfoHover} to an editor text hover.
 */
@NonNullByDefault
public abstract class EditorTextInfoHoverProxy implements ITextHover, ITextHoverExtension, ITextHoverExtension2 {
	
	
	private final InfoHoverDescriptor descriptor;
	private final SourceEditorViewerConfiguration sourceEditorConfig;
	
	private volatile @Nullable InfoHover hover;
	
	
	public EditorTextInfoHoverProxy(final InfoHoverDescriptor descriptor,
			final SourceEditorViewerConfiguration config) {
		this.descriptor= descriptor;
		this.sourceEditorConfig= config;
	}
	
	
	protected SourceEditor getEditor() {
		return this.sourceEditorConfig.getSourceEditor();
	}
	
	protected boolean ensureHover() {
		InfoHover hover= this.hover;
		if (hover == null) {
			synchronized (this) {
				hover= this.hover;
				if (hover == null) {
					hover= createHover();
					this.hover= hover;
				}
			}
		}
		return (hover != null);
	}
	
	private @Nullable InfoHover createHover() {
		final InfoHover hover= this.descriptor.createHover();
		if (hover instanceof CombinedHover) {
			final EffectiveHovers effectiveHovers= this.sourceEditorConfig.getConfiguredInfoHovers();
			((CombinedHover) hover).setHovers((effectiveHovers != null) ?
					effectiveHovers.getDescriptorsForCombined() :
					ImCollections.emptyList() );
		}
		return hover;
	}
	
	@Override
	public @Nullable IRegion getHoverRegion(final ITextViewer textViewer, final int offset) {
		return null;
	}
	
	@Override
	public @Nullable String getHoverInfo(final ITextViewer textViewer, final IRegion hoverRegion) {
		return null;
	}
	
	@Override
	public @Nullable Object getHoverInfo2(final ITextViewer textViewer, final IRegion hoverRegion) {
		final SourceEditor editor= getEditor();
		if (editor != null && ensureHover()) {
			try {
				final String contentType= (hoverRegion instanceof TypedRegion) ?
						((TypedRegion)hoverRegion).getType() :
						TextUtils.getContentType(editor.getViewer().getDocument(),
								editor.getDocumentContentInfo(), hoverRegion.getOffset(),
								hoverRegion.getLength() == 0 );
				
				final AssistInvocationContext context= createContext(hoverRegion, contentType,
						new NullProgressMonitor() );
				if (context != null) {
					try {
						final AtomicReference<Object> info= new AtomicReference<>();
						getInfo0(context, info, new InterruptNullProgressMonitor());
						return info.get();
					}
					catch (final StatusException e) {
						return null;
					}
				}
			}
			catch (final Exception e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, BUNDLE_ID,
						NLS.bind("An error occurred when preparing the information hover ''{0}'' (mouse).",
								this.descriptor.getName() ), e ));
			}
		}
		return null;
	}
	
	protected abstract @Nullable AssistInvocationContext createContext(IRegion region,
			String contentType,
			IProgressMonitor monitor );
	
	private void getInfo0(final AssistInvocationContext context,
			final AtomicReference<Object> infoValue,
			final ProgressMonitor m) throws StatusException {
		try {
			if (m.isCanceled()) {
				return;
			}
			
			final InfoHover hover= nonNullAssert(this.hover);
			final Object info= hover.getHoverInfo(context, m);
			if (info != null) {
				infoValue.set(info);
			}
		}
		catch (final StatusException e) {
			if (e.getStatus().getSeverity() == Status.CANCEL) {
				return;
			}
		}
	}
	
	
	@Override
	public @Nullable IInformationControlCreator getHoverControlCreator() {
		final InfoHover hover= this.hover;
		if (hover != null) {
			return hover.getHoverControlCreator();
		}
		return null;
	}
	
}
