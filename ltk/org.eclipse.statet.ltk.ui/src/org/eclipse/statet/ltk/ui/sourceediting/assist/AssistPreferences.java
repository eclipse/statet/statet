/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.quickassist.IQuickAssistAssistant;
import org.eclipse.swt.graphics.Color;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.IntPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.ui.RGBPref;

import org.eclipse.statet.ltk.ui.LtkUIPreferences;


/**
 * Preferences for content and quick assist assistant.
 */
@NonNullByDefault
public class AssistPreferences {
	
	
	private static final BooleanPref CONTENT_ASSIST_AUTO_ACTIVATION_ENABLED_PREF= new BooleanPref(
			LtkUIPreferences.ASSIST_PREF_QUALIFIER, LtkUIPreferences.CONTENT_ASSIST_AUTO_ACTIVATION_ENABLED_PREF_KEY);
	
	
	private final String qualifier;
	
	
	/**
	 * Preference for content assist auto activation
	 */
	private final BooleanPref autoActivationEnabled;
	
	/**
	 * Preference for content assist auto activation delay
	 */
	private final IntPref autoActivationDelay;
	
	
	private final Preference<Boolean> showSubstringMatches;
	
	
	/**
	 * Preference for content assist auto insert
	 */
	private final BooleanPref autoInsertSingle;
	
	/**
	 * Preference for content assist auto insert
	 */
	private final BooleanPref autoInsertPrefix;
	
	
	/**
	 * Preference key for content assist parameters color.
	 */
	private final RGBPref informationBackground;
	
	/**
	 * Preference for content assist parameters color
	 */
	private final RGBPref informationForeground;
	
//	/**
//	 * A named preference that holds the background color used in the code
//	 * assist selection dialog to mark replaced code.
//	 */
//	private final RGBPref fReplacementBackground;
//	
//	/**
//	 * A named preference that holds the foreground color used in the code
//	 * assist selection dialog to mark replaced code.
//	 */
//	private final RGBPref fReplacementForeground;
	
	
	public AssistPreferences(final String prefQualifier) {
		this.qualifier= prefQualifier;
		
		this.autoActivationEnabled= CONTENT_ASSIST_AUTO_ACTIVATION_ENABLED_PREF;
		this.autoActivationDelay= LtkUIPreferences.CONTENT_ASSIST_AUTO_ACTIVATION_DELAY_PREF;
		
		this.showSubstringMatches= LtkUIPreferences.CONTENT_ASSIST_SHOW_SUBSTRING_MATCHES_ENABLED_PREF;
		
		this.autoInsertSingle= new BooleanPref(prefQualifier, "AutoInsert.Single.enable"); //$NON-NLS-1$
		this.autoInsertPrefix= new BooleanPref(prefQualifier, "AutoInsert.Prefix.enable"); //$NON-NLS-1$
		
		this.informationBackground= LtkUIPreferences.CONTEXT_INFO_BACKGROUND_COLOR_PREF;
		this.informationForeground= LtkUIPreferences.CONTEXT_INFO_FOREGROUND_COLOR_PREF;
	}
	
	
//	/** Preference key for java content assist auto activation triggers */
//	private static final String AUTOACTIVATION_TRIGGERS_JAVA= PreferenceConstants.CODEASSIST_AUTOACTIVATION_TRIGGERS_JAVA;
//	/** Preference key for javadoc content assist auto activation triggers */
//	private static final String AUTOACTIVATION_TRIGGERS_JAVADOC= PreferenceConstants.CODEASSIST_AUTOACTIVATION_TRIGGERS_JAVADOC;
	
//	/** Preference key for visibility of proposals */
//	private static final String SHOW_VISIBLE_PROPOSALS= PreferenceConstants.CODEASSIST_SHOW_VISIBLE_PROPOSALS;
//	/** Preference key for alphabetic ordering of proposals */
//	private static final String ORDER_PROPOSALS= PreferenceConstants.CODEASSIST_ORDER_PROPOSALS;
//	/** Preference key for case sensitivity of proposals */
//	private static final String CASE_SENSITIVITY= PreferenceConstants.CODEASSIST_CASE_SENSITIVITY;
//	/** Preference key for filling argument names on method completion */
//	private static final String FILL_METHOD_ARGUMENTS= PreferenceConstants.CODEASSIST_FILL_ARGUMENT_NAMES;
	
	
	public String getGroupId() {
		return this.qualifier;
	}
	
	public BooleanPref getAutoActivationEnabledPref() {
		return this.autoActivationEnabled;
	}
	
	public Preference<Boolean> getShowSubstringMatchesPref() {
		return this.showSubstringMatches;
	}
	
	public BooleanPref getAutoInsertSinglePref() {
		return this.autoInsertSingle;
	}
	
	public BooleanPref getAutoInsertPrefixPref() {
		return this.autoInsertPrefix;
	}
	
	
	/**
	 * Configure the given content assistant according common StatET settings.
	 */
	public void configure(final ContentAssistant assistant) {
		final PreferenceAccess prefs= EPreferences.getInstancePrefs();
		
		assistant.enableAutoActivation(prefs.getPreferenceValue(this.autoActivationEnabled));
		assistant.setAutoActivationDelay(prefs.getPreferenceValue(this.autoActivationDelay));
		
		assistant.enableAutoInsert(prefs.getPreferenceValue(this.autoInsertSingle));
		assistant.enablePrefixCompletion(prefs.getPreferenceValue(this.autoInsertPrefix));
		
		{	final var color= new Color(prefs.getPreferenceValue(this.informationForeground));
			assistant.setContextInformationPopupForeground(color);
			assistant.setContextSelectorForeground(color);
		}
		{	final var color= new Color(prefs.getPreferenceValue(this.informationBackground));
			assistant.setContextInformationPopupBackground(color);
			assistant.setContextSelectorBackground(color);
		}
		
		if (assistant instanceof final ContentAssist assist) {
			assist.setShowSubstringMatches(prefs.getPreferenceValue(this.showSubstringMatches));
		}
		
//		assistant.enableColoredLabels(true);
	}
	
//	public static void configureInformationProposalMode(final ContentAssist assistant, final boolean enable) {
//		final ColorManager manager= StatetUIPlugin.getDefault().getColorManager();
//		final PreferenceAccess statet= PreferencesUtil.getInstancePrefs();
//		
//		assistant.setProposalSelectorForeground(manager.getColor(statet.getPreferenceValue(enable ?
//				PARAMETERS_FOREGROUND : PROPOSALS_FOREGROUND)));
//		assistant.setProposalSelectorBackground(manager.getColor(statet.getPreferenceValue(enable ?
//				PARAMETERS_BACKGROUND : PROPOSALS_BACKGROUND)));
//	}
	
	/**
	 * Configure the given quick assistant according common StatET settings.
	 */
	public void configure(final IQuickAssistAssistant assistant) {
	}
	
}
