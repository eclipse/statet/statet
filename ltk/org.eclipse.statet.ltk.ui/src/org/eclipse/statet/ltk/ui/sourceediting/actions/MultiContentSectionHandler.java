/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import java.util.IdentityHashMap;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler2;
import org.eclipse.jface.text.source.SourceViewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


/**
 * Command handler supporting separate implementation for each document content section type.
 * 
 * Implement {@link #createHandler(String)} for lazy initialization of the command handlers
 * of the section types.
 * 
 * @see DocContentSections
 */
@NonNullByDefault
public class MultiContentSectionHandler extends AbstractSourceEditorHandler {
	
	
	private static final IHandler2 NULL= new AbstractHandler() {
		@Override
		public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
			return null;
		}
	};
	
	
	private final DocContentSections sections;
	
	private final Map<String, IHandler2> handlers= new IdentityHashMap<>(8);
	
	
	public MultiContentSectionHandler(final DocContentSections sections) {
		this.sections= nonNullAssert(sections);
	}
	
	public MultiContentSectionHandler(final DocContentSections sections,
			final String sectionType1, final IHandler2 handler1) {
		this(sections, sectionType1, handler1, null, null);
	}
	
	public MultiContentSectionHandler(final DocContentSections sections,
			final @Nullable String sectionType1, final @Nullable IHandler2 handler1,
			final @Nullable String sectionType2, final @Nullable IHandler2 handler2) {
		this(sections);
		
		if (sectionType1 != null) {
			registerHandler(sectionType1, handler1);
		}
		if (sectionType2 != null) {
			registerHandler(sectionType2, handler2);
		}
	}
	
	
	protected final DocContentSections getSections() {
		return this.sections;
	}
	
	
	@Override
	public void dispose() {
		super.dispose();
		
		for (final Object handler : this.handlers.values()) {
			if (handler != NULL) {
				((IHandler2)handler).dispose();
			}
		}
		this.handlers.clear();
	}
	
	
	public void registerHandler(final String sectionType, final @Nullable IHandler2 handler) {
		this.handlers.put(nonNullAssert(sectionType), nonNullElse(handler, NULL));
	}
	
	protected final @Nullable IHandler2 getHandler(final String sectionType) {
		if (sectionType == DocContentSections.ERROR) {
			return null;
		}
		IHandler2 handler= this.handlers.get(sectionType);
		if (handler == null) {
			try {
				handler= createHandler(sectionType);
			}
			finally {
				registerHandler(sectionType, handler);
			}
		}
		return (handler != NULL) ? handler : null;
	}
	
	protected @Nullable IHandler2 createHandler(final String sectionType) {
		return null;
	}
	
	
	@Override
	protected @Nullable Object execute(final SourceEditor editor, final ExecutionEvent event)
			throws ExecutionException {
		final SourceViewer viewer= editor.getViewer();
		final IHandler2 handler= getHandler(this.sections.getType(
				nonNullAssert(viewer.getDocument()), viewer.getSelectedRange().x ));
		if (handler != null) {
			return handler.execute(event);
		}
		return null;
	}
	
}
