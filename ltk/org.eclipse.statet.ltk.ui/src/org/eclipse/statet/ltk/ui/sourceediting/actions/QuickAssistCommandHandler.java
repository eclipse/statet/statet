/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.isNull;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.jface.text.quickassist.IQuickAssistAssistant;
import org.eclipse.jface.text.quickassist.IQuickAssistProcessor;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistProcessorCommandExtension;


@NonNullByDefault
public class QuickAssistCommandHandler extends AbstractSourceEditorHandler
		implements IExecutableExtension {
	
	
	private String commandId;
	
	
	public QuickAssistCommandHandler(final String commandId) {
		this.commandId= commandId;
	}
	
	public QuickAssistCommandHandler() {
		this.commandId= nonNullLateInit();
	}
	
	@Override
	public void setInitializationData(final IConfigurationElement config,
			final String propertyName, final @Nullable Object data) throws CoreException {
		if (isNull(this.commandId)) {
			this.commandId= nonNullAssert(config.getAttribute("commandId")).intern(); //$NON-NLS-1$
		}
	}
	
	
	@Override
	protected boolean isEditAction() {
		return true;
	}
	
	
	@Override
	protected @Nullable Object execute(final SourceEditor editor,
			final ExecutionEvent event) throws ExecutionException {
		final var processor= getProcessor(editor);
		if (processor == null) {
			return ACTION_NOT_AVAILABLE;
		}
		final var document= nonNullAssert(editor.getViewer().getDocument());
		final var invocationContext= editor.getViewer().getQuickAssistInvocationContext();
		final AssistProposal proposal= processor.findQuickAssist(invocationContext, this.commandId);
		if (proposal == null) {
			return ACTION_NOT_AVAILABLE;
		}
		
		if (isEditAction() && !editor.isEditable(true)) {
			return DOCUMENT_NOT_EDITABLE;
		}
		proposal.apply(editor.getViewer(), (char)0, 0, invocationContext.getOffset());
		final Point selection= proposal.getSelection(document);
		if (selection != null) {
			editor.selectAndReveal(selection.x, selection.y);
		}
		return null;
	}
	
	private @Nullable QuickAssistProcessorCommandExtension getProcessor(final SourceEditor editor) {
		final IQuickAssistAssistant assistant= editor.getViewer().getQuickAssistAssistant();
		if (assistant != null) {
			final IQuickAssistProcessor processor= assistant.getQuickAssistProcessor();
			if (processor instanceof QuickAssistProcessorCommandExtension) {
				return (QuickAssistProcessorCommandExtension)processor;
			}
		}
		return null;
	}
	
}
