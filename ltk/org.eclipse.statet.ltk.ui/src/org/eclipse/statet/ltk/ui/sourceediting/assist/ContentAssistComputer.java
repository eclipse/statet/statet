/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


/**
 * Computes completions and context information displayed by the editor content assistant.
 * Contributions to the extension point <code>org.eclipse.statet.ltk.advancedContentAssist</code>
 * must implement this interface.
 */
@NonNullByDefault
public interface ContentAssistComputer {
	
	
	int COMBINED_MODE=                                      1 << 0;
	int SPECIFIC_MODE=                                      1 << 1;
	
	
	/**
	 * Informs the computer that a content assist session has started. This call will always be
	 * followed by a {@link #sessionEnded()} call, but not necessarily by calls to
	 * {@linkplain #computeCompletionProposals(ContentAssistInvocationContext, IProgressMonitor) computeCompletionProposals}
	 * or
	 * {@linkplain #computeContextInformation(ContentAssistInvocationContext, IProgressMonitor) computeContextInformation}.
	 * 
	 * @param editor the source editor the session belong to
	 * @param assist the content assistant of the editor starting the session
	 */
	void onSessionStarted(SourceEditor editor, ContentAssist assist);
	
	/**
	 * Informs the computer that a content assist session has ended. This call will always be after
	 * any calls to
	 * {@linkplain #computeCompletionProposals(ContentAssistInvocationContext, IProgressMonitor) computeCompletionProposals}
	 * and
	 * {@linkplain #computeContextInformation(ContentAssistInvocationContext, IProgressMonitor) computeContextInformation}.
	 */
	void onSessionEnded();
	
	/**
	 * Returns a list of completion proposals valid at the given invocation context.
	 * 
	 * @param context the context of the content assist invocation
	 * @param mode one of the mode constant defined in {@link ContentAssistComputer}
	 * @param proposals a set collecting the completion proposals
	 * @param monitor a progress monitor to report progress. The monitor is private to this
	 *     invocation, i.e. there is no need for the receiver to spawn a sub monitor.
	 */
	void computeCompletionProposals(final AssistInvocationContext context, int mode,
			final AssistProposalCollector proposals, final IProgressMonitor monitor);
	
	/**
	 * Returns context information objects valid at the given invocation context.
	 * 
	 * @param context the context of the content assist invocation
	 * @param proposals a set collecting the context information objects
	 * @param monitor a progress monitor to report progress. The monitor is private to this
	 *     invocation, i.e. there is no need for the receiver to spawn a sub monitor.
	 */
	void computeInformationProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor);
	
}
