/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * Simple {@link SourceEditor} for snippet editors or previewers.
 */
@NonNullByDefault
public class ViewerSourceEditorAdapter implements SourceEditor {
	
	
	private final SourceViewer sourceViewer;
	
	private final SourceEditorViewerConfigurator configurator;
	
	
	/**
	 * Creates a new adapter for the given viewer.
	 * 
	 * @param viewer the viewer
	 * @param configurator a configurator used for {@link SourceEditorAddon}, may be <code>null</code> (disables modules)
	 */
	public ViewerSourceEditorAdapter(final SourceViewer viewer, final SourceEditorViewerConfigurator configurator) {
		this.sourceViewer= viewer;
		this.configurator= configurator;
	}
	
	
	@Override
	public @Nullable IContentType getContentType() {
		return null;
	}
	
	@Override
	public @Nullable SourceUnit getSourceUnit() {
		return null;
	}
	
	@Override
	public @Nullable IWorkbenchPart getWorkbenchPart() {
		return null;
	}
	
	@Override
	public @Nullable IServiceLocator getServiceLocator() {
		return null;
	}
	
	@Override
	public SourceViewer getViewer() {
		return this.sourceViewer;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return this.configurator.getDocumentContentInfo();
	}
	
	
	@Override
	public @Nullable TextEditToolSynchronizer getTextEditToolSynchronizer() {
		return null;
	}
	
	@Override
	public boolean isEditable(final boolean validate) {
		return this.sourceViewer.isEditable();
	}
	
	@Override
	public void selectAndReveal(final int offset, final int length) {
		if (UIAccess.isOkToUse(this.sourceViewer)) {
			this.sourceViewer.setSelectedRange(offset, length);
			this.sourceViewer.revealRange(offset, length);
		}
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return null;
	}
	
}
