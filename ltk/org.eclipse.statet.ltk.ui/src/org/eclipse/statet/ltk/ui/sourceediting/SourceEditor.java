/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;


/**
 * A interface for source editors independent of IEditorPart
 */
@NonNullByDefault
public interface SourceEditor extends IAdaptable {
	
	
	/**
	 * Returns the content type the editor is intended for.
	 * 
	 * @return the content type or <code>null</code>
	 */
	@Nullable IContentType getContentType();
	
	/**
	 * Returns the source unit of editor input, if exists.
	 * 
	 * @return model element or <code>null</code>
	 */
	@Nullable SourceUnit getSourceUnit();
	
	/**
	 * Returns the part the editor belongs to
	 * 
	 * @return the part or <code>null</code>, if not in part
	 */
	@Nullable IWorkbenchPart getWorkbenchPart();
	
	/**
	 * Returns the service locator for the editor
	 * 
	 * @return service locator responsible for editor
	 */
	@Nullable IServiceLocator getServiceLocator();
	
	/**
	 * Allows access to the SourceViewer
	 * 
	 * @return the source viewer of the editor.
	 */
	SourceViewer getViewer();
	
	/**
	 * Returns the information about partitioning and content sections types of the document for 
	 * the content type the editor is configured for.
	 * 
	 * @return the document content information
	 */
	DocContentSections getDocumentContentInfo();
	
	/**
	 * Returns whether the text in this text editor (SourceViewer) can be changed by the user
	 * 
	 * @param validate causes final validation if editor input is editable
	 * @return <code>true</code> if it can be edited, and <code>false</code> if it is read-only
	 */
	boolean isEditable(boolean validate);
	
	/**
	 * Returns the region selected in this text editor
	 * 
	 * @return the selected region
	 */
	default TextRegion getSelectedRegion() {
		final Point selectedRange= getViewer().getSelectedRange();
		return LTKSelectionUtils.toTextRegion(selectedRange);
	}
	
	/**
	 * Selects and reveals the specified region in this text editor
	 *
	 * @param offset the offset of the selection
	 * @param length the length of the selection
	 */
	void selectAndReveal(int offset, int length);
	
	@Nullable TextEditToolSynchronizer getTextEditToolSynchronizer();
	
}
