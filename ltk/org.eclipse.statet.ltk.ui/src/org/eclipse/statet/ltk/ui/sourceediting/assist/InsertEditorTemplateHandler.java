/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.core.util.TextUtils;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateProposal.TemplateProposalParameters;
import org.eclipse.statet.ltk.ui.templates.SourceEditorTemplateContext;


@NonNullByDefault
public abstract class InsertEditorTemplateHandler extends AbstractHandler {
	
	
	public static final String TEMPLATE_ID_PARAMETER_ID= "templateId"; //$NON-NLS-1$
	
	
	/** plugin.xml */
	public InsertEditorTemplateHandler() {
	}
	
	
	protected abstract @Nullable TemplateCompletionComputer getComputer();
	
	protected AssistInvocationContext createContext(final SourceEditor editor)
			throws BadPartitioningException, BadLocationException {
		final TextRegion region= editor.getSelectedRegion();
		final String contentType= TextUtils.getContentType(editor.getViewer().getDocument(),
				editor.getDocumentContentInfo(), region.getStartOffset(), true );
		return new AssistInvocationContext(editor, region, contentType, ModelManager.NONE, null );
	}
	
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final TemplateCompletionComputer computer= getComputer();
		if (computer == null) {
			return null;
		}
		final Template template;
		{	final String templateId= event.getParameter(TEMPLATE_ID_PARAMETER_ID);
			template= (templateId != null) ?
					computer.getTemplateStore().findTemplateById(templateId) :
					null;
		}
		if (template == null) {
			return null;
		}
		final SourceEditor editor;
		{	final IWorkbenchPart activePart= WorkbenchUIUtils.getActivePart(event.getApplicationContext());
			editor= (activePart != null) ? activePart.getAdapter(SourceEditor.class) : null;
		}
		if (editor == null) {
			return null;
		}
		
		try {
			final AssistInvocationContext context= createContext(editor);
			final TextRegion region= context;
			final SourceEditorTemplateContext templateContext= computer.createTemplateContext(
					context, region, SourceEditorTemplateContext.FORMAT_START, true );
			if (templateContext == null) {
				return null;
			}
			
			final TemplateProposal proposal= computer.createProposal(
					new TemplateProposalParameters<>(context, region, templateContext, template) );
			
			final SourceViewer viewer= context.getSourceViewer();
			proposal.apply(viewer, (char) 0, 0, context.getEndOffset());
			
			final Point selection= proposal.getSelection(null);
			if (selection != null) {
				viewer.setSelectedRange(selection.x, selection.y);
				viewer.revealRange(selection.x, selection.y);
			}
		}
		catch (final Exception e) {
			new ExecutionException(
					NLS.bind("An error occurred when inserting the template ''{0}''.",
							template.getName() ),
					e );
		}
		
		return null;
	}
	
}
