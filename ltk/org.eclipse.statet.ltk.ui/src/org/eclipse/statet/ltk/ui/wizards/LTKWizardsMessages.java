/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.wizards;

import org.eclipse.osgi.util.NLS;


public class LTKWizardsMessages extends NLS {
	
	
	public static String NewProjectReferencePage_title;
	public static String NewProjectReferencePage_description;
	
	public static String ResourceGroup_NewFile_label;
	public static String ResourceGroup_error_EmptyName;
	public static String ResourceGroup_error_InvalidFilename;
	public static String ResourceGroup_error_ResourceExists;
	
	public static String NewElement_CreateFile_task;
	public static String NewElement_CreateProject_task;
	public static String NewElement_AddProjectToWorkingSet_task;
	public static String NewElement_error_DuringOperation_message;
	
	
	static {
		NLS.initializeMessages(LTKWizardsMessages.class.getName(), LTKWizardsMessages.class);
	}
	private LTKWizardsMessages() {}
	
}
