/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates.config;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;


class RegistryTemplateCategory extends TemplateCategory {
	
	
	private IConfigurationElement extElement;
	
	
	public RegistryTemplateCategory(final String id, final ImageDescriptor image, final String label,
			final ImageDescriptor itemImage, final IConfigurationElement extElement) {
		super(id, image, label, itemImage);
		
		this.extElement= extElement;
	}
	
	
	@Override
	ITemplateCategoryConfiguration getConfiguration() {
		ITemplateCategoryConfiguration configuration= super.getConfiguration();
		if (configuration == null) {
			if (this.extElement != null) {
				try {
					configuration= (ITemplateCategoryConfiguration) this.extElement
							.createExecutableExtension("configurationClass"); //$NON-NLS-1$
					setConfiguration(configuration);
				}
				catch (final CoreException e) {
					StatusManager.getManager().handle(new Status(IStatus.ERROR, BUNDLE_ID, 0,
							NLS.bind("An error occurred when loading template category configuration for category {0} of plugin ''{1}''",
									getId(),
									this.extElement.getContributor().getName() ),
							e ));
				}
				finally {
					this.extElement= null;
				}
			}
		}
		return configuration;
	}
	
}
