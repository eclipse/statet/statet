/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates.config;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class TemplateConfigUI {
	
	
	public static final String PREF_QUALIFIER= BUNDLE_ID + "/templates/config"; //$NON-NLS-1$
	
	
	private TemplateConfigUI() {}
	
}
