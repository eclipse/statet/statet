/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.swt;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class EnhStyledText extends StyledText {
	
	
	private @Nullable Color customBackgroundColor;
	private @Nullable Color customForegroundColor;
	
	private boolean inSetEnabled;
	
	
	public EnhStyledText(final Composite parent, final int style) {
		super(parent, style);
	}
	
	
	@Override
	public void setEnabled(final boolean enabled) {
		this.inSetEnabled= true;
		try {
			if (enabled) {
				super.setEnabled(true);
				applyCustomBackgroundColor();
				applyCustomForegroundColor();
			}
			else {
				super.setBackground(null);
				super.setForeground(null);
				super.setEnabled(false);
			}
		}
		finally {
			this.inSetEnabled= false;
		}
	}
	
	@Override
	public void setBackground(final @Nullable Color color) {
		if (this.inSetEnabled) {
			super.setBackground(color);
		}
		else {
			this.customBackgroundColor= color;
			if (isEnabled()) {
				applyCustomBackgroundColor();
			}
		}
	}
	
	private void applyCustomBackgroundColor() {
		super.setBackground(this.customBackgroundColor);
		if (this.customBackgroundColor == null) {
			super.setBackground(getBackground());
		}
	}
	
	@Override
	public void setForeground(final @Nullable Color color) {
		if (this.inSetEnabled) {
			super.setForeground(color);
		}
		else {
			this.customForegroundColor= color;
			if (isEnabled()) {
				applyCustomForegroundColor();
			}
		}
	}
	
	private void applyCustomForegroundColor() {
		super.setForeground(this.customForegroundColor);
		if (this.customForegroundColor == null) {
			super.setForeground(getForeground());
		}
	}
	
	
	public static EnhStyledText forSourceEditor(final Composite parent, final int styles) {
		final var styledText= new EnhStyledText(parent, styles);
		styledText.setLeftMargin(Math.max(styledText.getLeftMargin(), 2));
		return styledText;
	}
	
}
