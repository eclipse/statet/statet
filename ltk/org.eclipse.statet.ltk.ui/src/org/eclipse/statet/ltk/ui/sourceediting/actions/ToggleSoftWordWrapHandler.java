/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.ui.actions.ToggleBooleanPreferenceHandler;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * ITextEditorActionDefinitionIds#WORD_WRAP
 */
public class ToggleSoftWordWrapHandler extends ToggleBooleanPreferenceHandler {
	
	
	private final SourceViewer viewer;
	
	
	public ToggleSoftWordWrapHandler(final Preference<Boolean> pref,
			final SourceViewer viewer) {
		super(pref, ITextEditorActionDefinitionIds.WORD_WRAP);
		this.viewer= viewer;
		
		handleToggled(isPrefEnabled());
	}
	
	
	protected SourceViewer getViewer() {
		return this.viewer;
	}
	
	
	@Override
	protected void handleToggled(final boolean enabled) {
		final SourceViewer viewer= getViewer();
		if (UIAccess.isOkToUse(viewer)) {
			viewer.getTextWidget().setWordWrap(enabled);
		}
		
		super.handleToggled(enabled);
	}
	
	
	@Override
	public void dispose() {
		super.dispose();
	}
	
}
