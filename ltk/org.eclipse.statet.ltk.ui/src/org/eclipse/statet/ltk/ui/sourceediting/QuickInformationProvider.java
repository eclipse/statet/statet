/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.information.IInformationProvider;
import org.eclipse.jface.text.information.IInformationProviderExtension;
import org.eclipse.jface.text.information.IInformationProviderExtension2;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;

import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.ui.LtkActions;


@NonNullByDefault
public abstract class QuickInformationProvider implements IInformationProvider,
		IInformationProviderExtension, IInformationProviderExtension2 {
	
	// see IJavaEditorActionDefinitionIds
	
	private static String textOperation2commandId(final int operation) {
		switch (operation) {
		case SourceEditorViewer.SHOW_SOURCE_OUTLINE:
			return LtkActions.SHOW_QUICK_SOURCE_OUTLINE_COMMAND_ID;
		case SourceEditorViewer.SHOW_ELEMENT_OUTLINE:
			return LtkActions.SHOW_QUICK_ELEMENT_OUTLINE_COMMAND_ID;
		case SourceEditorViewer.SHOW_ELEMENT_HIERARCHY:
			return LtkActions.SHOW_QUICK_ELEMENT_HIERARCHY_COMMAND_ID;
		default:
			throw new UnsupportedOperationException(Integer.toString(operation));
		}
	}
	
	
	private final SourceEditor editor;
	
	private final String modelTypeId;
	
	private final String commandId;
	
	private @Nullable IInformationControlCreator creator;
	
	
	public QuickInformationProvider(final SourceEditor editor, final String modelType,
			final int textOperation) {
		this(editor, modelType, textOperation2commandId(textOperation));
	}
	
	public QuickInformationProvider(final SourceEditor editor, final String modelType,
			final String commandId) {
		this.editor= editor;
		this.modelTypeId= modelType;
		this.commandId= commandId;
	}
	
	
	public SourceEditor getEditor() {
		return this.editor;
	}
	
	public final String getCommandId() {
		return this.commandId;
	}
	
	public final String getModelTypeId() {
		return this.modelTypeId;
	}
	
	
	@Override
	public IRegion getSubject(final ITextViewer textViewer, final int offset) {
		return new Region(offset, 0);
	}
	
	@Override
	public @Nullable String getInformation(final ITextViewer textViewer, final IRegion subject) {
		return null;
	}
	
	@Override
	public @Nullable Object getInformation2(final ITextViewer textViewer, final IRegion subject) {
		final SourceUnit su= this.editor.getSourceUnit();
		if (su == null) {
			return null;
		}
		final SourceUnitModelInfo modelInfo= su.getModelInfo(getModelTypeId(), ModelManager.MODEL_FILE,
				new NullProgressMonitor() ); // ?
		if (modelInfo == null) {
			return null;
		}
		return LtkModelUtils.getCoveringSourceElement(modelInfo.getSourceElement(),
				JFaceTextRegion.toTextRegion(subject) );
	}
	
	@Override
	public IInformationControlCreator getInformationPresenterControlCreator() {
		var creator= this.creator;
		if (creator == null) {
			creator= createInformationPresenterControlCreator();
			this.creator= creator;
		}
		return creator;
	}
	
	protected abstract IInformationControlCreator createInformationPresenterControlCreator();
	
}
