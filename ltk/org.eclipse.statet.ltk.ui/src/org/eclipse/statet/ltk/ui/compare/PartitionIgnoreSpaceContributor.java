/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.compare;

import org.eclipse.compare.contentmergeviewer.IIgnoreWhitespaceContributor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension3;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;

import org.eclipse.statet.internal.ltk.ui.LtkUIPlugin;


@NonNullByDefault
public class PartitionIgnoreSpaceContributor implements IIgnoreWhitespaceContributor {
	
	
	private final IDocument document;
	
	private final String partitioning;
	private final PartitionConstraint partitionConstraint;
	
	
	public PartitionIgnoreSpaceContributor(final IDocument document,
			final String partitioning, final PartitionConstraint partitionConstraint) {
		if (!(document instanceof IDocumentExtension3)) {
			throw new IllegalArgumentException();
		}
		this.document= document;
		this.partitioning= partitioning;
		this.partitionConstraint= partitionConstraint;
	}
	
	
	@Override
	public boolean isIgnoredWhitespace(final int lineNumber, final int columnNumber) {
		try {
			final int offset= this.document.getLineOffset(lineNumber) + columnNumber;
			final var partition= ((IDocumentExtension3)this.document).getPartition(this.partitioning, offset, false);
			return (!this.partitionConstraint.matches(partition.getType()));
		}
		catch (final BadPartitioningException | BadLocationException e) {
			LtkUIPlugin.logUncriticalError(e);
			return true;
		}
	}
	
}
