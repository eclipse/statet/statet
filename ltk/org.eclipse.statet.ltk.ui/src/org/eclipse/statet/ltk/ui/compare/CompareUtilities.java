/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.platform: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.compare;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import org.eclipse.compare.IEncodedStreamContentAccessor;
import org.eclipse.compare.IStreamContentAccessor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
class CompareUtilities {
	
	
	public static String readString(final IStreamContentAccessor input) {
		try {
			String encoding= null;
			if (input instanceof IEncodedStreamContentAccessor) {
				encoding= ((IEncodedStreamContentAccessor)input).getCharset();
			}
			if (encoding == null) {
				encoding= ResourcesPlugin.getEncoding();
			}
			return readString(input, encoding);
		}
		catch (final CoreException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, BUNDLE_ID, -1,
					"An error occurred when loading compare input.", e));
		}
		return ""; //$NON-NLS-1$
	}
	
	public static String readString(final IStreamContentAccessor sca, final String encoding) throws CoreException {
		try {
			final var charset= Charset.forName(encoding);
			try (final var in= sca.getContents()) {
//				return IOUtils.readContentString(in, charset); is BOM already stripped?
				return new String(in.readAllBytes(), charset);
			}
		}
		catch (final IOException | UnsupportedCharsetException e) {
			throw new CoreException(new Status(IStatus.ERROR, BUNDLE_ID, -1, e.getMessage(), e));
		}
	}
	
}
