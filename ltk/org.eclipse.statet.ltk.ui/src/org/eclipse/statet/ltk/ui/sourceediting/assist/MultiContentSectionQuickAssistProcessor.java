/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.IdentityHashMap;
import java.util.Map;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.quickassist.IQuickAssistInvocationContext;
import org.eclipse.jface.text.quickassist.IQuickAssistProcessor;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.ISourceViewer;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;


/**
 * Quick assist processor supporting separate implementations for each document content section
 * type.
 * 
 * Implement {@link #createProcessor(String)} for lazy initialization of the processors of the
 * section types.
 * 
 * @see DocContentSections
 */
@NonNullByDefault
public class MultiContentSectionQuickAssistProcessor
		implements IQuickAssistProcessor, QuickAssistProcessorCommandExtension {
	
	
	private static final Object NULL= new Object();
	
	
	private final DocContentSections sections;
	
	private final Map<String, Object> processors= new IdentityHashMap<>(8);
	
	private @Nullable String errorMessage;
	
	
	public MultiContentSectionQuickAssistProcessor(final DocContentSections sections) {
		this.sections= nonNullAssert(sections);
	}
	
	
	protected void registerProcessor(final String sectionType, final IQuickAssistProcessor processor) {
		if (sectionType == null) {
			throw new NullPointerException("sectionType"); //$NON-NLS-1$
		}
		this.processors.put(sectionType, (processor != null) ? processor : NULL);
	}
	
	protected final @Nullable IQuickAssistProcessor getProcessor(final String sectionType) {
		if (sectionType == DocContentSections.ERROR) {
			return null;
		}
		Object processor= this.processors.get(sectionType);
		if (processor == null) {
			processor= NULL;
			try {
				final IQuickAssistProcessor newProcessor= createProcessor(sectionType);
				// newProcessor.init
				if (newProcessor != null) {
					processor= newProcessor;
				}
			}
			finally {
				this.processors.put(sectionType, processor);
			}
		}
		return (processor != NULL) ? (IQuickAssistProcessor) processor : null;
	}
	
	protected @Nullable IQuickAssistProcessor createProcessor(final String sectionType) {
		return null;
	}
	
	public @Nullable IQuickAssistProcessor getProcessor(final IQuickAssistInvocationContext invocationContext) {
		final ISourceViewer sourceViewer= invocationContext.getSourceViewer();
		final IDocument document;
		if (sourceViewer == null || (document= sourceViewer.getDocument()) == null) {
			return null;
		}
		return getProcessor(this.sections.getType(document, invocationContext.getOffset()));
	}
	
	
	@Override
	public boolean canFix(final Annotation annotation) {
		return false;
	}
	
	@Override
	public boolean canAssist(final IQuickAssistInvocationContext invocationContext) {
		return false;
	}
	
	
	@Override
	public @NonNull ICompletionProposal @Nullable [] computeQuickAssistProposals(
			final IQuickAssistInvocationContext invocationContext) {
		this.errorMessage= null;
		final IQuickAssistProcessor processor= getProcessor(invocationContext);
		if (processor != null) {
			try {
				return processor.computeQuickAssistProposals(invocationContext);
			}
			finally {
				this.errorMessage= processor.getErrorMessage();
			}
		}
		return null;
	}
	
	@Override
	public @Nullable String getErrorMessage() {
		return this.errorMessage;
	}
	
	
	@Override
	public @Nullable AssistProposal findQuickAssist(
			final IQuickAssistInvocationContext invocationContext,
			final String commandId) {
		final IQuickAssistProcessor processor= getProcessor(invocationContext);
		if (processor instanceof QuickAssistProcessorCommandExtension) {
			return ((QuickAssistProcessorCommandExtension)processor)
					.findQuickAssist(invocationContext, commandId);
		}
		return null;
	}
	
}
