/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationExtension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class AssistCompletionInformationProposalWrapper implements AssistInformationProposal,
		IContextInformationExtension {
	
	
	private final AssistProposal completionProposal;
	
	private @Nullable AssistInvocationContext context;
	
	
	public AssistCompletionInformationProposalWrapper(
			final AssistProposal completionProposal,
			final AssistInvocationContext context) {
		this.completionProposal= nonNullAssert(completionProposal);
		this.context= context;
	}
	
	
	@Override
	public String getContextDisplayString() {
		return this.completionProposal.getDisplayString();
	}
	
	@Override
	public @Nullable Image getImage() {
		return this.completionProposal.getImage();
	}
	
	
	public IContextInformation getContextInformation() {
		final AssistInvocationContext context= this.context;
		if (context != null) {
			try {
				this.completionProposal.apply(context.getSourceViewer(), SWT.CR, 0,
						context.getInvocationOffset() );
			}
			finally {
				this.context= null;
			}
		}
		return this.completionProposal.getContextInformation();
	}
	
	@Override
	public String getInformationDisplayString() {
		final IContextInformation contextInformation= getContextInformation();
		return contextInformation.getInformationDisplayString();
	}
	
	@Override
	public int getContextInformationPosition() {
		final IContextInformation contextInformation= getContextInformation();
		return (contextInformation instanceof IContextInformationExtension) ?
				((IContextInformationExtension) contextInformation).getContextInformationPosition() :
				-1;
	}
	
	
	@Override
	public int hashCode() {
		return this.completionProposal.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof AssistCompletionInformationProposalWrapper
						&& this.completionProposal.equals(((AssistCompletionInformationProposalWrapper) obj).completionProposal) )
				);
	}
	
}
