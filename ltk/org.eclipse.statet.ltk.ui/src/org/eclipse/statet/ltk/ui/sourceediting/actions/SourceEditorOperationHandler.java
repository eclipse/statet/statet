/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public class SourceEditorOperationHandler extends AbstractSourceEditorHandler {
	
	
	private final int textOperation;
	
	
	public SourceEditorOperationHandler(final int textOperation) {
		super();
		this.textOperation= textOperation;
	}
	
	public SourceEditorOperationHandler(final SourceEditor editor, final int textOperation) {
		super(editor);
		this.textOperation= textOperation;
	}
	
	
	protected final int getTextOperation() {
		return this.textOperation;
	}
	
	protected @Nullable ITextOperationTarget getOperationTarget(final SourceEditor editor) {
		ITextOperationTarget target= editor.getAdapter(ITextOperationTarget.class);
		if (target == null) {
			target= editor.getViewer().getTextOperationTarget();
		}
		return target;
	}
	
	
	@Override
	protected boolean isEditAction() {
		switch (this.textOperation) {
		case ITextOperationTarget.UNDO:
		case ITextOperationTarget.REDO:
		case ITextOperationTarget.CUT:
		case ITextOperationTarget.PASTE:
		case ITextOperationTarget.DELETE:
		case ITextOperationTarget.SHIFT_RIGHT:
		case ITextOperationTarget.SHIFT_LEFT:
		case ITextOperationTarget.PREFIX:
		case ITextOperationTarget.STRIP_PREFIX:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	protected boolean computeEnabled(final SourceEditor editor, final @Nullable Object evaluationContext) {
		if (super.computeEnabled(editor, evaluationContext)) {
			final ITextOperationTarget operationTarget= getOperationTarget(editor);
			return (operationTarget != null
					&& operationTarget.canDoOperation(this.textOperation) );
		}
		return false;
	}
	
	
	@Override
	protected @Nullable Object execute(final SourceEditor editor, final ExecutionEvent event)
			throws ExecutionException {
		final ITextOperationTarget operationTarget= getOperationTarget(editor);
		if (operationTarget != null && operationTarget.canDoOperation(this.textOperation)) {
			operationTarget.doOperation(this.textOperation);
			return null;
		}
		
		Display.getCurrent().beep();
		return null;
	}
	
}
