/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.swt.custom.ST;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public class GotoNextWordHandler extends SourceEditorTextHandler {
	
	
	public GotoNextWordHandler(final SourceEditor editor) {
		super(editor);
	}
	
	
	@Override
	protected int getTextActionId() {
		return ST.WORD_NEXT;
	}
	
	@Override
	protected void exec(final ExecData data) throws BadLocationException {
		final int newDocOffset= findNextWordOffset(data, data.getCaretDocOffset(), false);
		final int newWidgetOffset= data.toWidgetOffset(newDocOffset);
		if (newWidgetOffset >= 0) {
			if (data.getCaretWidgetOffset() != newWidgetOffset) {
				data.getWidget().setCaretOffset(newWidgetOffset);
			}
		}
		else {
			data.getWidget().invokeAction(ST.COLUMN_NEXT);
		}
	}
	
}
