/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates;

import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.ui.editors.text.templates.ContributionContextTypeRegistry;


public class WaContributionContextTypeRegistry extends ContributionContextTypeRegistry {
	
	
	private final String id;
	
	
	public WaContributionContextTypeRegistry(final String id) {
		super(id);
		
		this.id= id;
	}
	
	
	@Override
	public void addContextType(final String id) {
		assert (id != null);
		if (super.getContextType(id) != null) {
			return;
		}
		final TemplateContextType type= loadContextType(id);
		if (type != null) {
			addContextType(type);
		}
	}
	
	protected TemplateContextType loadContextType(final String id) {
		final TemplateContextType type= createContextType(id);
		if (type != null) {
			if (type instanceof IWaTemplateContextTypeExtension1) {
				((IWaTemplateContextTypeExtension1) type).init();
			}
			addContextType(type);
		}
		return type;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder("ContributionContextTypeRegistry ("); //$NON-NLS-1$
		sb.append("id= ").append(this.id); //$NON-NLS-1$
		sb.append(")"); //$NON-NLS-1$
		return sb.toString();
	}
	
}
