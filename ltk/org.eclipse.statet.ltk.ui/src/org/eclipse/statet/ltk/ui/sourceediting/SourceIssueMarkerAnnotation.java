/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationPresentation;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.texteditor.MarkerAnnotation;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ltk.ui.sourceediting.AnnotationPresentationConfig;
import org.eclipse.statet.ltk.issues.core.Issue;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;


@NonNullByDefault
final class SourceIssueMarkerAnnotation<TIssue extends Issue> extends MarkerAnnotation
		implements SourceIssueAnnotation<TIssue>, IAnnotationPresentation {
	
	
	final IssueTypeSet.IssueCategory<TIssue> issueCategory;
	
	private @Nullable SourceIssueEditorAnnotation overlay;
	
	private boolean isControlled;
	
	private final AnnotationPresentationConfig presentationConfig;
	
	
	public SourceIssueMarkerAnnotation(final IssueTypeSet.IssueCategory<TIssue> issueCategory,
			final String type, final IMarker marker) {
		super(type, marker);
		this.issueCategory= issueCategory;
		
		this.presentationConfig= SourceIssueEditorAnnotation.getIssuePresentationConfig(
				issueCategory, type );
	}
	
	
	@Override
	public IssueTypeSet.IssueCategory<TIssue> getIssueCategory() {
		return this.issueCategory;
	}
	
	
	boolean isControlled() {
		return this.isControlled;
	}
	
	@Override
	public boolean isMarkedDeleted() {
		return (this.isControlled || super.isMarkedDeleted());
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public @Nullable SourceIssueAnnotation<TIssue> getOverlay() {
		return (SourceIssueAnnotation)this.overlay;
	}
	
	@Override
	public ImList<Annotation> getOverlaidAnnotations() {
		return ImCollections.emptyList();
	}
	
	void disableOverlay() {
		this.isControlled= false;
		final var prev= this.overlay;
		if (prev != null) {
			prev.removeOverlaidAnnotation(this);
		}
		this.overlay= null;
	}
	
	void setOverlay(final @Nullable SourceIssueEditorAnnotation annotation) {
		this.isControlled= true;
		final var prev= this.overlay;
		if (prev != null) {
			prev.removeOverlaidAnnotation(this);
		}
		this.overlay= annotation;
		if (annotation != null) {
			annotation.addOverlaidAnnotation(this);
		}
	}
	
	
	@Override
	@SuppressWarnings("deprecation")
	public int getLayer() {
		return super.getLayer();
	}
	
	@Override
	@SuppressWarnings("deprecation")
	public void paint(final GC gc, final Canvas canvas, final Rectangle r) {
		if (getOverlay() != null) {
			return;
		}
		super.paint(gc, canvas, r);
	}
	
	@Override
	@SuppressWarnings("deprecation")
	protected @Nullable Image getImage(final Display display) {
		Image image;
		if (isMarkedDeleted()) {
			image= this.presentationConfig.getAwayImage();
		}
		else {
			image= this.presentationConfig.getImage();
		}
		if (image != null) {
			return image;
		}
		return super.getImage(display);
	}
	
}
