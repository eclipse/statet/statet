/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.resource.CompositeImageDescriptor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.SharedUIResources;


@NonNullByDefault
public class DecoratedElementImageDescriptor extends CompositeImageDescriptor {
	
	
	/** Flag to render the info adornment. */
	public final static int INFO=                           0b1 << 0;
	
	/** Flag to render the warning adornment. */
	public final static int WARNING=                        0b1 << 1;
	
	/** Flag to render the error adornment. */
	public final static int ERROR=                          0b1 << 2;
	
	
	/**
	 * Flag to render the 'deprecated' adornment.
	 */
	public final static int DEPRECATED=                     0b1 << 4;
	
	/**
	 * Flag to render the 'ignore optional compile problems' adornment.
	 */
	public final static int IGNORE_OPTIONAL_PROBLEMS=       0b1 << 8;
	
	
	private final ImageDescriptor baseImage;
	private final int flags;
	
	private @Nullable Point size;
	
	
	/**
	 * Creates a new JavaElementImageDescriptor.
	 *
	 * @param baseImage an image descriptor used as the base image
	 * @param flags flags indicating which adornments are to be rendered. See {@link #setAdornments(int)}
	 * 	for valid values.
	 * @param size the size of the resulting image
	 */
	public DecoratedElementImageDescriptor(final ImageDescriptor baseImage, final int flags) {
		this.baseImage= nonNullAssert(baseImage);
		this.flags= flags;
	}
	
	
	protected final ImageDescriptor getBaseImage() {
		return this.baseImage;
	}
	
	protected final int getFlags() {
		return this.flags;
	}
	
	@Override
	protected final Point getSize() {
		var size= this.size;
		if (size == null) {
			final var data= createCachedImageDataProvider(getBaseImage());
			size= new Point(data.getWidth(), data.getHeight());
			this.size= size;
		}
		return size;
	}
	
	
	@Override
	protected void drawCompositeImage(final int width, final int height) {
		if ((this.flags & DEPRECATED) != 0) { // draw *behind* the full image
			final Point size= getSize();
			final var data= createCachedImageDataProvider(
					SharedUIResources.getInstance().getImageDescriptor(
							SharedUIResources.OVR_DEPRECATED_IMAGE_ID ));
			drawImage(data, 0, size.y - data.getHeight());
		}
		
		{	final var data= createCachedImageDataProvider(getBaseImage());
			drawImage(data, 0, 0);
		}
		
		drawTopRight();
		drawBottomRight();
		drawBottomLeft();
	}
	
	private void addTopRightImage(final ImageDescriptor desc, final Point pos) {
		final var data= createCachedImageDataProvider(desc);
		final int x= pos.x - data.getWidth();
		if (x >= 0) {
			drawImage(data, x, pos.y);
			pos.x= x;
		}
	}
	
	private void addBottomRightImage(final ImageDescriptor desc, final Point pos) {
		final var data= createCachedImageDataProvider(desc);
		final int x= pos.x - data.getWidth();
		final int y= pos.y - data.getHeight();
		if (x >= 0 && y >= 0) {
			drawImage(data, x, y);
			pos.x= x;
		}
	}
	
	private void addBottomLeftImage(final ImageDescriptor desc, final Point pos) {
		final var data= createCachedImageDataProvider(desc);
		final int x= pos.x;
		final int y= pos.y - data.getHeight();
		final int xEnd= x + data.getWidth();
		if (xEnd < getSize().x && y >= 0) {
			drawImage(data, x, y);
			pos.x= xEnd;
		}
	}
	
	
	private void drawTopRight() {
	}
	
	private void drawBottomRight() {
	}
	
	private void drawBottomLeft() {
		final Point pos= new Point(0, getSize().y);
		if ((this.flags & ERROR) != 0) {
			addBottomLeftImage(
					SharedUIResources.getInstance().getImageDescriptor(
							SharedUIResources.OVR_ERROR_IMAGE_ID ),
					pos );
		}
		else if ((this.flags & WARNING) != 0) {
			addBottomLeftImage(
					SharedUIResources.getInstance().getImageDescriptor(
							SharedUIResources.OVR_WARNING_IMAGE_ID ),
					pos );
		}
		else if ((this.flags & INFO) != 0) {
			addBottomLeftImage(
					SharedUIResources.getInstance().getImageDescriptor(
							SharedUIResources.OVR_INFO_IMAGE_ID ),
					pos );
		}
		if ((this.flags & IGNORE_OPTIONAL_PROBLEMS) != 0) {
			addBottomLeftImage(
					SharedUIResources.getInstance().getImageDescriptor(
							SharedUIResources.OVR_IGNORE_OPTIONAL_PROBLEMS_IMAGE_ID ),
					pos );
		}
	}
	
	
	@Override
	public int hashCode() {
		return (this.baseImage.hashCode() ^ this.flags);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj != null && getClass() == obj.getClass()) {
			final DecoratedElementImageDescriptor other= (DecoratedElementImageDescriptor)obj;
			return (this.baseImage.equals(other.baseImage)
					&& this.flags == other.flags );
		}
		return false;
	}
	
}
