/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.source.ISourceViewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.LtkActions;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;


/**
 * A content assist executor can invoke content assist for a specific proposal category on an editor.
 */
@NonNullByDefault
public final class SpecificContentAssistHandler extends SourceEditorOperationHandler {
	
	
	private final ContentAssist contentAssist;
	
	
	/**
	 * Creates a new handler.
	 *
	 * @param registry the computer registry to use for the enablement of proposal categories
	 */
	public SpecificContentAssistHandler(final SourceEditor editor, final ContentAssist contentAssist) {
		super(editor, ISourceViewer.CONTENTASSIST_PROPOSALS);
		this.contentAssist= contentAssist;
	}
	
	
	/**
	 * Invokes content assist on <code>editor</code>, showing only proposals computed by the
	 * <code>CompletionProposalCategory</code> with the given <code>categoryId</code>.
	 *
	 * @param editor the editor to invoke code assist on
	 * @param categoryId the id of the proposal category to show proposals for
	 */
	@Override
	protected @Nullable Object execute(final SourceEditor editor, final ExecutionEvent event)
			throws ExecutionException {
		final String par= event.getParameter(LtkActions.SPECIFIC_CONTENT_ASSIST_CATEGORY_PARAMETER_ID);
		if (par == null) {
			return null;
		}
		
		final ITextOperationTarget operationTarget= getOperationTarget(editor);
		if (operationTarget != null && operationTarget.canDoOperation(getTextOperation())) {
			this.contentAssist.showPossibleCompletions(par);
			return null;
		}
		
		return null;
	}
	
}
