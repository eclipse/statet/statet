/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BlockTextSelection;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.swt.custom.ST;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public class DeleteNextWordHandler extends SourceEditorTextHandler {
	
	
	public DeleteNextWordHandler(final SourceEditor editor) {
		super(editor);
	}
	
	
	@Override
	protected int getTextActionId() {
		return ST.DELETE_WORD_NEXT;
	}
	
	@Override
	protected void exec(final ExecData data) throws BadLocationException {
		if (data.getWidget().getBlockSelection()) {
			final BlockTextSelection blockSelection= (BlockTextSelection) data.getViewer().getSelection();
			if (blockSelection.getEndColumn() > blockSelection.getStartColumn()
					|| data.getCaretDocOffset() == data.getCaretDocLineEndOffset()) {
				data.getWidget().invokeAction(ST.DELETE_NEXT);
				return;
			}
			final int newDocOffset;
			if (blockSelection.getStartColumn() > data.getCaretDocLineInformation().getLength()) {
				if (blockSelection.getStartLine() == blockSelection.getEndLine()) {
					data.getWidget().invokeAction(ST.LINE_END);
					return;
				}
				newDocOffset= data.getCaretDocLineEndOffset();
			}
			else {
				newDocOffset= findNextWordOffset(data, data.getCaretDocOffset(), true);
			}
			final int newWidgetOffset= data.toWidgetOffset(newDocOffset);
			if (newWidgetOffset >= 0) {
				expandBlockSelection(data, newWidgetOffset);
				data.getWidget().invokeAction(ST.DELETE_NEXT);
			}
		}
		else if (data.getWidget().getSelectionCount() > 0) {
			data.getWidget().invokeAction(ST.DELETE_NEXT);
		}
		else {
			final int newDocOffset= findNextWordOffset(data, data.getCaretDocOffset(), false);
			final IRegion docRegion= new Region(data.getCaretDocOffset(),
					newDocOffset - data.getCaretDocOffset() );
			delete(data, docRegion);
		}
	}
	
}
