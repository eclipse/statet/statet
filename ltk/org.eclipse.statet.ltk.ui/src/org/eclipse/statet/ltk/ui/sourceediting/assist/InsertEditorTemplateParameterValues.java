/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.IParameterValues;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.text.templates.ContextTypeRegistry;
import org.eclipse.text.templates.TemplatePersistenceData;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.templates.EnhTemplateStore;


@NonNullByDefault
public abstract class InsertEditorTemplateParameterValues implements IParameterValues {
	
	
	/** plugin.xml */
	public InsertEditorTemplateParameterValues() {
	}
	
	
	protected abstract EnhTemplateStore getTemplateStore();
	
	
	@Override
	public Map<String, String> getParameterValues() {
		final EnhTemplateStore templateStore= getTemplateStore();
		final ContextTypeRegistry contextTypeRegistry= templateStore.getContextTypeRegistry();
		
		final ImList<? extends TemplatePersistenceData> datas= templateStore.getTemplateDatas(false);
		
		final Map<String, String> parameters= new HashMap<>(datas.size());
		final List<String> labels= new ArrayList<>(datas.size());
		final StringBuilder sb= new StringBuilder();
		for (final TemplatePersistenceData data : datas) {
			final Template template= data.getTemplate();
			if (template != null) {
				final TemplateContextType contextType= contextTypeRegistry.getContextType(template.getContextTypeId());
				if (contextType == null) {
					continue;
				}
				
				sb.setLength(0);
				sb.append(contextType.getName());
				sb.append("\u2002>\u2002"); //$NON-NLS-1$
				
				sb.append(template.getName());
				if (!template.getDescription().isEmpty()) {
					sb.append(SourceProposal.QUALIFIER_SEPARATOR);
					sb.append(template.getDescription());
				}
				
				final String label= sb.toString();
				if (labels.contains(label)) {
					continue;
				}
				labels.add(label);
				
				final String id= data.getId();
				if (id != null) {
					parameters.put(label, id);
				}
			}
		}
		return parameters;
	}
	
}
