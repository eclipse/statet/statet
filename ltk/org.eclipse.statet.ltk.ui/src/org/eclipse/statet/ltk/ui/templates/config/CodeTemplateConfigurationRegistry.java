/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates.config;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;


public class CodeTemplateConfigurationRegistry {
	
	
	private static final String CATEGORY_ELEMENT= "category"; //$NON-NLS-1$
	
	
	private final String extensionPointId;
	
	private final List<TemplateCategory> categories;
	
	
	public CodeTemplateConfigurationRegistry(final String extensionPointId) {
		this.extensionPointId= extensionPointId;
		
		this.categories= new ArrayList<>();
		load(this.categories);
	}
	
	
	protected void load(final List<TemplateCategory> categories) {
		final IConfigurationElement[] elements= Platform.getExtensionRegistry()
				.getConfigurationElementsFor(this.extensionPointId);
		for (int i= 0; i < elements.length; i++) {
			switch (elements[i].getName()) {
			case CATEGORY_ELEMENT:
				createCategory(elements[i]);
				break;
			}
		}
	}
	
	private void createCategory(final IConfigurationElement element) {
		final String id= loadRequiredText(element, "id"); //$NON-NLS-1$
		final ImageDescriptor image= loadImage(element, "image"); //$NON-NLS-1$
		final String label= loadRequiredText(element, "label"); //$NON-NLS-1$
		final ImageDescriptor itemImage= loadImage(element, "itemImage"); //$NON-NLS-1$
		
		if (id != null && label != null) {
			this.categories.add(new RegistryTemplateCategory(id.intern(), image, label, itemImage,
					element ));
		}
	}
	
	
	String loadRequiredText(final IConfigurationElement element, final String attrName) {
		final String value= element.getAttribute(attrName);
		if (value != null && !value.isEmpty()) {
			return value;
		}
		return null;
	}
	
	ImageDescriptor loadImage(final IConfigurationElement element, final String attrName) {
		final String path= element.getAttribute(attrName);
		if (path != null) {
			return AbstractUIPlugin.imageDescriptorFromPlugin(element.getContributor().getName(), path);
		}
		return null;
	}
	
	
	public List<? extends TemplateCategory> getCategories() {
		return this.categories;
	}
	
}
