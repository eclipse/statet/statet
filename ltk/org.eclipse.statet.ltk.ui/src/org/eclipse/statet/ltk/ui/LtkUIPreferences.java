/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.IntPref;
import org.eclipse.statet.ecommons.preferences.ui.RGBPref;
import org.eclipse.statet.ecommons.text.ui.settings.DecorationPreferences;

import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistPreferences;


@NonNullByDefault
public class LtkUIPreferences {
	
	
	private static DecorationPreferences DECORATION_PREFERENCES= new DecorationPreferences(BUNDLE_ID);
	
	public static DecorationPreferences getEditorDecorationPreferences() {
		return DECORATION_PREFERENCES;
	}
	
	
	public static final String ASSIST_GROUP_ID= "Ltk/assist"; //$NON-NLS-1$
	
	public static final String ASSIST_PREF_QUALIFIER= BUNDLE_ID + "/assist"; //$NON-NLS-1$
	
	public static final String CONTENT_ASSIST_AUTO_ACTIVATION_ENABLED_PREF_KEY=
			"ContentAssist.AutoActivation.enabled"; //$NON-NLS-1$
	public static final String CONTENT_ASSIST_AUTO_ACTIVATION_DELAY_PREF_KEY=
			"ContentAssist.AutoActivation.delay"; //$NON-NLS-1$
	public static final IntPref CONTENT_ASSIST_AUTO_ACTIVATION_DELAY_PREF= new IntPref(
			ASSIST_PREF_QUALIFIER, CONTENT_ASSIST_AUTO_ACTIVATION_DELAY_PREF_KEY );
	
	public static final String CONTENT_ASSIST_SHOW_SUBSTRING_MATCHES_ENABLED_PREF_KEY=
			"ContentAssist.Show.SubstringMatches.enabled"; //$NON-NLS-1$
	public static final Preference<Boolean> CONTENT_ASSIST_SHOW_SUBSTRING_MATCHES_ENABLED_PREF= new BooleanPref(
			ASSIST_PREF_QUALIFIER, CONTENT_ASSIST_SHOW_SUBSTRING_MATCHES_ENABLED_PREF_KEY );
	
	public static final String CONTEXT_INFO_BACKGROUND_COLOR_PREF_KEY=
			"ContextInfo.Background.color"; //$NON-NLS-1$
	public static final RGBPref CONTEXT_INFO_BACKGROUND_COLOR_PREF= new RGBPref(
			ASSIST_PREF_QUALIFIER, CONTEXT_INFO_BACKGROUND_COLOR_PREF_KEY );
	
	public static final String CONTEXT_INFO_FOREGROUND_COLOR_PREF_KEY=
			"ContextInfo.Foreground.color"; //$NON-NLS-1$
	public static final RGBPref CONTEXT_INFO_FOREGROUND_COLOR_PREF= new RGBPref(
			ASSIST_PREF_QUALIFIER, CONTEXT_INFO_FOREGROUND_COLOR_PREF_KEY );
	
	private final static AssistPreferences ASSIST_PREFERENCES= new AssistPreferences(ASSIST_PREF_QUALIFIER);
	
	public static AssistPreferences getAssistPreferences() {
		return ASSIST_PREFERENCES;
	}
	
}
