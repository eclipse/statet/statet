/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;


@NonNullByDefault
public interface SourceStructContentProvider {
	
	
	default @Nullable SourceStructElement<?, ?> getSourceParent(
			final SourceStructElement<?, ?> element) {
		return element.getSourceParent();
	}
	
	default boolean hasSourceChildren(
			final SourceStructElement<?, ?> element,
			final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return element.hasSourceChildren(filter);
	}
	
	default List<? extends SourceStructElement<?, ?>> getSourceChildren(
			final SourceStructElement<?, ?> element,
			final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter,
			final @Nullable List<SourceStructElement<?, ?>> tmpList) {
		return element.getSourceChildren(filter);
	}
	
}
