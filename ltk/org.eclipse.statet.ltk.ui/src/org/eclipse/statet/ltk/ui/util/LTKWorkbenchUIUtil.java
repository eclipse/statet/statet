/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.util;

import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.IEditorStatusLine;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.SharedUIResources;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


/**
 * Util methods for Eclipse IDE workbench
 */
public class LTKWorkbenchUIUtil {
	
	
	public static AbstractDocument getDocument(final IWorkbenchPart part) {
		{	final SourceEditor editor= part.getAdapter(SourceEditor.class);
			if (editor != null) {
				return (AbstractDocument) editor.getViewer().getDocument();
			}
		}
		if (part instanceof final AbstractTextEditor textEditor) {
			final IDocumentProvider documentProvider= textEditor.getDocumentProvider();
			if (documentProvider != null) {
				return (AbstractDocument) documentProvider.getDocument(textEditor.getEditorInput());
			}
		}
		return null;
	}
	
	public static String getContentTypeId(final IWorkbenchPart part) {
		{	final IContentType contentType= part.getAdapter(IContentType.class);
			if (contentType != null) {
				return contentType.getId();
			}
		}
		if (part instanceof IEditorPart) {
			final IEditorInput input= ((IEditorPart) part).getEditorInput();
			{	final IFile file= ResourceUtil.getFile(input);
				if (file != null) {
					final IContentType contentType= IDE.guessContentType(file);
					return (contentType != null) ? contentType.getId() : null;
				}
			}
			{	String fileName= null;
				if (input instanceof IPathEditorInput) {
					fileName= ((IPathEditorInput) input).getPath().lastSegment();
				}
				else if (input instanceof IURIEditorInput) {
					fileName= URIUtil.lastSegment(((IURIEditorInput) input).getURI());
				}
				if (fileName != null) {
					final IContentType contentType= Platform.getContentTypeManager().findContentTypeFor(fileName);
					return (contentType != null) ? contentType.getId() : null;
				}
			}
		}
		return null;
	}
	
	
	public static @Nullable IFile getFile(final @Nullable IEditorInput input) {
		if (input instanceof IFileEditorInput) {
			return ((IFileEditorInput)input).getFile();
		}
		return null;
	}
	
	public static void openEditor(final IWorkbenchPage page, final IFile file,
			final @Nullable IRegion initialSelection) {
		final Display display= page.getWorkbenchWindow().getShell().getDisplay();
		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				IMarker marker= null;
				if (initialSelection != null) {
					try {
						marker= file.createMarker("org.eclipse.statet.ltk.resourceMarkers.InitialSelection", Map.of( //$NON-NLS-1$
								IMarker.CHAR_START, initialSelection.getOffset(),
								IMarker.CHAR_END, initialSelection.getOffset() + initialSelection.getLength() ));
					}
					catch (final CoreException e) {}
				}
				try {
					if (marker != null) {
						IDE.openEditor(page, marker, true);
					}
					else {
						IDE.openEditor(page, file, true);
					}
				}
				catch (final PartInitException e) {
					StatusManager.getManager().handle(new Status(IStatus.ERROR, SharedUIResources.BUNDLE_ID, -1,
							NLS.bind("Could not open editor for ''{0}''", file.getName()), e));
				}
				finally {
					if (marker != null) {
						try {
							marker.delete();
						}
						catch (final CoreException e) {}
					}
				}
			}
		});
	}
	
	public static void indicateStatus(final IStatus status, final ExecutionEvent executionEvent) {
		if (status.isOK()) {
			return;
		}
		if (status.getMessage() != null && executionEvent != null) {
			final IWorkbenchPart workbenchPart= HandlerUtil.getActivePart(executionEvent);
			if (workbenchPart != null) {
				final IEditorStatusLine statusLine= workbenchPart.getAdapter(IEditorStatusLine.class);
				if (statusLine != null) {
					statusLine.setMessage(status.getSeverity() == IStatus.ERROR, status.getMessage(), null);
				}
			}
		}
		if (status.getSeverity() == IStatus.ERROR) {
			Display.getCurrent().beep();
		}
	}
	
	
	private LTKWorkbenchUIUtil() {}
	
}
