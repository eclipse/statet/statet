/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates.config;

import java.io.IOException;
import java.util.List;

import org.eclipse.jface.text.templates.Template;
import org.eclipse.text.templates.TemplatePersistenceData;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface ITemplateContribution {
	
	
	List<? extends TemplatePersistenceData> getTemplates(String categoryId);
	
	@Nullable Template findTemplate(String contextTypeId, String name);
	
	void add(String categoryId, TemplatePersistenceData data);
	
	void delete(TemplatePersistenceData data);
	
	boolean hasDeleted();
	
	void restoreDeleted();
	
	void restoreDefaults();
	
	void revertEdits() throws IOException;
	
	void saveEdits() throws IOException;
	
}
