/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Interface to listen to {@link LTKInputData} state changes providing selection and model
 * information.
 * 
 * @see {@link PostSelectionWithElementInfoController}
 */
@NonNullByDefault
public interface SelectionWithElementInfoListener {
	
	
	void inputChanged();
	
	void stateChanged(LTKInputData state);
	
	
}
