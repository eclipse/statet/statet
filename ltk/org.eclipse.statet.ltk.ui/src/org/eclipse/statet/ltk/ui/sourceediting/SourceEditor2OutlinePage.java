/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.workbench.ContextHandlers;

import org.eclipse.statet.internal.ltk.ui.EditingMessages;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.ui.LtkActions;
import org.eclipse.statet.ltk.ui.refactoring.AbstractElementsHandler;
import org.eclipse.statet.ltk.ui.refactoring.CopyElementsHandler;
import org.eclipse.statet.ltk.ui.refactoring.CopyNamesHandler;
import org.eclipse.statet.ltk.ui.refactoring.CutElementsHandler;
import org.eclipse.statet.ltk.ui.refactoring.DeleteElementsHandler;
import org.eclipse.statet.ltk.ui.refactoring.PasteElementsHandler;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;


@NonNullByDefault
public abstract class SourceEditor2OutlinePage extends SourceEditor1OutlinePage {
	
	
	protected class SelectCodeRangeAction extends Action {
		
		
		public SelectCodeRangeAction(final CommonRefactoringFactory refactoring) {
			super();
			setText(EditingMessages.SelectSourceCode_label);
			
			setEnabled(!getSelection().isEmpty());
		}
		
		@Override
		public void run() {
			final @NonNull SourceStructElement<?, ?>[] elementArray= LTKSelectionUtils
					.getSelectedSourceStructElements(getSelection());
			if (elementArray != null) {
				RefactoringAdapter adapter= SourceEditor2OutlinePage.this.refactoring.createAdapter(elementArray);
				if (adapter == null) {
					adapter= SourceEditor2OutlinePage.this.refactoring.createAdapter(null);
				}
				final var elements= ImCollections.newList(elementArray,
						adapter.getModelElementComparator() );
				final IRegion range= adapter.getContinuousSourceRange(elements);
				if (range != null) {
					selectInEditor(new TextSelection(range.getOffset(), range.getLength()));
				}
			}
		}
		
	}
	
	
	private final CommonRefactoringFactory refactoring;
	
	
	public SourceEditor2OutlinePage(final SourceEditor1 editor, final String modelTypeId,
			final CommonRefactoringFactory refactoring, final String contextMenuId) {
		super(editor, modelTypeId, contextMenuId);
		this.refactoring= refactoring;
	}
	
	
	protected CommonRefactoringFactory getRefactoringFactory() {
		return this.refactoring;
	}
	
	@Override
	protected void initActions(final IServiceLocator serviceLocator,
			final ContextHandlers handlers) {
		super.initActions(serviceLocator, handlers);
		
		{	final AbstractElementsHandler handler= new CutElementsHandler(this.refactoring);
			handlers.addActivate(IWorkbenchCommandConstants.EDIT_CUT, handler);
			registerHandlerToUpdate(handler);
		}
		{	final AbstractElementsHandler handler= new CopyElementsHandler(this.refactoring);
			handlers.addActivate(IWorkbenchCommandConstants.EDIT_COPY, handler);
			registerHandlerToUpdate(handler);
		}
		{	final AbstractElementsHandler handler= new CopyNamesHandler(this.refactoring);
			handlers.addActivate(LtkActions.COPY_ELEMENT_NAME_COMMAND_ID, handler);
			registerHandlerToUpdate(handler);
		}
		{	final AbstractElementsHandler handler= new PasteElementsHandler(getSourceEditor(), this.refactoring);
			handlers.addActivate(IWorkbenchCommandConstants.EDIT_PASTE, handler);
		}
		{	final AbstractElementsHandler handler= new DeleteElementsHandler(this.refactoring);
			handlers.addActivate(IWorkbenchCommandConstants.EDIT_DELETE, handler);
		}
	}
	
	@Override
	protected void contextMenuAboutToShow(final IMenuManager menuManager) {
		final var site= getSite();
		
		menuManager.add(new SelectCodeRangeAction(this.refactoring));
		
		menuManager.add(new Separator(SharedUIResources.EDIT_COPYPASTE_MENU_ID));
		menuManager.add(new CommandContributionItem(
				new CommandContributionItemParameter(site,
						null, IWorkbenchCommandConstants.EDIT_CUT,
						CommandContributionItem.STYLE_PUSH )));
		menuManager.add(new CommandContributionItem(
				new CommandContributionItemParameter(site,
						null, IWorkbenchCommandConstants.EDIT_COPY,
						CommandContributionItem.STYLE_PUSH )));
		menuManager.add(new CommandContributionItem(
				new CommandContributionItemParameter(site,
						null, LtkActions.COPY_ELEMENT_NAME_COMMAND_ID,
						CommandContributionItem.STYLE_PUSH )));
		menuManager.add(new CommandContributionItem(
				new CommandContributionItemParameter(site,
						null, IWorkbenchCommandConstants.EDIT_PASTE,
						CommandContributionItem.STYLE_PUSH )));
		
		super.contextMenuAboutToShow(menuManager);
	}
	
}
