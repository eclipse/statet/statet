/*=============================================================================#
 # Copyright (c) 2007, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.util;

import java.util.Iterator;

import org.eclipse.jface.util.DelegatingDragAdapter;
import org.eclipse.jface.util.TransferDragSourceListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;

import org.eclipse.statet.ecommons.ui.util.SelectionTransferDragAdapter;


public class ViewerDragSupport {
	
	
	public static class TextDragSourceListener implements TransferDragSourceListener {
		
		
		private final StructuredViewer viewer;
		
		private String text;
		
		
		public TextDragSourceListener(final StructuredViewer viewer) {
			this.viewer= viewer;
		}
		
		
		@Override
		public Transfer getTransfer() {
			return TextTransfer.getInstance();
		}
		
		@Override
		public void dragStart(final DragSourceEvent event) {
			final IStructuredSelection selection= (IStructuredSelection) this.viewer.getSelection();
			final ILabelProvider labelProvider= (ILabelProvider) this.viewer.getLabelProvider();
			if (selection.isEmpty()) {
				event.doit= false;
				return;
			}
			final Iterator iterator= selection.iterator();
//			final String sep= System.getProperty("line.separator"); //$NON-NLS-1$
			final String sep= ", "; //$NON-NLS-1$
			final StringBuilder sb= new StringBuilder();
			while (iterator.hasNext()) {
				sb.append(labelProvider.getText(iterator.next()));
				sb.append(sep);
			}
			this.text= sb.substring(0, sb.length() - sep.length());
		}
		
		@Override
		public void dragSetData(final DragSourceEvent event) {
			event.data= this.text;
		}
		
		@Override
		public void dragFinished(final DragSourceEvent event) {
			this.text= ""; //$NON-NLS-1$
		}
		
	}
	
	
	private final StructuredViewer viewer;
	private final DelegatingDragAdapter delegatingAdapter;
	
	private boolean initialized;
	
	
	public ViewerDragSupport(final StructuredViewer viewer) {
		this.viewer= viewer;
		
		this.delegatingAdapter= new DelegatingDragAdapter();
		this.delegatingAdapter.addDragSourceListener(new SelectionTransferDragAdapter(this.viewer));
//		this.delegatingAdapter.addDragTargetListener(new FileTransferDropAdapter(this.viewer));
		
		this.initialized= false;
	}
	
	
	public void addDragSourceListener(final TransferDragSourceListener listener) {
		assert (!this.initialized);
		
		this.delegatingAdapter.addDragSourceListener(listener);
	}
	
	public void init() {
		assert (!this.initialized);
		
		this.viewer.addDragSupport((DND.DROP_DEFAULT | DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK),
				this.delegatingAdapter.getTransfers(), this.delegatingAdapter );
		
		this.initialized= true;
	}
	
}
