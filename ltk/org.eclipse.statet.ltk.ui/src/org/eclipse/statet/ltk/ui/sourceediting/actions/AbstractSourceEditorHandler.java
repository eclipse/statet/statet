/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public abstract class AbstractSourceEditorHandler extends AbstractHandler {
	
	
	protected static final Object ACTION_NOT_AVAILABLE= new Object();
	protected static final Object DOCUMENT_NOT_EDITABLE= new Object();
	
	
	private final @Nullable SourceEditor editor;
	
	
	public AbstractSourceEditorHandler() {
		this.editor= null;
	}
	
	public AbstractSourceEditorHandler(final SourceEditor editor) {
		this.editor= nonNullAssert(editor);
	}
	
	
	protected @Nullable SourceEditor getSourceEditor(final @Nullable Object context) {
		SourceEditor editor= this.editor;
		if (editor != null) {
			return editor;
		}
		final IWorkbenchPart part= WorkbenchUIUtils.getActivePart(context);
		if (part != null) {
			if (part instanceof SourceEditor) {
				return (SourceEditor)part;
			}
			editor= part.getAdapter(SourceEditor.class);
			if (editor != null) {
				return editor;
			}
		}
		return null;
	}
	
	protected boolean isSupported(final SourceEditor editor) {
		final SourceViewer viewer= editor.getViewer();
		return (UIAccess.isOkToUse(viewer)
				&& viewer.getDocument() != null );
	}
	
	protected boolean isEditAction() {
		return false;
	}
	
	protected boolean computeEnabled(final SourceEditor editor, final @Nullable Object evaluationContext) {
		return (isSupported(editor)
				&& (!isEditAction() || editor.isEditable(false)) );
	}
	
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		final SourceEditor editor= getSourceEditor(evaluationContext);
		setBaseEnabled(editor != null && computeEnabled(editor, evaluationContext));
	}
	
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final SourceEditor editor= getSourceEditor(event.getApplicationContext());
		if (!(editor != null && isSupported(editor))) {
			return null;
		}
		if (isEditAction() && !editor.isEditable(false)) {
			onDocumentNotEditable();
			return null;
		}
		
		final Object result= execute(editor, event);
		if (result == ACTION_NOT_AVAILABLE) {
			onActionNotAvailable();
			return null;
		}
		if (result == DOCUMENT_NOT_EDITABLE) {
			onDocumentNotEditable();
			return null;
		}
		return result;
	}
	
	protected abstract @Nullable Object execute(final SourceEditor editor,
			final ExecutionEvent event) throws ExecutionException;
	
	
	protected void onActionNotAvailable() {
		Display.getCurrent().beep();
	}
	
	protected void onDocumentNotEditable() {
		onActionNotAvailable();
	}
	
}
