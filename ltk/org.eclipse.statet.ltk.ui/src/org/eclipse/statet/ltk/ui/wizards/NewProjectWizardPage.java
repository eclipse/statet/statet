/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.wizards;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.resources.IProject;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;


public class NewProjectWizardPage extends WizardNewProjectCreationPage {
	
	
	private IObservableValue<IProject> projectValue;
	
	
	public NewProjectWizardPage(final String pageName) {
		super(pageName);
	}
	
	
	public IObservableValue<IProject> getProjectValue() {
		return this.projectValue;
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		this.projectValue= new WritableValue<>(null, IProject.class);
		
		super.createControl(parent);
	}
	
	@Override
	protected boolean validatePage() {
		final boolean isValid= super.validatePage();
		if (isValid) {
			this.projectValue.setValue(getProjectHandle());
		}
		return isValid;
	}
	
}
