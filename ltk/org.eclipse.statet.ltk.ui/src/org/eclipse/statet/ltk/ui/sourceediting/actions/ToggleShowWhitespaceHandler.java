/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IPainter;
import org.eclipse.jface.text.ITextViewerExtension2;
import org.eclipse.jface.text.WhitespaceCharacterPainter;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.ui.actions.ToggleBooleanPreferenceHandler;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * ITextEditorActionDefinitionIds#SHOW_WHITESPACE_CHARACTERS
 */
public class ToggleShowWhitespaceHandler extends ToggleBooleanPreferenceHandler {
	
	
	private final SourceViewer viewer;
	
	/** For customization of shown whitespaces, not for toggle */
	private final IPreferenceStore store;
	
	/** The painter. */
	private IPainter painter;
	
	
	public ToggleShowWhitespaceHandler(final Preference<Boolean> pref,
			final SourceViewer viewer, final IPreferenceStore store) {
		super(pref, ITextEditorActionDefinitionIds.SHOW_WHITESPACE_CHARACTERS);
		this.viewer= viewer;
		
		this.store= store;
		
		handleToggled(isPrefEnabled());
	}
	
	public ToggleShowWhitespaceHandler(final Preference<Boolean> pref, final SourceViewer viewer) {
		this(pref, viewer, EditorsUI.getPreferenceStore());
	}
	
	
	protected SourceViewer getViewer() {
		return this.viewer;
	}
	
	
	private void installPainter() {
		if (this.painter != null) {
			return;
		}
		
		final SourceViewer viewer= getViewer();
		if (UIAccess.isOkToUse(viewer)) {
			if (this.store != null) {
				this.painter= new WhitespaceCharacterPainter(viewer,
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_LEADING_SPACES),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_ENCLOSED_SPACES),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_TRAILING_SPACES),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_LEADING_IDEOGRAPHIC_SPACES),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_ENCLOSED_IDEOGRAPHIC_SPACES),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_TRAILING_IDEOGRAPHIC_SPACES),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_LEADING_TABS),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_ENCLOSED_TABS),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_TRAILING_TABS),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_CARRIAGE_RETURN),
						this.store.getBoolean(AbstractTextEditor.PREFERENCE_SHOW_LINE_FEED),
						this.store.getInt(AbstractTextEditor.PREFERENCE_WHITESPACE_CHARACTER_ALPHA_VALUE) );
			}
			else {
				this.painter= new WhitespaceCharacterPainter(viewer);
			}
			
			((ITextViewerExtension2) viewer).addPainter(this.painter);
		}
	}
	
	private void uninstallPainter() {
		if (this.painter == null) {
			return;
		}
		
		final SourceViewer viewer= getViewer();
		if (UIAccess.isOkToUse(viewer)) {
			((ITextViewerExtension2) viewer).removePainter(this.painter);
		}
		
		this.painter.deactivate(true);
		this.painter= null;
	}
	
	@Override
	protected void handleToggled(final boolean enabled) {
		if (enabled) {
			installPainter();
		}
		else {
			uninstallPainter();
		}
		
		super.handleToggled(enabled);
	}
	
	
	@Override
	public void dispose() {
		super.dispose();
	}
	
}
