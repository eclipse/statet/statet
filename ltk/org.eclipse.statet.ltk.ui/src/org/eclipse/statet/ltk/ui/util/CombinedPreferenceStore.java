/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.texteditor.ChainedPreferenceStore;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.ui.ScopedPreferenceStore;


/**
 * Util to create combination of preference stores.
 */
@NonNullByDefault
public class CombinedPreferenceStore {
	
	public static IPreferenceStore createStore(
			final @NonNull IPreferenceStore[] preferenceStores,
			final PreferenceAccess corePrefs, final @NonNull String[] coreQualifier) {
		
		ImList<IScopeContext> contexts= corePrefs.getPreferenceContexts();
		// default scope must not be included (will be automatically added)
		if (contexts.size() > 0 && contexts.getLast() instanceof DefaultScope) {
			contexts= contexts.subList(0, contexts.size() - 1);
		}
		final IScopeContext mainScope= (!contexts.isEmpty()) ? contexts.getFirst() : InstanceScope.INSTANCE;
		
		if (preferenceStores.length == 0 && contexts.size() <= 1 && coreQualifier.length == 1) {
			return new ScopedPreferenceStore(mainScope, coreQualifier[0]);
		}
		
		final List<IPreferenceStore> stores= new ArrayList<>();
		for (final String qualifier : coreQualifier) {
			final ScopedPreferenceStore store= new ScopedPreferenceStore(mainScope, qualifier);
			store.setSearchContexts(contexts);
			stores.add(store);
		}
		stores.addAll(ImCollections.newList(preferenceStores));
		return new ChainedPreferenceStore(
				stores.toArray(new @NonNull IPreferenceStore[stores.size()]) );
	}
	
	public static IPreferenceStore createStore(final PreferenceAccess corePrefs, final String coreQualifier) {
		return createStore(new @NonNull IPreferenceStore[0],
				corePrefs, new @NonNull String[] { coreQualifier });
	}
	
	public static IPreferenceStore createStore(final @NonNull IPreferenceStore... preferenceStores) {
		return new ChainedPreferenceStore(preferenceStores);
	}
	
	
	private CombinedPreferenceStore() {}
	
}
