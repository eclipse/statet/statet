/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.compare;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.IEditableContent;
import org.eclipse.compare.IStreamContentAccessor;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.ecommons.preferences.ui.SettingsUpdater;
import org.eclipse.statet.ecommons.text.ui.TextViewerJFaceUpdater;

import org.eclipse.statet.ltk.ui.sourceediting.SnippetEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.ViewerSourceEditorAdapter;


/**
 * Content viewer for source code using a {@link SourceEditorViewerConfigurator}
 * to setup the text viewer.
 */
public class CompareTextViewer extends Viewer {
	
	
	private final SourceViewer sourceViewer;
	private final SourceEditorViewerConfigurator configurator;
	
	private Object input;
	
	
	public CompareTextViewer(final Composite parent, final CompareConfiguration compareConfig,
			final SourceEditorViewerConfigurator viewerConfig) {
		this.configurator= viewerConfig;
		this.sourceViewer= new SourceViewer(parent, null, SnippetEditor.DEFAULT_MULTI_LINE_STYLE);
		this.sourceViewer.setEditable(false);
	}
	
	
	protected void initSourceViewer() {
		this.configurator.setTarget(new ViewerSourceEditorAdapter(this.sourceViewer, this.configurator) {
			@Override
			public boolean isEditable(final boolean validate) {
				return (CompareTextViewer.this.input instanceof IEditableContent && ((IEditableContent) CompareTextViewer.this.input).isEditable());
			}
		});
		new TextViewerJFaceUpdater(this.sourceViewer,
				this.configurator.getSourceViewerConfiguration().getPreferences() );
		new SettingsUpdater(this.configurator, this.sourceViewer.getControl());
		
		this.sourceViewer.activatePlugins();
	}
	
	@Override
	public Control getControl() {
		return this.sourceViewer.getControl();
	}
	
	@Override
	public void setInput(final Object input) {
		this.input= input;
		if (input instanceof IStreamContentAccessor) {
			final Document document= new Document(
					CompareUtilities.readString((IStreamContentAccessor)input));
			this.configurator.getDocumentSetupParticipant().setup(document);
			this.sourceViewer.setDocument(document);
		}
	}
	
	@Override
	public Object getInput() {
		return this.input;
	}
	
	@Override
	public void setSelection(final ISelection selection, final boolean reveal) {
		this.sourceViewer.setSelection(selection, reveal);
	}
	
	@Override
	public ISelection getSelection() {
		return this.sourceViewer.getSelection();
	}
	
	@Override
	public void refresh() {
	}
	
}
