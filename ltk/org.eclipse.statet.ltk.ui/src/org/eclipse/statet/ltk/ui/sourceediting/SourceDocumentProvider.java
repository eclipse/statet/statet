/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.editors.text.ForwardingDocumentProvider;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.IDocumentProvider;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.DocumentModelProvider;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.SourceUnitManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;


@NonNullByDefault
public class SourceDocumentProvider<T extends SourceUnit> extends TextFileDocumentProvider
		implements DocumentModelProvider, SourceUnitIssueSupport {
	
	
	public static class SourceFileInfo extends FileInfo {
		
		private @Nullable SourceUnit workingCopy;
		
	}
	
	
	private final String modelTypeId;
	private final @Nullable PartitionerDocumentSetupParticipant documentSetupParticipant;
	
	private final IssueTypeSet issueTypeSet;
	
	
	public SourceDocumentProvider(final String modelTypeId,
			final @Nullable PartitionerDocumentSetupParticipant documentSetupParticipant,
			final IssueTypeSet issueTypeSet) {
		this.modelTypeId= nonNullAssert(modelTypeId);
		this.documentSetupParticipant= documentSetupParticipant;
		this.issueTypeSet= issueTypeSet;
		
		IDocumentProvider parent= new TextFileDocumentProvider();
		if (documentSetupParticipant != null) {
			parent= new ForwardingDocumentProvider(documentSetupParticipant.getPartitioningId(),
					documentSetupParticipant, parent );
		}
		setParentDocumentProvider(parent);
	}
	
	
	@Override
	protected FileInfo createEmptyFileInfo() {
		return new SourceFileInfo();
	}
	
	@Override
	public void disconnect(final Object element) {
		final FileInfo info= getFileInfo(element);
		if (info instanceof final SourceFileInfo rinfo) {
			if (rinfo.fCount == 1 && rinfo.workingCopy != null) {
				final SubMonitor m= SubMonitor.convert(getProgressMonitor(), 1);
				try {
					rinfo.workingCopy.disconnect(m.newChild(1));
				}
				finally {
					rinfo.workingCopy= null;
					m.done();
				}
			}
		}
		super.disconnect(element);
	}
	
	@Override
	protected @Nullable FileInfo createFileInfo(final Object element) throws CoreException {
		final SourceFileInfo sourceInfo;
		{	final FileInfo info= super.createFileInfo(element);
			if (!(info instanceof SourceFileInfo)) {
				return null;
			}
			sourceInfo= (SourceFileInfo)info;
		}
		
		{	final IDocument document= getDocument(element);
			if (document instanceof AbstractDocument) {
				setupDocument((AbstractDocument)document);
			}
		}
		
		final SourceUnitManager suManager= LtkModels.getSourceUnitManager();
		final SubMonitor m= SubMonitor.convert(getProgressMonitor());
		SourceUnit pUnit= null;
		try {
			final IFile ifile= (element instanceof IAdaptable) ?
					((IAdaptable)element).getAdapter(IFile.class) : null;
			if (ifile != null) {
				m.setWorkRemaining(2);
				pUnit= suManager.getSourceUnit(this.modelTypeId, Ltk.PERSISTENCE_CONTEXT,
						ifile, m.newChild(1) );
				sourceInfo.workingCopy= suManager.getSourceUnit(Ltk.EDITOR_CONTEXT, pUnit, false, m.newChild(1));
			}
			else if (element instanceof IURIEditorInput) {
				m.setWorkRemaining(1);
				final IFileStore store;
				try {
					store= EFS.getStore(((IURIEditorInput) element).getURI());
				}
				catch (final CoreException e) {
					return sourceInfo;
				}
				sourceInfo.workingCopy= suManager.getSourceUnit(this.modelTypeId, Ltk.EDITOR_CONTEXT,
						store, m.newChild(1) );
			}
		}
		catch (final StatusException e) {
			throw EStatusUtils.convert(e);
		}
		finally {
			if (pUnit != null) {
				pUnit.disconnect(m);
			}
			m.done();
		}
		
		return sourceInfo;
	}
	
	protected void setupDocument(final AbstractDocument document) {
		if (this.documentSetupParticipant != null) {
			this.documentSetupParticipant.setup(document);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public @Nullable T getWorkingCopy(final Object element) {
		final FileInfo fileInfo= getFileInfo(element);
		if (fileInfo instanceof SourceFileInfo) {
			return (T)((SourceFileInfo)fileInfo).workingCopy;
		}
		return null;
	}
	
	@Override
	public @Nullable IAnnotationModel getAnnotationModel(@Nullable Object element) {
		if (element instanceof WorkspaceSourceUnit) {
			element= new FileEditorInput((IFile)((WorkspaceSourceUnit)element).getResource());
		}
		return super.getAnnotationModel(element);
	}
	
	@Override
	protected @Nullable IAnnotationModel createAnnotationModel(final IFile file) {
		return new SourceAnnotationModel(file, getIssueTypeSet());
	}
	
	
	@Override
	public IssueTypeSet getIssueTypeSet() {
		return this.issueTypeSet;
	}
	
	@Override
	public void clearIssues(final SourceUnit sourceUnit) throws CoreException {
	}
	
	@Override
	public @Nullable IssueRequestor createIssueRequestor(final SourceUnit sourceUnit) {
		final IAnnotationModel annotationModel= getAnnotationModel(sourceUnit);
		if (annotationModel instanceof SourceAnnotationModel) {
			return ((SourceAnnotationModel)annotationModel).createIssueRequestor();
		}
		return null;
	}
	
}
