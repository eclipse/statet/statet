/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates.config;

import org.eclipse.core.resources.IProject;
import org.eclipse.text.templates.ContextTypeRegistry;
import org.eclipse.text.templates.TemplatePersistenceData;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;


public interface ITemplateCategoryConfiguration {
	
	
	ITemplateContribution getTemplates();
	
	Preference<String> getDefaultPref();
	
	
	ContextTypeRegistry getContextTypeRegistry();
	
	String getDefaultContextTypeId();
	
	
	String getViewerConfigId(TemplatePersistenceData data);
	
	SourceEditorViewerConfigurator createViewerConfiguator(String viewerConfigId, TemplatePersistenceData data,
			TemplateVariableProcessor templateProcessor, IProject project);
	
}
