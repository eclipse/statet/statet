/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.AnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.texteditor.AbstractDocumentProvider;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.ltk.model.core.DocumentModelProvider;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.input.SourceFragmentEditorInput;


@NonNullByDefault
public class FragmentDocumentProvider extends AbstractDocumentProvider
		implements DocumentModelProvider {
	
	
	public class SourceElementInfo extends ElementInfo {
		
		private @Nullable SourceUnit workingCopy;
		
		public SourceElementInfo(final IDocument document, final IAnnotationModel model) {
			super(document, model);
		}
		
	}
	
	
	private final String modelTypeId;
	
	private final @Nullable IDocumentSetupParticipant documentSetupParticipant;
	
	
	public FragmentDocumentProvider(final String modelTypeId,
			final @Nullable PartitionerDocumentSetupParticipant documentSetupParticipant) {
		this.modelTypeId= nonNullAssert(modelTypeId);
		this.documentSetupParticipant= documentSetupParticipant;
	}
	
	
	@Override
	protected @Nullable ElementInfo createElementInfo(final Object element) throws CoreException {
		SourceUnit sourceUnit= null;
		AbstractDocument document= null;
		if (element instanceof final SourceFragmentEditorInput fragmentInput) {
			final SubMonitor m= SubMonitor.convert(getProgressMonitor(), 2);
			try {
				sourceUnit= LtkModels.getSourceUnitManager().getSourceUnit(this.modelTypeId, Ltk.EDITOR_CONTEXT,
						fragmentInput.getSourceFragment(), m.newChild(1));
				document= sourceUnit.getDocument(m.newChild(1));
			}
			catch (final Exception e) {}
			finally {
				m.done();
			}
		}
		if (document == null) {
			document= createDocument((sourceUnit != null) ? sourceUnit : element);
		}
		if (document != null) {
			setupDocument(document);
			final SourceElementInfo info= new SourceElementInfo(document, createAnnotationModel(element));
			info.workingCopy= sourceUnit;
			return info;
		}
		return null;
	}
	
	@Override
	protected @Nullable AbstractDocument createDocument(final Object element) throws CoreException {
		if (element instanceof SourceFragmentEditorInput) {
			final SourceFragment fragment= ((SourceFragmentEditorInput)element).getSourceFragment();
			return fragment.getDocument();
		}
		return null;
	}
	
	protected void setupDocument(final AbstractDocument document) {
		final var documentSetupParticipant= this.documentSetupParticipant;
		if (documentSetupParticipant != null) {
			documentSetupParticipant.setup(document);
		}
	}
	
	@Override
	protected IAnnotationModel createAnnotationModel(final Object element) throws CoreException {
		return new AnnotationModel();
	}
	
	@Override
	protected @Nullable IRunnableContext getOperationRunner(final @Nullable IProgressMonitor monitor) {
		return null;
	}
	
	@Override
	protected void doSaveDocument(final @Nullable IProgressMonitor monitor, final Object element,
			final IDocument document, final boolean overwrite) throws CoreException {
	}
	
	@Override
	protected void disposeElementInfo(final Object element, final ElementInfo elementInfo) {
		final SourceElementInfo info= (SourceElementInfo)elementInfo;
		if (info.workingCopy != null) {
			final SubMonitor m= SubMonitor.convert(getProgressMonitor(), 1);
			try {
				info.workingCopy.disconnect(m.newChild(1));
			}
			finally {
				info.workingCopy= null;
				m.done();
			}
		}
		super.disposeElementInfo(element, elementInfo);
	}
	
	@Override
	public @Nullable SourceUnit getWorkingCopy(final Object element) {
		final SourceElementInfo info= (SourceElementInfo)getElementInfo(element);
		if (info != null) {
			return info.workingCopy;
		}
		return null;
	}
	
}
