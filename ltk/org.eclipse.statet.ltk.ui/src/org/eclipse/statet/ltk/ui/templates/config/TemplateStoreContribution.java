/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates.config;

import java.io.IOException;
import java.util.List;

import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.text.templates.TemplatePersistenceData;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class TemplateStoreContribution implements ITemplateContribution {
	
	
	private final TemplateStore templateStore;
	
	
	public TemplateStoreContribution(final TemplateStore store) {
		this.templateStore= store;
	}
	
	
	@Override
	public List<TemplatePersistenceData> getTemplates(final String categoryId) {
		return ImCollections.newList(this.templateStore.getTemplateData(false));
	}
	
	@Override
	public @Nullable Template findTemplate(final String contextTypeId, final String name) {
		return this.templateStore.findTemplate(name, contextTypeId);
	}
	
	@Override
	public void add(final String categoryId, final TemplatePersistenceData data) {
		this.templateStore.add(data);
	}
	
	@Override
	public void delete(final TemplatePersistenceData data) {
		this.templateStore.delete(data);
	}
	
	@Override
	public void revertEdits() throws IOException {
		this.templateStore.load();
	}
	
	@Override
	public void saveEdits() throws IOException {
		this.templateStore.save();
	}
	
	@Override
	public boolean hasDeleted() {
		return (this.templateStore.getTemplateData(true).length != this.templateStore.getTemplateData(false).length);
	}
	
	@Override
	public void restoreDeleted() {
		this.templateStore.restoreDeleted();
	}
	
	@Override
	public void restoreDefaults() {
		this.templateStore.restoreDefaults(false);
	}
	
	
	@Override
	public int hashCode() {
		return this.templateStore.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj != null && getClass() == obj.getClass()) {
			final TemplateStoreContribution other= (TemplateStoreContribution) obj;
			return (this.templateStore == other.templateStore);
		}
		return false;
	}
	
}
