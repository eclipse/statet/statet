/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementDelta;


/**
 * Interface to listen to changes of object input of the type {@link LtkModelElement}.
 * 
 * @see ModelElementInputProvider
 */
public interface ModelElementInputListener<TModelElement extends LtkModelElement<?>> {
	
	
	/**
	 * The element of the provider has changed.
	 * 
	 * Directly called while changing the input.
	 * For longer tasks, wait for {@link #elementInitialInfo(LtkModelElement)}.
	 */
	void elementChanged(final @Nullable TModelElement element);
	
	/**
	 * First detail info for the element.
	 */
	void elementInitialInfo(final @Nullable TModelElement element);
	
	/**
	 * Detail info changed.
	 */
	void elementUpdatedInfo(final TModelElement element, final LtkModelElementDelta delta);
	
}
