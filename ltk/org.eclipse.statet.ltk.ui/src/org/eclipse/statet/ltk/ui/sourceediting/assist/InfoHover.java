/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.ui.util.InformationDispatchHandler;


/**
 * Interface for information hover providers.
 * 
 * Intend to be implemented by client and registered to the extension point
 * <code>org.eclipse.statet.ltk.advancedInfoHover</code>.
 */
@NonNullByDefault
public interface InfoHover {
	
	
	int MODE_TOOLTIP= InformationDispatchHandler.MODE_TOOLTIP;
	int MODE_PROPOSAL_INFO= InformationDispatchHandler.MODE_PROPOSAL_INFO;
	
	int MODE_FOCUS= InformationDispatchHandler.MODE_FOCUS;
	
	
	/**
	 * @param m optional progress monitor
	 * @see ITextHoverExtension2#getHoverInfo2(org.eclipse.jface.text.ITextViewer, org.eclipse.jface.text.IRegion)
	 */
	@Nullable Object getHoverInfo(AssistInvocationContext context,
			ProgressMonitor m) throws StatusException;
	
	/**
	 * @see ITextHoverExtension#getHoverControlCreator()
	 */
	@Nullable IInformationControlCreator getHoverControlCreator();
	
}
