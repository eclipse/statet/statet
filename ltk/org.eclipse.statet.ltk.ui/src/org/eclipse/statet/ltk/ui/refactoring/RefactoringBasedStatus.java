/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.refactoring;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;


/**
 * {@link IStatus} wrapping a {@link RefactoringStatus}.
 */
public class RefactoringBasedStatus implements IStatus {
	
	
	private final RefactoringStatus status;
	
	
	public RefactoringBasedStatus(final RefactoringStatus status) {
		this.status= status;
	}
	
	
	@Override
	public String getPlugin() {
		return BUNDLE_ID;
	}
	
	@Override
	public int getSeverity() {
		return convertSeverity(this.status.getSeverity());
	}
	
	@Override
	public boolean isOK() {
		return (this.status.getSeverity() == RefactoringStatus.OK);
	}
	
	@Override
	public String getMessage() {
		return this.status.getMessageMatchingSeverity(this.status.getSeverity());
	}
	
	@Override
	public int getCode() {
		return 0;
	}
	
	@Override
	public Throwable getException() {
		return null;
	}
	
	@Override
	public boolean isMultiStatus() {
		return false;
	}
	
	@Override
	public IStatus[] getChildren() {
		return null;
	}
	
	@Override
	public boolean matches(final int severityMask) {
		return (getSeverity() & severityMask) != 0;
	}
	
	
	public static int convertSeverity(final int severity) {
		switch (severity) {
		case RefactoringStatus.FATAL:
			return IStatus.ERROR;
		case RefactoringStatus.ERROR:
		case RefactoringStatus.WARNING:
			return IStatus.WARNING;
		case RefactoringStatus.INFO:
			return IStatus.INFO;
		default:
			return IStatus.OK;
		}
	}
	
}
