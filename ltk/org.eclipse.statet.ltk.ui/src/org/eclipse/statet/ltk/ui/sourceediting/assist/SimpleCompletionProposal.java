/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.util.TextUtils;
import org.eclipse.statet.ecommons.ui.SharedUIResources;


/**
 * Proposal completing a given replacement string.
 */
@NonNullByDefault
public class SimpleCompletionProposal<TContext extends AssistInvocationContext>
		extends SourceProposal<TContext> {
	
	
	/** The replacement string. */
	private final String replacementString;
	
	private @Nullable String description;
	
	
	public SimpleCompletionProposal(final ProposalParameters<? extends TContext> parameters,
			final String replacementString, final @Nullable String description) {
		super(parameters);
		
		this.replacementString= replacementString;
		this.description= description;
	}
	
	public SimpleCompletionProposal(final ProposalParameters<? extends TContext> parameters,
			final String replacementString) {
		super(parameters);
		
		this.replacementString= replacementString;
	}
	
	
	@Override
	protected final String getName() {
		return this.replacementString;
	}
	
	
	@Override
	public String getSortingString() {
		return this.replacementString;
	}
	
	
	@Override
	public String getDisplayString() {
		return getStyledDisplayString().getString();
	}
	
	@Override
	protected StyledString computeStyledText() {
		final StyledString styledText= new StyledString(getName());
		final var description= this.description;
		if (description != null) {
			styledText.append(QUALIFIER_SEPARATOR, StyledString.QUALIFIER_STYLER);
			styledText.append(description, StyledString.QUALIFIER_STYLER);
		}
		return styledText;
	}
	
	@Override
	public Image getImage() {
		return SharedUIResources.getImages().get(SharedUIResources.PLACEHOLDER_IMAGE_ID);
	}
	
	
	@Override
	protected int computeReplacementLength(final int replacementOffset, final Point selection,
			final int caretOffset, final boolean overwrite)
			throws BadLocationException, BadPartitioningException {
		int end= Math.max(caretOffset, selection.x + selection.y);
		if (overwrite) {
			final var context= getInvocationContext();
			final IDocument document= context.getDocument();
			final ITypedRegion partition= TextUtils.getPartition(document, context.getEditor().getDocumentContentInfo(), end, true);
			if (replacementOffset == partition.getOffset() || end > partition.getOffset()) {
				end= TextUtils.findCommonWord2End(document, end, partition.getOffset() + partition.getLength());
			}
		}
		return (end - replacementOffset);
	}
	
}
