/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.refactoring;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.internal.ltk.ui.refactoring.Messages;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;


/**
 * Command handler copying selected elements to clipboard.
 */
public class CopyElementsHandler extends AbstractElementsHandler {
	
	
	public CopyElementsHandler(final CommonRefactoringFactory refactoring) {
		super(refactoring);
	}
	
	
	@Override
	public void setEnabled(final Object context) {
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(context);
		if (selection != null) {
			setBaseEnabled(!selection.isEmpty());
		}
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		if (selection == null || selection.isEmpty()) {
			return null;
		}
		final SourceStructElement[] sourceElements= LTKSelectionUtils.getSelectedSourceStructElements(selection);
		if (sourceElements != null) {
			final RefactoringAdapter adapter= this.refactoring.createAdapter(sourceElements);
			if (adapter == null) {
				return null;
			}
			try {
				final String code= adapter.getSourceCodeStringedTogether(sourceElements, null);
				copyToClipboard(event, code);
			}
			catch (final CoreException e) {
				StatusManager.getManager().handle(new Status(
						IStatus.ERROR, adapter.getPluginIdentifier(), -1,
						Messages.CopyElements_error_message,
						e ),
						StatusManager.LOG | StatusManager.SHOW);
			}
		}
		return null;
	}
	
}
