/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates.config;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.templates.AbstractTemplatesPage;
import org.eclipse.ui.texteditor.templates.ITemplatesPage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.preferences.ui.SettingsUpdater;
import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;
import org.eclipse.statet.ecommons.text.core.util.TextUtils;
import org.eclipse.statet.ecommons.text.ui.TextViewerJFaceUpdater;
import org.eclipse.statet.ecommons.ui.ISettingsChangedHandler;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.ltk.ui.LtkUIPlugin;
import org.eclipse.statet.internal.ltk.ui.PreviewSourceViewer;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.ViewerSourceEditorAdapter;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateProposal.TemplateProposalParameters;
import org.eclipse.statet.ltk.ui.templates.EnhTemplateStore;
import org.eclipse.statet.ltk.ui.templates.EnhTemplateStore.WorkingCopy;
import org.eclipse.statet.ltk.ui.templates.SourceEditorTemplateContext;


/**
 * Abstract {@link ITemplatesPage} for SourceEditor1/SourceViewerConfigurator
 */
@NonNullByDefault
public abstract class AbstractEditorTemplatesPage extends AbstractTemplatesPage {
	
	
	private final EnhTemplateStore templateStore;
	private final EnhTemplateStore.WorkingCopy templateStoreWorkingCopy;
	private @Nullable Runnable templateStoreListener;
	
	private final ITextEditor editorPart;
	private final SourceEditor sourceEditor;
	
	private SourceEditor previewEditor;
	private final TemplateVariableProcessor previewTemplateProcessor;
	private final TemplateVariableProcessor editTemplateProcessor;
	
	private @Nullable SourceEditorViewerConfigurator currentPreviewConfigurator;
	private @Nullable TextViewerJFaceUpdater currentPreviewUpdater;
	
	
	protected AbstractEditorTemplatesPage(final EnhTemplateStore templateStore,
			final ITextEditor editorPart, final SourceEditor sourceEditor) {
		super(editorPart, sourceEditor.getViewer());
		
		this.templateStore= templateStore;
		this.templateStoreWorkingCopy= this.templateStore.getWorkingCopy();
		
		this.editorPart= editorPart;
		this.sourceEditor= sourceEditor;
		
		this.previewTemplateProcessor= new TemplateVariableProcessor();
		this.editTemplateProcessor= new TemplateVariableProcessor();
	}
	
	
	protected SourceEditor getSourceEditor() {
		return this.sourceEditor;
	}
	
	protected AssistInvocationContext createContext(
			final SourceEditor editor, final TextRegion region)
			throws BadPartitioningException, BadLocationException {
		final String contentType= TextUtils.getContentType(editor.getViewer().getDocument(),
				editor.getDocumentContentInfo(), region.getStartOffset(), true );
		return new AssistInvocationContext(editor, region, contentType, ModelManager.NONE, null );
	}
	
	@Override
	public TemplateStore getTemplateStore() {
		return this.templateStoreWorkingCopy;
	}
	
	@Override
	protected ContextTypeRegistry getContextTypeRegistry() {
		return this.templateStore.getContextTypeRegistry();
	}
	
	protected abstract @Nullable TemplateCompletionComputer getComputer(
			final AssistInvocationContext context, final Template template);
	
	
	@Override
	protected boolean isValidTemplate(final IDocument document, final Template template,
			final int offset, final int length) {
		final SourceEditor sourceEditor= getSourceEditor();
		if (sourceEditor == null) {
			return false;
		}
		try {
			final String[] contextIds= getContextTypeIds(document, offset);
			for (int i= 0; i < contextIds.length; i++) {
				if (contextIds[i].equals(template.getContextTypeId())) {
					final TextRegion region= new BasicTextRegion(offset, offset + length);
					final AssistInvocationContext context= createContext(sourceEditor, region);
					final TemplateCompletionComputer computer= getComputer(context, template);
					if (computer == null) {
						return false;
					}
					final SourceEditorTemplateContext templateContext= computer.createTemplateContext(
							context, region, 0, true );
					return (templateContext != null
							&& templateContext.canEvaluate(template) );
				}
			}
		}
		catch (final Exception e) {}
		return false;
	}
	
	@Override
	protected void insertTemplate(final Template template, final IDocument document) {
		final SourceEditor sourceEditor= getSourceEditor();
		if (sourceEditor == null || !sourceEditor.isEditable(true)) {
			return;
		}
		try {
			final AssistInvocationContext context= createContext(sourceEditor, sourceEditor.getSelectedRegion());
			final TextRegion region= context;
			final TemplateCompletionComputer computer= getComputer(context, template);
			if (computer == null) {
				return;
			}
			final SourceEditorTemplateContext templateContext= computer.createTemplateContext(
					context, region, 0, true );
			if (templateContext == null) {
				return;
			}
			final TemplateProposal proposal= new TemplateProposal(
					new TemplateProposalParameters<>(context, region, templateContext, template) );
			this.editorPart.getSite().getPage().activate(this.editorPart);
			proposal.apply(sourceEditor.getViewer(), (char)0, 0, region.getStartOffset());
		}
		catch (final Exception e) {
			LtkUIPlugin.log(new Status(IStatus.ERROR, BUNDLE_ID,
					"An error occurred when applying editor template.", e));
		}
	}
	
	
	@Override
	public void createControl(final Composite ancestor) {
		Runnable listener= this.templateStoreListener;
		if (listener == null) {
			listener= new Runnable() {
				@Override
				public void run() {
					final WorkingCopy templateStore= AbstractEditorTemplatesPage.this.templateStoreWorkingCopy;
					if (templateStore != null) {
						templateStore.load();
					}
				}
			};
			this.templateStore.addListener(listener);
			this.templateStoreListener= listener;
		}
		listener.run();
		
		super.createControl(ancestor);
	}
	
	@Override
	public void dispose() {
		final Runnable templateStoreListener= this.templateStoreListener;
		if (templateStoreListener != null) {
			this.templateStore.removeListener(templateStoreListener);
			this.templateStoreListener= null;
		}
		disposePreviewUpdater();
		
		super.dispose();
	}
	
	@Override
	protected SourceViewer createPatternViewer(final Composite parent) {
		final SourceViewer viewer= new PreviewSourceViewer(parent);
		
		final IDocument document= new Document();
		viewer.setDocument(document);
		
		this.previewEditor= new ViewerSourceEditorAdapter(viewer, null);
		new SettingsUpdater(new ISettingsChangedHandler() {
			@Override
			public void handleSettingsChanged(final Set<String> groupIds, final Map<String, Object> options) {
				final SourceEditorViewerConfigurator configurator= AbstractEditorTemplatesPage.this.currentPreviewConfigurator;
				if (configurator != null) {
					configurator.handleSettingsChanged(groupIds, options);
				}
			}
		}, viewer.getControl());
		
		return viewer;
	}
	
	@Override
	protected void updatePatternViewer(final @Nullable Template template) {
		final SourceViewer patternViewer= getPatternViewer();
		if (patternViewer == null || !(UIAccess.isOkToUse(patternViewer.getControl())) ) {
			return;
		}
		
		if (template != null) {
			final SourceEditorViewerConfigurator configurator= getTemplatePreviewConfig(template, this.previewTemplateProcessor);
			final TemplateContextType type= getContextTypeRegistry().getContextType(template.getContextTypeId());
			this.previewTemplateProcessor.setContextType(type);
			
			final SourceEditorViewerConfigurator currentConfigurator= this.currentPreviewConfigurator;
			if (configurator != currentConfigurator) {
				disposePreviewUpdater();
				if (currentConfigurator != null) {
					this.currentPreviewConfigurator= null;
					currentConfigurator.unconfigureTarget();
				}
				
				configurator.setTarget(this.previewEditor);
				this.currentPreviewConfigurator= configurator;
				this.currentPreviewUpdater= new TextViewerJFaceUpdater(patternViewer,
						configurator.getSourceViewerConfiguration().getPreferences() );
				
				final AbstractDocument document= new Document();
				configurator.getDocumentSetupParticipant().setup(document);
				configureDocument(document, type, configurator);
				document.set(template.getPattern());
				patternViewer.setDocument(document);
			}
			else {
				final AbstractDocument document= (AbstractDocument) patternViewer.getDocument();
				document.set(""); //$NON-NLS-1$
				configureDocument(document, type, configurator);
				document.set(template.getPattern());
			}
			
		}
		else {
			patternViewer.getDocument().set(""); //$NON-NLS-1$
		}
		patternViewer.setSelectedRange(0, 0);
	}
	
	private void disposePreviewUpdater() {
		final TextViewerJFaceUpdater updater= this.currentPreviewUpdater;
		if (updater != null) {
			this.currentPreviewUpdater= null;
			updater.dispose();
		}
	}
	
	@Override
	protected @Nullable Template editTemplate(final Template template, final boolean edit, final boolean isNameModifiable) {
		final SourceEditorViewerConfigurator configurator= getTemplateEditConfig(template, this.editTemplateProcessor);
		final org.eclipse.statet.ltk.ui.templates.config.EditTemplateDialog dialog= new org.eclipse.statet.ltk.ui.templates.config.EditTemplateDialog(
				getSite().getShell(), template, edit,
				org.eclipse.statet.ltk.ui.templates.config.EditTemplateDialog.EDITOR_TEMPLATE,
				configurator, this.editTemplateProcessor, getContextTypeRegistry(),
				TemplateConfigUI.PREF_QUALIFIER ) {
			
			@Override
			protected void configureForContext(final TemplateContextType contextType) {
				super.configureForContext(contextType);
				final SourceViewer sourceViewer= getSourceViewer();
				final AbstractDocument document= (AbstractDocument) sourceViewer.getDocument();
				AbstractEditorTemplatesPage.this.configureDocument(document, contextType, getSourceViewerConfigurator());
			}
		};
		if (dialog.open() == Window.OK) {
			return dialog.getTemplate();
		}
		return null;
	}
	
	
	protected abstract SourceEditorViewerConfigurator getTemplatePreviewConfig(
			Template template, TemplateVariableProcessor templateProcessor);
	
	protected abstract SourceEditorViewerConfigurator getTemplateEditConfig(
			Template template, TemplateVariableProcessor templateProcessor);
	
	/**
	 * Can be implemented to configure the document when the context is changed
	 * 
	 * @param document the document to adapt
	 * @param contextType the new context
	 * @param configurator the configurator of the viewer/document (preview or edit)
	 */
	protected void configureDocument(final AbstractDocument document,
			final TemplateContextType contextType, final SourceEditorViewerConfigurator configurator) {
	}
	
}
