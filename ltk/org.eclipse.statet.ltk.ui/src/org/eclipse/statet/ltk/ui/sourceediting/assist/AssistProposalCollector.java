/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class AssistProposalCollector {
	
	
	private final Map<AssistProposal, AssistProposal> proposals;
	
	
	public AssistProposalCollector() {
		this.proposals= new HashMap<>();
	}
	
	
	public void add(final AssistProposal proposal) {
		final AssistProposal existing= this.proposals.put(proposal, proposal);
		if (existing != null && existing.getRelevance() > proposal.getRelevance()) {
			this.proposals.put(existing, existing);
		}
	}
	
	public int getCount() {
		return this.proposals.size();
	}
	
	public AssistProposal[] toArray() {
		return this.proposals.values().toArray(new AssistProposal[this.proposals.size()]);
	}
	
	
}
