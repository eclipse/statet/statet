/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface SmartInsertSettings {
	
	
	enum TabAction {
		INSERT_TAB_CHAR,
		INSERT_TAB_LEVEL,
		INSERT_INDENT_LEVEL,
		CORRECT_INDENT;
	}
	
	enum HardWrapMode {
		UPTO_CURSOR,
		MERGE;
	}
	
	
	boolean isSmartInsertEnabledByDefault();
	
	TabAction getSmartInsertTabAction();
	
	
}
