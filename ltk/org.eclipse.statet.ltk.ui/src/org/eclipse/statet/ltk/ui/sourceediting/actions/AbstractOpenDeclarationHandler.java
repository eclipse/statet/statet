/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;
import org.eclipse.statet.ecommons.ui.SharedUIResources;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public abstract class AbstractOpenDeclarationHandler extends AbstractSourceEditorHandler {
	
	
	public AbstractOpenDeclarationHandler() {
	}
	
	
	@Override
	protected @Nullable Object execute(final SourceEditor editor, final ExecutionEvent event)
			throws ExecutionException {
		final ITextSelection selection= (ITextSelection)editor.getViewer().getSelection();
		if (execute(editor,
				JFaceTextRegion.newByStartLength(selection.getOffset(), selection.getLength())) ) {
			return null;
		}
		return ACTION_NOT_AVAILABLE;
	}
	
	protected abstract boolean execute(SourceEditor editor, TextRegion selection);
	
	
	protected void logError(final Exception e, final String name) {
		StatusManager.getManager().handle(new Status(IStatus.INFO, SharedUIResources.BUNDLE_ID, -1,
				NLS.bind("An error occurred when opening editor for the declaration of ''{0}''", name), e));
	}
	
}
