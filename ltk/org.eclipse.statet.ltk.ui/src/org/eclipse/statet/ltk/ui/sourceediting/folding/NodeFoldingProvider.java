/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.folding;

import java.lang.reflect.InvocationTargetException;
import java.util.IdentityHashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ui.sourceediting.folding.FoldingEditorAddon.FoldingStructureComputationContext;


@NonNullByDefault
public interface NodeFoldingProvider extends FoldingProvider {
	
	
	@SuppressWarnings("serial")
	class VisitorMap extends IdentityHashMap<String, AstVisitor> {
		
		
		private static final AstVisitor NONE= new AstVisitor() {
			@Override
			public void visit(final AstNode node) throws InvocationTargetException {
			}
		};
		
		
		private final Map<String, ? extends NodeFoldingProvider> providers;
		
		
		public VisitorMap(final Map<String, ? extends NodeFoldingProvider> embeddedProviders) {
			super(embeddedProviders.size() + 4);
			this.providers= embeddedProviders;
		}
		
		
		public @Nullable AstVisitor getOrCreate(final String type,
				final FoldingStructureComputationContext context) {
			AstVisitor visitor= super.get(type);
			if (visitor == null) {
				final NodeFoldingProvider provider= this.providers.get(type);
				if (provider != null) {
					visitor= provider.createVisitor(context);
				}
				if (visitor == null) {
					visitor= NONE;
				}
				put(type, visitor);
			}
			return (visitor != NONE) ? visitor : null;
		}
		
		@Override
		public @Nullable AstVisitor get(@SuppressWarnings("null") final Object key) {
			final AstVisitor visitor= super.get(key);
			return (visitor != NONE) ? visitor : null;
		}
		
		
	}
	
	
	AstVisitor createVisitor(FoldingStructureComputationContext context);
	
}
