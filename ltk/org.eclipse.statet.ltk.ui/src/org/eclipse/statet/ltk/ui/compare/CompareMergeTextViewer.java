/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.compare;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.contentmergeviewer.ContentMergeViewer;
import org.eclipse.compare.contentmergeviewer.IIgnoreWhitespaceContributor;
import org.eclipse.compare.contentmergeviewer.TextMergeViewer;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.ui.SettingsUpdater;
import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;
import org.eclipse.statet.ecommons.text.core.PartitionConstraint;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.ViewerSourceEditorAdapter;


/**
 * {@link ContentMergeViewer} for source code using a {@link SourceEditorViewerConfigurator}
 * to setup the text viewers.
 */
@NonNullByDefault
public abstract class CompareMergeTextViewer extends TextMergeViewer {
	
	
	private final List<TextViewer> textViewers= new ArrayList<>();
	
	private @Nullable PartitionerDocumentSetupParticipant documentSetupParticipant;
	
	
	@SuppressWarnings("null")
	public CompareMergeTextViewer(final Composite parent, final CompareConfiguration configuration) {
		super(parent, SWT.LEFT_TO_RIGHT, configuration);
	}
	
	
	@Override
	protected void setupDocument(final IDocument document) {
		getDocumentSetupParticipant().setup(document);
	}
	
	protected final PartitionerDocumentSetupParticipant getDocumentSetupParticipant() {
		var documentSetupParticipant= this.documentSetupParticipant;
		if (documentSetupParticipant == null) {
			documentSetupParticipant= createDocumentSetupParticipant();
			this.documentSetupParticipant= documentSetupParticipant;
		}
		return documentSetupParticipant;
	}
	
	protected abstract PartitionerDocumentSetupParticipant createDocumentSetupParticipant();
	
	@Override
	protected Optional<IIgnoreWhitespaceContributor> createIgnoreWhitespaceContributor(final IDocument document) {
		if (document instanceof IDocumentExtension3) {
			final var partitionConstraint= getIgnoreWhitespaceExcludePartitionConstraint();
			if (partitionConstraint != null) {
				return Optional.of(new PartitionIgnoreSpaceContributor(document,
						getDocumentSetupParticipant().getPartitioningId(), partitionConstraint ));
			}
		}
		return Optional.empty();
	}
	
	protected @Nullable PartitionConstraint getIgnoreWhitespaceExcludePartitionConstraint() {
		return null;
	}
	
	protected abstract SourceEditorViewerConfigurator createConfigurator(SourceViewer sourceViewer);
	
	@Override
	protected void configureTextViewer(final TextViewer textViewer) {
		if (textViewer instanceof final SourceViewer sourceViewer) {
			final SourceEditorViewerConfigurator configurator= createConfigurator(sourceViewer);
			configurator.setTarget(new ViewerSourceEditorAdapter(sourceViewer, configurator));
			
			new SettingsUpdater(configurator, sourceViewer.getControl());
		}
		else {
			super.configureTextViewer(textViewer);
		}
		
		this.textViewers.add(textViewer);
	}
	
}
