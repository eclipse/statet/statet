/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.refactoring;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.core.refactoring.participants.DeleteProcessor;
import org.eclipse.ltk.core.refactoring.participants.DeleteRefactoring;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.internal.ltk.ui.refactoring.Messages;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;


/**
 * Command handler deleting selected elements.
 */
public class DeleteElementsHandler extends AbstractElementsHandler {
	
	
	public DeleteElementsHandler(final CommonRefactoringFactory refactoring) {
		super(refactoring);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEnabled(final Object context) {
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(context);
		if (selection != null) {
			setBaseEnabled(!selection.isEmpty());
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		if (selection == null || selection.isEmpty()) {
			return null;
		}
		final SourceStructElement[] sourceElements= LTKSelectionUtils.getSelectedSourceStructElements(selection);
		if (sourceElements != null) {
			final RefactoringAdapter adapter= this.refactoring.createAdapter(sourceElements);
			if (adapter == null) {
				return null;
			}
			
			final IWorkbenchPart activePart= HandlerUtil.getActivePart(event);
			final IWorkbenchPartSite site= activePart.getSite();
			final Shell shell= site.getShell();
			final IProgressService progressService= site.getService(IProgressService.class);
			try {
				startCutRefactoring(sourceElements, adapter, shell, progressService);
			}
			catch (final InvocationTargetException e) {
				StatusManager.getManager().handle(new Status(
						IStatus.ERROR, adapter.getPluginIdentifier(), -1,
						Messages.DeleteElements_error_message,
						e.getCause() ),
						StatusManager.LOG | StatusManager.SHOW);
			}
			catch (final InterruptedException e) {
			}
		}
		return null;
	}
	
	private void startCutRefactoring(final Object[] elements, final RefactoringAdapter adapter,
			final Shell shell, final IProgressService context)
			throws InvocationTargetException, InterruptedException {
		final DeleteProcessor processor= this.refactoring.createDeleteProcessor(elements, adapter);
		if (processor == null) {
			return;
		}
		final Refactoring refactoring= new DeleteRefactoring(processor);
		final RefactoringExecutionHelper helper= new RefactoringExecutionHelper(refactoring, 
				RefactoringCore.getConditionCheckingFailedSeverity(), 
				shell, context );
		helper.perform(false, false);
	}
	
}
