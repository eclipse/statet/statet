/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.refactoring;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;

import org.eclipse.statet.ecommons.ui.util.DNDUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;


public abstract class AbstractElementsHandler extends AbstractHandler {
	
	
	protected final CommonRefactoringFactory refactoring;
	
	
	public AbstractElementsHandler(final CommonRefactoringFactory refactoring) {
		this.refactoring= refactoring;
	}
	
	
	protected boolean copyToClipboard(final ExecutionEvent event, final String sourceCode) {
		final Clipboard clipboard= new Clipboard(UIAccess.getDisplay());
		try {
			return DNDUtils.setContent(clipboard, 
					new Object[] { sourceCode }, 
					new Transfer[] { TextTransfer.getInstance() });
		}
		finally {
			if (clipboard != null && !clipboard.isDisposed()) {
				clipboard.dispose();
			}
		}
	}
	
	protected String getCodeFromClipboard(final ExecutionEvent event) {
		final Clipboard clipboard= new Clipboard(UIAccess.getDisplay());
		try {
			return (String) clipboard.getContents(TextTransfer.getInstance());
		}
		finally {
			if (clipboard != null && !clipboard.isDisposed()) {
				clipboard.dispose();
			}
		}
	}
	
}
