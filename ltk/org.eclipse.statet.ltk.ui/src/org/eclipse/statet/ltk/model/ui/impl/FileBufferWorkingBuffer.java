/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.ui.impl;

import static org.eclipse.statet.ltk.ui.LtkUI.BUNDLE_ID;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.DocumentRewriteSession;
import org.eclipse.jface.text.ISynchronizable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.ltk.model.core.element.SourceDocumentRunnable;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * BasicWorkingBuffer using {@link ITextFileBuffer}.
 * <p>
 * Usually used for editors / the editor context.</p>
 */
@NonNullByDefault
public class FileBufferWorkingBuffer extends org.eclipse.statet.ltk.model.core.impl.FileBufferWorkingBuffer {
	
	
	public static void syncExec(final SourceDocumentRunnable runnable)
			throws InvocationTargetException {
		final AtomicReference<InvocationTargetException> error= new AtomicReference<>();
		UIAccess.getDisplay().syncExec(new Runnable() {
			@Override
			public void run() {
				Object docLock= null;
				final AbstractDocument document= runnable.getDocument();
				if (document instanceof ISynchronizable) {
					docLock= ((ISynchronizable) document).getLockObject();
				}
				if (docLock == null) {
					docLock= new Object();
				}
				
				DocumentRewriteSession rewriteSession= null;
				try {
					if (runnable.getRewriteSessionType() != null) {
						rewriteSession= document.startRewriteSession(runnable.getRewriteSessionType());
					}
					synchronized (docLock) {
						if (runnable.getStampAssertion() > 0 && document.getModificationStamp() != runnable.getStampAssertion()) {
							throw new CoreException(new Status(IStatus.ERROR, BUNDLE_ID,
									"Document out of sync (usuallly caused by concurrent document modifications)." ));
						}
						runnable.run();
					}
				}
				catch (final InvocationTargetException e) {
					error.set(e);
				}
				catch (final Exception e) {
					error.set(new InvocationTargetException(e));
				}
				finally {
					if (rewriteSession != null) {
						document.stopRewriteSession(rewriteSession);
					}
				}
			}
		});
		if (error.get() != null) {
			throw error.get();
		}
	}
	
	
	public FileBufferWorkingBuffer(final SourceUnit unit) {
		super(unit);
	}
	
	
}
