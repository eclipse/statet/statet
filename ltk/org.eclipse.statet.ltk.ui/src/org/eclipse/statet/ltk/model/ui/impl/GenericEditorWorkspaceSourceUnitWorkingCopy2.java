/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.ui.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.IMarkerPositionResolver;
import org.eclipse.statet.ecommons.text.ui.AnnotationMarkerPositionResolver;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.element.SourceDocumentRunnable;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.core.impl.GenericSourceUnitWorkingCopy2;
import org.eclipse.statet.ltk.model.core.impl.WorkingBuffer;


@NonNullByDefault
public abstract class GenericEditorWorkspaceSourceUnitWorkingCopy2<
				TModelContainer extends SourceUnitModelContainer<? extends SourceUnit, ? extends SourceUnitModelInfo>>
		extends GenericSourceUnitWorkingCopy2<TModelContainer>
		implements WorkspaceSourceUnit {
	
	
	public GenericEditorWorkspaceSourceUnitWorkingCopy2(final WorkspaceSourceUnit from) {
		super(from);
	}
	
	
	@Override
	public WorkingContext getWorkingContext() {
		return Ltk.EDITOR_CONTEXT;
	}
	
	@Override
	public IResource getResource() {
		return ((WorkspaceSourceUnit) getUnderlyingUnit()).getResource();
	}
	
	@Override
	public @Nullable IMarkerPositionResolver getMarkerPositionResolver() {
		return AnnotationMarkerPositionResolver.createIfRequired(getResource());
	}
	
	@Override
	protected WorkingBuffer createWorkingBuffer(final SubMonitor m) {
		return new FileBufferWorkingBuffer(this);
	}
	
	@Override
	public void syncExec(final SourceDocumentRunnable runnable) throws InvocationTargetException {
		FileBufferWorkingBuffer.syncExec(runnable);
	}
	
}
