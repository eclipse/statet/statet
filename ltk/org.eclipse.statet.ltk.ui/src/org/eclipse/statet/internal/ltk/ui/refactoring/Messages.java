/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui.refactoring;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String DeleteElements_error_message;
	public static String CutElements_error_message;
	public static String MoveElements_error_message;
	public static String CopyElements_error_message;
	public static String PastingElements_error_message;
	
	public static String ExecutionHelper_CannotExecute_message;
	
	public static String RefactoringStarter_ConfirmSave_Always_message;
	public static String RefactoringStarter_ConfirmSave_message;
	public static String RefactoringStarter_ConfirmSave_title;
	public static String RefactoringStarter_UnexpectedException;
	
	
	static {
		initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
