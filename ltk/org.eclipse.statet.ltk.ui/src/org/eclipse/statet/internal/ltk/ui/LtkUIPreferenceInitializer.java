/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.ui.settings.DecorationPreferences;
import org.eclipse.statet.ecommons.workbench.ui.IWaThemeConstants;
import org.eclipse.statet.ecommons.workbench.ui.util.ThemeUtil;

import org.eclipse.statet.ltk.ui.LtkUI;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;


@NonNullByDefault
public class LtkUIPreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public LtkUIPreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IScopeContext context= DefaultScope.INSTANCE;
		final ThemeUtil theme= new ThemeUtil();
		
		{	final IEclipsePreferences node= context.getNode(LtkUI.BUNDLE_ID);
			node.putBoolean(DecorationPreferences.MATCHING_BRACKET_ENABLED_KEY, true);
			node.put(DecorationPreferences.MATCHING_BRACKET_COLOR_KEY,
					theme.getColorPrefValue(IWaThemeConstants.MATCHING_BRACKET_COLOR) );
		}
		{	final IEclipsePreferences node= context.getNode(LtkUIPreferences.ASSIST_PREF_QUALIFIER);
			node.putBoolean(LtkUIPreferences.CONTENT_ASSIST_AUTO_ACTIVATION_ENABLED_PREF_KEY, true);
			node.putInt(LtkUIPreferences.CONTENT_ASSIST_AUTO_ACTIVATION_DELAY_PREF_KEY, 150);
			node.putBoolean(LtkUIPreferences.CONTENT_ASSIST_SHOW_SUBSTRING_MATCHES_ENABLED_PREF_KEY, true);
			node.put(LtkUIPreferences.CONTEXT_INFO_BACKGROUND_COLOR_PREF_KEY,
					theme.getColorPrefValue(IWaThemeConstants.INFORMATION_BACKGROUND_COLOR) );
			node.put(LtkUIPreferences.CONTEXT_INFO_FOREGROUND_COLOR_PREF_KEY,
					theme.getColorPrefValue(IWaThemeConstants.INFORMATION_COLOR) );
		}
	}
	
}
