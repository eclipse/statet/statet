/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui.refactoring;

import org.eclipse.core.runtime.preferences.InstanceScope;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;

import org.eclipse.statet.ltk.ui.LtkUI;


public class ECommonsRefactoring {
	
	
	public static final BooleanPref PREF_SAVE_ALL_EDITORS= new BooleanPref(LtkUI.BUNDLE_ID + "/Refactoring", "SaveAll.enabled");
	
	
	public static boolean getSaveAllEditors() {
		return EPreferences.getInstancePrefs().getPreferenceValue(PREF_SAVE_ALL_EDITORS);
	}
	
	public static void setSaveAllEditors(final boolean save) {
		PreferenceUtils.setPrefValue(InstanceScope.INSTANCE, PREF_SAVE_ALL_EDITORS, save);
	}
	
}
