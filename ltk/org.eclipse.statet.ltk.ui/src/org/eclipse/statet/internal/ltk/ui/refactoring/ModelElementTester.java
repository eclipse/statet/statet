/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui.refactoring;

import java.util.Iterator;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.jface.viewers.IStructuredSelection;

import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


public class ModelElementTester extends PropertyTester {
	
	
	public static final String IS_ELEMENT_SELECTION= "isElementSelection"; //$NON-NLS-1$
	public static final String IS_ELEMENT_C1_TYPE_SELECTION= "isElementC1TypeSelection"; //$NON-NLS-1$
	public static final String IS_ELEMENT_C2_TYPE_SELECTION= "isElementC2TypeSelection"; //$NON-NLS-1$
	
	
	public ModelElementTester() {
	}
	
	
	@Override
	public boolean test(final Object receiver, final String property, final Object[] args, final Object expectedValue) {
		if (!(receiver instanceof IStructuredSelection)) {
			return false;
		}
		final IStructuredSelection selection= (IStructuredSelection)receiver;
		int mask= 0;
		if (IS_ELEMENT_C1_TYPE_SELECTION.equals(property)) {
			mask= LtkModelElement.MASK_C1;
		}
		else if (IS_ELEMENT_C2_TYPE_SELECTION.equals(property)) {
			mask= LtkModelElement.MASK_C12;
		}
		int numSu= 1;
		String modelType= (String) expectedValue;
		if (modelType.length() > 0 && modelType.charAt(modelType.length()-1) == '*') {
			modelType= modelType.substring(0, modelType.length() - 1);
			numSu= -1;
		}
		
		if (selection.isEmpty()) {
			return false;
		}
		
		final int[] types= parseInts(args);
		final Iterator iter= selection.iterator();
		boolean first= true;
		SourceUnit su= null;
		ITER_ELEMENTS : while (iter.hasNext()) {
			final Object obj= iter.next();
			if (obj instanceof final LtkModelElement element) {
				if (numSu == 1) {
					if (first) {
						first= false;
						if ((su= LtkModelUtils.getSourceUnit(element)) == null) {
							return false;
						}
					}
					else {
						if (su != LtkModelUtils.getSourceUnit(element)) {
							return false;
						}
					}
				}
				if (element.getModelTypeId().equals(modelType)) {
					if (mask == 0) {
						continue ITER_ELEMENTS;
					}
					for (int i= 0; i < types.length; i++) {
						if ((element.getElementType() & mask) == types[i]) {
							continue ITER_ELEMENTS;
						}
					}
				}
				return false;
			}
			else {
				return false;
			}
		}
		return true;
	}
	
	private int[] parseInts(final Object[] args) {
		final int[] ints= new int[args.length];
		for (int i= 0; i < args.length; i++) {
			ints[i]= ((Integer) args[i]).intValue();
		}
		return ints;
	}
	
}
