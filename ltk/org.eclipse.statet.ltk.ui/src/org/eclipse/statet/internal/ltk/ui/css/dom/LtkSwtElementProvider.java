/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui.css.dom;

import org.eclipse.e4.ui.css.core.dom.IElementProvider;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;

import org.w3c.dom.Element;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.sourceediting.swt.EnhStyledText;


@NonNullByDefault
@SuppressWarnings("restriction")
public class LtkSwtElementProvider implements IElementProvider {
	
	
	public LtkSwtElementProvider() {
	}
	
	
	@Override
	public @Nullable Element getElement(final Object element, final CSSEngine engine) {
		if (element instanceof EnhStyledText) {
			return new EnhStyledTextElement((EnhStyledText)element, engine);
		}
		return null;
	}
	
}
