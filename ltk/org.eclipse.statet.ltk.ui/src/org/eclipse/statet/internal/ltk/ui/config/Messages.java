/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui.config;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String Editors_title;
	public static String Editors_link;
	public static String Editors_Appearance;
	public static String Editors_HighlightMatchingBrackets;
	public static String Editors_AppearanceColors;
	public static String Editors_Color;
	public static String Editors_MatchingBracketsHighlightColor;
	public static String Editors_CodeAssistParametersForegrondColor;
	public static String Editors_CodeAssistParametersBackgroundColor;
	public static String Editors_ContentAssist;
	public static String Editors_ContentAssist_AutoTriggerDelay_label;
	public static String Editors_ContentAssist_AutoTriggerDelay_error_message;
	public static String Editors_ContentAssist_Show_SubstringMatches_label;
	public static String Editors_ContentAssist_AutoInsertSingle;
	public static String Editors_ContentAssist_AutoInsertCommon;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
