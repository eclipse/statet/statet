/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.text.ui;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String SyntaxColoring_link;
	public static String SyntaxColoring_List_label;
	public static String SyntaxColoring_MindExtraStyle_tooltip;
	public static String SyntaxColoring_Use_CustomStyle_label;
	public static String SyntaxColoring_Use_NoExtraStyle_label;
	public static String SyntaxColoring_Use_OtherStyle_label;
	public static String SyntaxColoring_Use_OtherStyleOf_label;
	public static String SyntaxColoring_Color;
	public static String SyntaxColoring_Bold;
	public static String SyntaxColoring_Italic;
	public static String SyntaxColoring_Underline;
	public static String SyntaxColoring_Strikethrough;
	public static String SyntaxColoring_Preview;
	
	public static String CodeStyle_TabWidth_label;
	public static String CodeStyle_TabWidth_error_message;
	public static String CodeStyle_Indent_group;
	public static String CodeStyle_Indent_Type_label;
	public static String CodeStyle_Indent_Type_UseTabs_name;
	public static String CodeStyle_Indent_Type_UseSpaces_name;
	public static String CodeStyle_Indent_Levels_label;
	public static String CodeStyle_Indent_ConserveExisting_label;
	public static String CodeStyle_Indent_NumOfSpaces_label;
	public static String CodeStyle_Indent_NumOfSpaces_error_message;
	public static String CodeStyle_Indent_ReplaceOtherTabs_label;
	public static String CodeStyle_LineWidth_label;
	public static String CodeStyle_LineWidth_error_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
