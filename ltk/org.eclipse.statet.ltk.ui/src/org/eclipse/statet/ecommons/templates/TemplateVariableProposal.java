/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.templates;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.templates.TemplateVariableResolver;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Shell;


/**
 * A proposal for insertion of template variables.
 */
public final class TemplateVariableProposal implements ICompletionProposal {
	
	
	private final TemplateVariableResolver variable;
	private final int offset;
	private final int length;
	private final ITextViewer viewer;
	
	private Point selection;
	
	
	/**
	 * Creates a template variable proposal.
	 * 
	 * @param variable the template variable
	 * @param offset the offset to replace
	 * @param length the length to replace
	 * @param viewer the viewer
	 */
	public TemplateVariableProposal(final TemplateVariableResolver variable, final int offset, final int length, final ITextViewer viewer) {
		this.variable= variable;
		this.offset= offset;
		this.length= length;
		this.viewer= viewer;
	}
	
	
	@Override
	public void apply(final IDocument document) {
		try {
			final String variable= this.variable.getType().equals("dollar") ? "$$" : "${" + this.variable.getType() + '}'; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			document.replace(this.offset, this.length, variable);
			this.selection= new Point(this.offset + variable.length(), 0);
		}
		catch (final BadLocationException e) {
			final Shell shell= this.viewer.getTextWidget().getShell();
			MessageDialog.openError(shell, TemplateMessages.TemplateVariableProposal_error_title, e.getMessage());
		}
	}
	
	@Override
	public Point getSelection(final IDocument document) {
		return this.selection;
	}
	
	@Override
	public String getDisplayString() {
		return this.variable.getType();
	}
	
	@Override
	public String getAdditionalProposalInfo() {
		return this.variable.getDescription();
	}
	
	@Override
	public Image getImage() {
		return null;
	}
	
	@Override
	public IContextInformation getContextInformation() {
		return null;
	}
	
}
