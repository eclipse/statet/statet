/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.workbench.search.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * Opens the Search dialog supporting parameters.
 */
public class OpenSearchDialogHandler extends AbstractHandler {
	
	
	public OpenSearchDialogHandler() {
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final String pageId= event.getParameter("pageId");
		IWorkbenchWindow window= HandlerUtil.getActiveWorkbenchWindow(event);
		if (window == null) {
			window= UIAccess.getActiveWorkbenchWindow(false);
		}
		if (pageId != null && pageId.length() > 0) {
			NewSearchUI.openSearchDialog(window, pageId);
		}
		else {
			NewSearchUI.openSearchDialog(window, null);
		}
		return null;
	}
	
}
