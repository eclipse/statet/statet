/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.workbench.ui.util;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.PlatformUI;


/**
 * Utility for theme settings
 */
public class ThemeUtil {
	
	
	private final ColorRegistry fColorRegistry;
	private final StringBuilder fBuffer;
	
	
	public ThemeUtil() {
		this.fColorRegistry= (PlatformUI.isWorkbenchRunning()) ?
				PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry() :
				null;
		this.fBuffer= new StringBuilder();
	}
	
	
	/**
	 * Return the theme color as encoded RGB value as used for preferences
	 * 
	 * @param key key of theme color
	 * @return encoded color
	 */
	public String getColorPrefValue(final String key) {
		final RGB rgb= (this.fColorRegistry != null) ? this.fColorRegistry.getRGB(key) : null;
		if (rgb != null) {
			this.fBuffer.setLength(0);
			this.fBuffer.append(rgb.red);
			this.fBuffer.append(',');
			this.fBuffer.append(rgb.green);
			this.fBuffer.append(',');
			this.fBuffer.append(rgb.blue);
			return this.fBuffer.toString();
		}
		return (key.endsWith("BackgroundColor")) ? "255,255,255" : "0,0,0"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
