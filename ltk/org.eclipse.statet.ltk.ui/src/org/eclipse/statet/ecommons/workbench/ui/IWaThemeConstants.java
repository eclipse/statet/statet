/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.workbench.ui;


/**
 * Global theme constants for the workbench
 */
public interface IWaThemeConstants {
	
	
	String DOC_VIEW_BACKGROUND_COLOR= "org.eclipse.statet.workbench.themes.DocViewBackgroundColor"; //$NON-NLS-1$
	String DOC_VIEW_FONT= "org.eclipse.statet.workbench.themes.DocViewFont"; //$NON-NLS-1$
	
	
	String CODE_DEFAULT_COLOR= "org.eclipse.statet.workbench.themes.CodeDefaultColor"; //$NON-NLS-1$
	
	String CODE_BRACKET_COLOR= "org.eclipse.statet.workbench.themes.CodeBracketColor"; //$NON-NLS-1$
	
	String CODE_PREPROCESSOR_COLOR= "org.eclipse.statet.workbench.themes.CodePreprocessorColor"; //$NON-NLS-1$
	
	String CODE_COMMENT_COLOR= "org.eclipse.statet.workbench.themes.CodeCommentColor"; //$NON-NLS-1$
	
	String CODE_COMMENT_TASKTAG_COLOR= "org.eclipse.statet.workbench.themes.CodeCommentTaskTagColor"; //$NON-NLS-1$
	
	String CODE_DOCU_COLOR= "org.eclipse.statet.workbench.themes.CodeDocuColor"; //$NON-NLS-1$
	
	String CODE_DOCU_TAG_COLOR= "org.eclipse.statet.workbench.themes.CodeDocuTagColor"; //$NON-NLS-1$
	
	String CODE_KEYWORD_COLOR= "org.eclipse.statet.workbench.themes.CodeKeywordColor"; //$NON-NLS-1$
	
	String CODE_CONST_COLOR= "org.eclipse.statet.workbench.themes.CodeConstColor"; //$NON-NLS-1$
	
	String CODE_STRING_COLOR= "org.eclipse.statet.workbench.themes.CodeStringColor"; //$NON-NLS-1$
	
	String CODE_NUMBER_COLOR= "org.eclipse.statet.workbench.themes.CodeNumberColor"; //$NON-NLS-1$
	
	String CODE_OPERATOR_COLOR= "org.eclipse.statet.workbench.themes.CodeOperatorColor"; //$NON-NLS-1$
	
	String CODE_SUB_COLOR= "org.eclipse.statet.workbench.themes.CodeSubColor"; //$NON-NLS-1$
	
	String CODE_DOC_COMMAND_COLOR= "org.eclipse.statet.workbench.themes.CodeDocCommandColor"; //$NON-NLS-1$
	
	String CODE_DOC_COMMAND_SPECIAL_COLOR= "org.eclipse.statet.workbench.themes.CodeDocCommandSpecialColor"; //$NON-NLS-1$
	
	String CODE_DOC_2ND_COLOR= "org.eclipse.statet.workbench.themes.CodeDoc2ndColor"; //$NON-NLS-1$
	
	String CODE_DOC_2ND_COMMAND_COLOR= "org.eclipse.statet.workbench.themes.CodeDoc2ndCommandColor"; //$NON-NLS-1$
	
	String CODE_DOC_2ND_SUB_COLOR= "org.eclipse.statet.workbench.themes.CodeDoc2ndSubColor"; //$NON-NLS-1$
	
	String CODE_VERBATIM_COLOR= "org.eclipse.statet.workbench.themes.CodeVerbatimColor"; //$NON-NLS-1$
	
	String CODE_UNDEFINED_COLOR= "org.eclipse.statet.workbench.themes.CodeUndefinedColor"; //$NON-NLS-1$
	
	String CODE_MOD1_BACKGROUND_COLOR= "org.eclipse.statet.workbench.themes.CodeMod1BackgroundColor"; //$NON-NLS-1$
	
	
	String CONSOLE_INPUT_COLOR= "org.eclipse.statet.workbench.themes.ConsoleInputColor"; //$NON-NLS-1$
	
	String CONSOLE_INFO_COLOR= "org.eclipse.statet.workbench.themes.ConsoleInfoColor"; //$NON-NLS-1$
	
	String CONSOLE_OUTPUT_COLOR= "org.eclipse.statet.workbench.themes.ConsoleOutputColor"; //$NON-NLS-1$
	
	String CONSOLE_ERROR_COLOR= "org.eclipse.statet.workbench.themes.ConsoleErrorColor"; //$NON-NLS-1$
	
	String CONSOLE_2ND_OUTPUT_COLOR= "org.eclipse.statet.workbench.themes.Console2ndOutputColor"; //$NON-NLS-1$
	
	
	String MATCHING_BRACKET_COLOR= "org.eclipse.statet.workbench.themes.MatchingBracketColor"; //$NON-NLS-1$
	
	// TODO: Make editable, use in all information hovers
	String INFORMATION_BACKGROUND_COLOR= "org.eclipse.statet.workbench.themes.InformationBackgroundColor"; //$NON-NLS-1$
	String INFORMATION_COLOR= "org.eclipse.statet.workbench.themes.InformationColor"; //$NON-NLS-1$
	
	
	String TABLE_FONT= "org.eclipse.statet.workbench.themes.TableFont"; //$NON-NLS-1$
	
	
}
