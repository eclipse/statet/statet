/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui.settings;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.ui.RGBPref;


/**
 * Preferences for source viewer/editor decorations.
 */
public class DecorationPreferences {
	
	
	public static final String MATCHING_BRACKET_ENABLED_KEY= "MatchingBrackets.enabled"; //$NON-NLS-1$
	public static final String MATCHING_BRACKET_COLOR_KEY= "MatchingBrackets.color"; //$NON-NLS-1$
	
	
	private final BooleanPref fMatchingBracketsEnabled;
	private final RGBPref fMatchingBracketsColor;
	
	
	public DecorationPreferences(final String commonQualifier) {
		this.fMatchingBracketsEnabled= new BooleanPref(commonQualifier, MATCHING_BRACKET_ENABLED_KEY);
		this.fMatchingBracketsColor= new RGBPref(commonQualifier, MATCHING_BRACKET_COLOR_KEY);
	}
	
	
	
	public BooleanPref getMatchingBracketsEnabled() {
		return this.fMatchingBracketsEnabled;
	}
	
	public RGBPref getMatchingBracketsColor() {
		return this.fMatchingBracketsColor;
	}
	
}
