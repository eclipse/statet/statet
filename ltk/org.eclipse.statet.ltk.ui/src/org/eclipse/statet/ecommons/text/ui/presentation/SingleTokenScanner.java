/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui.presentation;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.rules.Token;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class SingleTokenScanner implements ITokenScanner {
	
	private static final byte EOF= 0;
	private static final byte DEFAULT= 1;
	
	
	private final TextStyleManager<?> textStyles;
	
	private final IToken defaultToken;
	
	private int offset;
	private int length;
	
	private byte state;
	
	
	public SingleTokenScanner(final TextStyleManager<?> textStyles, final String defaultTokenKey) {
		this.textStyles= textStyles;
		
		this.defaultToken= this.textStyles.getToken(defaultTokenKey);
	}
	
	
	@Override
	public void setRange(final IDocument document, final int offset, final int length) {
		this.offset= offset;
		this.length= length;
		this.state= DEFAULT;
	}
	
	@Override
	public IToken nextToken() {
		if (this.state == DEFAULT) {
			this.state= EOF;
			return this.defaultToken;
		}
		return Token.EOF;
	}
	
	@Override
	public int getTokenOffset() {
		return this.offset;
	}
	
	@Override
	public int getTokenLength() {
		return this.length;
	}
	
}
