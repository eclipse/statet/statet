/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui.presentation;


public interface ITextPresentationConstants {
	
	/**
	 * Preference key suffix for enablement of optional text styles.
	 * Value: @value
	 */
	public static final String TEXTSTYLE_USE_SUFFIX= ".use"; //$NON-NLS-1$
	
	/**
	 * Preference key suffix for color text style preference keys.
	 * Value: @value
	 */
	public static final String TEXTSTYLE_COLOR_SUFFIX= ".color"; //$NON-NLS-1$
	
	/**
	 * Preference key suffix for bold text style preference keys.
	 * Value: @value
	 */
	public static final String TEXTSTYLE_BOLD_SUFFIX= ".bold"; //$NON-NLS-1$
	
	/**
	 * Preference key suffix for bold text style preference keys.
	 * Value: @value
	 */
	public static final String TEXTSTYLE_ITALIC_SUFFIX= ".italic"; //$NON-NLS-1$	
	
	/**
	 * Preference key suffix for strikethrough text style preference keys.
	 * Value: @value
	 */
	public static final String TEXTSTYLE_STRIKETHROUGH_SUFFIX= ".strikethrough"; //$NON-NLS-1$
	
	/**
	 * Preference key suffix for underline text style preference keys.
	 * Value: @value
	 */
	public static final String TEXTSTYLE_UNDERLINE_SUFFIX= ".underline"; //$NON-NLS-1$
	
	
	public static final String SETTINGSCHANGE_AFFECTSPRESENTATION_KEY= "affects.Presentation"; //$NON-NLS-1$
	
	
	public static final String ANNOTATIONS_COMMON_OCCURRENCES_TYPE= "org.eclipse.statet.ecommons.text.editorAnnotations.CommonOccurrences"; //$NON-NLS-1$
	
	public static final String ANNOTATIONS_WRITE_OCCURRENCES_TYPE= "org.eclipse.statet.ecommons.text.editorAnnotations.WriteOccurrences"; //$NON-NLS-1$
	
	
}
