/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui.assist;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.link.ILinkedModeListener;
import org.eclipse.jface.text.link.LinkedModeModel;
import org.eclipse.jface.text.link.LinkedModeUI.ExitFlags;
import org.eclipse.jface.text.link.LinkedModeUI.IExitPolicy;
import org.eclipse.jface.text.link.LinkedPosition;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.VerifyEvent;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Simple linked mode exit policy for auto delete code on backslash.
 */
@NonNullByDefault
public class LinkedModeAutodeleteLevel implements IExitPolicy {
	
	
	private final IDocument document;
	
	private final LinkedPosition position;
	
	private final int deleteLength;
	
	
	public LinkedModeAutodeleteLevel(final IDocument document, final LinkedPosition position,
			final int deleteLength) {
		this.document= document;
		this.position= position;
		this.deleteLength= deleteLength;
	}
	
	
	@Override
	public @Nullable ExitFlags doExit(final LinkedModeModel model,
			final VerifyEvent event, final int offset, final int length) {
		try {
			if (offset == this.position.getOffset() && length == 0) {
				switch (event.character) {
				case SWT.BS: // backspace
					this.document.replace(offset - this.deleteLength, this.deleteLength, ""); //$NON-NLS-1$
					return new ExitFlags(ILinkedModeListener.NONE, false);
				default:
					break;
				}
			}
			return new ExitFlags(ILinkedModeListener.NONE, true);
		}
		catch (final BadLocationException e) {
			return new ExitFlags(ILinkedModeListener.NONE, true);
		}
	}
	
}
