/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui.assist;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension2;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * An enhanced implementation of the <code>ICompletionProposal</code> interface implementing
 * the extension interfaces 1-2.
 * <p>
 * It uses a position to track its replacement offset and length. The position must be set up
 * externally.</p>
 */
@NonNullByDefault
public final class PositionBasedCompletionProposal implements ICompletionProposal, ICompletionProposalExtension, ICompletionProposalExtension2 {
	
	
	private final String replacementString;
	private final Position replacementPosition;
	
	private final @Nullable String displayString;
	private final @Nullable Image image;
	
	private final @Nullable String additionalProposalInfo;
	
	private final int cursorPosition;
	
	private final @Nullable IContextInformation contextInformation;
	
	
	/**
	 * Creates a new completion proposal. All fields are initialized based on the provided information.
	 *
	 * @param replacementString the actual string to be inserted into the document
	 * @param replacementPosition the position of the text to be replaced
	 * @param cursorPosition the position of the cursor following the insert relative to replacementOffset
	 * @param image the image to display for this proposal
	 * @param displayString the string to be displayed for the proposal
	 * @param contextInformation the context information associated with this proposal
	 * @param additionalProposalInfo the additional information associated with this proposal
	 */
	public PositionBasedCompletionProposal(final String replacementString, final Position replacementPosition,
			final int cursorPosition,
			final @Nullable Image image, final @Nullable String displayString,
			final @Nullable IContextInformation contextInformation,
			final @Nullable String additionalProposalInfo) {
		this.replacementString= nonNullAssert(replacementString);
		this.replacementPosition= nonNullAssert(replacementPosition);
		this.cursorPosition= cursorPosition;
		this.image= image;
		this.displayString= displayString;
		this.contextInformation= contextInformation;
		this.additionalProposalInfo= additionalProposalInfo;
	}
	
	/**
	 * Creates a new completion proposal based on the provided information.  The replacement string is
	 * considered being the display string too. All remaining fields are set to <code>null</code>.
	 *
	 * @param replacementString the actual string to be inserted into the document
	 * @param replacementPosition the position of the text to be replaced
	 * @param cursorPosition the position of the cursor following the insert relative to replacementOffset
	 */
	public PositionBasedCompletionProposal(final String replacementString, final Position replacementPosition,
			final int cursorPosition) {
		this(replacementString, replacementPosition, cursorPosition,
				null, null, null, null );
	}
	
	
	@Override
	@Deprecated
	public boolean isValidFor(final IDocument document, final int offset) {
		return false;
	}
	
	@Override
	public boolean validate(final IDocument document, final int offset,
			final @Nullable DocumentEvent event) {
		try {
			final String content= document.get(this.replacementPosition.getOffset(), offset - this.replacementPosition.getOffset());
			return (this.replacementString.startsWith(content));
		}
		catch (final BadLocationException e) {
			return false;
		}
	}
	
	
	@Override
	public @Nullable Image getImage() {
		return this.image;
	}
	
	@Override
	public String getDisplayString() {
		return nonNullElse(this.displayString, this.replacementString);
	}
	
	
	@Override
	public void selected(final ITextViewer viewer, final boolean smartToggle) {
	}
	
	@Override
	public void unselected(final ITextViewer viewer) {
	}
	
	@Override
	public @Nullable String getAdditionalProposalInfo() {
		return this.additionalProposalInfo;
	}
	
	
	@Override
	public char @Nullable [] getTriggerCharacters() {
		return null;
	}
	
	
	@Override
	@Deprecated
	public void apply(final IDocument document, final char trigger, final int offset) {
	}
	
	@Override
	public void apply(final IDocument document) {
		try {
			document.replace(this.replacementPosition.getOffset(), this.replacementPosition.getLength(),
					this.replacementString );
		}
		catch (final BadLocationException x) {
		}
	}
	
	@Override
	public void apply(final ITextViewer viewer, final char trigger, final int stateMask, final int offset) {
		apply(nonNullAssert(viewer.getDocument()));
	}
	
	
	@Override
	public @Nullable Point getSelection(final IDocument document) {
		return new Point(this.replacementPosition.getOffset() + this.cursorPosition, 0);
	}
	
	@Override
	public @Nullable IContextInformation getContextInformation() {
		return this.contextInformation;
	}
	
	@Override
	public int getContextInformationPosition() {
		return this.replacementPosition.getOffset();
	}
	
}
