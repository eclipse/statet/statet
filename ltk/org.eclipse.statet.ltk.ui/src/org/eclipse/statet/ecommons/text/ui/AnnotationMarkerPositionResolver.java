/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.texteditor.AbstractMarkerAnnotationModel;

import org.eclipse.statet.ecommons.text.IMarkerPositionResolver;


public class AnnotationMarkerPositionResolver implements IMarkerPositionResolver {
	
	
	public static IMarkerPositionResolver createIfRequired(final IResource file) {
		if (file.getType() == IResource.FILE) {
			final ITextFileBuffer buffer= FileBuffers.getTextFileBufferManager()
					.getTextFileBuffer(file.getFullPath(), LocationKind.IFILE);
			if (buffer != null) {
				final IDocument document= buffer.getDocument();
				final IAnnotationModel model= buffer.getAnnotationModel();
				if (model instanceof AbstractMarkerAnnotationModel) {
					return new AnnotationMarkerPositionResolver(document,
							(AbstractMarkerAnnotationModel) model );
				}
			}
		}
		return null;
	}
	
	
	
	private final IDocument fDocument;
	private final AbstractMarkerAnnotationModel fAnnotationModel;
	
	
	public AnnotationMarkerPositionResolver(final IDocument document,
			final AbstractMarkerAnnotationModel model) {
		this.fDocument= document;
		this.fAnnotationModel= model;
	}
	
	
	@Override
	public IDocument getDocument() {
		return this.fDocument;
	}
	
	@Override
	public Position getPosition(final IMarker marker) {
		synchronized (this.fAnnotationModel.getLockObject()) {
			return this.fAnnotationModel.getMarkerPosition(marker);
		}
	}
	
	@Override
	public int getLine(final IMarker marker) {
		final Position position= getPosition(marker);
		if (position != null) {
			try {
				return this.fDocument.getLineOfOffset(position.getOffset()) + 1;
			}
			catch (final BadLocationException e) {}
		}
		return -1;
	}
	
}
