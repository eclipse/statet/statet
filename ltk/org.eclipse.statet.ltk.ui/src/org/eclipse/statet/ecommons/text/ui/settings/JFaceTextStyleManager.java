/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui.settings;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants;


@NonNullByDefault
public class JFaceTextStyleManager extends PreferenceStoreTextStyleManager<TextAttribute> {
	
	
	public JFaceTextStyleManager(final IPreferenceStore preferenceStore,
			final String stylesGroupId) {
		super(preferenceStore, stylesGroupId);
	}
	
	
	@Override
	protected TextAttribute createStyleData(String key) {
		key= resolveUsedKey(key);
		
		final RGB rgb= PreferenceConverter.getColor(this.preferenceStore, key + ITextPresentationConstants.TEXTSTYLE_COLOR_SUFFIX);
		int style= this.preferenceStore.getBoolean(key + ITextPresentationConstants.TEXTSTYLE_BOLD_SUFFIX) ?
				SWT.BOLD : SWT.NORMAL;
		if (this.preferenceStore.getBoolean(key + ITextPresentationConstants.TEXTSTYLE_ITALIC_SUFFIX)) {
			style |= SWT.ITALIC;
		}
		if (this.preferenceStore.getBoolean(key + ITextPresentationConstants.TEXTSTYLE_UNDERLINE_SUFFIX)) {
			style |= TextAttribute.UNDERLINE;
		}
		if (this.preferenceStore.getBoolean(key + ITextPresentationConstants.TEXTSTYLE_STRIKETHROUGH_SUFFIX)) {
			style |= TextAttribute.STRIKETHROUGH;
		}
		
		return new TextAttribute(new Color(rgb), null, style);
	}
	
}
