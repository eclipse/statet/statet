/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - removed editor dependencies
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextViewerExtension4;
import org.eclipse.jface.text.source.ISourceViewer;


/**
 * Command handler for source viewers showing the information popup.
 * 
 * This action behaves in two different ways: If there is no current text hover, the information
 * is displayed using information presenter. If there is a current text hover, it is converted 
 * into a information presenter in order to make it sticky.
 */
public final class InformationDispatchHandler extends AbstractHandler {
	
	
	private final ISourceViewer fSourceViewer;
	
	/** The wrapped text operation action. */
	private final TextViewerAction fTextOperationAction;
	
	
	public InformationDispatchHandler(final ISourceViewer sourceViewer) {
		if (sourceViewer == null) {
			throw new NullPointerException("viewer"); //$NON-NLS-1$
		}
		this.fSourceViewer= sourceViewer;
		this.fTextOperationAction= new TextViewerAction(sourceViewer, ISourceViewer.INFORMATION);
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		if (this.fSourceViewer instanceof final ITextViewerExtension4 extension4) {
			if (extension4.moveFocusToWidgetToken()) {
				return null;
			}
		}
		// otherwise, just run the action
		if (this.fTextOperationAction.isEnabled()) {
			this.fTextOperationAction.run();
		}
		return null;
	}
	
}
