<?xml version='1.0' encoding='UTF-8'?>
<!--
 #=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<schema targetNamespace="org.eclipse.statet.ltk" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appinfo>
         <meta.schema id="ContentTypeActivation"
               plugin="org.eclipse.statet.ltk.core"
               name="Additional activation for content types"/>
      </appinfo>
      <documentation>
         This extension-point allows multiple content types in addition to the primary content type.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appinfo>
            <meta.element />
         </appinfo>
      </annotation>
      <complexType>
         <choice minOccurs="0" maxOccurs="unbounded">
            <element ref="contentType"/>
         </choice>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="contentType">
      <annotation>
         <documentation>
            Activation for content types
         </documentation>
      </annotation>
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  (Primary) content type id
               </documentation>
            </annotation>
         </attribute>
         <attribute name="secondaryId" type="string" use="required">
            <annotation>
               <documentation>
                  Id of secondary content type to associate to primary content type
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appinfo>
         <meta.section type="since"/>
      </appinfo>
      <documentation>
         0.5
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="examples"/>
      </appinfo>
      <documentation>
         Activate R content type in Sweave (R/LaTeX) documents:
&lt;pre&gt;
   &lt;extension
         point=&quot;org.eclipse.statet.ltk.ContentTypeActivation&quot;&gt;
      &lt;contentType
            id=&quot;org.eclipse.statet.r.contentTypes.RweaveTex&quot;
            secondaryId=&quot;org.eclipse.statet.r.contentTypes.R&quot;&gt;
      &lt;/contentType&gt;
   &lt;/extension&gt;
&lt;/pre&gt;
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="copyright"/>
      </appinfo>
      <documentation>
         Copyright (c) 2008, 2025 Stephan Wahlbrink and others.

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
which is available at https://www.apache.org/licenses/LICENSE-2.0.

SPDX-License-Identifier: EPL-2.0 OR Apache-2.0

Contributors:
    Stephan Wahlbrink &lt;sw@wahlbrink.eu&gt; - initial API and implementation
      </documentation>
   </annotation>

</schema>
