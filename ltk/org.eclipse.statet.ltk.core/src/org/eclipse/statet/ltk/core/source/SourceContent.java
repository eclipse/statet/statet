/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.source;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextLineInformation;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.text.core.util.TextLineInformationCreator;


/**
 * Source code with time stamp.
 */
@NonNullByDefault
public class SourceContent extends BasicTextRegion implements TextRegion {
	
	
	private static final TextLineInformationCreator LINES_CREATOR= new TextLineInformationCreator();
	
	
	private final long stamp;
	
	private final String string;
	
	private volatile @Nullable TextLineInformation stringLines;
	
	
	public SourceContent(final long stamp, final String text, final int startOffset) {
		super(startOffset, startOffset + text.length());
		this.stamp= stamp;
		this.string= text;
	}
	
	public SourceContent(final long stamp, final String text) {
		this(stamp, text, 0);
	}
	
	
	public final long getStamp() {
		return this.stamp;
	}
	
	
	public final String getString() {
		return this.string;
	}
	
	/**
	 * Returns the line information for the {@link #getString() text} with offsets refering to the
	 * string (<code>lines.getStartOffset(0) == 0</code>).
	 * 
	 * @return the line information
	 */
	public final TextLineInformation getStringLines() {
		TextLineInformation lines= this.stringLines;
		if (lines == null) {
			synchronized (LINES_CREATOR) {
				lines= this.stringLines;
				if (lines == null) {
					lines= LINES_CREATOR.create(this.string);
				}
			}
		}
		return lines;
	}
	
	
//	/**
//	 * Returns the offset of start of the {@link #getText() text} in the complete source.
//	 * 
//	 * @return the begin offset
//	 */
//	public final int getStartOffset()
	
//	/**
//	 * Returns the offset of end of the {@link #getText() text} in the complete source.
//	 * 
//	 * @return the end offset (exclusive)
//	 */
//	public final int getEndOffset() {
	
	/**
	 * Returns the text of the specified source region.
	 * 
	 * @param startOffset the start offset in the complete source
	 * @param endOffset the end offset in the complete source (exclusive)
	 * @return the text of the region
	 */
	public final String getString(final int startOffset, final int endOffset) {
		final int textStartOffset= getStartOffset();
		return this.string.substring(startOffset - textStartOffset, endOffset - textStartOffset);
	}
	
	/**
	 * Returns the text of the specified source region.
	 * 
	 * @param region the region in the complete source
	 * @return the text of the region
	 */
	public final String getString(final TextRegion region) {
		final int textStartOffset= getStartOffset();
		return this.string.substring(
				region.getStartOffset() - textStartOffset,
				region.getEndOffset() - textStartOffset );
	}
	
	/**
	 * Appends the text of the specified source region to the string builder.
	 * 
	 * @param sb the target
	 * @param startOffset the start offset in the complete source
	 * @param endOffset the end offset in the complete source (exclusive)
	 * @return the text of the region
	 */
	public final void appendStringTo(final StringBuilder sb, final int startOffset, final int endOffset) {
		final int textStartOffset= getStartOffset();
		sb.append(this.string, startOffset - textStartOffset, endOffset - textStartOffset);
	}
	
	/**
	 * Appends the text of the specified source region to the string builder.
	 * 
	 * @param sb the target
	 * @param region the region in the complete source
	 * @return the text of the region
	 */
	public final void appendStringTo(final StringBuilder sb, final TextRegion region) {
		final int textStartOffset= getStartOffset();
		sb.append(this.string,
				region.getStartOffset() - textStartOffset,
				region.getEndOffset() - textStartOffset );
	}
	
	/**
	 * Returns the character at the specified source offset.
	 * 
	 * @param offset the offset in the complete source
	 * @return the character
	 */
	public final char getChar(final int offset) {
		return this.string.charAt(offset - getStartOffset());
	}
	
	
	@Override
	public String toString() {
		return getString();
	}
	
}
