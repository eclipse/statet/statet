/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.util;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class EntityRole {
	
	
	public static final int MARCRELATOR=        1 <<  0;
	
	public static final int R_PERSON=           1 << 12;
	
	
	public static final ImList<EntityRole> COMMON_ROLES= ImCollections.newList(
			new EntityRole("aut", "Author",            MARCRELATOR | R_PERSON),
			new EntityRole("com", "Compiler",          MARCRELATOR | R_PERSON),
			new EntityRole("cre", "Creator",           MARCRELATOR | R_PERSON),
			new EntityRole("cph", "Copyright Holder",  MARCRELATOR | R_PERSON),
			new EntityRole("ctb", "Contributor",       MARCRELATOR | R_PERSON),
			new EntityRole("ctr", "Contractor",        MARCRELATOR | R_PERSON),
			new EntityRole("dtc", "Data Contributor",  MARCRELATOR | R_PERSON),
			new EntityRole("fnd", "Funder",            MARCRELATOR | R_PERSON),
			new EntityRole("rev", "Reviewer",          MARCRELATOR | R_PERSON),
			new EntityRole("ths", "Thesis Advisor",    MARCRELATOR | R_PERSON),
			new EntityRole("trl", "Translator",        MARCRELATOR | R_PERSON) );
	
	
	private final String code;
	private final String name;
	
	private final int flags;
	
	
	public EntityRole(final String code, final String name,
			final int flags) {
		this.code= code;
		this.name= name;
		this.flags= flags;
	}
	
	public EntityRole(final String code, final String name) {
		this(code, name, 0);
	}
	
	
	public String getCode() {
		return this.code;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getFlags() {
		return this.flags;
	}
	
	
	@Override
	public String toString() {
		return this.code;
	}
	
}
