/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.util;

import java.util.Comparator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.model.core.element.NameAccess;


@NonNullByDefault
public class NameUtils {
	
	
	public static final Comparator<NameAccess<?, ?>> NAME_POSITION_COMPARATOR=
			new Comparator<>() {
				private int getOffset(final @Nullable AstNode nameNode) {
					return (nameNode != null) ? nameNode.getStartOffset() : Integer.MAX_VALUE;
				}
				@Override
				public int compare(final NameAccess<?, ?> access1, final NameAccess<?, ?> access2) {
					return getOffset(access1.getNameNode()) - getOffset(access2.getNameNode());
				}
			};
	
	
	public static final boolean areEqual(final @Nullable String s1, final @Nullable String s2) {
		return ((s1 == s2)
				|| (s1 != null && s2 != null && s1.hashCode() == s2.hashCode() && s1.equals(s2)) );
	}
	
}
