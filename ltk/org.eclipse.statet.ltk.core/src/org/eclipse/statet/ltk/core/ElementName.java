/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core;

import org.eclipse.statet.jcommons.collections.IntIntervalArrays;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;


/**
 * Name of an element like an {@link LtkModelElement}.
 * <p>
 * A element name consists of one or multiple segments. Each segment is of a special
 * type (to differ different way to identify the element). The type is model type dependent.
 * The segment has raw segment name and a name which can be used to display it (including
 * all following segments).
 */
@NonNullByDefault
public interface ElementName {
	
	/**
	 * Returns the type the name is specified.
	 * 
	 * @return a model dependent type constant
	 */
	int getType();
	
	/**
	 * Returns the raw name of the segment.
	 * 
	 * @return the name or <code>null</code>
	 */
	@Nullable String getSegmentName();
	
	/**
	 * Returns a default text representation.
	 * 
	 * @return the name
	 */
	String getDisplayName();
	
	default int[] correctDisplayNameRegions(final int[] regions, final int offset) {
		return IntIntervalArrays.removeTail(regions, offset);
	}
	
	/**
	 * Returns the next child segment, if exists.
	 * 
	 * @return the next segment or <code>null</code>
	 */
	@Nullable ElementName getNextSegment();
	
	/**
	 * Returns the last segment (end).
	 * 
	 * @return the last segment
	 */
	default ElementName getLastSegment() {
		@NonNull ElementName lastSegment;
		ElementName nextSegment= this;
		do {
			lastSegment= nextSegment;
		} while ((nextSegment= nextSegment.getNextSegment()) != null);
		return lastSegment;
	}
	
}
