/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core;

import org.eclipse.statet.ltk.model.core.ModelTypeDescriptor;


/**
 * Allows multiple content types in addition to the primary content type.
 * <p>
 * E.g.: Sweave documents (LaTeX/R) inherit primary of LaTeX and have the secondary type of R.</p>
 */
public interface IExtContentTypeManager {
	
	
	String[] getSecondaryContentTypes(String primaryContentType);
	String[] getPrimaryContentTypes(String secondaryContentType);
	
	boolean matchesActivatedContentType(String primaryContentTypeId, String secondaryContentTypeId, boolean self);
	
	
	ModelTypeDescriptor getModelType(String modelTypeId);
	
	ModelTypeDescriptor getModelTypeForContentType(String contentTypeId);
	
}
