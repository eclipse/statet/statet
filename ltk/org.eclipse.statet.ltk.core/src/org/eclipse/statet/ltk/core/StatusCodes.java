/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.Status;


@NonNullByDefault
public class StatusCodes {
	
	
	public static final int SHIFT_SEVERITY=                 20;
	public static final int SHIFT_SEVERITY_IN_CHILD=        24;
	public static final int SHIFT_CTX1=                     16;
	public static final int SHIFT_CTX2=                     12;
	public static final int SHIFT_TYPE1=                     8;
	public static final int SHIFT_TYPE2=                     4;
	public static final int SHIFT_TYPE3=                     0;
	
	public static final int SEVERITY=                       0b111 << SHIFT_SEVERITY;
	public static final int INFO=                           Status.INFO << SHIFT_SEVERITY;
	public static final int WARNING=                        Status.WARNING << SHIFT_SEVERITY;
	public static final int ERROR=                          Status.ERROR << SHIFT_SEVERITY;
	
	public static final int SUBSEQUENT=                     0b1 << SHIFT_SEVERITY + 3;
	
	public static final int SEVERITY_IN_CHILD=              SEVERITY << SHIFT_SEVERITY_IN_CHILD - SHIFT_SEVERITY;
	public static final int WARNING_IN_CHILD=               WARNING << SHIFT_SEVERITY_IN_CHILD - SHIFT_SEVERITY;
	public static final int ERROR_IN_CHILD=                 ERROR << SHIFT_SEVERITY_IN_CHILD - SHIFT_SEVERITY;
	
	public static final int CTX1=                           0xF << SHIFT_CTX1;
	public static final int CTX2=                           0xF << SHIFT_CTX2;
	public static final int CTX12=                          CTX1 | CTX2;
	
	public static final int TYPE1=                          0xF << SHIFT_TYPE1;
	public static final int TYPE2=                          0xF << SHIFT_TYPE2;
	public static final int TYPE3=                          0xF << SHIFT_TYPE3;
	public static final int TYPE12=                         TYPE1 | TYPE2 | SEVERITY;
	public static final int TYPE123=                        TYPE12 | TYPE3;
	
	
	public static final int TYPE1_OK=                       0x0 << SHIFT_TYPE1;
	public static final int TYPE1_SYNTAX_TOKEN_INCORRECT=   0x1 << SHIFT_TYPE1;
	public static final int TYPE1_SYNTAX_TOKEN_UNEXPECTED=  0x2 << SHIFT_TYPE1;
	public static final int TYPE1_SYNTAX_TOKEN_MISSING=     0x3 << SHIFT_TYPE1;
	public static final int TYPE12_SYNTAX_NODE_MISSING=         TYPE1_SYNTAX_TOKEN_MISSING | 0x1 << SHIFT_TYPE2;
	public static final int TYPE1_RUNTIME_ERROR=            0xF << SHIFT_TYPE1;
	
	
	public static final byte getStatusSeverity(final int code) {
		return ((code & (SEVERITY | SEVERITY_IN_CHILD)) != 0) ?
				((code & (ERROR | ERROR_IN_CHILD)) != 0) ?
						Status.ERROR :
						((code & (WARNING | WARNING_IN_CHILD)) != 0) ?
								Status.WARNING :
								Status.INFO :
				Status.OK;
	}
	
	public static final boolean isError(final int code) {
		return ((code & (ERROR | ERROR_IN_CHILD)) != 0);
	}
	
	
	private StatusCodes() {
	}
	
}
