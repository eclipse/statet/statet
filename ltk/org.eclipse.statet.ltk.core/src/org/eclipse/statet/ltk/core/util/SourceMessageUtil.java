/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.string.Chars;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.core.source.StatusDetail;


@NonNullByDefault
public class SourceMessageUtil {
	
	
	private static final int FULL_TEXT_LIMIT= 95 + 3;
	private static final int MID_TEXT_LIMIT= 50 + 3;
	private static final int SHORT_TEXT_LIMIT= 25 + 3;
	
	private static final char SPECIAL_OPEN= '❬';
	private static final char SPECIAL_CLOSE= '❭';
	private static final String LIMIT_AFFIX= "\u200A…";
	
	
	private final StringBuilder tmpBuilder= new StringBuilder();
	
	private SourceContent sourceContent= nonNullLateInit();
	
	
	public SourceMessageUtil(final SourceContent sourceContent) {
		setSourceContent(sourceContent);
	}
	
	public SourceMessageUtil() {
	}
	
	
	public void setSourceContent(final SourceContent sourceContent) {
		this.sourceContent= nonNullAssert(sourceContent);
	}
	
	
	public final StringBuilder getStringBuilder() {
		this.tmpBuilder.setLength(0);
		return this.tmpBuilder;
	}
	
	
	private int indexOfSpecial(final String text, int startIndex, final int endIndex) {
		while (startIndex < endIndex) {
			final char c= text.charAt(startIndex);
			switch (c) {
			case 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F:
			case 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F:
			case 0x7F:
				return startIndex;
			case ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/':
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			case ':', ';', '<', '=', '>', '?', '@':
			case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
			case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
			case '{', '|', '}', '~':
				startIndex++;
				continue;
			default:
				switch (Character.getType(c)) {
				case Character.UNASSIGNED:
				case Character.CONTROL:
				case Character.FORMAT:
				case Character.PRIVATE_USE:
				case Character.SURROGATE:
					return startIndex;
				}
				startIndex++;
				continue;
			}
		}
		return -1;
	}
	
	private String checkQuoteText(final String text, final int startIndex, final int endIndex,
			final boolean limited) {
		int idx= indexOfSpecial(text, startIndex, endIndex);
		if (idx == -1) {
			return (startIndex == 0 && endIndex == text.length()) ?
					text : text.substring(startIndex, endIndex);
		}
		int lastIdx= 0;
		final var sb= getStringBuilder();
		ITER_CHARS: while (idx < endIndex) {
			final char c0= text.charAt(idx);
			char c1;
			switch (c0) {
			case ' ':
			case '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/':
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			case ':', ';', '<', '=', '>', '?', '@':
			case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
			case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
			case '{', '|', '}', '~':
				idx++;
				continue;
			case 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x0B, 0x0C, 0x0E, 0x0F:
			case 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F:
			case 0x7F:
				if (idx > lastIdx) {
					sb.append(text, lastIdx, idx);
				}
				sb.append(SPECIAL_OPEN);
				sb.append(Chars.getAsciiControlAbbr(c0));
				sb.append(SPECIAL_CLOSE);
				lastIdx= ++idx;
				continue;
			case '\t':
				if (idx > lastIdx) {
					sb.append(text, lastIdx, idx);
				}
				sb.append(SPECIAL_OPEN);
				do {
					sb.append("»");
				} while (++idx < endIndex && text.charAt(idx) == '\t');
				sb.append(SPECIAL_CLOSE);
				lastIdx= idx;
				continue;
			case '\n':
				if (idx > lastIdx) {
					sb.append(text, lastIdx, idx);
				}
				sb.append(SPECIAL_OPEN + "¶" + SPECIAL_CLOSE);
				lastIdx= ++idx;
				continue;
			case '\r':
				if (idx > lastIdx) {
					sb.append(text, lastIdx, idx);
				}
				if (++idx < endIndex && text.charAt(idx) == '\n') {
					sb.append(SPECIAL_OPEN + "¤¶" + SPECIAL_CLOSE);
				}
				else {
					sb.append(SPECIAL_OPEN);
					sb.append(Chars.getAsciiControlAbbr(c0));
					sb.append(SPECIAL_CLOSE);
				}
				lastIdx= ++idx;
				continue;
			default:
				switch (Character.getType(c0)) {
				case Character.UNASSIGNED:
				case Character.CONTROL:
				case Character.FORMAT:
				case Character.PRIVATE_USE:
					if (idx > lastIdx) {
						sb.append(text, lastIdx, idx);
					}
					sb.append(SPECIAL_OPEN);
					sb.append(Chars.formatCodePoint(c0));
					sb.append(SPECIAL_CLOSE);
					lastIdx= ++idx;
					continue;
				case Character.SURROGATE:
					if (idx > lastIdx) {
						sb.append(text, lastIdx, idx);
					}
					if (++idx < endIndex
							&& Character.isHighSurrogate(c0)
							&& Character.isLowSurrogate(c1= text.charAt(idx)) ) {
						sb.append(SPECIAL_OPEN);
						sb.append(Chars.formatCodePoint(Character.toCodePoint(c0, c1)));
						sb.append(SPECIAL_CLOSE);
						lastIdx= ++idx;
						continue;
					}
					else if (limited && idx == endIndex) {
						lastIdx= --idx;
						break ITER_CHARS;
					}
					else {
						sb.append(SPECIAL_OPEN + "!\u200A");
						sb.append(Chars.formatCodePoint(c0));
						sb.append(SPECIAL_CLOSE);
						lastIdx= idx;
						continue;
					}
				default:
					idx++;
					continue;
				}
			}
		}
		if (idx > lastIdx) {
			sb.append(text, lastIdx, idx);
		}
		if (limited) {
			sb.append(LIMIT_AFFIX);
		}
		return sb.toString();
	}
	
	public String checkQuoteText(final String text) {
		return checkQuoteText(text, 0, text.length(), false);
	}
	
	public String checkQuoteText(final String text, final int limit) {
		if (text.length() > limit) {
			return checkQuoteText(text, 0, limit - 3, true);
		}
		else {
			return checkQuoteText(text, 0, text.length(), false);
		}
	}
	
	public String checkQuoteText(final int startOffset, final int endOffset) {
		return checkQuoteText(
				this.sourceContent.getString(startOffset, endOffset),
				0, endOffset - startOffset, false );
	}
	
	public String checkQuoteText(final int startOffset, final int endOffset,
			final int limit) {
		if (endOffset - startOffset > limit) {
			return checkQuoteText(
					this.sourceContent.getString(startOffset, endOffset + limit - 3),
					0, limit - 3, true );
		}
		else {
			return checkQuoteText(
					this.sourceContent.getString(startOffset, endOffset),
					0, endOffset - startOffset, false );
		}
	}
	
	public String checkRawText(final String text, final int limit) {
		if (text.length() > limit) {
			final StringBuilder sb= getStringBuilder();
			sb.append(text, 0, limit - 3);
			sb.append('…');
			return sb.toString();
		}
		else {
			return text;
		}
	}
	
	public String checkRawText(final int startOffset, final int endOffset,
			final int limit) {
		if (endOffset - startOffset > limit) {
			final var sb= getStringBuilder();
			this.sourceContent.appendStringTo(sb, startOffset, endOffset + limit - 3);
			sb.append(LIMIT_AFFIX);
			return sb.toString();
		}
		else {
			return this.sourceContent.getString(startOffset, endOffset);
		}
	}
	
	
	public String checkFullQuoteText(final String text) {
		return checkQuoteText(text, FULL_TEXT_LIMIT);
	}
	
	public String getFullQuoteText(final AstNode node) throws BadLocationException {
		final String text= node.getText();
		if (text != null) {
			return checkQuoteText(text, FULL_TEXT_LIMIT);
		}
		else {
			return checkQuoteText(node.getStartOffset(), node.getEndOffset(), FULL_TEXT_LIMIT);
		}
	}
	
	public String getFullRawText(final AstNode node)
			throws BadLocationException {
		final String text= node.getText();
		if (text != null) {
			return checkRawText(text, FULL_TEXT_LIMIT);
		}
		else {
			return checkRawText(node.getStartOffset(), node.getEndOffset(), FULL_TEXT_LIMIT);
		}
	}
	
	
	public String checkMidQuoteText(final String text) {
		return checkQuoteText(text, MID_TEXT_LIMIT);
	}
	
	public String getMidQuoteText(final AstNode node, final int offset)
			throws BadLocationException {
		final String text= node.getText();
		if (text != null) {
			return checkQuoteText(text, MID_TEXT_LIMIT);
		}
		else {
			return checkQuoteText(node.getStartOffset() + offset, node.getEndOffset(), MID_TEXT_LIMIT);
		}
	}
	
	public String getMidRawText(final AstNode node, final int offset)
			throws BadLocationException {
		final String text= node.getText();
		if (text != null) {
			return checkRawText(text, MID_TEXT_LIMIT);
		}
		else {
			return checkRawText(node.getStartOffset() + offset, node.getEndOffset(), MID_TEXT_LIMIT);
		}
	}
	
	
	public String checkShortQuoteText(final String text) {
		return checkQuoteText(text, SHORT_TEXT_LIMIT);
	}
	
	public String getShortQuoteText(final AstNode node, final int offset)
			throws BadLocationException {
		final String text= node.getText();
		if (text != null) {
			return checkQuoteText(text, SHORT_TEXT_LIMIT);
		}
		else {
			return checkQuoteText(node.getStartOffset() + offset, node.getEndOffset(), SHORT_TEXT_LIMIT);
		}
	}
	
	public String getShortRawText(final AstNode node, final int offset)
			throws BadLocationException {
		final String text= node.getText();
		if (text != null) {
			return checkRawText(text, SHORT_TEXT_LIMIT);
		}
		else {
			return checkRawText(node.getStartOffset() + offset, node.getEndOffset(), SHORT_TEXT_LIMIT);
		}
	}
	
	
	public String getDetailQuoteText(final AstNode node, final int offset, final StatusDetail detail)
			throws BadLocationException {
		final String text= node.getText();
		if (text != null) {
			final int begin= detail.getStartOffset() - node.getStartOffset() - offset;
			return checkQuoteText(text.substring(begin, begin + detail.getLength()));
		}
		else {
			return checkQuoteText(detail.getStartOffset(), detail.getEndOffset());
		}
	}
	
	
	public int expandSpaceStart(final int offset) {
		switch ((this.sourceContent.contains(offset - 1)) ? this.sourceContent.getChar(offset - 1) : -1) {
		case ' ':
		case '\t':
			return offset - 1;
		default:
			return offset;
		}
	}
	
	public int expandSpaceEnd(final int offset) {
		switch ((this.sourceContent.contains(offset)) ? this.sourceContent.getChar(offset) : -1) {
		case ' ':
		case '\t':
			return offset + 1;
		default:
			return offset;
		}
	}
	
}
