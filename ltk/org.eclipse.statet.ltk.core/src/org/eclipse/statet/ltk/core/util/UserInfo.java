/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.util;

import java.util.Locale;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.SystemUtils;

import org.eclipse.statet.internal.ltk.core.GitConfigInfo;


@NonNullByDefault
public class UserInfo {
	
	
	private static UserInfo gAuthorInfo;
	
	
	private static final @Nullable String checkName(@Nullable String s) {
		if (s != null) {
			s= s.trim();
			if (s.length() >= 2) {
				return s;
			}
		}
		return null;
	}
	
	private static final @Nullable String checkEmail(@Nullable String s) {
		if (s != null) {
			s= s.trim();
			if (s.length() >= 2 && s.indexOf('@') > 0) {
				return s;
			}
		}
		return null;
	}
	
	static {
		int source;
		String name;
		String email;
		
		{	source= 1;
			name= checkName(System.getProperty("author.name")); //$NON-NLS-1$
			email= checkEmail(System.getProperty("author.email")); //$NON-NLS-1$
		}
		if (name == null || email == null) {
			source= 2; // try Git
			try {
				final UserInfo info= GitConfigInfo.load();
				if (name == null) {
					name= checkName(info.getName());
				}
				if (email == null) {
					email= checkEmail(info.getEmail());
				}
			}
			catch (final Throwable e) {
				name= checkName(System.getenv("GIT_AUTHOR_NAME")); //$NON-NLS-1$
				email= checkEmail(System.getenv("GIT_AUTHOR_EMAIL")); //$NON-NLS-1$
			}
		}
		
		if (name == null || email == null) {
			source= 3;
			if (name == null) {
				name= System.getProperty(SystemUtils.USER_NAME_KEY, ""); //$NON-NLS-1$
			}
			if (email == null) {
				email= name.toLowerCase(Locale.getDefault()).replace(' ', '.') + '@';
			}
		}
		
		gAuthorInfo= new UserInfo(name, email, source);
	}
	
	
	public static UserInfo getAuthorInfo() {
		return gAuthorInfo;
	}
	
	
	private final String name;
	
	private final String email;
	
	private final int source;
	
	
	public UserInfo(final String name, final String email, final int source) {
		this.name= name;
		this.email= email;
		this.source= source;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public int getSource() {
		return this.source;
	}
	
}
