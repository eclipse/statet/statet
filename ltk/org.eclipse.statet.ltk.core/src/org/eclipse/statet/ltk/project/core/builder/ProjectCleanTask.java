/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.project.core.builder;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.project.core.LtkProject;


@NonNullByDefault
public class ProjectCleanTask<TProject extends LtkProject,
				TSourceUnit extends WorkspaceSourceUnit,
				TBuildParticipant extends ProjectBuildParticipant<TProject, TSourceUnit>>
		extends ProjectTask<TProject, TSourceUnit, TBuildParticipant> {
	
	
	public ProjectCleanTask(final ProjectTaskBuilder<TProject, TSourceUnit, TBuildParticipant> builder) {
		super(builder);
	}
	
	
	public void clean(final SubMonitor m) {
		final IProject project= getProject();
		
		clear(project);
	}
	
}
