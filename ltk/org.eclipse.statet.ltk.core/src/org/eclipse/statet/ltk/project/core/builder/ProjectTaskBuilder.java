/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.project.core.builder;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.project.core.LtkProject;


@NonNullByDefault
public abstract class ProjectTaskBuilder<TProject extends LtkProject,
				TSourceUnit extends WorkspaceSourceUnit,
				TParticipant extends ProjectBuildParticipant<TProject, TSourceUnit>>
		extends IncrementalProjectBuilder {
	
	
	public static abstract class BuilderDefinition<TProject extends LtkProject,
			TSourceUnit extends WorkspaceSourceUnit,
			TBuildParticipant extends ProjectBuildParticipant<TProject, TSourceUnit>> {
		
		
		private final String bundleId;
		private final Plugin bundle;
		
		private final String projectNatureId;
		private final String projectTypeLabel;
		
		private final Class<TSourceUnit> sourceUnitType;
		
		private final Class<TBuildParticipant> participantType;
		
		
		public BuilderDefinition(final String bundleId, final Plugin bundle,
				final String projectNatureId, final String projectLabel,
				final Class<TSourceUnit> sourceUnitType,
				final Class<TBuildParticipant> participantType) {
			this.bundleId= bundleId;
			this.bundle= bundle;
			
			this.projectNatureId= projectNatureId;
			this.projectTypeLabel= projectLabel;
			
			this.sourceUnitType= sourceUnitType;
			
			this.participantType= participantType;
		}
		
		
		public String getBundleId() {
			return this.bundleId;
		}
		
		public Plugin getBundle() {
			return this.bundle;
		}
		
		
		public String getProjectNatureId() {
			return this.projectNatureId;
		}
		
		public String getProjectTypeLabel() {
			return this.projectTypeLabel;
		}
		
		
		public abstract int checkSourceUnitContent(final IContentType contentType);
		
		public Class<TSourceUnit> getSourceUnitType() {
			return this.sourceUnitType;
		}
		
		
		public Class<TBuildParticipant> getParticipantType() {
			return this.participantType;
		}
		
	}
	
	
	private TProject ltkProject= nonNullLateInit();
	
	
	public ProjectTaskBuilder() {
	}
	
	
	public abstract BuilderDefinition<TProject, TSourceUnit, TParticipant> getBuilderDefinition();
	
	public abstract ImList<IssueTypeSet> getIssueTypeSets();
	
	
	public TProject getLtkProject() {
		return this.ltkProject;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void startupOnInitialize() {
		super.startupOnInitialize();
		
		try {
			this.ltkProject= (TProject)getProject().getNature(
					getBuilderDefinition().getProjectNatureId() );
		}
		catch (final CoreException e) {
			log(e.getStatus());
		}
	}
	
	@SuppressWarnings("unused")
	private void check(final SubMonitor progress) throws CoreException {
		if (this.ltkProject == null) {
			throw new CoreException(new Status(IStatus.ERROR, getBuilderDefinition().getBundleId(),
					NLS.bind("{0} project nature is missing.",
							getBuilderDefinition().getProjectTypeLabel() )));
		}
	}
	
	
	@Override
	protected IProject @Nullable[] build(final int kind,
			final @Nullable Map<String, @Nullable String> args,
			final @Nullable IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 1 + 20);
		try {
			check(m.newChild(1, SubMonitor.SUPPRESS_NONE));
			
			final var projectTask= createBuildTask();
			projectTask.build(kind, m.newChild(20, SubMonitor.SUPPRESS_NONE));
			
			return null;
		}
		catch (final CoreException e) {
			if (e.getStatus().getSeverity() == IStatus.CANCEL) {
				throw new OperationCanceledException();
			}
			throw e;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	protected void clean(final @Nullable IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 1 + 20);
		try {
			check(m.newChild(1, SubMonitor.SUPPRESS_NONE));
			
			final var projectTask= createCleanTask();
			projectTask.clean(m.newChild(20, SubMonitor.SUPPRESS_NONE));
		}
		catch (final CoreException e) {
			if (e.getStatus().getSeverity() == IStatus.CANCEL) {
				throw new OperationCanceledException();
			}
			throw e;
		}
		finally {
			m.done();
		}
	}
	
	
	protected abstract ProjectBuildTask<TProject, TSourceUnit, TParticipant> createBuildTask();
	
	protected abstract ProjectCleanTask<TProject, TSourceUnit, TParticipant> createCleanTask();
	
	
	protected void log(final IStatus status) {
		final var log= getBuilderDefinition().getBundle().getLog();
		if (log != null) {
			log.log(status);
		}
	}
	
}
