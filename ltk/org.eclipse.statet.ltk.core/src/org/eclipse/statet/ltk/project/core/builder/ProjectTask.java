/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.project.core.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.issues.core.impl.ResourceMarkerIssueRequestor;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.project.core.LtkProject;
import org.eclipse.statet.ltk.project.core.builder.ProjectTaskBuilder.BuilderDefinition;


@NonNullByDefault
public abstract class ProjectTask<TProject extends LtkProject,
				TSourceUnit extends WorkspaceSourceUnit,
				TParticipant extends ProjectBuildParticipant<TProject, TSourceUnit>> {
	
	
	@SuppressWarnings("rawtypes")
	private static final ProjectBuildParticipant NO_PARTICIPANT= new ProjectBuildParticipant<>();
	
	
	private final Map<String, TParticipant> participants= new HashMap<>();
	
	private final ProjectTaskBuilder<TProject, TSourceUnit, TParticipant> projectBuilder;
	private final IProject project;
	
	private int buildType;
	
	protected final MultiStatus status;
	
	
	public ProjectTask(final ProjectTaskBuilder<TProject, TSourceUnit, TParticipant> projectBuilder) {
		this.projectBuilder= projectBuilder;
		this.project= projectBuilder.getProject();
		
		this.status= new MultiStatus(getBuilderDefinition().getBundleId(), 0,
				String.format("%1$s build status for '%2$s'",
						getBuilderDefinition().getProjectTypeLabel(),
						getProject().getName() ),
				null );
	}
	
	
	protected final ProjectTaskBuilder<TProject, TSourceUnit, TParticipant> getBuilder() {
		return this.projectBuilder;
	}
	
	protected final BuilderDefinition<TProject, TSourceUnit, TParticipant> getBuilderDefinition() {
		return this.projectBuilder.getBuilderDefinition();
	}
	
	public final IProject getProject() {
		return this.project;
	}
	
	public void setBuildType(final int buildType) {
		this.buildType= buildType;
	}
	
	protected final Collection<TParticipant> getParticipants() {
		final var values= this.participants.values();
		final List<TParticipant> list= new ArrayList<>(values.size());
		for (final var participant : values) {
			if (participant != null) {
				list.add(participant);
			}
		}
		return list;
	}
	
	protected final @Nullable TParticipant getParticipant(final String modelTypeId) {
		if (modelTypeId == null) {
			return null;
		}
		@Nullable TParticipant participant= this.participants.get(modelTypeId);
		if (participant == null) {
			participant= loadParticipant(modelTypeId);
			this.participants.put(modelTypeId, participant);
		}
		return (participant != NO_PARTICIPANT) ? participant : null;
	}
	
	@SuppressWarnings("unchecked")
	private TParticipant loadParticipant(final String modelTypeId) {
		final @Nullable TParticipant participant= (TParticipant)LtkModels.getModelAdapter(
				modelTypeId, getBuilderDefinition().getParticipantType() );
		if (participant == null) {
			return (TParticipant)NO_PARTICIPANT;
		}
		participant.ltkProject= getBuilder().getLtkProject();
		participant.buildType= this.buildType;
		participant.enabled= false;
		participant.init();
		return participant;
	}
	
	
	protected void clear(final IFile file) {
		try {
			ResourceMarkerIssueRequestor.clear(file, getBuilder().getIssueTypeSets());
		}
		catch (final CoreException e) {
			this.status.add(new Status(IStatus.ERROR, getBuilderDefinition().getBundleId(),
					String.format("An error occurred when clearing issue(s) of file '%1$s'.",
							file.getProjectRelativePath() ),
					e ));
		}
	}
	
	protected void clear(final IProject project) {
		try {
			ResourceMarkerIssueRequestor.clear(project, getBuilder().getIssueTypeSets());
		}
		catch (final CoreException e) {
			this.status.add(new Status(IStatus.ERROR, getBuilderDefinition().getBundleId(),
					"An error occurred when clearing issue(s) of the project.",
					e ));
		}
	}
	
	
	protected void finish() {
		if (!this.status.isOK()) {
			getBuilder().log(this.status);
		}
	}
	
}
