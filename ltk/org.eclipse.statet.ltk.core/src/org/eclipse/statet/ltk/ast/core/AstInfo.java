/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ast.core;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.source.SourceModelStamp;


/**
 * Container for AST.
 */
@NonNullByDefault
public class AstInfo {
	
	
	public static final int DEFAULT_LEVEL_MASK=            0xf;
	
	/**
	 * AST without any text informations.
	 */
	public static final int LEVEL_MINIMAL=                 0x1;
	
	/**
	 * AST ready for model processing.
	 */
	public static final int LEVEL_MODEL_DEFAULT=           0x4;
	
	
	private final SourceModelStamp stamp;
	
	private final int level;
	
	private final AstNode root;
	
	private final ImIdentitySet<String> embeddedTypes; 
	
	
	public AstInfo(final int level, final SourceModelStamp stamp, final AstNode root,
			final ImIdentitySet<String> embeddedTypes) {
		this.level= level;
		this.stamp= stamp;
		this.root= root;
		this.embeddedTypes= embeddedTypes;
	}
	
	public AstInfo(final int level, final SourceModelStamp stamp, final AstNode root) {
		this(level, stamp, root, ImCollections.emptyIdentitySet());
	}
	
	public AstInfo(final int level, final AstInfo ast) {
		this.level= level;
		this.stamp= ast.stamp;
		this.root= ast.root;
		this.embeddedTypes= ast.getEmbeddedTypes();
	}
	
	
	/**
	 * Returns the stamp of the AST.
	 * 
	 * @return the stamp
	 */
	public SourceModelStamp getStamp() {
		return this.stamp;
	}
	
	/**
	 * Returns the level of detail of the AST.
	 * 
	 * @return
	 */
	public final int getLevel() {
		return this.level;
	}
	
	public final AstNode getRoot() {
		return this.root;
	}
	
	public final ImIdentitySet<String> getEmbeddedTypes() {
		return this.embeddedTypes;
	}
	
}
