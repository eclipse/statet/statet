/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ast.core.util;

import static org.eclipse.statet.ltk.core.StatusCodes.CTX12;
import static org.eclipse.statet.ltk.core.StatusCodes.SUBSEQUENT;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE1;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE123;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE1_RUNTIME_ERROR;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.TextLineInformation;

import org.eclipse.statet.internal.ltk.core.LtkCorePlugin;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.LtkCore;
import org.eclipse.statet.ltk.core.StatusCodes;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.impl.AbstractSourceIssueReporter;


@NonNullByDefault
public class AbstractAstProblemReporter extends AbstractSourceIssueReporter {
	
	
	protected static final int MASK= TYPE123 | CTX12 | SUBSEQUENT;
	
	
	private final boolean reportSubsequent= false;
	
	
	public AbstractAstProblemReporter(final String modelTypeId) {
		super(modelTypeId);
	}
	
	
	protected final boolean requiredCheck(final int code) {
		return (code != StatusCodes.TYPE1_OK &&
				(this.reportSubsequent || (code & SUBSEQUENT) == 0) );
	}
	
	
	protected void handleCommonCodes(final AstNode node, final int code)
			throws BadLocationException, InvocationTargetException {
		switch (code & TYPE1) {
		
		case TYPE1_RUNTIME_ERROR:
			addProblem(Problem.SEVERITY_ERROR, code,
					"Error when parsing source code. Please submit a bug report with a code snippet / log entry.", //$NON-NLS-1$
					node.getStartOffset(), node.getStartOffset() );
			return;
		
		default:
			handleUnknownCodes(node);
			return;
		}
	}
	
	protected void handleUnknownCodes(final AstNode node) {
		final int code= (node.getStatusCode() & MASK);
		final StringBuilder sb= new StringBuilder();
		sb.append("Unhandled/Unknown code of AST node (").append(getModelTypeId()).append("):"); //$NON-NLS-1$ //$NON-NLS-2$
		sb.append('\n');
		sb.append("  code= ").append(String.format("0x%1$08X", code)); //$NON-NLS-1$
		sb.append('\n');
		sb.append("  node= ").append(node);
		sb.append(" (").append(node.getStartOffset()).append(", ").append(node.getLength()).append(')'); //$NON-NLS-1$ //$NON-NLS-2$
		sb.append('\n');
		
		final var sourceContent= getSourceContent();
		if (sourceContent != null) {
			final TextLineInformation lines= sourceContent.getStringLines();
			final int line= lines.getLineOfOffset(node.getStartOffset() - sourceContent.getStartOffset());
			sb.append("  Line ").append((line + 1)); //$NON-NLS-1$
			sb.append('\n');
			
			final int firstLine= Math.max(0, line - 2);
			final int lastLine= Math.min(lines.getNumberOfLines() - 1,
					lines.getLineOfOffset(node.getEndOffset() - sourceContent.getStartOffset()) + 2 );
			sb.append("  source (line ").append(firstLine + 1).append('-').append(lastLine + 1).append(")= \n"); //$NON-NLS-1$ //$NON-NLS-2$
			sb.append(sourceContent.getString(),
					lines.getStartOffset(firstLine), lines.getEndOffset(lastLine) );
		}
		LtkCorePlugin.log(new Status(IStatus.WARNING, LtkCore.BUNDLE_ID, sb.toString()));
	}
	
	
}
