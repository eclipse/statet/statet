/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ast.core;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.model.core.AttachmentsElement;
import org.eclipse.statet.jcommons.text.core.TextRegion;


/**
 * AST node.
 * <p>
 * The interface must be implemented by the language specific AST classes.</p>
 */
@NonNullByDefault
public interface AstNode extends TextRegion, AttachmentsElement {
	
	
	int NA_OFFSET= Integer.MIN_VALUE;
	
	
	int getStatusCode();
	
	/**
	 * {@inheritDoc}
	 * 
	 * <p>Obligatory offset property: {@link #NA_OFFSET} not supported.</p>
	 */
	@Override
	int getStartOffset();
	
	/**
	 * {@inheritDoc}
	 * 
	 * <p>Obligatory offset property: {@link #NA_OFFSET} not supported.</p>
	 */
	@Override
	int getEndOffset();
	
	@Override
	int getLength();
	
	
	/**
	 * Returns the text of the node.
	 * 
	 * <p>The meaning of the text depends on node type, for a string literal it is usually the
	 * final string value (decoded escape sequences, without quotes).</p>
	 * 
	 * @return the text or <code>null</code>
	 */
	default @Nullable String getText() {
		return null;
	}
	
	/**
	 * Returns the region of the {@link #getText() text} of the node.
	 * 
	 * @return the text region or <code>null</code>
	 */
	default @Nullable TextRegion getTextRegion() {
		return null;
	}
	
	
	void accept(final AstVisitor visitor) throws InvocationTargetException;
	void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException;
	
	@Nullable AstNode getParent();
	boolean hasChildren();
	int getChildCount();
	AstNode getChild(final int index);
	int getChildIndex(final AstNode child);
	
}
