/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ast.core.impl;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.SEVERITY_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.WARNING;
import static org.eclipse.statet.ltk.core.StatusCodes.WARNING_IN_CHILD;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public abstract class AbstractAstNode implements AstNode {
	
	
	private static final ImList<Object> NO_ATTACHMENT= ImCollections.emptyList();
	
	
	private int statusCode;
	
	private int startOffset;
	private int endOffset;
	
	private volatile ImList<Object> attachments= NO_ATTACHMENT;
	
	
	protected AbstractAstNode(final int statusCode,
			final int startOffset, final int endOffset) {
		this.statusCode= statusCode;
		
		this.startOffset= startOffset;
		this.endOffset= endOffset;
	}
	
	protected AbstractAstNode(final int statusCode) {
		this.statusCode= statusCode;
	}
	
	protected AbstractAstNode() {
	}
	
	
	@Override
	public final int getStatusCode() {
		return this.statusCode;
	}
	
	
	@Override
	public final void accept(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final int getStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public final int getEndOffset() {
		return this.endOffset;
	}
	
	@Override
	public final int getLength() {
		return this.endOffset - this.startOffset;
	}
	
	
	protected final void doSetStatusCode(final int statusCode) {
		this.statusCode= statusCode;
	}
	
	protected final void doSetStatusThis(final int statusCode) {
		this.statusCode= (this.statusCode & SEVERITY_IN_CHILD) | statusCode;
	}
	
	protected final void doAddStatusFlag(final int statusFlag) {
		this.statusCode|= statusFlag;
	}
	
	protected final void doClearStatusSeverityInChild() {
		this.statusCode= (this.statusCode & ~SEVERITY_IN_CHILD);
	}
	
	protected final void doSetStatusSeverityInChild(final int statusCode) {
		this.statusCode= (this.statusCode & ~SEVERITY_IN_CHILD) | statusCode;
	}
	
	protected final void doSetStatusSeverityOfChild(final int childStatusCode) {
		if ((childStatusCode & (ERROR | ERROR_IN_CHILD)) != 0) {
			this.statusCode= (this.statusCode & ~SEVERITY_IN_CHILD) | ERROR_IN_CHILD;
		}
		else if ((childStatusCode & (WARNING | WARNING_IN_CHILD)) != 0) {
			this.statusCode= (this.statusCode & ~SEVERITY_IN_CHILD) | WARNING_IN_CHILD;
		}
		else {
			this.statusCode= (this.statusCode & ~SEVERITY_IN_CHILD);
		}
	}
	
	protected final void doAddStatusSeverityOfChild(final int childStatusCode) {
		final int statusCode= this.statusCode;
		if ((statusCode & ERROR_IN_CHILD) != 0) {
			return;
		}
		if ((childStatusCode & (ERROR | ERROR_IN_CHILD)) != 0) {
			this.statusCode= (statusCode & ~SEVERITY_IN_CHILD) | ERROR_IN_CHILD;
		}
		else if ((childStatusCode & (WARNING | WARNING_IN_CHILD)) != 0) {
			this.statusCode= (statusCode & ~SEVERITY_IN_CHILD) | WARNING_IN_CHILD;
		}
	}
	
	protected final void doSetStartOffset(final int offset) {
		this.startOffset= offset;
	}
	
	protected final void doSetEndOffset(final int offset) {
		this.endOffset= offset;
	}
	
	protected final void doSetEndOffsetMin(final int offset) {
		if (offset > this.endOffset) {
			this.endOffset= offset;
		}
	}
	
	protected final void doSetStartEndOffset(final int startOffset, final int endOffset) {
		this.startOffset= startOffset;
		this.endOffset= endOffset;
	}
	
	protected final void doSetStartEndOffset(final int offset) {
		this.startOffset= offset;
		this.endOffset= offset;
	}
	
	
	@Override
	public synchronized void addAttachment(final Object data) {
		this.attachments= ImCollections.addElement(this.attachments, data);
	}
	
	@Override
	public synchronized void removeAttachment(final Object data) {
		this.attachments= ImCollections.removeElement(this.attachments, data);
	}
	
	@Override
	public ImList<Object> getAttachments() {
		return this.attachments;
	}
	
	
}
