/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.issues.core.Issue;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet.IssueCategory;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet.ProblemCategory;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet.TaskCategory;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.Task;


@NonNullByDefault
public abstract class BasicIssueRequestor implements IssueRequestor {
	
	private static final int STATE_OPEN= 1;
	private static final int STATE_FINISHING= 2;
	private static final int STATE_FINISHED= 3;
	
	
	public static class IssueBatch<TIssue extends Issue, TCategory extends IssueCategory<TIssue>> {
		
		
		private final TCategory category;
		
		final boolean isEnabled;
		
		final List<TIssue> acceptedIssues= new ArrayList<>();
		
		
		public IssueBatch(final TCategory category, final boolean isEnabled) {
			this.category= category;
			this.isEnabled= isEnabled;
		}
		
		
		public final TCategory getCategory() {
			return this.category;
		}
		
		public final boolean isEnabled() {
			return true;
		}
		
		public List<TIssue> getAcceptedIssues() {
			return this.acceptedIssues;
		}
		
	}
	
	public final static class TaskBatch extends IssueBatch<Task, TaskCategory> {
		
		
		public TaskBatch(final TaskCategory category, final boolean isEnabled) {
			super(category, isEnabled);
		}
		
		
	}
	
	public final static class ProblemBatch extends IssueBatch<Problem, ProblemCategory> {
		
		
		private final String id;
		
		
		public ProblemBatch(final ProblemCategory category, final boolean isEnabled) {
			super(category, isEnabled);
			this.id= category.getId();
		}
		
		
		public String getId() {
			return this.id;
		}
		
	}
	
	
	private final IssueTypeSet issueTypeSet;
	
	private final @Nullable TaskBatch taskCollector;
	private final ImList<ProblemBatch> problemCollectors;
	
	private int state= 1;
	
	
	public BasicIssueRequestor(final IssueTypeSet issueTypeSet,
			final WorkingContext workingContext) {
		this.issueTypeSet= issueTypeSet;
		
		this.taskCollector= createTaskCollector(issueTypeSet.getTaskCategory(),
				workingContext );
		this.problemCollectors= createProblemCollectors(issueTypeSet.getProblemCategories(),
				workingContext );
	}
	
	
	protected @Nullable TaskBatch createTaskCollector(final @Nullable TaskCategory category,
			final WorkingContext requiredContext) {
		if (category != null && category.getType(requiredContext) != null) {
			return new TaskBatch(category, shouldAccept(category));
		}
		return null;
	}
	
	protected boolean shouldAccept(final TaskCategory category) {
		return true;
	}
	
	protected ImList<ProblemBatch> createProblemCollectors(final ImList<ProblemCategory> categories,
			final WorkingContext requiredContext) {
		final var list= new ArrayList<ProblemBatch>(categories.size());
		for (final var category : categories) {
			if (category.getTypes(requiredContext) != null) {
				list.add(new ProblemBatch(category, shouldAccept(category)));
			}
		}
		return ImCollections.toList(list);
	}
	
	protected boolean shouldAccept(final ProblemCategory category) {
		return true;
	}
	
	
	public IssueTypeSet getIssueTypeSet() {
		return this.issueTypeSet;
	}
	
	protected final @Nullable TaskBatch getTaskCollector() {
		return this.taskCollector;
	}
	
	protected final ImList<ProblemBatch> getProblemCollector() {
		return this.problemCollectors;
	}
	
	protected final @Nullable ProblemBatch getProblemCollector(final String id) {
		for (final var collector : this.problemCollectors) {
			if (collector.id == id) {
				return collector;
			}
		}
		return null;
	}
	
	
	@Override
	public boolean isInterestedInTasks() {
		final var collector= getTaskCollector();
		return (collector != null && collector.isEnabled);
	}
	
	@Override
	public void acceptTask(final Task task) {
		final var collector= getTaskCollector();
		if (collector != null && collector.isEnabled) {
			collector.acceptedIssues.add(task);
		}
	}
	
	
	@Override
	public boolean isInterestedInProblems(final String id) {
		final var collector= getProblemCollector(id);
		return (collector != null && collector.isEnabled);
	}
	
	@Override
	public void acceptProblems(final Problem problem) {
		final var collector= getProblemCollector(problem.getCategoryId());
		if (collector != null && collector.isEnabled) {
			collector.acceptedIssues.add(problem);
		}
	}
	
	@Override
	public void acceptProblems(final String id, final List<Problem> problems) {
		final var collector= getProblemCollector(id);
		if (collector != null && collector.isEnabled) {
			collector.acceptedIssues.addAll(problems);
		}
	}
	
	
	@Override
	public void finish() throws StatusException {
		if (this.state != STATE_OPEN) {
			throw new IllegalStateException("Already finished");
		}
		this.state= STATE_FINISHING;
		
		try {
			reportIssues(this.taskCollector, this.problemCollectors);
		}
		catch (final CoreException e) {
			throw EStatusUtils.convert(e);
		}
		finally {
			this.state= STATE_FINISHED;
		}
	}
	
	
	protected abstract void reportIssues(final @Nullable TaskBatch taskBatch,
			final ImList<ProblemBatch> problemBatchs)
			throws CoreException;
	
	
}
