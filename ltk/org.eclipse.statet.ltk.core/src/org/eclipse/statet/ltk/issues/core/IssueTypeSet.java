/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.WorkingContext;


@NonNullByDefault
public class IssueTypeSet {
	
	
	public static interface IssueCategory<TIssue extends Issue> {
		
		
		@Nullable String mapType(WorkingContext fromContext, WorkingContext toContext,
				String type);
		
	}
	
	public static class TaskCategory implements IssueCategory<Task> {
		
		private final @Nullable String persistenceType;
		private final @Nullable String editorType;
		
		public TaskCategory(final @Nullable String persistenceType, final @Nullable String editorType) {
			this.persistenceType= persistenceType;
			this.editorType= editorType;
		}
		
		public @Nullable String getType(final WorkingContext context) {
			if (context == Ltk.PERSISTENCE_CONTEXT) {
				return this.persistenceType;
			}
			else if (context == Ltk.EDITOR_CONTEXT) {
				return this.editorType;
			}
			return null;
		}
		
		@Override
		public @Nullable String mapType(final WorkingContext fromContext, final WorkingContext toContext,
				final String type) {
			final String taskType= getType(fromContext);
			if (taskType != null && taskType == type) {
				return getType(toContext);
			}
			return null;
		}
		
	}
	
	public final static class ProblemTypes {
		
		private final String errorType;
		private final String warningType;
		private final String infoType;
		
		public ProblemTypes(final String errorType, final String warningType, final String infoType) {
			this.errorType= errorType;
			this.warningType= warningType;
			this.infoType= infoType;
		}
		
		public ProblemTypes(final String type) {
			this.errorType= type;
			this.warningType= type;
			this.infoType= type;
		}
		
		public String getType(final int severity) {
			switch (severity) {
			case Problem.SEVERITY_ERROR:
				return this.errorType;
			case Problem.SEVERITY_WARNING:
				return this.warningType;
			default:
				return this.infoType;
			}
		}
		
		public byte getSeverity(final String type) {
			if (type == this.errorType) {
				return Problem.SEVERITY_ERROR;
			}
			else if (type == this.warningType) {
				return Problem.SEVERITY_WARNING;
			}
			else if (type == this.infoType) {
				return Problem.SEVERITY_INFO;
			}
			return -1;
		}
		
	}
	
	public static class ProblemCategory implements IssueCategory<Problem> {
		
		private final String id;
		
		private final @Nullable ProblemTypes persistenceTypes;
		private final @Nullable ProblemTypes editorTypes;
		
		public ProblemCategory(final String id,
				final @Nullable ProblemTypes persistenceTypes, final @Nullable ProblemTypes editorTypes) {
			this.id= id;
			this.persistenceTypes= persistenceTypes;
			this.editorTypes= editorTypes;
		}
		
		public String getId() {
			return this.id;
		}
		
		public @Nullable ProblemTypes getTypes(final WorkingContext context) {
			if (context == Ltk.PERSISTENCE_CONTEXT) {
				return this.persistenceTypes;
			}
			else if (context == Ltk.EDITOR_CONTEXT) {
				return this.editorTypes;
			}
			return null;
		}
		
		@Override
		public @Nullable String mapType(final WorkingContext fromContext, final WorkingContext toContext,
				final String type) {
			ProblemTypes problemTypes= getTypes(fromContext);
			if (problemTypes != null) {
				final byte severity= problemTypes.getSeverity(type);
				if (severity != -1) {
					problemTypes= getTypes(toContext);
					if (problemTypes != null) {
						return problemTypes.getType(severity);
					}
				}
			}
			return null;
		}
		
	}
	
	
	private final String sourceId;
	
	private final TaskCategory taskCategory;
	
	private final ImList<ProblemCategory> problemCategories;
	
	private final ImIdentitySet<String> persistenceTypes;
	private final ImIdentitySet<String> editorTypes;
	
	
	public IssueTypeSet(final String sourceId,
			final TaskCategory taskCategory, final ImList<ProblemCategory> problemCategories) {
		this.sourceId= sourceId;
		this.taskCategory= taskCategory;
		this.problemCategories= problemCategories;
		
		{	final var list= new ArrayList<String>();
			collectAllTypes(list, Ltk.PERSISTENCE_CONTEXT);
			this.persistenceTypes= ImCollections.toIdentitySet(list);
			list.clear();
			collectAllTypes(list, Ltk.EDITOR_CONTEXT);
			this.editorTypes= ImCollections.toIdentitySet(list);
		}
	}
	
	public IssueTypeSet(final String sourceId, final IssueTypeSet baseSet,
			final @NonNull ProblemCategory... addProblemCategories) {
		this(sourceId, baseSet.taskCategory, ImCollections.concatList(
				baseSet.problemCategories, ImCollections.newList(addProblemCategories) ));
	}
	
	
	public String getSourceId() {
		return this.sourceId;
	}
	
	public TaskCategory getTaskCategory() {
		return this.taskCategory;
	}
	
	public ImList<ProblemCategory> getProblemCategories() {
		return this.problemCategories;
	}
	
	public @Nullable ProblemCategory getProblemCategory(final String categoryId) {
		for (final var category : this.problemCategories) {
			if (category.id == categoryId) {
				return category;
			}
		}
		return null;
	}
	
	public @Nullable IssueCategory<?> getCategory(final WorkingContext context,
			final String type) {
		{	final var allTypes= getAllTypes(context);
			if (allTypes == null || !allTypes.contains(type)) {
				return null;
			}
		}
		
		{	final var category= getTaskCategory();
			if (type.equals(category.getType(context))) {
				return category;
			}
		}
		for (final var category : getProblemCategories()) {
			final ProblemTypes problemTypes= category.getTypes(context);
			if (problemTypes != null
					&& (type.equals(problemTypes.errorType)
							|| type.equals(problemTypes.warningType)
							|| type.equals(problemTypes.infoType) )) {
				return category;
			}
		}
		return null;
	}
	
	
	private void collectAllTypes(final Collection<String> types, final WorkingContext context) {
		{	final String type= getTaskCategory().getType(context);
			if (type != null) {
				types.add(type);
			}
		}
		for (final var category : getProblemCategories()) {
			final ProblemTypes problemTypes= category.getTypes(context);
			if (problemTypes != null) {
				types.add(problemTypes.errorType);
				types.add(problemTypes.warningType);
				types.add(problemTypes.infoType);
			}
		}
	}
	
	public @Nullable ImIdentitySet<String> getAllTypes(final WorkingContext context) {
		if (context == Ltk.PERSISTENCE_CONTEXT) {
			return this.persistenceTypes;
		}
		else if (context == Ltk.EDITOR_CONTEXT) {
			return this.editorTypes;
		}
		return null;
	}
	
}
