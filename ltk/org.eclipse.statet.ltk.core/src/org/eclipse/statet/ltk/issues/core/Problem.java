/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core;

import org.eclipse.core.resources.IMarker;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Problem
 */
@NonNullByDefault
public interface Problem extends Issue {
	
	
	/**
	 * Error severity constant indicating an error.
	 * 
	 * {@link IMarker#SEVERITY_ERROR}
	 */
	static final byte SEVERITY_ERROR= 2;
	
	/**
	 * Error severity constant indicating a warning.
	 * 
	 * {@link IMarker#SEVERITY_WARNING}
	 */
	static final byte SEVERITY_WARNING= 1;
	
	/**
	 * Error severity constant indicating an information only.
	 * 
	 * {@link IMarker#SEVERITY_INFO}
	 */
	static final byte SEVERITY_INFO= 0;
	
	
	String getCategoryId();
	
	int getSeverity();
	int getCode();
	
}
