/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.ltk.issues.core.Task;
import org.eclipse.statet.ltk.issues.core.TaskPriority;


/**
 * Default implementation of {@link Task}.
 */
@NonNullByDefault
public class BasicTask extends BasicSourceIssue implements Task {
	
	
	private final TaskPriority priority;
	
	private final String message;
	
	
	public BasicTask(final TaskPriority priority, final String message,
			final int line, final int startOffset, final int endOffset) {
		super(line, startOffset, endOffset);
		this.priority= priority;
		this.message= message;
	}
	
	
	@Override
	public TaskPriority getPriority() {
		return this.priority;
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
	
	
	@Override
	public String toString() {
		final var sb= new ToStringBuilder("Task", getClass()); //$NON-NLS-1$
		sb.addProp("sourceOffset", '[', getSourceStartOffset(), getSourceEndOffset(), ')'); //$NON-NLS-1$
		sb.addProp("sourceLine", getSourceLine()); //$NON-NLS-1$
		sb.addProp("priority", getPriority()); //$NON-NLS-1$
		sb.addProp("message", getMessage()); //$NON-NLS-1$
		return sb.toString();
	}
	
}
