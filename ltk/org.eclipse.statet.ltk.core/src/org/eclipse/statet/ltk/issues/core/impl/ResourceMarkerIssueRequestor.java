/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.issues.core.IssueMarkers;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet.ProblemTypes;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.Task;


@NonNullByDefault
public class ResourceMarkerIssueRequestor extends BasicIssueRequestor {
	
	
	public static @Nullable ResourceMarkerIssueRequestor create(final @Nullable Object resource,
			final IssueTypeSet issueTypeSet) {
		if (resource instanceof IFile) {
			return new ResourceMarkerIssueRequestor((IFile)resource, issueTypeSet);
		}
		return null;
	}
	
	public static void clear(final @Nullable Object resource,
			final ImList<IssueTypeSet> issueTypeSets) throws CoreException {
		if (resource instanceof final IResource wsResource) {
			for (final var issueTypeSet : issueTypeSets) {
				final var allTypes= issueTypeSet.getAllTypes(Ltk.PERSISTENCE_CONTEXT);
				if (allTypes != null) {
					for (final var type : allTypes) {
						wsResource.deleteMarkers(type, true, IResource.DEPTH_INFINITE);
					}
				}
			}
		}
	}
	
	public static void clear(final @Nullable Object resource,
			final IssueTypeSet issueTypeSet) throws CoreException {
		clear(resource, ImCollections.newList(issueTypeSet));
	}
	
	
	private final IFile resource;
	
	
	public ResourceMarkerIssueRequestor(final IFile file, final IssueTypeSet issueTypeSet) {
		super(issueTypeSet, Ltk.PERSISTENCE_CONTEXT);
		
		this.resource= file;
	}
	
	
	@Override
	protected void reportIssues(final @Nullable TaskBatch taskBatch,
			final ImList<ProblemBatch> problemBatches)
			throws CoreException {
		if (taskBatch != null) {
			final String type= nonNullAssert(
					taskBatch.getCategory().getType(Ltk.PERSISTENCE_CONTEXT) );
			if (taskBatch.isEnabled()) {
				for (final var task : taskBatch.getAcceptedIssues()) {
					final var attributes= createTaskAttributes(task);
					this.resource.createMarker(type, attributes);
				}
			}
		}
		for (final var problemBatch : problemBatches) {
			final ProblemTypes types= nonNullAssert(
					problemBatch.getCategory().getTypes(Ltk.PERSISTENCE_CONTEXT) );
			if (problemBatch.isEnabled()) {
				for (final var problem : problemBatch.getAcceptedIssues()) {
					final var attributes= createProblemAttributes(problem);
					this.resource.createMarker(types.getType(problem.getSeverity()), attributes);
				}
			}
		}
	}
	
	protected Map<String, Object> createProblemAttributes(final Problem problem) {
		final var attributes= new HashMap<String, Object>(10);
		attributes.put(IMarker.SOURCE_ID, getIssueTypeSet().getSourceId());
		attributes.put(IssueMarkers.CATEGORY_ID_ATTR_NAME, problem.getCategoryId());
		attributes.put(IMarker.SEVERITY, IssueMarkers.toMarkerSeverity(problem.getSeverity()));
		attributes.put(IssueMarkers.CODE_ATTR_NAME, problem.getCode());
		attributes.put(IMarker.MESSAGE, problem.getMessage());
		
		if (problem.getSourceLine() >= 0) {
			attributes.put(IMarker.LINE_NUMBER, problem.getSourceLine() + 1);
		}
		if (problem.getSourceStartOffset() >= 0) {
			attributes.put(IMarker.CHAR_START, problem.getSourceStartOffset());
			attributes.put(IMarker.CHAR_END, problem.getSourceEndOffset());
		}
		attributes.put(IMarker.USER_EDITABLE, false);
		
		return attributes;
	}
	
	protected Map<String, Object> createTaskAttributes(final Task task) {
		final var attributes= new HashMap<String, Object>(10);
		attributes.put(IMarker.SOURCE_ID, getIssueTypeSet().getSourceId());
		attributes.put(IMarker.PRIORITY, IssueMarkers.toMarkerPriority(task.getPriority()));
		attributes.put(IMarker.MESSAGE, task.getMessage());
		
		if (task.getSourceLine() >= 0) {
			attributes.put(IMarker.LINE_NUMBER, task.getSourceLine() + 1);
		}
		if (task.getSourceStartOffset() >= 0) {
			attributes.put(IMarker.CHAR_START, task.getSourceStartOffset());
			attributes.put(IMarker.CHAR_END, task.getSourceEndOffset());
		}
		attributes.put(IMarker.USER_EDITABLE, false);
		
		return attributes;
	}
	
	
}
