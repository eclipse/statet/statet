/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.runtime.core.util.MessageBuilder;

import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.core.util.SourceMessageUtil;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;


@NonNullByDefault
public class AbstractSourceIssueReporter {
	
	
	private final String modelTypeId;
	
	private SourceContent sourceContent= nonNullLateInit();
	private IssueRequestor requestor= nonNullLateInit();
	
	private final SourceMessageUtil messageUtil= new SourceMessageUtil();
	private final MessageBuilder messageBuilder= new MessageBuilder();
	private final List<Problem> problemBuffer= new ArrayList<>(100);
	
	
	public AbstractSourceIssueReporter(final String modelTypeId) {
		this.modelTypeId= modelTypeId;
	}
	
	
	public final String getModelTypeId() {
		return this.modelTypeId;
	}
	
	protected SourceContent getSourceContent() {
		return this.sourceContent;
	}
	
	
	protected void init(final SourceContent content, final IssueRequestor requestor) {
		this.sourceContent= nonNullAssert(content);
		this.messageUtil.setSourceContent(content);
		this.requestor= nonNullAssert(requestor);
	}
	
	protected void flush() {
		if (!this.problemBuffer.isEmpty()) {
			this.requestor.acceptProblems(this.modelTypeId, this.problemBuffer);
		}
	}
	
	@SuppressWarnings("null")
	protected void clear() {
		this.problemBuffer.clear();
		this.sourceContent= null;
		this.requestor= null;
	}
	
	
	protected final SourceMessageUtil getMessageUtil() {
		return this.messageUtil;
	}
	
	protected final MessageBuilder getMessageBuilder() {
		return this.messageBuilder;
	}
	
	
	protected final void addProblem(final int severity, final int code, final String message,
			int startOffset, int endOffset) {
		if (startOffset < this.sourceContent.getStartOffset()) {
			startOffset= this.sourceContent.getStartOffset();
			if (endOffset < startOffset) {
				endOffset= startOffset;
			}
		}
		else {
			if (endOffset <= startOffset) {
				endOffset= startOffset + 1;
			}
		}
		if (endOffset > this.sourceContent.getEndOffset()) {
			endOffset= this.sourceContent.getEndOffset();
		}
		
		final int line= this.sourceContent.getStringLines().getLineOfOffset(startOffset);
		this.problemBuffer.add(new BasicProblem(this.modelTypeId, severity, code, message,
				line, startOffset, endOffset ));
		
		if (this.problemBuffer.size() >= 100) {
			this.requestor.acceptProblems(this.modelTypeId, this.problemBuffer);
			this.problemBuffer.clear();
		}
	}
	
	protected final void addProblem(final int severity, final int code, final String message) {
		this.problemBuffer.add(new BasicProblem(this.modelTypeId, severity, code, message));
		
		if (this.problemBuffer.size() >= 100) {
			this.requestor.acceptProblems(this.modelTypeId, this.problemBuffer);
			this.problemBuffer.clear();
		}
	}
	
}
