/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.LtkCore;


@NonNullByDefault
public interface Issue {
	
	
	/**
	 * Returns the line number in the sources.
	 * 
	 * @return the 0-based line number or {@code -1} if not applicable.
	 */
	int getSourceLine();
	
	/**
	 * Returns the start offset in the sources.
	 * 
	 * @return the start offset, or {@link LtkCore#NA_OFFSET} if not applicable.
	 */
	int getSourceStartOffset();
	
	/**
	 * Returns the end offset in the sources.
	 * 
	 * @return the end offset, exclusive, or {@link LtkCore#NA_OFFSET} if not applicable.
	 */
	int getSourceEndOffset();
	
	
	String getMessage();
	
}
