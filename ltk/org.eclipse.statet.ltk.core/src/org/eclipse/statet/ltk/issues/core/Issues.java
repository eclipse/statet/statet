/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.EnumListPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.StringListPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceStore;

import org.eclipse.statet.ltk.issues.core.impl.BasicTaskTag;


@NonNullByDefault
public final class Issues {
	
	
	static final String TASK_TAG_QUALIFIER= "org.eclipse.statet.ide.core/managment"; //$NON-NLS-1$
	
	public static final String TASK_TAG_KEYWORD_PREF_KEY= "TaskTags.keyword"; //$NON-NLS-1$
	public static final String TASK_TAG_PRIORITY_PREF_KEY= "TaskTags.priority"; //$NON-NLS-1$
	
	public static final Preference<List<String>> TASK_TAG_KEYWORD_PREF=
			new StringListPref(TASK_TAG_QUALIFIER, TASK_TAG_KEYWORD_PREF_KEY);
	public static final Preference<List<TaskPriority>> TASK_TAG_PRIORITY_PREF=
			new EnumListPref<>(TASK_TAG_QUALIFIER, TASK_TAG_PRIORITY_PREF_KEY, TaskPriority.class);
	
	
	public static ImList<TaskTag> loadTaskTags(final PreferenceAccess prefs,
			final Preference<List<String>> keywordPerf,
			final Preference<List<TaskPriority>> priorityPref) {
		final List<String> keywords= prefs.getPreferenceValue(keywordPerf);
		final List<TaskPriority> priorities= prefs.getPreferenceValue(priorityPref);
		
		if (keywords.size() == priorities.size()) {
			final var array= new @NonNull TaskTag[keywords.size()];
			for (int i= 0; i < array.length; i++) {
				array[i]= new BasicTaskTag(keywords.get(i), priorities.get(i));
			}
			return ImCollections.newList(array);
		}
		else {
			return ImCollections.emptyList();
		}
	}
	
	@Deprecated
	public static ImList<TaskTag> loadTaskTags(final PreferenceAccess prefs) {
		return loadTaskTags(prefs, TASK_TAG_KEYWORD_PREF, TASK_TAG_PRIORITY_PREF);
	}
	
	public static void saveTaskTags(final ImList<TaskTag> taskTags, final PreferenceStore prefs,
			final Preference<List<String>> keywordPerf,
			final Preference<List<TaskPriority>> priorityPref) {
		prefs.setPreferenceValue(keywordPerf, taskTags.map(TaskTag::getKeyword).toList());
		prefs.setPreferenceValue(priorityPref, taskTags.map(TaskTag::getPriority).toList());
	}
	
}
