/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.issues.core.Issue;


@NonNullByDefault
public abstract class BasicSourceIssue implements Issue {
	
	
	private final int line;
	private final int startOffset;
	private final int endOffset;
	
	
	public BasicSourceIssue(final int line, final int startOffset, final int endOffset) {
		if (startOffset > endOffset) {
			throw new IllegalArgumentException("startOffset > endOffset: startOffset= " + startOffset + ", endOffset= " + endOffset); //$NON-NLS-1$ //$NON-NLS-2$
		}
		this.line= line;
		this.startOffset= startOffset;
		this.endOffset= endOffset;
	}
	
	
	@Override
	public int getSourceLine() {
		return this.line;
	}
	
	@Override
	public int getSourceStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public int getSourceEndOffset() {
		return this.endOffset;
	}
	
	
}
