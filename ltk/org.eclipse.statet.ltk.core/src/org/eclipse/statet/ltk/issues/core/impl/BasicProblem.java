/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import static org.eclipse.statet.ltk.core.LtkCore.NA_OFFSET;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.ltk.issues.core.Problem;


/**
 * Default implementation of {@link Problem}.
 */
@NonNullByDefault
public class BasicProblem extends BasicSourceIssue implements Problem {
	
	
	private final String categoryId;
	
	private final int severity;
	private final int code;
	private final String message;
	
	
	public BasicProblem(final String categoryId,
			final int severity, final int code, final String message,
			final int line, final int startOffset, final int endOffset) {
		super(line, startOffset, endOffset);
		this.categoryId= categoryId;
		this.severity= severity;
		this.code= code;
		this.message= message;
	}
	
	public BasicProblem(final String categoryId,
			final int severity, final int code, final String message) {
		super(-1, NA_OFFSET, NA_OFFSET);
		this.categoryId= categoryId;
		this.severity= severity;
		this.code= code;
		this.message= message;
	}
	
	
	@Override
	public String getCategoryId() {
		return this.categoryId;
	}
	
	@Override
	public int getSeverity() {
		return this.severity;
	}
	
	@Override
	public int getCode() {
		return this.code;
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
	
	
	@Override
	public String toString() {
		final var sb= new ToStringBuilder("Problem", getClass()); //$NON-NLS-1$
		sb.addProp("sourceOffset", '[', getSourceStartOffset(), getSourceEndOffset(), ')'); //$NON-NLS-1$
		sb.addProp("sourceLine", getSourceLine()); //$NON-NLS-1$
		sb.addProp("categoryId", getCategoryId()); //$NON-NLS-1$
		sb.addProp("severity", getSeverity()); //$NON-NLS-1$
		sb.addProp("code", getCode()); //$NON-NLS-1$
		sb.addProp("message", getMessage()); //$NON-NLS-1$
		return sb.toString();
	}
	
}
