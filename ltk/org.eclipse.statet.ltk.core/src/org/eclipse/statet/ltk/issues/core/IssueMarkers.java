/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core;

import org.eclipse.core.resources.IMarker;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Resource marker support for issues.
 */
@NonNullByDefault
public class IssueMarkers {
	
	
	/**
	 * Attribute name for the id of the (problem) category.
	 */
	public static final String CATEGORY_ID_ATTR_NAME= "categoryId"; //$NON-NLS-1$
	
	/**
	 * Attribute name for the (problem) code.
	 */
	public static final String CODE_ATTR_NAME= "code"; //$NON-NLS-1$
	
	
	public static final int toMarkerPriority(final TaskPriority priority) {
		switch (priority) {
		case HIGH:
			return IMarker.PRIORITY_HIGH;
		case LOW:
			return IMarker.PRIORITY_LOW;
		default:
			return IMarker.PRIORITY_NORMAL;
		}
	}
	
	public static final int toMarkerSeverity(final int problemSeverity) {
		return problemSeverity;
	}
	
	
	private IssueMarkers() {
	}
	
}
