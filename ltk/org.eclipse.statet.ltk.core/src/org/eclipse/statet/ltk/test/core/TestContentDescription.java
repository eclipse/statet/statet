/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.test.core;

import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class TestContentDescription implements IContentDescription {
	
	
	private @Nullable String charsetString;
	
	
	public TestContentDescription() {
	}
	
	
	@Override
	public @Nullable IContentType getContentType() {
		return null;
	}
	
	@Override
	public @Nullable Object getProperty(final QualifiedName key) {
		if (key == IContentDescription.CHARSET) {
			return this.charsetString;
		}
		return null;
	}
	
	@Override
	public boolean isRequested(final QualifiedName key) {
		return (key == IContentDescription.CHARSET);
	}
	
	@Override
	public void setProperty(final QualifiedName key, final @Nullable Object value) {
		if (key == IContentDescription.CHARSET) {
			this.charsetString= (String)value;
		}
	}
	
	@Override
	public @Nullable String getCharset() {
		return this.charsetString;
	}
	
}
