/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.text.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.ROUND_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.text.TypedRegion;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CharPair;
import org.eclipse.statet.jcommons.text.core.CharPairSet;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;
import org.eclipse.statet.ecommons.text.core.TextTokenScanner;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;


/**
 * Utility methods for heuristic based R manipulations in an incomplete source file.
 * 
 * <p>
 * {@code SSpace} = "singleline white space" = white space characters within a line<br/>
 * {@code MSpace} = "multiline white space" = white space characters including line delemiter</br>
 * </p>
 * <p>An instance holds some internal position in the document and is therefore not threadsafe.</p>
 * 
 * @since 0.2
 */
@NonNullByDefault
public class HeuristicTokenScanner implements TextTokenScanner {
	
	
	/**
	 * Specifies the stop condition, upon which the <code>scan...</code> methods will decide whether
	 * to keep scanning or not. This interface may implemented by clients.
	 */
	protected static interface StopCondition {
		
		/**
		 * Instructs the scanner to return the current position.
		 * 
		 * @return <code>true</code> if the stop condition is met.
		 * @throws BadLocationException 
		 */
		public boolean matches(char c) throws BadLocationException;
		
	}
	
	
	protected static final PartitionConstraint ANY_PARTITIONS_CONSTRAINT= new PartitionConstraint() {
		
		@Override
		public boolean matches(final String contentType) {
			return true;
		}
		
	};
	
	protected static final CharPairSet COMMON_BRACKETS= new CharPairSet(
			ImCollections.newIdentityList(CURLY_BRACKETS, SQUARE_BRACKETS, ROUND_BRACKETS) );
	
	protected static final StopCondition COMMON_WORD_CONDITION= new StopCondition() {
				@Override
				public boolean matches(final char c) {
					return (!Character.isLetterOrDigit(c));
				}
			};
	
	
	protected static class SingleCharacterMatchCondition implements StopCondition {
		
		protected final int singleChar;
		
		public SingleCharacterMatchCondition(final char ch) {
			this.singleChar= ch;
		}
		
		@Override
		public boolean matches(final char c) {
			return (c == this.singleChar);
		}
		
	}
	
	protected static class CharacterMatchCondition implements StopCondition {
		
		protected final char[] chars;
		
		public CharacterMatchCondition(final char[] chars) {
			assert (chars != null);
			this.chars= chars;
		}
		
		@Override
		public boolean matches(final char c) {
			for (int i= 0; i < this.chars.length; i++) {
				if (c == this.chars[i]) {
					return true;
				}
			}
			return false;
		}
		
	}
	
	protected class StringMatchCondition implements StopCondition {
		
		protected final ImList<String> strings;
		
		public StringMatchCondition(final ImList<String> strings) {
			this.strings= strings;
		}
		
		@Override
		public boolean matches(final char c) {
			try {
				for (final String string : this.strings) {
					if (c == string.charAt(0) && string.regionMatches(1, HeuristicTokenScanner.this.document.get(
							HeuristicTokenScanner.this.offset + 1, string.length() - 1),
							0, string.length() - 1 )) {
						return true;
					}
				}
			}
			catch (final BadLocationException e) {}
			return false;
		}
		
	}
	
	
	/** The partitioning being used for scanning. */
	private final String partitioning;
	
	private final PartitionConstraint defaultPartitionConstraint;
	
	/** The document being scanned. */
	private IDocument document;
	/** The partition to scan in. */
	private PartitionConstraint partitionConstraint;
	
	/* internal scan state */
	
	/** the most recently read position. */
	private int offset;
	/** the most recently read character. */
	protected char ch;
	
	private @Nullable ITypedRegion currentPartition;
	private boolean currentPartitionMatched;
	private int currentPartitionStart;
	private int currentPartitionEnd;
	
	private final StopCondition nonSSpaceCondition;
	private final StopCondition nonMSpaceCondition;
	
	
	@SuppressWarnings("null")
	public HeuristicTokenScanner(final DocContentSections documentContentInfo,
			final PartitionConstraint defaultContentConstraint) {
		this.partitioning= documentContentInfo.getPartitioning();
		this.defaultPartitionConstraint= defaultContentConstraint;
		
		this.nonSSpaceCondition= createSSpaceCondition();
		this.nonMSpaceCondition= createMSpaceCondition();
	}
	
	
	public CharPairSet getDefaultBrackets() {
		return COMMON_BRACKETS;
	}
	
	
	public final IDocument getDocument() {
		return this.document;
	}
	
	public final String getDocumentPartitioning() {
		return this.partitioning;
	}
	
	public final PartitionConstraint getDefaultPartitionConstraint() {
		return this.defaultPartitionConstraint;
	}
	
	protected final PartitionConstraint getPartitionConstraint() {
		return this.partitionConstraint;
	}
	
	
	private void resetPartition() {
		this.currentPartitionMatched= false;
	}
	
	private boolean updatePartition(final int offset) throws BadLocationException {
		final var partition= TextUtilities.getPartition(this.document, this.partitioning, offset, false);
		this.currentPartition= partition;
		this.currentPartitionStart= partition.getOffset();
		final int length= partition.getLength();
		this.currentPartitionEnd= this.currentPartitionStart + length;
		return this.currentPartitionMatched= (length > 0 && this.partitionConstraint.matches(partition.getType()));
	}
	
	private boolean isValidPartitionOffset(final int offset) throws BadLocationException {
		return ((this.currentPartitionMatched
						&& offset >= this.currentPartitionStart && offset < this.currentPartitionEnd )
				|| updatePartition(offset) );
	}
	
	public int nextPartitionOffsetForward(final int offset) throws BadLocationException {
		return (isValidPartitionOffset(offset) || offset >= this.currentPartitionEnd) ?
				offset + 1 :
				this.currentPartitionEnd;
	}
	
	public int nextPartitionOffsetBackward(final int offset) throws BadLocationException {
		return (isValidPartitionOffset(offset)) ?
				offset - 1 :
				this.currentPartitionStart - 1;
	}
	
	
	public final char getChar() {
		return this.ch;
	}
	
	
	protected StopCondition createSSpaceCondition() {
		return new StopCondition() {
			@Override
			public boolean matches(final char c) throws BadLocationException {
				return (c < 0x1680) ? switch (c) {
						case ' ', '\t', 0xA0 -> false;
						default -> true;
						} :
						(Character.getType(c) != Character.SPACE_SEPARATOR);
			}
		};
	}
	
	protected StopCondition createMSpaceCondition() {
		return new StopCondition() {
			@Override
			public boolean matches(final char c) throws BadLocationException {
				return (c < 0x1680) ? switch (c) {
						case ' ', '\t', 0xA0 -> false;
						case '\n', '\r' -> false;
						default -> true;
						} :
						(Character.getType(c) != Character.SPACE_SEPARATOR);
			}
		};
	}
	
	
	/**
	 * Configures the scanner for the given document
	 * and the given partition type as partition constraint
	 * 
	 * @param document the document to scan
	 * @param partition the partition to scan in
	 */
	@Override
	public void configure(final IDocument document, final String partitionType) {
		assert (document != null && partitionType != null);
		this.document= document;
		this.partitionConstraint= new PartitionConstraint() {
			@Override
			public boolean matches(final String partitionTypeToTest) {
				return partitionType == partitionTypeToTest;
			}
		};
	}
	
	/**
	 * Configures the scanner for the given document
	 * and no partition constraint
	 * 
	 * @param document the document to scan
	 */
	public void configure(final IDocument document) {
		assert (document != null);
		this.document= document;
		this.partitionConstraint= ANY_PARTITIONS_CONSTRAINT;
	}
	
	/**
	 * Configures the scanner for the given document
	 * and the partition constraint for default partitions
	 * 
	 * @param document the document to scan
	 */
	public void configureDefaultPartitions(final IDocument document) {
		assert (document != null);
		this.document= document;
		this.partitionConstraint= getDefaultPartitionConstraint();
	}
	
	/**
	 * Configures the scanner for the given document
	 * and the given partition constraint
	 * 
	 * @param document the document to scan
	 * @param partition the partition to scan in
	 */
	public void configure(final IDocument document, final PartitionConstraint partitionConstraint) {
		assert (document != null && partitionConstraint != null);
		this.document= document;
		this.partitionConstraint= partitionConstraint;
	}
	
//	public void configure(IDocument document, int offset) throws BadLocationException {
//		configure(document, TextUtilities.getContentType(
//				document, partitioning, offset, false));
//	}
	
	
	protected int createForwardBound(final int start) throws BadLocationException {
		return this.document.getLength();
	}
	
	protected int createBackwardBound(final int start) throws BadLocationException {
		return 0;
	}
	
	protected final int checkForwardBound(final int start, int bound) throws BadLocationException {
		if (bound == UNBOUND) {
			bound= createForwardBound(start);
		}
		assert (bound >= start && bound <= this.document.getLength());
		return bound;
	}
	
	protected final int checkBackwardBound(final int start, int bound) throws BadLocationException {
		if (bound == UNBOUND) {
			bound= createBackwardBound(start);
		}
		assert (bound <= start && bound >= 0);
		return bound - 1;
	}
	
	
	/**
	 * Finds the lowest position <code>p</code> in <code>document</code> such that <code>start</code> &lt;= p &lt;
	 * <code>bound</code> and <code>condition.stop(document.getChar(p), p)</code> evaluates to <code>true</code>.
	 * 
	 * @param start the first character position in <code>document</code> to be considered
	 * @param bound the first position in <code>document</code> to not consider any more, with <code>bound</code> &gt; <code>start</code>, or <code>UNBOUND</code>
	 * @param condition the <code>StopCondition</code> to check
	 * @return the lowest position in [<code>start</code>, <code>bound</code>) for which <code>condition</code> holds, or <code>NOT_FOUND</code> if none can be found
	 * @throws BadLocationException 
	 */
	protected final int scanAnyForward(final int start, final int bound, final StopCondition condition,
			final char escapeChar)
			throws BadLocationException {
		int offset= start;
		while (offset < bound) {
			this.offset= offset;
			MATCH: if (condition.matches(this.ch= this.document.getChar(offset))) {
				if (escapeChar != NO_CHAR) {
					while (--offset >= start && this.document.getChar(offset) == escapeChar) {}
					if ((this.offset - offset) % 2 == 0) {
						offset= this.offset;
						break MATCH;
					}
				}
				return this.offset;
			}
			offset++;
		}
		this.offset= bound;
		this.ch= (this.offset >= 0 && this.offset < this.document.getLength()) ? this.document.getChar(this.offset) : (char)-1;
		return NOT_FOUND;
	}
	
	protected final int scanForward(final int start, int bound, final StopCondition condition,
			final char escapeChar)
			throws BadLocationException {
		if (bound == UNBOUND) {
			bound= this.document.getLength();
		}
		assert (start >= 0);
		
		resetPartition();
		int offset= start;
		while (offset < bound) {
			if (isValidPartitionOffset(offset)) {
				this.offset= offset;
				MATCH: if (condition.matches(this.ch= this.document.getChar(offset))) {
					if (escapeChar != NO_CHAR) {
						while (--offset >= start && this.document.getChar(offset) == escapeChar) {}
						if ((this.offset - offset) % 2 == 0) {
							offset= this.offset;
							break MATCH;
						}
					}
					return this.offset;
				}
			}
			offset= nextPartitionOffsetForward(offset);
		}
		this.offset= bound;
		this.ch= (this.offset >= 0 && this.offset < this.document.getLength()) ? this.document.getChar(this.offset) : (char)-1;
		return NOT_FOUND;
	}
	
	/**
	 * Finds the highest position <code>p</code> in <code>document</code> such that <code>bound</code> &lt; <code>p</code> &lt;= <code>start</code>
	 * and <code>condition.stop(document.getChar(p), p)</code> evaluates to <code>true</code>.
	 * 
	 * @param start the first character position in <code>document</code> to be considered
	 * @param bound the first position in <code>document</code> to not consider any more, with <code>bound</code> &lt; <code>start</code>, or <code>UNBOUND</code>
	 * @param condition the <code>StopCondition</code> to check
	 * @return the highest position in (<code>bound</code>, <code>start</code> for which <code>condition</code> holds, or <code>NOT_FOUND</code> if none can be found
	 * @throws BadLocationException 
	 */
	protected final int scanAnyBackward(final int start, final int bound, final StopCondition condition,
			final char escapeChar)
			throws BadLocationException {
		int offset= start;
		while (offset > bound) {
			this.offset= offset;
			MATCH: if (condition.matches(this.ch= this.document.getChar(offset))) {
				if (escapeChar != NO_CHAR) {
					while (--offset > bound && this.document.getChar(offset) == escapeChar) {}
					if ((this.offset - offset) % 2 == 0) {
						offset= this.offset;
						break MATCH;
					}
				}
				return this.offset;
			}
			offset--;
		}
		this.offset= bound;
		this.ch= (this.offset >= 0 && this.offset < this.document.getLength()) ? this.document.getChar(this.offset) : (char)-1;
		return NOT_FOUND;
	}
	
	protected final int scanBackward(final int start, int bound, final StopCondition condition,
			final char escapeChar)
			throws BadLocationException {
		if (bound == UNBOUND) {
			bound= -1;
		}
//		assert (start == 0 || start < document.getLength() );
		
		resetPartition();
		int offset= start;
		while (offset > bound) {
			if (isValidPartitionOffset(offset)) {
				this.offset= offset;
				MATCH: if (condition.matches(this.ch= this.document.getChar(offset))) {
					if (escapeChar != NO_CHAR) {
						while (--offset > bound && this.document.getChar(offset) == escapeChar) {}
						if ((this.offset - offset) % 2 == 0) {
							offset= this.offset;
							break MATCH;
						}
					}
					return this.offset;
				}
			}
			offset= nextPartitionOffsetBackward(offset);
		}
		this.offset= bound;
		this.ch= (this.offset >= 0 && this.offset < this.document.getLength()) ? this.document.getChar(this.offset) : (char)-1;
		return NOT_FOUND;
	}
	
	
	protected final @Nullable IRegion findAnyRegion(final int offset, final StopCondition condition,
			final boolean allowClosing)
					throws BadLocationException {
		int start= offset;
		int end= scanAnyForward(offset, createForwardBound(start), condition, NO_CHAR);
		if (end == NOT_FOUND) {
			end= this.offset;
		}
		if (allowClosing || end > offset) {
			start= scanAnyBackward(--start, createBackwardBound(start) - 1, condition, NO_CHAR);
			if (start == NOT_FOUND) {
				start= this.offset;
			}
			start++;
		}
		if (start < end) {
			return new Region(start, end - start);
		}
		return null;
	}
	
	protected final @Nullable IRegion findRegion(final int offset, final StopCondition condition,
			final boolean allowClosing)
			throws BadLocationException {
		int start= offset;
		int end= scanForward(offset, UNBOUND, condition, NO_CHAR);
		if (end == NOT_FOUND) {
			end= this.offset;
		}
		if (allowClosing || end > offset) {
			start= scanBackward(--start, UNBOUND, condition, NO_CHAR);
			if (start == NOT_FOUND) {
				start= this.offset;
			}
			start++;
		}
		if (start < end) {
			return new Region(start, end - start);
		}
		return null;
	}
	
	
	/**
	 * Finds the lowest position in <code>document</code> such that the position is &gt;= <code>position</code>
	 * and &lt; <code>bound</code> and <code>document.getChar(position) == ch</code> evaluates to <code>true</code>
	 * and the position is in the default partition.
	 * 
	 * @param offset the first character position in <code>document</code> to be considered
	 * @param bound the first position in <code>document</code> to not consider any more, with <code>bound</code> &gt; <code>position</code>, or <code>UNBOUND</code>
	 * @param ch the <code>char</code> to search for
	 * @return the lowest position of <code>ch</code> in (<code>bound</code>, <code>position</code>] that resides in a Java partition, or <code>NOT_FOUND</code> if none can be found
	 * @throws BadLocationException 
	 */
	public final int scanForward(final int offset, int bound, final char ch)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		return scanForward(offset, bound, new SingleCharacterMatchCondition(ch), NO_CHAR);
	}
	
	/**
	 * Finds the highest position in <code>document</code> such that the position is &lt;= <code>position</code>
	 * and &gt; <code>bound</code> and <code>document.getChar(position) == ch</code> evaluates to <code>true</code> for at least one
	 * ch in <code>chars</code> and the position is in the default partition.
	 * 
	 * @param offset the first character position in <code>document</code> to be considered
	 * @param bound 
	 * @param ch the <code>char</code> to search for
	 * @return the highest position of one element in <code>chars</code> in (<code>bound</code>, <code>position</code>] that resides in a Java partition, or <code>NOT_FOUND</code> if none can be found
	 * @throws BadLocationException 
	 */
	public final int scanBackward(final int offset, int bound, final char ch)
			throws BadLocationException {
		bound= checkBackwardBound(offset, bound);
		return scanBackward(offset, bound, new SingleCharacterMatchCondition(ch), NO_CHAR);
	}
	
	/**
	 * Finds the lowest position in <code>document</code> such that the position is &gt;= <code>position</code>
	 * and &lt; <code>bound</code> and <code>document.getChar(position) == ch</code> evaluates to <code>true</code> for at least one
	 * ch in <code>chars</code> and the position is in the default partition.
	 * 
	 * @param offset the first character position in <code>document</code> to be considered
	 * @param bound the first position in <code>document</code> to not consider any more, with <code>bound</code> &gt; <code>position</code>, or <code>UNBOUND</code>
	 * @param chars an array of <code>char</code> to search for
	 * @return the lowest position of a non-whitespace character in [<code>position</code>, <code>bound</code>) that resides in a Java partition, or <code>NOT_FOUND</code> if none can be found
	 * @throws BadLocationException 
	 */
	public final int scanForward(final int offset, int bound, final char[] chars)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		return scanForward(offset, bound, new CharacterMatchCondition(chars), NO_CHAR);
	}
	
	/**
	 * Finds the highest position in <code>document</code> such that the position is &lt;= <code>position</code>
	 * and &gt; <code>bound</code> and <code>document.getChar(position) == ch</code> evaluates to <code>true</code> for at least one
	 * ch in <code>chars</code> and the position is in the default partition.
	 * 
	 * @param offset the first character position in <code>document</code> to be considered
	 * @param bound 
	 * @param chars an array of <code>char</code> to search for
	 * @return the highest position of one element in <code>chars</code> in (<code>bound</code>, <code>position</code>] that resides in a Java partition, or <code>NOT_FOUND</code> if none can be found
	 * @throws BadLocationException 
	 */
	public final int scanBackward(final int offset, int bound, final char[] chars)
			throws BadLocationException {
		bound= checkBackwardBound(offset, bound);
		return scanBackward(offset, bound, new CharacterMatchCondition(chars), NO_CHAR);
	}
	
	public final int scanForward(final int offset, int bound, final String string)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		bound-= string.length();
		return scanForward(offset, bound, new StringMatchCondition(ImCollections.newList(string)), NO_CHAR);
	}
	
	public final int scanForward(final int offset, int bound, final ImList<String> strings)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		bound-= strings.getFirst().length();
		return scanForward(offset, bound, new StringMatchCondition(strings), NO_CHAR);
	}
	
	
	@Override
	public int findClosingPeer(int offset, final CharPair pair, final char escapeChar)
			throws BadLocationException {
		if (offset < 0) {
			throw new BadLocationException();
		}
		
		final StopCondition condition= new CharacterMatchCondition(pair.toArray());
		
		int depth= 1;
		final int bound= checkForwardBound(offset, UNBOUND);
		offset-= 1;
		while (true) {
			offset= scanForward(offset + 1, bound, condition, escapeChar);
			if (offset == NOT_FOUND) {
				return NOT_FOUND;
			}
			
			if (this.ch == pair.opening) {
				depth++;
			}
			else {
				depth--;
			}
			
			if (depth == 0) {
				return offset;
			}
		}
	}
	
	@Override
	public int findOpeningPeer(int offset, final CharPair pair, final char escapeChar)
			throws BadLocationException {
		if (offset > this.document.getLength()) {
			throw new BadLocationException();
		}
		
		final StopCondition condition= new CharacterMatchCondition(pair.toArray());
		
		int depth= 1;
		final int bound= checkBackwardBound(offset, UNBOUND);
		while (true) {
			offset= scanBackward(offset - 1, bound, condition, escapeChar);
			if (offset == NOT_FOUND) {
				return NOT_FOUND;
			}
			
			if (this.ch == pair.closing) {
				depth++;
			}
			else {
				depth--;
			}
			
			if (depth == 0) {
				return offset;
			}
		}
	}
	
	/**
	 * Computes bracket balance
	 * 
	 * @param backwardOffset searching backward before this offset
	 * @param forwardOffset searching forward after (including) this offset
	 * @param initial initial balance (e.g. known or not yet inserted between backward and forward offset)
	 * @param searchType interesting bracket type
	 * @return
	 * @throws BadLocationException 
	 */
	public int[] computePairBalance(int backwardOffset, int forwardOffset, final @Nullable TextRegion region,
			final CharPairSet brackets,
			final int[] initial, final int searchPairIndex)
			throws BadLocationException {
		class BracketBalanceCondition implements StopCondition {
			
			private CharPairSet. @Nullable CharMatch match;
			
			@Override
			public boolean matches(final char c) {
				return ((this.match= brackets.getMatch(c)) != null);
			}
			
		}
		final int[] balance= new int[brackets.getPairCount()];
		final var condition= new BracketBalanceCondition();
		int breakPairIndex= -1;
		ITER_BACKWARD: {
			final int bound= checkBackwardBound(backwardOffset, (region != null) ? region.getStartOffset() : UNBOUND);
			while (--backwardOffset > bound) {
				backwardOffset= scanBackward(backwardOffset, bound, condition, brackets.getEscapeChar());
				if (backwardOffset != NOT_FOUND) {
					final var match= nonNullAssert(condition.match);
					if (match.isOpening()) {
						balance[match.getPairIndex()]++;
						if (match.getPairIndex() != searchPairIndex && balance[match.getPairIndex()] > 0) {
							breakPairIndex= match.getPairIndex();
							break ITER_BACKWARD;
						}
					}
					else {
						balance[match.getPairIndex()]--;
					}
				}
				else {
					break ITER_BACKWARD;
				}
			}
		}
		for (int i= 0; i < balance.length; i++) {
			balance[i]= ((balance[i] > 0) ? balance[i] : 0) + initial[i];
		}
		ITER_FORWARD: {
			final int bound= checkForwardBound(forwardOffset, (region != null) ? region.getEndOffset() : UNBOUND);
			while (forwardOffset < bound) {
				forwardOffset= scanForward(forwardOffset, bound, condition, brackets.getEscapeChar());
				if (forwardOffset != NOT_FOUND) {
					final var match= nonNullAssert(condition.match);
					if (match.isOpening()) {
						balance[match.getPairIndex()]++;
					}
					else {
						balance[match.getPairIndex()]--;
					}
					if (breakPairIndex >= 0 && balance[breakPairIndex] == 0) {
						break ITER_FORWARD;
					}
					forwardOffset++;
				}
				else {
					break ITER_FORWARD;
				}
			}
		}
		
		return balance;
	}
	
	public int computePairBalance(int backwardOffset, int forwardOffset, final @Nullable TextRegion region,
			final CharPair pair, final char escapeChar, final int initial)
			throws BadLocationException {
		final StopCondition condition= new CharacterMatchCondition(pair.toArray());
		
		int balance= 0;
		ITER_BACKWARD: {
			final int bound= checkBackwardBound(backwardOffset, (region != null) ? region.getStartOffset() : UNBOUND);
			while (--backwardOffset > bound) {
				backwardOffset= scanBackward(backwardOffset, bound, condition, escapeChar);
				if (backwardOffset != NOT_FOUND) {
					if (this.ch == pair.opening) {
						balance++;
					}
					else {
						balance--;
					}
				}
				else {
					break ITER_BACKWARD;
				}
			}
		}
		balance= ((balance > 0) ? balance : 0) + initial;
		ITER_FORWARD: {
			final int bound= checkForwardBound(forwardOffset, (region != null) ? region.getEndOffset() : UNBOUND);
			while (forwardOffset < bound) {
				forwardOffset= scanForward(forwardOffset, bound, condition, escapeChar);
				if (forwardOffset != NOT_FOUND) {
					if (this.ch == pair.opening) {
						balance++;
					}
					else {
						balance--;
					}
					if (balance == 0) {
						break ITER_FORWARD;
					}
					forwardOffset++;
				}
				else {
					break ITER_FORWARD;
				}
			}
		}
		
		return balance;
	}
	
	
	/**
	 * Finds the smallest position in <code>document</code> such that the position is &gt;= <code>position</code>
	 * and &lt; <code>bound</code> and <code>Character.isWhitespace(document.getChar(pos))</code> evaluates to <code>false</code>.
	 * 
	 * @param offset the first character position in <code>document</code> to be considered
	 * @param bound the first position in <code>document</code> to not consider any more, with <code>bound</code> &gt; <code>position</code>, or <code>UNBOUND</code>
	 * @return the smallest position of a non-whitespace character in [<code>position</code>, <code>bound</code>), or <code>NOT_FOUND</code> if none can be found
	 * @throws BadLocationException 
	 */
	public final int findAnyNonSSpaceForward(final int offset, int bound)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		return scanAnyForward(offset, bound, this.nonSSpaceCondition, NO_CHAR);
	}
	
	public final int findAnyNonMSpaceForward(final int offset, int bound)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		return scanAnyForward(offset, bound, this.nonMSpaceCondition, NO_CHAR);
	}
	
	public final int findAnyNonSSpaceBackward(final int offset, int bound)
			throws BadLocationException {
		bound= checkBackwardBound(offset, bound);
		return scanAnyBackward(offset - 1, bound, this.nonSSpaceCondition, NO_CHAR);
	}
	
	public final int findAnyNonMSpaceBackward(final int offset, int bound)
			throws BadLocationException {
		bound= checkBackwardBound(offset, bound);
		return scanAnyBackward(offset - 1, bound, this.nonMSpaceCondition, NO_CHAR);
	}
	
	public final int findNonSSpaceForward(final int offset, int bound)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		return scanForward(offset, bound, this.nonSSpaceCondition, NO_CHAR);
	}
	
	public final int findNonMSpaceForward(final int offset, int bound)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		return scanForward(offset, bound, this.nonMSpaceCondition, NO_CHAR);
	}
	
	public final int findNonSSpaceBackward(final int offset, int bound)
			throws BadLocationException {
		bound= checkBackwardBound(offset, bound);
		return scanBackward(offset - 1, bound, this.nonSSpaceCondition, NO_CHAR);
	}
	
	public final int findNonMSpaceBackward(final int offset, int bound)
			throws BadLocationException {
		bound= checkBackwardBound(offset, bound);
		return scanBackward(offset - 1, bound, this.nonMSpaceCondition, NO_CHAR);
	}
	
	
	public final int expandAnySSpaceForward(final int offset, int bound)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		scanForward(offset, bound, this.nonSSpaceCondition, NO_CHAR);
		return this.offset;
	}
	
	public final int expandAnyMSpaceForward(final int offset, int bound)
			throws BadLocationException {
		bound= checkForwardBound(offset, bound);
		scanForward(offset, bound, this.nonMSpaceCondition, NO_CHAR);
		return this.offset;
	}
	
	public final int expandAnySSpaceBackward(final int offset, int bound)
			throws BadLocationException {
		bound= checkBackwardBound(offset, bound);
		scanBackward(offset - 1, bound, this.nonSSpaceCondition, NO_CHAR);
		return this.offset + 1;
	}
	
	public final int expandAnyMSpaceBackward(final int offset, int bound)
			throws BadLocationException {
		bound= checkBackwardBound(offset, bound);
		scanBackward(offset - 1, bound, this.nonMSpaceCondition, NO_CHAR);
		return this.offset + 1;
	}
	
	public @Nullable IRegion findAnySSpaceRegion(final int offset)
			throws BadLocationException {
		return findAnyRegion(offset, this.nonSSpaceCondition, false);
	}
	
	public @Nullable IRegion findAnyMSpaceRegion(final int offset)
			throws BadLocationException {
		return findAnyRegion(offset, this.nonMSpaceCondition, false);
	}
	
	public boolean isBlankLine(final int offset) throws BadLocationException {
		final IRegion line= this.document.getLineInformationOfOffset(offset);
		if (line.getLength() > 0) {
			return (findAnyNonSSpaceForward(line.getOffset(), line.getOffset() + line.getLength()) == NOT_FOUND);
		}
		return true;
	}
	
	public final @Nullable IRegion findCommonWord(final int offset)
			throws BadLocationException {
		return findAnyRegion(offset, COMMON_WORD_CONDITION, false);
	}
	
	
	public final int getFirstLineOfRegion(final IRegion region) throws BadLocationException {
		return this.document.getLineOfOffset(region.getOffset());
	}
	
	public final int getLastLineOfRegion(final IRegion region) throws BadLocationException {
		if (region.getLength() == 0) {
			return this.document.getLineOfOffset(region.getOffset());
		}
		return this.document.getLineOfOffset(region.getOffset() + region.getLength() - 1);
	}
	
	
	public final int count(int start, final int stop, final char c)
			throws BadLocationException {
		final SingleCharacterMatchCondition condition= new SingleCharacterMatchCondition(c);
		
		int count= 0;
		while (start < stop && (start= scanForward(start, stop, condition, NO_CHAR)) != NOT_FOUND) {
			count++;
			start++;
		}
		return count;
	}
	
	
	/**
	 * Returns the partition at <code>position</code>.
	 * 
	 * @param offset the position to get the partition for
	 * @return the partition at <code>position</code> or a dummy zero-length
	 *     partition if accessing the document fails
	 */
	public final ITypedRegion getPartition(final int offset) {
		try {
			return TextUtilities.getPartition(this.document, this.partitioning, offset, false);
		}
		catch (final BadLocationException e) {
			return new TypedRegion(this.offset, 0, "__no_partition_at_all"); //$NON-NLS-1$
		}
	}
	
}
