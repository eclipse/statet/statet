/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.text.core;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.Region;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CharPair;
import org.eclipse.statet.jcommons.text.core.CharPairSet;
import org.eclipse.statet.jcommons.text.core.CharPairSet.CharMatch;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;
import org.eclipse.statet.ecommons.text.core.TextTokenScanner;
import org.eclipse.statet.ecommons.text.core.util.TextUtils;


/**
 * Helper class for match pairs of characters.
 */
@NonNullByDefault
public class BasicCharPairMatcher implements CharPairMatcher {
	
	
	private static final char IGNORE= '\n';
	
	private static final byte NOTHING_FOUND= 0;
	private static final byte OPENING_NOT_FOUND= 1;
	private static final byte CLOSING_NOT_FOUND= 2;
	private static final byte PAIR_FOUND= 3;
	
	
	protected final CharPairSet pairs;
	protected final String partitioning;
	protected final PartitionConstraint partitionConstraint;
	protected final TextTokenScanner scanner;
	
	protected int offset;
	
	protected int beginPos;
	protected int endPos;
	protected int anchor;
	protected @Nullable ITypedRegion partition;
	
	
	public BasicCharPairMatcher(final CharPairSet pairs, final String partitioning,
			final PartitionConstraint partitionConstraint, final TextTokenScanner scanner) {
		this.pairs= pairs;
		this.scanner= scanner;
		this.partitioning= partitioning;
		this.partitionConstraint= partitionConstraint;
	}
	
	public BasicCharPairMatcher(final HeuristicTokenScanner scanner) {
		this.pairs= scanner.getDefaultBrackets();
		this.scanner= scanner;
		this.partitioning= scanner.getDocumentPartitioning();
		this.partitionConstraint= scanner.getDefaultPartitionConstraint();
	}
	
	
	@Override
	public @Nullable IRegion match(final IDocument document, final int offset) {
		if (document == null || offset < 0) {
			return null;
		}
		this.offset= offset;
		if (matchPairsAt(document, true) == PAIR_FOUND) {
			return new Region(this.beginPos, this.endPos - this.beginPos + 1);
		}
		else {
			return null;
		}
	}
	
	@Override
	public @Nullable IRegion match(final IDocument document, final int offset, final boolean auto) {
		if (document == null || offset < 0) {
			return null;
		}
		this.offset= offset;
		switch (matchPairsAt(document, auto)) {
		case OPENING_NOT_FOUND:
			return new Region(this.endPos, -1);
		case CLOSING_NOT_FOUND:
			return new Region(this.beginPos, -1);
		case PAIR_FOUND:
			return new Region(this.beginPos, this.endPos - this.beginPos + 1);
		default:
			return null;
		}
	}
	
	@Override
	public int getAnchor() {
		return this.anchor;
	}
	
	@Override
	public void dispose() {
		clear();
	}
	
	@Override
	public void clear() {
	}
	
	/**
	 * Search Pairs
	 * @param document 
	 * @param auto 
	 */
	protected byte matchPairsAt(final IDocument document, final boolean auto) {
		this.beginPos= -1;
		this.endPos= -1;
		
		// get the chars preceding and following the start position
		try {
			char thisChar= IGNORE;
			final ITypedRegion thisPartition= getPartition(document, this.offset);
			if (thisPartition != null && this.offset < document.getLength()) {
				thisChar= document.getChar(this.offset);
			}
			
			// check, if escaped
			char prevChar= IGNORE;
			final ITypedRegion prevPartition;
			if (this.offset > 0) {
				prevPartition= getPartition(document, this.offset - 1);
				if (prevPartition != null && auto) {
					prevChar= document.getChar(this.offset - 1);
					
					if (this.pairs.getEscapeChar() > 0) {
						final int partitionOffset= prevPartition.getOffset();
						int checkOffset= this.offset - 2;
						while (checkOffset >= partitionOffset) {
							if (document.getChar(checkOffset) == this.pairs.getEscapeChar()) {
								checkOffset--;
							}
							else {
								break;
							}
						}
						if ((this.offset - checkOffset) % 2 == 1) {
							// prev char is escaped
							prevChar= IGNORE;
						}
						else if (prevPartition.equals(thisPartition) && prevChar == this.pairs.getEscapeChar()) {
							// this char is escaped
							thisChar= IGNORE;
						}
					}
				}
			}
			else {
				prevPartition= null;
			}
			
			final var match= findChar(prevChar, prevPartition, thisChar, thisPartition);
			
			if (this.beginPos > -1) {		// closing peer
				this.anchor= LEFT;
				this.endPos= findClosingPeer(document, this.partition,
						this.beginPos + 1, match.getPair() );
				return (this.endPos > -1 && this.beginPos != this.endPos) ? PAIR_FOUND : CLOSING_NOT_FOUND;
			}
			else if (this.endPos > -1) {	// opening peer
				this.anchor= RIGHT;
				this.beginPos= findOpeningPeer(document, this.partition,
						this.endPos, match.getPair() );
				return (this.beginPos > -1 && this.beginPos != this.endPos) ? PAIR_FOUND : OPENING_NOT_FOUND;
			}
			
		} catch (final BadLocationException | BadPartitioningException e) {
		} // ignore
		
		return NOTHING_FOUND;
	}
	
	private @Nullable ITypedRegion getPartition(final IDocument document, final int offset)
			throws BadPartitioningException, BadLocationException {
		final ITypedRegion partition= TextUtils.getPartition(document, this.partitioning, offset, false);
		return (this.partitionConstraint.matches(partition.getType())) ?
				partition : null;
	}
	
	/**
	 * @param prevChar
	 * @param thisChar
	 * @return
	 */
	private @Nullable CharMatch findChar(final char prevChar, final ITypedRegion prevPartition,
			final char thisChar, final ITypedRegion thisPartition) {
		// search order 3{2 1}4
		final CharMatch thisMatch= this.pairs.getMatch(thisChar);
		if (thisMatch != null && thisMatch.isClosing()) {
			this.endPos= this.offset;
			this.partition= thisPartition;
			return thisMatch;
		}
		final CharMatch prevMatch= this.pairs.getMatch(prevChar);
		if (prevMatch != null && prevMatch.isOpening()) {
			this.beginPos= this.offset - 1;
			this.partition= prevPartition;
			return prevMatch;
		}
		if (thisMatch != null /* && thisMatch.isOpening() */) {
			this.beginPos= this.offset;
			this.partition= thisPartition;
			return thisMatch;
		}
		if (prevMatch != null /* && prevMatch.isClosing() */) {
			this.endPos= this.offset - 1;
			this.partition= prevPartition;
			return prevMatch;
		}
		this.partition= null;
		return null;
	}
	
	
	protected int findClosingPeer(final IDocument document, final ITypedRegion partition,
			final int start, final CharPair pair)
			throws BadLocationException {
		this.scanner.configure(document, partition.getType());
		return this.scanner.findClosingPeer(start, pair, this.pairs.getEscapeChar());
	}
	
	protected int findOpeningPeer(final IDocument document, final ITypedRegion partition,
			final int start, final CharPair pair)
			throws BadLocationException {
		this.scanner.configure(document, partition.getType());
		return this.scanner.findOpeningPeer(start, pair, this.pairs.getEscapeChar());
	}
	
}
