/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.text.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.IdentityHashMap;
import java.util.Map;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;


/**
 * Character pair matcher supporting separate implementation for each document content section
 * type.
 * 
 * Implement {@link #createHandler(String)} for lazy initialization of the matchers of the section
 * types.
 * 
 * @see DocContentSections
 */
@NonNullByDefault
public class MultiContentSectionCharPairMatcher implements CharPairMatcher {
	
	
	private static final Object NULL= new Object();
	
	
	private final DocContentSections sections;
	
	private final Map<String, Object> handlers= new IdentityHashMap<>(8);
	
	private @Nullable CharPairMatcher activeMatcher;
	
	
	public MultiContentSectionCharPairMatcher(final DocContentSections sections) {
		this.sections= nonNullAssert(sections);
	}
	
	public MultiContentSectionCharPairMatcher(final DocContentSections sections,
			final String sectionType1, final CharPairMatcher matcher1) {
		this(sections, sectionType1, matcher1, null, null);
	}
	
	public MultiContentSectionCharPairMatcher(final DocContentSections sections,
			final String sectionType1, final CharPairMatcher matcher1,
			final String sectionType2, final CharPairMatcher matcher2) {
		this(sections);
		
		if (sectionType1 != null) {
			registerHandler(sectionType1, matcher1);
		}
		if (sectionType2 != null) {
			registerHandler(sectionType2, matcher2);
		}
	}
	
	
	protected DocContentSections getSections() {
		return this.sections;
	}
	
	@Override
	public void dispose() {
		for (final Object handler : this.handlers.values()) {
			if (handler != NULL) {
				((CharPairMatcher)handler).dispose();
			}
		}
		this.handlers.clear();
	}
	
	
	public void registerHandler(final String sectionType, final CharPairMatcher matcher) {
		this.handlers.put(nonNullAssert(sectionType), matcher);
	}
	
	protected final @Nullable CharPairMatcher getHandler(final String sectionType) {
		if (sectionType == DocContentSections.ERROR) {
			return null;
		}
		Object handler= this.handlers.get(sectionType);
		if (handler == null) {
			handler= NULL;
			try {
				final CharPairMatcher newHandler= createHandler(sectionType);
				if (newHandler != null) {
					handler= newHandler;
				}
			}
			finally {
				this.handlers.put(sectionType, handler);
			}
		}
		return (handler != NULL) ? (CharPairMatcher)handler : null;
	}
	
	protected @Nullable CharPairMatcher createHandler(final String sectionType) {
		return null;
	}
	
	
	@Override
	public void clear() {
		final var activeMatcher= this.activeMatcher;
		if (activeMatcher != null) {
			this.activeMatcher= null;
			activeMatcher.clear();
		}
	}
	
	@Override
	public @Nullable IRegion match(final IDocument document, final int offset) {
		final var previousMatcher= this.activeMatcher;
		final var activeMatcher= getHandler(
				this.sections.getType(document, offset) );
		this.activeMatcher= activeMatcher;
		
		if (previousMatcher != null && previousMatcher != activeMatcher) {
			previousMatcher.clear();
		}
		
		if (activeMatcher != null) {
			return activeMatcher.match(document, offset);
		}
		return null;
	}
	
	@Override
	public @Nullable IRegion match(final IDocument document, final int offset, final boolean auto) {
		final var previousMatcher= this.activeMatcher;
		final var activeMatcher= getHandler(
				this.sections.getType(document, offset) );
		this.activeMatcher= activeMatcher;
		
		if (previousMatcher != null && previousMatcher != activeMatcher) {
			previousMatcher.clear();
		}
		
		if (activeMatcher != null) {
			return activeMatcher.match(document, offset, auto);
		}
		return null;
	}
	
	@Override
	public int getAnchor() {
		final var activeMatcher= this.activeMatcher;
		if (activeMatcher != null) {
			return activeMatcher.getAnchor();
		}
		return -1;
	}
	
}
