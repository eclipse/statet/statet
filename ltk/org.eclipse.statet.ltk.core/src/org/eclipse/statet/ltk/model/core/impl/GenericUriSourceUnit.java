/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


/**
 * Generic source unit for external files (URI/EFS).
 */
@NonNullByDefault
public abstract class GenericUriSourceUnit implements SourceUnit {
	
	
	private final String id;
	private final ElementName name;
	
	private final IFileStore fileStore;
	private WorkingBuffer buffer;
	
	private int counter= 0;
	
	
	@SuppressWarnings("null")
	public GenericUriSourceUnit(final String id, final IFileStore fileStore) {
		this.id= id;
		this.name= new ElementName() {
			@Override
			public int getType() {
				return 0x011; // see RElementName
			}
			@Override
			public String getDisplayName() {
				return GenericUriSourceUnit.this.fileStore.toString();
			}
			@Override
			public String getSegmentName() {
				return GenericUriSourceUnit.this.id;
			}
			@Override
			public @Nullable ElementName getNextSegment() {
				return null;
			}
		};
		this.fileStore= nonNullAssert(fileStore);
	}
	
	
	@Override
	public @Nullable SourceUnit getUnderlyingUnit() {
		return null;
	}
	
	@Override
	public boolean isSynchronized() {
		return this.buffer.isSynchronized();
	}
	
	@Override
	public String getId() {
		return this.id;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * A source unit of this type is usually of the type
	 * {@link LtkModelElement#C12_SOURCE_FILE C2_SOURCE_FILE}.
	 */
	@Override
	public int getElementType() {
		return LtkModelElement.C12_SOURCE_FILE;
	}
	
	@Override
	public ElementName getElementName() {
		return this.name;
	}
	
	@Override
	public boolean exists() {
		return this.counter > 0;
	}
	
	@Override
	public boolean isReadOnly() {
		return false;
	}
	
	@Override
	public boolean checkState(final boolean validate, final IProgressMonitor monitor) {
		return this.buffer.checkState(validate, monitor);
	}
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * A source unit of this type is usually doesn't have a resource/path.
	 */
	@Override
	public Object getResource() {
		return this.fileStore;
	}
	
	
	@Override
	public AbstractDocument getDocument(final @Nullable IProgressMonitor monitor) {
		return this.buffer.getDocument(monitor);
	}
	
	@Override
	public long getContentStamp(final @Nullable IProgressMonitor monitor) {
		return this.buffer.getContentStamp(monitor);
	}
	
	@Override
	public SourceContent getContent(final IProgressMonitor monitor) {
		return this.buffer.getContent(monitor);
	}
	
	
	@Override
	public @Nullable AstInfo getAstInfo(final @Nullable String type, final boolean ensureSync,
			final IProgressMonitor monitor) {
		return null;
	}
	
	@Override
	public @Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int syncLevel,
			final IProgressMonitor monitor) {
		return null;
	}
	
	
	@Override
	@SuppressWarnings("unused")
	public synchronized final void connect(final IProgressMonitor monitor) {
		this.counter++;
		if (this.counter == 1) {
			final SubMonitor m= SubMonitor.convert(monitor, 1);
			if (this.buffer == null) {
				m.setWorkRemaining(2);
				this.buffer= createWorkingBuffer(m.newChild(1));
			}
			register();
		}
	}
	
	@Override
	public synchronized final void disconnect(final IProgressMonitor monitor) {
		this.counter--;
		if (this.counter == 0) {
			final SubMonitor m= SubMonitor.convert(monitor, 2);
			this.buffer.releaseDocument(m.newChild(1));
			unregister();
		}
	}
	
	@Override
	public synchronized boolean isConnected() {
		return (this.counter > 0);
	}
	
	protected abstract WorkingBuffer createWorkingBuffer(SubMonitor m);
	
	protected void register() {
	}
	
	protected void unregister() {
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == IFileStore.class) {
			return (T)this.fileStore;
		}
		if (adapterType == PreferenceAccess.class) {
			return (T)EPreferences.getInstancePrefs();
		}
		return null;
	}
	
	
	@Override
	public String toString() {
		return getModelTypeId() + '/' + getWorkingContext() + ": " + getId(); //$NON-NLS-1$
	}
	
}
