/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


/**
 * {@link GenericResourceSourceUnit} plus:
 * <ul>
 *   <li>Support for {@link SourceUnitModelContainer}</li>
 * </ul>
 * 
 * @param <TModelContainer>
 */
@NonNullByDefault
public abstract class GenericResourceSourceUnit2<
				TModelContainer extends SourceUnitModelContainer<? extends SourceUnit, ? extends SourceUnitModelInfo>>
		extends GenericResourceSourceUnit {
	
	
	private final TModelContainer model;
	
	
	public GenericResourceSourceUnit2(final String id, final IFile file) {
		super(id, file);
		
		this.model= createModelContainer();
	}
	
	
	protected abstract TModelContainer createModelContainer();
	
	protected final TModelContainer getModelContainer() {
		return this.model;
	}
	
	
	@Override
	protected void unregister() {
		super.unregister();
		
		this.model.clear();
	}
	
	@Override
	public @Nullable AstInfo getAstInfo(final @Nullable String type, final boolean ensureSync,
			final IProgressMonitor monitor) {
		if (type == null || this.model.isContainerFor(type)) {
			final AstInfo ast= this.model.getCurrentAst();
			final long stamp= getResource().getModificationStamp();
			if (ast != null && ast.getStamp().getContentStamp() == stamp) {
				return ast;
			}
			// TODO
		}
		return null;
	}
	
	@Override
	public @Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int flags,
			final IProgressMonitor monitor) {
		if (type == null || this.model.isContainerFor(type)) {
			if ((flags & ModelManager.RECONCILE) != 0) {
				if ((flags & ModelManager.REFRESH) != 0) {
					this.model.clear();
				}
				// trigger rebuild?
				return null;
			}
			
			return this.model.getModelInfo(flags, monitor);
		}
		return null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == this.model.getAdapterClass()) {
			return (T)this.model;
		}
		return super.getAdapter(adapterType);
	}
	
}
