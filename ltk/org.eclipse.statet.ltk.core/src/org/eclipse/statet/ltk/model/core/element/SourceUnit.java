/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.element;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IDocumentExtension4;
import org.eclipse.jface.text.ISynchronizable;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.SourceUnitManager;


/**
 * A source unit provides a document for source code
 * <p>
 * The typical example for a source unit is a text file.</p>
 * <p>
 * Source units should be created using the {@link SourceUnitManager}.
 * For the progress monitors of the methods the SubMonitor pattern is applied.</p>
 */
@NonNullByDefault
public interface SourceUnit extends LtkModelElement<LtkModelElement<?>>, IAdaptable {
	
	
	long UNKNOWN_MODIFICATION_STAMP= IDocumentExtension4.UNKNOWN_MODIFICATION_STAMP;
	
	
	WorkingContext getWorkingContext();
	@Nullable SourceUnit getUnderlyingUnit();
	
	/**
	 * If the content is synchronized with the underlying unit.
	 * 
	 * @return <code>true</code> if synchronized, otherwise <code>false</code>
	 */
	boolean isSynchronized();
	
	/**
	 * The file resource of the source unit. The type depends on the source unit.
	 * 
	 * @return the resource or <code>null</code> if without resource
	 */
	@Nullable Object getResource();
	
	
	void connect(final IProgressMonitor monitor);
	void disconnect(final IProgressMonitor monitor);
	boolean isConnected();
	
	
	/**
	 * Checks if the source is modifiable.
	 * 
	 * @param validate if validate state finally
	 * @param monitor progress monitor
	 * @return <code>true</code> if not yet validated or validated and modifiable,
	 *     otherwise <code>false</code> (finally not modifiable).
	 */
	boolean checkState(final boolean validate, final IProgressMonitor monitor);
	
	/**
	 * Access to the document with the content of this source unit
	 * 
	 * You must be connected to the source unit. The document object is shared 
	 * and reused as long one task is connect. 
	 * Document changes should be executed using {@link #syncExec(SourceDocumentRunnable)}.
	 * 
	 * @param monitor progress monitor (optional but recommended)
	 * @return the shared document
	 */
	AbstractDocument getDocument(final @Nullable IProgressMonitor monitor);
	
	/**
	 * Returns the information about partitioning and content sections types of the document for 
	 * the content type of this source unit.
	 * 
	 * @return the document content information
	 */
	DocContentSections getDocumentContentInfo();
	
	/**
	 * Returns the current stamp of the content of this source unit.
	 * 
	 * The stamp is identical with the stamp of the SourceContent returned by
	 * {@link #getContent(IProgressMonitor)}, or <code>0</code> if unkown.
	 * 
	 * @param monitor progress monitor (optional but recommended)
	 * @return the current stamp
	 */
	long getContentStamp(final @Nullable IProgressMonitor monitor);
	
	/**
	 * Access to the current content of this source unit.
	 * 
	 * The content represents a snapshot usually recreated by each access.
	 * @param monitor progress monitor (optional but recommended)
	 * @return the current content
	 */
	SourceContent getContent(final IProgressMonitor monitor);
	
	/**
	 * Runs {@link SourceDocumentRunnable} with checks (modification stamp) and 
	 * the required 'power' (thread, synch), if necessary. The calling thread is
	 * blocked (syncExec) until the runnable finished
	 * 
	 * For usual editor documents, this is equivalent running in Display thread and synchronize
	 * using {@link ISynchronizable#getLockObject()}, if possible).
	 * 
	 * @param runnable the runnable
	 * @throws InvocationTargetException forwarded from runnable
	 */
	void syncExec(final SourceDocumentRunnable runnable) throws InvocationTargetException;
	
	@Nullable AstInfo getAstInfo(final @Nullable String type, final boolean ensureSync,
			final IProgressMonitor monitor);
	
	@Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int flags,
			final IProgressMonitor monitor);
	
	@Override
	default @Nullable LtkModelElement<?> getModelParent() {
		return null;
	}
	
	@Override
	default boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super LtkModelElement<?>> filter) {
		return false;
	}
	
	@Override
	default List<? extends LtkModelElement<?>> getModelChildren(final @Nullable LtkModelElementFilter<? super LtkModelElement<?>> filter) {
		return ImCollections.emptyList();
	}
	
}
