/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.ISynchronizable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceDocumentRunnable;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


@NonNullByDefault
public abstract class GenericFragmentSourceUnit implements SourceUnit {
	
	
	private final ElementName name;
	
	private final SourceFragment fragment;
	private final long timestamp;
	
	private @Nullable AbstractDocument document;
	
	private int counter= 0;
	
	
	public GenericFragmentSourceUnit(final String id, final SourceFragment fragment) {
		this.name= new ElementName() {
			@Override
			public int getType() {
				return 0x011;
			}
			@Override
			public String getDisplayName() {
				return GenericFragmentSourceUnit.this.fragment.getName();
			}
			@Override
			public String getSegmentName() {
				return GenericFragmentSourceUnit.this.fragment.getName();
			}
			@Override
			public @Nullable ElementName getNextSegment() {
				return null;
			}
		};
		this.fragment= nonNullAssert(fragment);
		this.timestamp= System.currentTimeMillis();
	}
	
	
	@Override
	public @Nullable SourceUnit getUnderlyingUnit() {
		return null;
	}
	
	@Override
	public boolean isSynchronized() {
		return true;
	}
	
	@Override
	public String getId() {
		return this.fragment.getId();
	}
	
	public SourceFragment getFragment() {
		return this.fragment;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * A source unit of this type is usually of the type
	 * {@link LtkModelElement#C12_SOURCE_CHUNK C2_SOURCE_CHUNK}.
	 */
	@Override
	public int getElementType() {
		return LtkModelElement.C12_SOURCE_CHUNK;
	}
	
	@Override
	public ElementName getElementName() {
		return this.name;
	}
	
	@Override
	public boolean exists() {
		return this.counter > 0;
	}
	
	@Override
	public boolean isReadOnly() {
		return true;
	}
	
	@Override
	public boolean checkState(final boolean validate, final IProgressMonitor monitor) {
		return false;
	}
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * A source unit of this type is usually doesn't have a resource/path.
	 */
	@Override
	public @Nullable Object getResource() {
		return null;
	}
	
	
	@Override
	public synchronized AbstractDocument getDocument(final @Nullable IProgressMonitor monitor) {
		AbstractDocument document= this.document;
		if (document == null) {
			document= this.fragment.getDocument();
			this.document= document;
		}
		return document;
	}
	
	@Override
	public long getContentStamp(final @Nullable IProgressMonitor monitor) {
		return this.timestamp;
	}
	
	@Override
	public SourceContent getContent(final IProgressMonitor monitor) {
		final AbstractDocument document= getDocument(monitor);
		Object lockObject= null;
		if (document instanceof ISynchronizable) {
			lockObject= ((ISynchronizable)document).getLockObject();
		}
		if (lockObject == null) {
			lockObject= this.fragment;
		}
		synchronized (lockObject) {
			return new SourceContent(document.getModificationStamp(), document.get());
		}
	}
	
	@Override
	public void syncExec(final SourceDocumentRunnable runnable) throws InvocationTargetException {
		runnable.run();
	}
	
	@Override
	public @Nullable AstInfo getAstInfo(final @Nullable String type, final boolean ensureSync,
			final IProgressMonitor monitor) {
		return null;
	}
	
	@Override
	public @Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int syncLevel,
			final IProgressMonitor monitor) {
		return null;
	}
	
	
	@Override
	public synchronized final void connect(final IProgressMonitor monitor) {
		this.counter++;
		if (this.counter == 1) {
			final SubMonitor m= SubMonitor.convert(monitor, 1);
			register();
		}
	}
	
	@Override
	public synchronized final void disconnect(final IProgressMonitor monitor) {
		this.counter--;
		if (this.counter == 0) {
			unregister();
		}
	}
	
	@Override
	public synchronized boolean isConnected() {
		return (this.counter > 0);
	}
	
	protected void register() {
	}
	
	protected void unregister() {
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == SourceFragment.class) {
			return (T)this.fragment;
		}
		return null;
	}
	
	
	@Override
	public String toString() {
		return getModelTypeId() + '/' + getWorkingContext() + ": " + getId(); //$NON-NLS-1$
	}
	
}
