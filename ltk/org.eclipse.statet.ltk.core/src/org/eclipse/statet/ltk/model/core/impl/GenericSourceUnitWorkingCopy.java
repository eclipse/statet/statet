/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


/**
 * Generic source unit for working copies based on the same unit in the underlying context
 */
@NonNullByDefault
public abstract class GenericSourceUnitWorkingCopy implements SourceUnit {
	
	
	private final SourceUnit from;
	private WorkingBuffer buffer;
	
	private int counter= 0;
	
	
	/**
	 * Creates new working copy of the source unit
	 * 
	 * @param from the underlying unit to create a working copy from
	 */
	@SuppressWarnings("null")
	public GenericSourceUnitWorkingCopy(final SourceUnit from) {
		this.from= from;
	}
	
	
	@Override
	public final SourceUnit getUnderlyingUnit() {
		return this.from;
	}
	
	protected final WorkingBuffer getWorkingBuffer() {
		return this.buffer;
	}
	
	@Override
	public boolean isSynchronized() {
		return this.buffer.isSynchronized();
	}
	
	@Override
	public String getModelTypeId() {
		return this.from.getModelTypeId();
	}
	
	@Override
	public int getElementType() {
		return this.from.getElementType();
	}
	
	@Override
	public ElementName getElementName() {
		return this.from.getElementName();
	}
	
	@Override
	public String getId() {
		return this.from.getId();
	}
	
	@Override
	public boolean exists() {
		return this.counter > 0;
	}
	
	@Override
	public boolean isReadOnly() {
		return false;
	}
	
	@Override
	public boolean checkState(final boolean validate, final IProgressMonitor monitor) {
		return this.buffer.checkState(validate, monitor);
	}
	
	@Override
	public @Nullable Object getResource() {
		return this.from.getResource();
	}
	
	
	@Override
	public AbstractDocument getDocument(final @Nullable IProgressMonitor monitor) {
		return this.buffer.getDocument(monitor);
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return this.from.getDocumentContentInfo();
	}
	
	@Override
	public long getContentStamp(final @Nullable IProgressMonitor monitor) {
		return this.buffer.getContentStamp(monitor);
	}
	
	@Override
	public SourceContent getContent(final IProgressMonitor monitor) {
		return this.buffer.getContent(monitor);
	}
	
	
	@Override
	public @Nullable AstInfo getAstInfo(final @Nullable String type, final boolean ensureSync,
			final IProgressMonitor monitor) {
		return null;
	}
	
	@Override
	public @Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int syncLevel,
			final IProgressMonitor monitor) {
		return null;
	}
	
	
	@Override
	@SuppressWarnings("unused")
	public synchronized final void connect(final IProgressMonitor monitor) {
		this.counter++;
		if (this.counter == 1) {
			final SubMonitor m= SubMonitor.convert(monitor, 1);
			if (this.buffer == null) {
				m.setWorkRemaining(2);
				this.buffer= createWorkingBuffer(m.newChild(1));
			}
			register();
			this.from.connect(m.newChild(1));
		}
	}
	
	@Override
	public synchronized final void disconnect(final IProgressMonitor monitor) {
		this.counter--;
		if (this.counter == 0) {
			final SubMonitor m= SubMonitor.convert(monitor, 2);
			this.buffer.releaseDocument(m.newChild(1));
			unregister();
			this.from.disconnect(m.newChild(1));
		}
	}
	
	@Override
	public synchronized boolean isConnected() {
		return (this.counter > 0);
	}
	
	protected abstract WorkingBuffer createWorkingBuffer(SubMonitor m);
	
	protected void register() {
	}
	
	protected void unregister() {
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return this.from.getAdapter(adapterType);
	}
	
	
	@Override
	public String toString() {
		return getModelTypeId() + '/' + getWorkingContext() + ": " + getId(); //$NON-NLS-1$
	}
	
}
