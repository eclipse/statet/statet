/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.element;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;


/**
 * Represents Source structure instead of model structure
 */
@NonNullByDefault
public interface SourceStructElement<
				TModelChild extends LtkModelElement<?>,
				TSourceChild extends SourceStructElement<?, ?>>
		extends SourceElement<TModelChild> {
	
	
	@Nullable SourceStructElement<?, ?> getSourceParent();
	boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super TSourceChild> filter);
	List<? extends TSourceChild> getSourceChildren(final @Nullable LtkModelElementFilter<? super TSourceChild> filter);
	
	@Override
	TextRegion getSourceRange();
	
}
