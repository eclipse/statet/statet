/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.element;

import java.util.List;

import org.eclipse.core.runtime.IAdaptable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.ElementName;


/**
 * Interface for all types of model elements
 */
@NonNullByDefault
public interface LtkModelElement<TModelChild extends LtkModelElement<?>> extends IAdaptable {
	
	
	static final int SHIFT_C1=          8;
	static final int SHIFT_C2=          4;
	static final int SHIFT_C3=          0;
	static final int MASK_C1=           0xF << SHIFT_C1;
	static final int MASK_C12=          MASK_C1 | 0xF << SHIFT_C2;
	static final int MASK_C123=         MASK_C12 | 0xF << SHIFT_C3;
	
	static final int C1_BUNDLE=         0x1 << SHIFT_C1;
	static final int C1_SOURCE=         0x2 << SHIFT_C1;
	static final int C12_SOURCE_FILE=       C1_SOURCE | 0x1 << SHIFT_C2;
	static final int C12_SOURCE_CHUNK=      C1_SOURCE | 0x8 << SHIFT_C2;
	static final int C1_IMPORT=         0x3 << SHIFT_C1;
	static final int C1_CLASS=          0x4 << SHIFT_C1;
	static final int C1_METHOD=         0x5 << SHIFT_C1;
	static final int C1_VARIABLE=       0x6 << SHIFT_C1;
	static final int C1_EMBEDDED=       0x8 << SHIFT_C1;
	
	
	String getModelTypeId();
	
	int getElementType();
	ElementName getElementName();
	String getId();
	
	boolean exists();
	boolean isReadOnly();
	
	@Nullable LtkModelElement<?> getModelParent();
	boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter); // can also be used to visit children
	List<? extends TModelChild> getModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter);
	
	
}
