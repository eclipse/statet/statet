/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.issues.core.impl.ResourceMarkerIssueRequestor;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public class ResourceIssueSupport implements SourceUnitIssueSupport {
	
	
	private final IssueTypeSet issueTypeSet;
	
	
	public ResourceIssueSupport(final IssueTypeSet issueTypeSet) {
		this.issueTypeSet= issueTypeSet;
	}
	
	
	@Override
	public IssueTypeSet getIssueTypeSet() {
		return this.issueTypeSet;
	}
	
	@Override
	public void clearIssues(final SourceUnit sourceUnit) throws CoreException {
		ResourceMarkerIssueRequestor.clear(sourceUnit.getResource(), getIssueTypeSet());
	}
	
	@Override
	public @Nullable IssueRequestor createIssueRequestor(final SourceUnit sourceUnit) {
		return ResourceMarkerIssueRequestor.create(sourceUnit.getResource(), getIssueTypeSet());
	}
	
}
