/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.util;

import java.util.Comparator;

import com.ibm.icu.text.Collator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public class LtkModelElementComparator implements Comparator<LtkModelElement> {
	
	
	private final Collator unitIdComparator;
	private final Collator elementIdComparator;
	
	
	public LtkModelElementComparator() {
		this(Collator.getInstance());
	}
	
	public LtkModelElementComparator(final Collator elementIdComparator) {
		this.unitIdComparator= Collator.getInstance();
		this.elementIdComparator= elementIdComparator;
	}
	
	
	@Override
	public int compare(final @Nullable LtkModelElement e1, final @Nullable LtkModelElement e2) {
		if (e1 == e2) {
			return 0;
		}
		if (e1 == null) {
			return 1;
		}
		if (e2 == null) {
			return -1;
		}
		final SourceUnit u1= LtkModelUtils.getSourceUnit(e1);
		final SourceUnit u2= LtkModelUtils.getSourceUnit(e2);
		final int result= compareSourceUnits(u1, u2);
		if (result != 0) {
			return result;
		}
		if (e1 instanceof SourceElement) {
			if (e2 instanceof SourceElement) {
				return compareSourceElementsInUnit((SourceElement)e1, (SourceElement)e2);
			}
			return -1;
		}
		else if (e2 instanceof SourceElement) { // && !(e1 instanceof SourceUnit)
			return 1;
		}
		else {
			return this.elementIdComparator.compare(e1.getId(), e2.getId());
		}
	}
	
	protected int compareSourceUnits(final @Nullable SourceUnit u1, final @Nullable SourceUnit u2) {
		if (u1 == u2) {
			return 0;
		}
		if (u1 == null) {
			return 1;
		}
		if (u2 == null) {
			return -1;
		}
		return this.unitIdComparator.compare(u1.getId(), u2.getId());
	}
	
	protected int compareSourceElementsInUnit(final SourceElement e1, final SourceElement e2) {
		return e1.getSourceRange().getStartOffset() - e2.getSourceRange().getStartOffset();
	}
	
}
