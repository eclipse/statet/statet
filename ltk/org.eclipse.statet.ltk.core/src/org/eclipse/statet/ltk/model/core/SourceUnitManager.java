/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.content.IContentType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * Manages shared instances of source units.
 * 
 * The supported object types depends on the model and working context. Typically, for
 * the workspace context it is IFile, for the editor context an existing source unit
 * (a working copy is created) or IFileStore (URI).
 */
@NonNullByDefault
public interface SourceUnitManager {
	
	
	public @Nullable String getSourceUnitId(Object from);
	
	/**
	 * Returns the source unit for the given object in the specified LTK model and working context.
	 * 
	 * The returned source unit is already connected. If {@code from} is a source unit, it is the
	 * underlying unit of the returned unit and is not disconnected.
	 * 
	 * @param modelTypeId the model type id.
	 * @param context the working context.
	 * @param from the object to get the source unit for.
	 * @param monitor
	 * @return the source unit.
	 * @throws StatusException
	 */
	public SourceUnit getSourceUnit(String modelTypeId, WorkingContext context, Object from,
			IProgressMonitor monitor) throws StatusException;
	
	/**
	 * Returns the source unit for the given object in the specified LTK model (by the parent unit)
	 * and working context.
	 * 
	 * The returned source unit is already connected. The source unit provided by {@code from} is
	 * the underlying unit of the returned unit and is disconnected if requested (only when successful).
	 * 
	 * @param context the working context.
	 * @param from the object to get the source unit for.
	 * @param disconnectFrom {@code true} to disconnect from the underlying unit.
	 * @param monitor
	 * @return the source unit.
	 * @throws StatusException
	 */
	public SourceUnit getSourceUnit(WorkingContext context, SourceUnit from,
			boolean disconnectFrom,
			IProgressMonitor monitor) throws StatusException;
	
	/**
	 * Returns the source unit for the given object in the specified LTK model and working context.
	 * 
	 * The returned source unit is already connected. If {@code from} is a source unit, it is the
	 * underlying unit of the returned unit and is disconnected (only when successful).
	 * 
	 * @param context the working context.
	 * @param from the object to get the source unit for.
	 * @param contentType the content type or {@code null} to detect it automatically.
	 * @param create whether a new source unit object should be created, if it does not yet exists.
	 * @param monitor
	 * @return the source unit or {@code null}.
	 */
	@Nullable SourceUnit getSourceUnit(final WorkingContext context, final Object from,
			final @Nullable IContentType contentType,
			final boolean create, final IProgressMonitor monitor);
	
	/**
	 * Returns the open source unit for the given object in the specified LTK model and working context.
	 * 
	 * The returned source unit is <em>not</em> already connected.
	 * 
	 * @param modelTypeId the model type id.
	 * @param context the working context.
	 * @param from the object to get the source unit for.
	 * @return the source unit or {@code null} if it is not open.
	 */
	public @Nullable SourceUnit getOpenSourceUnit(String modelTypeId, WorkingContext context, Object from);
	
	/**
	 * Returns the open source unit for the given object in the default LTK model (automatically detected
	 * based on the given object) and specified working context.
	 * 
	 * The returned source unit is <em>not</em> already connected.
	 * 
	 * @param context the working context.
	 * @param from the object to get the source unit for.
	 * @return the source unit or {@code null} if it is not open.
	 */
	public @Nullable SourceUnit getOpenSourceUnit(WorkingContext context, Object from);
	
	public List<SourceUnit> getOpenSourceUnits(List<String> modelTypeIds,
			@Nullable WorkingContext context);
	public List<SourceUnit> getOpenSourceUnits(String modelTypeId,
			@Nullable WorkingContext context);
	public List<SourceUnit> getOpenSourceUnits(List<String> modelTypeIds,
			@Nullable WorkingContext context, String sourceUnitId);
	public List<SourceUnit> getOpenSourceUnits(List<String> modelTypeIds,
			@Nullable WorkingContext context, Object from);
	
}
