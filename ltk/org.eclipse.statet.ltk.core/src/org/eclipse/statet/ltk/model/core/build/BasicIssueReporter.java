/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.build;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.TaskIssueConfig;
import org.eclipse.statet.ltk.issues.core.impl.TaskTagReporter;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


@NonNullByDefault
public class BasicIssueReporter<TSourceUnit extends SourceUnit,
		TSourceUnitModelInfo extends SourceUnitModelInfo> {
	
	
	private final String categoryId;
	
	/* explicite configs */
	private @Nullable TaskIssueConfig taskIssueConfig;
	
	private boolean isReportProblemsEnabled;
	private boolean isReportTasksEnabled;
	
	
	public BasicIssueReporter(final String categoryId) {
		this.categoryId= categoryId;
	}
	
	
	protected void configure(final TaskIssueConfig taskIssueConfig) {
		this.taskIssueConfig= taskIssueConfig;
	}
	
	protected @Nullable TaskTagReporter getTaskReporter() {
		return null;
	}
	
	protected final boolean shouldReportProblems() {
		return this.isReportProblemsEnabled;
	}
	
	protected final boolean shouldReportTasks() {
		return this.isReportTasksEnabled;
	}
	
	protected void clear() {
	}
	
	
	public void run(final TSourceUnit sourceUnit, final TSourceUnitModelInfo modelInfo,
			final SourceContent content,
			final IssueRequestor requestor, final int level) {
		this.isReportProblemsEnabled= requestor.isInterestedInProblems(this.categoryId);
		final TaskTagReporter taskReporter= getTaskReporter();
		this.isReportTasksEnabled= (taskReporter != null && requestor.isInterestedInTasks());
		if (!(this.isReportProblemsEnabled || this.isReportTasksEnabled)) {
			return;
		}
		
		if (shouldReportTasks()) {
			var taskIssueConfig= this.taskIssueConfig;
			if (taskIssueConfig == null) {
				final var prefs= EPreferences.getContextPrefs(sourceUnit);
				taskIssueConfig= TaskIssueConfig.getConfig(prefs);
			}
			taskReporter.configure(taskIssueConfig);
		}
		
		try {
			runReporters(sourceUnit, modelInfo, content, requestor, level);
		}
		finally {
			clear();
		}
	}
	
	protected void runReporters(final TSourceUnit sourceUnit, final TSourceUnitModelInfo modelInfo,
			final SourceContent content,
			final IssueRequestor requestor, final int level) {
		final var element= modelInfo.getSourceElement();
		runReporters(sourceUnit, element.getAdapter(AstNode.class),
				modelInfo, content, requestor, level );
		
	}
	
	protected void runReporters(final TSourceUnit sourceUnit, final @Nullable AstNode node,
			final @Nullable TSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
	}
	
}
