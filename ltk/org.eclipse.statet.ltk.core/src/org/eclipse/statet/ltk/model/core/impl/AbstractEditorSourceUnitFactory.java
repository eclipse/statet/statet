/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import org.eclipse.core.filesystem.IFileStore;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.ltk.model.core.SourceUnitFactory;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;


/**
 * Abstract factory for {@link Ltk#EDITOR_CONTEXT}.
 */
@NonNullByDefault
public abstract class AbstractEditorSourceUnitFactory implements SourceUnitFactory {
	
	
	@Override
	public @Nullable SourceUnit createSourceUnit(final String id, final Object from) {
		if (from instanceof WorkspaceSourceUnit) {
			return createSourceUnit(id, (WorkspaceSourceUnit)from);
		}
		if (from instanceof IFileStore) {
			return createSourceUnit(id, (IFileStore)from);
		}
		if (from instanceof SourceFragment) {
			return createSourceUnit(id, (SourceFragment)from);
		}
		return null;
	}
	
	
	protected abstract @Nullable SourceUnit createSourceUnit(final String id, final WorkspaceSourceUnit su);
	
	protected abstract @Nullable SourceUnit createSourceUnit(final String id, final IFileStore file);
	
	protected @Nullable SourceUnit createSourceUnit(final String id, final SourceFragment fragment) {
		return null;
	}
	
}
