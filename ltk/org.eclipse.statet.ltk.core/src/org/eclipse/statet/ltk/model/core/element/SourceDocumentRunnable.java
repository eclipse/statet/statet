/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.element;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.DocumentRewriteSessionType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Runnable to execute a document operation in a special context.
 * 
 * @see SourceUnit#syncExec(SourceDocumentRunnable)
 */
@NonNullByDefault
public abstract class SourceDocumentRunnable {
	
	
	private AbstractDocument document;
	private long stamp;
	private final DocumentRewriteSessionType rewriteSessionType;
	
	
	public SourceDocumentRunnable(final AbstractDocument document, final long assertedStamp, final DocumentRewriteSessionType rewriteSessionType) {
		this.document= document;
		this.stamp= assertedStamp;
		this.rewriteSessionType= rewriteSessionType;
	}
	
	
	public final DocumentRewriteSessionType getRewriteSessionType() {
		return this.rewriteSessionType;
	}
	
	public final void setNext(final AbstractDocument document, final long assertedStamp) {
		this.document= document;
		this.stamp= assertedStamp;
	}
	
	public final AbstractDocument getDocument() {
		return this.document;
	}
	
	public final long getStampAssertion() {
		return this.stamp;
	}
	
	
	public abstract void run() throws InvocationTargetException;
	
}
