/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.ElementName;


@NonNullByDefault
public final class NameAccessAccumulator<TName extends ElementName> {
	
	
	private final String label;
	
	private List<TName> list;
	
	
	public NameAccessAccumulator(final String label) {
		this.label= nonNullAssert(label);
		this.list= new ArrayList<>(8);
	}
	
	
	public void finish() {
		this.list= ImCollections.toList(this.list);
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public List<TName> getList() {
		return this.list;
	}
	
}
