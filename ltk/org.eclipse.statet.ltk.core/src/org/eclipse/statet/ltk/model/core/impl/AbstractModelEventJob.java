/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayDeque;
import java.util.HashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ltk.core.LtkCorePlugin;
import org.eclipse.statet.ltk.core.LtkCore;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.ElementChangedEvent;
import org.eclipse.statet.ltk.model.core.ElementChangedListener;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementDelta;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


/**
 * Abstract model update event job
 */
@NonNullByDefault
public abstract class AbstractModelEventJob<TModelElement
		extends LtkModelElement<?>, InfoType extends SourceUnitModelInfo> extends Job {
	
	
	protected class Task {
		
		private final TModelElement element;
		private @Nullable InfoType oldInfo;
		private @Nullable InfoType newInfo;
		
		
		public Task(final TModelElement element) {
			this.element= element;
		}
		
		
		public TModelElement getElement() {
			return this.element;
		}
		
		public @Nullable InfoType getOldInfo() {
			return this.oldInfo;
		}
		
		public @Nullable InfoType getNewInfo() {
			return this.newInfo;
		}
		
		
		void run() {
			final LtkModelElementDelta delta= createDelta(this);
			fireDelta(delta);
		}
	}
	
	
	private final AbstractModelManager modelManager;
	
	private final Object tasksLock= new Object();
	private final ArrayDeque<TModelElement> taskQueue= new ArrayDeque<>();
	private final HashMap<TModelElement, Task> taskDetail= new HashMap<>();
	
	private boolean working= false;
	private boolean stop= false;
	
	
	public AbstractModelEventJob(final AbstractModelManager manager) {
		super("Model Events for " + manager.getModelTypeId()); //$NON-NLS-1$
		setPriority(BUILD);
		setSystem(true);
		setUser(false);
		
		this.modelManager= manager;
	}
	
	
	public void addUpdate(final TModelElement element,
			final @Nullable InfoType oldModel, final @Nullable InfoType newModel) {
		nonNullAssert(element);
		synchronized (this.tasksLock) {
			Task task= this.taskDetail.get(element);
			if (task == null) {
				task= new Task(element);
				task.oldInfo= oldModel;
				this.taskDetail.put(element, task);
			}
			else {
				this.taskQueue.removeFirstOccurrence(element);
			}
			task.newInfo= newModel;
			this.taskQueue.addLast(element);
			
			if (!this.working) {
				schedule();
			}
		}
	}
	
	protected LtkModelElementDelta createDelta(final Task task) {
		return new BasicModelElementDelta(ModelManager.MODEL_FILE,
				task.getElement(),
				task.getOldInfo(), task.getNewInfo() );
	}
	
	
	@Override
	protected IStatus run(final IProgressMonitor monitor) {
		try {
			while (true) {
				final Task task;
				synchronized (this.tasksLock) {
					final @Nullable TModelElement element= this.taskQueue.pollFirst();
					if (element == null || this.stop) {
						return Status.OK_STATUS;
					}
					this.working= true;
					task= nonNullAssert(this.taskDetail.remove(element));
				}
				try {
					task.run();
				}
				catch (final Throwable e) {
					LtkCorePlugin.log(new Status(IStatus.ERROR, LtkCore.BUNDLE_ID,
							String.format("An error occurred when firing model event for %1$s.", //$NON-NLS-1$
									this.modelManager.getModelTypeId() ),
							e ));
				}
			}
		}
		finally {
			this.working= false;
		}
	}
	
	protected void dispose() {
		synchronized (this.tasksLock) {
			this.stop= true;
			this.taskQueue.clear();
			this.taskDetail.clear();
		}
	}
	
	private void fireDelta(final LtkModelElementDelta delta) {
		final SourceUnit su= LtkModelUtils.getSourceUnit(delta.getModelElement());
		if (su == null) {
			return;
		}
		final WorkingContext context= su.getWorkingContext();
		final ElementChangedEvent event= new ElementChangedEvent(delta, context);
		final ImIdentityList<ElementChangedListener> listeners= this.modelManager.getElementChangedListeners(context);
		for (final ElementChangedListener listener : listeners) {
			try {
				listener.elementChanged(event);
			}
			catch (final Exception | LinkageError | AssertionError e) {
				LtkCorePlugin.log(new Status(IStatus.ERROR, LtkCore.BUNDLE_ID,
						String.format("An error occurred while handling a model element change for %1$s.", //$NON-NLS-1$
								this.modelManager.getModelTypeId() ),
						e ));
			}
		}
	}
	
}
