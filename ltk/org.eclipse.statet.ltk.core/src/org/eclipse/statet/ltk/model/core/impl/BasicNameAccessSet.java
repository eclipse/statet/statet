/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.NameAccess;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;


@NonNullByDefault
public final class BasicNameAccessSet<TNameAccess extends NameAccess<?, TNameAccess>>
		implements NameAccessSet<TNameAccess> {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static final BasicNameAccessSet EMPTY_SET= new BasicNameAccessSet<>(Collections.EMPTY_MAP);
	
	@SuppressWarnings("unchecked")
	public static final <TNameAccess extends NameAccess<?, TNameAccess>> BasicNameAccessSet<TNameAccess> emptySet() {
		return EMPTY_SET;
	}
	
	
	private final ImList<String> labelsSorted;
	
	private final Map<String, NameAccessAccumulator<TNameAccess>> map;
	
	
	public BasicNameAccessSet(final Map<String, NameAccessAccumulator<TNameAccess>> map) {
		final String[] labelArray= new @NonNull String[map.size()];
		int i= 0;
		for (final Map.Entry<String, NameAccessAccumulator<TNameAccess>> entry : map.entrySet()) {
			labelArray[i++]= entry.getKey();
			entry.getValue().finish();
		}
		Arrays.sort(labelArray);
		
		this.labelsSorted= ImCollections.newList(labelArray);
		this.map= map;
	}
	
	
	@Override
	public ImList<String> getNames() {
		return this.labelsSorted;
	}
	
	@Override
	public @Nullable ImList<TNameAccess> getAllInUnit(final String label) {
		final NameAccessAccumulator<TNameAccess> shared= this.map.get(label);
		return (shared != null) ? (ImList<TNameAccess>)shared.getList() : null;
	}
	
}
