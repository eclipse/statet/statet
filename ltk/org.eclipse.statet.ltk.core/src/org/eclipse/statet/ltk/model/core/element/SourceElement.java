/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.element;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;


/**
 * A element in a {@link SourceUnit}.
 */
@NonNullByDefault
public interface SourceElement<TModelChild extends LtkModelElement<?>>
		extends LtkModelElement<TModelChild> {
	
	
	SourceUnit getSourceUnit();
	
	
	@Nullable TextRegion getNameSourceRange();
	
	@Nullable TextRegion getSourceRange();
	
	@Nullable TextRegion getDocumentationRange();
	
}
