/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.ltk.core.source.SourceConfig;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;


@NonNullByDefault
public class BasicSourceModelStamp implements SourceModelStamp {
	
	
	private final long sourceStamp;
	
	private final ImList<SourceConfig> sourceConfigs;
	
	
	public BasicSourceModelStamp(final long sourceStamp,
			final ImList<SourceConfig> sourceConfigs) {
		this.sourceStamp= sourceStamp;
		this.sourceConfigs= nonNullAssert(sourceConfigs);
	}
	
	public BasicSourceModelStamp(final long sourceStamp) {
		this.sourceStamp= sourceStamp;
		this.sourceConfigs= ImCollections.emptyList();
	}
	
	
	@Override
	public final long getContentStamp() {
		return this.sourceStamp;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public final <T> @Nullable T getConfig(final Class<T> type) {
		for (final var sourceConfig : this.sourceConfigs) {
			if (type.isInstance(type)) {
				return (T)sourceConfig;
			}
		}
		return null;
	}
	
	
	@Override
	public int hashCode() {
		int h= (int)(this.sourceStamp ^ (this.sourceStamp >>> 32));
		h= 31 * h + this.sourceConfigs.hashCode();
		return h;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final BasicSourceModelStamp other
						&& getClass() == other.getClass()
						&& this.sourceStamp == other.sourceStamp
						&& this.sourceConfigs.equals(other.sourceConfigs) ));
	}
	
	
	@Override
	public String toString() {
		final var sb= new ToStringBuilder(BasicSourceModelStamp.class);
		sb.append(' ', this.sourceStamp);
		sb.addProp("sourceConfigs", this.sourceConfigs); //$NON-NLS-1$
		return sb.toString();
	}
	
}
