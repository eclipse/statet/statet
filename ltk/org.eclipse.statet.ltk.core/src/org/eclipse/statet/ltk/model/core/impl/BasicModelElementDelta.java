/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementDelta;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


/**
 * Currently not really a delta
 */
@NonNullByDefault
public class BasicModelElementDelta implements LtkModelElementDelta {
	
	
	private final int level;
	private final LtkModelElement element;
	private final @Nullable SourceUnitModelInfo oldInfo;
	private final @Nullable AstInfo oldAst;
	private final @Nullable SourceUnitModelInfo newInfo;
	private final @Nullable AstInfo newAst;
	
	
	public BasicModelElementDelta(final int level,
			final LtkModelElement element,
			final @Nullable SourceUnitModelInfo oldInfo, final @Nullable SourceUnitModelInfo newInfo) {
		this.level= level;
		this.element= element;
		this.oldInfo= oldInfo;
		this.oldAst= (oldInfo != null) ? oldInfo.getAst() : null;
		this.newInfo= newInfo;
		this.newAst= (newInfo != null) ? newInfo.getAst() : null;
	}
	
	
	@Override
	public LtkModelElement getModelElement() {
		return this.element;
	}
	
	@Override
	public @Nullable AstInfo getOldAst() {
		return this.oldAst;
	}
	
	@Override
	public @Nullable AstInfo getNewAst() {
		return this.newAst;
	}
	
}
