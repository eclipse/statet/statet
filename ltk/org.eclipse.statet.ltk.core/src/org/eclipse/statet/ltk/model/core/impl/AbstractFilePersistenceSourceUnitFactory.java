/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import static org.eclipse.statet.internal.ltk.core.DefaultSourceUnitIdFactory.WORKSPACE_PREFIX;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.SourceUnitFactory;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * Abstract factory for {@link Ltk#PERSISTENCE_CONTEXT}.
 */
@NonNullByDefault
public abstract class AbstractFilePersistenceSourceUnitFactory implements SourceUnitFactory {
	
	
	public AbstractFilePersistenceSourceUnitFactory() {
	}
	
	
	@Override
	public @Nullable SourceUnit createSourceUnit(final String id, final Object from) {
		if (from instanceof IFile) {
			return createSourceUnit(id, (IFile)from);
		}
		else if (id.startsWith(WORKSPACE_PREFIX)) {
			final IPath path= Path.fromPortableString(id.substring(WORKSPACE_PREFIX.length(), id.length()));
			return createSourceUnit(id, ResourcesPlugin.getWorkspace().getRoot().getFile(path));
		}
		else {
			return null;
		}
	}
	
	
	protected abstract SourceUnit createSourceUnit(final String id, final IFile file);
	
}
