/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.ContentStamp;
import org.eclipse.ltk.core.refactoring.TextChange;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.text.edits.UndoEdit;

import org.eclipse.statet.internal.ltk.core.LtkCorePlugin;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * A {@link TextChange} for Ltk {@link SourceUnit}s.
 */
public final class SourceUnitChange extends TextFileChange {
	
	
	private final SourceUnit sourceUnit;
	
	private Position insertPosition;
	
	/** The (optional) refactoring descriptor */
//	private ChangeDescriptor descriptor;
	
	
	/**
	 * Creates a new <code>CompilationUnitChange</code>.
	 * 
	 * @param name the change's name mainly used to render the change in the UI
	 * @param su the compilation unit this text change works on
	 */
	public SourceUnitChange(final SourceUnit su) {
		super(su.getElementName().getDisplayName(), (IFile)su.getResource());
		assert (su != null);
		this.sourceUnit= su;
		try {
			setTextType(getFile().getContentDescription().getContentType()
					.getFileSpecs(IContentType.FILE_EXTENSION_SPEC | IContentType.IGNORE_USER_DEFINED)[0]);
		} catch (final Exception e) {}
//		setTextType(ECommonsLTK.getExtContentTypeManager().getContentTypeForModelType(su.getModelTypeId()));
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getModifiedElement(){
		return this.sourceUnit;
	}
	
	/**
	 * Returns the source unit this change works on.
	 * 
	 * @return the source unit this change works on
	 */
	public SourceUnit getSourceUnit() {
		return this.sourceUnit;
	}
	
	public void setInsertPosition(final Position position) {
		this.insertPosition= new Position(position.getOffset(), 0);
	}
	
	public Position getInsertPosition() {
		return this.insertPosition;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected IDocument acquireDocument(final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 3);
		this.sourceUnit.connect(m.newChild(1));
		final IDocument document= super.acquireDocument(m.newChild(2));
		if (this.insertPosition != null) {
			this.insertPosition= new Position(this.insertPosition.getOffset(), document.getLength()-this.insertPosition.getOffset());
			try {
				document.addPosition(this.insertPosition);
			}
			catch (final BadLocationException e) {
				this.insertPosition= null;
			}
		}
		return document;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void releaseDocument(final IDocument document, final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 3);
		if (this.insertPosition != null) {
			document.removePosition(this.insertPosition);
		}
		super.releaseDocument(document, m.newChild(2));
		this.sourceUnit.disconnect(m.newChild(1));
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Change createUndoChange(final UndoEdit edit, final ContentStamp stampToRestore) {
		try {
			return new UndoSourceUnitChange(getName(), this.sourceUnit, (IFile)super.getModifiedElement(), edit, stampToRestore, getSaveMode());
		}
		catch (final CoreException e) {
			LtkCorePlugin.log(new Status(IStatus.ERROR, LtkCorePlugin.BUNDLE_ID, -1,
					"Failed to create Refactoring Undo", e )); //$NON-NLS-1$
			return null;
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Class<T> adapterType) {
		if (adapterType == SourceUnit.class || adapterType == LtkModelElement.class) {
			return (T) this.sourceUnit;
		}
		return super.getAdapter(adapterType);
	}
	
//	/**
//	 * Sets the refactoring descriptor for this change
//	 * 
//	 * @param descriptor the descriptor to set
//	 */
//	public void setDescriptor(final ChangeDescriptor descriptor) {
//		this.descriptor= descriptor;
//	}
//	
//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	public ChangeDescriptor getDescriptor() {
//		return this.descriptor;
//	}

}
