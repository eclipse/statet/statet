/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Common refactoring messages.
 */
@NonNullByDefault
@SuppressWarnings("null")
public class RefactoringMessages extends NLS {
	
	
	public static String Common_FinalCheck_label;
	public static String Common_CreateChanges_label;
	public static String Common_Source_Project_label;
	public static String Common_Source_Workspace_label;
	
	
	static {
		initializeMessages(RefactoringMessages.class.getName(), RefactoringMessages.class);
	}
	private RefactoringMessages() {}
	
}
