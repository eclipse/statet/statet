/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.mapping.IResourceChangeDescriptionFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.ResourceChangeChecker;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.internal.ltk.refactoring.core.Messages;


public abstract class CommonPasteCodeProcessor extends RefactoringProcessor {
	
	
	private final RefactoringAdapter adapter;
	
	private final String code;
	private final RefactoringDestination destination;
	
	private Change insertChange;
	
	
	public CommonPasteCodeProcessor(final String code, final RefactoringDestination destination,
			final RefactoringAdapter adapter) {
		assert (code != null);
		assert (destination != null);
		assert (adapter != null);
		
		this.destination= destination;
		this.code= code;
		
		this.adapter= adapter;
	}
	
	
	@Override
	public abstract String getIdentifier();
	
	protected abstract String getRefactoringIdentifier();
	
	@Override
	public String getProcessorName() {
		return Messages.PasteRefactoring_label; 
	}
	
	@Override
	public Object[] getElements() {
		return this.destination.getInitialObjects().toArray();
	}
	
	public Change getInsertChange() {
		return this.insertChange;
	}
	
	@Override
	public boolean isApplicable() throws CoreException {
		return this.adapter.canInsertTo(this.destination);
	}
	
	@Override
	public RefactoringStatus checkInitialConditions(final IProgressMonitor monitor)
			throws CoreException {
		final RefactoringStatus result= new RefactoringStatus();
		this.adapter.checkInitialToModify(result, this.destination);
		return result;
	}
	
	@Override
	public RefactoringStatus checkFinalConditions(final IProgressMonitor monitor,
			final CheckConditionsContext context)
			throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_FinalCheck_label,
				2 + 10 + 1 );
		try{
			final RefactoringStatus result= new RefactoringStatus();
			
			this.destination.postProcess();
			this.adapter.checkFinalToModify(result, this.destination, m.newChild(1));
			m.worked(1);
			
			final TextChangeManager textManager= new TextChangeManager();
			
			this.insertChange= this.adapter.createChangeToInsert(getProcessorName(),
					this.code, this.destination, textManager, m.newChild(10) );
			
			final ResourceChangeChecker checker= context.getChecker(ResourceChangeChecker.class);
			final IResourceChangeDescriptionFactory deltaFactory= checker.getDeltaFactory();
			this.adapter.buildDeltaToModify(this.destination, deltaFactory);
			
			return result;
		}
		catch (final OperationCanceledException e) {
			throw e;
		}
		finally{
			m.done();
		}
	}
	
	@Override
	public RefactoringParticipant[] loadParticipants(final RefactoringStatus status,
			final SharableParticipants shared)
			throws CoreException {
		final List<RefactoringParticipant> result= new ArrayList<>();
//		adapter.addParticipantsTo(this.elementsToDelete, result, status, this, shared);
		return result.toArray(new RefactoringParticipant[result.size()]);
	}
	
	@Override
	public Change createChange(final IProgressMonitor monitor) throws CoreException {
		try {
			monitor.beginTask(RefactoringMessages.Common_CreateChanges_label, 1);
			final Map<String, String> arguments= new HashMap<>();
			final String description= Messages.PasteRefactoring_Code_description;
			final IProject resource= this.destination.getSingleProject();
			final String project= (resource != null) ? resource.getName() : null;
			final String source= (project != null) ?
					NLS.bind(RefactoringMessages.Common_Source_Project_label, project) :
					RefactoringMessages.Common_Source_Workspace_label;
//			final String header= NLS.bind(RefactoringCoreMessages.JavaDeleteProcessor_header, new String[] { String.valueOf(this.elements.length), source});
//			final int flags= JavaRefactoringDescriptor.JAR_MIGRATION | JavaRefactoringDescriptor.JAR_REFACTORING | RefactoringDescriptor.STRUCTURAL_CHANGE | RefactoringDescriptor.MULTI_CHANGE;
//			final JDTRefactoringDescriptorComment comment= new JDTRefactoringDescriptorComment(project, this, header);
//			if (this.deleteSubPackages)
//				comment.addSetting(RefactoringCoreMessages.JavaDeleteProcessor_delete_subpackages);
//			if (this.accessorsDeleted)
//				comment.addSetting(RefactoringCoreMessages.JavaDeleteProcessor_delete_accessors);
//			arguments.put(ATTRIBUTE_DELETE_SUBPACKAGES, Boolean.valueOf(this.deleteSubPackages).toString());
//			arguments.put(ATTRIBUTE_SUGGEST_ACCESSORS, Boolean.valueOf(this.suggestGetterSetterDeletion).toString());
//			arguments.put(ATTRIBUTE_RESOURCES, new Integer(this.resources.length).toString());
//			for (int offset= 0; offset < this.resources.length; offset++)
//				arguments.put(JavaRefactoringDescriptorUtil.ATTRIBUTE_ELEMENT + (offset + 1), JavaRefactoringDescriptorUtil.resourceToHandle(project, this.resources[offset]));
//			arguments.put(ATTRIBUTE_ELEMENTS, new Integer(this.modelElements.length).toString());
//			for (int offset= 0; offset < this.modelElements.length; offset++)
//				arguments.put(JavaRefactoringDescriptorUtil.ATTRIBUTE_ELEMENT + (offset + this.resources.length + 1), JavaRefactoringDescriptorUtil.elementToHandle(project, this.modelElements[offset]));
			final int flags= 0;
			final String comment= ""; //$NON-NLS-1$
			
			final CommonRefactoringDescriptor descriptor= new CommonRefactoringDescriptor(
					getIdentifier(), project, description, comment, arguments, flags);
			
			return new DynamicValidationRefactoringChange(descriptor,
					getProcessorName(), 
					new Change[] { this.insertChange },
					null );
		}
		finally {
			monitor.done();
		}
	}
	
}
