/*=============================================================================#
 # Copyright (c) 2006, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.ChangeDescriptor;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringChangeDescriptor;
import org.eclipse.ltk.core.refactoring.RefactoringDescriptor;


/**
 * Change with support for refactoring descriptors.
 */
public final class RefactoringChange extends CompositeChange {
	
	
	/** The refactoring descriptor */
	private final RefactoringDescriptor descriptor;
	
	
	/**
	 * Creates a new dynamic validation refactoring change.
	 * 
	 * @param descriptor
	 *     the refactoring descriptor
	 * @param name
	 *     the name of the change
	 */
	public RefactoringChange(final RefactoringDescriptor descriptor, final String name) {
		super(name);
		assert (descriptor != null);
		this.descriptor= descriptor;
	}
	
	/**
	 * Creates a new dynamic validation refactoring change.
	 * 
	 * @param descriptor
	 *     the refactoring descriptor
	 * @param name
	 *     the name of the change
	 * @param changes
	 *     the changes
	 */
	public RefactoringChange(final RefactoringDescriptor descriptor, final String name, final Change[] changes) {
		super(name, changes);
		assert (descriptor != null);
		this.descriptor= descriptor;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ChangeDescriptor getDescriptor() {
		return new RefactoringChangeDescriptor(this.descriptor);
	}
	
}
