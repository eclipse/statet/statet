/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.ltk.core.refactoring.TextChange;
import org.eclipse.ltk.core.refactoring.TextFileChange;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * A <code>TextChangeManager</code> manages associations between <code>SourceUnit</code>
 * or <code>IFile</code> and <code>TextChange</code> objects.
 */
public class TextChangeManager {
	
	
	private final Map<SourceUnit, TextFileChange> map= new HashMap<>(10);
	
	private final boolean keepExecutedTextEdits;
	
	
	public TextChangeManager() {
		this(false);
	}
	
	public TextChangeManager(final boolean keepExecutedTextEdits) {
		this.keepExecutedTextEdits= keepExecutedTextEdits;
	}
	
	
//	/**
//	 * Adds an association between the given compilation unit and the passed
//	 * change to this manager.
//	 * 
//	 * @param cu the compilation unit (key)
//	 * @param change the change associated with the compilation unit
//	 */
//	public void manage(final SourceUnit su, final TextChange change) {
//		thi.map.put(su, change);
//	}
	
	/**
	 * Returns the <code>TextChange</code> associated with the given source unit.
	 * If the manager does not already manage an association it creates a one.
	 * 
	 * @param su the source unit for which the text buffer change is requested
	 * @return the text change associated with the given source unit. 
	 */
	public TextFileChange get(final SourceUnit su) {
		TextFileChange result= this.map.get(su);
		if (result == null) {
			result= new SourceUnitChange(su);
			result.setKeepPreviewEdits(this.keepExecutedTextEdits);
			this.map.put(su, result);
		}
		return result;
	}
	
	/**
	 * Removes the <tt>TextChange</tt> managed under the given key
	 * <code>unit<code>.
	 * 
	 * @param unit the key determining the <tt>TextChange</tt> to be removed.
	 * @return the removed <tt>TextChange</tt>.
	 */
	public TextChange remove(final SourceUnit unit) {
		return this.map.remove(unit);
	}
	
	/**
	 * Returns all source units managed by this instance.
	 * 
	 * @return all source units managed by this instance
	 */	
	public SourceUnit[] getAllSourceUnits(){
		return this.map.keySet().toArray(new SourceUnit[this.map.keySet().size()]);
	}
	
	/**
	 * Returns all text changes managed by this instance.
	 * 
	 * @return all text changes managed by this instance
	 */
	public TextChange[] getAllChanges(){
		final Set<SourceUnit> suSet= this.map.keySet();
		final SourceUnit[] sus= suSet.toArray(new SourceUnit[suSet.size()]);
		Arrays.sort(sus, new Comparator<SourceUnit>() {
			@Override
			public int compare(final SourceUnit su1, final SourceUnit su2) {
				return su1.getId().compareTo(su2.getId());
			}
		});
		
		final TextChange[] textChanges= new TextChange[sus.length];
		for (int i= 0; i < sus.length; i++) {
			textChanges[i]= this.map.get(sus[i]);
		}
		return textChanges;
	}
	
	/**
	 * Returns if any text changes are managed for the specified source unit.
	 * 
	 * @param su the source unit
	 * @return <code>true</code> if any text changes are managed for the specified source unit and <code>false</code> otherwise
	 */
	public boolean containsChangesIn(final SourceUnit su){
		return this.map.containsKey(su);
	}
	
	/**
	 * Clears all associations between resources and text changes.
	 */
	public void clear() {
		this.map.clear();
	}
	
}
