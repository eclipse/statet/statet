/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.model.core.element.SourceElement;


public class RefactoringDestination extends ElementSet {
	
	
	public static enum Position {
		
		ABOVE,
		INTO,
		BELOW,
		AT,
		
	}
	
	
	private final Position position;
	private int offset;
	
	
	public RefactoringDestination(final Object element) {
		this(element, Position.INTO);
	}
	
	public RefactoringDestination(final Object element, final Position pos) {
		super(element);
		this.position= pos;
	}
	
	public RefactoringDestination(final SourceElement element, final int offset) {
		super(element);
		this.position= Position.AT;
		this.offset= offset;
	}
	
	
	public Position getPosition() {
		return this.position;
	}
	
	public int getOffset() {
		return this.offset;
	}
	
}
