/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ltk.core.refactoring.Refactoring;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class LtkRefactoring extends Refactoring {
	
	
	public abstract String getBundleId();
	
	public abstract String getIdentifier();
	
	
	protected CoreException handleUnexpectedError(final Throwable e) {
		return new CoreException(new Status(IStatus.ERROR, getBundleId(),
				"Unexpected error (concurrent change?)",
				e ));
	}
	
}
