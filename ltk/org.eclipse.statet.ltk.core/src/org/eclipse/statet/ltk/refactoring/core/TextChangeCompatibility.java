/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import java.util.List;

import org.eclipse.ltk.core.refactoring.CategorizedTextEditGroup;
import org.eclipse.ltk.core.refactoring.GroupCategorySet;
import org.eclipse.ltk.core.refactoring.TextChange;
import org.eclipse.ltk.core.refactoring.TextEditChangeGroup;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.RangeMarker;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.text.edits.TextEditGroup;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * A utility class to provide compatibility with the old text change API of adding text edits
 * directly and auto inserting them into the tree.
 */
@NonNullByDefault
public class TextChangeCompatibility {
	
	
	public static void addTextEdits(final TextChange change, final String name, final List<TextEdit> edits,
			final boolean enable)
			throws MalformedTreeException {
		switch (edits.size()) {
		case 0:
			return;
		case 1:
			addTextEdit1(change, name, edits.get(0), enable);
			return;
		default:
			addTextEdits1(change, name, edits.toArray(new @NonNull TextEdit[edits.size()]), enable);
			return;
		}
	}
	
	public static void addTextEdits(final TextChange change, final String name, final List<TextEdit> edits)
			throws MalformedTreeException {
		switch (edits.size()) {
		case 0:
			return;
		case 1:
			addTextEdit1(change, name, edits.get(0), true);
			return;
		default:
			addTextEdits1(change, name, edits.toArray(new @NonNull TextEdit[edits.size()]), true);
			return;
		}
	}
	
	public static void addTextEdit(final TextChange change, final String name, final TextEdit edit,
			final boolean enable)
			throws MalformedTreeException {
		addTextEdit1(change, name, edit, enable);
	}
	
	public static void addTextEdit(final TextChange change, final String name, final TextEdit edit)
			throws MalformedTreeException {
		addTextEdit1(change, name, edit, true);
	}
	
	private static void addTextEdits1(final TextChange change, final String name, final @NonNull TextEdit[] edits,
			final boolean enable)
			throws MalformedTreeException {
		final TextEditGroup group;
		final TextEdit root= getRoot(change);
		for (int i= 0; i < edits.length; i++) {
			insert(root, edits[i]);
		}
		group= new TextEditGroup(name, edits);
		
		final TextEditChangeGroup changeGroup= new TextEditChangeGroup(change, group);
		changeGroup.setEnabled(enable);
		change.addTextEditChangeGroup(changeGroup);
	}
	
	private static void addTextEdit1(final TextChange change, final String name, final TextEdit edit,
			final boolean enable)
			throws MalformedTreeException {
		insert(getRoot(change), edit);
		final TextEditGroup group= new TextEditGroup(name, edit);
		
		final TextEditChangeGroup changeGroup= new TextEditChangeGroup(change, group);
		changeGroup.setEnabled(enable);
		change.addTextEditChangeGroup(changeGroup);
	}
	
	public static void addTextEdit(final TextChange change, final String name, final TextEdit edit,
			final GroupCategorySet groupCategories)
			throws MalformedTreeException {
		insert(getRoot(change), edit);
		final TextEditGroup group= new CategorizedTextEditGroup(name, edit, groupCategories);
		
		final TextEditChangeGroup changeGroup= new TextEditChangeGroup(change, group);
		change.addTextEditChangeGroup(changeGroup);
	}
	
	public static void addMarker(final TextChange change, final RangeMarker marker)
			throws MalformedTreeException {
		insert(getRoot(change), marker);
	}
	
	
	public static void insert(final TextEdit parent, final TextEdit edit)
			throws MalformedTreeException {
		if (!parent.hasChildren()) {
			parent.addChild(edit);
			return;
		}
		final var children= parent.getChildren();
		// First dive down to find the right parent.
		for (int i= 0; i < children.length; i++) {
			final TextEdit child= children[i];
			if (covers(child, edit)) {
				insert(child, edit);
				return;
			}
		}
		// We have the right parent. Now check if some of the children have to
		// be moved under the new edit since it is covering it.
		int removed= 0;
		for (int i= 0; i < children.length; i++) {
			final TextEdit child= children[i];
			if (covers(edit, child)) {
				parent.removeChild(i - removed++);
				edit.addChild(child);
			}
		}
		parent.addChild(edit);
	}
	
	
	private static TextEdit getRoot(final TextChange change) {
		TextEdit root= change.getEdit();
		if (root == null) {
			root= new MultiTextEdit();
			change.setEdit(root);
		}
		return root;
	}
	
	private static boolean covers(final TextEdit thisEdit, final TextEdit otherEdit) {
		if (thisEdit.getLength() == 0) {	// an insertion point can't cover anything
			return false;
		}
		final int thisOffset= thisEdit.getOffset();
		final int thisEnd= thisEdit.getExclusiveEnd();
		if (otherEdit.getLength() == 0) {
			final int otherOffset= otherEdit.getOffset();
			return (thisOffset < otherOffset && otherOffset < thisEnd);
		}
		else {
			final int otherOffset= otherEdit.getOffset();
			final int otherEnd= otherEdit.getExclusiveEnd();
			return (thisOffset <= otherOffset && otherEnd <= thisEnd);
		}
	}
	
}
