/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.ContentStamp;
import org.eclipse.ltk.core.refactoring.UndoTextFileChange;
import org.eclipse.text.edits.UndoEdit;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * Undo variant of {@link SourceUnitChange}
 */
public class UndoSourceUnitChange extends UndoTextFileChange {
	
	
	private final SourceUnit sourceUnit;
	
	
	public UndoSourceUnitChange(final String name, final SourceUnit su, final IFile file, 
			final UndoEdit undo, final ContentStamp stampToRestore, final int saveMode) throws CoreException {
		super(name, file, undo, stampToRestore, saveMode);
		this.sourceUnit= su;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getModifiedElement() {
		return this.sourceUnit;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Change createUndoChange(final UndoEdit edit, final ContentStamp stampToRestore) throws CoreException {
		return new UndoSourceUnitChange(getName(), this.sourceUnit, (IFile)super.getModifiedElement(), edit, stampToRestore, getSaveMode());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Change perform(final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 1 + 8 + 1);
		this.sourceUnit.connect(m.newChild(1));
		try {
			return super.perform(m.newChild(8));
		}
		finally {
			this.sourceUnit.disconnect(m.newChild(1));
		}
	}
	
}
