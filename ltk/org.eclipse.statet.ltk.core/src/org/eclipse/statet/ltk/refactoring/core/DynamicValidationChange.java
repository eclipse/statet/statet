/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import static org.eclipse.statet.jcommons.util.Units.MINUTE_SECOND;
import static org.eclipse.statet.jcommons.util.Units.NANO_FACTOR;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import org.eclipse.statet.internal.ltk.refactoring.core.Messages;


public class DynamicValidationChange extends CompositeChange implements IResourceChangeListener {
	
	// 30 minutes
	private static final long LIFE_TIME= (long)30 * MINUTE_SECOND * NANO_FACTOR;
	
	
	private RefactoringStatus validationState= null;
	private long timeStamp;
	
	
	public DynamicValidationChange(final Change change) {
		super(change.getName());
		add(change);
		markAsSynthetic();
	}
	
	public DynamicValidationChange(final String name) {
		super(name);
		markAsSynthetic();
	}
	
	public DynamicValidationChange(final String name, final Change[] changes) {
		super(name, changes);
		markAsSynthetic();
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initializeValidationData(final IProgressMonitor pm) {
		super.initializeValidationData(pm);
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
		this.timeStamp= System.nanoTime();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		super.dispose();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public RefactoringStatus isValid(final IProgressMonitor pm) throws CoreException {
		if (this.validationState == null) {
			return super.isValid(pm);
		}
		return this.validationState;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Change createUndoChange(final Change[] childUndos) {
		final DynamicValidationChange result= new DynamicValidationChange(getName());
		for (int i= 0; i < childUndos.length; i++) {
			result.add(childUndos[i]);
		}
		return result;
	}
	
	@Override
	public void resourceChanged(final IResourceChangeEvent event) {
		if (System.nanoTime() - this.timeStamp < LIFE_TIME) {
			return;
		}
		this.validationState= RefactoringStatus.createFatalErrorStatus(
				Messages.DynamicValidationState_WorkspaceChanged_message);
		
		// remove listener from workspace tracker
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		
		// clear up the children to not hang onto too much memory
		final Change[] children= clear();
		for (int i= 0; i < children.length; i++) {
			final Change change= children[i];
			SafeRunner.run(new ISafeRunnable() {
				@Override
				public void run() throws Exception {
					change.dispose();
				}
				@Override
				public void handleException(final Throwable exception) {
				}
			});
		}
	}
	
}
