/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import org.eclipse.core.runtime.jobs.ISchedulingRule;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Offers optionally an scheduling rule to use when performing the refactoring.
 */
@NonNullByDefault
public interface ScheduledRefactoring {
	
	
	/**
	 * The scheduling rule used to perform the
	 * refactoring.
	 * 
	 * @return {@link ISchedulingRule} not null
	 */
	public ISchedulingRule getSchedulingRule();
	
}
