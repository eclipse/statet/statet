/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.mapping.IResourceChangeDescriptionFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.MoveProcessor;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.ReorgExecutionLog;
import org.eclipse.ltk.core.refactoring.participants.ResourceChangeChecker;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.internal.ltk.refactoring.core.Messages;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;


public abstract class CommonMoveProcessor extends MoveProcessor {
	
	
	private final RefactoringAdapter adapter;
	
	private final ElementSet elementsToMove;
	private final RefactoringDestination destination;
	
	private Change moveChange;
	private ReorgExecutionLog executionLog;
	
	
	public CommonMoveProcessor(final ElementSet elements, final RefactoringDestination destination,
			final RefactoringAdapter adapter) {
		assert (elements != null);
		assert (adapter != null);
		
		this.elementsToMove= elements;
		this.destination= destination;
		if (destination.getInitialObjects().size() != 1
				|| !(destination.getInitialObjects().get(0) instanceof LtkModelElement) ) {
			throw new IllegalArgumentException();
		}
		this.adapter= adapter;
	}
	
	
	@Override
	public abstract String getIdentifier();
	
	protected abstract String getRefactoringIdentifier();
	
	
	@Override
	public Object[] getElements() {
		final Object[] elements= this.elementsToMove.getInitialObjects().toArray(
				new Object[this.elementsToMove.getInitialObjects().size() + 1] );
		elements[elements.length - 1]= this.destination.getInitialObjects().get(0);
		return elements;
	}
	
	@Override
	public String getProcessorName() {
		return Messages.MoveRefactoring_label; 
	}
	
	@Override
	public boolean isApplicable() throws CoreException {
		return (this.adapter.canInsert(this.elementsToMove, this.destination)
				&& this.adapter.canDelete(this.elementsToMove) );
	}
	
	@Override
	public RefactoringStatus checkInitialConditions(final IProgressMonitor monitor)
			throws CoreException {
		final RefactoringStatus result= new RefactoringStatus();
		this.adapter.checkInitialToModify(result, this.destination);
		this.adapter.checkInitialToModify(result, this.elementsToMove);
		return result;
	}
	
	@Override
	public RefactoringStatus checkFinalConditions(final IProgressMonitor monitor,
			final CheckConditionsContext context)
			throws CoreException, OperationCanceledException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_FinalCheck_label,
				2 + 10 + 1 );
		try{
			final RefactoringStatus result= new RefactoringStatus();
			
			this.elementsToMove.removeElementsWithAncestorsOnList();
			
			this.elementsToMove.postProcess();
			this.adapter.checkFinalToDelete(result, this.elementsToMove);
			this.adapter.checkFinalToModify(result, this.destination, m.newChild(1));
			m.worked(1);
			
			final TextChangeManager textManager= new TextChangeManager();
			
			this.moveChange= this.adapter.createChangeToMove(getProcessorName(),
					this.elementsToMove, this.destination, textManager, m.newChild(10) );
			
			final ResourceChangeChecker checker= context.getChecker(ResourceChangeChecker.class);
			final IResourceChangeDescriptionFactory deltaFactory= checker.getDeltaFactory();
			this.adapter.buildDeltaToModify(this.elementsToMove, deltaFactory);
			
			return result;
		}
		catch (final OperationCanceledException e) {
			throw e;
		}
		finally{
			m.done();
		}
	}
	
	@Override
	public RefactoringParticipant[] loadParticipants(final RefactoringStatus status,
			final SharableParticipants shared)
			throws CoreException {
		final List<RefactoringParticipant> result= new ArrayList<>();
		this.executionLog= new ReorgExecutionLog();
		this.adapter.addParticipantsToMove(this.elementsToMove, this.destination, result, status, this,
				shared, this.executionLog );
		return result.toArray(new RefactoringParticipant[result.size()]);
	}
	
	@Override
	public Change createChange(final IProgressMonitor monitor)
			throws CoreException, OperationCanceledException {
		try {
			monitor.beginTask(RefactoringMessages.Common_CreateChanges_label, 1);
			final Map<String, String> arguments= new HashMap<>();
			final String description= (this.elementsToMove.getElementCount() == 1) ? 
					Messages.MoveRefactoring_description_singular : Messages.MoveRefactoring_description_plural;
			final IProject resource= this.elementsToMove.getSingleProject();
			final String project= (resource != null) ? resource.getName() : null;
			final String source= (project != null) ?
					NLS.bind(RefactoringMessages.Common_Source_Project_label, project) :
					RefactoringMessages.Common_Source_Workspace_label;
//			final String header= NLS.bind(RefactoringCoreMessages.JavaDeleteProcessor_header, new String[] { String.valueOf(this.elements.length), source});
//			final int flags= JavaRefactoringDescriptor.JAR_MIGRATION | JavaRefactoringDescriptor.JAR_REFACTORING | RefactoringDescriptor.STRUCTURAL_CHANGE | RefactoringDescriptor.MULTI_CHANGE;
//			final JDTRefactoringDescriptorComment comment= new JDTRefactoringDescriptorComment(project, this, header);
//			if (this.deleteSubPackages)
//				comment.addSetting(RefactoringCoreMessages.JavaDeleteProcessor_delete_subpackages);
//			if (this.accessorsDeleted)
//				comment.addSetting(RefactoringCoreMessages.JavaDeleteProcessor_delete_accessors);
//			arguments.put(ATTRIBUTE_DELETE_SUBPACKAGES, Boolean.valueOf(this.deleteSubPackages).toString());
//			arguments.put(ATTRIBUTE_SUGGEST_ACCESSORS, Boolean.valueOf(this.suggestGetterSetterDeletion).toString());
//			arguments.put(ATTRIBUTE_RESOURCES, new Integer(this.resources.length).toString());
//			for (int offset= 0; offset < this.resources.length; offset++)
//				arguments.put(JavaRefactoringDescriptorUtil.ATTRIBUTE_ELEMENT + (offset + 1), JavaRefactoringDescriptorUtil.resourceToHandle(project, this.resources[offset]));
//			arguments.put(ATTRIBUTE_ELEMENTS, new Integer(this.modelElements.length).toString());
//			for (int offset= 0; offset < this.modelElements.length; offset++)
//				arguments.put(JavaRefactoringDescriptorUtil.ATTRIBUTE_ELEMENT + (offset + this.resources.length + 1), JavaRefactoringDescriptorUtil.elementToHandle(project, this.modelElements[offset]));
			final int flags= 0;
			final String comment= ""; //$NON-NLS-1$
			
			final CommonRefactoringDescriptor descriptor= new CommonRefactoringDescriptor(
					getIdentifier(), project, description, comment, arguments, flags);
			
			return new DynamicValidationRefactoringChange(descriptor,
					Messages.MoveRefactoring_label, 
					new Change[] { this.moveChange },
					this.executionLog );
		}
		finally {
			monitor.done();
		}
	}
	
}
