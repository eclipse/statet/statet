/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.refactoring.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.resources.mapping.IResourceChangeDescriptionFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CopyArguments;
import org.eclipse.ltk.core.refactoring.participants.CopyParticipant;
import org.eclipse.ltk.core.refactoring.participants.DeleteArguments;
import org.eclipse.ltk.core.refactoring.participants.DeleteParticipant;
import org.eclipse.ltk.core.refactoring.participants.MoveArguments;
import org.eclipse.ltk.core.refactoring.participants.MoveParticipant;
import org.eclipse.ltk.core.refactoring.participants.ParticipantManager;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.ReorgExecutionLog;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;
import org.eclipse.ltk.core.refactoring.resource.DeleteResourceChange;
import org.eclipse.ltk.core.refactoring.resource.Resources;
import org.eclipse.osgi.util.NLS;
import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.MStringAUtils;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.util.StringUtils;

import org.eclipse.statet.ecommons.io.FileUtil;

import org.eclipse.statet.internal.ltk.refactoring.core.Messages;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.LtkCore;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.core.util.LtkModelElementComparator;
import org.eclipse.statet.ltk.text.core.HeuristicTokenScanner;


/**
 * Provides common functions for refacotring. 
 * Can be extended to adapt to language specific peculiarity.
 */
public abstract class RefactoringAdapter {
	
	
	private static final Comparator<LtkModelElement> MODELELEMENT_SORTER= new LtkModelElementComparator();
	
	
	private final String modelTypeId;
	
	
	public RefactoringAdapter(final String modelTypeId) {
		this.modelTypeId= modelTypeId;
	}
	
	
	public Comparator<LtkModelElement> getModelElementComparator() {
		return MODELELEMENT_SORTER;
	}
	
	public String getModelTypeId() {
		return this.modelTypeId;
	}
	
	public boolean isSupportedModelType(final String typeId) {
		return (this.modelTypeId == typeId);
	}
	
	public abstract String getPluginIdentifier();
	
	public abstract boolean isCommentContent(final ITypedRegion partition);
	
	public abstract HeuristicTokenScanner getScanner(final SourceUnit su);
	
	/**
	 * - Sort elements
	 * - Removes nested children.
	 * 
	 * @param elements must be sorted by unit and order
	 * @return
	 */
	public SourceStructElement[] checkElements(final SourceStructElement[] elements) {
		if (elements.length <= 1) {
			return elements;
		}
		Arrays.sort(elements, getModelElementComparator());
		SourceStructElement last= elements[0];
		SourceUnit unitOfLast= last.getSourceUnit();
		int endOfLast= last.getSourceRange().getEndOffset();
		final List<SourceStructElement> checked= new ArrayList<>(elements.length);
		for (final SourceStructElement element : elements) {
			final SourceUnit unit= element.getSourceUnit();
			final int end= last.getSourceRange().getEndOffset();
			if (unit != unitOfLast) {
				checked.add(element);
				last= element;
				unitOfLast= unit;
				endOfLast= end;
				continue;
			}
			if (end > endOfLast) {
				checked.add(element);
				last= element;
				endOfLast= end;
				continue;
			}
			// is child, ignore
			continue;
		}
		return checked.toArray(new SourceStructElement[checked.size()]);
	}
	
	public @Nullable IRegion getContinuousSourceRange(final List<SourceStructElement<?, ?>> elements) {
		if (elements == null || elements.isEmpty()) {
			return null;
		}
		final var element0= elements.get(0);
		final SourceUnit su= element0.getSourceUnit();
		if (su == null) {
			return null;
		}
		final AbstractDocument doc= su.getDocument(null);
		if (doc == null) {
			return null;
		}
		
		// check if no other code is between the elements
		// and create one single range including comments at line end
		try {
			final HeuristicTokenScanner scanner= getScanner(su);
			scanner.configure(doc);
			final int start= element0.getSourceRange().getStartOffset();
			int end= element0.getSourceRange().getEndOffset();
			
			for (int i= 1; i < elements.size(); i++) {
				final var element= elements.get(i);
				if (element.getSourceUnit() != su) {
					return null;
				}
				final int elementStart= element.getSourceRange().getStartOffset();
				final int elementEnd= elementStart + element.getSourceRange().getLength();
				if (elementEnd <= end) {
					continue;
				}
				int match;
				while (end < elementStart &&
						(match= scanner.findAnyNonMSpaceForward(end, elementStart)) >= 0) {
					final ITypedRegion partition= doc.getPartition(scanner.getDocumentPartitioning(), match, false);
					if (isCommentContent(partition)) {
						end= partition.getOffset() + partition.getLength();
					}
					else {
						return null;
					}
				}
				end= elementEnd;
			}
			final IRegion lastLine= doc.getLineInformationOfOffset(end);
			final int match= scanner.findAnyNonMSpaceForward(end, lastLine.getOffset() + lastLine.getLength());
			if (match >= 0) {
				final ITypedRegion partition= doc.getPartition(scanner.getDocumentPartitioning(), match, false);
				if (isCommentContent(partition)) {
					end= partition.getOffset() + partition.getLength();
				}
			}
			return new Region(start, end-start);
		}
		catch (final BadPartitioningException | BadLocationException e) {
		}
		return null;
	}
	
	public String getSourceCodeStringedTogether(final SourceStructElement[] sourceElements,
			final IProgressMonitor monitor) throws CoreException {
		return getSourceCodeStringedTogether(new ElementSet((Object[]) sourceElements), monitor);
	}
	
	public String getSourceCodeStringedTogether(final ElementSet sourceElements,
			final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor);
		SourceUnit lastUnit= null;
		HeuristicTokenScanner scanner= null;
		try {
			sourceElements.removeElementsWithAncestorsOnList();
			Collections.sort(sourceElements.getModelElements(), getModelElementComparator());
			
			AbstractDocument doc= null;
			final List<LtkModelElement<?>> modelElements= sourceElements.getModelElements();
			
			int workRemaining= modelElements.size() * 3 + 1;
			
			final StringBuilder sb= new StringBuilder(modelElements.size() * 100);
			final List<String> codeFragments= new ArrayList<>();
			for (final LtkModelElement<?> element : modelElements) {
				m.setWorkRemaining(workRemaining); workRemaining-= 3;
				
				final SourceUnit su= LtkModelUtils.getSourceUnit(element);
				if (su != lastUnit) {
					if (lastUnit != null) {
						lastUnit.disconnect(m.newChild(1));
						lastUnit= null;
					}
					su.connect(m.newChild(1));
					lastUnit= su;
					scanner= getScanner(su);
					doc= su.getDocument(monitor);
				}
				getSourceCode((SourceElement)element, doc, scanner, codeFragments);
				String lastFragment= null;
				for (int i= 0; i < codeFragments.size(); i++) {
					final String s= codeFragments.get(i);
					if (s.isEmpty()) {
						continue;
					}
					sb.append(lastFragment= s);
					if (s.charAt(s.length() - 1) != '\n') {
						sb.append(doc.getDefaultLineDelimiter());
					}
				}
				if (lastFragment != null && !isEmptyLine(MStringAUtils.getLastLine(lastFragment))
						&& shouldAppendEmptyLine((SourceElement)element) ) {
					sb.append(doc.getDefaultLineDelimiter());
				}
				codeFragments.clear();
				m.worked(1);
			}
			
			return sb.toString();
		}
		catch (final BadLocationException | BadPartitioningException e) {
			throw new CoreException(failDocAnalyzation(e));
		}
		finally {
			if (lastUnit != null) {
				m.setWorkRemaining(1);
				lastUnit.disconnect(m.newChild(1));
				lastUnit= null;
			}
		}
	}
	
	protected boolean shouldExpandEmptyLine(final SourceElement element) {
		return true;
	}
	
	protected boolean shouldAppendEmptyLine(final SourceElement element) {
		return true;
	}
	
	protected boolean isEmptyLine(final String s) {
		return StringUtils.isTrimEmpty(s);
	}
	
	protected void getSourceCode(final SourceElement element, final AbstractDocument doc,
			final HeuristicTokenScanner scanner, final List<String> codeFragments)
			throws BadLocationException, BadPartitioningException {
		final TextRegion range= expandElementRange(element, doc, scanner);
		if (range != null && range.getLength() > 0) {
			codeFragments.add(doc.get(range.getStartOffset(), range.getLength()));
		}
	}
	
	public TextRegion expandElementRange(final SourceElement element, final AbstractDocument document,
			final HeuristicTokenScanner scanner)
			throws BadLocationException, BadPartitioningException {
		final TextRegion sourceRange= element.getSourceRange();
		int start= sourceRange.getStartOffset();
		int end= start + sourceRange.getLength();
		
		final TextRegion docRange= element.getDocumentationRange();
		if (docRange != null) {
			if (docRange.getStartOffset() < start) {
				start= docRange.getStartOffset();
			}
			if (docRange.getEndOffset() > end) {
				end= docRange.getEndOffset();
			}
		}
		
		return expandSourceRange(start, end, document, scanner, element);
	}
	
	protected TextRegion expandSourceRange(final int start, int end, final AbstractDocument doc,
			final HeuristicTokenScanner scanner,
			final SourceElement element)
			throws BadLocationException, BadPartitioningException {
		scanner.configure(doc);
		
		final int startLine= doc.getLineOfOffset(start);
		int endLine= doc.getLineOfOffset(end);
		IRegion endLineInfo;
		int endLineNonBlank;
		endLineInfo= doc.getLineInformation(endLine);
		endLineNonBlank= scanner.findAnyNonMSpaceForward(end, endLineInfo.getOffset() + endLineInfo.getLength());
		if (endLineNonBlank >= 0) {
			final ITypedRegion partition= doc.getPartition(scanner.getDocumentPartitioning(),
					endLineNonBlank, false );
			if (isCommentContent(partition)) {
				end= partition.getOffset() + partition.getLength();
				if (end > endLineInfo.getOffset() + endLineInfo.getLength()) {
					endLine= doc.getLineOfOffset(end);
					endLineInfo= doc.getLineInformation(endLine);
					endLineNonBlank= scanner.findAnyNonMSpaceForward(end, endLineInfo.getOffset() + endLineInfo.getLength());
				}
			}
		}
		if (end > endLineInfo.getOffset() && endLineNonBlank < 0
				&& (startLine < endLine || scanner.findAnyNonMSpaceBackward(start, doc.getLineOffset(startLine)) < 0) ) {
			end= endLineInfo.getOffset() + doc.getLineLength(endLine);
			if (end > endLineInfo.getOffset() + endLineInfo.getLength()) {
				endLine= doc.getLineOfOffset(end);
				endLineInfo= doc.getLineInformation(endLine);
				endLineNonBlank= scanner.findAnyNonMSpaceForward(end, endLineInfo.getOffset() + endLineInfo.getLength());
			}
		}
		
		// add empty line
		if (end == endLineInfo.getOffset() && endLineNonBlank < 0
				&& shouldExpandEmptyLine(element) ) {
			end= endLineInfo.getOffset() + endLineInfo.getLength();
		}
		
		return new BasicTextRegion(start, end);
	}
	
	public TextRegion expandWhitespaceBlock(final AbstractDocument document, final TextRegion region,
			final HeuristicTokenScanner scanner) throws BadLocationException {
		scanner.configure(document);
		final int firstLine= document.getLineOfOffset(region.getStartOffset());
		int lastLine= document.getLineOfOffset(region.getEndOffset());
		if (lastLine > firstLine && document.getLineOffset(lastLine) == region.getEndOffset()) {
			lastLine--;
		}
		return new BasicTextRegion(
				scanner.expandAnyMSpaceBackward(region.getStartOffset(),
						document.getLineOffset(firstLine) ),
				scanner.expandAnyMSpaceForward(region.getEndOffset(),
						document.getLineOffset(lastLine) + document.getLineLength(lastLine) ));
	}
	
	
	public boolean canDelete(final ElementSet elements) {
		if (elements.getInitialObjects().size() == 0) {
			return false;
		}
		if (!elements.isOK()) {
			return false;
		}
		for (final LtkModelElement element : elements.getModelElements()) {
			if (!canDelete(element)) {
				return false;
			}
		}
		for (final IResource element : elements.getResources()) {
			if (!canDelete(element)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean canDelete(final LtkModelElement element) {
		if (!element.exists()) {
			return false;
		}
//		if ((element.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.PROJECT) {
//			return false;
//		}
		if (!isSupportedModelType(element.getModelTypeId())) {
			return false;
		}
		if (element.isReadOnly()) {
			return false;
		}
		return true;
	}
	
	public boolean canDelete(final IResource resource) {
		if (!resource.exists() || resource.isPhantom()) {
			return false;
		}
		if (resource.getType() == IResource.ROOT || resource.getType() == IResource.PROJECT) {
			return false;
		}
		if (resource.getParent() != null) {
			final ResourceAttributes attributes= resource.getParent().getResourceAttributes();
			if (attributes != null && attributes.isReadOnly()) {
				return false;
			}
		}
		return true;
	}
	
	public boolean canInsert(final ElementSet elements, final RefactoringDestination to) {
		if (to.getInitialObjects().get(0) instanceof SourceElement) {
			return canInsert(elements, (SourceElement)to.getInitialObjects().get(0), to.getPosition());
		}
		return false;
	}
	
	protected boolean canInsert(final ElementSet elements, final SourceElement to,
			final RefactoringDestination.Position pos) {
		if (elements.getInitialObjects().size() == 0) {
			return false;
		}
		if (!elements.isOK()) {
			return false;
		}
		if (!canInsertTo(to)) {
			return false;
		}
		for (final LtkModelElement element : elements.getModelElements()) {
			if (!canInsert(element)) {
				return false;
			}
		}
		for (final IResource element : elements.getResources()) {
//			if (!canInsert(element, parent) {
				return false;
//			}
		}
		return !elements.includes(to);
	}
	
	protected boolean canInsert(final LtkModelElement element) {
		if (!element.exists()) {
			return false;
		}
		if (!isSupportedModelType(element.getModelTypeId())) {
			return false;
		}
		return true;
	}
	
	public boolean canInsertTo(final RefactoringDestination destination) {
		if (destination.getModelElements().size() != 1) {
			return false;
		}
		return canInsertTo(destination.getModelElements().get(0));
	}
	
	protected boolean canInsertTo(final LtkModelElement element) {
		if (!element.exists()) {
			return false;
		}
		if (!isSupportedModelType(element.getModelTypeId())) {
			return false;
		}
		if (element.isReadOnly()) {
			return false;
		}
		return true;
	}
	
	
	public void checkInitialToModify(final RefactoringStatus result, final ElementSet elements) {
		final Set<IResource> resources= new HashSet<>();
		resources.addAll(elements.getResources());
		for(final LtkModelElement element : elements.getModelElements()) {
			final SourceUnit su= LtkModelUtils.getSourceUnit(element);
			if (su instanceof WorkspaceSourceUnit) {
				resources.add(((WorkspaceSourceUnit)su).getResource());
				continue;
			}
			result.addFatalError(Messages.Check_ElementNotInWS_message);
			return;
		}
		result.merge(RefactoringStatus.create(
				Resources.checkInSync(resources.toArray(new IResource[resources.size()]))
				));
	}
	
	public void checkFinalToModify(final RefactoringStatus result, final ElementSet elements, final IProgressMonitor monitor) {
		final Set<IResource> resources= new HashSet<>();
		resources.addAll(elements.getResources());
		for(final LtkModelElement element : elements.getModelElements()) {
			final SourceUnit su= LtkModelUtils.getSourceUnit(element);
			if (su instanceof WorkspaceSourceUnit) {
				resources.add(((WorkspaceSourceUnit)su).getResource());
				continue;
			}
			result.addFatalError(Messages.Check_ElementNotInWS_message);
			return;
		}
		final IResource[] array= resources.toArray(new IResource[resources.size()]);
		result.merge(RefactoringStatus.create(Resources.checkInSync(array)));
		result.merge(RefactoringStatus.create(Resources.makeCommittable(array, IWorkspace.VALIDATE_PROMPT)));
	}
	
	public void checkFinalToDelete(final RefactoringStatus result, final ElementSet elements) throws CoreException {
		for (final LtkModelElement element : elements.getModelElements()) {
			checkFinalToDelete(result, element);
		}
		for (final IResource element : elements.getResources()) {
			checkFinalToDelete(result, element);
		}
	}
	
	public void checkFinalToDelete(final RefactoringStatus result, final IResource element) throws CoreException {
		if (element.getType() == IResource.FILE) {
			warnIfDirty(result, (IFile)element);
			return;
		}
		else {
			element.accept(new IResourceVisitor() {
				@Override
				public boolean visit(final IResource visitedResource) throws CoreException {
					if (visitedResource instanceof IFile) {
						warnIfDirty(result, (IFile)visitedResource);
					}
					return true;
				}
			}, IResource.DEPTH_INFINITE, false);
		}
	}
	
	public void checkFinalToDelete(final RefactoringStatus result, final LtkModelElement element) throws CoreException {
		if ((element.getElementType() & LtkModelElement.MASK_C12) == LtkModelElement.C12_SOURCE_FILE) {
			if (element instanceof WorkspaceSourceUnit) {
				checkFinalToDelete(result, ((WorkspaceSourceUnit)element).getResource());
			}
		}
		else if ((element.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_BUNDLE
				&& element instanceof SourceStructElement) {
			final List<? extends LtkModelElement> children= ((SourceStructElement)element).getSourceChildren(null);
			for (final LtkModelElement child : children) {
				checkFinalToDelete(result, child);
			}
		}
	}
	
	public void warnIfDirty(final RefactoringStatus result, final IFile file) {
		if (file == null || !file.exists()) {
			return;
		}
		final ITextFileBuffer buffer= FileBuffers.getTextFileBufferManager().getTextFileBuffer(file.getFullPath(), LocationKind.IFILE);
		if (buffer != null && buffer.isDirty()) {
			if (buffer.isStateValidated() && buffer.isSynchronized()) {
				result.addWarning(NLS.bind(
					Messages.Check_FileUnsavedChanges_message,
					FileUtil.getFileUtil(file).getLabel()) );
			} else {
				result.addFatalError(NLS.bind(
					Messages.Check_FileUnsavedChanges_message, 
					FileUtil.getFileUtil(file).getLabel()) );
			}
		}
	}
	
	public boolean confirmDeleteOfReadOnlyElements(final ElementSet elements, final Object queries) throws CoreException {
		// TODO add query support
		return hasReadOnlyElements(elements);
	}
	
	public boolean hasReadOnlyElements(final ElementSet elements) throws CoreException {
		for (final IResource element : elements.getResources()) {
			if (hasReadOnlyElements(element)) {
				return true;
			}
		}
		for (final LtkModelElement element : elements.getModelElements()) {
			if (hasReadOnlyElements(element)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasReadOnlyElements(final IResource element) throws CoreException {
		if (isReadOnly(element)) {
			return true;
		}
		if (element instanceof IContainer) {
			final IResource[] members= ((IContainer)element).members(false);
			for (final IResource member : members) {
				if (hasReadOnlyElements(member)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean hasReadOnlyElements(final LtkModelElement element) throws CoreException {
		final SourceUnit su= LtkModelUtils.getSourceUnit(element);
		IResource resource= null;
		if (su instanceof WorkspaceSourceUnit) {
			resource= ((WorkspaceSourceUnit)su).getResource();
		}
		if (resource == null) {
			resource= element.getAdapter(IResource.class);
		}
		if (resource != null) {
			return hasReadOnlyElements(resource);
		}
		return false;
	}
	
	public boolean isReadOnly(final IResource element) {
		final ResourceAttributes attributes= element.getResourceAttributes();
		if (attributes != null) {
			return attributes.isReadOnly();
		}
		return false;
	}
	
	
	public void addParticipantsToDelete(final ElementSet elementsToDelete,
			final List<RefactoringParticipant> list,
			final RefactoringStatus status, final RefactoringProcessor processor, 
			final SharableParticipants shared)
			throws CoreException {
		final Set<String> natureIds= ElementSet.getAffectedProjectNatures(elementsToDelete);
		final String[] natures= natureIds.toArray(new String[natureIds.size()]);
		final DeleteArguments arguments= new DeleteArguments();
		for (final IResource resource : elementsToDelete.getResources()) {
			final DeleteParticipant[] deletes= ParticipantManager.loadDeleteParticipants(status, 
				processor, resource, 
				arguments, natures, shared);
			list.addAll(Arrays.asList(deletes));
		}
		for (final IResource resource : elementsToDelete.getResourcesOwnedByElements()) {
			final DeleteParticipant[] deletes= ParticipantManager.loadDeleteParticipants(status, 
				processor, resource, 
				arguments, natures, shared);
			list.addAll(Arrays.asList(deletes));
		}
		for (final LtkModelElement element : elementsToDelete.getModelElements()) {
			final DeleteParticipant[] deletes= ParticipantManager.loadDeleteParticipants(status, 
				processor, element, 
				arguments, natures, shared);
			list.addAll(Arrays.asList(deletes));
		}
	}
	
	public void addParticipantsToMove(final ElementSet elementsToMove,
			final RefactoringDestination destination,
			final List<RefactoringParticipant> list,
			final RefactoringStatus status, final RefactoringProcessor processor, 
			final SharableParticipants shared, final ReorgExecutionLog executionLog)
			throws CoreException {
		final Set<String> natureIds= ElementSet.getAffectedProjectNatures(
				ImCollections.newList(elementsToMove, destination) );
		final String[] natures= natureIds.toArray(new String[natureIds.size()]);
		final MoveArguments mArguments= new MoveArguments(destination.getModelElements().get(0),
				false );
//		for (final IResource resource : elementsToCopy.getResources()) {
//			final MoveParticipant[] deletes= ParticipantManager.loadMoveParticipants(status, 
//					processor, resource, arguments, natures, shared );
//			list.addAll(Arrays.asList(deletes));
//		}
//		for (final IResource resource : elementsToCopy.getResourcesOwnedByElements()) {
//			final MoveParticipant[] deletes= ParticipantManager.loadMoveParticipants(status, 
//					processor, resource, arguments, natures, shared );
//			list.addAll(Arrays.asList(deletes));
//		}
		for (final LtkModelElement element : elementsToMove.getModelElements()) {
			final MoveParticipant[] deletes= ParticipantManager.loadMoveParticipants(status,
					processor, element, mArguments, natures, shared );
			list.addAll(Arrays.asList(deletes));
		}
	}
	
	public void addParticipantsToCopy(final ElementSet elementsToCopy,
			final RefactoringDestination destination,
			final List<RefactoringParticipant> list,
			final RefactoringStatus status, final RefactoringProcessor processor, 
			final SharableParticipants shared, final ReorgExecutionLog executionLog)
			throws CoreException {
		final Set<String> natureIds= ElementSet.getAffectedProjectNatures(
				ImCollections.newList(elementsToCopy, destination) );
		final String[] natures= natureIds.toArray(new String[natureIds.size()]);
		final CopyArguments mArguments= new CopyArguments(destination.getModelElements().get(0),
				executionLog );
//		for (final IResource resource : elementsToCopy.getResources()) {
//			final CopyParticipant[] deletes= ParticipantManager.loadCopyParticipants(status,
//					processor, resource, arguments, natures, shared );
//			list.addAll(Arrays.asList(deletes));
//		}
//		for (final IResource resource : elementsToCopy.getResourcesOwnedByElements()) {
//			final CopyParticipant[] deletes= ParticipantManager.loadCopyParticipants(status,
//					processor, resource, arguments, natures, shared);
//			list.addAll(Arrays.asList(deletes));
//		}
		for (final LtkModelElement element : elementsToCopy.getModelElements()) {
			final CopyParticipant[] deletes= ParticipantManager.loadCopyParticipants(status,
					processor, element,
					mArguments, natures, shared);
			list.addAll(Arrays.asList(deletes));
		}
	}
	
	public void buildDeltaToDelete(final ElementSet elements,
			final IResourceChangeDescriptionFactory resourceDelta) {
		for (final IResource resource : elements.getResources()) {
			resourceDelta.delete(resource);
		}
		for (final IResource resource : elements.getResourcesOwnedByElements()) {
			resourceDelta.delete(resource);
		}
		for (final IFile file : elements.getFilesContainingElements()) {
			resourceDelta.change(file);
		}
	}
	
	public void buildDeltaToModify(final ElementSet elements,
			final IResourceChangeDescriptionFactory resourceDelta) {
		for (final IResource resource : elements.getResources()) {
			if (resource instanceof IFile) {
				resourceDelta.change((IFile)resource);
			}
		}
		for (final IResource resource : elements.getResourcesOwnedByElements()) {
			if (resource instanceof IFile) {
				resourceDelta.change((IFile)resource);
			}
		}
		for (final IFile file : elements.getFilesContainingElements()) {
			resourceDelta.change(file);
		}
	}
	
	/**
	 * @param changeName the name of the change
	 * @param resources the resources to delete
	 * @param manager the text change manager
	 * @return the created change
	 * @throws CoreException 
	 */
	public Change createChangeToDelete(final String changeName,
			final ElementSet elementsToDelete,
			final TextChangeManager manager, final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 2);
		final CompositeChange result= new CompositeChange(changeName);
		
		addChangesToDelete(result, elementsToDelete, manager, m.newChild(1));
		
		result.addAll(manager.getAllChanges());
		m.worked(1);
		return result;
	}
	
	public Change createChangeToMove(final String changeName, 
			final ElementSet elementsToMove, final RefactoringDestination destination,
			final TextChangeManager manager, final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 4);
		final CompositeChange result= new CompositeChange(changeName);
		
		final String code= getSourceCodeStringedTogether(elementsToMove, m.newChild(1));
		
		addChangesToDelete(result, elementsToMove, manager, m.newChild(1));
		addChangesToInsert(result, code, destination, manager, m.newChild(1));
		
		result.addAll(manager.getAllChanges());
		m.worked(1);
		return result;
	}
	
	public Change createChangeToCopy(final String changeName, 
			final ElementSet elementsToMove, final RefactoringDestination destination,
			final TextChangeManager manager, final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 3);
		final CompositeChange result= new CompositeChange(changeName);
		
		final String code= getSourceCodeStringedTogether(elementsToMove, m.newChild(1));
		
		addChangesToInsert(result, code, destination, manager, m.newChild(1));
		
		result.addAll(manager.getAllChanges());
		m.worked(1);
		return result;
	}
	
	public Change createChangeToInsert(final String changeName,
			final String code, final RefactoringDestination destination,
			final TextChangeManager manager,
			final IProgressMonitor monitor) throws CoreException {
		assert (changeName != null && code != null && destination != null && manager != null);
		final SubMonitor m= SubMonitor.convert(monitor, 2);
		final CompositeChange result= new CompositeChange(changeName);
		
		addChangesToInsert(result, code, destination, manager, m.newChild(1));
		
		result.addAll(manager.getAllChanges());
		m.worked(1);
		return result;
	}
	
	protected void addChangesToDelete(final CompositeChange result, 
			final ElementSet elements,
			final TextChangeManager manager, final SubMonitor m) throws CoreException {
		for (final IResource resource : elements.getResources()) {
			result.add(createChangeToDelete(elements, resource));
		}
		final Map<SourceUnit, List<LtkModelElement>> suSubChanges= new HashMap<>();
		for (final LtkModelElement element : elements.getModelElements()) {
			final IResource resource= elements.getOwningResource(element);
			if (resource != null) {
				result.add(createChangeToDelete(elements, resource));
			}
			else {
				final SourceUnit su= LtkModelUtils.getSourceUnit(element);
				List<LtkModelElement> list= suSubChanges.get(su);
				if (list == null) {
					list= new ArrayList<>(1);
					suSubChanges.put(su, list);
				}
				list.add(element);
			}
		}
		if (!suSubChanges.isEmpty()) {
			m.setWorkRemaining(suSubChanges.size() * 3);
			for (final Map.Entry<SourceUnit, List<LtkModelElement>> suChanges : suSubChanges.entrySet()) {
				createChangeToDelete(elements, suChanges.getKey(), suChanges.getValue(), manager, m);
			}
		}
	}
	
	private void createChangeToDelete(final ElementSet elements,
			final SourceUnit su, final List<LtkModelElement> elementsInUnit,
			final TextChangeManager manager, final SubMonitor m) throws CoreException {
		if (!(su instanceof WorkspaceSourceUnit)
				|| ((WorkspaceSourceUnit)su).getResource().getType() != IResource.FILE ) {
			throw new IllegalArgumentException();
		}
		su.connect(m.newChild(1));
		try {
			final MultiTextEdit rootEdit= getRootEdit(manager, su);
			
			final HeuristicTokenScanner scanner= getScanner(su);
			final AbstractDocument document= su.getDocument(null);
			for (final LtkModelElement element : elementsInUnit) {
				final SourceElement member= (SourceElement)element;
				final TextRegion sourceRange= expandElementRange(member, document, scanner);
				final DeleteEdit edit= new DeleteEdit(sourceRange.getStartOffset(), sourceRange.getLength());
				rootEdit.addChild(edit);
			}
			m.worked(1);
		}
		catch (final BadLocationException | BadPartitioningException e) {
			throw new CoreException(failCreation(e));
		}
		finally {
			su.disconnect(m.newChild(1));
		}
	}
	
	protected Change createChangeToDelete(final ElementSet elements, final LtkModelElement element) throws CoreException {
		final IResource resource= elements.getOwningResource(element);
		if (resource != null) {
			return createChangeToDelete(elements, resource);
		}
		throw new IllegalStateException(); 
	}
	
	protected Change createChangeToDelete(final ElementSet elements, final IResource resource) {
		if (resource.getType() == IResource.ROOT || resource.getType() == IResource.PROJECT) {
			throw new IllegalStateException();
		}
		return new DeleteResourceChange(resource.getFullPath(), true);
	}
	
	protected Change createChangeToDelete(final ElementSet elements, final SourceUnit su) throws CoreException {
		if (su instanceof WorkspaceSourceUnit) {
			return createChangeToDelete(elements, ((WorkspaceSourceUnit)su).getResource());
		}
		throw new IllegalStateException();
	}
	
	
	protected void addChangesToInsert(final CompositeChange result, final String code,
			final RefactoringDestination destination,
			final TextChangeManager manager, final SubMonitor m) throws CoreException {
		final SourceElement element= (SourceElement)destination.getModelElements().get(0);
		final SourceUnit su= LtkModelUtils.getSourceUnit(element);
		
		m.setWorkRemaining(3);
		createChangeToInsert(su, code, element, destination, manager, m);
	}
	
	private void createChangeToInsert(final SourceUnit su,
			final String code,
			final SourceElement desElement, final RefactoringDestination destination,
			final TextChangeManager manager, final SubMonitor m) throws CoreException {
		if (!(su instanceof WorkspaceSourceUnit)
				|| ((WorkspaceSourceUnit)su).getResource().getType() != IResource.FILE ) {
			throw new IllegalArgumentException();
		}
		su.connect(m.newChild(1));
		try {
			final AbstractDocument document= su.getDocument(m.newChild(1));
			final HeuristicTokenScanner scanner= getScanner(su);
			
			final int offset;
			if (destination.getPosition() == RefactoringDestination.Position.AT) {
				offset= destination.getOffset();
			}
			else {
				offset= getInsertionOffset(document, desElement, destination.getPosition(), scanner);
			}
			
			final MultiTextEdit rootEdit= getRootEdit(manager, su);
			
			final InsertEdit edit= new InsertEdit(offset, code);
			rootEdit.addChild(edit);
			((SourceUnitChange)manager.get(su)).setInsertPosition(new Position(edit.getOffset()));
			
			m.worked(1);
		}
		catch (final BadLocationException | BadPartitioningException e) {
			throw new CoreException(failCreation(e));
		}
		finally {
			su.disconnect(m.newChild(1));
		}
	}
	
	protected int getInsertionOffset(final AbstractDocument document, final SourceElement element,
			final RefactoringDestination.Position pos,
			final HeuristicTokenScanner scanner) throws BadLocationException, BadPartitioningException {
		final TextRegion range= expandElementRange(element, document, scanner);
		if (pos == RefactoringDestination.Position.ABOVE) {
			final int offset= range.getStartOffset();
			
			return offset;
		}
		else {
			int offset= range.getEndOffset();
			
			final int line= document.getLineOfOffset(offset);
			final IRegion lineInformation= document.getLineInformation(line);
			if (offset == lineInformation.getOffset() + lineInformation.getLength()) {
				offset+= document.getLineDelimiter(line).length();
			}
			return offset;
		}
	}
	
	private MultiTextEdit getRootEdit(final TextChangeManager manager, final SourceUnit su) {
		final TextFileChange textFileChange= manager.get(su);
		if (su.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
			textFileChange.setSaveMode(TextFileChange.LEAVE_DIRTY);
		}
		if (textFileChange.getEdit() == null) {
			textFileChange.setEdit(new MultiTextEdit());
		}
		return (MultiTextEdit)textFileChange.getEdit();
	}
	
	
	protected IStatus failDocAnalyzation(final Throwable e) {
		return new Status(IStatus.ERROR, LtkCore.BUNDLE_ID, Messages.Common_error_AnalyzingSourceDocument_message, e);
	}
	
	protected IStatus failCreation(final Throwable e) {
		return new Status(IStatus.ERROR, LtkCore.BUNDLE_ID, Messages.Common_error_CreatingElementChange_message, e);
	}
	
}
