/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.IOException;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ltk.core.IExtContentTypeManager;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.LtkCore;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.ModelTypeDescriptor;
import org.eclipse.statet.ltk.model.core.SourceUnitFactory;
import org.eclipse.statet.ltk.model.core.SourceUnitIdFactory;
import org.eclipse.statet.ltk.model.core.SourceUnitManager;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public class SourceUnitManagerImpl implements SourceUnitManager, Disposable {
	
	
	private static final String CONFIG_MODELTYPE_ID_ATTRIBUTE_NAME= "modelTypeId"; //$NON-NLS-1$
	private static final String CONFIG_CONTEXT_KEY_ATTRIBUTE_NAME= "contextKey"; //$NON-NLS-1$
	
	
	private static final class SuItem extends SoftReference<SourceUnit> {
		
		private final String key;
		
		public SuItem(final String key, final SourceUnit su, final ReferenceQueue<SourceUnit> queue) {
			super(su, queue);
			this.key= key;
		}
		
		public String getKey() {
			return this.key;
		}
		
		public void dispose() {
			final SourceUnit su= get();
			if (su != null && su.isConnected()) {
				LtkCorePlugin.log(
						new Status(IStatus.WARNING, LtkCorePlugin.BUNDLE_ID, -1,
								NLS.bind("Source Unit ''{0}'' disposed but connected.", su.getId()), null));
			}
			clear();
		}
		
	}
	
	private static class ContextItem {
		
		private final WorkingContext context;
		private final SourceUnitFactory factory;
		private final HashMap<String, SuItem> sus;
		private final ReferenceQueue<SourceUnit> susToClean;
		
		public ContextItem(final WorkingContext context, final SourceUnitFactory factory) {
			this.context= context;
			this.factory= factory;
			this.sus= new HashMap<>();
			this.susToClean= new ReferenceQueue<>();
		}
		
		@Override
		public int hashCode() {
			return this.context.hashCode();
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			if (obj instanceof ContextItem) {
				return ( ((ContextItem)obj).context == this.context);
			}
			return false;
		}
		
		
		public synchronized @Nullable SourceUnit getOpenSu(final String id) {
			final SuItem suItem= this.sus.get(id);
			if (suItem != null) {
				final SourceUnit su= suItem.get();
				if (su != null && suItem.refersTo(su)) {
					return su;
				}
			}
			return null;
		}
		
		public synchronized void appendOpenSus(final ArrayList<SourceUnit> list) {
			final Collection<SuItem> suItems= this.sus.values();
			list.ensureCapacity(list.size() + suItems.size());
			for (final SuItem suItem : suItems) {
				final SourceUnit su= suItem.get();
				if (su != null && suItem.refersTo(su)) {
					list.add(su);
				}
			}
		}
		
	}
	
	private static class ModelItem {
		
		
		private final String modelTypeId;
		
		private volatile ImList<ContextItem> contextItems= ImCollections.newList();
		
		
		public ModelItem(final String modelTypeId) {
			this.modelTypeId= modelTypeId;
		}
		
		public @Nullable ContextItem getContextItem(final WorkingContext context, final boolean create) {
			final ImList<ContextItem> contextItems= this.contextItems;
			for (final ContextItem contextItem : contextItems) {
				if (contextItem.context == context) {
					return contextItem;
				}
			}
			if (create) {
				synchronized (this) {
					if (contextItems != this.contextItems) {
						return getContextItem(context, true);
					}
					try {
						final IConfigurationElement[] elements= Platform.getExtensionRegistry().
								getConfigurationElementsFor("org.eclipse.statet.ltk.ModelTypes"); //$NON-NLS-1$
						IConfigurationElement matchingElement= null;
						for (final IConfigurationElement element : elements) {
							if (element.getName().equals("unitType") && element.isValid()) { //$NON-NLS-1$
								final String typeIdOfElement= element.getAttribute(CONFIG_MODELTYPE_ID_ATTRIBUTE_NAME);
								final String contextKeyOfElement= element.getAttribute(CONFIG_CONTEXT_KEY_ATTRIBUTE_NAME);
								if (this.modelTypeId.equals(typeIdOfElement)) {
									if ((contextKeyOfElement == null) || (contextKeyOfElement.length() == 0)) {
										matchingElement= element;
										continue;
									}
									if (contextKeyOfElement.equals(context.getKey())) {
										matchingElement= element;
										break;
									}
								}
							}
						}
						if (matchingElement != null) {
							final SourceUnitFactory factory= (SourceUnitFactory)matchingElement.createExecutableExtension("unitFactory"); //$NON-NLS-1$
							final ContextItem contextItem= new ContextItem(context, factory);
							this.contextItems= ImCollections.addElement(contextItems, contextItem);
							return contextItem;
						}
					}
					catch (final Exception e) {
						LtkCorePlugin.log(new Status(IStatus.ERROR, LtkCore.BUNDLE_ID, 0,
								"Error loading working context contributions", e )); //$NON-NLS-1$
					}
				}
			}
			return null;
		}
		
		public ImList<ContextItem> getOpenContextItems(final @Nullable WorkingContext context) {
			final ImList<ContextItem> contextItems= this.contextItems;
			if (context != null) {
				for (final ContextItem contextItem : contextItems) {
					if (contextItem.context == context) {
						return ImCollections.newList(contextItem);
					}
				}
				return ImCollections.emptyList();
			}
			else {
				return contextItems;
			}
		}
		
		@Override
		public int hashCode() {
			return this.modelTypeId.hashCode();
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			return (obj instanceof ModelItem
					&& this.modelTypeId.equals(((ModelItem)obj).modelTypeId));
		}
		
	}
	
	private class CleanupJob extends Job {
		
		private final Object scheduleLock= new Object();
		
		public CleanupJob() {
			super("SourceUnit Cleanup"); //$NON-NLS-1$
			setUser(false);
			setSystem(true);
			setPriority(DECORATE);
		}
		
		void initialSchedule() {
			synchronized (this.scheduleLock) {
				schedule(180000);
			}
		}
		
		void dispose() {
			synchronized (this.scheduleLock) {
				cancel();
			}
		}
		
		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			final int count= performCleanup();
			
			synchronized (this.scheduleLock) {
				if (monitor.isCanceled()) {
					return Status.CANCEL_STATUS;
				}
				else {
					schedule(count > 0 ? 60000 : 180000);
					return Status.OK_STATUS;
				}
			}
		}
		
	}
	
	
	private final CleanupJob cleanupJob= new CleanupJob();
	
	private volatile ImList<ModelItem> modelItems= ImCollections.newList();
	
	private final SourceUnitIdFactory defaultIdFactory= new DefaultSourceUnitIdFactory();
	
	private final IExtContentTypeManager contentManager= Ltk.getExtContentTypeManager();
	
	
	public SourceUnitManagerImpl() {
		this.cleanupJob.initialSchedule();
	}
	
	
	private int performCleanup() {
		int count= 0;
		final ImList<ModelItem> modelItems= this.modelItems;
		for (final ModelItem modelItem : modelItems) {
			final List<ContextItem> contextItems= modelItem.contextItems;
			for (final ContextItem contextItem : contextItems) {
				SuItem suItem;
				while ((suItem= (SuItem)contextItem.susToClean.poll()) != null){
					synchronized (contextItem.sus) {
						if (contextItem.sus.get(suItem.getKey()) == suItem) {
							contextItem.sus.remove(suItem.getKey());
						}
						suItem.dispose();
						count++;
					}
				}
			}
		}
		return count;
	}
	
	@Override
	public void dispose() {
		this.cleanupJob.dispose();
	}
	
	
	@Override
	public final @Nullable String getSourceUnitId(final Object from) {
		nonNullAssert(from);
		if (from instanceof SourceUnit) {
			return ((SourceUnit)from).getId();
		}
		return this.defaultIdFactory.createId(from);
	}
	
	@Override
	public SourceUnit getSourceUnit(final String modelTypeId, final WorkingContext context,
			final Object from,
			final IProgressMonitor monitor) throws StatusException {
		nonNullAssert(modelTypeId);
		nonNullAssert(context);
		
		final SourceUnit fromUnit;
		final String sourceUnitId;
		if (from instanceof SourceUnit) {
			fromUnit= (SourceUnit)from;
			sourceUnitId= fromUnit.getId();
		}
		else {
			fromUnit= null;
			sourceUnitId= this.defaultIdFactory.createId(from);
			if (sourceUnitId == null) {
				throw new StatusException(new ErrorStatus(LtkCore.BUNDLE_ID, NLS.bind(
						"Unsupported object type for source unit: ''{0}''.", from.getClass()) ));
			}
		}
		
		return getSourceUnit2(modelTypeId, context, sourceUnitId, from, monitor);
	}
	
	@Override
	public SourceUnit getSourceUnit(final WorkingContext context, final SourceUnit from,
			final boolean disconnectFrom,
			final IProgressMonitor monitor) throws StatusException {
		nonNullAssert(context);
		
		final SourceUnit sourceUnit= getSourceUnit2(from.getModelTypeId(), context, from.getId(), from, monitor);
		if (disconnectFrom) {
			from.disconnect(monitor);
		}
		
		return sourceUnit;
	}
	
	private SourceUnit getSourceUnit2(final String modelTypeId, final WorkingContext context,
			final String sourceUnitId, final Object from,
			final IProgressMonitor monitor) throws StatusException {
		final ModelItem modelItem= getModelItem(modelTypeId);
		final ContextItem contextItem= modelItem.getContextItem(context, true);
		if (contextItem == null) {
			final var sb= new ToStringBuilder("Cannot provide the source unit: " //$NON-NLS-1$
					+ "no factory is defined for the request."); //$NON-NLS-1$
			sb.addProp("modelType", modelTypeId);
			sb.addProp("context", context);
			throw new StatusException(new ErrorStatus(LtkCore.BUNDLE_ID, sb.build()));
		}
		
		SourceUnit sourceUnit= null;
		synchronized (contextItem) {
			SuItem suItem= contextItem.sus.get(sourceUnitId);
			if (suItem != null) {
				sourceUnit= suItem.get();
				if (!suItem.refersTo(sourceUnit)) {
					sourceUnit= null;
				}
			}
			if (sourceUnit == null) {
				sourceUnit= contextItem.factory.createSourceUnit(sourceUnitId, from);
				if (sourceUnit == null) {
					final var sb= new ToStringBuilder("Cannot provide the source unit: " //$NON-NLS-1$
							+ "the factory does not seem to support the request."); //$NON-NLS-1$
					sb.addProp("modelType", modelTypeId);
					sb.addProp("context", context);
					sb.addProp("factory", contextItem.factory);
					sb.addProp("from", from);
					throw new StatusException(new ErrorStatus(LtkCore.BUNDLE_ID, sb.build()));
				}
				if (sourceUnit.getModelTypeId() != modelItem.modelTypeId
						|| (sourceUnit.getWorkingContext() != context)
						|| (sourceUnit.getElementType() & LtkModelElement.MASK_C1) != LtkModelElement.C1_SOURCE) {
					final var sb= new ToStringBuilder("Cannot provide source unit: " //$NON-NLS-1$
							+ "object created by the factory does not comply with the request."); //$NON-NLS-1$
					sb.addProp("modelType", modelTypeId);
					sb.addProp("context", context);
					sb.addProp("factory", contextItem.factory);
					sb.addProp("from", from);
					sb.addProp("sourceUnit", sourceUnit);
					sb.addProp("sourceUnit.elementType", "0x%1$08X", sourceUnit.getElementType());
					throw new StatusException(new ErrorStatus(LtkCore.BUNDLE_ID, sb.build()));
				}
				suItem= new SuItem(sourceUnitId, sourceUnit, contextItem.susToClean);
				contextItem.sus.put(sourceUnitId, suItem);
			}
		}
		
		sourceUnit.connect(monitor);
		
		return sourceUnit;
	}
	
	@Override
	public @Nullable SourceUnit getSourceUnit(final WorkingContext context,
			final Object from, @Nullable final IContentType contentType, final boolean create,
			final IProgressMonitor monitor) {
		nonNullAssert(context);
		
		final SourceUnit fromUnit;
		final String modelTypeId;
		final String sourceUnitId;
		if (from instanceof SourceUnit) {
			fromUnit= (SourceUnit)from;
			modelTypeId= fromUnit.getModelTypeId();
			sourceUnitId= fromUnit.getId();
		}
		else {
			fromUnit= null;
			modelTypeId= detectModelTypeId(from, null);
			if (modelTypeId == null) {
				return null;
			}
			sourceUnitId= this.defaultIdFactory.createId(from);
			if (sourceUnitId == null) {
				return null;
			}
		}
		
		return getSourceUnit1(modelTypeId, context, sourceUnitId, from, fromUnit, create, monitor);
	}
	
	private @Nullable SourceUnit getSourceUnit1(final String modelTypeId, final WorkingContext context,
			final String sourceUnitId, final Object from, final @Nullable SourceUnit fromUnit,
			final boolean create,
			final IProgressMonitor monitor) {
		final ModelItem modelItem= getModelItem(modelTypeId);
		final ContextItem contextItem= modelItem.getContextItem(context, create);
		if (contextItem == null) {
			if (create) {
				final var sb= new ToStringBuilder("Cannot provide the source unit: " //$NON-NLS-1$
						+ "no factory is defined for the request."); //$NON-NLS-1$
				sb.addProp("modelType", modelTypeId);
				sb.addProp("context", context);
				throw new UnsupportedOperationException(sb.build());
			}
			return null;
		}
		
		SourceUnit sourceUnit= null;
		synchronized (contextItem) {
			SuItem suItem= contextItem.sus.get(sourceUnitId);
			if (suItem != null) {
				sourceUnit= suItem.get();
				if (!suItem.refersTo(sourceUnit)) {
					sourceUnit= null;
				}
			}
			if (sourceUnit == null && create) {
				sourceUnit= contextItem.factory.createSourceUnit(sourceUnitId, from);
				if (sourceUnit == null) {
					final var sb= new ToStringBuilder("Cannot provide the source unit: " //$NON-NLS-1$
							+ "the factory does not seem to support the request."); //$NON-NLS-1$
					sb.addProp("modelType", modelTypeId);
					sb.addProp("context", context);
					sb.addProp("factory", contextItem.factory);
					sb.addProp("from", from);
					LtkCorePlugin.log(new ErrorStatus(LtkCore.BUNDLE_ID, sb.build()));
					return null;
				}
				if (sourceUnit.getModelTypeId() != modelItem.modelTypeId
						|| (sourceUnit.getWorkingContext() != context)
						|| (sourceUnit.getElementType() & LtkModelElement.MASK_C1) != LtkModelElement.C1_SOURCE) {
					final var sb= new ToStringBuilder("Cannot provide source unit: " //$NON-NLS-1$
							+ "object created by the factory does not comply with the request."); //$NON-NLS-1$
					sb.addProp("modelType", modelTypeId);
					sb.addProp("context", context);
					sb.addProp("factory", contextItem.factory);
					sb.addProp("from", from);
					sb.addProp("sourceUnit", sourceUnit);
					sb.addProp("sourceUnit.elementType", "0x%1$08X", sourceUnit.getElementType());
					LtkCorePlugin.log(new ErrorStatus(LtkCore.BUNDLE_ID, sb.build()));
					return null;
				}
				suItem= new SuItem(sourceUnitId, sourceUnit, contextItem.susToClean);
				contextItem.sus.put(sourceUnitId, suItem);
			}
		}
		
		if (sourceUnit == null) {
			return null;
		}
		
		sourceUnit.connect(monitor);
		if (fromUnit != null) {
			fromUnit.disconnect(monitor);
		}
		
		return sourceUnit;
	}
	
	@Override
	public @Nullable SourceUnit getOpenSourceUnit(final String modelTypeId, final WorkingContext context,
			final Object from) {
		nonNullAssert(modelTypeId);
		nonNullAssert(context);
		
		final String sourceUnitId= getSourceUnitId(from);
		if (sourceUnitId == null) {
			return null;
		}
		
		return getOpenSourceUnit2(modelTypeId, context, sourceUnitId);
	}
	
	@Override
	public @Nullable SourceUnit getOpenSourceUnit(final WorkingContext context, final Object from) {
		nonNullAssert(context);
		
		final String modelTypeId;
		final String sourceUnitId;
		if (from instanceof final SourceUnit fromUnit) {
			modelTypeId= fromUnit.getModelTypeId();
			sourceUnitId= fromUnit.getId();
		}
		else {
			modelTypeId= detectModelTypeId(from, null);
			if (modelTypeId == null) {
				return null;
			}
			sourceUnitId= this.defaultIdFactory.createId(from);
			if (sourceUnitId == null) {
				return null;
			}
		}
		
		return getOpenSourceUnit2(modelTypeId, context, sourceUnitId);
	}
	
	private @Nullable SourceUnit getOpenSourceUnit2(final String modelTypeId, final WorkingContext context,
			final String sourceUnitId) {
		final ModelItem modelItem= getModelItem(modelTypeId);
		final ContextItem contextItem= modelItem.getContextItem(context, false);
		if (contextItem == null) {
			return null;
		}
		
		SourceUnit sourceUnit= null;
		synchronized (contextItem) {
			final SuItem suItem= contextItem.sus.get(sourceUnitId);
			if (suItem != null) {
				sourceUnit= suItem.get();
				if (!suItem.refersTo(sourceUnit)) {
					sourceUnit= null;
				}
			}
		}
		return sourceUnit;
	}
	
	@Override
	public List<SourceUnit> getOpenSourceUnits(final String modelTypeId,
			final @Nullable WorkingContext context) {
		final List<ModelItem> modelItems= getOpenModelItems(modelTypeId);
		if (modelItems.isEmpty()) {
			return Collections.emptyList();
		}
		
		final ArrayList<SourceUnit> list= new ArrayList<>();
		
		for (final ModelItem modelItem : modelItems) {
			final ImList<ContextItem> contextItems= modelItem.getOpenContextItems(context);
			if (contextItems.isEmpty()) {
				continue;
			}
			
			for (final ContextItem contextItem : contextItems) {
				contextItem.appendOpenSus(list);
			}
		}
		return list;
	}
	
	@Override
	public List<SourceUnit> getOpenSourceUnits(final List<String> modelTypeIds,
			final @Nullable WorkingContext context) {
		final List<ModelItem> includedModelItems= getOpenModelItems(modelTypeIds);
		if (includedModelItems.isEmpty()) {
			return Collections.emptyList();
		}
		
		final ArrayList<SourceUnit> list= new ArrayList<>(4);
		
		for (int i= 0; i < includedModelItems.size(); i++) {
			final ImList<ContextItem> contextItems= this.modelItems.get(i).getOpenContextItems(context);
			if (contextItems.isEmpty()) {
				continue;
			}
			
			for (final ContextItem contextItem : contextItems) {
				contextItem.appendOpenSus(list);
			}
		}
		return list;
	}
	
	@Override
	public List<SourceUnit> getOpenSourceUnits(final List<String> modelTypeIds,
			final @Nullable WorkingContext context, final String sourceUnitId) {
		final List<ModelItem> includedModelItems= getOpenModelItems(modelTypeIds);
		if (includedModelItems.isEmpty()) {
			return Collections.emptyList();
		}
		
		final ArrayList<SourceUnit> list= new ArrayList<>(4);
		
		for (int i= 0; i < includedModelItems.size(); i++) {
			final ImList<ContextItem> contextItems= this.modelItems.get(i).getOpenContextItems(context);
			if (contextItems.isEmpty()) {
				continue;
			}
			
			for (final ContextItem contextItem : contextItems) {
				final SourceUnit su= contextItem.getOpenSu(sourceUnitId);
				if (su != null) {
					list.add(su);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<SourceUnit> getOpenSourceUnits(final List<String> modelTypeIds,
			final @Nullable WorkingContext context, final Object from) {
		final String sourceUnitId= getSourceUnitId(from);
		if (sourceUnitId == null) {
			return ImCollections.emptyList();
		}
		return getOpenSourceUnits(modelTypeIds, context, sourceUnitId);
	}
	
	private @Nullable IContentType detectContentType(final Object from) {
		try {
			if (from instanceof final IFile file) {
				final IContentDescription contentDescription= file.getContentDescription();
				if (contentDescription != null) {
					return contentDescription.getContentType();
				}
				else {
					return null;
				}
			}
			else if (from instanceof final IFileStore file) {
				try (final var in= file.openInputStream(EFS.NONE, null)) {
					final IContentDescription contentDescription= Platform.getContentTypeManager()
							.getDescriptionFor(in, file.getName(), IContentDescription.ALL);
					if (contentDescription != null) {
						return contentDescription.getContentType();
					}
					else {
						return null;
					}
				}
			}
			else {
				return null;
			}
		}
		catch (final CoreException | IOException | UnsupportedOperationException e) {
			LtkCorePlugin.log(new Status(IStatus.ERROR, LtkCore.BUNDLE_ID, 0,
					"An error occurred when trying to detect content type of " + from,
					e ));
			return null;
		}
	}
	
	private @Nullable String detectModelTypeId(final Object from, @Nullable IContentType contentType) {
		if (contentType == null) {
			contentType= detectContentType(from);
			if (contentType == null) {
				return null;
			}
		}
		final ModelTypeDescriptor modelType= this.contentManager.getModelTypeForContentType(contentType.getId());
		return (modelType != null) ? modelType.getId() : null;
	}
	
	
	private ModelItem getModelItem(final String modelTypeId) {
		final ImList<ModelItem> modelItems= this.modelItems;
		for (final ModelItem modelItem : modelItems) {
			if (modelItem.modelTypeId == modelTypeId) {
				return modelItem;
			}
		}
		synchronized (this) {
			if (modelItems != this.modelItems) {
				return getModelItem(modelTypeId);
			}
			final ModelItem modelItem= new ModelItem(modelTypeId);
			this.modelItems= ImCollections.addElement(modelItems, modelItem);
			return modelItem;
		}
	}
	
	private List<ModelItem> getOpenModelItems(final @Nullable String modelTypeId) {
		final ImList<ModelItem> modelItems= this.modelItems;
		if (modelTypeId != null) {
			for (final ModelItem modelItem : modelItems) {
				if (modelItem.modelTypeId == modelTypeId) {
					return ImCollections.newList(modelItem);
				}
			}
			return ImCollections.emptyList();
		}
		else {
			return modelItems;
		}
	}
	
	private List<ModelItem> getOpenModelItems(final @Nullable List<String> modelTypeIds) {
		final ImList<ModelItem> modelItems= this.modelItems;
		if (modelTypeIds != null) {
			final List<ModelItem> matches= new ArrayList<>(modelTypeIds.size());
			for (final ModelItem modelItem : modelItems) {
				if (modelTypeIds.contains(modelItem.modelTypeId)) {
					matches.add(modelItem);
				}
			}
			return matches;
		}
		else {
			return modelItems;
		}
	}
	
}
