/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.core;

import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.util.FS;
import org.eclipse.jgit.util.SystemReader;

import org.eclipse.statet.ltk.core.util.UserInfo;


public class GitConfigInfo {
	
	
	public static UserInfo load() throws Exception {
		final StoredConfig config= SystemReader.getInstance().openUserConfig(null, FS.DETECTED);
		config.load();
		final String name= config.getString("user", null, "name"); //$NON-NLS-1$ //$NON-NLS-2$
		final String email= config.getString("user", null, "email"); //$NON-NLS-1$ //$NON-NLS-2$
		return new UserInfo(name, email, 2);
	}
	
	
}
