/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.core;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;

import org.eclipse.statet.ltk.core.IExtContentTypeManager;
import org.eclipse.statet.ltk.model.core.SourceUnitManager;


/**
 * The activator class controls the plug-in life cycle
 */
public final class LtkCorePlugin extends Plugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.ltk.core"; //$NON-NLS-1$
	
	
	private static LtkCorePlugin instance;
	private static LtkCorePlugin gSafe;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static LtkCorePlugin getInstance() {
		return instance;
	}
	
	/**
	 * Returns the shared plug-in instance
	 * 
	 * @return the shared instance
	 */
	public static LtkCorePlugin getSafe() {
		return gSafe;
	}
	
	
	public static void log(final IStatus status) {
		final LtkCorePlugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	public static final void log(final Status status) {
		log(StatusUtils.convert(status));
	}
	
	
	private boolean started;
	
	private final List<Disposable> disposables= new ArrayList<>();
	
	private ExtContentTypeServices contentTypeServices;
	private SourceUnitManagerImpl sourceUnitManagerImpl;
	
	private AdapterFactory modelAdapterFactory;
	
	
	/**
	 * The default constructor
	 */
	public LtkCorePlugin() {
		instance= this;
		gSafe= this;
	}
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		
		synchronized (this) {
			this.started= true;
			
			this.contentTypeServices= new ExtContentTypeServices();
			addStoppingListener(this.contentTypeServices);
			this.sourceUnitManagerImpl= new SourceUnitManagerImpl();
			addStoppingListener(this.sourceUnitManagerImpl);
		}
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
				this.contentTypeServices= null;
				this.sourceUnitManagerImpl= null;
			}
			
			try {
				for (final Disposable listener : this.disposables) {
					listener.dispose();
				}
			}
			finally {
				this.disposables.clear();
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	public void addStoppingListener(final Disposable listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.disposables.add(listener);
		}
	}
	
	public IExtContentTypeManager getContentTypeServices() {
		return this.contentTypeServices;
	}
	
	public SourceUnitManager getSourceUnitManager() {
		return this.sourceUnitManagerImpl;
	}
	
	public synchronized AdapterFactory getModelAdapterFactory() {
		if (this.modelAdapterFactory == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.modelAdapterFactory= new AdapterFactory("org.eclipse.statet.ltk.ModelAdapters"); //$NON-NLS-1$
		}
		return this.modelAdapterFactory;
	}
	
}
