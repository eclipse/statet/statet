/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.core;

import java.net.URI;
import java.nio.file.Path;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;

import org.eclipse.statet.jcommons.io.UriUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.ltk.model.core.SourceUnitIdFactory;


@NonNullByDefault
public class DefaultSourceUnitIdFactory implements SourceUnitIdFactory {
	
	
	public static final String WORKSPACE_PREFIX= "platform:/resource"; //$NON-NLS-1$
	
	public static final String createResourceId(final IResource file) {
		final IPath path= file.getFullPath();
		return WORKSPACE_PREFIX + path.toPortableString(); // eclipse-platform-resource
	}
	
	public static final String createResourceId(URI uri) {
		uri= uri.normalize();
		if (uri.getScheme() == null) {
			return "xxx:" + uri.toString(); //$NON-NLS-1$
		}
		else {
			return uri.toString();
		}
	}
	
	
	public DefaultSourceUnitIdFactory() {
	}
	
	
	@Override
	public @Nullable String createId(final Object from) {
		if (from instanceof IFile) {
			return createResourceId((IFile)from);
		}
		if (from instanceof IFileStore) {
			return createResourceId(((IFileStore)from).toURI());
		}
		if (from instanceof Path) {
			return createResourceId(((Path)from).toUri());
		}
		
		if (from instanceof SourceFragment) {
			return ((SourceFragment)from).getId();
		}
		
		if (from instanceof final String s) {
			final int sepIdx= s.indexOf(':');
			return (sepIdx > 0 && UriUtils.isValidScheme(s, 0, sepIdx)) ? s : null;
		}
		
		return null;
	}
	
}
