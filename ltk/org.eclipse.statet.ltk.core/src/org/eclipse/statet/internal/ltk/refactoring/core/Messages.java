/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.refactoring.core;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String Common_error_CannotCreateFromDescr_message;
	public static String Common_error_AnalyzingSourceDocument_message;
	public static String Common_error_CreatingElementChange_message;
	
	public static String Check_FileUnsavedChanges_message;
	public static String Check_ElementNotInWS_message;
	
	public static String DynamicValidationState_WorkspaceChanged_message;
	
	public static String DeleteRefactoring_label;
	public static String DeleteRefactoring_description_singular;
	public static String DeleteRefactoring_description_plural;
	
	public static String MoveRefactoring_label;
	public static String MoveRefactoring_description_singular;
	public static String MoveRefactoring_description_plural;
	
	public static String CopyRefactoring_label;
	public static String CopyRefactoring_description_singular;
	public static String CopyRefactoring_description_plural;
	
	public static String PasteRefactoring_label;
	public static String PasteRefactoring_Code_description;
	
	
	static {
		initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
