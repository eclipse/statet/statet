/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.jcommons.collections.ImCollections.newIntList;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.TextLineInformation;


@NonNullByDefault
public class SourceContentTest {
	
	
	public SourceContentTest() {
	}
	
	
	@Test
	public void getStamp() {
		SourceContent sourceContent;
		
		sourceContent= new SourceContent(1687351, "");
		assertEquals(1687351, sourceContent.getStamp());
	}
	
	
	@Test
	public void getStartOffset() {
		SourceContent sourceContent;
		
		sourceContent= new SourceContent(0, "");
		assertEquals(0, sourceContent.getStartOffset());
		sourceContent= new SourceContent(0, "abc");
		assertEquals(0, sourceContent.getStartOffset());
		
		sourceContent= new SourceContent(0, "abc", 32);
		assertEquals(32, sourceContent.getStartOffset());
	}
	
	@Test
	public void getEndOffset() {
		SourceContent sourceContent;
		
		sourceContent= new SourceContent(0, "");
		assertEquals(0, sourceContent.getEndOffset());
		sourceContent= new SourceContent(0, "abc");
		assertEquals(3, sourceContent.getEndOffset());
		
		sourceContent= new SourceContent(0, "abc", 32);
		assertEquals(32 + 3, sourceContent.getEndOffset());
	}
	
	
	@Test
	public void getString() {
		SourceContent sourceContent;
		
		sourceContent= new SourceContent(0, "");
		assertEquals("", sourceContent.getString());
		sourceContent= new SourceContent(0, "abc");
		assertEquals("abc", sourceContent.getString());
		
		sourceContent= new SourceContent(0, "abc", 32);
		assertEquals("abc", sourceContent.getString());
	}
	
	@Test
	public void getStringSubstring() {
		SourceContent sourceContent;
		
		sourceContent= new SourceContent(0, "");
		assertEquals("", sourceContent.getString(0, 0));
		sourceContent= new SourceContent(0, "abc");
		assertEquals("bc", sourceContent.getString(1, 3));
		sourceContent= new SourceContent(0, "abc");
		assertEquals("abc", sourceContent.getString(0, 3));
		
		sourceContent= new SourceContent(0, "abc", 32);
		assertEquals("bc", sourceContent.getString(32 + 1, 32 + 3));
		sourceContent= new SourceContent(0, "abc", 32);
		assertEquals("abc", sourceContent.getString(32, 32 + 3));
	}
	
	@Test
	public void getChar() {
		SourceContent sourceContent;
		
		sourceContent= new SourceContent(0, "abc");
		assertEquals('b', sourceContent.getChar(1));
		sourceContent= new SourceContent(0, "abc");
		assertEquals('a', sourceContent.getChar(0));
		
		sourceContent= new SourceContent(0, "abc", 32);
		assertEquals('b', sourceContent.getChar(32 + 1));
		sourceContent= new SourceContent(0, "abc", 32);
		assertEquals('a', sourceContent.getChar(32));
	}
	
	
	@Test
	public void getStringLines() {
		SourceContent sourceContent;
		
		sourceContent= new SourceContent(0, "abc");
		assertLines(newIntList(0, 3), sourceContent.getStringLines());
		sourceContent= new SourceContent(0, "abc\n");
		assertLines(newIntList(0, 4, 4), sourceContent.getStringLines());
		sourceContent= new SourceContent(0, "abc\n2nd line");
		assertLines(newIntList(0, 4, 12), sourceContent.getStringLines());
		
		sourceContent= new SourceContent(0, "abc", 32);
		assertLines(newIntList(0, 3), sourceContent.getStringLines());
		sourceContent= new SourceContent(0, "abc\n", 32);
		assertLines(newIntList(0, 4, 4), sourceContent.getStringLines());
		sourceContent= new SourceContent(0, "abc\n2nd line", 32);
		assertLines(newIntList(0, 4, 12), sourceContent.getStringLines());
	}
	
	private void assertLines(final IntList expectedOffsets, final TextLineInformation actual) {
		for (int line= 0; line < expectedOffsets.size() - 1; line++) {
			assertEquals(expectedOffsets.getAt(line), actual.getStartOffset(line));
			assertEquals(expectedOffsets.getAt(line + 1), actual.getEndOffset(line));
		}
	}
	
}
