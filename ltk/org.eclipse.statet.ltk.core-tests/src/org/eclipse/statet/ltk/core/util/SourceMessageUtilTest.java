/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.source.SourceContent;


@NonNullByDefault
public class SourceMessageUtilTest {
	
	
	public SourceMessageUtilTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void checkSourceText_String_checkArgs() {
		final SourceMessageUtil util= new SourceMessageUtil(new SourceContent(0, ""));
		
		assertThrows(NullPointerException.class, () -> {
			util.checkQuoteText(null);
		});
	}
	
	@Test
	public void checkQuoteText_String_Ascii() {
		final SourceMessageUtil util= new SourceMessageUtil(new SourceContent(0, ""));
		
		assertEquals("❬NUL❭",
				util.checkQuoteText("\u0000") );
		assertEquals("❬SOH❭",
				util.checkQuoteText("\u0001") );
		assertEquals("❬»❭",
				util.checkQuoteText("\t") );
		assertEquals("❬»»»❭",
				util.checkQuoteText("\t\t\t") );
		assertEquals("❬¶❭",
				util.checkQuoteText("\n") );
		assertEquals("❬CR❭",
				util.checkQuoteText("\r") );
		assertEquals("❬¤¶❭",
				util.checkQuoteText("\r\n") );
		assertEquals(" ",
				util.checkQuoteText(" ") );
		for (char c= 0x21; c < 0x7F; c++) {
			final String s= String.valueOf(c);
			assertEquals(s,
					util.checkQuoteText(s) );
		}
		assertEquals("❬DEL❭",
				util.checkQuoteText("\u007F") );
	}
	
	@Test
	public void checkQuoteText_String_Ext() {
		final SourceMessageUtil util= new SourceMessageUtil(new SourceContent(0, ""));
		
		assertEquals("Ä",
				util.checkQuoteText("Ä") );
		assertEquals("Ā",
				util.checkQuoteText("Ā") );
		assertEquals("Ḁ",
				util.checkQuoteText("Ḁ") );
		assertEquals("\u2021",
				util.checkQuoteText("\u2021") );
		assertEquals("\u2028",
				util.checkQuoteText("\u2028") );
		assertEquals("\u2E00",
				util.checkQuoteText("\u2E00") );
		
		// Control
		assertEquals("❬U+2066❭",
				util.checkQuoteText("\u2066") );
		
		// Private
		assertEquals("❬U+E000❭",
				util.checkQuoteText("\uE000") );
	}
	
	@Test
	public void checkQuoteText_String_Surrogates() {
		final SourceMessageUtil util= new SourceMessageUtil(new SourceContent(0, ""));
		
		assertEquals("❬U+010900❭",
				util.checkQuoteText("\uD802\uDD00") );
		assertEquals("❬U+0F0000❭",
				util.checkQuoteText("\uDB80\uDC00") );
		
		// Incomplete
		assertEquals("❬!\u200AU+DB80❭",
				util.checkQuoteText("\uDB80") );
		assertEquals("❬!\u200AU+DC00❭",
				util.checkQuoteText("\uDC00") );
	}
	
	@Test
	public void checkQuoteText_String() {
		final SourceMessageUtil util= new SourceMessageUtil(new SourceContent(0, ""));
		
		assertEquals("",
				util.checkQuoteText("") );
		assertEquals("Abc ❬»❭#comment❬¶❭",
				util.checkQuoteText("Abc \t#comment\n") );
	}
	
	@Test
	public void checkQuoteText_String_limited() {
		final SourceMessageUtil util= new SourceMessageUtil(new SourceContent(0, ""));
		
		assertEquals("",
				util.checkQuoteText("", 10) );
		assertEquals("Abc ❬»❭#comm",
				util.checkQuoteText("Abc \t#comm", 10) );
		assertEquals("Abc ❬»❭#c\u200A…",
				util.checkQuoteText("Abc \t#comment\n", 10) );
		
		assertEquals("Abc ❬»❭#\u200A…",
				util.checkQuoteText("Abc \t#\uDB80omment\n", 10) );
	}
	
}
