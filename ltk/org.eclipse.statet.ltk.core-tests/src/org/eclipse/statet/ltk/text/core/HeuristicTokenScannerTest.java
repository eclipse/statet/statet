/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.text.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.ROUND_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import static org.eclipse.statet.ecommons.text.core.TextTokenScanner.NOT_FOUND;
import static org.eclipse.statet.ecommons.text.core.TextTokenScanner.NO_CHAR;
import static org.eclipse.statet.ecommons.text.core.TextTokenScanner.UNBOUND;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.CharPairSet;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;
import org.eclipse.statet.ecommons.text.core.sections.BasicDocContentSections;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.core.util.FixDocumentPartitioner;


@NonNullByDefault
public class HeuristicTokenScannerTest {
	
	
	private static final String PARTITIONING= "TEST";
	
	private static final String DEFAULT_CONTENT_TYPE= "default";
	private static final String SPECIAL_CONTENT_TYPE= "string";
	
	private static final ImList<String> CONTENT_TYPES= ImCollections.newList(DEFAULT_CONTENT_TYPE, SPECIAL_CONTENT_TYPE);
	
	private static final DocContentSections DOC_CONTENT_SECTIONS= new BasicDocContentSections(PARTITIONING, "TypeA") {
		
		@Override
		public String getTypeByPartition(final String contentType) {
			return "TypeA";
		}
		
	};
	
	private static final PartitionConstraint DEFAULT_PARTITION_CONSTRAINT= new PartitionConstraint() {
		
		@Override
		public boolean matches(final String contentType) {
			return (contentType == DEFAULT_CONTENT_TYPE);
		}
		
	};
	
	
	private static final String D1_CONTENT= """
				{\s
					{	var array= ((123, 345);
						var strings= [\\[ " & " ];
						var strings= [ ")]}" \\]];
						var result= open(nested(\\);
					})
				}
				""";
	
	private static final int D1_L0= 0;
	private static final int D1_L1= D1_L0 + 3;
	private static final int D1_L2= D1_L1 + 27;
	private static final int D1_L3= D1_L2 + 28;
	private static final int D1_L4= D1_L3 + 28;
	private static final int D1_L5= D1_L4 + 30;
	private static final int D1_L6= D1_L5 + 4;
	private static final int D1_L7= D1_L6 + 2;
	private static final int D1_LENGTH= D1_L7;
	private static final int D1_S1= D1_L2 + 19;
	private static final int D1_S1_LENGTH= 5;
	private static final int D1_S2= D1_L3 + 17;
	private static final int D1_S2_LENGTH= 5;
	
	private static final CharPairSet PAIRS= new CharPairSet(
				ImCollections.newIdentityList(CURLY_BRACKETS, SQUARE_BRACKETS, ROUND_BRACKETS) );
	private static final CharPairSet PAIRS_WITH_ESC= new CharPairSet(
				ImCollections.newIdentityList(CURLY_BRACKETS, SQUARE_BRACKETS, ROUND_BRACKETS), '\\' );
	
	private static final byte ANY_PART=     1 << 0;
	private static final byte DEFAULT_PART= 1 << 1;
	
	private final Document document= new Document();
	
	private final HeuristicTokenScanner scanner= new HeuristicTokenScanner(DOC_CONTENT_SECTIONS, DEFAULT_PARTITION_CONSTRAINT);
	
	
	private void initTestDoc1() {
		this.document.set(D1_CONTENT);
		
		final var partitioner= new FixDocumentPartitioner(CONTENT_TYPES);
		partitioner.append(DEFAULT_CONTENT_TYPE, D1_L2 + 19);
		partitioner.append(SPECIAL_CONTENT_TYPE, D1_S1_LENGTH);
		partitioner.append(DEFAULT_CONTENT_TYPE, 21);
		partitioner.append(SPECIAL_CONTENT_TYPE, D1_S2_LENGTH);
		partitioner.append(DEFAULT_CONTENT_TYPE, D1_LENGTH - (D1_L2 + 19 + D1_S1_LENGTH + 21 + D1_S2_LENGTH));
		partitioner.connect(this.document);
		this.document.setDocumentPartitioner(PARTITIONING, partitioner);
	}
	
	
	@Test
	public void getDocumentPartitioning() {
		assertSame(PARTITIONING, this.scanner.getDocumentPartitioning());
	}
	
	@Test
	public void getDefaultPartitionConstraint() {
		assertSame(DEFAULT_PARTITION_CONSTRAINT, this.scanner.getDefaultPartitionConstraint());
	}
	
	@Test
	public void getDefaultBrackets() {
		assertEquals(PAIRS, this.scanner.getDefaultBrackets());
	}
	
	
	@Test
	public void getDocument() {
		this.scanner.configure(this.document);
		
		assertSame(this.document, this.scanner.getDocument());
	}
	
	
	private static interface StartBoundFunction {
		
		public int apply(int start, int bound) throws BadLocationException;
		
	}
	
	private void assertFindNonSSpaceForward(final byte part, final StartBoundFunction method)
			throws BadLocationException {
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 0, UNBOUND) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 0, D1_L0 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 0, D1_L0 + 0) );
		assertEquals(D1_L0 + 2,
				method.apply(D1_L0 + 1, UNBOUND) );
		assertEquals(D1_L0 + 2,
				method.apply(D1_L0 + 1, D1_L0 + 3) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 1, D1_L0 + 2) );
		assertEquals(D1_L0 + 2,
				method.apply(D1_L0 + 2, UNBOUND) );
		assertEquals(D1_L0 + 2,
				method.apply(D1_L0 + 2, D1_L0 + 3) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 2, D1_L0 + 2) );
		
		switch (part) {
		case ANY_PART:
			assertEquals(D1_S1 + 0,
					method.apply(D1_S1 + 0, UNBOUND) );
			break;
		case DEFAULT_PART:
			assertEquals(D1_S1 + D1_S1_LENGTH + 1,
					method.apply(D1_S1 + 0, UNBOUND) );
			break;
		}
		
		assertEquals(D1_L6 + 0,
				method.apply(D1_L6 + 0, UNBOUND) );
		assertEquals(D1_L6 + 1,
				method.apply(D1_L6 + 1, UNBOUND) );
		assertEquals(D1_L6 + 1,
				method.apply(D1_L6 + 1, D1_L6 + 2) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L6 + 1, D1_L6 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L7 + 0, UNBOUND) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L7 + 0, D1_LENGTH) );
	}
	
	private void assertFindNonMSpaceForward(final byte part, final StartBoundFunction method)
			throws BadLocationException {
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 0, UNBOUND) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 0, D1_L0 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 0, D1_L0 + 0) );
		assertEquals(D1_L1 + 1,
				method.apply(D1_L0 + 1, UNBOUND) );
		assertEquals(D1_L1 + 1,
				method.apply(D1_L0 + 1, D1_L1 + 2) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 1, D1_L1 + 1) );
		assertEquals(D1_L1 + 1,
				method.apply(D1_L0 + 2, UNBOUND) );
		assertEquals(D1_L1 + 1,
				method.apply(D1_L0 + 2, D1_L1 + 2) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 2, D1_L1 + 1) );
		
		switch (part) {
		case ANY_PART:
			assertEquals(D1_S1 + 0,
					method.apply(D1_S1 + 0, UNBOUND) );
			break;
		case DEFAULT_PART:
			assertEquals(D1_S1 + D1_S1_LENGTH + 1,
					method.apply(D1_S1 + 0, UNBOUND) );
			break;
		}
		
		assertEquals(D1_L6 + 0,
				method.apply(D1_L6 + 0, UNBOUND) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L6 + 1, UNBOUND) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L6 + 1, D1_L6 + 2) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L6 + 1, D1_L6 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L7 + 0, UNBOUND) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L7 + 0, D1_LENGTH) );
	}
	
	private void assertFindNonSSpaceBackward(final byte part, final StartBoundFunction method)
			throws BadLocationException {
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 0, UNBOUND) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 0, D1_L0 + 0) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 1, UNBOUND) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 1, D1_L0 + 0) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 1, D1_L0 + 1) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 2, UNBOUND) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 2, D1_L0 + 0) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 2, D1_L0 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 2, D1_L0 + 2) );
		assertEquals(D1_L0 + 2,
				method.apply(D1_L1 + 0, UNBOUND) );
		assertEquals(D1_L0 + 2,
				method.apply(D1_L1 + 0, D1_L0 + 2) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L1 + 0, D1_L0 + 3) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L1 + 0, D1_L1 + 0) );
		
		switch (part) {
		case ANY_PART:
			assertEquals(D1_S1 + 4,
					method.apply(D1_S1 + D1_S1_LENGTH, UNBOUND) );
			break;
		case DEFAULT_PART:
			assertEquals(D1_S1 - 2,
					method.apply(D1_S1 + D1_S1_LENGTH, UNBOUND) );
			break;
		}
		
		assertEquals(D1_L6 + 1,
				method.apply(D1_L7 + 0, UNBOUND) );
		assertEquals(D1_L6 + 1,
				method.apply(D1_L7 + 0, D1_L6 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L7 + 0, D1_L6 + 2) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L7 + 0, D1_L7 + 0) );
	}
	
	private void assertFindNonMSpaceBackward(final byte part, final StartBoundFunction method)
			throws BadLocationException {
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 0, UNBOUND) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 0, D1_L0 + 0) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 1, UNBOUND) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 1, D1_L0 + 0) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 1, D1_L0 + 1) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 2, UNBOUND) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L0 + 2, D1_L0 + 0) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 2, D1_L0 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L0 + 2, D1_L0 + 2) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L1 + 0, UNBOUND) );
		assertEquals(D1_L0 + 0,
				method.apply(D1_L1 + 0, D1_L0 + 0) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L1 + 0, D1_L0 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L1 + 0, D1_L1 + 0) );
		
		switch (part) {
		case ANY_PART:
			assertEquals(D1_S1 + 4,
					method.apply(D1_S1 + D1_S1_LENGTH, UNBOUND) );
			break;
		case DEFAULT_PART:
			assertEquals(D1_S1 - 2,
					method.apply(D1_S1 + D1_S1_LENGTH, UNBOUND) );
			break;
		}
		
		assertEquals(D1_L6 + 0,
				method.apply(D1_L7 + 0, UNBOUND) );
		assertEquals(D1_L6 + 0,
				method.apply(D1_L7 + 0, D1_L6 + 0) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L7 + 0, D1_L6 + 1) );
		assertEquals(NOT_FOUND,
				method.apply(D1_L7 + 0, D1_L7 + 0) );
	}
	
	@Test
	public void findAnyNonSSpaceForward_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertFindNonSSpaceForward(ANY_PART,
				(start, bound) -> this.scanner.findAnyNonSSpaceForward(start, bound) );
	}
	
	@Test
	public void findAnyNonSSpaceForward_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertFindNonSSpaceForward(ANY_PART,
				(start, bound) -> this.scanner.findAnyNonSSpaceForward(start, bound) );
	}
	
	@Test
	public void findAnyNonMSpaceForward_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertFindNonMSpaceForward(ANY_PART,
				(start, bound) -> this.scanner.findAnyNonMSpaceForward(start, bound) );
	}
	
	@Test
	public void findAnyNonMSpaceForward_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertFindNonMSpaceForward(ANY_PART,
				(start, bound) -> this.scanner.findAnyNonMSpaceForward(start, bound) );
	}
	
	@Test
	public void findAnyNonSSpaceBackward_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertFindNonSSpaceBackward(ANY_PART,
				(start, bound) -> this.scanner.findAnyNonSSpaceBackward(start, bound) );
	}
	
	@Test
	public void findAnyNonSSpaceBackward_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertFindNonSSpaceBackward(ANY_PART,
				(start, bound) -> this.scanner.findAnyNonSSpaceBackward(start, bound) );
	}
	
	@Test
	public void findAnyNonMSpaceBackward_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertFindNonMSpaceBackward(ANY_PART,
				(start, bound) -> this.scanner.findAnyNonMSpaceBackward(start, bound) );
	}
	
	@Test
	public void findAnyNonMSpaceBackward_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertFindNonMSpaceBackward(ANY_PART,
				(start, bound) -> this.scanner.findAnyNonMSpaceBackward(start, bound) );
	}
	
	@Test
	public void findNonSSpaceForward_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertFindNonSSpaceForward(ANY_PART,
				(start, bound) -> this.scanner.findNonSSpaceForward(start, bound) );
	}
	
	@Test
	public void findNonSSpaceForward_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertFindNonSSpaceForward(DEFAULT_PART,
				(start, bound) -> this.scanner.findNonSSpaceForward(start, bound) );
	}
	
	@Test
	public void findNonMSpaceForward_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertFindNonMSpaceForward(ANY_PART,
				(start, bound) -> this.scanner.findNonMSpaceForward(start, bound) );
	}
	
	@Test
	public void findNonMSpaceForward_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertFindNonMSpaceForward(DEFAULT_PART,
				(start, bound) -> this.scanner.findNonMSpaceForward(start, bound) );
	}
	
	@Test
	public void findNonSSpaceBackward_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertFindNonSSpaceBackward(ANY_PART,
				(start, bound) -> this.scanner.findNonSSpaceBackward(start, bound) );
	}
	
	@Test
	public void findNonSSpaceBackward_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertFindNonSSpaceBackward(DEFAULT_PART,
				(start, bound) -> this.scanner.findNonSSpaceBackward(start, bound) );
	}
	
	@Test
	public void findNonMSpaceBackward_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertFindNonMSpaceBackward(ANY_PART,
				(start, bound) -> this.scanner.findNonMSpaceBackward(start, bound) );
	}
	
	@Test
	public void findNonMSpaceBackward_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertFindNonMSpaceBackward(DEFAULT_PART,
				(start, bound) -> this.scanner.findNonMSpaceBackward(start, bound) );
	}
	
	
	@Test
	public void findClosingPeer_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertEquals(D1_L6 + 0,
				this.scanner.findClosingPeer(D1_L0 + 0, CURLY_BRACKETS, NO_CHAR) );
		assertEquals(D1_L5 + 1,
				this.scanner.findClosingPeer(D1_L0 + 1, CURLY_BRACKETS, NO_CHAR) );
		
		assertEquals(NOT_FOUND,
				this.scanner.findClosingPeer(D1_L1 + 14, ROUND_BRACKETS, NO_CHAR) );
		assertEquals(D1_S2 + 1,
				this.scanner.findClosingPeer(D1_L1 + 15, ROUND_BRACKETS, NO_CHAR) );
		
		assertEquals(D1_L3 + 24,
				this.scanner.findClosingPeer(D1_L3 + 22, SQUARE_BRACKETS, NO_CHAR) );
		assertEquals(D1_L3 + 25,
				this.scanner.findClosingPeer(D1_L3 + 22, SQUARE_BRACKETS, '\\') );
	}
	
	@Test
	public void findClosingPeer_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertEquals(NOT_FOUND,
				this.scanner.findClosingPeer(D1_L0 + 0, CURLY_BRACKETS, NO_CHAR) );
		assertEquals(D1_L6 + 0,
				this.scanner.findClosingPeer(D1_L0 + 1, CURLY_BRACKETS, NO_CHAR) );
		
		assertEquals(NOT_FOUND,
				this.scanner.findClosingPeer(D1_L1 + 14, ROUND_BRACKETS, NO_CHAR) );
		assertEquals(NOT_FOUND,
				this.scanner.findClosingPeer(D1_L1 + 15, ROUND_BRACKETS, NO_CHAR) );
		
		assertEquals(D1_L3 + 24,
				this.scanner.findClosingPeer(D1_L3 + 22, SQUARE_BRACKETS, NO_CHAR) );
		assertEquals(D1_L3 + 25,
				this.scanner.findClosingPeer(D1_L3 + 22, SQUARE_BRACKETS, '\\') );
		assertEquals(D1_L3 + 24,
				this.scanner.findClosingPeer(D1_L3 + 16, SQUARE_BRACKETS, NO_CHAR) );
		assertEquals(D1_L3 + 25,
				this.scanner.findClosingPeer(D1_L3 + 16, SQUARE_BRACKETS, '\\') );
	}
	
	@Test
	public void findOpeningPeer_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertEquals(NOT_FOUND,
				this.scanner.findOpeningPeer(D1_LENGTH, CURLY_BRACKETS, NO_CHAR) );
		assertEquals(NOT_FOUND,
				this.scanner.findOpeningPeer(D1_L6 + 0, CURLY_BRACKETS, NO_CHAR) );
		assertEquals(D1_L0 + 0,
				this.scanner.findOpeningPeer(D1_L5 + 0, CURLY_BRACKETS, NO_CHAR) );
		
		assertEquals(D1_L4 + 18,
				this.scanner.findOpeningPeer(D1_L5 + 2, ROUND_BRACKETS, NO_CHAR) );
		assertEquals(NOT_FOUND,
				this.scanner.findOpeningPeer(D1_L5 + 3, ROUND_BRACKETS, NO_CHAR) );
		
		assertEquals(D1_L2 + 17,
				this.scanner.findOpeningPeer(D1_L2 + 25, SQUARE_BRACKETS, NO_CHAR) );
		assertEquals(D1_L2 + 15,
				this.scanner.findOpeningPeer(D1_L2 + 25, SQUARE_BRACKETS, '\\') );
	}
	
	@Test
	public void findOpeningPeer_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertEquals(NOT_FOUND,
				this.scanner.findOpeningPeer(D1_LENGTH, CURLY_BRACKETS, NO_CHAR) );
		assertEquals(D1_L0 + 0,
				this.scanner.findOpeningPeer(D1_L6 + 0, CURLY_BRACKETS, NO_CHAR) );
		assertEquals(D1_L1 + 1,
				this.scanner.findOpeningPeer(D1_L5 + 0, CURLY_BRACKETS, NO_CHAR) );
		
		assertEquals(D1_L4 + 18,
				this.scanner.findOpeningPeer(D1_L5 + 2, ROUND_BRACKETS, NO_CHAR) );
		assertEquals(D1_L1 + 14,
				this.scanner.findOpeningPeer(D1_L5 + 3, ROUND_BRACKETS, NO_CHAR) );
		
		assertEquals(D1_L2 + 17,
				this.scanner.findOpeningPeer(D1_L2 + 25, SQUARE_BRACKETS, NO_CHAR) );
		assertEquals(D1_L2 + 15,
				this.scanner.findOpeningPeer(D1_L2 + 25, SQUARE_BRACKETS, '\\') );
		assertEquals(D1_L2 + 15,
				this.scanner.findOpeningPeer(D1_L3 + 25, SQUARE_BRACKETS, NO_CHAR) );
		assertEquals(D1_L3 + 15,
				this.scanner.findOpeningPeer(D1_L3 + 25, SQUARE_BRACKETS, '\\') );
	}
	
	@Test
	public void computePairBalance_singlePair_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertEquals(0,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, NO_CHAR, 0 ) );
		assertEquals(0,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, NO_CHAR, 1 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, NO_CHAR, 2 ) );
		assertEquals(0,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, '\\', 0 ) );
		assertEquals(0,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, '\\', 1 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, '\\', 2 ) );
		
		final var l2= new BasicTextRegion(D1_L2, D1_L3);
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, l2,
						SQUARE_BRACKETS, NO_CHAR, 0 ) );
		assertEquals(2,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, l2,
						SQUARE_BRACKETS, NO_CHAR, 1 ) );
		assertEquals(0,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, l2,
						SQUARE_BRACKETS, '\\', 0 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, l2,
						SQUARE_BRACKETS, '\\', 1 ) );
		
		final var l3= new BasicTextRegion(D1_L3, D1_L4);
		assertEquals(0,
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						SQUARE_BRACKETS, NO_CHAR, 2 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						SQUARE_BRACKETS, NO_CHAR, 3 ) );
		assertEquals(0,
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						SQUARE_BRACKETS, '\\', 1 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						SQUARE_BRACKETS, '\\', 2 ) );
	}
	
	@Test
	public void computePairBalance_singlePair_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertEquals(0,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, NO_CHAR, 0 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, NO_CHAR, 1 ) );
		assertEquals(2,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, NO_CHAR, 2 ) );
		assertEquals(0,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, '\\', 0 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, '\\', 1 ) );
		assertEquals(2,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, null,
						SQUARE_BRACKETS, '\\', 2 ) );
		
		final var l2= new BasicTextRegion(D1_L2, D1_L3);
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, l2,
						SQUARE_BRACKETS, NO_CHAR, 0 ) );
		assertEquals(2,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, l2,
						SQUARE_BRACKETS, NO_CHAR, 1 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 17, D1_L2 + 25, l2,
						SQUARE_BRACKETS, NO_CHAR, 1 ) );
		assertEquals(0,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, l2,
						SQUARE_BRACKETS, '\\', 0 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 18, D1_L2 + 25, l2,
						SQUARE_BRACKETS, '\\', 1 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L2 + 17, D1_L2 + 25, l2,
						SQUARE_BRACKETS, '\\', 1 ) );
		
		final var l3= new BasicTextRegion(D1_L3, D1_L4);
		assertEquals(0,
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						SQUARE_BRACKETS, NO_CHAR, 1 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						SQUARE_BRACKETS, NO_CHAR, 2 ) );
		assertEquals(0,
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						SQUARE_BRACKETS, '\\', 0 ) );
		assertEquals(1,
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						SQUARE_BRACKETS, '\\', 1 ) );
	}
	
	@Test
	public void computePairBalance_PairSet_allPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configure(this.document);
		
		assertArrayEquals(new int[] { 0, 0, 1 },
				this.scanner.computePairBalance(D1_L4 + 19, D1_L4 + 19, null,
						PAIRS, new int[] { 0, 0, 0 }, 2) );
		assertArrayEquals(new int[] { 0, 0, 2 },
				this.scanner.computePairBalance(D1_L4 + 19, D1_L4 + 19, null,
						PAIRS, new int[] { 0, 0, 1 }, 2) );
		assertArrayEquals(new int[] { 0, 0, 1 },
				this.scanner.computePairBalance(D1_L4 + 18, D1_L4 + 19, null,
						PAIRS, new int[] { 0, 0, 1 }, 2) );
		
		assertArrayEquals(new int[] { 0, 0, 2 },
				this.scanner.computePairBalance(D1_L4 + 19, D1_L4 + 19, null,
						PAIRS_WITH_ESC, new int[] { 0, 0, 0 }, 2) );
		assertArrayEquals(new int[] { 0, 0, 3 },
				this.scanner.computePairBalance(D1_L4 + 19, D1_L4 + 19, null,
						PAIRS_WITH_ESC, new int[] { 0, 0, 1 }, 2) );
		assertArrayEquals(new int[] { 0, 0, 2 },
				this.scanner.computePairBalance(D1_L4 + 18, D1_L4 + 19, null,
						PAIRS_WITH_ESC, new int[] { 0, 0, 1 }, 2) );
		
		assertArrayEquals(new int[] { 0, 2, 0 },
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, null,
						PAIRS, new int[] { 0, 0, 0 }, 1) );
		assertArrayEquals(new int[] { 0, 1, 0 },
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, null,
						PAIRS_WITH_ESC, new int[] { 0, 0, 0 }, 1) );
		
		final var l3= new BasicTextRegion(D1_L3, D1_L4);
		assertArrayEquals(new int[] { -1, -2, -1 },
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						PAIRS, new int[] { 0, 0, 0 }, 1) );
		assertArrayEquals(new int[] { -1, -1, -1 },
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						PAIRS_WITH_ESC, new int[] { 0, 0, 0 }, 1) );
	}
	
	@Test
	public void computePairBalance_PairSet_defaultPart() throws BadLocationException {
		initTestDoc1();
		
		this.scanner.configureDefaultPartitions(this.document);
		
		assertArrayEquals(new int[] { 0, 0, 2 },
				this.scanner.computePairBalance(D1_L4 + 19, D1_L4 + 19, null,
						PAIRS, new int[] { 0, 0, 0 }, 2) );
		assertArrayEquals(new int[] { 0, 0, 3 },
				this.scanner.computePairBalance(D1_L4 + 19, D1_L4 + 19, null,
						PAIRS, new int[] { 0, 0, 1 }, 2) );
		assertArrayEquals(new int[] { 0, 0, 2 },
				this.scanner.computePairBalance(D1_L4 + 18, D1_L4 + 19, null,
						PAIRS, new int[] { 0, 0, 1 }, 2) );
		
		assertArrayEquals(new int[] { 0, 0, 3 },
				this.scanner.computePairBalance(D1_L4 + 19, D1_L4 + 19, null,
						PAIRS_WITH_ESC, new int[] { 0, 0, 0 }, 2) );
		assertArrayEquals(new int[] { 0, 0, 4 },
				this.scanner.computePairBalance(D1_L4 + 19, D1_L4 + 19, null,
						PAIRS_WITH_ESC, new int[] { 0, 0, 1 }, 2) );
		assertArrayEquals(new int[] { 0, 0, 3 },
				this.scanner.computePairBalance(D1_L4 + 18, D1_L4 + 19, null,
						PAIRS_WITH_ESC, new int[] { 0, 0, 1 }, 2) );
		
		assertArrayEquals(new int[] { -2, 0, 1 },
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, null,
						PAIRS, new int[] { 0, 0, 0 }, 1) );
		assertArrayEquals(new int[] { -2, 0, 2 },
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, null,
						PAIRS_WITH_ESC, new int[] { 0, 0, 0 }, 1) );
		
		final var l3= new BasicTextRegion(D1_L3, D1_L4);
		assertArrayEquals(new int[] { 0, -1, 0 },
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						PAIRS, new int[] { 0, 0, 0 }, 1) );
		assertArrayEquals(new int[] { 0, 0, 0 },
				this.scanner.computePairBalance(D1_L3 + 16, D1_L3 + 17, l3,
						PAIRS_WITH_ESC, new int[] { 0, 0, 0 }, 1) );
	}
	
}
