/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.buildpath.ui;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.ltk.buildpath.ui.BuildpathsUIResources;


public class BuildpathsUIPlugin extends AbstractUIPlugin {
	
	
	private static BuildpathsUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static BuildpathsUIPlugin getInstance() {
		return instance;
	}
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		synchronized (this) {
			this.started= true;
		}
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
			
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	private void checkStarted() {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
	}
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		checkStarted();
		
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(BuildpathsUIResources.OBJ_SOURCE_ATTACHMENT_ATTRIBUTE_IMAGE_ID,
				ImageRegistryUtil.T_OBJ, "source_attachment.png" ); //$NON-NLS-1$
		util.register(BuildpathsUIResources.OBJ_INCLUSION_FILTER_ATTRIBUTE_IMAGE_ID,
				ImageRegistryUtil.T_OBJ, "inclusion_filter.png" ); //$NON-NLS-1$
		util.register(BuildpathsUIResources.OBJ_EXCLUSION_FILTER_ATTRIBUTE_IMAGE_ID,
				ImageRegistryUtil.T_OBJ, "exclusion_filter.png" ); //$NON-NLS-1$
		util.register(BuildpathsUIResources.OBJ_OUTPUT_FOLDER_ATTRIBUTE_IMAGE_ID,
				ImageRegistryUtil.T_OBJ, "output_folder.png" ); //$NON-NLS-1$
	}
	
}
