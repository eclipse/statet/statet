/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.ui.wizards;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.internal.ltk.buildpath.ui.Messages;
import org.eclipse.statet.ltk.buildpath.core.BuildpathAttribute;
import org.eclipse.statet.ltk.buildpath.ui.BuildpathListElement;
import org.eclipse.statet.ltk.buildpath.ui.BuildpathsUIDescription;


public class EditFilterWizard extends BuildPathWizard {
	
	
	private FilterWizardPage filterPage;
	
	
	public EditFilterWizard(final ImList<BuildpathListElement> existingEntries,
			final BuildpathListElement newEntry, final BuildpathsUIDescription uiDescription) {
		super(existingEntries, newEntry, Messages.ExclusionInclusion_Dialog_title, null,
				uiDescription);
	}
	
	
	@Override
	public void addPages() {
		super.addPages();
		
		this.filterPage= new FilterWizardPage(getEntryToEdit(), getUIDescription());
		addPage(this.filterPage);
	}
	
	public void setFocus(final String attributeName) {
		switch (attributeName) {
		case BuildpathAttribute.FILTER_INCLUSIONS:
		case BuildpathAttribute.FILTER_EXCLUSIONS:
			this.filterPage.setFocus(attributeName);
			break;
		default:
			break;
		}
	}
	
	@Override
	public boolean performFinish() {
		final BuildpathListElement entryToEdit= getEntryToEdit();
		entryToEdit.setAttribute(BuildpathAttribute.FILTER_INCLUSIONS, this.filterPage.getInclusionPatterns());
		entryToEdit.setAttribute(BuildpathAttribute.FILTER_EXCLUSIONS, this.filterPage.getExclusionPatterns());
		
		return true;
	}
	
}
