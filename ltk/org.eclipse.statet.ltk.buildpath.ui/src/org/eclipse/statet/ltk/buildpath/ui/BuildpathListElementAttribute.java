/*=============================================================================#
 # Copyright (c) 2010, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.ui;

import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.buildpath.core.BasicBuildpathAttribute;
import org.eclipse.statet.ltk.buildpath.core.BuildpathAttribute;


public class BuildpathListElementAttribute {
	
	
	private final BuildpathListElement element;
	
	private final String name;
	private Object value;
	
	private final boolean builtin;
	private final IStatus status;
	
	
	public BuildpathListElementAttribute(final BuildpathListElement parent,
			final String name, final Object value, final boolean builtin) {
		this.name= name;
		this.value= value;
		this.element= parent;
		
		this.builtin= builtin;
		this.status= parent.getContainerChildStatus(this);
	}
	
	
	public BuildpathListElement getParent() {
		return this.element;
	}
	
	
	/**
	 * Returns the name.
	 * @return String
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Returns the value.
	 * @return Object
	 */
	public Object getValue() {
		return this.value;
	}
	
	/**
	 * Sets the value.
	 * @param value value to set
	 */
	public void setValue(final Object value) {
		this.value= value;
		getParent().attributeChanged(this.name);
	}
	
	public boolean isBuiltin() {
		return this.builtin;
	}
	
	public IStatus getStatus() {
		return this.status;
	}
	
	public BuildpathAttribute getCoreAttribute() {
		return new BasicBuildpathAttribute(this.name, (this.value != null) ? this.value.toString() : null);
	}
	
	
	@Override
	public int hashCode() {
		return this.element.getPath().hashCode() + this.name.hashCode() * 89;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final BuildpathListElementAttribute other
						&& this.element.getPath().equals(other.element.getPath())
						&& this.name == other.name ));
	}
	
}
