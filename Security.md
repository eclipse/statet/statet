# Security Policy

This project follows [Eclipse Foundation Vulnerability Reporting Policy](https://www.eclipse.org/security/policy/).


## Reporting a Vulnerability

If you think you have found a vulnerability in Eclipse StatET you can report it using one of the
following ways:

  * Contact the [Eclipse Foundation Security Team](mailto:security@eclipse-foundation.org)
  * Create a [confidential issue](https://gitlab.eclipse.org/security/vulnerability-reports/-/issues/new?issuable_template=new_vulnerability)

You can find more information about reporting and disclosure at the [Eclipse Foundation Security page](https://www.eclipse.org/security/).
