/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics.util;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.rj.eclient.graphics.ERGraphic;
import org.eclipse.statet.rj.ts.core.AbstractRToolRunnable;
import org.eclipse.statet.rj.ts.core.RToolService;


public class CopyToDevRunnable extends AbstractRToolRunnable {
	
	
	private final ERGraphic graphic;
	private final String toDev;
	private final String toDevFile;
	private final String toDevArgs;
	
	
	public CopyToDevRunnable(final ERGraphic graphic, final String toDev,
			final String toDevFile, final String toDevArgs) {
		super("r/rj/gd/copy", "Copy R Graphic ('" + toDev + "')"); //$NON-NLS-1$
		this.graphic= graphic;
		this.toDev= toDev;
		this.toDevFile= toDevFile;
		this.toDevArgs= toDevArgs;
	}
	
	@Override
	public boolean changed(final int event, final Tool tool) {
		switch (event) {
		case MOVING_FROM:
			return false;
		default:
			return true;
		}
	}
	
	@Override
	public void run(final RToolService r, final ProgressMonitor m) throws StatusException {
		this.graphic.copy(this.toDev, this.toDevFile, this.toDevArgs, m);
	}
	
}
