/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.eclient.graphics;


public class RGraphics {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.rj.eclient.graphics"; //$NON-NLS-1$
	
	
	public static final String PREF_DISPLAY_QUALIFIER= BUNDLE_ID + "/display"; //$NON-NLS-1$
	
	/**
	 * Preference key: custom dpi setting (string "horizontal,vertical"), none/empty for default
	 * 
	 * @since 1.0
	 */
	public static final String PREF_DISPLAY_CUSTOM_DPI_KEY= "dpi.xy"; //$NON-NLS-1$
	
	
	public static final String FONTS_PREF_QUALIFIER= BUNDLE_ID + "/fonts"; //$NON-NLS-1$
	public static final String PREF_FONTS_SERIF_FONTNAME_KEY= "serif.name"; //$NON-NLS-1$
	public static final String PREF_FONTS_SANS_FONTNAME_KEY= "sans.name"; //$NON-NLS-1$
	public static final String PREF_FONTS_MONO_FONTNAME_KEY= "mono.name"; //$NON-NLS-1$
	
	/**
	 * Preference key: if (boolean) a special symbol font should be used
	 * 
	 * @since 1.0
	 */
	public static final String PREF_FONTS_SYMBOL_USE_KEY= "symbol.use"; //$NON-NLS-1$
	/**
	 * Preference key: name (string) of the symbol font (if special symbol font is enabled)
	 * 
	 * @since 1.0
	 */
	public static final String PREF_FONTS_SYMBOL_FONTNAME_KEY= "symbol.name"; //$NON-NLS-1$
	/**
	 * Preference key: encoding (known key) of the symbol font (if special symbol font is enabled)
	 * 
	 * @since 1.0
	 */
	public static final String PREF_FONTS_SYMBOL_ENCODING_KEY= "symbol.enc"; //$NON-NLS-1$
	
	
}
