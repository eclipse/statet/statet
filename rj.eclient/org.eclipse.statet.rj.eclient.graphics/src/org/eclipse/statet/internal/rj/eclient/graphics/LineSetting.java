/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.eclient.graphics;

import org.eclipse.swt.SWT;

import org.eclipse.statet.rj.eclient.graphics.ERGraphicInstruction;
import org.eclipse.statet.rj.graphic.core.RLineSetting;


public final class LineSetting extends RLineSetting implements ERGraphicInstruction {
	
	
	public LineSetting(final int type, final float width, final byte cap, final byte join,
			final float joinMiterLimit) {
		super(type, width, cap, join, joinMiterLimit);
	}
	
	
	public int swtCap() {
		switch(this.cap) {
		case CAP_ROUND:
			return SWT.CAP_ROUND;
		case CAP_BUTT:
			return SWT.CAP_FLAT;
		case CAP_SQUARE:
			return SWT.CAP_SQUARE;
		default:
			assert (false);
			return SWT.CAP_ROUND;
		}
	}
	
	public int swtJoin() {
		switch (this.join) {
		case JOIN_ROUND:
			return SWT.JOIN_ROUND;
		case JOIN_MITER:
			return SWT.JOIN_MITER;
		case JOIN_BEVEL:
			return SWT.JOIN_BEVEL;
		default:
			assert (false);
			return SWT.JOIN_ROUND;
		}
	}
	
	public int[] swtDashes() {
		int rPattern= this.type;
		int length= 0;
		while (rPattern != 0) {
			length++;
			rPattern>>>= 4;
		}
		final int[] dashes= new int[length];
		rPattern= this.type;
		for (int i= 0; i < length; i++) {
			dashes[i]= (rPattern & 0xf);
			rPattern>>>= 4;
		}
		return dashes;
	}
	
}
