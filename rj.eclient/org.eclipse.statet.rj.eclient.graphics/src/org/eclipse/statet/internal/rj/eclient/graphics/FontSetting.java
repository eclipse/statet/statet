/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.eclient.graphics;

import org.eclipse.swt.graphics.Font;

import org.eclipse.statet.rj.eclient.graphics.ERGraphicInstruction;
import org.eclipse.statet.rj.graphic.core.RFontSetting;


public final class FontSetting extends RFontSetting implements ERGraphicInstruction {
	
	
	public final Font swtFont;
	public final double[] swtProperties;
	
	
	public FontSetting(final String family, final int face, final float pointSize, final double lineHeight,
			final Font swtFont, final double[] swtProperties) {
		super(family, face, pointSize, lineHeight);
		this.swtFont= swtFont;
		this.swtProperties= swtProperties;
	}
	
	
}
