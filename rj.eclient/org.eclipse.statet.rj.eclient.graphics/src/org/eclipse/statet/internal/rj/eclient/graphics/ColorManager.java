/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.eclient.graphics;

import org.eclipse.swt.graphics.Color;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.collections.IntHashMap;


@NonNullByDefault
public class ColorManager {
	
	
	private final IntHashMap<Color> colors= new IntHashMap<>(0x7f);
	
	
	public ColorManager() {
	}
	
	
	public synchronized Color getColor(final int rgb) {
		Color color= this.colors.get(rgb);
		if (color == null) {
			color= new Color(
					(rgb & 0xff), ((rgb >>> 8) & 0xff), ((rgb >>> 16) & 0xff) );
			this.colors.put(rgb, color);
		}
		return color;
	}
	
	
}
