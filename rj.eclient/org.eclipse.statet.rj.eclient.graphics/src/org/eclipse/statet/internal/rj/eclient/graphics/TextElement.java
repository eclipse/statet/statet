/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.eclient.graphics;

import org.eclipse.statet.rj.eclient.graphics.ERGraphicInstruction;
import org.eclipse.statet.rj.graphic.core.RText;


public class TextElement extends RText implements ERGraphicInstruction {
	
	
	public final double swtStrWidth;
	
	
	public TextElement(final String text,
			final double x, final double y, final double rDeg, final double hAdj,
			final double swtStrWidth) {
		super(text, x, y, rDeg, hAdj);
		this.swtStrWidth= swtStrWidth;
	}
	
	
}
