/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.eclient.graphics;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.rj.eclient.graphics.RGraphics;


public class PreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public PreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IScopeContext defaultScope= DefaultScope.INSTANCE;
		final IEclipsePreferences node= defaultScope.getNode(RGraphics.FONTS_PREF_QUALIFIER);
		if (Platform.getOS().startsWith("win")) {
			node.put(RGraphics.PREF_FONTS_SERIF_FONTNAME_KEY, "Times New Roman"); //$NON-NLS-1$
			node.put(RGraphics.PREF_FONTS_SANS_FONTNAME_KEY, "Arial"); //$NON-NLS-1$
			node.put(RGraphics.PREF_FONTS_MONO_FONTNAME_KEY, "Courier New"); //$NON-NLS-1$
		}
		else {
			node.put(RGraphics.PREF_FONTS_SERIF_FONTNAME_KEY, "Serif"); //$NON-NLS-1$
			node.put(RGraphics.PREF_FONTS_SANS_FONTNAME_KEY, "Sans"); //$NON-NLS-1$
			node.put(RGraphics.PREF_FONTS_MONO_FONTNAME_KEY, "Mono"); //$NON-NLS-1$
		}
		node.putBoolean(RGraphics.PREF_FONTS_SYMBOL_USE_KEY, true);
		node.put(RGraphics.PREF_FONTS_SYMBOL_FONTNAME_KEY, "Symbol"); //$NON-NLS-1$
		node.put(RGraphics.PREF_FONTS_SYMBOL_ENCODING_KEY, "AdobeSymbol"); //$NON-NLS-1$
	}
	
}
