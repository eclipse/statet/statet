/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.eclient.graphics;

import org.eclipse.statet.rj.eclient.graphics.ERGraphicInstruction;
import org.eclipse.statet.rj.graphic.core.RLine;


public class LineElement extends RLine implements ERGraphicInstruction {
	
	
	public LineElement(final double x0, final double y0, final double x1, final double y1) {
		super(x0, y0, x1, y1);
	}
	
	
}
