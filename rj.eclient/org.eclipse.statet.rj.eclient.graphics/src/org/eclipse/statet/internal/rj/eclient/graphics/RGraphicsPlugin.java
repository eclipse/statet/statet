/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.eclient.graphics;

import static org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil.T_LOCTOOL;

import static org.eclipse.statet.rj.eclient.graphics.RGraphics.BUNDLE_ID;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.rj.eclient.graphics.RGraphics;


/**
 * Plug-in <code>org.eclipse.statet.rj.eclient.graphics</code>.
 */
public class RGraphicsPlugin extends AbstractUIPlugin {
	
	
	public static final String IMG_LOCTOOL_RESIZE_FIT_R=        BUNDLE_ID + "/images/loctool/resize-fit-r"; //$NON-NLS-1$
	
	public static final String IMG_LOCTOOL_LOCATOR_DONE=        BUNDLE_ID + "/images/loctool/locator-done"; //$NON-NLS-1$
	public static final String IMG_LOCTOOL_LOCATOR_CANCEL=      BUNDLE_ID + "/images/loctool/locator-cancel"; //$NON-NLS-1$
	
	
	private static RGraphicsPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static RGraphicsPlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private final List<Disposable> disposables= new ArrayList<>();
	
	
	public RGraphicsPlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					log(new Status(IStatus.ERROR, RGraphics.BUNDLE_ID,
							"Error occured when dispose module", //$NON-NLS-1$
							e ));
				}
			}
			this.disposables.clear();
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(IMG_LOCTOOL_RESIZE_FIT_R, T_LOCTOOL, "resize-fit-r.png");
		
		util.register(IMG_LOCTOOL_LOCATOR_DONE, T_LOCTOOL, "locator-done.png");
		util.register(IMG_LOCTOOL_LOCATOR_CANCEL, T_LOCTOOL, "locator-cancel.png");
	}
	
	public void registerPluginDisposable(final Disposable listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.disposables.add(listener);
		}
	}
	
}
