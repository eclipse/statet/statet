/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.help;

import org.eclipse.statet.r.ui.RUI;


/**
 * 
 */
public interface IRUIHelpContextIds {
	
	public static final String R_EDITOR = RUI.BUNDLE_ID + ".r_editor";  //$NON-NLS-1$
	
	public static final String R_ENV = RUI.BUNDLE_ID + ".r_env"; //$NON-NLS-1$
	
	public static final String R_HELP_VIEW = RUI.BUNDLE_ID + ".r_help-view"; //$NON-NLS-1$
	public static final String R_HELP_SEARCH_PAGE = RUI.BUNDLE_ID + ".r_help-search_page"; //$NON-NLS-1$
	
}
