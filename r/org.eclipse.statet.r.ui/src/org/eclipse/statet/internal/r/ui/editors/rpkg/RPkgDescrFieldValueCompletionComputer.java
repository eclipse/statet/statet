/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rpkg;

import static org.eclipse.statet.r.core.model.RCoreFunctions.BASE_C_ELEMENT_NAME;
import static org.eclipse.statet.r.core.model.RCoreFunctions.UTILS_Person_ELEMENT_NAME;
import static org.eclipse.statet.r.core.model.RCoreFunctions.UTILS_Person_NAME;
import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_FIELD_NAME_CONTENT_TYPE;

import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.link.LinkedModeModel;
import org.eclipse.jface.text.link.LinkedModeUI;
import org.eclipse.jface.text.link.LinkedPosition;
import org.eclipse.jface.text.link.LinkedPositionGroup;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.SearchPattern;

import org.eclipse.statet.ecommons.ui.SharedUIResources;

import org.eclipse.statet.dsl.dcf.core.source.ast.Field;
import org.eclipse.statet.internal.r.ui.editors.AbstractRCompletionElementComputer;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.util.EntityRole;
import org.eclipse.statet.ltk.core.util.UserInfo;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SimpleCompletionProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SourceProposal.ProposalParameters;
import org.eclipse.statet.r.core.model.RCoreFunctions;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RFunctionSpec;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;
import org.eclipse.statet.r.core.model.RPkgDescriptions;
import org.eclipse.statet.r.core.model.rlang.BasicRFrame;
import org.eclipse.statet.r.core.model.rlang.BasicRFrameSearchPath;
import org.eclipse.statet.r.core.model.rlang.BasicRLangMethod;
import org.eclipse.statet.r.core.model.rlang.RFrame;
import org.eclipse.statet.r.core.model.rlang.RFrameSearchPath;
import org.eclipse.statet.r.core.model.rlang.RLangElement;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext.FCallInfo;
import org.eclipse.statet.r.ui.sourceediting.RPkgDescrAssistInvocationContext;


@NonNullByDefault
public class RPkgDescrFieldValueCompletionComputer
		extends AbstractRCompletionElementComputer implements ContentAssistComputer {
	
	
	private static final int AUTHOR_NAME_SUBJECT= 1;
	private static final int AUTHOR_EMAIL_SUBJECT= 2;
	
	private static final ImSet<String> YES_NO_VALUES= ImCollections.newSet("yes", "no"); //$NON-NLS-1$ //$NON-NLS-2$
	
	
	protected RPkgDescrAssistInvocationContext descrContext;
	
	protected Field fieldNode;
	protected RPkgDescrFieldDefinition fieldDef;
	
	
	public RPkgDescrFieldValueCompletionComputer() {
		super(0);
	}
	
	
	@SuppressWarnings("null")
	protected boolean initField() {
		this.fieldNode= getDescrContext().getInvocationDescrFieldNode();
		final String fieldName;
		return (this.fieldNode != null
				&& (fieldName= this.fieldNode.getKey().getText()) != null
				&& (this.fieldDef= RPkgDescriptions.getFieldDefinition(fieldName)) != null );
	}
	
	protected void initR(final IProgressMonitor monitor) {
		if (!isRContext()) {
			initR(new RAssistInvocationContext(getDescrContext(), true, monitor), monitor);
		}
	}
	
	@Override
	@SuppressWarnings("null")
	protected void clear() {
		super.clear();
		this.descrContext= null;
		this.fieldNode= null;
		this.fieldDef= null;
	}
	
	
	protected RPkgDescrAssistInvocationContext getDescrContext() {
		return this.descrContext;
	}
	
	protected boolean isRContext() {
		return (getRContext() != null);
	}
	
	
	@Override
	public void computeCompletionProposals(final AssistInvocationContext context,
			final int mode, final AssistProposalCollector proposals,
			final IProgressMonitor monitor) {
		try {
			this.proposals= proposals;
			if (context instanceof RPkgDescrAssistInvocationContext
					&& context.getInvocationContentType() != RPKG_DESCR_FIELD_NAME_CONTENT_TYPE) {
				this.descrContext= (RPkgDescrAssistInvocationContext)context;
				computeCompletionProposals(mode, monitor);
			}
			else if (context instanceof RAssistInvocationContext) {
				this.descrContext= new RPkgDescrAssistInvocationContext(context, true, monitor);
				initR((RAssistInvocationContext)context, monitor);
				computeCompletionProposals(mode, monitor);
			}
		}
		finally {
			clear();
		}
	}
	
	@Override
	public void computeInformationProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		try {
			if (context instanceof RAssistInvocationContext) {
				this.descrContext= new RPkgDescrAssistInvocationContext(context, false, monitor);
				initR((RAssistInvocationContext)context, monitor);
				this.proposals= proposals;
				computeContextProposals(monitor);
			}
		}
		finally {
			clear();
		}
	}
	
	
	@Override
	protected void computeCompletionProposals(final int mode,
			final IProgressMonitor monitor) {
		if (!initField()) {
			return;
		}
		
		final RPkgDescrFieldDefinition fieldDef= this.fieldDef;
		if (fieldDef == RPkgDescriptions.Encoding_FIELD) {
			addEncodingCompletions(monitor);
			return;
		}
		if (fieldDef == RPkgDescriptions.Type_FIELD) {
			addPkgTypeCompletions(monitor);
			return;
		}
		if (fieldDef == RPkgDescriptions.License_FIELD) {
			addLicenseCompletions(monitor);
			return;
		}
		if (fieldDef == RPkgDescriptions.Author_FIELD) {
			addPersonCompletions(true, true, monitor);
			return;
		}
		if (fieldDef == RPkgDescriptions.Maintainer_FIELD) {
			addPersonCompletions(false, false, monitor);
			return;
		}
		if (fieldDef == RPkgDescriptions.Authors_R_FIELD) {
			if (isRContext()) {
				addPersonRCompletions(monitor);
			}
			return;
		}
		if (fieldDef == RPkgDescriptions.NeedsCompilation_FIELD) {
			addFixedCompletions(YES_NO_VALUES, monitor);
			return;
		}
		
		switch (fieldDef.getDataType()) {
		case RPkgDescrFieldDefinition.LOGICAL:
			addLogicalCompletions(monitor);
			return;
		case RPkgDescrFieldDefinition.DATE:
			addDateCompletions(monitor);
			return;
		case RPkgDescrFieldDefinition.TEXT:
			if (getIdentifierPreceding() == '\'') {
				addPackageCompletions(monitor);
			}
			return;
		case RPkgDescrFieldDefinition.PACKAGE_LIST:
		case RPkgDescrFieldDefinition.PACKAGE_DEPEND_LIST:
			addPackageCompletions(monitor);
			return;
		default:
			return;
		}
	}
	
	@Override
	protected void computeContextProposals(final IProgressMonitor monitor) {
		if (!initField()) {
			return;
		}
		
		final RPkgDescrFieldDefinition fieldDef= this.fieldDef;
		if (fieldDef == RPkgDescriptions.Authors_R_FIELD) {
			if (isRContext()) {
				final FCallInfo fCallInfo= getRContext().getFCallInfo();
				if (fCallInfo != null) {
					addFCallArgContexts(fCallInfo, new BasicRFrameSearchPath(createAuthorREnvs()));
				}
			}
		}
	}
	
	
	protected void addEncodingCompletions(final IProgressMonitor monitor) {
		final String prefix= getDescrContext().getIdentifierPrefix();
		
		if (isValueStart(this.fieldNode, getDescrContext().getIdentifierOffset())) {
			final List<String> values= ImCollections.newList("UTF-8"); //$NON-NLS-1$
			
			final var parameters= new ProposalParameters<>(
					getDescrContext(), getDescrContext().getIdentifierOffset(),
					new SearchPattern(SearchPattern.PREFIX_MATCH, prefix) );
			for (final String value : values) {
				if (parameters.matchesNamePattern(value)) {
					this.proposals.add(new SimpleValueCompletionProposal(parameters, value));
				}
			}
		}
	}
	
	protected void addPkgTypeCompletions(final IProgressMonitor monitor) {
		final String prefix= getDescrContext().getIdentifierPrefix();
		
		if (isValueStart(this.fieldNode, getDescrContext().getIdentifierOffset())) {
			final var parameters= new ProposalParameters<>(
					getDescrContext(), getDescrContext().getIdentifierOffset(),
					new SearchPattern(SearchPattern.PREFIX_MATCH, prefix) );
			for (final String value : RPkgDescriptions.PKG_TYPES) {
				if (parameters.matchesNamePattern(value)) {
					this.proposals.add(new SimpleValueCompletionProposal(parameters, value));
				}
			}
		}
	}
	
	protected void addLicenseCompletions(final IProgressMonitor monitor) {
		final String prefix= getDescrContext().getIdentifierPrefix();
		
		final var parameters= new ProposalParameters<>(
				getDescrContext(), getDescrContext().getIdentifierOffset(),
				new SearchPattern(getSearchMatchRules(), prefix) );
		
		final var values= new ArrayList<String>();
		values.add("file LICENSE"); //$NON-NLS-1$
		if (isValueStart(this.fieldNode, getDescrContext().getIdentifierOffset())) {
			values.add("Unlimited"); //$NON-NLS-1$
		}
		
		for (final var spec : RPkgDescriptions.STANDARD_LICENSE_SPECS) {
			if (parameters.matchesNamePattern(spec.abbr())) {
				this.proposals.add(new SimpleValueCompletionProposal(parameters, spec.abbr(),
						spec.name() ));
			}
		}
		for (final String value : values) {
			if (parameters.matchesNamePattern(value)) {
				this.proposals.add(new SimpleValueCompletionProposal(parameters, value));
			}
		}
	}
	
	protected void addLogicalCompletions(final IProgressMonitor monitor) {
		addFixedCompletions(RPkgDescriptions.LOGICAL_VALUES, monitor);
	}
	
	protected void addFixedCompletions(final ImCollection<String> values,
			final IProgressMonitor monitor) {
		final String prefix= getDescrContext().getIdentifierPrefix();
		
		if (isValueStart(this.fieldNode, getDescrContext().getIdentifierOffset())) {
			final var parameters= new ProposalParameters<>(
					getDescrContext(), getDescrContext().getIdentifierOffset(),
					new SearchPattern(SearchPattern.PREFIX_MATCH, prefix) );
			for (final String value : values) {
				if (parameters.matchesNamePattern(value)) {
					this.proposals.add(new SimpleValueCompletionProposal(parameters, value));
				}
			}
		}
	}
	
	protected void addDateCompletions(final IProgressMonitor monitor) {
		final String prefix= getDescrContext().getIdentifierPrefix();
		
		final var parameters= new ProposalParameters<>(
				getDescrContext(), getDescrContext().getIdentifierOffset(),
				new SearchPattern(getSearchMatchRules(), prefix) );
		
		if (isValueStart(this.fieldNode, getDescrContext().getIdentifierOffset())) {
			final LocalDate date= LocalDate.now(Clock.systemUTC());
			final List<String> values= ImCollections.newList(date.toString());
			
			for (final String value : values) {
				if (parameters.matchesNamePattern(value)) {
					this.proposals.add(new SimpleValueCompletionProposal(parameters, value));
				}
			}
		}
	}
	
	protected void addPersonCompletions(
			final boolean multiple, final boolean roles,
			final IProgressMonitor monitor) {
		final String prefix= getDescrContext().getIdentifierPrefix();
		final int preceding= getIdentifierPreceding();
		
		if (roles
				&& isInSquareBrackets(getDescrContext().getIdentifierOffset()) ) {
			final var parameters= new ProposalParameters<>(
					getDescrContext(), getDescrContext().getIdentifierOffset(),
					new SearchPattern(SearchPattern.PREFIX_MATCH, prefix), 30 );
			for (final var role : EntityRole.COMMON_ROLES) {
				if ((role.getFlags() & EntityRole.R_PERSON) != 0
						&& parameters.matchesNamePattern(role.getCode())) {
					this.proposals.add(new SimpleValueCompletionProposal(parameters,
							role.getCode(), role.getName() ));
				}
			}
			return;
		}
		
		{	final var parameters= new ProposalParameters<>(
					getDescrContext(), getDescrContext().getIdentifierOffset(),
					new SearchPattern(getSearchMatchRules(), prefix) );
			
			final UserInfo authorInfo= UserInfo.getAuthorInfo();
			final String authorName= authorInfo.getName();
			if (preceding == '<') {
				final List<String> values= new ArrayList<>();
				if (!authorInfo.getEmail().isEmpty()) {
					values.add(authorInfo.getEmail());
				}
				
				for (final String value : values) {
					if (parameters.matchesNamePattern(value)) {
						this.proposals.add(new SimpleValueCompletionProposal(parameters, value));
					}
				}
			}
			else if (multiple
					|| isValueStart(this.fieldNode, getDescrContext().getIdentifierOffset()) ) {
				final List<String> persons= new ArrayList<>();
				if (!authorName.isEmpty()) {
					persons.add(authorName);
					if (!authorInfo.getEmail().isEmpty()) {
						persons.add(authorName + " <" + authorInfo.getEmail() + '>'); //$NON-NLS-1$
					}
				}
				
				for (final String value : persons) {
					if (parameters.matchesNamePattern(value)) {
						this.proposals.add(new PersonValueCompletionProposal(parameters, value));
					}
				}
			}
		}
	}
	
	@Override
	protected boolean checkArgsDef(final RFunctionSpec. @Nullable Parameter parameter,
			final @Nullable RElementName fName,
			final boolean guess, final int relevance) {
		if (super.checkArgsDef(parameter, fName, guess, relevance)) {
			return true;
		}
		String name;
		if (parameter != null
				&& (name= parameter.getName()) != null) {
			final int prio= ARG_TYPE_PRIO + relevance;
			if (this.fieldDef == RPkgDescriptions.Authors_R_FIELD) {
				switch (name) {
				case "given": //$NON-NLS-1$
				case "family": //$NON-NLS-1$
				case "middle": //$NON-NLS-1$
					addSubject(AUTHOR_NAME_SUBJECT, prio);
					return true;
				case "email": //$NON-NLS-1$
					addSubject(AUTHOR_EMAIL_SUBJECT, prio);
					return true;
				default:
					return false;
				}
			}
		}
		return false;
	}
	
	protected void addPersonRCompletions(final IProgressMonitor monitor) {
		final RElementName prefixName= getRContext().getIdentifierElementName();
		final String prefix= getDescrContext().getIdentifierPrefix();
		
		final UserInfo authorInfo= UserInfo.getAuthorInfo();
		final String authorName= authorInfo.getName();
		
		final RFrameSearchPath searchPath= new BasicRFrameSearchPath(createAuthorREnvs());
		
		FCallInfo personFCallInfo= null;
		final FCallInfo fCallInfo= getRContext().getFCallInfo();
		if (fCallInfo != null) {
			if (isFun(UTILS_Person_ELEMENT_NAME, fCallInfo.getAccess())) {
				personFCallInfo= fCallInfo;
			}
			else {
				personFCallInfo= findFCall(fCallInfo.getNode().getRParent(), UTILS_Person_NAME);
			}
		}
		if (prefixName != null && prefixName.getNextSegment() == null) {
			if (this.in == IN_DEFAULT) {
				try {
					addMainElementCompletions(searchPath, prefixName);
				}
				catch (final Exception e) {
					log(e);
				}
			}
			
			if (fCallInfo != null) {
				try {
					addFCallArgCompletions(fCallInfo, prefixName, searchPath);
				}
				catch (final Exception e) {
					log(e);
				}
				
				try {
					ContextSubject subject;
					if ((subject= getSubject(RFunctionSpec.RELATOR_CODE)) != null && subject.prio > 0) {
						addRoleCodeCompletions(subject.prio);
					}
					
					final List<String> values= new ArrayList<>();
					if ((subject= getSubject(AUTHOR_NAME_SUBJECT)) != null && subject.prio > 0
							&& !authorName.isEmpty() ) {
						final int spaceIndex= authorName.lastIndexOf(' ');
						if (spaceIndex != -1) {
							values.add(authorName.substring(0, spaceIndex));
							values.add(authorName.substring(spaceIndex + 1, authorName.length()));
						}
						else {
							values.add(authorName);
						}
					}
					if ((subject= getSubject(AUTHOR_EMAIL_SUBJECT)) != null && subject.prio > 0
							&& !authorInfo.getEmail().isEmpty() ) {
						values.add(authorInfo.getEmail());
					}
					if (!values.isEmpty()) {
						final var parameters= new ProposalParameters<>(
								getDescrContext(), getDescrContext().getIdentifierOffset(),
								new SearchPattern(getSearchMatchRules(), prefix),
								ARG_TYPE_PRIO );
						
						for (final String value : values) {
							if (parameters.matchesNamePattern(value)) {
								this.proposals.add(new SimpleValueCompletionProposal(parameters, value));
							}
						}
					}
				}
				catch (final Exception e) {
					log(e);
				}
			}
		}
		
		if (this.in == IN_DEFAULT && personFCallInfo == null) {
			if (!authorName.isEmpty()) {
				final StringBuilder sb= new StringBuilder();
				final var rCodeStyle= getDescrContext().getRCoreAccess().getRCodeStyle();
				final int emailIndex= -1;
				
				sb.append(UTILS_Person_NAME + '(');
				final int spaceIndex= authorName.lastIndexOf(' ');
				if (spaceIndex != -1) {
					sb.append('"');
					sb.append(authorName, 0, spaceIndex);
					sb.append("\", \""); //$NON-NLS-1$
					sb.append(authorName, spaceIndex + 1, authorName.length());
					sb.append('"');
				}
				else {
					sb.append('"');
					sb.append(authorName);
					sb.append('"');
				}
				sb.append(", role"); //$NON-NLS-1$
				sb.append(rCodeStyle.getArgAssignString());
				final int roleIndex= sb.length();
				sb.append(')');
				
				final var parameters= new ProposalParameters<>(
										getDescrContext(), getDescrContext().getIdentifierOffset(),
										new SearchPattern(getSearchMatchRules(), prefix) );
				
				if (parameters.matchesNamePattern(authorName)) {
					this.proposals.add(new PersonRValueCompletionProposal(parameters,
							authorName,
							sb.toString(), roleIndex, emailIndex));
					
					if (!authorInfo.getEmail().isEmpty()) {
						sb.delete(sb.length() - 1, sb.length());
						sb.append(", email"); //$NON-NLS-1$
						sb.append(rCodeStyle.getArgAssignString());
						sb.append('"');
						sb.append(authorInfo.getEmail());
						sb.append('"');
						sb.append(')');
						
						this.proposals.add(new PersonRValueCompletionProposal(parameters,
								authorName + " <" + authorInfo.getEmail() + '>', //$NON-NLS-1$
								sb.toString(), roleIndex, emailIndex));
					}
				}
			}
		}
	}
	
	protected ImList<RFrame<?>> createAuthorREnvs() {
		final var rCoreFunctions= RCoreFunctions.getDefinitions(
				getRContext().getRCoreAccess().getRSourceConfig() );
		final var baseEnv= new BasicRFrame<RLangElement<?>>(RFrame.PACKAGE,
				RCoreFunctions.BASE_PACKAGE_NS_ELEMENT_NAME, ImCollections.newList(
						new BasicRLangMethod(BASE_C_ELEMENT_NAME, rCoreFunctions.BASE_C_fSpec) ));
		final var utilsEnv= new BasicRFrame<RLangElement<?>>(RFrame.PACKAGE,
				RCoreFunctions.UTILS_PACKAGE_NS_ELEMENT_NAME, ImCollections.newList(
						new BasicRLangMethod(UTILS_Person_ELEMENT_NAME, rCoreFunctions.UTILS_Person_fSpec) ));
		return ImCollections.newList(utilsEnv, baseEnv);
	}
	
	
	protected void addPackageCompletions(
			final IProgressMonitor monitor) {
		initR(monitor);
		final String prefix= getDescrContext().getIdentifierPrefix();
		
		addPkgNameCompletions(prefix, 0);
	}
	
	
	private int getIdentifierPreceding() {
		try {
			final IDocument document= getDescrContext().getDocument();
			if (getDescrContext().getIdentifierOffset() > 0) {
				return document.getChar(getDescrContext().getIdentifierOffset() - 1);
			}
			return -1;
		}
		catch (final BadLocationException e) {
			return -1;
		}
	}
	
	private boolean isValueStart(final Field fieldNode, final int offset) {
		try {
			final IDocument document= getDescrContext().getDocument();
			int startOffset= fieldNode.getValueIndicatorOffset();
			if (startOffset != AstNode.NA_OFFSET) {
				startOffset++;
			}
			else {
				startOffset= fieldNode.getKey().getEndOffset();
			}
			while (startOffset < offset) {
				switch (document.getChar(startOffset)) {
				case ' ', '\t':
					startOffset++;
					continue;
				default:
					return false;
				}
			}
			return true;
		}
		catch (final BadLocationException e) {
			return false;
		}
	}
	
	private boolean isInSquareBrackets(int offset) {
		try {
			final IDocument document= getDescrContext().getDocument();
			while (--offset >= 0) {
				switch (document.getChar(offset)) {
				case ' ', '\t':
				case ',':
				case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
							'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
							'0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
					continue;
				case '[':
					return true;
				default:
					return false;
				}
			}
			return false;
		}
		catch (final BadLocationException e) {
			return false;
		}
	}
	
	
	private static class SimpleValueCompletionProposal extends SimpleCompletionProposal<RPkgDescrAssistInvocationContext> {
		
		
		public SimpleValueCompletionProposal(final ProposalParameters<? extends RPkgDescrAssistInvocationContext> parameters,
				final String replacementString, final String description) {
			super(parameters, replacementString, description);
		}
		
		public SimpleValueCompletionProposal(final ProposalParameters<? extends RPkgDescrAssistInvocationContext> parameters,
				final String replacementString) {
			super(parameters, replacementString);
		}
		
		
	}
	
	private static class PersonValueCompletionProposal extends SimpleValueCompletionProposal {
		
		
		public PersonValueCompletionProposal(final ProposalParameters<? extends RPkgDescrAssistInvocationContext> parameters,
				final String replacementString) {
			super(parameters, replacementString);
		}
		
		
		@Override
		public Image getImage() {
			return SharedUIResources.getImages().get(SharedUIResources.OBJ_USER_IMAGE_ID);
		}
		
		
		@Override
		public @Nullable CharSequence getPrefixCompletionText(final IDocument document, final int offset) {
			return null;
		}
		
		
	}
	
	private static class PersonRValueCompletionProposal extends PersonValueCompletionProposal {
		
		
		private final String replacementString;
		private final int roleIndex;
		private final int emailIndex;
		
		
		public PersonRValueCompletionProposal(final ProposalParameters<? extends RPkgDescrAssistInvocationContext> parameters,
				final String name, final String replacementString, final int roleOffset, final int emailOffset) {
			super(parameters, name);
			this.replacementString= replacementString;
			this.roleIndex= roleOffset;
			this.emailIndex= emailOffset;
		}
		
		
		@Override
		protected void doApply(final char trigger, final int stateMask, final int caretOffset,
				final int replacementOffset, final int replacementLength)
				throws BadLocationException {
			final AssistInvocationContext context= getInvocationContext();
			final IDocument document= context.getDocument();
			final ApplyData applyData= getApplyData();
			
			final StringBuilder replacement= new StringBuilder(this.replacementString);
			int cursor= replacement.length();
			if (this.roleIndex >= 0) {
				cursor= this.roleIndex;
			}
			else if (this.emailIndex >= 0) {
				cursor= this.emailIndex;
			}
			
			document.replace(replacementOffset, replacementLength, replacement.toString());
			
			applyData.setSelection(replacementOffset + cursor);
			if (this.roleIndex >= 0 || this.emailIndex >= 0) {
				createLinkedMode(replacementOffset, replacement.length()).enter();
			}
		}
		
		private LinkedModeUI createLinkedMode(final int offset, final int mode)
				throws BadLocationException {
			final AssistInvocationContext context= getInvocationContext();
			final IDocument document= context.getDocument();
			
			final LinkedModeModel model= new LinkedModeModel();
			
			if (this.roleIndex >= 0) {
				final LinkedPositionGroup group= new LinkedPositionGroup();
				group.addPosition(new LinkedPosition(document, offset + this.roleIndex, 0));
				model.addGroup(group);
			}
			if (this.emailIndex >= 0) {
				final LinkedPositionGroup group= new LinkedPositionGroup();
				group.addPosition(new LinkedPosition(document, offset + this.emailIndex, 0));
				model.addGroup(group);
			}
			
			model.forceInstall();
			
			/* create UI */
			final LinkedModeUI ui= new LinkedModeUI(model, context.getSourceViewer());
			ui.setCyclingMode(LinkedModeUI.CYCLE_NEVER);
			ui.setExitPosition(context.getSourceViewer(), offset + (mode & 0xfff), 0, Integer.MAX_VALUE);
			return ui;
		}
		
	}
	
	
}