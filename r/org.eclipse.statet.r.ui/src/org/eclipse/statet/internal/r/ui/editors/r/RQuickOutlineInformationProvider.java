/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.r;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.sourceediting.QuickInformationProvider;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;
import org.eclipse.statet.r.ui.editors.RSourceEditor;


@NonNullByDefault
public class RQuickOutlineInformationProvider extends QuickInformationProvider {
	
	
	private @Nullable RHeuristicTokenScanner scanner;
	
	
	public RQuickOutlineInformationProvider(final RSourceEditor editor, final int viewerOperation) {
		super(editor, RModel.R_TYPE_ID, viewerOperation);
	}
	
	
	@Override
	public RSourceEditor getEditor() {
		return (RSourceEditor)super.getEditor();
	}
	
	private RHeuristicTokenScanner getScanner() {
		var scanner= this.scanner;
		if (scanner == null) {
			scanner= RHeuristicTokenScanner.create(getEditor().getDocumentContentInfo());
			this.scanner= scanner;
		}
		return scanner;
	}
	
	@Override
	public IRegion getSubject(final ITextViewer textViewer, final int offset) {
		try {
			final var scanner= getScanner();
			final IDocument document= nonNullAssert(textViewer.getDocument());
			scanner.configure(document);
			final IRegion word= scanner.findRWord(offset, false, true);
			if (word != null) {
				final ITypedRegion partition= scanner.getPartition(word.getOffset());
				if (RDocumentConstants.R_DEFAULT_CONTENT_CONSTRAINT.matches(partition.getType())
						|| partition.getType() == RDocumentConstants.R_STRING_CONTENT_TYPE
						|| partition.getType() == RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE) {
					return word;
				}
			}
		}
		catch (final Exception e) {
		}
		return new Region(offset, 0);
	}
	
	@Override
	public IInformationControlCreator createInformationPresenterControlCreator() {
		return new IInformationControlCreator() {
			@Override
			public IInformationControl createInformationControl(final Shell parent) {
				return new RQuickOutlineInformationControl(parent, getCommandId());
			}
		};
	}
	
}
