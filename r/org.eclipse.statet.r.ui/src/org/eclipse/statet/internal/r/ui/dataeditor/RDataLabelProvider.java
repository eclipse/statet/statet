/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import java.util.List;

import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.internal.r.ui.dataeditor.RDataEditorOutlinePage.VariablePropertyItem;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.ui.RLabelProvider;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;
import org.eclipse.statet.r.ui.dataeditor.RDataTableContentDescription;
import org.eclipse.statet.r.ui.dataeditor.RDataTableVariable;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;


public class RDataLabelProvider extends StyledCellLabelProvider {
	
	
	private final RLabelProvider rLabelProvider= new RLabelProvider(
			RLabelProvider.NO_STORE_TYPE | RLabelProvider.COUNT | RLabelProvider.NAMESPACE);
	
	
	public RDataLabelProvider() {
	}
	
	
	public Image getImage(final RDataTableVariable element) {
		switch (element.getVarType()) {
		case RDataTableVariable.LOGI:
			return RUI.getImage(RUI.IMG_OBJ_COL_LOGI);
		case RDataTableVariable.INT:
			return RUI.getImage(RUI.IMG_OBJ_COL_INT);
		case RDataTableVariable.NUM:
			return RUI.getImage(RUI.IMG_OBJ_COL_NUM);
		case RDataTableVariable.CPLX:
			return RUI.getImage(RUI.IMG_OBJ_COL_CPLX);
		case RDataTableVariable.CHAR:
			return RUI.getImage(RUI.IMG_OBJ_COL_CHAR);
		case RDataTableVariable.RAW:
			return RUI.getImage(RUI.IMG_OBJ_COL_RAW);
		case RDataTableVariable.FACTOR:
			return RUI.getImage(RUI.IMG_OBJ_COL_FACTOR);
		case RDataTableVariable.DATE:
			return RUI.getImage(RUI.IMG_OBJ_COL_DATE);
		case RDataTableVariable.DATETIME:
			return RUI.getImage(RUI.IMG_OBJ_COL_DATETIME);
		default:
			return null;
		}
	}
	
	@Override
	public void update(final ViewerCell cell) {
		Image image;
		final StyledString text= new StyledString();
		final Object element= cell.getElement();
		if (element instanceof final RDataTableContentDescription description) {
			if (description.getRElementStruct() instanceof CombinedRElement) {
				this.rLabelProvider.update(cell, (LtkModelElement)description.getRElementStruct());
				super.update(cell);
				return;
			}
			switch (description.getRElementStruct().getRObjectType()) {
			case RObject.TYPE_VECTOR:
				image= RUI.getImage(RUI.IMG_OBJ_VECTOR);
				break;
			case RObject.TYPE_ARRAY:
				image= RUI.getImage(RUI.IMG_OBJ_VECTOR);
				break;
			case RObject.TYPE_DATAFRAME:
				image= RUI.getImage(RUI.IMG_OBJ_VECTOR);
				break;
			default:
				image= null;
				break;
			}
			text.append(description.getElementName().toString());
		}
		else if (element instanceof final RDataTableVariable variable) {
			image= getImage(variable);
			text.append(variable.getName());
			
			if (element instanceof RDataTableColumn) {
				final RDataTableColumn column= (RDataTableColumn)variable;
				text.append(" : ", StyledString.DECORATIONS_STYLER); //$NON-NLS-1$
				final List<String> classNames= column.getClassNames();
				text.append(classNames.get(0), StyledString.DECORATIONS_STYLER);
				for (int i= 1; i < classNames.size(); i++) {
					text.append(", ", StyledString.DECORATIONS_STYLER); //$NON-NLS-1$
					text.append(classNames.get(i), StyledString.DECORATIONS_STYLER);
				}
				if (!classNames.contains(RDataUtils.getStoreClass(column.getDataStore()))) {
					text.append(" (", StyledString.DECORATIONS_STYLER); //$NON-NLS-1$
					text.append(RDataUtils.getStoreAbbr(column.getDataStore()), StyledString.DECORATIONS_STYLER);
					text.append(")", StyledString.DECORATIONS_STYLER); //$NON-NLS-1$
				}
			}
		}
		else if (element instanceof final VariablePropertyItem item) {
			image= null;
			text.append(item.getName());
			final int count= item.getCount();
			if (count >= 0) {
				text.append(" (", StyledString.COUNTER_STYLER); //$NON-NLS-1$
				text.append(Integer.toString(count), StyledString.COUNTER_STYLER);
				text.append(")", StyledString.COUNTER_STYLER); //$NON-NLS-1$
			}
		}
		else {
			image= null;
			text.append(element.toString());
		}
		
		cell.setText(text.getString());
		cell.setStyleRanges(text.getStyleRanges());
		cell.setImage(image);
		
		super.update(cell);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		this.rLabelProvider.dispose();
	}
	
}
