/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.preferences;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.persistence.TemplateStore;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.base.ext.templates.ICodeGenerationTemplateCategory;
import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.r.ui.sourceediting.RTemplateSourceViewerConfigurator;


/**
 * Integrates the R templates into the common StatET template
 * preference page. 
 */
public class RCodeTemplatesProvider implements ICodeGenerationTemplateCategory {
	
	
	public RCodeTemplatesProvider() {
	}
	
	@Override
	public String getProjectNatureId() {
		return RProjects.R_NATURE_ID;
	}
	
	@Override
	public TemplateStore getTemplateStore() {
		return RUIPlugin.getInstance().getRCodeGenerationTemplateStore();
	}
	
	@Override
	public ContextTypeRegistry getContextTypeRegistry() {
		return RUIPlugin.getInstance().getRCodeGenerationTemplateContextRegistry();
	}
	
	@Override
	public SourceEditorViewerConfigurator getEditTemplateDialogConfiguator(
			final TemplateVariableProcessor processor, final IProject project) {
		return new RTemplateSourceViewerConfigurator(
				RCore.getContextAccess(project),
				processor );
	}
	
}
