/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;

import org.eclipse.statet.ltk.ui.sourceediting.FragmentDocumentProvider;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.source.doc.RDocumentSetupParticipant;


public class RFragmentDocumentProvider extends FragmentDocumentProvider {
	
	
	private final Map<ToolProcess, ProcessListener> fProcesses= new HashMap<>();
	
	
	private class ProcessListener implements IDebugEventSetListener {
		
		private final ToolProcess fProcess;
		
		private final List<Object> fElements= new ArrayList<>(4);
		
		public ProcessListener(final ToolProcess process) {
			fProcess = process;
			
			init();
		}
		
		private void init() {
			DebugPlugin.getDefault().addDebugEventListener(this);
			fProcesses.put(fProcess, this);
		}
		
		private void dispose() {
			DebugPlugin.getDefault().removeDebugEventListener(this);
			fProcesses.remove(fProcess);
		}
		
		@Override
		public void handleDebugEvents(final DebugEvent[] events) {
			for (int i = 0; i < events.length; i++) {
				if (events[i].getSource() == fProcess) {
					if (events[i].getKind() == DebugEvent.TERMINATE) {
						terminated();
					}
				}
			}
		}
		
		protected void terminated() {
			Object[] elements;
			synchronized (fProcesses) {
				dispose();
				elements = fElements.toArray();
				fElements.clear();
			}
			
			for (final Object element : elements) {
				fireElementDeleted(element);
			}
		}
		
		public void add(final Object element) {
			fElements.add(element);
		}
		
		public void remove(final Object element) {
			if (fElements.remove(element) && fElements.isEmpty()) {
				dispose();
			}
		}
		
	}
	
	
	public RFragmentDocumentProvider() {
		super(RModel.R_TYPE_ID, new RDocumentSetupParticipant());
	}
	
	
	@Override
	protected ElementInfo createElementInfo(final Object element) throws CoreException {
		final ElementInfo info = super.createElementInfo(element);
		
		if (info != null && element instanceof IAdaptable) {
			final ToolProcess process = ((IAdaptable) element).getAdapter(ToolProcess.class);
			if (process != null) {
				synchronized (fProcesses) {
					ProcessListener listener = fProcesses.get(process);
					if (listener == null) {
						listener = new ProcessListener(process);
					}
					listener.add(element);
				}
			}
		}
		
		return info;
	}
	
	@Override
	protected void disposeElementInfo(final Object element, final ElementInfo info) {
		super.disposeElementInfo(element, info);
		
		if (element instanceof IAdaptable) {
			final ToolProcess process = ((IAdaptable) element).getAdapter(ToolProcess.class);
			if (process != null) {
				synchronized (fProcesses) {
					final ProcessListener listener = fProcesses.get(process);
					if (listener != null) {
						listener.remove(element);
					}
				}
			}
		}
	}
	
}
