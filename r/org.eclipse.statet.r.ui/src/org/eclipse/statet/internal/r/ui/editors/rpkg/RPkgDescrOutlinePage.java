/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rpkg;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor2OutlinePage;
import org.eclipse.statet.ltk.ui.util.ViewerDragSupport;
import org.eclipse.statet.ltk.ui.util.ViewerDropSupport;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.refactoring.RRefactoring;


@NonNullByDefault
public class RPkgDescrOutlinePage extends SourceEditor2OutlinePage {
	
	private class ContentFilter implements LtkModelElementFilter {
		
		public ContentFilter() {
		}
		
		@Override
		public boolean include(final LtkModelElement element) {
			return switch (element.getElementType()) {
			default -> true;
			};
		}
		
	}
	
	
	private final ContentFilter filter= new ContentFilter();
	
	
	public RPkgDescrOutlinePage(final SourceEditor1 editor) {
		super(editor, RModel.RPKG_DESCRIPTION_TYPE_ID,
				RRefactoring.getRPkgDescrFactory(),
				"org.eclipse.statet.r.menus.RPkgDescrOutlineViewContextMenu"); //$NON-NLS-1$
	}
	
	
	@Override
	protected IDialogSettings getDialogSettings() {
		return DialogUtils.getDialogSettings(RUIPlugin.getInstance(), "RPkgDescrOutlineView"); //$NON-NLS-1$
	}
	
	@Override
	protected LtkModelElementFilter getContentFilter() {
		return this.filter;
	}
	
	@Override
	protected void configureViewer(final TreeViewer viewer) {
		super.configureViewer(viewer);
		
		final ViewerDropSupport drop= new ViewerDropSupport(viewer, this,
				getRefactoringFactory() );
		drop.init();
		final ViewerDragSupport drag= new ViewerDragSupport(viewer);
		drag.init();
	}
	
	@Override
	protected void contributeToActionBars(final IServiceLocator serviceLocator,
			final IActionBars actionBars, final HandlerCollection handlers) {
		super.contributeToActionBars(serviceLocator, actionBars, handlers);
		
//		final IToolBarManager toolBarManager= actionBars.getToolBarManager();
//		
//		toolBarManager.appendToGroup(ECommonsUI.VIEW_SORT_MENU_ID,
//				new AlphaSortAction());
//		toolBarManager.appendToGroup(ECommonsUI.VIEW_FILTER_MENU_ID,
//				new FilterCommonVariables());
//		toolBarManager.appendToGroup(ECommonsUI.VIEW_FILTER_MENU_ID,
//				new FilterLocalDefinitions());
	}
	
}
