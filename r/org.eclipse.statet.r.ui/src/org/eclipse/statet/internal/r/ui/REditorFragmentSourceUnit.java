/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.model.RFragmentSourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnit;


@NonNullByDefault
public class REditorFragmentSourceUnit
		extends RFragmentSourceUnit
		implements RSourceUnit, RCoreAccess {
	
	
	public REditorFragmentSourceUnit(final String id, final SourceFragment fragment) {
		super(id, fragment);
	}
	
	
	@Override
	public WorkingContext getWorkingContext() {
		return Ltk.EDITOR_CONTEXT;
	}
	
}
