/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore;


@NonNullByDefault
abstract class Lock extends ReentrantLock {
	
	
	static final int ERROR_STATE= 4;
	static final int RELOAD_STATE= 3;
	static final int PAUSE_STATE= 2;
	static final int LOCAL_PAUSE_STATE= 1;
	
	
	int state;
	
	protected final Condition requestor= newCondition();
	protected final Condition worker= newCondition();
	
	boolean scheduled;
	
	
	Lock() {
	}
	
	
	final boolean isReady() throws LoadDataException {
		if (this.state > 0) {
			switch (this.state) {
			case Lock.LOCAL_PAUSE_STATE:
			case Lock.PAUSE_STATE:
				return false;
			case Lock.RELOAD_STATE:
				throw new LoadDataException(true);
			default:
				throw new LoadDataException(false);
			}
		}
		return true;
	}
	
	/* Not nice at this place, but handy */
	final <T> LazyRStore. @Nullable Fragment<T> getFragment(final LazyRStore<T> store,
			final long rowIdx, final long columnIdx,
			final int flags, final ProgressMonitor m) throws LoadDataException {
		lock();
		try {
			return (isReady()) ?
					store.getFragment(rowIdx, columnIdx, flags, m) :
					null;
		}
		finally {
			unlock();
		}
	}
	
	
	void notifyWorker() {
		this.worker.signalAll();
	}
	
	void clear() {
		this.requestor.signalAll();
		this.worker.signalAll();
	}
	
}
