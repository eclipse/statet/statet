/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.IntPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.StringPref;

import org.eclipse.statet.r.ui.RUI;


@NonNullByDefault
public class RHelpPreferences {
	
	
	public static final String RHELP_QUALIFIER= RUI.BUNDLE_ID + "/rhelpview"; //$NON-NLS-1$
	
	
	public static final String HOMEPAGE_URL_KEY= "homepage.url"; //$NON-NLS-1$
	
	public static final String SHOW_INTERNAL_ENABLED_KEY= "Show.Internal.enabled"; //$NON-NLS-1$
	
	public static final String SEARCH_REUSE_PAGE_ENABLED_KEY= "search.reuse_page.enabled"; //$NON-NLS-1$
	public static final String SEARCH_PREVIEW_FRAGMENTS_MAX_KEY= "search.preview_fragments.max"; //$NON-NLS-1$
	
	public static final Preference<String> HOMEPAGE_URL_PREF= new StringPref(
			RHELP_QUALIFIER, HOMEPAGE_URL_KEY);
	
	public static final Preference<Boolean> SHOW_INTERNAL_ENABLED_PREF= new BooleanPref(
			RHELP_QUALIFIER, SHOW_INTERNAL_ENABLED_KEY);
	
	public static final Preference<Boolean> SEARCH_REUSE_PAGE_ENABLED_PREF= new BooleanPref(
			RHELP_QUALIFIER, SEARCH_REUSE_PAGE_ENABLED_KEY);
	
	public static final Preference<Integer> SEARCH_PREVIEW_FRAGMENTS_MAX_PREF= new IntPref(
			RHELP_QUALIFIER, SEARCH_PREVIEW_FRAGMENTS_MAX_KEY);
	
}
