/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.search;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;

import org.eclipse.statet.ecommons.ui.workbench.DecoratingStyledLabelProvider;
import org.eclipse.statet.ecommons.workbench.search.ui.ExtTextSearchResultPage;
import org.eclipse.statet.ecommons.workbench.search.ui.LineElement;
import org.eclipse.statet.ecommons.workbench.search.ui.TextSearchResultContentProvider;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.ui.RLabelProvider;


public class RElementSearchResultPage extends ExtTextSearchResultPage<RSourceUnit, RElementMatch> {
	
	
	private static class LTKDecoratingLabelProvider extends DecoratingStyledLabelProvider {
		
		public LTKDecoratingLabelProvider(final IStyledLabelProvider provider) {
			super(provider);
		}
		
		
		@Override
		protected Object getElementToDecorate(final Object element) {
			if (element instanceof SourceUnit) {
				return ((SourceUnit)element).getResource();
			}
			return element;
		}
		
	}
	
	
	public RElementSearchResultPage() {
		super(RElementSearchResult.COMPARATOR);
	}
	
	
	@Override
	protected TextSearchResultContentProvider<RSourceUnit, RElementMatch, TreeViewer> createTreeContentProvider(final TreeViewer viewer) {
		return new RElementSearchResultTreeContentProvider(this, viewer);
	}
	
	@Override
	protected void configureTableViewer(final TableViewer viewer) {
		super.configureTableViewer(viewer);
		
		viewer.setLabelProvider(new LTKDecoratingLabelProvider(
				new RElementSearchLabelProvider(this, RLabelProvider.RESOURCE_PATH) ));
	}
	
	@Override
	protected void configureTreeViewer(final TreeViewer viewer) {
		super.configureTreeViewer(viewer);
		
		viewer.setLabelProvider(new LTKDecoratingLabelProvider(
				new RElementSearchLabelProvider(this, RLabelProvider.RESOURCE_PATH) ));
	}
	
	
	@Override
	public RElementMatch[] getDisplayedMatches(final Object element) {
		if (element instanceof LineElement<?>) {
			final List<RElementMatch> matches= getDisplayedMatches((LineElement<?>) element);
			return matches.toArray(new RElementMatch[matches.size()]);
		}
		return super.getDisplayedMatches(element);
	}
	
	public List<RElementMatch> getDisplayedMatches(final LineElement<?> group) {
		final RElementMatch[] allMatches= getInput().getPickedMatches(group.getElement());
		final List<RElementMatch> groupMatches= new ArrayList<>();
		for (final RElementMatch match : allMatches) {
			if (match.getMatchGroup() == group) {
				groupMatches.add(match);
			}
		}
		return groupMatches;
	}
	
	@Override
	public int getDisplayedMatchCount(final Object element) {
		if (element instanceof LineElement<?>) {
			return getDisplayedMatchCount((LineElement<?>) element);
		}
		return super.getDisplayedMatchCount(element);
	}
	
	public int getDisplayedMatchCount(final LineElement<?> group) {
		final RElementMatch[] allMatches= getInput().getPickedMatches(group.getElement());
		int count= 0;
		for (final RElementMatch match : allMatches) {
			if (match.getMatchGroup() == group) {
				count++;
			}
		}
		return count++;
	}
	
}
