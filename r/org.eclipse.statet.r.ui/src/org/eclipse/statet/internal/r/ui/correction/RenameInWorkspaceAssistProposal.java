/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.correction;

import org.eclipse.jface.text.ITextViewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.internal.r.ui.refactoring.RenameInWorkspaceWizard;
import org.eclipse.statet.ltk.ui.refactoring.RefactoringSaveHelper;
import org.eclipse.statet.ltk.ui.refactoring.RefactoringWizardExecutionHelper;
import org.eclipse.statet.ltk.ui.sourceediting.assist.BasicAssistProposal;
import org.eclipse.statet.r.core.model.RWorkspaceSourceUnit;
import org.eclipse.statet.r.core.refactoring.RenameInWorkspaceRefactoring;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class RenameInWorkspaceAssistProposal
		extends BasicAssistProposal<RAssistInvocationContext> {
	
	
	private final RAstNode nameNode;
	
	
	public RenameInWorkspaceAssistProposal(final RAssistInvocationContext invocationContext,
			final RAstNode nameNode) {
		super(invocationContext, "org.eclipse.statet.ltk.commands.RefactorRenameInWorkspace", //$NON-NLS-1$
				RUIMessages.Proposal_RenameInWorkspace_label,
				RUIMessages.Proposal_RenameInWorkspace_description );
		
		this.nameNode= nameNode;
	}
	
	
	@Override
	public void apply(final ITextViewer viewer, final char trigger, final int stateMask, final int offset) {
		final var context= getInvocationContext();
		
		RenameInWorkspaceRefactoring refactoring;
		{	refactoring= new RenameInWorkspaceRefactoring((RWorkspaceSourceUnit)context.getSourceUnit(),
					this.nameNode );
		}
		if (refactoring != null) {
			final RefactoringWizardExecutionHelper executionHelper= new RefactoringWizardExecutionHelper(
					new RenameInWorkspaceWizard(refactoring), RefactoringSaveHelper.SAVE_REFACTORING | RefactoringSaveHelper.EXCLUDE_ACTIVE_EDITOR, true);
			executionHelper.perform(viewer.getTextWidget().getShell());
		}
	}
	
}
