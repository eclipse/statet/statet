/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.model.ui.impl.GenericEditorWorkspaceSourceUnitWorkingCopy2;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RWorkspaceSourceUnit;
import org.eclipse.statet.r.core.model.build.RPkgDescrSourceUnitModelContainer;


@NonNullByDefault
public class RPkgDescrEditorWorkingCopy
		extends GenericEditorWorkspaceSourceUnitWorkingCopy2<RPkgDescrSourceUnitModelContainer>
		implements RWorkspaceSourceUnit {
	
	
	public RPkgDescrEditorWorkingCopy(final RWorkspaceSourceUnit from) {
		super(from);
	}
	
	@Override
	protected RPkgDescrSourceUnitModelContainer createModelContainer() {
		return new RPkgDescrSourceUnitModelContainer(this,
				RUIPlugin.getInstance().getRPkgDescrDocumentProvider() );
	}
	
	
	@Override
	public @Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int flags,
			final IProgressMonitor monitor) {
		if (type == RModel.R_TYPE_ID) {
			return RModel.getRModelInfo(getModelContainer().getModelInfo(flags, monitor));
		}
		return super.getModelInfo(type, flags, monitor);
	}
	
}
