/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import static org.eclipse.statet.r.ui.rhelp.RHelpUI.RHELP_TOPIC_INTERNAL_OBJ_IMAGE_ID;
import static org.eclipse.statet.r.ui.rhelp.RHelpUI.RHELP_TOPIC_OBJ_IMAGE_ID;

import org.eclipse.jface.text.contentassist.ICompletionProposalExtension6;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.RSymbolComparator;
import org.eclipse.statet.r.ui.rhelp.RHelpUI;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;
import org.eclipse.statet.rhelp.core.RHelpPage;


@NonNullByDefault
public class RHelpTopicCompletionProposal extends RSimpleCompletionProposal
		implements ICompletionProposalExtension6 {
	
	
	private ImList<String> pkgNames;
	private boolean isInternalOnly;
	
	
	public RHelpTopicCompletionProposal(final ProposalParameters<RAssistInvocationContext> parameters,
			final String replacementString, final @Nullable RHelpPage page) {
		super(parameters, replacementString);
		
		if (page != null) {
			this.pkgNames= ImCollections.newList(page.getPackage().getName());
			this.isInternalOnly= page.isInternal();
		}
		else {
			this.pkgNames= ImCollections.emptyList();
			this.isInternalOnly= false;
		}
	}
	
	
	public void addPage(final RHelpPage page) {
		this.pkgNames= ImCollections.addElementIfAbsent(this.pkgNames, page.getPackage().getName(),
				RSymbolComparator.R_NAMES_COLLATOR );
		if (!page.isInternal()) {
			this.isInternalOnly= false;
		}
	}
	
	
	@Override
	public StyledString computeStyledText() {
		final StyledString styledText= new StyledString(getName());
		final ImList<String> pkgNames= this.pkgNames;
		if (!pkgNames.isEmpty()) {
			styledText.append(QUALIFIER_SEPARATOR, StyledString.QUALIFIER_STYLER);
			styledText.append(pkgNames.get(0), StyledString.QUALIFIER_STYLER);
			for (int i= 1; i < pkgNames.size(); i++) {
				styledText.append(", ", StyledString.QUALIFIER_STYLER); //$NON-NLS-1$
				styledText.append(pkgNames.get(i), StyledString.QUALIFIER_STYLER);
			}
		}
		return styledText;
	}
	
	@Override
	public Image getImage() {
		return RHelpUI.getUIResources().getImage((this.isInternalOnly) ?
				RHELP_TOPIC_INTERNAL_OBJ_IMAGE_ID :
				RHELP_TOPIC_OBJ_IMAGE_ID );
	}
	
}
