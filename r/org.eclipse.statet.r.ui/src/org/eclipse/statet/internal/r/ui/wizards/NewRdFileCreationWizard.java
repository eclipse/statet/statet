/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.text.TextUtil;
import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.templates.TemplateUtils.EvaluatedTemplate;
import org.eclipse.statet.ltk.ui.wizards.NewElementWizard;
import org.eclipse.statet.ltk.ui.wizards.NewElementWizardPage.ResourceGroup;
import org.eclipse.statet.r.codegeneration.CodeGeneration;
import org.eclipse.statet.r.core.BasicRResourceSourceUnit;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.ui.RUI;


public class NewRdFileCreationWizard extends NewElementWizard {
	
	
	private static class NewRdFile extends NewFile {
		
		public NewRdFile(final IPath containerPath, final String resourceName) {
			super(containerPath, resourceName, RCore.RD_CONTENT_TYPE);
		}
		
		@Override
		protected String getInitialFileContent(final IFile newFileHandle,
				final SubMonitor progress) {
			final String lineDelimiter= TextUtil.getLineDelimiter(newFileHandle.getProject());
			try {
				final BasicRResourceSourceUnit rcu= BasicRResourceSourceUnit.createTempUnit(newFileHandle, "rd"); //$NON-NLS-1$
				final EvaluatedTemplate data= CodeGeneration.getNewRdFileContent(rcu, lineDelimiter);
				if (data != null) {
					this.initialSelection= data.getRegionToSelect();
					return data.getContent();
				}
			}
			catch (final CoreException e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, RUI.BUNDLE_ID, 0,
						"An error occured when applying template to new Rd file.",
						e ));
			}
			return null;
		}
	}
	
	private NewRdFileCreationWizardPage firstPage;
	
	private NewFile newRdFile;
	
	
	public NewRdFileCreationWizard() {
		setDialogSettings(DialogUtils.getDialogSettings(RUIPlugin.getInstance(), "NewElementWizard")); //$NON-NLS-1$
		setDefaultPageImageDescriptor(RUI.getImageDescriptor(RUIPlugin.IMG_WIZBAN_NEWRDFILE));
		setWindowTitle(Messages.NewRDocFileWizard_title);
	}
	
	@Override
	public void addPages() {
		super.addPages();
		this.firstPage= new NewRdFileCreationWizardPage(getSelection());
		addPage(this.firstPage);
	}
	
	@Override
	protected ISchedulingRule getSchedulingRule() {
		final ISchedulingRule rule= createRule(this.newRdFile.getResource());
		if (rule != null) {
			return rule;
		}
		
		return super.getSchedulingRule();
	}
	
	@Override
	public boolean performFinish() {
		// befor super, so it can be used in getSchedulingRule
		final ResourceGroup resourceGroup= this.firstPage.getResourceGroup();
		this.newRdFile= new NewRdFile(
				resourceGroup.getContainerFullPath(),
				resourceGroup.getResourceName() );
		
		final boolean result= super.performFinish();
		
		if (result && this.newRdFile.getResource() != null) {
			// select and open file
			selectAndReveal(this.newRdFile.getResource());
			openResource(this.newRdFile);
		}
		
		return result;
	}
	
	@Override
	protected void performOperations(final IProgressMonitor monitor) throws InterruptedException, CoreException, InvocationTargetException {
		final SubMonitor m= SubMonitor.convert(monitor, "Create new Rd file...", 10 + 1);
		
		this.newRdFile.createFile(m.newChild(10));
		
		this.firstPage.saveSettings();
		m.worked(1);
	}
	
}
