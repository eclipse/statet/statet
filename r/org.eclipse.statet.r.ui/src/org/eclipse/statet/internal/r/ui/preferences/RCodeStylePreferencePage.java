/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.preferences;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.PropertyAndPreferencePage;

import org.eclipse.statet.r.core.project.RProjects;


/**
 * A Property- and PreferencePage for RCodeStyle settings.
 */
public class RCodeStylePreferencePage extends PropertyAndPreferencePage {
	
	public static final String PREF_ID= "org.eclipse.statet.r.preferencePages.RCodeStyle"; //$NON-NLS-1$
	public static final String PROP_ID= "org.eclipse.statet.r.propertyPages.RCodeStyle"; //$NON-NLS-1$
	
	
	public RCodeStylePreferencePage() {
	}
	
	
	@Override
	protected String getPreferencePageID() {
		return PREF_ID;
	}
	
	@Override
	protected String getPropertyPageID() {
		return PROP_ID;
	}
	
	@Override
	protected boolean isProjectSupported(final IProject project) throws CoreException {
		return project.hasNature(RProjects.R_NATURE_ID);
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() throws CoreException {
		return new RCodeStylePreferenceBlock(getProject(), createStatusChangedListener());
	}
	
	@Override
	protected boolean hasProjectSpecificSettings(final IProject project) {
		return ((ManagedConfigurationBlock) getBlock()).hasProjectSpecificOptions(project);
	}
	
}
