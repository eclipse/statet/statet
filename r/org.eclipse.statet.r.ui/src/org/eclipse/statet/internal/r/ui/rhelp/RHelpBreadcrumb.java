/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import java.net.URI;
import java.util.Arrays;
import java.util.Locale;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNull;

import org.eclipse.statet.ecommons.ui.actions.SimpleContributionItem;
import org.eclipse.statet.ecommons.ui.viewers.breadcrumb.AbstractBreadcrumb;
import org.eclipse.statet.ecommons.ui.viewers.breadcrumb.BreadcrumbViewer;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.rhelp.core.REnvHelp;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rhelp.core.RHelpPage;
import org.eclipse.statet.rhelp.core.RPkgHelp;
import org.eclipse.statet.rhelp.core.http.RHelpHttpService;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvUtils;


public class RHelpBreadcrumb extends AbstractBreadcrumb {
	
	
	private class RHelpBreadcrumbViewer extends BreadcrumbViewer {
		
		
		public RHelpBreadcrumbViewer(final Composite parent) {
			super(parent, SWT.HORIZONTAL);
		}
		
		
		@Override
		protected void configureDropDownViewer(final TreeViewer viewer, final Object input) {
			viewer.setContentProvider(new RHelpContentProvider());
			viewer.setLabelProvider(new RHelpLabelProvider());
		}
		
		@Override
		public void fillDropDownContextMenu(final IMenuManager manager, final Object selection) {
			if (selection instanceof final REnv rEnv) {
				manager.add(new SimpleContributionItem(
						NLS.bind("Show overview of ''{0}''", rEnv.getName()), "o") {
					@Override
					protected void execute() throws ExecutionException {
						RHelpBreadcrumb.this.reveal(selection);
						setFocusToInput();
					}
				});
				manager.add(new SimpleContributionItem(
						NLS.bind("Show current page in ''{0}''", rEnv.getName()), "p") {
					@Override
					protected void execute() throws ExecutionException {
						switchTo((REnv) selection);
					}
				});
			}
		}
		
	}
	
	private class RHelpContentProvider implements ITreeContentProvider {
		
		
		@Override
		public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
		}
		
		@Override
		public void dispose() {
		}
		
		@Override
		public Object[] getElements(final Object inputElement) {
			return getChildren(inputElement);
		}
		
		@Override
		public Object getParent(final Object element) {
			if (element instanceof REnv || element instanceof String) {
				return RHelpBreadcrumb.this.rHelpManager;
			}
			if (element instanceof RPkgHelp) {
				return ((RPkgHelp) element).getREnv();
			}
			if (element instanceof RHelpPage) {
				return ((RHelpPage) element).getPackage();
			}
			if (element instanceof Object[]) {
				return ((Object[]) element)[0];
			}
			return null;
		}
		
		@Override
		public boolean hasChildren(final Object element) {
			if (element instanceof RHelpManager) {
				return true;
			}
			if (element instanceof REnv) {
				return RHelpBreadcrumb.this.rHelpManager.hasHelp((REnv) element);
			}
			if (element instanceof RPkgHelp) {
				return !((RPkgHelp) element).getPages().isEmpty();
			}
			return false;
		}
		
		@Override
		public Object[] getChildren(final Object element) {
			if (element instanceof RHelpManager) {
				final var rEnvsWithHelp= RHelpBreadcrumb.this.rHelpManager.getREnvsWithHelp();
				final @NonNull REnv[] children= rEnvsWithHelp.toArray(new @NonNull REnv[rEnvsWithHelp.size()]);
				Arrays.sort(children, REnvUtils.getComparator(Locale.getDefault()));
				return children;
			}
			if (element instanceof REnv) {
				final REnvHelp help= RHelpBreadcrumb.this.rHelpManager.getHelp((REnv) element);
				if (help != null) {
					try {
						return help.getPkgs().toArray();
					}
					finally {
						help.unlock();
					}
				}
				return new Object[0];
			}
			if (element instanceof RPkgHelp) {
				return ((RPkgHelp) element).getPages().toArray();
			}
			return new Object[0];
		}
		
	}
	
	
	private final RHelpHttpService rHelpHttpService= RCore.getRHelpHttpService();
	private final RHelpManager rHelpManager= RCore.getRHelpManager();
	
	private final RHelpViewPage page;
	
	private RHelpLabelProvider labelProvider;
	
	
	public RHelpBreadcrumb(final RHelpViewPage page) {
		this.page= page;
	}
	
	
	@Override
	public void setInput(Object element) {
		if (element == null) {
			element= this.rHelpHttpService;
		}
		if (this.labelProvider != null) {
//			fLabelProvider.setFocusObject(element);
		}
		super.setInput(element);
	}
	
	@Override
	protected BreadcrumbViewer createViewer(final Composite parent) {
		final RHelpBreadcrumbViewer viewer= new RHelpBreadcrumbViewer(parent);
		
		this.labelProvider= new RHelpLabelProvider(0);
		viewer.setLabelProvider(this.labelProvider);
		viewer.setToolTipLabelProvider(new RHelpLabelProvider(RHelpLabelProvider.TOOLTIP));
		
		viewer.setContentProvider(new RHelpContentProvider());
		
		return viewer;
	}
	
	@Override
	protected boolean hasInputFocus() {
		return this.page.isBrowserFocusControl();
	}
	
	@Override
	protected void setFocusToInput() {
		this.page.setFocusToBrowser();
	}
	
	@Override
	protected IServiceLocator getParentServiceLocator() {
		return this.page.getSite();
	}
	
	@Override
	protected boolean reveal(final Object element) {
		final URI url= this.rHelpHttpService.toHttpUrl(element,
				RHelpHttpService.BROWSE_TARGET );
		if (url != null) {
			this.page.setUrl(url);
			return true;
		}
		return false;
	}
	
	protected void switchTo(final REnv rEnv) {
		final URI url= this.rHelpHttpService.toHttpUrl(this.page.getCurrentUrl(),
				rEnv, RHelpHttpService.BROWSE_TARGET );
		if (url != null) {
			setFocusToInput();
			this.page.setUrl(url);
		}
	}
	
	@Override
	protected boolean open(final Object element) {
		return false;
	}
	
}
