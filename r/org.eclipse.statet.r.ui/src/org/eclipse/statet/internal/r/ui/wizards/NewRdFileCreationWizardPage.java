/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ltk.ui.wizards.NewElementWizardPage;
import org.eclipse.statet.r.core.project.RProjects;


/**
 * The "New" wizard page allows setting the container for
 * the new file as well as the file name. The page
 * will only accept file name without the extension or
 * with the extension that matches the expected one (r).
 */
public class NewRdFileCreationWizardPage extends NewElementWizardPage {
	
	
	private static final String fgDefaultExtension= ".Rd"; //$NON-NLS-1$
	
	
	private final ResourceGroup resourceGroup;
	
	
	/**
	 * Constructor.
	 */
	public NewRdFileCreationWizardPage(final IStructuredSelection selection) {
		super("NewRdFileCreationWizardPage", selection); //$NON-NLS-1$
		
		setTitle(Messages.NewRDocFileWizardPage_title);
		setDescription(Messages.NewRDocFileWizardPage_description);
		
		this.resourceGroup= new ResourceGroup(fgDefaultExtension,
				new ProjectNatureContainerFilter(RProjects.R_NATURE_ID) );
	}
	
	
	@Override
	protected void createContents(final Composite parent) {
		this.resourceGroup.createGroup(parent);
	}
	
	ResourceGroup getResourceGroup() {
		return this.resourceGroup;
	}
	
	@Override
	public void setVisible(final boolean visible) {
		super.setVisible(visible);
		if (visible) {
			this.resourceGroup.setFocus();
		}
	}
	
	public void saveSettings() {
		this.resourceGroup.saveSettings();
	}
	
	@Override
	protected void validatePage() {
		updateStatus(this.resourceGroup.validate());
	}
	
}
