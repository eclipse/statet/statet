/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.correction;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.actions.UIActions;

import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.ltk.ui.LtkUI;
import org.eclipse.statet.ltk.ui.LtkUIResources;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickRefactoringAssistProposal;
import org.eclipse.statet.r.core.refactoring.IfElseInvertRefactoring;
import org.eclipse.statet.r.core.refactoring.IfNotNullElseToCRefactoring;
import org.eclipse.statet.r.core.refactoring.IfNotNullElseToSpecialRefactoring;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class ChangeIfElseAssistProposal
		extends QuickRefactoringAssistProposal<RAssistInvocationContext> {
	
	
	public ChangeIfElseAssistProposal(final RAssistInvocationContext invocationContext,
			final IfElseInvertRefactoring refactoring) {
		super(invocationContext, UIActions.NO_COMMAND_ID,
				RUIMessages.Proposal_InvertIfElse_label,
				refactoring );
	}
	
	public ChangeIfElseAssistProposal(final RAssistInvocationContext invocationContext,
			final IfNotNullElseToSpecialRefactoring refactoring) {
		super(invocationContext, UIActions.NO_COMMAND_ID,
				RUIMessages.Proposal_ConvertIfNotNullElseToSpecial_label,
				refactoring );
	}
	
	public ChangeIfElseAssistProposal(final RAssistInvocationContext invocationContext,
			final IfNotNullElseToCRefactoring refactoring) {
		super(invocationContext, UIActions.NO_COMMAND_ID,
				(refactoring.getWithBlocks()) ?
						RUIMessages.Proposal_ConvertIfNotNullElseToCIfElse_WithBlocks_label :
						RUIMessages.Proposal_ConvertIfNotNullElseToCIfElse_label,
				refactoring );
	}
	
	
	@Override
	public Image getImage() {
		return LtkUI.getUIResources().getImage(LtkUIResources.OBJ_ASSIST_CHANGE_IF_IMAGE_ID);
	}
	
	
}
