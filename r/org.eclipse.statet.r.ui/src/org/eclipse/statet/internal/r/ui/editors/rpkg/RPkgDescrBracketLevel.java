/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rpkg;

import java.util.List;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.link.LinkedModeModel;
import org.eclipse.jface.text.link.LinkedPosition;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.assist.LinkedModeBracketLevel;


@NonNullByDefault
public class RPkgDescrBracketLevel extends LinkedModeBracketLevel {
	
	
	public static final class RoundBracketPosition extends InBracketPosition {
		
		public RoundBracketPosition(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '(';
		}
		
		@Override
		public char getCloseChar() {
			return ')';
		}
		
	}
	
	public static final class SquareBracketPosition extends InBracketPosition {
		
		public SquareBracketPosition(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '[';
		}
		
		@Override
		public char getCloseChar() {
			return ']';
		}
		
	}
	
	public static final class CurlyBracketPosition extends InBracketPosition {
		
		public CurlyBracketPosition(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '{';
		}
		
		@Override
		public char getCloseChar() {
			return '}';
		}
		
	}
	
	
	public static InBracketPosition createPosition(final char c, final IDocument document,
			final int offset, final int length, final int sequence) {
		return switch (c) {
		case '(' ->
				new RoundBracketPosition(document, offset, length, sequence);
		case '{' ->
				new CurlyBracketPosition(document, offset, length, sequence);
		case '[' ->
				new SquareBracketPosition(document, offset, length, sequence);
		default ->
				throw new IllegalArgumentException("Invalid position type: " + c); //$NON-NLS-1$
		};
	}
	
	
	public RPkgDescrBracketLevel(final LinkedModeModel model,
			final IDocument document, final DocContentSections docContentSections,
			final List<? extends LinkedPosition> positions, final int mode) {
		super(model, document, docContentSections, positions, mode);
	}
	
}
