/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable;

import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.HORIZONTAL;
import static org.eclipse.statet.ecommons.waltable.core.coordinate.Orientation.VERTICAL;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.ecommons.ui.SharedMessages;
import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.actions.UIActions;
import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.config.AbstractUiBindingConfiguration;
import org.eclipse.statet.ecommons.waltable.core.coordinate.Direction;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.resize.ColumnResizeEventMatcher;
import org.eclipse.statet.ecommons.waltable.resize.PositionResizeDragMode;
import org.eclipse.statet.ecommons.waltable.resize.core.InitializeAutoResizeCommand;
import org.eclipse.statet.ecommons.waltable.resize.ui.action.AutoResizeColumnAction;
import org.eclipse.statet.ecommons.waltable.resize.ui.action.ColumnResizeCursorAction;
import org.eclipse.statet.ecommons.waltable.selection.core.SelectRelativeCellCommand;
import org.eclipse.statet.ecommons.waltable.selection.ui.action.CellSelectionDragMode;
import org.eclipse.statet.ecommons.waltable.selection.ui.action.RowSelectionDragMode;
import org.eclipse.statet.ecommons.waltable.selection.ui.action.SelectCellAction;
import org.eclipse.statet.ecommons.waltable.sort.core.ClearSortCommand;
import org.eclipse.statet.ecommons.waltable.sort.core.SortDimPositionCommand;
import org.eclipse.statet.ecommons.waltable.sort.core.SortDirection;
import org.eclipse.statet.ecommons.waltable.sort.ui.action.SortColumnAction;
import org.eclipse.statet.ecommons.waltable.ui.NatEventData;
import org.eclipse.statet.ecommons.waltable.ui.action.AbstractNavigationAction;
import org.eclipse.statet.ecommons.waltable.ui.action.ClearCursorAction;
import org.eclipse.statet.ecommons.waltable.ui.action.NoOpMouseAction;
import org.eclipse.statet.ecommons.waltable.ui.binding.UiBindingRegistry;
import org.eclipse.statet.ecommons.waltable.ui.matcher.IMouseEventMatcher;
import org.eclipse.statet.ecommons.waltable.ui.matcher.KeyEventMatcher;
import org.eclipse.statet.ecommons.waltable.ui.matcher.MouseEventMatcher;
import org.eclipse.statet.ecommons.waltable.ui.menu.PopupMenuAction;
import org.eclipse.statet.ecommons.waltable.viewport.core.ScrollStepCommand;
import org.eclipse.statet.ecommons.waltable.viewport.core.SelectRelativePageCommand;
import org.eclipse.statet.ecommons.waltable.viewport.ui.action.ViewportSelectDimPositionsAction;


public class UIBindings {
	
	
	private static final int CELL= 1;
	private static final int PAGE= 2;
	private static final int TABLE= 3;
	
	
	public static class ScrollAction extends AbstractNavigationAction {
		
		
		private final int type;
		
		
		public ScrollAction(final Direction direction, final int type) {
			super(direction);
			
			this.type= type;
		}
		
		
		@Override
		public void run(final NatTable natTable, final KeyEvent event) {
			switch (this.type) {
			case CELL:
				natTable.doCommand(new ScrollStepCommand(getDirection()));
				break;
			case PAGE:
				break;
			case TABLE:
				natTable.doCommand(new ScrollStepCommand(getDirection()));
				break;
			default:
				throw new IllegalStateException();
			}
		}
		
	}
	
	public static class SelectRelativeAction extends AbstractNavigationAction {
		
		
		private final int type;
		
		
		public SelectRelativeAction(final Direction direction, final int type) {
			super(direction);
			
			this.type= type;
		}
		
		
		@Override
		public void run(final NatTable natTable, final KeyEvent event) {
			final int selectionFlags= (event.stateMask & SWT.SHIFT);
			switch (this.type) {
			case CELL:
				natTable.doCommand(new SelectRelativeCellCommand(getDirection(), 1, selectionFlags));
				break;
			case PAGE:
				natTable.doCommand(new SelectRelativePageCommand(getDirection(), selectionFlags));
				break;
			case TABLE:
				natTable.doCommand(new SelectRelativeCellCommand(getDirection(), -1, selectionFlags));
				break;
			default:
				throw new IllegalStateException();
			}
		}
		
	}
	
	
	public static class ColumnHeaderConfiguration extends AbstractUiBindingConfiguration {
		
		public ColumnHeaderConfiguration() {
		}
		
		@Override
		public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
			uiBindingRegistry.registerFirstMouseMoveBinding(new ColumnResizeEventMatcher(
					IMouseEventMatcher.NO_BUTTON, true),
					new ColumnResizeCursorAction() );
			uiBindingRegistry.registerMouseMoveBinding(new MouseEventMatcher(),
					new ClearCursorAction() );
			
			uiBindingRegistry.registerFirstMouseDragMode(new ColumnResizeEventMatcher(
					IMouseEventMatcher.LEFT_BUTTON, true),
					new PositionResizeDragMode(HORIZONTAL) );
			
			uiBindingRegistry.registerDoubleClickBinding(new ColumnResizeEventMatcher(
					IMouseEventMatcher.LEFT_BUTTON, true),
					new AutoResizeColumnAction() );
			uiBindingRegistry.registerSingleClickBinding(new ColumnResizeEventMatcher(
					IMouseEventMatcher.LEFT_BUTTON, true),
					new NoOpMouseAction() );
		}
		
	}
	
	
	public static class SelectionConfiguration extends AbstractUiBindingConfiguration {
		
		
		public SelectionConfiguration() {
		}
		
		
		@Override
		public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
			// scroll/roll navigation
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.CTRL, SWT.ARROW_UP),
					new ScrollAction(Direction.UP, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.CTRL, SWT.ARROW_DOWN),
					new ScrollAction(Direction.DOWN, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.CTRL, SWT.ARROW_LEFT),
					new ScrollAction(Direction.LEFT, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.CTRL, SWT.ARROW_RIGHT),
					new ScrollAction(Direction.RIGHT, CELL) );
			
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SCROLL_LOCK, SWT.PAGE_UP),
//					new ScrollAction(Direction.UP, Scale.PAGE) );
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SCROLL_LOCK, SWT.PAGE_DOWN),
//					new ScrollAction(Direction.DOWN, Scale.PAGE) );
			
			// move anchor
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.ARROW_UP),
					new SelectRelativeAction(Direction.UP, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.ARROW_DOWN),
					new SelectRelativeAction(Direction.DOWN, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.ARROW_LEFT),
					new SelectRelativeAction(Direction.LEFT, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.ARROW_RIGHT),
					new SelectRelativeAction(Direction.RIGHT, CELL) );
			
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.PAGE_UP),
					new SelectRelativeAction(Direction.UP, PAGE) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.PAGE_DOWN),
					new SelectRelativeAction(Direction.DOWN, PAGE) );
			
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.HOME),
					new SelectRelativeAction(Direction.LEFT, TABLE) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.END),
					new SelectRelativeAction(Direction.RIGHT, TABLE) );
			
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.CTRL, SWT.HOME),
					new SelectRelativeAction(Direction.UP, TABLE) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.CTRL, SWT.END),
					new SelectRelativeAction(Direction.DOWN, TABLE) );
			
			// resize selection
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.ARROW_UP),
					new SelectRelativeAction(Direction.UP, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.ARROW_DOWN),
					new SelectRelativeAction(Direction.DOWN, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.ARROW_LEFT),
					new SelectRelativeAction(Direction.LEFT, CELL) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.ARROW_RIGHT),
					new SelectRelativeAction(Direction.RIGHT, CELL) );
			
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.PAGE_UP),
					new SelectRelativeAction(Direction.UP, PAGE) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.PAGE_DOWN),
					new SelectRelativeAction(Direction.DOWN, PAGE) );
			
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.HOME),
					new SelectRelativeAction(Direction.LEFT, TABLE) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.END),
					new SelectRelativeAction(Direction.RIGHT, TABLE) );
			
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT | SWT.CTRL, SWT.HOME),
					new SelectRelativeAction(Direction.UP, TABLE) );
			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT | SWT.CTRL, SWT.END),
					new SelectRelativeAction(Direction.DOWN, TABLE) );
			
			
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT | SWT.CONTROL, SWT.ARROW_UP), new MoveToFirstRowAction());
//			
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.CR), new MoveSelectionAction(Direction.UP, false, false));
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT | SWT.CONTROL, SWT.CR), action);
//			
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.TAB), action);
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.CONTROL, SWT.TAB), action);
//			
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT, SWT.TAB), new MoveSelectionAction(Direction.LEFT, false, false));
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.SHIFT | SWT.CONTROL, SWT.TAB), action);
//			
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.NONE, SWT.CR), action);
//			uiBindingRegistry.registerKeyBinding(new KeyEventMatcher(SWT.CONTROL, SWT.CR), action);
			
			// mouse
			uiBindingRegistry.registerMouseDownBinding(
					new MouseEventMatcher(MouseEventMatcher.WILDCARD_MASK | SWT.CTRL | SWT.SHIFT,
							GridLabels.BODY, IMouseEventMatcher.LEFT_BUTTON ),
					new SelectCellAction());
			
			uiBindingRegistry.registerMouseDownBinding(
					new MouseEventMatcher(MouseEventMatcher.WILDCARD_MASK | SWT.CTRL | SWT.SHIFT,
							GridLabels.COLUMN_HEADER, IMouseEventMatcher.LEFT_BUTTON ),
					new ViewportSelectDimPositionsAction(HORIZONTAL));
			
			uiBindingRegistry.registerMouseDownBinding(
					new MouseEventMatcher(MouseEventMatcher.WILDCARD_MASK | SWT.CTRL | SWT.SHIFT,
							GridLabels.ROW_HEADER, IMouseEventMatcher.LEFT_BUTTON ),
					new ViewportSelectDimPositionsAction(VERTICAL));
			
			uiBindingRegistry.registerMouseDragMode(
					new MouseEventMatcher(MouseEventMatcher.WILDCARD_MASK | SWT.CTRL | SWT.SHIFT,
							GridLabels.BODY, IMouseEventMatcher.LEFT_BUTTON ),
					new CellSelectionDragMode());
			
			uiBindingRegistry.registerMouseDragMode(
					new MouseEventMatcher(MouseEventMatcher.WILDCARD_MASK | SWT.CTRL | SWT.SHIFT,
							GridLabels.ROW_HEADER, IMouseEventMatcher.LEFT_BUTTON ),
					new RowSelectionDragMode());
		}
		
	}
	
	public static class SortConfiguration extends AbstractUiBindingConfiguration {
		
		
		public SortConfiguration() {
		}
		
		
		@Override
		public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
			uiBindingRegistry.registerSingleClickBinding(
					new MouseEventMatcher(SWT.ALT, GridLabels.COLUMN_HEADER, 1),
					new SortColumnAction(false));
		}
		
	}
	
	
	public static class HeaderContextMenuConfiguration extends AbstractUiBindingConfiguration {
		
		
		private final MenuManager menuManager;
		
		
		public HeaderContextMenuConfiguration(final NatTable natTable,
				final IWorkbenchPart part) {
			this.menuManager= new MenuManager();
			this.menuManager.createContextMenu(natTable);
			natTable.addDisposeListener(new DisposeListener() {
				@Override
				public void widgetDisposed(final DisposeEvent e) {
					HeaderContextMenuConfiguration.this.menuManager.dispose();
				}
			});
			
			this.menuManager.add(new Separator(UIActions.VIEW_SORT_GROUP_ID));
			this.menuManager.add(new NatTableContributionItem(
					SharedUIResources.getImages().getDescriptor(SharedUIResources.LOCTOOL_SORT_ALPHA_IMAGE_ID), null,
					"Sort Increasing by Column", "I") {
				@Override
				protected void execute(final NatEventData eventData) throws ExecutionException {
					eventData.getNatTable().doCommand(new SortDimPositionCommand(
							eventData.getNatTable().getDim(HORIZONTAL),
							eventData.getColumnPosition(), SortDirection.ASC, false ));
				}
			});
			this.menuManager.add(new NatTableContributionItem("Sort Decreasing by Column", "D") {
				@Override
				protected void execute(final NatEventData eventData) throws ExecutionException {
					eventData.getNatTable().doCommand(new SortDimPositionCommand(
							eventData.getNatTable().getDim(HORIZONTAL),
							eventData.getColumnPosition(), SortDirection.DESC, false ));
				}
			});
			this.menuManager.add(new NatTableContributionItem("Clear All Sorting", "O") {
				@Override
				protected void execute(final NatEventData eventData) throws ExecutionException {
					eventData.getNatTable().doCommand(new ClearSortCommand());
				}
			});
			
			this.menuManager.add(new Separator());
			this.menuManager.add(new NatTableContributionItem("Auto Resize Column", "R") {
				@Override
				protected void execute(final NatEventData eventData) throws ExecutionException {
					eventData.getNatTable().doCommand(new InitializeAutoResizeCommand(
							eventData.getNatTable().getDim(HORIZONTAL), eventData.getColumnPosition() ));
				}
			});
			
			this.menuManager.add(new Separator(UIActions.OPEN_GROUP_ID));
			if (part != null) {
				final MenuManager showIn= new MenuManager("Sho&w In");
				showIn.setActionDefinitionId(IWorkbenchCommandConstants.NAVIGATE_SHOW_IN_QUICK_MENU);
				showIn.add(ContributionItemFactory.VIEWS_SHOW_IN.create(part.getSite().getWorkbenchWindow()));
				this.menuManager.add(showIn);
			}
			
			this.menuManager.add(new Separator(UIActions.ADDITIONS_GROUP_ID));
		}
		
		@Override
		public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
			uiBindingRegistry.registerSingleClickBinding(
					new MouseEventMatcher(SWT.NONE, GridLabels.COLUMN_HEADER, IMouseEventMatcher.RIGHT_BUTTON),
					new PopupMenuAction(this.menuManager.getMenu()));
		}
		
		public IMenuManager getMenuManager() {
			return this.menuManager;
		}
		
	}
	
	public static class BodyContextMenuConfiguration extends AbstractUiBindingConfiguration {
		
		
		private final MenuManager menuManager;
		
		
		public BodyContextMenuConfiguration(final NatTable natTable,
				final IServiceLocator serviceLocator) {
			this.menuManager= new MenuManager();
			this.menuManager.createContextMenu(natTable);
			natTable.addDisposeListener(new DisposeListener() {
				@Override
				public void widgetDisposed(final DisposeEvent e) {
					BodyContextMenuConfiguration.this.menuManager.dispose();
				}
			});
			
			this.menuManager.add(new Separator("edit"));
			this.menuManager.add(new CommandContributionItem(
					new CommandContributionItemParameter(serviceLocator,
							null, IWorkbenchCommandConstants.EDIT_COPY, null,
							null, null, null,
							SharedMessages.CopyAction_name, null, null,
							SWT.PUSH, null, false )));
		}
		
		@Override
		public void configureUiBindings(final UiBindingRegistry uiBindingRegistry) {
			uiBindingRegistry.registerSingleClickBinding(
					new MouseEventMatcher(SWT.NONE, GridLabels.BODY, IMouseEventMatcher.RIGHT_BUTTON),
					new PopupMenuAction(this.menuManager.getMenu()));
		}
		
		public IMenuManager getMenuManager() {
			return this.menuManager;
		}
		
	}
	
}
