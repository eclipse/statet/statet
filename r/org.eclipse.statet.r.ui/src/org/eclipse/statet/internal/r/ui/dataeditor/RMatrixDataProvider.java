/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;
import org.eclipse.statet.r.ui.dataeditor.RDataTableInput;
import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore;
import org.eclipse.statet.rj.services.util.dataaccess.RMatrixDataAdapter;
import org.eclipse.statet.rj.ts.core.RToolService;


public class RMatrixDataProvider extends AbstractRDataProvider<RArray<?>> {
	
	
	public RMatrixDataProvider(final RDataTableInput input, final RArray<?> struct) throws CoreException {
		super(input, new RMatrixDataAdapter(), struct);
		
		reset();
	}
	
	
	@Override
	public boolean getAllColumnsEqual() {
		return true;
	}
	
	@Override
	protected ContentDescription loadDescription(final RElementName name,
			final RArray<?> struct,
			final RToolService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		final long count= getColumnCount();
		
		final RDataTableColumn rowHeaderColumn= createNamesColumn(
				"rownames(" + getInput().getFullName() + ")",
				getAdapter().getRowCount(struct),
				r, m );
		
		final RDataTableColumn template= createColumn(struct.getData(),
						getInput().getFullName(), null, -1, null,
						r, m );
		RDataTableColumn[] dataColumns= null;
		if (count <= 2500) {
			final int l= (int) count;
			RStore names;
			final RObject rObject= r.evalData("colnames(" + getInput().getFullName() + ")", m);
			if (rObject != null && rObject.getRObjectType() == RObject.TYPE_VECTOR
					&& rObject.getLength() == l) {
				names= rObject.getData();
			}
			else {
				names= null;
			}
			dataColumns= new RDataTableColumn[l];
			for (int i= 0; i < l; i++) {
				dataColumns[i]= new RDataTableColumn(i,
						(names != null) ? names.getChar(i) : Integer.toString((i+1)), null, null,
						template.getVarType(), template.getDataStore(), template.getClassNames(),
						template.getDefaultFormat());
			}
		}
		
		return new ContentDescription(name, struct, r.getTool(),
				ImCollections.emptyList(), ImCollections.newList(rowHeaderColumn),
				ImCollections.emptyList(), dataColumns,
				template.getDefaultFormat() );
	}
	
	@Override
	protected void appendOrderCmd(final StringBuilder cmd, final SortColumn sortColumn) {
		cmd.append("order(");
		cmd.append(getInput().getFullName());
		cmd.append("[,");
		cmd.append((sortColumn.getIdx() + 1));
		cmd.append("],decreasing=");
		cmd.append(sortColumn.decreasing ? "TRUE" : "FALSE");
		cmd.append(')');
	}
	
	
	@Override
	protected Object getDataValue(final LazyRStore.Fragment<RArray<?>> fragment,
			final long rowIdx, final long columnIdx) {
		return fragment.getRObject().getData().get(RDataUtils.getDataIdx(fragment.getRowCount(),
				fragment.toLocalRowIdx(rowIdx), fragment.toLocalColumnIdx(columnIdx) ));
	}
	
	@Override
	protected Object getColumnName(final LazyRStore.Fragment<RArray<?>> fragment, final long columnIdx) {
		final RStore names= fragment.getRObject().getNames(1);
		if (names != null) {
			return names.get(columnIdx - fragment.getColumnBeginIdx());
		}
		else {
			return Long.toString(columnIdx + 1);
		}
	}
	
}
