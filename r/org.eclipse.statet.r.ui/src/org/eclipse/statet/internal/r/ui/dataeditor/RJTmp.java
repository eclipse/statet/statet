/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RJTmp {
	
	
	public static final String SET_REVERSE_INDEX= "rj:::tmp.setReverseIndex"; //$NON-NLS-1$
	public static final String SET_FILTERED_INDEX= "rj:::tmp.setFilteredIndex"; //$NON-NLS-1$
	public static final String SET_WHICH_INDEX= "rj:::tmp.setWhichIndex"; //$NON-NLS-1$
	public static final String GET_FILTERED_COUNT= "rj:::tmp.getFilteredCount"; //$NON-NLS-1$
	
	public static final String NAME_PAR= "name"; //$NON-NLS-1$
	public static final String INDEX_PAR= "index"; //$NON-NLS-1$
	public static final String FILTER_PAR= "filter"; //$NON-NLS-1$
	public static final String LEN_PAR= "len"; //$NON-NLS-1$
	
}
