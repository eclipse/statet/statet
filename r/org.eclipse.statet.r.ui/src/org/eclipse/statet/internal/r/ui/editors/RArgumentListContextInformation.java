/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.jface.text.contentassist.IContextInformationExtension;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.IntArrayList;
import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInformationProposal;
import org.eclipse.statet.r.core.model.RFunctionSpec;
import org.eclipse.statet.r.core.model.rlang.RLangMethod;
import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.ui.RLabelProvider;


@NonNullByDefault
public class RArgumentListContextInformation implements AssistInformationProposal,
		IContextInformationExtension {
	
	
	private final int fCallArgsOffset; // can be negative for FragmentDocument
	private final @Nullable FCall fCallNode;
	
	private final @Nullable RFunctionSpec fSpec;
	
	private final String information;
	private final ImIntList informationParamIndexes;
	
	
	public RArgumentListContextInformation(final int fCallArgsOffset,
			final @Nullable FCall fCallNode, final RLangMethod<?> method) {
		this.fCallArgsOffset= fCallArgsOffset;
		this.fCallNode= fCallNode;
		this.fSpec= method.getFunctionSpec();
		{	// build information string
			final StringBuilder sb= new StringBuilder();
			final IntList idxs= new IntArrayList();
			new RLabelProvider().appendArgumentInformation(sb, idxs, this.fSpec);
			this.information= sb.toString();
			this.informationParamIndexes= ImCollections.toIntList(idxs);
		}
	}
	
	
	public @Nullable FCall getFCallNode() {
		return this.fCallNode;
	}
	
	public int getFCallArgsOffset() {
		return this.fCallArgsOffset;
	}
	
	public @Nullable RFunctionSpec getFunctionSpec() {
		return this.fSpec;
	}
	
	
	@Override
	public String getContextDisplayString() {
		return getInformationDisplayString();
	}
	
	@Override
	public @Nullable Image getImage() {
		return null;
	}
	
	@Override
	public int getContextInformationPosition() {
		return Math.max(this.fCallArgsOffset, 0);
	}
	
	@Override
	public String getInformationDisplayString() {
		return this.information;
	}
	
	/**
	 * Returns the indexes of the parameters in the information display string.
	 * 
	 * @return list with the indexes
	 */
	public ImIntList getInformationDisplayStringParamIndexes() {
		return this.informationParamIndexes;
	}
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		// prevent stacking of context information at the same position
		return true;
	}
	
}
