/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.graphics;

import org.eclipse.ui.IWorkbenchPage;

import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.rj.eclient.graphics.ERGraphic;
import org.eclipse.statet.rj.eclient.graphics.PageBookRGraphicView;
import org.eclipse.statet.rj.ts.core.RTool;


public class ShowGraphicViewListener extends PageBookRGraphicView.ShowRequiredViewListener {
	
	
	public ShowGraphicViewListener() {
		super("org.eclipse.statet.r.views.RGraphic"); //$NON-NLS-1$
	}
	
	
	@Override
	protected IWorkbenchPage getBestPage(final ERGraphic graphic) {
		final RTool tool= graphic.getRHandle();
		if (tool instanceof ToolProcess) {
			return NicoUI.getToolRegistry().findWorkbenchPage(tool);
		}
		return super.getBestPage(graphic);
	}
	
}
