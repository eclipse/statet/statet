/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.ui.workbench.ContainerSelectionComposite.ContainerFilter;

import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;
import org.eclipse.statet.ltk.buildpath.core.BuildpathUtils;
import org.eclipse.statet.ltk.ui.wizards.NewElementWizardPage;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;


/**
 * The "New" wizard page allows setting the container for
 * the new file as well as the file name. The page
 * will only accept file name without the extension or
 * with the extension that matches the expected one (r).
 */
public class NewRFileCreationWizardPage extends NewElementWizardPage {
	
	
	private static final String fgDefaultExtension= ".R"; //$NON-NLS-1$
	
	private static class RSourceFolderFilter extends ContainerFilter {
		
		@Override
		public boolean select(final IContainer container) {
			try {
				final IProject project= container.getProject();
				if (container.getType() == IResource.PROJECT) {
					if (project.hasNature(RProjects.R_NATURE_ID)) {
						return true;
					}
				}
				else {
					final RProject rProject= RProjects.getRProject(project);
					if (rProject != null) {
						final ImList<BuildpathElement> buildpath= rProject.getRawBuildpath();
						for (final BuildpathElement sourceContainer : buildpath) {
							if (sourceContainer.getPath().isPrefixOf(container.getFullPath())) {
								return (!BuildpathUtils.isExcluded(container, sourceContainer));
							}
						}
					}
				}
			}
			catch (final CoreException e) {	}
			
			return false;
		}
		
	}
	
	
	private final ResourceGroup resourceGroup;
	
	/**
	 * Constructor.
	 */
	public NewRFileCreationWizardPage(final IStructuredSelection selection) {
		super("NewRFileCreationWizardPage", selection); //$NON-NLS-1$
		
		setTitle(Messages.NewRScriptFileWizardPage_title);
		setDescription(Messages.NewRScriptFileWizardPage_description);
		
		this.resourceGroup= new ResourceGroup(fgDefaultExtension, new RSourceFolderFilter());
	}
	
	
	@Override
	protected void createContents(final Composite parent) {
		this.resourceGroup.createGroup(parent);
	}
	
	ResourceGroup getResourceGroup() {
		return this.resourceGroup;
	}
	
	@Override
	public void setVisible(final boolean visible) {
		super.setVisible(visible);
		if (visible) {
			this.resourceGroup.setFocus();
		}
	}
	
	public void saveSettings() {
		this.resourceGroup.saveSettings();
	}
	
	@Override
	protected void validatePage() {
		updateStatus(this.resourceGroup.validate());
	}
	
}
