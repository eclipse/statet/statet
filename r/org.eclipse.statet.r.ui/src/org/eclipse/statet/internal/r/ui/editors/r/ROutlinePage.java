/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.r;

import static org.eclipse.statet.ecommons.ui.actions.UIActions.ADDITIONS_GROUP_ID;
import static org.eclipse.statet.ecommons.ui.actions.UIActions.VIEW_FILTER_GROUP_ID;
import static org.eclipse.statet.ecommons.ui.actions.UIActions.VIEW_SORT_GROUP_ID;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.ui.SharedMessages;
import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.base.ui.IStatetUIMenuIds;
import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.ui.ElementNameComparator;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor2OutlinePage;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.refactoring.RRefactoring;
import org.eclipse.statet.r.launching.RCodeLaunching;
import org.eclipse.statet.r.ui.RUI;


/**
 * Outline page for R sources
 */
@NonNullByDefault
public class ROutlinePage extends SourceEditor2OutlinePage {
	
	private static final ViewerComparator ALPHA_COMPARATOR= new ElementNameComparator(RElementName.NAMEONLY_COMPARATOR);
	
	private class AlphaSortAction extends ToggleAction {
		
		
		public AlphaSortAction() {
			super("sort.alphabetically.enabled", false, 2); //$NON-NLS-1$
			setText(SharedMessages.ToggleSortAction_name);
			setImageDescriptor(SharedUIResources.getImages().getDescriptor(SharedUIResources.LOCTOOL_SORT_ALPHA_IMAGE_ID));
			setToolTipText(SharedMessages.ToggleSortAction_tooltip);
		}
		
		@Override
		protected void configure(final boolean on) {
			final TreeViewer viewer= getViewer();
			if (viewer == null || !UIAccess.isOkToUse(viewer.getControl())) {
				return;
			}
			viewer.setComparator(on ? ALPHA_COMPARATOR : null);
		}
		
	}
	
	private class FilterCommonVariables extends ToggleAction {
		
		public FilterCommonVariables() {
			super("filter.common_var.enabled", false, 2); //$NON-NLS-1$
			setText(RUIMessages.Outline_HideGeneralVariables_name);
			setImageDescriptor(RUI.getImageDescriptor(RUIPlugin.IMG_LOCTOOL_FILTER_GENERAL));
			setToolTipText(RUIMessages.Outline_HideGeneralVariables_name);
		}
		
		@Override
		protected void configure(final boolean on) {
			ROutlinePage.this.contentFilter.hideCommonVariables= on;
			
			final TreeViewer viewer= getViewer();
			if (viewer == null || !UIAccess.isOkToUse(viewer.getControl())) {
				return;
			}
			viewer.refresh(false);
		}
		
	}
	
	private class FilterLocalDefinitions extends ToggleAction {
		
		public FilterLocalDefinitions() {
			super("filter.local.enabled", false, 2); //$NON-NLS-1$
			setText(RUIMessages.Outline_HideLocalElements_name);
			setImageDescriptor(RUI.getImageDescriptor(RUIPlugin.IMG_LOCTOOL_FILTER_LOCAL));
			setToolTipText(RUIMessages.Outline_HideLocalElements_name);
		}
		
		@Override
		protected void configure(final boolean on) {
			ROutlinePage.this.contentFilter.hideLocalDefinitions= on;
			
			final TreeViewer viewer= getViewer();
			if (viewer == null || !UIAccess.isOkToUse(viewer.getControl())) {
				return;
			}
			viewer.refresh(false);
		}
		
	}
	
	private class ContentFilter implements LtkModelElementFilter<SourceStructElement<?, ?>> {
		
		private boolean hideCommonVariables;
		private boolean hideLocalDefinitions;
		
		@Override
		public boolean include(final SourceStructElement<?, ?> element) {
			switch (element.getElementType()) {
			case RElement.R_ARGUMENT:
				return false;
			case RElement.R_GENERAL_VARIABLE:
				return !this.hideCommonVariables;
			case RElement.R_GENERAL_LOCAL_VARIABLE:
				return !this.hideCommonVariables && !this.hideLocalDefinitions;
			case RElement.R_COMMON_LOCAL_FUNCTION:
				return !this.hideLocalDefinitions;
			default:
				return true;
			}
		}
		
	}
	
	
	private final ContentFilter contentFilter= new ContentFilter();
	
	public ROutlinePage(final REditor editor) {
		super(editor, RModel.R_TYPE_ID,
				RRefactoring.getFactory(),
				"org.eclipse.statet.r.menus.ROutlineViewContextMenu"); //$NON-NLS-1$
	}
	
	
	@Override
	protected IDialogSettings getDialogSettings() {
		return DialogUtils.getDialogSettings(RUIPlugin.getInstance(), "ROutlineView"); //$NON-NLS-1$
	}
	
	@Override
	protected LtkModelElementFilter<SourceStructElement<?, ?>> getContentFilter() {
		return this.contentFilter;
	}
	
	@Override
	protected void contributeToActionBars(final IServiceLocator serviceLocator,
			final IActionBars actionBars, final HandlerCollection handlers) {
		super.contributeToActionBars(serviceLocator, actionBars, handlers);
		
		final IToolBarManager toolBarManager= actionBars.getToolBarManager();
		
		toolBarManager.appendToGroup(VIEW_SORT_GROUP_ID,
				new AlphaSortAction());
		toolBarManager.appendToGroup(VIEW_FILTER_GROUP_ID,
				new FilterCommonVariables());
		toolBarManager.appendToGroup(VIEW_FILTER_GROUP_ID,
				new FilterLocalDefinitions());
	}
	
	@Override
	protected void contextMenuAboutToShow(final IMenuManager menuManager) {
		final var site= getSite();
		
		super.contextMenuAboutToShow(menuManager);
		
		if (menuManager.find(IStatetUIMenuIds.GROUP_SUBMIT_MENU_ID) == null) {
			menuManager.insertBefore(ADDITIONS_GROUP_ID,
					new Separator(IStatetUIMenuIds.GROUP_SUBMIT_MENU_ID) );
		}
		
		menuManager.appendToGroup(IStatetUIMenuIds.GROUP_SUBMIT_MENU_ID, new CommandContributionItem(
				new CommandContributionItemParameter(site,
						null, RCodeLaunching.SUBMIT_SELECTION_COMMAND_ID, null,
						null, null, null,
						null, "R", null, //$NON-NLS-1$
						CommandContributionItem.STYLE_PUSH, null, false )));
		menuManager.appendToGroup(IStatetUIMenuIds.GROUP_SUBMIT_MENU_ID, new CommandContributionItem(
				new CommandContributionItemParameter(site,
						null, RCodeLaunching.SUBMIT_UPTO_SELECTION_COMMAND_ID, null,
						null, null, null,
						null, "U", null, //$NON-NLS-1$
						CommandContributionItem.STYLE_PUSH, null, false )));
		
		menuManager.add(new Separator(IStatetUIMenuIds.GROUP_ADD_MORE_ID));
	}
	
}
