/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.hyperlink.AbstractHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.ui.sourceediting.ROpenDeclarationHandler;


@NonNullByDefault
public class RElementHyperlinkDetector extends AbstractHyperlinkDetector {
	
	
	public RElementHyperlinkDetector() {
	}
	
	
	@Override
	public @NonNull IHyperlink @Nullable [] detectHyperlinks(final ITextViewer textViewer,
			final IRegion region, final boolean canShowMultipleHyperlinks) {
		final SourceEditor editor= getAdapter(SourceEditor.class);
		if (editor == null) {
			return null;
		}
		
		final List<IHyperlink> hyperlinks= new ArrayList<>(4);
		final var access= ROpenDeclarationHandler.searchAccess(editor,
				JFaceTextRegion.toTextRegion(region) );
		if (access instanceof RElementAccess) {
			hyperlinks.add(new OpenRElementHyperlink(editor, (RSourceUnit)editor.getSourceUnit(),
					(RElementAccess)access ));
		}
		
		if (!hyperlinks.isEmpty()) {
			return hyperlinks.toArray(new @NonNull IHyperlink[hyperlinks.size()]);
		}
		return null;
	}
	
}
