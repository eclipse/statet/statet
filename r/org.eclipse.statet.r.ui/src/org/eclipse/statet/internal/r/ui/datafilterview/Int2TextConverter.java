/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.core.conversion.ClassTypedConverter;


@NonNullByDefault
public class Int2TextConverter implements ClassTypedConverter<Integer, String> {
	
	
	public Int2TextConverter() {
	}
	
	
	@Override
	public Class<Integer> getFromType() {
		return Integer.TYPE;
	}
	
	@Override
	public Class<String> getToType() {
		return String.class;
	}
	
	@Override
	public String convert(final Integer fromObject) {
		return fromObject.toString();
	}
	
}
