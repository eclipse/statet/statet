/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.rlang.RFrame;
import org.eclipse.statet.r.core.model.rlang.RFrameSearchPath;
import org.eclipse.statet.r.core.model.rlang.RLangElement;
import org.eclipse.statet.r.core.model.rlang.RLangMethod;


@NonNullByDefault
public class FCallNamePattern {
	
	
	private final RElementName elementName;
	
	private final @Nullable String packageName;
	
	private final @Nullable String assignName;
	private final int assignLength;
	
	
	public FCallNamePattern(final RElementName name) {
		this.elementName= name;
		
		final RElementName scope= name.getScope();
		this.packageName= (scope != null && RElementName.isPackageFacetScopeType(scope.getType())) ?
				scope.getSegmentName() : null;
		
		if (this.elementName.getNextSegment() == null) {
			this.assignName= this.elementName.getSegmentName();
			this.assignLength= this.assignName.length();
		}
		else {
			this.assignName= null;
			this.assignLength= 0;
		}
	}
	
	
	public final RElementName getElementName() {
		return this.elementName;
	}
	
	public boolean matches(final RElementName candidateName) {
		String candidate0;
		return (candidateName != null
				&& (this.elementName.equals(candidateName, RElementName.IGNORE_SCOPE)
						|| (this.assignName != null && candidateName.getNextSegment() == null
								&& this.assignLength == (candidate0= candidateName.getSegmentName()).length() - 2
								&& this.elementName.getType() == candidateName.getType()
								&& candidate0.charAt(this.assignLength) == '<' && candidate0.charAt(this.assignLength + 1) == '-'
								&& candidate0.regionMatches(false, 0, this.assignName, 0, this.assignLength) )));
	}
	
	
	public void searchFDef(final RFrameSearchPath searchPath) {
		ITER_FRAMES: for (final var iter= searchPath.iterator(); iter.hasNext(); ) {
			final RFrame<?> frame= iter.next();
			if (this.packageName != null) {
				final RElementName frameName= frame.getElementName();
				if (!(frameName != null
						&& RElementName.isPackageFacetScopeType(frameName.getType())
						&& this.packageName.equals(frameName.getSegmentName()) )) {
					continue ITER_FRAMES;
				}
			}
			final List<? extends RLangElement<?>> elements= frame.getModelChildren(null);
			for (final var candidate : elements) {
				if (candidate.getModelTypeId() == RModel.R_TYPE_ID
						&& (candidate.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_METHOD
						&& matches(candidate.getElementName()) ) {
					handleMatch((RLangMethod)candidate, frame, iter);
				}
			}
		}
	}
	
	protected void handleMatch(final RLangMethod<?> element, final RFrame<?> frame,
			final RFrameSearchPath.Iterator iterator) {
	}
	
}
