/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.nolazy;

import java.net.URL;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;


public class RResourceDecorator extends BaseLabelProvider implements ILightweightLabelDecorator {
	
	
	private static final Path OVR_PATH= new Path("icons/ovr_16"); //$NON-NLS-1$
	
	
	private static ImageDescriptor fPackageProjectOverlay;
	
	
	/**
	 * Created by extension point
	 */
	public RResourceDecorator() {
	}
	
	
	@Override
	public void decorate(final Object element, final IDecoration decoration) {
		IProject project = null;
		if (element instanceof IProject) {
			project = (IProject) element;
		}
		if (project != null) {
			try {
				if (project.hasNature(org.eclipse.statet.r.core.project.RProjects.R_PKG_NATURE_ID)) {
					decoration.addOverlay(getPackageProjectOverlay(), IDecoration.TOP_LEFT);
				}
			}
			catch (final CoreException e) {
			}
		}
	}
	
	private ImageDescriptor getPackageProjectOverlay() {
		if (fPackageProjectOverlay == null) {
			fPackageProjectOverlay = createOverlayDescriptor("rpkg_project"); //$NON-NLS-1$
		}
		return fPackageProjectOverlay;
	}
	
	private ImageDescriptor createOverlayDescriptor(final String id) {
		final URL url= FileLocator.find(Platform.getBundle(org.eclipse.statet.r.ui.RUI.BUNDLE_ID),
				OVR_PATH.append(id + ".png"), null ); //$NON-NLS-1$
		return ImageDescriptor.createFromURL(url);
	}
	
}
