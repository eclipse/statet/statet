/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import org.eclipse.search.ui.ISearchPageScoreComputer;
import org.eclipse.ui.IEditorInput;

import org.eclipse.statet.ltk.ui.EditorUtils;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.ui.RUI;


public class RSearchPageScoreComputer implements ISearchPageScoreComputer {
	
	
	public RSearchPageScoreComputer() {
	}
	
	
	@Override
	public int computeScore(final String pageId, final Object input) {
		if (RUI.R_HELP_SEARCH_PAGE_ID.equals(pageId)) {
			if (input instanceof RElement || input instanceof RSourceUnit) {
				return 85;
			}
			if (input instanceof IEditorInput
					&& EditorUtils.isModelTypeEditorInput((IEditorInput) input, RModel.R_TYPE_ID)) {
				return 85;
			}
			return ISearchPageScoreComputer.LOWEST;
		}
		return ISearchPageScoreComputer.UNKNOWN;
	}
	
}
