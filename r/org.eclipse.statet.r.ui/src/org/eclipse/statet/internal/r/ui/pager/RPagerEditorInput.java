/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pager;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public class RPagerEditorInput implements IEditorInput {
	
	
	public static class TextFile {
		
		private final String name;
		private final String content;
		
		public TextFile(final String name, final String content) {
			this.name= name;
			this.content= content;
		}
		
		public String getName() {
			return this.name;
		}
		
		public String getContent() {
			return this.content;
		}
		
	}
	
	
	private final String name;
	private final RTool source;
	
	private final ImList<TextFile> files;
	
	
	public RPagerEditorInput(final String name, final RTool source,
			final ImList<TextFile> files) {
		this.name= name;
		this.source= source;
		this.files= files;
	}
	
	
	@Override
	public boolean exists() {
		return true;
	}
	
	@Override
	public @Nullable ImageDescriptor getImageDescriptor() {
		return RUI.getImageDescriptor(RUI.RD_EDITOR_ID);
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public String getToolTipText() {
		return this.name + '\n' + this.source.getLabel(Tool.LONG_LABEL);
	}
	
	
	public ImList<TextFile> getFiles() {
		return this.files;
	}
	
	@Override
	public @Nullable IPersistableElement getPersistable() {
		return null;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapter) {
		return null;
	}
	
}
