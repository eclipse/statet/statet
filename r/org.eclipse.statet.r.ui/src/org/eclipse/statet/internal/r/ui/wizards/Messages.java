/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String NewRProjectWizard_title;
	public static String NewRPkgProjectWizard_title;
	
	public static String NewRProjectWizardPage_title;
	public static String NewRProjectWizardPage_description;
	
	public static String NewRPkgProjectWizardPage_title;
	public static String RPkgWizardPage_title;
	public static String RPkgWizardPage_description;
	
	public static String NewRScriptFileWizard_title;
	public static String NewRScriptFileWizardPage_title;
	public static String NewRScriptFileWizardPage_description;
	
	public static String NewRDocFileWizard_title;
	public static String NewRDocFileWizardPage_title;
	public static String NewRDocFileWizardPage_description;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
