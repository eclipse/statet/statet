/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rpkg;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.ROUND_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import static org.eclipse.statet.ecommons.text.ui.assist.LinkedModeBracketLevel.AUTODELETE;

import static org.eclipse.statet.internal.r.ui.RUIPreferenceInitializer.R_EDITOR_NODE;
import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_ANY_CONTENT_CONSTRAINT;
import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_DEFAULT_CONTENT_CONSTRAINT;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.DocumentCommand;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.text.link.LinkedModeModel;
import org.eclipse.jface.text.link.LinkedModeUI;
import org.eclipse.jface.text.link.LinkedPositionGroup;
import org.eclipse.swt.events.KeyEvent;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.string.CharPair;
import org.eclipse.statet.jcommons.text.core.CharPairSet;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceObjectDef;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.text.IIndentSettings;
import org.eclipse.statet.ecommons.text.IndentUtil;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartition;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;

import org.eclipse.statet.ltk.ui.sourceediting.AbstractAutoEditStrategy;
import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RPkgDescrPartitionNodeType;
import org.eclipse.statet.r.core.source.util.RPkgDescrHeuristicTokenScanner;
import org.eclipse.statet.r.ui.editors.REditorOptions;


public class RPkgDescrAutoEditStrategy extends AbstractAutoEditStrategy {
	
	
	public static class Config implements SmartInsertSettings {
		
		
		private final boolean enabledByDefault;
		private final TabAction tabAction;
		private final boolean closeBrackets;
		
		
		public Config(final PreferenceAccess prefs) {
			this.enabledByDefault= prefs.getPreferenceValue(REditorOptions.SMARTINSERT_BYDEFAULT_ENABLED_PREF);
			this.tabAction= prefs.getPreferenceValue(REditorOptions.SMARTINSERT_TAB_ACTION_PREF);
			this.closeBrackets= true;
		}
		
		
		@Override
		public boolean isSmartInsertEnabledByDefault() {
			return this.enabledByDefault;
		}
		
		@Override
		public TabAction getSmartInsertTabAction() {
			return this.tabAction;
		}
		
	}
	
	private static final PreferenceObjectDef<Config> CONFIG_PREF_FACTORY=
			PreferenceObjectDef.createFactory(Config.class, ImCollections.newSet(
							R_EDITOR_NODE ),
					Config::new );
	
	
	private final RCoreAccess rCoreAccess;
	private Config settings;
	
	private RPkgDescrHeuristicTokenScanner scanner;
	private RCodeStyleSettings rCodeStyle;
	
	
	public RPkgDescrAutoEditStrategy(final RCoreAccess coreAccess, final SourceEditor editor) {
		super(editor);
		assert (coreAccess != null);
		
		this.rCoreAccess= coreAccess;
	}
	
	
	@Override
	public Config getSettings() {
		return this.settings;
	}
	
	@Override
	protected IIndentSettings getCodeStyleSettings() {
		return this.rCodeStyle;
	}
	
	
	@Override
	protected final TreePartition initCustomization(final int offset, final int ch)
			throws BadLocationException, BadPartitioningException {
		if (this.scanner == null) {
			this.scanner= createScanner();
		}
		this.settings= PreferenceUtils.getPreferenceObject(this.rCoreAccess.getPrefs(), CONFIG_PREF_FACTORY);
		this.rCodeStyle= this.rCoreAccess.getRCodeStyle();
		
		return super.initCustomization(offset, ch);
	}
	
	protected RPkgDescrHeuristicTokenScanner createScanner() {
		return new RPkgDescrHeuristicTokenScanner(getDocumentContentInfo());
	}
	
	@Override
	protected TextRegion computeValidRange(final int offset, final TreePartition partition, final int ch) {
		TreePartitionNode node= partition.getTreeNode();
		if (node.getType() instanceof RPkgDescrPartitionNodeType) {
			if (getDocumentContentInfo().getPrimaryType() == RDocumentConstants.RPKG_DESCR_PARTITIONING) {
				return super.computeValidRange(offset, partition, ch);
			}
			else {
				TreePartitionNode parent;
				while ((parent= node.getParent()) != null
						&& parent instanceof RPkgDescrPartitionNodeType) {
					node= parent;
				}
				return node;
			}
		}
		return null;
	}
	
	@Override
	protected RPkgDescrHeuristicTokenScanner getScanner() {
		return this.scanner;
	}
	
	@Override
	protected final void quitCustomization() {
		super.quitCustomization();
		
		this.rCodeStyle= null;
	}
	
	
	private final boolean isClosedBracket(final int backwardOffset, final int forwardOffset,
			final TextRegion region, final CharPair searchType)
			throws BadLocationException {
		final CharPairSet brackets= RPkgDescrHeuristicTokenScanner.DESCR_TEXT_BRACKETS;
		final int searchPairIndex= brackets.getPairIndex(searchType);
		int[] balance= new int[brackets.getPairCount()];
		balance[searchPairIndex]++;
		this.scanner.configure(getDocument());
		balance= this.scanner.computePairBalance(backwardOffset, forwardOffset, region,
				brackets, balance, searchPairIndex );
		return (balance[searchPairIndex] <= 0);
	}
	
	
	@Override
	protected char isCustomizeKey(final KeyEvent event) {
		switch (event.character) {
		case '{':
		case '[':
		case '(':
//		case '"':
//		case '\'':
			return event.character;
		case '\t':
			if (event.stateMask == 0) {
				return '\t';
			}
			break;
		case 0x0A:
		case 0x0D:
			if (getEditor3() != null) {
				return '\n';
			}
			break;
		default:
			break;
		}
		return 0;
	}
	
	@Override
	protected void doCustomizeKeyCommand(final char ch, final DocumentCommand command,
			final TreePartition partition) throws Exception {
		final String contentType= partition.getType();
		final int cEnd= command.offset+command.length;
		int linkedModeType= -1;
		int linkedModeOffset= -1;
		
		KEY: switch (ch) {
		case '\t':
			if (RPKG_DESCR_ANY_CONTENT_CONSTRAINT.matches(contentType)
					&& isRegularTabCommand(command)) {
				command.text= "\t"; //$NON-NLS-1$
				smartInsertOnTab(command, true);
				break KEY;
			}
			return;
		case '{':
			if (RPKG_DESCR_DEFAULT_CONTENT_CONSTRAINT.matches(contentType)) {
				command.text= "{"; //$NON-NLS-1$
				if (this.settings.closeBrackets) {
					if (!isClosedBracket(command.offset, cEnd, partition, CURLY_BRACKETS)) {
						command.text= "{}"; //$NON-NLS-1$
						linkedModeType= 2 | AUTODELETE;
					}
					else if (getChar(cEnd) == '}') {
						linkedModeType= 2;
					}
				}
				break KEY;
			}
			return;
		case '[':
			if (RPKG_DESCR_DEFAULT_CONTENT_CONSTRAINT.matches(contentType)) {
				command.text= "["; //$NON-NLS-1$
				if (this.settings.closeBrackets) {
					if (!isClosedBracket(command.offset, cEnd, partition, SQUARE_BRACKETS)) {
						command.text= "[]"; //$NON-NLS-1$
						linkedModeType= 2 | AUTODELETE;
					}
					else if (getChar(cEnd) == ']') {
						linkedModeType= 2;
					}
				}
				break KEY;
			}
			return;
		case '(':
			if (RPKG_DESCR_DEFAULT_CONTENT_CONSTRAINT.matches(contentType)) {
				command.text= "("; //$NON-NLS-1$
				if (this.settings.closeBrackets) {
					if (!isClosedBracket(command.offset, cEnd, partition, ROUND_BRACKETS)) {
						command.text= "()"; //$NON-NLS-1$
						linkedModeType= 2 | AUTODELETE;
					}
					else if (getChar(cEnd) == ')') {
						linkedModeType= 2;
					}
				}
				break KEY;
			}
			return;
		case '\n':
			if (RPKG_DESCR_DEFAULT_CONTENT_CONSTRAINT.matches(contentType)) {
				command.text= TextUtilities.getDefaultLineDelimiter(getDocument());
				smartIndentOnNewLine(command, contentType);
				break KEY;
			}
			return;
		default:
			assert (false);
			return;
		}
		
		if (command.doit && command.text.length() > 0 && getEditor().isEditable(true)) {
			getViewer().getTextWidget().setRedraw(false);
			try {
				applyCommand(command);
				updateSelection(command);
				
				if (linkedModeType >= 0) {
					if (linkedModeOffset < 0) {
						linkedModeOffset= command.offset;
					}
					createLinkedMode(linkedModeOffset, ch, linkedModeType).enter();
				}
			}
			finally {
				getViewer().getTextWidget().setRedraw(true);
			}
		}
	}
	
	@Override
	protected void doCustomizeOtherCommand(final DocumentCommand command, final TreePartition partition)
			throws Exception {
		final String contentType= partition.getType();
		
		if (RPKG_DESCR_DEFAULT_CONTENT_CONSTRAINT.matches(contentType)) {
			if (command.length == 0 && TextUtilities.equals(getDocument().getLegalLineDelimiters(), command.text) != -1) {
				smartIndentOnNewLine(command, contentType);
			}
		}
	}
	
	
	private void smartIndentOnNewLine(final DocumentCommand command, final String partitionType)
			throws Exception {
		final String lineDelimiter= command.text;
		smartIndentAfterNewLine1(command, lineDelimiter);
	}
	
	private void smartIndentAfterNewLine1(final DocumentCommand command, final String lineDelimiter)
			throws BadLocationException, BadPartitioningException, CoreException {
		final AbstractDocument doc= getDocument();
		final StringBuilder sb= new StringBuilder(command.text);
		int nlIndex= lineDelimiter.length();
		final int line= doc.getLineOfOffset(command.offset);
		int checkOffset= Math.max(0, command.offset);
		
		final ITypedRegion partition= doc.getPartition(
				this.scanner.getDocumentPartitioning(), checkOffset, true );
		
		final IndentUtil util= new IndentUtil(doc, this.rCodeStyle);
		final int column= util.getLineIndent(line, false)[IndentUtil.COLUMN_IDX];
		
		if (nlIndex <= sb.length()) {
			sb.insert(nlIndex, util.createIndentString(column));
		}
		command.text= sb.toString();
	}
	
	private LinkedModeUI createLinkedMode(final int offset, final char type, final int mode)
			throws BadLocationException {
		final LinkedModeModel model= new LinkedModeModel();
		int pos= 0;
		
		final var group= new LinkedPositionGroup();
		final var position= RPkgDescrBracketLevel.createPosition(type, getDocument(),
				offset + 1, 0, pos++ );
		group.addPosition(position);
		model.addGroup(group);
		
		model.forceInstall();
		
		final var level= new RPkgDescrBracketLevel(model, getDocument(), getDocumentContentInfo(),
				ImCollections.newList(position), (mode & 0xffff0000) );
		
		final LinkedModeUI ui= new LinkedModeUI(model, getViewer());
		ui.setCyclingMode(LinkedModeUI.CYCLE_NEVER);
		ui.setExitPosition(getViewer(), offset + (mode & 0xff), 0, pos);
		ui.setSimpleMode(true);
		ui.setExitPolicy(level);
		return ui;
	}
	
}
