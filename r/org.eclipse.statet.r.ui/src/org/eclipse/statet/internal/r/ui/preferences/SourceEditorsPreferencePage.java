/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.preferences;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.PreferenceUIUtils;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;


@NonNullByDefault
public class SourceEditorsPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public SourceEditorsPreferencePage() {
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() {
		return new SourceEditorsConfigurationBlock(createStatusChangedListener());
	}
	
}


@NonNullByDefault
class SourceEditorsConfigurationBlock extends ManagedConfigurationBlock {
	
	
	public SourceEditorsConfigurationBlock(final StatusChangeListener statusListener) {
		super(null, statusListener);
	}
	
	
	@Override
	public void createBlockArea(final Composite pageComposite) {
		addLinkHeader(pageComposite, PreferenceUIUtils.composeSeeAlsoPreferencePages()
				.add("All <a>Text Editors</a> in Eclipse", "org.eclipse.ui.preferencePages.GeneralTextEditor+")
				.add("All <a>Source Editors of StatET</a>", "org.eclipse.statet.ltk.preferencePages.SourceEditors")
				.toString() );
	}
	
}
