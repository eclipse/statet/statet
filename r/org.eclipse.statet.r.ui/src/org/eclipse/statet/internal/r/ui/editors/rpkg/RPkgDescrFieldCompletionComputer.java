/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rpkg;

import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_FIELD_NAME_CONTENT_TYPE;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.SearchPattern;

import org.eclipse.statet.internal.r.ui.editors.rpkg.RPkgDescrFieldNameCompletionProposal.FieldNameElementProposalParameters;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputer;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;
import org.eclipse.statet.r.core.model.RPkgDescriptions;
import org.eclipse.statet.r.ui.sourceediting.RPkgDescrAssistInvocationContext;


@NonNullByDefault
public class RPkgDescrFieldCompletionComputer implements ContentAssistComputer {
	
	
	protected static final int COMMON_PRIO= 40;
	protected static final int PRESENT_PRIO= -40;
	
	
	private static final Set<RPkgDescrFieldDefinition> COMMON_FIELDS= Set.of(
			RPkgDescriptions.Type_FIELD,
			RPkgDescriptions.Authors_R_FIELD,
			RPkgDescriptions.SystemRequirements_FIELD,
			RPkgDescriptions.Depends_FIELD,
			RPkgDescriptions.Imports_FIELD,
			RPkgDescriptions.Suggests_FIELD,
			RPkgDescriptions.Enhances_FIELD,
			RPkgDescriptions.URL_FIELD );
	
	
	private int searchMatchRules;
	
	
	public RPkgDescrFieldCompletionComputer() {
	}
	
	
	@Override
	public void onSessionStarted(final SourceEditor editor, final ContentAssist assist) {
		int matchRules= SearchPattern.PREFIX_MATCH;
		if (assist.getShowSubstringMatches()) {
			matchRules |= SearchPattern.SUBSTRING_MATCH;
		}
		this.searchMatchRules= matchRules;
	}
	
	@Override
	public void onSessionEnded() {
	}
	
	
	protected int getSearchMatchRules() {
		return this.searchMatchRules;
	}
	
	
	@Override
	public void computeCompletionProposals(final AssistInvocationContext context,
			final int mode, final AssistProposalCollector proposals,
			final IProgressMonitor monitor) {
		if (context instanceof RPkgDescrAssistInvocationContext
				&& context.getInvocationContentType() == RPKG_DESCR_FIELD_NAME_CONTENT_TYPE) {
			computeCompletionProposals((RPkgDescrAssistInvocationContext)context, mode, proposals,
					monitor );
		}
	}
	
	@Override
	public void computeInformationProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		if (context instanceof RPkgDescrAssistInvocationContext
				&& context.getInvocationContentType() == RPKG_DESCR_FIELD_NAME_CONTENT_TYPE) {
			computeContextProposals((RPkgDescrAssistInvocationContext)context, proposals, monitor);
		}
	}
	
	protected void computeCompletionProposals(final RPkgDescrAssistInvocationContext context, final int mode,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		final var modelInfo= context.getModelInfo();
		final String prefix= context.getIdentifierPrefix();
		if (modelInfo == null || prefix == null) {
			return;
		}
		
		final var parameters= new FieldNameElementProposalParameters(
				context, context.getIdentifierOffset(),
				new SearchPattern(getSearchMatchRules(), prefix) );
		
		for (final RPkgDescrFieldDefinition fieldDef : RPkgDescriptions.getAllFieldDefinitions()) {
			if ((fieldDef.getFlags() & (RPkgDescrFieldDefinition.R_DIST_ONLY | RPkgDescrFieldDefinition.BUILD_GENERATED)) != 0) {
				continue;
			}
			if (parameters.matchesNamePattern(fieldDef.getName())) {
				parameters.fieldDef= fieldDef;
				if (modelInfo != null && modelInfo.getField(fieldDef.getName()) != null) {
					parameters.baseRelevance= PRESENT_PRIO;
				}
				else if ((fieldDef.getFlags() & RPkgDescrFieldDefinition.REQUIRED) != 0
						|| COMMON_FIELDS.contains(fieldDef) ) {
					parameters.baseRelevance= COMMON_PRIO;
				}
				else {
					parameters.baseRelevance= 0;
				}
				
				proposals.add(new RPkgDescrFieldNameCompletionProposal(parameters));
			}
		}
	}
	
	public void computeContextProposals(final RPkgDescrAssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		if (context.getModelInfo() == null) {
			return;
		}
		
	}
	
	
}
