/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.NatTable;
import org.eclipse.statet.ecommons.waltable.copy.CopyToClipboardCommand;

import org.eclipse.statet.r.ui.dataeditor.RDataTableViewer;


@NonNullByDefault
public class CopyDataHandler extends AbstractHandler {
	
	
	private final RDataTableViewer tableViewer;
	
	
	public CopyDataHandler(final RDataTableViewer tableViewer) {
		this.tableViewer= tableViewer;
	}
	
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
//		final NatTable table= tableProvider.getNatTable();
//		setBaseEnabled(table != null && !table.isDisposed());
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final NatTable table= this.tableViewer.getTable();
		if (table != null) {
			table.doCommand(new CopyToClipboardCommand(", ", "\n", table.getConfigRegistry()));
		}
		return null;
	}
	
}
