/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.MessageUtils;

import org.eclipse.statet.r.core.pkgmanager.RView;


@NonNullByDefault
public class RViewLabelProvider extends CellLabelProvider {
	
	
	public RViewLabelProvider() {
	}
	
	
	@Override
	public void update(final ViewerCell cell) {
		final Object element= cell.getElement();
		if (element instanceof final RView view) {
			cell.setText(view.getName());
			return;
		}
		cell.setText(""); //$NON-NLS-1$
	}
	
	@Override
	public @Nullable String getToolTipText(final Object element) {
		if (element instanceof final RView view) {
			return MessageUtils.escapeForTooltip(view.getTopic());
		}
		return null;
	}
	
}
