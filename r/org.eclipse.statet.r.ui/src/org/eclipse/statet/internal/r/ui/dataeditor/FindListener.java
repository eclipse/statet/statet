/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface FindListener {
	
	
	final class FindEvent {
		
		public final IStatus status;
		
		public final long total;
		
		public final long rowIdx;
		
		public final long colIdx;
		
		
		public FindEvent(final IStatus status, final long total, final long rowIdx, final long colIdx) {
			this.status= status;
			this.total= total;
			this.rowIdx= rowIdx;
			this.colIdx= colIdx;
		}
		
	}
	
	
	void handleFindEvent(FindEvent event);
	
}
