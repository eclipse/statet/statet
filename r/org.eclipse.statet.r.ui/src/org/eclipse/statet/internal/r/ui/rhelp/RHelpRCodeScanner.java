/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import java.util.Map;
import java.util.Set;

import org.eclipse.jface.preference.IPreferenceStore;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.util.HtmlStripParserInput;

import org.eclipse.statet.ecommons.text.ui.settings.CssTextStyleManager;

import org.eclipse.statet.r.ui.RUIPreferenceConstants;
import org.eclipse.statet.r.ui.text.r.RDefaultTextStyleScanner;


@NonNullByDefault
public class RHelpRCodeScanner extends RDefaultTextStyleScanner {
	
	
	public RHelpRCodeScanner(final IPreferenceStore preferenceStore) {
		super(new CssTextStyleManager(preferenceStore,
				RUIPreferenceConstants.R.TS_GROUP_ID, RUIPreferenceConstants.R.TS_DEFAULT_ROOT ));
	}
	
	
	public void setHtml(final String html) {
		getLexer().reset(new HtmlStripParserInput(html).init());
	}
	
	
	public String getRootStyle() {
		final var textStyles= (CssTextStyleManager)getTextStyles();
		return textStyles.getRootStyleToken().getData();
	}
	
	@Override
	public void handleSettingsChanged(final Set<String> groupIds, final Map<String, Object> options) {
		final var textStyles= (CssTextStyleManager)getTextStyles();
		textStyles.handleSettingsChanged(groupIds, options);
		super.handleSettingsChanged(groupIds, options);
	}
	
}
