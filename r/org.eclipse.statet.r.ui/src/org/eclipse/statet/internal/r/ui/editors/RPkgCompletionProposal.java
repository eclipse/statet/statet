/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import org.eclipse.jface.text.contentassist.BoldStylerProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.viewers.ViewerLabelUtils;

import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.rj.renv.core.RPkgBuilt;


@NonNullByDefault
public class RPkgCompletionProposal extends RElementCompletionProposal {
	
	
	private final @Nullable RPkgBuilt pkgInfo;
	
	
	public RPkgCompletionProposal(final RElementProposalParameters parameters,
			final @Nullable RPkgBuilt pkgInfo) {
		super(parameters);
		
		this.pkgInfo= pkgInfo;
	}
	
	
	@Override
	protected int getMode() {
		return PACKAGE_NAME;
	}
	
	
	@Override
	protected String getName() {
		return getReplacementName().getSegmentName();
	}
	
	
	@Override
	public String getDisplayString() {
		return getReplacementName().getSegmentName();
	}
	
	@Override
	public StyledString computeStyledText() {
		final StyledString styledText= new StyledString(getDisplayString());
		if (this.pkgInfo != null) {
			styledText.append(QUALIFIER_SEPARATOR, StyledString.QUALIFIER_STYLER);
			styledText.append(this.pkgInfo.getTitle(), StyledString.QUALIFIER_STYLER);
		}
		return styledText;
	}
	
	@Override
	protected void styleMatchingRegions(final StyledString styledText,
			final int matchRule, final int[] matchingRegions,
			final BoldStylerProvider boldStylerProvider) {
		ViewerLabelUtils.setStyle(styledText, matchingRegions, boldStylerProvider.getBoldStyler());
	}
	
	@Override
	public Image getImage() {
		return RUI.getImage(RUI.IMG_OBJ_R_PACKAGE);
	}
	
}