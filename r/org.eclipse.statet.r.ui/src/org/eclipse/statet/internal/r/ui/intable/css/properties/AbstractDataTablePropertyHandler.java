/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable.css.properties;

import org.eclipse.e4.ui.css.core.dom.properties.ICSSPropertyHandler;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;

import org.w3c.dom.css.CSSValue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.intable.DataTable;


@NonNullByDefault
@SuppressWarnings("restriction")
public abstract class AbstractDataTablePropertyHandler implements ICSSPropertyHandler {
	
	
	protected static final boolean DEBUG= false;
	
	
	public AbstractDataTablePropertyHandler() {
	}
	
	
	@SuppressWarnings("unchecked")
	protected static <T> @Nullable T toSWT(final CSSValue value, final Class<T> valueType,
			final CSSEngine engine, final DataTable dataTable) throws Exception {
		return (T)engine.convert(value, valueType, dataTable.getDisplay());
	}
	
	
	protected static void print(final Object element, final @Nullable String pseudo,
			final String property, final CSSValue value) {
		final StringBuilder sb= new StringBuilder("CSSapply "); //$NON-NLS-1$
		sb.append(element);
		if (pseudo != null) {
			sb.append(':').append(pseudo);
		}
		sb.append(" { ").append(property).append(": "); //$NON-NLS-1$ //$NON-NLS-2$
		sb.append(value.getCssText());
		sb.append(" }"); //$NON-NLS-1$
		System.out.println(sb.toString());
	}
	
}
