/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable;

import org.eclipse.jface.preference.JFacePreferences;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.AbstractTextEditor;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.ui.swt.ColorUtils;
import org.eclipse.statet.ecommons.waltable.config.AbstractRegistryConfiguration;
import org.eclipse.statet.ecommons.waltable.config.CellConfigAttributes;
import org.eclipse.statet.ecommons.waltable.config.LayoutSizeConfig;
import org.eclipse.statet.ecommons.waltable.core.config.ConfigRegistry;
import org.eclipse.statet.ecommons.waltable.core.config.DisplayMode;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerCellPainter;
import org.eclipse.statet.ecommons.waltable.core.layer.LayerPainter;
import org.eclipse.statet.ecommons.waltable.core.style.BasicStyle;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle;
import org.eclipse.statet.ecommons.waltable.core.style.BorderStyle.LineStyle;
import org.eclipse.statet.ecommons.waltable.core.style.CellStyling;
import org.eclipse.statet.ecommons.waltable.core.style.GridStyling;
import org.eclipse.statet.ecommons.waltable.core.style.GridStyling.ConfigGridLineColorSupplier;
import org.eclipse.statet.ecommons.waltable.core.style.HorizontalAlignment;
import org.eclipse.statet.ecommons.waltable.core.style.Style;
import org.eclipse.statet.ecommons.waltable.core.style.VerticalAlignment;
import org.eclipse.statet.ecommons.waltable.core.swt.painters.GridLineCellLayerPainter;
import org.eclipse.statet.ecommons.waltable.freeze.IFreezeConfigAttributes;
import org.eclipse.statet.ecommons.waltable.grid.AlternatingRowLabelContributor;
import org.eclipse.statet.ecommons.waltable.grid.core.GridLabels;
import org.eclipse.statet.ecommons.waltable.grid.swt.CornerGridLineCellLayerPainter;
import org.eclipse.statet.ecommons.waltable.painter.cell.DiagCellPainter;
import org.eclipse.statet.ecommons.waltable.painter.cell.decorator.LineBorderDecorator;
import org.eclipse.statet.ecommons.waltable.sort.config.DefaultSortConfiguration;
import org.eclipse.statet.ecommons.waltable.sort.swt.SortableHeaderTextPainter;
import org.eclipse.statet.ecommons.waltable.style.SelectionStyleLabels;
import org.eclipse.statet.ecommons.workbench.ui.IWaThemeConstants;

import org.eclipse.statet.internal.r.ui.RUIPlugin;


public class PresentationConfig extends AbstractRegistryConfiguration implements Disposable,
		IPropertyChangeListener {
	
	
	private static PresentationConfig gInstance;
	
	public static PresentationConfig getInstance(final Display display) {
		if (gInstance == null) {
			gInstance= new PresentationConfig(display);
			RUIPlugin.getInstance().registerPluginDisposable(gInstance);
		}
		return gInstance;
	}
	
	
	private final Display display;
	
	private final Color headerGridColor;
	private final Color headerBackgroundColor;
	private final Color headerForegroundColor;
	
	private final Color bodyGridColor;
	private final Color bodyBackgroundColor;
	private final Color bodyEvenRowBackgroundColor;
	private final Color bodyOddRowBackgroundColor;
	private final Color bodyForegroundColor;
	
	private final Color headerSelectionBackgroundColor;
	private final Color headerSelectionForegroundColor;
	
	private final Color headerPlaceholderColor;
	
	private final Color headerFullSelectionBackgroundColor;
	private final Color headerFullSelectionForegroundColor;
	
	private final Color bodySelectionBackgroundColor;
	private final Color bodySelectionForegroundColor;
	
	private final Color bodyFreezeSeparatorColor;
	
	private final LayerPainter headerLayerPainter;
	private final LayerPainter headerLabelLayerPainter;
	
	private LayerCellPainter baseCellPainter;
	
	private LayerCellPainter headerCellPainter;
	private LayerCellPainter headerSortedCellPainter;
	private LayerCellPainter headerCornerCellPainter;
	
	private Font baseFont;
	private Font infoFont;
	
	private LayoutSizeConfig baseSizeConfig;
	
	private final CopyOnWriteIdentityListSet<Runnable> listeners= new CopyOnWriteIdentityListSet<>();
	
	
	public PresentationConfig(final Display display) {
		this.display= display;
		
		final ColorRegistry colorRegistry= JFaceResources.getColorRegistry();
		
		this.headerGridColor= display.getSystemColor(SWT.COLOR_DARK_GRAY);
		this.headerBackgroundColor= display.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND);
		this.headerForegroundColor= display.getSystemColor(SWT.COLOR_WIDGET_FOREGROUND);
		
		this.bodyGridColor= display.getSystemColor(SWT.COLOR_GRAY);
		this.bodyBackgroundColor= display.getSystemColor(SWT.COLOR_LIST_BACKGROUND);
		this.bodyEvenRowBackgroundColor= this.bodyBackgroundColor;
		this.bodyOddRowBackgroundColor= ColorUtils.blend(
				display.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW),
				this.bodyEvenRowBackgroundColor, 0.20f );
		
		this.bodyForegroundColor= display.getSystemColor(SWT.COLOR_LIST_FOREGROUND);
		
		this.headerSelectionBackgroundColor= ColorUtils.blend(
				display.getSystemColor(SWT.COLOR_LIST_SELECTION),
				this.headerBackgroundColor, 0.25f );
		this.headerSelectionForegroundColor= this.headerForegroundColor;
		
		this.headerPlaceholderColor= ColorUtils.blend(
				this.bodyGridColor,
				this.headerBackgroundColor, 0.25f );
		
		this.headerFullSelectionBackgroundColor= ColorUtils.blend(
				display.getSystemColor(SWT.COLOR_LIST_SELECTION),
				display.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW), 0.25f );
		this.headerFullSelectionForegroundColor= display.getSystemColor(SWT.COLOR_WIDGET_HIGHLIGHT_SHADOW);
		
		this.bodySelectionBackgroundColor= getSelectionBackgroundColor();
		this.bodySelectionForegroundColor= display.getSystemColor(SWT.COLOR_LIST_SELECTION_TEXT);
		
		this.bodyFreezeSeparatorColor= colorRegistry.get(JFacePreferences.DECORATIONS_COLOR);
		
		this.headerLayerPainter= new GridLineCellLayerPainter(
				new ConfigGridLineColorSupplier(GridLabels.COLUMN_HEADER) );
		this.headerLabelLayerPainter= new CornerGridLineCellLayerPainter(
				new ConfigGridLineColorSupplier(GridLabels.COLUMN_HEADER) );
		
		updateFonts();
		updateCellPainters();
		
		final FontRegistry fontRegistry= JFaceResources.getFontRegistry();
		fontRegistry.addListener(this);
	}
	
	private Color getSelectionBackgroundColor() {
		final var store= EditorsUI.getPreferenceStore();
		if (!store.getBoolean(AbstractTextEditor.PREFERENCE_COLOR_SELECTION_BACKGROUND_SYSTEM_DEFAULT)) {
			final RGB rgb= PreferenceConverter.getColor(store, AbstractTextEditor.PREFERENCE_COLOR_SELECTION_BACKGROUND);
			if (rgb != null) {
				return new Color(rgb);
			}
		}
		return this.display.getSystemColor(SWT.COLOR_LIST_SELECTION);
	}
	
	@Override
	public void dispose() {
		final FontRegistry fontRegistry= JFaceResources.getFontRegistry();
		if (fontRegistry != null) {
			fontRegistry.removeListener(this);
		}
	}
	
	
	private void updateFonts() {
		final GC gc= new GC(this.display);
		try {
			final FontRegistry fontRegistry= JFaceResources.getFontRegistry();
			this.baseFont= fontRegistry.get(IWaThemeConstants.TABLE_FONT);
			this.infoFont= fontRegistry.getItalic(IWaThemeConstants.TABLE_FONT);
			
			gc.setFont(this.baseFont);
			final FontMetrics fontMetrics= gc.getFontMetrics();
			final int textHeight= fontMetrics.getHeight();
			final int charWidth= (gc.textExtent("1234567890.-120").x + 5) / 15;
			final int textSpace= 3;
			
			this.baseSizeConfig= new LayoutSizeConfig(textSpace, textHeight, charWidth);
		}
		finally {
			gc.dispose();
		}
	}
	
	private void updateCellPainters() {
		this.baseCellPainter= new LineBorderDecorator(
				new RTextPainter(this.baseSizeConfig.getDefaultSpace()) );
		
		this.headerCellPainter= new RTextPainter(this.baseSizeConfig.getDefaultSpace());
		this.headerSortedCellPainter= new SortableHeaderTextPainter(
				new RTextPainter(this.baseSizeConfig.getDefaultSpace()), true, true );
		this.headerCornerCellPainter= new DiagCellPainter(this.headerGridColor);
	}
	
	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		switch (event.getProperty()) {
		case IWaThemeConstants.TABLE_FONT:
			updateFonts();
			updateCellPainters();
			notifyListeners();
			return;
		default:
			return;
		}
	}
	
	public LayerPainter getHeaderLayerPainter() {
		return this.headerLayerPainter;
	}
	
	public LayerPainter getHeaderLabelLayerPainter() {
		return this.headerLabelLayerPainter;
	}
	
	public LayoutSizeConfig getBaseSizeConfig() {
		return this.baseSizeConfig;
	}
	
	
	@Override
	public void configureRegistry(final ConfigRegistry configRegistry) {
		// base
		{	final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.bodyBackgroundColor);
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.bodyForegroundColor);
			cellStyle.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, HorizontalAlignment.RIGHT);
			cellStyle.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, VerticalAlignment.MIDDLE);
			cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, null);
			cellStyle.setAttributeValue(CellStyling.FONT, this.baseFont);
			cellStyle.setAttributeValue(CellStyling.CONTROL_FONT, this.infoFont);
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.baseCellPainter);
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle);
			configRegistry.registerAttribute(GridStyling.GRID_LINE_COLOR, this.bodyGridColor);
			
			configRegistry.registerAttribute(LayoutSizeConfig.CONFIG, this.baseSizeConfig);
		}
		
		// headers
		{	// column header
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.headerBackgroundColor);
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.headerForegroundColor);
			cellStyle.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, HorizontalAlignment.CENTER);
			cellStyle.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, VerticalAlignment.MIDDLE);
			cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, null);
			cellStyle.setAttributeValue(CellStyling.FONT, this.baseFont);
			cellStyle.setAttributeValue(CellStyling.CONTROL_FONT, this.infoFont);
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, GridLabels.COLUMN_HEADER );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.headerCellPainter,
					DisplayMode.NORMAL, GridLabels.COLUMN_HEADER );
			configRegistry.registerAttribute(GridStyling.GRID_LINE_COLOR, this.headerGridColor,
					DisplayMode.NORMAL, GridLabels.COLUMN_HEADER );
		}
		{	// column header label
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.headerBackgroundColor);
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.headerForegroundColor);
			cellStyle.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, HorizontalAlignment.RIGHT);
			cellStyle.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, VerticalAlignment.MIDDLE);
			cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, null);
			cellStyle.setAttributeValue(CellStyling.FONT, this.baseFont);
			cellStyle.setAttributeValue(CellStyling.CONTROL_FONT, this.infoFont);
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, GridLabels.COLUMN_HEADER_LABEL );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.headerCellPainter,
					DisplayMode.NORMAL, GridLabels.COLUMN_HEADER_LABEL );
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, GridLabels.CORNER );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.headerCornerCellPainter,
					DisplayMode.NORMAL, GridLabels.CORNER );
		}
		{	// row header
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.headerBackgroundColor);
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.headerForegroundColor);
			cellStyle.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, HorizontalAlignment.RIGHT);
			cellStyle.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, VerticalAlignment.MIDDLE);
			cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, null);
			cellStyle.setAttributeValue(CellStyling.FONT, this.baseFont);
			cellStyle.setAttributeValue(CellStyling.CONTROL_FONT, this.infoFont);
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, GridLabels.ROW_HEADER );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.headerCellPainter,
					DisplayMode.NORMAL, GridLabels.ROW_HEADER );
		}
		{	// row header
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.headerBackgroundColor);
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.headerForegroundColor);
			cellStyle.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, HorizontalAlignment.LEFT);
			cellStyle.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, VerticalAlignment.MIDDLE);
			cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, null);
			cellStyle.setAttributeValue(CellStyling.FONT, this.baseFont);
			cellStyle.setAttributeValue(CellStyling.CONTROL_FONT, this.infoFont);
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, GridLabels.ROW_HEADER_LABEL );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER, this.headerCellPainter,
					DisplayMode.NORMAL, GridLabels.ROW_HEADER_LABEL );
		}
		{	// placeholder header
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.headerPlaceholderColor);
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.headerForegroundColor);
			cellStyle.setAttributeValue(CellStyling.HORIZONTAL_ALIGNMENT, HorizontalAlignment.RIGHT);
			cellStyle.setAttributeValue(CellStyling.VERTICAL_ALIGNMENT, VerticalAlignment.MIDDLE);
			cellStyle.setAttributeValue(CellStyling.BORDER_STYLE, null);
			cellStyle.setAttributeValue(CellStyling.FONT, this.baseFont);
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, GridLabels.HEADER_PLACEHOLDER );
		}
		
		// alternating rows
		{	// body even row
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.bodyEvenRowBackgroundColor);
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, AlternatingRowLabelContributor.EVEN_ROW_CONFIG_TYPE );
		}
		{	// body odd row
			final Style cellStyle= new BasicStyle();
			
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.bodyOddRowBackgroundColor);
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, AlternatingRowLabelContributor.ODD_ROW_CONFIG_TYPE );
		}
		
		// selection
		{	// body selected cell
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR, this.bodySelectionBackgroundColor);
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR, this.bodySelectionForegroundColor);
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.SELECTED );
		}
		{	// body selection anchor
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.BORDER_STYLE,
					new BorderStyle(2, this.bodyForegroundColor, LineStyle.SOLID, -1) );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.NORMAL, SelectionStyleLabels.SELECTION_ANCHOR_STYLE );
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.SELECTED, SelectionStyleLabels.SELECTION_ANCHOR_STYLE );
		}
		{	// header with selection
			final Style cellStyle= new BasicStyle();
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR,
					this.headerSelectionForegroundColor );
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR,
					this.headerSelectionBackgroundColor );
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.SELECTED, GridLabels.COLUMN_HEADER );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.SELECTED, GridLabels.CORNER );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.SELECTED, GridLabels.ROW_HEADER );
		}
		{	// header fully selected
			final Style cellStyle= new BasicStyle();
			
			cellStyle.setAttributeValue(CellStyling.FOREGROUND_COLOR,
					this.headerFullSelectionForegroundColor );
			cellStyle.setAttributeValue(CellStyling.BACKGROUND_COLOR,
					this.headerFullSelectionBackgroundColor );
			
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.SELECTED, SelectionStyleLabels.COLUMN_FULLY_SELECTED_STYLE );
			configRegistry.registerAttribute(CellConfigAttributes.CELL_STYLE, cellStyle,
					DisplayMode.SELECTED, SelectionStyleLabels.ROW_FULLY_SELECTED_STYLE );
		}
		
		// sorting
		{	// header column sorted
			configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER,
					this.headerSortedCellPainter,
					DisplayMode.NORMAL, DefaultSortConfiguration.SORT_DOWN_CONFIG_TYPE);
			configRegistry.registerAttribute(CellConfigAttributes.CELL_PAINTER,
					this.headerSortedCellPainter,
					DisplayMode.NORMAL, DefaultSortConfiguration.SORT_UP_CONFIG_TYPE);
		}
		
		// freezing
		{	// body freezed
			configRegistry.registerAttribute(IFreezeConfigAttributes.SEPARATOR_COLOR,
					this.bodyFreezeSeparatorColor );
		}
	}
	
	
	public void addListener(final Runnable listener) {
		this.listeners.add(listener);
	}
	
	public void removeListener(final Runnable listener) {
		this.listeners.remove(listener);
	}
	
	private void notifyListeners() {
		for (final Runnable listener : this.listeners) {
			listener.run();
		}
	}
	
}
