/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.search;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.text.AbstractTextSearchResult;
import org.eclipse.search.ui.text.IEditorMatchAdapter;
import org.eclipse.search.ui.text.IFileMatchAdapter;
import org.eclipse.search.ui.text.Match;
import org.eclipse.ui.IEditorPart;

import org.eclipse.statet.ecommons.workbench.search.ui.ElementMatchComparator;
import org.eclipse.statet.ecommons.workbench.search.ui.ExtTextSearchResult;

import org.eclipse.statet.ltk.core.IExtContentTypeManager;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.ModelTypeDescriptor;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.util.LtkModelElementComparator;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.RSymbolComparator;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.ui.RUI;


public class RElementSearchResult extends ExtTextSearchResult<RSourceUnit, RElementMatch> 
		implements IFileMatchAdapter, IEditorMatchAdapter {
	
	
	private static SourceUnit getSourceUnit(final IEditorPart editor) {
		final SourceEditor sourceEditor= editor.getAdapter(SourceEditor.class);
		if (sourceEditor != null) {
			return sourceEditor.getSourceUnit();
		}
		return null;
	}
	
	
	public static final ElementMatchComparator<RSourceUnit, RElementMatch> COMPARATOR= new ElementMatchComparator<>(
			new RSourceUnit[0], new LtkModelElementComparator(RSymbolComparator.R_NAMES_COLLATOR),
			new RElementMatch[0], new DefaultMatchComparator<>() );
	
	
	private final RElementSearchQuery query;
	
	
	public RElementSearchResult(final RElementSearchQuery query) {
		super(COMPARATOR);
		
		this.query= query;
	}
	
	
	@Override
	public ImageDescriptor getImageDescriptor() {
		return RUI.getImageDescriptor(RUI.IMG_OBJ_R_SCRIPT);
	}
	
	@Override
	public String getLabel() {
		final StringBuilder sb= new StringBuilder(this.query.getSearchLabel());
		
		sb.append(" - "); //$NON-NLS-1$
		
		{	final int count= getMatchCount();
			sb.append(count);
			sb.append(' ');
			sb.append(this.query.getMatchLabel(count));
			sb.append(' ');
			sb.append("in");
			sb.append(' ');
			sb.append(this.query.getScopeLabel());
		}
		
		return sb.toString();
	}
	
	@Override
	public String getTooltip() {
		return getLabel();
	}
	
	@Override
	public ISearchQuery getQuery() {
		return this.query;
	}
	
	
	@Override
	public IFileMatchAdapter getFileMatchAdapter() {
		return this;
	}
	
	@Override
	public IFile getFile(Object element) {
		if (element instanceof SourceElement) {
			element= ((SourceElement)element).getSourceUnit();
		}
		if (element instanceof SourceUnit) {
			element= ((SourceUnit)element).getResource();
		}
		if (element instanceof IFile) {
			return (IFile) element;
		}
		return null;
	}
	
	@Override
	public Match[] computeContainedMatches(final AbstractTextSearchResult result,
			final IFile file) {
		final SourceUnit sourceUnit= LtkModels.getSourceUnitManager().getOpenSourceUnit(Ltk.PERSISTENCE_CONTEXT, file);
		if (sourceUnit != null) {
			final IExtContentTypeManager typeManager= Ltk.getExtContentTypeManager();
			final ModelTypeDescriptor modelType= typeManager.getModelType(sourceUnit.getModelTypeId());
			if (modelType != null && (modelType.getId() == RModel.R_TYPE_ID
					|| modelType.getSecondaryTypeIds().contains(RModel.R_TYPE_ID) )) {
				return getMatches(sourceUnit);
			}
		}
		return getComparator().getMatch0();
	}
	
	@Override
	public IEditorMatchAdapter getEditorMatchAdapter() {
		return this;
	}
	
	@Override
	public boolean isShownInEditor(final Match match, final IEditorPart editor) {
		SourceUnit su= getSourceUnit(editor);
		while (su != null) {
			if (su.equals(match.getElement())) {
				return true;
			}
			su= su.getUnderlyingUnit();
		}
		return false;
	}
	
	@Override
	public Match[] computeContainedMatches(final AbstractTextSearchResult result,
			final IEditorPart editor) {
		SourceUnit su= getSourceUnit(editor);
		while (su != null) {
			final RElementMatch[] matches= getMatches(su);
			if (matches.length > 0) {
				return matches;
			}
			su= su.getUnderlyingUnit();
		}
		return getComparator().getMatch0();
	}
	
}
