/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.r.ui.correction.ChangeIfElseAssistProposal;
import org.eclipse.statet.internal.r.ui.correction.ConvertFCallToPipeForwardAssistProposal;
import org.eclipse.statet.internal.r.ui.correction.RLinkedNamesAssistProposal;
import org.eclipse.statet.internal.r.ui.correction.RenameInRegionAssistProposal;
import org.eclipse.statet.internal.r.ui.correction.RenameInWorkspaceAssistProposal;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistComputer;
import org.eclipse.statet.r.core.model.RCoreFunctions;
import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.core.model.RModelUtils;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RWorkspaceSourceUnit;
import org.eclipse.statet.r.core.model.rlang.RCompositeSrcStrElement;
import org.eclipse.statet.r.core.model.rlang.RLangSrcStrElement;
import org.eclipse.statet.r.core.refactoring.FCallToPipeForwardRefactoring;
import org.eclipse.statet.r.core.refactoring.IfElseInvertRefactoring;
import org.eclipse.statet.r.core.refactoring.IfNotNullElseToCRefactoring;
import org.eclipse.statet.r.core.refactoring.IfNotNullElseToSpecialRefactoring;
import org.eclipse.statet.r.core.source.RSourceConstants;
import org.eclipse.statet.r.core.source.ast.CIfElse;
import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.Special;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class RQuickRefactoringComputer implements QuickAssistComputer {
	
	
	protected static final byte IN_DEFAULT= 1;
	protected static final byte IN_STRING= 2;
	
	
	private RAssistInvocationContext rContext= nonNullLateInit();
	protected byte in;
	
	protected AssistProposalCollector proposals= nonNullLateInit();
	
	
	public RQuickRefactoringComputer() {
	}
	
	
	protected void initR(final RAssistInvocationContext context,
			final IProgressMonitor monitor) {
		this.rContext= context;
		this.in= (context.getInvocationContentType() == RDocumentConstants.R_STRING_CONTENT_TYPE) ? IN_STRING : IN_DEFAULT;
	}
	
	@SuppressWarnings("null")
	protected void clear() {
		this.rContext= null;
		this.proposals= null;
	}
	
	
	protected final RAssistInvocationContext getRContext() {
		return this.rContext;
	}
	
	
	@Override
	public void computeAssistProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		try {
			this.proposals= proposals;
			if (context instanceof RAssistInvocationContext) {
				initR((RAssistInvocationContext)context, monitor);
				computeAssistProposals(monitor);
			}
		}
		finally {
			clear();
		}
	}
	
	public void computeAssistProposals(final IProgressMonitor monitor) {
		final RAstNode node= getRContext().getSelectionRAstNode();
		if (node == null) {
			return;
		}
		
		if (node.getNodeType() == NodeType.SYMBOL || node.getNodeType() == NodeType.STRING_CONST) {
			RAstNode candidate= node;
			SEARCH_ACCESS : while (candidate != null) {
				final List<Object> attachments= candidate.getAttachments();
				for (final Object attachment : attachments) {
					if (attachment instanceof RElementAccess access) {
						SUB: while (access != null) {
							if (access.getSegmentName() == null) {
								break SUB;
							}
							if (access.getNameNode() == node) {
								addAccessAssistProposals(access);
								break SEARCH_ACCESS;
							}
							access= access.getNextSegment();
						}
					}
				}
				candidate= candidate.getRParent();
			}
		}
		else if (getRContext().getLength() > 0 && getRContext().getSourceUnit() != null) {
			this.proposals.add(new RenameInRegionAssistProposal(getRContext()));
		}
		
		addFCallProposals(node);
		addFCallArgProposals(node);
		addIfElseProposals(node);
		addSpecialProposals(node);
	}
	
	protected void addAccessAssistProposals(final RElementAccess access) {
		final RAssistInvocationContext context= getRContext();
		final RAstNode accessNameNode= access.getNameNode();
		if (accessNameNode == null) {
			return;
		}
		final var allAccess= ImCollections.toIdentityList(access.getAllInUnit(false));
		final int current= allAccess.indexOf(access);
		if (current < 0) {
			return;
		}
		
		this.proposals.add(new RLinkedNamesAssistProposal(RLinkedNamesAssistProposal.IN_FILE,
				context, access ));
		
		if (allAccess.size() > 1) {
			this.proposals.add(new RLinkedNamesAssistProposal(RLinkedNamesAssistProposal.IN_FILE_PRECEDING,
					context, access ));
			this.proposals.add(new RLinkedNamesAssistProposal(RLinkedNamesAssistProposal.IN_FILE_FOLLOWING,
					context, access ));
			
			final TextRegion rChunk= getRChunk(context, accessNameNode);
			if (rChunk != null) {
				int chunkBegin= 0;
				for (final int offset= rChunk.getStartOffset();
						chunkBegin < current; chunkBegin++) {
					final RAstNode nameNode= allAccess.get(chunkBegin).getNameNode();
					if (nameNode != null && offset <= nameNode.getStartOffset()) {
						break;
					}
				}
				int chunkEnd= current + 1;
				for (final int offset= rChunk.getEndOffset();
						chunkEnd < allAccess.size(); chunkEnd++) {
					final RAstNode nameNode= allAccess.get(chunkEnd).getNameNode();
					if (nameNode != null && offset <= nameNode.getStartOffset()) {
						break;
					}
				}
				if (chunkEnd - chunkBegin > 1) {
					this.proposals.add(new RLinkedNamesAssistProposal(RLinkedNamesAssistProposal.IN_CHUNK,
							context, access, rChunk ));
				}
			}
		}
		
		if (context.getSourceUnit() instanceof RWorkspaceSourceUnit) {
			this.proposals.add(new RenameInWorkspaceAssistProposal(context, accessNameNode));
		}
	}
	
	protected @Nullable TextRegion getRChunk(final RAssistInvocationContext context,
			final RAstNode accessNameNode) {
		final var modelInfo= context.getModelInfo();
		if (modelInfo != null) {
			final var sourceElement= modelInfo.getSourceElement();
			if (sourceElement instanceof RCompositeSrcStrElement) {
				final var elements= ((RCompositeSrcStrElement)sourceElement).getCompositeElements();
				final RLangSrcStrElement element= LtkModelUtils.getCoveringSourceElement(
						elements, accessNameNode.getStartOffset() );
				if (element != null) {
					return element.getSourceRange();
				}
			}
		}
		return null;
	}
	
	protected void addFCallProposals(final RAstNode node) {
		final RSourceUnit sourceUnit= getRContext().getSourceUnit();
		final FCall fCall= findFCall(node);
		if (sourceUnit == null || fCall == null) {
			return;
		}
		final var rSourceConfig= getRContext().getRCoreAccess().getRSourceConfig();
		final FCall.Args args= fCall.getArgsChild();
		if (rSourceConfig.getLangVersion().compareTo(RSourceConstants.LANG_VERSION_4_1) >= 0
				&& args.getChildCount() > 0 ) {
			
			final var refactoring= new FCallToPipeForwardRefactoring(sourceUnit, fCall);
			final RefactoringStatus status= refactoring.checkInitialConditions(null);
			if (!status.hasFatalError()) {
				this.proposals.add(new ConvertFCallToPipeForwardAssistProposal(getRContext(), refactoring));
			}
		}
	}
	
	protected void addFCallArgProposals(final RAstNode node) {
		final RSourceUnit sourceUnit= getRContext().getSourceUnit();
		final FCall.Arg arg= findFCallArg(node, getRContext());
		final FCall fCall;
		if (sourceUnit == null || arg == null || (fCall= arg.getFCall()) == null) {
			return;
		}
		final var rSourceConfig= getRContext().getRCoreAccess().getRSourceConfig();
		if (rSourceConfig.getLangVersion().compareTo(RSourceConstants.LANG_VERSION_4_2) >= 0
				&& arg.hasName() ) {
			
			final var refactoring= new FCallToPipeForwardRefactoring(sourceUnit, fCall, arg);
			final RefactoringStatus status= refactoring.checkInitialConditions(null);
			if (!status.hasFatalError()) {
				this.proposals.add(new ConvertFCallToPipeForwardAssistProposal(getRContext(), refactoring));
			}
		}
	}
	
	protected void addIfElseProposals(final RAstNode node) {
		final RSourceUnit sourceUnit= getRContext().getSourceUnit();
		final CIfElse cIfElse= findIf(node, getRContext());
		if (sourceUnit == null || cIfElse == null) {
			return;
		}
		final var rSourceConfig= getRContext().getRCoreAccess().getRSourceConfig();
		RAstNode condNode= cIfElse.getCondChild();
		boolean not= false;
		if (condNode.getNodeType() == NodeType.NOT) {
			not= true;
			condNode= condNode.getChild(0);
		}
		if (cIfElse.hasElse()) {
			final var refactoring= new IfElseInvertRefactoring(sourceUnit, cIfElse);
			final RefactoringStatus status= refactoring.checkInitialConditions(null);
			if (!status.hasFatalError()) {
				this.proposals.add(new ChangeIfElseAssistProposal(getRContext(), refactoring));
			}
		}
		if (condNode.getNodeType() == NodeType.F_CALL) {
			final FCall fCall= (FCall)condNode;
			if (RModelUtils.matches(fCall, RCoreFunctions.BASE_IsNull_ELEMENT_NAME)
					&& rSourceConfig.getLangVersion().compareTo(RSourceConstants.LANG_VERSION_4_4) >= 0
					&& cIfElse.hasElse() ) {
				
				final var refactoring= new IfNotNullElseToSpecialRefactoring(sourceUnit, cIfElse);
				final RefactoringStatus status= refactoring.checkInitialConditions(null);
				if (!status.hasFatalError()) {
					this.proposals.add(new ChangeIfElseAssistProposal(getRContext(), refactoring));
				}
			}
		}
	}
	
	protected void addSpecialProposals(final RAstNode node) {
		final RSourceUnit sourceUnit= getRContext().getSourceUnit();
		final Special special= findSpecial(node, getRContext());
		if (sourceUnit == null || special == null) {
			return;
		}
		if (special.getText() == "||"
				&& getRContext().getEndOffset() < special.getRightChild().getStartOffset()) {
			
			{	final var refactoring= new IfNotNullElseToCRefactoring(sourceUnit, special, false);
				final RefactoringStatus status= refactoring.checkInitialConditions(null);
				if (!status.hasFatalError()) {
					this.proposals.add(new ChangeIfElseAssistProposal(getRContext(), refactoring));
				}
			}
			if (special.getLeftChild().getNodeType() != NodeType.BLOCK || special.getRightChild().getNodeType() != NodeType.BLOCK) {
				final var refactoring= new IfNotNullElseToCRefactoring(sourceUnit, special, true);
				final RefactoringStatus status= refactoring.checkInitialConditions(null);
				if (!status.hasFatalError()) {
					this.proposals.add(new ChangeIfElseAssistProposal(getRContext(), refactoring));
				}
			}
		}
	}
	
	
	private @Nullable FCall findFCall(@Nullable RAstNode node) {
		while (true) {
			if (node == null) {
				return null;
			}
			switch (node.getNodeType()) {
			case F_CALL:
				return (FCall)node;
			default:
				node= node.getRParent();
				continue;
			case SOURCELINES:
			case COMMENT:
			case ERROR, ERROR_TERM, DUMMY:
			case PIPE_FORWARD:
			case F_CALL_ARGS, F_CALL_ARG:
			case BLOCK:
				return null;
			}
		}
	}
	
	private FCall. @Nullable Arg findFCallArg(@Nullable RAstNode node, final TextRegion region) {
		while (true) {
			if (node == null) {
				return null;
			}
			switch (node.getNodeType()) {
			case F_CALL_ARG:
				return (FCall.Arg)node;
			case STRING_CONST:
			case SYMBOL:
				node= node.getRParent();
				continue;
			default:
				if (node.getStartOffset() == region.getStartOffset()
						|| node.getEndOffset() == region.getEndOffset() ) {
					node= node.getRParent();
					continue;
				}
				//$FALL-THROUGH$
			case SOURCELINES:
			case COMMENT:
			case ERROR, ERROR_TERM, DUMMY:
			case PIPE_FORWARD:
			case F_CALL, F_CALL_ARGS:
			case BLOCK:
				return null;
			}
		}
	}
	
	private @Nullable CIfElse findIf(@Nullable RAstNode node, final TextRegion region) {
		while (true) {
			if (node == null) {
				return null;
			}
			switch (node.getNodeType()) {
			case C_IF:
				final CIfElse c= (CIfElse)node;
				if (c.contains(region)
						&& (region.getStartOffset() < c.getThenChild().getStartOffset()
								|| (c.hasElse() && region.getStartOffset() >= c.getElseOffset() && region.getStartOffset() <= c.getElseOffset() + 4) )) {
					return c;
				}
				return null;
			default:
				node= node.getRParent();
				continue;
			case SOURCELINES:
			case COMMENT:
			case ERROR, ERROR_TERM, DUMMY:
			case C_FOR, C_WHILE, C_REPEAT:
			case BLOCK:
				return null;
			}
		}
	}
	
	private @Nullable Special findSpecial(@Nullable RAstNode node, final TextRegion region) {
		while (true) {
			if (node == null) {
				return null;
			}
			switch (node.getNodeType()) {
			case SPECIAL:
				final Special special= (Special)node;
				return special;
			default:
				node= node.getRParent();
				continue;
			case SOURCELINES:
			case COMMENT:
			case ERROR, ERROR_TERM, DUMMY:
			case C_IF, C_FOR, C_WHILE, C_REPEAT:
			case BLOCK:
				return null;
			}
		}
	}
	
	
}
