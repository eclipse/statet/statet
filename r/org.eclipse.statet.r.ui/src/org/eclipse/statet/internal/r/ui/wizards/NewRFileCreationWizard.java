/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;

import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.text.TextUtil;
import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.ui.templates.TemplateUtils.EvaluatedTemplate;
import org.eclipse.statet.ltk.ui.wizards.NewElementWizard;
import org.eclipse.statet.ltk.ui.wizards.NewElementWizardPage.ResourceGroup;
import org.eclipse.statet.r.codegeneration.CodeGeneration;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.ui.RUI;


public class NewRFileCreationWizard extends NewElementWizard {
	
	
	private static class NewRFile extends NewFile {
		
		public NewRFile(final IPath containerPath, final String resourceName) {
			super(containerPath, resourceName, RCore.R_CONTENT_TYPE);
		}
		
		@Override
		protected String getInitialFileContent(final IFile newFileHandle,
				final SubMonitor m) throws StatusException {
			final String lineDelimiter= TextUtil.getLineDelimiter(newFileHandle.getProject());
			final RSourceUnit sourceUnit= (RSourceUnit)LtkModels.getSourceUnitManager().getSourceUnit(
					RModel.R_TYPE_ID, Ltk.PERSISTENCE_CONTEXT, newFileHandle,
					m );
			try {
				final EvaluatedTemplate data= CodeGeneration.getNewRFileContent(sourceUnit, lineDelimiter);
				if (data != null) {
					this.initialSelection= data.getRegionToSelect();
					return data.getContent();
				}
				return null;
			}
			finally {
				sourceUnit.disconnect(m);
			}
		}
		
	}
	
	
	private NewRFileCreationWizardPage firstPage;
	
	private NewFile newRFile;
	
	
	public NewRFileCreationWizard() {
		setDialogSettings(DialogUtils.getDialogSettings(RUIPlugin.getInstance(), "NewElementWizard")); //$NON-NLS-1$
		setDefaultPageImageDescriptor(RUI.getImageDescriptor(RUIPlugin.IMG_WIZBAN_NEWRFILE));
		setWindowTitle(Messages.NewRScriptFileWizard_title);
	}
	
	@Override
	public void addPages() {
		super.addPages();
		this.firstPage= new NewRFileCreationWizardPage(getSelection());
		addPage(this.firstPage);
	}
	
	@Override
	protected ISchedulingRule getSchedulingRule() {
		final ISchedulingRule rule= createRule(this.newRFile.getResource());
		if (rule != null) {
			return rule;
		}
		
		return super.getSchedulingRule();
	}
	
	@Override
	public boolean performFinish() {
		// befor super, so it can be used in getSchedulingRule
		final ResourceGroup resourceGroup= this.firstPage.getResourceGroup();
		this.newRFile= new NewRFile(
				resourceGroup.getContainerFullPath(),
				resourceGroup.getResourceName() );
		
		final boolean result= super.performFinish();
		
		if (result && this.newRFile.getResource() != null) {
			// select and open file
			selectAndReveal(this.newRFile.getResource());
			openResource(this.newRFile);
		}
		
		return result;
	}
	
	@Override
	protected void performOperations(final IProgressMonitor monitor) throws InterruptedException, CoreException, InvocationTargetException {
		final SubMonitor m= SubMonitor.convert(monitor, "Create new R file...", 10 + 1);
		
		this.newRFile.createFile(m.newChild(10));
		
		this.firstPage.saveSettings();
		m.worked(1);
	}
	
}
