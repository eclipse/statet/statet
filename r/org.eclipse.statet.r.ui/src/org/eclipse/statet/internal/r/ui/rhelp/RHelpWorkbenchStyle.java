/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.preference.JFacePreferences;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.editors.text.EditorsPlugin;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.swt.ColorUtils;
import org.eclipse.statet.ecommons.ui.workbench.css.StylingUtils;


@NonNullByDefault
public class RHelpWorkbenchStyle {
	
	
	static final String DOC_BACKGROUND_COLOR_KEY= "org.eclipse.statet.workbench.themes.DocViewBackgroundColor"; //$NON-NLS-1$
	static final String DOC_FOREGROUND_COLOR_KEY= "org.eclipse.statet.workbench.themes.DocViewColor"; //$NON-NLS-1$
	static final String DOC_FONT_KEY= "org.eclipse.statet.workbench.themes.DocViewFont"; //$NON-NLS-1$
	
	
	static Color getDocBackgroundColor() {
		return nonNullAssert(JFaceResources.getColorRegistry().get(DOC_BACKGROUND_COLOR_KEY));
	}
	
	static Color getDocForegroundColor() {
		return nonNullAssert(JFaceResources.getColorRegistry().get(DOC_FOREGROUND_COLOR_KEY));
	}
	
	
	static StringBuilder appendCssColor(final StringBuilder sb, final RGB color) {
		sb.append('#');
		String s= Integer.toHexString(color.red);
		if (s.length() == 1) {
			sb.append('0');
		}
		sb.append(s);
		s= Integer.toHexString(color.green);
		if (s.length() == 1) {
			sb.append('0');
		}
		sb.append(s);
		s= Integer.toHexString(color.blue);
		if (s.length() == 1) {
			sb.append('0');
		}
		sb.append(s);
		return sb;
	}
	
	
	private static final String[] DEFAULT_MATCH_COLORS= new String[] {
		".SMATCH-A { background: #ceccf7; }\n", //$NON-NLS-1$
		".SMATCH-B { background: #ffffcf; }\n", //$NON-NLS-1$
		".SMATCH-C { background: aquamarine; }\n", //$NON-NLS-1$
		".SMATCH-D { background: palegreen; }\n", //$NON-NLS-1$
		".SMATCH-E { background: coral; }\n", //$NON-NLS-1$
		".SMATCH-F { background: wheat; }\n", //$NON-NLS-1$
		".SMATCH-G { background: khaki; }\n", //$NON-NLS-1$
		".SMATCH-H { background: lime; }\n", //$NON-NLS-1$
		".SMATCH-I { background: deepskyblue; }\n", //$NON-NLS-1$
		".SMATCH-J { background: plum; }\n", //$NON-NLS-1$
	};
	
	
	private final boolean isThemeStylingRequired;
	
	private final FontDescriptor dialogFontDescr;
	
	private RGB dialogBackgroundColor;
	private RGB dialogForegroundColor;
	private RGB buttonBackgroundColor;
	private RGB buttonForegroundColor;
	private RGB borderColor;
	private RGB selectionBackgroundColor;
	
	private RGB scrollbarBackgroundColor;
	private RGB scrollbarForegroundColor;
	private RGB scrollbarArrowColor;
	
	private final FontDescriptor docFontDescr;
	
	private final RGB docBackgroundColor;
	private final RGB docForegroundColor;
	
	
	public RHelpWorkbenchStyle() {
		final Display display= nonNullAssert(Display.getCurrent());
		this.isThemeStylingRequired= (StylingUtils.isStylingSupported()
				&& !StylingUtils.isCurrentThemeMatchingSystem(display) );
		
		this.dialogFontDescr= nonNullAssert(JFaceResources.getDialogFontDescriptor());
		
		final var shell= new Shell(display);
		try {
			final var composite= new ScrolledComposite(shell, SWT.V_SCROLL);
			final var button= new Button(composite, SWT.PUSH);
			if (this.isThemeStylingRequired) {
				final var stylingEngine= StylingUtils.getStylingEngine();
				stylingEngine.style(composite);
			}
			this.dialogBackgroundColor= composite.getBackground().getRGB();
			this.dialogForegroundColor= composite.getForeground().getRGB();
			this.buttonBackgroundColor= button.getBackground().getRGB();
			this.buttonForegroundColor= button.getForeground().getRGB();
			this.borderColor= display.getSystemColor(SWT.COLOR_WIDGET_BORDER).getRGB();
			this.selectionBackgroundColor= display.getSystemColor(SWT.COLOR_LIST_SELECTION).getRGB();
		}
		finally {
			shell.dispose();
		}
		
		if (this.isThemeStylingRequired) {
			switch (StylingUtils.getCurrentThemeType(display)) {
			case DARK:
				this.scrollbarBackgroundColor= new RGB(23, 23, 23);
				this.scrollbarForegroundColor= new RGB(77, 77, 77);
				this.scrollbarArrowColor= new RGB(103, 103, 103);
				break;
			default:
				this.scrollbarBackgroundColor= this.dialogBackgroundColor;
				this.scrollbarForegroundColor= ColorUtils.blend(this.dialogForegroundColor, this.dialogBackgroundColor, 0.15f);
				this.scrollbarArrowColor= ColorUtils.blend(this.dialogForegroundColor, this.dialogBackgroundColor, 0.5f);
				break;
			}
		}
		
		this.docFontDescr= nonNullAssert(JFaceResources.getFontDescriptor(DOC_FONT_KEY));
		
		this.docBackgroundColor= nonNullAssert(JFaceResources.getColorRegistry()
				.getRGB(DOC_BACKGROUND_COLOR_KEY) );
		this.docForegroundColor= nonNullAssert(JFaceResources.getColorRegistry()
				.getRGB(DOC_FOREGROUND_COLOR_KEY) );
	}
	
	
	public FontDescriptor getDialogFontDescr() {
		return this.dialogFontDescr;
	}
	
	public FontDescriptor getDocFontDescr() {
		return this.docFontDescr;
	}
	
	public RGB getDocBackgroundColorRGB() {
		return this.docBackgroundColor;
	}
	
	public RGB getDocForegroundColorRGB() {
		return this.docForegroundColor;
	}
	
	public RGB getDocBorderColorRGB() {
		return this.borderColor;
	}
	
	
	public void appendLinkDefinitions(final StringBuilder sb) {
		final RGB hyperlinkColor= nonNullAssert(JFaceResources.getColorRegistry()
				.getRGB(JFacePreferences.HYPERLINK_COLOR) );
		final RGB hyperlinkActiveColor= nonNullAssert(JFaceResources.getColorRegistry()
				.getRGB(JFacePreferences.ACTIVE_HYPERLINK_COLOR) );
		sb.append("a { color: "); //$NON-NLS-1$
						appendCssColor(sb, hyperlinkColor);
				sb.append("; }\n");//$NON-NLS-1$
		sb.append("a:hover, a:active, a:focus { color: "); //$NON-NLS-1$
						appendCssColor(sb, hyperlinkActiveColor);
				sb.append("; }\n"); //$NON-NLS-1$
		sb.append("a:visited { color: "); //$NON-NLS-1$
				appendCssColor(sb, new RGB(
						(hyperlinkColor.red + ((hyperlinkColor.red <= 127) ? +64 : -64)),
						(hyperlinkColor.green + this.docForegroundColor.green) / 2,
						(hyperlinkColor.blue + ((hyperlinkColor.blue > 32) ? -32 : +32) + this.docForegroundColor.blue ) / 2 ));
				sb.append("; }\n"); //$NON-NLS-1$
	}
	
	void appendMatchColors(final StringBuilder sb) {
		int i= 0;
		final RGB rgb= PreferenceConverter.getColor(EditorsPlugin.getDefault().getPreferenceStore(),
				"searchResultIndicationColor" ); //$NON-NLS-1$
		if (rgb != null) {
			sb.append(".SMATCH-A { background: "); //$NON-NLS-1$
			appendCssColor(sb, rgb);
			sb.append("; }\n"); //$NON-NLS-1$
			i++;
		}
		while (i < DEFAULT_MATCH_COLORS.length) {
			sb.append(DEFAULT_MATCH_COLORS[i++]);
		}
	}
	
	void appendRootColors(final StringBuilder sb) {
		sb.append("color: "); //$NON-NLS-1$
				appendCssColor(sb, this.docForegroundColor).append("; "); //$NON-NLS-1$
		
		if (this.isThemeStylingRequired) {
			sb.append("scrollbar-color: "); //$NON-NLS-1$
					appendCssColor(sb, this.scrollbarForegroundColor).append(" "); //$NON-NLS-1$
					appendCssColor(sb, this.scrollbarBackgroundColor).append(" ;"); //$NON-NLS-1$
			// IE
			sb.append("scrollbar-track-color: "); //$NON-NLS-1$
					appendCssColor(sb, this.scrollbarBackgroundColor).append("; "); //$NON-NLS-1$
			sb.append("scrollbar-base-color: "); //$NON-NLS-1$
					appendCssColor(sb, this.scrollbarBackgroundColor).append("; "); //$NON-NLS-1$
			sb.append("scrollbar-face-color: "); //$NON-NLS-1$
					appendCssColor(sb, this.scrollbarForegroundColor).append("; "); //$NON-NLS-1$
			sb.append("scrollbar-arrow-color: "); //$NON-NLS-1$
					appendCssColor(sb, this.scrollbarArrowColor).append("; "); //$NON-NLS-1$
		}
	}
	
	void appendScreenColors(final StringBuilder sb) {
		sb.append("color: "); //$NON-NLS-1$
				appendCssColor(sb, this.docForegroundColor).append("; "); //$NON-NLS-1$
		sb.append("background: "); //$NON-NLS-1$
				appendCssColor(sb, this.docBackgroundColor).append("; "); //$NON-NLS-1$
	}
	
	void appendDialogColors(final StringBuilder sb) {
		sb.append("background: "); //$NON-NLS-1$
				appendCssColor(sb, this.dialogBackgroundColor).append("; "); //$NON-NLS-1$
		sb.append("color: "); //$NON-NLS-1$
				appendCssColor(sb, this.dialogForegroundColor).append("; "); //$NON-NLS-1$
	}
	
	void appendDialogButtonDefinitions(final StringBuilder sb) {
		sb.append("button { "); //$NON-NLS-1$
				appendFont(sb, this.dialogFontDescr);
				sb.append("padding: 1px 4px 2px; " //$NON-NLS-1$
				+ "border: 1px solid "); //$NON-NLS-1$
						appendCssColor(sb, this.borderColor).append("; "); //$NON-NLS-1$
				sb.append("background: "); //$NON-NLS-1$
						appendCssColor(sb, this.buttonBackgroundColor).append("; "); //$NON-NLS-1$
				sb.append("color: "); //$NON-NLS-1$
						appendCssColor(sb, this.buttonForegroundColor).append("; "); //$NON-NLS-1$
				sb.append("}\n"); //$NON-NLS-1$
		sb.append("button:hover { " //$NON-NLS-1$
				+ "border-color: "); //$NON-NLS-1$
						appendCssColor(sb, ColorUtils.blend(
								this.borderColor, this.selectionBackgroundColor, 0.8f) )
						.append("; "); //$NON-NLS-1$
				sb.append("background: "); //$NON-NLS-1$
						appendCssColor(sb, ColorUtils.blend(
								this.buttonBackgroundColor, this.selectionBackgroundColor, 0.9f) )
						.append("; "); //$NON-NLS-1$
				sb.append("}\n"); //$NON-NLS-1$
		
	}
	
	void appendFont(final StringBuilder sb, final FontDescriptor fontDescription) {
		final FontData fontData= fontDescription.getFontData()[0];
		sb.append("font-family: '").append(fontData.getName()).append("'; "); //$NON-NLS-1$ //$NON-NLS-2$
		sb.append("font-size: ").append(fontData.getHeight()).append("pt; "); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
