/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.ecommons.ui.components.StatusInfo;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.util.PixelConverter;


public class StatusPage extends WizardPage {
	
	
	public static final String PAGE_NAME= "StatusPage"; //$NON-NLS-1$
	
	
	private final boolean allowIgnore;
	
	private IStatus status;
	
	private TableViewer childViewer;
	
	private Button ignoreControl;
	
	
	public StatusPage(final String title, final boolean allowIgnore) {
		super(PAGE_NAME);
		this.allowIgnore= allowIgnore;
		this.status= Status.OK_STATUS;
		
		setTitle(title);
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		initializeDialogUnits(parent);
		
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(LayoutUtils.newContentGrid(1));
		
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("&Issues:");
			label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		{	final Control detail= createDetailArea(composite);
			detail.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		}
		if (this.allowIgnore) {
			final Button button= new Button(composite, SWT.CHECK);
			this.ignoreControl= button;
			button.setText("Ignore shown issues and continue.");
			button.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					updateState();
				}
			});
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		}
		
		Dialog.applyDialogFont(composite);
		setControl(composite);
		
		setStatus(this.status);
	}
	
	protected Control createDetailArea(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newCompositeGrid(1));
		
		this.childViewer= new TableViewer(composite);
		this.childViewer.setContentProvider(ArrayContentProvider.getInstance());
		this.childViewer.setLabelProvider(new StatusLabelProvider());
		this.childViewer.getTable().setToolTipText(null);
		new ColumnViewerToolTipSupport(this.childViewer, ColumnViewerToolTipSupport.NO_RECREATE, false) {
			@Override
			protected Composite createViewerToolTipContentArea(final Event event,
					final ViewerCell cell, final Composite parent) {
				final Image image= getImage(event);
				final String text= getText(event);
				
				final Composite composite= new Composite(parent, SWT.NONE);
				composite.setLayout(LayoutUtils.newCompositeGrid((image != null) ? 2 : 1));
				composite.setBackgroundMode(SWT.INHERIT_DEFAULT);
				{	final Color color= getBackgroundColor(event);
					if (color != null) {
						composite.setBackground(color);
					}
				}
				
				if (image != null) {
					final Label label= new Label(composite, SWT.LEFT | SWT.TOP);
					label.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, false, false));
					label.setImage(image);
				}
				{	final Label label= new Label(composite, SWT.LEFT | SWT.WRAP | SWT.TRANSPARENT);
					final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
					gd.widthHint= new PixelConverter(label).convertWidthInCharsToPixels(80);
					label.setLayoutData(gd);
					label.setText(text);
					label.setForeground(getForegroundColor(event));
				}
				
				return composite;
			}
		};
		
		{	final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
			gd.heightHint= LayoutUtils.hintHeight(this.childViewer.getTable(), 8);
			gd.widthHint= 100;
			this.childViewer.getControl().setLayoutData(gd);
		}
		
		return composite;
	}
	
	
	public void setStatus(final IStatus status) {
		if (status == null) {
			throw new NullPointerException("status"); //$NON-NLS-1$
		}
		this.status= status;
		if (isControlCreated()) {
			StatusInfo.applyToStatusLine(this, status);
			this.childViewer.setInput(status.isMultiStatus() ? status.getChildren() : new Object[0]);
			if (this.ignoreControl != null) {
				this.ignoreControl.setEnabled(this.status.getSeverity() >= IStatus.ERROR);
			}
		}
		updateState();
	}
	
	private void updateState() {
		setPageComplete(this.status.getSeverity() < IStatus.ERROR
				|| (this.ignoreControl != null && this.ignoreControl.getSelection() ));
	}
	
}
