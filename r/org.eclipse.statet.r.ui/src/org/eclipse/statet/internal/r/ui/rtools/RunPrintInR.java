/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rtools;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.common.NotDefinedException;

import org.eclipse.statet.r.launching.AbstractRCommandHandler;


/**
 * Command handler for print(...)
 */
public class RunPrintInR extends AbstractRCommandHandler {
	
	
	public static final String COMMAND_ID= "org.eclipse.statet.r.commands.RunPrintInR"; //$NON-NLS-1$
	private static final String PAR_VAR= "var"; //$NON-NLS-1$
	
	
	public static String createCommandString(final String var) throws NotDefinedException {
		return createCommandString(COMMAND_ID, new String[][] {{ PAR_VAR, var }});
	}
	
	
	public RunPrintInR() {
		super(Messages.PrintCommand_name);
	}
	
	
	@Override
	public Object execute(final ExecutionEvent arg) throws ExecutionException {
		String var= arg.getParameter(PAR_VAR);
		if (var == null) {
			var= getRSelection();
			if (var == null) {
				return null;
			}
		}
//		"print(\""+RUtil.escapeDoubleQuote(var)+"\")"
		runCommand(var, false);
		return null;
	}
	
}
