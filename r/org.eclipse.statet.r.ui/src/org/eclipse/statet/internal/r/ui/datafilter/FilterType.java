/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilter;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class FilterType {
	
	
	public static final FilterType LEVEL= new FilterType(0, Messages.LevelFilter_label);
	public static final FilterType INTERVAL= new FilterType(1, Messages.IntervalFilter_label);
	public static final FilterType TEXT= new FilterType(2, Messages.TextFilter_label);
	
	
	private final int id;
	
	private final String label;
	
	
	public FilterType(final int id, final String label) {
		this.id= id;
		this.label= label;
	}
	
	
	public int getId() {
		return this.id;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	
	@Override
	public String toString() {
		return this.id + ": " + this.label.substring(1); //$NON-NLS-1$
	}
	
}
