/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.themes.IThemeManager;

import org.eclipse.statet.ecommons.workbench.ui.IWaThemeConstants;


public abstract class ResizeTableTextHandler extends AbstractHandler {
	
	
	public static class ZoomIn extends ResizeTableTextHandler {
		
		@Override
		protected int getDirection() {
			return 1;
		}
		
	}
	
	public static class ZoomOut extends ResizeTableTextHandler {
		
		@Override
		protected int getDirection() {
			return -1;
		}
		
	}
	
	
	protected ResizeTableTextHandler() {
	}
	
	protected abstract int getDirection();
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IThemeManager themeManager= PlatformUI.getWorkbench().getThemeManager();
		final FontRegistry fontRegistry= themeManager.getCurrentTheme().getFontRegistry();
		
		final Font font= fontRegistry.get(IWaThemeConstants.TABLE_FONT);
		if (font != null) {
			final FontDescriptor fontDescriptor= createFontDescriptor(font.getFontData());
			if (fontDescriptor != null) {
				fontRegistry.put(IWaThemeConstants.TABLE_FONT, fontDescriptor.getFontData());
			}
		}
		
		return null;
	}
	
	
	private FontDescriptor createFontDescriptor(final FontData[] currentFontData) {
		int fontSize= currentFontData[0].getHeight();
		fontSize= (getDirection() > 0) ?
				Math.max(fontSize + 1, fontSize * 9 / 16 * 2) :
				Math.min(fontSize - 1, (fontSize + 2) * 8 / 18 * 2);
		if (fontSize <= 0) {
			return null;
		}
		return FontDescriptor.createFrom(currentFontData).setHeight(fontSize);
	}
	
}
