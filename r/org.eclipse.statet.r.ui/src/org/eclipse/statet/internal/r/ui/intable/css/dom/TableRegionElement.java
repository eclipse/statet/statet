/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable.css.dom;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.css.dom.VirtualStylableElement;


@NonNullByDefault
@SuppressWarnings("restriction")
public class TableRegionElement extends VirtualStylableElement<TableElement> {
	
	
	public static final String BODY_NAME= "Body"; //$NON-NLS-1$
	public static final String HEADER_NAME= "Header"; //$NON-NLS-1$
	
	
	public TableRegionElement(final TableElement parent,
			final String name, final @Nullable String label,
			final ImList<@Nullable String> cellCssClasses, final @Nullable ImList<String> cellCssStaticPseudoClasses,
			final CSSEngine engine) {
		super(nonNullAssert(parent), name, label, engine);
		
		init(createChildren(cellCssClasses, (cellCssStaticPseudoClasses != null) ?
				cellCssStaticPseudoClasses : ImCollections.newList() ));
	}
	
	protected ImList<TableCellElement> createChildren(
			final ImList<@Nullable String> cssClasses, final ImList<String> cssStaticPseudoClasses) {
		final var children= new @NonNull TableCellElement[cssClasses.size()];
		int i= 0;
		for (final String cssClass : cssClasses) {
			final TableCellElement cellElement= new TableCellElement(this, cssClass, this.engine);
			for (final String pseudoClass : cssStaticPseudoClasses) {
				cellElement.addStaticPseudoInstance(pseudoClass);
			}
			children[i++]= cellElement;
		}
		return ImCollections.newList(children, 0, i);
	}
	
	
}
