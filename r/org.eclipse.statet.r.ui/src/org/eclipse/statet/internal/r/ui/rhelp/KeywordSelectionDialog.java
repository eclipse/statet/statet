/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;

import org.eclipse.statet.rhelp.core.RHelpKeywordGroup;
import org.eclipse.statet.rhelp.core.RHelpKeywordNode;


public class KeywordSelectionDialog extends ElementTreeSelectionDialog {
	
	
	public static class KeywordsContentProvider implements ITreeContentProvider {
		
		@Override
		public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
		}
		
		@Override
		public Object[] getElements(final Object inputElement) {
			return ((List<?>) inputElement).toArray();
		}
		
		@Override
		public void dispose() {
		}
		
		@Override
		public Object getParent(final Object element) {
			return null;
		}
		
		@Override
		public Object[] getChildren(final Object parentElement) {
			if (parentElement instanceof RHelpKeywordNode) {
				return ((RHelpKeywordNode) parentElement).getNestedKeywords().toArray();
			}
			return null;
		}
		
		@Override
		public boolean hasChildren(final Object element) {
			return (element instanceof RHelpKeywordNode
					&& !((RHelpKeywordNode) element).getNestedKeywords().isEmpty() );
		}
		
	}
	
	
	public KeywordSelectionDialog(final Shell parent, final List<RHelpKeywordGroup> keywords) {
		super(parent, new RHelpLabelProvider(), new KeywordsContentProvider());
		setShellStyle(getShellStyle() | SWT.SHEET);
		setTitle(Messages.KeywordSelection_title);
		setMessage(Messages.KeywordSelection_message);
		setInput(keywords);
		setAllowMultiple(false);
	}
	
	@Override
	protected Control createContents(final Composite parent) {
		final Control control= super.createContents(parent);
		getTreeViewer().expandToLevel(1);
		return control;
	}
	
}
