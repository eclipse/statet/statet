/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.intable;

import org.eclipse.statet.ecommons.waltable.core.command.AbstractLayerCommandHandler;
import org.eclipse.statet.ecommons.waltable.core.coordinate.LRangeList.ValueIterator;
import org.eclipse.statet.ecommons.waltable.resize.core.AutoResizePositionsCommand;


public class MultiColumnAutoResizeCommandHandler extends AbstractLayerCommandHandler<AutoResizePositionsCommand> {
	
	
	private final RDataLayer fDataLayer;
	
	
	public MultiColumnAutoResizeCommandHandler(final RDataLayer dataLayer) {
		this.fDataLayer= dataLayer;
	}
	
	
	@Override
	public Class<AutoResizePositionsCommand> getCommandClass() {
		return AutoResizePositionsCommand.class;
	}
	
	@Override
	protected boolean doCommand(final AutoResizePositionsCommand command) {
		for (final var posIter= new ValueIterator(command.getPositions()); posIter.hasNext(); ) {
			this.fDataLayer.setColumnWidthToAutoWidth(posIter.nextLong());
		}
		return true;
	}
	
}
