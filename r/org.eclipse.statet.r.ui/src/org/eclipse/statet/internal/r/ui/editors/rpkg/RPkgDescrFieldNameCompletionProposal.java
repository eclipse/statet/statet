/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rpkg;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_FIELD_NAME_CONTENT_TYPE;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.SearchPattern;

import org.eclipse.statet.ecommons.text.core.util.TextUtils;

import org.eclipse.statet.dsl.ui.DslUI;
import org.eclipse.statet.dsl.ui.DslUIResources;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SourceProposal;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;
import org.eclipse.statet.r.ui.sourceediting.RPkgDescrAssistInvocationContext;


@NonNullByDefault
public class RPkgDescrFieldNameCompletionProposal
		extends SourceProposal<RPkgDescrAssistInvocationContext> {
	
	
	public static class FieldNameElementProposalParameters extends ProposalParameters<RPkgDescrAssistInvocationContext> {
		
		
		public RPkgDescrFieldDefinition fieldDef;
		
		
		@SuppressWarnings("null")
		public FieldNameElementProposalParameters(
				final RPkgDescrAssistInvocationContext context, final int replacementOffset,
				final SearchPattern namePattern, final int baseRelevance) {
			super(context, replacementOffset, namePattern, baseRelevance);
		}
		
		@SuppressWarnings("null")
		public FieldNameElementProposalParameters(
				final RPkgDescrAssistInvocationContext context, final int replacementOffset,
				final SearchPattern namePattern) {
			super(context, replacementOffset, namePattern);
		}
		
	}
	
	
	protected final RPkgDescrFieldDefinition fieldDef;
	
	
	public RPkgDescrFieldNameCompletionProposal(final FieldNameElementProposalParameters parameters) {
		super(parameters);
		this.fieldDef= nonNullAssert(parameters.fieldDef);
	}
	
	
	@Override
	protected String getName() {
		return this.fieldDef.getName();
	}
	
	@Override
	protected int computeReplacementLength(final int replacementOffset, final Point selection,
			final int caretOffset, final boolean overwrite)
			throws BadPartitioningException, BadLocationException {
		int end= Math.max(caretOffset, selection.x + selection.y);
		if (overwrite) {
			final var context= getInvocationContext();
			final var document= context.getDocument();
			final var partition= TextUtils.getPartition(document, context.getEditor().getDocumentContentInfo(),
					replacementOffset, true );
			if (partition.getType() == RPKG_DESCR_FIELD_NAME_CONTENT_TYPE) {
				end= partition.getOffset() + partition.getLength();
			}
			else {
				end= TextUtils.findCommonWord2End(document, replacementOffset, document.getLength());
			}
		}
		return (end - replacementOffset);
	}
	
	
	@Override
	public String getSortingString() {
		return this.fieldDef.getName();
	}
	
	@Override
	public Image getImage() {
		return DslUI.getUIResources().getImage(DslUIResources.OBJ_SCALAR_ELEMENT_IMAGE_ID);
	}
	
	
	@Override
	public boolean isAutoInsertable() {
		return true;
	}
	
	
	@Override
	protected void doApply(final char trigger, final int stateMask, final int caretOffset,
			final int replacementOffset, final int replacementLength)
			throws BadLocationException {
		final var context= getInvocationContext();
		final var document= context.getDocument();
		final ApplyData applyData= getApplyData();
		
		final StringBuilder replacement= new StringBuilder(this.fieldDef.getName());
		int cursor= replacement.length();
		
		{	final int replacementEnd= replacementOffset + replacementLength;
			final char next= (replacementEnd < document.getLength()) ?
					document.getChar(replacementEnd) : 0;
			if (next == ':') {
				if (replacementEnd + 1 < document.getLength()
						&& document.getChar(replacementEnd + 1) == ' ') {
					cursor+= 2;
				}
				else {
					cursor++;
				}
			}
			else {
				replacement.append(':');
				if (next != ' ') {
					replacement.append(' ');
				}
				cursor+= 2;
			}
		}
		
		document.replace(replacementOffset, replacementLength, replacement.toString());
		applyData.setSelection(replacementOffset + cursor);
	}
	
	
}
