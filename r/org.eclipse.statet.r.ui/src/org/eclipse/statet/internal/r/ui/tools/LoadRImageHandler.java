/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.tools;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.wizard.WizardDialog;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.ui.actions.AbstractToolHandler;
import org.eclipse.statet.nico.ui.util.NicoWizardDialog;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.console.core.RProcess;


@NonNullByDefault
public class LoadRImageHandler extends AbstractToolHandler<RProcess> {
	
	
	public LoadRImageHandler() {
		super(RConsoleTool.TYPE, LoadRImageRunnable.REQUIRED_FEATURESET_ID);
	}
	
	
	@Override
	protected @Nullable Object execute(final RProcess tool,
			final ExecutionEvent event) throws ExecutionException {
		final LoadRImageWizard wizard = new LoadRImageWizard(tool);
		final WizardDialog dialog = new NicoWizardDialog(UIAccess.getActiveWorkbenchShell(true), wizard);
		dialog.setBlockOnOpen(false);
		dialog.open();
		
		return null;
	}
	
}
