/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Font;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.waltable.core.data.ControlData;

import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;


@NonNullByDefault
public class ColumnLabelProvider extends CellLabelProvider {
	
	
	private final RDataTableColumn column;
	
	private @Nullable Font infoFont;
	
	
	public ColumnLabelProvider(final RDataTableColumn column) {
		this.column= column;
	}
	
	
	@Override
	public void update(final ViewerCell cell) {
		final Object data= this.column.getDefaultFormat().modelToDisplayValue(cell.getElement());
		cell.setFont((data instanceof ControlData) ? getInfoFont() : null);
		cell.setText(data.toString());
	}
	
	protected Font getInfoFont() {
		var font= this.infoFont;
		if (font == null) {
			font= JFaceResources.getFontRegistry().getItalic(JFaceResources.DIALOG_FONT);
			this.infoFont= font;
		}
		return font;
	}
	
}
