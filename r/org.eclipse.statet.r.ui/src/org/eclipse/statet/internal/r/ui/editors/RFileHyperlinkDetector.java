/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.filesystem.IFileInfo;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.text.hyperlink.AbstractHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.text.core.input.OffsetStringParserInput;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;
import org.eclipse.statet.ecommons.text.ui.OpenFileHyperlink;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.source.RLexer;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;


@NonNullByDefault
public class RFileHyperlinkDetector extends AbstractHyperlinkDetector {
	
	
	public RFileHyperlinkDetector() {
	}
	
	
	@Override
	public @NonNull IHyperlink @Nullable [] detectHyperlinks(final ITextViewer textViewer,
			final IRegion region, final boolean canShowMultipleHyperlinks) {
		try {
			final SourceEditor editor= getAdapter(SourceEditor.class);
			final IDocument document= textViewer.getDocument();
			if (editor == null || document == null) {
				return null;
			}
			
			final List<IHyperlink> hyperlinks= new ArrayList<>(4);
			
			TextRegion textRegion= null;
			String path= null;
			final ITypedRegion partition= TextUtilities.getPartition(document,
					editor.getDocumentContentInfo().getPartitioning(), region.getOffset(), false );
			if (partition != null && partition.getLength() > 3) {
				if (partition.getType().equals(RDocumentConstants.R_COMMENT_CONTENT_TYPE)
						|| partition.getType().equals(RDocumentConstants.R_ROXYGEN_CONTENT_TYPE) ) {
					boolean quote= false;
					int start= region.getOffset();
					{	final int bound= partition.getOffset() + 1;
						{	final char c= document.getChar(start);
							if (c <= 0x22 || c == '>' || c == '<') {
								return null;
							}
						}
						while (start > bound) {
							final char c= document.getChar(start-1);
							if (c <= 0x22 || c == '>' || c == '<') {
								if (c == '"') {
									quote= true;
								}
								break;
							}
							start--;
						}
					}
					int stop= region.getOffset() + 1;
					{	final int bound= partition.getOffset()+partition.getLength();
						while (stop < bound) {
							final char c= document.getChar(stop);
							if (c <= 0x22 || c == '>' || c == '<') {
								if (quote || c != '"') {
									break;
								}
							}
							stop++;
						}
					}
					if (start < stop) {
						textRegion= new BasicTextRegion(start, stop);
						path= document.get(start, stop - start);
					}
				}
				else if (partition.getType().equals(RDocumentConstants.R_STRING_CONTENT_TYPE)) {
					final RLexer rLexer= new RLexer();
					rLexer.reset(new OffsetStringParserInput(
									document.get(partition.getOffset(), partition.getLength()),
									partition.getOffset() )
							.init(partition.getOffset(), partition.getOffset() + partition.getLength()) );
					switch (rLexer.next()) {
					case STRING_D:
					case STRING_S:
					case STRING_R:
						textRegion= rLexer.getTextRegion();
						path= rLexer.getText();
					default:
						break;
					}
				}
				
				if (path == null || textRegion == null) {
					return null;
				}
				
				IContainer relativeBase= null;
				final SourceUnit su= editor.getSourceUnit();
				if (su instanceof WorkspaceSourceUnit) {
					final IResource resource= ((WorkspaceSourceUnit)su).getResource();
//					final IRProject rProject= RProjects.getRProject(resource.getProject());
//					if (rProject != null) {
//						relativeBase= rProject.getRawBuildpath();
//					}
					if (relativeBase == null) {
						relativeBase= resource.getParent();
					}
				}
				final IFileStore store= FileUtil.getLocalFileStore(path, relativeBase);
				
				if (store != null) {
					final IFileInfo info= store.fetchInfo();
					if (info.exists() && !store.fetchInfo().isDirectory()) {
						hyperlinks.add(new OpenFileHyperlink(JFaceTextRegion.toJFaceRegion(textRegion), store));
					}
				}
			}
			if (!hyperlinks.isEmpty()) {
				return hyperlinks.toArray(new @NonNull IHyperlink[hyperlinks.size()]);
			}
		}
		catch (final BadLocationException | CoreException e) {}
		return null;
	}
	
}
