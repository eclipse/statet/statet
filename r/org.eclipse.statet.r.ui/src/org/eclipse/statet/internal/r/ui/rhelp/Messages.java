/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String Search_Query_label;
	
	public static String Search_PatternInTopics_label;
	public static String Search_PatternInFields_label;
	public static String Search_Pattern_label;
	public static String Search_SingleMatch_label;
	public static String Search_MultipleMatches_label;
	
	public static String PackageSelection_title;
	public static String PackageSelection_message;
	public static String PackageSelection_ClearAll_label;
	
	public static String KeywordSelection_title;
	public static String KeywordSelection_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
