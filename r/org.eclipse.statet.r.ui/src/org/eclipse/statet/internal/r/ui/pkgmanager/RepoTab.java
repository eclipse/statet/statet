/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.viewers.typed.ViewerProperties;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICheckable;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.TabItem;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils.CheckboxTableComposite;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils.TableComposite;

import org.eclipse.statet.r.core.pkgmanager.BasicRRepoSettings;
import org.eclipse.statet.r.core.pkgmanager.IRPkgManager;
import org.eclipse.statet.r.core.pkgmanager.RRepo;
import org.eclipse.statet.r.core.pkgmanager.RRepoSettings;


public class RepoTab extends Composite {
	
	
	private final RPkgManagerDialog dialog;
	
	private final TabItem tab;
	
	private List<RRepo> availableRepos;
	
	private IObservableSet<RRepo> selectedRepos;
	private IObservableValue<RRepo> selectedCran;
	private String biocVersion;
	private IObservableValue<RRepo> selectedBioc;
	
	private CheckboxTableViewer repoTable;
	private TableViewer CranTable;
	private Label biocLabel;
	private TableViewer biocTable;
	
	
	RepoTab(final RPkgManagerDialog dialog, final TabItem tab, final Composite parent) {
		super(parent, SWT.NONE);
		
		this.dialog= dialog;
		this.tab= tab;
		
		setLayout(LayoutUtils.newTabGrid(2, true));
		createContent(this);
	}
	
	
	private void createContent(final Composite parent) {
		// Column 1
		{
			final Composite composite= new Composite(parent, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			composite.setLayout(LayoutUtils.newCompositeGrid(1));
			
			{	final Label label= new Label(composite, SWT.NONE);
				label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
				label.setText("R&epositories:");
			}
			final CheckboxTableComposite table= new CheckboxTableComposite(composite,
					SWT.CHECK | SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION );
			this.repoTable= table.viewer;
			{	final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
				gd.heightHint= LayoutUtils.hintHeight(table.table, 15);
				gd.widthHint= LayoutUtils.hintWidth(table.table, 40);
				table.setLayoutData(gd);
			}
			table.addColumn("Repository", SWT.LEFT, new ColumnWeightData(100, false));
			
			table.viewer.setLabelProvider(new RRepoLabelProvider());
			table.viewer.setContentProvider(new ArrayContentProvider());
		}
		// Column 2
		{
			final Composite composite= new Composite(parent, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			composite.setLayout(LayoutUtils.newCompositeGrid(1));
			
			{
				final Label label= new Label(composite, SWT.NONE);
				label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
				label.setText("CR&AN Mirror:");
			}
			this.CranTable= createMirrorTable(composite);
			{
				final Label label= new Label(composite, SWT.NONE);
				label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
				label.setText("&Bioconductor Mirror:");
				this.biocLabel= label;
			}
			this.biocTable= createMirrorTable(composite);
		}
		
		{	final Link link= new Link(parent, SWT.NONE);
			link.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));
			link.setText("Custom repositories and mirrors can be configured in the "
					+ "<a href=\"org.eclipse.statet.r.preferencePages.RRepositories\">preferences</a>.");
			link.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					RepoTab.this.dialog.openPrefPage(e.text);
				}
			});
		}
	}
	
	private TableViewer createMirrorTable(final Composite parent) {
		final TableComposite table= new TableComposite(parent, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
		{
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
			gd.heightHint= LayoutUtils.hintHeight(table.table, 5);
			gd.widthHint= LayoutUtils.hintWidth(table.table, 40);
			table.setLayoutData(gd);
		}
		table.addColumn("Mirror", SWT.LEFT, new ColumnWeightData(100, false));
		
		table.viewer.setLabelProvider(new RRepoLabelProvider());
		table.viewer.setContentProvider(new ArrayContentProvider());
		
		return table.viewer;
	}
	
	
	RRepoSettings createRepoSettings() {
		final var list= new ArrayList<RRepo>(this.selectedRepos.size());
		for (final RRepo repo : this.availableRepos) {
			if (this.selectedRepos.contains(repo)) {
				list.add(repo);
			}
		}
		return new BasicRRepoSettings(list,
				this.selectedCran.getValue(),
				this.biocVersion, this.selectedBioc.getValue() );
	}
	
	
	TabItem getTab() {
		return this.tab;
	}
	
	List<RRepo> getAvailableRepos() {
		return this.availableRepos;
	}
	
	void addBindings(final DataBindingSupport db) {
		this.selectedRepos= new WritableSet<>(db.getRealm(), Collections.emptySet(), RRepo.class);
		this.selectedCran= new WritableValue<>(db.getRealm(), null, RRepo.class);
		this.selectedBioc= new WritableValue<>(db.getRealm(), null, RRepo.class);
		
		db.getContext().bindSet(
				ViewerProperties.checkedElements(RRepo.class)
						.observe((ICheckable)this.repoTable),
				this.selectedRepos );
		db.getContext().bindValue(
				ViewerProperties.singleSelection(RRepo.class)
						.observe(this.CranTable),
				this.selectedCran );
		db.getContext().bindValue(
				ViewerProperties.singleSelection(RRepo.class)
						.observe(this.biocTable),
				this.selectedBioc );
		
		this.selectedRepos.addChangeListener(this.dialog);
		this.selectedCran.addChangeListener(this.dialog);
		this.selectedBioc.addChangeListener(this.dialog);
	}
	
	void init() {
		this.repoTable.getTable().setSelection(0);
	}
	
	void updateSettings(final IRPkgManager.Ext pkgManager) {
		this.availableRepos= pkgManager.getAvailableRepos();
		final RRepoSettings repoSettings= pkgManager.getSelectedRepos();
		this.repoTable.setInput(this.availableRepos);
		if (this.selectedRepos.isEmpty()) {
			this.selectedRepos.addAll(repoSettings.getRepos());
		}
		else {
			this.selectedRepos.retainAll(this.availableRepos);
		}
		
		this.CranTable.setInput(pkgManager.getAvailableCRANMirrors());
		this.selectedCran.setValue(repoSettings.getCRANMirror());
		
		this.biocVersion= repoSettings.getBioCVersion();
		this.biocLabel.setText("&Bioconductor " + " (" + this.biocVersion + ") Mirror:");
		this.biocTable.setInput(pkgManager.getAvailableBioCMirrors());
		this.selectedBioc.setValue(repoSettings.getBioCMirror());
	}
	
}
