/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.graphics;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.rj.eclient.graphics.ERGraphic;
import org.eclipse.statet.rj.eclient.graphics.RGraphicCompositeActionSet;
import org.eclipse.statet.rj.eclient.graphics.RGraphicPage;
import org.eclipse.statet.rj.ts.core.RTool;


public class StatetRGraphicPage extends RGraphicPage {
	
	
	private class SaveInRHandler extends AbstractHandler {
		
		
		private final String devCmd;
		private final String devAbbr;
		
		
		public SaveInRHandler(final String cmd, final String abbr) {
			this.devCmd= cmd;
			this.devAbbr= abbr;
		}
		
		
		@Override
		public void setEnabled(final Object evaluationContext) {
			final RTool tool= getGraphic().getRHandle();
			setBaseEnabled(tool != null && !tool.isTerminated());
		}
		
		@Override
		public Object execute(final ExecutionEvent event) throws ExecutionException {
			final RTool tool= getGraphic().getRHandle();
			if (tool instanceof RProcess) {
				final StatetRGraphicCopyToDevWizard wizard= new StatetRGraphicCopyToDevWizard((RProcess)tool,
						getGraphic(), this.devCmd, this.devAbbr );
				final WizardDialog dialog= new WizardDialog(UIAccess.getActiveWorkbenchShell(true), wizard);
				dialog.setBlockOnOpen(false);
				dialog.open();
			}
			return null;
		}
		
	}
	
	
	public StatetRGraphicPage(final ERGraphic graphic) {
		super(graphic);
	}
	
	
	@Override
	protected RGraphicCompositeActionSet createActionSet() {
		return new RGraphicCompositeActionSet(getGraphicComposite()) {
			@Override
			public void contributeToActionsBars(final IServiceLocator serviceLocator,
					final IActionBars actionBars) {
				super.contributeToActionsBars(serviceLocator, actionBars);
				
				addSizeActions(serviceLocator, actionBars);
			}
		};
	}
	
	@Override
	protected void initActions(final IServiceLocator serviceLocator, final IActionBars actionBars) {
		super.initActions(serviceLocator, actionBars);
		
		final IMenuManager menu= actionBars.getMenuManager();
		
		menu.appendToGroup("save", new HandlerContributionItem(new CommandContributionItemParameter(
				getSite(), null, HandlerContributionItem.NO_COMMAND_ID, null,
				null, null, null,
				"Save as PDF (using R)...", "D", null,
				HandlerContributionItem.STYLE_PUSH, null, false),
				new SaveInRHandler("pdf", "pdf"))); //$NON-NLS-1$ //$NON-NLS-2$
		menu.appendToGroup("save", new HandlerContributionItem(new CommandContributionItemParameter(
				getSite(), null, HandlerContributionItem.NO_COMMAND_ID, null,
				null, null, null,
				"Save as EPS (using R)...", "E", null,
				HandlerContributionItem.STYLE_PUSH, null, false),
				new SaveInRHandler("postscript", "eps"))); //$NON-NLS-1$ //$NON-NLS-2$
		
	}
	
}
