/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.r;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SearchPattern;

import org.eclipse.statet.ecommons.ui.content.ITextElementFilter;
import org.eclipse.statet.ecommons.ui.content.TextElementFilter;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.ui.sourceediting.QuickOutlineInformationControl;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.ui.RLabelProvider;
import org.eclipse.statet.r.ui.sourceediting.ROpenDeclaration;
import org.eclipse.statet.r.ui.util.RNameSearchPattern;


public class RQuickOutlineInformationControl extends QuickOutlineInformationControl {
	
	
	private class ContentFilter implements LtkModelElementFilter {
		
		@Override
		public boolean include(final LtkModelElement element) {
			switch (element.getElementType()) {
			case RElement.R_ARGUMENT:
				return false;
			default:
				return true;
			}
		}
		
	}
	
	
	private final ContentFilter contentFilter= new ContentFilter();
	
	
	public RQuickOutlineInformationControl(final Shell parent, final String commandId) {
		super(parent, commandId, 1, new ROpenDeclaration());
	}
	
	
	@Override
	public String getModelTypeId(final int page) {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	protected LtkModelElementFilter getContentFilter() {
		return this.contentFilter;
	}
	
	@Override
	protected ITextElementFilter createNameFilter() {
		return new TextElementFilter() {
			@Override
			protected SearchPattern createSearchPattern() {
				return new RNameSearchPattern();
			}
		};
	}
	
	@Override
	protected void configureViewer(final TreeViewer viewer) {
		super.configureViewer(viewer);
		
		viewer.setLabelProvider(new RLabelProvider());
	}
	
}
