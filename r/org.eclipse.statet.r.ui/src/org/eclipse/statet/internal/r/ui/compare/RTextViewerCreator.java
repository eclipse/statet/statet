/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.compare;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.IViewerCreator;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.compare.CompareTextViewer;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.ui.sourceediting.RSourceViewerConfiguration;
import org.eclipse.statet.r.ui.sourceediting.RSourceViewerConfigurator;


@NonNullByDefault
public class RTextViewerCreator implements IViewerCreator {
	
	
	@Override
	public Viewer createViewer(final Composite parent, final CompareConfiguration config) {
		final var viewerConfigurator= new RSourceViewerConfigurator(
				RCore.getWorkbenchAccess(),
				new RSourceViewerConfiguration(0, null) );
		return new CompareTextViewer(parent, config, viewerConfigurator);
	}
	
}
