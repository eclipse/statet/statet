/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;

import org.eclipse.statet.rj.renv.core.RPkg;


class DetailGroup {
	
	
	public static abstract class ContentProvider implements ITreeContentProvider {
		
		
		protected final DetailGroup[] groups;
		
		
		public ContentProvider(final int num) {
			this.groups= new DetailGroup[num];
		}
		
		@Override
		public void dispose() {
		}
		
		@Override
		public Object[] getElements(final Object inputElement) {
			return this.groups;
		}
		
		@Override
		public Object getParent(final Object element) {
			return null;
		}
		
		@Override
		public boolean hasChildren(final Object element) {
			return (element instanceof DetailGroup && !((DetailGroup) element).getList().isEmpty());
		}
		@Override
		public Object[] getChildren(final Object parentElement) {
			return ((DetailGroup) parentElement).getList().toArray();
		}
		
	}
	
	
	private final int id;
	
	private final String label;
	
	private List<? extends RPkg> list= Collections.emptyList();
	
	
	public DetailGroup(final int id, final String label) {
		this.id= id;
		this.label= label;
	}
	
	
	public int getId() {
		return this.id;
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public void setList(final List<? extends RPkg> list) {
		this.list= list;
	}
	
	public void clearList() {
		this.list= Collections.emptyList();
	}
	
	public List<? extends RPkg> getList() {
		return this.list;
	}
	
}
