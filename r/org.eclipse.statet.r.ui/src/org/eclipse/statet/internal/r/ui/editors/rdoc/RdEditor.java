/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rdoc;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.util.LTKWorkbenchUIUtil;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.ui.editors.RdSourceViewerConfiguration;
import org.eclipse.statet.r.ui.editors.RdSourceViewerConfigurator;


public class RdEditor extends SourceEditor1 {
	
	
	private RdSourceViewerConfigurator config;
	
	
	public RdEditor() {
		super(RCore.RD_CONTENT_TYPE);
	}
	
	
	@Override
	public String getModelTypeId() {
		return "Rd"; // not yet a real type
	}
	
	@Override
	protected SourceEditorViewerConfigurator createConfiguration() {
		setDocumentProvider(RUIPlugin.getInstance().getRdDocumentProvider());
		
		this.config = new RdSourceViewerConfigurator(
				RCore.getWorkbenchAccess(),
				new RdSourceViewerConfiguration(0, this, null, null, null) );
		return this.config;
	}
	
	@Override
	public void createPartControl(final Composite parent) {
		super.createPartControl(parent);
		
		this.config.setTarget(this);
	}
	
	@Override
	protected void setupConfiguration(final IEditorInput newInput) {
		final var file= LTKWorkbenchUIUtil.getFile(newInput);
		this.config.setSource(RCore.getContextAccess(file));
	}
	
	@Override
	protected String[] collectContextMenuPreferencePages() {
		final String[] ids = super.collectContextMenuPreferencePages();
		final String[] more = new String[ids.length + 1];
		more[0]= "org.eclipse.statet.r.preferencePages.RdSyntaxColoring"; //$NON-NLS-1$
		System.arraycopy(ids, 0, more, 1, ids.length);
		return more;
	}
	
}
