/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.search;

import org.eclipse.search.ui.text.Match;

import org.eclipse.statet.ecommons.workbench.search.ui.LineElement;

import org.eclipse.statet.r.core.model.RSourceUnit;


public class RElementMatch extends Match {
	
	
	private final LineElement<RSourceUnit> group;
	
	
	public RElementMatch(final LineElement<RSourceUnit> group,
			final int offset, final int length,
			final boolean isWriteAccess) {
		super(group.getElement(), UNIT_CHARACTER, offset, length);
		
		this.group= group;
	}
	
	
	public LineElement<RSourceUnit> getMatchGroup() {
		return this.group;
	}
	
}
