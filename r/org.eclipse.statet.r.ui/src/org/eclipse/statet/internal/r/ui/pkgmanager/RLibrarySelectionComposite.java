/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.r.ui.REnvLabelProvider;
import org.eclipse.statet.rj.renv.core.RLibGroup;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.runtime.RLibLocationInfo;
import org.eclipse.statet.rj.renv.runtime.RuntimeRLibPaths;


public class RLibrarySelectionComposite extends Composite {
	
	
	public static class Validator implements IValidator<Object> {
		
		
		private RuntimeRLibPaths libPaths;
		
		private boolean isWritableRequired;
		
		
		public void setWritable(final boolean required) {
			this.isWritableRequired= required;
		}
		
		
		@Override
		public IStatus validate(final Object value) {
			if (!(value instanceof RLibLocation)) {
				return ValidationStatus.error("No library location selected where to install the package to.");
			}
//			final IRLibraryLocation location= (IRLibraryLocation) value;
//			if (location.isReadOnly() && !location.getDirectoryPath().startsWith("${workspace_loc")) { //$NON-NLS-1$
//				return ValidationStatus.warning("The selected library location is not recommend for user packages.");
//			}
			return ValidationStatus.ok();
		}
		
		private boolean matchesRequired(final RLibLocation libLocation) {
			if (!this.isWritableRequired) {
				return true;
			}
			final RLibLocationInfo info= this.libPaths.getInfo(libLocation);
			return (info != null && info.isWritable());
		}
		
	}
	
	
	private TreeViewer treeViewer;
	
	private final Validator validator;
	
	
	public RLibrarySelectionComposite(final Composite parent) {
		super(parent, SWT.NONE);
		
		this.validator= new Validator();
		setLayout(new FillLayout());
		createComponents(this);
	}
	
	
	private void createComponents(final Composite parent) {
		final Tree tree= new Tree(parent, SWT.BORDER | SWT.SINGLE | SWT.FULL_SELECTION);
		final TreeViewer viewer= new TreeViewer(tree);
		this.treeViewer= viewer;
		
		viewer.setLabelProvider(new REnvLabelProvider());
		viewer.setContentProvider(new ITreeContentProvider() {
			@Override
			public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
			}
			@Override
			public void dispose() {
			}
			@Override
			public Object[] getElements(final Object inputElement) {
				return ((List<?>) inputElement).toArray();
			}
			@Override
			public Object getParent(final Object element) {
				return null;
			}
			@Override
			public boolean hasChildren(final Object element) {
				if (element instanceof RLibGroup) {
					for (final RLibLocation location : ((RLibGroup) element).getLibLocations()) {
						if (RLibrarySelectionComposite.this.validator.matchesRequired(location)) {
							return true;
						}
					}
				}
				return false;
			}
			@Override
			public Object[] getChildren(final Object parentElement) {
				if (parentElement instanceof RLibGroup) {
					final List<? extends RLibLocation> all= ((RLibGroup) parentElement).getLibLocations();
					final List<RLibLocation> list= new ArrayList<>(all.size());
					for (final RLibLocation location : all) {
						if (RLibrarySelectionComposite.this.validator.matchesRequired(location)) {
							list.add(location);
						}
					}
					return list.toArray();
				}
				return null;
			}
		});
		viewer.setAutoExpandLevel(TreeViewer.ALL_LEVELS);
	}
	
	public GridData createGD() {
		final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.heightHint= LayoutUtils.hintHeight(this.treeViewer.getTree(), 8);
		return gd;
	}
	
	
	public Validator getValidator() {
		return this.validator;
	}
	
	public void setInput(final RuntimeRLibPaths libPaths) {
		this.validator.libPaths= libPaths;
		this.treeViewer.setInput(libPaths.getRLibGroups());
	}
	
	public TreeViewer getSelectionViewer() {
		return this.treeViewer;
	}
	
}
