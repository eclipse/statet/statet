/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ltk.ui.wizards.NewProjectWizardPage;


/**
 * The "New" wizard page allows setting the container for
 * the new file as well as the file name. The page
 * will only accept file name without the extension or
 * with the extension that matches the expected one (r).
 */
public class NewRProjectWizardPage extends NewProjectWizardPage {
	
	
	private final IStructuredSelection selection;
	
	
	/**
	 * Constructor.
	 */
	public NewRProjectWizardPage(final IStructuredSelection selection, final String title) {
		super("NewRProjectWizardPage"); //$NON-NLS-1$
		this.selection= selection;
		
		setTitle((title != null) ? title : Messages.NewRProjectWizardPage_title);
		setDescription(Messages.NewRProjectWizardPage_description);
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		super.createControl(parent);
		final Composite composite= (Composite) getControl();
		
		createWorkingSetGroup(composite, this.selection, new String[] {
				"org.eclipse.ui.resourceWorkingSetPage", //$NON-NLS-1$
				});
	}
	
}
