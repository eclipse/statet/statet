/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.tools;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String LoadData_Wizard_title;
	public static String LoadData_Wizard_SelectPage_title;
	public static String LoadData_Wizard_SelectPage_description;
	public static String LoadData_Wizard_File_label;
	public static String LoadData_Wizard_File_RImages_name;
	public static String LoadData_Runnable_label;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
