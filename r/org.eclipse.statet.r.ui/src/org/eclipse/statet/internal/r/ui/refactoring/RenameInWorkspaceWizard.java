/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.refactoring;

import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.typed.PojoProperties;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.databinding.wizard.WizardPageSupport;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.ltk.ui.refactoring.RefactoringBasedStatus;
import org.eclipse.statet.r.core.refactoring.RElementSearchProcessor.Mode;
import org.eclipse.statet.r.core.refactoring.RRefactoringAdapter;
import org.eclipse.statet.r.core.refactoring.RenameInWorkspaceRefactoring;


public class RenameInWorkspaceWizard extends RefactoringWizard {
	
	
	private static class InputPage extends UserInputWizardPage {
		
		
		public static final String PAGE_NAME= "RenameInWorkspace.InputPage"; //$NON-NLS-1$
		
		
		private Text variableNameControl;
		
		
		public InputPage() {
			super(PAGE_NAME);
		}
		
		@Override
		protected RenameInWorkspaceRefactoring getRefactoring() {
			return (RenameInWorkspaceRefactoring) super.getRefactoring();
		}
		
		@Override
		public void createControl(final Composite parent) {
			final Composite composite= new Composite(parent, SWT.NONE);
			composite.setLayout(LayoutUtils.newDialogGrid(2));
			setControl(composite);
			
			final RenameInWorkspaceRefactoring refactoring= getRefactoring();
			final String name= refactoring.getNewName();
			
			{	final Label label= new Label(composite, SWT.NONE);
				label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
				label.setText(NLS.bind(Messages.RenameInWorkspace_Wizard_header, 
						RRefactoringAdapter.getQuotedIdentifier(name) ));
				label.setFont(JFaceResources.getBannerFont());
			}
			
			LayoutUtils.addSmallFiller(composite, false);
			
			{	final Label label= new Label(composite, SWT.NONE);
				label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
				label.setText(Messages.RenameInWorkspace_Wizard_VariableName_label);
				
				this.variableNameControl= new Text(composite, SWT.BORDER);
				this.variableNameControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
				this.variableNameControl.setFont(JFaceResources.getTextFont());
			}
			
			{	final Group group= new Group(composite, SWT.NONE);
				group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
				group.setText("Scope:");
				group.setLayout(LayoutUtils.newGroupGrid(1));
				
				final List<Mode> modes= refactoring.getAvailableModes();
				if (modes.contains(Mode.WORKSPACE)) {
					final Button button= new Button(group, SWT.RADIO);
					button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
					button.setText("&Workspace (complete project tree)");
					button.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(final SelectionEvent e) {
							getRefactoring().setMode(Mode.WORKSPACE);
						}
					});
					if (refactoring.getMode() == Mode.WORKSPACE) {
						button.setSelection(true);
					}
				}
				if (modes.contains(Mode.CURRENT_AND_REFERENCING_PROJECTS)) {
					final Button button= new Button(group, SWT.RADIO);
					button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
					button.setText("Current and &referencing projects");
					button.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(final SelectionEvent e) {
							getRefactoring().setMode(Mode.CURRENT_AND_REFERENCING_PROJECTS);
						}
					});
					if (refactoring.getMode() == Mode.CURRENT_AND_REFERENCING_PROJECTS) {
						button.setSelection(true);
					}
				}
				if (modes.contains(Mode.CURRENT_PROJECT)) {
					final Button button= new Button(group, SWT.RADIO);
					button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
					button.setText("C&urrent project");
					button.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(final SelectionEvent e) {
							getRefactoring().setMode(Mode.CURRENT_PROJECT);
						}
					});
					if (refactoring.getMode() == Mode.CURRENT_PROJECT) {
						button.setSelection(true);
					}
				}
				if (modes.contains(Mode.CURRENT_FILE)) {
					final Button button= new Button(group, SWT.RADIO);
					button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
					button.setText("Current &file");
					button.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(final SelectionEvent e) {
							getRefactoring().setMode(Mode.CURRENT_FILE);
						}
					});
					if (refactoring.getMode() == Mode.CURRENT_FILE) {
						button.setSelection(true);
					}
				}
				if (modes.contains(Mode.LOCAL_FRAME)) {
					final Button button= new Button(group, SWT.RADIO);
					button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
					button.setText("&Local frame.");
					button.addSelectionListener(new SelectionAdapter() {
						@Override
						public void widgetSelected(final SelectionEvent e) {
							getRefactoring().setMode(Mode.LOCAL_FRAME);
						}
					});
					if (refactoring.getMode() == Mode.LOCAL_FRAME) {
						button.setSelection(true);
					}
				}
			}
			
			LayoutUtils.addSmallFiller(composite, false);
			Dialog.applyDialogFont(composite);
			
			initBindings();
			this.variableNameControl.selectAll();
//			PlatformUI.getWorkbench().getHelpSystem().setHelp(getControl(),);
		}
		
		protected void initBindings() {
			final Realm realm= Realm.getDefault();
			final DataBindingContext dbc= new DataBindingContext(realm);
			
			addBindings(dbc, realm);
			WizardPageSupport.create(this, dbc);
		}
		
		protected void addBindings(final DataBindingContext dbc, final Realm realm) {
			dbc.bindValue(
					WidgetProperties.text(SWT.Modify)
							.observe(this.variableNameControl),
					PojoProperties.value("newName", String.class) //$NON-NLS-1$
							.observe(getRefactoring()),
					new UpdateValueStrategy<String, String>()
							.setAfterGetValidator((final String text) -> {
								final RenameInWorkspaceRefactoring refactoring= getRefactoring();
								final RefactoringStatus status= refactoring.checkNewName(text);
								if (status.isOK() && refactoring.getCurrentName().equals(text)) {
									return Status.CANCEL_STATUS;
								}
								return new RefactoringBasedStatus(status);
							}),
					null );
		}
		
		@Override
		public void setVisible(final boolean visible) {
			super.setVisible(visible);
			this.variableNameControl.setFocus();
		}
		
	}
	
	
	public RenameInWorkspaceWizard(final RenameInWorkspaceRefactoring ref) {
		super(ref, DIALOG_BASED_USER_INTERFACE | PREVIEW_EXPAND_FIRST_NODE | NO_BACK_BUTTON_ON_STATUS_DIALOG);
		setDefaultPageTitle(Messages.RenameInWorkspace_Wizard_title);
	}
	
	
	@Override
	protected void addUserInputPages() {
		addPage(new InputPage());
	}
	
}
