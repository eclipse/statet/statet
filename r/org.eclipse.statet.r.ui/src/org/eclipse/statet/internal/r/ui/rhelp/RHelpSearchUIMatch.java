/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import org.eclipse.search.ui.text.Match;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rhelp.core.RHelpSearchMatch;


@NonNullByDefault
public class RHelpSearchUIMatch extends Match implements Comparable<RHelpSearchUIMatch> {
	
	
	private final RHelpSearchMatch coreMatch;
	
	
	public RHelpSearchUIMatch(final RHelpSearchMatch rMatch) {
		super(rMatch.getPage().getPackage(), Match.UNIT_CHARACTER, 0, 0);
		this.coreMatch= rMatch;
	}
	
	
	public RHelpSearchMatch getRHelpMatch() {
		return this.coreMatch;
	}
	
	
	@Override
	public int hashCode() {
		return this.coreMatch.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		return ((obj instanceof RHelpSearchUIMatch)
				&& this.coreMatch.equals(((RHelpSearchUIMatch) obj).getRHelpMatch()) );
	}
	
	@Override
	public int compareTo(final RHelpSearchUIMatch o) {
		return this.coreMatch.getPage().compareTo(o.getRHelpMatch().getPage());
	}
	
	
	@Override
	public String toString() {
		return this.coreMatch.toString();
	}
	
}
