/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import static org.eclipse.statet.r.ui.RUI.BUNDLE_ID;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.editors.text.templates.ContributionContextTypeRegistry;
import org.eclipse.ui.editors.text.templates.ContributionTemplateStore;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.ecommons.preferences.PreferencesManageListener;
import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.ui.settings.JFaceTextStyleManager;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;
import org.eclipse.statet.ecommons.ui.jface.resource.ImageDescriptorRegistry;
import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.internal.r.ui.editors.RFragmentDocumentProvider;
import org.eclipse.statet.internal.r.ui.editors.r.RDocumentProvider;
import org.eclipse.statet.internal.r.ui.editors.rdoc.RdDocumentProvider;
import org.eclipse.statet.internal.r.ui.graphics.ShowGraphicViewListener;
import org.eclipse.statet.internal.r.ui.wizards.RPkgTemplateContextType;
import org.eclipse.statet.ltk.ui.LtkUI;
import org.eclipse.statet.ltk.ui.sourceediting.SourceDocumentProvider;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHoverRegistry;
import org.eclipse.statet.ltk.ui.templates.EnhTemplateStore;
import org.eclipse.statet.ltk.ui.util.CombinedPreferenceStore;
import org.eclipse.statet.nico.core.ConsoleInstanceScope;
import org.eclipse.statet.nico.core.NicoCore;
import org.eclipse.statet.r.codegeneration.RCodeTemplateContextType;
import org.eclipse.statet.r.codegeneration.RdCodeTemplateContextType;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.source.doc.RPkgDescrDocumentSetupParticipant;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.RUIPreferenceConstants;
import org.eclipse.statet.r.ui.editors.REditorBuild;
import org.eclipse.statet.r.ui.editors.REditorOptions;
import org.eclipse.statet.r.ui.editors.templates.REditorTemplateContextType;
import org.eclipse.statet.r.ui.rhelp.RHelpUI;
import org.eclipse.statet.rj.eclient.graphics.ERGraphicsManager;
import org.eclipse.statet.rj.eclient.graphics.comclient.ERGraphicFactory;


/**
 * The main plugin class to be used in the desktop.
 */
public class RUIPlugin extends AbstractUIPlugin {
	
	
	public static final int INTERNAL_ERROR= 100;
	public static final int IO_ERROR= 101;
	
	
	public static final String IMG_WIZBAN_NEWRDFILE=            BUNDLE_ID + "/images/wizban/new.rd-file"; //$NON-NLS-1$
	public static final String IMG_WIZBAN_NEWRFILE=             BUNDLE_ID + "/images/wizban/new.r-file"; //$NON-NLS-1$
	public static final String IMG_WIZBAN_NEW_RPROJECT=         BUNDLE_ID + "/images/wizban/new.r_project"; //$NON-NLS-1$
	public static final String IMG_WIZBAN_NEW_RPKGPROJECT=      BUNDLE_ID + "/images/wizban/new.rpkg_project"; //$NON-NLS-1$
	
	public static final String PAGER_VIEW_IMAGE_ID=             BUNDLE_ID + "/images/view/pager"; //$NON-NLS-1$
	
	public static final String IMG_LOCTOOL_FILTER_GENERAL=      BUNDLE_ID + "/images/loctool/filter-general"; //$NON-NLS-1$
	public static final String IMG_LOCTOOL_FILTER_LOCAL=        BUNDLE_ID + "/images/loctool/filter-local"; //$NON-NLS-1$
	
	public static final String IMG_LOCTOOL_REFRESH_RECOMMENDED= BUNDLE_ID + "/images/loctool/refresh.recommended"; //$NON-NLS-1$
	
	private static final String R_CODE_TEMPLATES_KEY = "RCodeGen.templates"; //$NON-NLS-1$
	private static final String RD_CODE_TEMPLATES_KEY= "RdCodeGen.templates"; //$NON-NLS-1$
	private static final String RPKG_CODE_TEMPLATES_KEY= "RPkgCodeGen.templates"; //$NON-NLS-1$
	private static final String R_EDITOR_TEMPLATES_KEY = "REditor.templates"; //$NON-NLS-1$
	
	static final boolean DEBUG= Platform.getDebugBoolean(BUNDLE_ID + "/debug"); //$NON-NLS-1$
	
	
	public static boolean isSearchPlugInActivated() {
		return Platform.getBundle("org.eclipse.search").getState() == Bundle.ACTIVE; //$NON-NLS-1$
	}
	
	
	private static RUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static RUIPlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	public static final void log(final org.eclipse.statet.jcommons.status.Status status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(EStatusUtils.convert(status));
		}
	}
	
	public static void logError(final int code, final String message, final Throwable e) {
		log(new Status(IStatus.ERROR, BUNDLE_ID, code, message, e));
	}
	
	public static void logError(final String message, final Throwable e) {
		log(new Status(IStatus.ERROR, BUNDLE_ID, message, e));
	}
	
	public static void logUncriticalError(final Throwable e) {
		if (DEBUG) {
			log(new Status(IStatus.ERROR, BUNDLE_ID, "An unexpected error occured.", e));
		}
	}
	
	
	private boolean started;
	
	private ImageDescriptorRegistry imageDescriptorRegistry;
	
	private IPreferenceStore editorPreferenceStore;
	
	private @Nullable RDocumentProvider rDocumentProvider;
	private @Nullable RFragmentDocumentProvider rFragmentDocumentProvider;
	private @Nullable RdDocumentProvider rdDocumentProvider;
	private @Nullable SourceDocumentProvider<RSourceUnit> rPkgDescrDocumentProvider;
	
	private JFaceTextStyleManager rTextStyles;
	private JFaceTextStyleManager rdTextStyles;
	
	private RIdentifierGroups identifierGroups;
	private REditorOptions editorSettings;
	private REditorOptions consoleSettings;
	private List<PreferencesManageListener> prefUpdaters;
	
	private TemplateStore rCodeTemplatesStore;
	private ContextTypeRegistry rCodeTemplatesContextTypeRegistry;
	private TemplateStore rdCodeTemplatesStore;
	private ContextTypeRegistry rdCodeTemplatesContextTypeRegistry;
	private TemplateStore rPkgCodeTemplatesStore;
	private ContextTypeRegistry rPkgCodeTemplatesContextTypeRegistry;
	
	private EnhTemplateStore rEditorTemplatesStore;
	private ContextTypeRegistry rEditorContextTypeRegistry;
	
	private ContentAssistComputerRegistry rEditorContentAssistRegistry;
	private ContentAssistComputerRegistry rConsoleContentAssistRegistry;
	private ContentAssistComputerRegistry rPkgDescrContentAssistRegistry;
	private InfoHoverRegistry rEditorInfoHoverRegistry;
	
	private ERGraphicFactory commonRGraphicFactory;
	
	private List<Disposable> disposables;
	
	
	/**
	 * The constructor.
	 */
	public RUIPlugin() {
		instance= this;
	}
	
	
	/**
	 * This method is called upon plug-in activation
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		this.prefUpdaters= new ArrayList<>();
		this.disposables= new ArrayList<>();
		
		super.start(context);
		
		this.started= true;
	}
	
	/**
	 * This method is called when the plug-in is stopped
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
				this.imageDescriptorRegistry= null;
			}
			
			for (final Disposable d : this.disposables) {
				try {
					d.dispose();
				}
				catch (final Throwable e) {
					logError("Error occured when dispose module", e); //$NON-NLS-1$
				}
			}
			this.disposables= null;
			this.rDocumentProvider= null;
			this.rdDocumentProvider= null;
			this.editorPreferenceStore= null;
			this.rCodeTemplatesStore= null;
			this.rCodeTemplatesContextTypeRegistry= null;
			this.rdCodeTemplatesStore= null;
			this.rdCodeTemplatesContextTypeRegistry= null;
			this.rPkgCodeTemplatesStore= null;
			this.rPkgCodeTemplatesContextTypeRegistry= null;
			this.rEditorTemplatesStore= null;
			this.rEditorContextTypeRegistry= null;
			this.rEditorContentAssistRegistry= null;
			this.rConsoleContentAssistRegistry= null;
			this.rEditorInfoHoverRegistry= null;
			final Iterator<PreferencesManageListener> iter= this.prefUpdaters.iterator();
			while (iter.hasNext()) {
				iter.next().dispose();
			}
			this.prefUpdaters.clear();
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		util.register(IMG_WIZBAN_NEW_RPROJECT, ImageRegistryUtil.T_WIZBAN, "new-r_project.png"); //$NON-NLS-1$
		util.register(IMG_WIZBAN_NEW_RPKGPROJECT, ImageRegistryUtil.T_WIZBAN, "new-rpkg_project.png"); //$NON-NLS-1$
		util.register(IMG_WIZBAN_NEWRFILE, ImageRegistryUtil.T_WIZBAN, "new_r-file.png");  //$NON-NLS-1$
		util.register(IMG_WIZBAN_NEWRDFILE, ImageRegistryUtil.T_WIZBAN, "new_rd-file.png");  //$NON-NLS-1$
		
		util.register(RUI.IMG_OBJ_R_SCRIPT, ImageRegistryUtil.T_OBJ, "r-file_obj.gif"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_R_RUNTIME_ENV, ImageRegistryUtil.T_OBJ, "r_env.png");  //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_R_REMOTE_ENV, ImageRegistryUtil.T_OBJ, "r_env-remote.png");  //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_R_PACKAGE, ImageRegistryUtil.T_OBJ, "package.png");  //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_R_PACKAGE_NA, ImageRegistryUtil.T_OBJ, "package-nota.png");  //$NON-NLS-1$
		
		util.register(RHelpUI.RHELP_PAGE_OBJ_IMAGE_ID, ImageRegistryUtil.T_OBJ, "rhelp-page.png");  //$NON-NLS-1$
		util.register(RHelpUI.RHELP_PAGE_INTERNAL_OBJ_IMAGE_ID, ImageRegistryUtil.T_OBJ, "rhelp-page-internal.png");  //$NON-NLS-1$
		util.register(RHelpUI.RHELP_TOPIC_OBJ_IMAGE_ID, ImageRegistryUtil.T_OBJ, "rhelp-topic.png");  //$NON-NLS-1$
		util.register(RHelpUI.RHELP_TOPIC_INTERNAL_OBJ_IMAGE_ID, ImageRegistryUtil.T_OBJ, "rhelp-topic-internal.png");  //$NON-NLS-1$
		
		util.register(RHelpUI.RHELP_SEARCH_OBJ_IMAGE_ID, ImageRegistryUtil.T_TOOL, "rhelp-search.png");  //$NON-NLS-1$
		
		util.register(RHelpUI.RHELP_VIEW_IMAGE_ID, ImageRegistryUtil.T_VIEW, "rhelp.png"); //$NON-NLS-1$
		
		util.register(PAGER_VIEW_IMAGE_ID, ImageRegistryUtil.T_VIEW, "pager.png"); //$NON-NLS-1$
		
		util.register(RUI.IMG_OBJ_COMMON_FUNCTION, ImageRegistryUtil.T_OBJ, "function.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COMMON_LOCAL_FUNCTION, ImageRegistryUtil.T_OBJ, "function-local.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_GENERIC_FUNCTION, ImageRegistryUtil.T_OBJ, "generic_function.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_METHOD, ImageRegistryUtil.T_OBJ, "method.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_GENERAL_VARIABLE, ImageRegistryUtil.T_OBJ, "var.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_GENERAL_LOCAL_VARIABLE, ImageRegistryUtil.T_OBJ, "var-local.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_SLOT, ImageRegistryUtil.T_OBJ, "slot.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_PACKAGEENV, ImageRegistryUtil.T_OBJ, "packageenv.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_GLOBALENV, ImageRegistryUtil.T_OBJ, "globalenv.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_EMPTYENV, ImageRegistryUtil.T_OBJ, "emptyenv.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_OTHERENV, ImageRegistryUtil.T_OBJ, "otherenv.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_DATAFRAME, ImageRegistryUtil.T_OBJ, "dataframe.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_DATAFRAME_COLUMN, ImageRegistryUtil.T_OBJ, "dataframe_col.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_LIST, ImageRegistryUtil.T_OBJ, "list.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_VECTOR, ImageRegistryUtil.T_OBJ, "vector.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_ARRAY, ImageRegistryUtil.T_OBJ, "array.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_S4OBJ, ImageRegistryUtil.T_OBJ, "s4obj.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_S4OBJ_VECTOR, ImageRegistryUtil.T_OBJ, "s4obj-vector.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_S4OBJ_DATAFRAME_COLUMN, ImageRegistryUtil.T_OBJ, "s4obj-dataframe_col.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_NULL, ImageRegistryUtil.T_OBJ, "null.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_MISSING, ImageRegistryUtil.T_OBJ, "missing.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_PROMISE, ImageRegistryUtil.T_OBJ, "promise.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_ARGUMENT_ASSIGN, ImageRegistryUtil.T_OBJ, "arg-assign.png"); //$NON-NLS-1$
		
		util.register(RUI.IMG_OBJ_COL_LOGI, ImageRegistryUtil.T_OBJ, "col-logi.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COL_INT, ImageRegistryUtil.T_OBJ, "col-int.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COL_NUM, ImageRegistryUtil.T_OBJ, "col-num.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COL_CPLX, ImageRegistryUtil.T_OBJ, "col-cplx.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COL_CHAR, ImageRegistryUtil.T_OBJ, "col-char.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COL_RAW, ImageRegistryUtil.T_OBJ, "col-raw.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COL_FACTOR, ImageRegistryUtil.T_OBJ, "col-factor.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COL_DATE, ImageRegistryUtil.T_OBJ, "col-date.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_COL_DATETIME, ImageRegistryUtil.T_OBJ, "col-datetime.png"); //$NON-NLS-1$
		
		util.register(RUI.IMG_OBJ_LIBRARY_GROUP, ImageRegistryUtil.T_OBJ, "library.png"); //$NON-NLS-1$
		util.register(RUI.IMG_OBJ_LIBRARY_LOCATION, ImageRegistryUtil.T_OBJ, "package_folder.png"); //$NON-NLS-1$
		
		util.register(RUI.IMG_TOOL_ASSIST_TO_PIPE_FORWARD);
		
		util.register(IMG_LOCTOOL_FILTER_GENERAL);
		util.register(IMG_LOCTOOL_FILTER_LOCAL);
		util.register(RUI.IMG_LOCTOOL_SORT_PACKAGE);
		
		util.register(IMG_LOCTOOL_REFRESH_RECOMMENDED, ImageRegistryUtil.T_LOCTOOL, "refresh-rec.png"); //$NON-NLS-1$
	}
	
	public synchronized ImageDescriptorRegistry getImageDescriptorRegistry() {
		if (this.imageDescriptorRegistry == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.imageDescriptorRegistry= new ImageDescriptorRegistry();
		}
		return this.imageDescriptorRegistry;
	}
	
	
	public synchronized RDocumentProvider getRDocumentProvider() {
		var documentProvider= this.rDocumentProvider;
		if (documentProvider == null) {
			documentProvider= new RDocumentProvider();
			this.rDocumentProvider= documentProvider;
		}
		return documentProvider;
	}
	
	public synchronized RFragmentDocumentProvider getRFragmentDocumentProvider() {
		var documentProvider= this.rFragmentDocumentProvider;
		if (documentProvider == null) {
			documentProvider= new RFragmentDocumentProvider();
			this.rFragmentDocumentProvider= documentProvider;
		}
		return documentProvider;
	}
	
	public synchronized RdDocumentProvider getRdDocumentProvider() {
		var documentProvider= this.rdDocumentProvider;
		if (documentProvider == null) {
			documentProvider= new RdDocumentProvider();
			this.rdDocumentProvider= documentProvider;
		}
		return documentProvider;
	}
	
	public synchronized SourceDocumentProvider<RSourceUnit> getRPkgDescrDocumentProvider() {
		var documentProvider= this.rPkgDescrDocumentProvider;
		if (documentProvider == null) {
			documentProvider= new SourceDocumentProvider<>(RModel.RPKG_DESCRIPTION_TYPE_ID,
					new RPkgDescrDocumentSetupParticipant(),
					REditorBuild.R_ISSUE_TYPE_SET );
			this.rPkgDescrDocumentProvider= documentProvider;
		}
		return documentProvider;
	}
	
	
	public synchronized IPreferenceStore getEditorPreferenceStore() {
		var preferenceStore= this.editorPreferenceStore;
		if (preferenceStore == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			preferenceStore= CombinedPreferenceStore.createStore(
					getPreferenceStore(),
					LtkUI.getPreferenceStore(),
					EditorsUI.getPreferenceStore() );
			this.editorPreferenceStore= preferenceStore;
		}
		return preferenceStore;
	}
	
	public synchronized RIdentifierGroups getRIdentifierGroups() {
		if (this.identifierGroups == null) {
			this.identifierGroups= new RIdentifierGroups();
			this.prefUpdaters.add(new PreferencesManageListener(
					this.identifierGroups, EPreferences.getInstancePrefs(), RIdentifierGroups.GROUP_ID));
		}
		return this.identifierGroups;
	}
	
	public synchronized REditorOptions getREditorSettings(final PreferenceAccess prefs) {
		final ImList<IScopeContext> contexts= prefs.getPreferenceContexts();
		for (final IScopeContext context : contexts) {
			if (context.getName().equals(ConsoleInstanceScope.SCOPE)) {
				if (this.consoleSettings== null) {
					this.consoleSettings= new REditorOptions(1);
					this.prefUpdaters.add(new PreferencesManageListener(
							this.consoleSettings, NicoCore.getInstanceConsolePreferences(), REditorOptions.SMARTINSERT_GROUP_ID));
				}
				return this.consoleSettings;
			}
			if (context.getName().equals(InstanceScope.SCOPE)) {
				if (this.editorSettings == null) {
					this.editorSettings= new REditorOptions(1);
					this.prefUpdaters.add(new PreferencesManageListener(
							this.editorSettings, EPreferences.getInstancePrefs(), REditorOptions.SMARTINSERT_GROUP_ID));
				}
				return this.editorSettings;
			}
		}
		return null;
	}
	
	public synchronized PreferenceStoreTextStyleManager<TextAttribute> getRTextStyles() {
		if (this.rTextStyles == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.rTextStyles= new JFaceTextStyleManager(getPreferenceStore(),
					RUIPreferenceConstants.R.TS_GROUP_ID );
			PreferencesUtil.getSettingsChangeNotifier().addManageListener(this.rTextStyles);
		}
		return this.rTextStyles;
	}
	
	public synchronized PreferenceStoreTextStyleManager<TextAttribute> getRdTextStyles() {
		if (this.rdTextStyles == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.rdTextStyles= new JFaceTextStyleManager(getPreferenceStore(),
					RUIPreferenceConstants.Rd.TS_GROUP_ID );
			PreferencesUtil.getSettingsChangeNotifier().addManageListener(this.rdTextStyles);
		}
		return this.rdTextStyles;
	}
	
	/**
	 * Returns the template context type registry for the code generation
	 * templates.
	 * 
	 * @return the template context type registry
	 */
	public synchronized ContextTypeRegistry getRCodeGenerationTemplateContextRegistry() {
		if (this.rCodeTemplatesContextTypeRegistry == null) {
			this.rCodeTemplatesContextTypeRegistry= new ContributionContextTypeRegistry();
			
			RCodeTemplateContextType.registerContextTypes(this.rCodeTemplatesContextTypeRegistry);
		}
		return this.rCodeTemplatesContextTypeRegistry;
	}
	
	/**
	 * Returns the template store for the code generation templates.
	 * 
	 * @return the template store
	 */
	public synchronized TemplateStore getRCodeGenerationTemplateStore() {
		if (this.rCodeTemplatesStore == null) {
			this.rCodeTemplatesStore= new ContributionTemplateStore(
					getRCodeGenerationTemplateContextRegistry(), getPreferenceStore(),
					R_CODE_TEMPLATES_KEY );
			try {
				this.rCodeTemplatesStore.load();
			}
			catch (final IOException e) {
				RUIPlugin.logError(IO_ERROR, "Error occured when loading 'R code generation' template store.", e); 
			}
		}
		return this.rCodeTemplatesStore;
	}
	
	/**
	 * Returns the template context type registry for the code generation
	 * templates.
	 * 
	 * @return the template context type registry
	 */
	public synchronized ContextTypeRegistry getRdCodeGenerationTemplateContextRegistry() {
		if (this.rdCodeTemplatesContextTypeRegistry == null) {
			this.rdCodeTemplatesContextTypeRegistry= new ContributionContextTypeRegistry();
			
			RdCodeTemplateContextType.registerContextTypes(this.rdCodeTemplatesContextTypeRegistry);
		}
		return this.rdCodeTemplatesContextTypeRegistry;
	}
	
	/**
	 * Returns the template store for the code generation templates.
	 * 
	 * @return the template store
	 */
	public synchronized TemplateStore getRdCodeGenerationTemplateStore() {
		if (this.rdCodeTemplatesStore == null) {
			this.rdCodeTemplatesStore= new ContributionTemplateStore(
					getRdCodeGenerationTemplateContextRegistry(), getPreferenceStore(),
					RD_CODE_TEMPLATES_KEY );
			try {
				this.rdCodeTemplatesStore.load();
			}
			catch (final IOException e) {
				RUIPlugin.logError(IO_ERROR, "Error occured when loading 'Rd code generation' template store.", e); 
			}
		}
		return this.rdCodeTemplatesStore;
	}
	
	/**
	 * Returns the template context type registry for the code generation
	 * templates.
	 * 
	 * @return the template context type registry
	 */
	public synchronized ContextTypeRegistry getRPkgCodeGenerationTemplateContextRegistry() {
		if (this.rPkgCodeTemplatesContextTypeRegistry == null) {
			this.rPkgCodeTemplatesContextTypeRegistry= new ContributionContextTypeRegistry();
			
			RPkgTemplateContextType.registerContextTypes(this.rPkgCodeTemplatesContextTypeRegistry);
		}
		return this.rPkgCodeTemplatesContextTypeRegistry;
	}
	
	/**
	 * Returns the template store for the code generation templates.
	 * 
	 * @return the template store
	 */
	public synchronized TemplateStore getRPkgCodeGenerationTemplateStore() {
		if (this.rPkgCodeTemplatesStore == null) {
			this.rPkgCodeTemplatesStore= new ContributionTemplateStore(
					getRPkgCodeGenerationTemplateContextRegistry(), getPreferenceStore(),
					RPKG_CODE_TEMPLATES_KEY );
			try {
				this.rPkgCodeTemplatesStore.load();
			}
			catch (final IOException e) {
				RUIPlugin.logError(IO_ERROR, "Error occured when loading 'R package code generation' template store.", e); 
			}
		}
		return this.rPkgCodeTemplatesStore;
	}
	
	/**
	 * Returns the template context type registry for the code generation
	 * templates.
	 * 
	 * @return the template context type registry
	 */
	public synchronized ContextTypeRegistry getREditorTemplateContextRegistry() {
		if (this.rEditorContextTypeRegistry == null) {
			this.rEditorContextTypeRegistry= new ContributionContextTypeRegistry();
			
			REditorTemplateContextType.registerContextTypes(this.rEditorContextTypeRegistry);
		}
		return this.rEditorContextTypeRegistry;
	}
	
	/**
	 * Returns the template store for the code generation templates.
	 * 
	 * @return the template store
	 */
	public synchronized EnhTemplateStore getREditorTemplateStore() {
		if (this.rEditorTemplatesStore == null) {
			this.rEditorTemplatesStore= new EnhTemplateStore(
					getREditorTemplateContextRegistry(), getPreferenceStore(), R_EDITOR_TEMPLATES_KEY);
			try {
				this.rEditorTemplatesStore.load();
			} catch (final IOException e) {
				RUIPlugin.logError(IO_ERROR, "Error occured when loading 'R Editor' template store.", e); 
			}
		}
		return this.rEditorTemplatesStore;
	}
	
	public synchronized ContentAssistComputerRegistry getREditorContentAssistRegistry() {
		var contentAssistRegistry= this.rEditorContentAssistRegistry;
		if (contentAssistRegistry == null) {
			contentAssistRegistry= new ContentAssistComputerRegistry(
					RCore.R_CONTENT_ID,
					RUIPreferenceInitializer.R_EDITOR_NODE );
			this.disposables.add(contentAssistRegistry);
			this.rEditorContentAssistRegistry= contentAssistRegistry;
		}
		return contentAssistRegistry;
	}
	
	public synchronized ContentAssistComputerRegistry getRConsoleContentAssistRegistry() {
		var contentAssistRegistry= this.rConsoleContentAssistRegistry;
		if (contentAssistRegistry == null) {
			contentAssistRegistry= new ContentAssistComputerRegistry(
					RCore.R_CONTENT_ID + "Console",
					RUIPreferenceInitializer.R_CONSOLE_NODE );
			this.disposables.add(contentAssistRegistry);
			this.rConsoleContentAssistRegistry= contentAssistRegistry;
		}
		return contentAssistRegistry;
	}
	
	public synchronized ContentAssistComputerRegistry getRPkgDescrContentAssistRegistry() {
		var contentAssistRegistry= this.rPkgDescrContentAssistRegistry;
		if (contentAssistRegistry == null) {
			contentAssistRegistry= new ContentAssistComputerRegistry(
					RCore.RPKG_DESCRIPTION_CONTENT_ID,
					RUIPreferenceInitializer.R_CONSOLE_NODE );
			this.disposables.add(contentAssistRegistry);
			this.rPkgDescrContentAssistRegistry= contentAssistRegistry;
		}
		return contentAssistRegistry;
	}
	
	public synchronized InfoHoverRegistry getREditorInfoHoverRegistry() {
		if (this.rEditorInfoHoverRegistry == null) {
			this.rEditorInfoHoverRegistry= new InfoHoverRegistry(RCore.R_CONTENT_ID,
					RUIPreferenceInitializer.R_EDITOR_NODE, RUIPreferenceInitializer.REDITOR_HOVER_GROUP_ID);
			this.disposables.add(this.rEditorInfoHoverRegistry);
		}
		return this.rEditorInfoHoverRegistry;
	}
	
	
	public synchronized ERGraphicFactory getCommonRGraphicFactory() {
		if (this.commonRGraphicFactory == null) {
			final ERGraphicsManager manager= this.commonRGraphicFactory= new ERGraphicFactory();
			manager.addListener(new ShowGraphicViewListener());
		}
		return this.commonRGraphicFactory;
	}
	
	
	public void registerPluginDisposable(final Disposable d) {
		final List<Disposable> disposables= this.disposables;
		if (disposables != null) {
			disposables.add(d);
		}
		else {
			throw new IllegalStateException();
		}
	}
	
}
