/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilterview;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.dataeditor.AbstractRDataProvider;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class RStoreContentProvider implements IStructuredContentProvider {
	
	
	private RStore store;
	
	
	@Override
	public void inputChanged(final Viewer viewer, final @Nullable Object oldInput, final @Nullable Object newInput) {
		this.store= (RStore)newInput;
	}
	
	@Override
	public Object[] getElements(final @Nullable Object inputElement) {
		final Object[] array= this.store.toArray();
		for (int idx= 0; idx < array.length; idx++) {
			if (array[idx] == null) {
				array[idx]= AbstractRDataProvider.NA;
			}
		}
		return array;
	}
	
	@Override
	public void dispose() {
		this.store= null;
	}
	
}
