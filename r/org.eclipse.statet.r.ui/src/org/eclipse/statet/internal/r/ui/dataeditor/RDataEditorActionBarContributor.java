/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.dataeditor;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.StatusLineContributionItem;
import org.eclipse.ui.part.EditorActionBarContributor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RDataEditorActionBarContributor extends EditorActionBarContributor {
	
	
	private final StatusLineContributionItem contributionItem;
	
	
	public RDataEditorActionBarContributor() {
		this.contributionItem= new StatusLineContributionItem("data.dimension", 35);
	}
	
	
	@Override
	public void contributeToStatusLine(final IStatusLineManager statusLineManager) {
		super.contributeToStatusLine(statusLineManager);
		
		statusLineManager.add(this.contributionItem);
	}
	
	
	@Override
	public void dispose() {
		super.dispose();
		
		this.contributionItem.dispose();
	}
	
}
