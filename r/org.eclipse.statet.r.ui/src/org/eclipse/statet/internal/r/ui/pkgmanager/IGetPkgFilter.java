/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import java.util.Collections;
import java.util.Set;

import org.eclipse.statet.r.core.pkgmanager.IRPkgData;
import org.eclipse.statet.r.core.pkgmanager.IRPkgInfoAndData;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.runtime.RLibLocationInfo;
import org.eclipse.statet.rj.renv.runtime.RuntimeRLibPaths;


interface IGetPkgFilter {
	
	
	boolean exclude(IRPkgInfoAndData inst, IRPkgData avail);
	
}


class RequireInstFilter implements IGetPkgFilter {
	
	
	public RequireInstFilter() {
	}
	
	
	@Override
	public boolean exclude(final IRPkgInfoAndData inst, final IRPkgData avail) {
		return (inst == null);
	}
	
}


class LibSourceFilter implements IGetPkgFilter {
	
	static final Set<String> EXCLUDE_EPLUGIN= Collections.singleton(RLibLocation.EPLUGIN);
	
	
	private final Set<String> sources;
	
	public LibSourceFilter() {
		this(EXCLUDE_EPLUGIN);
	}
	
	public LibSourceFilter(final Set<String> sources) {
		this.sources= sources;
	}
	
	
	@Override
	public boolean exclude(final IRPkgInfoAndData inst, final IRPkgData avail) {
		return this.sources.contains(inst.getLibLocation().getSource());
	}
	
}


class ReadOnlyFilter implements IGetPkgFilter {
	
	
	private final RuntimeRLibPaths rLibPaths;
	
	
	public ReadOnlyFilter(final RuntimeRLibPaths rLibPaths) {
		this.rLibPaths= rLibPaths;
	}
	
	
	@Override
	public boolean exclude(final IRPkgInfoAndData inst, final IRPkgData avail) {
		final RLibLocationInfo info= this.rLibPaths.getInfo(inst.getLibLocation());
		return (info == null || !info.isWritable());
	}
	
}


class LaterVersionFilter implements IGetPkgFilter {
	
	
	public LaterVersionFilter() {
	}
	
	
	@Override
	public boolean exclude(final IRPkgInfoAndData inst, final IRPkgData avail) {
		return inst.getVersion().isGreaterEqualThan(avail.getVersion());
	}
	
}

class NotOlderVersionFilter implements IGetPkgFilter {
	
	
	public NotOlderVersionFilter() {
	}
	
	
	@Override
	public boolean exclude(final IRPkgInfoAndData inst, final IRPkgData avail) {
		return inst.getVersion().isSmallerThan(avail.getVersion());
	}
	
}
