/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rpkg;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.ROUND_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_ANY_CONTENT_CONSTRAINT;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.CharPairSet;

import org.eclipse.statet.ltk.text.core.BasicCharPairMatcher;
import org.eclipse.statet.ltk.text.core.CharPairMatcher;
import org.eclipse.statet.ltk.text.core.MultiContentSectionCharPairMatcher;
import org.eclipse.statet.r.core.source.doc.RPkgDescrDocumentContentInfo;
import org.eclipse.statet.r.core.source.util.RBracketPairMatcher;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;
import org.eclipse.statet.r.core.source.util.RPkgDescrHeuristicTokenScanner;


@NonNullByDefault
public class RPkgDescrBracketPairMatcher extends MultiContentSectionCharPairMatcher {
	
	
	private static final CharPairSet BRACKETS= new CharPairSet(
			ImCollections.newIdentityList(CURLY_BRACKETS, ROUND_BRACKETS, SQUARE_BRACKETS) );
	
	
	public RPkgDescrBracketPairMatcher() {
		super(RPkgDescrDocumentContentInfo.INSTANCE);
	}
	
	
	@Override
	protected @Nullable CharPairMatcher createHandler(final String sectionType) {
		switch (sectionType) {
		case RPkgDescrDocumentContentInfo.RPKG_DESCRIPTION:
			return new BasicCharPairMatcher(BRACKETS, getSections().getPartitioning(),
					RPKG_DESCR_ANY_CONTENT_CONSTRAINT,
					new RPkgDescrHeuristicTokenScanner(getSections()) );
		case RPkgDescrDocumentContentInfo.R:
			return new RBracketPairMatcher(
					RHeuristicTokenScanner.create(getSections()) );
		default:
			return null;
		}
	}
	
}
