/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import static org.eclipse.statet.r.core.model.RCoreFunctions.BASE_C_NAME;
import static org.eclipse.statet.r.core.model.RCoreFunctions.BASE_PACKAGE_NAME;
import static org.eclipse.statet.r.core.model.RFunctionSpec.AS_STRING;
import static org.eclipse.statet.r.core.model.RFunctionSpec.AS_SYMBOL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.text.core.SearchPattern;

import org.eclipse.statet.ecommons.text.core.FragmentDocument;
import org.eclipse.statet.ecommons.text.core.PartitionConstraint;
import org.eclipse.statet.ecommons.text.core.TextTokenScanner;

import org.eclipse.statet.internal.r.ui.FCallNamePattern;
import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.internal.r.ui.editors.RElementCompletionProposal.RElementProposalParameters;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.util.EntityRole;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.ui.ElementLabelProvider;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SimpleCompletionProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SourceProposal.ProposalParameters;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RSearchPattern;
import org.eclipse.statet.r.core.RSymbolComparator;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.model.RCoreFunctions;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RFunctionSpec;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.rlang.RFrame;
import org.eclipse.statet.r.core.model.rlang.RFrameSearchPath;
import org.eclipse.statet.r.core.model.rlang.RLangElement;
import org.eclipse.statet.r.core.model.rlang.RLangMethod;
import org.eclipse.statet.r.core.model.rlang.RSrcStrFrame;
import org.eclipse.statet.r.core.pkgmanager.IRPkgManager;
import org.eclipse.statet.r.core.rlang.RTokens;
import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.ui.RLabelProvider;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext.FCallInfo;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RReference;
import org.eclipse.statet.rj.renv.core.RPkgBuilt;
import org.eclipse.statet.rj.renv.core.RPkgCompilation;
import org.eclipse.statet.rj.renv.runtime.RPkgManagerDataset;


@NonNullByDefault
public abstract class AbstractRCompletionElementComputer
		implements ContentAssistComputer {
	
	
	private static final ImList<String> KEYWORDS;
	static {
		KEYWORDS= ImCollections.concatList(
				RTokens.CONSTANT_WORDS,
				RTokens.FLOWCONTROL_WORDS,
				RSymbolComparator.R_NAMES_COLLATOR );
	}
	
	private static final PartitionConstraint NO_R_COMMENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
			return (contentType != RDocumentConstants.R_COMMENT_CONTENT_TYPE);
		};
	};
	
	private final static int AS_STRING_VECTOR= RFunctionSpec.AS_STRING | RFunctionSpec.AS_N_VECTOR;
	
	
	protected static record FCallProposalContext(
			byte in,
			FCallInfo callInfo,
			boolean argName,
			boolean argValue ) {
		
		
		public boolean argAny() {
			return (this.argName || this.argValue);
		}
		
	}
	
	protected static class ContextSubject {
		
		private int type;
		public int prio;
		
	}
	
	
	protected static final int NA_PRIO= Integer.MIN_VALUE;
	
	protected static final int ARG_NAME_PRIO= 80;
	
	protected static final int ARG_TYPE_PRIO= 40;
	protected static final int ARG_TYPE_NO_PRIO= -40;
	
	protected static final byte IN_DEFAULT= 1;
	protected static final byte IN_STRING= 2;
	protected static final byte IN_STRING_VECTOR= 3;
	
	protected static final int CONTEXT_SUBJECT_COUNT= 4;
	
	
	protected final ElementLabelProvider labelProvider= new RLabelProvider(RLabelProvider.NAMESPACE);
	
	protected final int rSearchMode;
	
	private int searchMatchRules;
	
	private RAssistInvocationContext rContext= nonNullLateInit();
	protected byte in;
	
	protected int pkgNamePrio;
	private final ImList<ContextSubject> foundSubjects= ImCollections.newList(
			new ContextSubject(), new ContextSubject(), new ContextSubject(), new ContextSubject());
	
	private final HashSet<String> argNames= new HashSet<>();
	
	protected AssistProposalCollector proposals= nonNullLateInit();
	
	
	public AbstractRCompletionElementComputer(final int rSearchMode) {
		this.rSearchMode= rSearchMode;
	}
	
	
	@Override
	public void onSessionStarted(final SourceEditor editor, final ContentAssist assist) {
		int matchRules= SearchPattern.PREFIX_MATCH;
		if (assist.getShowSubstringMatches()) {
			matchRules |= SearchPattern.SUBSTRING_MATCH;
		}
		this.searchMatchRules= matchRules;
	}
	
	@Override
	public void onSessionEnded() {
	}
	
	protected void initR(final RAssistInvocationContext context,
			final IProgressMonitor monitor) {
		this.rContext= context;
		this.in= (context.getInvocationContentType() == RDocumentConstants.R_STRING_CONTENT_TYPE) ? IN_STRING : IN_DEFAULT;
	}
	
	@SuppressWarnings("null")
	protected void clear() {
		this.argNames.clear();
		this.rContext= null;
		this.proposals= null;
		
		this.pkgNamePrio= NA_PRIO;
		for (int i= 0; i < CONTEXT_SUBJECT_COUNT; i++) {
			final var subject= this.foundSubjects.get(i);
			subject.type= 0;
		}
	}
	
	
	protected final RAssistInvocationContext getRContext() {
		return this.rContext;
	}
	
	protected final int getRSearchMode() {
		if (this.rSearchMode != 0) {
			return this.rSearchMode;
		}
		return getRContext().getDefaultRFrameSearchMode();
	}
	
	protected int getSearchMatchRules() {
		return this.searchMatchRules;
	}
	
	protected final @Nullable ContextSubject getSubject(final int type) {
		for (int i= 0; i < CONTEXT_SUBJECT_COUNT; i++) {
			final var subject= this.foundSubjects.get(i);
			if (subject.type == type) {
				return subject;
			}
		}
		return null;
	}
	
	protected final int getSubjectPrio(final int type) {
		for (int i= 0; i < CONTEXT_SUBJECT_COUNT; i++) {
			final var subject= this.foundSubjects.get(i);
			if (subject.type == type) {
				return type;
			}
		}
		return NA_PRIO;
	}
	
	protected final void addSubject(final int type, final int prio) {
		for (int i= 0; i < CONTEXT_SUBJECT_COUNT; i++) {
			final var subject= this.foundSubjects.get(i);
			if (subject.type == type) {
				if (subject.prio > prio) {
					subject.prio= prio;
				}
				return;
			}
		}
		ContextSubject replaceSubject= null;
		for (int i= 0; i < CONTEXT_SUBJECT_COUNT; i++) {
			final var subject= this.foundSubjects.get(i);
			if (subject.type == 0) {
				replaceSubject= subject;
				break;
			}
			if (prio > subject.prio
					&& (replaceSubject == null || subject.prio <= replaceSubject.prio) ) {
				replaceSubject= subject;
			}
		}
		if (replaceSubject != null) {
			replaceSubject.type= type;
			replaceSubject.prio= prio;
		}
	}
	
	
	protected List<? extends RLangElement> getChildren(RLangElement e) {
		if (e instanceof final RReference ref) {
			final RObject rObject= ref.getResolvedRObject();
			if (rObject == null && e instanceof CombinedRElement && getRContext().getTool() != null) {
				getRContext().getToolReferencesUtil().resolve(ref, 0);
			}
			if (rObject instanceof CombinedRElement) {
				e= (CombinedRElement) rObject;
			}
		}
		return e.getModelChildren(null);
	}
	
	protected final @Nullable RPkgManagerDataset getRPkgDataset() {
		final IRPkgManager manager= RCore.getRPkgManager(getRContext().getRCoreAccess().getREnv());
		if (manager != null) {
			return manager.getDataset();
		}
		return null;
	}
	
	
	@Override
	public void computeCompletionProposals(final AssistInvocationContext context,
			final int mode, final AssistProposalCollector proposals,
			final IProgressMonitor monitor) {
		try {
			this.proposals= proposals;
			if (context instanceof RAssistInvocationContext) {
				initR((RAssistInvocationContext)context, monitor);
				computeCompletionProposals(mode, monitor);
			}
		}
		finally {
			clear();
		}
	}
	
	protected void computeCompletionProposals(final int mode,
			final IProgressMonitor monitor) {
	}
	
	@Override
	public void computeInformationProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		try {
			this.proposals= proposals;
			if (context instanceof RAssistInvocationContext) {
				initR((RAssistInvocationContext)context, monitor);
				computeContextProposals(monitor);
			}
		}
		finally {
			clear();
		}
	}
	
	protected void computeContextProposals(final IProgressMonitor monitor) {
	}
	
	
	protected void addKeywordCompletions(final RElementName prefixName) {
		final String prefixSegmentName= nonNullAssert(prefixName.getSegmentName());
		if (prefixName.getScope() != null) {
			return;
		}
		
		final String prefixSource= getRContext().getIdentifierPrefix();
		if (!prefixSegmentName.isEmpty() && prefixSource.charAt(0) != '`') {
			final ProposalParameters<RAssistInvocationContext> parameters= new ProposalParameters<>(
					getRContext(), getRContext().getIdentifierOffset(),
					new RSearchPattern(SearchPattern.PREFIX_MATCH, prefixSegmentName) );
			
			for (final String keyword : KEYWORDS) {
				if (parameters.matchesNamePattern(keyword)) {
					this.proposals.add(new RSimpleCompletionProposal(parameters, keyword));
				}
			}
		}
	}
	
	
	protected void addMainElementCompletions(final RFrameSearchPath searchPath, final RElementName prefixName)
			throws Exception {
		final String prefixSegmentName= nonNullAssert(prefixName.getSegmentName());
		
		final RElementProposalParameters parameters= new RElementProposalParameters(
				getRContext(), getRContext().getIdentifierLastSegmentOffset(),
				new RSearchPattern(getSearchMatchRules(), prefixSegmentName),
				this.labelProvider );
		
		final Set<String> mainNames= new HashSet<>();
		final List<String> methodNames= new ArrayList<>();
		
		for (final var iter= searchPath.iterator(); iter.hasNext();) {
			final RFrame<?> envir= iter.next();
			parameters.baseRelevance= iter.getRelevance();
			
			final List<? extends RElement<?>> elements= envir.getModelChildren(null);
			for (final RElement<?> element : elements) {
				final RElementName elementName= element.getElementName();
				final int c1type= (element.getElementType() & LtkModelElement.MASK_C1);
				final boolean isRich= (c1type == LtkModelElement.C1_METHOD);
				if ((isRich || c1type == LtkModelElement.C1_VARIABLE)
						&& isCompletable(elementName)
						&& parameters.matchesNamePattern(elementName.getSegmentName()) ) {
					if ((parameters.baseRelevance < 0) && !isRich
							&& mainNames.contains(elementName.getSegmentName()) ) {
						continue;
					}
					parameters.replacementName= elementName;
					parameters.element= element;
					
					final AssistProposal proposal= new RElementCompletionProposal(parameters);
					if (elementName.getNextSegment() == null) {
						if (isRich) {
							methodNames.add(elementName.getSegmentName());
						}
						else {
							mainNames.add(elementName.getSegmentName());
						}
					}
					this.proposals.add(proposal);
				}
			}
		}
		
		mainNames.addAll(methodNames);
		for (final RFrameSearchPath.Iterator iter= searchPath.iterator(); iter.hasNext();) {
			final RFrame<?> envir= iter.next();
			parameters.baseRelevance= 0;
			
			if (envir instanceof final RSrcStrFrame sframe) {
				final Set<@Nullable String> elementNames= sframe.getAllAccessNames();
				for (final String candidate : elementNames) {
					if (candidate != null
							&& !(candidate.equals(prefixSegmentName)
									&& (sframe.getAllAccessOf(candidate, false).size() <= 1) )
							&& !mainNames.contains(candidate)
							&& parameters.matchesNamePattern(candidate) ) {
						
						final AssistProposal proposal= new RSimpleCompletionProposal(parameters,
								candidate );
						mainNames.add(candidate);
						this.proposals.add(proposal);
					}
				}
			}
		}
	}
	
	
	protected FCallProposalContext createFCallProposalContext(final byte in, final FCallInfo fCallInfo)
			throws BadLocationException {
		boolean argName= false;
		boolean argValue= false;
		final int argIdx= fCallInfo.getInvocationArgIdx();
		if (argIdx >= 0) {
			final FCall.Arg arg= fCallInfo.getArg(argIdx);
			final int argBeginOffset= fCallInfo.getArgBeginOffset(argIdx);
			
			IDocument document= getRContext().getDocument();
			int offsetShift= 0;
			if (document instanceof final FragmentDocument inputDoc) {
				document= inputDoc.getMasterDocument();
				offsetShift= inputDoc.getOffsetInMasterDocument();
			}
			
			if (argBeginOffset != Integer.MIN_VALUE) {
				final var scanner= getRContext().getRHeuristicTokenScanner();
				scanner.configure(document, NO_R_COMMENT_CONSTRAINT);
				final int offset= getRContext().getIdentifierOffset();
				if (argBeginOffset == offset
						|| this.in == IN_STRING
						|| scanner.findNonMSpaceForward(argBeginOffset + offsetShift, offset + offsetShift) == TextTokenScanner.NOT_FOUND ) {
					argName= (this.in == IN_DEFAULT
							&& (getRContext().getIdentifierElementName().getScope() == null) );
					argValue= true;
				}
			}
			
			if (!argValue && arg != null && arg.getAssignOffset() != AstNode.NA_OFFSET) {
				final var scanner= getRContext().getRHeuristicTokenScanner();
				scanner.configure(document, NO_R_COMMENT_CONSTRAINT);
				final int offset= getRContext().getIdentifierOffset();
				if (arg.getAssignOffset() + 1 == offset
						|| this.in == IN_STRING
						|| scanner.findNonMSpaceForward(arg.getAssignOffset() + 1 + offsetShift, offset + offsetShift) == TextTokenScanner.NOT_FOUND ) {
					argValue= true;
				}
			}
		}
		return new FCallProposalContext(in, fCallInfo, argName, argValue);
	}
	
	protected boolean checkArgsDef(final RFunctionSpec. @Nullable Parameter parameter,
			final @Nullable RElementName fName,
			final boolean guess, final int relevance) {
		if (parameter != null) {
			final boolean typedDef= (parameter.getType() != 0);
			final int type= (typedDef) ? parameter.getType() :
					(guess) ? guessArgType(parameter.getName()) : 0;
			final int prio= ARG_TYPE_PRIO + relevance;
			if ((type & RFunctionSpec.PACKAGE_NAME) != 0
					&& prio > this.pkgNamePrio
					&& isValidNameContext(parameter) ) {
				this.pkgNamePrio= prio;
			}
			if ((type & RFunctionSpec.HELP_TOPIC_NAME) != 0
					&& isValidNameContext(parameter) ) {
				addSubject(RFunctionSpec.HELP_TOPIC_NAME, prio);
			}
			if ((type & RFunctionSpec.RELATOR_CODE) != 0
					&& isValidNameContext(parameter) ) {
				addSubject(RFunctionSpec.RELATOR_CODE, prio);
			}
			return typedDef;
		}
		return false;
	}
	
	protected boolean checkArgsDef(final FCallProposalContext fCall,
			final RFunctionSpec fSpec, @Nullable final RElementName fName,
			final boolean guess, final int relevance) {
		return checkArgsDef(
				RAsts.matchArgs(fCall.callInfo.getNode(), fSpec).getParamForFCall(fCall.callInfo.getInvocationArgIdx()),
				fName,
				guess, relevance );
	}
	
	protected void addFCallArgNameCompletions(final RLangMethod element,
			final RElementProposalParameters parameters) {
		final RFunctionSpec elementFSpec= element.getFunctionSpec();
		if (elementFSpec == null) {
			return;
		}
		for (int i= 0; i < elementFSpec.getParamCount(); i++) {
			final var param= elementFSpec.getParam(i);
			final String paramName= param.getName();
			if (paramName != null && !paramName.isEmpty() && !paramName.equals("...")) {
				if (parameters.matchesNamePattern(paramName)
						&& this.argNames.add(paramName) ) {
					final RElementName name= RElementName.create(RElementName.MAIN_DEFAULT, param.getName());
					parameters.replacementName= name;
					parameters.element= element;
					
					this.proposals.add(new RElementCompletionProposal.ArgumentProposal(parameters));
				}
			}
		}
	}
	
	protected class FCallArgHandler extends FCallNamePattern {
		
		private final FCallProposalContext fCall;
		
		private final @Nullable RCoreFunctions coreFunction;
		
		private int matchCount;
		
		private final RElementProposalParameters parameters;
		
		
		public FCallArgHandler(final FCallProposalContext fCall,
				final RElementProposalParameters parameters) {
			super(fCall.callInfo.getAccess());
			this.fCall= fCall;
			
			if (fCall.callInfo.getAccess().getNextSegment() == null) {
				this.coreFunction= RCoreFunctions.getDefinitions(
						getRContext().getRCoreAccess().getRSourceConfig() );
			}
			else {
				this.coreFunction= null;
			}
			this.parameters= parameters;
		}
		
		
		@Override
		public void searchFDef(final RFrameSearchPath searchPath) {
			super.searchFDef(searchPath);
			
			if (this.matchCount == 0
					&& this.coreFunction != null) {
				final RFunctionSpec coreFSpec= this.coreFunction.getFunctionSpec(
						getElementName().getSegmentName() );
				if (coreFSpec != null) {
					checkArgsDef(this.fCall, coreFSpec, getElementName(),
							false, 0 );
				}
			}
		}
		
		@Override
		protected void handleMatch(final RLangMethod<?> element, final RFrame<?> frame,
				final RFrameSearchPath.Iterator iterator) {
			final RFunctionSpec elementFSpec= element.getFunctionSpec();
			if (elementFSpec == null) {
				return;
			}
			
			final int contextRelevance= iterator.getRelevance();
			this.matchCount++;
			
			if (this.fCall.argName()) {
				this.parameters.baseRelevance= contextRelevance + ARG_NAME_PRIO;
				addFCallArgNameCompletions(element, this.parameters);
			}
			if (this.fCall.argValue()) {
				if (!checkArgsDef(this.fCall, elementFSpec, element.getElementName(),
								true, contextRelevance )
						&& frame.getFrameType() == RFrame.PACKAGE
						&& this.coreFunction != null
						&& this.coreFunction.getPackageNames().contains(frame.getElementName().getSegmentName()) ) {
					final RFunctionSpec coreFSpec= this.coreFunction.getFunctionSpec(
							getElementName().getSegmentName() );
					if (coreFSpec != null) {
						checkArgsDef(this.fCall, coreFSpec, element.getElementName(),
								false, contextRelevance );
					}
				}
			}
		}
		
	}
	
	protected void addFCallArgCompletions(final FCallInfo fCallInfo,
			final RElementName prefixName,
			final @Nullable RFrameSearchPath searchPath)
			throws Exception {
		final String prefixSegmentName= nonNullAssert(prefixName.getSegmentName());
		final byte savedIn= this.in;
		
		final var fCalls= new ArrayList<FCallProposalContext>(2);
		{	final FCallProposalContext mainFCall= createFCallProposalContext(savedIn, fCallInfo);
			fCalls.add(mainFCall);
			
			final FCallInfo enclosingFCallInfo;
			if (mainFCall.argValue()
					&& savedIn == IN_STRING && isVectorFun(fCallInfo.getAccess())
					&& (enclosingFCallInfo= findFCall(fCallInfo.getNode().getRParent(), null)) != null) {
				fCalls.add(new FCallProposalContext(IN_STRING_VECTOR, enclosingFCallInfo, false, true));
			}
		}
		
		final RElementProposalParameters parameters= new RElementProposalParameters(
				getRContext(), getRContext().getIdentifierOffset(),
				new RSearchPattern(getSearchMatchRules(), prefixSegmentName),
				this.labelProvider );
		
		for (final var fCall : fCalls) {
			if (!fCall.argAny()) {
				continue;
			}
			this.in= fCall.in();
			
			if (fCall.argValue() && !fCall.argName()) {
				final FCall.Arg arg= fCall.callInfo.getArg(fCall.callInfo.getInvocationArgIdx());
				if (arg != null && arg.hasName()) {
					checkArgsDef(new RFunctionSpec.Parameter(fCall.callInfo.getInvocationArgIdx(), arg.getNameChild().getText(),
									0, null ), 
							fCall.callInfo.getAccess(), true, 0);
				}
			}
			
			final FCallArgHandler search= new FCallArgHandler(fCall, parameters);
			search.searchFDef((searchPath != null) ? searchPath :
					fCall.callInfo.getSearchPath(getRSearchMode()) );
		}
		this.in= savedIn;
	}
	
	protected void addFCallArgContexts(final FCallInfo fCallInfo,
			final @Nullable RFrameSearchPath searchPath) {
		final RElementProposalParameters parameters= new RElementProposalParameters(
				getRContext(), fCallInfo.getNode().getArgsOpenOffset() + 1,
				this.labelProvider );
		
		final FCallNamePattern pattern= new FCallNamePattern(fCallInfo.getAccess()) {
			@Override
			protected void handleMatch(final RLangMethod<?> element, final RFrame<?> frame,
					final RFrameSearchPath.Iterator iterator) {
				parameters.baseRelevance= iterator.getRelevance();
				parameters.replacementName= element.getElementName();
				parameters.element= element;
				
				AbstractRCompletionElementComputer.this.proposals.add(
						new RElementCompletionProposal.ContextInformationProposal(
								parameters, fCallInfo.getNode() ));
			}
		};
		pattern.searchFDef((searchPath != null) ? searchPath :
				fCallInfo.getSearchPath(getRSearchMode()) );
	}
	
	
	protected void addPkgNameCompletions(final String prefix, final int prio) {
		final RElementProposalParameters parameters= new RElementProposalParameters(
				getRContext(), getRContext().getInvocationOffset() - prefix.length(),
				new RSearchPattern(getSearchMatchRules(), prefix), prio,
				this.labelProvider );
		
		final RPkgManagerDataset rPkgDataset= getRPkgDataset();
		
		final Collection<String> envNames;
		if (rPkgDataset != null) {
			final RPkgCompilation<? extends RPkgBuilt> pkgs= rPkgDataset.getInstalled();
			envNames= pkgs.getNames();
			for (final String pkgName : envNames) {
				if (parameters.matchesNamePattern(pkgName)) {
					parameters.replacementName= RElementName.create(RElementName.SCOPE_PACKAGE, pkgName);
					
					final AssistProposal proposal= new RPkgCompletionProposal(parameters,
							pkgs.getFirst(pkgName) );
					this.proposals.add(proposal);
				}
			}
		}
		else {
			envNames= ImCollections.emptySet();
		}
		
		final Set<String> workspaceNames= RModel.getRModelManager().getPkgNames();
		for (final String pkgName : workspaceNames) {
			if (!envNames.contains(pkgName)
					&& parameters.matchesNamePattern(pkgName) ) {
				parameters.replacementName= RElementName.create(RElementName.SCOPE_PACKAGE, pkgName);
				
				this.proposals.add(new RPkgCompletionProposal(parameters, null));
			}
		}
	}
	
	protected void addRoleCodeCompletions(final int prio) {
		final String prefixSource= getRContext().getIdentifierPrefix();
		int contentOffset= 1;
		final ProposalParameters<RAssistInvocationContext> parameters= new ProposalParameters<>(
				getRContext(), getRContext().getIdentifierOffset() + contentOffset,
				new SearchPattern(SearchPattern.PREFIX_MATCH, prefixSource.substring(contentOffset, prefixSource.length())) );
		
		for (final var role : EntityRole.COMMON_ROLES) {
			if ((role.getFlags() & EntityRole.R_PERSON) != 0
					&& parameters.matchesNamePattern(role.getCode())) {
				this.proposals.add(new SimpleCompletionProposal<>(parameters,
						role.getCode(), role.getName() ));
			}
		}
	}
	
	
	protected final boolean isPackageName(final RElementName elementName) {
		return ((elementName.getType() == RElementName.MAIN_DEFAULT 
						|| RElementName.isPackageFacetScopeType(elementName.getType()) )
				&& elementName.getNextSegment() == null );
	}
	
	@SuppressWarnings("null")
	protected final boolean isFun(final RElementName expected, final RElementName actual) {
		RElementName segment;
		return (actual.getType() == RElementName.MAIN_DEFAULT
				&& actual.getNextSegment() == null
				&& expected.getSegmentName().equals(actual.getSegmentName())
				&& ((segment= actual.getScope()) == null
						|| expected.getScope().getSegmentName().equals(segment.getSegmentName()) ));
	}
	
	protected final boolean isVectorFun(final RElementName elementName) {
		RElementName segment;
		return (elementName.getType() == RElementName.MAIN_DEFAULT
				&& elementName.getNextSegment() == null
				&& BASE_C_NAME.equals(elementName.getSegmentName())
				&& ((segment= elementName.getScope()) == null || BASE_PACKAGE_NAME.equals(segment.getSegmentName()) ));
	}
	
	protected final boolean isCompletable(@Nullable RElementName elementName) {
		if (elementName == null) {
			return false;
		}
		do {
			switch (elementName.getType()) {
			case RElementName.SUB_INDEXED_S:
			case RElementName.SUB_INDEXED_D:
				return false;
			}
			if (elementName.getSegmentName() == null) {
				return false;
			}
			elementName= elementName.getNextSegment();
		}
		while (elementName != null);
		return true;
	}
	
	protected final boolean isValidNameContext(final RFunctionSpec.Parameter parameter) {
		return switch (this.in) {
		case IN_STRING ->
				((parameter.getType() & AS_STRING) != 0);
		case IN_STRING_VECTOR ->
				((parameter.getType() & AS_STRING_VECTOR) == AS_STRING_VECTOR);
		default ->
				((parameter.getType() & AS_SYMBOL) != 0);
		};
	}
	
	protected final int guessArgType(final @Nullable String name) {
		if (name != null) {
			switch (name) {
			case "package":
				return RFunctionSpec.PACKAGE_NAME | RFunctionSpec.AS_STRING;
			default:
				break;
			}
		}
		return 0;
	}
	
	protected final @Nullable FCallInfo findFCall(@Nullable RAstNode node, final @Nullable String name) {
		while (node != null) {
			if (node.getNodeType() == NodeType.F_CALL_ARGS) {
				final var nextInfo= getRContext().createFCallInfo(((FCall.Args)node).getRParent());
				if (name == null
						|| (nextInfo != null && name.equals(nextInfo.getAccess().getSegmentName())) ) {
					return nextInfo;
				}
			}
			node= node.getRParent();
		}
		return null;
	}
	
	
	protected void log(final Exception e) {
		final var sb= new ToStringBuilder("An error occurred when gathering assist proposals.");
		sb.addProp("class", getClass().getName());
		sb.addProp("in", this.in);
		RUIPlugin.log(new Status(IStatus.ERROR, RUI.BUNDLE_ID, sb.build(), e));
	}
	
}
