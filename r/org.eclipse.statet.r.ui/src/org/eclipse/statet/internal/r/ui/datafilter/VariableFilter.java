/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.datafilter;

import java.util.Objects;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.ISetChangeListener;
import org.eclipse.core.databinding.observable.set.SetChangeEvent;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.r.ui.dataeditor.RDataTableColumn;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public abstract class VariableFilter {
	
	
	protected final static RCharacter32Store NO_VALUES= new RCharacter32Store();
	
	static final int SCHEDULE=             1 << 0;
	static final int SCHEDULE_WITH_DELAY=  SCHEDULE | 1 << 1;
	
	private class BindingListener implements IValueChangeListener<Object>, ISetChangeListener<Object> {
		
		@Override
		public void handleSetChange(final SetChangeEvent<?> event) {
			updateFilter(SCHEDULE_WITH_DELAY);
		}
		
		@Override
		public void handleValueChange(final ValueChangeEvent<?> event) {
			updateFilter(SCHEDULE_WITH_DELAY);
		}
		
	}
	
	
	private final FilterSet filterSet;
	
	private final RDataTableColumn column;
	
	private @Nullable Runnable listener;
	
	private @Nullable IStatus status;
	
	volatile boolean updateScheduled;
	
	private @Nullable String filterRExpression;
	
	private final BindingListener valueListener= new BindingListener();
	
	
	protected VariableFilter(final FilterSet set, final RDataTableColumn column) {
		this.filterSet= set;
		this.column= column;
	}
	
	
	public abstract FilterType getType();
	
	public FilterSet getSet() {
		return this.filterSet;
	}
	
	public RDataTableColumn getColumn() {
		return this.column;
	}
	
	public void load(final VariableFilter filter) {
	}
	
	public void reset() {
	}
	
	protected void scheduleUpdate() {
		this.updateScheduled= true;
		this.filterSet.scheduleUpdate(false);
	}
	
	protected void registerObservable(final IObservable observable) {
		if (observable instanceof IObservableValue) {
			((IObservableValue<?>)observable).addValueChangeListener(this.valueListener);
		}
		if (observable instanceof IObservableSet) {
			((IObservableSet<?>)observable).addSetChangeListener(this.valueListener);
		}
	}
	
	protected void updateFilter(final int flag) {
		String rExpression= createFilter(getColumn().getRExpression());
		if (rExpression != null && rExpression.isEmpty()) {
			rExpression= null;
		}
		if (Objects.equals(this.filterRExpression, rExpression)) {
			return;
		}
		this.filterRExpression= rExpression;
		
		if ((flag & SCHEDULE) != 0) {
			this.filterSet.updateEffectiveFilter((flag & SCHEDULE_WITH_DELAY) == SCHEDULE_WITH_DELAY);
		}
	}
	
	protected abstract @Nullable String createFilter(String varExpression);
	
	protected void runInRealm(final Runnable runnable) {
		this.filterSet.runInRealm(runnable);
	}
	
	
	public void setListener(final Runnable listener) {
		this.listener= listener;
	}
	
	protected abstract void updateData(RToolService r, ProgressMonitor m) throws StatusException, UnexpectedRDataException;
	
	protected void notifyListeners() {
		final var listener= this.listener;
		if (listener != null) {
			listener.run();
		}
	}
	
	protected abstract void setStatus(final IStatus message);
	
	protected final void doSetStatus(final IStatus status) {
		this.status= status;
	}
	
	protected final void clearStatus() {
		this.status= null;
	}
	
	public @Nullable IStatus getStatus() {
		return this.status;
	}
	
	public @Nullable String getFilterRExpression() {
		return this.filterRExpression;
	}
	
	public @Nullable String getFilterRExpression(final String mainExpression, final int nameFlags) {
		final String varExpression= mainExpression
				+ getColumn().getElementName().getDisplayName(nameFlags).substring(1);
		return createFilter(varExpression);
	}
	
	
//	@Override
//	public int hashCode() {
//		return this.column.getName().hashCode();
//	}
//	
//	@Override
//	public boolean equals(Object obj) {
//		return (obj instanceof VariableFilter
//				&& this.column.getName().equals(((VariableFilter)obj).this.column.getName()) );
//	}
	
}
