/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.r;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.Region;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.EditorTextInfoHoverProxy;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHoverDescriptor;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;
import org.eclipse.statet.r.ui.editors.RSourceEditor;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class REditorTextHover extends EditorTextInfoHoverProxy {
	
	
	private @Nullable RHeuristicTokenScanner scanner;
	
	
	public REditorTextHover(final RSourceEditor editor,
			final InfoHoverDescriptor descriptor, final SourceEditorViewerConfiguration config) {
		super(descriptor, config);
	}
	
	
	@Override
	public RSourceEditor getEditor() {
		return (RSourceEditor)super.getEditor();
	}
	
	private RHeuristicTokenScanner getScanner() {
		var scanner= this.scanner;
		if (scanner == null) {
			scanner= RHeuristicTokenScanner.create(getEditor().getDocumentContentInfo());
			this.scanner= scanner;
		}
		return scanner;
	}
	
	@Override
	public @Nullable IRegion getHoverRegion(final ITextViewer textViewer, final int offset) {
		try {
			final var scanner= getScanner();
			final IDocument document= nonNullAssert(textViewer.getDocument());
			scanner.configure(document);
			final IRegion word= scanner.findRWord(offset, false, true);
			if (word != null) {
				final ITypedRegion partition= scanner.getPartition(word.getOffset());
				if (RDocumentConstants.R_DEFAULT_CONTENT_CONSTRAINT.matches(partition.getType())
						|| partition.getType() == RDocumentConstants.R_STRING_CONTENT_TYPE
						|| partition.getType() == RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE) {
					return word;
				}
			}
			final char c= document.getChar(offset);
			if (c == '[') {
				final ITypedRegion partition= scanner.getPartition(offset);
				if (RDocumentConstants.R_DEFAULT_CONTENT_CONSTRAINT.matches(partition.getType())) {
					return new Region(offset, 1);
				}
			}
		}
		catch (final BadLocationException e) {
			RUIPlugin.logUncriticalError(e);
		}
		return null;
	}
	
	@Override
	protected @Nullable AssistInvocationContext createContext(final IRegion region,
			final String contentType,
			final IProgressMonitor monitor) {
		// we are not in UI thread
		final var scanner= getScanner();
		final RAssistInvocationContext context= new RAssistInvocationContext(getEditor(),
				region, contentType, scanner, monitor );
		if (context.getAstInfo() == null) {
			return null;
		}
		return context;
	}
	
}
