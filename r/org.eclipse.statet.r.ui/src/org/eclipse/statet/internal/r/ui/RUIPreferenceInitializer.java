/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;

import org.eclipse.statet.internal.r.ui.editors.r.DefaultRFoldingPreferences;
import org.eclipse.statet.internal.r.ui.rhelp.RHelpPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHoverRegistry;
import org.eclipse.statet.nico.core.NicoPreferenceNodes;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.launching.RRunDebugPreferenceConstants;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.RUIPreferenceConstants;
import org.eclipse.statet.r.ui.editors.REditorBuild;
import org.eclipse.statet.r.ui.editors.REditorOptions;


public class RUIPreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public static final String R_EDITOR_NODE= RUI.BUNDLE_ID + "/editor.r/options"; //$NON-NLS-1$
	public static final String R_CONSOLE_NODE= RUI.BUNDLE_ID + '/'+NicoPreferenceNodes.SCOPE_QUALIFIER+ "/editor.r/options"; // NicoPreferenceNodes.createScopeQualifier(REDITOR_NODE); //$NON-NLS-1$
	public static final String RPKG_EDITOR_NODE= RUI.BUNDLE_ID + "/editor/RPkgDescr"; //$NON-NLS-1$
	
	public static final String REDITOR_HOVER_GROUP_ID = "r/r.editor/hover"; //$NON-NLS-1$
	
	public static final BooleanPref CONSOLE_SMARTINSERT_CLOSECURLY_ENABLED = new BooleanPref(
			R_CONSOLE_NODE, "smartinsert.close_curlybrackets.enabled"); //$NON-NLS-1$
	public static final BooleanPref CONSOLE_SMARTINSERT_CLOSEROUND_ENABLED = new BooleanPref(
			R_CONSOLE_NODE, "smartinsert.close_roundbrackets.enabled"); //$NON-NLS-1$
	public static final BooleanPref CONSOLE_SMARTINSERT_CLOSESQUARE_ENABLED = new BooleanPref(
			R_CONSOLE_NODE, "smartinsert.close_squarebrackets.enabled"); //$NON-NLS-1$
	public static final BooleanPref CONSOLE_SMARTINSERT_CLOSESPECIAL_ENABLED = new BooleanPref(
			R_CONSOLE_NODE, "smartinsert.close_specialpercent.enabled"); //$NON-NLS-1$
	public static final BooleanPref CONSOLE_SMARTINSERT_CLOSESTRINGS_ENABLED = new BooleanPref(
			R_CONSOLE_NODE, "smartinsert.close_strings.enabled"); //$NON-NLS-1$
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store = RUIPlugin.getInstance().getPreferenceStore();
		EditorsUI.useAnnotationsPreferencePage(store);
		EditorsUI.useQuickDiffPreferencePage(store);
		
		final IScopeContext scope= DefaultScope.INSTANCE;
		final Map<Preference<?>, Object> map= new HashMap<>();
		RUIPreferenceConstants.initializeDefaultValues(scope);
		
		new REditorOptions(0).deliverToPreferencesMap(map);
		
		PreferenceUtils.setPrefValue(scope, CONSOLE_SMARTINSERT_CLOSECURLY_ENABLED, Boolean.FALSE);
		PreferenceUtils.setPrefValue(scope, CONSOLE_SMARTINSERT_CLOSEROUND_ENABLED, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, CONSOLE_SMARTINSERT_CLOSESQUARE_ENABLED, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, CONSOLE_SMARTINSERT_CLOSESPECIAL_ENABLED, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, CONSOLE_SMARTINSERT_CLOSESTRINGS_ENABLED, Boolean.TRUE);
		
		PreferenceUtils.setPrefValue(scope, REditorOptions.FOLDING_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, REditorOptions.FOLDING_RESTORE_STATE_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, REditorOptions.PREF_MARKOCCURRENCES_ENABLED, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, REditorBuild.PROBLEMCHECKING_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, REditorOptions.PREF_SPELLCHECKING_ENABLED, Boolean.FALSE);
		DefaultRFoldingPreferences.initializeDefaultValues(scope);
		
		scope.getNode(R_EDITOR_NODE).put(ContentAssistComputerRegistry.CIRCLING_ORDERED,
				"R.Default:false," + //$NON-NLS-1$
				"templates:true," + //$NON-NLS-1$
				"R.Expl.RuntimeElements:true," + //$NON-NLS-1$
				"R.Expl.PkgNames:false" ); //$NON-NLS-1$
		scope.getNode(R_EDITOR_NODE).put(ContentAssistComputerRegistry.DEFAULT_DISABLED,
				"R.Expl.PkgNames," + //$NON-NLS-1$
				"R.Expl.RuntimeElements" ); //$NON-NLS-1$
		
		scope.getNode(R_CONSOLE_NODE).put(ContentAssistComputerRegistry.CIRCLING_ORDERED,
				"R.Default:false," + //$NON-NLS-1$
				"templates:true" + //$NON-NLS-1$
				"R.Expl.PkgNames:false" ); //$NON-NLS-1$
		scope.getNode(R_CONSOLE_NODE).put(ContentAssistComputerRegistry.DEFAULT_DISABLED,
				"R.Expl.PkgNames" ); //$NON-NLS-1$
		
		scope.getNode(R_EDITOR_NODE).put(InfoHoverRegistry.TYPE_SETTINGS,
				"org.eclipse.statet.r.infoHovers.RCombined:true;," + //$NON-NLS-1$
				"org.eclipse.statet.r.infoHovers.RHelp:false;," + //$NON-NLS-1$
				"org.eclipse.statet.r.infoHovers.RDebug:true;M2"); //$NON-NLS-1$
		
		final IEclipsePreferences rHelp = scope.getNode(RHelpPreferences.RHELP_QUALIFIER);
		rHelp.put(RHelpPreferences.HOMEPAGE_URL_KEY, RCore.DEFAULT_RHELP_BROWSE_URL);
		rHelp.putBoolean(RHelpPreferences.SHOW_INTERNAL_ENABLED_KEY, true);
		rHelp.putBoolean(RHelpPreferences.SEARCH_REUSE_PAGE_ENABLED_KEY, true);
		rHelp.putInt(RHelpPreferences.SEARCH_PREVIEW_FRAGMENTS_MAX_KEY, 10);
		
		RRunDebugPreferenceConstants.initializeDefaultValues(scope);
		
		PreferenceUtils.setPrefValues(scope, map);
	}
	
}
