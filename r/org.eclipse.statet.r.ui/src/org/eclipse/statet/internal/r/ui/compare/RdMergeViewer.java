/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.compare;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;

import org.eclipse.statet.ltk.ui.compare.CompareMergeTextViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.r.ui.editors.RdDocumentSetupParticipant;
import org.eclipse.statet.r.ui.editors.RdSourceViewerConfiguration;
import org.eclipse.statet.r.ui.editors.RdSourceViewerConfigurator;


@NonNullByDefault
public class RdMergeViewer extends CompareMergeTextViewer {
	
	
	public RdMergeViewer(final Composite parent, final CompareConfiguration configuration) {
		super(parent, configuration);
	}
	
	
	@Override
	protected PartitionerDocumentSetupParticipant createDocumentSetupParticipant() {
		return new RdDocumentSetupParticipant();
	}
	
	@Override
	protected SourceEditorViewerConfigurator createConfigurator(final SourceViewer sourceViewer) {
		final RdSourceViewerConfigurator viewerConfigurator=
				new RdSourceViewerConfigurator(null,
						new RdSourceViewerConfiguration(SourceEditorViewerConfiguration.COMPARE_MODE) );
		return viewerConfigurator;
	}
	
}
