/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.rhelp;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.internal.r.ui.rhelp.RHelpWorkbenchStyle.appendCssColor;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.JFacePreferences;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.ide.IDE;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;

import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.preferences.SettingsChangeNotifier;
import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.ui.RIdentifierGroups;
import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.ui.RUIPreferenceConstants;
import org.eclipse.statet.rhelp.core.RHelpPage;
import org.eclipse.statet.rhelp.core.http.RunExamplesRunnable;
import org.eclipse.statet.rhelp.core.http.ee8.RHelpHttpServlet;
import org.eclipse.statet.rhelp.core.http.ee8.jetty.JettyForwardHandler;
import org.eclipse.statet.rhelp.core.http.ee8.jetty.JettyRHelpUtils;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public class RHelpUIServlet extends RHelpHttpServlet implements IPropertyChangeListener, SettingsChangeNotifier.ChangeListener {
	
	private static final long serialVersionUID= 1L;
	
	
	static final String STYLE_PARAM= "style"; //$NON-NLS-1$
	
	static final String HOVER_STYLE= "hover"; //$NON-NLS-1$
	
	
	public static class Browse extends RHelpUIServlet {
		
		private static final long serialVersionUID= 1L;
		
		@Override
		protected boolean canRunExamples() {
			return true;
		}
		
		@Override
		protected void collectCss(final StringBuilder sb) {
			final var style= new RHelpWorkbenchStyle();
			
			sb.append("body { "); //$NON-NLS-1$
					style.appendFont(sb, style.getDocFontDescr());
					sb.append("}\n"); //$NON-NLS-1$
			
			sb.append("@media only screen {\n"); //$NON-NLS-1$
			
			sb.append("html {"); //$NON-NLS-1$
					style.appendRootColors(sb);
					sb.append("}\n"); //$NON-NLS-1$
			sb.append("body { " //$NON-NLS-1$
					+ "margin: "); //$NON-NLS-1$
							sb.append(LayoutUtils.defaultVSpacing()).append("px "); //$NON-NLS-1$
							sb.append(LayoutUtils.defaultHSpacing()).append("px; "); //$NON-NLS-1$
					style.appendScreenColors(sb);
					sb.append("}\n"); //$NON-NLS-1$
			
			style.appendLinkDefinitions(sb);
			style.appendDialogButtonDefinitions(sb);
			
			sb.append("div.toc { " //$NON-NLS-1$
					+ "display: inline; " //$NON-NLS-1$
					+ "float: right; " //$NON-NLS-1$
					+ "border: 1px solid "); //$NON-NLS-1$
							appendCssColor(sb, style.getDocBorderColorRGB()).append("; "); //$NON-NLS-1$
					sb.append("}\n"); //$NON-NLS-1$
			
			sb.append("div.toc a, div.toc a:visited { " //$NON-NLS-1$
					+ "color: "); //$NON-NLS-1$
							appendCssColor(sb, style.getDocForegroundColorRGB()).append("; "); //$NON-NLS-1$
					sb.append("; }\n"); //$NON-NLS-1$
			
			sb.append("span.mnemonic, div.toc a.mnemonic { " //$NON-NLS-1$
					+ "text-decoration: underline; " //$NON-NLS-1$
					+ "}\n" ); //$NON-NLS-1$
			sb.append("hr { " //$NON-NLS-1$
					+ "border: 0; " //$NON-NLS-1$
					+ "height: 1px; " //$NON-NLS-1$
					+ "background: " ); //$NON-NLS-1$
							appendCssColor(sb, style.getDocBorderColorRGB()).append("; "); //$NON-NLS-1$
					sb.append("}\n"); //$NON-NLS-1$
			
			sb.append("body.error { "); //$NON-NLS-1$
					style.appendFont(sb, style.getDialogFontDescr());
					style.appendDialogColors(sb);
					sb.append("}\n"); //$NON-NLS-1$
			
			sb.append("}\n"); // @media //$NON-NLS-1$
			
			style.appendMatchColors(sb);
			
			super.collectCss(sb);
		}
		
	}
	
	public static class Info extends RHelpUIServlet {
		
		private static final long serialVersionUID= 1L;
		
		@Override
		protected void collectCss(final StringBuilder sb) {
			final var style= new RHelpWorkbenchStyle();
			
			final int vIndent= Math.max(1, LayoutUtils.defaultVSpacing() / 4);
			final int hIndent= Math.max(3, LayoutUtils.defaultHSpacing() / 2);
			
			sb.append("html { "); //$NON-NLS-1$
					style.appendFont(sb, style.getDialogFontDescr());
					style.appendRootColors(sb);
					sb.append("}\n"); //$NON-NLS-1$
			sb.append("body { "); //$NON-NLS-1$
					sb.append("margin: 0 ") //$NON-NLS-1$
							.append(hIndent).append("px ") //$NON-NLS-1$
							.append(vIndent).append("px; "); //$NON-NLS-1$
					style.appendScreenColors(sb);
					sb.append("}\n"); //$NON-NLS-1$
			
			style.appendLinkDefinitions(sb);
			style.appendDialogButtonDefinitions(sb);
			
			sb.append("h2, h3#description { " //$NON-NLS-1$
					+ "display: none; " //$NON-NLS-1$
					+ "}\n"); //$NON-NLS-1$
			sb.append("h3 { " //$NON-NLS-1$
					+ "font-size: 90%; " //$NON-NLS-1$
					+ "margin-bottom: 0.4em; " //$NON-NLS-1$
					+ "}\n"); //$NON-NLS-1$
			sb.append("p, pre { " //$NON-NLS-1$
					+ "margin-top: 0.4em; " //$NON-NLS-1$
					+ "margin-bottom: 0.4em; " //$NON-NLS-1$
					+ "}\n" ); //$NON-NLS-1$
			
			sb.append("hr { " //$NON-NLS-1$
					+ "visibility: hidden; " //$NON-NLS-1$
					+ "}\n"); //$NON-NLS-1$
			
			sb.append("body.error { "); //$NON-NLS-1$
					style.appendDialogColors(sb);
					sb.append("}\n"); //$NON-NLS-1$
			sb.append("body.error button {"
					+ "visibility: hidden; " //$NON-NLS-1$
					+ "}\n"); //$NON-NLS-1$
			
			super.collectCss(sb);
		}
		
	}
	
	
	private volatile @Nullable String cssStyle;
	
	private RHelpRCodeScanner rCodeScanner;
	
	private final Runnable updateStyles= this::updateStyles;
	private boolean updateStyleScheduled;
	
	
	@SuppressWarnings("null")
	public RHelpUIServlet() {
	}
	
	
	@Override
	public void init(final ServletConfig config) throws ServletException {
		super.init(config);
		
		init(RCore.getRHelpManager(),
				JettyRHelpUtils.newResourceHandler(config.getServletContext()),
				new JettyForwardHandler() );
		
		this.rCodeScanner= new RHelpRCodeScanner(RUIPlugin.getInstance().getEditorPreferenceStore());
		
		EditorsUI.getPreferenceStore().addPropertyChangeListener(this);
		JFaceResources.getFontRegistry().addListener(this);
		JFaceResources.getColorRegistry().addListener(this);
		PreferencesUtil.getSettingsChangeNotifier().addChangeListener(this);
		
		this.updateStyleScheduled= true;
		Display.getDefault().syncExec(this.updateStyles);
	}
	
	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		if (!this.updateStyleScheduled
				&& (event.getProperty().equals(RHelpWorkbenchStyle.DOC_BACKGROUND_COLOR_KEY)
						|| event.getProperty().equals(RHelpWorkbenchStyle.DOC_FOREGROUND_COLOR_KEY)
						|| event.getProperty().equals(RHelpWorkbenchStyle.DOC_FONT_KEY)
						|| event.getProperty().equals(JFaceResources.DIALOG_FONT)
						|| event.getProperty().equals(JFacePreferences.HYPERLINK_COLOR)
						|| event.getProperty().equals(JFacePreferences.ACTIVE_HYPERLINK_COLOR)
						|| event.getProperty().equals("searchResultIndicationColor") )) { //$NON-NLS-1$
			this.updateStyleScheduled= true;
			Display.getDefault().asyncExec(this.updateStyles);
		}
	}
	
	@Override
	public void settingsChanged(final Set<String> groupIds) {
		if (groupIds.contains(RUIPreferenceConstants.R.TS_GROUP_ID)
				|| groupIds.contains(RIdentifierGroups.GROUP_ID)) {
			final Map<String, Object> options= new HashMap<>();
			synchronized (this.rCodeScanner) {
				this.rCodeScanner.handleSettingsChanged(groupIds, options);
			}
		}
	}
	
	@Override
	public void destroy() {
		super.destroy();
		
		final IPreferenceStore preferenceStore= EditorsUI.getPreferenceStore();
		if (preferenceStore != null) {
			preferenceStore.removePropertyChangeListener(this);
		}
		JFaceResources.getFontRegistry().removeListener(this);
		JFaceResources.getColorRegistry().removeListener(this);
		PreferencesUtil.getSettingsChangeNotifier().removeChangeListener(this);
	}
	
	
	@Override
	protected boolean getShowInternal() {
		return EPreferences.getInstancePrefs().getPreferenceValue(
				RHelpPreferences.SHOW_INTERNAL_ENABLED_PREF );
	}
	
	@Override
	protected boolean canOpenFile(final String ext) {
		return true;
	}
	
	@Override
	protected void doOpenFile(final Path file) {
		try {
			final IFileStore fileStore= EFS.getLocalFileSystem().getStore(file.toUri());
			UIAccess.getDisplay().asyncExec(new Runnable() {
				@Override
				public void run() {
					final IWorkbenchPage page= UIAccess.getActiveWorkbenchPage(true);
					try {
						IDE.openEditorOnFileStore(page, fileStore);
					}
					catch (final PartInitException e) {}
				}
			});
		}
		catch (final Exception e) {}
	}
	
	
	@Override
	protected void doRunExamples(final RHelpPage helpPage,
			final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
		class Context {
			final IWorkbenchPage workbenchPage;
			final @Nullable IWorkbenchPart workbenchPart;
			final @Nullable RTool rTool;
			public Context(final IWorkbenchPage workbenchPage, final @Nullable IWorkbenchPart workbenchPart,
					final @Nullable RTool rTool) {
				this.workbenchPage= workbenchPage;
				this.workbenchPart= workbenchPart;
				this.rTool= rTool;
			}
		}
		final var rEnv= helpPage.getPackage().getREnv();
		final var runnable= new RunExamplesRunnable(helpPage);
		final @Nullable Context context= UIAccess.syncExecGet(() -> {
			final var uiPart= UIAccess.getActiveWorkbenchPart(true);
			final var uiPage= (uiPart != null) ?
					uiPart.getSite().getPage() : UIAccess.getActiveWorkbenchPage(true);
			if (uiPage == null) {
				return null;
			}
			return new Context(uiPage, uiPart, NicoUITools.findTool((final Tool tool) -> {
					final RTool rToolCandicate;
					if (runnable.canRunIn(tool)
							&& (rToolCandicate= (RTool)tool).getREnv() == rEnv
							&& !rToolCandicate.isTerminated() ) {
						return rToolCandicate;
					}
					return null;
				}, uiPart, uiPage ));
		});
		
		if (context != null && context.rTool != null) {
			final RTool rTool= nonNullAssert(context.rTool);
			runnable.changed(ToolRunnable.ADDING_TO, rTool);
			if (rTool.getQueue().add(runnable).getSeverity() == Status.OK) {
				final var console= NicoUITools.getConsole(rTool);
				if (console != null) {
					NicoUITools.showConsole(console, context.workbenchPage, false);
				}
				try {
					final var result= runnable.waitForResult();
					if (result != null) {
						servePageExamples(helpPage, result, req, resp);
						return;
					}
				}
				catch (final InterruptedException e) {
				}
				resp.setStatus(HttpServletResponse.SC_GATEWAY_TIMEOUT);
				return;
			}
		}
		
		final var errorData= new ErrorData(PC_OTHER, HttpServletResponse.SC_SERVICE_UNAVAILABLE,
				"Examples of R Environment '" + rEnv.getName() + "'" );
		errorData.getMessageBuilder()
				.append("No session of R is available to run the examples of the R environment '")
				.append(rEnv.getName())
				.append("'.");
		errorData.setFixInfos("<p>Launch a console to enable running of examples.</p>"
				+ infoStepList
				+ infoStepLaunchConsole
				+ "</ol>"
				+ infoRefreshButton );
		
		sendError(errorData, req, resp);
		return;
	}
	
	
	private static String infoStepList= "<ol class=\"c2\">";
	private static String infoStepCreateREnvironment=
			"<li>Open the preference page R &gt; R Environments, "
					+ "create an environment and "
					+ "mark it as default.</li>";
	private static String infoStepConfigureAndLaunchConsole=
			"<li>Configure and launch a console for this R environment.</li>";
	private static String infoStepLaunchConsole=
			"<li>Launch a console for this R environment.</li>";
	private static String infoStepIndexInConsole=
			"<li>By default the indexer is run automatically when the console is started.<br/>"
					+ "If the automatic indexing is disabled, execute the command 'Update R "
					+ "environment index'.</li>";
	
	private static String infoRefreshButton=
			"<p><button onClick=\"window.location.reload();\">Refresh Page</button></p>";
	
	
	@Override
	protected void enrichErrorData(final ErrorData errorData,
			final HttpServletRequest req) throws IOException {
		super.enrichErrorData(errorData, req);
		
		if (getClass() != Browse.class) {
			return;
		}
		
		switch (errorData.getProblemCode()) {
		case PC_ENV_MISSING:
			{	final String rEnvId= getREnvId(req);
				if (rEnvId.startsWith("default-")) { //$NON-NLS-1$
					final var rEnvManager= RCore.getREnvManager();
					if (rEnvManager.list().isEmpty()) {
						errorData.setFixInfos("<p>Configure an R environment and get it indexed "
								+ "to enable the R help support.</p>"
								+ infoStepList
								+ infoStepCreateREnvironment
								+ infoStepConfigureAndLaunchConsole
								+ infoStepIndexInConsole
								+ "</ol>"
								+ infoRefreshButton );
					}
					else {
						errorData.setFixInfos("<p>Configure an R environment as default or"
								+ " select a specific R environment.</p>"
								+ infoRefreshButton );
					}
				}
				else {
					errorData.setFixInfos("<p>Select a valid R environment.</p>");
				}
			}
			return;
		case PC_ENV_HELP_MISSING:
			errorData.setFixInfos("<p>Launch the console and get the R environment indexed "
					+ "to enable the R help support.</p>"
					+ infoStepList
					+ infoStepLaunchConsole
					+ infoStepIndexInConsole
					+ "</ol>"
					+ infoRefreshButton );
			return;
		default:
			return;
		}
	}
	
	
	private void updateStyles() {
		this.updateStyleScheduled= false;
		
		final StringBuilder sb= new StringBuilder(1024);
		collectCss(sb);
		sb.append(".header { display: none; }"); //$NON-NLS-1$
		this.cssStyle= sb.toString();
	}
	
	protected void collectCss(final StringBuilder sb) {
	}
	
	@Override
	protected void customizeCss(final PrintWriter writer) {
//		updateStyles();
		writer.print(this.cssStyle);
	}
	
	@Override
	protected void customizePageHtmlHeader(final HttpServletRequest req, final PrintWriter writer) {
		customizeHtmlHeader(req, writer, true);
	}
	
	@Override
	protected void customizeIndexHtmlHeader(final HttpServletRequest req, final PrintWriter writer) {
		customizeHtmlHeader(req, writer, false);
	}
	
	protected void customizeHtmlHeader(final HttpServletRequest req, final PrintWriter writer,
			final boolean page) {
		writer.println("<script type=\"text/javascript\">/* <![CDATA[ */"); //$NON-NLS-1$
		
		writer.println("function keyNavHandler(event) {"); //$NON-NLS-1$
		writer.println("if (!event) event= window.event;"); //$NON-NLS-1$
		writer.println("if (event.which) { key= event.which } else if (event.keyCode) { key= event.keyCode };"); //$NON-NLS-1$
		writer.println("if (!event.ctrlKey && !event.altKey) { var anchor= 0;"); //$NON-NLS-1$
		if (page) {
			writer.println("if (key == 68) anchor= \"#description\"; " + //$NON-NLS-1$
					"else if (key == 85) anchor= \"#usage\"; " + //$NON-NLS-1$
					"else if (key == 65) anchor= \"#arguments\"; " + //$NON-NLS-1$
					"else if (key == 73) anchor= \"#details\"; " + //$NON-NLS-1$
					"else if (key == 86) anchor= \"#value\"; " + //$NON-NLS-1$
					"else if (key == 79) anchor= \"#authors\"; " + //$NON-NLS-1$
					"else if (key == 82) anchor= \"#references\"; " + //$NON-NLS-1$
					"else if (key == 69) anchor= \"#examples\"; " + //$NON-NLS-1$
					"else if (key == 83) anchor= \"#seealso\";"); //$NON-NLS-1$
		}
		else {
			writer.println("if (key >= 65 && key <= 90) anchor= \"#idx\"+String.fromCharCode(key+32);"); //$NON-NLS-1$
		}
		writer.println("if (anchor) { window.location.hash= anchor; event.cancelBubble= true; return false; }"); //$NON-NLS-1$
		writer.println("} return true; }"); //$NON-NLS-1$
		writer.println("document.onkeydown= keyNavHandler;"); //$NON-NLS-1$
		
		writer.println("/* ]]> */</script>"); //$NON-NLS-1$
		
		if (HOVER_STYLE.equals(req.getParameter(STYLE_PARAM))) {
			writer.println("<style type=\"text/css\">body { overflow: hidden; }</style>"); //$NON-NLS-1$
		}
	}
	
	@Override
	protected void printRCode(final PrintWriter writer, final String html) {
		synchronized (this.rCodeScanner) {
			this.rCodeScanner.setHtml(html);
			writer.write("<span style=\""); //$NON-NLS-1$
			writer.write(this.rCodeScanner.getRootStyle());
			writer.write("\">"); //$NON-NLS-1$
			
			IToken token;
			int currentIdx= 0;
			while (!(token= this.rCodeScanner.nextToken()).isEOF()) {
				final String data= (String) token.getData();
				if (data != null) {
					final int tokenIdx= this.rCodeScanner.getTokenOffset();
					if (tokenIdx > currentIdx) {
						writer.write(html, currentIdx, tokenIdx-currentIdx);
					}
					writer.write("<span style=\""); //$NON-NLS-1$
					writer.write(data);
					writer.write("\">"); //$NON-NLS-1$
					writer.write(html, tokenIdx, this.rCodeScanner.getTokenLength());
					writer.write("</span>"); //$NON-NLS-1$
					currentIdx= tokenIdx + this.rCodeScanner.getTokenLength();
				}
			}
			writer.write(html, currentIdx, html.length()-currentIdx);
			
			writer.write("</span>"); //$NON-NLS-1$
		}
	}
	
}
