/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.refactoring;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.refactoring.RefactoringSaveHelper;
import org.eclipse.statet.ltk.ui.refactoring.RefactoringWizardExecutionHelper;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;
import org.eclipse.statet.r.core.model.RWorkspaceSourceUnit;
import org.eclipse.statet.r.core.refactoring.RenameInWorkspaceRefactoring;


public class RenameInWorkspaceHandler extends AbstractHandler {
	
	
	public RenameInWorkspaceHandler() {
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		final IWorkbenchPart activePart= WorkbenchUIUtils.getActivePart(event.getApplicationContext());
		if (selection == null || activePart == null) {
			return null;
		}
		final SourceUnit su= LTKSelectionUtils.getSingleSourceUnit(activePart);
		if (!(su instanceof RWorkspaceSourceUnit)) {
			return null;
		}
		RenameInWorkspaceRefactoring refactoring= null;
		if (selection instanceof final ITextSelection textSelection) {
			refactoring= new RenameInWorkspaceRefactoring((RWorkspaceSourceUnit) su,
					LTKSelectionUtils.toTextRegion(textSelection) );
		}
		if (refactoring != null) {
			final RefactoringWizardExecutionHelper executionHelper= new RefactoringWizardExecutionHelper(
					new RenameInWorkspaceWizard(refactoring), RefactoringSaveHelper.SAVE_REFACTORING | RefactoringSaveHelper.EXCLUDE_ACTIVE_EDITOR, true);
			executionHelper.perform(activePart.getSite().getShell());
		}
//			}
//			catch (final CoreException e) {
//				StatusManager.getManager().handle(new Status(
//						IStatus.ERROR, RUI.BUNDLE_ID, -1,
//						Messages.InlineTemp_Wizard_title, e),
//						StatusManager.LOG | StatusManager.SHOW);
//			}
		return null;
	}
	
}
