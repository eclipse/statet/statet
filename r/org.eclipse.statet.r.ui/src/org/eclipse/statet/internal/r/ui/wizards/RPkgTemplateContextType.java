/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.wizards;

import org.eclipse.jface.text.templates.TemplateVariableResolver;
import org.eclipse.text.templates.ContextTypeRegistry;

import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.ltk.ui.templates.SourceEditorContextType;


public class RPkgTemplateContextType extends SourceEditorContextType {
	
	
/* context types **************************************************************/
	public static final String NEW_DESCRIPTION_CONTEXTTYPE= "RPkg_NewDescriptionFile"; //$NON-NLS-1$
	public static final String NEW_NAMESPACE_CONTEXTTYPE= "RPkg_NewNamespaceFile"; //$NON-NLS-1$
	
/* templates ******************************************************************/
	public static final String NEW_DESCRIPTION_FILE_ID= "RPkg_NewDescriptionFile"; //$NON-NLS-1$
	public static final String NEW_NAMESPACE_FILE_ID= "RPkg_NewNamespaceFile"; //$NON-NLS-1$
	
/* variables ******************************************************************/
	public static final String R_PKG_NAME_VAR_NAME= "r_pkg_name"; //$NON-NLS-1$
	public static final String R_PKG_AUTHOR_VAR_NAME= "r_pkg_author"; //$NON-NLS-1$
	public static final String R_PKG_MAINTAINER_VAR_NAME= "r_pkg_maintainer"; //$NON-NLS-1$
	
	
	public static void registerContextTypes(final ContextTypeRegistry registry) {
		registry.addContextType(new RPkgTemplateContextType(NEW_DESCRIPTION_CONTEXTTYPE));
		registry.addContextType(new RPkgTemplateContextType(NEW_NAMESPACE_CONTEXTTYPE));
	}
	
	
	private static class RPkgNameVariableResolver extends TemplateVariableResolver {
		
		protected RPkgNameVariableResolver() {
			super(R_PKG_NAME_VAR_NAME, RUIMessages.Templates_Variable_RPkgName_description);
		}
		
	}
	
	
	RPkgTemplateContextType(final String contextName) {
		super(contextName);
		
		addCommonVariables();
		addSourceUnitGenerationVariables();
		addResolver(new RPkgNameVariableResolver());
	}
	
}
