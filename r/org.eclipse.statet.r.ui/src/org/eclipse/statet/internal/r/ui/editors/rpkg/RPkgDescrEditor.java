/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.editors.rpkg;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1OutlinePage;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.source.doc.RPkgDescrDocumentContentInfo;
import org.eclipse.statet.r.ui.sourceediting.RPkgDescrSourceViewerConfiguration;
import org.eclipse.statet.r.ui.sourceediting.RPkgDescrSourceViewerConfigurator;
import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public class RPkgDescrEditor extends SourceEditor1 {
	
	
	private RPkgDescrSourceViewerConfigurator config;
	
	
	public RPkgDescrEditor() {
		super(RCore.RPKG_DESCRIPTION_CONTENT_TYPE);
		this.config= nonNullAssert(this.config);
	}
	
	
	@Override
	protected void initializeEditor() {
		super.initializeEditor();
		
		setEditorContextMenuId("org.eclipse.statet.r.menus.RPkgDescrEditorContextMenu"); //$NON-NLS-1$
	}
	
	@Override
	protected SourceEditorViewerConfigurator createConfiguration() {
		setDocumentProvider(RUIPlugin.getInstance().getRPkgDescrDocumentProvider());
		
		enableStructuralFeatures(RModel.getRModelManager(), null, null);
		
		this.config= new RPkgDescrSourceViewerConfigurator(RCore.getWorkbenchAccess(),
				new RPkgDescrSourceViewerConfiguration(RPkgDescrDocumentContentInfo.INSTANCE, 0, this,
						null, null, null ));
		return this.config;
	}
	
	
	@Override
	public @Nullable RSourceUnit getSourceUnit() {
		return (RSourceUnit)super.getSourceUnit();
	}
	
	public RCoreAccess getRCoreAccess() {
		return this.config.getRCoreAccess();
	}
	
	@Override
	protected void setupConfiguration(final @Nullable IEditorInput newInput) {
		super.setupConfiguration(newInput);
		
		final RSourceUnit su= getSourceUnit();
		this.config.setSource(RCore.getContextAccess(su));
	}
	
	
	@Override
	protected void handlePreferenceStoreChanged(final org.eclipse.jface.util.PropertyChangeEvent event) {
		if (AbstractDecoratedTextEditorPreferenceConstants.EDITOR_TAB_WIDTH.equals(event.getProperty())
				|| AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS.equals(event.getProperty())) {
			return;
		}
		super.handlePreferenceStoreChanged(event);
	}
	
	
	@Override
	protected boolean isTabsToSpacesConversionEnabled() {
		return false;
	}
	
	
	@Override
	protected void collectContextMenuPreferencePages(final List<String> pageIds) {
		super.collectContextMenuPreferencePages(pageIds);
		pageIds.add("org.eclipse.statet.r.preferencePages.REditorOptions"); //$NON-NLS-1$
		pageIds.add("org.eclipse.statet.r.preferencePages.RTextStyles"); //$NON-NLS-1$
		pageIds.add("org.eclipse.statet.yaml.preferencePages.YamlTextStyles"); //$NON-NLS-1$
	}
	
	@Override
	protected SourceEditor1OutlinePage createOutlinePage() {
		return new RPkgDescrOutlinePage(this);
	}
	
	
	@Override
	public @NonNull String[] getShowInTargetIds() {
		return new @NonNull String[] {
			IPageLayout.ID_PROJECT_EXPLORER,
			IPageLayout.ID_OUTLINE };
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RCoreAccess.class) {
			return (T)getRCoreAccess();
		}
		if (adapterType == REnv.class) {
			return (T)getRCoreAccess().getREnv();
		}
		return super.getAdapter(adapterType);
	}
	
}
