/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.refactoring;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.typed.PojoProperties;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.databinding.wizard.WizardPageSupport;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.ltk.ui.refactoring.RefactoringWizard;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.ecommons.ui.components.ButtonGroup;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils.CheckboxTableComposite;

import org.eclipse.statet.ltk.ui.refactoring.RefactoringBasedStatus;
import org.eclipse.statet.r.core.refactoring.ExtractFunctionRefactoring;
import org.eclipse.statet.r.core.refactoring.ExtractFunctionRefactoring.Variable;


public class ExtractFunctionWizard extends RefactoringWizard {
	
	
	private static class InputPage extends UserInputWizardPage {
		
		
		public static final String PAGE_NAME= "ExtractFunction.InputPage"; //$NON-NLS-1$
		
		
		private static final ViewerFilter[] FILTER_DEFAULT= new ViewerFilter[] {
			new ViewerFilter() {
				@Override
				public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
					if (element instanceof final Variable variable) {
						return (variable.getUseAsArgumentDefault() || variable.getUseAsArgument());
					}
					return true;
				}
			}
		};
		
		private static final ViewerFilter[] FILTER_OFF= new ViewerFilter[0];
		
		
		private Text variableNameControl;
		private CheckboxTableViewer argumentsViewer;
		private ButtonGroup<Variable> argumentsButtons;
		
		
		public InputPage() {
			super(PAGE_NAME);
		}
		
		@Override
		protected ExtractFunctionRefactoring getRefactoring() {
			return (ExtractFunctionRefactoring) super.getRefactoring();
		}
		
		@Override
		public void createControl(final Composite parent) {
			final Composite composite= new Composite(parent, SWT.NONE);
			composite.setLayout(LayoutUtils.newDialogGrid(2));
			setControl(composite);
			
			{	final String title= Messages.ExtractFunction_Wizard_header;
				final Label label= new Label(composite, SWT.NONE);
				label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
				label.setText(title);
				label.setFont(JFaceResources.getBannerFont());
			}
			
			LayoutUtils.addSmallFiller(composite, false);
			
			{	final Label label= new Label(composite, SWT.NONE);
				label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
				label.setText(Messages.ExtractFunction_Wizard_VariableName_label);
				
				this.variableNameControl= new Text(composite, SWT.BORDER);
				this.variableNameControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
				this.variableNameControl.setFont(JFaceResources.getTextFont());
			}
			
			LayoutUtils.addSmallFiller(composite, false);
			
			final Control table= createArgumentsTable(composite);
			table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
			
			LayoutUtils.addSmallFiller(composite, false);
			Dialog.applyDialogFont(composite);
//			PlatformUI.getWorkbench().getHelpSystem().setHelp(getControl(),);
			
			this.argumentsViewer.setFilters(FILTER_DEFAULT);
			initBindings();
			for (final Variable variable : getRefactoring().getVariables()) {
				this.argumentsViewer.setChecked(variable, variable.getUseAsArgumentDefault());
			}
			this.argumentsButtons.updateState();
		}
		
		private Control createArgumentsTable(final Composite parent) {
			final Composite composite= new Composite(parent, SWT.NONE);
			composite.setLayout(LayoutUtils.newCompositeGrid(2));
			
			{	final Composite above= new Composite(composite, SWT.NONE);
				above.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false, 2, 1));
				above.setLayout(LayoutUtils.newCompositeGrid(2));
				
				final Label label= new Label(above, SWT.NONE);
				label.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, false));
				label.setText("Function &parameters:");
				label.addTraverseListener(new TraverseListener() {
					@Override
					public void keyTraversed(final TraverseEvent e) {
						if (e.detail == SWT.TRAVERSE_MNEMONIC) {
							e.doit= false;
							InputPage.this.argumentsViewer.getControl().setFocus();
						}
					}
				});
				final Button showAll= new Button(above, SWT.CHECK);
				showAll.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false));
				showAll.setText("Show &all identifiers");
				showAll.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(final SelectionEvent e) {
						if (showAll.getSelection()) {
							InputPage.this.argumentsViewer.setFilters(FILTER_OFF);
						}
						else {
							InputPage.this.argumentsViewer.setFilters(FILTER_DEFAULT);
						}
					}
				});
			}
			
			final CheckboxTableComposite table= new CheckboxTableComposite(composite, SWT.BORDER | SWT.SINGLE | SWT.FULL_SELECTION);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
			gd.heightHint= LayoutUtils.hintHeight(table.table, 12);
			table.setLayoutData(gd);
			table.table.setHeaderVisible(true);
			table.table.setLinesVisible(true);
			this.argumentsViewer= table.viewer;
			
			{	final TableViewerColumn column= table.addColumn("Variable", SWT.LEFT, new ColumnWeightData(1));
				column.setLabelProvider(new CellLabelProvider() {
					@Override
					public void update(final ViewerCell cell) {
						final Object element= cell.getElement();
						if (element instanceof Variable) {
							cell.setFont(JFaceResources.getTextFont());
							final Variable variable= (Variable) element;
							cell.setText(variable.getName());
							return;
						}
						cell.setText("");
					}
				});
			}
			
			this.argumentsViewer.setContentProvider(new ArrayContentProvider());
			
			this.argumentsButtons= new ButtonGroup<>(composite);
			this.argumentsButtons.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true));
			this.argumentsButtons.addUpButton(null);
			this.argumentsButtons.addDownButton(null);
			
			return composite;
		}
		
		protected void initBindings() {
			final Realm realm= Realm.getDefault();
			final DataBindingContext dbc= new DataBindingContext(realm);
			
			addBindings(dbc, realm);
			WizardPageSupport.create(this, dbc);
		}
		
		protected void addBindings(final DataBindingContext dbc, final Realm realm) {
			dbc.bindValue(
					WidgetProperties.text(SWT.Modify)
							.observe(this.variableNameControl),
					PojoProperties.value(ExtractFunctionRefactoring.class, "functionName", String.class)
							.observe(realm, getRefactoring()),
					new UpdateValueStrategy<String, String>()
							.setAfterGetValidator((final String value) ->
									new RefactoringBasedStatus(
											getRefactoring().checkFunctionName(value) )),
					null );
			this.argumentsViewer.addCheckStateListener(
					(final CheckStateChangedEvent event) -> {
						final Object element= event.getElement();
						if (element instanceof final Variable variable) {
							variable.setUseAsArgument(event.getChecked());
						}
					} );
			final IObservableList<Variable> argumentsList= new WritableList<>(realm, getRefactoring().getVariables(), Variable.class);
			this.argumentsViewer.setInput(argumentsList);
			this.argumentsButtons.connectTo(this.argumentsViewer, argumentsList, null);
			
//			dbc.bindValue(WidgetProperties.buttonSelection()
//							.observe(this.replaceAllControl),
//					PojoProperties.value(ExtractFunctionRefactoring.class, "replaceAllOccurrences", Boolean.TYPE)
//							.observe(realm, getRefactoring()) );
		}
		
		@Override
		public void setVisible(final boolean visible) {
			super.setVisible(visible);
			this.variableNameControl.setFocus();
		}
		
	}
	
	
	public ExtractFunctionWizard(final ExtractFunctionRefactoring ref) {
		super(ref, DIALOG_BASED_USER_INTERFACE | PREVIEW_EXPAND_FIRST_NODE | NO_BACK_BUTTON_ON_STATUS_DIALOG);
		setDefaultPageTitle(Messages.ExtractFunction_Wizard_title);
	}
	
	
	@Override
	protected void addUserInputPages() {
		addPage(new InputPage());
	}
	
}
