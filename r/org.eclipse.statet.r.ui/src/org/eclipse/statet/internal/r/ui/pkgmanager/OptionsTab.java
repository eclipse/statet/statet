/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.ui.pkgmanager;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabItem;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.r.ui.RUI;


public class OptionsTab extends Composite{
	
	
	private static final BooleanPref INSTALL_SUGGESTED_PREF= new BooleanPref(
			RUI.BUNDLE_ID + "/r.pkgmgr", "InstallSuggested.enabled"); //$NON-NLS-1$ //$NON-NLS-2$
	
	
	private final RPkgManagerDialog dialog;
	
	private final TabItem tab;
	
	private Button suggestedButton;
	
	
	public OptionsTab(final RPkgManagerDialog dialog, final TabItem tab, final Composite parent) {
		super(parent, SWT.NONE);
		
		this.dialog= dialog;
		this.tab= tab;
		
		setLayout(LayoutUtils.newTabGrid(1));
		createContent(this);
		
		initOptions();
	}
	
	
	boolean installSuggested() {
		return this.suggestedButton.getSelection();
	}
	
	
	private void createContent(final Composite parent) {
		final Composite filterCol= createInstall(parent);
		filterCol.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	}
	
	private Composite createInstall(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newGroupGrid(1));
		composite.setText("Installation");
		
		{	final Button button= new Button(composite, SWT.CHECK);
			this.suggestedButton= button;
			button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			button.setText("Include packages '&suggested' by the selected packages.");
		}
		
		return composite;
	}
	
	private void initOptions() {
		final Boolean value= EPreferences.getInstancePrefs()
				.getPreferenceValue(INSTALL_SUGGESTED_PREF);
		this.suggestedButton.setSelection(value != null && value.booleanValue());
		
		this.suggestedButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				PreferenceUtils.setPrefValue(InstanceScope.INSTANCE, INSTALL_SUGGESTED_PREF,
						OptionsTab.this.suggestedButton.getSelection(),
						PreferenceUtils.FLUSH_ASYNC );
			}
		});
	}
	
}
