/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.codegeneration;

import org.eclipse.text.templates.ContextTypeRegistry;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.templates.SourceEditorContextType;


@NonNullByDefault
public class RdCodeTemplateContextType extends SourceEditorContextType {
	
	
/* context types **************************************************************/
	public static final String NEW_RDOCFILE_CONTEXTTYPE= "rd_NewRDocFile_context"; //$NON-NLS-1$
	
/* templates ******************************************************************/
	public static final String NEW_RDOCFILE= "rd_NewRDocFile";	 //$NON-NLS-1$
	
	
	public static void registerContextTypes(final ContextTypeRegistry registry) {
		registry.addContextType(new RdCodeTemplateContextType(NEW_RDOCFILE_CONTEXTTYPE));
	}
	
	
	RdCodeTemplateContextType(final String contextName) {
		super(contextName);
		
		addCommonVariables();
		if (NEW_RDOCFILE_CONTEXTTYPE.equals(contextName)) {
			addSourceUnitGenerationVariables();
		}
		
	}
	
}
