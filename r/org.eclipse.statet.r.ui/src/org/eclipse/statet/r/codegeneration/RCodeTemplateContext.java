/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.codegeneration;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.templates.CodeGenerationTemplateContext;
import org.eclipse.statet.r.core.model.RElement;


@NonNullByDefault
public class RCodeTemplateContext extends CodeGenerationTemplateContext {
	
	
	RCodeTemplateContext(final String contextTypeName, final SourceUnit su, final String lineDelim) {
		super(RUIPlugin.getInstance().getRCodeGenerationTemplateContextRegistry().getContextType(contextTypeName),
				su, lineDelim);
	}
	
	
	public void setRElement(final RElement element) {
		setVariable(RCodeTemplateContextType.ELEMENT_NAME_VAR_NAME, element.getElementName().getDisplayName());
	}
	
}
