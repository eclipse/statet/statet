/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rhelp;

import java.net.URI;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.InfoStatus;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.ecommons.ui.mpbv.BrowserSession;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.ui.rhelp.RHelpView;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.util.AbstractREnvHandler;
import org.eclipse.statet.rhelp.core.http.RHelpHttpService;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public class OpenRHelpHandler extends AbstractREnvHandler {
	
	
	private final boolean reusePage;
	
	
	public OpenRHelpHandler(final boolean reusePage) {
		this.reusePage= reusePage;
	}
	
	public OpenRHelpHandler() {
		this(true);
	}
	
	
	private @Nullable URI getUrl(final REnv rEnv, final @Nullable String urlString) {
		if (urlString == null || urlString.isEmpty()
				|| urlString.equals("about:blank")) { //$NON-NLS-1$
			return RCore.getRHelpHttpService().getREnvHttpUrl(
					rEnv, RHelpHttpService.BROWSE_TARGET );
		}
		else {
			return RCore.getRHelpHttpService().toHttpUrl(
					urlString, rEnv, RHelpHttpService.BROWSE_TARGET );
		}
	}
	
	@Override
	protected @Nullable Object execute(final REnv rEnv,
			final ExecutionEvent event) throws ExecutionException {
		final URI url= getUrl(rEnv, event.getParameter("url")); //$NON-NLS-1$
		if (url == null) {
			return null;
		}
		try {
			final RHelpView view= (RHelpView)HandlerUtil.getActiveWorkbenchWindowChecked(event)
					.getActivePage()
					.showView(RUI.R_HELP_VIEW_ID);
			return show(view, url);
		}
		catch (final CoreException e) {
			throw new ExecutionException("An error occured when opening R help page in R help view.",
					e );
		}
	}
	
	public @Nullable Status execute(final RTool rTool, final @Nullable String urlString) {
		final REnv rEnv= rTool.getREnv();
		if (rEnv == null) {
			return new InfoStatus(RUI.BUNDLE_ID, "Not supported for this R tool.");
		}
		final URI url= getUrl(rEnv, urlString);
		if (url == null) {
			return null;
		}
		return UIAccess.getDisplay().syncCall(() -> {
			try {
				final RHelpView view;
				if (rTool instanceof ToolProcess) {
					view= (RHelpView)NicoUITools.getView(RUI.R_HELP_VIEW_ID, rTool, true);
				}
				else {
					view= (RHelpView)UIAccess.getActiveWorkbenchPage(true)
							.showView(RUI.R_HELP_VIEW_ID);
				}
				show(view, url);
				return Status.OK_STATUS;
			}
			catch (final CoreException e) {
				final Status status= new ErrorStatus(RUI.BUNDLE_ID,
						"An error occured when opening R help page in R help view.",
						e );
				StatusManager.getManager().handle(EStatusUtils.convert(status));
				return status;
			}
		});
	}
	
	private BrowserSession show(final RHelpView view, final URI url) {
		return view.openUrl(url, (OpenRHelpHandler.this.reusePage) ?
				BrowserSession.findSessionByUrl(view.getSessions(), url) :
				null );
	}
	
}
