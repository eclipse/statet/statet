/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.nico.core.util.ToolTerminateListener;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.rj.services.FQRObjectRef;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public class RToolDataTableInput implements RDataTableInput {
	
	
	private final RElementName elementName;
	
	private final String fullName;
	private final String shortName;
	
	private final FQRObjectRef<? extends RTool> elementRef;
	
	private final RTool tool;
	private @Nullable ToolTerminateListener processListener;
	
	private final CopyOnWriteIdentityListSet<RDataTableInput.StateListener> listeners= new CopyOnWriteIdentityListSet<>();
	
	
	public RToolDataTableInput(RElementName elementName, final FQRObjectRef<? extends RTool> elementRef) {
		if (elementName == null) {
			throw new NullPointerException("name"); //$NON-NLS-1$
		}
		if (elementRef == null) {
			throw new NullPointerException("elementRef"); //$NON-NLS-1$
		}
		if (!(elementRef.getRHandle() instanceof RTool)) {
			throw new IllegalArgumentException("Unsupported elementRef.rHandle"); //$NON-NLS-1$
		}
		
		this.elementName= elementName;
		this.elementRef= elementRef;
		this.fullName= RElementName.createDisplayName(elementName, RElementName.DISPLAY_FQN | RElementName.DISPLAY_EXACT);
		
		RElementName name= elementName;
		while (elementName.getNextSegment() != null) {
			if (elementName.getType() == RElementName.MAIN_DEFAULT) {
				name= elementName;
			}
			
			elementName= elementName.getNextSegment();
		}
		this.shortName= name.getDisplayName();
		
		this.tool= elementRef.getRHandle();
	}
	
	
	@Override
	public RElementName getElementName() {
		return this.elementName;
	}
	
	@Override
	public String getFullName() {
		return this.fullName;
	}
	
	@Override
	public String getName() {
		return this.shortName;
	}
	
	@Override
	public FQRObjectRef<? extends RTool> getElementRef() {
		return this.elementRef;
	}
	
	@Override
	public Tool getTool() {
		return this.tool;
	}
	
	@Override
	public boolean isAvailable() {
		return (!this.tool.isTerminated());
	}
	
	@Override
	public void addStateListener(final StateListener listener) {
		synchronized (this.listeners) {
			ToolTerminateListener processListener;
			if (this.listeners.add(listener)
					&& (processListener= this.processListener) == null) {
				processListener= new ToolTerminateListener(this.tool) {
					@Override
					public void toolTerminated() {
						final ImList<StateListener> listeners;
						synchronized (RToolDataTableInput.this.listeners) {
							dispose();
							RToolDataTableInput.this.processListener= null;
							
							listeners= RToolDataTableInput.this.listeners.toList();
						}
						for (final RDataTableInput.StateListener listener : listeners) {
							listener.tableUnavailable();
						}
					}
				};
				processListener.install();
				this.processListener= processListener;
			}
		}
	}
	
	@Override
	public void removeStateListener(final StateListener listener) {
		synchronized (this.listeners) {
			final ToolTerminateListener processListener;
			if (this.listeners.remove(listener)
					&& this.listeners.isEmpty()
					&& (processListener= this.processListener) != null) {
				this.processListener= null;
				processListener.dispose();
			}
		}
	}
	
	
	@Override
	public int hashCode() {
		return this.shortName.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RToolDataTableInput other
						&& this.tool.equals(other.tool)
						&& this.fullName.equals(other.fullName) ));
	}
	
	
	@Override
	public String toString() {
		return getClass().getName() + "(" + this.fullName //$NON-NLS-1$
				+ " in " + this.tool.getLabel(Tool.LONG_LABEL) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
