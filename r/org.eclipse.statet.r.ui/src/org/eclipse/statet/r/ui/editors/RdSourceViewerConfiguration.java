/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.editors;

import static org.eclipse.statet.r.ui.text.rd.IRdTextTokens.COMMENT;
import static org.eclipse.statet.r.ui.text.rd.IRdTextTokens.PLATFORM_SPECIF;
import static org.eclipse.statet.r.ui.text.rd.IRdTextTokens.TASK_TAG;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.source.ISourceViewer;

import org.eclipse.statet.ecommons.text.ui.presentation.SingleTokenScanner;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;
import org.eclipse.statet.ecommons.ui.ISettingsChangedHandler;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.text.core.CharPairMatcher;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.presentation.CommentScanner;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RdDocumentContentInfo;
import org.eclipse.statet.r.core.source.util.RdBracketPairMatcher;
import org.eclipse.statet.r.ui.text.rd.RdCodeScanner;
import org.eclipse.statet.r.ui.text.rd.RdDoubleClickStrategy;


/**
 * Default Configuration for SourceViewer of R documentations.
 */
public class RdSourceViewerConfiguration extends SourceEditorViewerConfiguration
		implements ISettingsChangedHandler {
	
	
	private static final String[] CONTENT_TYPES= RDocumentConstants.RDOC_CONTENT_TYPES.toArray(
			new String[RDocumentConstants.RDOC_CONTENT_TYPES.size()] );
	
	
	private final RdDoubleClickStrategy fDoubleClickStrategy;
	
	private RCoreAccess fRCoreAccess;
	
	
	public RdSourceViewerConfiguration(final int flags) {
		this(flags, null, null, null, null);
	}
	
	public RdSourceViewerConfiguration(final int flags,
			final SourceEditor sourceEditor,
			final RCoreAccess access,
			final IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		super(RdDocumentContentInfo.INSTANCE, flags, sourceEditor);
		setCoreAccess(access);
		
		setup((preferenceStore != null) ? preferenceStore : RUIPlugin.getInstance().getEditorPreferenceStore(),
				LtkUIPreferences.getEditorDecorationPreferences(),
				LtkUIPreferences.getAssistPreferences() );
		setTextStyles(textStyles);
		this.fDoubleClickStrategy = new RdDoubleClickStrategy();
	}
	
	protected void setCoreAccess(final RCoreAccess access) {
		this.fRCoreAccess = (access != null) ? access : RCore.getWorkbenchAccess();
	}
	
	
	@Override
	protected void initTextStyles() {
		setTextStyles(RUIPlugin.getInstance().getRdTextStyles());
	}
	
	@Override
	protected void initScanners() {
		final var textStyles= getTextStyles();
		
		addScanner(RDocumentConstants.RDOC_DEFAULT_CONTENT_TYPE,
				new RdCodeScanner(textStyles) );
		addScanner(RDocumentConstants.RDOC_COMMENT_CONTENT_TYPE,
				new CommentScanner(textStyles, COMMENT, TASK_TAG, this.fRCoreAccess.getPrefs()) );
		addScanner(RDocumentConstants.RDOC_PLATFORM_SPEC_CONTENT_TYPE,
				new SingleTokenScanner(textStyles, PLATFORM_SPECIF ) );
	}
	
	
	@Override
	public String[] getConfiguredContentTypes(final ISourceViewer sourceViewer) {
		return CONTENT_TYPES;
	}
	
	
	@Override
	public CharPairMatcher getPairMatcher() {
		return new RdBracketPairMatcher(getDocumentContentInfo());
	}
	
	@Override
	public ITextDoubleClickStrategy getDoubleClickStrategy(final ISourceViewer sourceViewer, final String contentType) {
		return this.fDoubleClickStrategy;
	}
	
	
	@Override
	public String[] getDefaultPrefixes(final ISourceViewer sourceViewer, final String contentType) {
		return new String[] { "%", "" }; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	
	@Override
	protected ContentAssist createContentAssistant(final ISourceViewer sourceViewer) {
		return null;
	}
	
}
