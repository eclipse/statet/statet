/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.ui.PartInitException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.actions.AbstractOpenDeclarationHandler;
import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RModelManager;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.Pipe;
import org.eclipse.statet.r.core.source.ast.PlaceholderDetail;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;


@NonNullByDefault
public class ROpenDeclarationHandler extends AbstractOpenDeclarationHandler {
	
	
	public static @Nullable Object searchAccess(final SourceEditor editor, final TextRegion region) {
		// not for sub elements
		try {
			final IDocument document= editor.getViewer().getDocument();
			if (document == null) {
				return null;
			}
			final RHeuristicTokenScanner scanner= RHeuristicTokenScanner.create(
					editor.getDocumentContentInfo() );
			final ITypedRegion partition= TextUtilities.getPartition(document,
					scanner.getDocumentPartitioning(), region.getStartOffset(), true );
			final SourceUnit su= editor.getSourceUnit();
			if (su instanceof RSourceUnit
					&& region.getStartOffset() < document.getLength()
					&& ( RDocumentConstants.R_DEFAULT_CONTENT_CONSTRAINT.matches(partition.getType())
						|| partition.getType() == RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE
						|| partition.getType() == RDocumentConstants.R_STRING_CONTENT_TYPE )) {
				
				final RSourceUnitModelInfo modelInfo= (RSourceUnitModelInfo)su.getModelInfo(RModel.R_TYPE_ID,
						RModelManager.MODEL_FILE, new NullProgressMonitor() );
				if (modelInfo != null) {
					final AstInfo astInfo= modelInfo.getAst();
					final AstSelection astSelection= AstSelection.search(astInfo.getRoot(),
							region.getStartOffset(), region.getEndOffset(),
							AstSelection.MODE_COVERING_SAME_LAST );
					final AstNode covering= astSelection.getCovering();
					if (covering instanceof final RAstNode node) {
						switch (node.getNodeType()) {
						case SYMBOL:
						case STRING_CONST:
							{	RAstNode current= node;
								do {
									final List<Object> attachments= current.getAttachments();
									for (final Object attachment : attachments) {
										if (attachment instanceof final RElementAccess access) {
											if (access.getNameNode() == node) {
												return access;
											}
										}
									}
									current= current.getRParent();
								} while (current != null);
								break;
							}
						case PLACEHOLDER:
							{	final List<Object> attachments= node.getAttachments();
								for (final Object attachment : attachments) {
									if (attachment instanceof PlaceholderDetail) {
										return attachment;
									}
								}
								break;
							}
						default:
							break;
						}
					}
				}
			}
		}
		catch (final BadLocationException e) {
			RUIPlugin.logUncriticalError(e);
		}
		return null;
	}
	
	
	public ROpenDeclarationHandler() {
	}
	
	
	@Override
	public boolean execute(final SourceEditor editor, final TextRegion selection) {
		final var access= searchAccess(editor, selection);
		String name= null;
		try {
			if (access instanceof final RElementAccess elementAccess) {
				name= elementAccess.getDisplayName();
				final List<SourceElement> list= RModel.searchDeclaration(elementAccess,
						(RSourceUnit)editor.getSourceUnit() );
				final var open= new ROpenDeclaration();
				final var element= open.selectElement(list, editor.getWorkbenchPart());
				if (element != null) {
					open.open(element, true);
					return true;
				}
			}
			else if (access instanceof PlaceholderDetail) {
				name= "_"; //$NON-NLS-1$
				var declNode= ((PlaceholderDetail)access).getSubstitute();
				if (declNode.getNodeType() == NodeType.PIPE_FORWARD) {
					declNode= ((Pipe)declNode).getTargetChild();
				}
				editor.selectAndReveal(declNode.getStartOffset(), declNode.getLength());
				return true;
			}
			return false;
		}
		catch (final PartInitException e) {
			logError(e, name);
			return false;
		}
		catch (final CoreException e) {
			if (e.getStatus().getSeverity() == IStatus.ERROR) {
				logError(e, name);
				return false;
			}
			return true; // cancelled
		}
	}
	
}
