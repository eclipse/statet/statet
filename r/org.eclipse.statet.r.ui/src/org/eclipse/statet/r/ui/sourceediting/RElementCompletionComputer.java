/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.ui.editors.AbstractRCompletionElementComputer;
import org.eclipse.statet.internal.r.ui.editors.RElementCompletionProposal;
import org.eclipse.statet.internal.r.ui.editors.RElementCompletionProposal.RElementProposalParameters;
import org.eclipse.statet.internal.r.ui.editors.RHelpTopicCompletionProposal;
import org.eclipse.statet.internal.r.ui.editors.RSimpleCompletionProposal;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SourceProposal.ProposalParameters;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RSearchPattern;
import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RFunctionSpec;
import org.eclipse.statet.r.core.model.rlang.RFrame;
import org.eclipse.statet.r.core.model.rlang.RFrameSearchPath.Iterator;
import org.eclipse.statet.r.core.model.rlang.RLangElement;
import org.eclipse.statet.r.core.model.rlang.RSrcStrFrame;
import org.eclipse.statet.r.core.rlang.RTokens;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext.FCallInfo;
import org.eclipse.statet.rhelp.core.REnvHelp;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rhelp.core.RHelpTopicEntry;
import org.eclipse.statet.rhelp.core.RPkgHelp;
import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public class RElementCompletionComputer
		extends AbstractRCompletionElementComputer implements ContentAssistComputer {
	
	
	public static class CompleteRuntime extends RElementCompletionComputer {
		
		public CompleteRuntime() {
			super(WorkbenchRFrameSearchPath.ENGINE_MODE);
		}
		
	}
	
	
	private final WorkbenchRFrameSearchPath searchPath= new WorkbenchRFrameSearchPath();
	
	
	protected RElementCompletionComputer(final int rSearchMode) {
		super(rSearchMode);
	}
	
	public RElementCompletionComputer() {
		super(0);
	}
	
	
	@Override
	protected void clear() {
		super.clear();
		this.searchPath.clear();
	}
	
	
	protected final boolean isSymbolCandidate(final String name) {
		for (int i= 0; i < name.length(); i++) {
			if (RTokens.isRobustSeparator(name.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	
	@Override
	protected void computeCompletionProposals(final int mode, final IProgressMonitor monitor) {
		if (getRContext().getModelInfo() == null) {
			return;
		}
		
		final RAstNode node= getRContext().getInvocationRAstNode();
		
		final RElementName prefixName= getRContext().getIdentifierElementName();
		if (prefixName == null) {
			return;
		}
		
		if (prefixName.getNextSegment() == null) {
			final String help= checkHelp();
			if (help != null) {
				addHelpTopicProposals(help, node, prefixName, ARG_TYPE_PRIO);
				
				if (prefixName.getScope() == null) {
					addPkgNameProposals(prefixName, ARG_TYPE_NO_PRIO);
				}
				return;
			}
			
			doComputeArgumentProposals(prefixName, monitor);
			if (this.in == IN_DEFAULT) {
				doComputeMainProposals(node, prefixName, monitor);
			}
			
			if (this.rSearchMode == 0 && this.in == IN_DEFAULT) {
				addKeywordCompletions(prefixName);
			}
			
			if (this.rSearchMode == 0) {
				if (isPackageName(prefixName)) {
					if (this.pkgNamePrio > 0) {
						addPkgNameProposals(prefixName, this.pkgNamePrio);
					}
					else if (!prefixName.getSegmentName().isEmpty()) {
						addPkgNameProposals(prefixName, ARG_TYPE_NO_PRIO);
					}
				}
				{	ContextSubject subject;
					if ((subject= getSubject(RFunctionSpec.HELP_TOPIC_NAME)) != null && subject.prio > 0) {
						addHelpTopicProposals(null, node, prefixName, subject.prio);
					}
					if ((subject= getSubject(RFunctionSpec.RELATOR_CODE)) != null && subject.prio > 0) {
						addRoleCodeCompletions(subject.prio);
					}
				}
			}
		}
		else {
			doComputeSubProposals(node, prefixName, monitor);
		}
	}
	
	
	private @Nullable String checkHelp() {
		try {
			if (getRContext().getIdentifierOffset() > 0
					&& getRContext().getDocument().getChar(getRContext().getIdentifierOffset() - 1) == '?') {
				final String prefix= getRContext().computeIdentifierPrefix(getRContext().getIdentifierOffset() - 1);
				if (prefix != null && !prefix.isEmpty()) {
					if (prefix.equals("class") || prefix.equals("methods")) { //$NON-NLS-1$ //$NON-NLS-2$
						return prefix;
					}
					return null;
				}
				return ""; //$NON-NLS-1$
			}
			return null;
		}
		catch (final BadPartitioningException | BadLocationException e) {
			log(e);
			return null;
		}
	}
	
	
	protected void doComputeArgumentProposals(final RElementName prefixName,
			final IProgressMonitor monitor) {
		try {
			final FCallInfo fCallInfo= getRContext().getFCallInfo();
			if (fCallInfo != null) {
				addFCallArgCompletions(fCallInfo, prefixName, null);
			}
		}
		catch (final Exception e) {
			log(e);
		}
	}
	
	protected void doComputeMainProposals(final RAstNode node, final RElementName prefixName,
			final IProgressMonitor monitor) {
		try {
			this.searchPath.init(getRContext(), node, getRSearchMode(), prefixName.getScope());
			
			addMainElementCompletions(this.searchPath, prefixName);
		}
		catch (final Exception e) {
			log(e);
		}
	}
	
	protected void doComputeSubProposals(final RAstNode node, final RElementName prefixName,
			final IProgressMonitor monitor) {
		this.searchPath.init(getRContext(), node, getRSearchMode(), prefixName.getScope());
		
		int count= 0;
		final String namePrefix;
		{	RElementName prefixSegment= prefixName;
			while (true) {
				count++;
				if (prefixSegment.getNextSegment() != null) {
					prefixSegment= prefixSegment.getNextSegment();
					continue;
				}
				else {
					break;
				}
			}
			namePrefix= (prefixSegment.getSegmentName() != null) ?
					prefixSegment.getSegmentName() : ""; //$NON-NLS-1$
		}
		
		final RElementProposalParameters parameters= new RElementProposalParameters(
				getRContext(), getRContext().getIdentifierLastSegmentOffset(),
				new RSearchPattern(getSearchMatchRules(), namePrefix),
				this.labelProvider );
		
		final Set<String> mainNames= new HashSet<>();
		final List<String> methodNames= new ArrayList<>();
		
		for (final var iter= this.searchPath.iterator(); iter.hasNext();) {
			final RFrame<?> envir= iter.next();
			parameters.baseRelevance= iter.getRelevance();
			
			final List<? extends RLangElement> elements= envir.getModelChildren(null);
			ITER_ELEMENTS: for (final RLangElement rootElement : elements) {
				final RElementName elementName= rootElement.getElementName();
				final int c1type= (rootElement.getElementType() & LtkModelElement.MASK_C1);
				final boolean isRich= (c1type == LtkModelElement.C1_METHOD);
				if (isRich || c1type == LtkModelElement.C1_VARIABLE) {
					RLangElement element= rootElement;
					RElementName prefixSegment= prefixName;
					RElementName elementSegment= elementName;
					ITER_SEGMENTS: for (int i= 0; i < count-1; i++) {
						if (elementSegment == null) {
							final List<? extends RLangElement> children= getChildren(element);
							for (final RLangElement child : children) {
								elementSegment= child.getElementName();
								if (isCompletable(elementSegment)
										&& elementSegment.getSegmentName().equals(prefixSegment.getSegmentName())) {
									element= child;
									prefixSegment= prefixSegment.getNextSegment();
									elementSegment= elementSegment.getNextSegment();
									continue ITER_SEGMENTS;
								}
							}
							continue ITER_ELEMENTS;
						}
						else {
							if (isCompletable(elementSegment)
									&& elementSegment.getSegmentName().equals(prefixSegment.getSegmentName())) {
								prefixSegment= prefixSegment.getNextSegment();
								elementSegment= elementSegment.getNextSegment();
								continue ITER_SEGMENTS;
							}
							continue ITER_ELEMENTS;
						}
					}
					
					final boolean childMode;
					final List<? extends RLangElement> children;
					if (elementSegment == null) {
						childMode= true;
						children= getChildren(element);
					}
					else {
						childMode= false;
						children= Collections.singletonList(element);
					}
					for (final RLangElement child : children) {
						if (childMode) {
							elementSegment= child.getElementName();
						}
						final String candidate= elementSegment.getSegmentName();
						if (isCompletable(elementSegment)
								&& parameters.matchesNamePattern(candidate) ) {
							if ((parameters.baseRelevance > 0) && !isRich
									&& mainNames.contains(candidate) ) {
								continue ITER_ELEMENTS;
							}
							parameters.replacementName= elementSegment;
							parameters.element= child;
							
							final AssistProposal proposal= new RElementCompletionProposal(parameters);
							if (elementSegment.getNextSegment() == null) {
								if (isRich) {
									methodNames.add(candidate);
								}
								else {
									mainNames.add(candidate);
								}
							}
							this.proposals.add(proposal);
						}
					}
				}
			}
		}
		
		mainNames.addAll(methodNames);
		for (final Iterator iter= this.searchPath.iterator(); iter.hasNext();) {
			final RFrame<?> envir= iter.next();
			parameters.baseRelevance= 0;
			
			if (envir instanceof final RSrcStrFrame sframe) {
				final List<? extends RElementAccess> allAccess= sframe.getAllAccessOf(
						prefixName.getSegmentName(), true );
				if (allAccess != null) {
					ITER_ELEMENTS: for (final RElementAccess elementAccess : allAccess) {
						RElementAccess elementSegment= elementAccess;
						RElementName prefixSegment= prefixName;
						ITER_SEGMENTS: for (int i= 0; i < count - 1; i++) {
							if (isCompletable(elementSegment)
									&& elementSegment.getSegmentName().equals(prefixSegment.getSegmentName())) {
								prefixSegment= prefixSegment.getNextSegment();
								elementSegment= elementSegment.getNextSegment();
								continue ITER_SEGMENTS;
							}
							continue ITER_ELEMENTS;
						}
						
						if (elementSegment == null || elementSegment.isSlave()) {
							continue ITER_ELEMENTS;
						}
						final String candidate= elementSegment.getSegmentName();
						if (candidate != null && isCompletable(elementSegment)
								&& !candidate.equals(namePrefix)
								&& !mainNames.contains(candidate)
								&& parameters.matchesNamePattern(candidate) ) {
							
							final AssistProposal proposal= new RSimpleCompletionProposal(parameters,
									candidate );
							mainNames.add(candidate);
							this.proposals.add(proposal);
						}
					}
				}
			}
		}
	}
	
	@Override
	protected void computeContextProposals(final IProgressMonitor monitor) {
		if (getRContext().getModelInfo() == null) {
			return;
		}
		
		final FCallInfo fCallInfo= getRContext().getFCallInfo();
		if (fCallInfo != null) {
			addFCallArgContexts(fCallInfo, null);
		}
	}
	
	
	protected final void addPkgNameProposals(final RElementName prefixName,
			final int prio) {
		final String prefixSegmentName= nonNullAssert(prefixName.getSegmentName());
		if (prefixName.getScope() != null
				|| !isSymbolCandidate(prefixSegmentName)) {
			return;
		}
		
		addPkgNameCompletions(prefixSegmentName, prio);
	}
	
	protected final void addHelpTopicProposals(final @Nullable String topicType, final RAstNode node,
			final RElementName prefixName,
			final int prio) {
		// (topic != null) => ?  /  (topic == null) => help()
		final String prefixSegmentName= nonNullAssert(prefixName.getSegmentName());
		
		if (topicType == null && (prefixName.getScope() != null || prefixSegmentName.isEmpty())) {
			return;
		}
		
		final REnv rEnv= getRContext().getRCoreAccess().getREnv();
		if (rEnv == null) {
			return;
		}
		
		final RHelpManager rHelpManager= RCore.getRHelpManager();
		
		final REnvHelp help= rHelpManager.getHelp(rEnv);
		if (help != null) {
			try {
				final ProposalParameters<RAssistInvocationContext> parameters= new ProposalParameters<>(
						getRContext(), (getRContext().getInvocationContentType() != RDocumentConstants.R_DEFAULT_CONTENT_TYPE) ?
								getRContext().getIdentifierLastSegmentOffset() + 1 :
								getRContext().getIdentifierLastSegmentOffset(),
						new RSearchPattern(getSearchMatchRules(), prefixSegmentName),
						prio );
				final Map<String, RHelpTopicCompletionProposal> map= new HashMap<>();
				this.searchPath.init(getRContext(), node, getRSearchMode(), prefixName.getScope());
				final Set<String> pkgNames= new HashSet<>();
				for (final RFrame<?> frame : this.searchPath) {
					if (frame.getFrameType() == RFrame.PACKAGE) {
						final String pkgName= frame.getElementName().getSegmentName();
						if (pkgName != null && pkgNames.add(pkgName)) {
							final RPkgHelp pkgHelp= help.getPkgHelp(pkgName);
							if (pkgHelp != null) {
								for (final RHelpTopicEntry topicEntry : pkgHelp.getTopics()) {
									final String topic= topicEntry.getTopic();
									if (parameters.matchesNamePattern(topic)) {
										
										RHelpTopicCompletionProposal proposal= map.get(topic);
										if (proposal == null) {
											proposal= new RHelpTopicCompletionProposal(
													parameters, topic, topicEntry.getPage() );
											map.put(topic, proposal);
											this.proposals.add(proposal);
										}
										else {
											proposal.addPage(topicEntry.getPage());
										}
									}
								}
							}
						}
					}
				}
			}
			finally {
				help.unlock();
			}
		}
	}
	
	
}
