/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.util;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.data.CombinedRList;
import org.eclipse.statet.r.ui.RLabelProvider;


@NonNullByDefault
public class RElementInputLabelProvider extends RLabelProvider {
	
	
	public RElementInputLabelProvider(final int style) {
		super(style);
	}
	
	public RElementInputLabelProvider() {
		this(COUNT);
	}
	
	
	protected @Nullable RElementInput<?> getInput() {
		return ((RElementInputContentProvider) getViewer().getContentProvider()).getInput();
	}
	
	@Override
	protected String getEnvCountInfo(final CombinedRList envir) {
		final StringBuilder textBuilder= getTextBuilder();
		textBuilder.append(" ("); //$NON-NLS-1$
		final RElementInput<?> input= getInput();
		if (input != null && input.hasEnvFilter()) {
			final Object[] children= input.getEnvChildren(envir);
			if (children != null) {
				textBuilder.append(children.length);
			}
			else {
				textBuilder.append('-');
			}
			textBuilder.append('/');
		}
		textBuilder.append(envir.getLength());
		textBuilder.append(')');
		return textBuilder.toString();
	}
	
}
