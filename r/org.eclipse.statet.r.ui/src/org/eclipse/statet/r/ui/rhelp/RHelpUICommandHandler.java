/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rhelp;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;
import org.eclipse.statet.jcommons.util.StringUtils;

import org.eclipse.statet.rj.ts.core.AbstractRToolCommandHandler;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class RHelpUICommandHandler extends AbstractRToolCommandHandler {
	
	
	public static final String SHOW_HELP_COMMAND_ID= "org.eclipse.statet.r.rhelp.showHelp"; //$NON-NLS-1$
	@Deprecated
	public static final String SHOW_HELP_COMMAND_OLD_ID= "showHelp"; //$NON-NLS-1$
	
	
	public RHelpUICommandHandler() {
	}
	
	
	@Override
	public Status execute(final String id, final RToolService r, final ToolCommandData data,
			final ProgressMonitor m) {
		switch (id) {
		case SHOW_HELP_COMMAND_ID:
		case SHOW_HELP_COMMAND_OLD_ID:
			{	String url= data.getStringRequired("url");
				if (url.startsWith("html:///")) { //$NON-NLS-1$
					int idx= url.indexOf("<head"); //$NON-NLS-1$
					if (idx >= 0) {
						idx= url.indexOf('>', idx + 5);
						if (idx >= 0) {
							url= StringUtils.insert(url, idx + 1, "<base href=\"about:\"/>"); //$NON-NLS-1$
						}
					}
				}
				
				final OpenRHelpHandler handler= new OpenRHelpHandler(true);
				final Status status= handler.execute(r.getTool(), url);
				return (status != null) ? status : Status.OK_STATUS;
			}
			
		default:
			throw new UnsupportedOperationException();
		}
	}
	
}
