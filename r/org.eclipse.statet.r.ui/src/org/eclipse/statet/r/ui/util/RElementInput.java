/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.model.rlang.RLangElement;


@NonNullByDefault
public class RElementInput<TSource> {
	
	
	private final TSource source;
	
	private final @Nullable LtkModelElementFilter<? super RLangElement> envFilter;
	private final @Nullable LtkModelElementFilter<? super RLangElement> otherFilter;
	
	private final @Nullable Map<CombinedRElement, CombinedRElement[]> envFilteredChildren;
	
	
	private CombinedRElement @Nullable [] rootElements;
	
	
	public RElementInput(final TSource source,
			final @Nullable LtkModelElementFilter<? super RLangElement> envFilter,
			final @Nullable LtkModelElementFilter<? super RLangElement> otherFilter) {
		this.source= source;
		
		this.envFilter= envFilter;
		this.otherFilter= otherFilter;
		
		this.envFilteredChildren= (envFilter != null) ? new HashMap<>() : null;
	}
	
	
	public TSource getSource() {
		return this.source;
	}
	
	
	public boolean hasEnvFilter() {
		return (this.envFilter != null);
	}
	
	public @Nullable LtkModelElementFilter<? super RLangElement<?>> getEnvFilter() {
		return this.envFilter;
	}
	
	public CombinedRElement[] getEnvChildren(final CombinedRElement rElement) {
		CombinedRElement[] children= this.envFilteredChildren.get(rElement);
		if (children == null) {
			final List<? extends CombinedRElement> list= rElement.getModelChildren(this.envFilter);
			children= list.toArray(new @NonNull CombinedRElement[list.size()]);
			this.envFilteredChildren.put(rElement, children);
		}
		return children;
	}
	
	public List<CombinedRElement> filterEnvChildren(final List<? extends CombinedRElement> children) {
		final List<CombinedRElement> list= new ArrayList<>(children.size());
		for (final CombinedRElement rElement : children) {
			if (this.envFilter.include(rElement)) {
				list.add(rElement);
			}
		}
		return list;
	}
	
	
	public boolean hasOtherFilter() {
		return (this.otherFilter != null);
	}
	
	public @Nullable LtkModelElementFilter<? super RLangElement> getOtherFilter() {
		return this.otherFilter;
	}
	
	
	public void setRootElements(final CombinedRElement @Nullable [] elements) {
		this.rootElements= elements;
	}
	
	public CombinedRElement @Nullable [] getRootElements() {
		return this.rootElements;
	}
	
}
