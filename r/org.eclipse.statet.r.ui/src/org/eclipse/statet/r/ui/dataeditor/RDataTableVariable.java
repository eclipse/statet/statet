/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public interface RDataTableVariable {
	
	
	int LOGI= RStore.LOGICAL;
	int INT= RStore.INTEGER;
	int NUM= RStore.NUMERIC;
	int CPLX= RStore.COMPLEX;
	int CHAR= RStore.CHARACTER;
	int RAW= RStore.RAW;
	int FACTOR= RStore.FACTOR;
	int DATE= 0x11;
	//	int TIME= 0x12;
	int DATETIME= 0x13;
	
	
	int COLUMN= 1;
	int ROW= 2;
	
	
	@Nullable String getName();
	
	int getVarType();
	
	int getVarPresentation();
	
}
