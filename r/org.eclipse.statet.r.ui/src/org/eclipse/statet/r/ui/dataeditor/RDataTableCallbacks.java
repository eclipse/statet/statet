/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RDataTableCallbacks {
	
	
	/**
	 * Returns the part the editor belongs to
	 * 
	 * @return the part or <code>null</code>, if not in part
	 */
	@Nullable IWorkbenchPart getWorkbenchPart();
	
	/**
	 * Returns the service locator for the editor
	 * 
	 * @return service locator responsible for editor
	 */
	@Nullable IServiceLocator getServiceLocator();
	
	
	boolean isCloseSupported();
	void close();
	
	void show(IStatus status);
	
}
