/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.models.core.util.ElementPartition;

import org.eclipse.statet.ltk.ui.ElementNameProvider;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.rj.data.RObject;


@NonNullByDefault
public class RElementInputUtils {
	
	
	public static class DoubleClickListener implements IDoubleClickListener {
		
		
		private final AbstractTreeViewer viewer;
		
		
		public DoubleClickListener(final AbstractTreeViewer viewer) {
			this.viewer= viewer;
		}
		
		
		@Override
		public void doubleClick(final DoubleClickEvent event) {
			final IStructuredSelection selection= (IStructuredSelection) event.getSelection();
			if (selection.size() != 1) {
				return;
			}
			final Object element= selection.getFirstElement();
			if (element instanceof final RObject object) {
				switch (object.getRObjectType()) {
				case RObject.TYPE_ENVIRONMENT:
				case RObject.TYPE_LIST:
				case RObject.TYPE_DATAFRAME:
				case RObject.TYPE_S4OBJECT:
				case RObject.TYPE_REFERENCE:
					this.viewer.setExpandedState(element, !this.viewer.getExpandedState(element));
					break;
				default:
					break;
				}
			}
		}
		
	}
	
	
	public static void addDoubleClickExpansion(final AbstractTreeViewer viewer) {
		viewer.addDoubleClickListener(new DoubleClickListener(viewer));
	}
	
	public static @Nullable RElementName getRElementName(final TreePath treePath,
			final ITreeSelection selection) {
		if (selection instanceof ElementNameProvider) {
			return (RElementName) ((ElementNameProvider) selection).getElementName(treePath);
		}
		
		if (treePath.getSegmentCount() == 0) {
			return null;
		}
		final List<RElementName> names= new ArrayList<>(treePath.getSegmentCount() + 4);
		int segmentIdx= 0;
		while (segmentIdx < treePath.getSegmentCount()) {
			final Object segment= treePath.getSegment(segmentIdx++);
			if (segment instanceof ElementPartition) {
				continue;
			}
			final CombinedRElement rElement= RElementInputContentProvider.getCombinedRElement(segment);
			if (rElement == null) {
				return null;
			}
			RElementName.addSegments(names, rElement.getElementName());
		}
		return RElementName.create(names);
	}
	
	
	private RElementInputUtils() {}
	
}
