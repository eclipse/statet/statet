/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.rj.services.FQRObjectRef;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public interface RDataTableInput {
	
	
	interface StateListener {
		
		void tableUnavailable();
		
	}
	
	
	RElementName getElementName();
	
	String getFullName();
	
	String getName();
	
	FQRObjectRef<? extends RTool> getElementRef();
	Tool getTool();
	
	boolean isAvailable();
	
	void addStateListener(StateListener listener);
	void removeStateListener(StateListener listener);
	
}
