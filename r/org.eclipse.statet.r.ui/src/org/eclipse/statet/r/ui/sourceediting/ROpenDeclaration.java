/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.StyledString;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.actions.OpenDeclaration;
import org.eclipse.statet.r.ui.RLabelProvider;


public class ROpenDeclaration extends OpenDeclaration {
	
	
	public ROpenDeclaration() {
	}
	
	
	@Override
	public ILabelProvider createLabelProvider() {
		return new RLabelProvider() {
			@Override
			public StyledString getStyledText(final LtkModelElement element) {
				final StyledString styledText= super.getStyledText(element);
				if (element instanceof SourceElement) {
					final SourceUnit su= ((SourceElement)element).getSourceUnit();
					if (su instanceof WorkspaceSourceUnit) {
						styledText.append("  ∙  "); //$NON-NLS-1$
						styledText.append(((IResource)su.getResource()).getFullPath().toString(),
								StyledString.QUALIFIER_STYLER );
					}
				}
				return styledText;
			}
			
		};
	}
	
}