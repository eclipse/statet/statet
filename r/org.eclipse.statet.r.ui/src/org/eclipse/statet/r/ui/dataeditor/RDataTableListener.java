/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RDataTableListener {
	
	
	/**
	 * @param input the new input or {@code null} if no input
	 * @param contentDescription the description of the input or {@code null} if no input
	 * @param viewDescription the initial view
	 */
	void onInputChanged(@Nullable RDataTableInput input,
			@Nullable RDataTableContentDescription contentDescription,
			@Nullable DataViewDescription viewDescription);
	
	default void onDataViewChanged(final DataViewDescription viewDescription) {
	}
	
}
