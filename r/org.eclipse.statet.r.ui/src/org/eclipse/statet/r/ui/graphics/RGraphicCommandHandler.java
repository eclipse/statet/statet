/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.graphics;

import static org.eclipse.statet.jcommons.status.Status.OK_STATUS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.rj.ts.core.AbstractRToolCommandHandler;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class RGraphicCommandHandler extends AbstractRToolCommandHandler {
	
	
	public RGraphicCommandHandler() {
	}
	
	
	@Override
	public Status execute(final String id, final RToolService r, final ToolCommandData data,
			final ProgressMonitor m) {
		data.setReturnData("factory", RUIPlugin.getInstance().getCommonRGraphicFactory()); //$NON-NLS-1$
		
		return OK_STATUS;
	}
	
}
