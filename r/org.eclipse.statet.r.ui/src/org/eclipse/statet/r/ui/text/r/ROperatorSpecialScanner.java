/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.text.r;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.ui.presentation.AbstractRuleBasedScanner;
import org.eclipse.statet.ecommons.text.ui.presentation.TextStyleManager;

import org.eclipse.statet.r.core.rlang.RTokens;


/**
 * Scanner for special operators.
 */
@NonNullByDefault
public class ROperatorSpecialScanner extends AbstractRuleBasedScanner {
	
	
	private static class OpSpecialRule implements IRule {
		
		
		private final IToken defaultOpToken;
		private final IToken invalidOpToken;
		
		private final StringBuilder buffer;
		private final Map<String, IToken> specialIdentifiers;
		
		
		public OpSpecialRule(final IToken userDefinedOpToken, final IToken invalidOpToken) {
			this.defaultOpToken= userDefinedOpToken;
			this.invalidOpToken= invalidOpToken;
			
			this.buffer= new StringBuilder();
			this.specialIdentifiers= new HashMap<>();
		}
		
		public void addSpecialOperators(final ImList<String> identifiers, final IToken token) {
			for (final String identifier : identifiers) {
				this.specialIdentifiers.put(identifier, token);
			}
		}
		
		
		@Override
		public IToken evaluate(final ICharacterScanner scanner) {
			int c= scanner.read();
			if (c == '%') {
				while (true) {
					c= scanner.read();
					switch (c) {
					case '%':
						return succeed();
					case '\n':
					case '\r':
					case ICharacterScanner.EOF:
						scanner.unread();
						this.buffer.setLength(0);
						return this.invalidOpToken;
					default:
						this.buffer.append((char)c);
					}
				}
			}
			else {
				scanner.unread();
				return Token.UNDEFINED;
			}
		}
		
		private IToken succeed() {
			final IToken token= this.specialIdentifiers.get(this.buffer.toString());
			this.buffer.setLength(0);
			return (token != null) ? token : this.defaultOpToken;
		}
		
	}
	
	
	public ROperatorSpecialScanner(final TextStyleManager<?> textStyles) {
		super(textStyles);
		
		initRules();
	}
	
	
	@Override
	protected void createRules(final List<IRule> rules) {
		final IToken invalidOpToken= getToken(IRTextTokens.UNDEFINED_KEY);
		
		final OpSpecialRule rule= new OpSpecialRule(getToken(IRTextTokens.OP_SUB_USERDEFINED_KEY),
				invalidOpToken );
		rule.addSpecialOperators(RTokens.FLOWCONTROL_SPECIAL_QUALIFIERS, getToken(IRTextTokens.OP_SUB_FLOWCONTROL_KEY));
		rule.addSpecialOperators(RTokens.COMMON_SPECIAL_QUALIFIERS, getToken(IRTextTokens.OP_KEY));
		rules.add(rule);
	}
	
}
