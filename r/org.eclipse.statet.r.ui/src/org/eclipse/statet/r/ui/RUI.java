/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.internal.r.ui.RUIPlugin;


/**
 * 
 */
public class RUI {
	
	public static final String BUNDLE_ID= "org.eclipse.statet.r.ui"; //$NON-NLS-1$
	
	private static final String NS= "org.eclipse.statet.r";
	
	public static final String R_EDITOR_ID= "org.eclipse.statet.r.editors.R"; //$NON-NLS-1$
	public static final String RD_EDITOR_ID= "org.eclipse.statet.r.editors.Rd"; //$NON-NLS-1$
	
	public static final String RPKG_DESCRIPTION_EDITOR_ID= "org.eclipse.statet.r.editors.RPkgDescription"; //$NON-NLS-1$
	
	public static final String R_HELP_VIEW_ID = "org.eclipse.statet.r.views.RHelp"; //$NON-NLS-1$
	public static final String R_HELP_SEARCH_PAGE_ID = "org.eclipse.statet.r.searchPages.RHelpPage"; //$NON-NLS-1$
	
	public static final String SOURCE_EDITORS_PREF_PAGE_ID= "org.eclipse.statet.r.preferencePages.SourceEditors"; //$NON-NLS-1$
	
	
	public static final String IMG_OBJ_R_SCRIPT=                NS + "/images/obj/r_script"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_RUNTIME_ENV=           NS + "/images/obj/r_environment"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_REMOTE_ENV=            NS + "/images/obj/r_environment.remote"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_PACKAGE=               NS + "/images/obj/r_package"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_PACKAGE_NA=            NS + "/images/obj/r_package-notavail"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_METHOD=                  NS + "/images/obj/method"; //$NON-NLS-1$
	public static final String IMG_OBJ_COMMON_FUNCTION=         NS + "/images/obj/function.common"; //$NON-NLS-1$
	public static final String IMG_OBJ_COMMON_LOCAL_FUNCTION=   NS + "/images/obj/function.common.local"; //$NON-NLS-1$
	public static final String IMG_OBJ_GENERIC_FUNCTION=        NS + "/images/obj/function.generic"; //$NON-NLS-1$
	public static final String IMG_OBJ_GENERAL_VARIABLE=        NS + "/images/obj/variable.common"; //$NON-NLS-1$
	public static final String IMG_OBJ_GENERAL_LOCAL_VARIABLE=  NS + "/images/obj/variable.common.local"; //$NON-NLS-1$
	public static final String IMG_OBJ_SLOT=                    NS + "/images/obj/variable.slot"; //$NON-NLS-1$
	public static final String IMG_OBJ_PACKAGEENV=              NS + "/images/obj/packageenv"; //$NON-NLS-1$
	public static final String IMG_OBJ_GLOBALENV=               NS + "/images/obj/globalenv"; //$NON-NLS-1$
	public static final String IMG_OBJ_EMPTYENV=                NS + "/images/obj/emptyenv"; //$NON-NLS-1$
	public static final String IMG_OBJ_OTHERENV=                NS + "/images/obj/otherenv"; //$NON-NLS-1$
	public static final String IMG_OBJ_LIST=                    NS + "/images/obj/list"; //$NON-NLS-1$
	public static final String IMG_OBJ_DATAFRAME=               NS + "/images/obj/dataframe"; //$NON-NLS-1$
	public static final String IMG_OBJ_DATAFRAME_COLUMN=        NS + "/images/obj/datastore"; //$NON-NLS-1$
	public static final String IMG_OBJ_VECTOR=                  NS + "/images/obj/vector"; //$NON-NLS-1$
	public static final String IMG_OBJ_ARRAY=                   NS + "/images/obj/array"; //$NON-NLS-1$
	public static final String IMG_OBJ_S4OBJ=                   NS + "/images/obj/s4obj"; //$NON-NLS-1$
	public static final String IMG_OBJ_S4OBJ_VECTOR=            NS + "/images/obj/s4obj.vector"; //$NON-NLS-1$
	public static final String IMG_OBJ_S4OBJ_DATAFRAME_COLUMN=  NS + "/images/obj/s4obj.dataframe_col"; //$NON-NLS-1$
	public static final String IMG_OBJ_NULL=                    NS + "/images/obj/null"; //$NON-NLS-1$
	public static final String IMG_OBJ_MISSING=                 NS + "/images/obj/missing"; //$NON-NLS-1$
	public static final String IMG_OBJ_PROMISE=                 NS + "/images/obj/promise"; //$NON-NLS-1$
	public static final String IMG_OBJ_ARGUMENT_ASSIGN=         NS + "/images/obj/argument.assign"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_COL_LOGI=                NS + "/images/obj/col.logi"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_INT=                 NS + "/images/obj/col.int"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_NUM=                 NS + "/images/obj/col.num"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_CPLX=                NS + "/images/obj/col.cplx"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_CHAR=                NS + "/images/obj/col.char"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_RAW=                 NS + "/images/obj/col.raw"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_FACTOR=              NS + "/images/obj/col.factor"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_DATE=                NS + "/images/obj/col.date"; //$NON-NLS-1$
	public static final String IMG_OBJ_COL_DATETIME=            NS + "/images/obj/col.datetime"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_LIBRARY_GROUP=           NS + "/images/obj/library.group"; //$NON-NLS-1$
	public static final String IMG_OBJ_LIBRARY_LOCATION=        NS + "/images/obj/library.location"; //$NON-NLS-1$
	
	public static final String IMG_TOOL_ASSIST_TO_PIPE_FORWARD= NS + "/images/tool/assist-to_pipe_forward"; //$NON-NLS-1$
	
	public static final String IMG_LOCTOOL_SORT_PACKAGE=        NS + "/images/loctool/sort-package"; //$NON-NLS-1$
	
	
	public static ImageDescriptor getImageDescriptor(final String key) {
		return RUIPlugin.getInstance().getImageRegistry().getDescriptor(key);
	}
	
	public static Image getImage(final String key) {
		return RUIPlugin.getInstance().getImageRegistry().get(key);
	}
	
}
