/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.editors;

import static org.eclipse.statet.internal.r.ui.RUIPreferenceInitializer.R_EDITOR_NODE;

import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.AbstractPreferencesModelObject;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.EnumPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings;


@NonNullByDefault
public class REditorOptions extends AbstractPreferencesModelObject
		implements SmartInsertSettings {
	// Default values see RUIPreferenceInitializer
	
	public static final String GROUP_ID = "r/r.editor/options"; //$NON-NLS-1$
	
	
	public static final String SMARTINSERT_GROUP_ID = "r/r.editor/smartinsert"; //$NON-NLS-1$
	
	public static final BooleanPref SMARTINSERT_BYDEFAULT_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "smartinsert.as_default.enabled"); //$NON-NLS-1$
	
	public static final BooleanPref SMARTINSERT_ONPASTE_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "smartinsert.on_paste.enabled"); //$NON-NLS-1$
	
	public static final EnumPref<TabAction> SMARTINSERT_TAB_ACTION_PREF= new EnumPref<>(
			R_EDITOR_NODE, "SmartInsert.Tab.action", TabAction.class, //$NON-NLS-1$
			TabAction.INSERT_INDENT_LEVEL );
	
	public static final BooleanPref SMARTINSERT_CLOSECURLY_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "smartinsert.close_curlybrackets.enabled"); //$NON-NLS-1$
	public static final BooleanPref SMARTINSERT_CLOSEROUND_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "smartinsert.close_roundbrackets.enabled"); //$NON-NLS-1$
	public static final BooleanPref SMARTINSERT_CLOSESQUARE_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "smartinsert.close_squarebrackets.enabled"); //$NON-NLS-1$
	public static final BooleanPref SMARTINSERT_CLOSESPECIAL_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "smartinsert.close_specialpercent.enabled"); //$NON-NLS-1$
	public static final BooleanPref SMARTINSERT_CLOSESTRINGS_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "smartinsert.close_strings.enabled"); //$NON-NLS-1$
	
	
	public static final BooleanPref PREF_SPELLCHECKING_ENABLED = new BooleanPref(
			R_EDITOR_NODE, "spellcheck.enabled"); //$NON-NLS-1$
	
	// not in group
	public static final BooleanPref FOLDING_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "folding.enabled"); //$NON-NLS-1$
	
	public static final String FOLDING_SHARED_GROUP_ID = "r/r.editor/folding.shared"; //$NON-NLS-1$
	
	public static final BooleanPref FOLDING_RESTORE_STATE_ENABLED_PREF = new BooleanPref(
			R_EDITOR_NODE, "Folding.RestoreState.enabled"); //$NON-NLS-1$
	
	public static final BooleanPref PREF_MARKOCCURRENCES_ENABLED = new BooleanPref(
			R_EDITOR_NODE, "markoccurrences.enabled"); //$NON-NLS-1$
	
	
	private boolean fIsSmartByDefaultEnabled;
	private TabAction fSmartTabAction;
	private boolean fIsSmartCurlyBracketsEnabled;
	private boolean fIsSmartRoundBracketsEnabled;
	private boolean fIsSmartSquareBracketsEnabled;
	private boolean fIsSmartSpecialPercentEnabled;
	private boolean fIsSmartStringsEnabled;
	private boolean fIsSmartPasteEnabled;
	
	
	public REditorOptions(final int mode) {
		if (mode >= 1) {
			installLock();
		}
		loadDefaults();
	}
	
	@Override
	public String[] getNodeQualifiers() {
		return new String[0];
	}
	
	@Override
	public void loadDefaults() {
		fIsSmartByDefaultEnabled = true;
		fSmartTabAction = SMARTINSERT_TAB_ACTION_PREF.store2Usage(null);
		fIsSmartCurlyBracketsEnabled = true;
		fIsSmartRoundBracketsEnabled = true;
		fIsSmartSquareBracketsEnabled = true;
		fIsSmartSpecialPercentEnabled = true;
		fIsSmartStringsEnabled = true;
		fIsSmartPasteEnabled = true;
	}
	
	@Override
	public void load(final PreferenceAccess prefs) {
		fIsSmartByDefaultEnabled = prefs.getPreferenceValue(SMARTINSERT_BYDEFAULT_ENABLED_PREF);
		fSmartTabAction = prefs.getPreferenceValue(SMARTINSERT_TAB_ACTION_PREF);
		fIsSmartCurlyBracketsEnabled = prefs.getPreferenceValue(SMARTINSERT_CLOSECURLY_ENABLED_PREF);
		fIsSmartRoundBracketsEnabled = prefs.getPreferenceValue(SMARTINSERT_CLOSEROUND_ENABLED_PREF);
		fIsSmartSquareBracketsEnabled = prefs.getPreferenceValue(SMARTINSERT_CLOSESQUARE_ENABLED_PREF);
		fIsSmartSpecialPercentEnabled = prefs.getPreferenceValue(SMARTINSERT_CLOSESPECIAL_ENABLED_PREF);
		fIsSmartStringsEnabled = prefs.getPreferenceValue(SMARTINSERT_CLOSESTRINGS_ENABLED_PREF);
		fIsSmartPasteEnabled = prefs.getPreferenceValue(SMARTINSERT_ONPASTE_ENABLED_PREF);
	}
	
	@Override
	public Map<Preference<?>, Object> deliverToPreferencesMap(final Map<Preference<?>, Object> map) {
		map.put(SMARTINSERT_BYDEFAULT_ENABLED_PREF, fIsSmartByDefaultEnabled);
		map.put(SMARTINSERT_TAB_ACTION_PREF, fSmartTabAction);
		map.put(SMARTINSERT_CLOSECURLY_ENABLED_PREF, fIsSmartCurlyBracketsEnabled);
		map.put(SMARTINSERT_CLOSEROUND_ENABLED_PREF, fIsSmartRoundBracketsEnabled);
		map.put(SMARTINSERT_CLOSESQUARE_ENABLED_PREF, fIsSmartSquareBracketsEnabled);
		map.put(SMARTINSERT_CLOSESPECIAL_ENABLED_PREF, fIsSmartSpecialPercentEnabled);
		map.put(SMARTINSERT_CLOSESTRINGS_ENABLED_PREF, fIsSmartStringsEnabled);
		map.put(SMARTINSERT_ONPASTE_ENABLED_PREF, fIsSmartPasteEnabled);
		return map;
	}
	
	
	@Override
	public boolean isSmartInsertEnabledByDefault() {
		return fIsSmartByDefaultEnabled;
	}
	
	@Override
	public TabAction getSmartInsertTabAction() {
		return fSmartTabAction;
	}
	
	public boolean isSmartPasteEnabled() {
		return fIsSmartPasteEnabled;
	}
	public boolean isSmartCurlyBracketsEnabled() {
		return fIsSmartCurlyBracketsEnabled;
	}
	public boolean isSmartRoundBracketsEnabled() {
		return fIsSmartRoundBracketsEnabled;
	}
	public boolean isSmartSquareBracketsEnabled() {
		return fIsSmartSquareBracketsEnabled;
	}
	public boolean isSmartSpecialPercentEnabled() {
		return fIsSmartSpecialPercentEnabled;
	}
	public boolean isSmartStringsEnabled() {
		return fIsSmartStringsEnabled;
	}
	
}
