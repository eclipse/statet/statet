/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.r.core.model.RElementName;


@NonNullByDefault
public class RPkgCompletionComputer extends RElementCompletionComputer {
	
	
	public RPkgCompletionComputer() {
	}
	
	
	@Override
	protected void computeCompletionProposals(final int mode,
			final IProgressMonitor monitor) {
		final RElementName prefixName= getRContext().getIdentifierElementName();
		if (prefixName == null || !isPackageName(prefixName)) {
			return;
		}
		
		addPkgNameProposals(prefixName, ARG_TYPE_NO_PRIO);
	}
	
	@Override
	public void computeInformationProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
	}
	
}
