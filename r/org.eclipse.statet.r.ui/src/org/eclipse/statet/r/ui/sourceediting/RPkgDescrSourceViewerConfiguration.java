/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_CONTENT_TYPES;
import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_DEFAULT_CONTENT_TYPE;
import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_FIELD_NAME_CONTENT_TYPE;

import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.information.IInformationProvider;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.source.ISourceViewer;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.text.IIndentSettings;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.presentation.SingleTokenScanner;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.dsl.dcf.ui.util.FieldNameTextStyleScanner;
import org.eclipse.statet.dsl.ui.DslUI;
import org.eclipse.statet.dsl.ui.text.DslTextStyles;
import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.internal.r.ui.editors.rpkg.RPkgDescrAutoEditStrategy;
import org.eclipse.statet.internal.r.ui.editors.rpkg.RPkgDescrBracketPairMatcher;
import org.eclipse.statet.internal.r.ui.editors.rpkg.RPkgDescrContentAssistProcessor;
import org.eclipse.statet.internal.r.ui.editors.rpkg.RPkgDescrQuickOutlineInformationProvider;
import org.eclipse.statet.ltk.text.core.CharPairMatcher;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.EcoReconciler2;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorAddon;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceUnitReconcilingStrategy;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistProcessor;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RPkgDescrDocumentContentInfo;
import org.eclipse.statet.r.ui.editors.REditorOptions;


@NonNullByDefault
public class RPkgDescrSourceViewerConfiguration extends SourceEditorViewerConfiguration {
	
	
	private static final @NonNull String[] CONTENT_TYPES= RPKG_DESCR_CONTENT_TYPES.toArray(String[]::new);
	
	
	private RCoreAccess rCoreAccess;
	
	private @Nullable RPkgDescrAutoEditStrategy descrAutoEditStrategy;
	private @Nullable RAutoEditStrategy rAutoEditStrategy;
	
	
	public RPkgDescrSourceViewerConfiguration(final DocContentSections documentContentInfo, final int flags,
			final SourceEditor editor,
			final @Nullable RCoreAccess access,
			final @Nullable IPreferenceStore preferenceStore,
			final @Nullable PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		super(documentContentInfo, flags, editor);
		setCoreAccess(access);
		
		setup((preferenceStore != null) ? preferenceStore : RUIPlugin.getInstance().getEditorPreferenceStore(),
				LtkUIPreferences.getEditorDecorationPreferences(),
				LtkUIPreferences.getAssistPreferences() );
		if (textStyles != null) {
			setTextStyles(textStyles);
		}
	}
	
	public RPkgDescrSourceViewerConfiguration(final int flags) {
		this(RPkgDescrDocumentContentInfo.INSTANCE, flags, null, null, null, null);
	}
	
	protected void setCoreAccess(final @Nullable RCoreAccess access) {
		this.rCoreAccess= (access != null) ? access : RCore.getWorkbenchAccess();
	}
	
	
	@Override
	protected void initTextStyles() {
		setTextStyles(DslUI.getTextStyleManager());
	}
	
	@Override
	protected void initScanners() {
		final var textStyles= getTextStyles();
		
		addScanner(RPKG_DESCR_DEFAULT_CONTENT_TYPE,
				new SingleTokenScanner(textStyles, DslTextStyles.TS_DEFAULT) );
		addScanner(RPKG_DESCR_FIELD_NAME_CONTENT_TYPE,
				new FieldNameTextStyleScanner(textStyles) );
	}
	
	@Override
	protected ITokenScanner getScanner(String contentType) {
		if (contentType != RPKG_DESCR_FIELD_NAME_CONTENT_TYPE) {
			contentType= RPKG_DESCR_DEFAULT_CONTENT_TYPE;
		}
		return super.getScanner(contentType);
	}
	
	
	@Override
	public List<SourceEditorAddon> getAddOns() {
		final List<SourceEditorAddon> addons= super.getAddOns();
		if (this.descrAutoEditStrategy != null) {
			addons.add(this.descrAutoEditStrategy);
		}
		if (this.rAutoEditStrategy != null) {
			addons.add(this.rAutoEditStrategy);
		}
		return addons;
	}
	
	
	@Override
	public @NonNull String[] getConfiguredContentTypes(final ISourceViewer sourceViewer) {
		return CONTENT_TYPES;
	}
	
	
	@Override
	public CharPairMatcher createPairMatcher() {
		return new RPkgDescrBracketPairMatcher();
	}
	
	
	@Override
	protected IIndentSettings getIndentSettings() {
		return this.rCoreAccess.getRCodeStyle();
	}
	
	
	@Override
	public boolean isSmartInsertSupported() {
		return true;
	}
	
	@Override
	public boolean isSmartInsertByDefault() {
		return EPreferences.getInstancePrefs()
				.getPreferenceValue(REditorOptions.SMARTINSERT_BYDEFAULT_ENABLED_PREF);
	}
	
	
	@Override
	public @NonNull IAutoEditStrategy[] getAutoEditStrategies(final ISourceViewer sourceViewer, final String contentType) {
		if (getSourceEditor() == null) {
			return super.getAutoEditStrategies(sourceViewer, contentType);
		}
		switch (getDocumentContentInfo().getTypeByPartition(contentType)) {
		case RPkgDescrDocumentContentInfo.R: {
				var autoEditStrategy= this.rAutoEditStrategy;
				if (autoEditStrategy == null) {
					autoEditStrategy= new RAutoEditStrategy(this.rCoreAccess, getSourceEditor());
					this.rAutoEditStrategy= autoEditStrategy;
				}
				return new @NonNull IAutoEditStrategy[] { autoEditStrategy };
			}
		default: {
				var autoEditStrategy= this.descrAutoEditStrategy;
				if (autoEditStrategy == null) {
					autoEditStrategy= new RPkgDescrAutoEditStrategy(this.rCoreAccess, getSourceEditor());
					this.descrAutoEditStrategy= autoEditStrategy;
				}
				return new @NonNull IAutoEditStrategy[] { autoEditStrategy };
			}
		}
	}
	
	
	@Override
	public @Nullable IReconciler getReconciler(final ISourceViewer sourceViewer) {
		final SourceEditor editor= getSourceEditor();
		if (!(editor instanceof SourceEditor1)) {
			return null;
		}
		final EcoReconciler2 reconciler= new EcoReconciler2(editor);
		reconciler.setDelay(500);
		reconciler.addReconcilingStrategy(new SourceUnitReconcilingStrategy());
		
//		final IReconcilingStrategy spellingStrategy= getSpellingStrategy(sourceViewer);
//		if (spellingStrategy != null) {
//			reconciler.addReconcilingStrategy(spellingStrategy);
//		}
		
		return reconciler;
	}
	
	
	@Override
	public void initContentAssist(final ContentAssist assistant) {
		final ContentAssistComputerRegistry registry= RUIPlugin.getInstance().getRPkgDescrContentAssistRegistry();
		
		{	final ContentAssistProcessor processor= new RPkgDescrContentAssistProcessor(assistant,
					RPKG_DESCR_DEFAULT_CONTENT_TYPE, registry, getSourceEditor());
			assistant.setContentAssistProcessor(processor, RPKG_DESCR_DEFAULT_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new RPkgDescrContentAssistProcessor(assistant,
					RPKG_DESCR_FIELD_NAME_CONTENT_TYPE, registry, getSourceEditor());
			assistant.setContentAssistProcessor(processor, RPKG_DESCR_FIELD_NAME_CONTENT_TYPE);
		}
		
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_DEFAULT_CONTENT_TYPE, registry, getSourceEditor() );
			processor.setCompletionProposalAutoActivationCharacters(new char[] { '$', '@' });
			processor.setContextInformationAutoActivationCharacters(new char[] { ',' });
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_DEFAULT_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE );
		}
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_STRING_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_STRING_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_COMMENT_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_COMMENT_CONTENT_TYPE);
		}
	}
	
	@Override
	protected void collectHyperlinkDetectorTargets(final Map<String, IAdaptable> targets,
			final ISourceViewer sourceViewer) {
//		targets.put("org.eclipse.statet.yaml.editorHyperlinks.YamlEditorTarget", getSourceEditor()); //$NON-NLS-1$
	}
	
	
	@Override
	protected @Nullable IInformationProvider getQuickInformationProvider(final ISourceViewer sourceViewer,
			final int operation) {
		final SourceEditor editor= getSourceEditor();
		if (editor == null) {
			return null;
		}
		return switch (operation) {
		case SourceEditorViewer.SHOW_SOURCE_OUTLINE ->
				new RPkgDescrQuickOutlineInformationProvider(editor, operation);
		default ->
				null;
		};
	}
	
}
