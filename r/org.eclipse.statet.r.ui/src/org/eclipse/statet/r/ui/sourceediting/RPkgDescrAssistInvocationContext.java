/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_DEFAULT_CONTENT_TYPE;
import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RPKG_DESCR_FIELD_NAME_CONTENT_TYPE;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextUtilities;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.NodeType;
import org.eclipse.statet.dsl.dcf.core.source.ast.Field;
import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceUnitModelInfo;
import org.eclipse.statet.r.core.source.util.RPkgDescrHeuristicTokenScanner;


/**
 * AssistInvocationContext for R package description
 */
@NonNullByDefault
public class RPkgDescrAssistInvocationContext extends AssistInvocationContext {
	
	
	private static String checkProposalContentType(final String contentType,
			final SourceEditor editor, final int offset) {
		try {
			if (contentType == RPKG_DESCR_DEFAULT_CONTENT_TYPE
					&& editor.getViewer().getDocument().getLineInformationOfOffset(offset).getOffset() == offset) {
				return RPKG_DESCR_FIELD_NAME_CONTENT_TYPE;
			}
		}
		catch (final BadLocationException e) {
			RUIPlugin.logUncriticalError(e);
		}
		return contentType;
	}
	
	
	private @Nullable RPkgDescrHeuristicTokenScanner scanner;
	
	
	public RPkgDescrAssistInvocationContext(final SourceEditor editor,
			final int offset, final String contentType,
			final boolean isProposal,
			final IProgressMonitor monitor) {
		super(editor, offset,
				(isProposal) ? checkProposalContentType(contentType, editor, offset) : contentType,
				(isProposal) ? ModelManager.MODEL_FILE : ModelManager.NONE,
				monitor );
	}
	
	public RPkgDescrAssistInvocationContext(final AssistInvocationContext base,
			final boolean isProposal,
			final IProgressMonitor monitor) {
		super(base,
				(isProposal) ? ModelManager.MODEL_FILE : ModelManager.NONE,
				monitor );
	}
	
	
	@Override
	protected String getModelTypeId() {
		return RModel.RPKG_DESCRIPTION_TYPE_ID;
	}
	
	public RCoreAccess getRCoreAccess() {
		return RCore.getContextAccess(getSourceUnit());
	}
	
	
	@Override
	public int getTabWidth() {
		return getRCoreAccess().getRCodeStyle().getTabWidth();
	}
	
	
	public final RPkgDescrHeuristicTokenScanner getHeuristicTokenScanner() {
		var scanner= this.scanner;
		if (scanner == null) {
			scanner= new RPkgDescrHeuristicTokenScanner(getEditor().getDocumentContentInfo());
			this.scanner= scanner;
		}
		return scanner;
	}
	
	
	@Override
	public @Nullable RPkgDescrSourceUnitModelInfo getModelInfo() {
		return (RPkgDescrSourceUnitModelInfo)super.getModelInfo();
	}
	
	public @Nullable DslAstNode getInvocationDescrAstNode() {
		AstNode node= getInvocationAstSelection().getCovering();
		if (node == null) {
			node= getAstInfo().getRoot();
		}
		while (node != null) {
			if (node instanceof DslAstNode) {
				return (DslAstNode)node;
			}
			node= node.getParent();
		}
		return null;
	}
	
	public @Nullable Field getInvocationDescrFieldNode() {
		DslAstNode node= getInvocationDescrAstNode();
		while (node != null && node.getNodeType() != NodeType.KEY_VALUE_ENTRY) {
			node= node.getDslParent();
		}
		return (Field)node;
	}
	
	public @Nullable DslAstNode getSelectionDescrAstNode() {
		final AstNode node= getAstSelection().getCovering();
		return (node instanceof DslAstNode) ? (DslAstNode)node : null;
	}
	
	
	@Override
	protected @Nullable String computeIdentifierPrefix(final int offset)
			throws BadPartitioningException, BadLocationException {
		final IDocument document= getDocument();
		
		int startOffset;
		if (getInvocationContentType() == RPKG_DESCR_FIELD_NAME_CONTENT_TYPE) {
			final ITypedRegion partition= TextUtilities.getPartition(document,
					getEditor().getDocumentContentInfo().getPartitioning(), offset, true );
			if (partition.getType() == RPKG_DESCR_FIELD_NAME_CONTENT_TYPE) {
				startOffset= partition.getOffset();
				return document.get(startOffset, offset - startOffset);
			}
			return ""; //$NON-NLS-1$
		}
		
		return super.computeIdentifierPrefix(offset);
	}
	
}
