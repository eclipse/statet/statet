/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.editors.templates;

import org.eclipse.text.templates.ContextTypeRegistry;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.ui.RUIMessages;
import org.eclipse.statet.ltk.ui.templates.SourceEditorContextType;
import org.eclipse.statet.ltk.ui.templates.SourceUnitVariableResolver;


/**
 * Definition of context types in R code editors. The "editor contexts"
 * are usually used in the template content assistant
 * (in contrast to {@link org.eclipse.statet.r.codegeneration.RCodeTemplateContextType}).
 */
@NonNullByDefault
public class REditorTemplateContextType extends SourceEditorContextType {
	
	
//	/**
//	 * Resolver for Array-Variables.
//	 */
//	protected static class VectorVar extends TemplateVariableResolver {
//
//		public VectorVar() {
//			super("vector", Messages.TemplateVariable_Vector_description);  //$NON-NLS-1$
//		}
//		
//		protected String resolve(TemplateContext context) {
//			return "vector";
//		}
//	}
//
//	/**
//	 * Resolver for a Variables.
//	 */
//	protected static class XVar extends TemplateVariableResolver {
//
//		public XVar() {
//			super("variable", Messages.TemplateVariable_Variable_description);  
//		}
//		
//		protected String resolve(TemplateContext context) {
//			return "x";
//		}
//	}
//
//	/**
//	 * Resolver for Idx-Variables.
//	 */
//	protected static class IndexVar extends TemplateVariableResolver {
//
//		public IndexVar() {
//			super("index", Messages.TemplateVariable_Index_description);  
//		}
//		
//		protected String resolve(TemplateContext context) {
//			return "i";
//		}
//	}
//
//	/**
//	 * Resolver for function parameter.
//	 */
//	protected static class Parameter extends TemplateVariableResolver {
//
//		public Parameter() {
//			super("parameter", Messages.TemplateVariable_Parameter_description);  
//		}
//		
//		protected String resolve(TemplateContext context) {
//			return "par";
//		}
//	}
	
	
/* context types **************************************************************/
	
	/**
	 * Common context for R source code.
	 */
	public static final String RCODE_CONTEXTTYPE_ID= "r-code"; //$NON-NLS-1$
	
	/**
	 * Context for Roxygen documentation comments.
	 */
	public static final String ROXYGEN_CONTEXTTYPE_ID= "roxygen"; //$NON-NLS-1$
	
	
	public static void registerContextTypes(final ContextTypeRegistry registry) {
		registry.addContextType(new REditorTemplateContextType(RCODE_CONTEXTTYPE_ID,
				RUIMessages.EditorTemplates_RCodeContext_label ));
		registry.addContextType(new REditorTemplateContextType(ROXYGEN_CONTEXTTYPE_ID,
				RUIMessages.EditorTemplates_RoxygenContext_label ));
	}
	
	
	REditorTemplateContextType(final String id, final String name) {
		super(id, name);
		
		addCommonVariables();
		addEditorVariables();
		
		addResolver(new SourceUnitVariableResolver.FileName());
		
//		if (RCODE_CONTEXTTYPE_ID.equals(id)) {
//			addResolver(new VectorVar());
//			addResolver(new XVar());
//			addResolver(new IndexVar());
//			addResolver(new Parameter());
//		}
	}
	
}
