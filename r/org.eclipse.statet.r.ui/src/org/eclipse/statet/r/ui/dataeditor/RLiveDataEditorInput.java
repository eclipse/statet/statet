/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.dataeditor;

import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IPersistableElement;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.services.FQRObjectRef;
import org.eclipse.statet.rj.ts.core.RTool;


/**
 * Editor input of a R data elements in an R process.
 */
@NonNullByDefault
public class RLiveDataEditorInput extends PlatformObject implements RDataEditorInput {
	
	
	public static boolean isSupported(final RObject element) {
		switch (element.getRObjectType()) {
		case RObject.TYPE_VECTOR:
		case RObject.TYPE_DATAFRAME:
			return true;
		case RObject.TYPE_ARRAY:
			return (((RArray<?>)element).getDim().getLength() == 2);
		default:
			return false;
		}
	}
	
	
	private final RToolDataTableInput input;
	
	
	public RLiveDataEditorInput(final RElementName elementName, final FQRObjectRef<? extends RTool> elementRef) {
		this.input= new RToolDataTableInput(elementName, elementRef);
	}
	
	
	@Override
	public @Nullable ImageDescriptor getImageDescriptor() {
		return null;
	}
	
	@Override
	public String getName() {
		return this.input.getName();
	}
	
	@Override
	public String getToolTipText() {
		return NLS.bind("{0} in {1}",
				this.input.getElementName().getDisplayName(),
				this.input.getTool().getLabel(Tool.LONG_LABEL));
	}
	
	@Override
	public boolean exists() {
		return this.input.isAvailable();
	}
	
	@Override
	public @Nullable IPersistableElement getPersistable() {
		return null;
	}
	
	@Override
	public RToolDataTableInput getRDataTableInput() {
		return this.input;
	}
	
	
	@Override
	public int hashCode() {
		return this.input.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RLiveDataEditorInput) {
			return (this.input.equals(((RLiveDataEditorInput)obj).input));
		}
		return false;
	}
	
}
