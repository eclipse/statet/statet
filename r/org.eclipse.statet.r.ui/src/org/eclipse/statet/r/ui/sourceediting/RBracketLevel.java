/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.link.LinkedModeModel;
import org.eclipse.jface.text.link.LinkedPosition;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.TextUtil;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.assist.LinkedModeBracketLevel;


@NonNullByDefault
public final class RBracketLevel extends LinkedModeBracketLevel {
	
	
	public static final class CurlyBracketPosition extends InBracketPosition {
		
		public CurlyBracketPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '{';
		}
		
		@Override
		public char getCloseChar() {
			return '}';
		}
		
	}
	
	public static final class RoundBracketPosition extends InBracketPosition {
		
		public RoundBracketPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '(';
		}
		
		@Override
		public char getCloseChar() {
			return ')';
		}
		
	}
	
	public static final class SquareBracketPosition extends InBracketPosition {
		
		public SquareBracketPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '[';
		}
		
		@Override
		public char getCloseChar() {
			return ']';
		}
		
	}
	
	
	public final static class StringDPosition extends InBracketPosition {
		
		public StringDPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '"';
		}
		
		@Override
		public char getCloseChar() {
			return '"';
		}
		
		@Override
		public boolean insertCR(final int charOffset) {
			return false;
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			return (TextUtil.countBackward(getDocument(), offset, '\\') % 2 == 1);
		}
		
	}
	
	public final static class StringSPosition extends InBracketPosition {
		
		public StringSPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '\'';
		}
		
		@Override
		public char getCloseChar() {
			return '\'';
		}
		
		@Override
		public boolean insertCR(final int charOffset) {
			return false;
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			return (TextUtil.countBackward(getDocument(), offset, '\\') % 2 == 1);
		}
		
	}
	
	protected static abstract class StringRPosition extends InBracketPosition {
		
		private final int closeLength;
		
		public StringRPosition(final int nDashes,
				final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
			this.closeLength= 2 + nDashes;
		}
		
		@Override
		protected int getCloseLength() {
			return this.closeLength;
		}
		
	}
	
	public final static class StringRRoundPosition extends StringRPosition {
		
		public StringRRoundPosition(final int nDashes,
				final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(nDashes, doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '(';
		}
		
		@Override
		public char getCloseChar() {
			return ')';
		}
		
	}
	
	public final static class StringRSquarePosition extends StringRPosition {
		
		public StringRSquarePosition(final int nDashes,
				final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(nDashes, doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '[';
		}
		
		@Override
		public char getCloseChar() {
			return ']';
		}
		
	}
	
	public final static class StringRCurlyPosition extends StringRPosition {
		
		public StringRCurlyPosition(final int nDashes,
				final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(nDashes, doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '{';
		}
		
		@Override
		public char getCloseChar() {
			return '}';
		}
		
	}
	
	public final static class SymbolGPosition extends InBracketPosition {
		
		public SymbolGPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '`';
		}
		
		@Override
		public char getCloseChar() {
			return '`';
		}
		
		@Override
		public boolean insertCR(final int charOffset) {
			return false;
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			return (TextUtil.countBackward(getDocument(), offset, '\\') % 2 == 1);
		}
		
	}
	
	public final static class OpSpecialPosition extends InBracketPosition {
		
		public OpSpecialPosition(final IDocument doc, final int offset, final int length,
				final int sequence) {
			super(doc, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '%';
		}
		
		@Override
		public char getCloseChar() {
			return '%';
		}
		
		@Override
		public boolean insertCR(final int charOffset) {
			return false;
		}
		
		@Override
		public boolean matchesClose(final LinkedModeBracketLevel level, final int offset, final char character) {
			return (getOffset() + getLength() == offset && getCloseChar() == character);
		}
		
	}
	
	
	public RBracketLevel(final LinkedModeModel model,
			final IDocument document, final DocContentSections documentContentInfo,
			final InBracketPosition position,
			final boolean consoleMode, final boolean autoDelete) {
		this(model, document, documentContentInfo,
				ImCollections.<LinkedPosition>newList(position),
				((consoleMode) ? CONSOLE_MODE : 0) | ((autoDelete) ? AUTODELETE : 0));
	}
	
	public RBracketLevel(final LinkedModeModel model,
			final IDocument document, final DocContentSections documentContentInfo,
			final List<? extends LinkedPosition> positions, final int mode) {
		super(model, document, documentContentInfo, positions, mode);
	}
	
}
