/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.ROUND_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import static org.eclipse.statet.ecommons.text.core.TextTokenScanner.NO_CHAR;
import static org.eclipse.statet.ecommons.text.ui.assist.LinkedModeBracketLevel.AUTODELETE;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.DefaultIndentLineAutoEditStrategy;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentCommand;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.text.link.LinkedModeModel;
import org.eclipse.jface.text.link.LinkedModeUI;
import org.eclipse.jface.text.link.LinkedModeUI.IExitPolicy;
import org.eclipse.jface.text.link.LinkedPosition;
import org.eclipse.jface.text.link.LinkedPositionGroup;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.VerifyKeyListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.ITextEditorExtension3;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CharPair;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.CharPairSet;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.ecommons.text.IndentUtil;
import org.eclipse.statet.ecommons.text.TextUtil;
import org.eclipse.statet.ecommons.text.core.TextTokenScanner;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartition;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionUtils;
import org.eclipse.statet.ecommons.text.ui.assist.LinkedModeAutodeleteLevel;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorAddon;
import org.eclipse.statet.nico.ui.console.InputSourceViewer;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.ast.RParser;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RPartitionNodeType;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;
import org.eclipse.statet.r.core.source.util.RSourceIndenter;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.editors.REditorOptions;


/**
 * Auto edit strategy for R code:
 *  - auto indent on keys
 *  - special indent with tab key
 *  - auto indent on paste
 *  - auto close of pairs
 */
@NonNullByDefault
public class RAutoEditStrategy extends DefaultIndentLineAutoEditStrategy
		implements SourceEditorAddon {
	
	
	private static final StringParserInput DISPLAY_PARSER_INPUT= new StringParserInput();
	
	private static final StringParserInput getParserInput(final String text) {
		return (Display.getCurrent() == Display.getDefault()) ?
				DISPLAY_PARSER_INPUT.reset(text) : new StringParserInput(text);
	}
	
	private static final RParser DISPLAY_R_PARSER= new RParser(AstInfo.LEVEL_MINIMAL);
	
	private static final RParser getRParser() {
		return (Display.getCurrent() == Display.getDefault()) ?
				DISPLAY_R_PARSER : new RParser(AstInfo.LEVEL_MINIMAL);
	}
	
	
	private class RealTypeListener implements VerifyKeyListener {
		@Override
		public void verifyKey(final VerifyEvent event) {
			if (!event.doit) {
				return;
			}
			switch (event.character) {
			case '{':
			case '}':
			case '(':
			case ')':
			case '[':
			case '%':
			case '"':
			case '\'':
			case '`':
			case '#':
				event.doit= !customizeKeyPressed(event.character);
				return;
			case '\t':
				if (event.stateMask == 0) {
					event.doit= !customizeKeyPressed(event.character);
				}
				return;
			case 0x0A:
			case 0x0D:
				if (RAutoEditStrategy.this.editor3 != null) {
					event.doit= !customizeKeyPressed('\n');
				}
				return;
			default:
				return;
			}
		}
	}
	
	
	private final SourceEditor editor;
	private final @Nullable ITextEditorExtension3 editor3;
	private final DocContentSections documentContentInfo;
	private final SourceViewer viewer;
	private final RealTypeListener typeListener;
	
	private final RCoreAccess rCoreAccess;
	private final REditorOptions editorOptions;
	
	private AbstractDocument document;
	private TextRegion validRange;
	private RHeuristicTokenScanner scanner;
	private RCodeStyleSettings rCodeStyle;
	private @Nullable RSourceIndenter indenter;
	
	private boolean ignoreCommands= false;
	
	
	@SuppressWarnings("null")
	public RAutoEditStrategy(final RCoreAccess rCoreAccess, final SourceEditor editor) {
		assert (rCoreAccess != null);
		assert (editor != null);
		
		this.editor= editor;
		this.documentContentInfo= editor.getDocumentContentInfo();
		
		this.rCoreAccess= rCoreAccess;
		this.editorOptions= RUIPlugin.getInstance().getREditorSettings(rCoreAccess.getPrefs());
		assert (this.editorOptions != null);
		
		this.viewer= this.editor.getViewer();
		this.editor3= (editor instanceof SourceEditor1) ? (SourceEditor1) editor : null;
		this.typeListener= new RealTypeListener();
	}
	
	@Override
	public void install(final SourceEditor editor) {
		assert (editor.getViewer() == this.viewer);
		this.viewer.prependVerifyKeyListener(this.typeListener);
	}
	
	@Override
	public void uninstall() {
		this.viewer.removeVerifyKeyListener(this.typeListener);
	}
	
	
	private final @Nullable ITypedRegion initCustomization(final int offset, final int c)
			throws BadLocationException, BadPartitioningException {
		assert(this.document != null);
		if (this.scanner == null) {
			this.scanner= createScanner();
		}
		this.rCodeStyle= this.rCoreAccess.getRCodeStyle();
		
		final ITypedRegion partition= this.document.getPartition(
				this.scanner.getDocumentPartitioning(), offset, true );
		// InputDocument of console does not (yet) return a TreePartition
		this.validRange= (partition instanceof TreePartition) ?
				getValidRange(offset, (TreePartition) partition, c) :
				new BasicTextRegion(0, this.document.getLength());
		return (this.validRange != null) ? partition : null;
	}
	
	protected RHeuristicTokenScanner createScanner() {
		return RHeuristicTokenScanner.create(this.documentContentInfo);
	}
	
	protected @Nullable TextRegion getValidRange(final int offset, final TreePartition partition, final int c) {
		if (this.documentContentInfo.getPrimaryType() == RDocumentConstants.R_PARTITIONING) {
			return new BasicTextRegion(0, this.document.getLength());
		}
		return TreePartitionUtils.searchNodeUp(partition.getTreeNode(), 
				RPartitionNodeType.DEFAULT_ROOT );
	}
	
	protected final IDocument getDocument() {
		return this.document;
	}
	
	protected final DocContentSections getDocumentContentInfo() {
		return this.documentContentInfo;
	}
	
	protected RSourceIndenter getSourceIndenter() {
		var indenter= this.indenter;
		if (indenter == null) {
			indenter= new RSourceIndenter(this.scanner);
			this.indenter= indenter;
		}
		indenter.setup(this.rCoreAccess);
		return indenter;
	}
	
	@SuppressWarnings("null")
	private final void quitCustomization() {
		this.document= null;
		this.rCodeStyle= null;
	}
	
	
	private final boolean isSmartInsertEnabled() {
		return ((this.editor3 != null) ?
				(this.editor3.getInsertMode() == ITextEditorExtension3.SMART_INSERT) :
				this.editorOptions.isSmartInsertEnabledByDefault() );
	}
	
	private final boolean isBlockSelection() {
		final StyledText textWidget= this.viewer.getTextWidget();
		return (textWidget.getBlockSelection() && textWidget.getSelectionRanges().length > 2);
	}
	
	private final boolean isClosedBracket(final int backwardOffset, final int forwardOffset,
			final CharPair searchType)
			throws BadLocationException {
		final CharPairSet brackets= RHeuristicTokenScanner.R_BRACKETS;
		final int searchPairIndex= brackets.getPairIndex(searchType);
		int[] balance= new int[brackets.getPairCount()];
		balance[searchPairIndex]++;
		this.scanner.configureDefaultPartitions(this.document);
		balance= this.scanner.computePairBalance(backwardOffset, forwardOffset, this.validRange,
				brackets, balance, searchPairIndex );
		return (balance[searchPairIndex] <= 0);
	}
	
	private final boolean isClosedString(int offset, final int end, final boolean endVirtual, final char sep)
			throws BadLocationException {
		this.scanner.configure(this.document);
		boolean in= true; // we start always inside after a sep
		final char[] chars= new char[] { sep, '\\' };
		while (offset < end) {
			offset= this.scanner.scanForward(offset, end, chars);
			if (offset == TextTokenScanner.NOT_FOUND) {
				offset= end;
				break;
			}
			offset++;
			if (this.scanner.getChar() == '\\') {
				offset++;
			}
			else {
				in= !in;
			}
		}
		return (offset == end) && (!in ^ endVirtual);
	}
	
	private final boolean isRoxygenLine(final int lineOffset) throws BadLocationException {
		this.scanner.configure(this.document);
		final int next= this.scanner.findAnyNonSSpaceForward(lineOffset, this.validRange.getEndOffset());
		return (next != TextTokenScanner.NOT_FOUND
				&& next + 1 < this.validRange.getEndOffset() && this.document.get(next, 2).equals("#'") ); //$NON-NLS-1$
	}
	
	private boolean isCharAt(final int offset, final char c) throws BadLocationException {
		return (offset >= this.validRange.getStartOffset() && offset < this.validRange.getEndOffset()
				&& this.document.getChar(offset) == c);
	}
	
	private boolean isValueChar(final int offset) throws BadLocationException {
		if (offset >= this.validRange.getStartOffset() && offset < this.validRange.getEndOffset()) {
			final int c= this.document.getChar(offset);
			return (c == '"' || c == '\'' || c == '`' || Character.isLetterOrDigit(c));
		}
		return false;
	}
	
	private @Nullable String checkStringR(final int startOffset, final int offsetClose, final int cEndOffset,
			final CharPair bracketType)
			throws BadLocationException {
		final int length= offsetClose - startOffset;
		final char cQuote;
		{	// Validate
			if (length < 2) {
				return null;
			}
			int offset= startOffset;
			switch (this.document.getChar(offset++)) {
			case 'R':
			case 'r':
				break;
			default:
				return null;
			}
			switch (cQuote= this.document.getChar(offset++)) {
			case '\"':
			case '\'':
				break;
			default:
				return null;
			}
			while (offset < offsetClose) {
				if (this.document.getChar(offset++) == '-') {
					continue;
				}
				else {
					return null;
				}
			}
		}
		final StringBuilder sb= new StringBuilder(length + 1);
		final String closeSequence;
		{	// Create sequence
			sb.append(bracketType.closing);
			for (int i= 1; i < length - 1; i++) {
				sb.append('-');
			}
			sb.append(cQuote);
			closeSequence= sb.toString();
		}
		
		this.scanner.configure(this.document);
		final int closeOffset= this.scanner.scanForward(cEndOffset,
				Math.min(this.validRange.getEndOffset(), cEndOffset + 0x1000),
				closeSequence );
		if (closeOffset != TextTokenScanner.NOT_FOUND) {
			sb.setCharAt(0, bracketType.opening);
			sb.append('r');
			sb.reverse();
			final var openSequence0= sb.toString();
			sb.setCharAt(0, 'R');
			final var openSequence1= sb.toString();
			if (this.scanner.scanForward(cEndOffset, closeOffset, ImCollections.newList(openSequence0, openSequence1)) == TextTokenScanner.NOT_FOUND) {
				return null;
			}
		}
		return closeSequence;
	}
	
	private boolean isAfterRoxygen(final int offset) throws BadLocationException {
		this.scanner.configure(this.document);
		final int line= this.document.getLineOfOffset(offset);
		if (line > 0 && this.scanner.findAnyNonSSpaceBackward(offset, this.document.getLineOffset(line)) == TextTokenScanner.NOT_FOUND) {
			final IRegion prevLineInfo= this.document.getLineInformation(line - 1);
			if (prevLineInfo.getLength() > 0 && TextUtilities.getPartition(this.document,
							this.scanner.getDocumentPartitioning(),
							prevLineInfo.getOffset() + prevLineInfo.getLength() - 1, false )
					.getType() == RDocumentConstants.R_ROXYGEN_CONTENT_TYPE) {
				return true;
			}
		}
		return false;
	}
	
	
	@Override
	public void customizeDocumentCommand(final IDocument d, final DocumentCommand c) {
		if (this.ignoreCommands || !c.doit || c.text == null) {
			return;
		}
		if (!isSmartInsertEnabled() || isBlockSelection()) {
			super.customizeDocumentCommand(d, c);
			return;
		}
		
		try {
			this.document= (AbstractDocument) d;
			final ITypedRegion partition= initCustomization(c.offset, -1);
			if (partition == null) {
				return;
			}
			final String contentType= partition.getType();
			
			if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE) {
				if (c.length == 0 && TextUtilities.equals(d.getLegalLineDelimiters(), c.text) != -1) {
					smartIndentOnNewLine(c, contentType);
				}
				else if (c.text.length() > 1 && this.editorOptions.isSmartPasteEnabled()) {
					smartPaste(c);
				}
			}
		}
		catch (final Exception e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, RUI.BUNDLE_ID, -1,
					"An error occurred when customizing action for document command in R auto edit strategy.", e )); //$NON-NLS-1$
		}
		finally {
			quitCustomization();
		}
	}
	
	/**
	 * Second main entry method for real single key presses.
	 * 
	 * @return <code>true</code>, if key was processed by method
	 */
	private boolean customizeKeyPressed(final char c) {
		if (!isSmartInsertEnabled() || !UIAccess.isOkToUse(this.viewer) || isBlockSelection()) {
			return false;
		}
		
		try {
			this.document= (AbstractDocument)this.viewer.getDocument();
			ITextSelection selection= (ITextSelection) this.viewer.getSelection();
			final ITypedRegion partition= initCustomization(selection.getOffset(), c);
			if (partition == null) {
				return false;
			}
			final String contentType= partition.getType();
			this.ignoreCommands= true;
			
			final DocumentCommand command= new DocumentCommand() {};
			command.offset= selection.getOffset();
			command.length= selection.getLength();
			command.doit= true;
			command.shiftsCaret= true;
			command.caretOffset= -1;
			LinkedModeFactory linkedMode= null;
			boolean contextInfo= false;
			int cStartOffset= command.offset;
			final int cEndOffset= command.offset + command.length;
			
			KEY: switch (c) {
			case '\t':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE
						|| contentType == RDocumentConstants.R_COMMENT_CONTENT_TYPE
						|| contentType == RDocumentConstants.R_ROXYGEN_CONTENT_TYPE ) {
					if (command.length == 0 || this.document.getLineOfOffset(command.offset) == this.document.getLineOfOffset(cEndOffset)) {
						command.text= "\t"; //$NON-NLS-1$
						switch (smartIndentOnTab(command)) {
						case -1:
							return false;
						case 0:
							break;
						case 1:
							break KEY;
						}
						
						if (this.rCodeStyle.getReplaceOtherTabsWithSpaces()) {
							final IndentUtil indentation= new IndentUtil(this.document, this.rCodeStyle);
							command.text= indentation.createTabSpacesCompletionString(
									indentation.getColumn(command.offset) );
							break KEY;
						}
					}
				}
				return false;
			case '}':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE) {
					command.text= "}"; //$NON-NLS-1$
					smartIndentOnClosingBracket(command);
					break KEY;
				}
				return false;
			case '{':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE) {
					command.text= "{"; //$NON-NLS-1$
					if (this.editorOptions.isSmartCurlyBracketsEnabled()
							&& !isValueChar(cEndOffset)) {
						if (!isClosedBracket(command.offset, cEndOffset, CURLY_BRACKETS)) {
							command.text= "{}"; //$NON-NLS-1$
							linkedMode= createLinkedModeCurlyBracket(1, AUTODELETE);
						}
						else if (isCharAt(cEndOffset, '}')) {
							linkedMode= createLinkedModeCurlyBracket(1, 0);
						}
					}
					cStartOffset= smartIndentOnFirstLineCharDefault2(command);
					break KEY;
				}
				else if (contentType == RDocumentConstants.R_STRING_CONTENT_TYPE
						&& this.editorOptions.isSmartStringsEnabled()
						&& !isValueChar(cEndOffset) ) {
					final String closeSequence= checkStringR(partition.getOffset(),
							command.offset, cEndOffset, CURLY_BRACKETS );
					if (closeSequence != null) {
						command.text= '{' + closeSequence;
						linkedMode= createLinkedModeStringRCurly(1 + closeSequence.length(), AUTODELETE);
						break KEY;
					}
				}
				return false;
			case '(':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE) {
					command.text= "("; //$NON-NLS-1$
					if (this.editorOptions.isSmartRoundBracketsEnabled()
							&& !isValueChar(cEndOffset)) {
						if (!isClosedBracket(command.offset, cEndOffset, ROUND_BRACKETS)) {
							command.text= "()"; //$NON-NLS-1$
							linkedMode= createLinkedModeRoundBracket(2, AUTODELETE);
						}
						else if (isCharAt(cEndOffset, ')')) {
							linkedMode= createLinkedModeRoundBracket(2, 0);
						}
					}
					if (isValueChar(command.offset - 1)) {
						contextInfo= true;
					}
					break KEY;
				}
				else if (contentType == RDocumentConstants.R_STRING_CONTENT_TYPE
						&& this.editorOptions.isSmartStringsEnabled()
						&& !isValueChar(cEndOffset) ) {
					final String closeSequence= checkStringR(partition.getOffset(),
							command.offset, cEndOffset, ROUND_BRACKETS );
					if (closeSequence != null) {
						command.text= '(' + closeSequence;
						linkedMode= createLinkedModeStringRRound(1 + closeSequence.length(), AUTODELETE);
						break KEY;
					}
				}
				return false;
			case ')':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE) {
					command.text= ")"; //$NON-NLS-1$
					cStartOffset= smartIndentOnFirstLineCharDefault2(command); // required?
					break KEY;
				}
				return false;
			case '[':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE) {
					command.text= "["; //$NON-NLS-1$
					if (this.editorOptions.isSmartSquareBracketsEnabled()
							&& !isValueChar(cEndOffset)) {
						if (!isClosedBracket(command.offset, cEndOffset, SQUARE_BRACKETS)) {
							command.text= "[]"; //$NON-NLS-1$
							if (TextUtil.countBackward(this.document, command.offset, '[') % 2 == 1
									&& isCharAt(cEndOffset, ']') ) {
								linkedMode= createLinkedModeSquareBracket(3, AUTODELETE);
							}
							else {
								linkedMode= createLinkedModeSquareBracket(2, AUTODELETE);
							}
						}
						else if (isCharAt(cEndOffset, ']')) {
							linkedMode= createLinkedModeSquareBracket(2, 0);
						}
					}
					break KEY;
				}
				else if (contentType == RDocumentConstants.R_STRING_CONTENT_TYPE
						&& this.editorOptions.isSmartStringsEnabled()
						&& !isValueChar(cEndOffset) ) {
					final String closeSequence= checkStringR(partition.getOffset(),
							command.offset, cEndOffset, SQUARE_BRACKETS );
					if (closeSequence != null) {
						command.text= '[' + closeSequence;
						linkedMode= createLinkedModeStringRSquare(1 + closeSequence.length(), AUTODELETE);
						break KEY;
					}
				}
				return false;
			case '%':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE
						&& this.editorOptions.isSmartSpecialPercentEnabled()) {
					final IRegion line= this.document.getLineInformationOfOffset(cEndOffset);
					this.scanner.configure(this.document, RDocumentConstants.R_OPERATOR_SPECIAL_CONTENT_TYPE);
					if (this.scanner.count(cEndOffset, line.getOffset() + line.getLength(), '%') % 2 == 0) {
						command.text= "%%"; //$NON-NLS-1$
						linkedMode= createLinkedModeOpSpecial(2, AUTODELETE);
						break KEY;
					}
				}
				return false;
			case '`':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE
						&& this.editorOptions.isSmartStringsEnabled()
						&& !isValueChar(cEndOffset) && !isValueChar(command.offset - 1) ) {
					final IRegion line= this.document.getLineInformationOfOffset(cEndOffset);
					if (!isClosedString(cEndOffset, line.getOffset() + line.getLength(), false, c)) {
						command.text= "``"; //$NON-NLS-1$
						linkedMode= createLinkedModeSymbolG(2, AUTODELETE);
						break KEY;
					}
				}
				return false;
			case '"':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE
						&& this.editorOptions.isSmartStringsEnabled()
						&& !isValueChar(cEndOffset) && !isValueChar(command.offset - 1) ) {
					final IRegion line= this.document.getLineInformationOfOffset(cEndOffset);
					if (!isClosedString(cEndOffset, line.getOffset() + line.getLength(), false, c)) {
						command.text= "\"\""; //$NON-NLS-1$
						linkedMode= createLinkedModeStringD(2, AUTODELETE);
						break KEY;
					}
				}
				return false;
			case '\'':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE
						&& this.editorOptions.isSmartStringsEnabled()
						&& !isValueChar(cEndOffset) && !isValueChar(command.offset - 1) ) {
					final IRegion line= this.document.getLineInformationOfOffset(cEndOffset);
					if (!isClosedString(cEndOffset, line.getOffset() + line.getLength(), false, c)) {
						command.text= "\'\'"; //$NON-NLS-1$
						linkedMode= createLinkedModeStringS(2, AUTODELETE);
						break KEY;
					}
				}
				return false;
			case '\n':
				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE
						|| contentType == RDocumentConstants.R_COMMENT_CONTENT_TYPE
						|| contentType == RDocumentConstants.R_ROXYGEN_CONTENT_TYPE) {
					command.text= TextUtilities.getDefaultLineDelimiter(this.document);
					linkedMode= smartIndentOnNewLine(command, contentType);
					break KEY;
				}
				return false;
			case '#':
 				if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE
						&& isAfterRoxygen(command.offset)) {
					command.text= "#' "; //$NON-NLS-1$
					break KEY;
				}
				return false;
			default:
				assert (false);
				return false;
			}
			
			if (command.text.length() > 0 && this.editor.isEditable(true)) {
				this.viewer.getTextWidget().setRedraw(false);
				try {
					this.document.replace(command.offset, command.length, command.text);
					final int cursor= (command.caretOffset >= 0) ? command.caretOffset :
							command.offset + command.text.length();
					selection= new TextSelection(this.document, cursor, 0);
					this.viewer.setSelection(selection, true);
					
					if (linkedMode != null) {
						linkedMode.createLinkedMode(cStartOffset).enter();
					}
				}
				finally {
					this.viewer.getTextWidget().setRedraw(true);
				}
				
				if (contextInfo
						&& this.viewer.canDoOperation(ISourceViewer.CONTENTASSIST_CONTEXT_INFORMATION)) {
					this.viewer.doOperation(ISourceViewer.CONTENTASSIST_CONTEXT_INFORMATION);
				}
			}
			return true;
		}
		catch (final Exception e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, RUI.BUNDLE_ID, -1,
					"An error occurred when customizing action for pressed key in R auto edit strategy.", e )); //$NON-NLS-1$
		}
		finally {
			this.ignoreCommands= false;
			quitCustomization();
		}
		return false;
	}
	
	/**
	 * Generic method to indent lines using the RSourceIndenter, called algorithm 2.
	 * @param c handle to read and save the document informations
	 * @param indentCurrentLine
	 * @param setCaret positive values indicates the line to set the caret
	 * @param traceCursor offset to update and return (offset at state after insertion of c.text)
	 */
	private Position[] smartIndentLine2(final DocumentCommand c, final boolean indentCurrentLine, final int setCaret,
			final Position... tracePos) throws BadLocationException, BadPartitioningException, CoreException {
		if (this.editor3 == null) {
			return tracePos;
		}
		final TextRegion validRegion= this.validRange;
		
		// new algorithm using RSourceIndenter
		final int cEnd= c.offset + c.length;
		if (cEnd > validRegion.getEndOffset()) {
			return tracePos;
		}
		this.scanner.configure(this.document);
		final int smartEnd;
		final String smartAppend;
		if (endsWithNewLine(c.text)) {
			final IRegion cEndLine= this.document.getLineInformationOfOffset(cEnd);
			final int validEnd= Math.min(cEndLine.getOffset() + cEndLine.getLength(), validRegion.getEndOffset());
			smartEnd= this.scanner.expandAnySSpaceForward(cEnd, validEnd);
			switch(this.scanner.getChar()) {
			case '}':
			case '{':
			case '|':
			case '&':
				smartAppend= ""; //$NON-NLS-1$
				break;
			default:
				smartAppend= "DUMMY+"; //$NON-NLS-1$
				break;
			}
		}
		else {
			smartEnd= cEnd;
			smartAppend= ""; //$NON-NLS-1$
		}
		
		int shift= 0;
		if (c.offset < validRegion.getStartOffset()
				|| c.offset > validRegion.getEndOffset()) {
			return tracePos;
		}
		if (c.offset > 2500) {
			final int line= this.document.getLineOfOffset(c.offset) - 40;
			if (line >= 10) {
				shift= this.document.getLineOffset(line);
				final ITypedRegion partition= this.document.getPartition(
						this.scanner.getDocumentPartitioning(), shift, true );
				if (partition.getType() != RDocumentConstants.R_DEFAULT_CONTENT_TYPE) {
					shift= partition.getOffset();
				}
			}
		}
		if (shift < validRegion.getStartOffset()) {
			shift= validRegion.getStartOffset();
		}
		int dummyDocEnd= cEnd+1500;
		if (dummyDocEnd > validRegion.getEndOffset()) {
			dummyDocEnd= validRegion.getEndOffset();
		}
		final String text;
		{	final StringBuilder s= new StringBuilder(
					(c.offset-shift) +
					c.text.length() +
					(smartEnd-cEnd) +
					smartAppend.length() +
					(dummyDocEnd-smartEnd) );
			s.append(this.document.get(shift, c.offset-shift));
			s.append(c.text);
			if (smartEnd - cEnd > 0) {
				s.append(this.document.get(cEnd, smartEnd - cEnd));
			}
			s.append(smartAppend);
			s.append(this.document.get(smartEnd, dummyDocEnd - smartEnd));
			text= s.toString();
		}
		
		// Create temp doc to compute indent
		int dummyCoffset= c.offset-shift;
		int dummyCend= dummyCoffset + c.text.length();
		final AbstractDocument dummyDoc= new Document(text);
		final TextParserInput parserInput= getParserInput(text);
		
		// Lines to indent
		int dummyFirstLine= dummyDoc.getLineOfOffset(dummyCoffset);
		final int dummyLastLine= dummyDoc.getLineOfOffset(dummyCend);
		if (!indentCurrentLine) {
			dummyFirstLine++;
		}
		if (dummyFirstLine > dummyLastLine) {
			return tracePos;
		}
		
		// Compute indent
		final RParser rParser= getRParser();
		final RSourceIndenter indenter= getSourceIndenter();
		final TextEdit edit= indenter.getIndentEdits(dummyDoc, rParser.parseSourceUnit(parserInput.init()),
				0, dummyFirstLine, dummyLastLine );
		
		// Apply indent to temp doc
		final Position cPos= new Position(dummyCoffset, c.text.length());
		dummyDoc.addPosition(cPos);
		if (tracePos != null) {
			for (int i= 0; i < tracePos.length; i++) {
				tracePos[i].offset -= shift;
				dummyDoc.addPosition(tracePos[i]);
			}
		}
		
		c.length= c.length + edit.getLength()
				// add space between two replacement regions minus overlaps with c.text
				- TextUtil.overlaps(edit.getOffset(), edit.getExclusiveEnd(), dummyCoffset, dummyCend);
		if (edit.getOffset() < dummyCoffset) { // move offset, if edit begins before c
			dummyCoffset= edit.getOffset();
			c.offset= shift + dummyCoffset;
		}
		edit.apply(dummyDoc, TextEdit.NONE);
		
		// Read indent for real doc
		int dummyChangeEnd= edit.getExclusiveEnd();
		dummyCend= cPos.getOffset() + cPos.getLength();
		if (!cPos.isDeleted && dummyCend > dummyChangeEnd) {
			dummyChangeEnd= dummyCend;
		}
		c.text= dummyDoc.get(dummyCoffset, dummyChangeEnd-dummyCoffset);
		if (setCaret != 0) {
			c.caretOffset= shift + indenter.getNewIndentOffset(dummyFirstLine + setCaret - 1);
			c.shiftsCaret= false;
		}
		indenter.clear();
		if (tracePos != null) {
			for (int i= 0; i < tracePos.length; i++) {
				tracePos[i].offset+= shift;
			}
		}
		return tracePos;
	}
	
	private final boolean endsWithNewLine(final String text) {
		for (int i= text.length()-1; i >= 0; i--) {
			final char c= text.charAt(i);
			if (c == '\r' || c == '\n') {
				return true;
			}
			if (c != ' ' && c != '\t') {
				return false;
			}
		}
		return false;
	}
	
	
	private @Nullable LinkedModeFactory smartIndentOnNewLine(final DocumentCommand c, final String contentType)
			throws BadLocationException, BadPartitioningException, CoreException {
		final String lineDelimiter= c.text;
		try {
			final int before= c.offset - 1;
			final int behind= c.offset + c.length;
			if (contentType == RDocumentConstants.R_DEFAULT_CONTENT_TYPE) {
				if (before >= 0 && behind < this.validRange.getEndOffset()
						&& this.document.getChar(before) == '{' && this.document.getChar(behind) == '}' ) {
					c.text= c.text + c.text;
				}
			}
			else if (contentType == RDocumentConstants.R_ROXYGEN_CONTENT_TYPE) {
				if (!isRoxygenLine(behind)) {
					c.text= c.text + "#' "; //$NON-NLS-1$
					smartIndentLine2(c, false, 0);
					return createLinkedModeRoxygen(c.text.length(), AUTODELETE);
				}
			}
			smartIndentLine2(c, false, 1);
			return null;
		}
		catch (final Exception e) {
			RUIPlugin.logError(RUIPlugin.INTERNAL_ERROR, "An error occurred while customize a command in R auto edit strategy (algorithm 2).", e); //$NON-NLS-1$
			smartIndentAfterNewLine1(c, lineDelimiter);
			return null;
		}
	}
	
	private void smartIndentAfterNewLine1(final DocumentCommand c, final String lineDelimiter)
			throws BadLocationException, BadPartitioningException, CoreException {
		final StringBuilder sb= new StringBuilder(c.text);
		int nlIndex= lineDelimiter.length();
		
		final int line= this.document.getLineOfOffset(c.offset);
		int checkOffset= Math.max(0, c.offset);
		
		final ITypedRegion partition= this.document.getPartition(
				this.scanner.getDocumentPartitioning(), checkOffset, true );
		if (partition.getType() == RDocumentConstants.R_COMMENT_CONTENT_TYPE) {
			checkOffset= partition.getOffset();
		}
		
		final IndentUtil util= new IndentUtil(this.document, this.rCodeStyle);
		final int column= util.getLineIndent(line, false)[IndentUtil.COLUMN_IDX];
		
		if (checkOffset > 0) {
			// new block?:
			this.scanner.configure(this.document);
			final int match= this.scanner.findAnyNonSSpaceBackward(checkOffset, this.document.getLineOffset(line));
			if (match != TextTokenScanner.NOT_FOUND
					&& this.document.getChar(match) == '{' ) {
				final String indent= util.createIndentString(util.getNextLevelColumn(column, 1));
				sb.insert(nlIndex, indent);
				nlIndex+= indent.length() + lineDelimiter.length();
			}
		}
		
		if (nlIndex <= sb.length()) {
			sb.insert(nlIndex, util.createIndentString(column));
		}
		c.text= sb.toString();
	}
	
	private int smartIndentOnTab(final DocumentCommand c) throws BadLocationException {
		final IRegion line= this.document.getLineInformation(this.document.getLineOfOffset(c.offset));
		int first;
		this.scanner.configure(this.document);
		first= this.scanner.findAnyNonSSpaceBackward(c.offset, line.getOffset());
		if (first != TextTokenScanner.NOT_FOUND) { // not first char
			return 0;
		}
//		first= scanner.findAnyNonBlankForward(c.offset, line.getOffset()+line.getLength(), false);
//		if (c.offset == line.getOffset() || c.offset != first) {
//			try {
//				final Position cursorPos= new Position(first, 0);
//				smartIndentLine2(c, true, 0, new Position[] { cursorPos });
//				c.caretOffset= cursorPos.getOffset();
//				return 1;
//			}
//			catch (final Exception e) {
//				RUIPlugin.logError(RUIPlugin.INTERNAL_ERROR, "An error occurred while customize a command in R auto edit strategy (algorithm 2).", e); //$NON-NLS-1$
//				return -1;
//			}
//		}
		final IndentUtil indentation= new IndentUtil(this.document, this.rCodeStyle);
		final int column= indentation.getColumn(c.offset);
		if (this.editorOptions.getSmartInsertTabAction() != SmartInsertSettings.TabAction.INSERT_TAB_CHAR) {
			c.text= indentation.createIndentCompletionString(column);
		}
		return 1;
	}
	
	private void smartIndentOnClosingBracket(final DocumentCommand c) throws BadLocationException {
		final int lineOffset= this.document.getLineOffset(this.document.getLineOfOffset(c.offset));
		this.scanner.configure(this.document);
		if (this.scanner.findAnyNonSSpaceBackward(c.offset, lineOffset) != TextTokenScanner.NOT_FOUND) {
			// not first char
			return;
		}
		
		try {
			final Position cursorPos= new Position(c.offset + 1, 0);
			smartIndentLine2(c, true, 0, cursorPos);
			c.caretOffset= cursorPos.getOffset();
			return;
		}
		catch (final Exception e) {
			RUIPlugin.logError(RUIPlugin.INTERNAL_ERROR, "An error occurred while customize a command in R auto edit strategy (algorithm 2).", e); //$NON-NLS-1$
			smartIndentOnClosingBracket1(c);
		}
	}
	
	private void smartIndentOnClosingBracket1(final DocumentCommand c) throws BadLocationException {
		final int lineOffset= this.document.getLineOffset(this.document.getLineOfOffset(c.offset));
		final int blockStart= this.scanner.findOpeningPeer(lineOffset, CURLY_BRACKETS, NO_CHAR);
		if (blockStart == TextTokenScanner.NOT_FOUND) {
			return;
		}
		final IndentUtil util= new IndentUtil(this.document, this.rCodeStyle);
		final int column= util.getLineIndent(this.document.getLineOfOffset(blockStart), false)[IndentUtil.COLUMN_IDX];
		c.text= util.createIndentString(column) + c.text;
		c.length += c.offset - lineOffset;
		c.offset= lineOffset;
	}
	
	private int smartIndentOnFirstLineCharDefault2(final DocumentCommand c) throws BadLocationException {
		final int lineOffset= this.document.getLineOffset(this.document.getLineOfOffset(c.offset));
		this.scanner.configure(this.document);
		if (this.scanner.findAnyNonSSpaceBackward(c.offset, lineOffset) != TextTokenScanner.NOT_FOUND) {
			// not first char
			return c.offset;
		}
		
		try {
			final Position cursorPos= new Position(c.offset + 1, 0);
			smartIndentLine2(c, true, 0, cursorPos);
			return (c.caretOffset= cursorPos.getOffset()) - 1;
		}
		catch (final Exception e) {
			RUIPlugin.logError(RUIPlugin.INTERNAL_ERROR, "An error occurred while customize a command in R auto edit strategy (algorithm 2).", e); //$NON-NLS-1$
			return -1;
		}
	}
	
	private void smartPaste(final DocumentCommand c) throws BadLocationException {
		final int lineOffset= this.document.getLineOffset(this.document.getLineOfOffset(c.offset));
		this.scanner.configure(this.document);
		final boolean firstLine= (this.scanner.findAnyNonSSpaceBackward(c.offset, lineOffset) == TextTokenScanner.NOT_FOUND);
		try {
			smartIndentLine2(c, firstLine, 0);
		}
		catch (final Exception e) {
			RUIPlugin.logError(RUIPlugin.INTERNAL_ERROR, "An error occurred while customize a command in R auto edit strategy (algorithm 2).", e); //$NON-NLS-1$
		}
	}
	
	
	private abstract class LinkedModeFactory {
		
		/** diff to offset= cStartOffset */
		protected final int endOffset;
		
		protected final int flags;
		
		
		public LinkedModeFactory(final int endOffset, int flags) {
			this.endOffset= endOffset;
			if ((RAutoEditStrategy.this.viewer instanceof InputSourceViewer)) {
				flags|= RBracketLevel.CONSOLE_MODE;
			}
			this.flags= flags;
		}
		
		
		protected abstract LinkedPosition createPosition(final int offset, final int pos);
		
		protected IExitPolicy createLevel(final LinkedModeModel model, final LinkedPosition position) {
			return new RBracketLevel(model, getDocument(), getDocumentContentInfo(),
					ImCollections.newList(position), this.flags );
		}
		
		LinkedModeUI createLinkedMode(final int offset) throws BadLocationException {
			final LinkedModeModel model= new LinkedModeModel();
			int pos= 0;
			
			final var group= new LinkedPositionGroup();
			final var position= createPosition(offset, pos++);
			group.addPosition(position);
			model.addGroup(group);
			
			model.forceInstall();
			
			final var level= createLevel(model, position);
			
			final LinkedModeUI ui= new LinkedModeUI(model, RAutoEditStrategy.this.viewer);
			ui.setCyclingMode(LinkedModeUI.CYCLE_NEVER);
			if (level instanceof RBracketLevel) {
				ui.setExitPosition(RAutoEditStrategy.this.viewer, offset + this.endOffset, 0, pos);
			}
			ui.setSimpleMode(true);
			ui.setExitPolicy(level);
			return ui;
		}
	}
	
	private LinkedModeFactory createLinkedModeCurlyBracket(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.CurlyBracketPosition(
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeRoundBracket(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.RoundBracketPosition(
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeSquareBracket(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.SquareBracketPosition(
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeSymbolG(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.SymbolGPosition(
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeStringD(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.StringDPosition(
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeStringS(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.StringSPosition(
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeStringRRound(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.StringRRoundPosition(this.endOffset - 2,
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeStringRSquare(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.StringRSquarePosition(this.endOffset - 2,
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeStringRCurly(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.StringRCurlyPosition(this.endOffset - 2,
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeOpSpecial(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new RBracketLevel.OpSpecialPosition(
						getDocument(), offset + 1, 0, pos );
			}
		};
	}
	
	private LinkedModeFactory createLinkedModeRoxygen(final int endOffset, final int flags) {
		return new LinkedModeFactory(endOffset, flags) {
			@Override
			protected LinkedPosition createPosition(final int offset, final int pos) {
				return new LinkedPosition(getDocument(), offset + this.endOffset, 0, pos);
			}
			@Override
			protected IExitPolicy createLevel(final LinkedModeModel model, final LinkedPosition position) {
				return new LinkedModeAutodeleteLevel(getDocument(), position, 3);
			}
		};
	}
	
}
