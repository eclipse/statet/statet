/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.sourceediting;

import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.information.IInformationProvider;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.reconciler.IReconcilingStrategy;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.spelling.SpellingReconcileStrategy;
import org.eclipse.ui.texteditor.spelling.SpellingService;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.text.IIndentSettings;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.presentation.SingleTokenScanner;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.internal.r.ui.editors.r.REditor;
import org.eclipse.statet.internal.r.ui.editors.r.REditorInformationProvider;
import org.eclipse.statet.internal.r.ui.editors.r.REditorTextHover;
import org.eclipse.statet.internal.r.ui.editors.r.RQuickOutlineInformationProvider;
import org.eclipse.statet.ltk.text.core.CharPairMatcher;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.EcoReconciler2;
import org.eclipse.statet.ltk.ui.sourceediting.EditorInformationProvider;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorAddon;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceUnitReconcilingStrategy;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistProcessor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHoverDescriptor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHoverRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistProcessor;
import org.eclipse.statet.ltk.ui.sourceediting.presentation.CommentScanner;
import org.eclipse.statet.nico.ui.console.ConsolePageEditor;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RDocumentContentInfo;
import org.eclipse.statet.r.core.source.util.RBracketPairMatcher;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;
import org.eclipse.statet.r.ui.editors.REditorOptions;
import org.eclipse.statet.r.ui.editors.RSourceEditor;
import org.eclipse.statet.r.ui.text.r.IRTextTokens;
import org.eclipse.statet.r.ui.text.r.RDefaultTextStyleScanner;
import org.eclipse.statet.r.ui.text.r.RDoubleClickStrategy;
import org.eclipse.statet.r.ui.text.r.ROperatorSpecialScanner;
import org.eclipse.statet.r.ui.text.r.RoxygenScanner;


/**
 * Default Configuration for SourceViewer of R code.
 */
@NonNullByDefault
public class RSourceViewerConfiguration extends SourceEditorViewerConfiguration {
	
	
	private static final @NonNull String[] CONTENT_TYPES= RDocumentConstants.R_CONTENT_TYPES.toArray(String[]::new);
	
	
	private RDoubleClickStrategy doubleClickStrategy;
	
	private RAutoEditStrategy autoEditStrategy;
	
	private RCoreAccess coreAccess;
	
	
	public RSourceViewerConfiguration(final int flags,
			final @Nullable IPreferenceStore store) {
		this(RDocumentContentInfo.INSTANCE, flags, null, null, store, null);
	}
	
	public RSourceViewerConfiguration(final DocContentSections documentContentInfo, final int flags,
			final RSourceEditor sourceEditor,
			final RCoreAccess access,
			final @Nullable IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		super(documentContentInfo, flags, sourceEditor);
		setCoreAccess(access);
		setup((preferenceStore != null) ? preferenceStore : RUIPlugin.getInstance().getEditorPreferenceStore(),
				LtkUIPreferences.getEditorDecorationPreferences(),
				LtkUIPreferences.getAssistPreferences() );
		setTextStyles(textStyles);
	}
	
	protected void setCoreAccess(final RCoreAccess access) {
		this.coreAccess= (access != null) ? access : RCore.getWorkbenchAccess();
	}
	
	
	@Override
	protected RSourceEditor getSourceEditor() {
		return (RSourceEditor)super.getSourceEditor();
	}
	
	
	@Override
	protected void initTextStyles() {
		setTextStyles(RUIPlugin.getInstance().getRTextStyles());
	}
	
	@Override
	protected void initScanners() {
		final var textStyles= getTextStyles();
		
		addScanner(RDocumentConstants.R_DEFAULT_CONTENT_TYPE,
				new RDefaultTextStyleScanner(textStyles) );
		addScanner(RDocumentConstants.R_OPERATOR_SPECIAL_CONTENT_TYPE,
				new ROperatorSpecialScanner(textStyles) );
		addScanner(RDocumentConstants.R_STRING_CONTENT_TYPE,
				new SingleTokenScanner(textStyles, IRTextTokens.STRING_KEY) );
		addScanner(RDocumentConstants.R_COMMENT_CONTENT_TYPE,
				new CommentScanner(textStyles, IRTextTokens.COMMENT_KEY, IRTextTokens.TASK_TAG_KEY,
						this.coreAccess.getPrefs() ));
		addScanner(RDocumentConstants.R_ROXYGEN_CONTENT_TYPE,
				new RoxygenScanner(textStyles, this.coreAccess.getPrefs()) );
	}
	
	@Override
	protected ITokenScanner getScanner(String contentType) {
		if (contentType == RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE) {
			contentType= RDocumentConstants.R_STRING_CONTENT_TYPE;
		}
		return super.getScanner(contentType);
	}
	
	
	public RCoreAccess getRCoreAccess() {
		return this.coreAccess;
	}
	
	@Override
	public List<SourceEditorAddon> getAddOns() {
		final List<SourceEditorAddon> addons= super.getAddOns();
		if (this.autoEditStrategy != null) {
			addons.add(this.autoEditStrategy);
		}
		return addons;
	}
	
	
	@Override
	public @NonNull String[] getConfiguredContentTypes(final ISourceViewer sourceViewer) {
		return CONTENT_TYPES;
	}
	
	
	@Override
	public CharPairMatcher createPairMatcher() {
		return new RBracketPairMatcher(
				RHeuristicTokenScanner.create(getDocumentContentInfo()) );
	}
	
	@Override
	public ITextDoubleClickStrategy getDoubleClickStrategy(final ISourceViewer sourceViewer, final String contentType) {
		if (this.doubleClickStrategy == null) {
			this.doubleClickStrategy= new RDoubleClickStrategy(
					RHeuristicTokenScanner.create(getDocumentContentInfo()) );
		}
		return this.doubleClickStrategy;
	}
	
	@Override
	public @NonNull String[] getDefaultPrefixes(final ISourceViewer sourceViewer, final String contentType) {
		return new @NonNull String[] { "#", "" }; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	@Override
	protected IIndentSettings getIndentSettings() {
		return this.coreAccess.getRCodeStyle();
	}
	
	
	@Override
	public boolean isSmartInsertSupported() {
		return true;
	}
	
	@Override
	public boolean isSmartInsertByDefault() {
		return EPreferences.getInstancePrefs()
				.getPreferenceValue(REditorOptions.SMARTINSERT_BYDEFAULT_ENABLED_PREF);
	}
	
	
	@Override
	public @NonNull IAutoEditStrategy[] getAutoEditStrategies(final ISourceViewer sourceViewer, final String contentType) {
		if (getSourceEditor() == null) {
			return super.getAutoEditStrategies(sourceViewer, contentType);
		}
		if (this.autoEditStrategy == null) {
			this.autoEditStrategy= createRAutoEditStrategy();
		}
		return new @NonNull IAutoEditStrategy[] { this.autoEditStrategy };
	}
	
	protected RAutoEditStrategy createRAutoEditStrategy() {
		return new RAutoEditStrategy(this.coreAccess, getSourceEditor());
	}
	
	
	@Override
	public @Nullable IReconciler getReconciler(final ISourceViewer sourceViewer) {
		final RSourceEditor editor= getSourceEditor();
		if (!(editor instanceof SourceEditor1 || editor instanceof ConsolePageEditor)) {
			return null;
		}
		final EcoReconciler2 reconciler= new EcoReconciler2(editor);
		reconciler.setDelay(500);
		reconciler.addReconcilingStrategy(new SourceUnitReconcilingStrategy());
		
		if (editor instanceof REditor) {
			final IReconcilingStrategy spellingStrategy= getSpellingStrategy(sourceViewer);
			if (spellingStrategy != null) {
				reconciler.addReconcilingStrategy(spellingStrategy);
			}
		}
		
		return reconciler;
	}
	
	protected IReconcilingStrategy getSpellingStrategy(final ISourceViewer sourceViewer) {
		if (!(this.coreAccess.getPrefs().getPreferenceValue(REditorOptions.PREF_SPELLCHECKING_ENABLED)
				&& this.fPreferenceStore.getBoolean(SpellingService.PREFERENCE_SPELLING_ENABLED)) ) {
			return null;
		}
		final SpellingService spellingService= EditorsUI.getSpellingService();
		if (spellingService.getActiveSpellingEngineDescriptor(this.fPreferenceStore) == null) {
			return null;
		}
		return new SpellingReconcileStrategy(sourceViewer, spellingService);
	}
	
	
	@Override
	public void initContentAssist(final ContentAssist assistant) {
		final ContentAssistComputerRegistry registry= RUIPlugin.getInstance().getREditorContentAssistRegistry();
		
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_DEFAULT_CONTENT_TYPE, registry, getSourceEditor() );
			processor.setCompletionProposalAutoActivationCharacters(new char[] { '$', '@' });
			processor.setContextInformationAutoActivationCharacters(new char[] { ',' });
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_DEFAULT_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE );
		}
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_STRING_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_STRING_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_COMMENT_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_COMMENT_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new RContentAssistProcessor(assistant,
					RDocumentConstants.R_ROXYGEN_CONTENT_TYPE, registry, getSourceEditor() );
			processor.setCompletionProposalAutoActivationCharacters(new char[] { '@', '\\' });
			assistant.setContentAssistProcessor(processor, RDocumentConstants.R_ROXYGEN_CONTENT_TYPE);
		}
	}
	
	@Override
	protected @Nullable QuickAssistProcessor createQuickAssistProcessor() {
		final RSourceEditor editor= getSourceEditor();
		if (editor != null) {
			return new RQuickAssistProcessor(editor);
		}
		return null;
	}
	
	@Override
	protected boolean isInfoHoverDefaultContentType(final String contentType) {
		return RDocumentConstants.R_CODE_CONTENT_CONSTRAINT.matches(contentType);
	}
	
	@Override
	protected InfoHoverRegistry getInfoHoverRegistry() {
		return RUIPlugin.getInstance().getREditorInfoHoverRegistry();
	}
	
	@Override
	protected @Nullable ITextHover createInfoHover(final InfoHoverDescriptor descriptor) {
		final RSourceEditor editor= getSourceEditor();
		if (editor != null) {
			return new REditorTextHover(editor, descriptor, this);
		}
		return null;
	}
	
	@Override
	protected @Nullable EditorInformationProvider getInformationProvider() {
		final RSourceEditor editor= getSourceEditor();
		if (editor != null) {
			return new REditorInformationProvider(editor);
		}
		return null;
	}
	
	
	@Override
	protected void collectHyperlinkDetectorTargets(final Map<String, IAdaptable> targets,
			final ISourceViewer sourceViewer) {
		targets.put("org.eclipse.statet.r.editorHyperlinks.REditorTarget", getSourceEditor()); //$NON-NLS-1$
	}
	
	
	@Override
	protected @Nullable IInformationProvider getQuickInformationProvider(final ISourceViewer sourceViewer,
			final int operation) {
		final RSourceEditor editor= getSourceEditor();
		if (editor == null) {
			return null;
		}
		switch (operation) {
		case SourceEditorViewer.SHOW_SOURCE_OUTLINE:
			return new RQuickOutlineInformationProvider(editor, operation);
		default:
			return null;
		}
	}
	
}
