/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.pkgmanager;

import java.util.List;

import org.eclipse.statet.r.core.pkgmanager.RPkgAction;


public class StartAction {
	
	
	public static final int UNINSTALL= RPkgAction.UNINSTALL;
	public static final int INSTALL= RPkgAction.INSTALL;
	public static final int REINSTALL= 3;
	
	
	private final int action;
	private final List<String> pkgNames;
	
	
	public StartAction(final int action) {
		this(action, null);
	}
	
	public StartAction(final int action, final List<String> pkgNames) {
		this.action= action;
		this.pkgNames= pkgNames;
	}
	
	
	public int getAction() {
		return this.action;
	}
	
	public List<String> getPkgNames() {
		return this.pkgNames;
	}
	
}
