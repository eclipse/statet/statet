/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.editors;

import org.eclipse.statet.ecommons.text.Partitioner;
import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;

import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.ui.text.rd.RdFastPartitionScanner;


/**
 * The document setup participant for Rd.
 */
public class RdDocumentSetupParticipant extends PartitionerDocumentSetupParticipant {
	
	
	private static final String[] CONTENT_TYPES= RDocumentConstants.RDOC_CONTENT_TYPES.toArray(
			new String[RDocumentConstants.RDOC_CONTENT_TYPES.size()] );
	
	
	public RdDocumentSetupParticipant() {
	}
	
	
	@Override
	public String getPartitioningId() {
		return RDocumentConstants.RDOC_PARTITIONING;
	}
	
	@Override
	protected Partitioner createDocumentPartitioner() {
		return new Partitioner(new RdFastPartitionScanner(), CONTENT_TYPES);
	}
	
}
