/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;

import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.models.core.util.ElementPartition;
import org.eclipse.statet.ecommons.ui.components.StatusInfo;
import org.eclipse.statet.ecommons.ui.util.DNDUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.util.ViewActionUtil;

import org.eclipse.statet.r.core.model.RElementName;


@NonNullByDefault
public class CopyRElementNameHandler extends AbstractHandler {
	
	
	private final ViewActionUtil actionUtil;
	
	
	public CopyRElementNameHandler(final ViewActionUtil actionUtil) {
		this.actionUtil= actionUtil;
	}
	
	
	private ITreeSelection getSelection() {
		final ISelectionProvider selectionProvider= this.actionUtil.getSelectionProvider();
		return (ITreeSelection) selectionProvider.getSelection();
	}
	
	protected boolean isValidSelection(final ITreeSelection selection) {
		if (selection == null || selection.isEmpty()) {
			return false;
		}
		for (final Object element : selection.toList()) {
			if (element instanceof ElementPartition) {
				return false;
			}
		}
		return true;
	}
	
	protected @Nullable RElementName getRElementName(final TreePath treePath,
			final ITreeSelection selection) {
		return RElementInputUtils.getRElementName(treePath, selection);
	}
	
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		setBaseEnabled(isValidSelection(getSelection()));
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		if (!UIAccess.isOkToUse(this.actionUtil.getControl())) {
			return null;
		}
		final ITreeSelection selection= getSelection();
		if (!isValidSelection(selection)) {
			return null;
		}
		
		final String text= createData(selection);
		if (text != null) {
			copy(text);
		}
		
		return null;
	}
	
	protected @Nullable String createData(final ITreeSelection selection) {
		final TreePath[] treePaths= selection.getPaths();
		final List<String> names= new ArrayList<>(selection.size());
		for (int i= 0; i < treePaths.length; i++) {
			final RElementName elementName= getRElementName(treePaths[i], selection);
			final String name= (elementName != null) ? elementName.getDisplayName() : null;
			if (name != null) {
				names.add(name);
			}
			else {
				this.actionUtil.getStatusLine().setMessage(new StatusInfo(IStatus.WARNING,
						"Could not copy element name for the selected objects." ));
				return null;
			}
		}
		return (names.size() == 1) ?
				names.get(0) : CollectionUtils.toString(names, ", "); //$NON-NLS-1$
	}
	
	private void copy(final String text) {
		DNDUtils.setContent(this.actionUtil.getClipboard(),
				new String[] { text },
				new Transfer[] { TextTransfer.getInstance() } );
	}
	
}
