/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui;

import java.nio.file.Path;

import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;

import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.RLibGroup;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.core.RPkg;


public class REnvLabelProvider extends StyledCellLabelProvider {
	
	
	public static String getSafeLabel(final RLibLocation location) {
		String label= location.getLabel();
		if (label == null) {
			final Path store= location.getDirectoryPath();
			if (store != null) {
				label= store.toString();
			}
		}
		if (label == null) {
			label= location.getDirectory();
		}
		return label;
	}
	
	
	public REnvLabelProvider() {
	}
	
	
	@Override
	public void update(final ViewerCell cell) {
		final Object element = cell.getElement();
		if (element instanceof REnv) {
			update(cell, (REnv) element);
		}
		else if (element instanceof REnvConfiguration) {
			update(cell, ((REnvConfiguration) element).getREnv());
		}
		else if (element instanceof RLibGroup) {
			update(cell, (RLibGroup) element);
		}
		else if (element instanceof RLibLocation) {
			update(cell, (RLibLocation) element);
		}
		else if (element instanceof RPkg) {
			update(cell, (RPkg) element);
		}
		finishUpdate(cell);
	}
	
	protected void finishUpdate(final ViewerCell cell) {
		super.update(cell);
	}
	
	protected void update(final ViewerCell cell, final REnv rEnv) {
		cell.setImage(RUI.getImage(/*(rEnv.getId()) ?
				RUI.IMG_OBJ_R_REMOTE_ENV : */RUI.IMG_OBJ_R_RUNTIME_ENV ));
		cell.setText(rEnv.getName());
	}
	
	protected void update(final ViewerCell cell, final REnvConfiguration rEnvConfig) {
		cell.setImage(RUI.getImage((rEnvConfig.isRemote()) ?
				RUI.IMG_OBJ_R_REMOTE_ENV : RUI.IMG_OBJ_R_RUNTIME_ENV ));
		cell.setText(rEnvConfig.getName());
	}
	
	protected void update(final ViewerCell cell, final RLibGroup libGroup) {
		cell.setImage(RUI.getImage(RUI.IMG_OBJ_LIBRARY_GROUP));
		cell.setText(libGroup.getLabel());
	}
	
	protected void update(final ViewerCell cell, final RLibLocation libLocation) {
		cell.setImage(RUI.getImage(RUI.IMG_OBJ_LIBRARY_LOCATION));
		cell.setText(getSafeLabel(libLocation));
	}
	
	protected void update(final ViewerCell cell, final RPkg pkg) {
		cell.setImage(null);
		cell.setText(pkg.getName());
	}
	
}
