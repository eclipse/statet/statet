/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.text.rd;


/**
 * Keys of TextTokens recognized by text-parser (syntax-highlighting)
 */
public interface RdTextTokens {
	
	public static final String ROOT = "text_Rd_"; //$NON-NLS-1$
	
	public static final String DEFAULT = ROOT+"rdDefault"; //$NON-NLS-1$
	public static final String VERBATIM = ROOT+"rdVerbatim"; //$NON-NLS-1$
	public static final String COMMENT = ROOT+"rdComment"; //$NON-NLS-1$
	public static final String PLATFORM_SPECIF = ROOT+"rdPlatformSpecif"; //$NON-NLS-1$
	
	public static final String SECTION_TAG = ROOT+"rdSectionTag"; //$NON-NLS-1$
	public static final String SUBSECTION_TAG = ROOT+"rdSubSectionTag"; //$NON-NLS-1$
	public static final String OTHER_TAG = ROOT+"rdOtherTag"; //$NON-NLS-1$
	public static final String UNLISTED_TAG = ROOT+"rdUnlistedTag"; //$NON-NLS-1$
	
	public static final String BRACKETS = "rdBrackets"; //$NON-NLS-1$
	
	public static final String TASK_TAG = ROOT+"taskTag"; //$NON-NLS-1$
	
}
