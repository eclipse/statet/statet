/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.launching.ui;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.EnvironmentTab;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;


public class EnvironmentTabForR extends EnvironmentTab {
	
	
	private static final String VAR_R_KEEP_PKG_SOURCE = "R_KEEP_PKG_SOURCE"; //$NON-NLS-1$
	
	
	public EnvironmentTabForR() {
	}
	
	
	@Override
	protected void handleTableSelectionChanged(final SelectionChangedEvent event) {
		super.handleTableSelectionChanged(event);
		final IStructuredSelection selection = (IStructuredSelection) this.environmentTable.getSelection();
		if (selection.size() > 0) {
			for (final Iterator<?> iter = selection.iterator(); iter.hasNext(); ) {
				final Object variable = iter.next();
				if (variable != null && VAR_R_KEEP_PKG_SOURCE.equals(variable.toString())) {
					this.envRemoveButton.setEnabled(false);
				}
			}
		}
	}
	
	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
		super.setDefaults(configuration);
		final Map<String, String> map= new HashMap<>();
		map.put(VAR_R_KEEP_PKG_SOURCE, "yes"); //$NON-NLS-1$
		configuration.setAttribute(ILaunchManager.ATTR_ENVIRONMENT_VARIABLES, map);
	}
	
}
