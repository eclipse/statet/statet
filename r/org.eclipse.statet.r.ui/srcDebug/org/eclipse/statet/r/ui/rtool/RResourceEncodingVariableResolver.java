/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rtool;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.ui.editors.text.IEncodingSupport;

import org.eclipse.statet.ecommons.resources.core.variables.ResourceVariableResolver;
import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.workbench.workspace.ResourceVariableUtil;


public class RResourceEncodingVariableResolver extends ResourceVariableResolver {
	
	
	public RResourceEncodingVariableResolver(final ResourceVariableUtil util) {
		super(util);
	}
	
	
	@Override
	public String resolveValue(final IDynamicVariable variable, final String argument) throws CoreException {
		final String encoding = getEncoding(variable, getResource(variable, RESOURCE, argument));
		return (encoding != null) ? encoding : "unknown";
	}
	
	
	protected String getEncoding(final IDynamicVariable variable, final IResource resource)
			throws CoreException {
		final Context context= getContext();
		final IAdaptable adaptable= (context instanceof ResourceVariableUtil) ?
				((ResourceVariableUtil) context).getWorkbenchPart() :
				UIAccess.getActiveWorkbenchPart(false);
		if (adaptable != null) {
			final IEncodingSupport encodingSupport = adaptable
					.getAdapter(IEncodingSupport.class);
			if (encodingSupport != null) {
				return encodingSupport.getEncoding();
			}
		}
		if (resource instanceof IFile) {
			return ((IFile) resource).getCharset(true); 
		}
		if (resource instanceof IContainer) {
			return ((IContainer) resource).getDefaultCharset(true);
		}
		return null;
	}
	
}
