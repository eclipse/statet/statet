/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rtool;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;

import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.models.core.util.ElementPartition;
import org.eclipse.statet.ecommons.ui.util.DNDUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.util.ViewActionUtil;


@NonNullByDefault
public class CopyRElementHandler extends AbstractHandler {
	
	
	private final ViewActionUtil actionUtil;
	
	private final ILabelProvider labelProvider;
	
	
	public CopyRElementHandler(final ViewActionUtil actionUtil, final ILabelProvider labelProvider) {
		this.actionUtil= actionUtil;
		this.labelProvider= labelProvider;
	}
	
	
	private IStructuredSelection getSelection() {
		final ISelectionProvider selectionProvider= this.actionUtil.getSelectionProvider();
		return (IStructuredSelection) selectionProvider.getSelection();
	}
	
	protected boolean isValidSelection(final IStructuredSelection selection) {
		if (selection == null || selection.isEmpty()) {
			return false;
		}
		for (final Object element : selection.toList()) {
			if (element instanceof ElementPartition) {
				return false;
			}
		}
		return true;
	}
	
	private @Nullable String getText(final Object element) {
		return this.labelProvider.getText(element);
	}
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		setBaseEnabled(isValidSelection(getSelection()));
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		if (!UIAccess.isOkToUse(this.actionUtil.getControl())) {
			return null;
		}
		final IStructuredSelection selection= getSelection();
		if (!isValidSelection(selection)) {
			return null;
		}
		
		final String text= createData(selection);
		if (text != null) {
			copy(text);
		}
		
		return null;
	}
	
	protected @Nullable String createData(final IStructuredSelection selection) {
		final List<String> texts= new ArrayList<>(selection.size());
		for (final Object element : selection.toList()) {
			texts.add(nonNullElse(getText(element), "-")); //$NON-NLS-1$
		}
		return (texts.size() == 1) ?
				texts.get(0) : CollectionUtils.toString(texts, ", "); //$NON-NLS-1$
	}
	
	private void copy(final String text) {
		DNDUtils.setContent(this.actionUtil.getClipboard(),
				new String[] { text },
				new Transfer[] { TextTransfer.getInstance() } );
	}
	
}
