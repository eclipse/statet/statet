/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rtool;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.core.variables.IDynamicVariableResolver;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;

import org.eclipse.statet.ecommons.text.core.util.TextUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.internal.r.ui.rtools.Messages;
import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.ui.ElementNameProvider;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.ui.RUI;
import org.eclipse.statet.r.ui.editors.RSourceEditor;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


public class RElementNameVariableResolver implements IDynamicVariableResolver {
	
	
	public static final String R_OBJECT_NAME_NAME = "r_object_name"; //$NON-NLS-1$
	
	
	public RElementNameVariableResolver() {
	}
	
	
	@Override
	public String resolveValue(final IDynamicVariable variable, final String argument)
			throws CoreException {
		final IWorkbenchPart part = UIAccess.getActiveWorkbenchPart(false);
		if (part != null) {
			final IWorkbenchPartSite site = part.getSite();
			final ISelectionProvider selectionProvider = site.getSelectionProvider();
			if (selectionProvider != null) {
				final ISelection selection = selectionProvider.getSelection();
				
				if (selection instanceof final IStructuredSelection structuredSelection) {
					if (selection.isEmpty()) {
						throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID,
								NLS.bind(Messages.Variable_error_EmptySelection_message,
										R_OBJECT_NAME_NAME )));
					}
					if (structuredSelection.size() == 1) {
						final Object element = structuredSelection.getFirstElement();
						if (element instanceof RElement) { 
							final ElementName elementName;
							if (selection instanceof ElementNameProvider) {
								elementName = ((ElementNameProvider) selection).getElementName(
										(selection instanceof ITreeSelection) ?
												((ITreeSelection) selection).getPaths()[0] :
												element );
							}
							else {
								elementName = ((RElement)element).getElementName();
							}
							return checkName(elementName);
						}
					}
					final LtkModelElement[] elements = LTKSelectionUtils.getSelectedElements(
							(IStructuredSelection) selection );
					if (elements != null && elements.length == 1
							&& elements[0].getModelTypeId() == RModel.R_TYPE_ID) {
						return checkName(elements[0].getElementName());
					}
					throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID,
							NLS.bind(Messages.Variable_error_NoSingleRElement_message,
									R_OBJECT_NAME_NAME )));
				}
			}
			
			final SourceEditor editor= part.getAdapter(SourceEditor.class);
			if (editor instanceof RSourceEditor) {
				try {
					final RSourceEditor rEditor= (RSourceEditor) editor;
					final Point range = rEditor.getViewer().getSelectedRange();
					final String contentType= TextUtils.getContentType(
							rEditor.getViewer().getDocument(), rEditor.getDocumentContentInfo(),
							range.x, (range.y == 0) );
					final RAssistInvocationContext context= new RAssistInvocationContext(rEditor,
							new Region(range.x, range.y), contentType, null, null);
					return checkName(context.getNameSelection());
				}
				catch (final BadPartitioningException | BadLocationException e) {
					RUIPlugin.logUncriticalError(e);
				}
			}
		}
		throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID,
				NLS.bind(Messages.Variable_error_NoSingleRElement_message,
						R_OBJECT_NAME_NAME )));
	}
	
	private String checkName(final ElementName elementName) throws CoreException {
		if (elementName instanceof RElementName) {
			final String name = ((RElementName) elementName).getDisplayName(
					(RElementName.DISPLAY_FQN | RElementName.DISPLAY_EXACT) );
			if (name != null) {
				return name;
			}
		}
		throw new CoreException(new Status(IStatus.ERROR, RUI.BUNDLE_ID,
				NLS.bind(Messages.Variable_error_InvalidObject_QualifiedName_message,
						R_OBJECT_NAME_NAME )));
	}
	
}
