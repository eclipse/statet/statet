/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.ui.rtool;

import org.eclipse.jface.util.TransferDragSourceListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RElementViewerDragSourceListener implements TransferDragSourceListener {
	
	
	private final CopyRElementHandler commandHandler;
	
	private final StructuredViewer viewer;
	
	private String text= ""; //$NON-NLS-1$
	
	
	public RElementViewerDragSourceListener(final CopyRElementHandler commandHandler, 
			final StructuredViewer viewer) {
		this.commandHandler= commandHandler;
		this.viewer= viewer;
	}
	
	
	@Override
	public Transfer getTransfer() {
		return TextTransfer.getInstance();
	}
	
	private ITreeSelection getSelection() {
		return (ITreeSelection) this.viewer.getSelection();
	}
	
	@Override
	public void dragStart(final DragSourceEvent event) {
		final IStructuredSelection selection= getSelection();
		if (!this.commandHandler.isValidSelection(selection)) {
			event.doit= false;
			return;
		}
		
		final String text= this.commandHandler.createData(selection);
		if (text != null) {
			this.text= text;
		}
		else {
			event.doit= false;
		}
	}
	
	@Override
	public void dragSetData(final DragSourceEvent event) {
		event.data= this.text;
	}
	
	@Override
	public void dragFinished(final DragSourceEvent event) {
		this.text= ""; //$NON-NLS-1$
	}
	
}
