/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.launcher;

import java.util.Map;

import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.internal.r.debug.ui.RLaunchingMessages;


/**
 * Launch shortcut, which submits 
 *  - the current line/selection directly to R (text editor)
 * and does not change the focus by default.
 * 
 * Low requirements: ITextSelection is sufficient
 */
public class SubmitSelectionForTextHandler extends SubmitSelectionHandler implements IElementUpdater {
	
	
	public static class AndGotoConsole extends SubmitSelectionForTextHandler {
		
		
		public AndGotoConsole() {
			super(true);
		}
		
		
		@Override
		protected String appendVariant(final String label) {
			return label + RLaunchingMessages.SubmitCode_GotoConsole_affix;
		}
		
	}
	
	
	public SubmitSelectionForTextHandler() {
		this(false);
	}
	
	protected SubmitSelectionForTextHandler(final boolean gotoConsole) {
		super(gotoConsole);
	}
	
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
		try {
			element.setText(appendVariant(RLaunchingMessages.SubmitCode_TextSelection_label));
		}
		finally {
			WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
		}
	}
	
	protected String appendVariant(final String label) {
		return label;
	}
	
}
