/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.launcher;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.AbstractTextEditor;

import org.eclipse.statet.internal.r.debug.ui.RLaunchingMessages;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;
import org.eclipse.statet.ltk.ui.util.LTKWorkbenchUIUtil;
import org.eclipse.statet.r.launching.ICodeSubmitContentHandler;
import org.eclipse.statet.r.launching.RCodeLaunching;


/**
 * Launch shortcut, which submits the whole script directly to R
 * and does not change the focus.
 */
public class SubmitFileDirectLaunchShortcut implements ILaunchShortcut {
	
	
	private final boolean fGotoConsole;
	
	
	public SubmitFileDirectLaunchShortcut() {
		this(false);
	}
	
	protected SubmitFileDirectLaunchShortcut(final boolean gotoConsole) {
		fGotoConsole = gotoConsole;
	}
	
	
	@Override
	public void launch(final ISelection selection, final String mode) {
		assert mode.equals("run"); //$NON-NLS-1$
		
		try {
			final IFile[] files = LTKSelectionUtils.getSelectedFiles(selection);
			if (files != null) {
				final int last = files.length-1;
				for (int i = 0; i <= last; i++) {
					final List<String> lines = LaunchShortcutUtil.getCodeLines(files[i]);
					
					RCodeLaunching.runRCodeDirect(lines, (i == last) && fGotoConsole, null);
				}
				return;
			}
			
			LaunchShortcutUtil.handleUnsupportedExecution(null);
		}
		catch (final Exception e) {
			LaunchShortcutUtil.handleRLaunchException(e,
					RLaunchingMessages.RScriptLaunch_error_message, null);
		}
	}
	
	@Override
	public void launch(final IEditorPart editor, final String mode) {
		assert mode.equals("run"); //$NON-NLS-1$
		
		try {
			if (editor instanceof final AbstractTextEditor redt) {
				final ICodeSubmitContentHandler handler = RCodeLaunching.getCodeSubmitContentHandler(
						LTKWorkbenchUIUtil.getContentTypeId(redt) );
				final IDocument document = redt.getDocumentProvider().getDocument(
						editor.getEditorInput() );
				final List<String> lines = handler.getCodeLines(document);
				
				RCodeLaunching.runRCodeDirect(lines, fGotoConsole, null);
				return;
			}
			
			LaunchShortcutUtil.handleUnsupportedExecution(null);
		}
		catch (final Exception e) {
			LaunchShortcutUtil.handleRLaunchException(e,
					RLaunchingMessages.RScriptLaunch_error_message, null);
		}
	}
	
}
