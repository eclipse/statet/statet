/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.launcher;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.ecommons.text.TextUtil;

import org.eclipse.statet.r.launching.ICodeSubmitContentHandler;


/**
 * Handler for R script files.
 */
public class DefaultContentHandler implements ICodeSubmitContentHandler {
	
	
	public DefaultContentHandler() {
	}
	
	
	@Override
	public void setup(final IDocument document) {
	}
	
	@Override
	public List<String> getCodeLines(final IDocument document)
			throws BadLocationException, CoreException {
		final ArrayList<String> lines= new ArrayList<>(document.getNumberOfLines() + 1);
		
		TextUtil.addLines(document, 0, document.getLength(), lines);
		
		return lines;
	}
	
	@Override
	public List<String> getCodeLines(final IDocument document, final int offset, final int length)
			throws CoreException, BadLocationException {
		final ArrayList<String> lines= new ArrayList<>(
				document.getNumberOfLines(0, length) + 1 );
		
		TextUtil.addLines(document, offset, length, lines);
		
		return lines;
	}
	
}
