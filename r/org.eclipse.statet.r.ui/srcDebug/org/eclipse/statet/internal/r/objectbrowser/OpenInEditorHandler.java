/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.objectbrowser;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.ui.dataeditor.RDataEditor;
import org.eclipse.statet.r.ui.dataeditor.RLiveDataEditorInput;
import org.eclipse.statet.r.ui.util.RElementInputContentProvider;
import org.eclipse.statet.rj.ts.core.RTool;


@NonNullByDefault
public class OpenInEditorHandler extends AbstractHandler {
	
	
	public OpenInEditorHandler() {
	}
	
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		final IWorkbenchPart activePart= WorkbenchUIUtils.getActivePart(evaluationContext);
		if (activePart instanceof final ObjectBrowserView browser) {
			final ToolProcess tool= browser.getTool();
			final ITreeSelection selection= browser.getSelection();
			if (tool != null && !tool.isTerminated()
					&& selection.size() == 1) {
				final CombinedRElement rElement= RElementInputContentProvider.getCombinedRElement(selection.getFirstElement());
				setBaseEnabled(rElement != null
						&& RLiveDataEditorInput.isSupported(rElement) );
				return;
			}
		}
		setBaseEnabled(false);
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPart activePart= WorkbenchUIUtils.getActivePart(event.getApplicationContext());
		if (activePart instanceof final ObjectBrowserView browser) {
			final RTool tool= browser.getTool();
			final ITreeSelection selection= browser.getSelection();
			if (tool != null && selection.size() == 1) {
				final RElementName elementName= browser.getFQElementName(selection.getPaths()[0]);
				
				RDataEditor.open(browser.getSite().getPage(), tool, elementName, null);
				return null;
			}
		}
		return null;
	}
	
}
