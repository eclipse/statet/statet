/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.objectbrowser;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.r.console.core.RProcessREnvironment;
import org.eclipse.statet.r.core.model.rlang.RLangElement;
import org.eclipse.statet.r.ui.util.RElementInput;


@NonNullByDefault
class ContentInput extends RElementInput<RProcess> {
	
	
	List<? extends RProcessREnvironment> searchEnvirs;
	
	private final boolean filterUserspace;
	
	
	public ContentInput(final RProcess source,
			final boolean filterUserspace,
			final @Nullable LtkModelElementFilter<? super RLangElement> envFilter,
			final @Nullable LtkModelElementFilter<? super RLangElement> otherFilter) {
		super(source, envFilter, otherFilter);
		
		this.filterUserspace= filterUserspace;
	}
	
	
	public boolean isFilterUserspace() {
		return this.filterUserspace;
	}
	
}
