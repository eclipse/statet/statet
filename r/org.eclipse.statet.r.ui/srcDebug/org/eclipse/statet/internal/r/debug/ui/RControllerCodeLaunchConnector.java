/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.texteditor.IEditorStatusLine;

import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;
import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolSessionData;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.console.NIConsole;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.launching.IRCodeSubmitConnector;


/**
 * Connector for NICO consoles.
 */
public class RControllerCodeLaunchConnector implements IRCodeSubmitConnector {
	
	
	public static final String ID= "org.eclipse.statet.r.rCodeLaunchConnector.RNewConsoleConnector"; //$NON-NLS-1$
	
	
	public static interface CommandsCreator {
		
		Status submitTo(final ToolController controller);
		
	}
	
	
	@Override
	public boolean submit(final List<String> lines, final boolean gotoConsole) throws CoreException {
		return submit(new CommandsCreator() {
			@Override
			public Status submitTo(final ToolController controller) {
				return controller.submit(lines, SubmitType.EDITOR);
			}
		}, gotoConsole);
	}
	
	public boolean submit(final CommandsCreator rCommands, final boolean gotoConsole) throws CoreException {
		final WorkbenchToolSessionData session= UIAccess.getDisplay().syncCall(() -> {
			final IWorkbenchPage page= UIAccess.getActiveWorkbenchPage(true);
			return NicoUI.getToolRegistry().getActiveToolSession(page);
		});
		final ToolController controller= NicoUITools.accessController(RConsoleTool.TYPE, session.getTool());
		final Status status= rCommands.submitTo(controller);
		if (status.getSeverity() >= Status.ERROR) {
			throw new CoreException(StatusUtils.convert(status));
		}
		final NIConsole console= NicoUITools.getConsole(session);
		if (console != null) {
			NicoUITools.showConsole(console, session.getPage(), gotoConsole);
		}
		return true;
	}
	
	@Override
	public void gotoConsole() throws CoreException {
		final IWorkbenchPage page= UIAccess.getActiveWorkbenchPage(true);
		final WorkbenchToolSessionData session= NicoUI.getToolRegistry().getActiveToolSession(page);
		final NIConsole console= NicoUITools.getConsole(session);
		if (console != null) {
			NicoUITools.showConsole(console, page, true);
			return;
		}
		else {
			// TODO Move to registry actions/throw exceptions
			final IWorkbenchPart part = page.getActivePart();
			if (part != null) {
				final IEditorStatusLine statusLine = part.getAdapter(IEditorStatusLine.class);
				if (statusLine != null) {
					statusLine.setMessage(true, RLaunchingMessages.SubmitCode_error_NoRSession_message, null);
				}
			}
			Display.getCurrent().beep();
		}
	}
	
}
