/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.r.launching.RCodeLaunching;


public class GotoRConsole implements IHandler {
	
	
	public GotoRConsole() {
	}
	
	
	@Override
	public void dispose() {
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		try {
			RCodeLaunching.gotoRConsole();
		}
		catch (final CoreException e) {
			throw new ExecutionException("Error occured when Goto R Console", e);
		}
		return null;
	}
	
	
	@Override
	public boolean isEnabled() {
		return true;
	}
	
	@Override
	public boolean isHandled() {
		return true;
	}
	
	@Override
	public void addHandlerListener(final IHandlerListener handlerListener) {
	}
	
	@Override
	public void removeHandlerListener(final IHandlerListener handlerListener) {
	}
	
}
