/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.launcher;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.ui.RUI;


/**
 * Launch shortcut, which submits the commands (touched by selection)
 * and goes to next commands.
 * 
 * Supports only text editors and R doc (with AST).
 */
public class SubmitEntireCommandAndGotoNextCommandHandler extends SubmitEntireCommandHandler {
	
	
	public SubmitEntireCommandAndGotoNextCommandHandler() {
		super(false);
	}
	
	
	@Override
	protected void postLaunch(final Data data) {
		try {
			final ImList<RAstNode> nodes= data.nodes;
			final int offset= getNextOffset(nodes.getLast(), data.document);
			UIAccess.getDisplay().asyncExec(new Runnable() {
				@Override
				public void run() {
					data.editor.selectAndReveal(offset, 0);
				}
			});
		}
		catch (final BadLocationException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, RUI.BUNDLE_ID, -1, "Error occurred when updating selection", null)); //$NON-NLS-1$
		}
	}
	
	protected int getNextOffset(RAstNode node, final IDocument doc) throws BadLocationException {
		RAstNode parent;
		while ((parent = node.getRParent()) != null) {
			final int lastIdx = parent.getChildIndex(node);
			if (lastIdx + 1 < parent.getChildCount()) {
				return parent.getChild(lastIdx + 1).getStartOffset();
			}
			node = parent;
		}
		final int line = doc.getLineOfOffset(node.getEndOffset());
		if (line + 1 < doc.getNumberOfLines()) {
			return doc.getLineOffset(line + 1);
		}
		return doc.getLength();
	}
	
}
