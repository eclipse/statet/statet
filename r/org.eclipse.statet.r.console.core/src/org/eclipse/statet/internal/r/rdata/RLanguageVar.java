/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.rdata;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.ExternalizableRObject;
import org.eclipse.statet.rj.data.impl.RLanguageImpl;


@NonNullByDefault
public class RLanguageVar extends BasicCombinedRElement
		implements RLanguage, ExternalizableRObject {
	
	
	private final byte type;
	
	private final String className1;
	
	
	public RLanguageVar(final RLanguage org,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name) {
		super(parent, name);
		
		this.type= org.getLanguageType();
		this.className1= org.getRClassName();
	}
	
	public RLanguageVar(final RJIO io, final RObjectFactory factory,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name)
			throws IOException {
		super(parent, name);
		
		//-- options
		final int options= io.readInt();
		//-- special attributes
		this.type= io.readByte();
		this.className1= ((options & RObjectFactory.O_CLASS_NAME) != 0) ?
				io.readString() :
				RLanguageImpl.getBaseClassname(this.type);
		//-- data
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		int options= 0;
		if (!this.className1.equals(RLanguageImpl.getBaseClassname(this.type))) {
			options |= RObjectFactory.O_CLASS_NAME;
		}
		io.writeInt(options);
		io.writeByte(this.type);
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			io.writeString(this.className1);
		}
		//-- data
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_LANGUAGE;
	}
	
	@Override
	public byte getLanguageType() {
		return this.type;
	}
	
	@Override
	public String getRClassName() {
		return this.className1;
	}
	
	
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public @Nullable String getSource() {
		return null;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
	
	@Override
	public int getElementType() {
		return R_GENERAL_VARIABLE;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter filter) {
		return false;
	}
	
	@Override
	public List<? extends CombinedRElement> getModelChildren(final @Nullable LtkModelElementFilter filter) {
		return Collections.emptyList();
	}
	
}
