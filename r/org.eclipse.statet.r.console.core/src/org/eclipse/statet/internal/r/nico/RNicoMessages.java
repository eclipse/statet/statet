/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.nico;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class RNicoMessages extends NLS {
	
	
	public static String Rterm_StartTask_label;
	public static String RTerm_CancelTask_label;
	public static String RTerm_CancelTask_SendSignal_label;
	public static String RTerm_error_Starting_message;
	
	public static String R_Info_Started_message;
	public static String R_Info_Reconnected_message;
	public static String R_Info_Disconnected_message;
	public static String R_Info_ConnectionLost_message;
	public static String R_Info_Stopped_message;
	
	
	static {
		NLS.initializeMessages(RNicoMessages.class.getName(), RNicoMessages.class);
	}
	private RNicoMessages() {}
	
}
