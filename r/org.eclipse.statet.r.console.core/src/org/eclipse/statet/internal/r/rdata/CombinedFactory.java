/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.rdata;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;


@NonNullByDefault
public class CombinedFactory extends DefaultRObjectFactory {
	
	
	public static final String FACTORY_ID= "combined"; //$NON-NLS-1$
	public static final CombinedFactory INSTANCE= new CombinedFactory();
	
	
	@Override
	public @Nullable BasicCombinedRElement readObject(final RJIO io) throws IOException {
		assert ((io.flags & F_ONLY_STRUCT) != 0);
		return readObject(io, null, null);
	}
	
	public @Nullable BasicCombinedRElement readObject(final RJIO io,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name)
			throws IOException {
		final byte type= io.readByte();
		int options;
		switch (type) {
		case -1:
			return null;
		case RObject.TYPE_NULL:
			return new RNullVar(parent, name);
		case RObject.TYPE_VECTOR: {
			return new RVectorVar<>(io, this, parent, name); }
		case RObject.TYPE_ARRAY:
			return new RArrayVar<>(io, this, parent, name);
		case RObject.TYPE_LIST:
			options= io.readInt();
			return new RListVar(io, this, options, parent, name);
		case RObject.TYPE_DATAFRAME:
			options= io.readInt();
			return new RDataFrameVar(io, this, options, parent, name);
		case RObject.TYPE_ENVIRONMENT:
			return new REnvironmentVar(io, this, parent, name);
		case RObject.TYPE_LANGUAGE:
			return new RLanguageVar(io, this, parent, name);
		case RObject.TYPE_FUNCTION:
			return new RFunction2(io, this, parent, name);
		case RObject.TYPE_REFERENCE:
			return new RReferenceVar(io, this, parent, name);
		case RObject.TYPE_S4OBJECT:
			return new RS4ObjectVar(io, this, parent, name);
		case RObject.TYPE_OTHER:
			return new ROtherVar(io, this, parent, name);
		case RObject.TYPE_MISSING:
			return new RMissingVar(parent, name);
		case RObject.TYPE_PROMISE:
			return new RPromiseVar(parent, name);
		default:
			throw new IOException("object type= " + type);
		}
	}
	
	@Override
	public RList readAttributeList(final RJIO io) throws IOException {
		return new RListVar(io, this, io.readInt(), null, null);
	}
	
}
