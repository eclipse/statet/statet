/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.rdata;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.ExternalizableRObject;
import org.eclipse.statet.rj.data.impl.RInteger32Store;


public final class RArrayVar<TData extends RStore> extends BasicCombinedRElement
		implements RArray<TData>, ExternalizableRObject {
	
	
	private final long length;
	private final TData data;
	
	private String className1;
	private final RInteger32Store dimAttribute;
	
	
	public RArrayVar(final TData data, final String className1, final int[] dim,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name) {
		super(parent, name);
		if (data == null || className1 == null || dim == null) {
			throw new NullPointerException();
		}
		this.length= RDataUtils.computeLengthFromDim(dim);
		if (data.getLength() >= 0 && data.getLength() != this.length) {
			throw new IllegalArgumentException("dim");
		}
		this.className1= className1;
		this.dimAttribute= new RInteger32Store(dim);
		this.data= data;
	}
	
	public RArrayVar(final RJIO io, final RObjectFactory factory,
			final @Nullable BasicCombinedRElement parent, final @Nullable RElementName name)
			throws IOException {
		super(parent, name);
		
		//-- options
		final int options= io.readInt();
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			this.className1= io.readString();
		}
		this.length= io.readVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK));
		final int[] dim= io.readIntArray();
		this.dimAttribute= new RInteger32Store(dim);
		assert ((options & RObjectFactory.O_WITH_NAMES) == 0);
		//-- data
		this.data= (TData) factory.readStore(io, this.length);
		
		if ((options & RObjectFactory.O_CLASS_NAME) == 0) {
			this.className1= (dim.length == 2) ? RObject.CLASSNAME_MATRIX : RObject.CLASSNAME_ARRAY;
		}
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		final int n= (int) this.dimAttribute.getLength();
		//-- options
		int options= io.getVULongGrade(this.length);
		if (!this.className1.equals((n == 2) ?
				RObject.CLASSNAME_MATRIX : RObject.CLASSNAME_ARRAY )) {
			options |= RObjectFactory.O_CLASS_NAME;
		}
		io.writeInt(options);
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			io.writeString(this.className1);
		}
		io.writeVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK), this.length);
		io.writeInt(n);
		this.dimAttribute.writeExternal(io);
		//-- data
		factory.writeStore(this.data, io);
	}
	
	
	@Override
	public final byte getRObjectType() {
		return TYPE_ARRAY;
	}
	
	@Override
	public String getRClassName() {
		return this.className1;
	}
	
	@Override
	public long getLength() {
		return this.length;
	}
	
	@Override
	public RIntegerStore getDim() {
		return this.dimAttribute;
	}
	
	@Override
	public @Nullable RCharacterStore getDimNames() {
		return null;
	}
	
	@Override
	public @Nullable RStore getNames(final int dim) {
		return null;
	}
	
	
	@Override
	public TData getData() {
		return this.data;
	}
	
	
	@Override
	public int getElementType() {
		return R_GENERAL_VARIABLE;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter filter) {
		return false;
	}
	
	@Override
	public List<? extends CombinedRElement> getModelChildren(final @Nullable LtkModelElementFilter filter) {
		return Collections.emptyList();
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("RObject type=array, class=").append(getRClassName());
		sb.append("\n\tlength=").append(getLength());
		sb.append("\n\tdim=");
		this.dimAttribute.appendTo(sb);
		sb.append("\n\tdata: ");
		sb.append(this.data.toString());
		return sb.toString();
	}
	
}
