/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.rdata;

import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;


@NonNullByDefault
public abstract class BasicCombinedRElement implements CombinedRElement {
	
	
	private @Nullable BasicCombinedRElement parent;
	
	private RElementName elementName;
	
	
	protected BasicCombinedRElement(final @Nullable BasicCombinedRElement parent,
			final @Nullable RElementName name) {
		this.parent= parent;
		this.elementName= name;
	}
	
	
	@Override
	public @Nullable RList getAttributes() {
		return null;
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public @Nullable String getId() {
		return null; // not yet implemented
	}
	
	@Override
	public final RElementName getElementName() {
		return this.elementName;
	}
	
	/** Internal */
	public final void setElementName(final RElementName name) {
		this.elementName= name;
	}
	
	@Override
	public boolean exists() {
		return true;
	}
	
	@Override
	public boolean isReadOnly() {
		return true;
	}
	
	/** Internal */
	public void setParent(final BasicCombinedRElement parent) {
		this.parent= parent;
	}
	
	@Override
	public final @Nullable BasicCombinedRElement getModelParent() {
		return this.parent;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == LtkModelElement.class) {
			return (T) this;
		}
		if (adapterType == RObject.class) {
			return (T) this;
		}
		return null;
	}
	
	
	@Override
	public final int hashCode() {
		if (this.parent != null) {
			return singleHash() - this.parent.singleHash();
		}
		return singleHash();
	}
	
	protected int singleHash() {
		return this.elementName.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof BasicCombinedRElement) {
			final CombinedRElement other= (CombinedRElement) obj;
			return (this.elementName.equals(other.getElementName())
					&& Objects.equals(this.parent, other.getModelParent()) );
		}
		return false;
	}
	
}
