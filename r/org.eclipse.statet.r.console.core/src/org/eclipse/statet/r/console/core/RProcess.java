/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.core;

import com.ibm.icu.text.DateFormat;

import org.eclipse.debug.core.ILaunch;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.r.core.renv.IREnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;


/**
 * Process (in the Eclipse debug framework) for an R instance.
 */
public class RProcess extends ToolProcess implements RConsoleTool {
	
	
	private final @Nullable IREnvConfiguration rEnvConfig;
	
	
	/**
	 * Creates an new R process handle.
	 * 
	 * @param launch
	 * @param rEnvConfig
	 * @param labelPrefix
	 * @param name
	 * @param address
	 * @param wd
	 * @param timestamp
	 */
	public RProcess(final ILaunch launch, final @Nullable IREnvConfiguration rEnvConfig,
			final String labelPrefix, final String name,
			final String address, final String wd, final long timestamp) {
		super(launch, RConsoleTool.TYPE, labelPrefix, name,
				address, wd, timestamp);
		this.rEnvConfig= rEnvConfig;
	}
	
	
	@Override
	public @Nullable REnv getREnv() {
		return (this.rEnvConfig != null) ? this.rEnvConfig.getREnv() : null;
	}
	
	@Override
	public RWorkspace getWorkspace() {
		return (RWorkspace) super.getWorkspace();
	}
	
	@Override
	public String createTimestampComment(final long timestamp) {
		final String datetime= DateFormat.getDateTimeInstance().format(timestamp);
		// default R format (R: timestamp())
		return "##------ " + datetime + " ------##\n";
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == REnv.class) {
			return (T) getREnv();
		}
		if (adapterType == REnvConfiguration.class || adapterType == IREnvConfiguration.class) {
			return (T) this.rEnvConfig;
		}
		return super.getAdapter(adapterType);
	}
	
}
