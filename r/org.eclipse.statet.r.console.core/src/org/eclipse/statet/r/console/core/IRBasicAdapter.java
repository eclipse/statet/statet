/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.core;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.nico.core.runtime.ConsoleService;
import org.eclipse.statet.nico.core.runtime.ToolController;


/**
 * Interface to access R by a ToolRunnable.
 */
public interface IRBasicAdapter extends ConsoleService {
	
	
	/**
	 * This flag indicates that the current input is incomplete.
	 * 
	 * The prompt have to be a instance of {@link IncompleteInputPrompt<RunnableAdapterType, WorkspaceType>}.
	 */
	int META_PROMPT_INCOMPLETE_INPUT = 1 << 8;
	
	
	@Override
	RProcess getTool();
	
	@Override
	ToolController getController();
	
	@Override
	RWorkspace getWorkspace();
	
	/**
	 * Quits R.
	 * <code>q()</code>
	 * 
	 * @throws StatusException 
	 */
	void quit(final ProgressMonitor m) throws StatusException;
	
	void briefAboutToChange();
	void briefChanged(int o);
	void briefChanged(Object changed, int o);
	int getChangeStamp();
	
}
