/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.core;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.DebugEvent;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.ecommons.io.FileUtil;

import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.rlang.RLangSrcStrElement;
import org.eclipse.statet.r.nico.IRSrcref;
import org.eclipse.statet.r.nico.RSrcref;
import org.eclipse.statet.rj.server.dbg.DbgRequest;
import org.eclipse.statet.rj.server.dbg.SrcfileData;
import org.eclipse.statet.rj.server.dbg.Srcref;


/**
 * Helper for rj.server.dbg in Eclipse/StatET
 */
@NonNullByDefault
public class RDbg {
	
	
	public static String getElementId(RLangSrcStrElement element) {
		SourceStructElement parent= element.getSourceParent();
		if (!(parent instanceof RLangSrcStrElement)
				|| (parent.getElementType() & RElement.MASK_C1) == RElement.C1_SOURCE ) {
			return element.getId();
		}
		final StringBuilder sb= new StringBuilder(element.getId());
		element= (RLangSrcStrElement) parent;
		while (true) {
			sb.insert(0, '/');
			sb.insert(0, element.getId());
			parent= element.getSourceParent();
			if (!(parent instanceof RLangSrcStrElement)
					|| (parent.getElementType() & RElement.MASK_C1) == RElement.C1_SOURCE ) {
				return sb.toString();
			}
			element= (RLangSrcStrElement) parent;
		}
	}
	
	public static long getTimestamp(final SourceUnit su, final IProgressMonitor monitor) {
		if (su.isSynchronized() && su.getResource() != null) {
			final FileUtil fileUtil= FileUtil.getFileUtil(su.getResource());
			if (fileUtil != null) {
				try {
					return fileUtil.getTimeStamp(monitor);
				}
				catch (final CoreException e) {
				}
			}
		}
		return 0;
	}
	
	public static long getTimestamp(final SourceUnit su, final ProgressMonitor m) {
		if (su.isSynchronized() && su.getResource() != null) {
			final FileUtil fileUtil = FileUtil.getFileUtil(su.getResource());
			if (fileUtil != null) {
				try {
					return fileUtil.getTimeStamp(EStatusUtils.convert(m));
				}
				catch (final CoreException e) {
				}
			}
		}
		return 0;
	}
	
	public static SrcfileData createRJSrcfileData(final IResource resource) {
		final IPath location= resource.getLocation();
		return new SrcfileData(resource.getFullPath().toPortableString(),
				(location != null) ? location.toString() : null,
				resource.getLocalTimeStamp() );
	}
	
	public static int[] createRJSrcref(final IRSrcref srcref) {
		final int[] array= new int[6];
		
		if (srcref.getFirstLine() >= 0) {
			array[Srcref.BEGIN_LINE]= srcref.getFirstLine() + 1;
		}
		else {
			array[Srcref.BEGIN_LINE]= Srcref.NA;
		}
		if (srcref.getFirstColumn() >= 0) {
			array[Srcref.BEGIN_COLUMN]= srcref.getFirstColumn() + 1;
		}
		else {
			array[Srcref.BEGIN_COLUMN]= Srcref.NA;
		}
		array[Srcref.BEGIN_BYTE]= Srcref.NA;
		
		if (srcref.getLastLine() >= 0) {
			array[Srcref.END_LINE]= srcref.getLastLine() + 1;
		}
		else {
			array[Srcref.END_LINE]= Srcref.NA;
		}
		if (srcref.getLastColumn() >= 0) {
			array[Srcref.END_COLUMN]= srcref.getLastColumn() + 1;
		}
		else {
			array[Srcref.END_COLUMN]= Srcref.NA;
		}
		array[Srcref.END_BYTE]= Srcref.NA;
		
		return array;
	}
	
	public static @Nullable IRSrcref createStatetSrcref(final int @Nullable [] data) {
		if (data == null || data.length < 6) {
			return null;
		}
		return new RSrcref(
				(data[Srcref.BEGIN_LINE] > 0) ? data[Srcref.BEGIN_LINE] - 1 : -1,
				(data[Srcref.BEGIN_COLUMN] > 0) ? data[Srcref.BEGIN_COLUMN] -1 : -1,
				(data[Srcref.END_LINE] > 0) ? data[Srcref.END_LINE] - 1 : -1,
				(data[Srcref.END_COLUMN] > 0) ? data[Srcref.END_COLUMN] : -1 );
	}
	
	public static int getResumeEventDetail(final byte op) {
		switch (op) {
		case DbgRequest.RESUME:
			return DebugEvent.CLIENT_REQUEST;
		case DbgRequest.STEP_INTO:
			return DebugEvent.STEP_INTO;
		case DbgRequest.STEP_OVER:
			return DebugEvent.STEP_OVER;
		case DbgRequest.STEP_RETURN:
			return DebugEvent.STEP_RETURN;
		default:
			return DebugEvent.UNSPECIFIED;
		}
	}
	
	
	private RDbg() {
	}
	
}
