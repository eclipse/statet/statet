/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.core;

import org.eclipse.core.runtime.IAdaptable;

import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.nico.core.runtime.ToolWorkspace;
import org.eclipse.statet.rj.ts.core.RTool;


public interface RConsoleTool extends RTool, IAdaptable {
	
	
	/**
	 * Feature set for R, providing basic methods independent 
	 * of the implementation of integration in the controller.
	 * <p>
	 * Feature Set:
	 * <ul>
	 * <li>{@link ToolService} implements {@link IRBasicAdapter}</li>
	 * <li>{@link ToolWorkspace} instance of {@link RWorkspace}</li>
	 * </ul>
	 */
	public static final String R_BASIC_FEATURESET_ID = "org.eclipse.statet.r.basic"; //$NON-NLS-1$
	
	/**
	 * Feature set for R, providing methods to work directly with R data
	 * 
	 * Data are exchanged as RObject by the RJ data library. Operations usually 
	 * doesn't appears in the console output.
	 * <p>
	 * Feature Set:
	 * <ul>
	 * <li>{@link ToolService} implements {@link IRDataAdapter}</li>
	 * </ul>
	 */
	public static final String R_DATA_FEATURESET_ID = "org.eclipse.statet.r.data";
	
}
