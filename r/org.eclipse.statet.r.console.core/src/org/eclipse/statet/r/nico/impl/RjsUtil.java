/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.nico.impl;

import static org.eclipse.statet.nico.core.runtime.IToolEventHandler.LOGIN_SSH_HOST_DATA_KEY;
import static org.eclipse.statet.nico.core.runtime.IToolEventHandler.LOGIN_SSH_PORT_DATA_KEY;
import static org.eclipse.statet.nico.core.runtime.IToolEventHandler.LOGIN_USERNAME_DATA_KEY;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.security.SecureRandom;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils;
import org.eclipse.statet.jcommons.net.Port;
import org.eclipse.statet.jcommons.net.core.RSAccessClientSession;
import org.eclipse.statet.jcommons.net.core.RemoteTarget;
import org.eclipse.statet.jcommons.net.core.ssh.SshTarget;
import org.eclipse.statet.jcommons.runtime.ProcessConfigBuilder;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;

import org.eclipse.statet.ecommons.net.core.ECommonsNetCore;

import org.eclipse.statet.internal.r.console.core.RConsoleCorePlugin;
import org.eclipse.statet.rj.server.FxCallback;


@NonNullByDefault
public class RjsUtil {
	
	
	public static boolean isRSAccessSupported(final ToolCommandData connectionData) {
		final String protocol= connectionData.getString("protocol");
		return (protocol != null && protocol.equals("ssh"));
	}
	
	public static RemoteTarget getRemoteTarget(final ToolCommandData connectionData) throws StatusException {
		final String sshHost= connectionData.getStringRequired(LOGIN_SSH_HOST_DATA_KEY);
		final int sshPort= connectionData.getInt(LOGIN_SSH_PORT_DATA_KEY, -1);
		final String username= connectionData.getStringRequired(LOGIN_USERNAME_DATA_KEY);
		
		return new SshTarget(sshHost, (sshPort > 0) ? new Port(sshPort) : null, username);
	}
	
	public static RSAccessClientSession getRSAccessClientSession(final RemoteTarget target,
			final ProgressMonitor m) throws StatusException {
		return ECommonsNetCore.getSessionManager().getClientSession(target, m);
	}
	
	public static @Nullable RSAccessClientSession getRSAccessClientSession(final ToolCommandData connectionData,
			final ProgressMonitor m) throws StatusException {
		if (isRSAccessSupported(connectionData)) {
			return getRSAccessClientSession(getRemoteTarget(connectionData), m);
		}
		return null;
	}
	
	
	public static RMIClientSocketFactory createRMIOverSshClientSocketFactory(final RSAccessClientSession session) {
		return new RMIClientSocketFactory() {
			@Override
			public Socket createSocket(final @Nullable String host, final int port) throws IOException {
//				System.out.println("SshSocket new: to= " + host + ":" + port);
				final ProgressMonitor m= new NullProgressMonitor();
				try {
					return session.createDirectTcpIpSocket(new Port(port), m);
				}
				catch (final StatusException e) {
					final IOException ioException= new IOException();
					ioException.initCause(e);
					throw ioException;
				}
			}
		};
	}
	
	public static void startRemoteServerOverSsh(final RSAccessClientSession session,
			final String command, final Map<String, String> envVars,
			final ProgressMonitor m) throws StatusException {
		m.setWorkRemaining(2 + 2 + 1);
		Exception error= null;
		final ByteArrayOutputStream output= new ByteArrayOutputStream();
		try (final var remoteProcess= session.exec(new ProcessConfigBuilder()
					.setCommandString(command)
					.addEnvironmentVars(envVars)
					.setOutputStream(output)
					.build(), m.newSubMonitor(2) )) {
			final int statusCode= remoteProcess.waitFor(null, m.newSubMonitor(2) );
			
			if (statusCode != 0) {
				final var sb= new ObjectUtils.ToStringBuilder("Remote process has exit with an error:");
				sb.addProp("ExitStatusCode", statusCode);
				sb.addProp("Message", output.toString(StandardCharsets.UTF_8));
				error= new RemoteException(sb.toString());
			}
		}
		catch (final StatusException e) {
			error= e;
		}
		if (error != null) {
			throw new StatusException(new ErrorStatus(RConsoleCorePlugin.BUNDLE_ID,
					"Failed to start remote R server over SSH.", error ));
		}
	}
	
	public static void handleFxCallback(final RSAccessClientSession session, final FxCallback callback,
			final ProgressMonitor m) throws StatusException {
		final byte[] clientKey= new byte[1024];
		new SecureRandom().nextBytes(clientKey);
		final String filename= callback.getFilename();
		final byte[] content= callback.createContent(clientKey);
		
		Exception error= null;
		try (final var remoteProcess= session.exec(new ProcessConfigBuilder()
				.setCommandString("cat >> " + filename) //$NON-NLS-1$
				.setInputStream(new ByteArrayInputStream(content))
				.build(), m.newSubMonitor(1) )) {
			final int statusCode= remoteProcess.waitFor(null, m.newSubMonitor(1) );
			
			if (statusCode != 0) {
				final var sb= new ObjectUtils.ToStringBuilder("Remote process has exit with an error:");
				sb.addProp("ExitStatusCode", statusCode);
				error= new RemoteException(sb.toString());
			}
		}
		catch (final StatusException e) {
			error= e;
		}
		if (error != null) {
			throw new StatusException(new ErrorStatus(RConsoleCorePlugin.BUNDLE_ID,
					"Failed to authenticate over SSH connection.",
					error ));
		}
	}
	
	public static String getVersionString(final int @Nullable [] version) {
		if (version == null) {
			return "no version information";
		}
		if (version.length >= 3) {
			final StringBuilder sb= new StringBuilder();
			sb.append(version[0]);
			sb.append('.');
			sb.append((version[1] >= 0) ? Integer.toString(version[1]) : "x");
			sb.append('.');
			sb.append((version[2] >= 0) ? Integer.toString(version[2]) : "x");
			return sb.toString();
		}
		return "invalid version information";
	}
	
}
