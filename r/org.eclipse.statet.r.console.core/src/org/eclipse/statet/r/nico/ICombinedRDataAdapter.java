/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.nico;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.r.rdata.BasicCombinedRElement;
import org.eclipse.statet.internal.r.rdata.DirectReferenceVar;
import org.eclipse.statet.internal.r.rdata.RFunction2;
import org.eclipse.statet.internal.r.rdata.RLanguageVar;
import org.eclipse.statet.internal.r.rdata.RMissingVar;
import org.eclipse.statet.internal.r.rdata.RNullVar;
import org.eclipse.statet.internal.r.rdata.ROtherVar;
import org.eclipse.statet.internal.r.rdata.RPromiseVar;
import org.eclipse.statet.internal.r.rdata.RReferenceVar;
import org.eclipse.statet.internal.r.rdata.RVectorVar;
import org.eclipse.statet.r.console.core.IRDataAdapter;
import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.data.CombinedRReference;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.rj.data.RFunction;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RReference;
import org.eclipse.statet.rj.data.RVector;


@NonNullByDefault
public interface ICombinedRDataAdapter extends IRDataAdapter {
	
	
	static CombinedRReference createReference(final long handle, final RElementName name,
			final byte type, final String className) {
		return new RReferenceVar(handle, type, className, null, name);
	}
	
	static CombinedRReference createReference(final CombinedRElement element, final RElementName name) {
		return new DirectReferenceVar(element, null, name);
	}
	
	static CombinedRElement create(final RObject rObject,
			final RElementName name) {
		final BasicCombinedRElement parent= null;
		switch (rObject.getRObjectType()) {
		case RObject.TYPE_NULL:
			return new RNullVar(parent, name);
		case RObject.TYPE_VECTOR:
			return new RVectorVar((RVector<?>) rObject, parent, name);
//		case RObject.TYPE_ARRAY:
//		case RObject.TYPE_DATAFRAME:
//		case RObject.TYPE_LIST:
//		case RObject.TYPE_ENVIRONMENT:
//		case RObject.TYPE_S4OBJECT:
		case RObject.TYPE_LANGUAGE:
			return new RLanguageVar((RLanguage) rObject, parent, name);
		case RObject.TYPE_FUNCTION:
			return new RFunction2((RFunction) rObject, parent, name);
		case RObject.TYPE_REFERENCE:
			return new RReferenceVar((RReference) rObject, parent, name);
		case RObject.TYPE_OTHER:
			return new ROtherVar(rObject.getRClassName(), parent, name);
		case RObject.TYPE_MISSING:
			return new RMissingVar(parent, name);
		case RObject.TYPE_PROMISE:
			return new RPromiseVar(parent, name);
		default:
			throw new UnsupportedOperationException();
		}
	}
	
	
	CombinedRElement evalCombinedStruct(String command,
			int options, int depth, @Nullable RElementName name,
			final ProgressMonitor m) throws StatusException;
	
	CombinedRElement evalCombinedStruct(String command, @Nullable RObject envir,
			int options, int depth, @Nullable RElementName name,
			final ProgressMonitor m) throws StatusException;
	
	CombinedRElement evalCombinedStruct(RElementName name,
			int options, int depth,
			final ProgressMonitor m) throws StatusException;
	
	CombinedRElement evalCombinedStruct(RReference reference,
			int options, int depth, @Nullable RElementName name,
			final ProgressMonitor m) throws StatusException;
	
	@Nullable CombinedRElement findCombinedStruct(RElementName symbol,
			@Nullable RObject env, boolean inherits,
			int options, int depth,
			final ProgressMonitor m) throws StatusException;
	
	@Nullable CombinedRElement findCombinedStruct(RElementName symbol,
			@Nullable RElementName envName, boolean inherits,
			int options, int depth,
			final ProgressMonitor m) throws StatusException;
	
}
