/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.nico;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.TextUtil;


public class RSrcref implements IRSrcref {
	
	
	public static int getOffset(final AbstractDocument document, final int line, final int column)
			throws BadLocationException {
		int currentColumn = 0;
		int currentOffset = document.getLineOffset(line);
		final int l = document.getLength();
		while (currentColumn < column) {
			if (currentOffset >= l) {
				return -1;
			}
			final char c = document.getChar(currentOffset++);
			switch (c) {
			case '\n':
			case '\r':
				return -1;
			case '\t':
				currentColumn += 8 - (currentColumn % 8);
				continue;
			default:
				currentColumn++;
				continue;
			}
		}
		return currentOffset;
	}
	
	
	private final int fFirstLine;
	private final int fFirstColumn;
	private final int fLastLine;
	private final int fLastColumn;
	
	
	public RSrcref(final int beginLine, final int beginColumn, final int endLine, final int endColumn) {
		fFirstLine = beginLine;
		fFirstColumn = beginColumn;
		fLastLine = endLine;
		fLastColumn = endColumn;
	}
	
	public RSrcref(final IDocument document, final TextRegion region) throws BadLocationException {
		fFirstLine = document.getLineOfOffset(region.getStartOffset());
		fFirstColumn = TextUtil.getColumn(document, region.getStartOffset(), fFirstLine, 8);
		fLastLine = document.getLineOfOffset(region.getEndOffset());
		fLastColumn = TextUtil.getColumn(document, region.getEndOffset() - 1, fLastLine, 8); // exclusive -> inclusive
	}
	
	
	@Override
	public boolean hasBeginDetail() {
		return (fFirstLine >= 0 && fFirstColumn >= 0);
	}
	
	@Override
	public int getFirstLine() {
		return fFirstLine;
	}
	
	@Override
	public int getFirstColumn() {
		return fFirstColumn;
	}
	
	@Override
	public boolean hasEndDetail() {
		return (fLastLine >= 0 && fLastColumn >= 0);
	}
	
	@Override
	public int getLastLine() {
		return fLastLine;
	}
	
	@Override
	public int getLastColumn() {
		return fLastColumn;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(this.getClass().getName());
		sb.append("\n\t").append("firstChar= ")
				.append("in line ").append(fFirstLine >= 0 ? fFirstLine : "NA")
				.append(" at column ").append(fFirstColumn >= 0 ? fFirstColumn : "NA");
		sb.append("\n\t").append("lastChar= ")
				.append("in line ").append(fLastLine >= 0 ? fLastLine : "NA")
				.append(" at column ").append(fLastColumn >= 0 ? fLastColumn : "NA");
		return sb.toString();
	}
	
}
