<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.4"?>
<!--
 #=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<plugin>
   
<!-- breakpoints -->
   <extension
         point="org.eclipse.core.resources.markers"
         id="org.eclipse.statet.r.resourceMarkers.RBreakpoint">
      <super
            type="org.eclipse.debug.core.breakpointMarker">
      </super>
   </extension>
   <extension
         point="org.eclipse.core.resources.markers"
         id="org.eclipse.statet.r.resourceMarkers.RGenericLineBreakpoint">
      <super
            type="org.eclipse.statet.r.resourceMarkers.RBreakpoint">
      </super>
      <super
            type="org.eclipse.debug.core.lineBreakpointMarker">
      </super>
      <persistent
            value="true">
      </persistent>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.ElementTypeAttribute">
      </attribute>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.ElementIdAttribute">
      </attribute>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.ElementLabelAttribute">
      </attribute>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.SubLabelAttribute">
      </attribute>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.RExprIndexAttribute">
      </attribute>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.ConditionEnabledAttribute">
      </attribute>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.ConditionExprAttribute">
      </attribute>
   </extension>
   <extension
         point="org.eclipse.core.resources.markers"
         id="org.eclipse.statet.r.resourceMarkers.RLineBreakpoint">
      <super
            type="org.eclipse.statet.r.resourceMarkers.RGenericLineBreakpoint">
      </super>
      <persistent
            value="true">
      </persistent>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.ExpressionIndexAttribute">
      </attribute>
   </extension>
   <extension
         point="org.eclipse.core.resources.markers"
         id="org.eclipse.statet.r.resourceMarkers.RMethodBreakpoint">
      <super
            type="org.eclipse.statet.r.resourceMarkers.RGenericLineBreakpoint">
      </super>
      <persistent
            value="true">
      </persistent>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.EntryBreakpointAttribute">
      </attribute>
      <attribute
            name="org.eclipse.statet.r.resourceMarkers.ExitBreakpointAttribute">
      </attribute>
   </extension>
   <extension
         point="org.eclipse.core.resources.markers"
         id="org.eclipse.statet.r.resourceMarkers.RExceptionBreakpoint">
      <super
            type="org.eclipse.statet.r.resourceMarkers.RBreakpoint">
      </super>
      <persistent
            value="true">
      </persistent>
   </extension>
   <extension
         point="org.eclipse.debug.core.breakpoints">
      <breakpoint
            id="org.eclipse.statet.r.debugBreakpoints.RLineBreakpoint"
            class="org.eclipse.statet.internal.r.debug.core.breakpoints.LineBreakpointImpl"
            markerType="org.eclipse.statet.r.resourceMarkers.RLineBreakpoint"
            name="%breakpoints_RLineBreakpoint_name">
      </breakpoint>
      <breakpoint
            id="org.eclipse.statet.r.debugBreakpoints.RMethodBreakpoint"
            class="org.eclipse.statet.internal.r.debug.core.breakpoints.MethodBreakpointImpl"
            markerType="org.eclipse.statet.r.resourceMarkers.RMethodBreakpoint"
            name="%breakpoints_RMethodBreakpoint_name">
      </breakpoint>
      <breakpoint
            id="org.eclipse.statet.r.debugBreakpoints.RExceptionBreakpoint"
            class="org.eclipse.statet.internal.r.debug.core.breakpoints.ExceptionBreakpointImpl"
            markerType="org.eclipse.statet.r.resourceMarkers.RExceptionBreakpoint"
            name="%breakpoints_RExceptionBreakpoint_name">
      </breakpoint>
   </extension>
   <extension
         point="org.eclipse.debug.core.sourceLocators">
      <sourceLocator
            id="org.eclipse.statet.r.debugSourceLocators.RSourceLocator"
            class="org.eclipse.statet.r.debug.core.sourcelookup.RSourceLookupDirector"
            name="%sourceLocators_RSourceLocator_name">
      </sourceLocator>
   </extension>
   
<!-- expressions -->
   <extension
         point="org.eclipse.core.runtime.adapters">
      <factory
            adaptableType="org.eclipse.debug.core.model.IExpression"
            class="org.eclipse.statet.internal.r.debug.core.eval.ExpressionAdapterFactory">
         <adapter
               type="org.eclipse.statet.r.debug.core.RVariable"/>
      </factory>
   </extension>
   <extension
         point="org.eclipse.debug.core.watchExpressionDelegates">
      <watchExpressionDelegate
            debugModel="org.eclipse.statet.r.debugModels.R"
            delegateClass="org.eclipse.statet.internal.r.debug.core.eval.WatchExpressionDelegate">
      </watchExpressionDelegate>
   </extension>
   
<!-- sourcelookup -->
   <extension
         point="org.eclipse.debug.core.sourcePathComputers">
      <sourcePathComputer
            id="org.eclipse.statet.r.debugSourcePathComputers.RSourcePathComputer"
            class="org.eclipse.statet.internal.r.debug.core.sourcelookup.RSourcePathComputer">
      </sourcePathComputer>
   </extension>
   <extension
         point="org.eclipse.debug.core.sourceContainerTypes">
      <sourceContainerType
            id="org.eclipse.statet.r.debugSourceContainers.REnvLibraryPathType"
            class="org.eclipse.statet.internal.r.debug.core.sourcelookup.REnvLibraryPathSourceContainerTypeDelegate"
            name="%sourceContainers_REnvLibraryPathType_name"
            description="%sourceContainers_REnvLibraryPathType_description">
      </sourceContainerType>
      <sourceContainerType
            id="org.eclipse.statet.r.debugSourceContainers.RLibraryType"
            class="org.eclipse.statet.internal.r.debug.core.sourcelookup.RLibrarySourceContainerTypeDelegate"
            name="%sourceContainers_RLibraryType_name"
            description="%sourceContainers_RLibraryType_description">
      </sourceContainerType>
      <sourceContainerType
            id="org.eclipse.statet.r.debugSourceContainers.AllRProjectsType"
            class="org.eclipse.statet.internal.r.debug.core.sourcelookup.AllRProjectsSourceContainerTypeDelegate"
            name="%sourceContainers_AllRProjectsType_name"
            description="%sourceContainers_AllRProjectsType_description">
      </sourceContainerType>
      <sourceContainerType
            id="org.eclipse.statet.r.debugSourceContainers.RProjectType"
            class="org.eclipse.statet.internal.r.debug.core.sourcelookup.RProjectSourceContainerTypeDelegate"
            name="%sourceContainers_RProjectType_name"
            description="%sourceContainers_RProjectType_description">
      </sourceContainerType>
   </extension>
   
</plugin>
