/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.breakpoints;

import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface RMethodBreakpoint extends RLineBreakpoint {
	
	
	/**
	 * Returns whether this breakpoint causes execution to suspend on entry to methods.
	 * 
	 * @return whether suspend on entry is enabled
	 * @exception CoreException if unable to access the property from this breakpoint's underlying marker
	 */
	boolean isEntry() throws CoreException;
	
	/**
	 * Returns whether this breakpoint causes execution to suspend on exit of methods.
	 * 
	 * @return whether suspend on exit is enabled
	 * @exception CoreException if unable to access the property from this breakpoint's underlying marker
	 */
	boolean isExit() throws CoreException;	
	
	/**
	 * Sets whether this breakpoint causes execution to suspend on entry to methods.
	 * 
	 * @param enabled whether this breakpoint causes execution to suspend on entry to methods
	 * @exception CoreException if unable to set the property on this breakpoint's underlying marker
	 */
	void setEntry(boolean enabled) throws CoreException;	
	
	/**
	 * Sets whether this breakpoint causes execution to suspend on exit of methods.
	 * 
	 * @param enabled whether this breakpoint causes execution to suspend on exit of methods
	 * @exception CoreException if unable to set the property on this breakpoint's underlying marker
	 */
	void setExit(boolean enabled) throws CoreException;	
	
}
