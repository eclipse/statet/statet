/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.sourcelookup;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.sourcelookup.AbstractSourceLookupDirector;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.debug.core.sourcelookup.ISourceContainerType;
import org.eclipse.debug.core.sourcelookup.ISourceLookupParticipant;
import org.eclipse.debug.core.sourcelookup.containers.DefaultSourceContainer;

import org.eclipse.statet.internal.r.debug.core.sourcelookup.RSourceLookupParticipant;


public class RSourceLookupDirector extends AbstractSourceLookupDirector {
	
	
	private static final Set<String> gFilteredSourceContainerTypes;
	static {
		gFilteredSourceContainerTypes= new HashSet<>();
	}
	
	
	@Override
	public void initializeDefaults(final ILaunchConfiguration configuration) throws CoreException {
		dispose();
		setLaunchConfiguration(configuration);
		setSourceContainers(new ISourceContainer[] {
				new AllRProjectsSourceContainer(),
				new DefaultSourceContainer(),
		});
		initializeParticipants();
	}
	
	@Override
	public void initializeParticipants() {
		addParticipants(new ISourceLookupParticipant[] {
				new RSourceLookupParticipant(),
		});
	}
	
	@Override
	public boolean supportsSourceContainerType(final ISourceContainerType type) {
		return !gFilteredSourceContainerTypes.contains(type.getId());
	}
	
	@Override
	protected void cacheResolvedElement(final List duplicates, final Object sourceElement) {
		if (sourceElement instanceof RSourceLookupMatch) {
			((RSourceLookupMatch)sourceElement).select();
		}
		super.cacheResolvedElement(duplicates, sourceElement);
	}
	
}
