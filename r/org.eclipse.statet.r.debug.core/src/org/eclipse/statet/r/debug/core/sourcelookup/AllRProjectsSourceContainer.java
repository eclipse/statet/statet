/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.sourcelookup;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.debug.core.sourcelookup.ISourceContainerType;
import org.eclipse.debug.core.sourcelookup.containers.CompositeSourceContainer;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.debug.core.sourcelookup.Messages;
import org.eclipse.statet.r.core.project.RProjects;


@NonNullByDefault
public class AllRProjectsSourceContainer extends CompositeSourceContainer implements RSourceContainer {
	
	
	public static final String TYPE_ID= "org.eclipse.statet.r.debugSourceContainers.AllRProjectsType"; //$NON-NLS-1$
	
	
	public AllRProjectsSourceContainer() {
	}
	
	
	@Override
	public ISourceContainerType getType() {
		return getSourceContainerType(TYPE_ID);
	}
	
	@Override
	public String getName() {
		return Messages.AllRProjectsSourceContainer_name;
	}
	
	@Override
	protected @NonNull ISourceContainer[] createSourceContainers() throws CoreException {
		final List<ISourceContainer> list= new ArrayList<>();
		final IProject[] projects= ResourcesPlugin.getWorkspace().getRoot().getProjects();
		for (final IProject project : projects) {
			try {
				if (project.isOpen() && project.hasNature(RProjects.R_NATURE_ID)) {
					final ISourceContainer container= new RProjectSourceContainer(project, false);
					container.init(getDirector());
					list.add(container);
				}
			}
			catch (final Exception e) {}
		}
		return list.toArray(new @NonNull ISourceContainer[list.size()]);
	}
	
	
	@Override
	public @Nullable Object findSourceElement(final @Nullable URI fileUri, final IFile[] fileInWorkspace) throws CoreException {
		final var containers= getSourceContainers();
		for (int i= 0; i < containers.length; i++) {
			final Object element= ((RProjectSourceContainer)containers[i]).findSourceElement(
					fileUri, fileInWorkspace );
			if (element != null) {
				return element;
			}
		}
		return null;
	}
	
	@Override
	public void findSourceElement(final IPath path, final List<Object> elements) throws CoreException {
		final var containers= getSourceContainers();
		for (int i= 0; i < containers.length; i++) {
			((RProjectSourceContainer)containers[i]).findSourceElement(path, elements);
		}
	}
	
	
	@Override
	public int hashCode() {
		return getClass().hashCode() + 1;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj instanceof AllRProjectsSourceContainer);
	}
	
}
