/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface RValue extends IValue {
	
	
	@Override
	RDebugTarget getDebugTarget();
	
	RVariable getAssignedVariable();
	
	@Override
	String getValueString() throws DebugException;
	
	String getDetailString();
	
}
