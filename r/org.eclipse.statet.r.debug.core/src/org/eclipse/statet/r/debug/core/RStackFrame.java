/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core;

import org.eclipse.debug.core.model.IStackFrame;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RElementName;


/**
 * Represents an R stack frame in the Eclipse debug model for R.
 */
@NonNullByDefault
public interface RStackFrame extends IStackFrame {
	
	
	@Override
	RDebugTarget getDebugTarget();
	
	@Override
	RThread getThread();
	
	
	/**
	 * Informal filename
	 * @return filename or <code>null</code>, if not available
	 */
	@Nullable String getInfoFileName();
	
	/**
	 * Informal line number
	 * @return linenumber or <code>-1</code>, if not available
	 */
	int getInfoLineNumber();
	
	/**
	 * Position index of the frame in R (one-based like in R)
	 */
	int getPosition();
	
	/**
	 * R element name
	 */
	RElementName getElementName();
	
	
	@Override
	<T> @Nullable T getAdapter(final Class<T> type);
	
}
