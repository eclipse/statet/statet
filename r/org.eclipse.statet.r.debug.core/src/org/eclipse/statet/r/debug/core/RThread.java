/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core;

import org.eclipse.debug.core.model.IThread;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.core.eval.IEvaluationListener;


/**
 * Represents an R thread in the Eclipse debug model for R.
 */
@NonNullByDefault
public interface RThread extends IThread {
	
	
	@Override
	RDebugTarget getDebugTarget();
	
	@Override
	@Nullable RStackFrame getTopStackFrame();
	
	void evaluate(String expressionText, RStackFrame stackFrame,
			boolean forceReevaluate, IEvaluationListener listener);
	
	
	@Override
	<T> @Nullable T getAdapter(final Class<T> type);
	
}
