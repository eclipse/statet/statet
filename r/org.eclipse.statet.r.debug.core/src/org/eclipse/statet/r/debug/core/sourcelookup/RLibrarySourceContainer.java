/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.sourcelookup;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.sourcelookup.ISourceContainerType;
import org.eclipse.debug.core.sourcelookup.containers.AbstractSourceContainer;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.io.FileUtil;

import org.eclipse.statet.internal.r.debug.core.RDebugCorePlugin;
import org.eclipse.statet.internal.r.debug.core.sourcelookup.Messages;
import org.eclipse.statet.r.core.RCore;


@NonNullByDefault
public class RLibrarySourceContainer extends AbstractSourceContainer {
	
	
	public static final String TYPE_ID= "org.eclipse.statet.r.debugSourceContainers.RLibraryType"; //$NON-NLS-1$
	
	
	private static @Nullable Path resolve(final String path) {
		if (path != null) {
			try {
				final IFileStore store= FileUtil.expandToLocalFileStore(path, null, null);
				if (store != null) {
					return Paths.get(store.toURI());
				}
			}
			catch (final Exception e) {
				RDebugCorePlugin.log(new Status(IStatus.WARNING, RCore.BUNDLE_ID, 0,
						NLS.bind("Could not resolve configured R library path ''{0}}'' of " +
								"a source lookup entry.", path ), e));
			}
		}
		return null;
	}
	
	
	private final String location;
	private final @Nullable Path locationPath;
	
	
	public RLibrarySourceContainer(final String locationPath, final @Nullable Path locationStore) {
		this.location= nonNullAssert(locationPath);
		this.locationPath= locationStore;
	}
	
	public RLibrarySourceContainer(final String locationPath) {
		this(locationPath, resolve(locationPath));
	}
	
	
	@Override
	public ISourceContainerType getType() {
		return getSourceContainerType(TYPE_ID);
	}
	
	@Override
	public String getName() {
		final String s= this.location.toString();
		if (this.locationPath == null) {
			return s + Messages.RLibrarySourceContainer_name_UnresolvablePath_message;
		}
		return s;
	}
	
	/**
	 * @return the location
	 */
	public String getLocation() {
		return this.location;
	}
	
	/**
	 * @return the store, if resolved
	 */
	public @Nullable Path getLocationPath() {
		return this.locationPath;
	}
	
	@Override
	public @NonNull Object[] findSourceElements(final String name) throws CoreException {
		return EMPTY;
	}
	
	
	@Override
	public int hashCode() {
		return this.location.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof RLibrarySourceContainer
						&& this.location.equals(((RLibrarySourceContainer)obj).location)) );
	}
	
}
