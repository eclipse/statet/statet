/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core.breakpoints;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.core.model.IBreakpoint;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.RDebugTarget;


@NonNullByDefault
public interface RBreakpoint extends IBreakpoint {
	
	
	interface ITargetData extends IAdaptable {
		
		boolean isInstalled();
		
	}
	
	
	/**
	 * Returns the type of this breakpoint
	 * 
	 * @return the type id
	 */
	String getBreakpointType();
	
//	/**
//	 * Returns the hit count of this breakpoint
//	 * 
//	 * @return the hit count, or -1 if the breakpoint does not have a hit count.
//	 * @exception CoreException if unable to access the property from this breakpoint's underlying marker
//	 */
//	int getHitCount() throws CoreException;
//	
//	/**
//	 * Sets the hit count of this breakpoint.
//	 * If this breakpoint is currently disabled and the hit count
//	 * is set greater than -1, this breakpoint is automatically enabled.
//	 * 
//	 * @param count the new hit count
//	 * @exception CoreException if unable to set the property on this breakpoint's underlying marker
//	 */
//	void setHitCount(int count) throws CoreException;
	
	@Nullable ITargetData registerTarget(RDebugTarget target, ITargetData data);
	@Nullable ITargetData unregisterTarget(RDebugTarget target);
	@Nullable ITargetData getTargetData(RDebugTarget target);
	
	
	/**
	 * Returns if the breakpoint is installed in any R engine.
	 * 
	 * @return if the breakpoint is installed
	 * @throws CoreException 
	 */
	boolean isInstalled() throws CoreException;
	
}
