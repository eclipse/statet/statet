/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.debug.core;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RVariable extends IVariable {
	
	
	@Override
	RDebugTarget getDebugTarget();
	
	@Nullable RVariable getParent();
	
	@Override
	String getName();
	
	@Override
	String getReferenceTypeName() throws DebugException;
	
	
	@Override
	RValue getValue() throws DebugException;
	
	@Override
	boolean supportsValueModification();
	
	@Override
	boolean verifyValue(String expression) throws DebugException;
	
	@Override
	void setValue(String expression) throws DebugException;
	
	
	@Override
	<T> @Nullable T getAdapter(final Class<T> type);
	
}
