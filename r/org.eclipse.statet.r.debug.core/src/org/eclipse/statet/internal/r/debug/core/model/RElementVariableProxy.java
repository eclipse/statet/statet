/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.debug.core.RElementVariable;
import org.eclipse.statet.r.debug.core.RThread;
import org.eclipse.statet.r.debug.core.RVariable;


@NonNullByDefault
public class RElementVariableProxy extends RVariableProxy implements RElementVariable {
	
	
	private volatile @Nullable RElementName fqName;
	
	
	public RElementVariableProxy(final RElementVariable variable, final RVariable parent) {
		super(variable, parent);
	}
	
	
	@Override
	public final RThread getThread() {
		return ((RElementVariable) this.variable).getThread();
	}
	
	@Override
	public final CombinedRElement getElement() {
		return ((RElementVariable) this.variable).getElement();
	}
	
	@Override
	public @Nullable RElementName getFQElementName() {
		RElementName name= this.fqName;
		if (name == null) {
			name= this.fqName= BasicRElementVariable.createFQElementName(this);
		}
		return name;
	}
	
}
