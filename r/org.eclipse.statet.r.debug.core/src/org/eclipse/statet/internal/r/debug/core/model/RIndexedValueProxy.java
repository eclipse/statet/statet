/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IVariable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.debug.core.RVariable;


@NonNullByDefault
public class RIndexedValueProxy extends RValueProxy implements RIndexElementValue {
	
	
	public RIndexedValueProxy(final RIndexValueInternal value, final RVariable variable) {
		super(value, variable);
	}
	
	
	@Override
	public CombinedRElement getElement() {
		final RIndexValueInternal indexed= (RIndexValueInternal) this.value;
		return indexed.getElement();
	}
	
	@Override
	public final @NonNull IVariable[] getVariables() throws DebugException {
		final RIndexValueInternal indexed= (RIndexValueInternal)this.value;
		return indexed.getPartitionFactory().getVariables(this);
	}
	
	
	@Override
	public final long getSize() throws DebugException {
		final RIndexValueInternal indexed= (RIndexValueInternal)this.value;
		return indexed.getSize();
	}
	
	@Override
	public final @NonNull RVariable[] getVariables(final long offset, final int length) {
		final RIndexValueInternal indexed= (RIndexValueInternal)this.value;
		return indexed.getVariables(offset, length, this.variable);
	}
	
}
