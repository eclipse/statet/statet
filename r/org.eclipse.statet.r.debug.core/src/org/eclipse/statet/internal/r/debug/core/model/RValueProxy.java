/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IVariable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.RDebugTarget;
import org.eclipse.statet.r.debug.core.RValue;
import org.eclipse.statet.r.debug.core.RVariable;


@NonNullByDefault
public class RValueProxy implements RValue {
	
	
	public static RValueProxy create(RValue value, final RVariable variable) {
		if (value instanceof RValueProxy) {
			value= ((RValueProxy)value).value;
		}
		if (value instanceof RIndexValueInternal) {
			return new RIndexedValueProxy((RIndexValueInternal)value, variable);
		}
		return new RValueProxy(value, variable);
	}
	
	
	protected final RValue value;
	
	protected final RVariable variable;
	
	
	public RValueProxy(final RValue value, final RVariable variable) {
		this.value= value;
		this.variable= variable;
	}
	
	
	@Override
	public RVariable getAssignedVariable() {
		return this.variable;
	}
	
	@Override
	public final String getModelIdentifier() {
		return this.value.getModelIdentifier();
	}
	
	@Override
	public final RDebugTarget getDebugTarget() {
		return this.value.getDebugTarget();
	}
	
	@Override
	public final ILaunch getLaunch() {
		return this.value.getLaunch();
	}
	
	
	@Override
	public final String getReferenceTypeName() throws DebugException {
		return this.value.getReferenceTypeName();
	}
	
	@Override
	public final String getValueString() throws DebugException {
		if (this.value instanceof REnvValue) {
			return ((REnvValue) this.value).getValueString(this.variable);
		}
		return this.value.getValueString();
	}
	
	@Override
	public String getDetailString() {
		if (this.value instanceof REnvValue) {
			return ((REnvValue) this.value).getDetailString(this.variable);
		}
		return this.value.getDetailString();
	}
	
	
	@Override
	public final boolean isAllocated() throws DebugException {
		return this.value.isAllocated();
	}
	
	@Override
	public final boolean hasVariables() throws DebugException {
		return this.value.hasVariables();
	}
	
	@Override
	public @NonNull IVariable[] getVariables() throws DebugException {
		final IVariable[] orgVariables= this.value.getVariables();
		if (orgVariables.length == 0) {
			return RElementVariableValue.NO_VARIABLES;
		}
		final @NonNull IVariable[] proxyVariables= new @NonNull IVariable[orgVariables.length];
		for (int i= 0; i < orgVariables.length; i++) {
			proxyVariables[i]= RVariableProxy.create((RVariable)orgVariables[i], this.variable);
		}
		return proxyVariables;
//		return orgVariables;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> type) {
		return this.value.getAdapter(type);
	}
	
	
	@Override
	public final int hashCode() {
		return this.value.hashCode();
	}
	
//	@Override
//	public final boolean equals(final Object obj) {
//		if (this == obj) {
//			return true;
//		}
//		return this.value.equals(obj);
//	}
	
	@Override
	public final String toString() {
		return this.value.toString();
	}
	
}
