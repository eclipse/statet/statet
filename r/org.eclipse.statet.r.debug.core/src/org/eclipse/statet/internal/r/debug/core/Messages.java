/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String DebugContext_label;
	public static String DebugContext_UpdateStackFrame_task;
	public static String DebugContext_UpdateVariables_task;
	
	public static String Expression_Validate_Invalid_message;
	public static String Expression_Validate_Detail_SingleExpression_message;
	public static String Expression_Validate_Detail_DetailMissing_message;
	public static String Expression_Evaluate_task;
	public static String Expression_Evaluate_Cancelled_message;
	public static String Expression_Clean_task;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
