/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.DebugElement;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.debug.core.RDebugCorePlugin;
import org.eclipse.statet.r.debug.core.RDebugModel;


@NonNullByDefault
public class RDebugElement extends DebugElement {
	
	
	public RDebugElement(final RDebugTargetImpl target) {
		super(target);
	}
	
	
	@Override
	public final String getModelIdentifier() {
		return RDebugModel.IDENTIFIER;
	}
	
	@Override
	public RDebugTargetImpl getDebugTarget() {
		return (RDebugTargetImpl)super.getDebugTarget();
	}
	
	
	protected DebugException newNotSupported() {
		return new DebugException(new Status(IStatus.ERROR, RDebugCorePlugin.BUNDLE_ID,
				DebugException.NOT_SUPPORTED, "Not supported.", null));
	}
	
	protected DebugException newRequestLoadDataFailed() {
		return new DebugException(new Status(IStatus.ERROR, RDebugCorePlugin.BUNDLE_ID,
				DebugException.TARGET_REQUEST_FAILED, "Request failed: cannot load R data.", null));
	}
	
	protected DebugException newRequestSetDataFailed() {
		return new DebugException(new Status(IStatus.ERROR, RDebugCorePlugin.BUNDLE_ID,
				DebugException.TARGET_REQUEST_FAILED, "Request failed: cannot set R data.", null));
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> type) {
		return super.getAdapter(type);
	}
	
}
