/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.eval;

import org.eclipse.debug.core.model.IDebugElement;
import org.eclipse.debug.core.model.IWatchExpressionDelegate;
import org.eclipse.debug.core.model.IWatchExpressionListener;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.core.eval.EvaluationWatchExpressionResult;
import org.eclipse.statet.ecommons.debug.core.eval.IEvaluationListener;
import org.eclipse.statet.ecommons.debug.core.eval.IEvaluationResult;

import org.eclipse.statet.internal.r.debug.core.model.RStackFrameImpl;
import org.eclipse.statet.r.debug.core.RStackFrame;
import org.eclipse.statet.r.debug.core.RThread;


@NonNullByDefault
public class WatchExpressionDelegate implements IWatchExpressionDelegate, IEvaluationListener {
	
	
	private IWatchExpressionListener listener;
	
	
	public WatchExpressionDelegate() {
	}
	
	
	@Override
	public void evaluateExpression(final String expression, final IDebugElement context,
			final IWatchExpressionListener listener) {
		boolean force= false;
		final StackTraceElement[] stackTrace= Thread.currentThread().getStackTrace();
		final int end= Math.min(stackTrace.length, 8);
		for (int i= 2; i < end; i++) {
			final String className= stackTrace[i].getClassName();
			if (className.equals("org.eclipse.debug.internal.ui.viewers.update.DefaultWatchExpressionModelProxy")) { //$NON-NLS-1$
				break;
			}
			else if (className.equals("org.eclipse.debug.internal.ui.actions.expressions.ReevaluateWatchExpressionAction")) { //$NON-NLS-1$
				force= true;
			}
		}
		
		final RStackFrameImpl frame= checkContext(context);
		if (frame == null) {
			listener.watchEvaluationFinished(null);
			return;
		}
		
		this.listener= listener;
		frame.getThread().evaluate(expression, frame, force, this);
	}
	
	
	private @Nullable RStackFrameImpl checkContext(final IDebugElement context) {
		RStackFrame frame= null;
		if (context instanceof RStackFrame) {
			frame= (RStackFrame)context;
		}
		else if (context instanceof RThread) {
			frame= ((RThread)context).getTopStackFrame();
		}
		if (frame == null) {
			return null;
		}
		return (RStackFrameImpl)frame;
	}
	
	
	@Override
	public void evaluationFinished(final IEvaluationResult result) {
		if (result.getStatus() < IEvaluationResult.SKIPPED) {
			this.listener.watchEvaluationFinished(new EvaluationWatchExpressionResult(result));
		}
		else {
			result.free();
		}
	}
	
}
