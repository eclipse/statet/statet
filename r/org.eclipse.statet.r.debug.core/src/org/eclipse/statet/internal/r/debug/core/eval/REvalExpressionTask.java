/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.eval;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.debug.core.model.RMainThread;
import org.eclipse.statet.internal.r.debug.core.model.RStackFrameImpl;


@NonNullByDefault
public final class REvalExpressionTask {
	
	
	public static class Key {
		
		private final Long handle;
		
		private final String expression;
		
		
		public Key(final Long handle, final String expression) {
			this.handle= handle;
			this.expression= expression;
		}
		
		@Override
		public int hashCode() {
			return this.handle.hashCode() + this.expression.hashCode();
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			return (this == obj
					|| (obj instanceof final Key other
							&& this.handle.equals(other.handle)
							&& this.expression.equals(other.expression) ));
		}
		
	}
	
	
	private final String rExpression;
	
	private final RStackFrameImpl frame;
	
	private final Key key;
	
	
	public REvalExpressionTask(final String expressionText, final RStackFrameImpl frame) {
		this.rExpression= expressionText;
		this.frame= frame;
		
		this.key= new Key(frame.getDbgFrame().getHandle(), expressionText);
	}
	
	
	public String getRExpression() {
		return this.rExpression;
	}
	
	public RStackFrameImpl getStackFrame() {
		return this.frame;
	}
	
	public RMainThread getThread() {
		return this.frame.getThread();
	}
	
	public Key getKey() {
		return this.key;
	}
	
}
