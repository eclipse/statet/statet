/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import java.util.Arrays;

import org.eclipse.debug.core.DebugException;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.data.CombinedRElement;
import org.eclipse.statet.r.debug.core.RIndexedVariableItem;
import org.eclipse.statet.r.debug.core.RValue;
import org.eclipse.statet.r.debug.core.RVariable;
import org.eclipse.statet.rj.data.RDataUtils;


@NonNullByDefault
public class RArrayIndexVariable extends BasicRVariable implements RIndexedVariableItem, RValue {
	
	
	private final RArrayValue mainValue;
	
	private final int[] dimIndex;
	
	
	public RArrayIndexVariable(final RArrayValue value, final int[] index,
			final RVariable parent) {
		super(value.getDebugTarget(), parent);
		this.mainValue= value;
		this.dimIndex= index;
	}
	
	
	@Override
	public long[] getIndex() {
		final long[] index= new long[this.dimIndex.length];
		for (int i= 0; i < index.length; i++) {
			index[i]= this.dimIndex[i];
		}
		return index;
	}
	
	@Override
	public String getName() {
		final StringBuilder sb= new StringBuilder();
		{	sb.append('[');
			for (int i= 0; i < this.mainValue.dimCount - 1; i++) {
				sb.append(this.dimIndex[i] + 1);
				sb.append(", "); //$NON-NLS-1$
			}
			sb.append(this.dimIndex[this.mainValue.dimCount - 1] + 1);
			sb.append(']');
		}
		{	final String name= this.mainValue.getDimItemName(0, this.dimIndex[0]);
			if (name != null) {
				sb.append(' ');
				sb.append(name);
			}
		}
		return sb.toString();
	}
	
	
	@Override
	public boolean hasValueChanged() throws DebugException {
		return this.mainValue.hasValueChanged(this.dimIndex);
	}
	
	@Override
	public RValue getValue() throws DebugException {
		return this;
	}
	
	@Override
	public RVariable getAssignedVariable() {
		return this;
	}
	
	@Override
	public boolean isAllocated() throws DebugException {
		return true;
	}
	
	@Override
	public String getReferenceTypeName() throws DebugException {
		final CombinedRElement element= this.mainValue.element;
		
		return RDataUtils.getStoreAbbr(element.getData());
	}
	
	@Override
	public String getValueString() throws DebugException {
		final String data= this.mainValue.getDataExpr(this.dimIndex);
		if (data == null) {
			throw newRequestLoadDataFailed();
		}
		return data;
	}
	
	@Override
	public boolean supportsValueModification() {
		return true;
	}
	
	@Override
	public boolean verifyValue(final String expression) throws DebugException {
		return this.mainValue.validateDataExpr(expression);
	}
	
	@Override
	public void setValue(final String expression) throws DebugException {
		this.mainValue.setDataExpr(this.dimIndex, expression);
	}
	
	@Override
	public String getDetailString() {
		final String data= this.mainValue.getDataExpr(this.dimIndex);
		if (data == null) {
			return "<error>"; //$NON-NLS-1$
		}
		return data;
	}
	
	
	@Override
	public boolean hasVariables() throws DebugException {
		return false;
	}
	
	@Override
	public @NonNull RVariable[] getVariables() throws DebugException {
		return RElementVariableValue.NO_VARIABLES;
	}
	
	
	@Override
	public int hashCode() {
		return this.mainValue.hashCode() + Arrays.hashCode(this.dimIndex);
	}
	
	@Override
	public boolean equals(@Nullable Object obj) {
		return (this == obj
				|| this == (obj= RVariableProxy.unproxy(obj))
				|| (obj instanceof final RArrayIndexVariable other
						&& this.mainValue.equals(other.mainValue)
						&& Arrays.equals(this.dimIndex, other.dimIndex) ));
	}
	
}
