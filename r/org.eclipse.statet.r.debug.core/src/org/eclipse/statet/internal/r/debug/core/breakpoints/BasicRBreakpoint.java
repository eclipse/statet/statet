/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.breakpoints;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.model.Breakpoint;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.RDebugModel;
import org.eclipse.statet.r.debug.core.RDebugTarget;
import org.eclipse.statet.r.debug.core.breakpoints.RBreakpoint;


@NonNullByDefault
public abstract class BasicRBreakpoint extends Breakpoint implements RBreakpoint {
	
	
	public static final String INSTALL_COUNT_MARKER_ATTR= "org.eclipse.statet.r.resourceMarkers.InstallCountAttribute"; //$NON-NLS-1$
	
//	public static final String HIT_COUNT_MARKER_ATTR= "org.eclipse.statet.r.resourceMarkers.HitCountAttribute"; //$NON-NLS-1$
	
	
	private final Map<RDebugTarget, ITargetData> installedTargets= new ConcurrentHashMap<>();
	private final AtomicInteger installCount= new AtomicInteger();
	
	
	protected BasicRBreakpoint() {
		updateInstallCount(this.installCount.get());
	}
	
	
	@Override
	public String getModelIdentifier() {
		return RDebugModel.IDENTIFIER;
	}
	
	/**
	 * Add this breakpoint to the breakpoint manager, or sets it as unregistered.
	 */
	protected void register(final boolean register) throws CoreException {
		final DebugPlugin plugin= DebugPlugin.getDefault();
		if (plugin != null && register) {
			plugin.getBreakpointManager().addBreakpoint(this);
		}
		else {
			setRegistered(false);
		}
	}
	
	protected void update() {
	}
	
	
//	public int getHitCount() throws CoreException {
//		return ensureMarker().getAttribute(HIT_COUNT_MARKER_ATTR, -1);
//	}
//	
//	public void setHitCount(final int count) throws CoreException {
//		if (getHitCount() != count) {
//			if (!isEnabled() && count > -1) {
//				setAttributes(new String [] { ENABLED, HIT_COUNT_MARKER_ATTR },
//						new Object[]{ Boolean.TRUE, Integer.valueOf(count) });
//			}
//			else {
//				setAttributes(new String[]{ HIT_COUNT_MARKER_ATTR },
//						new Object[]{ Integer.valueOf(count) });
//			}
//			update();
//		}
//	}
	
	
	@Override
	public @Nullable ITargetData registerTarget(final RDebugTarget target, final ITargetData data) {
		nonNullAssert(target);
		final ITargetData oldData= this.installedTargets.put(target, data);
		if (oldData != null && oldData.isInstalled()) {
			if (data == null || !data.isInstalled()) {
				updateInstallCount(this.installCount.decrementAndGet());
			}
		}
		else {
			if (data != null && data.isInstalled()) {
				updateInstallCount(this.installCount.incrementAndGet());
			}
		}
		return oldData;
	}
	
	@Override
	public @Nullable ITargetData unregisterTarget(final RDebugTarget target) {
		nonNullAssert(target);
		final ITargetData oldData= this.installedTargets.remove(target);
		if (oldData != null && oldData.isInstalled()) {
			updateInstallCount(this.installCount.decrementAndGet());
		}
		return oldData;
	}
	
	private void updateInstallCount(final int count) {
		try {
			ensureMarker().setAttribute(INSTALL_COUNT_MARKER_ATTR, count);
		}
		catch (final CoreException e) {}
	}
	
	@Override
	public @Nullable ITargetData getTargetData(final RDebugTarget target) {
		return this.installedTargets.get(target);
	}
	
	
	@Override
	public boolean isInstalled() throws CoreException {
		return (this.installCount.get() > 0);
	}
	
}
