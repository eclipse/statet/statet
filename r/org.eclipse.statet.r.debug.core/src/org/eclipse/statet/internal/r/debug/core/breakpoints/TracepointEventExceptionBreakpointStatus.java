/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.breakpoints;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.breakpoints.RBreakpoint;
import org.eclipse.statet.r.debug.core.breakpoints.RExceptionBreakpointStatus;
import org.eclipse.statet.rj.server.dbg.TracepointEvent;


@NonNullByDefault
public class TracepointEventExceptionBreakpointStatus extends TracepointEventBreakpointStatus
		implements RExceptionBreakpointStatus {
	
	
	public TracepointEventExceptionBreakpointStatus(final TracepointEvent event,
			final @Nullable String label, final @Nullable RBreakpoint breakpoint) {
		super(event, label, breakpoint);
	}
	
	
}
