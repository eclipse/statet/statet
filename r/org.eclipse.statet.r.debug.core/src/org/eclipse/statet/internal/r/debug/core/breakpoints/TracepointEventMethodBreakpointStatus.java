/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.breakpoints;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.breakpoints.RBreakpoint;
import org.eclipse.statet.r.debug.core.breakpoints.RMethodBreakpointStatus;
import org.eclipse.statet.rj.server.dbg.Tracepoint;
import org.eclipse.statet.rj.server.dbg.TracepointEvent;
import org.eclipse.statet.rj.server.dbg.TracepointState;


@NonNullByDefault
public class TracepointEventMethodBreakpointStatus extends TracepointEventBreakpointStatus
		implements RMethodBreakpointStatus {
	
	
	public TracepointEventMethodBreakpointStatus(final TracepointEvent event,
			final @Nullable String label, final @Nullable RBreakpoint breakpoint) {
		super(event, label, breakpoint);
	}
	
	
	@Override
	public boolean isEntry() {
		return (this.event.getType() == Tracepoint.TYPE_FB
				&& (this.event.getFlags() & TracepointState.FLAG_MB_ENTRY) != 0 );
	}
	
	@Override
	public boolean isExit() {
		return (this.event.getType() == Tracepoint.TYPE_FB
				&& (this.event.getFlags() & TracepointState.FLAG_MB_EXIT) != 0 );
	}
	
}
