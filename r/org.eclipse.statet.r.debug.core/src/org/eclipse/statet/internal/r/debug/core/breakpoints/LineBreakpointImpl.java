/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.breakpoints;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.debug.core.RDebugModel;


@NonNullByDefault
public class LineBreakpointImpl extends GenericLineBreakpoint {
	
	
	public static final String R_LINE_BREAKPOINT_MARKER_TYPE= "org.eclipse.statet.r.resourceMarkers.RLineBreakpoint"; //$NON-NLS-1$
	
	
	public LineBreakpointImpl(final IResource resource, final int lineNumber,
			final int charStart, final int charEnd,
			final int elementType, final String elementId, final String elementLabel, final String subLabel,
			final boolean temporary) throws CoreException {
		
		final Map<String, Object> attributes= new HashMap<>();
		addStandardLineBreakpointAttributes(attributes, true, lineNumber, charStart, charEnd,
				elementType, elementId, elementLabel, subLabel );
		
		final IWorkspaceRunnable wr= new IWorkspaceRunnable() {
			@Override
			public void run(final IProgressMonitor monitor) throws CoreException {
				setMarker(resource.createMarker(R_LINE_BREAKPOINT_MARKER_TYPE, attributes));
				
				register(!temporary);
				if (temporary) {
					setPersisted(false);
				}
			}
		};
		run(ResourcesPlugin.getWorkspace().getRuleFactory().markerRule(resource), wr);
	}
	
	public LineBreakpointImpl() {
	}
	
	
	@Override
	public String getBreakpointType() {
		return RDebugModel.R_LINE_BREAKPOINT_TYPE_ID;
	}
	
}
