/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.breakpoints;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.debug.core.RDebugModel;
import org.eclipse.statet.r.debug.core.breakpoints.RMethodBreakpoint;


@NonNullByDefault
public class MethodBreakpointImpl extends LineBreakpointImpl implements RMethodBreakpoint {
	
	
	public static final String R_METHOD_BREAKPOINT_MARKER_TYPE= "org.eclipse.statet.r.resourceMarkers.RMethodBreakpoint"; //$NON-NLS-1$
	
	public static final String ENTRY_MARKER_ATTR= "org.eclipse.statet.r.resourceMarkers.EntryBreakpointAttribute"; //$NON-NLS-1$
	
	public static final String EXIT_MARKER_ATTR= "org.eclipse.statet.r.resourceMarkers.ExitBreakpointAttribute"; //$NON-NLS-1$
	
	
	public MethodBreakpointImpl(final IResource resource, final int lineNumber,
			final int charStart, final int charEnd,
			final int elementType, final String elementId, final String elementLabel, final String subLabel,
			final boolean temporary) throws CoreException {
		
		final Map<String, Object> attributes= new HashMap<>();
		addStandardLineBreakpointAttributes(attributes, true, lineNumber, charStart, charEnd,
				elementType, elementId, elementLabel, subLabel );
		attributes.put(ENTRY_MARKER_ATTR, Boolean.TRUE);
		
		final IWorkspaceRunnable wr= new IWorkspaceRunnable() {
			@Override
			public void run(final IProgressMonitor monitor) throws CoreException {
				setMarker(resource.createMarker(R_METHOD_BREAKPOINT_MARKER_TYPE, attributes));
				
				register(!temporary);
				if (temporary) {
					setPersisted(false);
				}
			}
		};
		run(ResourcesPlugin.getWorkspace().getRuleFactory().markerRule(resource), wr);
	}
	
	public MethodBreakpointImpl() {
	}
	
	
	@Override
	public String getBreakpointType() {
		return RDebugModel.R_METHOD_BREAKPOINT_TYPE_ID;
	}
	
	
	@Override
	public void setEnabled(final boolean enabled) throws CoreException {
		if (isEnabled() != enabled) {
			if (enabled && !(isEntry() || isExit())) {
				setAttributes(new String[] { ENABLED, ENTRY_MARKER_ATTR },
						new Object[] { Boolean.TRUE, Boolean.TRUE });
			}
			else {
				setAttribute(ENABLED, enabled);
			}
		}
	}
	
	@Override
	public boolean isEntry() throws CoreException {
		return ensureMarker().getAttribute(ENTRY_MARKER_ATTR, true);
	}
	
	@Override
	public boolean isExit() throws CoreException {
		return ensureMarker().getAttribute(EXIT_MARKER_ATTR, false);
	}
	
	@Override
	public void setEntry(final boolean enabled) throws CoreException {
		if (isEntry() != enabled) {
			if (!isEnabled() && enabled) {
				setAttributes(new String[] { ENABLED, ENTRY_MARKER_ATTR },
						new Object[] { Boolean.TRUE, Boolean.TRUE });
			}
			else if (!(enabled || isExit())){
				setAttributes(new String[] { ENABLED, ENTRY_MARKER_ATTR },
						new Object[] { Boolean.FALSE, Boolean.FALSE });
			}
			else {
				setAttribute(ENTRY_MARKER_ATTR, enabled);
			}
			update();
		}
	}
	
	@Override
	public void setExit(final boolean enabled) throws CoreException {
		if (isExit() != enabled) {
			if (!isEnabled() && enabled) {
				setAttributes(new String[] { ENABLED, EXIT_MARKER_ATTR },
						new Object[] { Boolean.TRUE, Boolean.TRUE });
			}
			else if (!(enabled || isEntry())){
				setAttributes(new String[] { ENABLED, EXIT_MARKER_ATTR },
						new Object[] { Boolean.FALSE, Boolean.FALSE });
			}
			else {
				setAttribute(EXIT_MARKER_ATTR, enabled);
			}
			update();
		}
	}
	
}
