/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IValue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.RVariable;


@NonNullByDefault
public abstract class BasicRVariable extends RDebugElement implements RVariable {
	
	
	private @Nullable RVariable parent;
	
	
	public BasicRVariable(final RDebugTargetImpl target, final @Nullable RVariable parent) {
		super(target);
		
		this.parent= parent;
	}
	
	
	@Override
	public final @Nullable RVariable getParent() {
		return this.parent;
	}
	
	void setParent(final RVariable parent) {
		assert (this.parent == null);
		this.parent= parent;
	}
	
	
	@Override
	public boolean verifyValue(final IValue value) throws DebugException {
		throw newNotSupported();
	}
	
	@Override
	public void setValue(final String expression) throws DebugException {
		throw newNotSupported();
	}
	
	@Override
	public void setValue(final IValue value) throws DebugException {
		throw newNotSupported();
	}
	
}
