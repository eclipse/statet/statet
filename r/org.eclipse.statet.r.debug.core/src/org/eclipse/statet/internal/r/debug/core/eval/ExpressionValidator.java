/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.eval;

import java.util.List;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.internal.r.debug.core.Messages;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.Task;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.r.core.model.build.RIssueReporter;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;
import org.eclipse.statet.r.core.source.ast.RParser;


@NonNullByDefault
public class ExpressionValidator implements IssueRequestor {
	
	
	private static class StopReporterException extends RuntimeException {
		
		private static final long serialVersionUID= 1L;
		
		public StopReporterException() {
		}
		
	}
	
	
	private final RParser rParser= new RParser(AstInfo.LEVEL_MODEL_DEFAULT);
	
	private final RIssueReporter rProblemReporter= new RIssueReporter();
	
	private @Nullable String errorMessage;
	
	
	public ExpressionValidator() {
	}
	
	
	public @Nullable String checkExpression(final String expression) {
		try {
			final RAstNode node= this.rParser.parseExpr(new StringParserInput(expression).init());
			if (node == null) {
				this.errorMessage= Messages.Expression_Validate_Detail_SingleExpression_message;
			}
			else if (RAsts.hasErrors(node)) {
				final SourceContent content= new SourceContent(0, expression);
				try {
					this.rProblemReporter.run(node, content, this, ModelManager.AST);
				}
				catch (final StopReporterException e) {}
				if (this.errorMessage == null) {
					this.errorMessage= Messages.Expression_Validate_Detail_DetailMissing_message;
				}
			}
			if (this.errorMessage != null) {
				return NLS.bind(Messages.Expression_Validate_Invalid_message, this.errorMessage);
			}
			return null;
		}
		finally {
			this.errorMessage= null;
		}
	}
	
	
	@Override
	public boolean isInterestedInProblems(final String categoryId) {
		return true;
	}
	
	@Override
	public void acceptProblems(final Problem problem) {
		this.errorMessage= problem.getMessage();
		throw new StopReporterException();
	}
	
	@Override
	public void acceptProblems(final String categoryId, final List<Problem> problems) {
		for (final Problem problem : problems) {
			acceptProblems(problem);
		}
	}
	
	
	@Override
	public boolean isInterestedInTasks() {
		return false;
	}
	
	@Override
	public void acceptTask(final Task task) {
	}
	
	
	@Override
	public void finish() {
	}
	
}
