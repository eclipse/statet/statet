/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.core.model;

import java.util.function.Consumer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.core.RVariable;


@NonNullByDefault
public class RElementVariableCompactStore {
	
	
	private final @Nullable BasicRElementVariable[] array;
	
	
	public RElementVariableCompactStore(final int length) {
		this.array= new @Nullable BasicRElementVariable[length];
	}
	
	
	public void set(final int idx, final BasicRElementVariable value) {
		this.array[idx]= value;
	}
	
	public @Nullable BasicRElementVariable get(final int idx) {
		return this.array[idx];
	}
	
	public @Nullable BasicRElementVariable clear(final int idx) {
		final BasicRElementVariable value= this.array[idx];
		this.array[idx]= null;
		return value;
	}
	
	public void forEachSet(final Consumer<BasicRElementVariable> action) {
		for (int idx= 0; idx < this.array.length; idx++) {
			final BasicRElementVariable value= this.array[idx];
			if (value != null) {
				action.accept(value);
			}
		}
	}
	
	public void toArray(final @Nullable RVariable[] to, final int idx) {
		System.arraycopy(this.array, 0, to, idx, this.array.length);
	}
	
}
