/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ide.core;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;

import org.eclipse.statet.ltk.issues.core.Issues;
import org.eclipse.statet.ltk.issues.core.TaskPriority;


@NonNullByDefault
public class IdeCorePreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IScopeContext scope= DefaultScope.INSTANCE;
		
		final Map<Preference<?>, Object> map= new HashMap<>();
		
		PreferenceUtils.setPrefValue(scope, Issues.TASK_TAG_KEYWORD_PREF,
				ImCollections.newList("TODO", "FIXME") ); //$NON-NLS-1$ //$NON-NLS-2$
		PreferenceUtils.setPrefValue(scope, Issues.TASK_TAG_PRIORITY_PREF,
				ImCollections.newList(TaskPriority.NORMAL, TaskPriority.NORMAL) );
		
		PreferenceUtils.setPrefValues(scope, map);
	}
	
}
