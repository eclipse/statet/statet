/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ide.core;

import java.util.concurrent.CopyOnWriteArraySet;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.Plugin;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class BaseCorePlugin extends Plugin {
	
	
	private static @Nullable BaseCorePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	@SuppressWarnings("null")
	public static BaseCorePlugin getInstance() {
		return instance;
	}
	
	
	private boolean started;
	
	private final CopyOnWriteArraySet<Disposable> disposables= new CopyOnWriteArraySet<>();
	
	
	/**
	 * The constructor
	 */
	public BaseCorePlugin() {
		instance= this;
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
			}
			
			try {
				for (final Disposable listener : this.disposables) {
					listener.dispose();
				}
			}
			finally {
				this.disposables.clear();
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	public void addStopListener(final Disposable listener) {
		this.disposables.add(listener);
	}
	
	public void removeStopListener(final Disposable listener) {
		this.disposables.remove(listener);
	}
	
	
}
