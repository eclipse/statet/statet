/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.cmd.ui.launching;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.EnvironmentTab;

import org.eclipse.statet.internal.r.cmd.ui.launching.RCmdMainTab;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.launching.core.RLaunching;


public class RCmdLaunching {
	
	
	public static final String R_CMD_CONFIGURATION_TYPE_ID = "org.eclipse.statet.r.launchConfigurations.RCmdTool"; //$NON-NLS-1$
	
	public static final String R_CMD_PROCESS_TYPE = "R.cmd"; //$NON-NLS-1$
	
	public static final String R_CMD_COMMAND_ATTR_NAME = RCmdMainTab.NS + "arguments.cmd"; //$NON-NLS-1$
	public static final String R_CMD_OPTIONS_ATTR_NAME= RCmdMainTab.NS + "arguments.options"; //$NON-NLS-1$
	public static final String R_CMD_RESOURCE_ATTR_NAME= RCmdMainTab.NS + "arguments.resource"; //$NON-NLS-1$
	
	public static final String WORKING_DIRECTORY_ATTR_NAME= RLaunching.ATTR_WORKING_DIRECTORY;
	
	public static final String RENV_CODE_ATTR_NAME= RLaunching.ATTR_RENV_CODE;
	
	
	public static ILaunchConfigurationWorkingCopy createNewRCmdConfig(final String name, final String cmd) throws CoreException {
		final ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
		final ILaunchConfigurationType type = launchManager.getLaunchConfigurationType(R_CMD_CONFIGURATION_TYPE_ID);
		final ILaunchConfigurationWorkingCopy config = type.newInstance(null, name);
		
		initializeRCmdConfig(config, cmd);
		
		return config;
	}
	
	public static void initializeRCmdConfig(final ILaunchConfigurationWorkingCopy config,
			final String cmd) {
		new EnvironmentTab().setDefaults(config);
		new CommonTab().setDefaults(config);
		config.setAttribute(R_CMD_COMMAND_ATTR_NAME, cmd);
		config.setAttribute(R_CMD_OPTIONS_ATTR_NAME, ""); //$NON-NLS-1$
		config.setAttribute(R_CMD_RESOURCE_ATTR_NAME, "${resource_loc}"); //$NON-NLS-1$
		config.setAttribute(WORKING_DIRECTORY_ATTR_NAME, "${container_loc}"); //$NON-NLS-1$
		config.setAttribute(RENV_CODE_ATTR_NAME, RCore.DEFAULT_WORKBENCH_ENV_ID);
	}
	
	
	private RCmdLaunching() {
	}
	
}
