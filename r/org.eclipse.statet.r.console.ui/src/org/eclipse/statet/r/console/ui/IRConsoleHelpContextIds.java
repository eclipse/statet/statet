/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.ui;

import org.eclipse.statet.r.ui.RUI;


public interface IRConsoleHelpContextIds {
	
	
	String R_CONSOLE = RUI.BUNDLE_ID + ".r_console"; //$NON-NLS-1$
	String R_CONSOLE_LAUNCH = RUI.BUNDLE_ID + ".r_console-launch"; //$NON-NLS-1$
	
	String R_REMOTE_CONSOLE_LAUNCH = RUI.BUNDLE_ID + ".r_remote_console-launch"; //$NON-NLS-1$
	
}
