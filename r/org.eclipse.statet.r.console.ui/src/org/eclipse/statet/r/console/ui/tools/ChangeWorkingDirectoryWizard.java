/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.console.ui.tools;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.databinding.wizard.WizardPageSupport;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.workbench.ResourceInputComposite;

import org.eclipse.statet.internal.r.console.ui.Messages;
import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.actions.ToolAction;
import org.eclipse.statet.nico.ui.util.NicoWizardDialog;
import org.eclipse.statet.nico.ui.util.ToolInfoGroup;


/**
 * Wizard to change the working directory of R.
 */
public class ChangeWorkingDirectoryWizard extends Wizard {
	
	
	public static class ChangeAction extends ToolAction {
		
		public ChangeAction(final ToolProvider support) {
			super(support, true);
			
			setId("org.eclipse.statet.r.tools.ChangeWorkingDirectory"); //$NON-NLS-1$
			setText(Messages.ChangeWorkingDir_Action_label);
		}
		
		@Override
		public void run() {
			final ToolProcess tool = getTool();
			if (tool == null) {
				return;
			}
			final ChangeWorkingDirectoryWizard wizard = new ChangeWorkingDirectoryWizard(tool);
			final WizardDialog dialog = new NicoWizardDialog(UIAccess.getActiveWorkbenchShell(true), wizard);
			dialog.setBlockOnOpen(false);
			dialog.open();
		}
	}
	
	
	private class SelectWDDialog extends WizardPage {
		
		private static final String SETTINGS_HISTORY = "statet:location.workingdir"; //$NON-NLS-1$
		
		
		private final WritableValue fNewLocationString;
		
		private ResourceInputComposite fLocationGroup;
		
		private DataBindingContext fDbc;
		
		
		public SelectWDDialog() {
			super("ChangeWorkingDirectory"); //$NON-NLS-1$
			
			setTitle(Messages.ChangeWorkingDir_SelectDialog_title);
			setDescription(Messages.ChangeWorkingDir_SelectDialog_message);
			
			final Realm realm= Realm.getDefault();
			this.fNewLocationString= new WritableValue(realm, "", String.class); //$NON-NLS-1$
		}
		
		@Override
		public void createControl(final Composite parent) {
			initializeDialogUnits(parent);
			
			final Composite container = new Composite(parent, SWT.NONE);
			container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			container.setLayout(new GridLayout());
			setControl(container);
			
			createContents(container);
			LayoutUtils.addSmallFiller(container, true);
			final ToolInfoGroup info = new ToolInfoGroup(container, fTool);
			info.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			Dialog.applyDialogFont(container);
			
//			initFields();
//			validate();
			setErrorMessage(null);
			setMessage(null);
		}
		
		protected void createContents(final Composite container) {
			fLocationGroup = new ResourceInputComposite(container,
					ResourceInputComposite.STYLE_COMBO,
					ResourceInputComposite.MODE_DIRECTORY | ResourceInputComposite.MODE_OPEN,
					Messages.ChangeWorkingDir_Resource_label);
			fLocationGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			fLocationGroup.setHistory(getDialogSettings().getArray(SETTINGS_HISTORY));
			
			final IFileStore current = fTool.getWorkspace().getWorkspaceDir();
			String dir = ""; //$NON-NLS-1$
			if (current != null) {
				final IPath path = URIUtil.toPath(current.toURI());
				if (path != null) {
					dir = path.toOSString();
				}
			}
			
			final Realm realm = Realm.getDefault();
			fDbc = new DataBindingContext(realm);
			fDbc.bindValue(
					fLocationGroup.getObservable(),
					fNewLocationString,
					new UpdateValueStrategy<>()
							.setAfterGetValidator(fLocationGroup.getValidator()),
					null );
			
			WizardPageSupport.create(this, fDbc);
		}
		
		public void saveSettings() {
			final IDialogSettings settings = getDialogSettings();
			DialogUtils.saveHistorySettings(settings, SETTINGS_HISTORY, (String) fNewLocationString.getValue());
		}
		
		public IFileStore getResource() {
			return fLocationGroup.getResourceAsFileStore();
		}
		
		@Override
		public void dispose() {
			if (fDbc != null) {
				fDbc.dispose();
				fDbc = null;
			}
			super.dispose();
		}
	}
	
	
	ToolProcess fTool;
	SelectWDDialog fPage;
	
	
	public ChangeWorkingDirectoryWizard(final ToolProcess tool) {
		fTool = tool;
		
		setDialogSettings(DialogUtils.getDialogSettings(RConsoleUIPlugin.getInstance(), ChangeWDRunnable.TYPE_ID+"-Wizard")); //$NON-NLS-1$
		setWindowTitle(Messages.ChangeWorkingDir_Task_label);
		setNeedsProgressMonitor(false);
	}
	
	@Override
	public void addPages() {
		fPage = new SelectWDDialog();
		addPage(fPage);
	}
	
	@Override
	public boolean performFinish() {
		fPage.saveSettings();
		
		final ChangeWDRunnable runnable = new ChangeWDRunnable(fPage.getResource());
		fTool.getQueue().add(runnable);
		return true;
	}
	
}
