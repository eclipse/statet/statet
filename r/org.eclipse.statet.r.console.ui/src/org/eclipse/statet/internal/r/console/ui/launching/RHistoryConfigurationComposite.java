/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import org.eclipse.core.databinding.beans.typed.BeanProperties;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.nico.core.NicoVariables;
import org.eclipse.statet.nico.core.util.HistoryTrackingConfiguration;


public class RHistoryConfigurationComposite extends RTrackingConfigurationComposite {
	
	
	static final String HISTORY_TRACKING_DEFAULT_PATH= "${" + NicoVariables.SESSION_STARTUP_WD_VARNAME + "}/.Rhistory"; //$NON-NLS-1$ //$NON-NLS-2$
	
	
	private Button autoloadControl;
	
	
	public RHistoryConfigurationComposite(final Composite parent) {
		super(parent);
		
		setStreamsEnabled(false);
	}
	
	
	@Override
	protected void configure() {
		addSaveTemplate(new SaveTemplate("'.Rhistory' in R working directory", HISTORY_TRACKING_DEFAULT_PATH));
		addSaveTemplate(new SaveTemplate("'.Rhistory' in user home directory", "${user_home}/.RHistory")); //$NON-NLS-2$
	}
	
	@Override
	protected Composite createAdditionalOptions(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setText("History Actions:");
		composite.setLayout(LayoutUtils.newGroupGrid(1));
		
		this.autoloadControl= new Button(composite, SWT.CHECK);
		this.autoloadControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		this.autoloadControl.setText("&Load history from file at startup");
		
		addDefaultAdditionalOptions(composite);
		
		return composite;
	}
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		super.addBindings(db);
		
		db.getContext().bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.autoloadControl),
				BeanProperties.value(HistoryTrackingConfiguration.class, "loadHistory", Boolean.TYPE)
						.observe(getInput()) );
	}
	
	
	@Override
	public HistoryTrackingConfiguration getInput() {
		return (HistoryTrackingConfiguration)super.getInput();
	}
	
}
