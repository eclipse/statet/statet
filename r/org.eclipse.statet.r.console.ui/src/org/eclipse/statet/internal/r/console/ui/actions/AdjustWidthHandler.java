/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.TextConsoleViewer;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.part.PageBookView;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.ecommons.ts.ui.ToolRunnableDecorator;
import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolRegistry;
import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolSessionData;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.console.ui.Messages;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.console.NIConsole;
import org.eclipse.statet.nico.ui.console.NIConsolePage;
import org.eclipse.statet.r.console.core.IRBasicAdapter;
import org.eclipse.statet.r.console.core.RConsoleTool;


public class AdjustWidthHandler extends AbstractHandler {
	
	
	private static class AdjustWidthRunnable implements ToolRunnable, ToolRunnableDecorator {
		
		
		private final int fWidth;
		
		
		public AdjustWidthRunnable(final int width) {
			fWidth = width;
		}
		
		
		@Override
		public String getTypeId() {
			return "r/console/width"; //$NON-NLS-1$
		}
		
		@Override
		public Image getImage() {
			return ConsolePlugin.getImage(IConsoleConstants.IMG_VIEW_CONSOLE);
		}
		
		@Override
		public String getLabel() {
			return Messages.AdjustWidth_task;
		}
		
		@Override
		public boolean canRunIn(final Tool tool) {
			return (tool.isProvidingFeatureSet(RConsoleTool.R_BASIC_FEATURESET_ID));
		}
		
		@Override
		public boolean changed(final int event, final Tool tool) {
			if (event == MOVING_FROM) {
				return false;
			}
			return true;
		}
		
		@Override
		public void run(final ToolService service, final ProgressMonitor m) throws StatusException {
			final IRBasicAdapter r = (IRBasicAdapter) service;
			r.submitToConsole("options(width = "+fWidth+"L)", m); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
	}
	
	
	public AdjustWidthHandler() {
	}
	
	
	@Override
	public void setEnabled(final Object evaluationContext) {
		setBaseEnabled((evaluationContext instanceof IEvaluationContext)
				&& isValid((ToolProcess) ((IEvaluationContext) evaluationContext)
						.getVariable(WorkbenchToolRegistry.ACTIVE_TOOL_SOURCE_NAME) ));
	}
	
	private boolean isValid(final Tool tool) {
		return (tool != null && tool.getMainType() == RConsoleTool.TYPE
				&& !tool.isTerminated() );
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final WorkbenchToolSessionData session= NicoUI.getToolRegistry().getActiveToolSession(
				UIAccess.getActiveWorkbenchPage(true) );
		if (!isValid(session.getTool())) {
			return null;
		}
		final NIConsole console= NicoUITools.getConsole(session);
		if (console == null) {
			return null;
		}
		final IConsoleView consoleView = NicoUITools.getConsoleView(console, session.getPage());
		if (consoleView == null || !(consoleView instanceof PageBookView)) {
			return null;
		}
		consoleView.display(console);
		final IPage page = ((PageBookView) consoleView).getCurrentPage();
		if (page instanceof NIConsolePage && ((NIConsolePage) page).getConsole() == console) {
			int width = getWidth(((NIConsolePage) page).getOutputViewer());
			if (width >= 0) {
				if (width < 10) {
					width = 10;
				}
				console.getProcess().getQueue().add(new AdjustWidthRunnable(width));
			}
		}
		return null;
	}
	
	private int getWidth(final TextConsoleViewer viewer) {
		if (UIAccess.isOkToUse(viewer)) {
			final GC gc = new GC(Display.getCurrent());
			try {
				gc.setFont(viewer.getTextWidget().getFont());
				final FontMetrics fontMetrics = gc.getFontMetrics();
				final double charWidth = fontMetrics.getAverageCharacterWidth();
				final int clientWidth = viewer.getTextWidget().getClientArea().width;
				return (int)(clientWidth / charWidth + 0x0.FFFFFp0);
			}
			finally {
				gc.dispose();
			}
		}
		return -1;
	}
	
}
