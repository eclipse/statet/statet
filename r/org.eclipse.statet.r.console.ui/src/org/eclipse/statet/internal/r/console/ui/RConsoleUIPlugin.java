/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.internal.r.console.ui.snippets.RSnippets;


public class RConsoleUIPlugin extends AbstractUIPlugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.r.console.ui"; //$NON-NLS-1$
	
	
	public static final String IMG_OBJ_SNIPPETS=                BUNDLE_ID + "/images/obj/snippets"; //$NON-NLS-1$
	
	
	private static RConsoleUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static RConsoleUIPlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	public static final void logError(final String message, final Throwable e) {
		log(new org.eclipse.core.runtime.Status(IStatus.ERROR, BUNDLE_ID, message, e));
	}
	
	
	private boolean started;
	
	
	private RSnippets rSnippets;
	
	
	/** Created via framework */
	public RConsoleUIPlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		final ImageRegistryUtil util = new ImageRegistryUtil(this);
		
		util.register(IMG_OBJ_SNIPPETS, ImageRegistryUtil.T_OBJ, "snippets.png"); //$NON-NLS-1$
	}
	
	
	public synchronized RSnippets getRSnippets() {
		if (this.rSnippets == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.rSnippets = new RSnippets();
		}
		return this.rSnippets;
	}
	
}
