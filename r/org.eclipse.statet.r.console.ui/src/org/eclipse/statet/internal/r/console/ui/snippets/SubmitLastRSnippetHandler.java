/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.snippets;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.ecommons.ui.util.MenuUtils;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;


public class SubmitLastRSnippetHandler extends AbstractHandler implements IElementUpdater {
	
	
	private final RSnippets snippets;
	
	
	public SubmitLastRSnippetHandler() {
		this.snippets= RConsoleUIPlugin.getInstance().getRSnippets();
	}
	
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
		try {
			final TemplateStore templateStore= this.snippets.getTemplateStore();
			final String name= this.snippets.getLastSnippet();
			final Template lastTemplate= (name != null) ?
					templateStore.findTemplate(name) : null;
			if (lastTemplate != null) {
				element.setTooltip(lastTemplate.getDescription());
			}
			else {
				element.setTooltip(null);
			}
		}
		finally {
			WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
		}
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final String name= this.snippets.getLastSnippet();
		final Template template= (name != null) ?
				this.snippets.getTemplateStore().findTemplate(name) : null;
		if (template != null) {
			this.snippets.run(template, event);
			return null;
		}
		{	// Show pull down menu
			final Object trigger= event.getTrigger();
			if (trigger instanceof Event) {
				final Widget widget= ((Event) trigger).widget;
				if (widget instanceof final ToolItem ti) {
					final MenuManager menuManager= new MenuManager();
					final Menu menu= menuManager.createContextMenu(ti.getParent());
					MenuUtils.registerOneWayMenu(menuManager,
							"org.eclipse.statet.r.menus.RunRSnippetMain" ); //$NON-NLS-1$
					MenuUtils.setPullDownPosition(menu, ti);
					
					menu.setVisible(true);
				}
			}
		}
		
		return null;
	}
	
	
}
