/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.ui.ILaunchConfigurationTab2;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.WrappedLaunchConfigurationTab;


/**
 * This class wraps StandardVMCommandTab to add:
 *   - "(javaw / javaw-utf8)" as default label
 *   - allow "javaw" as specific configuration (to avoid javaw-utf8)
 */
@NonNullByDefault
class ExtJavaStandardVMCommonTab extends WrappedLaunchConfigurationTab {
	
	
	static final Pattern STANDARD_VM_TAB_CLASS_PATTERN= Pattern.compile("\\Qorg.eclipse.jdt.\\E.*\\Q.StandardVMCommandTab\\E"); //$NON-NLS-1$
	
	private static final String JAVA_COMMAND_MARKER= "XXX-SPECIFIED-JAVA-COMMAND-XXX"; //$NON-NLS-1$
	
	
	private @Nullable Text javaCommandText;
	
	
	public ExtJavaStandardVMCommonTab(final ILaunchConfigurationTab2 vmTab) {
		super(vmTab);
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		super.createControl(parent);
		
		final Control control= getControl();
		if (control instanceof Composite) {
			for (final Control child : ((Composite)control).getChildren()) {
				if (child instanceof final Button label) {
					final String text= label.getText();
					if (text.contains("(javaw)")) { //$NON-NLS-1$
						label.setText(text.replace("(javaw)", "(javaw / javaw-utf8)")); //$NON-NLS-1$ //$NON-NLS-2$
					}
				}
				else if (child instanceof Text) {
					this.javaCommandText= (Text)child;
					break;
				}
			}
		}
	}
	
	
	@Override
	public void initializeFrom(final ILaunchConfiguration configuration) {
		try {
			final var vmSpecificAttributes= configuration.getAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_INSTALL_TYPE_SPECIFIC_ATTRS_MAP,
					(Map<String, String>)null );
			final String javaCommand;
			final Text javaCommandText;
			if (vmSpecificAttributes != null
					&& (javaCommand= vmSpecificAttributes.get(IJavaLaunchConfigurationConstants.ATTR_JAVA_COMMAND)) != null
					&& !javaCommand.isEmpty()
					&& (javaCommandText= this.javaCommandText) != null ) {
				
				vmSpecificAttributes.put(IJavaLaunchConfigurationConstants.ATTR_JAVA_COMMAND, JAVA_COMMAND_MARKER);
				try {
					super.initializeFrom(configuration);
					
					if (javaCommandText.getText().equals(JAVA_COMMAND_MARKER)) {
						javaCommandText.setText(javaCommand);
						return;
					}
				}
				finally {
					vmSpecificAttributes.put(IJavaLaunchConfigurationConstants.ATTR_JAVA_COMMAND, javaCommand);
				}
			}
		}
		catch (final Exception e) {}
		
		super.initializeFrom(configuration);
	}
	
}
