/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.page;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistProcessor;
import org.eclipse.statet.nico.ui.console.NIConsolePage;
import org.eclipse.statet.r.console.ui.RConsole;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.doc.RDocumentContentInfo;
import org.eclipse.statet.r.ui.editors.RSourceEditor;
import org.eclipse.statet.r.ui.sourceediting.RContentAssistProcessor;
import org.eclipse.statet.r.ui.sourceediting.RSourceViewerConfiguration;
import org.eclipse.statet.r.ui.sourceediting.RSourceViewerConfigurator;


public class RInputConfigurator extends RSourceViewerConfigurator {
	
	
	private static class ThisConfiguration extends RSourceViewerConfiguration {
		
		public ThisConfiguration(final RSourceEditor sourceEditor) {
			super(RDocumentContentInfo.INSTANCE, 0, sourceEditor, null, null, null);
		}
		
		@Override
		public void initContentAssist(final ContentAssist assistant) {
			final ContentAssistComputerRegistry registry = RUIPlugin.getInstance().getRConsoleContentAssistRegistry();
			
			final ContentAssistProcessor codeProcessor = new RContentAssistProcessor(assistant,
					RDocumentConstants.R_DEFAULT_CONTENT_TYPE, registry, getSourceEditor());
			codeProcessor.setCompletionProposalAutoActivationCharacters(new char[] { '$', '@' });
			codeProcessor.setContextInformationAutoActivationCharacters(new char[] { '(', ',' });
			assistant.setContentAssistProcessor(codeProcessor, RDocumentConstants.R_DEFAULT_CONTENT_TYPE);
			
			final ContentAssistProcessor symbolProcessor = new RContentAssistProcessor(assistant,
					RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE, registry, getSourceEditor());
			assistant.setContentAssistProcessor(symbolProcessor, RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE);
			
			final ContentAssistProcessor stringProcessor = new RContentAssistProcessor(assistant,
					RDocumentConstants.R_STRING_CONTENT_TYPE, registry, getSourceEditor());
			assistant.setContentAssistProcessor(stringProcessor, RDocumentConstants.R_STRING_CONTENT_TYPE);
		}
		
	}
	
	
	private final NIConsolePage page;
	
	
	public RInputConfigurator(final NIConsolePage page, final RSourceEditor inputEditor) {
		super((RConsole) page.getConsole(), new ThisConfiguration(inputEditor));
		this.page = page;
	}
	
}
