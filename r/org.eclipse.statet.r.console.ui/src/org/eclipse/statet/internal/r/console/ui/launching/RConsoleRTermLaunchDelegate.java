/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbenchPage;

import org.eclipse.statet.jcommons.runtime.ProcessUtils;

import org.eclipse.statet.ecommons.debug.core.util.LaunchUtils;
import org.eclipse.statet.ecommons.debug.ui.util.UnterminatedLaunchAlerter;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.r.console.ui.Messages;
import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;
import org.eclipse.statet.nico.core.runtime.ToolRunner;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.console.NIConsoleColorAdapter;
import org.eclipse.statet.nico.ui.util.WorkbenchStatusHandler;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.r.console.ui.RConsole;
import org.eclipse.statet.r.console.ui.launching.RConsoleLaunching;
import org.eclipse.statet.r.core.renv.IREnvConfiguration;
import org.eclipse.statet.r.core.renv.IREnvConfiguration.Exec;
import org.eclipse.statet.r.launching.core.RLaunching;
import org.eclipse.statet.r.launching.ui.REnvTab;
import org.eclipse.statet.r.nico.impl.RTermController;


public class RConsoleRTermLaunchDelegate implements ILaunchConfigurationDelegate {
	
	
	@Override
	public void launch(final ILaunchConfiguration configuration, final String mode,
			final ILaunch launch, final IProgressMonitor monitor)
			throws CoreException {
		final IWorkbenchPage page= UIAccess.getActiveWorkbenchPage(false);
		final SubMonitor m= SubMonitor.convert(monitor, 15);
		
		final long timestamp= System.currentTimeMillis();
		
		m.worked(1);
		if (m.isCanceled()) {
			return;
		}
		
		// r env
		final IREnvConfiguration renv= RLaunching.getREnvConfig(configuration, true);
//		renv.validate();
		
		// working directory
		final IFileStore workingDirectory= REnvTab.getWorkingDirectory(configuration);
		
		m.worked(1);
		if (m.isCanceled()) {
			return;
		}
		
		final ProcessBuilder builder= new ProcessBuilder();
		builder.directory(workingDirectory.toLocalFile(EFS.NONE, null));
		
		// environment
		final Map<String, String> envVariables= renv.getEnvironmentsVariables();
		addJavaConf(configuration, envVariables);
		
		final Map<String, String> envp= builder.environment();
		LaunchUtils.configureEnvironment(envp, configuration, envVariables);
		
		final List<String> cmdLine= builder.command();
		cmdLine.addAll(0, renv.getExecCommand(Exec.TERM));
		if (Platform.getOS().startsWith("win")) { //$NON-NLS-1$
			cmdLine.add("--ess"); //$NON-NLS-1$
		}
		else {
			cmdLine.add("--interactive");
		}
		
		// arguments
		cmdLine.addAll(Arrays.asList(
				LaunchUtils.getProcessArguments(configuration, RConsoleLaunching.ATTR_OPTIONS) ));
		
		m.worked(1);
		if (m.isCanceled()) {
			return;
		}
		
		final String encoding= configuration.getAttribute(DebugPlugin.ATTR_CONSOLE_ENCODING, ""); //$NON-NLS-1$
		Charset charset;
		try {
			if (encoding.length() > 0) {
				charset= Charset.forName(encoding);
			}
			else {
				charset= Charset.defaultCharset();
			}
		} catch (final Exception e) {
			throw new CoreException(new Status(IStatus.ERROR, RConsoleUIPlugin.BUNDLE_ID, 0,
					NLS.bind(Messages.LaunchDelegate_error_InvalidUnsupportedConsoleEncoding_message, encoding),
					e ));
		}
		
		m.worked(1);
		if (m.isCanceled()) {
			return;
		}
		
		// create process
		UnterminatedLaunchAlerter.registerLaunchType(RConsoleLaunching.R_CONSOLE_CONFIGURATION_TYPE_ID);
		
		final RProcess process= new RProcess(launch, renv,
				LaunchUtils.createLaunchPrefix(configuration), renv.getName() + " / Rterm " + LaunchUtils.createProcessTimestamp(timestamp), //$NON-NLS-1$
				null,
				workingDirectory.toString(),
				timestamp );
		process.setAttribute(IProcess.ATTR_CMDLINE, ProcessUtils.generateCommandLine(cmdLine));
		
		final RTermController controller= new RTermController(process, builder, charset);
		process.init(controller);
		RConsoleLaunching.registerDefaultHandlerTo(controller);
		
		m.worked(5);
		
		RConsoleLaunching.scheduleStartupSnippet(controller, configuration);
		
		final RConsole console= new RConsole(process, new NIConsoleColorAdapter());
		NicoUITools.startConsoleLazy(console, page,
				configuration.getAttribute(RConsoleLaunching.ATTR_PIN_CONSOLE, false));
		
		new ToolRunner().runInBackgroundThread(process, new WorkbenchStatusHandler());
		
		if (monitor != null) {
			monitor.done();
		}
	}
	
	
	protected void addJavaConf(final ILaunchConfiguration configuration,
			final Map<String, String> envVariables) {
		try {
			final IVMInstall vmInstall= JavaRuntime.computeVMInstall(configuration);
			if (vmInstall == null) {
				return;
			}
			final File vmLocation= vmInstall.getInstallLocation();
			if (vmLocation != null) {
				envVariables.put("JAVA_HOME", vmLocation.getAbsolutePath()); //$NON-NLS-1$
			}
		}
		catch (final CoreException e) {
		}
	}
	
}
