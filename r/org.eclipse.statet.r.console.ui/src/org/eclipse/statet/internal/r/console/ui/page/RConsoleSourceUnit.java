/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.page;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.core.util.AbstractFragmentDocument;
import org.eclipse.statet.ecommons.text.core.util.TextUtils;

import org.eclipse.statet.internal.r.ui.RUIPlugin;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.element.SourceDocumentRunnable;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.nico.ui.console.GenericConsoleSourceUnit;
import org.eclipse.statet.r.console.ui.RConsole;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.build.RSourceUnitModelContainer;
import org.eclipse.statet.r.core.source.doc.RDocumentContentInfo;


@NonNullByDefault
public class RConsoleSourceUnit extends GenericConsoleSourceUnit implements RSourceUnit {
	
	
	private final RConsole rConsole;
	
	private final RSourceUnitModelContainer model= new RSourceUnitModelContainer(this, 
			RUIPlugin.getInstance().getRDocumentProvider() ) {
		
		@Override
		public SourceContent getParseContent(final IProgressMonitor monitor) {
			final AbstractFragmentDocument doc= getDocument();
			synchronized (TextUtils.getLockObject(doc)) {
				return new SourceContent(doc.getModificationStamp(),
						doc.getMasterDocument().get(), -doc.getOffsetInMasterDocument() );
			}
		}
		
	};
	
	
	public RConsoleSourceUnit(final RConsolePage page, final AbstractFragmentDocument document) {
		super(page.toString(), document);
		this.rConsole= page.getConsole();
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return RDocumentContentInfo.INSTANCE;
	}
	
	
	@Override
	public @Nullable AstInfo getAstInfo(final @Nullable String type, final boolean ensureSync,
			final IProgressMonitor monitor) {
		if (type == null || this.model.isContainerFor(type)) {
			return this.model.getAstInfo(ensureSync, monitor);
		}
		return null;
	}
	
	@Override
	public @Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int flags,
			final IProgressMonitor monitor) {
		if (type == null || this.model.isContainerFor(type)) {
			return this.model.getModelInfo(flags, monitor);
		}
		return null;
	}
	
	@Override
	public void syncExec(final SourceDocumentRunnable runnable) throws InvocationTargetException {
		throw new UnsupportedOperationException();
	}
	
	public RCoreAccess getRCoreAccess() {
		return this.rConsole;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RCoreAccess.class) {
			return (T)getRCoreAccess();
		}
		if (adapterType == PreferenceAccess.class) {
			return (T)getRCoreAccess().getPrefs();
		}
		return super.getAdapter(adapterType);
	}
	
	
}
