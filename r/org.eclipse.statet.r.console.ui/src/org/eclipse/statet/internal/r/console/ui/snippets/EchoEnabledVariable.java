/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.snippets;

import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.variables.core.DynamicVariable;

import org.eclipse.statet.r.launching.RCodeLaunching;


public class EchoEnabledVariable extends DynamicVariable {
	
	
	public EchoEnabledVariable() {
		super(RSnippets.ECHO_ENABLED_VARIABLE);
	}
	
	
	@Override
	public String getValue(final String argument) throws CoreException {
		final Boolean echo= EPreferences.getInstancePrefs().getPreferenceValue(
				RCodeLaunching.ECHO_ENABLED_PREF );
		return (echo != null && echo.booleanValue()) ?
				"TRUE" : "FALSE"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
