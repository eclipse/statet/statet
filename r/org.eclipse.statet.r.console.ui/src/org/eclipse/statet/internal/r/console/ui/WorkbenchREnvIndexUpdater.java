/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui;

import java.nio.file.Path;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.r.console.core.IRBasicAdapter;
import org.eclipse.statet.rhelp.core.REnvHelpConfiguration;
import org.eclipse.statet.rhelp.core.RHelpManager;
import org.eclipse.statet.rhelp.core.update.REnvIndexUpdater;
import org.eclipse.statet.rj.renv.runtime.RPkgManager;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
public class WorkbenchREnvIndexUpdater extends REnvIndexUpdater {
	
	
	private class WorkbenchIndexJob extends IndexJob {
		
		private final Job workbenchJob;
		
		public WorkbenchIndexJob(final String name) {
			this.workbenchJob= new Job(name) {
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					return EStatusUtils.convert(
							WorkbenchIndexJob.this.run(EStatusUtils.convert(monitor)) );
				}
			};
			this.workbenchJob.setPriority(Job.LONG);
			this.workbenchJob.setSystem(true);
			this.workbenchJob.schedule();
		}
		
		@Override
		protected void cancel() {
			this.workbenchJob.cancel();
		}
		
		@Override
		protected void join() {
			while (true) {
				try {
					this.workbenchJob.join();
					return;
				}
				catch (final InterruptedException e) {}
			}
		}
		
	}
	
	
	public WorkbenchREnvIndexUpdater(final REnvHelpConfiguration rEnvConfig,
			final RHelpManager rHelpManager, final RPkgManager rPkgManager) {
		super(rEnvConfig, rHelpManager, rPkgManager);
	}
	
	
	@Override
	protected IndexJob scheduleIndexJob(final String name) {
		return new WorkbenchIndexJob(name);
	}
	
	@Override
	protected Path toSystemPath(final String rPath,
			final RService r, final ProgressMonitor m) throws StatusException {
		if (r instanceof IRBasicAdapter) {
			return ((IRBasicAdapter)r).getWorkspace().toSystemPath(rPath);
		}
		return super.toSystemPath(rPath, r, m);
	}
	
}
