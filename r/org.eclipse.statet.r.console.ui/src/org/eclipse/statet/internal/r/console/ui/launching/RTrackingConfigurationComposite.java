/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.ui.util.VariableFilterUtils;

import org.eclipse.statet.nico.core.NicoVariables;
import org.eclipse.statet.nico.core.util.TrackingConfiguration;
import org.eclipse.statet.nico.ui.util.TrackingConfigurationComposite;
import org.eclipse.statet.r.console.core.RWorkspace;


public class RTrackingConfigurationComposite extends TrackingConfigurationComposite {
	
	
	static final String TRANSCRIPT_TRACKING_DEFAULT_PATH= "${"+NicoVariables.SESSION_STARTUP_WD_VARNAME+"}/${"+NicoVariables.SESSION_STARTUP_DATE_VARNAME+":yyyy-MM-dd HH-mm-ss}.Rtr"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	
	
	public RTrackingConfigurationComposite(final Composite parent) {
		super(parent);
	}
	
	@Override
	protected void configure() {
		addSaveTemplate(new SaveTemplate(".Rtr file with timestamp", TRANSCRIPT_TRACKING_DEFAULT_PATH));
	}
	
	
	@Override
	protected void create() {
		super.create();
		
		getPathInput().setShowInsertVariable(true, VariableFilterUtils.DEFAULT_NON_ITERACTIVE_FILTERS,
				RWorkspace.ADDITIONAL_R_VARIABLES);
	}
	
	
	@Override
	public void setInput(final TrackingConfiguration config) {
		super.setInput(config);
		final String id= config.getId();
		setLabelEnabled(id.startsWith(RConsoleOptionsTab.CUSTOM_TRACKING_ID_PREFIX));
	}
	
}
