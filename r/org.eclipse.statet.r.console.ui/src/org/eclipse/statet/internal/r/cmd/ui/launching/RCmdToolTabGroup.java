/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.cmd.ui.launching;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.debug.ui.RefreshTab;

import org.eclipse.statet.ecommons.debug.ui.util.CheckedCommonTab;

import org.eclipse.statet.r.launching.ui.EnvironmentTabForR;
import org.eclipse.statet.r.launching.ui.REnvTab;


/**
 * 
 */
public class RCmdToolTabGroup extends AbstractLaunchConfigurationTabGroup {
	
	
	public RCmdToolTabGroup() {
	}
	
	
	@Override
	public void createTabs(final ILaunchConfigurationDialog dialog, final String mode) {
		final ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] {
				new RCmdMainTab(),
				new REnvTab(true),
				new RefreshTab(),
				new EnvironmentTabForR(),
				new CheckedCommonTab()
		};
		setTabs(tabs);
	}
	
}
