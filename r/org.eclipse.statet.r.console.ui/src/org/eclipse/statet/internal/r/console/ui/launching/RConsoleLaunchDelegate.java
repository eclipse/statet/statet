/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.ecommons.debug.core.util.LaunchUtils;

import org.eclipse.statet.internal.r.console.ui.RConsoleUIPlugin;
import org.eclipse.statet.r.console.ui.launching.AbstractRConsoleLaunchDelegate;
import org.eclipse.statet.r.console.ui.launching.RConsoleLaunching;


public class RConsoleLaunchDelegate extends AbstractRConsoleLaunchDelegate {
	
	
	public RConsoleLaunchDelegate() {
	}
	
	
	@Override
	public void launch(final ILaunchConfiguration configuration, final String mode, final ILaunch launch,
			final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= LaunchUtils.initProgressMonitor(configuration, monitor, 10);
		try {
			if (m.isCanceled()) {
				return;
			}
			
			final String type= configuration.getAttribute(RConsoleLaunching.ATTR_TYPE, ""); //$NON-NLS-1$
			if (type.equals(RConsoleLaunching.LOCAL_RTERM) || type.equals("rterm")) { //$NON-NLS-1$
				new RConsoleRTermLaunchDelegate().launch(configuration, mode, launch, m);
				return;
			}
			if (type.equals(RConsoleLaunching.LOCAL_RJS)) {
				new RConsoleRJLaunchDelegate().launch(configuration, mode, launch, m);
				return;
			}
			throw new CoreException(new Status(IStatus.ERROR, RConsoleUIPlugin.BUNDLE_ID, 0,
					NLS.bind("R Console launch type ''{0}'' is not available.", type),
					null ));
		}
		finally {
			m.done();
		}
	}
	
}
