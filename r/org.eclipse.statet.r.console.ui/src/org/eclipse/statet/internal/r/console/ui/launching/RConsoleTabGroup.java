/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.console.ui.launching;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.debug.ui.sourcelookup.SourceLookupTab;
import org.eclipse.jdt.debug.ui.launchConfigurations.JavaClasspathTab;

import org.eclipse.statet.nico.ui.util.CommonTabForConsole;
import org.eclipse.statet.r.launching.ui.EnvironmentTabForR;
import org.eclipse.statet.r.launching.ui.REnvTab;


public class RConsoleTabGroup extends AbstractLaunchConfigurationTabGroup {
	
	
	public static class ExtJavaClasspathTab extends JavaClasspathTab {
		
		@Override
		public String getName() {
			return "Java "+super.getName(); //$NON-NLS-1$
		}
		
	}
	
	
	public RConsoleTabGroup() {
	}
	
	
	@Override
	public void createTabs(final ILaunchConfigurationDialog dialog, final String mode) {
		final RConsoleMainTab mainTab= new RConsoleMainTab();
		final REnvTab renvTab= new REnvTab(true);
		final boolean jdt= true;
		
		final ILaunchConfigurationTab[] tabs= jdt ? new ILaunchConfigurationTab[] {
				mainTab,
				renvTab,
				new RConsoleOptionsTab(false),
				new EnvironmentTabForR(),
				
				new ExtJavaJRETab(mainTab, renvTab),
				new ExtJavaClasspathTab(),
				new SourceLookupTab(),
				
				new CommonTabForConsole()
		} : new ILaunchConfigurationTab[] {
				mainTab,
				renvTab,
				new EnvironmentTabForR(),
				
				new CommonTabForConsole()
		};
		setTabs(tabs);
	}
	
}
