/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.debug.ui;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String UnterminatedLaunchAlerter_WorkbenchClosing_title;
	public static String UnterminatedLaunchAlerter_WorkbenchClosing_message;
	public static String UnterminatedLaunchAlerter_WorkbenchClosing_button_Continue;
	public static String UnterminatedLaunchAlerter_WorkbenchClosing_button_Cancel;
	
	public static String HelpRequestor_Close_name;
	public static String HelpRequestor_Close_tooltip;
	public static String HelpRequestor_Task_name;
	public static String HelpRequestor_error_WhenRunProcess_message;
	public static String HelpRequestor_error_WhenReadOutput_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
