/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.util;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.ui.CommonTab;


public class CheckedCommonTab extends CommonTab {
	
	
	private boolean fLoading;
	
	
	public CheckedCommonTab() {
	}
	
	
	@Override
	public void initializeFrom(final ILaunchConfiguration configuration) {
		this.fLoading = true;
		try {
			super.initializeFrom(configuration);
		}
		finally {
			this.fLoading = false;
			scheduleUpdateJob(); // E-3.7 still necessary?
		}
	}
	
	@Override
	protected long getUpdateJobDelay() {
		return (this.fLoading) ? 10000 : super.getUpdateJobDelay();
	}
	
}
