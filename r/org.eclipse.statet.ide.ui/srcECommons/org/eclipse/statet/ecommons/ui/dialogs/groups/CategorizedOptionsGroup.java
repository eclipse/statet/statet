/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs.groups;

import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Scrollable;

import org.eclipse.statet.ecommons.ui.util.PixelConverter;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


public abstract class CategorizedOptionsGroup<ItemT extends CategorizedOptionsGroup.CategorizedItem> 
		extends StructuredSelectionOptionsGroup<TreeViewer, ItemT> {

	
	public static class CategorizedItem {

		private int fCategoryIndex;
		private final String fName;
		
		public CategorizedItem(final String name) {
			
			this.fName = name;
		}
		
		public String getName() {
			
			return this.fName;
		}

		protected void setCategory(final int index) {
			
			this.fCategoryIndex = index;
		}
		
		public int getCategoryIndex() {
			
			return this.fCategoryIndex;
		}
	}

	private class CategorizedItemLabelProvider extends LabelProvider {

		@Override
		public String getText(final Object element) {
			if (element instanceof String) {
				return (String) element;
			}
			return ((CategorizedItem) element).getName(); // ItemT
		}
	}

	private class CategorizedItemContentProvider implements ITreeContentProvider {
	
		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
		}

		@Override
		public Object[] getElements(final Object inputElement) {
			
			return CategorizedOptionsGroup.this.fCategorys;
		}

		@Override
		public boolean hasChildren(final Object element) {

			return element instanceof String;
		}

		@Override
		public Object[] getChildren(final Object parentElement) {
			
			if (parentElement instanceof String) {
				final int idx = getIndexOfCategory(parentElement);
				return CategorizedOptionsGroup.this.fCategoryChilds[idx];
			}
			
			return new Object[0];
		}

		@Override
		public Object getParent(final Object element) {
			
			if (element instanceof String) {
				return null;
			}
			
			final int idx = ((CategorizedItem) element).getCategoryIndex();
			return CategorizedOptionsGroup.this.fCategorys[idx];
		}
	}
	
	public String[] fCategorys;
	public ItemT[][] fCategoryChilds;
	
	public CategorizedOptionsGroup(final boolean grabSelectionHorizontal, final boolean grabVertical) {
		super(grabSelectionHorizontal, grabVertical);
	}
	
	@Override
	protected TreeViewer createSelectionViewer(final Composite parent) {
		
		final TreeViewer viewer = new TreeViewer(parent, SWT.BORDER | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setLabelProvider(new CategorizedItemLabelProvider());

		return viewer;
	}
	
	@Override
	protected GridData createSelectionGridData() {
		
		final GridData gd = super.createSelectionGridData();
		
		final Control control = getStructuredViewer().getControl();
		final PixelConverter pixel = new PixelConverter(control);
		gd.heightHint = pixel.convertHeightInCharsToPixels(9);
		int maxWidth = 0;
		for (final CategorizedItem item : getListModel()) {
			maxWidth = Math.max(maxWidth, pixel.convertWidthInCharsToPixels(item.getName().length()));
		}
		final ScrollBar vBar = ((Scrollable) control).getVerticalBar();
		if (vBar != null)
		 {
			maxWidth += vBar.getSize().x * 4; // scrollbars and tree indentation guess
		}
		gd.widthHint = maxWidth;
		
		return gd;
	}
	
	@Override
	protected IContentProvider createContentProvider() {
		
		return new CategorizedItemContentProvider();
	}
	
	@Override
	protected IDoubleClickListener createDoubleClickListener() {
		
		return new IDoubleClickListener() {
			@Override
			public void doubleClick(final DoubleClickEvent event) {
				final IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				if (selection != null && selection.size() == 1) {
					final Object item = selection.getFirstElement();
					if (item instanceof String) {
						if (getStructuredViewer().getExpandedState(item)) {
							getStructuredViewer().collapseToLevel(item, AbstractTreeViewer.ALL_LEVELS);
						}
						else {
							getStructuredViewer().expandToLevel(item, 1);
						}
					}
					else {
						handleDoubleClick(getSingleItem(selection), selection);
					}
				}
			}
		};
	}
	
	@Override
	public void initFields() {
		super.initFields();
		
		getStructuredViewer().getControl().getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				final TreeViewer viewer = getStructuredViewer();
				if (viewer != null && UIAccess.isOkToUse(viewer)) {
					viewer.setSelection(new StructuredSelection(CategorizedOptionsGroup.this.fCategorys[0]));
				}
			}
		});
	}
	
	@Override
	public ItemT getSingleItem(final IStructuredSelection selection) {
		
		if (selection.getFirstElement() instanceof String) {
			return null;
		}
		return super.getSingleItem(selection);
	}

	
	public void generateListModel() {

		for (int i = 0; i < this.fCategorys.length; i++) {
			for (int j = 0; j < this.fCategoryChilds[i].length; j++) {
				this.fCategoryChilds[i][j].setCategory(i);
				getListModel().add(this.fCategoryChilds[i][j]);
			}
		}
	}
	
	public int getIndexOfCategory(final Object category) {
		
		for (int i = 0; i < this.fCategorys.length; i++) {
			if (this.fCategorys[i] == category) {
				return i;
			}
		}
		return -1;
	}
	
}
