/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.workbench.search.ui;


public class LineElement<E> implements IMatchGroup<E> {
	
	
	private final E element;
	
	private final int lineNumber;
	private final int startOffset;
	private final String text;
	
	
	public LineElement(final E element,
			final int lineNumber, final int lineStartOffset, final String text) {
		this.element= element;
		this.lineNumber= lineNumber;
		this.startOffset= lineStartOffset;
		this.text= text;
	}
	
	
	@Override
	public E getElement() {
		return this.element;
	}
	
	public int getLine() {
		return this.lineNumber;
	}
	
	public String getText() {
		return this.text;
	}
	
	public int getOffset() {
		return this.startOffset;
	}
	
	public boolean contains(final int offset) {
		return this.startOffset <= offset && offset < this.startOffset + this.text.length();
	}
	
	public int getLength() {
		return this.text.length();
	}
	
}
