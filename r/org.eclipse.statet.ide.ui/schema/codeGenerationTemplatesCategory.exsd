<?xml version='1.0' encoding='UTF-8'?>
<!--
 #=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<schema targetNamespace="org.eclipse.statet.base.ui" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appinfo>
         <meta.schema plugin="org.eclipse.statet.base.ui"
               id="codeGenerationTemplatesCategory"
               name="(Deprecated) Adds a type of templates to the central edit-dialog for codegeneration-templates"/>
      </appinfo>
      <documentation>
         This extension-point allows to add a new category of templates to the central dialog for editing codegeneration-templates
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appinfo>
            <meta.element />
         </appinfo>
      </annotation>
      <complexType>
         <sequence>
            <element ref="category" minOccurs="0" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="category">
      <annotation>
         <appinfo>
            <meta.element labelAttribute="name"/>
         </appinfo>
      </annotation>
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  ID, which identify the category. All template-IDs in the template-store must start with this category-ID. The category-ID must not be a substring of another category-ID.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  UI-name of template-category (root-node in treelist).
               </documentation>
            </annotation>
         </attribute>
         <attribute name="providerClass" type="string" use="required">
            <annotation>
               <documentation>
                  An implementation of &lt;code&gt;org.eclipse.statet.base.ext.templates.ICodeGenerationTemplateCategory&lt;/code&gt;
               </documentation>
               <appinfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.statet.base.ext.templates.ICodeGenerationTemplateCategory"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appinfo>
         <meta.section type="since"/>
      </appinfo>
      <documentation>
         0.6
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="examples"/>
      </appinfo>
      <documentation>
         See usage in plugin &lt;code&gt;org.eclipse.statet.r.ui&lt;/code&gt;
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="copyright"/>
      </appinfo>
      <documentation>
Copyright (c) 2005, 2025 Stephan Wahlbrink and others.

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
which is available at https://www.apache.org/licenses/LICENSE-2.0.

SPDX-License-Identifier: EPL-2.0 OR Apache-2.0

Contributors:
    Stephan Wahlbrink &lt;sw@wahlbrink.eu&gt; - initial API and implementation
      </documentation>
   </annotation>

</schema>
