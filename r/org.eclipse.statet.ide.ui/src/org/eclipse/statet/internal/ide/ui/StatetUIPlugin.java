/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ide.ui;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.base.ui.StatetImages;


/**
 * The main plugin class to be used in the desktop.
 */
public class StatetUIPlugin extends AbstractUIPlugin {
	
	/**
	 * Plugin-ID
	 * Value: @value
	 */
	public static final String BUNDLE_ID= "org.eclipse.statet.ide.ui"; //$NON-NLS-1$
	
	
	public static void log(final IStatus status) {
		if (status != null) {
			getInstance().getLog().log(status);
		}
	}
	
	public static void logError(final String message, final Throwable e) {
		log(new Status(IStatus.ERROR, BUNDLE_ID, message, e));
	}
	
	public static void logUnexpectedError(final Throwable e) {
		log(new Status(IStatus.ERROR, BUNDLE_ID, 0, StatetMessages.InternalError_UnexpectedException, e));
	}
	
	
	private static StatetUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static StatetUIPlugin getInstance() {
		return instance;
	}
	
	
	private List<Disposable> fDisposables;
	
	
	/**
	 * The constructor.
	 */
	public StatetUIPlugin() {
		instance= this;
	}
	
	
	/**
	 * This method is called upon plug-in activation
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		this.fDisposables= new ArrayList<>();
		super.start(context);
	}
	
	/**
	 * This method is called when the plug-in is stopped
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			for (final Disposable d : this.fDisposables) {
				try {
					d.dispose();
				}
				catch (final Throwable e) {
					logError("Error occured when dispose module", e); //$NON-NLS-1$
				}
			}
			this.fDisposables = null;
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(StatetImages.OBJ_IMPORT, ImageRegistryUtil.T_OBJ, "ltk-import.png"); //$NON-NLS-1$
		util.register(StatetImages.OBJ_CLASS, ImageRegistryUtil.T_OBJ, "ltk-class.png"); //$NON-NLS-1$
		util.register(StatetImages.OBJ_CLASS_EXT, ImageRegistryUtil.T_OBJ, "ltk-class_ext.png"); //$NON-NLS-1$
		
		util.register(StatetImages.TOOL_REFRESH, ImageRegistryUtil.T_TOOL, "refresh.png"); //$NON-NLS-1$
		util.register(StatetImages.TOOLD_REFRESH, ImageRegistryUtil.T_TOOLD, "refresh.png"); //$NON-NLS-1$
	}
	
	public void registerPluginDisposable(final Disposable d) {
		final List<Disposable> disposables= this.fDisposables;
		if (disposables != null) {
			disposables.add(d);
		}
		else {
			throw new IllegalStateException();
		}
	}
	
}
