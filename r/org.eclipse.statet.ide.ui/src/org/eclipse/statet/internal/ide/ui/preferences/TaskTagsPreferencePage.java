/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ide.ui.preferences;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.PropertyAndPreferencePage;

import org.eclipse.statet.base.core.StatetProject;
import org.eclipse.statet.internal.ide.ui.StatetUIPlugin;


/**
 * The page to configure the task tags
 */
public class TaskTagsPreferencePage extends PropertyAndPreferencePage {
	
	public static final String PREF_ID = "org.eclipse.statet.base.ui.preferencePages.TaskTags"; //$NON-NLS-1$
	public static final String PROP_ID = "org.eclipse.statet.base.propertyPages.TaskTags"; //$NON-NLS-1$
	
	
	public TaskTagsPreferencePage() {
		setPreferenceStore(StatetUIPlugin.getInstance().getPreferenceStore());
		
		// only used when page is shown programatically
		setTitle(Messages.TaskTags_title); 
		setDescription(Messages.TaskTags_description);
	}
	
	@Override
	protected String getPreferencePageID() {
		return PREF_ID;
	}
	
	@Override
	protected String getPropertyPageID() {
		return PROP_ID;
	}
	
	@Override
	protected boolean isProjectSupported(final IProject project) throws CoreException {
		return project.hasNature(StatetProject.NATURE_ID);
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() 
			throws CoreException {
		return new TaskTagsConfigurationBlock(getProject(), createStatusChangedListener());
	}
	
	@Override
	protected boolean hasProjectSpecificSettings(final IProject project) {
		return ((ManagedConfigurationBlock) getBlock()).hasProjectSpecificOptions(project);
	}
	
}
