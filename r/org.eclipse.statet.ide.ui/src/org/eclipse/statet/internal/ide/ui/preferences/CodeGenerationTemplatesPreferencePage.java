/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ide.ui.preferences;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.PropertyAndPreferencePage;

import org.eclipse.statet.base.core.StatetProject;
import org.eclipse.statet.internal.ide.ui.StatetUIPlugin;


/**
 * The page to configure the code templates.
 */
public class CodeGenerationTemplatesPreferencePage extends PropertyAndPreferencePage {
	
	public static final String PREF_ID = "org.eclipse.statet.base.preferencePages.CodeGenerationTemplates"; //$NON-NLS-1$
	public static final String PROP_ID = "org.eclipse.statet.base.propertyPages.CodeGenerationTemplates"; //$NON-NLS-1$
	
//	public static final String DATA_SELECT_TEMPLATE = "CodeTemplatePreferencePage.select_template"; //$NON-NLS-1$
	
	
	public CodeGenerationTemplatesPreferencePage() {
		setPreferenceStore(StatetUIPlugin.getInstance().getPreferenceStore());
		
		// only used when page is shown programatically
		setTitle(Messages.CodeTemplates_title);		 
	}
	
	
	@Override
	protected String getPreferencePageID() {
		return PREF_ID;
	}
	
	@Override
	protected String getPropertyPageID() {
		return PROP_ID;
	}
	
	@Override
	protected boolean isProjectSupported(final IProject project) throws CoreException {
		return project.hasNature(StatetProject.NATURE_ID);
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock()
			throws CoreException {
		return new CodeGenerationTemplatesConfigurationBlock(getProject());
	}
	
	@Override
	public void applyData(final Object data) {
//		if (data instanceof Map) {
//			Object id = ((Map) data).get(DATA_SELECT_TEMPLATE);
//			if (id instanceof String) {
//				System.out.println("todo");
//				final TemplatePersistenceData[] templates = block.fTemplateStore.getTemplateData();
//				TemplatePersistenceData template = null;
//				for (int index = 0; index < templates.length; index++) {
//					template = templates[index];
//					if (template.getId().equals(id)) {
//						block.postSetSelection(template);
//						break;
//					}
//				}
//			}
//		}
		super.applyData(data);
	}
	
	@Override
	protected boolean hasProjectSpecificSettings(final IProject project) {
		return CodeGenerationTemplatesStore.hasProjectSpecificTempates(project);
	}
	
}
