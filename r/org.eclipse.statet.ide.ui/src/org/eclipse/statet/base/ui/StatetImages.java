/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.base.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.internal.ide.ui.StatetUIPlugin;


public class StatetImages {
	// Image files must be registered in StatetUIPlugin
	
	public static final String OBJ_IMPORT= StatetUIPlugin.BUNDLE_ID + "/images/obj/import"; //$NON-NLS-1$
	public static final String OBJ_CLASS= StatetUIPlugin.BUNDLE_ID + "/images/obj/class"; //$NON-NLS-1$
	public static final String OBJ_CLASS_EXT= StatetUIPlugin.BUNDLE_ID + "/images/obj/class_ext"; //$NON-NLS-1$
	
	public static final String TOOL_REFRESH= StatetUIPlugin.BUNDLE_ID + "/images/tool/refresh"; //$NON-NLS-1$
	public static final String TOOLD_REFRESH= StatetUIPlugin.BUNDLE_ID + "/images/toold/refresh"; //$NON-NLS-1$
	
	
	public static ImageDescriptor getDescriptor(final String key) {
		return StatetUIPlugin.getInstance().getImageRegistry().getDescriptor(key);
	}
	
	public static Image getImage(final String key) {
		return StatetUIPlugin.getInstance().getImageRegistry().get(key);
	}
	
	
	private StatetImages() {}
	
}
