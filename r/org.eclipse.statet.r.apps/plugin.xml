<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.4"?>
<!--
 #=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<plugin>
   
   <extension
         point="org.eclipse.core.resources.natures"
         id="org.eclipse.statet.r.apps.resourceProjects.RApp"
         name="R App&#x2002;[StatET]">
      <requires-nature
            id="org.eclipse.statet.r.resourceProjects.R"/>
      <runtime>
         <run
               class="org.eclipse.statet.intern.r.apps.core.RAppProject"/>
      </runtime>
   </extension>
   
   <extension
         point="org.eclipse.core.expressions.propertyTesters">
      <propertyTester
            id="org.eclipse.statet.r.apps.expressionProperties.RShinyResources"
            namespace="org.eclipse.statet.r.apps"
            properties="isRShinyAppResource"
            type="org.eclipse.core.resources.IResource"
            class="org.eclipse.statet.internal.r.apps.core.shiny.RShinyResourceTester">
      </propertyTester>
   </extension>
   <extension
         point="org.eclipse.core.expressions.definitions">
      <definition
            id="org.eclipse.statet.r.apps.expressions.isEditorInputActive.RShinyResource">
         <with
               variable="activeEditorInput">
            <adapt
                  type="org.eclipse.core.resources.IResource">
               <test
                     property="org.eclipse.core.resources.projectNature"
                     value="org.eclipse.statet.r.resourceProjects.R"/>
               <or>
                  <test
                        property="org.eclipse.core.resources.projectNature"
                        value="org.eclipse.statet.r.apps.resourceProjects.RApp"/>
                  <test
                        property="org.eclipse.statet.r.apps.isRShinyAppResource"
                        forcePluginActivation="true"/>
               </or>
            </adapt>
         </with>
      </definition>
      <definition
            id="org.eclipse.statet.r.apps.expressions.isSelectionActive.RShinyResource">
         <with
               variable="selection">
            <count
                  value="1"/>
            <iterate>
               <adapt
                     type="org.eclipse.core.resources.IResource">
                  <test
                        property="org.eclipse.core.resources.projectNature"
                        value="org.eclipse.statet.r.resourceProjects.R"/>
                  <or>
                     <test
                           property="org.eclipse.core.resources.projectNature"
                           value="org.eclipse.statet.r.apps.resourceProjects.RApp"/>
                     <test
                           property="org.eclipse.statet.r.apps.isRShinyAppResource"
                           forcePluginActivation="true"/>
                  </or>
               </adapt>
            </iterate>
         </with>
      </definition>
   </extension>
   
   <extension
         point="org.eclipse.statet.ecommons.ts.UIDecorators">
      <runnable
            typeId="org.eclipse.statet.r.apps/RunApp"
            icon="icons/obj_16/r_app.png">
      </runnable>
      
   </extension>
   
   <extension
         point="org.eclipse.debug.core.launchConfigurationTypes">
      <launchConfigurationType
            id="org.eclipse.statet.r.apps.launchConfigurations.RAppControl"
            name="%launchConfigurations_RApp_name"
            modes="run"
            delegate="org.eclipse.statet.internal.r.apps.ui.launching.AppControlLaunchDelegate">
      </launchConfigurationType>
   </extension>
   
   <extension
         point="org.eclipse.debug.ui.launchConfigurationTypeImages">
      <launchConfigurationTypeImage
            id="org.eclipse.statet.r/images/tool/RApp"
            configTypeID="org.eclipse.statet.r.apps.launchConfigurations.RAppControl"
            icon="icons/obj_16/r_app.png">
      </launchConfigurationTypeImage>
   </extension>
   <extension
         point="org.eclipse.debug.ui.launchConfigurationTabGroups">
      <launchConfigurationTabGroup
            id="org.eclipse.statet.r.apps.launchConfigurationTabGroups.RAppControl"
            type="org.eclipse.statet.r.apps.launchConfigurations.RAppControl"
            class="org.eclipse.statet.internal.r.apps.ui.launching.AppControlConfigTabGroup">
      </launchConfigurationTabGroup>
   </extension>
   
   <extension
         point="org.eclipse.debug.ui.launchShortcuts">
      <shortcut
            id="org.eclipse.statet.r.launchShortcuts.RShinyApp"
            label="%launchShortcuts_RShinyApp_name"
            icon="icons/tool_16/run-r_app.png"
            class="org.eclipse.statet.internal.r.apps.ui.shiny.actions.AppLaunchShortcut"
            modes="run">
         <configurationType
               id="org.eclipse.statet.r.apps.launchConfigurations.RAppControl"/>
         <contextualLaunch>
            <enablement>
               <reference
                     definitionId="org.eclipse.statet.r.apps.expressions.isSelectionActive.RShinyResource"/>
            </enablement>
         </contextualLaunch>
      </shortcut>
   </extension>
   
   <extension
         point="org.eclipse.ui.commands">
      <command
            id="org.eclipse.statet.r.apps.commands.RunAppDefault"
            categoryId="org.eclipse.debug.ui.category.run"
            name="%commands_RunApp_name"
            description="%commands_RunApp_description">
      </command>
   </extension>
   <extension
         point="org.eclipse.ui.commandImages">
      <image
            commandId="org.eclipse.statet.r.apps.commands.RunAppDefault"
            icon="icons/tool_16/run-r_app.png">
      </image>
   </extension>
   <extension
         point="org.eclipse.ui.handlers">
      <handler
            commandId="org.eclipse.statet.r.apps.commands.RunAppDefault"
            class="org.eclipse.statet.internal.r.apps.ui.shiny.actions.RunActiveAppConfigWorkbenchHandler">
         <activeWhen>
            <reference
                  definitionId="org.eclipse.statet.r.apps.expressions.isEditorInputActive.RShinyResource"/>
         </activeWhen>
      </handler>
   </extension>
   
   <extension
         point="org.eclipse.ui.bindings">
      <key
            commandId="org.eclipse.statet.r.apps.commands.RunAppDefault"
            contextId="org.eclipse.ui.contexts.window"
            schemeId="org.eclipse.ui.defaultAcceleratorConfiguration"
            sequence="M1+R A">
      </key>
   </extension>
   
   <extension
         point="org.eclipse.ui.menus">
      <menuContribution
            locationURI="toolbar:org.eclipse.ui.main.toolbar?before=org.eclipse.ui.workbench.navigate">
         <toolbar
               id="org.eclipse.statet.r.apps.menus.AppTools"
               label="%toolbars_AppTools_label">
            <separator
                  name="run_app"
                  visible="false">
            </separator>
            <command
                  id="org.eclipse.statet.r.apps.menus.RunApp"
                  commandId="org.eclipse.statet.r.apps.commands.RunAppDefault"
                  style="pulldown">
               <visibleWhen>
                  <reference
                        definitionId="org.eclipse.statet.r.apps.expressions.isEditorInputActive.RShinyResource"/>
               </visibleWhen>
            </command>
            <separator
                  name="additions"
                  visible="false">
            </separator>
         </toolbar>
      </menuContribution>
      <menuContribution
            locationURI="menu:org.eclipse.statet.r.apps.menus.RunApp">
         <dynamic
               id="org.eclipse.statet.r.apps.menus.RunAppItems"
               class="org.eclipse.statet.internal.r.apps.ui.shiny.actions.RunAppConfigsDropdownContribution">
         </dynamic>
         <separator
               name="additions"
               visible="true">
         </separator>
      </menuContribution>
   </extension>
   
   <extension
         point="org.eclipse.ui.views">
      <view
            id="org.eclipse.statet.r.apps.views.AppViewer"
            category="org.eclipse.statet.workbench.views.StatetCategory"
            name="%views_AppViewer_name"
            icon="icons/obj_16/r_app.png"
            class="org.eclipse.statet.internal.r.apps.ui.viewer.AppBrowserView"
            restorable="true">
      </view>
   </extension>
   <extension
         point="org.eclipse.ui.views">
      <view
            id="org.eclipse.statet.r.apps.views.VariableViewer"
            category="org.eclipse.statet.workbench.views.StatetCategory"
            name="%views_AppVarBrowser_name"
            icon="icons/view_16/r_app_var_browser.png"
            class="org.eclipse.statet.internal.r.apps.ui.variables.AppVarView"
            restorable="true">
      </view>
   </extension>
   
</plugin>
