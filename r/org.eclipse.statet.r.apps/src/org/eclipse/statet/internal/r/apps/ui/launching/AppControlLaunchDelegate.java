/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.launching;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;
import org.eclipse.debug.ui.DebugUITools;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class AppControlLaunchDelegate extends LaunchConfigurationDelegate {
	
	
	public AppControlLaunchDelegate() {
	}
	
	
	@Override
	protected IProject @Nullable [] getBuildOrder(final ILaunchConfiguration configuration,
			final String mode) throws CoreException {
		final IResource resource= DebugUITools.getSelectedResource();
		final IProject project;
		if (resource != null && (project= resource.getProject()) != null) {
			return computeReferencedBuildOrder(new IProject[] { project });
		}
		return null;
	}
	
	
	@Override
	public void launch(final ILaunchConfiguration configuration,
			final String mode, final @Nullable ILaunch launch,
			final @Nullable IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, "Configuring App Operation...", 3 + 3);
		
		final AppControlLaunchConfig config= new AppControlLaunchConfig();
		config.initSource(configuration, m.newChild(1));
		config.initAddress(configuration, m.newChild(1));
		config.initOperation(configuration, m.newChild(1));
		
		final AppRunner runner= new AppRunner(config);
		runner.startApp(config.getWorkbenchPage());
	}
	
}
