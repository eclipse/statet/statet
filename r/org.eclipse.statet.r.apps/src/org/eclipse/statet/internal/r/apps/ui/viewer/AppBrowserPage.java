/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.viewer;

import static org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI.TERMINATE_COMMAND_ID;
import static org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI.TERMINATE_RELAUNCH_COMMAND_ID;

import static org.eclipse.statet.internal.r.apps.ui.viewer.AppBrowserView.APP_CONTROL_GROUP_ID;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;
import org.eclipse.statet.ecommons.ui.mpbv.PageBookBrowserPage;
import org.eclipse.statet.ecommons.ui.workbench.ContextHandlers;

import org.eclipse.statet.internal.r.apps.ui.Messages;
import org.eclipse.statet.r.apps.ui.RApp;


@NonNullByDefault
public class AppBrowserPage extends PageBookBrowserPage {
	
	
	private class StopAppHandler extends AbstractHandler {
		
		@Override
		public void setEnabled(final @Nullable Object evaluationContext) {
			final AppBrowserSession session= getSession();
			final RApp app= session.getLatestApp();
			setBaseEnabled(app != null && app.canStopApp());
		}
		
		@Override
		public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
			final AppBrowserSession session= getSession();
			final RApp app= session.getLatestApp();
			if (app != null) {
				app.stopApp();
			}
			return null;
		}
		
	}
	
	private class RestartAppHandler extends AbstractHandler {
		
		@Override
		public void setEnabled(final @Nullable Object evaluationContext) {
			final AppBrowserSession session= getSession();
			final RApp app= session.getLatestApp();
			setBaseEnabled(app != null && app.canRestartApp());
		}
		
		@Override
		public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
			try {
				final AppBrowserSession session= getSession();
				final RApp app= session.getLatestApp();
				if (app != null) {
					app.restartApp(getSite().getPage());
				}
				return null;
			}
			catch (final CoreException e) {
				// show message?
				return null;
			}
		}
		
	}
	
	
	public AppBrowserPage(final AppBrowserView view, final AppBrowserSession session) {
		super(view, session);
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}
	
	
	@Override
	public AppBrowserSession getSession() {
		return (AppBrowserSession) super.getSession();
	}
	
	
	@Override
	protected @Nullable Control createAddressBar(final Composite parent) {
		return null;
	}
	
	@Override
	protected void initActions(final IServiceLocator serviceLocator, final ContextHandlers handlers) {
		super.initActions(serviceLocator, handlers);
		
		handlers.addActivate(TERMINATE_COMMAND_ID, new StopAppHandler());
		handlers.addActivate(TERMINATE_RELAUNCH_COMMAND_ID, new RestartAppHandler());
	}
	
	@Override
	protected void contributeToActionBars(final IServiceLocator serviceLocator,
			final IActionBars actionBars, final HandlerCollection handlers) {
		super.contributeToActionBars(serviceLocator, actionBars, handlers);
		
		final IToolBarManager toolBarManager= actionBars.getToolBarManager();
		
		toolBarManager.appendToGroup(APP_CONTROL_GROUP_ID, new HandlerContributionItem(
				new CommandContributionItemParameter(
						serviceLocator, TERMINATE_RELAUNCH_COMMAND_ID,
						TERMINATE_RELAUNCH_COMMAND_ID, null,
						null, null, null,
						Messages.Action_RestartApp_label, null, Messages.Action_RestartApp_label,
						HandlerContributionItem.STYLE_PUSH,
						null, false ),
				handlers ));
		toolBarManager.appendToGroup(APP_CONTROL_GROUP_ID, new HandlerContributionItem(
				new CommandContributionItemParameter(
						serviceLocator, TERMINATE_COMMAND_ID,
						TERMINATE_COMMAND_ID, null,
						null, null, null,
						Messages.Action_StopApp_label, null, Messages.Action_StopApp_label,
						HandlerContributionItem.STYLE_PUSH,
						null, false ),
				handlers ));
	}
	
}
