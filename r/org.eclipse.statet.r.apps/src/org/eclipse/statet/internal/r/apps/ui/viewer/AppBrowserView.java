/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.viewer;

import java.net.URI;

import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.ui.actions.UIActions;
import org.eclipse.statet.ecommons.ui.mpbv.BrowserSession;
import org.eclipse.statet.ecommons.ui.mpbv.PageBookBrowserPage;
import org.eclipse.statet.ecommons.ui.mpbv.PageBookBrowserView;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.r.apps.ui.AppRegistry;
import org.eclipse.statet.r.apps.ui.AppRegistry.AppStateEvent;


@NonNullByDefault
public class AppBrowserView extends PageBookBrowserView {
	
	
	public static final String VIEW_ID= "org.eclipse.statet.r.apps.views.AppViewer"; //$NON-NLS-1$
	
	public static final String APP_CONTROL_GROUP_ID= "app.Control"; //$NON-NLS-1$
	
	
	private AppRegistry.Listener appRegistryListener;
	
	
	/** plugin.xml */
	public AppBrowserView() {
		super();
	}
	
	@Override
	public void dispose() {
		if (this.appRegistryListener != null) {
			AppRegistry.getInstance().removeListener(this.appRegistryListener);
			this.appRegistryListener= null;
		}
		
		super.dispose();
	}
	
	
	@Override
	public void createPartControl(final Composite parent) {
		super.createPartControl(parent);
		
		this.appRegistryListener= new AppRegistry.Listener() {
			@Override
			public void onAppStateChanged(final AppStateEvent event) {
				UIAccess.getDisplay(getSite().getShell()).asyncExec(() -> {
					if (isCurrent(event.getId())) {
						updateTitle();
						updateState();
					}
				});
			}
		};
		AppRegistry.getInstance().addListener(this.appRegistryListener);
	}
	
	@Override
	protected void contributeToActionBars(final IServiceLocator serviceLocator,
			final IActionBars actionBars, final HandlerCollection handlers) {
		super.contributeToActionBars(serviceLocator, actionBars, handlers);
		
		final IToolBarManager toolBarManager= actionBars.getToolBarManager();
		toolBarManager.insertBefore(UIActions.ADDITIONS_GROUP_ID,
				new Separator(APP_CONTROL_GROUP_ID) );
	}
	
	@Override
	protected PageBookBrowserPage doCreatePage(final BrowserSession session) {
		if (session instanceof AppBrowserSession) {
			return new AppBrowserPage(this, (AppBrowserSession) session);
		}
		return super.doCreatePage(session);
	}
	
	
	private boolean isCurrent(final URI id) {
		final BrowserSession session= getCurrentSession();
		return (session != null && id.equals(session.getId())
				&& UIAccess.isOkToUse(getPageBook()) );
	}
	
}
