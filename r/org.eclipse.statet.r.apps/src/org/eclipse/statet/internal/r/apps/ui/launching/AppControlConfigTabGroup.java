/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.launching;

import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigPresets;


@NonNullByDefault
public class AppControlConfigTabGroup extends AbstractLaunchConfigurationTabGroup {
	
	
	private static final LaunchConfigPresets PRESETS;
	static {
		final LaunchConfigPresets presets= new LaunchConfigPresets(
				AppControlConfigs.TYPE_ID );
		
		final ILaunchConfigurationWorkingCopy config= presets.add("R Shiny App");
		config.setAttribute(AppControlConfigs.START_R_SNIPPET_CODE_ATTR_NAME,
				"library(shiny);\n" + //$NON-NLS-1$
				"shiny::runApp(\"${resource_loc}\", " + //$NON-NLS-1$
						"host= \"${" + AppControlConfigs.APP_HOST_VAR_NAME + "}\", " +
						"port= ${" + AppControlConfigs.APP_PORT_VAR_NAME + "}, " + //$NON-NLS-1$
						"launch.browser= FALSE)" ); //$NON-NLS-1$
		config.setAttribute(AppControlConfigs.STOP_R_SNIPPET_CODE_ATTR_NAME,
				"shiny::stopApp()");
		config.setAttribute(AppControlConfigs.VARIABLES_CODE_ATTR_NAME,
				".rj.tmp$appDomains" );
		
		PRESETS= presets;
	}
	
	public static LaunchConfigPresets getPresets() {
		return PRESETS;
	}
	
	public static void initDefaults(final ILaunchConfigurationWorkingCopy config) {
		AppControlConfigMainTab.initDefaults(config);
		AppControlConfigViewTab.initDefaults(config);
	}
	
	
	public AppControlConfigTabGroup() {
	}
	
	
	@Override
	public void createTabs(final ILaunchConfigurationDialog dialog, final String mode) {
		final ILaunchConfigurationTab[] tabs= new ILaunchConfigurationTab[] {
				new AppControlConfigMainTab(PRESETS),
				new AppControlConfigViewTab(),
				new CommonTab()
		};
		setTabs(tabs);
	}
	
}
