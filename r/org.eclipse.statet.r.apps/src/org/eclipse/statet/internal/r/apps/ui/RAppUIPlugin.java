/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.internal.r.apps.ui.launching.AppControlManager;
import org.eclipse.statet.internal.r.apps.ui.launching.AppType;
import org.eclipse.statet.r.apps.ui.RAppUIResources;


@NonNullByDefault
public class RAppUIPlugin extends AbstractUIPlugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.r.apps"; //$NON-NLS-1$
	
	
	private static RAppUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static RAppUIPlugin getInstance() {
		return RAppUIPlugin.instance;
	}
	
	
	public static void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	public static void logError(final String message, final Throwable e) {
		log(new Status(IStatus.ERROR, BUNDLE_ID, message, e));
	}
	
	
	private boolean started;
	
	private List<Disposable> disposables;
	
	private @Nullable AppControlManager launchConfigManager;
	
	
	@SuppressWarnings("null")
	public RAppUIPlugin() {
	}
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.disposables= new ArrayList<>();
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					log(new Status(IStatus.ERROR, BUNDLE_ID,
							"Error occured while dispose module", //$NON-NLS-1$
							e ));
				}
			}
			this.disposables= null;
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(RAppUIResources.TOOL_VIEW_IMAGE_ID, ImageRegistryUtil.T_TOOL, "view-app.png"); //$NON-NLS-1$
	}
	
	
	public synchronized AppControlManager getRunAppConfigManager(final AppType appType) {
		if (this.launchConfigManager == null) {
			this.launchConfigManager= new AppControlManager(appType);
			this.disposables.add(this.launchConfigManager);
		}
		return this.launchConfigManager;
	}
	
}
