/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.shiny.actions;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.ui.menus.IWorkbenchContribution;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.ui.config.actions.RunConfigsDropdownContribution;


@NonNullByDefault
public class RunAppConfigsDropdownContribution extends RunConfigsDropdownContribution<IContainer>
		implements IWorkbenchContribution, IExecutableExtension {
	
	
	/** For instantiation via plugin.xml */
	public RunAppConfigsDropdownContribution() {
		super(new AppActionUtil(AppActionUtil.ACTIVE_EDITOR_MODE));
	}
	
	
	@Override
	protected AppActionUtil getUtil() {
		return (AppActionUtil) super.getUtil();
	}
	
	
}
