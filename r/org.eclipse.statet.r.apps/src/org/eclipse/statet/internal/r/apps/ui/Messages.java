/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String Variable_AppHost_description;
	public static String Variable_AppPort_description;
	
	public static String RunAction_RunApp_label;
	
	public static String Operation_MainTab_name;
	
	public static String Operation_MainTab_AppFolder_label;
	public static String Operation_AppFolder_info;
	
	public static String Operation_MainTab_AppAddress_group;
	public static String Operation_MainTab_AppHost_label;
	public static String Operation_AppHost_info;
	public static String Operation_MainTab_AppPort_label;
	public static String Operation_AppPort_error_SpecInvalid_message;
	public static String Operation_AppPort_info;
	
	public static String Operation_MainTab_Start_group;
	public static String Operation_StartApp_RCode_label;
	public static String Operation_StartApp_RCode_error_SpecMissing_message;
	public static String Operation_StartApp_RCode_error_SpecInvalid_message;
	public static String Operation_StopApp_RCode_label;
	public static String Operation_StartApp_StopRunningApp_label;
	
	public static String Operation_ViewTab_name;
	public static String Operation_ViewTab_label;
	public static String Operation_ViewTab_Operation_label;
	public static String Operation_Viewer_None_label;
	public static String Operation_Viewer_WorkbenchExternal_label;
	public static String Operation_Viewer_WorkbenchView_label;
	public static String Operation_Viewer_error_Run_message;
	public static String Operation_Variables_label;
	public static String Operation_Variables_RCode_label;
	public static String Operation_Variables_ShowView_label;
	public static String Operation_Variables_error_Run_message;
	
	public static String Action_RestartApp_label;
	public static String Action_StopApp_label;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
