/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.apps.ui.shiny.actions;

import org.eclipse.core.resources.IContainer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.ui.config.actions.ActionUtil;
import org.eclipse.statet.ecommons.debug.ui.config.actions.RunActiveConfigLaunchShortcut;


@NonNullByDefault
public class AppLaunchShortcut extends RunActiveConfigLaunchShortcut<IContainer> {
	
	
	public AppLaunchShortcut() {
		super(new AppActionUtil(ActionUtil.ACTIVE_MENU_SELECTION_MODE));
	}
	
	
}
