/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.apps.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.apps.ui.RAppUIPlugin;


@NonNullByDefault
public class RAppUIResources {
	
	
	private static final String NS= "org.eclipse.statet.r.apps"; //$NON-NLS-1$
	
	
	public static final String TOOL_VIEW_IMAGE_ID=              NS + "/images/tool/View"; //$NON-NLS-1$
	
	
	public static final RAppUIResources INSTANCE= new RAppUIResources();
	
	
	private final ImageRegistry registry;
	
	
	private RAppUIResources() {
		this.registry= RAppUIPlugin.getInstance().getImageRegistry();
	}
	
	
	public @Nullable ImageDescriptor getImageDescriptor(final String id) {
		return this.registry.getDescriptor(id);
	}
	
	public @Nullable Image getImage(final String id) {
		return this.registry.get(id);
	}
	
}
