/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.apps.ui;

import static org.eclipse.statet.jcommons.status.Status.OK_STATUS;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.r.core.data.CombinedRElement;


@NonNullByDefault
public class VariablesData {
	
	
	private final String expression;
	
	private final Status status;
	
	private final @Nullable ImList<CombinedRElement> elements;
	
	
	public VariablesData(final String expression,
			final ImList<CombinedRElement> elements) {
		this.expression= expression;
		this.status= OK_STATUS;
		this.elements= elements;
	}
	
	public VariablesData(final String expression,
			final Status status) {
		this.expression= expression;
		this.status= status;
		this.elements= null;
	}
	
	
	public String getExpression() {
		return this.expression;
	}
	
	public Status getStatus() {
		return this.status;
	}
	
	public @Nullable ImList<CombinedRElement> getElements() {
		return this.elements;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder("VariablesData"); //$NON-NLS-1$
		sb.addProp("expression", this.expression); //$NON-NLS-1$
		sb.addProp("status", this.status); //$NON-NLS-1$
		sb.addProp("elements", this.elements); //$NON-NLS-1$
		return sb.toString();
	}
	
}
