/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.apps.ui;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IWorkbenchPage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;


@NonNullByDefault
public interface RApp {
	
	
	class AppEvent {
		
		private final RApp app;
		
		
		public AppEvent(final RApp app) {
			this.app= app;
		}
		
		
		public RApp getApp() {
			return this.app;
		}
		
	}
	
	interface Listener {
		
		
		void onVariablesChanged(AppEvent event);
		
	}
	
	
	IResource getResource();
	
	@Nullable Tool getTool();
	
	boolean isRunning();
	
	void startApp(IWorkbenchPage page) throws CoreException;
	boolean canRestartApp();
	void restartApp(IWorkbenchPage page) throws CoreException;
	
	boolean canStopApp();
	void stopApp();
	
	
	void addListener(Listener listener);
	void removeListener(Listener listener);
	
	@Nullable VariablesData getVariables();
	void refreshVariables();
	
}
