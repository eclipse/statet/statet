/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.util;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;
import org.eclipse.statet.jcommons.ts.core.ToolCommandHandler;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.nico.core.runtime.ConsoleService;


@NonNullByDefault
public abstract class AbstractConsoleCommandHandler implements ToolCommandHandler {
	
	
	@Override
	public Status execute(final String id, final ToolService service, final ToolCommandData data,
			final ProgressMonitor m) throws StatusException {
		return execute(id, (ConsoleService)service, data, m);
	}
	
	protected abstract Status execute(String id, ConsoleService service, ToolCommandData data,
			ProgressMonitor m) throws StatusException;
	
}
