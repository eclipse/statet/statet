/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.util;

import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;

import org.eclipse.statet.jcommons.ts.core.Tool;


public abstract class ToolTerminateListener implements IDebugEventSetListener {
	
	
	private final Tool tool;
	
	
	public ToolTerminateListener(final Tool tool) {
		this.tool= tool;
	}
	
	
	public void install() {
		final DebugPlugin debugPlugin= DebugPlugin.getDefault();
		if (debugPlugin != null) {
			debugPlugin.addDebugEventListener(this);
			if (this.tool.isTerminated()) {
				debugPlugin.removeDebugEventListener(this);
				toolTerminated();
			}
		}
	}
	
	public void dispose() {
		final DebugPlugin debugPlugin= DebugPlugin.getDefault();
		if (debugPlugin != null) {
			debugPlugin.removeDebugEventListener(this);
		}
	}
	
	@Override
	public void handleDebugEvents(final DebugEvent[] events) {
		for (int i= 0; i < events.length; i++) {
			final DebugEvent event= events[i];
			if (event.getSource() == this.tool
					&& event.getKind() == DebugEvent.TERMINATE) {
				toolTerminated();
			}
		}
	}
	
	public abstract void toolTerminated();
	
}
