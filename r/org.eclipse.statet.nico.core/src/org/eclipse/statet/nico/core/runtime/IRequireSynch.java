/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * Optional interface indicating, that the controller requires explicite sync
 * if necessary.
 */
public interface IRequireSynch {
	
	
	/**
	 * @param m
	 * @return pattern (optional), matching possible output of synch command
	 * @throws CoreException
	 */
	Pattern synch(final ProgressMonitor m) throws StatusException;
	
}
