/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;


/**
 * Runnables for classic console input.
 */
@NonNullByDefault
public interface ConsoleRunnable extends ToolRunnable {
	
	
	/**
	 * Return the submit type of this entry. The same runnable should
	 * always return the same type.
	 * 
	 * @return the type
	 */
	SubmitType getSubmitType();
	
}
