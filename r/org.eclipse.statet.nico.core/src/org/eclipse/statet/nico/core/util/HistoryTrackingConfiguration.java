/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.util;

import java.util.EnumSet;

import org.eclipse.statet.nico.core.runtime.SubmitType;


/**
 * 
 */
public class HistoryTrackingConfiguration extends TrackingConfiguration {
	
	
	public static final String HISTORY_TRACKING_ID = "history"; //$NON-NLS-1$
	
	
	private boolean fLoadHistory;
	
	
	public HistoryTrackingConfiguration(final String id) {
		super(id);
	}
	
	public HistoryTrackingConfiguration(final String id, final HistoryTrackingConfiguration template) {
		super(id, template);
		
		fLoadHistory = template.fLoadHistory;
	}
	
	
	@Override
	public void loadDefaults() {
		super.loadDefaults();
		
		setTrackStreamInfo(false);
		setTrackStreamInput(true);
		setTrackStreamInputHistoryOnly(true);
		setTrackStreamOutput(false);
		setTrackStreamOutputTruncate(false);
		setSubmitTypes(EnumSet.of(SubmitType.CONSOLE));
		
		setLoadHistory(true);
	}
	
	
	public boolean getLoadHistory() {
		return fLoadHistory;
	}
	
	public void setLoadHistory(final boolean enable) {
		final boolean oldValue = fLoadHistory;
		fLoadHistory = enable;
		firePropertyChange("loadHistory", oldValue, enable); //$NON-NLS-1$
	}
	
}
