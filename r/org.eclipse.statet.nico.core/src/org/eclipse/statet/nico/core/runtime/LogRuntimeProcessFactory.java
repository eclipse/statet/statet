/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import java.util.Map;

import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.IProcessFactory;
import org.eclipse.debug.core.model.IProcess;


public class LogRuntimeProcessFactory implements IProcessFactory {
	
	
	public static final String FACTORY_ID = "org.eclipse.statet.nico.LogRuntimeProcessFactory"; //$NON-NLS-1$
	
	
	public LogRuntimeProcessFactory() {
	}
	
	
	@Override
	public IProcess newProcess(final ILaunch launch, final Process process, final String label,
			final Map attributes) {
		return new LogRuntimeProcess(launch, process, label, attributes);
	}
	
}
