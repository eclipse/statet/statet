/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import org.eclipse.statet.jcommons.ts.core.ToolRunnable;


/**
 * Progress informations about the current task.
 * It comply a fixed state of a progress monitor.
 */
public interface IProgressInfo {
	
	
	/**
	 * Main label of current task.
	 */
	public String getLabel();
	
	/**
	 * Label of current subtask.
	 */
	public String getSubLabel();
	
	/**
	 * Ratio worked [0-1], -1 for UNKOWN.
	 */
	public double getWorked();
	
	/**
	 * Current runnable
	 * @return the runnable, can be <code>null</code>
	 */
	public ToolRunnable getRunnable();
	
}
