/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import java.io.IOException;

import org.eclipse.debug.core.model.IStreamsProxy;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * None buffered streams
 */
@NonNullByDefault
public class ToolStreamProxy implements IStreamsProxy {
	
	
	private final ToolStreamMonitor inputMonitor= new ToolStreamMonitor();
	private final ToolStreamMonitor infoMonitor= new ToolStreamMonitor();
	private final ToolStreamMonitor standardOutputMonitor= new ToolStreamMonitor();
	private final ToolStreamMonitor standardErrorMonitor= new ToolStreamMonitor();
	private final ToolStreamMonitor systemOutputMonitor= new ToolStreamMonitor();
	
	
	public ToolStreamProxy() {
	}
	
	
	@Override
	public void write(final String input) throws IOException {
		throw new IOException("Function is not supported."); //$NON-NLS-1$
	}
	
	public ToolStreamMonitor getInputStreamMonitor() {
		return this.inputMonitor;
	}
	
	public ToolStreamMonitor getInfoStreamMonitor() {
		return this.infoMonitor;
	}
	
	@Override
	public ToolStreamMonitor getOutputStreamMonitor() {
		return this.standardOutputMonitor;
	}
	
	@Override
	public ToolStreamMonitor getErrorStreamMonitor() {
		return this.standardErrorMonitor;
	}
	
	public ToolStreamMonitor getSystemOutputMonitor() {
		return this.systemOutputMonitor;
	}
	
	
	public void dispose() {
		this.inputMonitor.dispose();
		this.infoMonitor.dispose();
		this.standardOutputMonitor.dispose();
		this.standardErrorMonitor.dispose();
		this.systemOutputMonitor.dispose();
	}
	
}
