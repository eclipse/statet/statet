/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import org.eclipse.core.filesystem.IFileStore;


/**
 * A tool session track.
 */
public interface ITrack {
	
	
	/**
	 * A name of the track (usually the name from the configuration).
	 * For the identification by the user.
	 * 
	 * @return the name
	 */
	String getName();
	
	/**
	 * Flushes the buffer of the track, if available.
	 */
	void flush();
	
	/**
	 * Returns the file the track is written to (if configured).
	 * 
	 * @return the current file or <code>null</code>
	 */
	IFileStore getFile();
	
}
