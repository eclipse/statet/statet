/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import java.util.EnumSet;

import org.eclipse.statet.internal.nico.core.Messages;


public enum SubmitType {
	
	/** Console interaction by user */
	CONSOLE (Messages.SubmitType_Console_label),
	/** Submit from editor and other code based views */
	EDITOR (Messages.SubmitType_Editor_label),
	/** Submit from tools (like the object browser) */
	TOOLS (Messages.SubmitType_Tools_label),
	/** Others, e.g. from controller */
	OTHER (Messages.SubmitType_Other_label),
	;
	
	
	public static EnumSet<SubmitType> getDefaultSet() {
		return EnumSet.range(CONSOLE, OTHER);
	}
	
	
	private String fLabel;
	
	
	SubmitType(final String label) {
		fLabel = label;
	}
	
	
	public String getLabel() {
		return fLabel;
	}
	
}
