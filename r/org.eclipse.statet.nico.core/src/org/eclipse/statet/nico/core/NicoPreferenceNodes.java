/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core;

import org.eclipse.statet.jcommons.util.StringUtils;

import org.eclipse.statet.ecommons.preferences.core.Preference.IntPref;


/**
 * 
 */
public class NicoPreferenceNodes {
	
	
	public static final String SCOPE_QUALIFIER = "org.eclipse.statet.nico.scope"; //$NON-NLS-1$
	
	public static final String createScopeQualifier(final String qualifier) {
		final int idx= qualifier.indexOf('/') + 1; // no match -> 0
		return StringUtils.insert(qualifier, idx, SCOPE_QUALIFIER + '/');
	}
	
	public static final String CAT_HISTORY_QUALIFIER = NicoCore.BUNDLE_ID + "/history"; //$NON-NLS-1$
	
	
	public static final IntPref KEY_DEFAULT_TIMEOUT = new IntPref(NicoCore.BUNDLE_ID, "timeout.default"); //$NON-NLS-1$
	
	
}
