/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.core.runtime;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import com.ibm.icu.text.DateFormat;

import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchesListener;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamsProxy;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.debug.core.model.AbstractProcess;
import org.eclipse.statet.ecommons.io.FileUtil;

import org.eclipse.statet.internal.nico.core.NicoCorePlugin;
import org.eclipse.statet.nico.core.runtime.ToolController.IToolStatusListener;
import org.eclipse.statet.nico.core.runtime.ToolWorkspace.Listener;


/**
 * Provides <code>IProcess</code> for a <code>ToolController</code>.
 */
@NonNullByDefault
public class ToolProcess extends AbstractProcess implements IProcess, Tool, IToolStatusListener {
	
	public static final String PROCESS_TYPE_SUFFIX= ".nico"; //$NON-NLS-1$
	
	
	public static final int EXITCODE_DISCONNECTED= 101;
	
	
	private final String mainType;
	private final Set<String> featureSets= new HashSet<>();
	private final String toolLabelShort;
	private final String toolLabelLong;
	private @Nullable String toolLabelTrimmedWD;
	private String toolLabelStatus;
	
	private final String address;
	private final long connectionTimestamp;
	private long startupTimestamp;
	private String startupWD;
	Map<String, Object> connectionInfo;
	
	private @Nullable ToolController controller;
	private final Queue queue;
	private final History history;
	private ToolWorkspace workspace;
	
	private final Object disposeLock= new Object();
	private int retain;
	private boolean isDisposed;
	
	private final boolean captureOutput;
	
	private volatile ToolStatus status= ToolStatus.STARTING;
	
	private final CopyOnWriteIdentityListSet<Runnable> terminationListeners= new CopyOnWriteIdentityListSet<>();
	
	private ImList<? extends ITrack> tracks= ImCollections.emptyList();
	
	
	public ToolProcess(final ILaunch launch, final String mainType,
			final String labelPrefix, final String name,
			final String address, final String wd, final long timestamp) {
		super(launch, name);
		this.mainType= mainType;
		this.address= address;
		this.startupWD= wd;
		this.startupTimestamp= timestamp;
		this.connectionTimestamp= timestamp;
		
		this.toolLabelShort= labelPrefix;
		this.toolLabelLong= labelPrefix + ' ' + name;
		this.toolLabelStatus= ToolStatus.STARTING.getMarkedLabel();
		this.toolLabelTrimmedWD= trimPath(wd);
		
//		final String captureOutput= launch.getAttribute(DebugPlugin.ATTR_CAPTURE_OUTPUT);
//		fCaptureOutput= !("false".equals(captureOutput)
//				&& !captureLogOnly(launch.getLaunchConfiguration())); //$NON-NLS-1$
		this.captureOutput= false;
		
		DebugPlugin.getDefault().getLaunchManager().addLaunchListener(new ILaunchesListener() {
			@Override
			public void launchesAdded(final @NonNull ILaunch[] launches) {
			}
			@Override
			public void launchesChanged(final @NonNull ILaunch[] launches) {
			}
			@Override
			public void launchesRemoved(final @NonNull ILaunch[] launches) {
				for (final ILaunch launch : launches) {
					if (getLaunch() == launch) {
						final DebugPlugin plugin= DebugPlugin.getDefault();
						if (plugin != null) {
							plugin.getLaunchManager().removeLaunchListener(this);
							dispose();
						}
					}
				}
			}
		});
		
		this.queue= new Queue(this);
		this.history= new History(this);
	}
	
	public void init(final ToolController controller) {
		this.controller= controller;
		this.workspace= this.controller.getWorkspace();
		this.workspace.addPropertyListener(new Listener() {
			@Override
			public void propertyChanged(final ToolWorkspace workspace, final Map<String, Object> properties) {
				final var attributes= getAttributes();
				final DebugEvent nameEvent;
				synchronized (attributes) {
					ToolProcess.this.toolLabelTrimmedWD= null;
					nameEvent= doSet(attributes, IProcess.ATTR_PROCESS_LABEL, computeConsoleLabel());
				}
				if (nameEvent != null) {
					fireEvent(nameEvent);
				}
			}
		});
		
		this.toolLabelTrimmedWD= null;
		final var attributes= getAttributes();
		doSet(attributes, IProcess.ATTR_PROCESS_LABEL, computeConsoleLabel());
		doSet(attributes, IProcess.ATTR_PROCESS_TYPE, (this.mainType + PROCESS_TYPE_SUFFIX).intern());
		
		this.history.init();
		
		created();
	}
	
	
	public void registerFeatureSet(final String featureSetID) {
		this.featureSets.add(featureSetID);
	}
	
	@Override
	public boolean isProvidingFeatureSet(final String featureSetID) {
		return this.featureSets.contains(featureSetID);
	}
	
	private String computeConsoleLabel() {
		String wd= this.toolLabelTrimmedWD;
		if (wd == null) {
			wd= this.toolLabelTrimmedWD= trimPath(FileUtil.toString(this.workspace.getWorkspaceDir()));
		}
		return this.toolLabelShort + ' ' + getLabel() + "  ∙  " + wd + "   \t " + this.toolLabelStatus; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	private String trimPath(final @Nullable String path) {
		if (path == null) {
			return "–"; //$NON-NLS-1$
		}
		if (path.length() < 30) {
			return path;
		}
		final int post1= prevPathSeperator(path, path.length() - 1);
		if (post1 < 25) {
			return path;
		}
		final int post2= prevPathSeperator(path, post1 - 1);
		if (post2 < 20) {
			return path;
		}
		final int pre1= nextPathSeperator(path, 0);
		int pre2= nextPathSeperator(path, pre1 + 1);
		if (pre2 > 12) {
			pre2= 10;
		}
		else {
			pre2++;
		}
		if (post2 - pre2 < 10) {
			return path;
		}
		return path.substring(0, pre2) + " ... " + path.substring(post2); //$NON-NLS-1$
	}
	
	private int prevPathSeperator(final String path, final int offset) {
		final int idx1= path.lastIndexOf('/', offset);
		final int idx2= path.lastIndexOf('\\', offset);
		return (idx1 > idx2) ?
				idx1 : idx2;
	}
	
	private int nextPathSeperator(final String path, final int offset) {
		final int idx1= path.indexOf('/', offset);
		final int idx2= path.indexOf('\\', offset);
		return (idx1 >= 0 && idx1 < idx2) ?
				idx1 : idx2;
	}
	
	@Override
	public final String getMainType() {
		return this.mainType;
	}
	
	/**
	 * Returns the name of the process, usually with a timestamp.
	 * <p>
	 * For example: <code>R-2.11.1 / RJ (29.11.2010 12:56:37)</code>
	 * 
	 * @return
	 */
//	@Override
//	public String getLabel() {
//		return super.getLabel();
//	}
	
	/**
	 * Returns a label of the tool, usually based on the launch configuration.
	 * <p>
	 * {@link Tool#DEFAULT_LABEL DEFAULT_LABEL}: &lt;name of launch config&gt; [&lt;name of launch type&gt;]<br/>
	 * For example: <code>RJ-2.11 [R Console]</code>
	 * </p><p>
	 * {@link Tool#LONG_LABEL LONG_LABEL}: &lt;short label&gt; &lt;name&gt;<br/>
	 * For example: <code>RJ-2.11 [R Console] R-2.11.1 / RJ (29.11.2010 12:49:51)</code>
	 * </p>
	 * 
	 * @param config allows to configure the information to include in the label
	 * @return the label
	 */
	@Override
	public String getLabel(final int config) {
		if ((config & LONG_LABEL) != 0) {
			return this.toolLabelLong;
		}
		return this.toolLabelShort;
	}
	
	public @Nullable ToolController getController() {
		return this.controller;
	}
	
	public History getHistory() {
		return this.history;
	}
	
	@Override
	public Queue getQueue() {
		return this.queue;
	}
	
	@Override
	public IStreamsProxy getStreamsProxy() {
		return (this.captureOutput && this.controller != null) ? this.controller.getStreams() : null;
	}
	
	
	@Override
	public ToolWorkspace getWorkspace() {
		return this.workspace;
	}
	
	public ToolStatus getToolStatus() {
		return this.status;
	}
	
	
	void setExitValue(final int value) {
		doSetExitValue(value);
	}
	
	@Override
	public boolean canTerminate() {
		return (!isTerminated());
	}
	
	@Override
	public void terminate() throws DebugException {
		final var controller= this.controller;
		if (controller != null) {
			controller.scheduleQuit();
		}
	}
	
	@Override
	public boolean isTerminated() {
		return (this.status == ToolStatus.TERMINATED);
	}
	
	@Override
	public void addTerminationListener(final Runnable listener) {
		this.terminationListeners.add(nonNullAssert(listener));
		if (isTerminated() && this.terminationListeners.remove(listener)) {
			listener.run();
		}
	}
	
	@Override
	public void removeTerminationListener(final Runnable listener) {
		this.terminationListeners.remove(listener);
	}
	
	void onTerminated() {
		for (final var listener : this.terminationListeners.clearToList()) {
			try {
				listener.run();
			}
			catch (final Exception e) {
				NicoCorePlugin.logError("An error occured when notifying a tool listener.", e);
			}
		}
	}
	
	
	/** Called by Controller */
	@Override
	public void controllerStatusChanged(final ToolStatus oldStatus, final ToolStatus newStatus,
			final List<DebugEvent> eventCollection) {
		this.status= newStatus;
		
		if (newStatus == ToolStatus.TERMINATED) {
			this.controller= null;
			eventCollection.add(new DebugEvent(ToolProcess.this, DebugEvent.TERMINATE));
		}
		
		final var attributes= getAttributes();
		final DebugEvent nameEvent;
		synchronized (attributes) {
			this.toolLabelStatus= newStatus.getMarkedLabel();
			nameEvent= doSet(attributes, IProcess.ATTR_PROCESS_LABEL, computeConsoleLabel());
		}
		if (nameEvent != null) {
			eventCollection.add(nameEvent);
		}
	}
	
//	public void controllerBusyChanged(final boolean isBusy, final List<DebugEvent> eventCollection) {
//		eventCollection.add(new DebugEvent(this, DebugEvent.MODEL_SPECIFIC, 
//				isBusy ? (ToolProcess.BUSY | 0x1) : (ToolProcess.BUSY | 0x0)));
//	}
//	
	private final void dispose() {
		synchronized (this.disposeLock) {
			this.isDisposed= true;
			if (this.retain > 0) {
				return;
			}
		}
		doDispose();
	}
	
	public Map<String, Object> getConnectionInfo() {
		return this.connectionInfo;
	}
	
	public void prepareRestart(final Map<String, Object> data) {
		if (this.status != ToolStatus.TERMINATED) {
			throw new IllegalStateException();
		}
		if (data == null) {
			throw new NullPointerException();
		}
		data.put("process", this);
		data.put("processDispose", poseponeDispose());
		data.put("address", this.address);
		data.put("connectionInfo", this.connectionInfo);
	}
	
	public void restartCompleted(final Map<String, Object> data) {
		if (data == null) {
			throw new NullPointerException();
		}
		if (data.get("process") != this) {
			throw new IllegalArgumentException();
		}
		approveDispose(data.get("processDispose"));
	}
	
	/**
	 * Prevents to dispose the resources so you can still access the tool
	 * and its queue.
	 * It is important to call #approveDispose later to release the resources.
	 * 
	 * @return ticket to approve the disposal
	 */
	private final @Nullable Object poseponeDispose() {
		synchronized (this.disposeLock) {
			if (this.retain <= 0 && this.isDisposed) {
				return null;
			}
			this.retain++;
		}
		return new AtomicBoolean(true);
	}
	
	/**
	 * @see #poseponeDispone
	 */
	private final void approveDispose(final Object ticket) {
		if (ticket instanceof AtomicBoolean && ((AtomicBoolean) ticket).getAndSet(false)) {
			synchronized (this.disposeLock) {
				this.retain--;
				if (this.retain > 0 || !this.isDisposed) {
					return;
				}
			}
			doDispose();
		}
	}
	
	protected void doDispose() {
		if (this.queue != null) {
			synchronized (this.queue) {
				this.queue.internal_dispose();
			}
		}
		if (this.history != null) {
			this.history.dispose();
		}
		this.terminationListeners.clear();
	}
	
	
	void setTracks(final List<? extends ITrack> tracks) {
		this.tracks= ImCollections.toList(tracks);
	}
	
	public ImList<? extends ITrack> getTracks() {
		return this.tracks;
	}
	
	void setStartupTimestamp(final long timestamp) {
		this.startupTimestamp= timestamp;
	}
	
	public long getStartupTimestamp() {
		return this.startupTimestamp;
	}
	
	public long getConnectionTimestamp() {
		return this.connectionTimestamp;
	}
	
	public String createTimestampComment(final long timestamp) {
		return DateFormat.getDateTimeInstance().format(timestamp) + '\n';
	}
	
	void setStartupWD(final String wd) {
		this.startupWD= wd;
	}
	
	public String getStartupWD() {
		return this.startupWD;
	}
	
	
	@Override
	public String toString() {
		return this.toolLabelLong;
	}
	
}
