/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.BasicProgressMonitor;
import org.eclipse.statet.jcommons.status.BasicProgressMonitor.ProgressData;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.ts.core.ToolRunnable;

import org.eclipse.statet.nico.core.runtime.IProgressInfo;


@NonNullByDefault
public class RunnableProgressData extends ProgressData implements IProgressInfo {
	
	
	private final @Nullable ToolRunnable runnable;
	
	private @Nullable String label;
	private volatile boolean refreshLabel;
	
	private @Nullable ProgressMonitor root;
	
	
	public RunnableProgressData(final ToolRunnable runnable) {
		this.runnable= runnable;
		
		setMainTaskName(runnable.getLabel());
	}
	
	public RunnableProgressData(final String name) {
		this.runnable= null;
		
		setMainTaskName(name);
	}
	
	
	@Override
	public void setCanceled(final boolean state) {
		super.setCanceled(state);
	}
	
	@Override
	protected void onDataChanged(final byte data) {
		switch (data) {
		case MAIN_TASK_NAME:
		case CANCELED:
		case BLOCKED:
			this.refreshLabel= true;
			break;
		default:
			break;
		}
	}
	
	@Override
	public String getLabel() {
		String label= this.label;
		if (this.refreshLabel || label == null) {
			this.refreshLabel= false;
			label= createLabelWithStatus();
			this.label= label;
		}
		return label;
	}
	
	private String createLabelWithStatus() {
		final String mainTaskName= nonNullElse(getMainTaskName(), ""); //$NON-NLS-1$
		if (isCanceled()) {
			return NLS.bind(Messages.Progress_Canceled_label,
					mainTaskName );
		}
		final Status blocked= getBlocked();
		if (blocked != null) {
			return NLS.bind(Messages.Progress_Blocked_label, (new Object[] {
					mainTaskName, blocked.getMessage() }));
		}
		return mainTaskName;
	}
	
	@Override
	public String getSubLabel() {
		return nonNullElse(getSubTaskName(), ""); //$NON-NLS-1$
	}
	
	@Override
	public @Nullable ToolRunnable getRunnable() {
		return this.runnable;
	}
	
	@Override
	public double getWorked() {
		if (isProgressUnknown()) {
			return -1;
		}
		return 1 - getProgress();
	}
	
	
	public ProgressMonitor getRoot() {
		ProgressMonitor root= this.root;
		if (root == null) {
			root= new BasicProgressMonitor(this, ProgressMonitor.SUPPRESS_BEGINTASK_NAME);
			this.root= root;
		}
		return root;
	}
	
}
