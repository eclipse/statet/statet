/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import static org.eclipse.statet.ltk.model.core.element.LtkModelElement.MASK_C12;

import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrElement;
import org.eclipse.statet.r.core.source.util.RPkgDescrHeuristicTokenScanner;


@NonNullByDefault
public class RPkgDescrRefactoringAdapter extends RefactoringAdapter {
	
	
	public RPkgDescrRefactoringAdapter() {
		super(RModel.RPKG_DESCRIPTION_TYPE_ID);
	}
	
	
	@Override
	public String getPluginIdentifier() {
		return RCore.BUNDLE_ID;
	}
	
	@Override
	public RPkgDescrHeuristicTokenScanner getScanner(final SourceUnit su) {
		return new RPkgDescrHeuristicTokenScanner(su.getDocumentContentInfo());
	}
	
	@Override
	public boolean isCommentContent(final ITypedRegion partition) {
		return false;
	}
	
	@Override
	protected boolean shouldExpandEmptyLine(final SourceElement element) {
		return (element.getModelTypeId() == RModel.RPKG_DESCRIPTION_TYPE_ID
				&& (element.getElementType() & MASK_C12) == RPkgDescrElement.C12_RECORD);
	}
	
	@Override
	protected boolean shouldAppendEmptyLine(final SourceElement element) {
		return shouldExpandEmptyLine(element);
	}
	
}
