/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.renv;

import java.util.List;
import java.util.concurrent.locks.Lock;

import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;

import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnvManager;


/**
 * Manages R environment configurations.
 */
public interface IREnvManager extends REnvManager {
	
	
	String PREF_QUALIFIER= RCore.BUNDLE_ID + "/r.environments"; //$NON-NLS-1$
	
	
	void set(ImList<IREnvConfiguration> configs, String defaultConfigName) throws CoreException;
	
	Lock getReadLock();
	List<IREnvConfiguration> getConfigurations();
	
	REnv getDefault();
	IREnvConfiguration.WorkingCopy newConfiguration(String type);
	
	/**
	 * {@inheritDoc}
	 * <p>Only supported for IREnvConfiguration with type {@link IREnvConfiguration#CONTRIB_TEMP_REMOTE_TYPE}</p>
	 */
	@Override
	void add(@NonNull REnvConfiguration rEnvConfig);
	/**
	 * {@inheritDoc}
	 * <p>Only supported for IREnvConfiguration with type {@link IREnvConfiguration#CONTRIB_TEMP_REMOTE_TYPE}</p>
	 */
	@Override
	void delete(@NonNull REnv rEnv);
	
}
