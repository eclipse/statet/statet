/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_WHILE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>while ( §cond§ ) §cont§</code>
 */
@NonNullByDefault
public final class CWhileLoop extends RAstNode {
	
	
	int condOpenOffset= NA_OFFSET;
	final Expression condExpr= new Expression();
	int condCloseOffset= NA_OFFSET;
	final Expression loopExpr= new Expression();
	
	
	CWhileLoop() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.C_WHILE;
	}
	
	@Override
	public final RTerminal getOperator(final int index) {
		return RTerminal.WHILE;
	}
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 2;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		return switch (index) {
		case 0 -> this.condExpr.node;
		case 1 -> this.loopExpr.node;
		default ->
				throw new IndexOutOfBoundsException();
		};
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.condExpr.node == child) {
			return 0;
		}
		if (this.loopExpr.node == child) {
			return 1;
		}
		return -1;
	}
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		if (this.loopExpr.node == child) {
			return this.loopExpr;
		}
		if (this.condExpr.node == child) {
			return this.condExpr;
		}
		return null;
	}
	
	public final int getCondOpenOffset() {
		return this.condOpenOffset;
	}
	
	public final RAstNode getCondChild() {
		return this.condExpr.node;
	}
	
	public final int getCondCloseOffset() {
		return this.condCloseOffset;
	}
	
	public final RAstNode getContChild() {
		return this.loopExpr.node;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.condExpr.node.acceptInR(visitor);
		this.loopExpr.node.acceptInR(visitor);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.condExpr.node);
		visitor.visit(this.loopExpr.node);
	}
	
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final Expression getRightExpr() {
		return this.loopExpr;
	}
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.C_WHILE == element.getNodeType());
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		if (this.condExpr == expr) {
			return TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING | CTX12_WHILE;
		}
		if (this.loopExpr == expr) {
			return TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_WHILE;
		}
		throw new IllegalArgumentException();
	}
	
	@SuppressWarnings("unused")
	final void updateOffsets() {
		if (this.loopExpr.node != null) {
			doSetEndOffset(this.loopExpr.node.getEndOffset());
		}
		else if (this.condCloseOffset != NA_OFFSET) {
			doSetEndOffset(this.condCloseOffset + 1);
		}
		else if (this.condExpr.node != null) {
			doSetEndOffset(this.condExpr.node.getEndOffset());
		}
		else if (this.condOpenOffset != NA_OFFSET) {
			doSetEndOffset(this.condOpenOffset + 1);
		}
		else {
			doSetEndOffset(getStartOffset() + 5);
		}
	}
	
}
