/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.internal.r.core.pkgmanager.RVarRepo;
import org.eclipse.statet.rj.renv.core.RLibGroup;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.runtime.RLibLocationInfo;
import org.eclipse.statet.rj.renv.runtime.RPkgManager;
import org.eclipse.statet.rj.renv.runtime.RPkgManagerDataset;
import org.eclipse.statet.rj.renv.runtime.RuntimeRLibPaths;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
public class RPkgUtils extends org.eclipse.statet.rj.renv.core.RPkgUtils {
	
	
	public static boolean DEBUG= Boolean.getBoolean("org.eclipse.statet.r.core.pkgmanager.debug"); //$NON-NLS-1$
	
	
	public static @Nullable RRepo getRepoById(final Collection<? extends RRepo> list, final String id) {
		for (final RRepo repo : list) {
			if (id.equals(repo.getId())) {
				return repo;
			}
		}
		return null;
	}
	
	public static @Nullable RRepo getRepoByName(final Collection<? extends RRepo> list, final String name) {
		for (final RRepo repo : list) {
			if (name.equals(repo.getName())) {
				return repo;
			}
		}
		return null;
	}
	
	public static @Nullable RRepo getRepoByURL(final Collection<? extends RRepo> list, final RRepo repo) {
		return (repo instanceof RVarRepo) ?
				getRepoByRawURL(list, ((RVarRepo) repo).getRawURL()) :
				getRepoByURL(list, repo.getURL());
	}
	
	public static @Nullable RRepo getRepoByURL(final Collection<? extends RRepo> list, String url) {
		url= RRepo.checkRepoURL(url);
		for (final RRepo repo : list) {
			if (url.equals(repo.getURL())) {
				return repo;
			}
		}
		return null;
	}
	
	private static @Nullable RVarRepo getRepoByRawURL(final Collection<? extends RRepo> list, String url) {
		url= RRepo.checkRepoURL(url);
		for (final RRepo repo : list) {
			if (repo instanceof RVarRepo && url.equals(((RVarRepo) repo).getRawURL())) {
				return (RVarRepo) repo;
			}
		}
		return null;
	}
	
	public static @Nullable RRepo findRepo(final Collection<? extends RRepo> list, final RRepo repo) {
		RRepo found= getRepoById(list, repo.getId());
		if (found == null && !repo.getURL().isEmpty()) {
			found= getRepoByURL(list, repo);
		}
		if (found == null && !repo.getName().isEmpty()) {
			found= getRepoByName(list, repo.getName());
		}
		return found;
	}
	
	
	private static final ImList<String> DEFAULT_INSTALL_ORDER= ImCollections.newList(
			RLibGroup.R_USER, RLibGroup.R_OTHER, RLibGroup.R_SITE, RLibGroup.R_DEFAULT );
	
	public static @Nullable RLibLocation getDefaultInstallLocation(final RuntimeRLibPaths rLibPaths) {
		for (final String groupId : DEFAULT_INSTALL_ORDER) {
			final RLibGroup group= rLibPaths.getRLibGroup(groupId);
			if (group != null) {
				for (final RLibLocation location : group.getLibLocations()) {
					final RLibLocationInfo locationInfo= rLibPaths.getInfo(location);
					if (locationInfo != null && locationInfo.isWritable()) {
						return location;
					}
				}
			}
		}
		return null;
	}
	
	public static boolean areInstalled(final RPkgManager manager, final List<String> pkgNames,
			final RService r, final ProgressMonitor m) {
//		manager.check(flags, r, m); TODO
		final RPkgManagerDataset dataset= manager.getDataset();
		if (dataset == null) {
			return true;
		}
		for (final String pkgName : pkgNames) {
			if (!dataset.getInstalled().contains(pkgName)) {
				return false;
			}
		}
		return true;
	}
	
	
}
