/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Definitions for RAst nodes.
 */
@NonNullByDefault
public enum NodeType {
	
	
	SOURCELINES ("R",       100000000, Assoc.CONTAINER), //$NON-NLS-1$
	COMMENT ("#",                  -1, -1), //$NON-NLS-1$
	DOCU_AGGREGATION ("#'",        -1, -1), //$NON-NLS-1$
	DOCU_TAG ("@",                 -1, -1), //$NON-NLS-1$
	DOCU_TEXT ("abc",              -1, -1), //$NON-NLS-1$
	ERROR ("‼",                    99, Assoc.LEFTSTD), //$NON-NLS-1$
	ERROR_TERM ("‼",                1, Assoc.TERM), //$NON-NLS-1$
	DUMMY ("",                      1, Assoc.CONTAINER), //$NON-NLS-1$
	
	SYMBOL ("sym",                  1, Assoc.TERM), //$NON-NLS-1$
	PLACEHOLDER ("_",               1, Assoc.TERM), //$NON-NLS-1$
	STRING_CONST ("str",            1, Assoc.TERM), //$NON-NLS-1$
	NUM_CONST ("num",               1, Assoc.TERM), //$NON-NLS-1$
	NULL_CONST ("null",             1, Assoc.TERM), //$NON-NLS-1$
	
	BLOCK ("{ }",                  11, Assoc.CONTAINER), //$NON-NLS-1$
	GROUP ("( )",                  11, Assoc.CONTAINER), //$NON-NLS-1$
	
	SUB_INDEXED_S ("[",            12, Assoc.CONTAINER), //$NON-NLS-1$
	SUB_INDEXED_D ("[[",           12, Assoc.CONTAINER), //$NON-NLS-1$
	SUB_INDEXED_ARGS ("○",         12, Assoc.CONTAINER), //$NON-NLS-1$
	SUB_INDEXED_ARG ("•",          12, Assoc.CONTAINER), //$NON-NLS-1$
	
	NS_GET ("::",                  13, Assoc.TERM), //$NON-NLS-1$
	NS_GET_INT (":::",             13, Assoc.TERM), //$NON-NLS-1$
	
	SUB_NAMED_PART ("$",           14, Assoc.LEFTSTD), //$NON-NLS-1$
	SUB_NAMED_SLOT ("@",           14, Assoc.LEFTSTD), //$NON-NLS-1$
	
	POWER ("^",                   101, Assoc.RIGHTSTD), //$NON-NLS-1$
	SIGN ("±",                    102, Assoc.LEFTSTD), //$NON-NLS-1$
	SEQ (":",                     103, Assoc.LEFTSTD), //$NON-NLS-1$
	SPECIAL ("%",                 104, Assoc.LEFTSTD), //$NON-NLS-1$
	MULT ("*/",                   105, Assoc.LEFTSTD), //$NON-NLS-1$
	ADD ("+-",                    106, Assoc.LEFTSTD), //$NON-NLS-1$
	RELATIONAL ("<>",             111, Assoc.NOSTD), //$NON-NLS-1$
	NOT ("!",                     112, Assoc.LEFTSTD), //$NON-NLS-1$
	AND ("&",                     113, Assoc.LEFTSTD), //$NON-NLS-1$
	OR ("|",                      114, Assoc.LEFTSTD), //$NON-NLS-1$
	
	MODEL ("~",                  1001, Assoc.LEFTSTD), //$NON-NLS-1$
	
	PIPE_FORWARD ("|>",           104, Assoc.LEFTSTD), //$NON-NLS-1$ // == SPECIAL
	
	A_RIGHT ("->",              10001, Assoc.LEFTSTD), //$NON-NLS-1$
	A_EQUALS ("=",              10005, Assoc.RIGHTSTD), // SPEC: precedence= 10002  //$NON-NLS-1$
	A_LEFT ("<-",               10003, Assoc.RIGHTSTD), //$NON-NLS-1$
	A_COLON (":=",              10003, Assoc.RIGHTSTD), // SPEC: missing            //$NON-NLS-1$
	
	C_IF ("if",                100001, Assoc.RIGHTSTD), //$NON-NLS-1$
	C_FOR ("for",              100002, Assoc.LEFTSTD), //$NON-NLS-1$
	C_WHILE ("while",          100002, Assoc.LEFTSTD), //$NON-NLS-1$
	C_REPEAT ("repeat",        100002, Assoc.LEFTSTD), //$NON-NLS-1$
	C_NEXT ("next",                 1, Assoc.TERM), //$NON-NLS-1$
	C_BREAK ("break",               1, Assoc.TERM), //$NON-NLS-1$
	F_DEF ("def",              100002, Assoc.RIGHTSTD), //$NON-NLS-1$
	F_DEF_ARGS ("○",               12, Assoc.CONTAINER), //$NON-NLS-1$
	F_DEF_ARG ("•",                12, Assoc.CONTAINER), //$NON-NLS-1$
	F_CALL ("call",                11, Assoc.LEFTSTD), //$NON-NLS-1$
	F_CALL_ARGS ("○",              12, Assoc.CONTAINER), //$NON-NLS-1$
	F_CALL_ARG ("•",               12, Assoc.CONTAINER), //$NON-NLS-1$
	
	HELP ("?",                  10101, Assoc.LEFTSTD), //$NON-NLS-1$
	;
	
	
	static interface Assoc {
		byte TERM= 1;
		byte CONTAINER= 2;
		byte NOSTD= 3;
		byte LEFTSTD= 4;
		byte RIGHTSTD= 5;
	}
	
	
	public final String label;
	public final int opPrec;
	final int opAssoc;
	
	
	NodeType(final String label, final int precedence, final int assoc) {
		this.label= label;
		this.opPrec= precedence;
		this.opAssoc= assoc;
	}
	
}
