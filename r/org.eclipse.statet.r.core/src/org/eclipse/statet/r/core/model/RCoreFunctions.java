/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import static org.eclipse.statet.r.core.model.RFunctionSpec.AS_N_VECTOR;
import static org.eclipse.statet.r.core.model.RFunctionSpec.AS_STRING;
import static org.eclipse.statet.r.core.model.RFunctionSpec.AS_SYMBOL;
import static org.eclipse.statet.r.core.model.RFunctionSpec.CLASS_NAME;
import static org.eclipse.statet.r.core.model.RFunctionSpec.FUN_OBJ;
import static org.eclipse.statet.r.core.model.RFunctionSpec.HELP_TOPIC_NAME;
import static org.eclipse.statet.r.core.model.RFunctionSpec.METHOD_NAME;
import static org.eclipse.statet.r.core.model.RFunctionSpec.METHOD_OBJ;
import static org.eclipse.statet.r.core.model.RFunctionSpec.OTHER_SPECIFIC_OBJ;
import static org.eclipse.statet.r.core.model.RFunctionSpec.PACKAGE_NAME;
import static org.eclipse.statet.r.core.model.RFunctionSpec.RELATOR_CODE;
import static org.eclipse.statet.r.core.model.RFunctionSpec.UNSPECIFIC_NAME;
import static org.eclipse.statet.r.core.model.RFunctionSpec.UNSPECIFIC_OBJ;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.source.RSourceConfig;


@NonNullByDefault
public class RCoreFunctions {
	
	
	private static final RCoreFunctions DEFAULT= new RCoreFunctions();
	
	public static RCoreFunctions getDefinitions(final @Nullable RSourceConfig config) {
		return DEFAULT;
	}
	
	
	public static final String BASE_PACKAGE_NAME= "base";
	public static final RElementName BASE_PACKAGE_NS_ELEMENT_NAME= RElementName.create(RElementName.SCOPE_NS, BASE_PACKAGE_NAME);
	
	
	public static final String BASE_Assign_NAME= "assign";
	public static final RElementName BASE_Assign_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Assign_NAME);
	public final RFunctionSpec BASE_Assign_fSpec;
	
	public static final String BASE_Remove_NAME= "remove";
	public static final RElementName BASE_Remove_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Remove_NAME);
	public static final String BASE_Remove_ALIAS_RM= "rm";
	public static final RElementName BASE_Remove_ALIAS_RM_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Remove_ALIAS_RM);
	public final RFunctionSpec BASE_Remove_fSpec;
	
	public static final String BASE_Exists_NAME= "exists";
	public static final RElementName BASE_Exists_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Exists_NAME);
	public final RFunctionSpec BASE_Exists_fSpec;
	
	public static final String BASE_Get_NAME= "get";
	public static final RElementName BASE_Get_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Get_NAME);
	public final RFunctionSpec BASE_Get_fSpec;
	
	public static final String BASE_Save_NAME= "save";
	public static final RElementName BASE_Save_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Save_NAME);
	public final RFunctionSpec BASE_Save_fSpec;
	
	
	public static final String BASE_IsNull_NAME= "is.null";
	public static final RElementName BASE_IsNull_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_IsNull_NAME);
	public final RFunctionSpec BASE_IsNull_fSpec;
	
	
	public static final String BASE_Call_NAME= "call";
	public static final RElementName BASE_Call_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Call_NAME);
	public final RFunctionSpec BASE_Call_fSpec;
	
	public static final String BASE_DoCall_NAME= "do.call";
	public static final RElementName BASE_DoCall_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_DoCall_NAME);
	public final RFunctionSpec BASE_DoCall_fSpec;
	
	
	public static final String BASE_Library_NAME= "library";
	public static final RElementName BASE_Library_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Library_NAME);
	public final RFunctionSpec BASE_Library_fSpec;
	
	public static final String BASE_Require_NAME= "require";
	public static final RElementName BASE_Require_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_Require_NAME);
	public final RFunctionSpec BASE_Require_fSpec;
	
	public static final String BASE_GlobalEnv_NAME= "globalenv";
	public static final RElementName BASE_GlobalEnv_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_GlobalEnv_NAME);
	public final RFunctionSpec BASE_GlobalEnv_fSpec;
	
	public static final String BASE_TopEnv_NAME= "topenv";
	public static final RElementName BASE_TopEnv_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_TopEnv_NAME);
	public final RFunctionSpec BASE_TopEnv_fSpec;
	
	public static final String BASE_GetNamespace_NAME= "getNamespace";
	public static final RElementName BASE_GetNamespace_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_GetNamespace_NAME);
	public final RFunctionSpec BASE_GetNamespace_fSpec;
	
	public static final String BASE_GetNamespaceName_NAME= "getNamespaceName";
	public static final RElementName BASE_GetNamespaceName_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_GetNamespaceName_NAME);
	public final RFunctionSpec BASE_GetNamespaceName_fSpec;
	
	public static final String BASE_GetNamespaceVersion_NAME= "getNamespaceVersion";
	public static final RElementName BASE_GetNamespaceVersion_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_GetNamespaceVersion_NAME);
	public final RFunctionSpec BASE_GetNamespaceVersion_fSpec;
	
	public static final String BASE_GetNamespaceExports_NAME= "getNamespaceExports";
	public static final RElementName BASE_GetNamespaceExports_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_GetNamespaceExports_NAME);
	public final RFunctionSpec BASE_GetNamespaceExports_fSpec;
	
	public static final String BASE_GetNamespaceImports_NAME= "getNamespaceImports";
	public static final RElementName BASE_GetNamespaceImports_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_GetNamespaceImports_NAME);
	public final RFunctionSpec BASE_GetNamespaceImports_fSpec;
	
	public static final String BASE_GetNamespaceUsers_NAME= "getNamespaceUsers";
	public static final RElementName BASE_GetNamespaceUsers_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_GetNamespaceUsers_NAME);
	public final RFunctionSpec BASE_GetNamespaceUsers_fSpec;
	
	public static final String BASE_GetExportedValue_NAME= "getExportedValue";
	public static final RElementName BASE_GetExportedValue_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_GetExportedValue_NAME);
	public final RFunctionSpec BASE_GetExportedValue_fSpec;
	
	public static final String BASE_AttachNamespace_NAME= "attachNamespace";
	public static final RElementName BASE_AttachNamespace_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_AttachNamespace_NAME);
	public final RFunctionSpec BASE_AttachNamespace_fSpec;
	
	public static final String BASE_LoadNamespace_NAME= "loadNamespace";
	public static final RElementName BASE_LoadNamespace_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_LoadNamespace_NAME);
	public final RFunctionSpec BASE_LoadNamespace_fSpec;
	
	public static final String BASE_RequireNamespace_NAME= "requireNamespace";
	public static final RElementName BASE_RequireNamespace_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_RequireNamespace_NAME);
	public final RFunctionSpec BASE_RequireNamespace_fSpec;
	
	public static final String BASE_IsNamespaceLoaded_NAME= "isNamespaceLoaded";
	public static final RElementName BASE_IsNamespaceLoaded_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_IsNamespaceLoaded_NAME);
	public final RFunctionSpec BASE_IsNamespaceLoaded_fSpec;
	
	public static final String BASE_UnloadNamespace_NAME= "unloadNamespace";
	public static final RElementName BASE_UnloadNamespace_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_UnloadNamespace_NAME);
	public final RFunctionSpec BASE_UnloadNamespace_fSpec;
	
	
	public static final String BASE_C_NAME= "c";
	public static final RElementName BASE_C_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_C_NAME);
	public final RFunctionSpec BASE_C_fSpec;
	
	public static final String BASE_DataFrame_NAME= "data.frame";
	public static final RElementName BASE_DataFrame_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_DataFrame_NAME);
	public final RFunctionSpec BASE_DataFrame_fSpec;
	
	
	public static final String BASE_UseMethod_NAME= "UseMethod";
	public static final RElementName BASE_UseMethod_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_UseMethod_NAME);
	public final RFunctionSpec BASE_UseMethod_fSpec;
	
	public static final String BASE_NextMethod_NAME= "NextMethod";
	public static final RElementName BASE_NextMethod_ELEMENT_NAME= createFunName(BASE_PACKAGE_NS_ELEMENT_NAME, BASE_NextMethod_NAME);
	public final RFunctionSpec BASE_NextMethod_fSpec;
	
	
	public static final String UTILS_PACKAGE_NAME= "utils";
	public static final RElementName UTILS_PACKAGE_NS_ELEMENT_NAME= RElementName.create(RElementName.SCOPE_NS, UTILS_PACKAGE_NAME);
	
	
	public static final String UTILS_Methods_NAME= "methods";
	public static final RElementName UTILS_Methods_ELEMENT_NAME= createFunName(UTILS_PACKAGE_NS_ELEMENT_NAME, UTILS_Methods_NAME);
	public final RFunctionSpec UTILS_Methods_fSpec;
	
	public static final String UTILS_GetS3Method_NAME= "getS3method";
	public static final RElementName UTILS_GetS3Method_ELEMENT_NAME= createFunName(UTILS_PACKAGE_NS_ELEMENT_NAME, UTILS_GetS3Method_NAME);
	public final RFunctionSpec UTILS_GetS3Method_fSpec;
	
	
	public static final String UTILS_Help_NAME= "help";
	public static final RElementName UTILS_Help_ELEMENT_NAME= createFunName(UTILS_PACKAGE_NS_ELEMENT_NAME, UTILS_Help_NAME);
	public final RFunctionSpec UTILS_Help_fSpec;
	
	public static final String UTILS_Person_NAME= "person";
	public static final RElementName UTILS_Person_ELEMENT_NAME= createFunName(UTILS_PACKAGE_NS_ELEMENT_NAME, UTILS_Person_NAME);
	public final RFunctionSpec UTILS_Person_fSpec;
	
	
	public static final String METHODS_PACKAGE_NAME= "methods";
	public static final RElementName METHODS_PACKAGE_NS_ELEMENT_NAME= RElementName.create(RElementName.SCOPE_NS, METHODS_PACKAGE_NAME);
	
	
	public static final String METHODS_SetGeneric_NAME= "setGeneric";
	public static final RElementName METHODS_SetGeneric_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SetGeneric_NAME);
	public final RFunctionSpec METHODS_SetGeneric_fSpec;
	
	public static final String METHODS_SetGroupGeneric_NAME= "setGroupGeneric";
	public static final RElementName METHODS_SetGroupGeneric_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SetGroupGeneric_NAME);
	public final RFunctionSpec METHODS_SetGroupGeneric_fSpec;
	
	public static final String METHODS_RemoveGeneric_NAME= "removeGeneric";
	public static final RElementName METHODS_RemoveGeneric_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_RemoveGeneric_NAME);
	public final RFunctionSpec METHODS_RemoveGeneric_fSpec;
	
	public static final String METHODS_IsGeneric_NAME= "isGeneric";
	public static final RElementName METHODS_IsGeneric_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_IsGeneric_NAME);
	public final RFunctionSpec METHODS_IsGeneric_fSpec;
	
	public static final String METHODS_IsGroup_NAME= "isGroup";
	public static final RElementName METHODS_IsGroup_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_IsGroup_NAME);
	public final RFunctionSpec METHODS_IsGroup_fSpec;
	
	public static final String METHODS_Signature_NAME= "signature";
	public static final RElementName METHODS_Signature_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_Signature_NAME);
	public final RFunctionSpec METHODS_Signature_fSpec;
	
	
	public static final String METHODS_SetClass_NAME= "setClass";
	public static final RElementName METHODS_SetClass_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SetClass_NAME);
	public final RFunctionSpec METHODS_SetClass_fSpec;
	
	public static final String METHODS_SetClassUnion_NAME= "setClassUnion";
	public static final RElementName METHODS_SetClassUnion_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SetClassUnion_NAME);
	public final RFunctionSpec METHODS_SetClassUnion_fSpec;
	
	public static final String METHODS_Representation_NAME= "representation";
	public static final RElementName METHODS_Representation_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_Representation_NAME);
	public final RFunctionSpec METHODS_Representation_fSpec;
	
	public static final String METHODS_Prototype_NAME= "prototype";
	public static final RElementName METHODS_Prototype_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_Prototype_NAME);
	public final RFunctionSpec METHODS_Prototype_fSpec;
	
	public static final String METHODS_SetIs_NAME= "setIs";
	public static final RElementName METHODS_SetIs_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SetIs_NAME);
	public final RFunctionSpec METHODS_SetIs_fSpec;
	
	public static final String METHODS_RemoveClass_NAME= "removeClass";
	public static final RElementName METHODS_RemoveClass_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_RemoveClass_NAME);
	public final RFunctionSpec METHODS_RemoveClass_fSpec;
	
	public static final String METHODS_ResetClass_NAME= "resetClass";
	public static final RElementName METHODS_ResetClass_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_ResetClass_NAME);
	public final RFunctionSpec METHODS_ResetClass_fSpec;
	
	public static final String METHODS_SetAs_NAME= "setAs";
	public static final RElementName METHODS_SetAs_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SetAs_NAME);
	public final RFunctionSpec METHODS_SetAs_fSpec;
	
	public static final String METHODS_SetValidity_NAME= "setValidity";
	public static final RElementName METHODS_SetValidity_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SetValidity_NAME);
	public final RFunctionSpec METHODS_SetValidity_fSpec;
	
	public static final String METHODS_IsClass_NAME= "isClass";
	public static final RElementName METHODS_IsClass_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_IsClass_NAME);
	public final RFunctionSpec METHODS_IsClass_fSpec;
	
	public static final String METHODS_Extends_NAME= "extends";
	public static final RElementName METHODS_Extends_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_Extends_NAME);
	public final RFunctionSpec METHODS_Extends_fSpec;
	
	public static final String METHODS_GetClass_NAME= "getClass";
	public static final RElementName METHODS_GetClass_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_GetClass_NAME);
	public final RFunctionSpec METHODS_GetClass_fSpec;
	
	public static final String METHODS_GetClassDef_NAME= "getClassDef";
	public static final RElementName METHODS_GetClassDef_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_GetClassDef_NAME);
	public final RFunctionSpec METHODS_GetClassDef_fSpec;
	
	public static final String METHODS_FindClass_NAME= "findClass";
	public static final RElementName METHODS_FindClass_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_FindClass_NAME);
	public final RFunctionSpec METHODS_FindClass_fSpec;
	
	
	public static final String METHODS_New_NAME= "new";
	public static final RElementName METHODS_New_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_New_NAME);
	public final RFunctionSpec METHODS_New_fSpec;
	
	public static final String METHODS_As_NAME= "as";
	public static final RElementName METHODS_As_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_As_NAME);
	public final RFunctionSpec METHODS_As_fSpec;
	
	public static final String METHODS_Is_NAME= "is";
	public static final RElementName METHODS_Is_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_Is_NAME);
	public final RFunctionSpec METHODS_Is_fSpec;
	
	
	public static final String METHODS_SetMethod_NAME= "setMethod";
	public static final RElementName METHODS_SetMethod_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SetMethod_NAME);
	public final RFunctionSpec METHODS_SetMethod_fSpec;
	
	public static final String METHODS_RemoveMethod_NAME= "removeMethod";
	public static final RElementName METHODS_RemoveMethod_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_RemoveMethod_NAME);
	public final RFunctionSpec METHODS_RemoveMethod_fSpec;
	
	public static final String METHODS_RemoveMethods_NAME= "removeMethods";
	public static final RElementName METHODS_RemoveMethods_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_RemoveMethods_NAME);
	public final RFunctionSpec METHODS_RemoveMethods_fSpec;
	
	public static final String METHODS_ExistsMethod_NAME= "existsMethod";
	public static final RElementName METHODS_ExistsMethod_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_ExistsMethod_NAME);
	public final RFunctionSpec METHODS_ExistsMethod_fSpec;
	
	public static final String METHODS_HasMethod_NAME= "hasMethod";
	public static final RElementName METHODS_HasMethod_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_HasMethod_NAME);
	public final RFunctionSpec METHODS_HasMethod_fSpec;
	
	public static final String METHODS_GetMethod_NAME= "getMethod";
	public static final RElementName METHODS_GetMethod_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_GetMethod_NAME);
	public final RFunctionSpec METHODS_GetMethod_fSpec;
	
	public static final String METHODS_SelectMethod_NAME= "selectMethod";
	public static final RElementName METHODS_SelectMethod_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_SelectMethod_NAME);
	public final RFunctionSpec METHODS_SelectMethod_fSpec;
	
	public static final String METHODS_GetMethods_NAME= "getMethods";
	public static final RElementName METHODS_GetMethods_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_GetMethods_NAME);
	public final RFunctionSpec METHODS_GetMethods_fSpec;
	
	public static final String METHODS_FindMethod_NAME= "findMethod";
	public static final RElementName METHODS_FindMethod_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_FindMethod_NAME);
	public final RFunctionSpec METHODS_FindMethod_fSpec;
	
	public static final String METHODS_DumpMethod_NAME= "dumpMethod";
	public static final RElementName METHODS_DumpMethod_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_DumpMethod_NAME);
	public final RFunctionSpec METHODS_DumpMethod_fSpec;
	
	public static final String METHODS_DumpMethods_NAME= "dumpMethods";
	public static final RElementName METHODS_DumpMethods_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_DumpMethods_NAME);
	public final RFunctionSpec METHODS_DumpMethods_fSpec;
	
	public static final String METHODS_Slot_NAME= "slot";
	public static final RElementName METHODS_Slot_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_Slot_NAME);
	public static final String METHODS_Slot_assign_NAME= "slot<-";
	public static final RElementName METHODS_Slot_assign_ELEMENT_NAME= createFunName(METHODS_PACKAGE_NS_ELEMENT_NAME, METHODS_Slot_assign_NAME);
	public final RFunctionSpec METHODS_Slot_fSpec;
	
	
	private static final class FunctionEntry {
		
		private final RElementName elementName;
		
		private final RFunctionSpec fSpec;
		
		
		public FunctionEntry(final RElementName elementName, final RFunctionSpec fSpec) {
			this.elementName= elementName;
			this.fSpec= fSpec;
		}
		
		
		@Override
		public String toString() {
			return this.elementName.toString();
		}
		
	}
	
	
	private final ImSet<String> packageNames;
	
	private final HashMap<String, FunctionEntry> nameSpecMap= new HashMap<>();
	
	private final Map<String, FunctionEntry> nameSpecMapIm= Collections.unmodifiableMap(this.nameSpecMap);
	
	
	protected RCoreFunctions() {
		this.packageNames= ImCollections.newSet(
				BASE_PACKAGE_NAME,
				UTILS_PACKAGE_NAME,
				METHODS_PACKAGE_NAME );
		
		this.BASE_Assign_fSpec= createBaseAssign();
		this.nameSpecMap.put(BASE_Assign_NAME, new FunctionEntry(BASE_Assign_ELEMENT_NAME,
				this.BASE_Assign_fSpec ));
		
		this.BASE_Remove_fSpec= createBaseRemove();
		this.nameSpecMap.put(BASE_Remove_NAME, new FunctionEntry(BASE_Remove_ELEMENT_NAME,
				this.BASE_Remove_fSpec ));
		this.nameSpecMap.put(BASE_Remove_ALIAS_RM, new FunctionEntry(BASE_Remove_ALIAS_RM_ELEMENT_NAME,
				this.BASE_Remove_fSpec ));
		
		this.BASE_Exists_fSpec= createBaseExists();
		this.nameSpecMap.put(BASE_Exists_NAME, new FunctionEntry(BASE_Exists_ELEMENT_NAME,
				this.BASE_Exists_fSpec ));
		
		this.BASE_Get_fSpec= createBaseGet();
		this.nameSpecMap.put(BASE_Get_NAME, new FunctionEntry(BASE_Get_ELEMENT_NAME,
				this.BASE_Get_fSpec ));
		
		this.BASE_Save_fSpec= createBaseSave();
		this.nameSpecMap.put(BASE_Save_NAME, new FunctionEntry(BASE_Save_ELEMENT_NAME,
				this.BASE_Save_fSpec ));
		
		
		this.BASE_IsNull_fSpec= createBaseIsNull();
		this.nameSpecMap.put(BASE_IsNull_NAME, new FunctionEntry(BASE_IsNull_ELEMENT_NAME,
				this.BASE_IsNull_fSpec ));
		
		
		this.BASE_Call_fSpec= createBaseCall();
		this.nameSpecMap.put(BASE_Call_NAME, new FunctionEntry(BASE_Call_ELEMENT_NAME,
				this.BASE_Call_fSpec ));
		
		this.BASE_DoCall_fSpec= createBaseDoCall();
		this.nameSpecMap.put(BASE_DoCall_NAME, new FunctionEntry(BASE_DoCall_ELEMENT_NAME,
				this.BASE_DoCall_fSpec ));
		
		
		this.BASE_Library_fSpec= createBaseLibrary();
		this.nameSpecMap.put(BASE_Library_NAME, new FunctionEntry(BASE_Library_ELEMENT_NAME,
				this.BASE_Library_fSpec ));
		
		this.BASE_Require_fSpec= createBaseRequire();
		this.nameSpecMap.put(BASE_Require_NAME, new FunctionEntry(BASE_Require_ELEMENT_NAME,
				this.BASE_Require_fSpec ));
		
		this.BASE_GlobalEnv_fSpec= createBaseGlobalenv();
		this.nameSpecMap.put(BASE_GlobalEnv_NAME, new FunctionEntry(BASE_GlobalEnv_ELEMENT_NAME,
				this.BASE_GlobalEnv_fSpec ));
		
		this.BASE_TopEnv_fSpec= createBaseTopenv();
		this.nameSpecMap.put(BASE_TopEnv_NAME, new FunctionEntry(BASE_TopEnv_ELEMENT_NAME,
				this.BASE_TopEnv_fSpec ));
		
		this.BASE_GetNamespace_fSpec= createBaseGetNamespace();
		this.nameSpecMap.put(BASE_GetNamespace_NAME, new FunctionEntry(BASE_GetNamespace_ELEMENT_NAME,
				this.BASE_GetNamespace_fSpec ));
		
		this.BASE_GetNamespaceName_fSpec= createBaseGetNamespaceName();
		this.nameSpecMap.put(BASE_GetNamespaceName_NAME, new FunctionEntry(BASE_GetNamespaceName_ELEMENT_NAME,
				this.BASE_GetNamespaceName_fSpec ));
		
		this.BASE_GetNamespaceVersion_fSpec= createBaseGetNamespaceVersion();
		this.nameSpecMap.put(BASE_GetNamespaceVersion_NAME, new FunctionEntry(BASE_GetNamespaceVersion_ELEMENT_NAME,
				this.BASE_GetNamespaceVersion_fSpec ));
		
		this.BASE_GetNamespaceExports_fSpec= createBaseGetNamespaceExports();
		this.nameSpecMap.put(BASE_GetNamespaceExports_NAME, new FunctionEntry(BASE_GetNamespaceExports_ELEMENT_NAME,
				this.BASE_GetNamespaceExports_fSpec ));
		
		this.BASE_GetNamespaceImports_fSpec= createBaseGetNamespaceImports();
		this.nameSpecMap.put(BASE_GetNamespaceImports_NAME, new FunctionEntry(BASE_GetNamespaceImports_ELEMENT_NAME,
				this.BASE_GetNamespaceImports_fSpec ));
		
		this.BASE_GetNamespaceUsers_fSpec= createBaseGetNamespaceUsers();
		this.nameSpecMap.put(BASE_GetNamespaceUsers_NAME, new FunctionEntry(BASE_GetNamespaceUsers_ELEMENT_NAME,
				this.BASE_GetNamespaceUsers_fSpec ));
		
		this.BASE_GetExportedValue_fSpec= createBaseGetExportedValue();
		this.nameSpecMap.put(BASE_GetExportedValue_NAME, new FunctionEntry(BASE_GetExportedValue_ELEMENT_NAME,
				this.BASE_GetExportedValue_fSpec ));
		
		this.BASE_AttachNamespace_fSpec= createBaseAttachNamespace();
		this.nameSpecMap.put(BASE_AttachNamespace_NAME, new FunctionEntry(BASE_AttachNamespace_ELEMENT_NAME,
				this.BASE_AttachNamespace_fSpec ));
		
		this.BASE_LoadNamespace_fSpec= createBaseLoadNamespace();
		this.nameSpecMap.put(BASE_LoadNamespace_NAME, new FunctionEntry(BASE_LoadNamespace_ELEMENT_NAME,
				this.BASE_LoadNamespace_fSpec ));
		
		this.BASE_RequireNamespace_fSpec= createBaseRequireNamespace();
		this.nameSpecMap.put(BASE_RequireNamespace_NAME, new FunctionEntry(BASE_RequireNamespace_ELEMENT_NAME,
				this.BASE_RequireNamespace_fSpec ));
		
		this.BASE_IsNamespaceLoaded_fSpec= createBaseIsNamespaceLoaded();
		this.nameSpecMap.put(BASE_IsNamespaceLoaded_NAME, new FunctionEntry(BASE_IsNamespaceLoaded_ELEMENT_NAME,
				this.BASE_IsNamespaceLoaded_fSpec ));
		
		this.BASE_UnloadNamespace_fSpec= createBaseUnloadNamespace();
		this.nameSpecMap.put(BASE_UnloadNamespace_NAME, new FunctionEntry(BASE_UnloadNamespace_ELEMENT_NAME,
				this.BASE_UnloadNamespace_fSpec ));
		
		
		this.BASE_C_fSpec= createBaseC();
		this.nameSpecMap.put(BASE_C_NAME, new FunctionEntry(BASE_C_ELEMENT_NAME,
				this.BASE_C_fSpec ));
		
		this.BASE_DataFrame_fSpec= createBaseC();
		this.nameSpecMap.put(BASE_DataFrame_NAME, new FunctionEntry(BASE_DataFrame_ELEMENT_NAME,
				this.BASE_C_fSpec ));
		
		this.BASE_UseMethod_fSpec= createBaseUseMethod();
		this.nameSpecMap.put(BASE_UseMethod_NAME, new FunctionEntry(BASE_UseMethod_ELEMENT_NAME,
				this.BASE_UseMethod_fSpec ));
		
		this.BASE_NextMethod_fSpec= createBaseNextMethod();
		this.nameSpecMap.put(BASE_NextMethod_NAME, new FunctionEntry(BASE_NextMethod_ELEMENT_NAME,
				this.BASE_NextMethod_fSpec ));
		
		
		this.UTILS_Methods_fSpec= createUtilsMethods();
		this.nameSpecMap.put(UTILS_Methods_NAME, new FunctionEntry(UTILS_Methods_ELEMENT_NAME,
				this.UTILS_Methods_fSpec ));
		
		this.UTILS_GetS3Method_fSpec= createUtilsGetS3Method();
		this.nameSpecMap.put(UTILS_GetS3Method_NAME, new FunctionEntry(UTILS_GetS3Method_ELEMENT_NAME,
				this.UTILS_GetS3Method_fSpec ));
		
		this.UTILS_Help_fSpec= createUtilsHelp();
		this.nameSpecMap.put(UTILS_Help_NAME, new FunctionEntry(UTILS_Help_ELEMENT_NAME,
				this.UTILS_Help_fSpec ));
		
		this.UTILS_Person_fSpec= createUtilsPerson();
		this.nameSpecMap.put(UTILS_Person_NAME, new FunctionEntry(UTILS_Person_ELEMENT_NAME,
				this.UTILS_Person_fSpec ));
		
		
		this.METHODS_SetGeneric_fSpec= createMethodsSetGeneric();
		this.nameSpecMap.put(METHODS_SetGeneric_NAME, new FunctionEntry(METHODS_SetGeneric_ELEMENT_NAME,
				this.METHODS_SetGeneric_fSpec ));
		
		this.METHODS_SetGroupGeneric_fSpec= createMethodsSetGroupGeneric();
		this.nameSpecMap.put(METHODS_SetGroupGeneric_NAME, new FunctionEntry(METHODS_SetGroupGeneric_ELEMENT_NAME,
				this.METHODS_SetGroupGeneric_fSpec ));
		
		this.METHODS_RemoveGeneric_fSpec= createMethodsRemoveGeneric();
		this.nameSpecMap.put(METHODS_RemoveGeneric_NAME, new FunctionEntry(METHODS_RemoveGeneric_ELEMENT_NAME,
				this.METHODS_RemoveGeneric_fSpec ));
		
		this.METHODS_IsGeneric_fSpec= createMethodsIsGeneric();
		this.nameSpecMap.put(METHODS_IsGeneric_NAME, new FunctionEntry(METHODS_IsGeneric_ELEMENT_NAME,
				this.METHODS_IsGeneric_fSpec ));
		
		this.METHODS_IsGroup_fSpec= createMethodsIsGroup();
		this.nameSpecMap.put(METHODS_IsGroup_NAME, new FunctionEntry(METHODS_IsGroup_ELEMENT_NAME,
				this.METHODS_IsGroup_fSpec ));
		
		this.METHODS_Signature_fSpec= createMethodsSignature();
		this.nameSpecMap.put(METHODS_Signature_NAME, new FunctionEntry(METHODS_Signature_ELEMENT_NAME,
				this.METHODS_Signature_fSpec ));
		
		this.METHODS_SetClass_fSpec= createMethodsSetClass();
		this.nameSpecMap.put(METHODS_SetClass_NAME, new FunctionEntry(METHODS_SetClass_ELEMENT_NAME,
				this.METHODS_SetClass_fSpec ));
		
		this.METHODS_SetClassUnion_fSpec= createMethodsSetClassUnion();
		this.nameSpecMap.put(METHODS_SetClassUnion_NAME, new FunctionEntry(METHODS_SetClassUnion_ELEMENT_NAME,
				this.METHODS_SetClassUnion_fSpec ));
		
		this.METHODS_Representation_fSpec= createMethodsRepresentation();
		this.nameSpecMap.put(METHODS_Representation_NAME, new FunctionEntry(METHODS_Representation_ELEMENT_NAME,
				this.METHODS_Representation_fSpec ));
		
		this.METHODS_Prototype_fSpec= createMethodsPrototype();
		this.nameSpecMap.put(METHODS_Prototype_NAME, new FunctionEntry(METHODS_Prototype_ELEMENT_NAME,
				this.METHODS_Prototype_fSpec ));
		
		this.METHODS_SetIs_fSpec= createMethodsSetIs();
		this.nameSpecMap.put(METHODS_SetIs_NAME, new FunctionEntry(METHODS_SetIs_ELEMENT_NAME,
				this.METHODS_SetIs_fSpec ));
		
		this.METHODS_RemoveClass_fSpec= createMethodsRemoveClass();
		this.nameSpecMap.put(METHODS_RemoveClass_NAME, new FunctionEntry(METHODS_RemoveClass_ELEMENT_NAME,
				this.METHODS_RemoveClass_fSpec ));
		
		this.METHODS_ResetClass_fSpec= createMethodsResetClass();
		this.nameSpecMap.put(METHODS_ResetClass_NAME, new FunctionEntry(METHODS_ResetClass_ELEMENT_NAME,
				this.METHODS_ResetClass_fSpec ));
		
		this.METHODS_SetAs_fSpec= createMethodsSetAs();
		this.nameSpecMap.put(METHODS_SetAs_NAME, new FunctionEntry(METHODS_SetAs_ELEMENT_NAME,
				this.METHODS_SetAs_fSpec ));
		
		this.METHODS_SetValidity_fSpec= createMethodsSetValidity();
		this.nameSpecMap.put(METHODS_SetValidity_NAME, new FunctionEntry(METHODS_SetValidity_ELEMENT_NAME,
				this.METHODS_SetValidity_fSpec ));
		
		this.METHODS_IsClass_fSpec= createMethodsIsClass();
		this.nameSpecMap.put(METHODS_IsClass_NAME, new FunctionEntry(METHODS_IsClass_ELEMENT_NAME,
				this.METHODS_IsClass_fSpec ));
		
		this.METHODS_Extends_fSpec= createMethodsExtends();
		this.nameSpecMap.put(METHODS_Extends_NAME, new FunctionEntry(METHODS_Extends_ELEMENT_NAME,
				this.METHODS_Extends_fSpec ));
		
		this.METHODS_GetClass_fSpec= createMethodsGetClass();
		this.nameSpecMap.put(METHODS_GetClass_NAME, new FunctionEntry(METHODS_GetClass_ELEMENT_NAME,
				this.METHODS_GetClass_fSpec ));
		
		this.METHODS_GetClassDef_fSpec= createMethodsGetClassDef();
		this.nameSpecMap.put(METHODS_GetClassDef_NAME, new FunctionEntry(METHODS_GetClassDef_ELEMENT_NAME,
				this.METHODS_GetClassDef_fSpec ));
		
		this.METHODS_FindClass_fSpec= createMethodsFindClass();
		this.nameSpecMap.put(METHODS_FindClass_NAME, new FunctionEntry(METHODS_FindClass_ELEMENT_NAME,
				this.METHODS_FindClass_fSpec ));
		
		this.METHODS_New_fSpec= createMethodsNew();
		this.nameSpecMap.put(METHODS_New_NAME, new FunctionEntry(METHODS_New_ELEMENT_NAME,
				this.METHODS_New_fSpec ));
		
		this.METHODS_As_fSpec= createMethodsAs();
		this.nameSpecMap.put(METHODS_As_NAME, new FunctionEntry(METHODS_As_ELEMENT_NAME,
				this.METHODS_As_fSpec ));
		
		this.METHODS_Is_fSpec= createMethodsIs();
		this.nameSpecMap.put(METHODS_Is_NAME, new FunctionEntry(METHODS_Is_ELEMENT_NAME,
				this.METHODS_Is_fSpec ));
		
		this.METHODS_SetMethod_fSpec= createMethodsSetMethod();
		this.nameSpecMap.put(METHODS_SetMethod_NAME, new FunctionEntry(METHODS_SetMethod_ELEMENT_NAME,
				this.METHODS_SetMethod_fSpec ));
		
		this.METHODS_RemoveMethod_fSpec= createMethodsRemoveMethod();
		this.nameSpecMap.put(METHODS_RemoveMethod_NAME, new FunctionEntry(METHODS_RemoveMethod_ELEMENT_NAME,
				this.METHODS_RemoveMethod_fSpec ));
		
		this.METHODS_RemoveMethods_fSpec= createMethodsRemoveMethods();
		this.nameSpecMap.put(METHODS_RemoveMethods_NAME, new FunctionEntry(METHODS_RemoveMethods_ELEMENT_NAME,
				this.METHODS_RemoveMethods_fSpec ));
		
		this.METHODS_ExistsMethod_fSpec= createMethodsExistsMethod();
		this.nameSpecMap.put(METHODS_ExistsMethod_NAME, new FunctionEntry(METHODS_ExistsMethod_ELEMENT_NAME,
				this.METHODS_ExistsMethod_fSpec ));
		
		this.METHODS_HasMethod_fSpec= createMethodsHasMethod();
		this.nameSpecMap.put(METHODS_HasMethod_NAME, new FunctionEntry(METHODS_HasMethod_ELEMENT_NAME,
				this.METHODS_HasMethod_fSpec ));
		
		this.METHODS_GetMethod_fSpec= createMethodsGetMethod();
		this.nameSpecMap.put(METHODS_GetMethod_NAME, new FunctionEntry(METHODS_GetMethod_ELEMENT_NAME,
				this.METHODS_GetMethod_fSpec ));
		
		this.METHODS_SelectMethod_fSpec= createMethodsSelectMethod();
		this.nameSpecMap.put(METHODS_SelectMethod_NAME, new FunctionEntry(METHODS_SelectMethod_ELEMENT_NAME,
				this.METHODS_SelectMethod_fSpec ));
		
		this.METHODS_GetMethods_fSpec= createMethodsGetMethods();
		this.nameSpecMap.put(METHODS_GetMethods_NAME, new FunctionEntry(METHODS_GetMethods_ELEMENT_NAME,
				this.METHODS_GetMethods_fSpec ));
		
		this.METHODS_FindMethod_fSpec= createMethodsFindMethod();
		this.nameSpecMap.put(METHODS_FindMethod_NAME, new FunctionEntry(METHODS_FindMethod_ELEMENT_NAME,
				this.METHODS_FindMethod_fSpec ));
		
		this.METHODS_DumpMethod_fSpec= createMethodsDumpMethod();
		this.nameSpecMap.put(METHODS_DumpMethod_NAME, new FunctionEntry(METHODS_DumpMethod_ELEMENT_NAME,
				this.METHODS_DumpMethod_fSpec ));
		
		this.METHODS_DumpMethods_fSpec= createMethodsDumpMethods();
		this.nameSpecMap.put(METHODS_DumpMethods_NAME, new FunctionEntry(METHODS_DumpMethods_ELEMENT_NAME,
				this.METHODS_DumpMethods_fSpec ));
		
		this.METHODS_Slot_fSpec= createMethodsSlot();
		this.nameSpecMap.put(METHODS_Slot_NAME, new FunctionEntry(METHODS_Slot_ELEMENT_NAME,
				this.METHODS_Slot_fSpec ));
	}
	
	
	protected RFunctionSpec createBaseAssign() {
		return new RFunctionSpecBuilder()
				.addParam("x", UNSPECIFIC_NAME | AS_STRING)
				.addParams("value", "pos", "envir", "inherits", "immediate")
				.build();
	}
	
	protected RFunctionSpec createBaseRemove() {
		return new RFunctionSpecBuilder()
				.addParam("...", UNSPECIFIC_NAME | AS_STRING | AS_SYMBOL)
				.addParams("list", "pos", "envir", "inherits")
				.build();
	}
	
	protected RFunctionSpec createBaseExists() {
		return new RFunctionSpecBuilder()
				.addParam("x", UNSPECIFIC_NAME | AS_STRING)
				.addParams("where", "envir", "frame", "mode", "inherits")
				.build();
	}
	
	protected RFunctionSpec createBaseGet() {
		return new RFunctionSpecBuilder()
				.addParam("x", UNSPECIFIC_NAME | AS_STRING)
				.addParams("pos", "envir", "mode", "inherits")
				.build();
	}
	
	protected RFunctionSpec createBaseGet0() {
		return new RFunctionSpecBuilder()
				.addParam("x", UNSPECIFIC_NAME | AS_STRING)
				.addParams("envir", "mode", "inherits", "ifnotfound")
				.build();
	}
	
	protected RFunctionSpec createBaseSave() {
		return new RFunctionSpecBuilder()
				.addParam("...", UNSPECIFIC_NAME | AS_STRING | AS_SYMBOL)
				.addParam("list", UNSPECIFIC_NAME | AS_STRING)
				.addParams("file", "ascii", "version", "envir", "compress", "compress_level",
						"eval.promises", "precheck" )
				.build();
	}
	
	protected RFunctionSpec createBaseIsNull() {
		return new RFunctionSpecBuilder()
				.addParam("x", UNSPECIFIC_OBJ)
				.build();
	}
	
	protected RFunctionSpec createBaseCall() {
		return new RFunctionSpecBuilder()
				.addParam("name", UNSPECIFIC_NAME | AS_STRING)
				.addParam("...")
				.build();
	}
	
	protected RFunctionSpec createBaseDoCall() {
		return new RFunctionSpecBuilder()
				.addParam("what", FUN_OBJ | AS_SYMBOL | AS_STRING)
				.addParams("args", "quote", "envir")
				.build();
	}
	
	protected RFunctionSpec createBaseLibrary() {
		return new RFunctionSpecBuilder()
				.addParam("package", PACKAGE_NAME | AS_STRING | AS_SYMBOL)
				.addParam("help", PACKAGE_NAME | AS_STRING | AS_SYMBOL)
				.addParams("pos", "lib.loc", "character.only", "logical.return", "warn.conflicts",
						"quietly", "verbose" )
				.build();
	}
	
	protected RFunctionSpec createBaseRequire() {
		return new RFunctionSpecBuilder()
				.addParam("package", PACKAGE_NAME | AS_STRING | AS_SYMBOL)
				.addParams("lib.loc", "quietly", "warn.conflicts", "character.only")
				.build();
	}
	
	protected RFunctionSpec createBaseGlobalenv() {
		return new RFunctionSpec();
	}
	
	protected RFunctionSpec createBaseTopenv() {
		return new RFunctionSpec(
				"envir", "matchThisEnv");
	}
	
	protected RFunctionSpec createBaseGetNamespace() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING | AS_SYMBOL)
				.build();
	}
	
	protected RFunctionSpec createBaseGetNamespaceName() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING)
				.build();
	}
	
	protected RFunctionSpec createBaseGetNamespaceVersion() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING)
				.build();
	}
	
	protected RFunctionSpec createBaseGetNamespaceExports() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING)
				.build();
	}
	
	protected RFunctionSpec createBaseGetNamespaceImports() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING)
				.build();
	}
	
	protected RFunctionSpec createBaseGetNamespaceUsers() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING)
				.build();
	}
	
	protected RFunctionSpec createBaseGetExportedValue() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING)
				.addParam("name")
				.build();
	}
	
	protected RFunctionSpec createBaseAttachNamespace() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING)
				.addParams("pos", "depends")
				.build();
	}
	
	protected RFunctionSpec createBaseLoadNamespace() {
		return new RFunctionSpecBuilder()
				.addParam("package", PACKAGE_NAME | AS_STRING)
				.addParams("lib.loc", "keep.source", "partial", "versionCheck")
				.build();
	}
	
	protected RFunctionSpec createBaseRequireNamespace() {
		return new RFunctionSpecBuilder()
				.addParam("package", PACKAGE_NAME | AS_STRING)
				.addParams("...", "quietly")
				.build();
	}
	
	protected RFunctionSpec createBaseIsNamespaceLoaded() {
		return new RFunctionSpecBuilder()
				.addParam("name", PACKAGE_NAME | AS_STRING | AS_SYMBOL)
				.build();
	}
	
	protected RFunctionSpec createBaseUnloadNamespace() {
		return new RFunctionSpecBuilder()
				.addParam("ns", PACKAGE_NAME | AS_STRING)
				.build();
	}
	
	
	protected RFunctionSpec createBaseC() {
		return new RFunctionSpecBuilder()
				.addParam("...")
				.addParam("recursive")
				.build();
	}
	
	protected RFunctionSpec createBaseDataFrame() {
		return new RFunctionSpecBuilder()
				.addParam("...")
				.addParam("row.names")
				.addParam("check.rows")
				.addParam("check.names")
				.addParam("stringsAsFactors")
				.build();
	}
	
	protected RFunctionSpec createBaseUseMethod() {
		return new RFunctionSpecBuilder()
				.addParam("generic", METHOD_NAME)
				.addParam("object")
				.build();
	}
	
	protected RFunctionSpec createBaseNextMethod() {
		return new RFunctionSpecBuilder()
				.addParam("name", METHOD_NAME)
				.addParams("object", "...")
				.build();
	}
	
	protected RFunctionSpec createUtilsMethods() {
		return new RFunctionSpecBuilder()
				.addParam("generic.function", METHOD_OBJ | AS_SYMBOL | AS_STRING)
				.addParam("class", CLASS_NAME)
				.build();
	}
	
	protected RFunctionSpec createUtilsGetS3Method() {
		return new RFunctionSpecBuilder()
				.addParam("f", METHOD_OBJ | AS_STRING)
				.addParam("class", CLASS_NAME)
				.addParam("optional")
				.build();
	}
	
	protected RFunctionSpec createUtilsHelp() {
		return new RFunctionSpecBuilder()
				.addParam("topic", HELP_TOPIC_NAME | AS_STRING | AS_SYMBOL)
				.addParam("package", PACKAGE_NAME | AS_STRING | AS_SYMBOL)
				.addParams("lib.loc", "verbose", "try.all.packages", "help_type")
				.build();
	}
	
	protected RFunctionSpec createUtilsPerson() {
		return new RFunctionSpecBuilder()
				.addParams("given", "family", "middle")
				.addParam("email")
				.addParam("role", RELATOR_CODE | AS_STRING | AS_N_VECTOR)
				.addParam("comment")
				.returns(OTHER_SPECIFIC_OBJ, UTILS_Person_NAME)
				.build();
	}
	
	
	protected RFunctionSpec createMethodsSetGeneric() {
		return new RFunctionSpec(
				"name", "def", "group", "valueClass", "where", "package",
				"signature" , "useAsDefault" , "genericFunction");
	}
	
	protected RFunctionSpec createMethodsSetGroupGeneric() {
		return new RFunctionSpec(
				"name", "def", "group", "valueClass", "knownMembers",
				"where", "package");
	}
	
	protected RFunctionSpec createMethodsRemoveGeneric() {
		return new RFunctionSpecBuilder()
				.addParam("f", METHOD_NAME)
				.addParam("where")
				.build();
	}
	
	protected RFunctionSpec createMethodsIsGeneric() {
		return new RFunctionSpecBuilder()
				.addParam("f", METHOD_NAME)
				.addParams("where", "fdef", "getName")
				.build();
	}
	
	protected RFunctionSpec createMethodsIsGroup() {
		return new RFunctionSpecBuilder()
				.addParam("f", METHOD_NAME)
				.addParams("where", "fdef")
				.build();
	}
	
	protected RFunctionSpec createMethodsSignature() {
		return new RFunctionSpec(
				"...");
	}
	
	protected RFunctionSpec createMethodsSetClass() {
		return new RFunctionSpecBuilder()
			.addParam("Class", CLASS_NAME)
			.addParams("representation", "prototype", "contains", "validity",
				"access", "where", "version", "sealed", "package")
			.build();
	}
	
	protected RFunctionSpec createMethodsSetClassUnion() {
		return new RFunctionSpecBuilder()
			.addParam("name", CLASS_NAME)
			.addParams("members", "where")
			.build();
	}
	
	protected RFunctionSpec createMethodsRepresentation() {
		return new RFunctionSpec(
				"...");
	}
	
	protected RFunctionSpec createMethodsPrototype() {
		return new RFunctionSpec(
				"...");
	}
	
	protected RFunctionSpec createMethodsSetIs() {
		return new RFunctionSpec(
				"class1", "class2", "test", "coerce", "replace", "by", "where",
				"classDef", "extensionObject", "doComplete");
	}
	
	protected RFunctionSpec createMethodsRemoveClass() {
		return new RFunctionSpecBuilder()
				.addParam("Class", CLASS_NAME)
				.addParam("where")
				.build();
	}
	
	protected RFunctionSpec createMethodsResetClass() {
		return new RFunctionSpecBuilder()
				.addParam("Class") // no naming
				.addParams("classDef", "where")
				.build();
	}
	
	protected RFunctionSpec createMethodsSetAs() {
		return new RFunctionSpecBuilder()
				.addParam("from", CLASS_NAME)
				.addParam("to", CLASS_NAME)
				.addParams("def", "replace", "where")
				.build();
	}
	
	protected RFunctionSpec createMethodsSetValidity() {
		return new RFunctionSpecBuilder()
				.addParam("Class", CLASS_NAME) // no naming
				.addParams("method", "where")
				.build();
	}
	
	protected RFunctionSpec createMethodsIsClass() {
		return new RFunctionSpecBuilder()
				.addParam("Class", CLASS_NAME)
				.addParams("formal", "where")
				.build();
	}
	
	protected RFunctionSpec createMethodsGetClass() {
		return new RFunctionSpec(
				"Class", ".Force", "where");
	}
	
	protected RFunctionSpec createMethodsGetClassDef() {
		return new RFunctionSpec(
				"Class", "where", "package");
	}
	
	protected RFunctionSpec createMethodsFindClass() {
		return new RFunctionSpec(
				"Class", "where", "unique");
	}
	
	protected RFunctionSpec createMethodsExtends() {
		return new RFunctionSpecBuilder()
				.addParam("class1", CLASS_NAME)
				.addParam("class2", CLASS_NAME)
				.addParams("maybe", "fullInfo")
				.build();
	}
	
	protected RFunctionSpec createMethodsNew() {
		return new RFunctionSpecBuilder()
				.addParam("Class", CLASS_NAME)
				.addParam("...")
				.build();
	}
	
	protected RFunctionSpec createMethodsAs() {
		return new RFunctionSpecBuilder()
				.addParam("object")
				.addParam("Class", CLASS_NAME)
				.addParams("strict", "ext")
				.build();
	}
	
	protected RFunctionSpec createMethodsIs() {
		return new RFunctionSpecBuilder()
				.addParam("object")
				.addParam("class2", CLASS_NAME)
				.build();
	}
	
	protected RFunctionSpec createMethodsSetMethod() {
		return new RFunctionSpec(
				"f", "signature", "definition", "where", "valueClass", "sealed");
	}
	
	protected RFunctionSpec createMethodsRemoveMethod() {
		return new RFunctionSpec(
				"f", "signature", "where");
	}
	
	protected RFunctionSpec createMethodsRemoveMethods() {
		return new RFunctionSpec(
				"f", "where", "all");
	}
	
	protected RFunctionSpec createMethodsExistsMethod() {
		return new RFunctionSpecBuilder()
				.addParam("f", METHOD_NAME)
				.addParams("signature", "where")
				.build();
	}
	
	protected RFunctionSpec createMethodsHasMethod() {
		return new RFunctionSpecBuilder()
				.addParam("f", METHOD_NAME)
				.addParams("signature", "where")
				.build();
	}
	
	protected RFunctionSpec createMethodsGetMethod() {
		return new RFunctionSpec(
				"f", "signature", "where", "optional", "mlist", "fdef");
	}
	
	protected RFunctionSpec createMethodsSelectMethod() {
		return new RFunctionSpec(
				"f", "signature", "optional", "useInherited", "mlist", "fdef", "verbose");
	}
	
	protected RFunctionSpec createMethodsGetMethods() {
		return new RFunctionSpec(
				"f", "where");
	}
	
	protected RFunctionSpec createMethodsFindMethod() {
		return new RFunctionSpec(
				"f", "signature", "where");
	}
	
	protected RFunctionSpec createMethodsDumpMethod() {
		return new RFunctionSpecBuilder()
				.addParam("f", METHOD_NAME)
				.addParams("signature", "file", "where", "def")
				.build();
	}
	
	protected RFunctionSpec createMethodsDumpMethods() {
		return new RFunctionSpecBuilder()
				.addParam("f", METHOD_NAME)
				.addParams("file", "signature", "methods", "where")
				.build();
	}
	
	protected RFunctionSpec createMethodsSlot() {
		return new RFunctionSpec(
				"object", "name", "check");
	}
	
	
	public ImSet<String> getPackageNames() {
		return this.packageNames;
	}
	
	public Set<String> getKnownFunctions() {
		return this.nameSpecMapIm.keySet();
	}
	
	public @Nullable RElementName getElementName(final @Nullable String name) {
		final FunctionEntry spec= this.nameSpecMap.get(name);
		return (spec != null) ? spec.elementName : null;
	}
	
	public @Nullable RFunctionSpec getFunctionSpec(final @Nullable String name) {
		final FunctionEntry spec= this.nameSpecMap.get(name);
		return (spec != null) ? spec.fSpec : null;
	}
	
	
	private static RElementName createFunName(final RElementName packageName, final String name) {
		return RElementName.addScope(
				RElementName.create(RElementName.MAIN_DEFAULT, name),
				packageName );
	}
	
	private static RElementName createClassName(final RElementName packageName, final String name) {
		return RElementName.addScope(
				RElementName.create(RElementName.MAIN_CLASS, name),
				packageName );
	}
	
}
