/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.doc;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.isNonNull;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode.END_UNCLOSED;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.input.DocumentParserInput;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNodeScan;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNodeScan.State;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNodeScanner;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNodeType;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitioner;
import org.eclipse.statet.ecommons.text.core.treepartitioner.WrappedPartitionNodeScan;

import org.eclipse.statet.dsl.dcf.core.source.DcfLexer;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;
import org.eclipse.statet.r.core.model.RPkgDescriptions;


@NonNullByDefault
public class RPkgDescrPartitionNodeScanner implements TreePartitionNodeScanner {
	
	
	public static final @Nullable TreePartitionNode findDcfRootNode(@Nullable TreePartitionNode node) {
		while (true) {
			if (node == null) {
				return null;
			}
			if (node.getType() instanceof RPkgDescrPartitionNodeType) {
				break;
			}
			node= node.getParent();
		}
		TreePartitionNode parentNode;
		while ((parentNode= node.getParent()) != null
				&& parentNode.getType() instanceof RPkgDescrPartitionNodeType) {
			node= parentNode;
		}
		return node;
	}
	
	
	private final DcfLexer lexer;
	
	private TreePartitionNodeScan scan;
	
	private TreePartitionNode rootNode;
	
	/** The current node */
	private TreePartitionNode node;
	/** The current node type */
	private RPkgDescrPartitionNodeType type;
	
	private final RChunkPartitionNodeScanner rScanner= new RChunkPartitionNodeScanner();
	
	private WrappedPartitionNodeScan rScan;
	
	private int rStartOffset;
	private @Nullable TreePartitionNode rStartNode;
	
	
	public RPkgDescrPartitionNodeScanner() {
		this.lexer= new DcfLexer();
	}
	
	
	@Override
	public RPkgDescrPartitionNodeType getDefaultRootType() {
		return RPkgDescrPartitionNodeType.DEFAULT_ROOT;
	}
	
	@Override
	public void checkRestartState(final State state,
			final IDocument document, final TreePartitioner partitioner)
			throws BadLocationException {
		final RPkgDescrPartitionNodeType rootType= getDefaultRootType();
		TreePartitionNode node= state.node;
		
		int offset= state.offset;
		
		while (node.getType() != rootType) {
			offset= node.getStartOffset();
			node= node.getParent();
		}
		
		state.offset= offset;
		state.node= node;
	}
	
	@Override
	public void execute(final TreePartitionNodeScan scan) {
		this.scan= scan;
		this.node= null;
		this.rScan= new WrappedPartitionNodeScan(scan);
		
		setRange(scan.getStartOffset(), scan.getEndOffset());
		init();
		assert (this.rootNode != null && this.node != null);
		
		process();
		
		this.rScan= null;
	}
	
	protected TreePartitionNodeScan getScan() {
		return this.scan;
	}
	
	protected void setRange(final int startOffset, final int endOffset) {
		final var input= new DocumentParserInput(getScan().getDocument());
		this.lexer.reset(input.init(startOffset, endOffset));
	}
	
	protected void init() {
		final TreePartitionNode beginNode= getScan().getBeginNode();
		if (beginNode.getType() instanceof RPkgDescrPartitionNodeType) {
			initNode(beginNode, (RPkgDescrPartitionNodeType)beginNode.getType());
		}
		else {
			this.node= beginNode;
			addNode(getDefaultRootType(), getScan().getStartOffset());
			this.rootNode= this.node;
		}
		
		this.rStartNode= null;
	}
	
	
	protected final void initNode(final TreePartitionNode node, final RPkgDescrPartitionNodeType type) {
		if (isNonNull(this.node)) {
			throw new IllegalStateException();
		}
		this.node= node;
		this.type= type;
		this.rootNode= nonNullAssert(findDcfRootNode(node));
	}
	
	protected final void addNode(final RPkgDescrPartitionNodeType type, final int offset) {
		this.node= this.scan.add(type, this.node, offset, 0);
		this.type= type;
	}
	
	protected final void addNode(final TreePartitionNodeType type, final RPkgDescrPartitionNodeType descrType,
			final int offset) {
		this.node= this.scan.add(type, this.node, offset, 0);
		this.type= descrType;
	}
	
	protected final TreePartitionNode getNode() {
		return this.node;
	}
	
	@SuppressWarnings("null")
	protected final void exitNode(final int offset, final int flags) {
		this.scan.expand(this.node, offset, flags, true);
		this.node= this.node.getParent();
		this.type= (RPkgDescrPartitionNodeType)this.node.getType();
	}
	
//	protected final void exitNode() {
//		this.node= this.node.getParent();
//		this.type= (RPkgDescrPartitionNodeType)this.node.getType();
//	}
	
	
	private void process() {
		int valueEndOffset= -1;
		while (true) {
			final byte type= this.lexer.next();
			switch (type) {
			case DcfLexer.BLANK_LINE:
			case DcfLexer.VALUE:
			case DcfLexer.VALUE_EMPTY_LINE:
				continue;
			case DcfLexer.LINEBREAK:
				valueEndOffset= this.lexer.getOffset();
				continue;
			case DcfLexer.EOF:
				valueEndOffset= this.lexer.getOffset();
				//$FALL-THROUGH$
			default:
				if (this.rStartNode != null) {
					scanRValue(valueEndOffset);
					exitNode(valueEndOffset, 0);
				}
				break;
			}
			switch (type) {
			case DcfLexer.EOF:
				handleEOF(this.type);
				return;
			case DcfLexer.FIELD_NAME:
				addNode(RPkgDescrPartitionNodeType.FIELD_NAME, this.lexer.getOffset());
				exitNode(this.lexer.getEndOffset(), 0);
				if (isRField(this.lexer.getText())) {
					assert (this.rStartNode == null);
					
					addNode(this.rScanner.getDefaultRootType(), RPkgDescrPartitionNodeType.FIELD_VALUE_R,
							this.rStartOffset= valueEndOffset= this.lexer.getEndOffset() );
					this.rStartNode= getNode();
				}
				continue;
			case DcfLexer.UNKNOWN:
				{	final int endIdx= isPotentialFieldName(this.lexer.getText());
					if (endIdx >= 0) {
						addNode(RPkgDescrPartitionNodeType.FIELD_NAME, this.lexer.getOffset());
						exitNode(this.lexer.getOffset() + endIdx, END_UNCLOSED);
					}
				}
				continue;
			default:
				continue;
			}
		}
	}
	
	protected void scanRValue(final int endOffset) {
		final TreePartitionNode node= nonNullAssert(this.rStartNode);
		this.rStartNode= null;
		this.rScan.init(this.rStartOffset, endOffset, node);
		this.rScanner.execute(this.rScan);
		this.rScan.exit();
	}
	
	protected void handleEOF(final RPkgDescrPartitionNodeType type) {
		this.scan.expand(this.node, this.scan.getEndOffset(), 0, true);
	}
	
	
	private int isPotentialFieldName(final @Nullable String s) {
		if (s == null) {
			return -1;
		}
		int idx= 0;
		ITER_CHAR: for (; idx < s.length(); idx++) {
			switch (s.charAt(idx)) {
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
			case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
			case '-', '_', '/', '.':
				continue ITER_CHAR;
			case ' ':
				break ITER_CHAR;
			default:
				return -1;
			}
		}
		return idx;
	}
	
	protected boolean isRField(final @Nullable String name) {
		if (name != null) {
			final var fieldDef= RPkgDescriptions.getFieldDefinition(name);
			return (fieldDef != null && fieldDef.getDataType() == RPkgDescrFieldDefinition.R_SYNTAX);
		}
		return false;
	}
	
}

class RChunkPartitionNodeScanner extends RPartitionNodeScanner {
	
	
	public RChunkPartitionNodeScanner() {
		super(false);
	}
	
	
}
