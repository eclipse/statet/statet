/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.build;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceUnitModelInfo;
import org.eclipse.statet.r.core.project.RIssues;


@NonNullByDefault
public class RPkgDescrSourceUnitModelContainer
		extends SourceUnitModelContainer<RSourceUnit, RPkgDescrSourceUnitModelInfo> {
	
	
	public static final IssueTypeSet ISSUE_TYPE_SET= new IssueTypeSet(RCore.BUNDLE_ID,
			RIssues.TASK_CATEGORY,
			ImCollections.newList(
					RIssues.RPKG_MODEL_PROBLEM_CATEGORY ));
	
	
	public RPkgDescrSourceUnitModelContainer(final RSourceUnit unit,
			final @Nullable SourceUnitIssueSupport issueSupport) {
		super(unit, issueSupport);
	}
	
	
	@Override
	public Class<?> getAdapterClass() {
		return RPkgDescrSourceUnitModelContainer.class;
	}
	
	@Override
	public boolean isContainerFor(final String modelTypeId) {
		return (modelTypeId == RModel.RPKG_DESCRIPTION_TYPE_ID);
	}
	
	@Override
	protected ModelManager getModelManager() {
		return RModel.getRModelManager();
	}
	
}
