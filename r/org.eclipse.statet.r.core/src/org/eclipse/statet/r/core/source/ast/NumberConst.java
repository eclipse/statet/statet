/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * 
 */
@NonNullByDefault
public final class NumberConst extends SingleValue {
	
	
	private final RTerminal type;
	
	private @Nullable String text;
	
	
	NumberConst(final RTerminal type) {
		this.type= type;
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.NUM_CONST;
	}
	
	@Override
	public final RTerminal getOperator(final int index) {
		return this.type;
	}
	
	@Override
	public final @Nullable String getText() {
		return this.text;
	}
	
	
	@Override
	public final @Nullable TextRegion getTextRegion() {
		return this;
	}
	
	@Override
	void setText(final @Nullable String text, final @Nullable TextRegion textRegion) {
		this.text= text;
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.NUM_CONST == element.getNodeType());
	}
	
}
