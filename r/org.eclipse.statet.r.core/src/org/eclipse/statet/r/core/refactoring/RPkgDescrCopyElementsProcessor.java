/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.refactoring.core.CommonCopyProcessor;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination;


@NonNullByDefault
public class RPkgDescrCopyElementsProcessor extends CommonCopyProcessor {
	
	
	public RPkgDescrCopyElementsProcessor(final ElementSet elementsToCopy,
			final RefactoringDestination destination, final RefactoringAdapter adapter) {
		super(elementsToCopy, destination, adapter);
	}
	
	
	@Override
	public String getIdentifier() {
		return RRefactoring.COPY_RPKG_DESCR_ELEMENTS_PROCESSOR_ID;
	}
	
	@Override
	protected String getRefactoringIdentifier() {
		return RRefactoring.COPY_RPKG_DESCR_ELEMENTS_REFACTORING_ID;
	}
	
}
