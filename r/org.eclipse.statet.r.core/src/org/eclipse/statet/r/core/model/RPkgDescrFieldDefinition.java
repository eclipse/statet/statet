/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RPkgDescrFieldDefinition {
	
	
	public static final int REQUIRED= 1 << 0;
	public static final int R_DIST_ONLY= 1 << 1;
	public static final int BUILD_GENERATED= 1 << 3;
	
	
	public static final int LOGICAL= 1;
	public static final int DATE= 2;
	public static final int TEXT= 3;
	public static final int URL= 4;
	public static final int URL_LIST= 5;
	public static final int PACKAGE_LIST= 6;
	public static final int PACKAGE_DEPEND_LIST= 7;
	public static final int R_SYNTAX= 10;
	
	
	private final String name;
	
	private final int flags;
	
	private final int dataType;
	
	
	public RPkgDescrFieldDefinition(final String name, final int flags,
			final int dataType) {
		this.name= name;
		this.flags= flags;
		this.dataType= dataType;
	}
	
	public RPkgDescrFieldDefinition(final String name) {
		this(name, 0, 0);
	}
	
	
	public String getName() {
		return this.name;
	}
	
	public int getFlags() {
		return this.flags;
	}
	
	public int getDataType() {
		return this.dataType;
	}
	
	public boolean isRequired() {
		return ((this.flags & REQUIRED) != 0);
	}
	
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
