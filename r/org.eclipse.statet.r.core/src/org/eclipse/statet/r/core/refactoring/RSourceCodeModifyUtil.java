/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEdit;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.ecommons.text.IndentUtil;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.Group;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RParser;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;
import org.eclipse.statet.r.core.source.util.RSourceIndenter;
import org.eclipse.statet.rj.util.RCodeBuilder;


@NonNullByDefault
public final class RSourceCodeModifyUtil extends RCodeBuilder {
	
	
	private final RRefactoringAdapter adapter;
	private final SourceUnit sourceUnit;
	
	private final RCoreAccess coreAccess;
	private final AbstractDocument sourceDocument;
	
	private final RCodeStyleSettings codeStyle;
	private final String lineSeparator;
	
	private @Nullable RHeuristicTokenScanner scanner;
	private @Nullable IndentUtil indentUtil;
	private @Nullable RSourceIndenter indenter;
	
	private final ArrayList<TextEdit> edits= new ArrayList<>();
	
	
	public RSourceCodeModifyUtil(final RRefactoringAdapter adapter,
			final SourceUnit sourceUnit, final SubMonitor m) {
		this.adapter= adapter;
		this.sourceUnit= sourceUnit;
		this.coreAccess= RCore.getContextAccess(sourceUnit);
		this.sourceDocument= sourceUnit.getDocument(m);
		this.codeStyle= this.coreAccess.getRCodeStyle();
		this.lineSeparator= this.sourceDocument.getDefaultLineDelimiter();
	}
	
	
	public RCoreAccess getCoreAccess() {
		return this.coreAccess;
	}
	
	public AbstractDocument getSourceDocument() {
		return this.sourceDocument;
	}
	
	public RCodeStyleSettings getCodeStyle() {
		return this.codeStyle;
	}
	
	public RHeuristicTokenScanner getScanner() {
		var scanner= this.scanner;
		if (scanner == null) {
			scanner= this.adapter.getScanner(this.sourceUnit);
			this.scanner= scanner;
		}
		scanner.configure(this.sourceDocument);
		return scanner;
	}
	
	protected IndentUtil getIndentUtil() {
		var indentUtil= this.indentUtil;
		if (indentUtil == null) {
			indentUtil= new IndentUtil(this.sourceDocument, this.codeStyle);
			this.indentUtil= indentUtil;
		}
		return indentUtil;
	}
	
	protected RSourceIndenter getSourceIndenter() {
		var indenter= this.indenter;
		if (indenter == null) {
			indenter= new RSourceIndenter(getScanner(), this.coreAccess);
			this.indenter= indenter;
		}
		return indenter;
	}
	
	
	protected String getSource(final int startOffset, final int endOffset)
			throws BadLocationException {
		return this.sourceDocument.get(startOffset, endOffset - startOffset);
	}
	
	protected String getSource(final TextRegion node)
			throws BadLocationException {
		return this.sourceDocument.get(node.getStartOffset(), node.getLength());
	}
	
	
	@Override
	public RSourceCodeModifyUtil clear() {
		this.edits.clear();
		super.clear();
		return this;
	}
	
	public void clearCodeBuffer() {
		this.sb.setLength(0);
	}
	
	
	public void appendTrim(int startOffset, int endOffset)
			throws BadLocationException {
		final var scanner= getScanner();
		startOffset= scanner.expandAnyMSpaceForward(startOffset, endOffset);
		endOffset= scanner.expandAnyMSpaceBackward(endOffset, startOffset);
		this.sb.append(getSource(startOffset, endOffset));
	}
	
	public void appendTrim(final TextRegion region)
			throws BadLocationException {
		appendTrim(region.getStartOffset(), region.getEndOffset());
	}
	
	public void appendUnwrapGroup(final RAstNode node)
			throws BadLocationException {
		if (node.getNodeType() == NodeType.GROUP) {
			final var group= (Group)node;
			final int closeOffset= group.getGroupCloseOffset();
			appendTrim(group.getStartOffset() + 1, (closeOffset != NA_OFFSET) ? closeOffset : group.getEndOffset());
		}
		else {
			appendTrim(node);
		}
	}
	
	public void appendLineSeparator() {
		this.sb.append(this.lineSeparator);
	}
	
	
	public void appendGroup(final String code) {
		this.sb.append('(');
		append(code);
		this.sb.append(')');
	}
	
	public void appendGroup(final RAstNode node) throws BadLocationException {
		this.sb.append('(');
		appendTrim(node);
		this.sb.append(')');
	}
	
	public void append(final String code, final boolean wrapInGroup) throws BadLocationException {
		if (wrapInGroup) {
			appendGroup(code);
		}
		else {
			append(code);
		}
	}
	
	public void append(final RAstNode node, final boolean wrapInGroup) throws BadLocationException {
		if (wrapInGroup) {
			appendGroup(node);
		}
		else {
			appendTrim(node);
		}
	}
	
	public void appendFDefBodyBlock(final int startOffset, final int endOffset)
			throws BadLocationException {
		if (this.codeStyle.getNewlineFDefBodyBlockBefore()) {
			this.sb.append(this.lineSeparator);
		}
		else {
			this.sb.append(' ');
		}
		this.sb.append('{');
		this.sb.append(this.lineSeparator);
		appendTrim(startOffset, endOffset);
		this.sb.append(this.lineSeparator);
		this.sb.append('}');
	}
	
	public void appendFlowControlBody(final RAstNode node, final boolean wrapInBlock)
			throws BadLocationException {
		if (wrapInBlock) {
			if (this.codeStyle.getNewlineFlowControlBodyBlockBefore()) {
				this.sb.append(this.lineSeparator);
			}
			else {
				this.sb.append(' ');
			}
			this.sb.append('{');
			this.sb.append(this.lineSeparator);
			appendUnwrapGroup(node);
			this.sb.append(this.lineSeparator);
			this.sb.append('}');
		}
		else {
			this.sb.append(' ');
			appendTrim(node);
		}
	}
	
	public void appendOperator(final RTerminal op) {
		switch (op) {
		case ARROW_LEFT_S:
			if (this.codeStyle.getWhitespaceAssignBefore()) {
				this.sb.append(' ');
			}
			this.sb.append(RTerminal.S_ARROW_LEFT);
			if (this.codeStyle.getWhitespaceAssignAfter()) {
				this.sb.append(' ');
			}
			return;
		case ARROW_LEFT_D:
			if (this.codeStyle.getWhitespaceAssignBefore()) {
				this.sb.append(' ');
			}
			this.sb.append(RTerminal.S_ARROW_LEFT_D);
			if (this.codeStyle.getWhitespaceAssignAfter()) {
				this.sb.append(' ');
			}
			return;
		case ARROW_RIGHT_S:
			if (this.codeStyle.getWhitespaceAssignAfter()) {
				this.sb.append(' ');
			}
			this.sb.append(RTerminal.S_ARROW_RIGHT);
			if (this.codeStyle.getWhitespaceAssignBefore()) {
				this.sb.append(' ');
			}
			return;
		case ARROW_RIGHT_D:
			if (this.codeStyle.getWhitespaceAssignAfter()) {
				this.sb.append(' ');
			}
			this.sb.append(RTerminal.S_ARROW_RIGHT_D);
			if (this.codeStyle.getWhitespaceAssignBefore()) {
				this.sb.append(' ');
			}
			return;
		case PIPE_RIGHT:
			if (this.codeStyle.getWhitespacePipeBefore()) {
				this.sb.append(' ');
			}
			this.sb.append(RTerminal.S_PIPE_RIGHT);
			if (this.codeStyle.getWhitespacePipeAfter()) {
				this.sb.append(' ');
			}
			return;
		default:
			appendOperator(op.text);
			return;
		}
	}
	
	public void appendArgAssign() {
		append(this.codeStyle.getArgAssignString());
	}
	
	public void appendOperator(final String s) {
		if (this.codeStyle.getWhitespaceOtherOpBefore()) {
			this.sb.append(' ');
		}
		this.sb.append(s);
		if (this.codeStyle.getWhitespaceOtherOpAfter()) {
			this.sb.append(' ');
		}
	}
	
	
	/**
	 * Prepare the insertion of a command (text) before another command (offset)
	 * 
	 * The method prepares the insertion by modifying the text and returning the offset
	 * where to insert the modified text
	 * 
	 * @param offset the offset where to insert the command
	 * @return the offset to insert the modified text
	 * @throws BadLocationException
	 * @throws CoreException
	 */
	public int prepareInsertBefore(final int offset)
			throws BadLocationException, CoreException {
		final var indentUtil= getIndentUtil();
		final int line= this.sourceDocument.getLineOfOffset(offset);
		final int[] lineIndent= indentUtil.getLineIndent(line, false);
		if (lineIndent[IndentUtil.OFFSET_IDX] == offset) { // first char/command in line
			this.sb.insert(0, indentUtil.createIndentString(lineIndent[IndentUtil.COLUMN_IDX]));
			this.sb.append(this.lineSeparator);
			return this.sourceDocument.getLineOffset(line);
		}
		else {
			this.sb.append("; "); //$NON-NLS-1$
			return offset;
		}
	}
	
	public void correctIndent(final int changeOffset, final int baseOffset)
			throws BadLocationException, CoreException {
		final var indentUtil= getIndentUtil();
		final int column= indentUtil.getColumn(baseOffset);
		final String initialIndent= indentUtil.createIndentString(column);
		final var prefix= new StringBuilder();
		prefix.append(initialIndent).append("1").append(this.lineSeparator); //$NON-NLS-1$
		int changeLine0= 1;
		if (baseOffset < changeOffset) {
			prefix.append(initialIndent);
			prefix.append(this.sourceDocument.get(baseOffset, changeOffset - baseOffset));
			changeLine0+= this.sourceDocument.getNumberOfLines(baseOffset, changeOffset - baseOffset);
		}
		this.sb.insert(0, prefix);
		final String text= this.sb.toString();
		
		final Document tempDoc= new Document(text);
		final TextParserInput parseInput= new StringParserInput(text);
		final RParser rParser= new RParser(AstInfo.LEVEL_MINIMAL);
		final TextEdit edits= getSourceIndenter().getIndentEdits(tempDoc, rParser.parseSourceUnit(parseInput.init()),
				0, changeLine0, tempDoc.getNumberOfLines() - 1 );
		edits.apply(tempDoc, 0);
		this.sb.replace(0, this.sb.length(),
				tempDoc.get(prefix.length(), tempDoc.getLength() - prefix.length()) );
	}
	
	public void correctIndent(final int changeOffset)
			throws BadLocationException, CoreException {
		correctIndent(changeOffset, changeOffset);
	}
	
	
	public InsertEdit createInsertEdit(final int offset) {
		return new InsertEdit(offset, this.sb.toString());
	}
	
	public ReplaceEdit createReplaceEdit(final int startOffset, final int endOffset) {
		return new ReplaceEdit(startOffset, endOffset - startOffset, this.sb.toString());
	}
	
	public ReplaceEdit createReplaceEdit(final TextRegion region) {
		return new ReplaceEdit(region.getStartOffset(), region.getLength(), this.sb.toString());
	}
	
	public DeleteEdit createDeleteEdit(final int startOffset, final int endOffset) {
		return new DeleteEdit(startOffset, endOffset - startOffset);
	}
	
	public DeleteEdit createDeleteEdit(final TextRegion region) {
		return new DeleteEdit(region.getStartOffset(), region.getLength());
	}
	
	
	public void addInsertEdit(final int offset) {
		this.edits.add(createInsertEdit(offset));
	}
	
	public void addReplaceEdit(final int startOffset, final int endOffset) {
		this.edits.add(createReplaceEdit(startOffset, endOffset));
	}
	
	public void addReplaceEdit(final TextRegion region) {
		this.edits.add(createReplaceEdit(region));
	}
	
	public void addDeleteEdit(final int startOffset, final int endOffset) {
		this.edits.add(createDeleteEdit(startOffset, endOffset));
	}
	
	public void addDeleteEdit(final TextRegion region) {
		this.edits.add(createDeleteEdit(region));
	}
	
	
	public void addUnwrapGroupEdits(final Group group)
			throws BadLocationException {
		final var scanner= getScanner();
		addDeleteEdit(group.getStartOffset(), scanner.expandAnySSpaceForward(group.getStartOffset() + 1, group.getEndOffset()));
		final int closeOffset= group.getGroupCloseOffset();
		if (closeOffset != NA_OFFSET) {
			this.edits.add(new DeleteEdit(closeOffset, 1));
		}
	}
	
	public void addWrapInGroupEdits(final RAstNode node)
			throws BadLocationException {
		this.edits.add(new InsertEdit(node.getStartOffset(), RTerminal.S_GROUP_OPEN));
		this.edits.add(new InsertEdit(node.getEndOffset(), RTerminal.S_GROUP_CLOSE));
	}
	
	public void addInverseConditionEdits(final RAstNode node)
			throws BadLocationException {
		switch (node.getNodeType()) {
		case NOT: {
				this.edits.add(new DeleteEdit(node.getStartOffset(), 1));
				final RAstNode inner= node.getChild(0);
				if (inner.getNodeType() == NodeType.GROUP) {
					addUnwrapGroupEdits((Group)inner);
				}
				return;
			}
		case RELATIONAL: {
				super.clear();
				final RTerminal newOperator= switch (node.getOperator(0)) {
						case REL_NE -> RTerminal.REL_EQ;
						case REL_EQ -> RTerminal.REL_NE;
						case REL_LT -> RTerminal.REL_GE;
						case REL_LE -> RTerminal.REL_GT;
						case REL_GT -> RTerminal.REL_LE;
						case REL_GE -> RTerminal.REL_LT;
						default -> throw new RuntimeException();
						};
				final int startOffset= node.getChild(0).getEndOffset();
				final int endOffset= node.getChild(1).getStartOffset();
				if (this.codeStyle.getWhitespaceOtherOpBefore()) {
					this.sb.append(' ');
				}
				this.sb.append(newOperator.text);
				if (this.codeStyle.getWhitespaceOtherOpAfter()) {
					this.sb.append(' ');
				}
				addReplaceEdit(startOffset, endOffset);
				return;
			}
		default:
			break;
		}
		{	// default
			this.edits.add(new InsertEdit(node.getStartOffset(), RTerminal.S_NOT));
			if (node.getNodeType().opPrec > NodeType.NOT.opPrec) {
				addWrapInGroupEdits(node);
			}
			return;
		}
	}
	
	public List<TextEdit> getEdits() {
		return this.edits;
	}
	
	
}
