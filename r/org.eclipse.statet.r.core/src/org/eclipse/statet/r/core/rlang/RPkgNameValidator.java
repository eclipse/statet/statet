/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rlang;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.core.Messages;


@NonNullByDefault
public class RPkgNameValidator implements IValidator<String> {
	
	
	private static final String TRANSLATION_PREFIX= "Translation-"; //$NON-NLS-1$
	private static final int TRANSLATION_PREFIX_LENGTH= 12;
	
	
	private boolean required;
	
	
	public RPkgNameValidator() {
		this.required= true;
	}
	
	
	public void setRequired(final boolean isRequired) {
		this.required= isRequired;
	}
	
	
	@Override
	public IStatus validate(final String name) {
		if (name == null || name.isEmpty()) {
			if (this.required) {
				return ValidationStatus.error(Messages.RPkgName_Validation_error_Empty_message);
			}
			else {
				return ValidationStatus.ok();
			}
		}
		{	final char c= name.charAt(0);
			if (!Character.isLetter(c)) {
				return ValidationStatus.error(
						NLS.bind(Messages.RPkgName_Validation_error_InvalidFirstChar_message, c) );
			}
			if (c > 127) {
				return ValidationStatus.error(
						NLS.bind(Messages.RPkgName_Validation_error_InvalidNoAscii_message, c) );
			}
		}
		final boolean translation= name.startsWith(TRANSLATION_PREFIX);
		for (int i= translation ? TRANSLATION_PREFIX_LENGTH : 1; i < name.length(); i++) {
			final char c= name.charAt(i);
			if (!(Character.isLetterOrDigit(c) || c == '.')) {
				return ValidationStatus.error(
						NLS.bind(Messages.RPkgName_Validation_error_InvalidChar_message, c) );
			}
			if (c > 127) {
				return ValidationStatus.error(
						NLS.bind(Messages.RPkgName_Validation_error_InvalidNoAscii_message, c) );
			}
		}
		{	final char c= name.charAt(name.length() - 1);
			if (c == '.') {
				return ValidationStatus.error(
						NLS.bind(Messages.RPkgName_Validation_error_InvalidDotAtEnd_message, c) );
			}
		}
		if (translation) {
			if (name.length() == TRANSLATION_PREFIX_LENGTH) {
				return ValidationStatus.error(Messages.RPkgName_Validation_error_IncompleteTranslation_message);
			}
		}
		else {
			if (name.length() == 1) {
				return ValidationStatus.error(Messages.RPkgName_Validation_error_InvalidSingleChar_message);
			}
		}
		return ValidationStatus.ok();
	}
	
}
