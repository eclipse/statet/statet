/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.util.PreferenceAccessWrapper;

import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public class RCoreAccessWrapper extends PreferenceAccessWrapper
		implements RCoreAccess {
	
	
	private RCoreAccess parent;
	
	
	public RCoreAccessWrapper(final RCoreAccess rCoreAccess) {
		nonNullAssert(rCoreAccess);
		
		updateParent(null, rCoreAccess);
	}
	
	
	public synchronized RCoreAccess getParent() {
		return this.parent;
	}
	
	public synchronized boolean setParent(final RCoreAccess rCoreAccess) {
		nonNullAssert(rCoreAccess);
		
		if (rCoreAccess != this.parent) {
			updateParent(this.parent, rCoreAccess);
			return true;
		}
		return false;
	}
	
	protected void updateParent(final @Nullable RCoreAccess oldParent, final RCoreAccess newParent) {
		this.parent= newParent;
		
		super.setPreferenceContexts(newParent.getPrefs().getPreferenceContexts());
	}
	
	@Override
	public void setPreferenceContexts(final ImList<IScopeContext> contexts) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public REnv getREnv() {
		return this.parent.getREnv();
	}
	
	@Override
	public RSourceConfig getRSourceConfig() {
		return this.parent.getRSourceConfig();
	}
	
	@Override
	public RCodeStyleSettings getRCodeStyle() {
		return this.parent.getRCodeStyle();
	}
	
}
