/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.BUILD_GENERATED;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.DATE;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.LOGICAL;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.PACKAGE_DEPEND_LIST;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.PACKAGE_LIST;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.REQUIRED;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.R_DIST_ONLY;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.R_SYNTAX;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.TEXT;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.URL;
import static org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition.URL_LIST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RPkgDescriptions {
	
	
	public static final RPkgDescrFieldDefinition Encoding_FIELD= new RPkgDescrFieldDefinition("Encoding"); //$NON-NLS-1$
	
	public static final RPkgDescrFieldDefinition Type_FIELD= new RPkgDescrFieldDefinition("Type"); //$NON-NLS-1$
	
	public static final RPkgDescrFieldDefinition Package_FIELD= new RPkgDescrFieldDefinition("Package", //$NON-NLS-1$
			REQUIRED, 0 );
	
	public static final RPkgDescrFieldDefinition Version_FIELD= new RPkgDescrFieldDefinition("Version", //$NON-NLS-1$
			REQUIRED, 0 );
	public static final RPkgDescrFieldDefinition Date_FIELD= new RPkgDescrFieldDefinition("Date", //$NON-NLS-1$
			0, DATE );
	
	
	public static final RPkgDescrFieldDefinition License_FIELD= new RPkgDescrFieldDefinition("License", //$NON-NLS-1$
			REQUIRED, 0 );
	
	public static final RPkgDescrFieldDefinition Title_FIELD= new RPkgDescrFieldDefinition("Title", //$NON-NLS-1$
			REQUIRED, TEXT );
	
	public static final RPkgDescrFieldDefinition Description_FIELD= new RPkgDescrFieldDefinition("Description", //$NON-NLS-1$
			REQUIRED, TEXT );
	
	public static final RPkgDescrFieldDefinition Author_FIELD= new RPkgDescrFieldDefinition("Author", //$NON-NLS-1$
			REQUIRED, 0 );
	public static final RPkgDescrFieldDefinition Maintainer_FIELD= new RPkgDescrFieldDefinition("Maintainer", //$NON-NLS-1$
			REQUIRED, 0 );
	public static final RPkgDescrFieldDefinition Authors_R_FIELD= new RPkgDescrFieldDefinition("Authors@R", //$NON-NLS-1$
			0, R_SYNTAX );
	
	public static final RPkgDescrFieldDefinition Copyright_FIELD= new RPkgDescrFieldDefinition("Copyright"); //$NON-NLS-1$
	
	public static final RPkgDescrFieldDefinition OS_type_FIELD= new RPkgDescrFieldDefinition("OS_type"); //$NON-NLS-1$
	public static final RPkgDescrFieldDefinition SystemRequirements_FIELD= new RPkgDescrFieldDefinition("SystemRequirements"); //$NON-NLS-1$
	public static final RPkgDescrFieldDefinition Depends_FIELD= new RPkgDescrFieldDefinition("Depends",
			0, PACKAGE_DEPEND_LIST );
	public static final RPkgDescrFieldDefinition Imports_FIELD= new RPkgDescrFieldDefinition("Imports", //$NON-NLS-1$
			0, PACKAGE_DEPEND_LIST );
	public static final RPkgDescrFieldDefinition Suggests_FIELD= new RPkgDescrFieldDefinition("Suggests", //$NON-NLS-1$
			0, PACKAGE_DEPEND_LIST );
	public static final RPkgDescrFieldDefinition Enhances_FIELD= new RPkgDescrFieldDefinition("Enhances", //$NON-NLS-1$
			0, PACKAGE_DEPEND_LIST );
	public static final RPkgDescrFieldDefinition LinkingTo_FIELD= new RPkgDescrFieldDefinition("LinkingTo"); //$NON-NLS-1$
	
	public static final RPkgDescrFieldDefinition Additional_repositories_FIELD= new RPkgDescrFieldDefinition("Additional_repositories"); //$NON-NLS-1$
	
	public static final RPkgDescrFieldDefinition URL_FIELD= new RPkgDescrFieldDefinition("URL", //$NON-NLS-1$
			0, URL_LIST );
	
	public static final RPkgDescrFieldDefinition BugReports_FIELD= new RPkgDescrFieldDefinition("BugReports", //$NON-NLS-1$
			0, URL );
	public static final RPkgDescrFieldDefinition Contact_FIELD= new RPkgDescrFieldDefinition("Contact"); //$NON-NLS-1$
	
	public static final RPkgDescrFieldDefinition Priority_FIELD= new RPkgDescrFieldDefinition("Priority", //$NON-NLS-1$
			R_DIST_ONLY, 0 );
	
	public static final RPkgDescrFieldDefinition Collate_FIELD= new RPkgDescrFieldDefinition("Collate"); //$NON-NLS-1$
	public static final RPkgDescrFieldDefinition Collate_unix_FIELD= new RPkgDescrFieldDefinition("Collate.unix"); //$NON-NLS-1$
	public static final RPkgDescrFieldDefinition Collate_windows_FIELD= new RPkgDescrFieldDefinition("Collate.windows"); //$NON-NLS-1$
	
	public static final RPkgDescrFieldDefinition KeepSource_FIELD= new RPkgDescrFieldDefinition("KeepSource", //$NON-NLS-1$
			0, LOGICAL );
	public static final RPkgDescrFieldDefinition ByteCompile_FIELD= new RPkgDescrFieldDefinition("ByteCompile", //$NON-NLS-1$
			0, LOGICAL );
	public static final RPkgDescrFieldDefinition UseLTO_FIELD= new RPkgDescrFieldDefinition("UseLTO", //$NON-NLS-1$
			0, LOGICAL );
	public static final RPkgDescrFieldDefinition StagedInstall_FIELD= new RPkgDescrFieldDefinition("StagedInstall", //$NON-NLS-1$
			0, LOGICAL );
	public static final RPkgDescrFieldDefinition NeedsCompilation_FIELD= new RPkgDescrFieldDefinition("NeedsCompilation", //$NON-NLS-1$
			0, 0 );
	public static final RPkgDescrFieldDefinition Biarch_FIELD= new RPkgDescrFieldDefinition("Biarch", //$NON-NLS-1$
			0, LOGICAL );
	public static final RPkgDescrFieldDefinition BuildVignettes_FIELD= new RPkgDescrFieldDefinition("BuildVignettes", //$NON-NLS-1$
			0, LOGICAL );
	public static final RPkgDescrFieldDefinition VignetteBuilder_FIELD= new RPkgDescrFieldDefinition("VignetteBuilder", //$NON-NLS-1$
			0, PACKAGE_LIST );
	
	public static final RPkgDescrFieldDefinition Classification_ACM_2012_FIELD= new RPkgDescrFieldDefinition("Classification/ACM-2012"); //$NON-NLS-1$
	public static final RPkgDescrFieldDefinition Classification_ACM_FIELD= new RPkgDescrFieldDefinition("Classification/ACM"); //$NON-NLS-1$
	public static final RPkgDescrFieldDefinition Classification_JEL_FIELD= new RPkgDescrFieldDefinition("Classification/JEL"); //$NON-NLS-1$
	public static final RPkgDescrFieldDefinition Classification_MSC_2010_FIELD= new RPkgDescrFieldDefinition("Classification/MSC-2010"); //$NON-NLS-1$
	public static final RPkgDescrFieldDefinition Classification_MSC_FIELD= new RPkgDescrFieldDefinition("Classification/MSC"); //$NON-NLS-1$
	
	public static final RPkgDescrFieldDefinition Built_FIELD= new RPkgDescrFieldDefinition("Built", //$NON-NLS-1$
			BUILD_GENERATED, 0 );
	public static final RPkgDescrFieldDefinition Packaged_FIELD= new RPkgDescrFieldDefinition("Packaged", //$NON-NLS-1$
			BUILD_GENERATED, 0 );
	
	
	private static final ImList<RPkgDescrFieldDefinition> ALL_LIST= ImCollections.newList(
			Encoding_FIELD,
			Type_FIELD,
			Package_FIELD,
			Version_FIELD, Date_FIELD,
			License_FIELD,
			Title_FIELD,
			Description_FIELD,
			Author_FIELD, Maintainer_FIELD, Authors_R_FIELD,
			Copyright_FIELD,
			OS_type_FIELD, SystemRequirements_FIELD, Depends_FIELD, Imports_FIELD, Suggests_FIELD, Enhances_FIELD, LinkingTo_FIELD,
			Additional_repositories_FIELD,
			URL_FIELD,
			BugReports_FIELD, Contact_FIELD,
			Priority_FIELD,
			Collate_FIELD, Collate_unix_FIELD, Collate_windows_FIELD,
			KeepSource_FIELD, ByteCompile_FIELD, UseLTO_FIELD, StagedInstall_FIELD, NeedsCompilation_FIELD, Biarch_FIELD, BuildVignettes_FIELD, VignetteBuilder_FIELD,
			Classification_ACM_2012_FIELD, Classification_ACM_FIELD, Classification_JEL_FIELD, Classification_MSC_2010_FIELD, Classification_MSC_FIELD,
			Built_FIELD, Packaged_FIELD );
	private static final Map<String, RPkgDescrFieldDefinition> ALL_MAP;
	private static final ImList<RPkgDescrFieldDefinition> REQUIRED_LIST;
	static {
		final var map= new HashMap<String, RPkgDescrFieldDefinition>(ALL_LIST.size());
		final var required= new ArrayList<RPkgDescrFieldDefinition>();
		for (final var def : ALL_LIST) {
			map.put(def.getName(), def);
			if (def.isRequired()) {
				required.add(def);
			}
		}
		ALL_MAP= map;
		REQUIRED_LIST= ImCollections.toList(required);
	}
	
	
	public static final ImList<RPkgDescrFieldDefinition> getAllFieldDefinitions() {
		return ALL_LIST;
	}
	
	public static final ImList<RPkgDescrFieldDefinition> getRequiredFieldDefinitions() {
		return REQUIRED_LIST;
	}
	
	public static final @Nullable RPkgDescrFieldDefinition getFieldDefinition(final String name) {
		return ALL_MAP.get(name);
	}
	
	
	public static final String PACKAGE_PKG_TYPE= "Package"; //$NON-NLS-1$
	public static final String TRANSLATION_PKG_TYPE= "Translation"; //$NON-NLS-1$
	public static final String FRONTEND_PKG_TYPE= "Frontend"; //$NON-NLS-1$
	public static final ImSet<String> PKG_TYPES= ImCollections.newSet(
			PACKAGE_PKG_TYPE, TRANSLATION_PKG_TYPE, FRONTEND_PKG_TYPE );
	
	public static final ImSet<String> LOGICAL_VALUES= ImCollections.newSet(
			"yes", "no", //$NON-NLS-1$ //$NON-NLS-2$
			"true", "false" ); //$NON-NLS-1$ //$NON-NLS-2$
	
	public static final byte parseLogical(final String text) {
		switch (text.length()) {
		case 2:
			switch (text.charAt(0)) {
			case 'N':
			case 'n':
				switch (text.charAt(1)) {
				case 'O':
				case 'o':
					return 0;
				}
			}
			return -1;
		case 3:
			switch (text.charAt(0)) {
			case 'Y':
			case 'y':
				switch (text.charAt(1)) {
				case 'E':
				case 'e':
					switch (text.charAt(2)) {
					case 'S':
					case 's':
						return 1;
					}
				}
			}
			return -1;
		case 4:
			switch (text.charAt(0)) {
			case 'T':
			case 't':
				switch (text.charAt(1)) {
				case 'R':
				case 'r':
					switch (text.charAt(2)) {
					case 'U':
					case 'u':
						switch (text.charAt(3)) {
						case 'E':
						case 'e':
							return 1;
						}
					}
				}
			}
			return -1;
		case 5:
			switch (text.charAt(0)) {
			case 'F':
			case 'f':
				switch (text.charAt(1)) {
				case 'A':
				case 'a':
					switch (text.charAt(2)) {
					case 'L':
					case 'l':
						switch (text.charAt(3)) {
						case 'S':
						case 's':
							return 0;
						}
					}
				}
			}
			return -1;
		default:
			return -1;
		}
	}
	
	public static record LicenseSpec(
			String abbr,
			String name) {
		
		@Override
		public String toString() {
			return this.abbr;
		}
		
	}
	
	public static final ImSet<LicenseSpec> STANDARD_LICENSE_SPECS= ImCollections.newSet(
			new LicenseSpec("GPL-2",        "GNU General Public License v2.0"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("GPL-3",        "GNU General Public License v3.0"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("LGPL-2",       "GNU Lesser General Public License v2.0"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("LGPL-2.1",     "GNU Lesser General Public License v2.1"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("LGPL-3",       "GNU Lesser General Public License v3.0"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("AGPL-3",       "GNU Affero General Public License v3.0"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("Artistic-2.0", "Artistic License 2.0"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("BSD_2_clause", "BSD 2-Clause License"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("BSD_3_clause", "BSD 3-Clause License"), //$NON-NLS-1$ //$NON-NLS-2$
			new LicenseSpec("MIT",          "MIT License") ); //$NON-NLS-1$ //$NON-NLS-2$
	
	
	private RPkgDescriptions() {
	}
	
}
