/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.IntFunction;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.IRegion;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.IntArrayList;
import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.Asts;
import org.eclipse.statet.r.core.model.RCoreFunctions;
import org.eclipse.statet.r.core.model.RFunctionSpec;
import org.eclipse.statet.r.core.model.RFunctionSpec.Parameter;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * 
 */
@NonNullByDefault
public class RAsts extends Asts {
	
	
	private static class LowestFDefAssignmentSearchVisitor extends GenericVisitor implements AstVisitor {
		
		private final int startOffset;
		private final int stopOffset;
		private boolean inAssignment;
		private @Nullable RAstNode assignment;
		
		
		public LowestFDefAssignmentSearchVisitor(final int offset) {
			this.startOffset= offset;
			this.stopOffset= offset;
		}
		
		
		@Override
		public void visit(final AstNode node) throws InvocationTargetException {
			if (node instanceof RAstNode) {
				((RAstNode) node).acceptInR(this);
				return;
			}
			if (node.getEndOffset() >= this.startOffset && this.stopOffset >= node.getStartOffset()) {
				node.acceptInChildren(this);
				return;
			}
		}
		
		@Override
		public void visitNode(final RAstNode node) throws InvocationTargetException {
			if (node.getEndOffset() >= this.startOffset && this.stopOffset >= node.getStartOffset()) {
				node.acceptInRChildren(this);
				return;
			}
		}
		
		@Override
		public void visit(final Assignment node) throws InvocationTargetException {
			if (this.inAssignment) {
				node.getSourceChild().acceptInR(this);
				return;
			}
			if (node.getEndOffset() >= this.startOffset && this.stopOffset >= node.getStartOffset()) {
				this.inAssignment= true;
				node.getSourceChild().acceptInR(this);
				this.inAssignment= false;
				return;
			}
		}
		
		@Override
		public void visit(final FDef node) throws InvocationTargetException {
			if (this.inAssignment
					|| (node.getEndOffset() >= this.startOffset && this.stopOffset >= node.getStartOffset()) ) {
				RAstNode take= node;
				RAstNode candidate= node.getRParent();
				// TODO: use analyzed ElementAccess if possible
				AssignExpr assign= null;
				while (candidate != null && (assign= checkAssign(candidate)) != null
						&& assign.valueNode == take) {
					take= assign.assignNode;
					candidate= take.getRParent();
				}
				this.assignment= take;
				throw new OperationCanceledException();
			}
		}
		
	}
	
	
	public static @Nullable RAstNode findLowestFDefAssignment(final AstNode root, final int offset) {
		final LowestFDefAssignmentSearchVisitor visitor= new LowestFDefAssignmentSearchVisitor(offset);
		try {
			root.accept(visitor);
		}
		catch (final OperationCanceledException | InvocationTargetException e) {
		}
		return visitor.assignment;
	}
	
	
	private static class DeepestCommandsSearchVisitor extends GenericVisitor implements AstVisitor {
		
		private final int startOffset;
		private final int stopOffset;
		private @Nullable RAstNode container;
		private final List<RAstNode> commands= new ArrayList<>();
		
		
		public DeepestCommandsSearchVisitor(final int startOffset, final int stopOffset) {
			this.startOffset= startOffset;
			this.stopOffset= stopOffset;
		}
		
		
		@Override
		public void visit(final AstNode node) throws InvocationTargetException {
			if (node instanceof RAstNode) {
				((RAstNode) node).acceptInR(this);
				return;
			}
			if (node.getEndOffset() >= this.startOffset && this.stopOffset >= node.getStartOffset()) {
				node.acceptInChildren(this);
				return;
			}
		}
		
		@Override
		public void visitNode(final RAstNode node) throws InvocationTargetException {
			if (node.getEndOffset() >= this.startOffset && ((this.startOffset == this.stopOffset) ?
					this.stopOffset >= node.getStartOffset() : this.stopOffset > node.getStartOffset()) ) {
				if (this.container != null && this.container == node.rParent) {
					this.commands.add(node);
				}
			}
			
			if (node.getStartOffset() <= this.startOffset && this.stopOffset <= node.getEndOffset()) {
				node.acceptInRChildren(this);
				return;
			}
		}
		
		@Override
		public void visit(final Block node) throws InvocationTargetException {
			if (node.getStartOffset() <= this.startOffset && this.stopOffset <= node.getEndOffset()) {
				this.commands.clear();
				if (node.getStartOffset() == this.startOffset && this.stopOffset == node.getEndOffset()) {
					this.commands.add(node);
					this.container= null;
					return;
				}
				this.container= node;
				
				node.acceptInRChildren(this);
				
				if (this.commands.isEmpty() && node.getEndOffset() > this.startOffset) {
					this.commands.add(node);
				}
				return;
			}
		}
		
		@Override
		public void visit(final SourceComponent node) throws InvocationTargetException {
			if (node.getEndOffset() >= this.startOffset && this.stopOffset >= node.getStartOffset()) {
				this.commands.clear();
				this.container= node;
				
				node.acceptInRChildren(this);
				return;
			}
		}
		
	}
	
	private static class NextCommandsSearchVisitor extends GenericVisitor implements AstVisitor {
		
		private final int offset;
		private @Nullable RAstNode container;
		private @Nullable RAstNode next;
		
		
		public NextCommandsSearchVisitor(final int offset) {
			this.offset= offset;
		}
		
		
		@Override
		public void visit(final AstNode node) throws InvocationTargetException {
			if (node instanceof RAstNode) {
				((RAstNode) node).acceptInR(this);
				return;
			}
			if (node.getEndOffset() >= this.offset && this.offset >= node.getStartOffset()) {
				node.acceptInChildren(this);
				return;
			}
		}
		
		@Override
		public void visitNode(final RAstNode node) throws InvocationTargetException {
			if (this.next == null) {
				if (node.getStartOffset() >= this.offset) {
					if (this.container != null && this.container == node.rParent) {
						this.next= node;
						return;
					}
					else {
						node.acceptInRChildren(this);
						return;
					}
				}
			}
		}
		
		@Override
		public void visit(final Block node) throws InvocationTargetException {
			if (this.next == null) {
				if (node.getStartOffset() >= this.offset) {
					if (this.container != null && this.container == node.rParent) {
						this.next= node;
						return;
					}
					else {
						node.acceptInRChildren(this);
						return;
					}
				}
				if (node.getEndOffset() >= this.offset) {
					this.container= node;
					node.acceptInRChildren(this);
					return;
				}
			}
		}
		
		@Override
		public void visit(final SourceComponent node) throws InvocationTargetException {
			if (this.next == null) {
				final AstNode parent= node.getParent();
				if (node.getEndOffset() >= this.offset &&
						// R script file or inside R chunk
						(parent == null || (parent.getStartOffset() <= this.offset && this.offset <= parent.getEndOffset())) ) {
					this.container= node;
					node.acceptInRChildren(this);
					return;
				}
			}
		}
		
	}
	
	public static ImList<RAstNode> findDeepestCommands(final AstNode root, final int startOffset, final int endOffset) {
		final DeepestCommandsSearchVisitor visitor= new DeepestCommandsSearchVisitor(startOffset, endOffset);
		try {
			root.accept(visitor);
		}
		catch (final InvocationTargetException e) {
		}
		return ImCollections.toList(visitor.commands);
	}
	
	public static @Nullable RAstNode findNextCommands(final AstNode root, final int offset) {
		final NextCommandsSearchVisitor visitor= new NextCommandsSearchVisitor(offset);
		try {
			root.accept(visitor);
		}
		catch (final InvocationTargetException e) {
		}
		return visitor.next;
	}
	
	public static @Nullable RAstNode findSurroundingCommand(@Nullable AstNode node) {
		while (node != null) {
			if (node instanceof RAstNode cand) {
				RAstNode parent;
				R_AST: while ((parent= cand.getRParent()) != null) {
					switch (parent.getNodeType()) {
					case SOURCELINES:
					case BLOCK:
						return cand;
					default:
						cand= parent;
						continue R_AST;
					}
				}
				node= cand.getParent();
			}
			else {
				node= node.getParent();
			}
		}
		return null;
	}
	
	public static @Nullable RAstNode findSurrounding(@Nullable AstNode node, final NodeType nodeType) {
		while (node != null) {
			if (node instanceof RAstNode cand) {
				R_AST: while (true) {
					if (cand.getNodeType() == nodeType) {
						return cand;
					}
					if (cand.getNodeType() == NodeType.SOURCELINES) {
						return null;
					}
					final RAstNode parent= cand.getRParent();
					if (parent != null) {
						cand= parent;
						continue R_AST;
					}
					else {
						node= cand.getParent();
						break R_AST;
					}
				}
			}
			else {
				node= node.getParent();
			}
		}
		return null;
	}
	
	
	public static class AssignExpr {
		
		public static final Object GLOBAL= new Object();
		public static final Object LOCAL= new Object();
		
		public final Object environment;
		public final RAstNode assignNode;
		public final RAstNode targetNode;
		public final RAstNode valueNode;
		
		public AssignExpr(final RAstNode assign, final Object env, final RAstNode target, final RAstNode source) {
			this.assignNode= assign;
			this.environment= env;
			this.targetNode= target;
			this.valueNode= source;
		}
		
	}
	
	public static @Nullable AssignExpr checkAssign(final RAstNode node) {
		switch (node.getNodeType()) {
		case A_LEFT:
		case A_RIGHT:
		case A_EQUALS:
			final Assignment assignNode= (Assignment) node;
			if (assignNode.isSearchOperator()) {
				return new AssignExpr(node, AssignExpr.GLOBAL, assignNode.getTargetChild(), assignNode.getSourceChild());
			}
			else {
				return new AssignExpr(node, AssignExpr.LOCAL, assignNode.getTargetChild(), assignNode.getSourceChild());
			}
		case F_CALL:
			final FCall callNode= (FCall)node;
			final RAstNode refChild= callNode.getRefChild();
			if (refChild.getNodeType() == NodeType.SYMBOL) {
				final String text= refChild.getText();
				if (text != null) {
					if (text.equals(RCoreFunctions.BASE_Assign_NAME)) {
						final FCallArgMatch args= matchArgs(callNode, RCoreFunctions.getDefinitions(null).BASE_Assign_fSpec);
						return new AssignExpr(node, AssignExpr.LOCAL, args.allocatedArgs[0], args.allocatedArgs[1]);
					}
				}
			}
			break;
		case F_CALL_ARGS:
			return checkAssign(node.getRParent());
		case F_CALL_ARG:
			return checkAssign(node.getRParent().getRParent());
		default:
			break;
		}
		return null;
	}
	
	
	public static final class FCallArgMatch {
		
		private final RFunctionSpec fSpec;
		
		private final FCall.Args fCallArgsNode;
		private final int fCallArgStartIdx;
		
		public final FCall. @Nullable Arg[] allocatedArgs;
		public final FCall.Arg[] ellipsisArgs;
		public final FCall.Arg[] otherArgs;
		
		private final int[] argsNode2paramIndex;
		
		
		private FCallArgMatch(final RFunctionSpec fSpec,
				final FCall.Args fCallArgsNode, final int fCallArgStartIdx,
				final FCall. @Nullable Arg[] allocatedArgs,
				final FCall.Arg[] ellipsisArgs, final FCall.Arg[] otherArgs,
				final int[] argsNode2paramIdx) {
			this.fSpec= fSpec;
			this.fCallArgsNode= fCallArgsNode;
			this.fCallArgStartIdx= fCallArgStartIdx;
			this.allocatedArgs= allocatedArgs;
			this.ellipsisArgs= ellipsisArgs;
			this.otherArgs= otherArgs;
			this.argsNode2paramIndex= argsNode2paramIdx;
		}
		
		
		public RFunctionSpec getFunctionSpec() {
			return this.fSpec;
		}
		
		
		public FCall.Args getFCallArgsNode() {
			return this.fCallArgsNode;
		}
		
		public FCall. @Nullable Arg getArgNode(final int paramIdx) {
			if (paramIdx >= 0) {
				return this.allocatedArgs[paramIdx];
			}
			else {
				return null;
			}
		}
		
		public FCall. @Nullable Arg getArgNode(final String name) {
			return getArgNode(this.fSpec.indexOfParam(name));
		}
		
		@SuppressWarnings("null")
		public @Nullable RAstNode getArgValueNode(final int paramIdx) {
			if (paramIdx >= 0 && this.allocatedArgs[paramIdx] != null) {
				return this.allocatedArgs[paramIdx].getValueChild();
			}
			else {
				return null;
			}
		}
		
		public @Nullable RAstNode getArgValueNode(final String name) {
			return getArgValueNode(this.fSpec.indexOfParam(name));
		}
		
		
		public RFunctionSpec. @Nullable Parameter getParamForFCall(int fCallArgIdx) {
			if (fCallArgIdx >= 0) {
				fCallArgIdx+= this.fCallArgStartIdx;
				if (this.fSpec.getParamCount() > this.fCallArgStartIdx) {
					if (fCallArgIdx < this.argsNode2paramIndex.length) {
						if (this.argsNode2paramIndex[fCallArgIdx] >= 0) {
							return this.fSpec.getParam(this.argsNode2paramIndex[fCallArgIdx]);
						}
					}
					else if (fCallArgIdx == this.fCallArgStartIdx
							&& this.argsNode2paramIndex.length == this.fCallArgStartIdx) {
						return this.fSpec.getParam(this.fCallArgStartIdx);
					}
				}
			}
			return null;
		}
		
		public RFunctionSpec. @Nullable Parameter getParamForInject(final int injectArgIdx) {
			if (injectArgIdx >= 0 && injectArgIdx < this.fCallArgStartIdx
					&& this.fSpec.getParamCount() > 0
					&& this.argsNode2paramIndex[injectArgIdx] >= 0) {
				return this.fSpec.getParam(this.argsNode2paramIndex[injectArgIdx]);
			}
			return null;
		}
		
		/** for tests */
		int[] argsNode2paramIdx() {
			return this.argsNode2paramIndex.clone();
		}
		
		
		@Override
		public String toString() {
			return Arrays.toString(this.argsNode2paramIndex);
		}
		
	}
	
	public static final FCall. @NonNull Arg[] NonNull_NO_ARGS= new FCall.Arg[0];
	public static final FCall. @Nullable Arg[] Nullable_NO_ARGS= new FCall.Arg[0];
	
	private static final int FAIL= -1;
	private static final int ELLIPSIS= -2;
	private static final int TEST_PARTIAL= -3;
	private static final int TEST_POSITION= -4;
	private static final int NA= -5;
		
	/**
	 * Reads the arguments of a function call for the given function definition
	 * 
	 * If follows mainly the R rules expect if R would fail.
	 * 
	 * 1) Exact matching on tags.
	 *    a) First supplied named argument matching exactly a formal name
	 *       is assigned to its formal arguments (position) in <code>argsNode</code>
	 *    b) Additional supplied arguments matching exactly the formal name
	 *       with an assignment of a) (error) are added to <code>otherArgs</code>
	 * 2) Partial matching on tags
	 *    a) If the name of the supplied argument matches partially more than one 
	 *       unmatched formal name (error), it is added to <code>otherArgs</code>
	 *    b) First supplied argument matching partially one unmatched formal name
	 *       is assigned to its formal arguments (position) in <code>argsNode</code>
	 *    c) Additional supplied arguments matching partially the unmatched formal name 
	 *       with an assignment of b) (error) are treated like arguments without any match
	 *       in 1) and 2) continue with 3).
	 * 3) Positional matching.
	 *    a) Any unnamed supplied argument is assigned to unmatched formal
	 *       arguments (empty positions) in <code>argsNode</code>, in order.
	 *       Until all If there is a ‘...’ argument or there is no more empty position.
	 *    b) If there is a ‘...’ argument, all remaining supplied arguments
	 *       are added to <code>ellipsisArgs</code>
	 * 4) Remaining supplied arguments (error) are assigned to <code>otherArgs</code>
	 * 
	 * (see paragraph 'Argument matching' in 'R Language Definition')
	 * 
	 * @param fCallArgs the arguments of the function call
	 * @param fSpec the arguments definition
	 * @param replValueArg the value node of the 'value' argument of a replacement function
	 * @param inject0Args the value nodes of injected arguments
	 *     replacing an present placeholder as argument value or
	 *     inserted at index 0 as argument values
	 * @return the match
	 */
	public static FCallArgMatch matchArgs(final FCall.Args fCallArgs,
			final RFunctionSpec fSpec,
			final @Nullable RAstNode replValueArg,
			final ImList<? extends @Nullable RAstNode> inject0Args) {
		if (replValueArg != null && !inject0Args.isEmpty()) {
			throw new IllegalArgumentException();
		}
		final int fCallArgStartIdx;
		final int fCallPlaceholderIdx;
		final IntFunction<FCall.Arg> getFCallArg;
		if (inject0Args.isEmpty()) {
			fCallArgStartIdx= 0;
			fCallPlaceholderIdx= -1;
			getFCallArg= new IntFunction<>() {
				@Override
				public FCall.Arg apply(final int nodeIdx) {
					return fCallArgs.getChild(nodeIdx);
				}
			};
		}
		else {
			fCallPlaceholderIdx= (inject0Args.size() == 1) ? RAsts.indexOfPlaceholder(fCallArgs) : -1;
			if (fCallPlaceholderIdx >= 0) {
				fCallArgStartIdx= 0;
				getFCallArg= new IntFunction<>() {
					@Override
					public FCall.Arg apply(final int nodeIdx) {
						return (nodeIdx == fCallPlaceholderIdx) ?
								createPlaceholderArg(fCallArgs.getChild(fCallPlaceholderIdx), inject0Args.get(0)) :
								fCallArgs.getChild(nodeIdx);
					}
				};
			}
			else {
				fCallArgStartIdx= inject0Args.size();
				getFCallArg= new IntFunction<>() {
					@Override
					public FCall.Arg apply(final int nodeIdx) {
						return (nodeIdx < fCallArgStartIdx) ?
								createInjectArg(fCallArgs, inject0Args.get(nodeIdx)) :
								fCallArgs.getChild(nodeIdx - fCallArgStartIdx);
					}
				};
			}
		}
		final int nodeArgCount= fCallArgStartIdx + fCallArgs.getChildCount();
		final int paramCount= fSpec.getParamCount();
		final FCall. @Nullable Arg[] allocatedArgs= (paramCount > 0) ?
				new FCall. @Nullable Arg[paramCount] : Nullable_NO_ARGS;
		final int ellipsisParamIdx= fSpec.indexOfParam("..."); //$NON-NLS-1$
		final int replValueParamIdx= (replValueArg != null) ? fSpec.indexOfParam("value") : -1; //$NON-NLS-1$
		
		final int[] argsNode2paramIdx= new int[nodeArgCount];
		int replValue2paramIdx;
		int ellipsisCount= 0;
		int failCount= 0;
		boolean testPartial= false;
		
		if (replValueArg != null) {
			if (replValueParamIdx > 0) {
				allocatedArgs[replValueParamIdx]= createInjectArg(fCallArgs, replValueArg);
				replValue2paramIdx= replValueParamIdx;
			}
			else if (replValueParamIdx == 0) {
				replValue2paramIdx= FAIL;
				failCount++;
			}
			else {
				replValue2paramIdx= ELLIPSIS;
				ellipsisCount++;
			}
		}
		else {
			replValue2paramIdx= NA;
		}
		for (int nodeIdx= 0; nodeIdx < fCallArgStartIdx; nodeIdx++) {
			argsNode2paramIdx[nodeIdx]= TEST_POSITION;
		}
		for (int nodeIdx= fCallArgStartIdx; nodeIdx < nodeArgCount; nodeIdx++) {
			final FCall.Arg argNode= getFCallArg.apply(nodeIdx);
			if (argNode.hasName()) {
				final Parameter param= fSpec.getParam(argNode.getNameChild().getText());
				if (param != null && param.index != ellipsisParamIdx) {
					if (allocatedArgs[param.index] == null) {
						allocatedArgs[param.index]= argNode;
						argsNode2paramIdx[nodeIdx]= param.index;
					}
					else {
						failCount++;
						argsNode2paramIdx[nodeIdx]= FAIL;
					}
				}
				else {
					testPartial= true;
					argsNode2paramIdx[nodeIdx]= TEST_PARTIAL;
				}
			}
			else {
				argsNode2paramIdx[nodeIdx]= TEST_POSITION;
			}
		}
		
		final int testStop= (ellipsisParamIdx >= 0) ? ellipsisParamIdx : paramCount;
		if (testPartial) {
			FCall. @Nullable Arg[] partialArgs= null;
			ITER_ARGS: for (int nodeIdx= fCallArgStartIdx; nodeIdx < nodeArgCount; nodeIdx++) {
				if (argsNode2paramIdx[nodeIdx] == TEST_PARTIAL) {
					final FCall.Arg argNode= getFCallArg.apply(nodeIdx);
					int matchIdx= -1;
					final String name= argNode.getNameChild().getText();
					if (name != null) {
						for (int paramIdx= 0; paramIdx < testStop; paramIdx++) {
							final String paramName;
							if (allocatedArgs[paramIdx] == null
									&& (paramName= fSpec.getParam(paramIdx).getName()) != null
									&& paramName.startsWith(name) ) {
								if (matchIdx < 0) {
									matchIdx= paramIdx;
								}
								else {
									failCount++;
									argsNode2paramIdx[nodeIdx]= FAIL;
									continue ITER_ARGS;
								}
							}
						}
					}
					if (matchIdx >= 0) {
						if (partialArgs == null) {
							partialArgs= new FCall. @Nullable Arg[testStop];
							partialArgs[matchIdx]= argNode;
							argsNode2paramIdx[nodeIdx]= matchIdx;
							continue ITER_ARGS;
						}
						if (partialArgs[matchIdx] == null) {
							partialArgs[matchIdx]= argNode;
							argsNode2paramIdx[nodeIdx]= matchIdx;
							continue ITER_ARGS;
						}
					}
					ellipsisCount++;
					argsNode2paramIdx[nodeIdx]= ELLIPSIS;
					continue ITER_ARGS;
				}
			}
			if (partialArgs != null) {
				for (int i= 0; i < testStop; i++) {
					if (partialArgs[i] != null) {
						allocatedArgs[i]= partialArgs[i];
					}
				}
			}
		}
		
		if (replValueArg != null && replValueParamIdx != 0
				&& allocatedArgs[0] == null && nodeArgCount > 0) {
			switch (argsNode2paramIdx[0]) {
			case FAIL:
				break;
			case ELLIPSIS:
				ellipsisCount--;
				//$FALL-THROUGH$
			case TEST_POSITION:
				allocatedArgs[0]= getFCallArg.apply(0);
				argsNode2paramIdx[0]= 0;
				break;
			default:
				allocatedArgs[0]= allocatedArgs[argsNode2paramIdx[0]];
				allocatedArgs[argsNode2paramIdx[0]]= null;
				argsNode2paramIdx[0]= 0;
				break;
			}
		}
		
		{	int paramIdx= 0;
			ITER_ARGS: for (int nodeIdx= 0; nodeIdx < nodeArgCount; nodeIdx++) {
				if (argsNode2paramIdx[nodeIdx] == TEST_POSITION) {
					ITER_DEFS: while (paramIdx < testStop) {
						if (allocatedArgs[paramIdx] == null) {
							argsNode2paramIdx[nodeIdx]= paramIdx;
							allocatedArgs[paramIdx++]= getFCallArg.apply(nodeIdx);
							continue ITER_ARGS;
						}
						else {
							paramIdx++;
							continue ITER_DEFS;
						}
					}
					argsNode2paramIdx[nodeIdx]= ELLIPSIS;
					ellipsisCount++;
					continue ITER_ARGS;
				}
			}
		}
		
		if (ellipsisParamIdx == -1 && ellipsisCount > 0) {
			ITER_ARGS: for (int nodeIdx= 0; nodeIdx < nodeArgCount; nodeIdx++) {
				if (argsNode2paramIdx[nodeIdx] == ELLIPSIS) {
					argsNode2paramIdx[nodeIdx]= FAIL;
				}
			}
			if (replValue2paramIdx == ELLIPSIS) {
				replValue2paramIdx= FAIL;
			}
			failCount+= ellipsisCount;
			ellipsisCount= 0;
		}
		final FCall.Arg[] ellipsisArgs= (ellipsisCount > 0) ? new FCall. @NonNull Arg[ellipsisCount] : NonNull_NO_ARGS;
		final FCall.Arg[] otherArgs= (failCount > 0) ? new FCall. @NonNull Arg[failCount] : NonNull_NO_ARGS;
		if (ellipsisCount > 0 || failCount > 0) {
			int ellipsisIdx= 0;
			int otherIdx= 0;
			ITER_ARGS: for (int nodeIdx= 0; nodeIdx < nodeArgCount; nodeIdx++) {
				switch (argsNode2paramIdx[nodeIdx]) {
				case ELLIPSIS:
					ellipsisArgs[ellipsisIdx++]= getFCallArg.apply(nodeIdx);
					argsNode2paramIdx[nodeIdx]= ellipsisParamIdx;
					continue ITER_ARGS;
				case FAIL:
					otherArgs[otherIdx++]= getFCallArg.apply(nodeIdx);
					continue ITER_ARGS;
				}
			}
			switch (replValue2paramIdx) {
			case ELLIPSIS:
				ellipsisArgs[ellipsisIdx++]= createInjectArg(fCallArgs, replValueArg);
				break;
			case FAIL:
				otherArgs[otherIdx++]= createInjectArg(fCallArgs, replValueArg);
				break;
			default:
				break;
			}
		}
		return new FCallArgMatch(fSpec, fCallArgs, fCallArgStartIdx,
				allocatedArgs, ellipsisArgs, otherArgs,
				argsNode2paramIdx );
	}
	
	@SuppressWarnings("null")
	private static FCall.Arg createInjectArg(final FCall.Args argsNode, final @Nullable RAstNode valueNode) {
		final var arg= new FCall.Arg(argsNode,
				(valueNode != null) ? valueNode.getStartOffset() : NA_OFFSET, NA_OFFSET );
		arg.valueExpr.node= valueNode;
		arg.updateOffsets();
		return arg;
	}
	
	@SuppressWarnings("null")
	private static FCall.Arg createPlaceholderArg(final FCall.Arg argNode, final @Nullable RAstNode valueNode) {
		final var arg= new FCall.Arg(argNode.getRParent(),
				argNode.getStartOffset(), argNode.getEndOffset() );
		arg.argName= argNode.argName;
		arg.valueExpr.node= valueNode;
		return arg;
	}
	
	public static FCallArgMatch matchArgs(final FCall fCallNode, final RFunctionSpec fSpec) {
		@Nullable RAstNode replValueArg= null;
		ImList<@Nullable RAstNode> inject0Args= ImCollections.emptyList();
		final RAstNode parentNode= fCallNode.getRParent();
		if (parentNode != null) {
			switch (parentNode.getNodeType()) {
			case PIPE_FORWARD:
				{	final Pipe pipeNode= (Pipe)parentNode;
					if (pipeNode.getTargetChild() == fCallNode) {
						inject0Args= ImCollections.newList(pipeNode.getSourceChild());
					}
					break;
				}
			case A_LEFT:
			case A_EQUALS:
			case A_RIGHT:
				{	final Assignment assignNode= (Assignment)parentNode;
					if (assignNode.getTargetChild() == fCallNode) {
						replValueArg= assignNode.getSourceChild();
					}
					break;
				}
			default:
				break;
			}
		}
		return matchArgs(fCallNode.getArgsChild(), fSpec, replValueArg, inject0Args);
	}
	
	
	/**
	 * @return position of the element name, if possible (symbol or strings), otherwise <code>null</code>.
	 */
	public static @Nullable TextRegion getElementNameRegion(final RAstNode node) {
		switch (node.getNodeType()) {
		case SYMBOL:
		case STRING_CONST:
			return node.getTextRegion();
		default:
			return null;
		}
	}
	
	
	static int indexOfPlaceholder(final CallArgs args) {
		final var argChildren= args.getArgChildren();
		for (int idx= 0; idx < argChildren.size(); idx++) {
			final CallArg arg= argChildren.get(idx);
			final RAstNode argValue= arg.getValueChild();
			if (argValue != null
					&& argValue.getNodeType() == NodeType.PLACEHOLDER
					&& argValue.getOperator(0) == RTerminal.PIPE_PLACEHOLDER ) {
				return idx;
			}
		}
		return -1;
	}
	
	
	public static int @Nullable [] computeRExpressionIndex(RAstNode node, final RAstNode baseNode) {
		final IntList topdown= new IntArrayList();
		ITER_DOWN: while (node != baseNode) {
			RAstNode parent= nonNullAssert(node.getRParent());
			switch (parent.getNodeType()) {
			
			case SOURCELINES:
				topdown.add(1 + parent.getChildIndex(node));
				node= parent;
				continue ITER_DOWN;
			
			case BLOCK:
			case GROUP:
			
			case NS_GET:
			case NS_GET_INT:
			
			case SUB_NAMED_PART:
			case SUB_NAMED_SLOT:
			
			case POWER:
			case SIGN:
			case SEQ:
			case SPECIAL:
			case MULT:
			case ADD:
			case RELATIONAL:
			case NOT:
			case AND:
			case OR:
			
			case MODEL:
			case HELP:
			
			case A_LEFT:
			case A_EQUALS:
			case A_COLON:
				topdown.add(2 + parent.getChildIndex(node));
				node= parent;
				continue;
			case A_RIGHT:
				topdown.add(3 - parent.getChildIndex(node));
				node= parent;
				continue ITER_DOWN;
			
			case C_IF:
			case C_FOR:
			case C_WHILE:
			case C_REPEAT:
				topdown.add(2 + parent.getChildIndex(node));
				node= parent;
				continue;
			
			case F_DEF:
				topdown.add(2 + parent.getChildIndex(node));
				node= parent;
				continue ITER_DOWN;
			case F_DEF_ARGS:
				node= parent;
				continue ITER_DOWN;
			case F_DEF_ARG:
				{	if (parent == baseNode) {
						return null;
					}
					final FDef.Arg arg= (FDef.Arg)parent;
					/* FDef.Args*/ parent= nonNullAssert(arg.getRParent());
					if (node == arg.getDefaultChild()) {
						topdown.add(1 + parent.getChildIndex(arg));
					}
					node= parent;
					continue ITER_DOWN;
				}
			
			case F_CALL:
				if (node == parent.getChild(0)) {
					topdown.add(1);
				}
				node= parent;
				continue ITER_DOWN;
			case F_CALL_ARGS:
				node= parent;
				continue ITER_DOWN;
			case F_CALL_ARG:
				{	if (parent == baseNode) {
						return null;
					}
					final FCall.Arg arg= (FCall.Arg)parent;
					/* FCall.Args*/ parent= nonNullAssert(arg.getRParent());
					if (node == arg.getValueChild()) {
						topdown.add(2 + parent.getChildIndex(arg));
					}
					node= parent;
					continue ITER_DOWN;
				}
			case PIPE_FORWARD:
				{	final var pipe= (Pipe)parent;
					final RAstNode target= pipe.getTargetChild();
					final RAstNode subRef;
					CallArgs callArgs;
					int callShift= 0;
					switch (target.getNodeType()) {
					case F_CALL:
						subRef= null;
						callArgs= ((FCall)target).getArgsChild();
						callShift= 2;
						break;
					case SUB_INDEXED_S:
					case SUB_INDEXED_D:
						subRef= ((SubIndexed)target).getRefChild();
						callArgs= ((SubIndexed)target).getArgsChild();
						callShift= 3;
						break;
					case SUB_NAMED_PART:
					case SUB_NAMED_SLOT:
						subRef= ((SubNamed)target).getRefChild();
						callArgs= null;
						break;
					default:
						return null;
					}
					
					if (subRef != null) {
						if (subRef.getNodeType() == NodeType.PLACEHOLDER
								&& subRef.getOperator(0) == RTerminal.PIPE_PLACEHOLDER ) {
							if (node == pipe.getSourceChild()) {
								topdown.add(2);
							}
							node= parent;
							continue ITER_DOWN;
						}
					}
					if (callArgs != null) {
						final int indexPlaceholder= indexOfPlaceholder(callArgs);
						if (node == pipe.getSourceChild()) {
							topdown.add((indexPlaceholder >= 0) ? callShift + indexPlaceholder : callShift);
						}
						else if (/* node == pipe.getTargetChild() &&*/ indexPlaceholder == -1) {
							final int i= topdown.size() - 1;
							if (i >= 0) {
								final int index= topdown.getAt(i);
								if (index > 1) {
									topdown.setAt(i, index + 1);
								}
							}
						}
						node= parent;
						continue ITER_DOWN;
					}
					return null;
				}
			
			case SUB_INDEXED_S:
			case SUB_INDEXED_D:
				if (node == parent.getChild(0)) {
					topdown.add(2);
				}
				node= parent;
				continue ITER_DOWN;
			case SUB_INDEXED_ARGS:
				node= parent;
				continue ITER_DOWN;
			case SUB_INDEXED_ARG:
				{	if (parent == baseNode) {
						return null;
					}
					final SubIndexed.Arg arg= (SubIndexed.Arg)parent;
					/* SubIndexed.Args*/ parent= nonNullAssert(arg.getRParent());
					if (node == arg.getValueChild()) {
						topdown.add(3 + parent.getChildIndex(arg));
					}
					node= parent;
					continue ITER_DOWN;
				}
			
			case ERROR:
			case DUMMY:
				return null;
			
			case COMMENT:
			case DOCU_AGGREGATION:
			case DOCU_TAG:
			case DOCU_TEXT:
			case ERROR_TERM:
			case STRING_CONST:
			case NUM_CONST:
			case NULL_CONST:
			case SYMBOL:
			case C_BREAK:
			case C_NEXT:
			default:
				throw new IllegalStateException("Unexpected parent: type= " + node.getNodeType()); //$NON-NLS-1$
			}
		}
		final int l= topdown.size();
		final int[] path= new int[l];
		for (int i= 0; i < l;) {
			path[i]= topdown.getAt(l - ++i);
		}
		return path;
	}
	
	public static @Nullable List<RAstNode> computeRExpressionNodes(RAstNode node, final RAstNode baseNode) {
		final List<RAstNode> nodes= new ArrayList<>();
		ITER_DOWN: while (node != baseNode) {
			RAstNode parent= nonNullAssert(node.getRParent());
			switch (parent.getNodeType()) {
			
			case SOURCELINES:
			
			case BLOCK:
			case GROUP:
			
			case NS_GET:
			case NS_GET_INT:
			
			case SUB_NAMED_PART:
			case SUB_NAMED_SLOT:
			
			case POWER:
			case SIGN:
			case SEQ:
			case SPECIAL:
			case MULT:
			case ADD:
			case RELATIONAL:
			case NOT:
			case AND:
			case OR:
			
			case MODEL:
			case HELP:
			
			case A_LEFT:
			case A_RIGHT:
			case A_EQUALS:
			case A_COLON:
			
			case C_IF:
			case C_FOR:
			case C_WHILE:
			case C_REPEAT:
				nodes.add(node);
				node= parent;
				continue ITER_DOWN;
			
			case F_DEF:
				nodes.add(node);
				node= parent;
				continue ITER_DOWN;
			case F_DEF_ARGS:
				node= parent;
				continue ITER_DOWN;
			case F_DEF_ARG:
				{	if (parent == baseNode) {
						return null;
					}
					final FDef.Arg arg= (FDef.Arg)parent;
					/* FDef.Args*/ parent= nonNullAssert(arg.getRParent());
					if (node == arg.getDefaultChild()) {
						nodes.add(node);
					}
					node= parent;
					continue ITER_DOWN;
				}
			
			case F_CALL:
				if (node == parent.getChild(0)) {
					nodes.add(node);
				}
				node= parent;
				continue ITER_DOWN;
			case F_CALL_ARGS:
				node= parent;
				continue ITER_DOWN;
			case F_CALL_ARG:
				{	if (parent == baseNode) {
						return null;
					}
					final FCall.Arg arg= (FCall.Arg)parent;
					/* FCall.Args*/ parent= nonNullAssert(arg.getRParent());
					if (node == arg.getValueChild()) {
						nodes.add(node);
					}
					node= parent;
					continue ITER_DOWN;
				}
			case PIPE_FORWARD:
				{	final var pipe= (Pipe)parent;
					if (node == pipe.getSourceChild()) {
						nodes.add(node);
					}
					node= parent;
					continue ITER_DOWN;
				}
			
			case SUB_INDEXED_S:
			case SUB_INDEXED_D:
				if (node == parent.getChild(0)) {
					nodes.add(node);
				}
				node= parent;
				continue ITER_DOWN;
			case SUB_INDEXED_ARGS:
				node= parent;
				continue ITER_DOWN;
			case SUB_INDEXED_ARG:
				{	if (parent == baseNode) {
						return null;
					}
					final SubIndexed.Arg arg= (SubIndexed.Arg)parent;
					/* SubIndexed.Args*/ parent= nonNullAssert(arg.getRParent());
					if (node == arg.getValueChild()) {
						nodes.add(node);
					}
					node= parent;
					continue ITER_DOWN;
				}
			
			case ERROR:
			case DUMMY:
				return null;
			
			case COMMENT:
			case DOCU_AGGREGATION:
			case DOCU_TAG:
			case DOCU_TEXT:
			case ERROR_TERM:
			case STRING_CONST:
			case NUM_CONST:
			case NULL_CONST:
			case SYMBOL:
			case C_BREAK:
			case C_NEXT:
			default:
				throw new IllegalStateException("Unexpected parent: type= " + node.getNodeType()); //$NON-NLS-1$
			}
		}
		if (nodes.size() > 1) {
			Collections.reverse(nodes);
		}
		return nodes;
	}
	
	public static RAstNode getRRootNode(RAstNode node, final @Nullable IRegion region) {
		if (region == null) {
			return node.getRRoot();
		}
		while (node.getRParent() != null) {
			final RAstNode parent= node.getRParent();
			final int beginDiff;
			final int endDiff;
			if ((beginDiff= region.getOffset() - parent.getStartOffset()) > 0
					|| (endDiff= region.getOffset() + region.getLength() - parent.getEndOffset()) < 0 ) {
				return node;
			}
			else if (beginDiff == 0 && endDiff == 0) {
				return (parent.getNodeType() == NodeType.SOURCELINES) ? node : parent;
			}
			else {
				node= parent;
				continue;
			}
		}
		return node;
	}
	
	@SuppressWarnings("null")
	public static boolean isParentChild(final RAstNode parent, RAstNode child) {
		while ((child= child.getRParent()) != null) {
			if (child == parent) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isSequenceNode(final RAstNode node) {
		return switch (node.getNodeType()) {
		case SOURCELINES, BLOCK -> true;
		default -> false;
		};
	}
	
	public static @Nullable RAstNode getSequenceChild(RAstNode node) {
		RAstNode parent;
		while ((parent= node.getRParent()) != null) {
			switch (parent.getNodeType()) {
			case SOURCELINES, BLOCK:
				return node;
			default:
				node= parent;
				continue;
			}
		}
		return null;
	}
	
	public static RAstNode getSequenceChildElseRoot(RAstNode node) {
		RAstNode parent;
		while ((parent= node.getRParent()) != null) {
			switch (parent.getNodeType()) {
			case SOURCELINES, BLOCK:
				return node;
			default:
				node= parent;
				continue;
			}
		}
		return node;
	}
	
	
	public static @Nullable Object toJava(@Nullable RAstNode node) {
		while (node != null) {
			switch (node.getNodeType()) {
			case NUM_CONST:
				switch (node.getOperator(0)) {
				case NUM_NUM:
					return parseNum(node.getText());
				case NUM_INT:
					return parseInt(node.getText());
				case TRUE:
					return Boolean.TRUE;
				case FALSE:
					return Boolean.FALSE;
				default:
					break;
				}
				return null;
			case F_CALL_ARG:
				node= ((FCall.Arg)node).getValueChild();
				continue;
			default:
				return null;
			}
		}
		return null;
	}
	
	public static @Nullable Boolean toJavaBoolean(@Nullable RAstNode node) {
		while (node != null) {
			switch (node.getNodeType()) {
			case NUM_CONST:
				switch (node.getOperator(0)) {
				case NUM_NUM: {
						final Double num= parseNum(node.getText());
						if (num != null && num.doubleValue() == Math.rint(num.doubleValue())) {
							return Boolean.valueOf(num.intValue() != 0);
						}
					}
					break;
				case NUM_INT: {
						final Integer num= parseInt(node.getText());
						if (num != null) {
							return Boolean.valueOf(num.intValue() != 0);
						}
					}
					break;
				case TRUE:
					return Boolean.TRUE;
				case FALSE:
					return Boolean.FALSE;
				default:
					break;
				}
				return null;
			case F_CALL_ARG:
				node= ((FCall.Arg)node).getValueChild();
				continue;
			default:
				return null;
			}
		}
		return null;
	}
	
	public static boolean toJavaBooleanValue(final @Nullable RAstNode node, final boolean defaultValue) {
		final Boolean value= toJavaBoolean(node);
		return (value != null) ? value : defaultValue;
	}
	
	public static @Nullable Integer toJavaInt(@Nullable RAstNode node, final boolean coerceNonNumeric) {
		while (node != null) {
			switch (node.getNodeType()) {
			case NUM_CONST:
				switch (node.getOperator(0)) {
				case NUM_NUM: {
						final Double num= parseNum(node.getText());
						if (num != null && num.doubleValue() == Math.rint(num.doubleValue())) {
							return num.intValue();
						}
					}
					break;
				case NUM_INT:
					return parseInt(node.getText());
				case TRUE:
					return (coerceNonNumeric) ? 1 : null;
				case FALSE:
					return (coerceNonNumeric) ? 0 : null;
				default:
					break;
				}
				return null;
			case F_CALL_ARG:
				node= ((FCall.Arg)node).getValueChild();
				continue;
			default:
				return null;
			}
		}
		return null;
	}
	
	public static @Nullable Integer toJavaInt(final @Nullable RAstNode node) {
		return toJavaInt(node, true);
	}
	
	public static @Nullable Float toJavaFloat(@Nullable RAstNode node) {
		while (node != null) {
			switch (node.getNodeType()) {
			case NUM_CONST:
				switch (node.getOperator(0)) {
				case NUM_NUM: {
						final Double num= parseNum(node.getText());
						if (num != null && Math.abs(num.doubleValue()) <= Float.MAX_VALUE) {
							return num.floatValue();
						}
					}
					break;
				case NUM_INT: {
						final Integer num= parseInt(node.getText());
						if (num != null) {
							return num.floatValue();
						}
					}
					break;
				case TRUE:
					return 1f;
				case FALSE:
					return 0f;
				default:
					break;
				}
				return null;
			case F_CALL_ARG:
				node= ((FCall.Arg) node).getValueChild();
				continue;
			default:
				return null;
			}
		}
		return null;
	}
	
	public static @Nullable Double toJavaDouble(@Nullable RAstNode node) {
		while (node != null) {
			switch (node.getNodeType()) {
			case NUM_CONST:
				switch (node.getOperator(0)) {
				case NUM_NUM: {
						final Double num= parseNum(node.getText());
						if (num != null) {
							return num;
						}
					}
					break;
				case NUM_INT: {
						final Integer num= parseInt(node.getText());
						if (num != null) {
							return num.doubleValue();
						}
					}
					break;
				case TRUE:
					return 1.0;
				case FALSE:
					return 0.0;
				default:
					break;
				}
				return null;
			case F_CALL_ARG:
				node= ((FCall.Arg)node).getValueChild();
				continue;
			default:
				return null;
			}
		}
		return null;
	}
	
	private static @Nullable Double parseNum(final String text) {
		if (text != null && !text.isEmpty()) {
			try {
				return Double.valueOf(text);
			}
			catch (final NumberFormatException e) {}
		}
		return null;
	}
	
	private static @Nullable Integer parseInt(String text) {
		if (text != null && !text.isEmpty()) {
			try {
				if (text.endsWith("L")) { //$NON-NLS-1$
					text= text.substring(0, text.length() - 1);
				}
				if (text.startsWith("0x")) { //$NON-NLS-1$
					text= text.substring(2);
					return Integer.valueOf(text, 16);
				}
				else {
					return Integer.valueOf(text);
				}
			}
			catch (final NumberFormatException e) {}
		}
		return null;
	}
	
}
