/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * 
 */
@NonNullByDefault
public abstract class Placeholder extends SingleValue {
	
	
	static final class PipeIn extends Placeholder {
		
		
		PipeIn() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.PIPE_PLACEHOLDER;
		}
		
		@Override
		public @Nullable String getText() {
			return null;
		}
		
		
	}
	
	
	Placeholder() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.PLACEHOLDER;
	}
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	@Override
	public final @Nullable TextRegion getTextRegion() {
		return this;
	}
	
	@Override
	void setText(final @Nullable String text, final @Nullable TextRegion textRegion) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		if (NodeType.PLACEHOLDER == element.getNodeType()) {
			final Placeholder other= (Placeholder)element;
			return (getOperator(0) == other.getOperator(0));
		}
		return false;
	}
	
	@Override
	void appendPathElement(final StringBuilder s) {
//		if (parent != null) {
//			s.append(parent.getEqualsIndex(this));
//		}
		s.append('$');
		s.append(getOperator(0));
	}
	
}
