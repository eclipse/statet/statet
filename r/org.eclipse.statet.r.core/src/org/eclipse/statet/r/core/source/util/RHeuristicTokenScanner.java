/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.util;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.ROUND_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.CharPairSet;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.text.core.HeuristicTokenScanner;
import org.eclipse.statet.r.core.rlang.RTokens;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;


@NonNullByDefault
public class RHeuristicTokenScanner extends HeuristicTokenScanner {
	
	
	public static final CharPairSet R_BRACKETS= new CharPairSet(
			ImCollections.newIdentityList(CURLY_BRACKETS, ROUND_BRACKETS, SQUARE_BRACKETS) );
	
	private static final StopCondition R_WORD_CONDITION= new StopCondition() {
				@Override
				public boolean matches(final char c) {
					return RTokens.isRobustSeparator(c);
				}
			};
	private static final StopCondition R_WORD_DOT_SEP_CONDITION= new StopCondition() {
				@Override
				public boolean matches(final char c) {
					return RTokens.isRobustSeparator(c, true);
				}
			};
	
	
	public static RHeuristicTokenScanner create(final DocContentSections documentContentInfo) {
		return (documentContentInfo.getPrimaryType() == RDocumentConstants.R_PARTITIONING) ?
				new RHeuristicTokenScanner(documentContentInfo) :
				new RChunkHeuristicTokenScanner(documentContentInfo);
	}
	
	
	protected RHeuristicTokenScanner(final DocContentSections documentContentInfo) {
		super(documentContentInfo, RDocumentConstants.R_DEFAULT_CONTENT_CONSTRAINT);
	}
	
	
	@Override
	public CharPairSet getDefaultBrackets() {
		return R_BRACKETS;
	}
	
	
	public @Nullable IRegion findRWord(final int position, final boolean isDotSeparator, final boolean allowEnd)
			throws BadLocationException {
		return findAnyRegion(position, (isDotSeparator) ? R_WORD_DOT_SEP_CONDITION : R_WORD_CONDITION, true);
	}
	
	
}
