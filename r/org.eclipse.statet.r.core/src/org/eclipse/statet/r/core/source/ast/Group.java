/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_IN_GROUP_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>( ... )</code>
 */
@NonNullByDefault
public final class Group extends RAstNode {
	
	
	final Expression expr= new Expression();
	int groupCloseOffset= NA_OFFSET;
	
	
	Group() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.GROUP;
	}
	
	@Override
	public final RTerminal getOperator(final int index) {
		return RTerminal.GROUP_OPEN;
	}
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 1;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		return switch (index) {
		case 0 -> this.expr.node;
		default ->
			throw new IndexOutOfBoundsException();
		};
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.expr.node == child) {
			return 0;
		}
		return -1;
	}
	
	public final RAstNode getExprChild() {
		return this.expr.node;
	}
	
	public final int getGroupCloseOffset() {
		return this.groupCloseOffset;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.expr.node.acceptInR(visitor);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.expr.node);
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		if (this.expr.node == child) {
			return this.expr;
		}
		return null;
	}
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.GROUP == element.getNodeType());
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		if (this.expr == expr) {
			return TYPE123_SYNTAX_EXPR_IN_GROUP_MISSING;
		}
		throw new IllegalArgumentException();
	}
	
	@SuppressWarnings("unused")
	final void updateOffsets() {
		if (this.groupCloseOffset != NA_OFFSET) {
			doSetEndOffset(this.groupCloseOffset + 1);
		}
		else if (this.expr.node != null){
			doSetEndOffset(this.expr.node.getEndOffset());
		}
		else {
			doSetEndOffset(getStartOffset() + 1);
		}
	}
	
}
