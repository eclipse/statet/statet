/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.source.RTerminal;


@NonNullByDefault
public abstract class CallArgs extends RAstNode {
	
	
	CallArgs(final RAstNode parent) {
		super(parent);
	}
	
	CallArgs() {
		super();
	}
	
	
	@Override
	public final @Nullable RTerminal getOperator(final int index) {
		return null;
	}
	
	public abstract ImIntList getSeparatorOffsets();
	
	
	@Override
	public abstract CallArg getChild(final int index);
	
	public abstract ImList<? extends CallArg> getArgChildren();
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		return null;
	}
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		throw new IllegalArgumentException();
	}
	
}
