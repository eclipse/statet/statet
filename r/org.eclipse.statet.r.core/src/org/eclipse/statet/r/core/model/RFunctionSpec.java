/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Formals of an R function
 */
@NonNullByDefault
public final class RFunctionSpec implements Immutable {
	
	
	public static final int UNKNOWN=                        0;
	
	private static final int OBJ_SHIFT= 0;
	private static final int NAME_SHIFT= 8;
	private static final int OTHER_SHIFT= 16;
	private static final int FLAGS_SHIFT= 24;
	
	public static final int UNSPECIFIC_OBJ=                 1 << 0 << OBJ_SHIFT;
	public static final int FUN_OBJ=                        1 << 2 << OBJ_SHIFT;
	public static final int CLASS_OBJ=                      1 << 3 << OBJ_SHIFT;
	public static final int METHOD_OBJ=                     1 << 4 << OBJ_SHIFT;
	public static final int OTHER_SPECIFIC_OBJ=             1 << 6 << OBJ_SHIFT;
	
	public static final int UNSPECIFIC_NAME=                1 << 0 << NAME_SHIFT;
	public static final int PACKAGE_NAME=                   1 << 1 << NAME_SHIFT;
	public static final int CLASS_NAME=                     1 << 3 << NAME_SHIFT;
	public static final int METHOD_NAME=                    1 << 4 << NAME_SHIFT;
	
	public static final int FILE_NAME=                      1 << 1 << OTHER_SHIFT;
	public static final int HELP_TOPIC_NAME=                1 << 2 << OTHER_SHIFT;
	public static final int RELATOR_CODE=                   1 << 4 << OTHER_SHIFT;
	
	public static final int MASK_AS=                        0xF << FLAGS_SHIFT;
	public static final int AS_SYMBOL=                      1 << 0 << FLAGS_SHIFT;
	public static final int AS_STRING=                      1 << 1 << FLAGS_SHIFT;
	public static final int AS_N_VECTOR=                    1 << 2 << FLAGS_SHIFT;
	
	@SuppressWarnings("unused")
	private static void test(final int type) {
		switch (type) {
		case UNSPECIFIC_OBJ:
		case FUN_OBJ:
		case CLASS_OBJ:
		case METHOD_OBJ:
		case OTHER_SPECIFIC_OBJ:
			break;
		case UNSPECIFIC_NAME:
		case PACKAGE_NAME:
		case CLASS_NAME:
		case METHOD_NAME:
			break;
		case FILE_NAME:
		case HELP_TOPIC_NAME:
			break;
		case RELATOR_CODE:
			break;
		case AS_SYMBOL:
		case AS_STRING:
		case AS_N_VECTOR:
		default:
			break;
		}
	}
	
	
	public static interface TypeDescription extends Immutable {
		
		
		public int getType();
		
		public @Nullable String getClassName();
		
	}
	
	public static final class Parameter implements TypeDescription {
		
		
		public final int index;
		
		private final @Nullable String name;
		
		private final int type;
		
		private final @Nullable String className;
//		private final String defaultAsCode;
		
		
		public Parameter(final int index, final @Nullable String name, final int type,
				final @Nullable String className) {
			this.index= index;
			this.name= name;
			this.type= type;
			this.className= className;
		}
		
		Parameter(final int index, final DataStream in) throws IOException {
			this.index= index;
			this.name= in.readString();
			this.type= in.readInt();
			this.className= in.readString();
		}
		
		void writeTo(final DataStream out) throws IOException {
			out.writeString(this.name);
			out.writeInt(this.type);
			out.writeString(this.className);
		}
		
		
		public @Nullable String getName() {
			return this.name;
		}
		
		@Override
		public int getType() {
			return this.type;
		}
		
		@Override
		public @Nullable String getClassName() {
			return this.className;
		}
		
		
		@Override
		public int hashCode() {
			return this.index * 31 + Objects.hashCode(this.name);
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			if (this == obj) {
				return true;
			}
			return (obj instanceof final Parameter other
					&& this.index == other.index && Objects.equals(this.name, other.name)
					&& this.type == other.type && Objects.equals(this.className, other.className) );
		}
		
	}
	
	public static final class ReturnValue implements TypeDescription {
		
		
		private final int type;
		
		private final @Nullable String className;
		
		
		public ReturnValue(final int type,
				final @Nullable String className) {
			this.type= type;
			this.className= className;
		}
		
		ReturnValue(final DataStream in) throws IOException {
			this.type= in.readInt();
			this.className= in.readString();
		}
		
		void writeTo(final DataStream out) throws IOException {
			out.writeInt(this.type);
			out.writeString(this.className);
		}
		
		
		@Override
		public int getType() {
			return this.type;
		}
		
		@Override
		public @Nullable String getClassName() {
			return this.className;
		}
		
		
		@Override
		public int hashCode() {
			return this.type * 31 + Objects.hashCode(this.className);
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			if (this == obj) {
				return true;
			}
			return (obj instanceof final ReturnValue other
					&& this.type == other.type && Objects.equals(this.className, other.className) );
		}
		
	}
	
	
	protected final @NonNull Parameter[] parameters;
	protected final ImList<? extends ReturnValue> returns;
	
	
	/**
	 * For more detailed definitions, use an {@link RFunctionSpecBuilder}.
	 */
	public RFunctionSpec(final String... names) {
		this.parameters= new @NonNull Parameter[names.length];
		for (int i= 0; i < names.length; i++) {
			this.parameters[i]= new Parameter(i, names[i], 0, null);
		}
		this.returns= ImCollections.emptyList();
	}
	
	RFunctionSpec(final @NonNull Parameter[] parameters,
			final ImList<? extends ReturnValue> returns) {
		this.parameters= parameters;
		this.returns= returns;
	}
	
	
	public int getParamCount() {
		return this.parameters.length;
	}
	
	@SuppressWarnings("null")
	public boolean containsParam(final @Nullable String name) {
		if (name != null) {
			for (int i= 0; i < this.parameters.length; i++) {
				final Parameter parameter= this.parameters[i];
				if (parameter.name != null && parameter.name.equals(name)) {
					return true;
				}
			}
		}
		return false;
	}
	
	@SuppressWarnings("null")
	public @Nullable Parameter getParam(final @Nullable String name) {
		if (name != null) {
			for (int i= 0; i < this.parameters.length; i++) {
				final Parameter parameter= this.parameters[i];
				if (parameter.name != null && parameter.name.equals(name)) {
					return parameter;
				}
			}
		}
		return null;
	}
	
	public Parameter getParam(final int index) {
		return this.parameters[index];
	}
	
	@SuppressWarnings("null")
	public int indexOfParam(final @Nullable String name) {
		if (name != null) {
			for (int i= 0; i < this.parameters.length; i++) {
				final Parameter parameter= this.parameters[i];
				if (parameter.name != null && parameter.name.equals(name)) {
					return parameter.index;
				}
			}
		}
		return -1;
	}
	
	public ImList<? extends ReturnValue> getReturns() {
		return this.returns;
	}
	
	
	static final byte SER_V1= 1;
	
	public RFunctionSpec(final DataStream in, final byte v) throws IOException {
		switch (v) {
		case SER_V1:
			{	final int l= in.readInt();
				this.parameters= new @NonNull Parameter[l];
				for (int i= 0; i < l; i++) {
					this.parameters[i]= new Parameter(i, in);
				}
			}
			{	final int l= in.readInt();
				switch (l) {
				case 0:
					this.returns= ImCollections.emptyList();
					break;
				case 1:
					this.returns= ImCollections.newList(
							new ReturnValue(in) );
					break;
				default:
					final var array= new @NonNull ReturnValue[l];
					for (int i= 0; i < l; i++) {
						array[i]= new ReturnValue(in);
					}
					this.returns= ImCollections.newList(array);
					break;
				}
			}
			return;
		default:
			throw new IOException("Format not supported: v= " + v);
		}
	}
	
	public void writeTo(final DataStream out, final byte v) throws IOException {
		{	final int l= this.parameters.length;
			out.writeInt(l);
			for (int i= 0; i < l; i++) {
				this.parameters[i].writeTo(out);
			}
		}
		{	out.writeInt(this.returns.size());
			for (final var r : this.returns) {
				r.writeTo(out);
			}
		}
	}
	
	
	@Override
	public int hashCode() {
		return this.parameters.hashCode() * 31 + this.returns.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		return (obj instanceof final RFunctionSpec other
				&& Arrays.equals(this.parameters, other.parameters)
				&& this.returns.equals(other.returns) );
	}
	
}
