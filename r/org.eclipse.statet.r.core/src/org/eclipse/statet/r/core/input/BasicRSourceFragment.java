/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.input;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.text.AbstractDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.input.BasicSourceFragment;
import org.eclipse.statet.r.core.source.RSourceConfig;


@NonNullByDefault
public class BasicRSourceFragment extends BasicSourceFragment implements RSourceFragment {
	
	
	private final RSourceConfig rSourceConfig;
	
	
	public BasicRSourceFragment(final String id, final String name, final String fullName,
			final RSourceConfig rSourceConfig,
			final AbstractDocument document) {
		super(id, name, fullName, document);
		this.rSourceConfig= nonNullAssert(rSourceConfig);
	}
	
	
	@Override
	public RSourceConfig getRSourceConfig() {
		return this.rSourceConfig;
	}
	
	
	@Override
	protected Class<?> getFragmentType() {
		return RSourceFragment.class;
	}
	
	@Override
	@SuppressWarnings("null")
	public boolean equals(final @Nullable Object obj) {
		return (super.equals(obj)
				&& this.rSourceConfig.equals(((RSourceFragment)obj).getRSourceConfig()) );
	}
	
}
