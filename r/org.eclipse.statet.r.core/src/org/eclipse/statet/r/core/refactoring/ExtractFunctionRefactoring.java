/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.osgi.util.NLS;
import org.eclipse.text.edits.InsertEdit;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.r.core.refactoring.Messages;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringDescriptor;
import org.eclipse.statet.ltk.refactoring.core.LtkRefactoring;
import org.eclipse.statet.ltk.refactoring.core.RefactoringChange;
import org.eclipse.statet.ltk.refactoring.core.RefactoringMessages;
import org.eclipse.statet.ltk.refactoring.core.SourceUnitChange;
import org.eclipse.statet.ltk.refactoring.core.TextChangeCompatibility;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RUtil;
import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RModelManager;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.model.rlang.RFrame;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.Assignment;
import org.eclipse.statet.r.core.source.ast.Block;
import org.eclipse.statet.r.core.source.ast.FDef;
import org.eclipse.statet.r.core.source.ast.GenericVisitor;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;


@NonNullByDefault
public class ExtractFunctionRefactoring extends LtkRefactoring {
	
	
	private class VariableSearcher extends GenericVisitor {
		
		private final int start= ExtractFunctionRefactoring.this.operationRegion.getStartOffset();
		private final int stop= ExtractFunctionRefactoring.this.operationRegion.getEndOffset();
		
		@Override
		public void visitNode(final RAstNode node) throws InvocationTargetException {
			if (node.getStartOffset() >= this.stop || node.getEndOffset() < this.start) {
				return;
			}
			final List<Object> attachments= node.getAttachments();
			for (final Object attachment : attachments) {
				if (attachment instanceof final RElementAccess access) {
					if (access.getType() != RElementName.MAIN_DEFAULT
							|| access.getSegmentName() == null) {
						continue;
					}
					final RAstNode nameNode= access.getNameNode();
					if (nameNode != null
							&& nameNode.getStartOffset() >= this.start && nameNode.getEndOffset() <= this.stop) {
						add(access);
					}
				}
			}
			node.acceptInRChildren(this);
		}
		
		@Override
		public void visit(final FDef node) throws InvocationTargetException {
		}
		
		private void add(final RElementAccess access) {
			final String name= access.getSegmentName();
			if (name == null
					|| access.getFrame().getFrameType() == RFrame.PACKAGE) {
				return;
			}
			Variable variable= ExtractFunctionRefactoring.this.variablesMap.get(name);
			if (variable == null) {
				variable= new Variable(name);
				ExtractFunctionRefactoring.this.variablesMap.put(name, variable);
			}
			variable.checkAccess(access);
		}
		
	}
	
	public class Variable {
		
		
		private final String name;
		private boolean asArgument;
		private boolean asArgumentDefault;
		private RElementAccess firstAccess;
		private RElementAccess lastAccess;
		
		
		public Variable(final String name) {
			this.name= name;
		}
		
		
		void checkAccess(final RElementAccess access) {
			if (this.firstAccess == null
					|| access.getNode().getStartOffset() < this.firstAccess.getNode().getStartOffset()) {
				this.firstAccess= access;
				this.asArgumentDefault= this.asArgument= (!access.isWriteAccess() && !access.isFunctionAccess());
			}
			if (this.lastAccess == null
					|| access.getNode().getStartOffset() > this.lastAccess.getNode().getStartOffset()) {
				this.lastAccess= access;
			}
		}
		
		public String getName() {
			return this.name;
		}
		
		public boolean getUseAsArgumentDefault() {
			return this.asArgumentDefault;
		}
		
		public boolean getUseAsArgument() {
			return this.asArgument;
		}
		
		public void setUseAsArgument(final boolean enable) {
			this.asArgument= enable;
		}
		
	}
	
	
	private final RRefactoringAdapter adapter= new RRefactoringAdapter();
	private final ElementSet elementSet;
	
	private TextRegion selectionRegion;
	private TextRegion operationRegion;
	
	private final RSourceUnit sourceUnit;
	private @Nullable ImList<RAstNode> statements;
	
//	private RAstNode container;
	private Map<String, Variable> variablesMap;
	private List<Variable> variablesList;
	private String functionName;
	
	
	/**
	 * Creates a new extract function refactoring.
	 * @param su the source unit
	 * @param region (selected) region of the statements to extract
	 */
	public ExtractFunctionRefactoring(final RSourceUnit su, final TextRegion selection) {
		this.sourceUnit= su;
		this.elementSet= new ElementSet(su);
		
		if (selection != null && selection.getStartOffset() >= 0 && selection.getLength() >= 0) {
			this.selectionRegion= selection;
		}
	}
	
	
	@Override
	public String getBundleId() {
		return RCore.BUNDLE_ID;
	}
	
	@Override
	public String getIdentifier() {
		return RRefactoring.EXTRACT_FUNCTION_REFACTORING_ID;
	}
	
	@Override
	public String getName() {
		return Messages.ExtractFunction_label;
	}
	
	
	public void setFunctionName(final String newName) {
		this.functionName= newName;
	}
	
	public String getFunctionName() {
		return this.functionName;
	}
	
	public List<Variable> getVariables() {
		return this.variablesList;
	}
	
	
	@Override
	public RefactoringStatus checkInitialConditions(final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, 6);
		try {
			if (this.selectionRegion != null) {
				this.sourceUnit.connect(m.newChild(1));
				try {
					final AbstractDocument document= this.sourceUnit.getDocument(monitor);
					final RHeuristicTokenScanner scanner= this.adapter.getScanner(this.sourceUnit);
					
					ImList<RAstNode> statements= null;
					final RSourceUnitModelInfo modelInfo= (RSourceUnitModelInfo)this.sourceUnit.getModelInfo(RModel.R_TYPE_ID, RModelManager.MODEL_FILE, m.newChild(1));
					if (modelInfo != null) {
						final TextRegion region= this.adapter.trimToAstRegion(document,
								this.selectionRegion, scanner );
						final AstInfo ast= modelInfo.getAst();
						if (ast != null) {
							final AstSelection astSelection= AstSelection.search(ast.getRoot(),
									region.getStartOffset(), region.getEndOffset(),
									AstSelection.MODE_COVERING_SAME_LAST );
							final AstNode covering= astSelection.getCovering();
							if (covering instanceof final RAstNode rCovering) {
								if ((rCovering.getStartOffset() == region.getStartOffset() && rCovering.getLength() == region.getLength())
										|| !RAsts.isSequenceNode(rCovering) ) {
									statements= ImCollections.newList(rCovering);
								}
								else {
									final int count= rCovering.getChildCount();
									final List<RAstNode> childList= new ArrayList<>(count);
									int i= 0;
									for (; i < count; i++) {
										final RAstNode child= rCovering.getChild(i);
										if (child == astSelection.getChildFirstTouching()) {
											break;
										}
									}
									for (; i < count; i++) {
										final RAstNode child= rCovering.getChild(i);
										childList.add(child);
										if (child == astSelection.getChildLastTouching()) {
											break;
										}
									}
									if (!childList.isEmpty()) {
										statements= ImCollections.toList(childList);
									}
								}
							}
						}
					}
					
					if (statements != null) {
						this.statements= statements;
						this.operationRegion= this.adapter.expandSelectionRegion(document,
								new BasicTextRegion(
										statements.getFirst().getStartOffset(),
										statements.getLast().getEndOffset() ),
								this.selectionRegion, scanner );
					}
				}
				finally {
					this.sourceUnit.disconnect(m.newChild(1));
				}
			}
			
			if (this.statements == null) {
				return RefactoringStatus.createFatalErrorStatus(Messages.ExtractFunction_error_InvalidSelection_message);
			}
			final RefactoringStatus result= new RefactoringStatus();
			this.adapter.checkInitialToModify(result, this.elementSet);
			m.worked(1);
			
			if (result.hasFatalError()) {
				return result;
			}
			
			checkStatements(result);
			m.worked(2);
			return result;
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
		finally {
			m.done();
		}
	}
	
	private void checkStatements(final RefactoringStatus result) {
		final var statements= nonNullAssert(this.statements);
		for (final RAstNode node : statements) {
			if (RAsts.hasErrors(node)) {
				result.merge(RefactoringStatus.createWarningStatus(Messages.ExtractFunction_warning_SelectionSyntaxError_message));
				break;
			}
		}
		if (this.selectionRegion != null
				&& (this.selectionRegion.getStartOffset() != this.operationRegion.getStartOffset()
						|| this.selectionRegion.getLength() != this.operationRegion.getLength() )) {
			result.merge(RefactoringStatus.createWarningStatus(Messages.ExtractFunction_warning_ChangedRange_message));
		}
		
		this.variablesMap= new HashMap<>();
		final VariableSearcher searcher= new VariableSearcher();
		try {
			for (final RAstNode node : statements) {
				node.acceptInR(searcher);
			}
		} catch (final InvocationTargetException e) {}
		this.variablesList= new ArrayList<>(this.variablesMap.values());
		Collections.sort(this.variablesList, new Comparator<Variable>() {
			@Override
			public int compare(final Variable o1, final Variable o2) {
				return RElementAccess.NAME_POSITION_COMPARATOR.compare(o1.firstAccess, o2.firstAccess);
			}
		});
		this.functionName= ""; //$NON-NLS-1$
	}
	
	public RefactoringStatus checkFunctionName(final String name) {
		final String message= this.adapter.validateIdentifier(name, "The function name");
		if (message != null) {
			return RefactoringStatus.createFatalErrorStatus(message);
		}
		return new RefactoringStatus();
	}
	
	
	@Override
	public RefactoringStatus checkFinalConditions(final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_FinalCheck_label, 3);
		try {
			final RefactoringStatus status= checkFunctionName(this.functionName);
			this.adapter.checkFinalToModify(status, this.elementSet, m.newChild(2));
			return status;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public Change createChange(final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_CreateChanges_label, 3);
		try {
			final TextFileChange textFileChange= new SourceUnitChange(this.sourceUnit);
			if (this.sourceUnit.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
				textFileChange.setSaveMode(TextFileChange.LEAVE_DIRTY);
			}
			createChanges(textFileChange, m.newChild(2));
			
			final Map<String, String> arguments= new HashMap<>();
			final String varName= RRefactoringAdapter.getUnquotedIdentifier(this.functionName);
			final String description= NLS.bind(Messages.ExtractFunction_Descriptor_description,
					RUtil.formatVarName(varName) );
			final IProject resource= this.elementSet.getSingleProject();
			final String project= (resource != null) ? resource.getName() : null;
			final String source= (project != null) ? NLS.bind(RefactoringMessages.Common_Source_Project_label, project) : RefactoringMessages.Common_Source_Workspace_label;
			final int flags= 0;
			final String comment= ""; //$NON-NLS-1$
			final CommonRefactoringDescriptor descriptor= new CommonRefactoringDescriptor(
					getIdentifier(), project, description, comment, arguments, flags);
			m.worked(1);
			
			return new RefactoringChange(descriptor, getName(),
					new Change[] { textFileChange } );
		}
		catch (final BadLocationException e) {
			throw new CoreException(new Status(IStatus.ERROR, RCore.BUNDLE_ID, "Unexpected error (concurrent change?)", e));
		}
		finally {
			m.done();
		}
	}
	
	private void createChanges(final TextFileChange change, final SubMonitor m) throws BadLocationException, CoreException {
		m.setWorkRemaining(3 + 2 * 4);
		
		this.sourceUnit.connect(m.newChild(1));
		try {
			final var util= new RSourceCodeModifyUtil(this.adapter, this.sourceUnit, m.newChild(1));
			final var document= util.getSourceDocument();
			
			final var statements= nonNullAssert(this.statements);
			final RAstNode baseNode= RAsts.getSequenceChildElseRoot(statements.getFirst());
			final boolean isBaseNode= (baseNode == statements.getFirst());
			
			int startOffset;
			int endOffset;
			final RAstNode lastNode;
			if (statements.size() == 1 && statements.getFirst().getNodeType() == NodeType.BLOCK) {
				final Block block= (Block)statements.getFirst();
				startOffset= block.getStartOffset() + 1;
				endOffset= block.getBlockCloseOffset();
				if (endOffset == NA_OFFSET) {
					endOffset= block.getEndOffset();
				}
				lastNode= (block.getChildCount() > 0) ?
						block.getChild(block.getChildCount() - 1) : block.getChild(123);
			}
			else {
				startOffset= statements.getFirst().getStartOffset();
				lastNode= statements.getLast();
				endOffset= lastNode.getEndOffset();
			}
			
			// define fun
			util.clear();
			util.append(this.functionName);
			util.appendOperator(RTerminal.ARROW_LEFT_S);
			util.append("function("); //$NON-NLS-1$
			boolean hasArguments= false;
			for (final Variable variable : this.variablesList) {
				if (variable.getUseAsArgument()) {
					util.append(variable.getName());
					util.append(", "); //$NON-NLS-1$
					hasArguments= true;
				}
			}
			if (hasArguments) {
				util.deleteEnd(2);
			}
			util.append(')');
			if (startOffset < endOffset) {
				final IRegion lastLine= document.getLineInformationOfOffset(endOffset - 1);
				endOffset= Math.min(endOffset, lastLine.getOffset() + lastLine.getLength());
			}
			util.appendFDefBodyBlock(startOffset, endOffset);
			
			if (isBaseNode) {
				util.appendLineSeparator();
			}
			util.correctIndent(baseNode.getStartOffset());
			if (!isBaseNode) {
				util.appendLineSeparator();
			}
			
			final TextRegion region= this.adapter.expandWhitespaceBlock(document,
					this.operationRegion, util.getScanner() );
			final int insertOffset= this.adapter.expandWhitespaceBlock(document,
					this.adapter.expandSelectionRegion(document, new BasicTextRegion(baseNode.getStartOffset()),
							this.operationRegion, util.getScanner() ),
					util.getScanner() ).getStartOffset();
			if (insertOffset == region.getStartOffset()) {
				TextChangeCompatibility.addTextEdit(change, Messages.ExtractFunction_Changes_ReplaceOldWithFunctionDef_name,
						util.createReplaceEdit(region) );
			}
			else {
				TextChangeCompatibility.addTextEdit(change, Messages.ExtractFunction_Changes_DeleteOld_name,
						util.createDeleteEdit(region) );
				TextChangeCompatibility.addTextEdit(change, Messages.ExtractFunction_Changes_AddFunctionDef_name,
						util.createInsertEdit(insertOffset) );
			}
			m.worked(4);
			
			{	// replace by fun call
				util.clear();
				if (isBaseNode && lastNode.getNodeType() == NodeType.A_LEFT) {
					final Assignment assignment= (Assignment)lastNode;
					util.appendTrim(assignment.getTargetChild());
					util.appendOperator(assignment.getOperator(0));
				}
				util.append(this.functionName);
				util.append('(');
				for (final Variable variable : this.variablesList) {
					if (variable.getUseAsArgument()) {
						util.append(variable.getName());
						util.appendArgAssign();
						util.append(variable.getName());
						util.append(", "); //$NON-NLS-1$
					}
				}
				if (hasArguments) {
					util.deleteEnd(2);
				}
				util.append(')');
				if (isBaseNode
						|| document.getLineOfOffset(this.operationRegion.getEndOffset()) < document.getLineOfOffset(region.getEndOffset()) ) {
					util.appendLineSeparator();
				}
				
				TextChangeCompatibility.addTextEdit(change, Messages.ExtractFunction_Changes_AddFunctionCall_name,
						new InsertEdit(region.getEndOffset(), util.toString()) );
				m.worked(4);
			}
		}
		finally {
			this.sourceUnit.disconnect(m.newChild(1));
		}
	}
	
}
