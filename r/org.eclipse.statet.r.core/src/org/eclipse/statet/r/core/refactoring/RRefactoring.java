/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.core.refactoring.RRefactoringFactory;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;


/**
 * R refactoring
 */
@NonNullByDefault
public class RRefactoring {
	
	
	public static final String DELETE_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.r.refactoring.DeleteElementsOperation"; //$NON-NLS-1$
	
	public static final String RENAME_IN_WORKSPACE_REFACTORING_ID= "org.eclipse.statet.r.refactoring.RenameInWorkspaceOperation"; //$NON-NLS-1$
	
	public static final String RENAME_IN_REGION_REFACTORING_ID= "org.eclipse.statet.r.refactoring.RenameInRegionOperation"; //$NON-NLS-1$
	
	public static final String INLINE_TEMP_REFACTORING_ID= "org.eclipse.statet.r.refactoring.InlineTempOperation"; //$NON-NLS-1$
	
	public static final String EXTRACT_TEMP_REFACTORING_ID= "org.eclipse.statet.r.refactoring.ExtractTempOperation"; //$NON-NLS-1$
	
	public static final String EXTRACT_FUNCTION_REFACTORING_ID= "org.eclipse.statet.r.refactoring.ExtractFunctionOperation"; //$NON-NLS-1$
	
	public static final String CONVERT_FUNCTION_TO_S4_METHOD_REFACTORING_ID= "org.eclipse.statet.r.refactoring.ConvertFunctionToS4MethodOperation"; //$NON-NLS-1$
	
	public static final String CONVERT_FCALL_TO_PIPE_FORWARD_REFACTORING_ID= "org.eclipse.statet.r.refactoring.ConvertFCallToPipeForwardOperation"; //$NON-NLS-1$
	
	public static final String INVERT_IFELSE_REFACTORING_ID= "org.eclipse.statet.r.refactoring.InvertIfElseOperation"; //$NON-NLS-1$
	public static final String CONVERT_IFNOTNULLELSE_TO_SPECIAL_REFACTORING_ID= "org.eclipse.statet.r.refactoring.ConvertIfNotNullElseToSpecialOperation"; //$NON-NLS-1$
	public static final String CONVERT_IFNOTNULLELSE_TO_CIFELSE_REFACTORING_ID= "org.eclipse.statet.r.refactoring.ConvertIfNotNullElseToCIfElseOperation"; //$NON-NLS-1$
	
	public static final String PASTE_CODE_REFACTORING_ID= "org.eclipse.statet.r.refactoring.PasteCodeOperation"; //$NON-NLS-1$
	
	
	public static final String DELETE_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.r.refactoring.DeleteElementsProcessor"; //$NON-NLS-1$
	
	public static final String PASTE_CODE_PROCESSOR_ID= "org.eclipse.statet.r.refactoring.PasteCodeProcessor"; //$NON-NLS-1$
	
	
	private static final CommonRefactoringFactory R_FACTORY= new RRefactoringFactory();
	
	public static CommonRefactoringFactory getFactory() {
		return R_FACTORY;
	}
	
	
	public static final String DELETE_RPKG_DESCR_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.r.refactoring.DeleteRPkgDescrElementsOperation"; //$NON-NLS-1$
	
	public static final String MOVE_RPKG_DESCR_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.r.refactoring.MoveRPkgDescrElementsOperation"; //$NON-NLS-1$
	
	public static final String COPY_RPKG_DESCR_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.r.refactoring.CopyRPkgDescrElementsOperation"; //$NON-NLS-1$
	
	public static final String PASTE_RPKG_DESCR_CODE_REFACTORING_ID= "org.eclipse.statet.r.refactoring.PasteYamlCodeOperation"; //$NON-NLS-1$
	
	public static final String DELETE_RPKG_DESCR_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.r.refactoring.DeleteRPkgDescrElementsProcessor"; //$NON-NLS-1$
	
	public static final String MOVE_RPKG_DESCR_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.r.refactoring.MoveRPkgDescrElementsProcessor"; //$NON-NLS-1$
	
	public static final String COPY_RPKG_DESCR_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.r.refactoring.CopyRPkgDescrElementsProcessor"; //$NON-NLS-1$
	
	public static final String PASTE_RPKG_DESCR_CODE_PROCESSOR_ID= "org.eclipse.statet.r.refactoring.PasteRPkgDescrCodeProcessor"; //$NON-NLS-1$
	
	
	private static final CommonRefactoringFactory RPKG_DESCR_FACTORY= new RPkgDescrRefactoringFactory();
	
	public static CommonRefactoringFactory getRPkgDescrFactory() {
		return RPKG_DESCR_FACTORY;
	}
	
	
}
