/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rlang;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.model.RElementName;


// TODO extend RElement? return as model parent of elements
@NonNullByDefault
public interface RFrame<TModelChild extends RLangElement<?>> {
	
	/** Simple project */
	public static final int PROJECT= 1;
	/** Package project */
	public static final int PACKAGE= 2;
	
	public static final int EXPLICIT= 4;
	
	/** Function frame */
	public static final int FUNCTION= 5;
	/** Class context (not a real frame) */
	public static final int CLASS= 6;
	
	
	public int getFrameType();
//	String getFrameName();
	/** Combination of frametype and name */
	public @Nullable String getFrameId();
	public @Nullable RElementName getElementName();
	
	public boolean hasModelChildren(@Nullable LtkModelElementFilter<? super TModelChild> filter);
	public List<? extends TModelChild> getModelChildren(@Nullable LtkModelElementFilter<? super TModelChild> filter);
	public List<? extends RFrame<?>> getPotentialParents();
	
}
