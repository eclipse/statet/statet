/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rlang;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RFunctionSpec;


/**
 * An R function like element, can be a general function, a generic function or a method
 */
@NonNullByDefault
public interface RLangMethod<TModelChild extends RLangElement>
		extends RLangElement<TModelChild> {
	
	
	/**
	 * Returns the function specification (formal arguments) of this function/method.
	 * 
	 * @return the specification or <code>null</code> if unknown
	 */
	public @Nullable RFunctionSpec getFunctionSpec();
	
}
