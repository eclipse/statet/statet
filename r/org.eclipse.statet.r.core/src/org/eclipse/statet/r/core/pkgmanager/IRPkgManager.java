/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import java.util.List;
import java.util.concurrent.locks.Lock;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.preferences.core.Preference;

import org.eclipse.statet.internal.r.core.pkgmanager.RRepoListPref;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.runtime.RPkgManager;
import org.eclipse.statet.rj.renv.runtime.RPkgManagerDataset;
import org.eclipse.statet.rj.renv.runtime.RuntimeRLibPaths;
import org.eclipse.statet.rj.services.RPlatform;


public interface IRPkgManager extends RPkgManager {
	
	String PREF_QUALIFIER= RCore.BUNDLE_ID + "/r.pkgmanager"; //$NON-NLS-1$
	
	Preference<List<RRepo>> CUSTOM_REPO_PREF= new RRepoListPref(PREF_QUALIFIER, "CustomRepo.list"); //$NON-NLS-1$
	Preference<List<RRepo>> CUSTOM_CRAN_MIRROR_PREF= new RRepoListPref(PREF_QUALIFIER, "CustomCRANMirror.list"); //$NON-NLS-1$
	Preference<List<RRepo>> CUSTOM_BIOC_MIRROR_PREF= new RRepoListPref(PREF_QUALIFIER, "CustomBIOCMirror.list"); //$NON-NLS-1$
	
	
	interface Event {
		
		
		REnv getREnv();
		
		int reposChanged();
		
		int pkgsChanged();
		
		RPkgManagerDataset getOldPkgSet();
		RPkgManagerDataset getNewPkgSet();
		IRPkgChangeSet getInstalledPkgChangeSet();
		
		int viewsChanged();
		
	}
	
	interface Listener {
		
		
		void handleChange(Event event);
		
	}
	
	interface Ext extends IRPkgManager {
		
		
		List<RRepo> getAvailableRepos();
		List<RRepo> getAvailableBioCMirrors();
		List<RRepo> getAvailableCRANMirrors();
		@Nullable RRepo getRepo(String repoId);
		
		RRepoSettings getSelectedRepos();
		void setSelectedRepos(RRepoSettings settings);
		Status getReposStatus(RRepoSettings settings);
		
		IRPkgSet.Ext getExtRPkgSet();
		@Nullable List<? extends RView> getRViews();
		
		void apply(final Tool rTool);
		
		void loadPkgs(final Tool rTool,
				final List<? extends IRPkgInfoAndData> pkgs, final boolean expliciteLocation);
		
	}
	
	
	@Nullable RPlatform getRPlatform();
	
	Lock getReadLock();
	Lock getWriteLock();
	
	void addListener(Listener listener);
	void removeListener(Listener listener);
	
	/**
	 * Returns the R libPath (state of last check).
	 * 
	 * @return the lib path
	 */
	RuntimeRLibPaths getRLibPaths();
	
	IRPkgData addToCache(IFileStore store, IProgressMonitor monitor) throws CoreException;
	void perform(Tool rTool, List<? extends RPkgAction> actions);
	
}
