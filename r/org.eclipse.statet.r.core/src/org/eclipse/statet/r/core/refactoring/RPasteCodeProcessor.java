/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import org.eclipse.statet.ltk.refactoring.core.CommonPasteCodeProcessor;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination;


public class RPasteCodeProcessor extends CommonPasteCodeProcessor {
	
	
	public RPasteCodeProcessor(final String code, final RefactoringDestination destination,
			final RRefactoringAdapter adapter) {
		super(code, destination, adapter);
	}
	
	
	@Override
	public String getIdentifier() {
		return RRefactoring.PASTE_CODE_PROCESSOR_ID;
	}
	
	@Override
	protected String getRefactoringIdentifier() {
		return RRefactoring.PASTE_CODE_REFACTORING_ID;
	}
	
}
