/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§base§ ^ §power§</code>
 */
@NonNullByDefault
public final class Power extends StdBinary {
	
	
	Power() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.POWER;
	}
	
	@Override
	public final RTerminal getOperator(final int index) {
		return RTerminal.POWER;
	}
	
	public final RAstNode getBaseChild() {
		return this.leftExpr.node;
	}
	
	public final RAstNode getExpChild() {
		return this.rightExpr.node;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.POWER == element.getNodeType());
	}
	
	@Override
	public final boolean equalsValue(final RAstNode element) {
		if (NodeType.POWER == element.getNodeType()) {
			final Power other= (Power)element;
			return (this.leftExpr.node.equalsValue(other.leftExpr.node)
					&& this.rightExpr.node.equalsValue(other.rightExpr.node) );
		}
		return false;
	}
	
}
