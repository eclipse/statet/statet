/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§target§ &lt;- §source§</code>
 * <code>§target§ &lt;&lt;- §source§</code>
 * <code>§source§ -&gt; §target§</code>
 * <code>§source§ -&gt;&gt; §target§</code>
 * <code>§target§= §source§</code>
 */
@NonNullByDefault
public abstract class Assignment extends StdBinary {
	
	
	static final class LeftS extends Assignment {
		
		
		LeftS() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.A_LEFT;
		}
		
		@Override
		public final RAstNode getTargetChild() {
			return this.leftExpr.node;
		}
		
		@Override
		public final RAstNode getSourceChild() {
			return this.rightExpr.node;
		}
		
		@Override
		final Expression getTargetExpr() {
			return this.leftExpr;
		}
		
		@Override
		final Expression getSourceExpr() {
			return this.rightExpr;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.ARROW_LEFT_S;
		}
		
		@Override
		public final boolean isSearchOperator() {
			return false;
		}
		
	}
	
	
	static final class LeftD extends Assignment {
		
		
		LeftD() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.A_LEFT;
		}
		
		@Override
		public final RAstNode getTargetChild() {
			return this.leftExpr.node;
		}
		
		@Override
		public final RAstNode getSourceChild() {
			return this.rightExpr.node;
		}
		
		@Override
		final Expression getTargetExpr() {
			return this.leftExpr;
		}
		
		@Override
		final Expression getSourceExpr() {
			return this.rightExpr;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.ARROW_LEFT_D;
		}
		
		@Override
		public final boolean isSearchOperator() {
			return true;
		}
		
	}
	
	
	static final class LeftE extends Assignment {
		
		
		LeftE() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.A_EQUALS;
		}
		
		@Override
		public final RAstNode getTargetChild() {
			return this.leftExpr.node;
		}
		
		@Override
		public final RAstNode getSourceChild() {
			return this.rightExpr.node;
		}
		
		@Override
		final Expression getTargetExpr() {
			return this.leftExpr;
		}
		
		@Override
		final Expression getSourceExpr() {
			return this.rightExpr;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.EQUAL;
		}
		
		@Override
		public final boolean isSearchOperator() {
			return false;
		}
		
	}
	
	
	static final class LeftC extends Assignment {
		
		
		LeftC() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.A_COLON;
		}
		
		@Override
		public final RAstNode getTargetChild() {
			return this.leftExpr.node;
		}
		
		@Override
		public final RAstNode getSourceChild() {
			return this.rightExpr.node;
		}
		
		@Override
		final Expression getTargetExpr() {
			return this.leftExpr;
		}
		
		@Override
		final Expression getSourceExpr() {
			return this.rightExpr;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.COLON_EQUAL;
		}
		
		@Override
		public final boolean isSearchOperator() {
			return false;
		}
		
	}
	
	
	static final class RightS extends Assignment {
		
		
		RightS() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.A_RIGHT;
		}
		
		@Override
		public final RAstNode getTargetChild() {
			return this.rightExpr.node;
		}
		
		@Override
		public final RAstNode getSourceChild() {
			return this.leftExpr.node;
		}
		
		@Override
		final Expression getTargetExpr() {
			return this.rightExpr;
		}
		
		@Override
		final Expression getSourceExpr() {
			return this.leftExpr;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.ARROW_RIGHT_S;
		}
		
		@Override
		public final boolean isSearchOperator() {
			return false;
		}
		
	}
	
	
	static final class RightD extends Assignment {
		
		
		RightD() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.A_RIGHT;
		}
		
		@Override
		public final RAstNode getTargetChild() {
			return this.rightExpr.node;
		}
		
		@Override
		public final RAstNode getSourceChild() {
			return this.leftExpr.node;
		}
		
		@Override
		final Expression getTargetExpr() {
			return this.rightExpr;
		}
		
		@Override
		final Expression getSourceExpr() {
			return this.leftExpr;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.ARROW_RIGHT_D;
		}
		
		@Override
		public final boolean isSearchOperator() {
			return true;
		}
		
	}
	
	
	Assignment() {
	}
	
	
	public abstract RAstNode getTargetChild();
	
	public abstract RAstNode getSourceChild();
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	public abstract boolean isSearchOperator();
	
	abstract Expression getTargetExpr();
	
	abstract Expression getSourceExpr();
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		if (element instanceof final Assignment other) {
			final RAstNode thisTarget= getTargetExpr().node;
			final RAstNode otherTarget= other.getTargetExpr().node;
			return ((isSearchOperator() == other.isSearchOperator())
					&& ((thisTarget == otherTarget)
							|| (thisTarget != null && thisTarget.equalsSingle(otherTarget)) )
					);
		}
		return false;
	}
	
	@Override
	public final boolean equalsValue(final RAstNode element) {
		if (getOperator(0) == element.getOperator(0)) {
			final Assignment other= (Assignment)element;
			return (this.leftExpr.node.equalsValue(other.leftExpr.node)
					&& this.rightExpr.node.equalsValue(other.rightExpr.node) );
		}
		return false;
	}
	
}
