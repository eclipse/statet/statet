/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.util;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;

import static org.eclipse.statet.r.core.source.doc.RDocumentConstants.RDOC_DEFAULT_CONTENT_CONSTRAINT;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.CharPairSet;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.text.core.BasicCharPairMatcher;
import org.eclipse.statet.ltk.text.core.HeuristicTokenScanner;
import org.eclipse.statet.r.core.source.doc.RdDocumentContentInfo;


@NonNullByDefault
public class RdBracketPairMatcher extends BasicCharPairMatcher {
	
	
	private static final CharPairSet BRACKETS= new CharPairSet(
			ImCollections.newIdentityList(CURLY_BRACKETS),
			'\\' );
	
	
	public RdBracketPairMatcher(final DocContentSections documentContentInfo) {
		super(BRACKETS,
				documentContentInfo.getPartitioning(), RDOC_DEFAULT_CONTENT_CONSTRAINT,
				new HeuristicTokenScanner(documentContentInfo, RDOC_DEFAULT_CONTENT_CONSTRAINT) );
	}
	
	public RdBracketPairMatcher() {
		this(RdDocumentContentInfo.INSTANCE);
	}
	
	
}
