/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;
import static org.eclipse.statet.r.core.source.RTerminal.isHWhitespace;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.osgi.util.NLS;
import org.eclipse.text.edits.RangeMarker;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.refactoring.Messages;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.util.SourceMessageUtil;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringDescriptor;
import org.eclipse.statet.ltk.refactoring.core.QuickRefactoring;
import org.eclipse.statet.ltk.refactoring.core.RefactoringChange;
import org.eclipse.statet.ltk.refactoring.core.RefactoringMessages;
import org.eclipse.statet.ltk.refactoring.core.SourceUnitChange;
import org.eclipse.statet.ltk.refactoring.core.TextChangeCompatibility;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.source.RSourceConstants;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
public class FCallToPipeForwardRefactoring extends QuickRefactoring {
	
	
	private final RRefactoringAdapter adapter= new RRefactoringAdapter();
	private final ElementSet elementSet;
	
	private final RSourceUnit sourceUnit;
	
	private final FCall fCall;
	private final int inputArgIndex;
	private @Nullable Boolean usePlaceholder;
	
	
	public FCallToPipeForwardRefactoring(final RSourceUnit su, final FCall fCall,
			final FCall.Arg arg) {
		this.sourceUnit= su;
		this.elementSet= new ElementSet(su);
		
		this.fCall= fCall;
		this.inputArgIndex= fCall.getArgsChild().getChildIndex(arg);
		this.usePlaceholder= Boolean.TRUE;
	}
	
	public FCallToPipeForwardRefactoring(final RSourceUnit su, final FCall fCall) {
		this.sourceUnit= su;
		this.elementSet= new ElementSet(su);
		
		this.fCall= fCall;
		this.inputArgIndex= 0;
		this.usePlaceholder= null;
	}
	
	
	@Override
	public String getBundleId() {
		return RCore.BUNDLE_ID;
	}
	
	@Override
	public String getIdentifier() {
		return RRefactoring.CONVERT_FCALL_TO_PIPE_FORWARD_REFACTORING_ID;
	}
	
	@Override
	public String getName() {
		return Messages.FCallToPipeForwardMethod_label;
	}
	
	
	@Override
	public RefactoringStatus checkInitialConditions(
			final @Nullable IProgressMonitor monitor) {
		final SubMonitor m= SubMonitor.convert(monitor, 1);
		try {
			final var rCoreAccess= RCore.getContextAccess(this.sourceUnit);
			final var fCall= this.fCall;
			final var args= fCall.getArgsChild();
			if (args.getChildCount() == 0) {
				return RefactoringStatus.createFatalErrorStatus(Messages.FCallToPipeForwardMethod_error_MissingArg_message);
			}
			final var inputArg= args.getChild(this.inputArgIndex);
			if (!inputArg.hasValue()) {
				return RefactoringStatus.createFatalErrorStatus(Messages.FCallToPipeForwardMethod_error_MissingArgValue_message);
			}
			if (this.usePlaceholder == null) {
				this.usePlaceholder= (inputArg.hasName()
						&& (rCoreAccess.getRSourceConfig().getLangVersion().compareTo(RSourceConstants.LANG_VERSION_4_2) >= 0) );
			}
			
			final RefactoringStatus result= new RefactoringStatus();
			this.adapter.checkInitialToModify(result, this.elementSet);
			
			if (result.hasFatalError()) {
				return result;
			}
			
			return result;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public RefactoringStatus checkFinalConditions(
			final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_FinalCheck_label, 3);
		try {
			final RefactoringStatus status= new RefactoringStatus();
			this.adapter.checkFinalToModify(status, this.elementSet, m.newChild(2));
			return status;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public Change createChange(
			final IProgressMonitor monitor) throws CoreException, OperationCanceledException {
				final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_CreateChanges_label, 3);
		try {
			final var textFileChange= new SourceUnitChange(this.sourceUnit);
			if (this.sourceUnit.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
				textFileChange.setSaveMode(TextFileChange.LEAVE_DIRTY);
			}
			createChanges(textFileChange, m.newChild(2));
			
			textFileChange.setInsertPosition(new Position(this.fCall.getStartOffset(), 0));
			
			final Map<String, String> arguments= new HashMap<>();
			
			final var messageUtil= new SourceMessageUtil(this.sourceUnit.getContent(m));
			final String description= NLS.bind(Messages.FCallToPipeForwardMethod_Descriptor_description,
					messageUtil.getFullQuoteText(this.fCall.getRefChild()),
					messageUtil.getFullQuoteText(this.fCall.getArgsChild().getChild(this.inputArgIndex).getValueChild() ));
			final IProject resource= this.elementSet.getSingleProject();
			final String project= (resource != null) ? resource.getName() : null;
			final String source= (project != null) ?
					NLS.bind(RefactoringMessages.Common_Source_Project_label, project) :
					RefactoringMessages.Common_Source_Workspace_label;
			final int flags= 0;
			final String comment= ""; //$NON-NLS-1$
			final CommonRefactoringDescriptor descriptor= new CommonRefactoringDescriptor(
					getIdentifier(), project, description, comment, arguments, flags);
			
			return new RefactoringChange(descriptor, getName(),
					new Change[] { textFileChange } );
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public SourceUnitChange createTextChange(
			final SubMonitor m) throws CoreException {
		m.setWorkRemaining(2);
		try {
			final var textFileChange= new SourceUnitChange(this.sourceUnit);
			
			createChanges(textFileChange, m.newChild(2));
			
			return textFileChange;
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
	}
	
	private void createChanges(final SourceUnitChange change,
			final SubMonitor m) throws BadLocationException, CoreException {
		m.setWorkRemaining(3 + 2 * 4);
		
		this.sourceUnit.connect(m.newChild(1));
		try {
			final var util= new RSourceCodeModifyUtil(this.adapter, this.sourceUnit, m.newChild(1));
			final var document= util.getSourceDocument();
			
			final var fCall= this.fCall;
			final var fCallArgs= fCall.getArgsChild();
			final var inputArg= fCallArgs.getChild(this.inputArgIndex);
			
			final RAstNode valueNode= nonNullAssert(inputArg.getValueChild());
			final int inputStart= valueNode.getStartOffset();
			final int inputEnd= valueNode.getEndOffset();
			final int delStart= inputArg.getStartOffset();
			int sepStart= NA_OFFSET;
			int sepEnd= NA_OFFSET;
			if (this.inputArgIndex < fCallArgs.getSeparatorOffsets().size()) {
				sepEnd= sepStart= fCallArgs.getSeparatorOffsets().getAt(this.inputArgIndex);
				if (sepEnd != NA_OFFSET) {
					sepEnd++;
					if (sepEnd < document.getLength()
							&& isHWhitespace(document.getChar(sepEnd))
							&& (sepEnd + 1 == document.getLength()
							|| document.getChar(sepEnd + 1) != '#' )) {
						sepEnd++;
					}
				}
			}
			else {
				sepEnd= sepStart= fCall.getArgsCloseOffset();
			}
			if (sepEnd == NA_OFFSET) {
				sepEnd= sepStart= inputArg.getEndOffset();
			}
			
			{	util.clear();
				util.append(util.getSource(inputStart, inputEnd),
						(valueNode.getNodeType().opPrec > NodeType.PIPE_FORWARD.opPrec) );
				util.appendOperator(RTerminal.PIPE_RIGHT);
				
				TextChangeCompatibility.addTextEdit(change, Messages.FCallToPipeForwardMethod_Changes_DeleteOld_name,
						util.createInsertEdit(fCall.getStartOffset()) );
				m.worked(4);
			}
			{	util.clear();
				if (nonNullAssert(this.usePlaceholder)) {
					util.append(RTerminal.S_UNDERSCORE);
					
					TextChangeCompatibility.addTextEdit(change, Messages.FCallToPipeForwardMethod_Changes_DeleteOld_name,
							util.createReplaceEdit(inputStart, inputEnd) );
				}
				else {
					if (inputEnd < sepStart) {
						TextChangeCompatibility.addTextEdit(change, Messages.FCallToPipeForwardMethod_Changes_DeleteOld_name,
								util.createDeleteEdit(delStart, inputEnd) );
						if (sepStart < sepEnd) {
							TextChangeCompatibility.addTextEdit(change, Messages.FCallToPipeForwardMethod_Changes_DeleteOld_name,
									util.createDeleteEdit(sepStart, sepEnd) );
						}
					}
					else {
						TextChangeCompatibility.addTextEdit(change, Messages.FCallToPipeForwardMethod_Changes_DeleteOld_name,
								util.createDeleteEdit(delStart, sepEnd) );
					}
				}
				m.worked(4);
			}
			
			TextChangeCompatibility.addMarker(change, new RangeMarker(fCall.getEndOffset(), 0));
		}
		finally {
			this.sourceUnit.disconnect(m.newChild(1));
		}
	}
	
}
