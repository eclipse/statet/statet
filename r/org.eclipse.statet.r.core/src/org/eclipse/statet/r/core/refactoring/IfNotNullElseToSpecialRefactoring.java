/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.osgi.util.NLS;
import org.eclipse.text.edits.RangeMarker;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.refactoring.Messages;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.util.SourceMessageUtil;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringDescriptor;
import org.eclipse.statet.ltk.refactoring.core.QuickRefactoring;
import org.eclipse.statet.ltk.refactoring.core.RefactoringChange;
import org.eclipse.statet.ltk.refactoring.core.RefactoringMessages;
import org.eclipse.statet.ltk.refactoring.core.SourceUnitChange;
import org.eclipse.statet.ltk.refactoring.core.TextChangeCompatibility;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RCoreFunctions;
import org.eclipse.statet.r.core.model.RModelUtils;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.CIfElse;
import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;


@NonNullByDefault
public class IfNotNullElseToSpecialRefactoring extends QuickRefactoring {
	
	
	private final RRefactoringAdapter adapter= new RRefactoringAdapter();
	private final ElementSet elementSet;
	
	private final RSourceUnit sourceUnit;
	
	private final CIfElse sourceNode;
	private @Nullable RAstNode primaryNode;
	private @Nullable RAstNode elseNode;
	
	
	public IfNotNullElseToSpecialRefactoring(final RSourceUnit su, final CIfElse sourceNode) {
		this.sourceUnit= su;
		this.elementSet= new ElementSet(su);
		
		this.sourceNode= sourceNode;
	}
	
	
	@Override
	public String getBundleId() {
		return RCore.BUNDLE_ID;
	}
	
	@Override
	public String getIdentifier() {
		return RRefactoring.CONVERT_IFNOTNULLELSE_TO_SPECIAL_REFACTORING_ID;
	}
	
	@Override
	public String getName() {
		return Messages.IfNotNullElseToSpecial_label;
	}
	
	
	@Override
	public RefactoringStatus checkInitialConditions(
			final @Nullable IProgressMonitor monitor) {
		final SubMonitor m= SubMonitor.convert(monitor, 1);
		try {
			final var rCoreAccess= RCore.getContextAccess(this.sourceUnit);
			final CIfElse sourceNode= this.sourceNode;
			RAstNode condNode= sourceNode.getCondChild();
			boolean not= false;
			if (condNode.getNodeType() == NodeType.NOT) {
				not= true;
				condNode= condNode.getChild(0);
			}
			final FCall fCall;
			if (condNode.getNodeType() == NodeType.F_CALL
					&& RModelUtils.matches((fCall= (FCall)condNode), RCoreFunctions.BASE_IsNull_ELEMENT_NAME)
					&& sourceNode.hasElse() ) {
				final RAstNode primaryNode= RAsts.matchArgs(fCall,
								RCoreFunctions.getDefinitions(rCoreAccess.getRSourceConfig()).BASE_IsNull_fSpec )
						.getArgValueNode(0);
				RAstNode matchNode;
				RAstNode elseNode;
				if (not) {
					matchNode= sourceNode.getThenChild();
					elseNode= sourceNode.getElseChild();
				}
				else {
					matchNode= sourceNode.getElseChild();
					elseNode= sourceNode.getThenChild();
				}
				if (primaryNode != null && (primaryNode.equalsValue(matchNode)
						|| (matchNode.getNodeType() == NodeType.BLOCK && matchNode.getChildCount() == 1
								&& primaryNode.equalsValue(matchNode.getChild(0)) ))) {
					if (elseNode.getNodeType() == NodeType.BLOCK && elseNode.getChildCount() == 1) {
						elseNode= elseNode.getChild(0);
					}
					this.primaryNode= primaryNode;
					this.elseNode= elseNode;
				}
			}
			if (this.primaryNode == null || this.elseNode == null) {
				return RefactoringStatus.createFatalErrorStatus("Cannot convert the statement.");
			}
			
			final RefactoringStatus result= new RefactoringStatus();
			this.adapter.checkInitialToModify(result, this.elementSet);
			
			if (result.hasFatalError()) {
				return result;
			}
			
			return result;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public RefactoringStatus checkFinalConditions(
			final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_FinalCheck_label, 3);
		try {
			final RefactoringStatus status= new RefactoringStatus();
			this.adapter.checkFinalToModify(status, this.elementSet, m.newChild(2));
			return status;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public Change createChange(
			final IProgressMonitor monitor) throws CoreException, OperationCanceledException {
				final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_CreateChanges_label, 3);
		try {
			final var textFileChange= new SourceUnitChange(this.sourceUnit);
			if (this.sourceUnit.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
				textFileChange.setSaveMode(TextFileChange.LEAVE_DIRTY);
			}
			createChanges(textFileChange, m.newChild(2));
			
			textFileChange.setInsertPosition(new Position(this.sourceNode.getStartOffset(), 0));
			
			final Map<String, String> arguments= new HashMap<>();
			
			final var messageUtil= new SourceMessageUtil(this.sourceUnit.getContent(m));
			final String description= NLS.bind(Messages.IfNotNullElseToSpecial_Descriptor_description,
					messageUtil.getFullQuoteText(this.primaryNode) );
			final IProject resource= this.elementSet.getSingleProject();
			final String project= (resource != null) ? resource.getName() : null;
			final String source= (project != null) ?
					NLS.bind(RefactoringMessages.Common_Source_Project_label, project) :
					RefactoringMessages.Common_Source_Workspace_label;
			final int flags= 0;
			final String comment= ""; //$NON-NLS-1$
			final CommonRefactoringDescriptor descriptor= new CommonRefactoringDescriptor(
					getIdentifier(), project, description, comment, arguments, flags);
			
			return new RefactoringChange(descriptor, getName(),
					new Change[] { textFileChange } );
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public SourceUnitChange createTextChange(
			final SubMonitor m) throws CoreException {
		m.setWorkRemaining(2);
		try {
			final var textFileChange= new SourceUnitChange(this.sourceUnit);
			
			createChanges(textFileChange, m.newChild(2));
			
			return textFileChange;
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
	}
	
	private void createChanges(final SourceUnitChange change,
			final SubMonitor m) throws BadLocationException, CoreException {
		m.setWorkRemaining(3 + 1 * 4);
		
		this.sourceUnit.connect(m.newChild(1));
		try {
			final var util= new RSourceCodeModifyUtil(this.adapter, this.sourceUnit, m.newChild(1));
			
			final CIfElse sourceNode= this.sourceNode;
			final var primaryNode= nonNullAssert(this.primaryNode);
			final var elseNode= nonNullAssert(this.elseNode);
			final int changeStart= sourceNode.getStartOffset();
			final int changeEnd= sourceNode.getEndOffset();
			
			{	util.clear();
				util.append(primaryNode,
						(primaryNode.getNodeType().opPrec > NodeType.SPECIAL.opPrec) );
				util.appendOperator(RTerminal.S_SPECIAL_IFNOTNULLELSE);
				util.append(elseNode,
						(elseNode.getNodeType().opPrec >= NodeType.SPECIAL.opPrec
								&& !(elseNode.getNodeType() == NodeType.SPECIAL && elseNode.getText() == "||") ));
				
				final var parentNode= sourceNode.getRParent();
				if (parentNode != null && parentNode.getNodeType().opPrec <= NodeType.SPECIAL.opPrec
						&& !(parentNode.getNodeType() == NodeType.SPECIAL && parentNode.getText() == "||") ) {
					util.insert(0, '(');
					util.append(')');
				}
				
				TextChangeCompatibility.addTextEdit(change, Messages.IfNotNullElse_Changes_name,
						util.createReplaceEdit(changeStart, changeEnd) );
				m.worked(4);
			}
			
			TextChangeCompatibility.addMarker(change, new RangeMarker(changeEnd, 0));
		}
		finally {
			this.sourceUnit.disconnect(m.newChild(1));
		}
	}
	
}
