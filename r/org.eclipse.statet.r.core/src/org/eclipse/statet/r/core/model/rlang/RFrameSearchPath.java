/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rlang;

import java.util.List;
import java.util.NoSuchElementException;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RFrameSearchPath extends Iterable<RFrame<?>> {
	
	
	public static class Iterator implements java.util.Iterator<RFrame<?>> {
		
		private final @NonNull List<RFrame<?>>[] frames;
		
		private int listIter0;
		private int listIter1= -1;
		private @Nullable RFrame<?> next;
		
		
		public Iterator(final @NonNull List<RFrame<?>>[] frames) {
			this.frames= frames;
		}
		
		
		@Override
		public boolean hasNext() {
			if (this.next != null) {
				return true;
			}
			ITER_0 : while (this.listIter0 < this.frames.length) {
				if (++this.listIter1 < this.frames[this.listIter0].size()) {
					this.next= this.frames[this.listIter0].get(this.listIter1);
					return true;
				}
				else {
					this.listIter0++;
					this.listIter1= -1;
					continue ITER_0;
				}
			}
			return false;
		}
		
		@Override
		public RFrame<?> next() {
			if (hasNext()) {
				final @NonNull RFrame<?> frame= this.next;
				this.next= null;
				return frame;
			}
			throw new NoSuchElementException();
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
		public int getEnvirGroup() {
			return this.listIter0;
		}
		
		protected int getPosInGroup() {
			return this.listIter1;
		}
		
		public int getRelevance() {
			return this.frames.length - getEnvirGroup() - 1;
		}
		
	}
	
	
	@Override
	public Iterator iterator();
	
}
