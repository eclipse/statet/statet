/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>&</code>
 * <code>|</code>
 */
@NonNullByDefault
public abstract class Logical extends StdBinary {
	
	
	static final class Or extends Logical {
		
		
		Or() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.OR;
		}
		
		@Override
		public RTerminal getOperator(final int index) {
			return RTerminal.OR;
		}
		
	}
	
	static final class OrD extends Logical {
		
		
		OrD() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.OR;
		}
		
		@Override
		public RTerminal getOperator(final int index) {
			return RTerminal.OR_D;
		}
		
	}
	
	static final class And extends Logical {
		
		
		And() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.AND;
		}
		
		@Override
		public RTerminal getOperator(final int index) {
			return RTerminal.AND;
		}
		
	}
	
	static final class AndD extends Logical {
		
		
		AndD() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.AND;
		}
		
		@Override
		public RTerminal getOperator(final int index) {
			return RTerminal.AND_D;
		}
		
	}
	
	
	protected Logical() {
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (getNodeType() == element.getNodeType());
	}
	
}
