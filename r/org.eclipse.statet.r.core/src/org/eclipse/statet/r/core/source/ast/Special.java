/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.source.RTerminal;


@NonNullByDefault
public final class Special extends StdBinary {
	
	
	@Nullable String qualifier;
	
	
	Special() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.SPECIAL;
	}
	
	@Override
	public final RTerminal getOperator(final int index) {
		return RTerminal.SPECIAL;
	}
	
	@Override
	public @Nullable String getText() {
		return this.qualifier;
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.SPECIAL == element.getNodeType());
	}
	
	@Override
	public boolean equalsValue(final RAstNode element) {
		if (NodeType.SPECIAL == element.getNodeType()) {
			final Special other= (Special)element;
			return (Objects.equals(this.qualifier, other.qualifier)
					&& this.leftExpr.node.equalsValue(other.leftExpr.node)
					&& this.rightExpr.node.equalsValue(other.rightExpr.node) );
		}
		return false;
	}
	
}
