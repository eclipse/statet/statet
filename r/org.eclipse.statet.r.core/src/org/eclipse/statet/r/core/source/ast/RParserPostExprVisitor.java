/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.ltk.core.StatusCodes.CTX12;
import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE12;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE1_OK;
import static org.eclipse.statet.ltk.core.StatusCodes.WARNING;
import static org.eclipse.statet.ltk.core.StatusCodes.WARNING_IN_CHILD;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_PIPE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TOKEN_MULTI_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TOKEN_UNNAMED_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION;
import static org.eclipse.statet.r.core.source.ast.RParser.LANG_VERSION_4_1;
import static org.eclipse.statet.r.core.source.ast.RParser.LANG_VERSION_4_2;
import static org.eclipse.statet.r.core.source.ast.RParser.LANG_VERSION_4_3;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * update offsets in expressions
 * not for control statements or function def/calls
 */
@NonNullByDefault
/* package */ final class RParserPostExprVisitor extends RAstVisitor {
	
	private static final int ERROR_MASK= (ERROR | ERROR_IN_CHILD);
	private static final int WARNING_MASK= (WARNING | WARNING_IN_CHILD);
	
	
	public static void updateParentStatus(final RAstNode node) {
		final var rParent= node.rParent;
		if (rParent == null || (rParent.getStatusCode() & ERROR_IN_CHILD) != 0) {
			return;
		}
		final int statusCode= node.getStatusCode();
		if ((statusCode & ERROR_MASK) != 0) {
			rParent.setStatusSeverityInChild(ERROR_IN_CHILD);
		}
		else if ((statusCode & WARNING_MASK) != 0) {
			rParent.setStatusSeverityInChild(WARNING_IN_CHILD);
		}
	}
	
	
	private final RParser parser;
	
	private int severityInChild;
	
	
	public RParserPostExprVisitor(final RParser parser) {
		this.parser= parser;
	}
	
	
	public int check(final RAstNode node) {
		this.severityInChild= 0;
		try {
			node.acceptInR(this);
		}
		catch (final InvocationTargetException e) {
			// not used
		}
		return this.severityInChild;
	}
	
	private void updateSeverityInChild(final RAstNode node) {
		if (this.severityInChild == ERROR_IN_CHILD) {
			return;
		}
		final int statusCode= node.getStatusCode();
		if ((statusCode & ERROR_MASK) != 0) {
			this.severityInChild= ERROR_IN_CHILD;
		}
		else if ((statusCode & WARNING_MASK) != 0) {
			this.severityInChild= WARNING_IN_CHILD;
		}
	}
	
	private void doAcceptIn(final RAstNode child) throws InvocationTargetException {
		final var savedSeverity= this.severityInChild;
		this.severityInChild= 0;
		child.acceptInR(this);
		final var severityInChild= this.severityInChild;
		if (severityInChild != 0) {
			child.rParent.setStatusSeverityInChild(severityInChild);
			this.severityInChild= Math.max(savedSeverity, severityInChild);
		}
		else {
			this.severityInChild= savedSeverity;
		}
	}
	
	private void doAccecptInChildren(final RAstNode node) throws InvocationTargetException {
		final var savedSeverity= this.severityInChild;
		this.severityInChild= 0;
		node.acceptInRChildren(this);
		final var severityInChild= this.severityInChild;
		if (severityInChild != 0) {
			node.setStatusSeverityInChild(severityInChild);
			this.severityInChild= Math.max(savedSeverity, severityInChild);
		}
		else {
			this.severityInChild= savedSeverity;
		}
	}
	
	private void markSubsequentIfStatus12(final RAstNode node, final int statusCode) {
		if ((node.getStatusCode() & (TYPE12 | CTX12)) == statusCode) {
			node.setStatusSubsequent();
		}
	}
	
	
	@Override
	public void visit(final SourceComponent node) {
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Block node) {
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Group node) {
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final CIfElse node) throws InvocationTargetException {
		if (node.withElse) {
			doAcceptIn(node.elseExpr.node);
		}
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final CForLoop node) throws InvocationTargetException {
		doAcceptIn(node.loopExpr.node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final CRepeatLoop node) throws InvocationTargetException {
		doAcceptIn(node.loopExpr.node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final CWhileLoop node) throws InvocationTargetException {
		doAcceptIn(node.loopExpr.node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final FCall node) throws InvocationTargetException {
		doAcceptIn(node.refExpr.node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final FCall.Args node) {
//		throw new IllegalStateException();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final FCall.Arg node) {
//		throw new IllegalStateException();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final FDef node) throws InvocationTargetException {
		doAcceptIn(node.expr.node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final FDef.Args node) {
//		throw new IllegalStateException();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final FDef.Arg node) {
//		throw new IllegalStateException();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Assignment node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Pipe node) throws InvocationTargetException {
		final RAstNode target= node.getTargetChild();
		boolean isTargetValid= false;
		final RAstNode subRef;
		final CallArgs callArgs;
		switch (target.getNodeType()) {
		case F_CALL:
			isTargetValid= true;
			subRef= null;
			callArgs= ((FCall)target).getArgsChild();
			break;
		case SUB_INDEXED_S:
		case SUB_INDEXED_D:
			subRef= ((SubIndexed)target).getRefChild();
			callArgs= ((SubIndexed)target).getArgsChild();
			break;
		case SUB_NAMED_PART:
		case SUB_NAMED_SLOT:
			subRef= ((SubNamed)target).getRefChild();
			callArgs= null;
			break;
		default:
			subRef= null;
			callArgs= null;
			break;
		}
		
		Placeholder placeholder0= null;
		if (subRef != null) {
			if (subRef.getNodeType() == NodeType.PLACEHOLDER
					&& subRef.getOperator(0) == RTerminal.PIPE_PLACEHOLDER ) {
				if (this.parser.getLangVersion() >= LANG_VERSION_4_3) {
					isTargetValid= true;
					placeholder0= (Placeholder)subRef;
					subRef.setStatus(TYPE1_OK);
				}
				else if (this.parser.getLangVersion() < LANG_VERSION_4_1) {
					isTargetValid= true;
					markSubsequentIfStatus12(subRef, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE);
				}
			}
		}
		if (callArgs != null) {
			if (this.parser.getLangVersion() >= LANG_VERSION_4_2) {
				for (final var callArg : callArgs.getArgChildren()) {
					final RAstNode argValue= callArg.getValueChild();
					if (argValue != null
							&& argValue.getNodeType() == NodeType.PLACEHOLDER
							&& argValue.getOperator(0) == RTerminal.PIPE_PLACEHOLDER ) {
						if (placeholder0 != null) {
							argValue.setStatus(TYPE123_SYNTAX_TOKEN_MULTI_UNEXPECTED | CTX12_PIPE);
						}
						else {
							placeholder0= (Placeholder)argValue;
							isTargetValid= true;
							if (!callArg.hasName()) {
								argValue.setStatus(TYPE123_SYNTAX_TOKEN_UNNAMED_UNEXPECTED | CTX12_PIPE);
							}
							else {
								argValue.setStatus(TYPE1_OK);
								callArg.clearStatusSeverityInChild();
								updateParentStatus(callArg.getNameChild());
							}
						}
					}
				}
				if (placeholder0 != null) {
					callArgs.clearStatusSeverityInChild();
					for (final var callArg : callArgs.getArgChildren()) {
						updateParentStatus(callArg);
					}
					target.clearStatusSeverityInChild();
					updateParentStatus(callArgs);
				}
			}
			else if (this.parser.getLangVersion() < LANG_VERSION_4_1) {
				for (final var callArg : callArgs.getArgChildren()) {
					final RAstNode argValue= callArg.getValueChild();
					if (argValue != null
							&& argValue.getNodeType() == NodeType.PLACEHOLDER
							&& argValue.getOperator(0) == RTerminal.PIPE_PLACEHOLDER ) {
						markSubsequentIfStatus12(argValue, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE);
					}
				}
			}
		}
		
		if (this.parser.getLangVersion() >= LANG_VERSION_4_1) {
			if (!isTargetValid
					&& (target.getStatusCode() & (TYPE12 | CTX12)) != (TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | CTX12_PIPE) ) {
				target.setStatusThis(TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE);
			}
		}
		else {
			markSubsequentIfStatus12(node.getSourceChild(), TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING | CTX12_PIPE);
			markSubsequentIfStatus12(target, TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | CTX12_PIPE);
			markSubsequentIfStatus12(target, TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE);
		}
		if ((this.parser.getAstLevel() & AstInfo.DEFAULT_LEVEL_MASK) >= AstInfo.LEVEL_MODEL_DEFAULT
				&& placeholder0 != null) {
			placeholder0.addAttachment(new PlaceholderDetail(placeholder0, node.getSourceChild()));
		}
		
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	
	@Override
	public void visit(final Model node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Relational node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Logical node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Arithmetic node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Power node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Seq node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Special node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Sign node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final SubIndexed node) throws InvocationTargetException {
		doAcceptIn(node.expr.node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final SubIndexed.Args node) {
//		throw new IllegalStateException();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final SubIndexed.Arg node) {
//		throw new IllegalStateException();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final SubNamed node) throws InvocationTargetException {
		doAcceptIn(node.expr.node);
		// name by scanner
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final NSGet node) throws InvocationTargetException {
		// children by scanner
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Symbol node) {
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Placeholder node) {
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final StringConst node) {
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final NumberConst node) {
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Help node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
	@Override
	public void visit(final Dummy node) throws InvocationTargetException {
		doAccecptInChildren(node);
		node.updateOffsets();
		updateSeverityInChild(node);
	}
	
/*	
	@Override
	public void visit(CLoopCommand node) throws InvocationTargetException {
	}
	
	@Override
	public void visit(NullConst node) throws InvocationTargetException {
	}
*/	
/*	
	public void visit(final Comment node) throws InvocationTargetException {
	}
	
	public void visit(final DocuComment node) throws InvocationTargetException {
		node.acceptInRChildren(this);
	}
	
	public void visit(final DocuTag node) throws InvocationTargetException {
	}
	
	public void visit(final DocuText node) throws InvocationTargetException {
	}
*/
	
}
