/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.r.core.model.rlang.RChunkSrcStrElement;
import org.eclipse.statet.r.core.model.rlang.RSrcFrame;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public interface RModelManager extends ModelManager {
	
	
	/**
	 * Returns the frame for the project scope containing all main element definition in the
	 * project.
	 * 
	 * @param rProject the R project
	 * @return the R frame
	 * @throws CoreException
	 */
	@Nullable RSrcFrame<?> getProjectFrame(final RProject rProject) throws CoreException;
	
	/**
	 * Returns the package names of R package projects in the workspace.
	 * 
	 * @return set with all package names
	 */
	Set<String> getPkgNames();
	
	@Nullable RSrcFrame<?> getPkgProjectFrame(final String pkgName) throws CoreException;
	
	
	RSourceUnitModelInfo reconcile(final RSourceUnit su, final SourceUnitModelInfo modelInfo,
			final List<? extends RChunkSrcStrElement> chunks, List<? extends SourceComponent> inlineNodes,
			final int level, final IProgressMonitor monitor);
	
	/**
	 * Find source units with references to the specified name in the specified project.
	 * 
	 * Note: The returned source units are already {@link SourceUnit#connect(IProgressMonitor) connected}.
	 * The caller is responsible to disconnect, if they are no longer required.
	 * 
	 * @param rProject the R project
	 * @param name the name of the R element
	 * @param monitor
	 * @return list of referencing source units
	 * @throws CoreException
	 */
	@Nullable List<SourceUnit> findReferencingSourceUnits(final RProject rProject, final RElementName name,
			final IProgressMonitor monitor) throws CoreException;
	
	
}
