/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.rlang;

import java.util.Arrays;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * Provides util-method for tokens of the R-language.
 * 'Tokes' means tokens according to R-language definition, they defines not
 * directly <code>Token</code>, implementions of <code>IToken</code>.
 */
@NonNullByDefault
public final class RTokens {
	
	
	public static final ImList<String> CONSTANT_WORDS= ImCollections.newList(
			RTerminal.S_NULL,
			RTerminal.S_NA,
			RTerminal.S_NA_REAL,
			RTerminal.S_NA_INT,
			RTerminal.S_NA_CPLX,
			RTerminal.S_NA_CHAR,
			RTerminal.S_INF,
			RTerminal.S_NAN,
			RTerminal.S_TRUE,
			RTerminal.S_FALSE );
	
	public static final ImList<String> FLOWCONTROL_WORDS= ImCollections.newList(
			RTerminal.S_IF,
			RTerminal.S_ELSE,
			RTerminal.S_FOR,
			RTerminal.S_IN,
			RTerminal.S_WHILE,
			RTerminal.S_REPEAT,
			RTerminal.S_NEXT,
			RTerminal.S_BREAK,
			RTerminal.S_FUNCTION );
	
	public static final ImList<String> COMMON_SPECIAL_OPERATORS= ImCollections.newList(
			RTerminal.S_SPECIAL_MODULO, RTerminal.S_SPECIAL_INTDIV,
			RTerminal.S_SPECIAL_MATRIXPROD, RTerminal.S_SPECIAL_OUTERPROD, RTerminal.S_SPECIAL_KRONECKERPROD,
			"%in%" ); //$NON-NLS-1$
	public static final ImList<String> COMMON_SPECIAL_QUALIFIERS= COMMON_SPECIAL_OPERATORS
			.map((s) -> s.substring(1, s.length() - 1).intern())
			.toList();
	public static final ImList<String> FLOWCONTROL_SPECIAL_OPERATORS= ImCollections.newList(
			RTerminal.S_SPECIAL_IFNOTNULLELSE );
	public static final ImList<String> FLOWCONTROL_SPECIAL_QUALIFIERS= FLOWCONTROL_SPECIAL_OPERATORS
			.map((s) -> s.substring(1, s.length() - 1).intern())
			.toList();
	
	
	// ----
	
	private static final char[] SEPARATOR_CHARS= {
			'!', '$', '%', '&', '(', ')',
			'*', '+', ',', '-', '/',
			':', ';' , '<', '=', '>',
			'[', ']', '^', '{', '|', '}', '~',
			'@',
	};
	
	private static final char[] WHITESPACE_CHARS= { // on changes, note methods below
			' ', '\t',
	};
	
	private static final char[] NEWLINE_CHARS= {
			'\r', '\n',
	};
	
	private static boolean[] SEPARATOR_MAP;
	
	static {
		SEPARATOR_MAP= new boolean[128];
		Arrays.fill(SEPARATOR_MAP, false);
		for (int i= 0; i < RTokens.SEPARATOR_CHARS.length; i++) {
			SEPARATOR_MAP[RTokens.SEPARATOR_CHARS[i]]= true;
		}
		for (int i= 0; i < RTokens.WHITESPACE_CHARS.length; i++) {
			SEPARATOR_MAP[RTokens.WHITESPACE_CHARS[i]]= true;
		}
		for (int i= 0; i < RTokens.NEWLINE_CHARS.length; i++) {
			SEPARATOR_MAP[RTokens.NEWLINE_CHARS[i]]= true;
		}
	}
	
	public static boolean isSeparator(final int c) {
		return (c < 0   // EOF
				|| (c <= 127 && SEPARATOR_MAP[c]) );
	}
	
	public static boolean isRobustSeparator(final int c, final boolean isDotSeparator) {
		switch (c) {
		case '.':
			return isDotSeparator;
		case '_':
			return false;
		default:
			return !Character.isLetterOrDigit(c);
		}
	}
	
	public static boolean isRobustSeparator(final int c) {
		switch (c) {
		case '.':
		case '_':
			return false;
		default:
			return !Character.isLetterOrDigit(c);
		}
	}
	
	public static boolean isDigit(final int c) {
		return (c >= 0x30 && c <= 0x39);
	}
	
	public static boolean isHexDigit(final int c) {
		return ( c >= 0x30 && (c <= 0x39 || 		// 0-9
				(c >= 0x41 && (c <= 0x46 ||			// A-F
				(c >= 0x61 && c <= 0x66) ))));		// a-f
	}
	
	public static boolean isWhitespace(final int c) {
		switch (c) {
		case ' ':
		case '\t':
		case '\n':
		case '\r':
			return true;
		default:
			return false;
		}
	}
	
}
