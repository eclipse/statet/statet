/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rpkg;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.ltk.model.core.impl.GenericFragmentSourceUnit2;
import org.eclipse.statet.r.core.RCodeStyleSettings;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.input.RSourceFragment;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.build.RPkgDescrSourceUnitModelContainer;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.doc.RPkgDescrDocumentContentInfo;
import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public abstract class RPkgDescrFragmentSourceUnit
		extends GenericFragmentSourceUnit2<RPkgDescrSourceUnitModelContainer>
		implements RSourceUnit, RCoreAccess {
	
	
	private final RSourceConfig rSourceConfig;
	
	
	public RPkgDescrFragmentSourceUnit(final String id, final SourceFragment fragment,
			final RSourceConfig rSourceConfig) {
		super(id, fragment);
		this.rSourceConfig= nonNullAssert(rSourceConfig);
	}
	
	public RPkgDescrFragmentSourceUnit(final String id, final SourceFragment fragment) {
		this(id, fragment,
				(fragment instanceof RSourceFragment) ?
						((RSourceFragment)fragment).getRSourceConfig() :
						RCore.getWorkbenchAccess().getRSourceConfig() );
	}
	
	@Override
	protected RPkgDescrSourceUnitModelContainer createModelContainer() {
		return new RPkgDescrSourceUnitModelContainer(this, null);
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.RPKG_DESCRIPTION_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return RPkgDescrDocumentContentInfo.INSTANCE;
	}
	
	@Override
	public int getElementType() {
		return R_OTHER_SU;
	}
	
	
	@Override
	protected void register() {
		super.register();
		RModel.getRModelManager().registerDependentUnit(this);
	}
	
	@Override
	protected void unregister() {
		super.unregister();
		RModel.getRModelManager().deregisterDependentUnit(this);
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return RCore.getWorkbenchAccess().getPrefs();
	}
	
	@Override
	public REnv getREnv() {
		final REnv rEnv= getFragment().getAdapter(REnv.class);
		if (rEnv != null) {
			return rEnv;
		}
		return RCore.getREnvManager().getDefault();
	}
	
	@Override
	public RSourceConfig getRSourceConfig() {
		return this.rSourceConfig;
	}
	
	@Override
	public RCodeStyleSettings getRCodeStyle() {
		return RCore.getWorkbenchAccess().getRCodeStyle();
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RCoreAccess.class) {
			return (T)this;
		}
		if (adapterType == PreferenceAccess.class) {
			return (T)EPreferences.getInstancePrefs();
		}
		return super.getAdapter(adapterType);
	}
	
	
}
