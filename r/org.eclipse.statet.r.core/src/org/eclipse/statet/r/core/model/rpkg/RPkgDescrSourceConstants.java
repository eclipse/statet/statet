/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rpkg;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE1;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE2;
import static org.eclipse.statet.ltk.core.StatusCodes.WARNING;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants;


@NonNullByDefault
public interface RPkgDescrSourceConstants extends DcfSourceConstants {
	
	
	static int TYPE1_MODEL_DESCR_RECORD=                    0x8 << SHIFT_TYPE1;
	static int TYPE12_FIELD_REQUIRED_MISSING=                   TYPE1_MODEL_DESCR_RECORD | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_FIELD_DUPLICATE=                          TYPE1_MODEL_DESCR_RECORD | 0x6 << SHIFT_TYPE2 | WARNING;
	
	static int TYPE1_MODEL_DESCR_VALUE=                     0x9 << SHIFT_TYPE1;
	static int TYPE12_FIELD_VALUE_MISSING=                      TYPE1_MODEL_DESCR_VALUE | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE12_FIELD_VALUE_INVALID=                      TYPE1_MODEL_DESCR_VALUE | 0x2 << SHIFT_TYPE2 | ERROR;
	
}
