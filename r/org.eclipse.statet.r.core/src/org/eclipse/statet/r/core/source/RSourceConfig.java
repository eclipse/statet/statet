/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.util.Version;

import org.eclipse.statet.ltk.core.source.SourceConfig;


@NonNullByDefault
public class RSourceConfig implements SourceConfig {
	
	
	private final Version langVersion;
	
	
	public RSourceConfig(final Version langVersion) {
		this.langVersion= langVersion;
	}
	
	
	public Version getLangVersion() {
		return this.langVersion;
	}
	
	
	@Override
	public int hashCode() {
		return this.langVersion.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof RSourceConfig
						&& this.langVersion.equals(((RSourceConfig)obj).langVersion) ));
	}
	
	
	@Override
	public String toString() {
		final var sb= new ToStringBuilder(RSourceConfig.class);
		sb.addProp("langVersion", this.langVersion.toString()); //$NON-NLS-1$
		return sb.toString();
	}
	
	
	public static final RSourceConfig DEFAULT_CONFIG= new RSourceConfig(
			RSourceConstants.LANG_VERSION_4_4 );
	
}
