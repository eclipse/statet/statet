/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.r.core.source.RTerminal;


@NonNullByDefault
public class SourceComponent extends ExpressionList {
	
	
	@Nullable AstNode parent;
	
	@Nullable ImList<RAstNode> comments;
	
	
	SourceComponent(final int statusCode, final @Nullable AstNode parent,
			final int startOffset, final int endOffset) {
		super(statusCode, null);
		this.parent= parent;
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	SourceComponent(final @Nullable AstNode parent) {
		super();
		this.parent= parent;
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.SOURCELINES;
	}
	
	@Override
	public final @Nullable RTerminal getOperator(final int index) {
		return null;
	}
	
	
	/**
	 * The comment nodes in this source component
	 * 
	 * @return the comments or <code>null</code>, if disabled
	 */
	public @Nullable ImList<RAstNode> getComments() {
		return this.comments;
	}
	
	
	@Override
	public @Nullable AstNode getParent() {
		return this.parent;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	public void acceptInRComments(final RAstVisitor visitor) throws InvocationTargetException {
		final var comments= this.comments;
		if (comments == null) {
			return;
		}
		for (int i= 0; i < comments.size(); i++) {
			comments.get(i).acceptInR(visitor);
		}
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.SOURCELINES == element.getNodeType());
	}
	
}
