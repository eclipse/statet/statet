/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rlang;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RFunctionSpec;
import org.eclipse.statet.r.core.model.RModel;


@NonNullByDefault
public class BasicRLangMethod implements RLangMethod<RLangElement<?>> {
	
	
	private final RElementName elementName;
	
	private final RFunctionSpec fSpec;
	
	
	public BasicRLangMethod(final RElementName elementName,
			final RFunctionSpec fSpec) {
		this.elementName= elementName;
		this.fSpec= fSpec;
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public int getElementType() {
		return RElement.R_COMMON_FUNCTION;
	}
	
	@Override
	public String getId() {
		return null;
	}
	
	@Override
	public RElementName getElementName() {
		return this.elementName;
	}
	
	@Override
	public @Nullable RFunctionSpec getFunctionSpec() {
		return this.fSpec;
	}
	
	
	@Override
	public @Nullable RElement<? extends RElement<?>> getModelParent() {
		return null;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangElement<?>> filter) {
		return false;
	}
	
	@Override
	public List<? extends RLangElement<?>> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangElement<?>> filter) {
		return ImCollections.emptyList();
	}
	
	
	@Override
	public boolean exists() {
		return true;
	}
	
	@Override
	public boolean isReadOnly() {
		return true;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapter) {
		return null;
	}
	
}
