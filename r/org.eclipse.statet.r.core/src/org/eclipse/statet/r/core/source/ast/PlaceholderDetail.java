/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.model.core.AttachmentsElement;


@NonNullByDefault
public class PlaceholderDetail {
	
	
	public static final @Nullable PlaceholderDetail get(final AttachmentsElement element) {
		for (final Object attachment : element.getAttachments()) {
			if (attachment instanceof PlaceholderDetail) {
				return (PlaceholderDetail)attachment;
			}
		}
		return null;
	}
	
	
	private final Placeholder placeholder;
	
	private final RAstNode substitute;
	
	
	public PlaceholderDetail(final Placeholder placeholder, final RAstNode substitute) {
		this.placeholder= placeholder;
		this.substitute= substitute;
	}
	
	
	public Placeholder getPlaceholder() {
		return this.placeholder;
	}
	
	public RAstNode getSubstitute() {
		return this.substitute;
	}
	
}
