/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.build;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.rmodel.RAstProblemReporter;
import org.eclipse.statet.internal.r.core.rmodel.RTaskTagReporter;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.TaskIssueConfig;
import org.eclipse.statet.ltk.model.core.build.BasicIssueReporter;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.model.rlang.RCompositeSrcStrElement;
import org.eclipse.statet.r.core.model.rlang.RLangSrcStrElement;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
public class RIssueReporter extends BasicIssueReporter<RSourceUnit, RSourceUnitModelInfo> {
	
	
	private final RAstProblemReporter syntaxProblemReporter= new RAstProblemReporter();
	
	private final RTaskTagReporter taskReporter= new RTaskTagReporter();
	
	
	public RIssueReporter() {
		super(RModel.R_TYPE_ID);
	}
	
	
	@Override
	protected RTaskTagReporter getTaskReporter() {
		return this.taskReporter;
	}
	
	
	@Override
	public void configure(final TaskIssueConfig taskIssueConfig) {
		super.configure(taskIssueConfig);
	}
	
	
	@Override
	protected void runReporters(final RSourceUnit sourceUnit, final RSourceUnitModelInfo modelInfo,
			final SourceContent content,
			final IssueRequestor requestor, final int level) {
		final RLangSrcStrElement element= modelInfo.getSourceElement();
		if (element instanceof RCompositeSrcStrElement) {
			final var elements= ((RCompositeSrcStrElement)element).getCompositeElements();
			for (final RLangSrcStrElement rChunk : elements) {
				runReporters(sourceUnit, rChunk.getAdapter(AstNode.class),
						modelInfo, content, requestor, level );
			}
		}
		else {
			runReporters(sourceUnit, element.getAdapter(AstNode.class),
					modelInfo, content, requestor, level );
		}
	}
	
	public void run(final RAstNode node,
			final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (requestor.isInterestedInProblems(RModel.R_TYPE_ID)) {
			this.syntaxProblemReporter.run(node, content, requestor);
		}
	}
	
	
	@Override
	protected void runReporters(final RSourceUnit sourceUnit, final @Nullable AstNode node,
			final @Nullable RSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (node == null) {
			return;
		}
		if (node instanceof RAstNode) {
			runReporters(sourceUnit, (RAstNode)node,
					modelInfo, content, requestor, level );
		}
		else {
			final int n= node.getChildCount();
			for (int i= 0; i < n; i++) {
				final AstNode child= node.getChild(i);
				if (child instanceof RAstNode) {
					runReporters(sourceUnit, (RAstNode)child,
							modelInfo, content, requestor, level );
				}
			}
		}
	}
	
	private void runReporters(final RSourceUnit sourceUnit, final RAstNode node,
			final @Nullable RSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (shouldReportProblems()) {
			this.syntaxProblemReporter.run(node, content, requestor);
		}
		if (shouldReportTasks()) {
			this.taskReporter.run(sourceUnit, node, content, requestor);
		}
	}
	
}
