/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.doc;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.BasicDocContentSections;


@NonNullByDefault
public class RPkgDescrDocumentContentInfo extends BasicDocContentSections {
	
	
	public static final String RPKG_DESCRIPTION=            RDocumentConstants.RPKG_DESCR_PARTITIONING;
	public static final String R=                           RDocumentConstants.R_PARTITIONING;
	
	
	public static final RPkgDescrDocumentContentInfo INSTANCE= new RPkgDescrDocumentContentInfo();
	
	
	public RPkgDescrDocumentContentInfo() {
		super(RDocumentConstants.RPKG_DESCR_PARTITIONING,
				ImCollections.newList(RPKG_DESCRIPTION, R) );
	}
	
	
	@Override
	public String getTypeByPartition(final String contentType) {
		if (RDocumentConstants.R_ANY_CONTENT_CONSTRAINT.matches(contentType)) {
			return R;
		}
		return RPKG_DESCRIPTION;
	}
	
}
