/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;
import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.SUBSEQUENT;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE1_RUNTIME_ERROR;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_FDEF;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_FOR;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_IF;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_PIPE;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_WHILE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_SEQREL_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_CC_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_CONDITION_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_CONDITION_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_ELEMENTNAME_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FDEF_ARGS_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_IF_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_IN_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_NODE_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_OPERATOR_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_SYMBOL_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_UNKNOWN;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.IntArrayList;
import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.string.BasicStringFactory;
import org.eclipse.statet.jcommons.string.StringFactory;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;
import org.eclipse.statet.jcommons.util.Version;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.StatusDetail;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.source.RLexer;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.RSourceConstants;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.NodeType.Assoc;


/**
 * Scanner to create a R AST.
 */
@NonNullByDefault
public final class RParser {
	
	
	private static final byte LINE_MODE_CONSOLE= 1;
	private static final byte LINE_MODE_BLOCK= 2;
	private static final byte LINE_MODE_EAT= 3;
	
	public static final int LEXER_CONFIG= RLexer.SKIP_WHITESPACE;
	
	public static final int COLLECT_COMMENTS= 1 << 0;
	public static final int ROXYGEN_COMMENTS= 1 << 3;
	
	
	static final class ArgsBuilder<T> {
		
		final ArrayList<T> args= new ArrayList<>(8);
		
		final IntList sepOffsets= new IntArrayList(8);
		
	}
	
	private static final class ExprContext {
		final RAstNode rootNode;
		final Expression rootExpr;
		RAstNode lastNode;
		@Nullable Expression openExpr;
		final byte lineMode;
		
		public ExprContext(final RAstNode node, final Expression expr, final byte eatLines) {
			this.rootNode= this.lastNode= node;
			this.rootExpr= this.openExpr= expr;
			this.lineMode= eatLines;
		}
		
		final void update(final RAstNode lastNode, final @Nullable Expression openExpr) {
			this.lastNode= lastNode;
			if (openExpr == null || openExpr.node != null) {
				this.openExpr= null;
			}
			else {
				this.openExpr= openExpr;
			}
		}
	}
	
	private static final class RoxygenCollector {
		
		private Comment[] lines= new @NonNull Comment[64];
		private int lineCount;
		private @Nullable DocuComment current;
		
		void init() {
			this.lineCount= 0;
			this.current= null;
		}
		
		void add(final Comment comment) {
			if (this.current == null) {
				this.current= new DocuComment();
			}
			comment.rParent= this.current;
			
			if (this.lineCount == this.lines.length) {
				this.lines= Arrays.copyOf(this.lines, this.lineCount + 64);
			}
			this.lines[this.lineCount++]= comment;
		}
		
		boolean hasComment() {
			return (this.current != null);
		}
		
		DocuComment finish(final @Nullable RLexer lexer) {
			final DocuComment comment= new DocuComment();
			final Comment[] lines= Arrays.copyOf(this.lines, this.lineCount);
			comment.lines= lines;
			comment.setStartEndOffset(lines[0].getStartOffset(), lines[this.lineCount - 1].getEndOffset());
			comment.nextOffset= (lexer != null && lexer.getType() != RTerminal.EOF) ? lexer.getOffset() : NA_OFFSET;
			
			this.lineCount= 0;
			this.current= null;
			return comment;
		}
		
	}
	
	
	static final int LANG_VERSION_4_0= 0;
	static final int LANG_VERSION_4_1= 1;
	static final int LANG_VERSION_4_2= 2;
	static final int LANG_VERSION_4_3= 3;
	static final int LANG_VERSION_4_4= 4;
	
	private static int toRLangVersionInt(final Version version) {
		if (version.compareTo(RSourceConstants.LANG_VERSION_4_4) >= 0) {
			return LANG_VERSION_4_4;
		}
		if (version.compareTo(RSourceConstants.LANG_VERSION_4_3) >= 0) {
			return LANG_VERSION_4_3;
		}
		if (version.compareTo(RSourceConstants.LANG_VERSION_4_2) >= 0) {
			return LANG_VERSION_4_2;
		}
		if (version.compareTo(RSourceConstants.LANG_VERSION_4_1) >= 0) {
			return LANG_VERSION_4_1;
		}
		return LANG_VERSION_4_0;
	}
	
	private static Version toRLangVersionObj(final int version) {
		return switch (version) {
		case LANG_VERSION_4_0 ->	RSourceConstants.LANG_VERSION_4_0;
		case LANG_VERSION_4_1 ->	RSourceConstants.LANG_VERSION_4_1;
		case LANG_VERSION_4_2 ->	RSourceConstants.LANG_VERSION_4_2;
		case LANG_VERSION_4_3 ->	RSourceConstants.LANG_VERSION_4_3;
		case LANG_VERSION_4_4 ->	RSourceConstants.LANG_VERSION_4_4;
		default ->
				throw new IllegalArgumentException();
		};
	}
	
	
	private final RLexer lexer;
	private final int level;
	
	private int rLangVersion;
	
	private TextParserInput parseInput;
	
	private final RParserPostExprVisitor postVisitor= new RParserPostExprVisitor(this);
	
	private RTerminal nextType;
	private boolean wasLinebreak;
	
	private int commentsLevel;
	private final List<RAstNode> comments= new ArrayList<>();
	private @Nullable RoxygenCollector roxygen;
	
	private final boolean createText;
	private final StringFactory symbolTextFactory;
	
	
	public RParser(final RSourceConfig rSourceConfig,
			final int level, final RLexer lexer, final @Nullable StringFactory symbolTextFactory) {
		this.level= level;
		this.lexer= nonNullAssert(lexer);
		setRSourceConfig(rSourceConfig);
		
		this.createText= ((level & AstInfo.DEFAULT_LEVEL_MASK) > AstInfo.LEVEL_MINIMAL);
		this.symbolTextFactory= (symbolTextFactory != null) ? symbolTextFactory : BasicStringFactory.INSTANCE;
	}
	
	public RParser(final RSourceConfig rSourceConfig,
			final int level, final @Nullable StringFactory symbolTextFactory) {
		this(rSourceConfig, level, new RLexer((level == AstInfo.LEVEL_MINIMAL) ?
						(RLexer.DEFAULT | LEXER_CONFIG | RLexer.ENABLE_QUICK_CHECK) :
						(RLexer.DEFAULT | LEXER_CONFIG) ),
				symbolTextFactory );
	}
	
	public RParser(final int level) {
		this(RSourceConfig.DEFAULT_CONFIG, level, (StringFactory)null);
	}
	
	
	public void setCommentLevel(int level) {
		if ((level & COLLECT_COMMENTS) == 0) {
			level= 0;
			this.comments.clear();
		}
		else {
			level= (level & (COLLECT_COMMENTS | ROXYGEN_COMMENTS));
		}
		this.commentsLevel= level;
		
		if ((this.commentsLevel & ROXYGEN_COMMENTS) != 0 && this.roxygen == null) {
			this.roxygen= new RoxygenCollector();
		}
	}
	
	
	int getLangVersion() {
		return this.rLangVersion;
	}
	
	public Version getRLangVersion() {
		return toRLangVersionObj(this.rLangVersion);
	}
	
	public void setRSourceConfig(final RSourceConfig config) {
		this.rLangVersion= toRLangVersionInt(config.getLangVersion());
	}
	
	public int getAstLevel() {
		return this.level;
	}
	
	
	protected final TextParserInput getParseInput() {
		return this.parseInput;
	}
	
	protected void initInput(final TextParserInput input, final @Nullable AstNode parent) {
		this.parseInput= nonNullAssert(input);
	}
	
	@SuppressWarnings("null")
	private void clearInput() {
		this.parseInput= null;
	}
	
	protected void clearData() {
		if (this.commentsLevel != 0) {
			this.comments.clear();
		}
	}
	
	private void handleParseError(final Exception e) {
		CommonsRuntime.log(new ErrorStatus(RCore.BUNDLE_ID,
				"An error occured while parsing R source code. Input:\n"
						+ this.parseInput.toString(),
				e ));
		
		clearData();
	}
	
	
	public SourceComponent parseSourceUnit(final TextParserInput input) {
		initInput(input, null);
		try {
			init();
			
			final SourceComponent sourceNode= scanSourceUnit(null, false);
			
			return sourceNode;
		}
		catch (final Exception e) {
			handleParseError(e);
			return createErrorSourceComponent(null);
		}
		finally {
			clearInput();
		}
	}
	
	public SourceComponent parseSourceFragment(final TextParserInput input,
			final @Nullable AstNode parent, final boolean expand) {
		initInput(input, parent);
		try {
			init();
			
			final SourceComponent sourceNode= scanSourceUnit(parent, expand);
			
			return sourceNode;
		}
		catch (final Exception e) {
			handleParseError(e);
			return createErrorSourceComponent(parent);
		}
		finally {
			clearInput();
		}
	}
	
	public SourceComponent parseSourceFragment(final TextParserInput input,
			final @Nullable AstNode parent) {
		return parseSourceFragment(input, parent, false);
	}
	
	private SourceComponent createErrorSourceComponent(final @Nullable AstNode parent) {
		final int startOffset= this.parseInput.getStartIndex();
		int endOffset= this.parseInput.getStopIndex();
		if (endOffset < startOffset) {
			endOffset= startOffset;
		}
		final SourceComponent dummy= new SourceComponent(TYPE1_RUNTIME_ERROR, parent,
				startOffset, endOffset );
		if (this.commentsLevel != 0) {
			dummy.comments= ImCollections.emptyList();
		}
		return dummy;
	}
	
	public @Nullable RAstNode parseExpr(final TextParserInput input) {
		initInput(input, null);
		try {
			init();
			
			final SourceComponent sourceNode= scanSourceUnit(null, false);
			
			if (sourceNode.getChildCount() == 1) {
				return sourceNode.getChild(0);
			}
			return null;
		}
		catch (final Exception e) {
			handleParseError(e);
			return null;
		}
		finally {
			clearInput();
		}
	}
	
	public @Nullable FDef parseFDef(final TextParserInput input) {
		initInput(input, null);
		try {
			init();
			
			FDef node;
			switch (this.nextType) {
			case FUNCTION:
			case FUNCTION_B:
				node= scanFDef((ExprContext)null);
				break;
			default:
				node= null;
				break;
			}
			
			return node;
		}
		catch (final Exception e) {
			handleParseError(e);
			return null;
		}
		finally {
			clearInput();
		}
	}
	
	public FCall. @Nullable Args parseFCallArgs(final TextParserInput input, final boolean expand) {
		initInput(input, null);
		try {
			init();
			
			final FCall callNode= new FCall();
			callNode.setEndOffset(NA_OFFSET);
			scanInSpecArgs(callNode.args);
			if (expand) {
				callNode.args.setStartEndOffset(input.getStartIndex(), input.getIndex());
			}
			
			return callNode.args;
		}
		catch (final Exception e) {
			handleParseError(e);
			return null;
		}
		finally {
			clearInput();
		}
	}
	
	
	private void init() {
		this.lexer.reset(this.parseInput);
		
		clearData();
		clearArgsBuilder();
		if (this.commentsLevel != 0) {
			if ((this.commentsLevel & ROXYGEN_COMMENTS) != 0) {
				this.roxygen.init();
			}
		}
		this.nextType= RTerminal.LINEBREAK;
		consumeToken();
	}
	
	final SourceComponent scanSourceUnit(final @Nullable AstNode parent, final boolean expand) {
		final SourceComponent node= new SourceComponent(parent);
		scanInExprList(node, true);
//		if (this.nextType == RTerminal.EOF) {
//			this.next.type= null;
//		}
		if (this.commentsLevel != 0) {
			node.comments= ImCollections.toList(this.comments);
		}
		
		if (expand) {
			node.setStartEndOffset(this.lexer.getInput().getStartIndex(),
					this.lexer.getInput().getStopIndex() );
		}
		else if (node.getChildCount() > 0) {
			node.setStartEndOffset(node.getChild(0).getStartOffset(),
					node.getChild(node.getChildCount() - 1).getEndOffset() );
		}
		else {
			node.setStartEndOffset(this.lexer.getInput().getStartIndex());
		}
		return node;
	}
	
	final void scanInExprList(final ExpressionList node, final boolean script) {
		ITER_TOKEN: while (true) {
			switch (this.nextType) {
			
			case EOF:
				break ITER_TOKEN;
				
			case LINEBREAK:
				consumeToken();
				continue ITER_TOKEN;
			
			default:
				{
					Expression expr= node.appendNewExpr();
					final ExprContext context= new ExprContext(node, expr,
							script ? LINE_MODE_CONSOLE : LINE_MODE_BLOCK );
					scanInExpression(context);
					
					if (expr.node == null) {
						node.expressions.remove(context.rootExpr);
						expr= null;
					}
					else {
						checkExpression(context);
					}
					switch (this.nextType) {
					
					case SEMICOLON:
						if (expr != null) {
							node.setSeparator(this.lexer.getOffset());
							consumeToken();
							continue ITER_TOKEN;
						}
						// else error like comma
						//$FALL-THROUGH$
					case COMMA:
						{
							expr= node.appendNewExpr();
							expr.node= errorFromNext(node);
						}
						continue ITER_TOKEN;
						
					case SUB_INDEXED_CLOSE:
					case BLOCK_CLOSE:
					case GROUP_CLOSE:
						if (script) {
							expr= node.appendNewExpr();
							expr.node= errorFromNext(node);
							continue ITER_TOKEN;
						}
						break ITER_TOKEN;
					}
				}
			}
		}
	}
	
	final int scanInGroup(final RAstNode node, final Expression expr) {
		final ExprContext context= new ExprContext(node, expr, LINE_MODE_EAT);
		scanInExpression(context);
		return checkExpression(context);
	}
	
	final void scanInExpression(final ExprContext context) {
		this.wasLinebreak= false;
		ITER_TOKEN : while(true) {
			
			if (this.wasLinebreak && context.lineMode < LINE_MODE_EAT && context.openExpr == null) {
				break ITER_TOKEN;
			}
			
			switch (this.nextType) {
			
			case LINEBREAK:
				if (context.lineMode < LINE_MODE_EAT && context.openExpr == null) {
					break ITER_TOKEN;
				}
				consumeToken();
				continue ITER_TOKEN;
				
			case SYMBOL:
			case SYMBOL_G:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, createSymbol(null));
				continue ITER_TOKEN;
			case PIPE_PLACEHOLDER:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				{	final Placeholder node= new Placeholder.PipeIn();
					setupFromSourceToken((RAstNode)node);
					if (this.rLangVersion < LANG_VERSION_4_2) {
						node.setStatus(TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE);
					}
					else {
						node.setStatus(TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_PIPE);
					}
					appendNonOp(context, node);
					consumeToken();
					continue ITER_TOKEN;
				}
			
			case TRUE:
			case FALSE:
			case NUM_NUM:
			case NUM_INT:
			case NUM_CPLX:
			case NA:
			case NA_REAL:
			case NA_INT:
			case NA_CPLX:
			case NA_CHAR:
			case NAN:
			case INF:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, createNumberConst(null));
				continue ITER_TOKEN;
			case NULL:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, createNullConst(null));
				continue ITER_TOKEN;
				
			case STRING_D:
			case STRING_S:
			case STRING_R:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, createStringConst(null));
				continue ITER_TOKEN;
				
			case ARROW_LEFT_S:
			case ARROW_LEFT_D:
			case ARROW_RIGHT_S:
			case ARROW_RIGHT_D:
			case EQUAL:
			case COLON_EQUAL:
				appendOp(context, createAssignment());
				continue ITER_TOKEN;
			
			case PIPE_RIGHT:
				{	final Pipe node= new Pipe.Forward();
					setupFromSourceToken(node);
					if (this.rLangVersion < LANG_VERSION_4_1) {
						node.setStatus(TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE);
					}
					consumeToken();
					appendOp(context, node);
					continue ITER_TOKEN;
				}
			
			case TILDE:
				if (context.openExpr != null) {
					appendNonOp(context, createModel());
				}
				else {
					appendOp(context, createModel());
				}
				continue ITER_TOKEN;
				
			case PLUS:
			case MINUS:
				if (context.openExpr != null) {
					appendNonOp(context, createSign());
				}
				else {
					appendOp(context, createArithmetic());
				}
				continue ITER_TOKEN;
			case MULT:
			case DIV:
				appendOp(context, createArithmetic());
				continue ITER_TOKEN;
			case POWER:
				appendOp(context, createPower());
				continue ITER_TOKEN;
				
			case SEQ:
				appendOp(context, createSeq());
				continue ITER_TOKEN;
				
			case SPECIAL:
				appendOp(context, createSpecial());
				continue ITER_TOKEN;
				
			case REL_LT:
			case REL_LE:
			case REL_EQ:
			case REL_GE:
			case REL_GT:
			case REL_NE:
				appendOp(context, createRelational());
				continue ITER_TOKEN;
				
			case OR:
			case OR_D:
			case AND:
			case AND_D:
				appendOp(context, createLogical());
				continue ITER_TOKEN;
				
			case NOT:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, createSign());
				continue ITER_TOKEN;
			
			case IF:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, scanCIf(context));
				continue ITER_TOKEN;
			case ELSE:
				if (context.rootNode.getNodeType() == NodeType.C_IF) {
					break ITER_TOKEN;
				}
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				
				appendNonOp(context, scanCElse(context));
				continue ITER_TOKEN;
			
			case FOR:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, scanCForLoop(context));
				continue ITER_TOKEN;
			case REPEAT:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, scanCRepeatLoop(context));
				continue ITER_TOKEN;
			case WHILE:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, scanCWhileLoop(context));
				continue ITER_TOKEN;
			
			case BREAK:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, createLoopCommand());
				continue ITER_TOKEN;
			case NEXT:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, createLoopCommand());
				continue ITER_TOKEN;
			
			case FUNCTION:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, scanFDef(context));
				continue ITER_TOKEN;
			case FUNCTION_B:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, scanFDef(context));
				continue ITER_TOKEN;
				
			case GROUP_OPEN:
				if (context.openExpr != null) {
					appendNonOp(context, scanGroup());
				}
				else {
					appendOp(context, scanFCall());
				}
				continue ITER_TOKEN;
				
			case BLOCK_OPEN:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, scanBlock());
				continue ITER_TOKEN;
			
			case EOF:
				break ITER_TOKEN;
				
			case NS_GET:
			case NS_GET_INT:
				if (this.wasLinebreak && context.openExpr == null) {
					break ITER_TOKEN;
				}
				appendNonOp(context, scanNSGet(context));
				continue ITER_TOKEN;
				
			case SUB_INDEXED_S_OPEN:
			case SUB_INDEXED_D_OPEN:
				appendOp(context, scanSubIndexed(context));
				continue ITER_TOKEN;
				
			case SUB_NAMED_PART:
			case SUB_NAMED_SLOT:
				appendOp(context, scanSubNamed(context));
				continue ITER_TOKEN;
				
			case QUESTIONMARK:
				if (context.openExpr != null) {
					appendNonOp(context, createHelp());
					continue ITER_TOKEN;
				}
				else {
					appendOp(context, createHelp());
					continue ITER_TOKEN;
				}
				
			case UNKNOWN:
			case IN:
				appendNonOp(context, errorFromNext(null));
				continue ITER_TOKEN;
				
			case COMMA:
			case SEMICOLON:
			case SUB_INDEXED_CLOSE:
			case BLOCK_CLOSE:
			case GROUP_CLOSE:
				break ITER_TOKEN;
				
			default:
				throw new IllegalStateException("Unhandled token in expr-scanner: "+this.nextType.name());
			}
		}
	}
	
	final Group scanGroup() {
		final Group node= new Group();
		setupFromSourceToken(node);
		consumeToken();
		scanInGroup(node, node.expr);
		if (this.nextType == RTerminal.GROUP_CLOSE) {
			node.groupCloseOffset= this.lexer.getOffset();
			node.setEndOffset(node.groupCloseOffset + 1);
			consumeToken();
			return node;
		}
		else {
			node.setStatus(TYPE12_SYNTAX_CC_NOT_CLOSED);
			node.setEndOffset(this.lexer.getOffset());
			return node;
		}
	}
	
	final Block scanBlock() {
		final Block node= new Block();
		setupFromSourceToken(node);
		consumeToken();
		scanInExprList(node, false);
		if (this.nextType == RTerminal.BLOCK_CLOSE) {
			node.blockCloseOffset= this.lexer.getOffset();
			node.setEndOffset(node.blockCloseOffset + 1);
			consumeToken();
			return node;
		}
		else {
			node.setStatus(TYPE12_SYNTAX_CC_NOT_CLOSED);
			node.setEndOffset(this.lexer.getOffset());
			return node;
		}
	}
	
	final NSGet scanNSGet(final ExprContext context) {
		final NSGet node= switch (this.nextType) {
				case NS_GET ->				new NSGet.Std();
				case NS_GET_INT ->			new NSGet.Internal();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		node.operatorOffset= this.lexer.getOffset();
		consumeToken();
		
		// setup ns
		switch (context.lastNode.getNodeType()) {
		case SYMBOL:
		case STRING_CONST:
			{
				node.namespace= (SingleValue) context.lastNode;
				final RAstNode base= context.lastNode.rParent;
				node.namespace.rParent= node;
				final Expression expr= base.getExpr(node.namespace);
				if (expr != null) {
					expr.node= null;
				}
				else {
					throw new IllegalStateException(); // ?
				}
				context.update(base, expr);
				node.setStartOffset(node.namespace.getStartOffset());
				break;
			}
		default:
			node.namespace= errorNonExistingSymbol(node, node.getStartOffset(),
					TYPE12_SYNTAX_ELEMENTNAME_MISSING );
			break;
		}
		
		// element
		switch (this.nextType) {
		case STRING_D:
		case STRING_S:
		case STRING_R:
			node.element= createStringConst(node);
			node.setEndOffset(node.element.getEndOffset());
			return node;
		case SYMBOL:
		case SYMBOL_G:
			node.element= createSymbol(node);
			node.setEndOffset(node.element.getEndOffset());
			return node;
		default:
			node.element= errorNonExistingSymbol(node, node.getEndOffset(),
					TYPE12_SYNTAX_ELEMENTNAME_MISSING );
			return node;
		}
	}
	
	final SubNamed scanSubNamed(final ExprContext context) {
		final SubNamed node= switch (this.nextType) {
				case SUB_NAMED_PART ->		new SubNamed.Named();
				case SUB_NAMED_SLOT ->		new SubNamed.Slot();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		node.operatorOffset= this.lexer.getOffset();
		consumeToken();
		readLines();
		
		switch (this.nextType) {
		case STRING_D:
		case STRING_S:
		case STRING_R:
			node.subname= createStringConst(node);
			node.setEndOffset(node.subname.getEndOffset());
			return node;
		case SYMBOL:
		case SYMBOL_G:
			node.subname= createSymbol(node);
			node.setEndOffset(node.subname.getEndOffset());
			return node;
		default:
			node.subname= errorNonExistingSymbol(node, node.getEndOffset(),
					TYPE12_SYNTAX_ELEMENTNAME_MISSING );
			return node;
		}
	}
	
	final SubIndexed scanSubIndexed(final ExprContext context) {
		final SubIndexed node= switch (this.nextType) {
				case SUB_INDEXED_S_OPEN ->	new SubIndexed.S();
				case SUB_INDEXED_D_OPEN ->	new SubIndexed.D();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		node.openOffset= this.lexer.getOffset();
		consumeToken();
		readLines();
		
		scanInSpecArgs(node.sublist);
		
		RParserPostExprVisitor.updateParentStatus(node.sublist);
		if (this.nextType == RTerminal.SUB_INDEXED_CLOSE) {
			node.closeOffset= this.lexer.getOffset();
			consumeToken();
			
			if (node.getNodeType() == NodeType.SUB_INDEXED_D) {
				if (this.nextType == RTerminal.SUB_INDEXED_CLOSE) {
					node.close2Offset= this.lexer.getOffset();
					node.setEndOffset(node.close2Offset + 1);
					consumeToken();
					return node;
				}
				else {
					node.setStatus(TYPE12_SYNTAX_NODE_NOT_CLOSED);
					node.setEndOffset(node.closeOffset + 1);
					return node;
				}
			}
			else {
				node.setEndOffset(node.closeOffset + 1);
				return node;
			}
		}
		else {
			node.setStatus(TYPE12_SYNTAX_NODE_NOT_CLOSED);
			node.setEndOffset(node.sublist.getEndOffset());
			return node;
		}
	}
	
	final CIfElse scanCIf(final ExprContext context) {
		final CIfElse node= new CIfElse();
		setupFromSourceToken(node);
		consumeToken();
		int ok= 0;
		readLines();
		
		if (this.nextType == RTerminal.GROUP_OPEN) {
			node.condOpenOffset= this.lexer.getOffset();
			node.setEndOffset(this.lexer.getOffset() + 1);
			consumeToken();
			readLines();
			
			// condition
			ok+= scanInGroup(node, node.condExpr);
			
			if (this.nextType == RTerminal.GROUP_CLOSE) {
				node.condCloseOffset= this.lexer.getOffset();
				node.setEndOffset(node.condCloseOffset + 1);
				consumeToken();
				ok= 1;
				readLines();
			}
			else {
				node.setStatus(TYPE12_SYNTAX_CONDITION_NOT_CLOSED);
				node.setEndOffset(node.condExpr.node.getEndOffset());
			}
		}
		else {
			node.setStatus(TYPE12_SYNTAX_CONDITION_MISSING);
			node.condExpr.node= errorNonExistExpression(node, node.getEndOffset(),
					TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING | SUBSEQUENT | CTX12_IF );
		}
		
		// then
		if (ok > 0 || recoverCCont()) {
			final ExprContext thenContext= new ExprContext(node, node.thenExpr, context.lineMode);
			scanInExpression(thenContext);
			checkExpression(thenContext);
			node.setEndOffset(node.thenExpr.node.getEndOffset());
			if (context.lineMode >= LINE_MODE_BLOCK) {
				readLines();
			}
		}
		else {
			node.thenExpr.node= errorNonExistExpression(node, node.condExpr.node.getEndOffset(),
					TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | SUBSEQUENT | CTX12_IF );
		}
		
		// else
		if (this.nextType == RTerminal.ELSE) {
			node.withElse= true;
			node.elseOffset= this.lexer.getOffset();
			consumeToken();
			// else body is added via common expression processing
		}
		
		return node;
	}
	
	final CIfElse scanCElse(final ExprContext context) { // else without if
		final CIfElse node= new CIfElse();
		setupFromSourceToken(node);
		node.setStatus(TYPE12_SYNTAX_IF_MISSING);
		node.condExpr.node= errorNonExistExpression(node, node.getStartOffset(),
				TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING | SUBSEQUENT | CTX12_IF );
		node.thenExpr.node= errorNonExistExpression(node, node.getStartOffset(),
				TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | SUBSEQUENT | CTX12_IF );
		node.elseOffset= this.lexer.getOffset();
		node.withElse= true;
		consumeToken();
		
		return node;
	}
	
	final CForLoop scanCForLoop(final ExprContext context) {
		final CForLoop node= new CForLoop();
		setupFromSourceToken(node);
		consumeToken();
		int ok= 0;
		readLines();
		
		if (this.nextType == RTerminal.GROUP_OPEN) {
			node.condOpenOffset= this.lexer.getOffset();
			consumeToken();
			readLines();
			
			// condition
			switch (this.nextType) {
			case SYMBOL:
			case SYMBOL_G:
				node.varSymbol= createSymbol(node);
				readLines();
				break;
			default:
				node.varSymbol= errorNonExistingSymbol(node, node.condOpenOffset + 1,
						TYPE12_SYNTAX_SYMBOL_MISSING );
				ok--;
				break;
			}
			
			if (this.nextType == RTerminal.IN) {
				node.inOffset= this.lexer.getOffset();
				node.setEndOffset(node.inOffset + 2);
				consumeToken();
				readLines();
				
				ok+= scanInGroup(node, node.condExpr);
			}
			else {
				node.setStatus((ok >= 0) ? TYPE12_SYNTAX_IN_MISSING :
						(TYPE12_SYNTAX_IN_MISSING | SUBSEQUENT) );
				node.setEndOffset(node.varSymbol.getEndOffset());
				node.condExpr.node= errorNonExistExpression(node, node.getEndOffset(),
						TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING | SUBSEQUENT );
			}
			
			if (this.nextType == RTerminal.GROUP_CLOSE) {
				node.condCloseOffset= this.lexer.getOffset();
				node.setEndOffset(node.condCloseOffset + 1);
				consumeToken();
				ok= 1;
				readLines();
			}
			else {
				if ((node.getStatusCode() & ERROR) == 0) {
					node.setStatus((ok >= 0) ? TYPE12_SYNTAX_CONDITION_NOT_CLOSED :
						(TYPE12_SYNTAX_CONDITION_NOT_CLOSED | SUBSEQUENT) );
				}
				node.setEndOffset(node.condExpr.node.getEndOffset());
			}
		}
		else { // missing GROUP_OPEN
			node.setStatus(TYPE12_SYNTAX_CONDITION_MISSING);
			node.varSymbol= errorNonExistingSymbol(node, node.getEndOffset(),
					TYPE12_SYNTAX_SYMBOL_MISSING | SUBSEQUENT );
			node.condExpr.node= errorNonExistExpression(node, node.getEndOffset(),
					TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING | SUBSEQUENT | CTX12_FOR);
		}
		
		// loop
		if (ok <= 0 && !recoverCCont()) {
			node.loopExpr.node= errorNonExistExpression(node, node.getEndOffset(),
					TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | SUBSEQUENT | CTX12_FOR );
		}
		
		return node;
	}
	
	final CWhileLoop scanCWhileLoop(final ExprContext context) {
		final CWhileLoop node= new CWhileLoop();
		setupFromSourceToken(node);
		consumeToken();
		int ok= 0;
		readLines();
		
		if (this.nextType == RTerminal.GROUP_OPEN) {
			node.condOpenOffset= this.lexer.getOffset();
			node.setEndOffset(node.condOpenOffset + 1);
			consumeToken();
			readLines();
			
			// condition
			ok+= scanInGroup(node, node.condExpr);
			
			if (this.nextType == RTerminal.GROUP_CLOSE) {
				node.condCloseOffset= this.lexer.getOffset();
				node.setEndOffset(node.condCloseOffset + 1);
				consumeToken();
				ok= 1;
				readLines();
			}
			else {
				node.setStatus((ok >= 0) ? TYPE12_SYNTAX_CONDITION_NOT_CLOSED :
					(TYPE12_SYNTAX_CONDITION_NOT_CLOSED | SUBSEQUENT) );
				node.setEndOffset(node.condExpr.node.getEndOffset());
			}
		}
		else {
			node.setStatus(TYPE12_SYNTAX_CONDITION_MISSING);
			node.condExpr.node= errorNonExistExpression(node, node.getEndOffset(),
					TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING | SUBSEQUENT | CTX12_WHILE );
		}
		
		// loop
		if (ok <= 0 && !recoverCCont()) {
			node.loopExpr.node= errorNonExistExpression(node, node.getEndOffset(),
					TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | SUBSEQUENT | CTX12_WHILE );
		}
		
		return node;
	}
	
	final CRepeatLoop scanCRepeatLoop(final ExprContext context) {
		final CRepeatLoop node= new CRepeatLoop();
		setupFromSourceToken(node);
		consumeToken();
		
		return node;
	}
	
	final FDef scanFDef(final @Nullable ExprContext context) {
		final FDef node;
		switch (this.nextType) {
		case FUNCTION:
			node= new FDef.Function();
			setupFromSourceToken(node);
			break;
		case FUNCTION_B:
			node= new FDef.B();
			setupFromSourceToken(node);
			if (this.rLangVersion < LANG_VERSION_4_1) {
				node.setStatus(TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_FDEF);
			}
			break;
		default:
			throw new IllegalStateException();
		}
		consumeToken();
		int ok= 0;
		readLines();
		
		if (this.nextType == RTerminal.GROUP_OPEN) {
			node.argsOpenOffset= this.lexer.getOffset();
			node.setEndOffset(node.argsOpenOffset + 1);
			consumeToken();
			readLines();
			
			// args
			scanInFDefArgs(node.args);
			
			if (this.nextType == RTerminal.GROUP_CLOSE) {
				node.argsCloseOffset= this.lexer.getOffset();
				node.setEndOffset(node.argsCloseOffset + 1);
				consumeToken();
				ok= 1;
				readLines();
			}
			else {
				node.setEndOffset(node.args.getEndOffset());
				if ((node.getStatusCode() & ERROR) == 0) {
					node.setStatus(TYPE12_SYNTAX_FDEF_ARGS_NOT_CLOSED);
				}
			}
		}
		else {
			node.args.finishMissing(node.getEndOffset());
			if ((node.getStatusCode() & ERROR) == 0) {
				node.setStatus(TYPE12_SYNTAX_FDEF_ARGS_MISSING);
			}
		}
		
		// body
		if (ok <= 0 && !recoverCCont()) {
			node.expr.node= errorNonExistExpression(node, node.getEndOffset(),
					TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | SUBSEQUENT | CTX12_FDEF );
		}
		
		return node;
	}
	
	final FCall scanFCall() {
		final FCall node= new FCall();
		
		setupFromSourceToken(node);
		node.argsOpenOffset= this.lexer.getOffset();
		consumeToken();
		readLines();
		
		scanInSpecArgs(node.args);
		
		RParserPostExprVisitor.updateParentStatus(node.args);
		if (this.nextType == RTerminal.GROUP_CLOSE) {
			node.argsCloseOffset= this.lexer.getOffset();
			node.setEndOffset(node.argsCloseOffset + 1);
			consumeToken();
		}
		else {
			node.setStatus(TYPE12_SYNTAX_NODE_NOT_CLOSED);
			node.setEndOffset(node.args.getEndOffset());
		}
		
		return node;
	}
	
	final void scanInFDefArgs(final FDef.Args args) {
		final var builder= this.<FDef.Arg>getArgsBuilder();
		args.setStartEndOffset(args.rParent.getEndOffset());
		ITER_ARGS : while (true) {
			final FDef.Arg arg= new FDef.Arg(args);
			switch(this.nextType) {
			case SYMBOL:
			case SYMBOL_G:
				arg.argName= createSymbol(arg);
				arg.setStartEndOffset(arg.argName.getStartOffset(), arg.argName.getEndOffset());
				readLines();
				break;
			case EQUAL:
			case COMMA:
				arg.setStartEndOffset(this.lexer.getOffset());
				break;
			default:
				if (builder.args.isEmpty()) {
					break ITER_ARGS;
				}
				arg.setStartEndOffset(args.getEndOffset());
				break;
			}
			
			if (arg.argName == null) {
				arg.argName= errorNonExistingSymbol(arg, arg.getEndOffset(),
						TYPE12_SYNTAX_SYMBOL_MISSING );
			}
			
			if (this.nextType == RTerminal.EQUAL) {
				arg.setEndOffset(this.lexer.getOffset() + 1);
				consumeToken();
				
				final Expression expr= arg.addDefault();
				scanInGroup(arg, expr);
				arg.setEndOffset(arg.defaultExpr.node.getEndOffset());
			}
			
			builder.args.add(arg);
			RParserPostExprVisitor.updateParentStatus(arg);
			if (this.nextType == RTerminal.COMMA) {
				args.setEndOffset(this.lexer.getOffset() + 1);
				consumeToken();
				readLines();
				continue ITER_ARGS;
			}
			else {
				args.setStartEndOffset(builder.args.get(0).getStartOffset(), arg.getEndOffset());
				break ITER_ARGS;
			}
		}
		args.finish(builder);
		returnArgsBuilder(builder);
	}
	
	final void scanInSpecArgs(final FCall.Args args) {
		final var builder= this.<FCall.Arg>getArgsBuilder();
		args.setStartEndOffset(args.rParent.getEndOffset());
		ITER_ARGS : while (true) {
			final FCall.Arg arg= new FCall.Arg(args);
			arg.setStartOffset(this.lexer.getOffset());
			switch(this.nextType) {
			case SYMBOL:
				arg.argName= createSymbol(arg);
				readLines();
				break;
			case STRING_D:
			case STRING_S:
			case STRING_R:
				arg.argName= createStringConst(arg);
				readLines();
				break;
			case NULL:
				arg.argName= createNullConst(arg);
				readLines();
				break;
			case EQUAL:
				arg.argName= errorNonExistingSymbol(arg, this.lexer.getOffset(),
						TYPE12_SYNTAX_ELEMENTNAME_MISSING );
				break;
			default:
				break;
			}
			if (arg.argName != null) {
				if (this.nextType == RTerminal.EQUAL) {
					RParserPostExprVisitor.updateParentStatus(arg.argName);
					arg.equalsOffset= this.lexer.getOffset();
					arg.setEndOffset(arg.equalsOffset + 1);
					consumeToken();
					
					final ExprContext valueContext= new ExprContext(arg, arg.valueExpr, LINE_MODE_EAT);
					scanInExpression(valueContext);
					if (arg.valueExpr.node != null) { // empty items are allowed
						checkExpression(valueContext);
						arg.setEndOffset(arg.valueExpr.node.getEndOffset());
					}
				}
				else {
					// argName -> valueExpr
					arg.valueExpr.node= arg.argName;
					arg.argName= null;
					
					final ExprContext valueContext= new ExprContext(arg, arg.valueExpr, LINE_MODE_EAT);
					valueContext.update(arg.valueExpr.node, null);
					scanInExpression(valueContext);
					checkExpression(valueContext);
					arg.setEndOffset(arg.valueExpr.node.getEndOffset());
				}
			}
			else {
				final ExprContext valueContext= new ExprContext(arg, arg.valueExpr, LINE_MODE_EAT);
				scanInExpression(valueContext);
				if (arg.valueExpr.node != null) { // empty items are allowed
					checkExpression(valueContext);
					arg.setEndOffset(arg.valueExpr.node.getEndOffset());
				}
				else {
					arg.setStartEndOffset(args.getEndOffset());
				}
			}
			
			if (this.nextType == RTerminal.COMMA) {
				builder.args.add(arg);
				RParserPostExprVisitor.updateParentStatus(arg);
				builder.sepOffsets.add(this.lexer.getOffset());
				args.setEndOffset(this.lexer.getOffset() + 1);
				consumeToken();
				readLines();
				continue ITER_ARGS;
			}
			else {
				if (arg.hasChildren() || !builder.args.isEmpty()) {
					builder.args.add(arg);
					RParserPostExprVisitor.updateParentStatus(arg);
					args.setStartEndOffset(builder.args.get(0).getStartOffset(), arg.getEndOffset());
				}
				break ITER_ARGS;
			}
		}
		args.finish(builder);
		returnArgsBuilder(builder);
	}
	
	final void scanInSpecArgs(final SubIndexed.Args args) {
		final var builder= this.<SubIndexed.Arg>getArgsBuilder();
		args.setStartEndOffset(args.rParent.getEndOffset());
		ITER_ARGS : while (true) {
			final SubIndexed.Arg arg= new SubIndexed.Arg(args);
			arg.setStartOffset(this.lexer.getOffset());
			switch(this.nextType) {
			case SYMBOL:
				arg.argName= createSymbol(arg);
				readLines();
				break;
			case STRING_D:
			case STRING_S:
			case STRING_R:
				arg.argName= createStringConst(arg);
				readLines();
				break;
			case NULL:
				arg.argName= createNullConst(arg);
				readLines();
				break;
			case EQUAL:
				arg.argName= errorNonExistingSymbol(arg, this.lexer.getOffset(),
						TYPE12_SYNTAX_ELEMENTNAME_MISSING );
				break;
			default:
				break;
			}
			if (arg.argName != null) {
				if (this.nextType == RTerminal.EQUAL) {
					RParserPostExprVisitor.updateParentStatus(arg.argName);
					arg.equalsOffset= this.lexer.getOffset();
					arg.setEndOffset(arg.equalsOffset + 1);
					consumeToken();
					
					final ExprContext valueContext= new ExprContext(arg, arg.valueExpr, LINE_MODE_EAT);
					scanInExpression(valueContext);
					if (arg.valueExpr.node != null) { // empty items are allowed
						checkExpression(valueContext);
						arg.setEndOffset(arg.valueExpr.node.getEndOffset());
					}
				}
				else {
					// argName -> valueExpr
					arg.valueExpr.node= arg.argName;
					arg.argName= null;
					
					final ExprContext valueContext= new ExprContext(arg, arg.valueExpr, LINE_MODE_EAT);
					valueContext.update(arg.valueExpr.node, null);
					scanInExpression(valueContext);
					checkExpression(valueContext);
					arg.setEndOffset(arg.valueExpr.node.getEndOffset());
				}
			}
			else {
				final ExprContext valueContext= new ExprContext(arg, arg.valueExpr, LINE_MODE_EAT);
				scanInExpression(valueContext);
				if (arg.valueExpr.node != null) { // empty items are allowed
					checkExpression(valueContext);
					arg.setEndOffset(arg.valueExpr.node.getEndOffset());
				}
				else {
					arg.setStartEndOffset(args.getEndOffset());
				}
			}
			
			if (this.nextType == RTerminal.COMMA) {
				builder.args.add(arg);
				RParserPostExprVisitor.updateParentStatus(arg);
				builder.sepOffsets.add(this.lexer.getOffset());
				args.setEndOffset(this.lexer.getOffset() + 1);
				consumeToken();
				readLines();
				continue ITER_ARGS;
			}
			else {
				if (arg.hasChildren() || !builder.args.isEmpty()) {
					builder.args.add(arg);
					RParserPostExprVisitor.updateParentStatus(arg);
					args.setStartEndOffset(builder.args.get(0).getStartOffset(), arg.getEndOffset());
				}
				break ITER_ARGS;
			}
		}
		args.finish(builder);
		returnArgsBuilder(builder);
	}
	
	final boolean recoverCCont() {
		return !this.wasLinebreak
			&& (this.nextType == RTerminal.SYMBOL || this.nextType == RTerminal.SYMBOL_G || this.nextType == RTerminal.BLOCK_OPEN);
	}
	
	final void appendNonOp(final ExprContext context, final RAstNode newNode) {
		if (context.openExpr != null) {
			newNode.rParent= context.lastNode;
			context.openExpr.node= newNode;
		}
		else {
			// setup missing op
			final Dummy.Operator error= new Dummy.Operator(TYPE12_SYNTAX_OPERATOR_MISSING);
			error.rParent= context.rootNode;
			error.leftExpr.node= context.rootExpr.node;
			error.setStartEndOffset(newNode.getStartOffset());
			context.rootExpr.node= error;
			// append news
			newNode.rParent= error;
			error.rightExpr.node= newNode;
			context.rootExpr.node= error;
		}
		context.update(newNode, newNode.getRightExpr());
		return;
	}
	
	final void appendOp(final ExprContext context, final RAstNode newNode) {
		if (context.openExpr != null) {
			context.openExpr.node= errorNonExistExpression(context.lastNode, newNode.getStartOffset(),
					newNode.getMissingExprStatus(newNode.getLeftExpr()) );
			context.update(context.openExpr.node, null);
		}
		
		final int newP= newNode.getNodeType().opPrec;
		RAstNode left= context.lastNode;
		RAstNode cand= context.lastNode;
		
		ITER_CAND : while (cand != null && cand != context.rootNode) {
			final NodeType candType= cand.getNodeType();
			if (candType.opPrec == newP) {
				switch (candType.opAssoc) {
				case Assoc.NOSTD:
					left= cand;
					if ((newNode.getStatusCode() & ERROR) == 0) {
						newNode.setStatus(TYPE123_SYNTAX_SEQREL_UNEXPECTED);
					}
					break ITER_CAND;
				case Assoc.LEFTSTD:
					left= cand;
					break ITER_CAND;
				case Assoc.RIGHTSTD:
				default:
					break ITER_CAND;
				}
			}
			if (candType.opPrec > newP) {
				break ITER_CAND;
			}
			left= cand;
			cand= cand.rParent;
		}
		
		final RAstNode baseNode= left.rParent;
		if (baseNode == null) {
			throw new IllegalStateException(); // DEBUG
		}
		final Expression baseExpr= baseNode.getExpr(left);
		newNode.getLeftExpr().node= left;
		left.rParent= newNode;
		baseExpr.node= newNode;
		newNode.rParent= baseNode;
		
		context.update(newNode, newNode.getRightExpr());
		return;
	}
	
	Dummy.Terminal errorNonExistExpression(final RAstNode parent, final int stopHint, final int statusCode) {
		final Dummy.Terminal error= new Dummy.Terminal(statusCode, parent,
				(stopHint != NA_OFFSET) ? stopHint : parent.getEndOffset() );
		parent.setStatusSeverityInChild(ERROR_IN_CHILD);
		return error;
	}
	
	Dummy.Terminal errorFromNext(final @Nullable RAstNode parent) {
		final Dummy.Terminal error= new Dummy.Terminal((this.nextType == RTerminal.UNKNOWN) ?
						TYPE12_SYNTAX_TOKEN_UNKNOWN : TYPE12_SYNTAX_TOKEN_UNEXPECTED,
				parent,
				this.lexer.getOffset(), this.lexer.getEndOffset() );
		if (this.createText) {
			error.text= this.lexer.getText();
		}
		consumeToken();
		if (parent != null) {
			parent.setStatusSeverityInChild(ERROR_IN_CHILD);
		}
		return error;
	}
	
	Symbol errorNonExistingSymbol(final RAstNode parent, final int offset, final int statusCode) {
		final Symbol error= new Symbol.Std(statusCode, parent, offset, offset);
		error.setText("", null); //$NON-NLS-1$
		error.setStatus(statusCode);
		parent.setStatusSeverityInChild(ERROR_IN_CHILD);
		return error;
	}
	
	protected Symbol createSymbol(final @Nullable RAstNode parent) {
		final Symbol symbol= switch (this.nextType) {
				case SYMBOL_G ->			new Symbol.G();
				case SYMBOL ->				new Symbol.Std();
				default ->
						throw new IllegalStateException();
				};
		symbol.rParent= parent;
		setupFromSourceToken(symbol);
		RParserPostExprVisitor.updateParentStatus(symbol);
		consumeToken();
		return symbol;
	}
	
	protected NumberConst createNumberConst(final @Nullable RAstNode parent) {
		final NumberConst num= new NumberConst(this.nextType);
		num.rParent= parent;
		setupFromSourceToken(num);
		consumeToken();
		return num;
	}
	
	protected NullConst createNullConst(final RAstNode parent) {
		final NullConst num= new NullConst();
		num.rParent= parent;
		setupFromSourceToken(num);
		consumeToken();
		return num;
	}
	
	protected StringConst createStringConst(final @Nullable RAstNode parent) {
		final StringConst str= switch (this.nextType) {
				case STRING_D ->			new StringConst.D();
				case STRING_S ->			new StringConst.S();
				case STRING_R ->			new StringConst.R();
				default ->
						throw new IllegalStateException();
				};
		str.rParent= parent;
		setupFromSourceToken(str);
		consumeToken();
		return str;
	}
	
	protected Assignment createAssignment() {
		final Assignment node= switch (this.nextType) {
				case ARROW_LEFT_S ->		new Assignment.LeftS();
				case ARROW_LEFT_D ->		new Assignment.LeftD();
				case ARROW_RIGHT_S ->		new Assignment.RightS();
				case ARROW_RIGHT_D ->		new Assignment.RightD();
				case EQUAL ->				new Assignment.LeftE();
				case COLON_EQUAL ->			new Assignment.LeftC();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected Model createModel() {
		final Model node= new Model();
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected CLoopCommand createLoopCommand() {
		final CLoopCommand node= switch (this.nextType) {
				case NEXT ->				new CLoopCommand.Next();
				case BREAK ->				new CLoopCommand.Break();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected Sign createSign() {
		final Sign node= switch (this.nextType) {
				case PLUS ->				new Sign.PlusSign();
				case MINUS ->				new Sign.MinusSign();
				case NOT ->					new Sign.Not();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected Arithmetic createArithmetic() {
		final Arithmetic node= switch (this.nextType) {
				case PLUS ->				new Arithmetic.Plus();
				case MINUS ->				new Arithmetic.Minus();
				case MULT ->				new Arithmetic.Mult();
				case DIV ->					new Arithmetic.Div();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected Power createPower() {
		final Power node= new Power();
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected Seq createSeq() {
		final Seq node= new Seq();
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected Special createSpecial() {
		final Special node= new Special();
		setupFromSourceToken(node);
		if (this.createText) {
			node.qualifier= this.lexer.getText(this.symbolTextFactory);
		}
		consumeToken();
		return node;
	}
	
	protected Relational createRelational() {
		final Relational node= switch (this.nextType) {
				case REL_LT ->				new Relational.LT();
				case REL_LE ->				new Relational.LE();
				case REL_EQ ->				new Relational.EQ();
				case REL_GE ->				new Relational.GE();
				case REL_GT ->				new Relational.GT();
				case REL_NE ->				new Relational.NE();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected Logical createLogical() {
		final Logical node= switch (this.nextType) {
				case AND ->					new Logical.And();
				case AND_D ->				new Logical.AndD();
				case OR ->					new Logical.Or();
				case OR_D ->				new Logical.OrD();
				default ->
						throw new IllegalStateException();
				};
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	protected Help createHelp() {
		final Help node= new Help();
		setupFromSourceToken(node);
		consumeToken();
		return node;
	}
	
	private final void setupFromSourceToken(final RAstNode node) {
		node.setStatus(this.lexer.getFlags());
		node.setStartEndOffset(this.lexer.getOffset(), this.lexer.getEndOffset());
	}
	
	private final void setupFromSourceToken(final Symbol node) {
		node.setStatus(this.lexer.getFlags());
		node.setStartEndOffset(this.lexer.getOffset(), this.lexer.getEndOffset());
		if (this.createText) {
			node.setText(this.lexer.getText(this.symbolTextFactory),
					this.lexer.getTextRegion() );
			final StatusDetail statusDetail= this.lexer.getStatusDetail();
			if (statusDetail != null) {
				node.addAttachment(statusDetail);
			}
		}
	}
	
	private final void setupFromSourceToken(final SingleValue node) {
		node.setStatus(this.lexer.getFlags());
		node.setStartEndOffset(this.lexer.getOffset(), this.lexer.getEndOffset());
		if (this.createText) {
			node.setText(this.lexer.getText(),
					this.lexer.getTextRegion() );
			final StatusDetail statusDetail= this.lexer.getStatusDetail();
			if (statusDetail != null) {
				node.addAttachment(statusDetail);
			}
		}
	}
	
	private final int checkExpression(final ExprContext context) {
		int state= 0;
		if (context.openExpr != null && context.openExpr.node == null) {
			context.openExpr.node= errorNonExistExpression(context.lastNode, context.lastNode.getEndOffset(),
					context.lastNode.getMissingExprStatus(context.openExpr) );
			state= -1;
		}
		context.rootNode.setStatusSeverityInChild(this.postVisitor.check(context.rootExpr.node));
		return state;
	}
	
	private final void readLines() {
		while (this.nextType == RTerminal.LINEBREAK) {
			consumeToken();
		}
	}
	
	private final void consumeToken() {
		this.wasLinebreak= (this.nextType == RTerminal.LINEBREAK);
		this.nextType= this.lexer.next();
		switch (this.nextType) {
		case COMMENT:
		case ROXYGEN_COMMENT:
			if ((this.commentsLevel & ROXYGEN_COMMENTS) != 0) {
				consumeCommentWithRoxygen();
			}
			else {
				consumeComment();
			}
			return;
		default:
			return;
		}
	}
	
	
	private void consumeCommentWithRoxygen() {
		while (true) {
			final Comment comment;
			switch (this.nextType) {
			case COMMENT:
				if (this.roxygen.hasComment()) {
					this.comments.add(this.roxygen.finish(this.lexer));
				}
				comment= new Comment.CommonLine();
				setupFromSourceToken(comment);
				this.comments.add(comment);
				
				this.nextType= this.lexer.next();
				continue;
				
			case ROXYGEN_COMMENT:
				comment= new Comment.RoxygenLine();
				setupFromSourceToken(comment);
				this.roxygen.add(comment);
				
				this.nextType= this.lexer.next();
				continue;
				
			case LINEBREAK:
				this.nextType= this.lexer.next();
				if (this.nextType == RTerminal.LINEBREAK && this.roxygen.hasComment()) {
					this.comments.add(this.roxygen.finish(null));
				}
				continue;
				
			default:
				if (this.roxygen.hasComment()) {
					this.comments.add(this.roxygen.finish(this.lexer));
				}
				
				this.wasLinebreak= true;
				return;
			}
		}
	}
	
	private void consumeComment() {
		while (true) {
			switch (this.nextType) {
			case COMMENT:
			case ROXYGEN_COMMENT:
				if (this.commentsLevel != 0) {
					final Comment comment= (this.nextType == RTerminal.ROXYGEN_COMMENT) ?
							new Comment.RoxygenLine() :
							new Comment.CommonLine();
					setupFromSourceToken(comment);
					this.comments.add(comment);
				} // no break
				
				this.nextType= this.lexer.next();
				continue;
				
			case LINEBREAK:
				this.nextType= this.lexer.next();
				continue;
				
			default:
				this.wasLinebreak= true;
				return;
			}
		}
	}
	
	
	private final List<ArgsBuilder<?>> argBuilders= new ArrayList<>(16);
	private int argBuildersIdx;
	
	private void clearArgsBuilder() {
		int idx= this.argBuildersIdx;
		while (idx > 0) {
			final var builder= this.argBuilders.get(--idx);
			builder.args.clear();
			builder.sepOffsets.clear();
		}
		this.argBuildersIdx= 0;
	}
	
	@SuppressWarnings("unchecked")
	private <TArg> ArgsBuilder<TArg> getArgsBuilder() {
		final ArgsBuilder<TArg> builder;
		if (this.argBuildersIdx < this.argBuilders.size()) {
			builder= (ArgsBuilder<TArg>)this.argBuilders.get(this.argBuildersIdx);
		}
		else {
			builder= new ArgsBuilder<>();
			this.argBuilders.add(builder);
		}
		this.argBuildersIdx++;
		return builder;
	}
	
	private void returnArgsBuilder(final ArgsBuilder<?> builder) {
		builder.args.clear();
		builder.sepOffsets.clear();
		this.argBuildersIdx--;
	}
	
}
