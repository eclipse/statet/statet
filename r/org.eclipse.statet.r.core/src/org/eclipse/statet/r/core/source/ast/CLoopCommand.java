/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>next</code>
 * <code>break</code>
 */
@NonNullByDefault
public abstract class CLoopCommand extends RAstNode {
	
	
	static final class Break extends CLoopCommand {
		
		
		Break() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.C_BREAK;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.BREAK;
		}
		
		
		@Override
		public final RTerminal getTerminal() {
			return RTerminal.BREAK;
		}
		
	}
	
	static final class Next extends CLoopCommand {
		
		
		Next() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.C_NEXT;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.NEXT;
		}
		
		
		@Override
		public final RTerminal getTerminal() {
			return RTerminal.NEXT;
		}
		
	}
	
	
	CLoopCommand() {
	}
	
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	
	@Override
	public final boolean hasChildren() {
		return false;
	}
	
	@Override
	public final int getChildCount() {
		return 0;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		throw new IndexOutOfBoundsException();
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		return -1;
	}
	
	public abstract RTerminal getTerminal();
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) {
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) {
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		return null;
	}
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (getNodeType() == element.getNodeType());
	}
	
	@Override
	public final boolean equalsValue(final RAstNode element) {
		return (getNodeType() == element.getNodeType());
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		throw new IllegalArgumentException();
	}
	
}
