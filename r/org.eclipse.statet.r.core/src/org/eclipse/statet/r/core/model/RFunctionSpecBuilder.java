/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Builder;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RFunctionSpec.Parameter;
import org.eclipse.statet.r.core.model.RFunctionSpec.ReturnValue;


/**
 * Builder for {@link RFunctionSpec}
 */
@NonNullByDefault
public final class RFunctionSpecBuilder implements Builder<RFunctionSpec> {
	
	
	private final List<Parameter> parameters= new ArrayList<>();
	private ImList<? extends ReturnValue> returns= ImCollections.emptyList();
	
	
	public RFunctionSpecBuilder() {
	}
	
	
	public RFunctionSpecBuilder addParam(final @Nullable String name, final int type,
			final @Nullable String className) {
		this.parameters.add(new Parameter(this.parameters.size(), name, type, className));
		return this;
	}
	
	public RFunctionSpecBuilder addParam(final @Nullable String name, final int type) {
		this.parameters.add(new Parameter(this.parameters.size(), name, type, null));
		return this;
	}
	
	public RFunctionSpecBuilder addParam(final @Nullable String name) {
		this.parameters.add(new Parameter(this.parameters.size(), name, 0, null));
		return this;
	}
	
	public RFunctionSpecBuilder addParams(final @Nullable String... name) {
		for (int i= 0; i < name.length; i++) {
			this.parameters.add(new Parameter(this.parameters.size(), name[i], 0, null));
		}
		return this;
	}
	
	
	public RFunctionSpecBuilder returns(final ImList<? extends ReturnValue> returnTypes) {
		this.returns= nonNullAssert(returnTypes);
		return this;
	}
	
	public RFunctionSpecBuilder returns(final int type,
			final @Nullable String className) {
		this.returns= ImCollections.newList(new ReturnValue(type, className));
		return this;
	}
	
	
	@Override
	public RFunctionSpec build() {
		return new RFunctionSpec(
				this.parameters.toArray(new @NonNull Parameter[this.parameters.size()]),
				this.returns );
	}
	
}
