/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>{ ... }</code>
 */
@NonNullByDefault
public final class Block extends ExpressionList {
	
	
	int blockCloseOffset= NA_OFFSET;
	
	
	Block() {
		super();
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.BLOCK;
	}
	
	@Override
	public final RTerminal getOperator(final int index) {
		return RTerminal.BLOCK_OPEN;
	}
	
	public int getBlockCloseOffset() {
		return this.blockCloseOffset;
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.BLOCK == element.getNodeType());
	}
	
	
	final void updateOffsets() {
		if (this.blockCloseOffset != NA_OFFSET) {
			doSetEndOffset(this.blockCloseOffset + 1);
		}
		else {
			final int count= getChildCount();
			if (count > 0) {
				doSetEndOffset(getChild(count - 1).getEndOffset());
			}
			else {
				doSetEndOffset(getStartOffset() + 1);
			}
		}
	}
	
}
