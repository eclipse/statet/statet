/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rlang;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.r.core.model.RSourceUnit;


@NonNullByDefault
public interface RLangSrcStrElement
		extends RLangSrcElement<RLangSrcStrElement>,
				SourceStructElement<RLangSrcStrElement, RLangSrcStrElement> {
	
	
	@Override
	public RSourceUnit getSourceUnit();
	
	@Override
	public boolean hasModelChildren(@Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter);
	@Override
	public List<? extends RLangSrcStrElement> getModelChildren(@Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter);
	
	@Override
	public boolean hasSourceChildren(@Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter);
	@Override
	public List<? extends RLangSrcStrElement> getSourceChildren(@Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter);
	
}
