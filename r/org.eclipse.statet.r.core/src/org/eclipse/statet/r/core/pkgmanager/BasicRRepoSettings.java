/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Collection;
import java.util.Objects;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicRRepoSettings implements RRepoSettings {
	
	
	private final ImList<RRepo> repos;
	
	private final @Nullable RRepo cranMirror;
	
	private final @Nullable String biocVersion;
	private final @Nullable RRepo biocMirror;
	
	
	public BasicRRepoSettings(final ImList<RRepo> repos, final @Nullable RRepo cranMirror,
			final @Nullable String bioCVersion, final @Nullable RRepo bioCMirror) {
		this.repos= nonNullAssert(repos);
		this.cranMirror= cranMirror;
		this.biocVersion= bioCVersion;
		this.biocMirror= bioCMirror;
	}
	
	public BasicRRepoSettings(final Collection<RRepo> repos, final @Nullable RRepo cranMirror,
			final @Nullable String bioCVersion, final @Nullable RRepo bioCMirror) {
		this.repos= ImCollections.toList(repos);
		this.cranMirror= cranMirror;
		this.biocVersion= bioCVersion;
		this.biocMirror= bioCMirror;
	}
	
	
	@Override
	public ImList<RRepo> getRepos() {
		return this.repos;
	}
	
	@Override
	public @Nullable RRepo getRepo(final String repoId) {
		if (repoId == RRepo.WS_CACHE_SOURCE_ID) {
			return RRepo.WS_CACHE_SOURCE_REPO;
		}
		if (repoId == RRepo.WS_CACHE_BINARY_ID) {
			return RRepo.WS_CACHE_BINARY_REPO;
		}
		return RPkgUtils.getRepoById(this.repos, repoId);
	}
	
	@Override
	public @Nullable RRepo getCRANMirror() {
		return this.cranMirror;
	}
	
	@Override
	public @Nullable String getBioCVersion() {
		return this.biocVersion;
	}
	
	@Override
	public @Nullable RRepo getBioCMirror() {
		return this.biocMirror;
	}
	
	
	@Override
	public int hashCode() {
		int h= this.repos.hashCode();
		h= h * 31 + Objects.hashCode(this.cranMirror);
		h= h * 31 + Objects.hashCode(this.biocVersion);
		h= h * 31 + Objects.hashCode(this.biocMirror);
		return h;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RRepoSettings other
						&& this.repos.equals(other.getRepos())
						&& Objects.equals(this.cranMirror, other.getCRANMirror())
						&& Objects.equals(this.biocVersion, other.getBioCVersion())
						&& Objects.equals(this.biocMirror, other.getBioCMirror()) ));
	}
	
}
