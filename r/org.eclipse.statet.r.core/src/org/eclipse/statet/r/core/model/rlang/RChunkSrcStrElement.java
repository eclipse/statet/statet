/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rlang;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.r.core.rmodel.EmbeddedRBuildElement;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public abstract class RChunkSrcStrElement extends EmbeddedRBuildElement
		implements RLangSrcStrElement {
	
	
	public RChunkSrcStrElement(final SourceStructElement<?, ?> parent, final AstNode node,
			final RElementName name, final @Nullable TextRegion nameRegion) {
		super(parent, node, name, nameRegion);
	}
	
	
	protected AstNode getNode() {
		return this.node;
	}
	
	protected abstract ImList<SourceComponent> getSourceComponents();
	
	
	@Override
	public final int getElementType() {
		return RElement.C12_SOURCE_CHUNK;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == SourceComponent.class) {
			return (T)getSourceComponents();
		}
		return super.getAdapter(adapterType);
	}
	
}
