/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§namespace§ :: §element§</code>
 * <code>§namespace§ ::: §element§</code>
 */
@NonNullByDefault
public abstract class NSGet extends RAstNode {
	
	static final class Std extends NSGet {
		
		
		Std() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.NS_GET;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.NS_GET;
		}
		
	}
	
	static final class Internal extends NSGet {
		
		
		Internal() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.NS_GET_INT;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.NS_GET_INT;
		}
		
	}
	
	
	SingleValue namespace;
	int operatorOffset;
	SingleValue element;
	
	
	NSGet() {
	}
	
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 2;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		return switch (index) {
		case 0 -> this.namespace;
		case 1 -> this.element;
		default ->
				throw new IndexOutOfBoundsException();
		};
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.namespace == child) {
			return 0;
		}
		if (this.element == child) {
			return 1;
		}
		return -1;
	}
	
	public final RAstNode getNamespaceChild() {
		return this.namespace;
	}
	
	public final RAstNode getElementChild() {
		return this.element;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.namespace.acceptInR(visitor);
		this.element.acceptInR(visitor);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.namespace);
		visitor.visit(this.element);
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		return null;
	}
	
	@Override
	final @Nullable Expression getLeftExpr() {
		return null;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (getNodeType() == element.getNodeType());
	}
	
	@Override
	public final boolean equalsValue(final RAstNode element) {
		if (getNodeType() == element.getNodeType()) {
			final NSGet other= (NSGet)element;
			return (this.namespace.equalsValue(other.namespace)
					&& this.element.equalsValue(other.element) );
		}
		return false;
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		throw new IllegalArgumentException();
	}
	
	final void updateOffsets() {
		doSetStartEndOffset(this.namespace.getStartOffset(), this.element.getEndOffset());
	}
	
}
