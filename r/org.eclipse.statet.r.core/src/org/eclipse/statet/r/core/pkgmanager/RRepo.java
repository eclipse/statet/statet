/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import java.net.URL;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.RPkgType;


@NonNullByDefault
public class RRepo {
	
	
	public static final String CUSTOM_PREFIX= "custom-"; //$NON-NLS-1$
	public static final String SPECIAL_PREFIX= "special-"; //$NON-NLS-1$
	public static final String R_PREFIX= "r-"; //$NON-NLS-1$
	
	public static final String CRAN_ID= R_PREFIX + "CRAN"; //$NON-NLS-1$
	public static final String BIOC_ID_PREFIX= R_PREFIX + "BioC"; //$NON-NLS-1$
	public static final String WS_CACHE_PREFIX= SPECIAL_PREFIX + "ws-cache-"; //$NON-NLS-1$
	public static final String WS_CACHE_SOURCE_ID= WS_CACHE_PREFIX + "source"; // RPkgType.SOURCE.name().toLowerCase() 
	public static final String WS_CACHE_BINARY_ID= WS_CACHE_PREFIX + "binary"; // RPkgType.BINARY.name().toLowerCase() 
	
	public static final RRepo WS_CACHE_SOURCE_REPO= new RRepo(WS_CACHE_SOURCE_ID,
			"Local Packages", null, RPkgType.SOURCE );
	public static final RRepo WS_CACHE_BINARY_REPO= new RRepo(WS_CACHE_BINARY_ID,
			"Local Packages", null, RPkgType.BINARY );
	
	
	public static String checkRepoURL(final String url) {
		if (url.length() > 0 && url.charAt(url.length() - 1) != '/') {
			return url + '/';
		}
		return url;
	}
	
	public static @Nullable String hintName(final RRepo repo) {
		try {
			final URL url= new URL(repo.getURL());
			if (url.getHost() != null) {
				return url.getHost();
			}
			else if (url.getPath() != null){
				return url.getPath();
			}
		}
		catch (final Exception e) {}
		return null;
	}
	
	
	private final String id;
	
	private String name;
	
	private String url;
	
	private @Nullable RPkgType pkgType;
	
	
	@SuppressWarnings("null")
	public RRepo(final String id, final @Nullable String name, final @Nullable String url,
			final @Nullable RPkgType pkgType) {
		this.id= id.intern();
		setName(name);
		setURL(url);
		this.pkgType= pkgType;
	}
	
	public RRepo(final String id) {
		this(id, null, null, null);
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public void set(final RRepo template) {
		this.name= template.name;
		this.url= template.url;
		this.pkgType= template.pkgType;
	}
	
	public void setName(final @Nullable String name) {
		this.name= (name != null) ? name : ""; //$NON-NLS-1$
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setURL(final @Nullable String url) {
		this.url= (url != null) ? checkRepoURL(url) : ""; //$NON-NLS-1$
	}
	
	public String getURL() {
		return this.url;
	}
	
	public @Nullable RPkgType getPkgType() {
		return this.pkgType;
	}
	
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RRepo other
						&& this.id.equals(other.id) ));
	}
	
	
	@Override
	public String toString() {
		return getName() + " (" + getURL() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
