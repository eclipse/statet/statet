/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§left§ &lt; §right§</code>
 * <code>§left§ &lt;= §right§</code>
 * <code>§left§ == §right§</code>
 * <code>§left§ =&gt; §right§</code>
 * <code>§left§ &gt; §right§</code>
 * <code>§left§ != §right§</code>
 */
@NonNullByDefault
public abstract class Relational extends StdBinary {
	
	
	static final class LT extends Relational {
		
		
		LT() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.REL_LT;
		}
		
		
		@Override
		public boolean equalsValue(final RAstNode element) {
			if (NodeType.RELATIONAL == element.getNodeType()) {
				final Relational other= (Relational)element;
				return (   ((element.getOperator(0) == RTerminal.REL_LT)
								&& this.leftExpr.node.equalsValue(other.leftExpr.node)
								&& this.rightExpr.node.equalsValue(other.rightExpr.node) )
						|| ((element.getOperator(0) == RTerminal.REL_GE)
								&& this.leftExpr.node.equalsValue(other.rightExpr.node)
								&& this.rightExpr.node.equalsValue(other.leftExpr.node) ));
			}
			return false;
		}
		
	}
	
	static final class LE extends Relational {
		
		
		LE() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.REL_LE;
		}
		
		
		@Override
		public boolean equalsValue(final RAstNode element) {
			if (NodeType.RELATIONAL == element.getNodeType()) {
				final Relational other= (Relational)element;
				return (   ((element.getOperator(0) == RTerminal.REL_LE)
								&& this.leftExpr.node.equalsValue(other.leftExpr.node)
								&& this.rightExpr.node.equalsValue(other.rightExpr.node) )
						|| ((element.getOperator(0) == RTerminal.REL_GT)
								&& this.leftExpr.node.equalsValue(other.rightExpr.node)
								&& this.rightExpr.node.equalsValue(other.leftExpr.node) ));
			}
			return false;
		}
		
	}
	
	static final class EQ extends Relational {
		
		
		EQ() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.REL_EQ;
		}
		
		
		@Override
		public boolean equalsValue(final RAstNode element) {
			if (NodeType.RELATIONAL == element.getNodeType()) {
				final Relational other= (Relational)element;
				return (   ((element.getOperator(0) == RTerminal.REL_EQ)
								&& this.leftExpr.node.equalsValue(other.leftExpr.node)
								&& this.rightExpr.node.equalsValue(other.rightExpr.node) )
						|| ((element.getOperator(0) == RTerminal.REL_EQ)
								&& this.leftExpr.node.equalsValue(other.rightExpr.node)
								&& this.rightExpr.node.equalsValue(other.leftExpr.node) ));
			}
			return false;
		}
		
	}
	
	static final class GE extends Relational {
		
		
		GE() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.REL_GE;
		}
		
		
		@Override
		public boolean equalsValue(final RAstNode element) {
			if (NodeType.RELATIONAL == element.getNodeType()) {
				final Relational other= (Relational)element;
				return (   ((element.getOperator(0) == RTerminal.REL_GE)
								&& this.leftExpr.node.equalsValue(other.leftExpr.node)
								&& this.rightExpr.node.equalsValue(other.rightExpr.node) )
						|| ((element.getOperator(0) == RTerminal.REL_LT)
								&& this.leftExpr.node.equalsValue(other.rightExpr.node)
								&& this.rightExpr.node.equalsValue(other.leftExpr.node) ));
			}
			return false;
		}
		
	}
	
	static final class GT extends Relational {
		
		
		GT() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.REL_GT;
		}
		
		
		@Override
		public boolean equalsValue(final RAstNode element) {
			if (NodeType.RELATIONAL == element.getNodeType()) {
				final Relational other= (Relational)element;
				return (   ((element.getOperator(0) == RTerminal.REL_GT)
								&& this.leftExpr.node.equalsValue(other.leftExpr.node)
								&& this.rightExpr.node.equalsValue(other.rightExpr.node) )
						|| ((element.getOperator(0) == RTerminal.REL_LE)
								&& this.leftExpr.node.equalsValue(other.rightExpr.node)
								&& this.rightExpr.node.equalsValue(other.leftExpr.node) ));
			}
			return false;
		}
		
	}
	
	static final class NE extends Relational {
		
		
		NE() {
		}
		
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.REL_NE;
		}
		
		
		@Override
		public boolean equalsValue(final RAstNode element) {
			if (NodeType.RELATIONAL == element.getNodeType()) {
				final Relational other= (Relational)element;
				return (   ((element.getOperator(0) == RTerminal.REL_NE)
								&& this.leftExpr.node.equalsValue(other.leftExpr.node)
								&& this.rightExpr.node.equalsValue(other.rightExpr.node) )
						|| ((element.getOperator(0) == RTerminal.REL_NE)
								&& this.leftExpr.node.equalsValue(other.rightExpr.node)
								&& this.rightExpr.node.equalsValue(other.leftExpr.node) ));
			}
			return false;
		}
		
	}
	
	
	Relational() {
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.RELATIONAL;
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.RELATIONAL == element.getNodeType());
	}
	
}
