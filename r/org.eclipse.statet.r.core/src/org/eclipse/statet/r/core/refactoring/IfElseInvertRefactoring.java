/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.osgi.util.NLS;
import org.eclipse.text.edits.RangeMarker;
import org.eclipse.text.edits.ReplaceEdit;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.refactoring.Messages;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringDescriptor;
import org.eclipse.statet.ltk.refactoring.core.QuickRefactoring;
import org.eclipse.statet.ltk.refactoring.core.RefactoringChange;
import org.eclipse.statet.ltk.refactoring.core.RefactoringMessages;
import org.eclipse.statet.ltk.refactoring.core.SourceUnitChange;
import org.eclipse.statet.ltk.refactoring.core.TextChangeCompatibility;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.source.ast.CIfElse;


@NonNullByDefault
public class IfElseInvertRefactoring extends QuickRefactoring {
	
	
	private final RRefactoringAdapter adapter= new RRefactoringAdapter();
	private final ElementSet elementSet;
	
	private final RSourceUnit sourceUnit;
	
	private final CIfElse sourceNode;
	
	
	public IfElseInvertRefactoring(final RSourceUnit su, final CIfElse sourceNode) {
		this.sourceUnit= su;
		this.elementSet= new ElementSet(su);
		
		this.sourceNode= sourceNode;
	}
	
	
	@Override
	public String getBundleId() {
		return RCore.BUNDLE_ID;
	}
	
	@Override
	public String getIdentifier() {
		return RRefactoring.INVERT_IFELSE_REFACTORING_ID;
	}
	
	@Override
	public String getName() {
		return Messages.IfElseInvert_label;
	}
	
	
	@Override
	public RefactoringStatus checkInitialConditions(
			final @Nullable IProgressMonitor monitor) {
		final SubMonitor m= SubMonitor.convert(monitor, 1);
		try {
			final CIfElse sourceNode= this.sourceNode;
			if (!sourceNode.hasElse()) {
				return RefactoringStatus.createFatalErrorStatus(Messages.IfElseInvert_error_MissingElse_message);
			}
			
			final RefactoringStatus result= new RefactoringStatus();
			this.adapter.checkInitialToModify(result, this.elementSet);
			
			if (result.hasFatalError()) {
				return result;
			}
			
			return result;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public RefactoringStatus checkFinalConditions(
			final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_FinalCheck_label, 3);
		try {
			final RefactoringStatus status= new RefactoringStatus();
			this.adapter.checkFinalToModify(status, this.elementSet, m.newChild(2));
			return status;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public Change createChange(
			final IProgressMonitor monitor) throws CoreException, OperationCanceledException {
				final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_CreateChanges_label, 3);
		try {
			final var textFileChange= new SourceUnitChange(this.sourceUnit);
			if (this.sourceUnit.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
				textFileChange.setSaveMode(TextFileChange.LEAVE_DIRTY);
			}
			createChanges(textFileChange, m.newChild(2));
			
			textFileChange.setInsertPosition(new Position(this.sourceNode.getStartOffset(), 0));
			
			final Map<String, String> arguments= new HashMap<>();
			
			final String description= Messages.IfElseInvert_Descriptor_description;
			final IProject resource= this.elementSet.getSingleProject();
			final String project= (resource != null) ? resource.getName() : null;
			final String source= (project != null) ?
					NLS.bind(RefactoringMessages.Common_Source_Project_label, project) :
					RefactoringMessages.Common_Source_Workspace_label;
			final int flags= 0;
			final String comment= ""; //$NON-NLS-1$
			final CommonRefactoringDescriptor descriptor= new CommonRefactoringDescriptor(
					getIdentifier(), project, description, comment, arguments, flags);
			
			return new RefactoringChange(descriptor, getName(),
					new Change[] { textFileChange } );
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public SourceUnitChange createTextChange(
			final SubMonitor m) throws CoreException {
		m.setWorkRemaining(2);
		try {
			final var textFileChange= new SourceUnitChange(this.sourceUnit);
			
			createChanges(textFileChange, m.newChild(2));
			
			return textFileChange;
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
	}
	
	private void createChanges(final SourceUnitChange change,
			final SubMonitor m) throws BadLocationException, CoreException {
		m.setWorkRemaining(3 + 2 * 4);
		
		this.sourceUnit.connect(m.newChild(1));
		try {
			final var util= new RSourceCodeModifyUtil(this.adapter, this.sourceUnit, m.newChild(1));
			
			final CIfElse sourceNode= this.sourceNode;
			
			{	util.addInverseConditionEdits(sourceNode.getCondChild());
				
				TextChangeCompatibility.addTextEdits(change, Messages.IfElseInvert_Changes_InvertCond_name,
						util.getEdits() );
				m.worked(4);
			}
			{	final var thenNode= sourceNode.getThenChild();
				final var elseNode= sourceNode.getElseChild();
				
				TextChangeCompatibility.addTextEdit(change, Messages.IfElseInvert_Changes_ExchangeThen_name,
						new ReplaceEdit(thenNode.getStartOffset(), thenNode.getLength(),
								util.getSource(elseNode) ) );
				TextChangeCompatibility.addTextEdit(change, Messages.IfElseInvert_Changes_ExchangeElse_name,
						new ReplaceEdit(elseNode.getStartOffset(), elseNode.getLength(),
								util.getSource(thenNode) ) );
				m.worked(4);
			}
			
			TextChangeCompatibility.addMarker(change, new RangeMarker(sourceNode.getEndOffset(), 0));
		}
		finally {
			this.sourceUnit.disconnect(m.newChild(1));
		}
	}
	
}
