/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_ELSE;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_IF;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_BODY_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>if ( §cond§ ) §then§</code>
 * <code>if ( §cond§ ) §then§ else §else§</code>
 */
@NonNullByDefault
public final class CIfElse extends RAstNode {
	
	
	boolean withElse= false;
	
	int condOpenOffset= NA_OFFSET;
	final Expression condExpr= new Expression();
	int condCloseOffset= NA_OFFSET;
	final Expression thenExpr= new Expression();
	int elseOffset= NA_OFFSET;
	final Expression elseExpr= new Expression();
	
	
	CIfElse() {
		super();
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.C_IF;
	}
	
	@Override
	public final @Nullable RTerminal getOperator(final int index) {
		return null;
	}
	
	
	public final boolean hasElse() {
		return this.withElse;
	}
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return this.withElse ? 3 : 2;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		switch (index) {
		case 0:
			return this.condExpr.node;
		case 1:
			return this.thenExpr.node;
		case 2:
			if (this.withElse) {
				return this.elseExpr.node;
			}
			//$FALL-THROUGH$
		default:
			throw new IndexOutOfBoundsException();
		}
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.condExpr.node == child) {
			return 0;
		}
		if (this.thenExpr.node == child) {
			return 1;
		}
		if (this.elseExpr.node == child) {
			return 2;
		}
		return -1;
	}
	
	public final int getCondOpenOffset() {
		return this.condOpenOffset;
	}
	
	public final RAstNode getCondChild() {
		return this.condExpr.node;
	}
	
	public final int getCondCloseOffset() {
		return this.condCloseOffset;
	}
	
	public final RAstNode getThenChild() {
		return this.thenExpr.node;
	}
	
	public final int getElseOffset() {
		return this.elseOffset;
	}
	
	public final RAstNode getElseChild() {
		return this.elseExpr.node;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.condExpr.node.acceptInR(visitor);
		this.thenExpr.node.acceptInR(visitor);
		if (this.withElse) {
			this.elseExpr.node.acceptInR(visitor);
		}
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.condExpr.node);
		visitor.visit(this.thenExpr.node);
		if (this.withElse) {
			visitor.visit(this.elseExpr.node);
		}
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		if (this.thenExpr.node == child) {
			return this.thenExpr;
		}
		if (this.elseExpr.node == child) {
			return this.elseExpr;
		}
		if (this.condExpr.node == child) {
			return this.condExpr;
		}
		return null;
	}
	
	@Override
	final Expression getLeftExpr() {
		return this.thenExpr;
	}
	
	@Override
	final Expression getRightExpr() {
		if (this.withElse) {
			return this.elseExpr;
		}
		return this.thenExpr;
	}
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (NodeType.C_IF == element.getNodeType());
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		if (this.condExpr == expr) {
			return TYPE123_SYNTAX_EXPR_AS_CONDITION_MISSING | CTX12_IF;
		}
		if (this.thenExpr == expr) {
			return TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_IF;
		}
		if (this.withElse && this.elseExpr == expr) {
			return TYPE123_SYNTAX_EXPR_AS_BODY_MISSING | CTX12_ELSE;
		}
		throw new IllegalArgumentException();
	}
	
	@SuppressWarnings("unused")
	final void updateOffsets() {
		if (this.withElse && this.elseExpr.node != null) {
			doSetEndOffset(this.elseExpr.node.getEndOffset());
		}
		else if (this.withElse && this.elseOffset != NA_OFFSET) {
			doSetEndOffset(this.elseOffset + 4);
		}
		else if (this.thenExpr.node != null) {
			doSetEndOffset(this.thenExpr.node.getEndOffset());
		}
		else if (this.condCloseOffset != NA_OFFSET) {
			doSetEndOffset(this.condCloseOffset + 1);
		}
		else if (this.condExpr.node != null) {
			doSetEndOffset(this.condExpr.node.getEndOffset());
		}
		else if (this.condOpenOffset != NA_OFFSET) {
			doSetEndOffset(this.condOpenOffset + 1);
		}
		else {
			doSetEndOffset(getStartOffset() + 2);
		}
	}
	
}
