/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§expr§ + §expr§ - §expr§</code>
 * <code>§expr§ * §expr§ / §expr§</code>
 */
@NonNullByDefault
public abstract class Arithmetic extends StdBinary {
	
	
	static final class Plus extends Arithmetic {
		
		
		public Plus() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.ADD;
		}
		
		@Override
		public RTerminal getOperator(final int index) {
			return RTerminal.PLUS;
		}
		
	}
	
	static final class Minus extends Arithmetic {
		
		
		public Minus() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.ADD;
		}
		
		@Override
		public RTerminal getOperator(final int index) {
			return RTerminal.MINUS;
		}
		
	}
	
	static final class Mult extends Arithmetic {
		
		
		public Mult() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.MULT;
		}
		
		@Override
		public RTerminal getOperator(final int index) {
			return RTerminal.MULT;
		}
		
	}
	
	static final class Div extends Arithmetic {
		
		
		public Div() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.MULT;
		}
		
		@Override
		public RTerminal getOperator(final int index) {
			return RTerminal.DIV;
		}
		
	}
	
	
	Arithmetic() {
	}
	
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (getNodeType() == element.getNodeType());
	}
	
}
