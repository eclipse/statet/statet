/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.ltk.core.StatusCodes.SUBSEQUENT;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.impl.AbstractAstNode;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * A node of a R AST
 */
@NonNullByDefault
public abstract class RAstNode extends AbstractAstNode
		implements AstNode {
	
	
	static final RAstNode[] NO_CHILDREN= new RAstNode[0];
	
	
	@Nullable RAstNode rParent;
	
	
	protected RAstNode(final int statusCode, final @Nullable RAstNode parent) {
		super(statusCode);
		this.rParent= parent;
	}
	
	protected RAstNode(final @Nullable RAstNode parent) {
		super();
		this.rParent= parent;
	}
	
	protected RAstNode() {
		super();
	}
	
	
	public abstract NodeType getNodeType();
	
	public abstract @Nullable RTerminal getOperator(final int index);
	
	
	/**
	 * @return the parent node, if it is an RAstNode too, otherwise <code>null</code>
	 */
	public @Nullable RAstNode getRParent() {
		return this.rParent;
	}
	
	@Override
	public @Nullable AstNode getParent() {
		return this.rParent;
	}
	
	public final RAstNode getRRoot() {
		RAstNode candidate= this;
		RAstNode p;
		while ((p= candidate.rParent) != null) {
			candidate= p;
		}
		return candidate;
	}
	
	@Override
	public abstract boolean hasChildren();
	@Override
	public abstract int getChildCount();
	@Override
	public abstract RAstNode getChild(int index);
	@Override
	public abstract int getChildIndex(AstNode child);
	
	
	public abstract void acceptInR(RAstVisitor visitor) throws InvocationTargetException;
	
	public abstract void acceptInRChildren(RAstVisitor visitor) throws InvocationTargetException;
	
	
	final void setStatus(final int statusCode) {
		doSetStatusCode(statusCode);
	}
	
	final void setStatusThis(final int statusCode) {
		doSetStatusThis(statusCode);
	}
	
	final void clearStatusSeverityInChild() {
		doClearStatusSeverityInChild();
	}
	
	final void setStatusSeverityInChild(final int statusCode) {
		doSetStatusSeverityInChild(statusCode);
	}
	
	final void setStatusSubsequent() {
		doAddStatusFlag(SUBSEQUENT);
	}
	
	final void setStartOffset(final int offset) {
		doSetStartOffset(offset);
	}
	
	final void setEndOffset(final int offset) {
		doSetEndOffset(offset);
	}
	
	final void setStartEndOffset(final int startOffset, final int endOffset) {
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	final void setStartEndOffset(final int offset) {
		doSetStartEndOffset(offset);
	}
	
	
	int getEqualsIndex(final RAstNode element) {
		int index= 0;
		final int n= getChildCount();
		for (int i= 0; i < n; i++) {
			final RAstNode child= getChild(i);
			if (child == element) {
				return index;
			}
			if (child.equalsSingle(element)) {
				index++;
			}
		}
		return -1;
	}
	
	
	abstract @Nullable Expression getExpr(RAstNode child);
	abstract @Nullable Expression getLeftExpr();
	abstract @Nullable Expression getRightExpr();
	
	public final boolean equalsIgnoreAst(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof RAstNode) || !equalsSingle((RAstNode) obj)) {
			return false;
		}
		
		RAstNode me= this;
		RAstNode other= (RAstNode) obj;
		while (me != other) {
			if (me.rParent == null || other.rParent == null) {
				return (me.rParent == null && other.rParent == null);
			}
			if ((!me.rParent.equalsSingle(other.rParent))
					|| (me.rParent.getEqualsIndex(me) != other.rParent.getEqualsIndex(other))
					) {
				return false;
			}
			me= me.rParent;
			other= other.rParent;
		}
		return true;
	}
	
	abstract boolean equalsSingle(RAstNode element);
	
	public boolean equalsValue(final RAstNode element) {
		if (getNodeType() != element.getNodeType()) {
			return false;
		}
		final int count= getChildCount();
		if (count != element.getChildCount()) {
			return false;
		}
		for (int i= 0; i < count; i++) {
			if (!getChild(i).equalsValue(element.getChild(i))) {
				return false;
			}
		}
		return true;
	}
	
	
	void appendPathElement(final StringBuilder s) {
//		if (parent != null) {
//			s.append(parent.getEqualsIndex(this));
//		}
		s.append('$');
		s.append(getNodeType().ordinal());
	}
	
	public int hashCodeIgnoreAst() {
		final StringBuilder path= new StringBuilder();
		if (this.rParent != null) {
			if (this.rParent.rParent != null) {
				path.append(this.rParent.rParent.getNodeType().ordinal());
			}
			path.append('$');
			path.append(this.rParent.getNodeType().ordinal());
		}
		appendPathElement(path);
		return path.toString().hashCode();
	}
	
	
	@Override
	public String toString() {
		final StringBuilder s= new StringBuilder();
//		s.append("«");
		s.append(getNodeType().label);
//		s.append(" § " + startOffset + "," + endOffset);
//		s.append("»");
		return s.toString();
	}
	
	
	abstract int getMissingExprStatus(Expression expr);
	
}
