/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import java.time.Instant;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.BasicRPkgBuilt;
import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.core.RNumVersion;


@NonNullByDefault
public class BasicRPkgInfo extends BasicRPkgBuilt implements RPkgInfo {
	
	
	private final int flags;
	private final @Nullable Instant installTimestamp;
	private final String repoId;
	
	
	public BasicRPkgInfo(final String name, final RNumVersion version, final @Nullable String built,
			final @Nullable String title, final RLibLocation lib,
			final int flags, final @Nullable Instant installTimestamp, final @Nullable String repoId) {
		super(name, version,
				nonNullElse(built, ""), //$NON-NLS-1$
				nonNullElse(title, ""), //$NON-NLS-1$
				lib);
		this.flags= flags;
		this.installTimestamp= installTimestamp;
		this.repoId= (repoId != null) ? repoId.intern() : ""; //$NON-NLS-1$
	}
	
	
	@Override
	public int getFlags() {
		return this.flags;
	}
	
	@Override
	public @Nullable Instant getInstallTimestamp() {
		return this.installTimestamp;
	}
	
	@Override
	public String getRepoId() {
		return this.repoId;
	}
	
}
