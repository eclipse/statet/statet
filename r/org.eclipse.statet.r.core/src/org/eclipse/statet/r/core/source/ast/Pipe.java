/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_PIPE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§source§ |&gt;- §target§</code>
 */
@NonNullByDefault
public abstract class Pipe extends StdBinary {
	
	
	static final class Forward extends Pipe {
		
		
		Forward() {
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.PIPE_FORWARD;
		}
		
		@Override
		public final RAstNode getTargetChild() {
			return this.rightExpr.node;
		}
		
		@Override
		public final RAstNode getSourceChild() {
			return this.leftExpr.node;
		}
		
		@Override
		final Expression getTargetExpr() {
			return this.rightExpr;
		}
		
		@Override
		final Expression getSourceExpr() {
			return this.leftExpr;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.PIPE_RIGHT;
		}
		
	}
	
	
	Pipe() {
	}
	
	
	public abstract RAstNode getTargetChild();
	
	public abstract RAstNode getSourceChild();
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public abstract /*@NonNull*/ RTerminal getOperator(int index);
	
	abstract Expression getTargetExpr();
	
	abstract Expression getSourceExpr();
	
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		return (getNodeType() == element.getNodeType());
	}
	
	@Override
	public final boolean equalsValue(final RAstNode element) {
		if (getOperator(0) == element.getOperator(0)) {
			final Pipe other= (Pipe)element;
			return (this.leftExpr.node.equalsValue(other.leftExpr.node)
					&& this.rightExpr.node.equalsValue(other.rightExpr.node) );
		}
		return false;
	}
	
	
	@Override
	int getMissingExprStatus(final Expression expr) {
		if (this.leftExpr == expr) {
			return TYPE123_SYNTAX_EXPR_BEFORE_OP_MISSING | CTX12_PIPE;
		}
		if (this.rightExpr == expr) {
			return TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | CTX12_PIPE;
		}
		throw new IllegalArgumentException();
	}
	
}
