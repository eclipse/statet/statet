/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rlang;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.model.RElementName;


@NonNullByDefault
public class BasicRFrame<TModelChild extends RLangElement<?>>
		implements RFrame<TModelChild> {
	
	
	private final int frameType;
	
	private final RElementName elementName;
	
	private final ImList<? extends TModelChild> modelChildren;
	
	
	public BasicRFrame(final int frameType, final RElementName elementName,
			final ImList<? extends TModelChild> modelChildren) {
		this.frameType= frameType;
		this.elementName= elementName;
		this.modelChildren= modelChildren;
	}
	
	
	@Override
	public int getFrameType() {
		return this.frameType;
	}
	
	@Override
	public @Nullable String getFrameId() {
		return null;
	}
	
	@Override
	public @Nullable RElementName getElementName() {
		return this.elementName;
	}
	
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super TModelChild> filter) {
		return LtkModelUtils.hasChildren(this.modelChildren, filter);
	}
	
	@Override
	public List<? extends TModelChild> getModelChildren(
			final @Nullable LtkModelElementFilter<? super TModelChild> filter) {
		return LtkModelUtils.getChildren(this.modelChildren, filter);
	}
	
	@Override
	public List<? extends RFrame<?>> getPotentialParents() {
		return ImCollections.emptyList();
	}
	
}
