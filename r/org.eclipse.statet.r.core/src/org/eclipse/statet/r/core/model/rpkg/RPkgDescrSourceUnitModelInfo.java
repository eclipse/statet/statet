/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model.rpkg;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;


/**
 * Container for model information of a R source unit
 */
@NonNullByDefault
public interface RPkgDescrSourceUnitModelInfo extends SourceUnitModelInfo {
	
	
	@Override
	RPkgDescrSourceStructElement getSourceElement();
	
	<T extends RPkgDescrField<RPkgDescrSourceStructElement> & RPkgDescrSourceStructElement> ImList<@NonNull ? extends T> getFields();
	<T extends RPkgDescrField<RPkgDescrSourceStructElement> & RPkgDescrSourceStructElement> @Nullable T getField(String name);
	default <T extends RPkgDescrField<RPkgDescrSourceStructElement> & RPkgDescrSourceStructElement> @Nullable T getField(final RPkgDescrFieldDefinition def) {
		return getField(def.getName());
	}
	
}
