/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.r.core.source.ast.FCall;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;


@NonNullByDefault
public class RModelUtils {
	
	
	public static TextRegion getNameSourceRegion(final RElementAccess access) {
		final RAstNode nameNode= access.getNameNode();
		if (nameNode != null) {
			final TextRegion nameRegion= RAsts.getElementNameRegion(nameNode);
			if (nameRegion != null) {
				return nameRegion;
			}
			return nameNode;
		}
		return new BasicTextRegion(access.getNode().getStartOffset());
	}
	
	public static boolean matches(final FCall fCall, final RElementName questedName) {
		final RElementAccess name= RElementAccess.getMainElementAccessOfNode(fCall);
		return (name != null
				&& name.equals(questedName, RElementName.IGNORE_SCOPE)
				&& (name.getScope() == null || name.getScope().equals(questedName.getScope())) );
	}
	
	
	private RModelUtils() {
	}
	
}
