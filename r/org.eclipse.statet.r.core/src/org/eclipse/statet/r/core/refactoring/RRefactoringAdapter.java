/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;

import org.eclipse.statet.internal.r.core.refactoring.Messages;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.core.StatusCodes;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.source.RLexer;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.Assignment;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.doc.RDocumentConstants;
import org.eclipse.statet.r.core.source.util.RHeuristicTokenScanner;


/**
 * RefactoringAdapter for R
 */
public class RRefactoringAdapter extends RefactoringAdapter {
	
	
	private volatile @Nullable RLexer lexer;
	
	
	public RRefactoringAdapter() {
		super(RModel.R_TYPE_ID);
	}
	
	
	@Override
	public String getPluginIdentifier() {
		return RCore.BUNDLE_ID;
	}
	
	@Override
	public RHeuristicTokenScanner getScanner(final SourceUnit su) {
		return RHeuristicTokenScanner.create(su.getDocumentContentInfo());
	}
	
	@Override
	public boolean isCommentContent(final ITypedRegion partition) {
		return (partition != null
				&& partition.getType() == RDocumentConstants.R_COMMENT_CONTENT_TYPE );
	}
	
	public TextRegion trimToAstRegion(final AbstractDocument document, final TextRegion region,
			final RHeuristicTokenScanner scanner)
			throws BadLocationException {
		scanner.configure(document, RDocumentConstants.R_CODE_CONTENT_CONSTRAINT);
		int start= region.getStartOffset();
		int stop= region.getEndOffset();
		int result;
		
		while (stop > start) {
			result= scanner.findNonMSpaceBackward(stop, start);
			if (result >= 0) {
				if (scanner.getChar() == ';') {
					stop= result;
					continue;
				}
				else {
					stop= result + 1;
					break;
				}
			}
			else {
				stop= start;
				break;
			}
		}
		
		while (start < stop) {
			result= scanner.findNonMSpaceForward(start, stop);
			if (result >= 0) {
				if (scanner.getChar() == ';') {
					start= result + 1;
					continue;
				}
				else {
					start= result;
					break;
				}
			}
			else {
				start= stop;
				break;
			}
		}
		
		return new BasicTextRegion(start, stop);
	}
	
	public RAstNode searchPotentialNameNode(final SourceUnit su, TextRegion region,
			final boolean allowAssignRegion, final IProgressMonitor monitor)
			throws BadLocationException {
		final SubMonitor m= SubMonitor.convert(monitor, 5);
		
		su.connect(m.newChild(1));
		try {
			final AbstractDocument document= su.getDocument(m.newChild(1));
			final RHeuristicTokenScanner scanner= getScanner(su);
			
			region= trimToAstRegion(document, region, scanner);
			
			final SourceUnitModelInfo modelInfo= su.getModelInfo(RModel.R_TYPE_ID,
					ModelManager.MODEL_FILE, m.newChild(1) );
			if (modelInfo != null) {
				final AstSelection astSelection= AstSelection.search(modelInfo.getAst().getRoot(),
						region.getStartOffset(), region.getEndOffset(),
						AstSelection.MODE_COVERING_SAME_LAST );
				if (astSelection.getCovering() instanceof RAstNode) {
					final RAstNode node= (RAstNode) astSelection.getCovering();
					return getPotentialNameNode(node, allowAssignRegion);
				}
			}
			return null;
		}
		finally {
			m.setWorkRemaining(1);
			su.disconnect(m.newChild(1));
		}
	}
	
	public TextRegion expandSelectionRegion(final AbstractDocument document, final TextRegion region,
			final TextRegion limit, final RHeuristicTokenScanner scanner)
			throws BadLocationException {
		scanner.configure(document, new PartitionConstraint() {
			@Override
			public boolean matches(final String contentType) {
				return (contentType != RDocumentConstants.R_COMMENT_CONTENT_TYPE);
			}
		});
		final int min= limit.getStartOffset();
		final int max= limit.getEndOffset();
		int start= region.getStartOffset();
		int end= region.getEndOffset();
		int result;
		
		try {
			while (start > min) {
				result= scanner.findNonMSpaceBackward(start, min);
				if (result >= 0) {
					if (RDocumentConstants.R_DEFAULT_CONTENT_CONSTRAINT.matches(scanner.getPartition(result).getType())
							&& scanner.getChar() == ';') {
						start= result;
						continue;
					}
					else {
						start= result + 1;
						break;
					}
				}
				else {
					start= min;
					break;
				}
			}
			
			while (end < max) {
				result= scanner.findNonMSpaceForward(end, max);
				if (result >= 0) {
					if (RDocumentConstants.R_DEFAULT_CONTENT_CONSTRAINT.matches(scanner.getPartition(result).getType())
							&& scanner.getChar() == ';') {
						end= result + 1;
						continue;
					}
					else {
						end= result;
						break;
					}
				}
				else {
					end= max;
					break;
				}
			}
		}
		catch (final BadLocationException e) {
			
		}
		
		return new BasicTextRegion(start, end);
	}
	
	
	public String validateIdentifier(final String value, final String identifierMessageName) {
		if (value == null || value.isEmpty()) { 
			return (identifierMessageName != null) ?
					NLS.bind(Messages.RIdentifiers_error_EmptyFor_message, identifierMessageName, Messages.RIdentifiers_error_Empty_message) :
					Messages.RIdentifiers_error_Empty_message;
		}
		var lexer= this.lexer;
		if (lexer == null) {
			lexer= new RLexer();
			this.lexer= lexer;
		}
		synchronized (lexer) {
			lexer.reset(new StringParserInput(value).init());
			final RTerminal nextToken= lexer.next();
			if (nextToken == RTerminal.EOF) {
				return (identifierMessageName != null) ?
						NLS.bind(Messages.RIdentifiers_error_EmptyFor_message, identifierMessageName, Messages.RIdentifiers_error_Empty_message) :
						Messages.RIdentifiers_error_Empty_message;
			}
			if ((nextToken != RTerminal.SYMBOL && nextToken != RTerminal.SYMBOL_G)
					|| ((lexer.getFlags() & StatusCodes.ERROR) != 0)
					|| (lexer.next() != RTerminal.EOF) ) {
				return (identifierMessageName != null) ?
						NLS.bind(Messages.RIdentifiers_error_InvalidFor_message, identifierMessageName, Messages.RIdentifiers_error_Empty_message) :
						Messages.RIdentifiers_error_Invalid_message;
			}
		}
		return null;
	}
	
	static RAstNode getPotentialNameNode(final RAstNode node, final boolean allowAssignRegion) {
		switch (node.getNodeType()) {
		case A_LEFT:
		case A_RIGHT:
		case A_EQUALS:
		case A_COLON:
			if (allowAssignRegion) {
				final Assignment assignment= (Assignment) node;
				if (assignment.isSearchOperator()) {
					switch (assignment.getTargetChild().getNodeType()) {
					case SYMBOL:
					case STRING_CONST:
						return assignment.getTargetChild();
					default:
						break;
					}
				}
			}
			return null;
		case SYMBOL:
		case STRING_CONST:
			return node;
		default:
			return null;
		}
	}
	
	public static String getQuotedIdentifier(final String identifier) {
		int length;
		if (identifier == null || (length= identifier.length()) == 0) {
			return "";
		}
		if (identifier.charAt(0) == '`') {
			if (length > 1 && identifier.charAt(length - 1) == '`') {
				return identifier;
			}
			else {
				return identifier + '`';
			}
		}
		else {
			return '`' + identifier + '`';
		}
	}
	
	public static String getUnquotedIdentifier(final String identifier) {
		int length;
		if (identifier == null || (length= identifier.length()) == 0) {
			return "";
		}
		if (identifier.charAt(0) == '`') {
			if (length > 1 && identifier.charAt(length - 1) == '`') {
				return identifier.substring(1, length - 2);
			}
			else {
				return identifier.substring(1, length - 1);
			}
		}
		else {
			return identifier;
		}
	}
	
	
}
