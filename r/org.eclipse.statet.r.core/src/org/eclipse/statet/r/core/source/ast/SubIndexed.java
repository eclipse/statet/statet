/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_EXPR_AS_REF_MISSING;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.r.core.source.RTerminal;


/**
 * <code>§ref§ [ §args§ ]</code>
 * <code>§ref§ [[ §args§ ]]</code>
 */
@NonNullByDefault
public abstract class SubIndexed extends RAstNode {
	
	
	static final class S extends SubIndexed {
		
		
		S() {
			super();
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.SUB_INDEXED_S;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.SUB_INDEXED_S_OPEN;
		}
		
	}
	
	static final class D extends SubIndexed {
		
		
		D() {
			super();
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.SUB_INDEXED_D;
		}
		
		@Override
		public final RTerminal getOperator(final int index) {
			return RTerminal.SUB_INDEXED_D_OPEN;
		}
		
	}
	
	public static final class Args extends CallArgs {
		
		
		private ImList<Arg> args;
		private ImIntList sepOffsets;
		
		
		@SuppressWarnings("null")
		Args(final SubIndexed parent) {
			super(parent);
		}
		
		void finish(final RParser.ArgsBuilder<Arg> argsBuilder) {
			this.args= ImCollections.toList(argsBuilder.args);
			this.sepOffsets= ImCollections.toIntList(argsBuilder.sepOffsets);
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.SUB_INDEXED_ARGS;
		}
		
		@Override
		public ImIntList getSeparatorOffsets() {
			return this.sepOffsets;
		}
		
		
		@Override
		public final boolean hasChildren() {
			return (!this.args.isEmpty());
		}
		
		@Override
		public final int getChildCount() {
			return this.args.size();
		}
		
		@Override
		public final Arg getChild(final int index) {
			return this.args.get(index);
		}
		
		@Override
		public final int getChildIndex(final AstNode child) {
			for (int i= this.args.size() - 1; i >= 0; i--) {
				if (this.args.get(i) == child) {
					return i;
				}
			}
			return -1;
		}
		
		@Override
		public ImList<Arg> getArgChildren() {
			return this.args;
		}
		
		@Override
		public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
			visitor.visit(this);
		}
		
		@Override
		public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
			for (final RAstNode arg : this.args) {
				arg.acceptInR(visitor);
			}
		}
		
		@Override
		public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
			for (final RAstNode child : this.args) {
				visitor.visit(child);
			}
		}
		
		
		@Override
		final boolean equalsSingle(final RAstNode element) {
			return (NodeType.SUB_INDEXED_ARGS == element.getNodeType());
		}
		
	}
	
	public static final class Arg extends CallArg {
		
		
		Arg(final Args parent) {
			super(parent);
		}
		
		
		@Override
		public final NodeType getNodeType() {
			return NodeType.SUB_INDEXED_ARG;
		}
		
		
		@Override
		public @Nullable Args getRParent() {
			return (Args)this.rParent;
		}
		
		
		@Override
		public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
			visitor.visit(this);
		}
		
		
		@Override
		public final boolean equalsSingle(final RAstNode element) {
			return (NodeType.SUB_INDEXED_ARG == element.getNodeType());
		}
		
	}
	
	
	final Expression expr= new Expression();
	final Args sublist= new Args(this);
	int openOffset= NA_OFFSET;
	int closeOffset= NA_OFFSET;
	int close2Offset= NA_OFFSET;
	
	
	SubIndexed() {
		super();
	}
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 2;
	}
	
	@Override
	public final RAstNode getChild(final int index) {
		return switch (index) {
		case 0 -> this.expr.node;
		case 1 -> this.sublist;
		default ->
				throw new IndexOutOfBoundsException();
		};
	}
	
	public final RAstNode getRefChild() {
		return this.expr.node;
	}
	
	public final Args getArgsChild() {
		return this.sublist;
	}
	
	public final int getSublistOpenOffset() {
		return this.openOffset;
	}
	
	public final int getSublistCloseOffset() {
		return this.closeOffset;
	}
	
	public final int getSublistClose2Offset() {
		return this.closeOffset;
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.expr.node == child) {
			return 0;
		}
		if (this.sublist == child) {
			return 1;
		}
		return -1;
	}
	
	@Override
	public final void acceptInR(final RAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInRChildren(final RAstVisitor visitor) throws InvocationTargetException {
		this.expr.node.acceptInR(visitor);
		this.sublist.acceptInR(visitor);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.expr.node);
		visitor.visit(this.sublist);
	}
	
	
	@Override
	final @Nullable Expression getExpr(final RAstNode child) {
		if (this.expr.node == child) {
			return this.expr;
		}
		return null;
	}
	@Override
	final Expression getLeftExpr() {
		return this.expr;
	}
	
	@Override
	final @Nullable Expression getRightExpr() {
		return null;
	}
	
	@Override
	public final boolean equalsSingle(final RAstNode element) {
		if (getNodeType() == element.getNodeType()) {
			final SubIndexed other= (SubIndexed)element;
			return ((this.expr.node == other.expr.node
							|| (this.expr.node != null && this.expr.node.equalsSingle(other.expr.node)) )
					);
		}
		return false;
	}
	
	
	@Override
	final int getMissingExprStatus(final Expression expr) {
		if (this.expr == expr) {
			return TYPE123_SYNTAX_EXPR_AS_REF_MISSING;
		}
		throw new IllegalArgumentException();
	}
	
	final void updateOffsets() {
		if (this.close2Offset != NA_OFFSET) {
			doSetStartEndOffset(this.expr.node.getStartOffset(), this.close2Offset + 1);
		}
		else if (this.closeOffset != NA_OFFSET) {
			doSetStartEndOffset(this.expr.node.getStartOffset(), this.closeOffset + 1);
		}
		else {
			doSetStartEndOffset(this.expr.node.getStartOffset(), this.sublist.getEndOffset());
		}
	}
	
}
