/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.doc;

import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.treepartitioner.BasicPartitionNodeType;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;


@NonNullByDefault
public abstract class RPkgDescrPartitionNodeType extends BasicPartitionNodeType {
	
	
	public static final RPkgDescrPartitionNodeType DEFAULT_ROOT= new RPkgDescrPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.RPKG_DESCR_DEFAULT_CONTENT_TYPE;
		}
		
		@Override
		public boolean prefereAtBegin(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
		@Override
		public boolean prefereAtEnd(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
	};
	
	public static final RPkgDescrPartitionNodeType FIELD_NAME= new RPkgDescrPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.RPKG_DESCR_FIELD_NAME_CONTENT_TYPE;
		}
		
		@Override
		public boolean prefereAtBegin(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
	};
	
	public static final RPkgDescrPartitionNodeType FIELD_VALUE_R= new RPkgDescrPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return RDocumentConstants.RPKG_DESCR_DEFAULT_CONTENT_TYPE;
		}
		
		@Override
		public boolean prefereAtBegin(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
		@Override
		public boolean prefereAtEnd(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
	};
	
	
	protected RPkgDescrPartitionNodeType() {
	}
	
	
}
