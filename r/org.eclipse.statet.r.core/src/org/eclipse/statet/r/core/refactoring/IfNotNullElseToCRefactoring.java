/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.refactoring;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Position;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.osgi.util.NLS;
import org.eclipse.text.edits.RangeMarker;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.refactoring.Messages;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.util.SourceMessageUtil;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringDescriptor;
import org.eclipse.statet.ltk.refactoring.core.QuickRefactoring;
import org.eclipse.statet.ltk.refactoring.core.RefactoringChange;
import org.eclipse.statet.ltk.refactoring.core.RefactoringMessages;
import org.eclipse.statet.ltk.refactoring.core.SourceUnitChange;
import org.eclipse.statet.ltk.refactoring.core.TextChangeCompatibility;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.RAsts;
import org.eclipse.statet.r.core.source.ast.Special;


@NonNullByDefault
public class IfNotNullElseToCRefactoring extends QuickRefactoring {
	
	
	private final RRefactoringAdapter adapter= new RRefactoringAdapter();
	private final ElementSet elementSet;
	
	private final RSourceUnit sourceUnit;
	
	private final Special sourceNode;
	
	private final boolean withBlocks;
	
	
	public IfNotNullElseToCRefactoring(final RSourceUnit su, final Special sourceNode,
			final boolean withBlocks) {
		this.sourceUnit= su;
		this.elementSet= new ElementSet(su);
		
		this.sourceNode= sourceNode;
		
		this.withBlocks= withBlocks;
	}
	
	
	@Override
	public String getBundleId() {
		return RCore.BUNDLE_ID;
	}
	
	@Override
	public String getIdentifier() {
		return RRefactoring.CONVERT_IFNOTNULLELSE_TO_CIFELSE_REFACTORING_ID;
	}
	
	@Override
	public String getName() {
		return Messages.IfNotNullElseToCIfElse_label;
	}
	
	
	public boolean getWithBlocks() {
		return this.withBlocks;
	}
	
	
	@Override
	public RefactoringStatus checkInitialConditions(
			final @Nullable IProgressMonitor monitor) {
		final SubMonitor m= SubMonitor.convert(monitor, 1);
		try {
			final var rCoreAccess= RCore.getContextAccess(this.sourceUnit);
			final Special sourceNode= this.sourceNode;
			if (sourceNode.getText() != "||") {
				return RefactoringStatus.createFatalErrorStatus("Cannot convert the statement.");
			}
			
			final RefactoringStatus result= new RefactoringStatus();
			this.adapter.checkInitialToModify(result, this.elementSet);
			
			if (result.hasFatalError()) {
				return result;
			}
			
			return result;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public RefactoringStatus checkFinalConditions(
			final IProgressMonitor monitor) throws CoreException {
		final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_FinalCheck_label, 3);
		try {
			final RefactoringStatus status= new RefactoringStatus();
			this.adapter.checkFinalToModify(status, this.elementSet, m.newChild(2));
			return status;
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public Change createChange(
			final IProgressMonitor monitor) throws CoreException, OperationCanceledException {
				final SubMonitor m= SubMonitor.convert(monitor, RefactoringMessages.Common_CreateChanges_label, 3);
		try {
			final var textFileChange= new SourceUnitChange(this.sourceUnit);
			if (this.sourceUnit.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
				textFileChange.setSaveMode(TextFileChange.LEAVE_DIRTY);
			}
			createChanges(textFileChange, m.newChild(2));
			
			textFileChange.setInsertPosition(new Position(this.sourceNode.getStartOffset(), 0));
			
			final Map<String, String> arguments= new HashMap<>();
			
			final var messageUtil= new SourceMessageUtil(this.sourceUnit.getContent(m));
			final String description= NLS.bind(Messages.IfNotNullElseToCIfElse_Descriptor_description,
					messageUtil.getFullQuoteText(this.sourceNode.getLeftChild()) );
			final IProject resource= this.elementSet.getSingleProject();
			final String project= (resource != null) ? resource.getName() : null;
			final String source= (project != null) ?
					NLS.bind(RefactoringMessages.Common_Source_Project_label, project) :
					RefactoringMessages.Common_Source_Workspace_label;
			final int flags= 0;
			final String comment= ""; //$NON-NLS-1$
			final CommonRefactoringDescriptor descriptor= new CommonRefactoringDescriptor(
					getIdentifier(), project, description, comment, arguments, flags);
			
			return new RefactoringChange(descriptor, getName(),
					new Change[] { textFileChange } );
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
		finally {
			m.done();
		}
	}
	
	@Override
	public SourceUnitChange createTextChange(
			final SubMonitor m) throws CoreException {
		m.setWorkRemaining(2);
		try {
			final var textFileChange= new SourceUnitChange(this.sourceUnit);
			
			createChanges(textFileChange, m.newChild(2));
			
			return textFileChange;
		}
		catch (final BadLocationException e) {
			throw handleUnexpectedError(e);
		}
	}
	
	private void createChanges(final SourceUnitChange change,
			final SubMonitor m) throws BadLocationException, CoreException {
		m.setWorkRemaining(3 + 1 * 4);
		
		this.sourceUnit.connect(m.newChild(1));
		try {
			final var util= new RSourceCodeModifyUtil(this.adapter, this.sourceUnit, m.newChild(1));
			
			final var sourceNode= this.sourceNode;
			final var primaryNode= sourceNode.getLeftChild();
			final var elseNode= sourceNode.getRightChild();
			
			final RAstNode baseNode= RAsts.getSequenceChildElseRoot(sourceNode);
			final int changeStart= sourceNode.getStartOffset();
			final int changeEnd= sourceNode.getEndOffset();
			
			{	util.clear();
				util.append("if (!is.null(");
				util.appendUnwrapGroup(primaryNode);
				util.append("))");
				util.appendFlowControlBody(primaryNode,
						(this.withBlocks && primaryNode.getNodeType() != NodeType.BLOCK) );
				util.append(" else");
				util.appendFlowControlBody(elseNode,
						(this.withBlocks && elseNode.getNodeType() != NodeType.BLOCK) );
				
				if (this.withBlocks) {
					util.correctIndent(changeStart, baseNode.getStartOffset());
				}
				
				TextChangeCompatibility.addTextEdit(change, Messages.IfNotNullElse_Changes_name,
						util.createReplaceEdit(changeStart, changeEnd) );
				m.worked(4);
			}
			
			TextChangeCompatibility.addMarker(change, new RangeMarker(changeEnd, 0));
		}
		finally {
			this.sourceUnit.disconnect(m.newChild(1));
		}
	}
	
}
