/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.pkgmanager;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.RLibLocation;
import org.eclipse.statet.rj.renv.core.RPkg;


public abstract class RPkgAction {
	
	
	public static final int UNINSTALL=                      1;
	public static final int INSTALL=                        2;
	
	
	public static class Install extends RPkgAction {
		
		
		private final IRPkgData pkg;
		
		private RLibLocation target;
		
		private final IRPkgInfoAndData reference;
		
		
		public Install(final IRPkgData pkg, final RLibLocation target, final IRPkgInfoAndData reference) {
			this.pkg= pkg;
			this.target= target;
			this.reference= reference;
		}
		
		
		@Override
		public int getAction() {
			return INSTALL;
		}
		
		@Override
		public RLibLocation getLibLocation() {
			return this.target;
		}
		
		public void setLibraryLocation(final RLibLocation location) {
			this.target= location;
		}
		
		@Override
		public IRPkgData getPkg() {
			return this.pkg;
		}
		
		@Override
		public String getRepoId() {
			return this.pkg.getRepoId();
		}
		
		public IRPkgInfoAndData getReferencePkg() {
			return this.reference;
		}
		
		
		@Override
		public int hashCode() {
			return 9251 + this.pkg.hashCode();
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			return (this == obj
					|| (obj instanceof final Install other
							&& this.pkg.equals(other.pkg) ));
		}
		
		
		@Override
		public String toString() {
			return "RPkgAction INSTALL " + getPkg(); //$NON-NLS-1$
		}
		
	}
	
	
	public static class Uninstall extends RPkgAction {
		
		
		private final RPkgInfo pkg;
		
		
		public Uninstall(final RPkgInfo pkg) {
			this.pkg= pkg;
		}
		
		
		@Override
		public int getAction() {
			return UNINSTALL;
		}
		
		@Override
		public RPkg getPkg() {
			return this.pkg;
		}
		
		@Override
		public String getRepoId() {
			return null;
		}
		
		@Override
		public RLibLocation getLibLocation() {
			return this.pkg.getLibLocation();
		}
		
		
		@Override
		public int hashCode() {
			return 1269275 + this.pkg.hashCode();
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			return (this == obj
					|| (obj instanceof final Uninstall other
							&& this.pkg.equals(other.pkg) ));
		}
		
		
		@Override
		public String toString() {
			return "RPkgAction UNINSTALL " + getPkg(); //$NON-NLS-1$
		}
		
	}
	
	
	private RPkgAction() {
	}
	
	
	public abstract int getAction();
	
	public abstract RPkg getPkg();
	
	public abstract String getRepoId();
	public abstract RLibLocation getLibLocation();
	
}
