/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceUnitModelInfo;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceUnitModelInfo;


@NonNullByDefault
public class RPkgDescrSourceUnitModelInfoImpl extends BasicSourceUnitModelInfo implements RPkgDescrSourceUnitModelInfo {
	
	
	private final RPkgDescrContainerSourceElement.SourceContainer sourceElement;
	
	private final ImList<? extends RPkgDescrFieldSourceElement> fields;
	
	
	public RPkgDescrSourceUnitModelInfoImpl(final AstInfo ast, final RPkgDescrContainerSourceElement.SourceContainer unitElement) {
		super(ast);
		this.sourceElement= unitElement;
		
		this.fields= ImCollections.toList(RPkgDescrContainerSourceElement.getFields(this.sourceElement));
	}
	
	
	@Override
	public RPkgDescrContainerSourceElement.SourceContainer getSourceElement() {
		return this.sourceElement;
	}
	
	@Override
	public ImList<? extends RPkgDescrFieldSourceElement> getFields() {
		return this.fields;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public @Nullable RPkgDescrFieldSourceElement getField(final String name) {
		for (int i= this.fields.size() - 1; i >= 0; i--) {
			final var field= this.fields.get(i);
			if (name.equals(field.getElementName().getSegmentName())) {
				return field;
			}
		}
		return null;
	}
	
	
}
