/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.refactoring;

import org.eclipse.ltk.core.refactoring.participants.DeleteProcessor;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination;
import org.eclipse.statet.r.core.refactoring.RDeleteProcessor;
import org.eclipse.statet.r.core.refactoring.RPasteCodeProcessor;
import org.eclipse.statet.r.core.refactoring.RRefactoringAdapter;


public class RRefactoringFactory extends CommonRefactoringFactory {
	
	
	public RRefactoringFactory() {
	}
	
	
	@Override
	public RefactoringAdapter createAdapter(final Object elements) {
		return new RRefactoringAdapter();
	}
	
	@Override
	public DeleteProcessor createDeleteProcessor(final Object elements, final RefactoringAdapter adapter) {
		return new RDeleteProcessor(createElementSet(elements), (RRefactoringAdapter) adapter);
	}
	
	@Override
	public RefactoringProcessor createPasteProcessor(final Object elementsToPaste,
			final RefactoringDestination destination, final RefactoringAdapter adapter) {
		if (elementsToPaste instanceof String) {
			return new RPasteCodeProcessor((String) elementsToPaste, destination, (RRefactoringAdapter) adapter);
		}
		return super.createPasteProcessor(elementsToPaste, destination, adapter);
	}
	
}
