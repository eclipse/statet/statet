/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import java.util.Collection;
import java.util.regex.Pattern;

import org.eclipse.statet.r.core.pkgmanager.RRepo;
import org.eclipse.statet.r.core.pkgmanager.RRepoSettings;
import org.eclipse.statet.rj.renv.core.RPkgType;


public class RVarRepo extends RRepo {
	
	
	static final String CRAN_MIRROR_VAR= "%cran"; //$NON-NLS-1$
	static final String BIOC_MIRROR_VAR= "%bm"; //$NON-NLS-1$
	static final String BIOC_VERSION_VAR= "%v"; //$NON-NLS-1$
	
	private static final Pattern CRAN_MIRROR_VAR_PATTERN= Pattern.compile(Pattern.quote(CRAN_MIRROR_VAR) + "/?");
	private static final Pattern BIOC_MIRROR_VAR_PATTERN= Pattern.compile(Pattern.quote(BIOC_MIRROR_VAR) + "/?");
	private static final Pattern BIOC_VERSION_VAR_PATTERN= Pattern.compile(Pattern.quote(BIOC_VERSION_VAR));
	
	
	static boolean hasVars(final String url) {
		return (url.indexOf(BIOC_MIRROR_VAR) >= 0
				|| url.indexOf(BIOC_VERSION_VAR) >= 0
				|| url.indexOf(CRAN_MIRROR_VAR) >= 0);
	}
	
	static boolean requireCRANMirror(final Collection<RRepo> repos) {
		for (final RRepo repo : repos) {
			if (repo instanceof RVarRepo) {
				if (((RVarRepo) repo).getRawURL().indexOf(RVarRepo.CRAN_MIRROR_VAR) >= 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	static boolean requireBioCMirror(final Collection<RRepo> repos) {
		for (final RRepo repo : repos) {
			if (repo instanceof RVarRepo) {
				if (((RVarRepo) repo).getRawURL().indexOf(RVarRepo.BIOC_MIRROR_VAR) >= 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	static RRepo create(final String id, final String name, final String url, final RPkgType pkgType) {
		return (hasVars(url)) ? new RVarRepo(id, name, url, pkgType) : new RRepo(id, name, url, pkgType);
	}
	
	
	private String rawURL;
	
	
	public RVarRepo(final String id, final String name, final String url, final RPkgType pkgType) {
		super(id, name, url, pkgType);
		this.rawURL= getURL();
	}
	
	
	public void updateURL(final RRepoSettings settings) {
		String url= this.rawURL;
		if (settings.getCRANMirror() != null) {
			url= CRAN_MIRROR_VAR_PATTERN.matcher(url).replaceAll(settings.getCRANMirror().getURL());
		}
		if (settings.getBioCMirror() != null) {
			url= BIOC_MIRROR_VAR_PATTERN.matcher(url).replaceAll(settings.getBioCMirror().getURL());
		}
		if (settings.getBioCVersion() != null) {
			url= BIOC_VERSION_VAR_PATTERN.matcher(url).replaceAll(settings.getBioCVersion());
		}
		if (!url.equals(getURL())) {
			super.setURL(url);
		}
	}
	
	@Override
	public void setURL(final String url) {
		super.setURL(url);
		this.rawURL= url;
	}
	
	public String getRawURL() {
		return this.rawURL;
	}
	
}
