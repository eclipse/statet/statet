/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.SubIndexed;


@NonNullByDefault
final class SubIndexedDElementAccess extends SubAbstractElementAccess {
	
	
	final SubIndexed node;
	
	
	SubIndexedDElementAccess(final ElementAccess root, final SubIndexed node) {
		super(root);
		this.node= node;
	}
	
	
	@Override
	public final int getType() {
		return RElementName.SUB_INDEXED_D;
	}
	
	@Override
	public final @Nullable String getSegmentName() {
		return null;
	}
	
	@Override
	public final RAstNode getNode() {
		return this.node;
	}
	
	@Override
	public final @Nullable RAstNode getNameNode() {
		return null;
	}
	
	
	@Override
	public final ImList<? extends RElementAccess> getAllInUnit(final boolean includeSlaves) {
		return ImCollections.newList();
	}
	
}
