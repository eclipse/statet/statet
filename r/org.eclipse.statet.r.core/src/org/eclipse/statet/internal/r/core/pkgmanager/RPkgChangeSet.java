/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import java.util.List;

import org.eclipse.statet.jcommons.collections.SortedArraySet;

import org.eclipse.statet.r.core.pkgmanager.IRPkgChangeSet;
import org.eclipse.statet.r.core.pkgmanager.RPkgInfo;
import org.eclipse.statet.rj.renv.core.RPkgUtils;


public class RPkgChangeSet implements IRPkgChangeSet {
	
	
	final SortedArraySet<String> names= new SortedArraySet<>(new String[8], 0, RPkgUtils.NAMES_COLLATOR);
	
	final RPkgListImpl<RPkgInfo> added= new RPkgListImpl<>(8);
	final RPkgListImpl<RPkgInfo> changed= new RPkgListImpl<>(8);
	final RPkgListImpl<RPkgInfo> deleted= new RPkgListImpl<>(8);
	
	
	@Override
	public List<String> getNames() {
		return this.names;
	}
	
	@Override
	public RPkgListImpl<RPkgInfo> getAdded() {
		return this.added;
	}
	
	@Override
	public RPkgListImpl<RPkgInfo> getChanged() {
		return this.changed;
	}
	
	@Override
	public RPkgListImpl<RPkgInfo> getDeleted() {
		return this.deleted;
	}
	
}
