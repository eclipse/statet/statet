/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String Descr_FieldRequiredMissing_message;
	public static String Descr_FieldDuplicate_message;
	
	public static String Descr_FieldValueMissing_message;
	public static String Descr_FieldValueInvalid_LogiLiteral_message;
	public static String Descr_FieldValueInvalid_Url_message;
	public static String Descr_FieldValueInvalid_Url_Detail_message;
	
	public static String RPkgName_InvalidChar_message;
	public static String RPkgName_InvalidFirstChar_message;
	public static String RPkgName_InvalidEndCharDot_message;
	public static String RPkgName_InvalidSingleChar_message;
	public static String RPkgName_IncompleteTranslation_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	
	private Messages() {}
	
}
