/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core;

import static org.eclipse.statet.r.core.RCore.BUNDLE_ID;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import org.eclipse.core.net.proxy.IProxyService;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.jobs.Job;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;

import org.eclipse.statet.internal.r.core.pkgmanager.REnvPkgManager;
import org.eclipse.statet.internal.r.core.renv.REnvManagerImpl;
import org.eclipse.statet.internal.r.core.rhelp.WorkbenchRHelpHttpService;
import org.eclipse.statet.internal.r.core.rhelp.WorkbenchRHelpManager;
import org.eclipse.statet.internal.r.core.rmodel.RModelManagerImpl;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.RCoreAccess;
import org.eclipse.statet.r.core.pkgmanager.IRPkgManager;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.rhelp.core.REnvHelp;
import org.eclipse.statet.rhelp.core.http.RHelpHttpService;
import org.eclipse.statet.rj.renv.core.REnv;


/**
 * The main plug-in class to be used in the desktop.
 */
public class RCorePlugin extends Plugin {
	
	
	private class PreloadRHelpJob extends Job {
		
		public PreloadRHelpJob() {
			super("Prepare R help");
			setSystem(true);
			setPriority(Job.DECORATE);
		}
		
		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			final REnvManagerImpl rEnvManager= getREnvManager();
			if (rEnvManager == null) {
				return org.eclipse.core.runtime.Status.CANCEL_STATUS;
			}
			final REnv rEnv= rEnvManager.getDefault().resolve();
			if (rEnv != null) {
				final REnvHelp help= getRHelpManager().getHelp(rEnv);
				if (help != null) {
					help.unlock();
				}
			}
			
			getRHelpHttpService().ensureIsRunning();
			
			return org.eclipse.core.runtime.Status.OK_STATUS;
		}
		
	}
	
	
	private static RCorePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static RCorePlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	public static final void logError(final String message, final Throwable e) {
		log(new org.eclipse.core.runtime.Status(IStatus.ERROR, BUNDLE_ID, message, e));
	}
	
	public static final void log(final Status status) {
		log(StatusUtils.convert(status));
	}
	
	
	private boolean started;
	
	private final List<Disposable> disposables= new ArrayList<>();
	
	private REnvManagerImpl rEnvManager;
	
	private RCoreAccessImpl workspaceCoreAccess;
	private RCoreAccessImpl defaultsCoreAccess;
	
	private RModelManagerImpl rModelManager;
	private ResourceTracker resourceTracker;
	
	private REnvPkgManager rEnvPkgManager;
	private WorkbenchRHelpManager rHelpManager;
	private WorkbenchRHelpHttpService rHelpHttpService;
	
	private ServiceTracker proxyService;
	
	
	/**
	 * The constructor.
	 */
	public RCorePlugin() {
	}
	
	
	/**
	 * This method is called upon plug-in activation
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		updatePreferences();
		
		this.rEnvManager= new REnvManagerImpl();
		
		this.workspaceCoreAccess= new RCoreAccessImpl(
				EPreferences.getInstancePrefs(),
				this.rEnvManager.getDefault(),
				RSourceConfig.DEFAULT_CONFIG );
		
		this.rModelManager= new RModelManagerImpl();
		this.resourceTracker= new ResourceTracker(this.rModelManager);
		
		this.rEnvPkgManager= new REnvPkgManager(this.rEnvManager);
		this.rHelpManager= new WorkbenchRHelpManager(this.rEnvManager);
		this.disposables.add(this.rHelpManager);
		this.rHelpHttpService= new WorkbenchRHelpHttpService(this.rHelpManager);
		this.disposables.add(this.rHelpHttpService);
		
		new PreloadRHelpJob().schedule(1000);
		
		this.started= true;
	}
	
	/**
	 * This method is called when the plug-in is stopped
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
			if (this.resourceTracker != null) {
				try {
					this.resourceTracker.dispose();
				}
				catch (final Exception e) {}
				this.resourceTracker= null;
			}
			
			if (this.rModelManager != null) {
				this.rModelManager.dispose();
				this.rModelManager= null;
			}
			if (this.workspaceCoreAccess != null) {
				this.workspaceCoreAccess.dispose();
				this.workspaceCoreAccess= null;
			}
			if (this.defaultsCoreAccess != null) {
				this.defaultsCoreAccess.dispose();
				this.defaultsCoreAccess= null;
			}
			if (this.rEnvManager != null) {
				this.rEnvManager.dispose();
				this.rEnvManager= null;
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					logError("Error occured when dispose module", e);
				}
			}
			this.disposables.clear();
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	private void checkStarted() {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
	}
	
	
	public REnvManagerImpl getREnvManager() {
		return this.rEnvManager;
	}
	
	public RModelManagerImpl getRModelManager() {
		return this.rModelManager;
	}
	
	public ResourceTracker getResourceTracker() {
		return this.resourceTracker;
	}
	
	public REnvPkgManager getREnvPkgManager() {
		return this.rEnvPkgManager;
	}
	
	public WorkbenchRHelpManager getRHelpManager() {
		return this.rHelpManager;
	}
	
	public RHelpHttpService getRHelpHttpService() {
		return this.rHelpHttpService;
	}
	
	public synchronized RCoreAccess getWorkspaceRCoreAccess() {
		if (this.workspaceCoreAccess == null) {
			checkStarted();
		}
		return this.workspaceCoreAccess;
	}
	
	public synchronized RCoreAccess getDefaultsRCoreAccess() {
		if (this.defaultsCoreAccess == null) {
			checkStarted();
			this.defaultsCoreAccess= new RCoreAccessImpl(
					EPreferences.getDefaultPrefs(),
					this.rEnvManager.getDefault(),
					RSourceConfig.DEFAULT_CONFIG );
		}
		return this.defaultsCoreAccess;
	}
	
	public synchronized IProxyService getProxyService() {
		if (this.proxyService == null) {
			checkStarted();
			this.proxyService= new ServiceTracker(getBundle().getBundleContext(),
					IProxyService.class.getName(), null );
			this.proxyService.open();
		}
		return (IProxyService) this.proxyService.getService();
	}
	
	
	private void updatePreferences() {
		try {
			// Statet 4.5
			PreferenceUtils.mergeNode(RCore.BUNDLE_ID + IRPkgManager.PREF_QUALIFIER,
					IRPkgManager.PREF_QUALIFIER,
					false );
		}
		catch (final Exception e) {
			logError("An error occurred when updating deprecated preferences.", e);
		}
	}
	
}
