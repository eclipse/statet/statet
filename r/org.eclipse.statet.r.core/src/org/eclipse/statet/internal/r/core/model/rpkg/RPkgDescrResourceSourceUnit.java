/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.model.core.impl.ResourceIssueSupport;
import org.eclipse.statet.r.core.BasicRResourceSourceUnit;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RWorkspaceSourceUnit;
import org.eclipse.statet.r.core.model.build.RPkgDescrSourceUnitModelContainer;
import org.eclipse.statet.r.core.source.doc.RPkgDescrDocumentContentInfo;


@NonNullByDefault
public final class RPkgDescrResourceSourceUnit extends BasicRResourceSourceUnit implements RWorkspaceSourceUnit {
	
	
	private static final SourceUnitIssueSupport ISSUE_SUPPORT=
			new ResourceIssueSupport(RPkgDescrSourceUnitModelContainer.ISSUE_TYPE_SET);
	
	
	private final RPkgDescrSourceUnitModelContainer model= new RPkgDescrSourceUnitModelContainer(this, ISSUE_SUPPORT);
	
	
	public RPkgDescrResourceSourceUnit(final String id, final IFile file) {
		super(id, file);
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.RPKG_DESCRIPTION_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return RPkgDescrDocumentContentInfo.INSTANCE;
	}
	
	@Override
	public int getElementType() {
		return RSourceUnit.R_WORKSPACE_SU;
	}
	
	
	@Override
	protected void unregister() {
		super.unregister();
		
		this.model.clear();
	}
	
	
	@Override
	public @Nullable AstInfo getAstInfo(final @Nullable String type, final boolean ensureSync,
			final IProgressMonitor monitor) {
		final AstInfo ast= this.model.getCurrentAst();
		final long stamp= getResource().getModificationStamp();
		if (ast != null && ast.getStamp().getContentStamp() == stamp) {
			return ast;
		}
		// TODO ask saved
		return null;
	}
	
	@Override
	public @Nullable SourceUnitModelInfo getModelInfo(final @Nullable String type, final int syncLevel,
			final IProgressMonitor monitor) {
		if (type == null || type == RModel.RPKG_DESCRIPTION_TYPE_ID) {
			return this.model.getModelInfo(syncLevel, monitor);
		}
		return null;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RPkgDescrSourceUnitModelContainer.class) {
			return (T)this.model;
		}
		return super.getAdapter(adapterType);
	}
	
	
}
