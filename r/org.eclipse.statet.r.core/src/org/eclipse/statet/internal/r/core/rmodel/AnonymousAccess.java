/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.rlang.RSrcStrFrame;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
public final class AnonymousAccess extends RElementAccess {
	
	
	private final RAstNode node;
	private final RSrcStrFrame frame;
	
	
	public AnonymousAccess(final RAstNode node, final RSrcStrFrame frame) {
		this.node= node;
		this.frame= frame;
	}
	
	
	@Override
	public int getType() {
		return RElementName.ANONYMOUS;
	}
	
	
	@Override
	public RSrcStrFrame getFrame() {
		return this.frame;
	}
	
	@Override
	public ImList<? extends RElementAccess> getAllInUnit(final boolean includeSlaves) {
		return ImCollections.newList(this);
	}
	
	@Override
	public boolean isWriteAccess() {
		return true;
	}
	
	@Override
	public boolean isFunctionAccess() {
		return true;
	}
	
	@Override
	public boolean isCallAccess() {
		return false;
	}
	
	@Override
	public RAstNode getNode() {
		return this.node;
	}
	
	@Override
	public @Nullable RAstNode getNameNode() {
		return null;
	}
	
	
	@Override
	public @Nullable RElementName getScope() {
		return null;
	}
	
	@Override
	public @Nullable String getSegmentName() {
		return null;
	}
	
	@Override
	public @Nullable RElementAccess getNextSegment() {
		return null;
	}
	
}
