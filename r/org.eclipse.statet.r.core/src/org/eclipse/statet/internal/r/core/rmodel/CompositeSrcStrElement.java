/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.rlang.RCompositeSrcStrElement;
import org.eclipse.statet.r.core.model.rlang.RLangSrcStrElement;


@NonNullByDefault
public class CompositeSrcStrElement extends RSourceFileElement
		implements RCompositeSrcStrElement {
	
	
	private final ImList<? extends EmbeddedRBuildElement> compositeElements;
	
	private final TextRegion sourceRange;
	
	private volatile @Nullable List<RLangSrcStrElement> allSourceChildren;
	
	
	public CompositeSrcStrElement(final RSourceUnit sourceUnit,
			final SourceModelStamp stamp,
			final BuildSourceFrame envir,
			final List<? extends EmbeddedRBuildElement> elements, final TextRegion sourceRange) {
		super(sourceUnit, stamp, envir);
		
		this.compositeElements= ImCollections.toList(elements);
		this.sourceRange= sourceRange;
	}
	
	
//	@Override
//	public int getElementType() {
//		return RElement.C2_SOURCE_FILE | 0x1;
//	}
	
	@Override
	public ImList<? extends RLangSrcStrElement> getCompositeElements() {
		return this.compositeElements;
	}
	
	
	@Override
	public TextRegion getSourceRange() {
		return this.sourceRange;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter) {
		for (final EmbeddedRBuildElement element : this.compositeElements) {
			if (element.hasSourceChildren(filter)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public List<RLangSrcStrElement> getSourceChildren(final @Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter) {
		if (filter == null) {
			List<RLangSrcStrElement> children= this.allSourceChildren;
			if (children == null) {
				final List<? extends RLangSrcStrElement>[] compositeLists= new @NonNull List[this.compositeElements.size()];
				for (int i= 0; i < compositeLists.length; i++) {
					compositeLists[i]= this.compositeElements.get(i).getSourceChildren(null);
				}
				children= ImCollections.concatList(compositeLists);
				this.allSourceChildren= children;
			}
			return children;
		}
		else {
			final List<RLangSrcStrElement> children= new ArrayList<>();
			for (final EmbeddedRBuildElement element : this.compositeElements) {
				final List<? extends RLangSrcStrElement> list= element.getSourceChildren(null);
				for (final var child : list) {
					if (filter.include(child)) {
						children.add(child);
					}
				}
			}
			return children;
		}
	}
	
}
