/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.dcf.core.source.ast.EmbeddingValue;
import org.eclipse.statet.ltk.model.core.build.EmbeddingForeignReconcileTask;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceStructElement;


@NonNullByDefault
public class EmbeddingReconcileTask implements EmbeddingForeignReconcileTask<EmbeddingValue, RPkgDescrSourceStructElement> {
	
	
	private final EmbeddingValue node;
	
	
	public EmbeddingReconcileTask(final EmbeddingValue node) {
		this.node= node;
	}
	
	
	@Override
	public String getForeignTypeId() {
		return this.node.getForeignTypeId();
	}
	
	@Override
	public EmbeddingValue getAstNode() {
		return this.node;
	}
	
	@Override
	public RPkgDescrSourceStructElement getEmbeddingElement() {
		return null;
	}
	
	@Override
	public void setEmbeddedElement(final SourceStructElement element) {
	}
	
}
