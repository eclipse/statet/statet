/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.nostart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.content.IContentDescription;

import org.eclipse.statet.jcommons.io.ByteOrderMark;
import org.eclipse.statet.jcommons.io.IOUtils;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.io.TextContentDescriber;


/**
 * Detects encoding of Rd file based on optional \encoding element.
 * 
 * The \encoding element must be the first command in Rd file.
 */
@NonNullByDefault
public class RdTextContentDescriber extends TextContentDescriber {
	
	
	private static final @NonNull QualifiedName[] SUPPORTED_OPTIONS= new @NonNull QualifiedName[] {
			IContentDescription.CHARSET,
			IContentDescription.BYTE_ORDER_MARK,
	};
	
	
	private static final String ENCODING_COMMAND_NAME= "encoding"; //$NON-NLS-1$
	private static final Pattern BRACKET_CONTENT_PATTERN= Pattern.compile("\\s*\\{\\s*(\\S*)\\s*\\}.*"); //$NON-NLS-1$
	
	
	public RdTextContentDescriber() {
	}
	
	
	@Override
	public @NonNull QualifiedName[] getSupportedOptions() {
		return SUPPORTED_OPTIONS;
	}
	
	@Override
	public int describe(final Reader contents, final @Nullable IContentDescription description) throws IOException {
		if (description != null && description.isRequested(IContentDescription.CHARSET)) {
			final BufferedReader reader= new BufferedReader(contents);
			final String encoding= parse(reader);
			if (encoding == null) {
				return INDETERMINATE;
			}
			description.setProperty(IContentDescription.CHARSET, encoding);
		}
		return VALID;
	}
	
	@Override
	public int describe(final InputStream contents, final @Nullable IContentDescription description) throws IOException {
		final ByteOrderMark bom= checkByteOrderMark(contents, description);
		
		if (description != null && description.isRequested(IContentDescription.CHARSET)) {
			contents.reset();
			final var reader= new BufferedReader(
					IOUtils.newStreamContentReader(contents, bom, StandardCharsets.ISO_8859_1) );
			final String encoding= parse(reader);
			if (encoding != null) {
				description.setProperty(IContentDescription.CHARSET, encoding);
			}
		}
		
		return VALID;
	}
	
	private @Nullable String parse(final BufferedReader reader) throws IOException {
		String line;
		ITER_LINES: while ((line= reader.readLine()) != null) {
			ITER_CHARS: for (int idx= 0; idx < line.length(); idx++) {
				switch (line.charAt(idx)) {
				case ' ':
				case '\t':
					continue ITER_CHARS;
				case '%':
					continue ITER_LINES;
				case '\\':
					if (line.regionMatches(idx + 1, ENCODING_COMMAND_NAME, 0, ENCODING_COMMAND_NAME.length())) {
						final Matcher matcher= BRACKET_CONTENT_PATTERN.matcher(line.substring(idx + 1 + ENCODING_COMMAND_NAME.length()));
						if (matcher.matches()) {
							return matcher.group(1);
						}
					}
					break ITER_LINES;
				default:
					break ITER_LINES;
				}
			}
		}
		return null;
	}
	
}
