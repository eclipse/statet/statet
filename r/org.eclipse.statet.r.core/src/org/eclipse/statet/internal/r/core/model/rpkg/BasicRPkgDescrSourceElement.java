/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.model.DslElementName;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceStructElement;


@NonNullByDefault
public abstract class BasicRPkgDescrSourceElement implements RPkgDescrSourceStructElement {
	
	
	protected static final ImList<RPkgDescrSourceStructElement> NO_CHILDREN= ImCollections.emptyList();
	
	
	protected DslElementName name;
	protected int occurrenceCount;
	protected TextRegion nameRegion;
	
	protected final DslAstNode astNode;
	
	protected int modelStatusCode;
	
	
	protected BasicRPkgDescrSourceElement(final DslAstNode astNode) {
		this.astNode= astNode;
	}
	
	
	@Override
	public final String getModelTypeId() {
		return RModel.RPKG_DESCRIPTION_TYPE_ID;
	}
	
	@Override
	public String getId() {
		final String name= getElementName().getDisplayName();
		final StringBuilder sb= new StringBuilder(name.length() + 16);
		sb.append(Integer.toHexString(getElementType() & MASK_C12));
		sb.append(':');
		sb.append(name);
		sb.append('#');
		sb.append(this.occurrenceCount);
		return sb.toString();
	}
	
	@Override
	public DslElementName getElementName() {
		return this.name;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
	@Override
	public TextRegion getSourceRange() {
		return this.astNode;
	}
	
	@Override
	public TextRegion getNameSourceRange() {
		return this.nameRegion;
	}
	
	
	@Override
	public int getModelStatusCode() {
		return this.modelStatusCode;
	}
	
	public void setModelStatusCode(final int statusCode) {
		this.modelStatusCode= statusCode;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == AstNode.class) {
			return (T)this.astNode;
		}
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return (getElementType() & MASK_C12) * getElementName().hashCode() + this.occurrenceCount;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final BasicRPkgDescrSourceElement other
						&& (getElementType() & MASK_C12) == (getElementType() & MASK_C12)
						&& this.occurrenceCount == other.occurrenceCount
						&& ((getElementType() & MASK_C1) == C1_SOURCE || getSourceParent().equals(other.getSourceParent()))
						&& getElementName().equals(other.getElementName()) ));
	}
	
}
