/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.RProjectNature;
import org.eclipse.statet.internal.r.core.model.rpkg.RPkgDescrReconciler;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.model.core.impl.AbstractModelManager;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RModelManager;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.model.build.RPkgDescrSourceUnitModelContainer;
import org.eclipse.statet.r.core.model.build.RSourceUnitModelContainer;
import org.eclipse.statet.r.core.model.rlang.RChunkSrcStrElement;
import org.eclipse.statet.r.core.model.rlang.RSrcFrame;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public class RModelManagerImpl extends AbstractModelManager implements RModelManager {
	
	
	private static class RContextItem extends ContextItem {
		
		public final HashMap<String, SourceUnit> worksheets;
		
		public RContextItem(final WorkingContext context) {
			super(context);
			this.worksheets= new HashMap<>();
		}
		
	}
	
	
	private final RReconciler rReconciler= new RReconciler(this);
	private @Nullable RPkgDescrReconciler rPkgDescrReconciler;
	
	private final RModelEventJob eventJob= new RModelEventJob(this);
	
	private final RModelIndex index= new RModelIndex(this);
	
	
	public RModelManagerImpl() {
		super(RModel.R_TYPE_ID);
	}
	
	
	public void dispose() {
		this.eventJob.dispose();
		this.index.dispose();
	}
	
	
	public RModelEventJob getEventJob() {
		return this.eventJob;
	}
	
	public RModelIndex getIndex() {
		return this.index;
	}
	
	
	protected RReconciler getRReconciler() {
		return this.rReconciler;
	}
	
	protected synchronized RPkgDescrReconciler getRPkgDescrReconciler() {
		var reconciler= this.rPkgDescrReconciler;
		if (reconciler == null) {
			reconciler= new RPkgDescrReconciler(this);
			this.rPkgDescrReconciler= reconciler;
		}
		return reconciler;
	}
	
	
	@Override
	protected ContextItem doCreateContextItem(final WorkingContext context) {
		return new RContextItem(context);
	}
	
	@Override
	public void registerDependentUnit(final SourceUnit copy) {
		assert (copy.getModelTypeId().equals(RModel.R_TYPE_ID) ?
				copy.getElementType() == RSourceUnit.R_OTHER_SU : true);
		
		final RContextItem contextItem= (RContextItem)getContextItemCreate(copy.getWorkingContext());
		synchronized (contextItem) {
			final String key= copy.getId() + '+' + copy.getModelTypeId();
			contextItem.worksheets.put(key, copy);
		}
	}
	
	@Override
	public void deregisterDependentUnit(final SourceUnit copy) {
		final RContextItem contextItem= (RContextItem)getContextItemCreate(copy.getWorkingContext());
		synchronized (contextItem) {
			contextItem.worksheets.remove(copy.getId() + '+' + copy.getModelTypeId());
		}
	}
	
	public @Nullable SourceUnit getWorksheetCopy(final String type, final String id, final WorkingContext context) {
		final RContextItem contextItem= (RContextItem)getContextItem(context);
		if (contextItem != null) {
			synchronized (contextItem) {
				return contextItem.worksheets.get(id + '+' + type);
			}
		}
		return null;
	}
	
	
	@Override
	public void reconcile(final SourceUnitModelContainer<?, ?> adapter,
			final int level, final IProgressMonitor monitor) {
		if (adapter instanceof RSourceUnitModelContainer) {
			getRReconciler().reconcile((RSourceUnitModelContainer)adapter, level, monitor);
		}
		else if (adapter instanceof RPkgDescrSourceUnitModelContainer) {
			getRPkgDescrReconciler().reconcile((RPkgDescrSourceUnitModelContainer)adapter, level, monitor);
		}
	}
	
	@Override
	public RSourceUnitModelInfo reconcile(final RSourceUnit sourceUnit, final SourceUnitModelInfo modelInfo,
			final List<? extends RChunkSrcStrElement> chunks, final List<? extends SourceComponent> inlineNodes,
			final int level, final IProgressMonitor monitor) {
		if (sourceUnit == null) {
			throw new NullPointerException("sourceUnit"); //$NON-NLS-1$
		}
		return getRReconciler().reconcile(sourceUnit, modelInfo, chunks, inlineNodes, level, monitor);
	}
	
	
	@Override
	public @Nullable RSrcFrame<?> getProjectFrame(final RProject rProject) throws CoreException {
		return this.index.getProjectFrame(rProject);
	}
	
	@Override
	public Set<String> getPkgNames() {
		return this.index.getPkgNames();
	}
	
	@Override
	public @Nullable RSrcFrame<?> getPkgProjectFrame(final String pkgName) throws CoreException {
		final String projectName= this.index.getPkgProject(nonNullAssert(pkgName));
		if (projectName != null) {
			final IProject project= ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			final RProjectNature rProject= RProjectNature.getRProject(project);
			if (rProject != null) {
				return getProjectFrame(rProject);
			}
		}
		return null;
	}
	
	@Override
	public @Nullable List<SourceUnit> findReferencingSourceUnits(final RProject rProject, final RElementName name,
			final IProgressMonitor monitor) throws CoreException {
		return this.index.findReferencingSourceUnits(rProject, name, monitor);
	}
	
}
