/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.internal.r.core.RProjectNature;
import org.eclipse.statet.internal.r.core.rmodel.RModelManagerImpl;
import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;
import org.eclipse.statet.ltk.buildpath.core.BuildpathUtils;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.SourceUnitManager;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RWorkspaceSourceUnit;
import org.eclipse.statet.r.core.project.RBuildpaths;
import org.eclipse.statet.r.core.project.RIssues;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.project.RProjects;
import org.eclipse.statet.r.core.project.RTaskMarkerHandler;


@NonNullByDefault
public class RBuilder implements IResourceDeltaVisitor, IResourceVisitor {
	
	
	private static IContainer getContainer(final IProject project, final IPath path) {
		return (path.isEmpty()) ? project : project.getFolder(path);
	}
	
	
	private final SourceUnitManager suManager= LtkModels.getSourceUnitManager();
	
	private final List<IFile> toRemoveRSU= new ArrayList<>();
	private final ArrayList<RWorkspaceSourceUnit> toUpdateRSU= new ArrayList<>();
	
	private final RModelManagerImpl modelManager;
	
	private final IncrementalProjectBuilder projectBuilder;
	
	private MultiStatus status;
	
	private IProject project;
	/** Project relative path of R pkg root */
	private @Nullable IPath pkgRootPath;
	/** Project relative path of R pkg description file */
	private @Nullable IPath pkgDescriptionPath;
	private ImList<BuildpathElement> sourceContainters;
	private BuildpathElement currentSourceContainer;
	
	
	@SuppressWarnings("null")
	public RBuilder(final IncrementalProjectBuilder projectBuilder) {
		this.modelManager= RCorePlugin.getInstance().getRModelManager();
		this.projectBuilder= projectBuilder;
	}
	
	
	private void init(final RProject rProject) {
		this.project= rProject.getProject();
		this.status= new MultiStatus(RCore.BUNDLE_ID, 0, "R build status for " + this.project.getName(), null);
		
		this.sourceContainters= rProject.getRawBuildpath();
		final var pkgRootAbsPath= rProject.getPkgRootPath();
		if (pkgRootAbsPath != null) {
			this.pkgRootPath= pkgRootAbsPath.removeFirstSegments(1);
			this.pkgDescriptionPath= this.pkgRootPath.append(RBuildpaths.PKG_DESCRIPTION_FILE_PATH);
		}
		else {
			this.pkgRootPath= null;
			this.pkgDescriptionPath= null;
		}
		
		this.currentSourceContainer= null;
		
		initRd(rProject);
	}
	
	private boolean isValidSourceFolder(final IResource resource) {
		if (this.currentSourceContainer != null) {
			if (!BuildpathUtils.isExcluded(resource, this.currentSourceContainer)) {
				return true;
			}
		}
		return false;
	}
	
	@SuppressWarnings("null")
	private void clear(final SubMonitor m) {
		this.currentSourceContainer= null;
		
		if (!this.toUpdateRSU.isEmpty()) {
			for (final RSourceUnit su : this.toUpdateRSU) {
				if (su != null) {
					su.disconnect(m);
				}
			}
			this.toUpdateRSU.clear();
		}
		this.toRemoveRSU.clear();
		
		this.project= null;
	}
	
	
	public IStatus buildFull(final RProject rProject, final IProgressMonitor monitor) {
		init(rProject);
		
		final SubMonitor m= SubMonitor.convert(monitor, 2 + 10 + 1);
		try {
			m.subTask(NLS.bind("Collecting resource changes of ''{0}''.", this.project.getName()));
			IContainer pkgRoot= null;
			if (this.pkgRootPath != null) {
				pkgRoot= visitPkgRoot(getContainer(this.project, this.pkgRootPath));
			}
			m.worked(1);
			
			for (final BuildpathElement sourceContainer : this.sourceContainters) {
				final IResource resource= this.project.findMember(
						sourceContainer.getPath().removeFirstSegments(1) );
				if (resource != null) {
					this.currentSourceContainer= sourceContainer;
					resource.accept(this);
				}
				
				if (m.isCanceled()) {
					throw new OperationCanceledException();
				}
			}
			m.worked(1);
			
			this.modelManager.getIndex().update(rProject, pkgRoot,
					null, this.toUpdateRSU,
					this.status, m.newChild(10) );
		}
		catch (final CoreException e) {
			this.status.add(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					"An error occurred when indexing the project.", e) );
		}
		finally {
			clear(m);
		}
		return this.status;
	}
	
	public IStatus buildIncremental(final RProject rProject, final IResourceDelta delta, final IProgressMonitor monitor) {
		init(rProject);
		
		final SubMonitor m= SubMonitor.convert(monitor, 2 + 10 + 1);
		try {
			m.subTask(NLS.bind("Collecting resource changes of ''{0}''.", this.project.getName()));
			IContainer pkgRoot= null;
			if (this.pkgRootPath != null) {
				pkgRoot= visitPkgRoot(delta.findMember(this.pkgRootPath));
			}
			m.worked(1);
			
			for (final BuildpathElement sourceContainer : this.sourceContainters) {
				final IResourceDelta sourceDelta= delta.findMember(
						sourceContainer.getPath().removeFirstSegments(1) );
				if (sourceDelta != null) {
					this.currentSourceContainer= sourceContainer;
					sourceDelta.accept(this);
				}
				
				if (m.isCanceled()) {
					throw new OperationCanceledException();
				}
			}
			m.worked(1);
			
			this.modelManager.getIndex().update(rProject, pkgRoot,
					this.toRemoveRSU, this.toUpdateRSU,
					this.status, m.newChild(10) );
		}
		catch (final CoreException e) {
			this.status.add(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					"An error occurred when indexing the project.", e ));
		}
		finally {
			clear(m);
		}
		return this.status;
	}
	
	public void clean(final IProject project, final IProgressMonitor monitor) {
		this.project= project;
		
		final SubMonitor m= SubMonitor.convert(monitor, 10 + 1);
		try {
			this.project.deleteMarkers(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, false, IResource.DEPTH_INFINITE);
			
			clearAllMarkers(this.project, IResource.DEPTH_INFINITE);
			
			this.modelManager.getIndex().clear(this.project);
		}
		catch (final CoreException e) {
			this.status.add(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					"An error occurred when indexing the project.", e) );
		}
		finally {
			clear(m);
		}
	}
	
	
	private IContainer visitPkgRoot(final IContainer pkgRoot) throws CoreException {
		this.project.deleteMarkers(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, false, IResource.DEPTH_INFINITE);
		
		if (pkgRoot.exists()) {
			visitPkgFile(pkgRoot.findMember(RBuildpaths.PKG_DESCRIPTION_FILE_PATH));
		}
		
		return pkgRoot;
	}
	
	private @Nullable IContainer visitPkgRoot(final @Nullable IResourceDelta delta) throws CoreException {
		if (delta != null
				&& delta.getResource() instanceof final IContainer pkgRoot ) {
			switch (delta.getKind()) {
			
			case IResourceDelta.ADDED:
			case IResourceDelta.CHANGED:
				boolean changed= false;
				changed|= visitPkgFile(delta.findMember(RBuildpaths.PKG_DESCRIPTION_FILE_PATH));
				if (changed) {
					this.project.deleteMarkers(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, false, IResource.DEPTH_ZERO);
					pkgRoot.deleteMarkers(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, false, IResource.DEPTH_ZERO);
					return pkgRoot;
				}
				break;
				
			case IResourceDelta.REMOVED:
				this.project.deleteMarkers(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, false, IResource.DEPTH_ZERO);
				if ((delta.getFlags() & IResourceDelta.MOVED_TO) != 0) {
					final IResource movedTo= pkgRoot.getWorkspace().getRoot().findMember(delta.getMovedToPath());
					if (movedTo != null && !isRPkgRootLocation(movedTo)) {
						pkgRoot.deleteMarkers(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, false, IResource.DEPTH_ZERO);
					}
				}
				return pkgRoot;
			}
		}
		return null;
	}
	
	
	private void visitPkgFile(final @Nullable IResource resource) {
		if (resource instanceof IFile) {
			clearAllMarkers(resource, IResource.DEPTH_ZERO);
		}
	}
	
	private boolean visitPkgFile(final @Nullable IResourceDelta delta) throws CoreException {
		final IResource resource;
		if (delta != null
				&& (resource= delta.getResource()) instanceof IFile) {
			switch (delta.getKind()) {
			
			case IResourceDelta.ADDED:
			case IResourceDelta.CHANGED:
				clearAllMarkers(resource, IResource.DEPTH_ZERO);
				break;
				
			case IResourceDelta.REMOVED:
				break;
			}
			
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean visit(final IResource resource) throws CoreException {
		try {
			if (!isValidSourceFolder(resource)) {
				return false;
			}
			
			if (resource instanceof final IFile file) {
				final IContentDescription contentDescription= file.getContentDescription();
				if (contentDescription == null) {
					return true;
				}
				final IContentType contentType= contentDescription.getContentType();
				if (contentType == null) {
					return true;
				}
				switch (contentType.getId()) {
				case RCore.RPKG_DESCRIPTION_CONTENT_ID: {
						if (!resource.getProjectRelativePath().equals(this.pkgDescriptionPath)) {
							clearAllMarkers(resource, IResource.DEPTH_ZERO);
						}
						return true;
					}
				case RCore.R_CONTENT_ID: {
						clearRMarkers(resource, IResource.DEPTH_INFINITE);
						this.toUpdateRSU.add((RWorkspaceSourceUnit)this.suManager.getSourceUnit(
								RModel.R_TYPE_ID, Ltk.PERSISTENCE_CONTEXT, file, null ));
						return true;
					}
				case RCore.RD_CONTENT_ID: {
						clearRMarkers(resource, IResource.DEPTH_INFINITE);
						doParseRd(file);
						return true;
					}
				default:
					break;
				}
			}
			
			return true;
		}
		catch (final StatusException | CoreException e) {
			this.status.add(new Status(IStatus.ERROR, RCore.BUNDLE_ID,
					String.format("An error occurred when checking file '%1$s'",
							resource.getProjectRelativePath() ),
					e ));
			return false;
		}
	}
	
	@Override
	public boolean visit(final IResourceDelta delta) throws CoreException {
		final IResource resource= delta.getResource();
		try {
			if (!isValidSourceFolder(resource)) {
				return false;
			}
			
			switch (delta.getKind()) {
			
			case IResourceDelta.ADDED:
			case IResourceDelta.CHANGED:
				if (resource instanceof final IFile file) {
					final IContentDescription contentDescription= file.getContentDescription();
					if (contentDescription == null) {
						return true;
					}
					final IContentType contentType= contentDescription.getContentType();
					if (contentType == null) {
						return true;
					}
					switch (contentType.getId()) {
					case RCore.RPKG_DESCRIPTION_CONTENT_ID: {
							if (!delta.getProjectRelativePath().equals(this.pkgDescriptionPath)) {
								clearAllMarkers(resource, IResource.DEPTH_ZERO);
							}
							return true;
						}
					case RCore.R_CONTENT_ID: {
							clearRMarkers(resource, IResource.DEPTH_ZERO);
							this.toUpdateRSU.add((RWorkspaceSourceUnit)this.suManager.getSourceUnit(
									RModel.R_TYPE_ID, Ltk.PERSISTENCE_CONTEXT, file, null ));
							return true;
						}
					case RCore.RD_CONTENT_ID: {
							clearRMarkers(resource, IResource.DEPTH_ZERO);
							doParseRd(file);
							return true;
						}
					default:
						break;
					}
				}
				return true;
			
			case IResourceDelta.REMOVED:
				if ((delta.getFlags() & IResourceDelta.MOVED_TO) != 0) {
					final IResource movedTo= resource.getWorkspace().getRoot().findMember(delta.getMovedToPath());
					if (movedTo != null && !isRSourceLocation(movedTo)) {
						clearAllMarkers(movedTo, IResource.DEPTH_INFINITE);
					}
				}
				if (resource instanceof IFile) {
					this.toRemoveRSU.add((IFile) resource);
				}
				return true;
			}
			
			return true;
		}
		catch (final StatusException | CoreException e) {
			this.status.add(new Status(IStatus.ERROR, RCore.BUNDLE_ID,
					String.format("An error occurred when checking file '%1$s'",
							resource.getProjectRelativePath() ),
					e ));
			return false;
		}
	}
	
	
	private boolean isRSourceLocation(final IResource resource) throws CoreException {
		final IProject project= resource.getProject();
		if (project == this.project) {
			// TODO check buildpath
			return true;
		}
		if (project.hasNature(RProjects.R_NATURE_ID)) {
			// TODO check buildpath
			return true;
		}
		return false;
	}
	
	private boolean isRPkgRootLocation(final IResource resource) throws CoreException {
		final IProject project= resource.getProject();
		if (project == this.project) {
			return (resource.getProjectRelativePath().equals(this.pkgRootPath));
		}
		else if (project.hasNature(RProjects.R_PKG_NATURE_ID)) {
			final RProjectNature rProject= RProjectNature.getRProject(project);
			return (rProject != null && resource.getFullPath().equals(rProject.getPkgRootPath()));
		}
		return false;
	}
	
	private void clearAllMarkers(final IResource resource, final int depth) {
		try {
			resource.deleteMarkers(RIssues.R_MODEL_PROBLEM_MARKER_TYPE, false, depth);
			resource.deleteMarkers(RIssues.RPKG_MODEL_PROBLEM_MARKER_TYPE, false, depth);
			resource.deleteMarkers(RIssues.TASK_MARKER_TYPE, false, depth);
		}
		catch (final CoreException e) {
			RCorePlugin.logError("R Builder: Failed to remove old markers.", e);
		}
	}
	
	private void clearRMarkers(final IResource resource, final int depth) {
		try {
			resource.deleteMarkers(RIssues.R_MODEL_PROBLEM_MARKER_TYPE, false, depth);
			resource.deleteMarkers(RIssues.TASK_MARKER_TYPE, false, depth);
		}
		catch (final CoreException e) {
			RCorePlugin.logError("R Builder: Failed to remove old markers.", e);
		}
	}
	
	
/*-- Rd --*/
	
	private final RTaskMarkerHandler taskMarkerHandler= new RTaskMarkerHandler();
	
	protected void initRd(final RProject project) {
		this.taskMarkerHandler.init(project);
	}
	
	protected void doParseRd(final IFile file) throws CoreException {
		try {
			final SourceContent sourceContent= new SourceContent(0, file.readString());
			this.taskMarkerHandler.setup(sourceContent, file);
			new RdParser(sourceContent, this.taskMarkerHandler).check();
		}
		catch (final CoreException e) {
			this.status.add(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					NLS.bind("An error occurred when parsing Rd file ''{0}''", file.getFullPath().toString()),
					e ));
		}
	}
	
}
