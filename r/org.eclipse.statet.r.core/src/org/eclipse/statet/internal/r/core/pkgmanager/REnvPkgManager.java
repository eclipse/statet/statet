/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.internal.r.core.renv.REnvManagerImpl;
import org.eclipse.statet.r.core.renv.IREnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnv;


public class REnvPkgManager {
	
	
	private final REnvManagerImpl rEnvManager;
	
	private final Map<REnv, RPkgManagerImpl> rPkgManagers= new HashMap<>();
	
	
	public REnvPkgManager(final REnvManagerImpl rEnvManager) {
		this.rEnvManager= rEnvManager;
	}
	
	
	public synchronized RPkgManagerImpl getManager(REnv rEnv) {
		RPkgManagerImpl mgr= null;
		rEnv= rEnv.resolve();
		if (rEnv != null && !rEnv.isDeleted()) {
			mgr= this.rPkgManagers.get(rEnv);
			if (mgr == null) {
				final IREnvConfiguration rEnvConfig= rEnv.get(IREnvConfiguration.class);
				if (rEnvConfig != null) {
					mgr= new RPkgManagerImpl(rEnvConfig);
					this.rPkgManagers.put(rEnv, mgr);
				}
			}
		}
		return mgr;
	}
	
}
