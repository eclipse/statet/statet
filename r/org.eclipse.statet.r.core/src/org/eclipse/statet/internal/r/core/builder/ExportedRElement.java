/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.IOException;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.rlang.RLangSrcElement;


@NonNullByDefault
public class ExportedRElement implements RLangSrcElement<RLangSrcElement<?>> {
	
	
	private final RLangSrcElement<?> parent;
	
	private final int elementType;
	private final RElementName elementName;
	private final String elementId;
	
	private final int sourceOffset;
	private final int sourceLength;
	private final int nameOffset;
	private final int nameLength;
	
	
	public ExportedRElement(final RLangSrcElement<?> parent,
			final RLangSrcElement<?> sourceElement) {
		this.parent= parent;
		this.elementType= sourceElement.getElementType();
		this.elementName= RElementName.cloneName(sourceElement.getElementName(), false);
		this.elementId= sourceElement.getId();
		
		{	final TextRegion sourceRange= sourceElement.getSourceRange();
			if (sourceRange != null) {
				this.sourceOffset= sourceRange.getStartOffset();
				this.sourceLength= sourceRange.getLength();
			}
			else {
				this.sourceOffset= -1;
				this.sourceLength= 0;
			}
		}
		{
			final TextRegion sourceRange= sourceElement.getNameSourceRange();
			if (sourceRange != null) {
				this.nameOffset= sourceRange.getStartOffset();
				this.nameLength= sourceRange.getLength();
			}
			else {
				this.nameOffset= -1;
				this.nameLength= 0;
			}
		}
	}
	
	
	ExportedRElement(final RLangSrcElement<?> parent, final DataStream in, final byte o) throws IOException {
		this.parent= parent;
		
		this.elementType= in.readInt();
		this.elementName= RElementName.readNonNull(in);
		this.elementId= in.readNonNullString();
		
		this.sourceOffset= in.readInt();
		this.sourceLength= in.readInt();
		this.nameOffset= in.readInt();
		this.nameLength= in.readInt();
	}
	
	void writeTo(final DataStream out, final byte o) throws IOException {
		out.writeByte(o);
		out.writeInt(this.elementType);
		RElementName.write(this.elementName, out);
		out.writeString(this.elementId);
		
		out.writeInt(this.sourceOffset);
		out.writeInt(this.sourceLength);
		out.writeInt(this.nameOffset);
		out.writeInt(this.nameLength);
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public String getId() {
		return this.elementId;
	}
	
	@Override
	public int getElementType() {
		return this.elementType;
	}
	
	@Override
	public RElementName getElementName() {
		return this.elementName;
	}
	
	@Override
	public boolean exists() {
		return true;
	}
	
	@Override
	public boolean isReadOnly() {
		return false;
	}
	
	
	@Override
	public RLangSrcElement<?> getModelParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangSrcElement<?>> filter) {
		return false;
	}
	
	@Override
	public List<? extends RLangSrcElement<?>> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangSrcElement<?>> filter) {
		return ImCollections.emptyList();
	}
	
	
	@Override
	public SourceUnit getSourceUnit() {
		return this.parent.getSourceUnit();
	}
	
	@Override
	public @Nullable TextRegion getSourceRange() {
		if (this.sourceOffset >= 0) {
			return new BasicTextRegion(this.sourceOffset, this.sourceOffset + this.sourceLength);
		}
		return null;
	}
	
	@Override
	public @Nullable TextRegion getNameSourceRange() {
		if (this.nameOffset >= 0) {
			return new BasicTextRegion(this.nameOffset, this.nameOffset + this.nameLength);
		}
		return null;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return null;
	}
	
}
