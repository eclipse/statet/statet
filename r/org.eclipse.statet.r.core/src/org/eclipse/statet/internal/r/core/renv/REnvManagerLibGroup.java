/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.renv;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.internal.r.core.Messages;
import org.eclipse.statet.r.core.renv.RLibGroupWorkingCopy;
import org.eclipse.statet.rj.renv.core.BasicRLibGroup;
import org.eclipse.statet.rj.renv.core.BasicRLibLocation;
import org.eclipse.statet.rj.renv.core.RLibGroup;
import org.eclipse.statet.rj.renv.core.RLibLocation;


public abstract class REnvManagerLibGroup {
	
	
	public static String getLabel(final String id) {
		if (id.equals(RLibGroup.R_DEFAULT)) {
			return Messages.REnvConfiguration_DefaultLib_label;
		}
		if (id.equals(RLibGroup.R_SITE)) {
			return Messages.REnvConfiguration_SiteLibs_label;
		}
		if (id.equals(RLibGroup.R_USER)) {
			return Messages.REnvConfiguration_UserLibs_label;
		}
		if (id.equals(RLibGroup.R_OTHER)) {
			return Messages.REnvConfiguration_OtherLibs_label;
		}
		return null;
	}
	
	
	public static class Final extends BasicRLibGroup {
		
		
		private static ImList<RLibLocation> copy(final List<? extends RLibLocation> locations) {
			final RLibLocation[] copies= new RLibLocation[locations.size()];
			for (int i= 0; i < copies.length; i++) {
				copies[i]= new BasicRLibLocation(locations.get(i));
			}
			return ImCollections.newList(copies);
		}
		
		public Final(final String id, final String label, final List<RLibLocation> libraries) {
			super(id, label, ImCollections.toList(libraries));
		}
		
		public Final(final RLibGroup template) {
			super(template.getId(), template.getLabel(), copy(template.getLibLocations()));
		}
		
	}
	
	public static class Editable extends BasicRLibGroup implements RLibGroupWorkingCopy {
		
		
		private static List<RLibLocation> copy(final List<? extends RLibLocation> locations) {
			return new ArrayList<>(locations);
		}
		
		
		public Editable(final RLibGroup template) {
			super(template.getId(), template.getLabel(), copy(template.getLibLocations()));
		}
		
		public Editable(final String id, final String label) {
			super(id, label, new ArrayList<>());
		}
		
		
		@Override
		public List<RLibLocation> getLibLocations() {
			return (List<RLibLocation>) super.getLibLocations();
		}
		
		@Override
		public RLibLocation newLibrary(final String path) {
			return new BasicRLibLocation(RLibLocation.USER, path, null);
		}
		
		@Override
		public RLibLocation setLibrary(final RLibLocation library, final String directory) {
			final List<RLibLocation> list= getLibLocations();
			final int idx= list.indexOf(library);
			final RLibLocation newLibrary= newLibrary(directory);
			list.set(idx, newLibrary);
			return newLibrary;
		}
		
	}
	
}
