/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.eclipse.statet.internal.r.core.rmodel.RSourceModel.NO_R_SOURCE_STRUCT_CHILDREN;

import java.util.List;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.rlang.RFrame;
import org.eclipse.statet.r.core.model.rlang.RLangSrcStrElement;


@NonNullByDefault
public abstract class EmbeddedRBuildElement implements BuildSourceFrameElement {
	
	
	protected final SourceStructElement<?, ?> parent;
	protected final AstNode node;
	
	private final RElementName name;
	protected final @Nullable TextRegion nameRegion;
	int occurrenceCount;
	
	BuildSourceFrame envir;
	List<? extends RLangSrcStrElement> sourceChildrenProtected= NO_R_SOURCE_STRUCT_CHILDREN;
	
	
	public EmbeddedRBuildElement(final SourceStructElement<?, ?> parent, final AstNode node,
			final RElementName name, final @Nullable TextRegion nameRegion) {
		this.parent= parent;
		this.node= node;
		
		this.name= name;
		this.nameRegion= nameRegion;
	}
	
	
	@Override
	public void setSourceChildren(final List<? extends RLangSrcStrElement> children) {
		this.sourceChildrenProtected= children;
	}
	
	@Override
	public BuildSourceFrame getBuildFrame() {
		return this.envir;
	}
	
	
	@Override
	public final String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public RElementName getElementName() {
		return this.name;
	}
	
	@Override
	public final String getId() {
		final String name= getElementName().getDisplayName();
		final StringBuilder sb= new StringBuilder(name.length() + 10);
		sb.append(Integer.toHexString(getElementType() & MASK_C12));
		sb.append(':');
		sb.append(name);
		sb.append('#');
		sb.append(this.occurrenceCount);
		return sb.toString();
	}
	
	
	@Override
	public RSourceUnit getSourceUnit() {
		return (RSourceUnit)this.parent.getSourceUnit();
	}
	
	@Override
	public boolean exists() {
		return this.parent.exists();
	}
	
	@Override
	public boolean isReadOnly() {
		return this.parent.isReadOnly();
	}
	
	
	@Override
	public @Nullable RElement<?> getModelParent() {
		return null;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter) {
		return false;
	}
	
	@Override
	public List<? extends RLangSrcStrElement> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter) {
		return NO_R_SOURCE_STRUCT_CHILDREN;
	}
	
	
	@Override
	public @Nullable SourceStructElement<?, ?> getSourceParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter) {
		return LtkModelUtils.<RLangSrcStrElement>hasChildren(this.sourceChildrenProtected, filter);
	}
	
	@Override
	public List<? extends RLangSrcStrElement> getSourceChildren(final @Nullable LtkModelElementFilter<? super RLangSrcStrElement> filter) {
		return LtkModelUtils.<RLangSrcStrElement>getChildren(this.sourceChildrenProtected, filter);
	}
	
	@Override
	public TextRegion getSourceRange() {
		return this.node;
	}
	
	@Override
	public @Nullable TextRegion getNameSourceRange() {
		return this.nameRegion;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RFrame.class) {
			return (T)this.envir;
		}
		if (adapterType == AstNode.class) {
			return (T)this.node;
		}
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return RElement.C12_SOURCE_CHUNK * Objects.hashCode(this.name.getSegmentName()) + this.occurrenceCount;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final EmbeddedRBuildElement other
						&& this.occurrenceCount == other.occurrenceCount
						&& this.name.equals(other.name) ));
	}
	
}
