/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.dcf.core.source.DcfLexer;
import org.eclipse.statet.dsl.dcf.core.source.ast.DcfRParser;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;
import org.eclipse.statet.r.core.model.RPkgDescriptions;


@NonNullByDefault
public class RPkgDescrDcfParser extends DcfRParser {
	
	
	public RPkgDescrDcfParser(final int level, final DcfLexer lexer) {
		super(level, lexer);
	}
	
	public RPkgDescrDcfParser() {
		super();
	}
	
	
	@Override
	protected @Nullable String getEmbeddingType(final @Nullable String name) {
		if (name != null) {
			final var fieldDef= RPkgDescriptions.getFieldDefinition(name);
			if (fieldDef != null && fieldDef.getDataType() == RPkgDescrFieldDefinition.R_SYNTAX) {
				return RModel.R_TYPE_ID;
			}
		}
		return null;
	}
	
}
