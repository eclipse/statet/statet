/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.model.DslElementName;
import org.eclipse.statet.dsl.dcf.core.source.ast.Field;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrField;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceStructElement;


@NonNullByDefault
public class RPkgDescrFieldSourceElement extends BasicRPkgDescrSourceElement
		implements RPkgDescrField<RPkgDescrSourceStructElement> {
	
	
	private final RPkgDescrContainerSourceElement parent;
	
	private final @Nullable RPkgDescrFieldDefinition fieldDef;
	
	
	public RPkgDescrFieldSourceElement(final RPkgDescrContainerSourceElement parent,
			final Field astNode, final @Nullable String name,
			final @Nullable RPkgDescrFieldDefinition fieldDef) {
		super(astNode);
		this.parent= parent;
		this.name= DslElementName.create(C12_FIELD, (fieldDef != null) ? fieldDef.getName() : name);
		this.nameRegion= astNode.getKey();
		
		this.fieldDef= fieldDef;
	}
	
	
	@Override
	public int getElementType() {
		return C12_FIELD;
	}
	
	@Override
	public SourceUnit getSourceUnit() {
		return this.parent.getSourceUnit();
	}
	
	@Override
	public boolean exists() {
		return this.parent.exists();
	}
	
	@Override
	public boolean isReadOnly() {
		return this.parent.isReadOnly();
	}
	
	
	@Override
	public RPkgDescrContainerSourceElement getSourceParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return false;
	}
	
	@Override
	public List<? extends SourceStructElement<?, ?>> getSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return NO_CHILDREN;
	}
	
	@Override
	public RPkgDescrContainerSourceElement getModelParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RPkgDescrSourceStructElement> filter) {
		return false;
	}
	
	@Override
	public List<? extends RPkgDescrSourceStructElement> getModelChildren(final @Nullable LtkModelElementFilter<? super RPkgDescrSourceStructElement> filter) {
		return NO_CHILDREN;
	}
	
	
	public Field getAstNode() {
		return (Field)this.astNode;
	}
	
	@Override
	public @Nullable RPkgDescrFieldDefinition getFieldDef() {
		return this.fieldDef;
	}
	
	@Override
	public @Nullable String getValueText() {
		return getAstNode().getValue().getText();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == Field.class) {
			return (T)getAstNode();
		}
		return super.getAdapter(adapterType);
	}
	
	
}
