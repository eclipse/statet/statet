/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.IOException;

import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RFunctionSpec;
import org.eclipse.statet.r.core.model.rlang.RLangSrcElement;
import org.eclipse.statet.r.core.model.rlang.RMethodSrcElement;


@NonNullByDefault
public class ExportedRMethod extends ExportedRElement
		implements RMethodSrcElement<RLangSrcElement<?>> {
	
	
	private final @Nullable RFunctionSpec fSpec;
	
	
	public ExportedRMethod(final RLangSrcElement<?> parent,
			final RMethodSrcElement<?> sourceElement) {
		super(parent, sourceElement);
		this.fSpec= sourceElement.getFunctionSpec();
	}
	
	
	static final byte SER_ELEMENT_TYPE= 2;
	static final byte SER_WITH_SPEC= 1 << 6;
	
	ExportedRMethod(final RLangSrcElement<?> parent, final DataStream in, final byte o) throws IOException {
		super(parent, in, o);
		this.fSpec= ((o & SER_WITH_SPEC) != 0) ? new RFunctionSpec(in, (byte)1) : null;
	}
	
	@Override
	void writeTo(final DataStream out, byte o) throws IOException {
		o= SER_ELEMENT_TYPE;
		if (this.fSpec != null) {
			o|= SER_WITH_SPEC;
		}
		super.writeTo(out, o);
		if (this.fSpec != null) {
			this.fSpec.writeTo(out, (byte)1);
		}
	}
	
	
	@Override
	public @Nullable RFunctionSpec getFunctionSpec() {
		return this.fSpec;
	}
	
}
