/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.pkgmanager.RView;


@NonNullByDefault
class RViewImpl implements RView {
	
	
	private final String name;
	private String topic;
	
	private final ImList<Pkg> pkgs;
	
	
	public RViewImpl(final String name, final String topic, final ImList<Pkg> pkgs) {
		this.name= nonNullAssert(name);
		this.topic= nonNullElse(topic, ""); //$NON-NLS-1$
		this.pkgs= nonNullAssert(pkgs);
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	
	public void setTopic(final String topic) {
		this.topic= topic;
	}
	
	@Override
	public String getTopic() {
		return this.topic;
	}
	
	@Override
	public ImList<Pkg> getPkgList() {
		return this.pkgs;
	}
	
	
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RView other 
						&& this.name.equals(other.getName()) ));
	}
	
}
