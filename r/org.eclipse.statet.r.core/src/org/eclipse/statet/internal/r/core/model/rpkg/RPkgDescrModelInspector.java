/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.issues.core.Problem.SEVERITY_ERROR;
import static org.eclipse.statet.ltk.issues.core.Problem.SEVERITY_WARNING;
import static org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceConstants.TYPE12_FIELD_DUPLICATE;
import static org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceConstants.TYPE12_FIELD_REQUIRED_MISSING;
import static org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID;
import static org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.OperationCanceledException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.dcf.core.source.ast.Field;
import org.eclipse.statet.dsl.dcf.core.source.ast.Value;
import org.eclipse.statet.ltk.ast.core.Asts;
import org.eclipse.statet.ltk.core.StatusCodes;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.impl.AbstractSourceIssueReporter;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;
import org.eclipse.statet.r.core.model.RPkgDescriptions;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceUnitModelInfo;


@NonNullByDefault
public class RPkgDescrModelInspector extends AbstractSourceIssueReporter {
	
	
	private final HashMap<String, Object> fieldsMap= new HashMap<>();
	
	
	public RPkgDescrModelInspector() {
		super(RModel.RPKG_DESCRIPTION_TYPE_ID);
	}
	
	
	@Override
	protected void clear() {
		super.clear();
		
		this.fieldsMap.clear();
	}
	
	public void run(final RPkgDescrSourceUnitModelInfo modelInfo,
			final SourceContent content,
			final IssueRequestor requestor) {
		try {
			init(content, requestor);
			
			checkFields(modelInfo);
			
			flush();
		}
		catch (final OperationCanceledException e) {}
		finally {
			clear();
		}
	}
	
	
	private void checkFields(final RPkgDescrSourceUnitModelInfo modelInfo) {
		String pkgType= RPkgDescriptions.PACKAGE_PKG_TYPE;
		
		final var missingRequired= new ArrayList<RPkgDescrFieldDefinition>();
		for (final var fieldDef : RPkgDescriptions.getRequiredFieldDefinitions()) {
			final var field= modelInfo.getField(fieldDef.getName());
			if (field == null) {
				missingRequired.add(fieldDef);
				continue;
			}
			if (fieldDef == RPkgDescriptions.Type_FIELD) {
				final String value= field.getValueText();
				if (value != null) {
					pkgType= value;
				}
			}
		}
		if (modelInfo.getField(RPkgDescriptions.Authors_R_FIELD.getName()) != null) {
			missingRequired.remove(RPkgDescriptions.Author_FIELD);
			missingRequired.remove(RPkgDescriptions.Maintainer_FIELD);
		}
		if (!missingRequired.isEmpty()) {
			((BasicRPkgDescrSourceElement)modelInfo.getSourceElement())
					.setModelStatusCode(TYPE12_FIELD_REQUIRED_MISSING);
			for (final var fieldDef : missingRequired) {
				addProblem(SEVERITY_ERROR, TYPE12_FIELD_REQUIRED_MISSING,
						getMessageBuilder().bind(Messages.Descr_FieldRequiredMissing_message,
								fieldDef.getName() ));
			}
		}
		
		final var fields= modelInfo.getFields();
		for (final var field : fields) {
			final String name= field.getElementName().getSegmentName();
			if (name == null) {
				continue;
			}
			final RPkgDescrFieldSourceElement element= (RPkgDescrFieldSourceElement)field;
			final Object prev= this.fieldsMap.put(name, element);
			if (prev != null) {
				if (prev instanceof List<?>) {
					this.fieldsMap.put(name, ImCollections.addElement((List<RPkgDescrFieldSourceElement>)prev, element));
				}
				else {
					this.fieldsMap.put(name, ImCollections.newList(prev, element));
				}
			}
			
			final RPkgDescrFieldDefinition fieldDef= element.getFieldDef();
			if (fieldDef != null) {
				final Field fieldNode= nonNullAssert(element.getAdapter(Field.class));
				if (Asts.hasErrors(fieldNode)) {
					element.setModelStatusCode(StatusCodes.ERROR);
					continue;
				}
				if (fieldDef == RPkgDescriptions.Encoding_FIELD) {
					requireNonEmptyValue(element, fieldNode, SEVERITY_ERROR);
					continue;
				}
				if (fieldDef == RPkgDescriptions.Type_FIELD) {
					requireNonEmptyValue(element, fieldNode, SEVERITY_ERROR);
					continue;
				}
				if (fieldDef == RPkgDescriptions.Package_FIELD) {
					checkRPkgName(element, fieldNode, pkgType);
					continue;
				}
				if (fieldDef == RPkgDescriptions.License_FIELD) {
					requireNonEmptyValue(element, fieldNode, SEVERITY_ERROR);
					continue;
				}
				if (fieldDef == RPkgDescriptions.Author_FIELD
						|| fieldDef == RPkgDescriptions.Maintainer_FIELD
						|| fieldDef == RPkgDescriptions.Authors_R_FIELD) {
					requireNonEmptyValue(element, fieldNode, SEVERITY_ERROR);
					continue;
				}
				if (fieldDef == RPkgDescriptions.NeedsCompilation_FIELD) {
					requireNonEmptyValue(element, fieldNode, SEVERITY_ERROR);
					continue;
				}
				
				switch (fieldDef.getDataType()) {
				case RPkgDescrFieldDefinition.LOGICAL:
					checkLogicalValue(fieldDef, element, fieldNode, SEVERITY_ERROR);
					continue;
				case RPkgDescrFieldDefinition.URL:
					checkUrlValue(fieldDef, element, fieldNode, SEVERITY_WARNING);
					continue;
				default:
					continue;
				}
			}
		}
		for (final var entry : this.fieldsMap.entrySet()) {
			if (entry.getValue() instanceof List<?>) {
				final var message= getMessageBuilder().bind(Messages.Descr_FieldDuplicate_message,
						entry.getKey() );
				final var elements= (List<RPkgDescrFieldSourceElement>)entry.getValue();
				for (final var element : elements) {
					final TextRegion range= element.getNameSourceRange();
					addProblem(element, SEVERITY_WARNING, TYPE12_FIELD_DUPLICATE, message,
							range.getStartOffset(), range.getEndOffset() );
				}
			}
		}
	}
	
	private @Nullable String requireNonEmptyValue(final RPkgDescrFieldSourceElement element,
			final Field fieldNode, final int problemSeverity) {
		final String text= element.getValueText();
		if (text == null || text.isEmpty()) {
			final var valueNode= fieldNode.getValue();
			addProblem(element, problemSeverity, TYPE12_FIELD_VALUE_MISSING,
					getMessageBuilder().bind(Messages.Descr_FieldValueMissing_message,
							element.getElementName().getDisplayName() ),
					(valueNode.getLength() == 0) ? valueNode.getStartOffset() - 1 : valueNode.getStartOffset(), valueNode.getEndOffset());
			return null;
		}
		return text;
	}
	
	private void checkRPkgName(final RPkgDescrFieldSourceElement element, final Field fieldNode,
			final String pkgType) {
		final String text= requireNonEmptyValue(element, fieldNode, SEVERITY_ERROR);
		if (text == null) {
			return;
		}
		final int l= text.length();
		for (int idx= 0; idx < l; idx++) {
			switch (text.charAt(idx)) {
			case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
			case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			case '.':
				continue;
			default:
				addProblem(element, SEVERITY_ERROR, TYPE12_FIELD_VALUE_INVALID,
						getMessageBuilder().bind(Messages.RPkgName_InvalidChar_message,
								getMessageUtil().checkFullQuoteText(text) ),
						getValueTextStartOffset(fieldNode, idx), getValueTextEndOffset(fieldNode, idx + 1) );
				return;
			}
		}
		{	switch (text.charAt(0)) {
			case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
			case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
				break;
			default:
				addProblem(element, SEVERITY_ERROR, TYPE12_FIELD_VALUE_INVALID,
						getMessageBuilder().bind(Messages.RPkgName_InvalidFirstChar_message,
								getMessageUtil().checkFullQuoteText(text) ),
						getValueTextStartOffset(fieldNode, 0), getValueTextEndOffset(fieldNode, 1) );
				return;
			}
		}
		{	if (l < 2) {
				addValueProblem(element, SEVERITY_ERROR, TYPE12_FIELD_VALUE_INVALID,
						getMessageBuilder().bind(Messages.RPkgName_InvalidSingleChar_message,
								getMessageUtil().checkFullQuoteText(text) ),
						fieldNode );
				return;
			}
		}
		{	if (text.charAt(l - 1) == '.') {
				addProblem(element, SEVERITY_ERROR, TYPE12_FIELD_VALUE_INVALID,
						getMessageBuilder().bind(Messages.RPkgName_InvalidEndCharDot_message,
								getMessageUtil().checkFullQuoteText(text) ),
						getValueTextStartOffset(fieldNode, l - 1), getValueTextEndOffset(fieldNode, l) );
				return;
			}
		}
	}
	
	private void checkLogicalValue(final RPkgDescrFieldDefinition fieldDef,
			final RPkgDescrFieldSourceElement element, final Field fieldNode,
			final int problemSeverity) {
		final String text= requireNonEmptyValue(element, fieldNode, problemSeverity);
		if (text == null) {
			return;
		}
		if (RPkgDescriptions.parseLogical(text) < 0) {
			addValueProblem(element, problemSeverity, TYPE12_FIELD_VALUE_INVALID,
					getMessageBuilder().bind(Messages.Descr_FieldValueInvalid_LogiLiteral_message,
							fieldDef.getName() ),
					fieldNode );
			return;
		}
	}
	
	private void checkUrlValue(final RPkgDescrFieldDefinition fieldDef,
			final RPkgDescrFieldSourceElement element, final Field fieldNode,
			final int problemSeverity) {
		final String text= requireNonEmptyValue(element, fieldNode, problemSeverity);
		if (text == null) {
			return;
		}
		try {
			new URI(text);
		}
		catch (final URISyntaxException e) {
			String message= getMessageBuilder().bind(Messages.Descr_FieldValueInvalid_Url_message,
					fieldDef.getName() );
			int index= e.getIndex();
			final int c= (index >= 0) ? text.charAt(index) : -1;
			if ((c == ' ' || c == '\n')
					&& text.startsWith("http", index + 1) ) { //$NON-NLS-1$
				index= -1;
			}
			else {
				String reason= e.getReason();
				if (reason.length() > 0) {
					final var sb= getMessageUtil().getStringBuilder();
					final int cp0= reason.codePointAt(0);
					sb.appendCodePoint(Character.toLowerCase(cp0));
					sb.append(reason, Character.charCount(cp0), reason.length());
					reason= sb.toString();
				}
				message+= ' ' + getMessageBuilder().bind(Messages.Descr_FieldValueInvalid_Url_Detail_message,
						getMessageUtil().checkFullQuoteText(text), reason );
			}
			if (index >= 0) {
				addProblem(element, problemSeverity, TYPE12_FIELD_VALUE_INVALID,
						message,
						getValueTextStartOffset(fieldNode, index), getValueTextEndOffset(fieldNode, index + 1) );
			}
			else {
				addValueProblem(element, problemSeverity, TYPE12_FIELD_VALUE_INVALID,
						message, fieldNode );
			}
		}
	}
	
	private int getValueTextStartOffset(final Field fieldNode, int offset) {
		final DslAstNode valueNode= fieldNode.getValue();
		if (valueNode instanceof Value) {
			final var textRegions= ((Value)valueNode).getTextRegions();
			for (final var textRegion : textRegions) {
				if (offset < textRegion.getLength()) {
					return textRegion.getStartOffset() + offset;
				}
				if (offset == textRegion.getLength()) {
					final var sourceContent= getSourceContent();
					final int sourceOffset= textRegion.getStartOffset() + offset;
					if (sourceOffset < sourceContent.getLength()
							&& sourceContent.getChar(sourceOffset) == '\n') {
						return sourceOffset - 1;
					}
					return sourceOffset;
				}
				offset-= 1 + textRegion.getLength();
			}
		}
		return valueNode.getStartOffset();
	}
	
	private int getValueTextEndOffset(final Field fieldNode, int offset) {
		final DslAstNode valueNode= fieldNode.getValue();
		if (valueNode instanceof Value) {
			final var textRegions= ((Value)valueNode).getTextRegions();
			for (final var textRegion : textRegions) {
				if (offset <= textRegion.getLength()) {
					return textRegion.getStartOffset() + offset;
				}
				offset-= 1 + textRegion.getLength();
			}
		}
		return valueNode.getEndOffset();
	}
	
	private final void addProblem(final BasicRPkgDescrSourceElement element,
			final int problemSeverity, final int code, final String message,
			final int startOffset, final int endOffset) {
		element.setModelStatusCode(code);
		
		addProblem(problemSeverity, code, message, startOffset, endOffset);
	}
	
	private void addValueProblem(final BasicRPkgDescrSourceElement element,
			final int problemSeverity, final int code, final String message,
			final Field fieldNode) {
		element.setModelStatusCode(code);
		
		final DslAstNode valueNode= fieldNode.getValue();
		if (valueNode instanceof Value) {
			final var textRegions= ((Value)valueNode).getTextRegions();
			if (!textRegions.isEmpty()) {
				addProblem(problemSeverity, code, message,
						textRegions.getFirst().getStartOffset(),
						textRegions.getLast().getEndOffset() );
				return;
			}
		}
		addProblem(problemSeverity, code, message,
				valueNode.getStartOffset(), valueNode.getEndOffset() );
	}
	
}
