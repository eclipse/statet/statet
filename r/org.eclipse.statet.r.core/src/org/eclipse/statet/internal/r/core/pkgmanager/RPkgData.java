/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import java.util.List;

import org.eclipse.statet.r.core.pkgmanager.IRPkgData;
import org.eclipse.statet.rj.renv.core.BasicRPkg;
import org.eclipse.statet.rj.renv.core.RNumVersion;
import org.eclipse.statet.rj.renv.core.RPkg;


public class RPkgData extends BasicRPkg implements IRPkgData {
	
	
	private String license;
	
	private List<? extends RPkg> dependsList;
	private List<? extends RPkg> importsList;
	private List<? extends RPkg> linkingToList;
	private List<? extends RPkg> suggestsList;
	private List<? extends RPkg> enhancesList;
	
	private final String repoId;
	
	private String priority;
	
	
	public RPkgData(final String name, final RNumVersion version,
			final String repoId) {
		super(name, version);
		this.license= ""; //$NON-NLS-1$
		this.repoId= (repoId != null) ? repoId.intern() : ""; //$NON-NLS-1$
		this.priority= "other"; //$NON-NLS-1$
	}
	
	
	protected void setLicense(final String s) {
		this.license= (s != null && !s.isEmpty()) ? s : ""; //$NON-NLS-1$
	}
	
	@Override
	public String getLicense() {
		return this.license;
	}
	
	protected void setPriority(final String s) {
		this.priority= (s != null && !s.isEmpty()) ? s.intern() : "other"; //$NON-NLS-1$
	}
	
	@Override
	public String getPriority() {
		return this.priority;
	}
	
	protected void setDepends(final List<? extends RPkg> list) {
		this.dependsList= list;
	}
	
	@Override
	public List<? extends RPkg> getDepends() {
		return this.dependsList;
	}
	
	protected void setImports(final List<? extends RPkg> list) {
		this.importsList= list;
	}
	
	@Override
	public List<? extends RPkg> getImports() {
		return this.importsList;
	}
	
	protected void setLinkingTo(final List<? extends RPkg> list) {
		this.linkingToList= list;
	}
	
	@Override
	public List<? extends RPkg> getLinkingTo() {
		return this.linkingToList;
	}
	
	protected void setSuggests(final List<? extends RPkg> list) {
		this.suggestsList= list;
	}
	
	@Override
	public List<? extends RPkg> getSuggests() {
		return this.suggestsList;
	}
	
	protected void setEnhances(final List<? extends RPkg> list) {
		this.enhancesList= list;
	}
	
	@Override
	public List<? extends RPkg> getEnhances() {
		return this.enhancesList;
	}
	
	@Override
	public String getRepoId() {
		return this.repoId;
	}
	
}
