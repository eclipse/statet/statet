/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.buildpath.core.BasicBuildpathElement;
import org.eclipse.statet.ltk.buildpath.core.BuildpathAttribute;
import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;
import org.eclipse.statet.ltk.buildpath.core.BuildpathElementType;
import org.eclipse.statet.ltk.buildpath.core.BuildpathUtils;
import org.eclipse.statet.r.core.project.RBuildpaths;


@NonNullByDefault
public class RBuildpathPrefs {
	
	
	public static final String STAMP_KEY= "stamp"; //$NON-NLS-1$
	private static final String VERSION_KEY= "version"; //$NON-NLS-1$
	
	private static final String PATH_PREF_KEY= "path"; //$NON-NLS-1$
	
	private static final ImList<IPath> DEFAULT_INCLUSION_PATTERNS= ImCollections.newList();
	private static final ImList<IPath> DEFAULT_EXCLUSION_PATTERNS= ImCollections.<IPath>newList(
			RBuildpaths.PKG_RCHECK_FOLDER_PATH.addTrailingSeparator() );
	
	
	private final IEclipsePreferences rootNode;
	
	private final IProject project;
	
	
	public RBuildpathPrefs(final IScopeContext context, final String qualifier,
				final IProject project) {
		this.rootNode= context.getNode(qualifier);
		this.project= project;
	}
	
	
	public ImList<BuildpathElement> load() {
		final List<BuildpathElement> elements= new ArrayList<>();
		
		{	final String childName= BuildpathElement.SOURCE + ".0";
			final Preferences entryNode= this.rootNode.node(childName);
			final BuildpathElementType type= RBuildpaths.R_SOURCE_TYPE;
			
			final IPath path= this.project.getFullPath();
			ImList<IPath> inclusionPatterns= BuildpathUtils.decodePatterns(
					entryNode.get(BuildpathAttribute.FILTER_INCLUSIONS, null) );
			if (inclusionPatterns == null) {
				inclusionPatterns= DEFAULT_INCLUSION_PATTERNS;
			}
			ImList<IPath> exclusionPatterns= BuildpathUtils.decodePatterns(
					entryNode.get(BuildpathAttribute.FILTER_EXCLUSIONS, null) );
			if (exclusionPatterns == null) {
				exclusionPatterns= DEFAULT_EXCLUSION_PATTERNS;
			}
			
			elements.add(new BasicBuildpathElement(type, path,
					inclusionPatterns, exclusionPatterns,
					null, null, null, null, true, null ));
		}
		
		return ImCollections.toList(elements);
	}
	
	public void save(final List<BuildpathElement> elements, final boolean flush) {
		try {
			for (final BuildpathElement element : elements) {
				if (element.getType() == RBuildpaths.R_SOURCE_TYPE
						&& element.getPath().equals(this.project.getFullPath()) ) {
					final String childName= BuildpathElement.SOURCE + ".0";
					final Preferences entryNode= this.rootNode.node(childName);
					
					IPath path= element.getPath();
					if (path.isAbsolute() && this.project.getFullPath().isPrefixOf(path)) {
						path= path.removeFirstSegments(1).makeRelative();
					}
					else {
						continue;
					}
					entryNode.put(PATH_PREF_KEY, path.toPortableString());
					entryNode.put(BuildpathAttribute.FILTER_INCLUSIONS,
							BuildpathUtils.encodePatterns(element.getInclusionPatterns()) );
					entryNode.put(BuildpathAttribute.FILTER_EXCLUSIONS,
							BuildpathUtils.encodePatterns(element.getExclusionPatterns()) );
					
					this.rootNode.putInt(VERSION_KEY, 1);
					this.rootNode.putLong(STAMP_KEY, System.currentTimeMillis());
					
					if (flush) {
						this.rootNode.flush();
					}
				}
			}
		}
		catch (final BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
