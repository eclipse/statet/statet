/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.rlang.RFrame;
import org.eclipse.statet.r.core.model.rlang.RLangSrcElement;


@NonNullByDefault
public class RUnitElement implements RLangSrcElement<RLangSrcElement<?>> {
	
	
	public static RUnitElement read(final RSourceUnit su, final CompositeFrame envir, final InputStream input) throws IOException, ClassNotFoundException {
		try (final DataStream in= DataStream.get(input)) {
			if (in.readVersion() != 1) {
				throw new IOException("Unsupported data version, please rebuild the index (clean workspace).");
			}
			final var element= new RUnitElement(su, envir, in);
			if (input.available() != 0) {
				throw new IOException(String.format("Invalid data: next= %1$02X.", input.read()));
			}
			return element;
		}
	}
	
	
	private final SourceUnit sourceUnit;
	CompositeFrame envir;
	private final List<ExportedRElement> elements;
	
	
	public RUnitElement(final RSourceUnit su, final List<ExportedRElement> children) {
		this.sourceUnit= su;
		this.elements= Collections.unmodifiableList(children);
	}
	
	private RUnitElement(final RSourceUnit su, final CompositeFrame envir, final DataStream in) throws IOException {
		this.sourceUnit= su;
		this.envir= envir;
		
		{	final var array= new @NonNull ExportedRElement[in.readInt()];
			for (int i= 0; i < array.length; i++) {
				final byte o= in.readByte();
				array[i]= switch (o & 0xF) {
						case 0 -> new ExportedRElement(this, in, o);
						case ExportedRClass.SER_ELEMENT_TYPE -> new ExportedRClass(this, in, o);
						case ExportedRMethod.SER_ELEMENT_TYPE -> new ExportedRMethod(this, in, o);
						default ->
							throw new IOException(String.format("Invalid data: o= %1$02X.", o));
						};
			}
			this.elements= ImCollections.newList(array);
		}
	}
	
	public void save(final OutputStream output) throws IOException {
		try (final DataStream out= DataStream.get(output)) {
			out.writeVersion(1);
			
			{	final int l= this.elements.size();
				out.writeInt(l);
				for (int i= 0; i < l; i++) {
					this.elements.get(i).writeTo(out, (byte)0);
				}
			}
		}
	}
	
	
	@Override
	public String getModelTypeId() {
		return RModel.R_TYPE_ID;
	}
	
	@Override
	public int getElementType() {
		return LtkModelElement.C12_SOURCE_FILE;
	}
	
	@Override
	public RElementName getElementName() {
		final ElementName elementName= this.sourceUnit.getElementName();
		if (elementName instanceof RElementName) {
			return (RElementName)elementName;
		}
		return RElementName.create(RElementName.RESOURCE, elementName.getSegmentName());
	}
	
	@Override
	public String getId() {
		return this.sourceUnit.getId();
	}
	
	@Override
	public boolean exists() {
		return true;
	}
	
	@Override
	public boolean isReadOnly() {
		return this.sourceUnit.isReadOnly();
	}
	
	@Override
	public SourceUnit getSourceUnit() {
		return this.sourceUnit;
	}
	
	@Override
	public @Nullable RLangSrcElement<?> getModelParent() {
		return null;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangSrcElement<?>> filter) {
		return LtkModelUtils.<RLangSrcElement<?>>hasChildren(this.elements, filter);
	}
	
	@Override
	public List<? extends RLangSrcElement<?>> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangSrcElement<?>> filter) {
		return LtkModelUtils.<RLangSrcElement<?>>getChildren(this.elements, filter);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == RFrame.class) {
			return (T)this.envir;
		}
		return null;
	}
	
	
	@Override
	public @Nullable TextRegion getSourceRange() {
		return null;
	}
	
	@Override
	public @Nullable TextRegion getNameSourceRange() {
		return null;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
}
