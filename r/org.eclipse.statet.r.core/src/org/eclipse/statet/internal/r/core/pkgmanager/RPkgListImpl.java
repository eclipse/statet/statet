/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.renv.core.RPkg;
import org.eclipse.statet.rj.renv.core.RPkgList;
import org.eclipse.statet.rj.renv.core.RPkgUtils;


@NonNullByDefault
public final class RPkgListImpl<T extends RPkg> extends ArrayList<T> implements RPkgList<T> {
	
	
	private static final long serialVersionUID= -3022375551268568786L;
	
	
	public RPkgListImpl(final int capacity) {
		super(capacity);
	}
	
	
	@Override
	public int indexOf(final @Nullable String name) {
		if (name == null) {
			return -1;
		}
		int low= 0;
		int high= super.size() - 1;
		while (low <= high) {
			final int mid= (low + high) >>> 1;
			final int diff= RPkgUtils.NAMES_COLLATOR.compare(super.get(mid).getName(), name);
			if (diff < 0) {
				low= mid + 1;
			}
			else if (diff > 0) {
				high= mid - 1;
			}
			else {
				return mid;
			}
		}
		return -(low + 1);
	}
	
	public int indexOf(final String name, int low) {
		int high= super.size() - 1;
		while (low <= high) {
			final int mid= (low + high) >>> 1;
			final int diff= RPkgUtils.NAMES_COLLATOR.compare(super.get(mid).getName(), name);
			if (diff < 0) {
				low= mid + 1;
			}
			else if (diff > 0) {
				high= mid - 1;
			}
			else {
				return mid;
			}
		}
		return -(low + 1);
	}
	
	@Override
	public int indexOf(final @Nullable Object o) {
		if (o instanceof RPkg) {
			return indexOf(((RPkg) o).getName());
		}
		return -1;
	}
	
	@Override
	public boolean contains(final @Nullable String name) {
		return (indexOf(name) >= 0);
	}
	
	@Override
	public @Nullable T get(final @Nullable String name) {
		final int idx= indexOf(name);
		return (idx >= 0) ? super.get(idx) : null;
	}
	
	@Override
	public boolean add(final T pkg) {
		final int idx= indexOf(pkg.getName());
		if (idx < 0) {
			super.add(-idx - 1, pkg);
			return true;
		}
		return false;
	}
	
	@Override
	public void add(final int index, final T element) {
		super.add(index, nonNullAssert(element));
	}
	
	public void set(final T pkg) {
		final int idx= indexOf(pkg.getName());
		if (idx < 0) {
			super.add(-idx - 1, pkg);
		}
		else {
			set(idx, pkg);
		}
	}
	
	public void remove(final String name) {
		final int idx= indexOf(name);
		if (idx >= 0) {
			remove(idx);
		}
	}
	
}
