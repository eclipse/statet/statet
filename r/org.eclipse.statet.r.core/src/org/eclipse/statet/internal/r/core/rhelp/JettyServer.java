/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rhelp;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;

import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.http.jetty.JettyConfigurator;
import org.eclipse.equinox.http.jetty.JettyConstants;

import org.eclipse.statet.jcommons.net.CommonsNet;

import org.eclipse.statet.internal.r.core.RCorePlugin;


public class JettyServer {
	
	
	private static final String WEBAPP_NAME= "rhelp"; //$NON-NLS-1$
	private static final String OTHER_INFO= "org.eclipse.statet.r.rhelp"; //$NON-NLS-1$
	
	
	private final String host;
	private int port;
	
	
	public JettyServer() {
		this.host= loadHost();
		this.port= -1;
	}
	
	
	protected String loadHost() {
		String serverProperty= RCorePlugin.getInstance().getBundle().getBundleContext().getProperty("server_host"); //$NON-NLS-1$
		if (serverProperty != null && (serverProperty= serverProperty.trim()).length() > 0) {
			return serverProperty;
		}
		return CommonsNet.LOCAL_LOOPBACK_STRING;
	}
	
	public void startServer() throws Exception {
		final Bundle bundle= Platform.getBundle("org.eclipse.equinox.http.registry"); //$NON-NLS-1$
		if (bundle == null) {
			throw new IllegalStateException("bundle 'org.eclipse.equinox.http.registry' is missing."); //$NON-NLS-1$
		}
		
		final Dictionary<String, Object> dict= new Hashtable<>();
		dict.put(JettyConstants.HTTP_HOST, this.host);
		dict.put(JettyConstants.HTTP_PORT, Integer.valueOf((this.port == -1) ? 0 : this.port)); 
		
		// set the base URL
		dict.put(JettyConstants.CONTEXT_PATH, "/rhelp"); //$NON-NLS-1$
		dict.put(JettyConstants.OTHER_INFO, OTHER_INFO);
		
		dict.put(JettyConstants.CONTEXT_SESSIONINACTIVEINTERVAL, Integer.valueOf(30 * 60)); // 30 minutes
		
		JettyConfigurator.startServer(WEBAPP_NAME, dict);
		
		if (bundle.getState() == Bundle.RESOLVED) {
			bundle.start(Bundle.START_TRANSIENT);
		}
		if (this.port == -1) {
			// Jetty selected a port number for us
			final ServiceReference[] reference= bundle.getBundleContext().getServiceReferences(
					"org.osgi.service.http.HttpService", "(" + JettyConstants.OTHER_INFO + "=" + OTHER_INFO + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			final Object assignedPort= reference[0].getProperty(JettyConstants.HTTP_PORT);
			this.port= Integer.parseInt((String)assignedPort);
		}
	}
	
	public void stopServer() throws Exception {
		JettyConfigurator.stopServer(WEBAPP_NAME);
		this.port= -1;
	}
	
	public String getHost() {
		return this.host;
	}
	
	public int getPort() {
		return this.port;
	}
	
}
