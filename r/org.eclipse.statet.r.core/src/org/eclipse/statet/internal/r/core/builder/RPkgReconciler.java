/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.r.core.model.rpkg.RPkgDescrReconciler;
import org.eclipse.statet.internal.r.core.rmodel.RModelManagerImpl;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.StatusCodes;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.SourceUnitManager;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RPkgDescriptions;
import org.eclipse.statet.r.core.model.build.RPkgDescrSourceUnitModelContainer;
import org.eclipse.statet.r.core.project.RBuildpaths;
import org.eclipse.statet.r.core.project.RIssues;


@NonNullByDefault
public class RPkgReconciler {
	
	
	private final SourceUnitManager sourceUnitManager;
	
	private final RModelManagerImpl manager;
	
	private @Nullable RPkgDescrReconciler rPkgDescrReconciler;
	
	
	public RPkgReconciler(final RModelManagerImpl manager) {
		this.sourceUnitManager= LtkModels.getSourceUnitManager();
		this.manager= manager;
	}
	
	
	protected synchronized RPkgDescrReconciler getRPkgDescrReconciler() {
		var reconciler= this.rPkgDescrReconciler;
		if (reconciler == null) {
			reconciler= new RPkgDescrReconciler(this.manager);
			this.rPkgDescrReconciler= reconciler;
		}
		return reconciler;
	}
	
	
	public RPkgData parsePkgData(final IContainer pkgRoot,
			final MultiStatus status, final SubMonitor m) {
		final RPkgData result= new RPkgData();
		
		try {
			final IProject project= pkgRoot.getProject();
			
			if (!pkgRoot.exists()) {
				project.createMarker(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, Map.of(
						IMarker.SEVERITY, IMarker.SEVERITY_ERROR,
						IMarker.MESSAGE, NLS.bind("R package folder ''{0}'' is missing.",
								pkgRoot.getProjectRelativePath() ),
						IMarker.LOCATION, "R Project Configuration" ));
				
				return result;
			}
			
			final IFile file= pkgRoot.getFile(RBuildpaths.PKG_DESCRIPTION_FILE_PATH);
			if (file.exists()) {
				final var sourceUnit= this.sourceUnitManager.getSourceUnit(
						RModel.RPKG_DESCRIPTION_TYPE_ID, Ltk.PERSISTENCE_CONTEXT, file, m );
				try {
					final RPkgDescrSourceUnitModelContainer adapter= sourceUnit.getAdapter(RPkgDescrSourceUnitModelContainer.class);
					if (adapter != null) {
						final var issueSupport= adapter.getIssueSupport();
						if (issueSupport != null) {
							issueSupport.clearIssues(adapter.getSourceUnit());
						}
						
						final var model= getRPkgDescrReconciler().reconcile(adapter,
								ModelManager.MODEL_FILE | ModelManager.RECONCILE,
								m );
						
						if (model != null) {
							final var field= model.getField(RPkgDescriptions.Package_FIELD);
							if (field != null
									&& !StatusCodes.isError(field.getModelStatusCode()) ) {
								result.setPkgName(nonNullAssert(field.getValueText()));
							}
						}
					}
				}
				finally {
					sourceUnit.disconnect(m);
				}
				
				if (result.getPkgName() == null) {
					pkgRoot.createMarker(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, Map.of(
							IMarker.SEVERITY, IMarker.SEVERITY_ERROR,
							IMarker.MESSAGE, "'DESCRIPTION' file for R package is incomplete." ));
				}
			}
			else {
				pkgRoot.createMarker(RIssues.BUILDPATH_PROBLEM_MARKER_TYPE, Map.of(
						IMarker.SEVERITY, IMarker.SEVERITY_ERROR,
						IMarker.MESSAGE, "'DESCRIPTION' file for R package is missing." ));
				
				return result;
			}
		}
		catch (final CoreException | StatusException e) {
			status.add(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					NLS.bind("An error occurred when parsing R package ''DESCRIPTION'' file ''{0}''.",
							pkgRoot.getFullPath().toString() ),
					e ));
		}
		
		return result;
	}
	
	
}
