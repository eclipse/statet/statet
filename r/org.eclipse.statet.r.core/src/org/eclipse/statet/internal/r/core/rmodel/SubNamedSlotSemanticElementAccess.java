/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
final class SubNamedSlotSemanticElementAccess extends SubAbstractElementAccess {
	
	
	private final RAstNode nameNode;
	
	
	SubNamedSlotSemanticElementAccess(final ElementAccess root, final RAstNode nameNode) {
		super(root);
		this.nameNode= nameNode;
	}
	
	
	@Override
	public final int getType() {
		return RElementName.SUB_NAMEDSLOT;
	}
	
	@Override
	public final @Nullable String getSegmentName() {
		return this.nameNode.getText();
	}
	
	@Override
	public final RAstNode getNode() {
		return getRoot().getNode(); // ?
	}
	
	@Override
	public final RAstNode getNameNode() {
		return this.nameNode;
	}
	
}
