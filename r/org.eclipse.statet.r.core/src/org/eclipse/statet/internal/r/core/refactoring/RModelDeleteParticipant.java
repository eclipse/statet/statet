/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.refactoring;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.DeleteParticipant;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.internal.r.core.rmodel.RModelIndex;
import org.eclipse.statet.r.core.project.RProjects;


public class RModelDeleteParticipant extends DeleteParticipant {
	
	
	private class DeleteProjectChange extends Change {
		
		
		private final IProject project;
		
		
		public DeleteProjectChange(final IProject project) {
			this.project= project;
		}
		
		
		@Override
		public String getName() {
			return NLS.bind(Messages.RModel_DeleteProject_name, this.project.getName());
		}
		
		@Override
		public Object getModifiedElement() {
			return this.project;
		}
		
		@Override
		public void initializeValidationData(final IProgressMonitor monitor) {
		}
		
		@Override
		public RefactoringStatus isValid(final IProgressMonitor monitor) throws CoreException,
				OperationCanceledException {
			final RefactoringStatus status= new RefactoringStatus();
			return status;
		}
		
		@Override
		public Change perform(final IProgressMonitor monitor) throws CoreException {
			final RModelIndex index= RCorePlugin.getInstance().getRModelManager().getIndex();
			if (index != null) {
				index.updateProjectConfigRemoved(this.project);
			}
			
			return null;
		}
		
		
		@Override
		public String toString() {
			return getName();
		}
		
	}
	
	
	private IProject project;
	
	
	public RModelDeleteParticipant() {
	}
	
	
	@Override
	public String getName() {
		return Messages.RModel_DeleteParticipant_name;
	}
	
	@Override
	protected boolean initialize(final Object element) {
		if (element instanceof IProject) {
			try {
				if (((IProject) element).hasNature(RProjects.R_NATURE_ID)) {
					this.project= (IProject) element;
					return true;
				}
			}
			catch (final CoreException e) {}
		}
		return false;
	}
	
	@Override
	public RefactoringStatus checkConditions(final IProgressMonitor monitor,
			final CheckConditionsContext context)
			throws OperationCanceledException {
		final RefactoringStatus status= new RefactoringStatus();
		return status;
	}
	
	@Override
	public Change createChange(final IProgressMonitor monitor) throws CoreException,
			OperationCanceledException {
		if (this.project != null) {
			return new DeleteProjectChange(this.project);
		}
		return null;
	}
	
}
