/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.r.core.pkgmanager.RRepo;
import org.eclipse.statet.r.core.pkgmanager.RView;
import org.eclipse.statet.rj.data.RDataFrame;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
class RViewTasks {
	
	
	static @Nullable ImList<RView> loadRViews(final ImList<RRepo> repos,
			final RService r, final ProgressMonitor m) {
		m.beginSubTask("Loading R task views...");
		try {
			if (repos.isEmpty()) {
				return ImCollections.emptyList();
			}
			final RList rViews;
			{	final FunctionCall call= r.createFunctionCall("ctv::available.views"); //$NON-NLS-1$
				call.add("repos", DefaultRObjectFactory.INSTANCE.createCharVector(
						repos.map(RRepo::getURL).toList() ));
				rViews= RDataUtils.checkRList(call.evalData(m), "ctvlist"); //$NON-NLS-1$
			}
			final int nViews= RDataUtils.checkIntLength(rViews);
			final List<RViewImpl> views= new ArrayList<>(nViews);
			for (int iView= 0; iView < nViews; iView++) {
				final RList rView= RDataUtils.checkRList(rViews.get(iView), "ctv"); //$NON-NLS-1$
				final String name= RDataUtils.checkSingleCharValue(rView.get("name")); //$NON-NLS-1$
				final String topic= RDataUtils.checkSingleCharValue(rView.get("topic")); //$NON-NLS-1$
				final RDataFrame rPkgList= RDataUtils.checkRDataFrame(rView.get("packagelist"));
				final var rPkgsName= RDataUtils.checkRCharColumn(rPkgList, "name"); //$NON-NLS-1$
				final var rPkgsCore= RDataUtils.checkRLogiColumn(rPkgList, "core"); //$NON-NLS-1$
				final var viewPkgs= new RView.Pkg[RDataUtils.checkIntLength(rPkgList)];
				for (int iPkg= 0; iPkg < viewPkgs.length; iPkg++) {
					viewPkgs[iPkg]= new RView.Pkg(nonNullAssert(rPkgsName.get(iPkg)),
							rPkgsCore.getLogi(iPkg) );
				}
				views.add(new RViewImpl(name, topic, ImCollections.newList(viewPkgs)));
			}
			return ImCollections.toList(views);
		}
		catch (final StatusException | UnexpectedRDataException e) {
			RCorePlugin.logError("An error occurred when loading R task views.", e);
			return null;
		}
	}
	
	
}
