/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.nostart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.content.IContentDescription;

import org.eclipse.statet.jcommons.io.ByteOrderMark;
import org.eclipse.statet.jcommons.io.IOUtils;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.util.StringUtils;

import org.eclipse.statet.ecommons.io.TextContentDescriber;


/**
 * Detects encoding of R package DESCRIPTION based on optional encoding field.
 */
@NonNullByDefault
public class RPkgDescriptionContentDescriber extends TextContentDescriber {
	
	
	private static final @NonNull QualifiedName[] SUPPORTED_OPTIONS= new @NonNull QualifiedName[] {
			IContentDescription.CHARSET,
			IContentDescription.BYTE_ORDER_MARK,
	};
	
	
	private static final String PACKAGE_FIELD= "Package:"; //$NON-NLS-1$
	private static final String ENCODING_FIELD= "Encoding:"; //$NON-NLS-1$
	
	private static class ParseResult {
		
		
		private boolean hasPackageDecl;
		private @Nullable String charset;
		
		
		public ParseResult() {
		}
		
		
		public void setEncoding(final String encoding) {
			if (this.charset == null) {
				this.charset= encoding;
			}
		}
		
		public void setPackage() {
			this.hasPackageDecl= true;
		}
		
		
		public boolean isIndeterminate() {
			return (!this.hasPackageDecl);
		}
		
		public boolean hasCharsetSpec() {
			return (this.charset != null);
		}
		
		public @Nullable String getCharsetName() {
			return this.charset;
		}
		
	}
	
	
	public RPkgDescriptionContentDescriber() {
	}
	
	
	@Override
	public @NonNull QualifiedName[] getSupportedOptions() {
		return SUPPORTED_OPTIONS;
	}
	
	@Override
	public int describe(final Reader contents, final @Nullable IContentDescription description) throws IOException {
		if (description != null && description.isRequested(IContentDescription.CHARSET)) {
			final BufferedReader reader= new BufferedReader(contents);
			final ParseResult result= parse(reader);
			if (result.isIndeterminate()) {
				return INDETERMINATE;
			}
			if (result.hasCharsetSpec()) {
				description.setProperty(IContentDescription.CHARSET, result.getCharsetName());
			}
		}
		return VALID;
	}
	
	@Override
	public int describe(final InputStream contents, final @Nullable IContentDescription description) throws IOException {
		final ByteOrderMark bom= checkByteOrderMark(contents, description);
		
		{	contents.reset();
			final var reader= new BufferedReader(
					IOUtils.newStreamContentReader(contents, bom, StandardCharsets.ISO_8859_1) );
			final ParseResult result= parse(reader);
			if (result.hasCharsetSpec() && description != null) {
				description.setProperty(IContentDescription.CHARSET, result.getCharsetName());
			}
			if (result.isIndeterminate()) {
				return INDETERMINATE;
			}
		}
		
		return VALID;
	}
	
	private ParseResult parse(final BufferedReader reader) throws IOException {
		final ParseResult result= new ParseResult();
		String line;
		while ((line= reader.readLine()) != null) {
			if (line.startsWith(PACKAGE_FIELD)) {
				result.setPackage();
			}
			else if (line.startsWith(ENCODING_FIELD)) {
				result.setEncoding(StringUtils.trim(line, ENCODING_FIELD.length(), line.length()));
			}
		}
		return result;
	}
	
}
