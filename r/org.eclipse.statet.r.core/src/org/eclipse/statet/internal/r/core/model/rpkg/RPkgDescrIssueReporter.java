/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.dcf.core.source.ast.DcfAstProblemReporter;
import org.eclipse.statet.internal.r.core.rmodel.RAstProblemReporter;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.model.core.build.BasicIssueReporter;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceUnitModelInfo;
import org.eclipse.statet.r.core.source.ast.RAstNode;


@NonNullByDefault
public class RPkgDescrIssueReporter extends BasicIssueReporter<RSourceUnit, RPkgDescrSourceUnitModelInfo> {
	
	
	private final DcfAstProblemReporter syntaxProblemReporter= new DcfAstProblemReporter(RModel.RPKG_DESCRIPTION_TYPE_ID) {
		
		
		@Override
		protected void handleEmbeddedNode(final EmbeddingAstNode embeddingNode) {
			final AstNode node= embeddingNode.getForeignNode();
			if (node == null) {
				return;
			}
			if (embeddingNode.getForeignTypeId() == RModel.R_TYPE_ID) {
				RPkgDescrIssueReporter.this.rAstNodes.add((RAstNode)node);
			}
		}
		
	};
	
	private final List<RAstNode> rAstNodes= new ArrayList<>();
	private final RAstProblemReporter rSyntaxProblemReporter= new RAstProblemReporter(RModel.RPKG_DESCRIPTION_TYPE_ID);
	
	private final RPkgDescrModelInspector rPkgDescrProblemReporter= new RPkgDescrModelInspector();
	
	
	public RPkgDescrIssueReporter() {
		super(RModel.RPKG_DESCRIPTION_TYPE_ID);
	}
	
	
	@Override
	protected void clear() {
		this.rAstNodes.clear();
		super.clear();
	}
	
	
	@Override
	protected void runReporters(final RSourceUnit sourceUnit, final @Nullable AstNode node,
			final @Nullable RPkgDescrSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (node == null) {
			return;
		}
		if (node instanceof DslAstNode) {
			runReporters(sourceUnit, (DslAstNode)node, modelInfo,
					content, requestor, level );
		}
	}
	
	private void runReporters(final RSourceUnit sourceUnit, final DslAstNode node,
			final @Nullable RPkgDescrSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (shouldReportProblems()) {
			this.syntaxProblemReporter.run(node, content, requestor);
			this.rSyntaxProblemReporter.run(this.rAstNodes, content, requestor);
			
			if (modelInfo != null) {
				this.rPkgDescrProblemReporter.run(modelInfo, content, requestor);
			}
		}
	}
	
	
}
