/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.rlang.RFrame;
import org.eclipse.statet.r.core.model.rlang.RLangSrcElement;
import org.eclipse.statet.r.core.model.rlang.RSrcFrame;


@NonNullByDefault
public class CompositeFrame implements RSrcFrame<RLangSrcElement<?>> {
	
	
	private final int frameType;
	
	private final RElementName elementName;
	
	public final Map<String, RUnitElement> modelElements;
	private final Lock lock;
	
	
	public CompositeFrame(final ReadWriteLock lock,
			final @Nullable String packageName, final String projectName,
			final @Nullable Map<String, RUnitElement> elements) {
		this.lock= lock.readLock();
		this.modelElements= (elements != null) ? elements : new HashMap<>();
		
		if (packageName != null) {
			this.frameType= PACKAGE;
			this.elementName= RElementName.create(RElementName.SCOPE_PACKAGE, packageName);
		}
		else {
			this.frameType= PROJECT;
			this.elementName= RElementName.create(RElementName.SCOPE_PROJECT, projectName);
		}
	}
	
	public CompositeFrame(final ReadWriteLock lock,
			final @Nullable String packageName, final String projectName,
			final CompositeFrame copyFrom) {
		this(lock, packageName, projectName, copyFrom.modelElements);
	}
	
	public CompositeFrame(final ReadWriteLock lock,
			final @Nullable String packageName, final String projectName) {
		this(lock, packageName, projectName, (Map<String, RUnitElement>)null);
	}
	
	
	@Override
	public int getFrameType() {
		return this.frameType;
	}
	
	@Override
	public @Nullable String getFrameId() {
		return null;
	}
	
	@Override
	public RElementName getElementName() {
		return this.elementName;
	}
	
	@Override
	public List<? extends RLangSrcElement<?>> getModelElements() {
		this.lock.lock();
		try {
			final Collection<RUnitElement> values= this.modelElements.values();
			final List<RLangSrcElement<?>> list= new ArrayList<>(values.size());
			list.addAll(values);
			return list;
		}
		finally {
			this.lock.unlock();
		}
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super RLangSrcElement<?>> filter) {
		this.lock.lock();
		try {
			if (this.modelElements.isEmpty()) {
				return false;
			}
			for (final RUnitElement element : this.modelElements.values()) {
				if (element.hasModelChildren(filter)) {
					return true;
				}
			}
			return false;
		}
		finally {
			this.lock.unlock();
		}
	}
	
	@Override
	public List<? extends RLangSrcElement<?>> getModelChildren(final @Nullable LtkModelElementFilter<? super RLangSrcElement<?>> filter) {
		this.lock.lock();
		try {
			if (this.modelElements.isEmpty()) {
				return ImCollections.emptyList();
			}
			final var children= new ArrayList<RLangSrcElement<?>>();
			for (final var element : this.modelElements.values()) {
				final List<? extends RLangSrcElement<?>> elementChildren= element.getModelChildren(null);
				if (!elementChildren.isEmpty()) {
					children.ensureCapacity(children.size() + elementChildren.size());
					for (final var child : elementChildren) {
						if (filter == null || filter.include(child)) {
							children.add(child);
						}
					}
				}
			}
			return children;
		}
		finally {
			this.lock.unlock();
		}
	}
	
	@Override
	public List<? extends RFrame<?>> getPotentialParents() {
		return ImCollections.emptyList();
	}
	
	
	public @Nullable RUnitElement setModelElement(final String suId, final RUnitElement element) {
		element.envir= this;
		return this.modelElements.put(suId, element);
	}
	
	public @Nullable RUnitElement removeModelElement(final String suId) {
		return this.modelElements.remove(suId);
	}
	
	public void removeModelElements(final String modelTypeId) {
		for (final Iterator<RUnitElement> iter= this.modelElements.values().iterator(); iter.hasNext(); ) {
			final RUnitElement unitElement= iter.next();
			if (unitElement.getModelTypeId() == modelTypeId) {
				iter.remove();
			}
		}
	}
	
	
	@Override
	public String toString() {
		final var sb= new ToStringBuilder("CompositeFrame", getClass() ); //$NON-NLS-1$
		sb.addProp("frameType", "0x%02X", this.frameType); //$NON-NLS-1$ //$NON-NLS-2$
		sb.addProp("elementName", this.elementName); //$NON-NLS-1$
		return sb.build();
	}
	
}
