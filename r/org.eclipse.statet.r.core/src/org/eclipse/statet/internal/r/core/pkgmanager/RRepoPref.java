/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import org.eclipse.statet.ecommons.preferences.core.Preference;

import org.eclipse.statet.r.core.pkgmanager.RRepo;


public class RRepoPref extends Preference<RRepo> {
	
	
	public RRepoPref(final String qualifier, final String key) {
		super(qualifier, key);
	}
	
	
	@Override
	public Class<RRepo> getUsageType() {
		return RRepo.class;
	}
	
	@Override
	public RRepo store2Usage(final String storeValue) {
		final String s= storeValue;
		if (s != null && !s.isEmpty()) {
			final String[] parts= IS1_SEPARATOR_PATTERN.split(s); 
			if (parts.length >= 3) {
				return RVarRepo.create(parts[0].intern(), parts[1], parts[2],
						(parts.length >= 4) ? Util.getPkgType(parts[3]) : null );
			}
		}
		return null;
	}
	
	@Override
	public String usage2Store(final RRepo repo) {
		final StringBuilder sb= new StringBuilder(32);
		sb.append(repo.getId());
		sb.append(IS1_SEPARATOR_CHAR);
		sb.append(repo.getName());
		sb.append(IS1_SEPARATOR_CHAR);
		sb.append((repo instanceof RVarRepo) ? ((RVarRepo) repo).getRawURL() : repo.getURL());
		sb.append(IS1_SEPARATOR_CHAR);
		if (repo.getPkgType() != null) {
			sb.append(repo.getPkgType());
		}
		return sb.toString();
	}
	
}
