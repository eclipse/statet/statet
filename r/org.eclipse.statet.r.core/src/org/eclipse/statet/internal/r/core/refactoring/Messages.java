/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.refactoring;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String SearchScope_Workspace_label;
	public static String SearchScope_CurrentAndReferencingProjects_label;
	public static String SearchScope_CurrentProject_label;
	public static String SearchScope_CurrentFile_label;
	public static String SearchScope_LocalFrame_label;
	
	public static String SearchProcessor_label;
	
	public static String RenameInWorkspace_label;
	public static String RenameInWorkspace_Descriptor_description;
	public static String RenameInWorkspace_error_InvalidSelection_message;
	public static String RenameInWorkspace_warning_NoDefinition_message;
	public static String RenameInWorkspace_warning_MultipleDefinitions_message;
	public static String RenameInWorkspace_Changes_ReplaceOccurrence_name;
	
	public static String RenameInRegion_label;
	public static String RenameInRegion_Descriptor_description;
	public static String RenameInRegion_Changes_VariableGroup_name;
	public static String RenameInRegion_Changes_ReplaceOccurrence_name;
	public static String RenameInRegion_Changes_ReplaceOccurrenceOf_name;
	
	public static String InlineTemp_label;
	public static String InlineTemp_Descriptor_description;
	public static String InlineTemp_error_InvalidSelection_message;
	public static String InlineTemp_error_InvalidSelectionNotLocal_message;
	public static String InlineTemp_error_InvalidSelectionParameter_message;
	public static String InlineTemp_error_InvalidSelectionNoArrow_message;
	public static String InlineTemp_error_MissingDefinition_message;
	public static String InlineTemp_warning_ValueSyntaxError_message;
	public static String InlineTemp_Changes_DeleteAssignment_name;
	public static String InlineTemp_Changes_ReplaceAssignment_name;
	public static String InlineTemp_Changes_ReplaceReference_name;
	
	public static String ExtractTemp_label;
	public static String ExtractTemp_Descriptor_description;
	public static String ExtractTemp_error_InvalidSelection_message;
	public static String ExtractTemp_error_InvalidSelectionType_message;
	public static String ExtractTemp_error_InvalidSelectionFHeader_message;
	public static String ExtractTemp_warning_OccurrencesSyntaxError_message;
	public static String ExtractTemp_warning_ChangedRange_message;
	public static String ExtractTemp_Changes_AddVariable_name;
	public static String ExtractTemp_Changes_ReplaceOccurrence_name;
	
	public static String ExtractFunction_label;
	public static String ExtractFunction_Descriptor_description;
	public static String ExtractFunction_error_InvalidSelection_message;
	public static String ExtractFunction_warning_SelectionSyntaxError_message;
	public static String ExtractFunction_warning_ChangedRange_message;
	public static String ExtractFunction_Changes_AddFunctionDef_name;
	public static String ExtractFunction_Changes_DeleteOld_name;
	public static String ExtractFunction_Changes_ReplaceOldWithFunctionDef_name;
	public static String ExtractFunction_Changes_AddFunctionCall_name;
	
	public static String FunctionToS4Method_label;
	public static String FunctionToS4Method_Descriptor_description;
	public static String FunctionToS4Method_error_InvalidSelection_message;
	public static String FunctionToS4Method_error_SelectionAlreadyS4_message;
	public static String FunctionToS4Method_warning_SelectionSyntaxError_message;
	public static String FunctionToS4Method_Changes_DeleteOld_name;
	public static String FunctionToS4Method_Changes_AddGenericDef_name;
	public static String FunctionToS4Method_Changes_AddMethodDef_name;
	
	public static String FCallToPipeForwardMethod_label;
	public static String FCallToPipeForwardMethod_Descriptor_description;
	public static String FCallToPipeForwardMethod_error_MissingArg_message;
	public static String FCallToPipeForwardMethod_error_MissingArgValue_message;
	public static String FCallToPipeForwardMethod_Changes_AddPipe_name;
	public static String FCallToPipeForwardMethod_Changes_DeleteOld_name;
	
	public static String IfElseInvert_label;
	public static String IfElseInvert_Descriptor_description;
	public static String IfElseInvert_error_MissingElse_message;
	public static String IfElseInvert_Changes_InvertCond_name;
	public static String IfElseInvert_Changes_ExchangeThen_name;
	public static String IfElseInvert_Changes_ExchangeElse_name;
	public static String IfNotNullElseToSpecial_label;
	public static String IfNotNullElseToSpecial_Descriptor_description;
	public static String IfNotNullElseToCIfElse_label;
	public static String IfNotNullElseToCIfElse_Descriptor_description;
	public static String IfNotNullElse_Changes_name; 
	
	public static String RIdentifiers_error_Empty_message;
	public static String RIdentifiers_error_EmptyFor_message;
	public static String RIdentifiers_error_Invalid_message;
	public static String RIdentifiers_error_InvalidFor_message;
	
	public static String RModel_DeleteParticipant_name;
	public static String RModel_DeleteProject_name;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
