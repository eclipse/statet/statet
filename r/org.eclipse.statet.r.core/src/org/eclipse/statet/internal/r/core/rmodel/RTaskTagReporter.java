/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.impl.TaskTagReporter;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.source.ast.NodeType;
import org.eclipse.statet.r.core.source.ast.RAstNode;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public class RTaskTagReporter extends TaskTagReporter {
	
	
	public RTaskTagReporter() {
	}
	
	
	public void run(final RSourceUnit sourceUnit, final RAstNode node,
			final SourceContent sourceContent,
			final IssueRequestor requestor) {
		if (node instanceof SourceComponent) {
			setup(sourceContent, requestor);
			final var comments= nonNullAssert(((SourceComponent)node).getComments());
			for (final RAstNode comment : comments) {
				if (comment.getNodeType() == NodeType.DOCU_AGGREGATION) {
					final int childCount= comment.getChildCount();
					for (int childIndex= 0; childIndex < childCount; childIndex++) {
						final RAstNode commentLine= comment.getChild(childIndex);
						checkForTasks(commentLine.getStartOffset() + 2, commentLine.getEndOffset());
					}
				}
				else {
					checkForTasks(comment.getStartOffset() + 1, comment.getEndOffset());
				}
			}
		}
	}
	
}
