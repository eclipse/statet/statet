/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.r.core.project.RTaskMarkerHandler;


public class RdParser {
	
	private static final char[][] PLATFORM_KEYWORDS= { // without '#'
		"ifdef".toCharArray(), //$NON-NLS-1$
		"ifndef".toCharArray(), //$NON-NLS-1$
		"endif".toCharArray() }; //$NON-NLS-1$
	
	private enum Last { NONE, NEWLINE, BACKSLASH }
	
	
	private final SourceContent sourceContent;
	private final char[] content;
	
	private final RTaskMarkerHandler markers;
	
	private int currentOffset= 0;
	private final int currentLine= 1;
	private Last lastChar= Last.NONE;
	
	
	public RdParser(final SourceContent sourceContent, final RTaskMarkerHandler markers) {
		this.sourceContent= sourceContent;
		this.content= sourceContent.getString().toCharArray();
		this.markers= markers;
	}
	
	public void check() throws CoreException {
		READ: for (; this.currentOffset < this.content.length; this.currentOffset++) {
				
			if (checkNewLine()) {
				continue READ;
			}
			
			if (checkBackslash()) {
				continue READ;
			}
					
			final char current= this.content[this.currentOffset];
			switch (current) {
			case '%':
				readComment();
				continue READ;
			
			case '#':
				if (this.lastChar == Last.NEWLINE) {
					CHECK_KEYS: for (int i= 0; i < PLATFORM_KEYWORDS.length; i++) {
						int offset= this.currentOffset + 1;
						CHECK_KEYCHARS: for (int j= 0; j < PLATFORM_KEYWORDS[i].length; j++) {
							if (offset < this.content.length && PLATFORM_KEYWORDS[i][j] == this.content[offset++]) {
								continue CHECK_KEYCHARS;
							}
							continue CHECK_KEYS;
						}
						readPlatformInstruction(PLATFORM_KEYWORDS[i]);
					}
				}
				continue READ;
			}
		}
	}
	
	private void readPlatformInstruction(final char[] keyword) {
		final int start= this.currentOffset;
		int end= this.currentOffset;
		
		READ: for (this.currentOffset++; this.currentOffset < this.content.length; this.currentOffset++) {
			
			end= this.currentOffset;
			if (checkNewLine()) {
				break READ;
			}
		}
	}
	
	private void readComment() throws CoreException {
		final int start= this.currentOffset;
		int end= this.currentOffset;
		
		READ: for (this.currentOffset++; this.currentOffset < this.content.length; this.currentOffset++) {
			end= this.currentOffset;
			if (checkNewLine()) {
				end--;
				break READ;
			}
		}
		try {
			this.markers.checkForTasks(start, end);
		}
		catch (final BadLocationException e) {
		}
	}
	
	private boolean checkNewLine() {
		final char current= this.content[this.currentOffset];
		if (current == '\r' || current == '\n') {
			
			if (current == '\r' && this.currentOffset + 1 < this.content.length
					&& this.content[this.currentOffset + 1] == '\n' ) {
				this.currentOffset++;
			}
			
			this.lastChar= Last.NEWLINE;
			return true;
		}
		return false;
	}
	
	private boolean checkBackslash() {
		if (this.content[this.currentOffset] == '\\') {
			this.lastChar= Last.BACKSLASH;
			return true;
		}
		if (this.lastChar == Last.BACKSLASH) {
			this.lastChar= Last.NONE;
			return true;
		}
		return false;
	}
	
}
