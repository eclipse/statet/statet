/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core;

import org.eclipse.core.resources.IFile;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.r.core.rmodel.RResourceSourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.impl.AbstractFilePersistenceSourceUnitFactory;


/**
 * Factory for common R script files
 */
@NonNullByDefault
public class RSourceUnitFactory extends AbstractFilePersistenceSourceUnitFactory {
	
	
	public RSourceUnitFactory() {
	}
	
	
	@Override
	protected SourceUnit createSourceUnit(final String id, final IFile file) {
		return new RResourceSourceUnit(id, file);
	}
	
}
