/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.dsl.dcf.core.source.ast.DcfRecord;
import org.eclipse.statet.dsl.dcf.core.source.ast.EmbeddingValue;
import org.eclipse.statet.dsl.dcf.core.source.ast.Field;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.r.core.model.RPkgDescriptions;


@NonNullByDefault
public class RPkgDescrSourceAnalyzer {
	
	
	private static final Integer ZERO= Integer.valueOf(0);
	private static final BiFunction<@Nullable String, @Nullable Integer, Integer> COUNTER= (final @Nullable String key, final @Nullable Integer count) -> (count == null) ? ZERO : Integer.valueOf(count + 1);
	
	
	private SourceUnit sourceUnit= nonNullLateInit();
	private AstInfo ast= nonNullLateInit();
	
	private final List<RPkgDescrFieldSourceElement> recordBuilder= new ArrayList<>();
	private final Map<@Nullable String, Integer> fieldNames= new HashMap<>();
	
	private final List<EmbeddingReconcileTask> embeddedItems= new ArrayList<>();
	
	
	public RPkgDescrSourceAnalyzer() {
	}
	
	
	protected void clear() {
		this.embeddedItems.clear();
	}
	
	public RPkgDescrSourceUnitModelInfoImpl createModel(final SourceUnit sourceUnit, final AstInfo ast) {
		clear();
		try {
			this.sourceUnit= sourceUnit;
			this.ast= ast;
			
			final var rootNode= (SourceComponent)ast.getRoot();
			final var element= new RPkgDescrContainerSourceElement.SourceContainer(
					this.sourceUnit, ast.getStamp(),
					rootNode );
			processSourceLines(element, rootNode);
			
			final var modelInfo= new RPkgDescrSourceUnitModelInfoImpl(ast, element);
			return modelInfo;
		}
		finally {
			this.recordBuilder.clear();
			this.fieldNames.clear();
		}
	}
	
	public List<EmbeddingReconcileTask> getEmbeddedItems() {
		return this.embeddedItems;
	}
	
	
	private void processSourceLines(final RPkgDescrContainerSourceElement element,
			final SourceComponent sourceComponent) {
		for (int childIndex= 0; childIndex < sourceComponent.getChildCount(); childIndex++) {
			final var childNode= sourceComponent.getChild(childIndex);
			if (childNode instanceof DcfRecord) {
				createRecordElement(element, (DcfRecord)childNode);
//				childElement.occurrenceCount= recordCount++;
//				childElement.name= DslElementName.create(DslElementName.RECORD_NUM,
//						Integer.toString(recordCount + 1) );
			}
		}
		
		element.children= ImCollections.toList(this.recordBuilder);
		this.recordBuilder.clear();
		this.fieldNames.clear();
	}
	
	private void createRecordElement(final RPkgDescrContainerSourceElement parent,
			final DcfRecord node) {
		final var contentNodes= node.getContentNodes();
		for (int childIndex= 0; childIndex < contentNodes.size(); childIndex++) {
			final var childNode= contentNodes.get(childIndex);
			if (childNode instanceof Field) {
				createFieldElement(parent, (Field)childNode);
			}
		}
		return;
	}
	
	private void createFieldElement(final RPkgDescrContainerSourceElement parent,
			final Field node) {
		final String name= node.getKey().getText();
		final var element= new RPkgDescrFieldSourceElement(parent, node, name,
				(name != null) ? RPkgDescriptions.getFieldDefinition(name) : null );
		element.occurrenceCount= this.fieldNames.compute(name, COUNTER);
		this.recordBuilder.add(element);
		final DslAstNode value= node.getValue();
		if (value instanceof EmbeddingValue) {
			this.embeddedItems.add(new EmbeddingReconcileTask((EmbeddingValue)value));
		}
	}
	
}
