/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.builder;

import java.io.IOException;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.model.rlang.RClassSrcElement;
import org.eclipse.statet.r.core.model.rlang.RLangSrcElement;


@NonNullByDefault
public class ExportedRClass extends ExportedRElement
		implements RClassSrcElement<RLangSrcElement<?>> {
	
	
	private final List<String> superClassNames;
	
	
	public ExportedRClass(final RLangSrcElement<?> parent,
			final RClassSrcElement<?> sourceElement) {
		super(parent, sourceElement);
		this.superClassNames= sourceElement.getExtendedClassNames();
	}
	
	
	static final byte SER_ELEMENT_TYPE= 1;
	
	ExportedRClass(final RLangSrcElement<?> parent, final DataStream in, final byte o) throws IOException {
		super(parent, in, o);
		this.superClassNames= ImCollections.newList(in.readNonNullStringArray());
	}
	
	@Override
	void writeTo(final DataStream out, byte o) throws IOException {
		o= SER_ELEMENT_TYPE;
		super.writeTo(out, o);
		out.writeStringList(this.superClassNames);
	}
	
	
	@Override
	public List<String> getExtendedClassNames() {
		return this.superClassNames;
	}
	
}
