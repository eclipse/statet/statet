/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.pkgmanager;

import java.time.Instant;

import org.eclipse.statet.jcommons.lang.NonNull;

import org.eclipse.statet.r.core.pkgmanager.IRPkgChangeSet;
import org.eclipse.statet.r.core.pkgmanager.IRPkgManager.Event;
import org.eclipse.statet.r.core.pkgmanager.IRPkgSet;
import org.eclipse.statet.rj.renv.core.REnv;


final class Change implements Event {
	
	
	private final REnv rEnv;
	final long stamp;
	final @NonNull Instant timestamp;
	
	int repos;
	
	int pkgs;
	IRPkgSet oldPkgs;
	IRPkgSet newPkgs;
	RPkgChangeSet installedPkgs;
	
	int views;
	
	
	public Change(final REnv rEnv) {
		this.rEnv= rEnv;
		this.stamp= Util.stamp();
		this.timestamp= Instant.now();
	}
	
	
	@Override
	public REnv getREnv() {
		return this.rEnv;
	}
	
	@Override
	public int reposChanged() {
		return this.repos;
	}
	
	@Override
	public int pkgsChanged() {
		return this.pkgs;
	}
	
	@Override
	public IRPkgSet getOldPkgSet() {
		return this.oldPkgs;
	}
	
	@Override
	public IRPkgSet getNewPkgSet() {
		return this.newPkgs;
	}
	
	@Override
	public IRPkgChangeSet getInstalledPkgChangeSet() {
		return this.installedPkgs;
	}
	
	@Override
	public int viewsChanged() {
		return this.views;
	}
	
}
