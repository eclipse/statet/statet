/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.core.RProjectNature;
import org.eclipse.statet.internal.r.core.builder.ExportedRClass;
import org.eclipse.statet.internal.r.core.builder.ExportedRElement;
import org.eclipse.statet.internal.r.core.builder.ExportedRMethod;
import org.eclipse.statet.internal.r.core.builder.RPkgData;
import org.eclipse.statet.internal.r.core.builder.RUnitElement;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.model.rlang.RClassSrcElement;
import org.eclipse.statet.r.core.model.rlang.RLangSrcElement;
import org.eclipse.statet.r.core.model.rlang.RMethodSrcElement;
import org.eclipse.statet.r.core.model.rlang.RSrcStrFrame;
import org.eclipse.statet.r.core.project.RProject;


@NonNullByDefault
public class RModelIndexOrder {
	
	
	protected static class Result {
		
		public final String unitId;
		public final RUnitElement exportedElement;
		public final Set<String> defaultNames;
		
		public Result(final RUnitElement root, final Set<String> defaultNames) {
			this.unitId= root.getId();
			this.exportedElement= root;
			this.defaultNames= defaultNames;
		}
		
	}
	
	
	protected final RProjectNature rProject;
	protected final String projectName;
	
	protected @Nullable RPkgData pkgData;
	
	protected final List<Result> updated= new ArrayList<>();
	
	protected final List<String> removed= new ArrayList<>();
	
	protected final List<String> modelTypeIds;
	protected final boolean isFullBuild;
	
	
	public RModelIndexOrder(final RProject rProject,
			final List<String> modelTypeIds, final boolean isFullBuild) {
		this.rProject= (RProjectNature) rProject;
		this.projectName= rProject.getProject().getName();
		this.modelTypeIds= modelTypeIds;
		this.isFullBuild= isFullBuild;
	}
	
	
	protected @Nullable Result createResult(final RSourceUnit sourceUnit,
			final @Nullable RSourceUnitModelInfo model) {
		if (model == null) {
			return null;
		}
		
		final RSrcStrFrame topFrame= model.getTopFrame();
		final List<? extends RLangSrcElement> children= topFrame.getModelChildren(null);
		final var exports= new ArrayList<ExportedRElement>(children.size());
		final RUnitElement root= new RUnitElement(sourceUnit, exports);
		for (final var element : children) {
			final int type= element.getElementType();
			switch (type & RElement.MASK_C1) {
			case RElement.C1_METHOD:
				exports.add(new ExportedRMethod(root, (RMethodSrcElement<?>)element));
				break;
			case RElement.C1_CLASS:
				exports.add(new ExportedRClass(root, (RClassSrcElement<?>)element));
				break;
			case RElement.C1_VARIABLE:
				exports.add(new ExportedRElement(root, element));
				break;
			default:
				continue;
			}
		}
		final Set<String> names= new HashSet<>();
		names.addAll(topFrame.getAllAccessNames());
		final var frames= model.getReferencedFrames();
		for (final var frame : frames.values()) {
			names.addAll(frame.getAllAccessNames());
		}
		
		return new Result(root, names);
	}
	
	protected void addRemovedUnit(final String unitId) {
		if (!this.isFullBuild) {
			this.removed.add(unitId);
		}
	}
	
}
