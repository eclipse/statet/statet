/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.text.core.input.OffsetStringParserInput;
import org.eclipse.statet.jcommons.text.core.input.RegionParserInput;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.dsl.dcf.core.source.ast.DcfRParser;
import org.eclipse.statet.dsl.dcf.core.source.ast.EmbeddingValue;
import org.eclipse.statet.dsl.dcf.core.source.ast.Value;
import org.eclipse.statet.internal.r.core.RCorePlugin;
import org.eclipse.statet.internal.r.core.rmodel.RModelManagerImpl;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.model.build.RPkgDescrSourceUnitModelContainer;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceUnitModelInfo;
import org.eclipse.statet.r.core.project.RProject;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.ast.RParser;


@NonNullByDefault
public class RPkgDescrReconciler {
	
	
	protected static final class Data {
		
		public final RPkgDescrSourceUnitModelContainer adapter;
		
		public final RSourceUnit sourceUnit;
		
		public final SourceContent content;
		
		@Nullable AstInfo ast;
		
		@Nullable RPkgDescrSourceUnitModelInfo oldModel;
		@Nullable RPkgDescrSourceUnitModelInfo newModel;
		
		
		public Data(final RPkgDescrSourceUnitModelContainer adapter, final SubMonitor monitor) {
			this.adapter= adapter;
			this.sourceUnit= adapter.getSourceUnit();
			this.content= adapter.getParseContent(monitor);
		}
		
		
		boolean isOK() {
			return (this.content != null);
		}
		
		@SuppressWarnings("null")
		public AstInfo getAst() {
			return this.ast;
		}
		
		@SuppressWarnings("null")
		public RPkgDescrSourceUnitModelInfo getModel() {
			return this.newModel;
		}
		
	}
	
	
	private final RModelManagerImpl rManager;
	protected boolean stop= false;
	
	private final Object raLock= new Object();
	private final StringParserInput raInput= new StringParserInput(0x1000);
	private final RegionParserInput raRegionInput= new RegionParserInput(this.raInput, "\n"); //$NON-NLS-1$
	private final RPkgDescrDcfParser raParser= new RPkgDescrDcfParser();
	
	private final List<TextRegion> raRegions= new ArrayList<>();
	private final RParser raRParser= new RParser(AstInfo.LEVEL_MODEL_DEFAULT);
	
	private final Object rmLock= new Object();
	private final RPkgDescrSourceAnalyzer rmSourceAnalyzer= new RPkgDescrSourceAnalyzer();
	
	private final Object riLock= new Object();
	private final RPkgDescrIssueReporter riReporter= new RPkgDescrIssueReporter();
	
	private @Nullable RProject project;
	private @Nullable MultiStatus statusCollector;
	
	
	public RPkgDescrReconciler(final RModelManagerImpl manager) {
		this.rManager= manager;
	}
	
	
	public void init(final RProject project, final MultiStatus statusCollector) {
		this.project= nonNullAssert(project);
		this.statusCollector= statusCollector;
	}
	
	void stop() {
		this.stop= true;
	}
	
	
	public @Nullable RPkgDescrSourceUnitModelInfo reconcile(final RPkgDescrSourceUnitModelContainer adapter,
			final int flags,
			final IProgressMonitor monitor) {
		final var m= SubMonitor.convert(monitor);
		
		final Data data= new Data(adapter, m);
		if (data == null || !data.isOK()) {
			adapter.clear();
			return null;
		}
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		
		synchronized (this.raLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			
			updateAst(data, flags);
		}
		
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		if ((flags & 0xf) < ModelManager.MODEL_FILE) {
			return null;
		}
		
		synchronized (this.rmLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			
			final boolean updated= updateModel(data, flags, m);
			if (updated) {
				this.rManager.getEventJob().addUpdate(data.sourceUnit,
						data.oldModel, data.newModel );
			}
		}
		
		if ((flags & ModelManager.RECONCILE) != 0 && data.newModel != null) {
			synchronized (this.riLock) {
				if (this.stop || m.isCanceled()
						|| data.newModel != data.adapter.getCurrentModel() ) {
					return null;
				}
				
				reportIssues(data, flags);
			}
		}
		
		return data.newModel;
	}
	
	
	protected final void updateAst(final Data data, final int flags) {
		final RSourceConfig rSourceConfig= RCore.getContextAccess(data.adapter.getSourceUnit())
				.getRSourceConfig();
		final BasicSourceModelStamp stamp= new BasicSourceModelStamp(data.content.getStamp(),
				ImCollections.newList(rSourceConfig) );
		
		AstInfo ast= data.adapter.getCurrentAst();
		if (ast != null && !stamp.equals(ast.getStamp())) {
			ast= null;
		}
		if (ast != null) {
			data.ast= ast;
		}
		else {
			final SourceComponent sourceNode;
			List<EmbeddingValue> embeddingNodes;
			
			final TextParserInput input;
			if (data.content.getStartOffset() != 0) {
				input= new OffsetStringParserInput(data.content.getString(), data.content.getStartOffset());
			}
			else {
				input= this.raInput.reset(data.content.getString());
			}
			
			this.raParser.setCommentLevel(DcfRParser.COLLECT_COMMENTS);
			this.raParser.setCollectEmebeddedNodes(true);
			
			sourceNode= this.raParser.parseSourceUnit(
					this.raInput.init(data.content) );
			
			embeddingNodes= this.raParser.getEmbeddingNodes();
			final ImIdentitySet<String> embeddedTypeIds;
			if (!embeddingNodes.isEmpty()) {
				this.raRParser.setRSourceConfig(rSourceConfig);
				this.raRParser.setCommentLevel(0);
				embeddedTypeIds= updateEmbeddingAst(data.content, embeddingNodes);
			}
			else {
				embeddedTypeIds= ImCollections.emptyIdentitySet();
			}
			
			ast= new AstInfo(1, stamp, sourceNode, embeddedTypeIds);
			
			synchronized (data.adapter) {
				data.adapter.setAst(ast);
			}
			data.ast= ast;
		}
	}
	
	protected final boolean updateModel(final Data data, final int flags,
			final SubMonitor m) {
		RPkgDescrSourceUnitModelInfo model= data.adapter.getCurrentModel();
		if (model != null && !data.getAst().getStamp().equals(model.getStamp())) {
			model= null;
		}
		if (model != null) {
			data.newModel= model;
			return false;
		}
		else {
			model= this.rmSourceAnalyzer.createModel(data.adapter.getSourceUnit(), data.getAst());
			final boolean isOK= (model != null);
			
			if (isOK) {
				if (!data.ast.getEmbeddedTypes().isEmpty()) {
					reconcileEmbeddedModel(model,
							this.rmSourceAnalyzer.getEmbeddedItems(),
							data.adapter, flags,
							m );
				}
				
				synchronized (data.adapter) {
					data.oldModel= data.adapter.getCurrentModel();
					data.adapter.setModel(model);
				}
				data.newModel= model;
				return true;
			}
			return false;
		}
	}
	
	protected void reportIssues(final Data data, final int flags) {
		try {
			final var issueSupport= data.adapter.getIssueSupport();
			if (issueSupport == null) {
				return;
			}
			final var issueRequestor= issueSupport.createIssueRequestor(data.sourceUnit);
			if (issueRequestor != null) {
				try {
					this.riReporter.run(data.sourceUnit, data.getModel(), data.content,
							issueRequestor, flags );
				}
				finally {
					issueRequestor.finish();
				}
			}
		}
		catch (final Exception e) {
			handleStatus(new Status(IStatus.ERROR, RCore.BUNDLE_ID, 0,
					String.format("An error occurred when reporting issues for source unit %1$s.",
							data.sourceUnit ),
					e ));
		}
	}
	
	
	private ImIdentitySet<String> updateEmbeddingAst(final SourceContent content, final List<EmbeddingValue> embeddingNodes) {
		final Map<String, Boolean> ids= new IdentityHashMap<>(4);
		for (final EmbeddingValue embeddingNode : embeddingNodes) {
			ids.put(embeddingNode.getForeignTypeId(), Boolean.TRUE);
			if (embeddingNode.getForeignTypeId() != RModel.R_TYPE_ID) {
				continue;
			}
			
			this.raRegionInput.reset(getCodeRegions(embeddingNode));
			final var rCodeNode= this.raRParser.parseSourceFragment(
					this.raRegionInput.init(embeddingNode.getStartOffset(), embeddingNode.getEndOffset()),
					embeddingNode, true );
			embeddingNode.setForeignNode(rCodeNode);
		}
		return ImCollections.toIdentitySet(ids.keySet());
	}
	
	private void reconcileEmbeddedModel(final RPkgDescrSourceUnitModelInfo mainModel,
			final List<EmbeddingReconcileTask> tasks,
			final RPkgDescrSourceUnitModelContainer adapter,
			final int level, final IProgressMonitor monitor) {
		if (tasks == null || tasks.isEmpty()) {
			return;
		}
		
		final var inlineNodes= new ArrayList<org.eclipse.statet.r.core.source.ast.SourceComponent>();
		
		for (final EmbeddingReconcileTask task : tasks) {
			if (task.getForeignTypeId() != RModel.R_TYPE_ID) {
				continue;
			}
			
			final EmbeddingAstNode embeddingNode= task.getAstNode();
			inlineNodes.add((org.eclipse.statet.r.core.source.ast.SourceComponent)embeddingNode.getForeignNode());
		}
		
		final RSourceUnitModelInfo modelInfo= this.rManager.reconcile(adapter.getSourceUnit(),
				mainModel, ImCollections.emptyList(), inlineNodes, level, monitor );
//		mainModel.addAttachment(modelInfo);
	}
	
	
	protected void handleStatus(final IStatus status) {
		final MultiStatus collector= this.statusCollector;
		if (collector != null) {
			collector.add(status);
		}
		else {
			RCorePlugin.log(status);
		}
	}
	
	
	private ImList<TextRegion> getCodeRegions(final EmbeddingValue node) {
		final var textRegions= node.getTextRegions();
		this.raRegions.clear();
		for (final TextRegion region : textRegions) {
			if (!(region instanceof Value.EmptyLine)) {
				this.raRegions.add(region);
			}
		}
		return (this.raRegions.size() == textRegions.size()) ?
				textRegions : ImCollections.toList(this.raRegions);
	}
	
}
