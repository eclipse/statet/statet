/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * {@link RAstProblemReporter}
 */
@NonNullByDefault
@SuppressWarnings("null")
public class ProblemMessages extends NLS {
	
	
	public static String Syntax_SymbolStartInvalid_message;
	public static String Syntax_StringNotClosed_message;
	public static String Syntax_QuotedSymbolNotClosed_message;
	public static String Syntax_StringROpeningIncomplete_message;
	public static String Syntax_StringRNotClosed_message;
	public static String Syntax_SpecialNotClosed_message;
	
	public static String Syntax_Number_HexDigitMissing_message;
	public static String Syntax_Number_HexFloatExpMissing_message;
	public static String Syntax_Number_ExpDigitMissing_message;
	public static String Syntax_Number_NonIntWithLLiteral_message;
	public static String Syntax_Number_IntWithDecPoint_message;
	
	public static String Syntax_String_NullCharNotAllowed_message;
	public static String Syntax_String_CodePointInvalid_message;
	public static String Syntax_String_EscapeSeqHexDigitMissing_message;
	public static String Syntax_String_EscapeSeqNotClosed_message;
	public static String Syntax_String_QuotedSymbol_EscapeSeqUnexpected_message;
	public static String Syntax_String_EscapeSeqUnknown_message;
	
	public static String Syntax_TokenUnknown_message;
	public static String Syntax_TokenUnexpected_message;
	public static String Syntax_TokenUnexpected_SeqRel_message;
	public static String Syntax_ExprUnexpected_FCallAfterPipeExpected_message;
	
	public static String Syntax_ExprInGroupMissing_message;
	public static String Syntax_ExprBeforeOpMissing_message;
	public static String Syntax_ExprAfterOpMissing_message;
	public static String Syntax_ExprAsConditionMissing_message;
	public static String Syntax_ExprAsForSequenceMissing_message;
	public static String Syntax_ExprAsThenBodyMissing_message;
	public static String Syntax_ExprAsElseBodyMissing_message;
	public static String Syntax_ExprAsLoopBodyMissing_message;
	public static String Syntax_ExprAsFdefBodyMissing_message;
	public static String Syntax_ExprAsFdefArgDefaultMissing_message;
	public static String Syntax_ElementnameMissing_message;
	public static String Syntax_FCallAfterPipeMissing_message;
	public static String Syntax_SymbolMissing_message;
	
	public static String Syntax_FcallArgsNotClosed_message;
	public static String Syntax_OperatorMissing_message;
	public static String Syntax_SubindexedNotClosed_message;
	
	public static String Syntax_GroupNotClosed_message;
	public static String Syntax_BlockNotClosed_message;
	public static String Syntax_ConditionMissing_message;
	public static String Syntax_IfOfElseMissing_message;
	public static String Syntax_InOfForConditionMissing_message;
	public static String Syntax_ConditionNotClosed_message;
	
	public static String Syntax_FdefArgsMissing_message;
	public static String Syntax_FdefArgsNotClosed_message;
	
	public static String Syntax_FdefShorthandUnsupported_message;
	public static String Syntax_PipeRightUnsupported_message;
	public static String Syntax_PipePlaceholderUnexpected_message;
	public static String Syntax_PipePlaceholderUnnamedUnexpected_message;
	public static String Syntax_PipePlaceholderMultipleUnexpected_message;
	public static String Syntax_PipePlaceholderUnsupported_message;
	
	// automatically generated
	public static String Syntax_SubindexedNotClosed_S_message;
	public static String Syntax_SubindexedNotClosed_Done_message;
	public static String Syntax_SubindexedNotClosed_Dboth_message;
	public static String Syntax_ConditionMissing_If_message;
	public static String Syntax_ConditionMissing_For_message;
	public static String Syntax_ConditionMissing_While_message;
	public static String Syntax_ConditionNotClosed_If_message;
	public static String Syntax_ConditionNotClosed_For_message;
	public static String Syntax_ConditionNotClosed_While_message;
	
	
	static {
		NLS.initializeMessages(ProblemMessages.class.getName(), ProblemMessages.class);
		
		// Combined messages
		Syntax_SubindexedNotClosed_S_message= NLS.bind(Syntax_SubindexedNotClosed_message, "[…]", "]"); //$NON-NLS-1$ //$NON-NLS-2$
		Syntax_SubindexedNotClosed_Done_message= NLS.bind(Syntax_SubindexedNotClosed_message, "[[…]]", "]"); //$NON-NLS-1$ //$NON-NLS-2$
		Syntax_SubindexedNotClosed_Dboth_message= NLS.bind(Syntax_SubindexedNotClosed_message, "[[…]]", "]]"); //$NON-NLS-1$ //$NON-NLS-2$
		Syntax_ConditionMissing_If_message= NLS.bind(Syntax_ConditionMissing_message, "if"); //$NON-NLS-1$
		Syntax_ConditionMissing_For_message= NLS.bind(Syntax_ConditionMissing_message, "for"); //$NON-NLS-1$
		Syntax_ConditionMissing_While_message= NLS.bind(Syntax_ConditionMissing_message, "while"); //$NON-NLS-1$
		Syntax_ConditionNotClosed_If_message= NLS.bind(Syntax_ConditionNotClosed_message, "if"); //$NON-NLS-1$
		Syntax_ConditionNotClosed_For_message= NLS.bind(Syntax_ConditionNotClosed_message, "for"); //$NON-NLS-1$
		Syntax_ConditionNotClosed_While_message= NLS.bind(Syntax_ConditionNotClosed_message, "while"); //$NON-NLS-1$
	}
	
	private ProblemMessages() {}
	
}
