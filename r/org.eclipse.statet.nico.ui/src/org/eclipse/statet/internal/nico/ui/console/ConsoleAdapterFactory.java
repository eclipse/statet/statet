/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.console;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.part.PageBookView;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


/**
 * Required for 'adapt' expressions because adapters are not in classpath of ConsoleView.
 */
@NonNullByDefault
public class ConsoleAdapterFactory implements IAdapterFactory {
	
	
	private final @NonNull Class<?>[] ADAPTERS= new @NonNull Class[] {
		SourceEditor.class
	};
	
	
	/** plugin.xml */
	public ConsoleAdapterFactory() {
	}
	
	
	@Override
	public @NonNull Class<?>[] getAdapterList() {
		return this.ADAPTERS;
	}
	
	@Override
	public <T> @Nullable T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adaptableObject instanceof PageBookView) {
			final IPage page= ((PageBookView)adaptableObject).getCurrentPage();
			if (page instanceof IAdaptable) {
				return ((IAdaptable)page).getAdapter(adapterType);
			}
		}
		return null;
	}
	
}
