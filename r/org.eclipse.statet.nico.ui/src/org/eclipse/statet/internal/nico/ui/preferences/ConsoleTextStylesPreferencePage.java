/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.preferences;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.preferences.ui.ScopedPreferenceStore;
import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.presentation.AbstractTextStylesConfigurationBlock;
import org.eclipse.statet.ltk.ui.util.CombinedPreferenceStore;
import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.nico.ui.NicoUIPreferences;


@NonNullByDefault
public class ConsoleTextStylesPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public ConsoleTextStylesPreferencePage() {
		final ScopedPreferenceStore store= new ScopedPreferenceStore(InstanceScope.INSTANCE,
				NicoUIPreferences.OUTPUT_QUALIFIER );
		setPreferenceStore(store);
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() {
		return new ConsoleTextStylesConfigurationBlock();
	}
	
}


@NonNullByDefault
class ConsoleTextStylesConfigurationBlock extends AbstractTextStylesConfigurationBlock {
	
	
	public ConsoleTextStylesConfigurationBlock() {
	}
	
	
	@Override
	protected String getSettingsGroup() {
		return ConsolePreferences.OUTPUT_TEXTSTYLE_GROUP_ID;
	}
	
	@Override
	protected ImList<SyntaxNode> createItems() {
		return ImCollections.newList(
			new StyleNode(Messages.TextStyle_Input_label, Messages.TextStyle_Input_description,
					NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, ImCollections.newList(
						SyntaxNode.createUseCustomStyle() )),
			new StyleNode(Messages.TextStyle_Info_label, Messages.TextStyle_Info_description,
					NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, ImCollections.newList(
						SyntaxNode.createUseCustomStyle() )),
			new StyleNode(Messages.TextStyle_StandardOutput_label, Messages.TextStyle_StandardOutput_description,
					NicoUIPreferences.OUTPUT_STD_OUTPUT_ROOT_KEY, ImCollections.newList(
						SyntaxNode.createUseCustomStyle() ),
				new StyleNode(Messages.TextStyle_SystemOutput_label, Messages.TextStyle_SystemOutput_description,
						NicoUIPreferences.OUTPUT_SYSTEM_OUTPUT_STREAM_ROOT_KEY, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ))),
			new StyleNode(Messages.TextStyle_StandardError_label, Messages.TextStyle_StandardError_description,
					NicoUIPreferences.OUTPUT_STD_ERROR_STREAM_ROOT_KEY, ImCollections.newList(
						SyntaxNode.createUseCustomStyle() )),
			
			new CategoryNode(Messages.TextStyle_SpecialBackground_label,
					new BackgroundNode(SubmitType.OTHER.getLabel(),
							NLS.bind(Messages.TextStyle_SpecialBackground_Tasks_description, SubmitType.OTHER.getLabel()),
							NicoUIPreferences.OUTPUT_OTHER_TASKS_BACKGROUND_ROOT_KEY )
			)
		);
	}
	
	@Override
	protected String getLinkMessage() {
		return Messages.TextStyle_link;
	}
	
	@Override
	protected boolean isTextAttributesSupported() {
		return false;
	}
	
	@Override
	protected String getPreviewFileName() {
		return "ConsoleTextStylesPreviewCode.txt"; //$NON-NLS-1$
	}
	
	@Override
	protected IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new PartitionerDocumentSetupParticipant() {
			@Override
			public String getPartitioningId() {
				return ConsoleTextStylesPreviewPartitioner.PARTITIONING;
			}
			
			@Override
			protected IDocumentPartitioner createDocumentPartitioner() {
				return new ConsoleTextStylesPreviewPartitioner();
			}
		};
	}
	
	@Override
	protected SourceEditorViewerConfiguration getSourceEditorViewerConfiguration(
			final IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		return new ConsolePreviewSourceViewerConfiguration(0,
				CombinedPreferenceStore.createStore(
						preferenceStore,
//						StatetUIServices.getBaseUIPreferenceStore(),
						EditorsUI.getPreferenceStore() ),
				textStyles );
	}
	
}
