/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.console;

import static org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants.EDITOR_CURRENT_LINE_COLOR;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.text.source.ISharedTextColors;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.workbench.css.StylingUtils;

import org.eclipse.statet.nico.ui.console.NIConsoleColorAdapter;
import org.eclipse.statet.nico.ui.console.NIConsoleOutputStream;


@NonNullByDefault
public class PromptHighlighter implements IPropertyChangeListener {
	
	
	private Control control;
	
	private final IPreferenceStore preferenceStore;
	private final PreferenceAccess prefAccess;
	private final ISharedTextColors sharedColors;
	
	private @Nullable Color foregroundColor;
	private @Nullable Color backgroundColor;
	
	private boolean isHighlightEnabled;
	
	
	public PromptHighlighter(final Control control,
			final PreferenceAccess prefAccess, final IPreferenceStore preferenceStore) {
		this.control= control;
		StylingUtils.disableStyling(control);
		this.prefAccess= prefAccess;
		this.preferenceStore= preferenceStore;
		this.sharedColors= EditorsUI.getSharedTextColors();
		
		this.preferenceStore.addPropertyChangeListener(this);
		
		updateControl();
	}
	
	public void dispose() {
		this.control= null;
		this.preferenceStore.removePropertyChangeListener(this);
	}
	
	
	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		if (event.getProperty().equals(EDITOR_CURRENT_LINE_COLOR)) {
			Display.getDefault().asyncExec(() -> {
				updateSettings();
				updateControl();
			});
		}
	}
	
	public void setHighlight(final boolean enable) {
		if (enable == this.isHighlightEnabled) {
			return;
		}
		this.isHighlightEnabled= enable;
		updateControl();
	}
	
	public void updateSettings() {
		this.foregroundColor= null;
		this.backgroundColor= null;
	}
	
	public void updateControl() {
		final Control control= this.control;
		if (!UIAccess.isOkToUse(control)) {
			return;
		}
		
		Color foregroundColor;
		Color backgroundColor;
		if (this.isHighlightEnabled) {
			foregroundColor= this.foregroundColor;
			if (foregroundColor == null) {
				final RGB rgb= this.prefAccess.getPreferenceValue(
						NIConsoleColorAdapter.getForegroundColorPref(NIConsoleOutputStream.INFO_STREAM_ID) );
				foregroundColor= this.sharedColors.getColor(rgb);
				this.foregroundColor= foregroundColor;
			}
			backgroundColor= this.backgroundColor;
			if (backgroundColor == null) {
				final RGB rgb= PreferenceConverter.getColor(
						this.preferenceStore, EDITOR_CURRENT_LINE_COLOR );
				backgroundColor= this.sharedColors.getColor(rgb);
				this.backgroundColor= backgroundColor;
			}
			
		}
		else {
			foregroundColor= control.getDisplay().getSystemColor(
					SWT.COLOR_WIDGET_DISABLED_FOREGROUND );
			backgroundColor= null;
		}
		control.setForeground(foregroundColor);
		control.setBackground(backgroundColor);
	}
	
}
