/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.nico.core.NicoCore;
import org.eclipse.statet.nico.ui.NicoUIResources;


/**
 * The activator class controls the plug-in life cycle
 */
@NonNullByDefault
public final class NicoUIPlugin extends AbstractUIPlugin {
	
	
	public static final int INTERNAL_ERROR = 100;
	
	
	private static NicoUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static NicoUIPlugin getInstance() {
		return instance;
	}
	
	
	public static void logError(final int code, final String message, final Throwable e) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR, NicoCore.BUNDLE_ID, code, message, e));
	}
	
	
	private ToolRegistry toolRegistry;
	
	private @Nullable DecoratorsRegistry uiDecoratorsRegistry;
	
	
	/**
	 * The constructor
	 */
	public NicoUIPlugin() {
		instance= this;
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		
		this.toolRegistry= new ToolRegistry();
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			if (this.toolRegistry != null) {
				this.toolRegistry.dispose();
				this.toolRegistry= null;
			}
			if (this.uiDecoratorsRegistry != null) {
				this.uiDecoratorsRegistry= null;
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		final ImageRegistryUtil util = new ImageRegistryUtil(this);
		
		util.register(NicoUIResources.OBJ_TASK_CONSOLECOMMAND_IMAGE_ID, ImageRegistryUtil.T_OBJ, "task-consolecommand.png"); //$NON-NLS-1$
		util.register(NicoUIResources.OBJ_TASK_DUMMY_IMAGE_ID, ImageRegistryUtil.T_OBJ, "task-dummy.png"); //$NON-NLS-1$
		util.register(NicoUIResources.OBJ_CONSOLECOMMAND_IMAGE_ID, ImageRegistryUtil.T_OBJ, "consolecommand.png"); //$NON-NLS-1$
	}
	
	public ToolRegistry getToolRegistry() {
		return this.toolRegistry;
	}
	
	public synchronized DecoratorsRegistry getUIDecoratorsRegistry() {
		var registry= this.uiDecoratorsRegistry;
		if (registry == null) {
			registry= new DecoratorsRegistry();
			this.uiDecoratorsRegistry= registry;
		}
		return registry;
	}
	
}
