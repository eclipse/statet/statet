/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui;

import java.util.HashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.ecommons.ts.ui.ToolRunnableDecorator;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


public class DecoratorsRegistry {
	
	
	private static final String DECORATORS_EXTENSION_POINT= "org.eclipse.statet.ecommons.ts.UIDecorators"; //$NON-NLS-1$
	
	private static final String RUNNABLE_ELEMENT_NAME= "runnable"; //$NON-NLS-1$
	
	private static final String RUNNABLE_ID_ATTRIBUTE_NAME= "typeId"; //$NON-NLS-1$
	private static final String ICON_ATTRIBUTE_NAME= "icon"; //$NON-NLS-1$
	
	
	private class RunnableDecorator implements ToolRunnableDecorator {
		
		ImageDescriptor iconDescriptor;
		
		Image icon;
		
		
		@Override
		public Image getImage() {
			if (this.icon == null && this.iconDescriptor != null) {
				this.icon= DecoratorsRegistry.this.manager.createImageWithDefault(this.iconDescriptor);
			}
			return this.icon;
		}
		
	}
	
	private final HashMap<String, RunnableDecorator> runnableIconMap= new HashMap<>();
	
	private final Display display;
	private ResourceManager manager;
	
	
	public DecoratorsRegistry() {
		this.display= UIAccess.getDisplay();
		final Runnable runnable= new Runnable() {
			@Override
			public void run() {
				DecoratorsRegistry.this.manager= JFaceResources.getResources(DecoratorsRegistry.this.display);
				DecoratorsRegistry.this.display.disposeExec(new Runnable() {
					@Override
					public void run() {
						DecoratorsRegistry.this.dispose();
					}
				});
				DecoratorsRegistry.this.load();
			}
		};
		if (this.display.getThread() == Thread.currentThread()) {
			runnable.run();
		}
		else {
			this.display.syncExec(runnable);
		}
	}
	
	
	private void load() {
		final IConfigurationElement[] configurationElements= Platform.getExtensionRegistry()
				.getConfigurationElementsFor(DECORATORS_EXTENSION_POINT);
		for (final IConfigurationElement configuration : configurationElements) {
			if (configuration.getName().equals(RUNNABLE_ELEMENT_NAME)) {
				final String typeId= configuration.getAttribute(RUNNABLE_ID_ATTRIBUTE_NAME);
				if (typeId == null || typeId.isEmpty()) {
					// TODO log
				}
				else {
					final RunnableDecorator decorators= new RunnableDecorator();
					final String icon= configuration.getAttribute(ICON_ATTRIBUTE_NAME);
					final String namespaceId= configuration.getNamespaceIdentifier();
					if (icon != null && icon.length() > 0) {
						decorators.iconDescriptor= AbstractUIPlugin
								.imageDescriptorFromPlugin(namespaceId, icon);
					}
					this.runnableIconMap.put(typeId, decorators);
				}
			}
		}
	}
	
	
	public ToolRunnableDecorator getDecoratorForRunnable(final String typeId) {
		return this.runnableIconMap.get(typeId);
	}
	
	private void dispose() {
		for (final RunnableDecorator decorator : this.runnableIconMap.values()) {
			if (decorator.icon != null) {
				this.manager.destroyImage(decorator.iconDescriptor);
			}
		}
		this.runnableIconMap.clear();
	}
	
}
