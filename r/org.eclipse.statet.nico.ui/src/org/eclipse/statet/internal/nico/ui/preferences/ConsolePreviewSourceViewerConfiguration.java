/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.preferences;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.source.ISourceViewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.BasicDocContentSections;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.presentation.SingleTokenScanner;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;


@NonNullByDefault
public class ConsolePreviewSourceViewerConfiguration extends SourceEditorViewerConfiguration {
	
	
	private static final String[] CONTENT_TYPES= ConsoleTextStylesPreviewPartitioner.CONTENT_TYPES.toArray(
			new String[ConsoleTextStylesPreviewPartitioner.CONTENT_TYPES.size()] );
	
	
	private static final DocContentSections CONTENT_INFO= new BasicDocContentSections(ConsoleTextStylesPreviewPartitioner.PARTITIONING,
			ConsoleTextStylesPreviewPartitioner.PARTITIONING) {
		@Override
		public String getTypeByPartition(final String contentType) {
			return ConsoleTextStylesPreviewPartitioner.PARTITIONING;
		}
	};
	
	
	public ConsolePreviewSourceViewerConfiguration(final int flags,
			final IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		super(CONTENT_INFO, flags, null);
		
		setup(preferenceStore, null, null);
		setTextStyles(textStyles);
	}
	
	
	@Override
	public String[] getConfiguredContentTypes(final @Nullable ISourceViewer sourceViewer) {
		return CONTENT_TYPES;
	}
	
	@Override
	protected void initScanners() {
		final var textStyles= getTextStyles();
		
		for (final String contentType : getConfiguredContentTypes(null)) {
			addScanner(contentType,
					new SingleTokenScanner(textStyles, contentType) );
		}
	}
	
}
