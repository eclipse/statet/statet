/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.operation.IRunnableWithProgress;

import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;

import org.eclipse.statet.nico.core.runtime.History.Entry;
import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUIMessages;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.views.HistoryView;


public class HistorySubmitAction extends Action {
	
	
	private final HistoryView view;
	
	
	public HistorySubmitAction(final HistoryView view) {
		super(NicoUIMessages.SubmitAction_name);
		
		setId("org.eclipse.statet.nico.addviews.submit"); //$NON-NLS-1$
		
		this.view= view;
	}
	
	@Override
	public void run() {
		final Entry[] selection= this.view.getSelection();
		final ToolProcess process= this.view.getTool();
		final ToolController controller= (process != null) ? process.getController() : null;
		if (selection == null || controller == null) {
			return;
		}
		
		final IRunnableWithProgress runnable= new IRunnableWithProgress() {
			@Override
			public void run(final IProgressMonitor monitor) throws InterruptedException, InvocationTargetException {
				final SubMonitor m= SubMonitor.convert(monitor);
				try {
					m.beginTask(NicoUITools.createSubmitMessage(controller.getTool()), 2 + 8);
					
					final List<String> commands= HistoryView.createCommandList(selection);
					m.worked(2);
					
					final Status status= controller.submit(commands, SubmitType.EDITOR,
							m.newChild(8) );
					if (status.getSeverity() >= Status.ERROR) {
						throw new CoreException(StatusUtils.convert(status));
					}
				}
				catch (final CoreException e) {
					throw new InvocationTargetException(e);
				}
				finally {
					m.done();
				}
			}
			
		};
		NicoUITools.runSubmitInBackground(process, runnable, this.view.getSite().getShell());
	}
	
}
