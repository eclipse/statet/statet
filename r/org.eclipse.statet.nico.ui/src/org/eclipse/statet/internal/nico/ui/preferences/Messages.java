/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.preferences;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String TextStyle_link;
	public static String TextStyle_Input_label;
	public static String TextStyle_Input_description;
	public static String TextStyle_Info_label;
	public static String TextStyle_Info_description;
	public static String TextStyle_StandardOutput_label;
	public static String TextStyle_StandardOutput_description;
	public static String TextStyle_SystemOutput_label;
	public static String TextStyle_SystemOutput_description;
	public static String TextStyle_StandardError_label;
	public static String TextStyle_StandardError_description;
	public static String TextStyle_SpecialBackground_label;
	public static String TextStyle_SpecialBackground_Tasks_description;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
