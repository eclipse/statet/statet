/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.preferences;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.core.validation.IntegerValidator;
import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.nico.core.runtime.SubmitType;
import org.eclipse.statet.nico.ui.NicoUIPreferences;
import org.eclipse.statet.nico.ui.util.SubmitTypeSelectionComposite;


@NonNullByDefault
public class ConsolePreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public ConsolePreferencePage() {
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() throws CoreException {
		return new ConsolePreferenceBlock(createStatusChangedListener());
	}
	
}


@NonNullByDefault
class ConsolePreferenceBlock extends ManagedConfigurationBlock {
	
	
	private SubmitTypeSelectionComposite inputSubmitTypeControl;
	
	private Text outputCharLimitControl;
	private SubmitTypeSelectionComposite outputSubmitTypeControl;
	
	
	public ConsolePreferenceBlock(final @Nullable StatusChangeListener statusListener) {
		super(null, statusListener);
	}
	
	
	@Override
	protected void createBlockArea(final Composite pageComposite) {
		final Map<Preference<?>, @Nullable String> prefs= new HashMap<>();
		
		prefs.put(ConsolePreferences.HISTORYNAVIGATION_SUBMIT_TYPES_PREF, ConsolePreferences.GROUP_ID);
		prefs.put(NicoUIPreferences.OUTPUT_CHARLIMIT_PREF, ConsolePreferences.GROUP_ID);
		prefs.put(NicoUIPreferences.OUTPUT_FILTER_SUBMITTYPES_INCLUDE_PREF, ConsolePreferences.GROUP_ID);
		
		setupPreferenceManager(prefs);
		
		final Composite appearance= createOutputOptions(pageComposite);
		appearance.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		final Composite input= createInputOptions(pageComposite);
		input.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		initBindings();
		updateControls();
	}
	
	private Composite createInputOptions(final Composite parent) {
		final Group group= new Group(parent, SWT.NONE);
		group.setText("Input" + ':');
		group.setLayout(LayoutUtils.newGroupGrid(1));
		
		{	final Label label= new Label(group, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
			label.setText("Include in &history navigation (up/down) input from:");
			
			this.inputSubmitTypeControl= new SubmitTypeSelectionComposite(group);
			this.inputSubmitTypeControl.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true));
		}
		
		return group;
	}
	
	private Composite createOutputOptions(final Composite parent) {
		final Group group= new Group(parent, SWT.NONE);
		group.setText("Output" + ':');
		group.setLayout(LayoutUtils.newGroupGrid(2));
		
		{	final Label label= new Label(group, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			label.setText("S&ize (characters):");
			final Text text= new Text(group, SWT.BORDER | SWT.RIGHT);
			final GridData gd= new GridData(SWT.LEFT, SWT.CENTER, true, false);
			gd.widthHint= LayoutUtils.hintWidth(text, 12);
			text.setLayoutData(gd);
			text.setTextLimit(20);
			this.outputCharLimitControl= text;
		}
		
		LayoutUtils.addSmallFiller(group, false);
		
		{	final Label label= new Label(group, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
			label.setText("&Print content from:");
			
			this.outputSubmitTypeControl= new SubmitTypeSelectionComposite(group);
			this.outputSubmitTypeControl.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, true, 2, 1));
			this.outputSubmitTypeControl.setEditable(EnumSet.of(SubmitType.OTHER));
		}
		
		return group;
	}
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		db.getContext().bindValue(
				WidgetProperties.text(SWT.Modify)
						.observe(this.outputCharLimitControl),
				createObservable(NicoUIPreferences.OUTPUT_CHARLIMIT_PREF),
				new UpdateValueStrategy<String, Integer>()
						.setAfterGetValidator(new IntegerValidator(100000, 1000000000,
								"Invalid char limit specified (100000, 1000000000)." )),
				null );
		db.getContext().bindValue(
				this.outputSubmitTypeControl.getObservable(),
				createObservable(NicoUIPreferences.OUTPUT_FILTER_SUBMITTYPES_INCLUDE_PREF) );
		
		db.getContext().bindValue(
				this.inputSubmitTypeControl.getObservable(),
				createObservable(ConsolePreferences.HISTORYNAVIGATION_SUBMIT_TYPES_PREF) );
	}
	
}
