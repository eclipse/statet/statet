/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;

import org.eclipse.statet.nico.core.runtime.History.Entry;
import org.eclipse.statet.nico.ui.views.HistoryView;


public class HistoryDragAdapter extends DragSourceAdapter {
	
	
	private final HistoryView fView;
	
	private Entry[] fCurrentSelection;
	
	
	public HistoryDragAdapter(final HistoryView view) {
		fView = view;
	}
	
	
	@Override
	public void dragStart(final DragSourceEvent event) {
		fCurrentSelection = fView.getSelection();
		
		event.doit = (fCurrentSelection != null && fCurrentSelection.length > 0);
	}
	
	@Override
	public void dragSetData(final DragSourceEvent event) {
		final String text = HistoryView.createTextBlock(fCurrentSelection);
		event.data = text;
	}
	
	@Override
	public void dragFinished(final DragSourceEvent event) {
		fCurrentSelection = null;
	}
	
}
