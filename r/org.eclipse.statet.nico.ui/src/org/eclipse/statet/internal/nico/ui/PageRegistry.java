/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.contexts.DebugContextEvent;
import org.eclipse.debug.ui.contexts.IDebugContextListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.progress.WorkbenchJob;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.ActiveToolListener;
import org.eclipse.statet.jcommons.ts.core.ActiveToolListener.ActiveToolEvent;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolRegistryListener;
import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolSessionData;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.console.NIConsole;


/**
 * part of tool registry per workbench page
 */
@NonNullByDefault
class PageRegistry implements IDebugEventSetListener, IDebugContextListener, ToolProvider {
	
	
	static final String SHOW_CONSOLE_JOB_NAME= "Show NIConsole"; //$NON-NLS-1$
	
	
	private class ShowConsoleViewJob extends WorkbenchJob {
		
		private final int delay;
		
		private volatile @Nullable NIConsole consoleToShow;
		private volatile boolean activate;
		
		
		public ShowConsoleViewJob(final int delay) {
			super(SHOW_CONSOLE_JOB_NAME);
			setSystem(true);
			setPriority(Job.SHORT);
			this.delay= delay;
		}
		
		
		public void schedule(final NIConsole console, final boolean activate) {
			cancel();
			this.consoleToShow= console;
			this.activate= activate;
			schedule(this.delay);
		}
		
		@Override
		public IStatus runInUIThread(final IProgressMonitor monitor) {
			final NIConsole console= this.consoleToShow;
			if (PageRegistry.this.isClosed || console == null) {
				return Status.CANCEL_STATUS;
			}
			try {
				final IWorkbenchPart activePart= PageRegistry.this.page.getActivePart();
				if (activePart instanceof IConsoleView) {
					if (console == ((IConsoleView)activePart).getConsole()) {
						activePart.setFocus();
						return Status.OK_STATUS;
					}
				}
				final IConsoleView view= searchView(console);
				return showInView(view, monitor);
			}
			catch (final PartInitException e) {
				NicoUIPlugin.logError(NicoUIPlugin.INTERNAL_ERROR, "Error of unexpected type occured, when showing a console view.", e); //$NON-NLS-1$
				return Status.OK_STATUS;
			}
			finally {
				this.consoleToShow= null;
			}
		}
		
		private IStatus showInView(@Nullable IConsoleView view, final IProgressMonitor monitor) throws PartInitException {
			final NIConsole console= this.consoleToShow;
			final boolean activate= this.activate;
			if (console == null
					|| PageRegistry.this.isClosed || monitor.isCanceled()) {
				return Status.CANCEL_STATUS;
			}
			
			if (view == null) {
				final String secId= console.getType() + System.currentTimeMillis(); // force creation
				view= (IConsoleView)PageRegistry.this.page.showView(
						IConsoleConstants.ID_CONSOLE_VIEW, secId, IWorkbenchPage.VIEW_CREATE );
			}
			view.display(console);
			if (activate) {
				PageRegistry.this.page.activate(view);
			}
			else {
				PageRegistry.this.page.bringToTop(view);
			}
			finish(view);
			return Status.OK_STATUS;
		}
		
		protected void finish(final IConsoleView view) {
		}
		
	}
	
	private class OnConsoleChangedJob extends Job implements ISchedulingRule {
		
		private volatile @Nullable NIConsole console;
		private volatile @Nullable IViewPart source;
		private volatile @Nullable List<ToolProcess> exclude;
		
		public OnConsoleChangedJob() {
			super("NicoUI Registry - On Console Changed");
			setSystem(true);
			setPriority(Job.SHORT);
		}
		
		@Override
		public boolean belongsTo(final Object family) {
			return (family == PageRegistry.this);
		}
		
		@Override
		public boolean contains(final ISchedulingRule rule) {
			return false;
		}
		
		@Override
		public boolean isConflicting(final ISchedulingRule rule) {
			if (rule instanceof Job) {
				return ((Job)rule).belongsTo(PageRegistry.this);
			}
			return false;
		}
		
		
		public void scheduleActivated(final NIConsole console, final @Nullable IViewPart source) {
			synchronized (PageRegistry.this) {
				final var exclude= this.exclude;
				if (console != null
						&& exclude != null && exclude.contains(console.getProcess()) ) {
					return;
				}
				cancel(); // ensure delay
				this.console= console;
				this.source= source;
				schedule(50);
			}
		}
		
		public void scheduleRemoved(final List<ToolProcess> exclude) {
			synchronized (PageRegistry.this) {
				final var console= this.console;
				if (console != null
						&& exclude.contains(console.getProcess())) {
					this.console= null;
				}
				else if (PageRegistry.this.activeProcess == null
						&& !exclude.contains(PageRegistry.this.activeProcess) ) {
					return;
				}
				cancel(); // ensure delay
				this.exclude= exclude;
				schedule(200);
			}
		}
		
		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			if (PageRegistry.this.isClosed) {
				return Status.OK_STATUS;
			}
			var console= this.console;
			final List<ToolProcess> exclude= this.exclude;
			
			if (console == null) {
				final AtomicReference<@Nullable NIConsole> ref= new AtomicReference<>();
				UIAccess.getDisplay(PageRegistry.this.page.getWorkbenchWindow().getShell()).syncExec(() -> {
					ref.set(searchConsole((exclude != null) ? exclude : Collections.emptyList()));
				});
				console= ref.get();
			}
			
			IViewPart source;
			synchronized(PageRegistry.this) {
				if (monitor.isCanceled()) {
					return Status.CANCEL_STATUS;
				}
				if (this.console == console) {
					this.console= null;
					this.exclude= null;
				}
				
				if ((console == PageRegistry.this.activeConsole) || (console == null && PageRegistry.this.activeConsole == null)) {
					return Status.OK_STATUS;
				}
				PageRegistry.this.activeProcess= (console != null) ? console.getProcess() : null;
				PageRegistry.this.activeConsole= console;
				source= this.source;
			}
			
			// don't cancel after process is changed
			notifyActiveToolSessionChanged(source);
			
			return Status.OK_STATUS;
		}
		
	}
	
	private class OnToolTerminatedJob extends Job implements ISchedulingRule {
		
		private volatile @Nullable ToolProcess tool;
		
		public OnToolTerminatedJob() {
			super("NicoUI Registry - On Tool Terminated"); //$NON-NLS-1$
			setSystem(true);
			setPriority(Job.SHORT);
		}
		
		@Override
		public boolean belongsTo(final Object family) {
			return (family == PageRegistry.this);
		}
		
		@Override
		public boolean contains(final ISchedulingRule rule) {
			return false;
		}
		
		@Override
		public boolean isConflicting(final ISchedulingRule rule) {
			if (rule instanceof Job) {
				return ((Job)rule).belongsTo(PageRegistry.this);
			}
			return false;
		}
		
		
		public void scheduleTerminated(final ToolProcess tool) {
			this.tool= tool;
			schedule(0);
		}
		
		@Override
		public synchronized IStatus run(final IProgressMonitor monitor) {
			final ToolProcess tool= this.tool;
			this.tool= null;
			
			if (getActiveProcess() == tool) {
				notifyToolTerminated();
			}
			return Status.OK_STATUS;
		}
		
	}
	
	
	private final IWorkbenchPage page;
	private boolean isClosed;
	
	private @Nullable ToolProcess activeProcess;
	private @Nullable NIConsole activeConsole;
	
	private final CopyOnWriteIdentityListSet<Object> listeners;
	
	private final ShowConsoleViewJob showConsoleViewJob= new ShowConsoleViewJob(100);
	private final OnConsoleChangedJob consoleUpdateJob= new OnConsoleChangedJob();
	private final OnToolTerminatedJob terminatedJob= new OnToolTerminatedJob();
	
	
	PageRegistry(final IWorkbenchPage page, final Set<WorkbenchToolRegistryListener> initial) {
		this.page= page;
		this.listeners= new CopyOnWriteIdentityListSet<>(initial);
		
		DebugUITools.getDebugContextManager().getContextService(this.page.getWorkbenchWindow()).addDebugContextListener(this);
		DebugPlugin.getDefault().addDebugEventListener(this);
	}
	
	
	public synchronized void dispose() {
		this.isClosed= true;
		DebugPlugin.getDefault().removeDebugEventListener(this);
		DebugUITools.getDebugContextManager().getContextService(this.page.getWorkbenchWindow()).removeDebugContextListener(this);
		this.showConsoleViewJob.cancel();
		this.consoleUpdateJob.cancel();
		this.terminatedJob.cancel();
		
		this.listeners.clear();
		this.activeProcess= null;
		this.activeConsole= null;
	}
	
	
	@Override
	public void debugContextChanged(final DebugContextEvent event) {
		final ISelection selection= event.getContext();
		if (selection instanceof final IStructuredSelection sel) {
			if (sel.size() == 1) {
				final Object element= sel.getFirstElement();
				ToolProcess tool= null;
				if (element instanceof IAdaptable) {
					final IProcess process= ((IAdaptable)element).getAdapter(IProcess.class);
					if (process instanceof ToolProcess) {
						tool= (ToolProcess)process;
					}
				}
				if (tool == null && element instanceof ILaunch) {
					final IProcess[] processes= ((ILaunch)element).getProcesses();
					for (int i= 0; i < processes.length; i++) {
						if (processes[i] instanceof ToolProcess) {
							tool= (ToolProcess)processes[i];
							break;
						}
					}
				}
				if (tool != null) {
					final var console= NicoUITools.getConsole(tool);
					if (console != null) {
						showConsole(console, false);
					}
				}
			}
		}
	}
	
	@Override
	public void handleDebugEvents(final @NonNull DebugEvent[] events) {
		final ToolProcess tool= this.activeProcess;
		if (tool == null) {
			return;
		}
		for (final DebugEvent event : events) {
			if (event.getSource() == tool && event.getKind() == DebugEvent.TERMINATE) {
				this.terminatedJob.scheduleTerminated(tool);
			}
		}
	}
	
	
	void addListener(final WorkbenchToolRegistryListener listener) {
		this.listeners.add(listener);
		if (this.isClosed) {
			this.listeners.clear();
		}
	}
	
	void removeListener(final WorkbenchToolRegistryListener listener) {
		this.listeners.remove(listener);
	}
	
	
	public IWorkbenchPage getPage() {
		return this.page;
	}
	
	public synchronized @Nullable ToolProcess getActiveProcess() {
		return this.activeProcess;
	}
	
	public synchronized WorkbenchToolSessionData createSessionInfo() {
		return new WorkbenchToolSessionData(this.activeProcess, this.activeConsole, this.page, null);
	}
	
	public @Nullable IConsoleView getConsoleView(final NIConsole console) {
		if (this.isClosed) {
			return null;
		}
		return searchView(console);
	}
	
	void handleConsolesRemoved(final List<ToolProcess> tools) {
		this.consoleUpdateJob.scheduleRemoved(tools);
	}
	
	void handleActiveConsoleChanged(final NIConsole console, final @Nullable IViewPart source) {
		this.consoleUpdateJob.scheduleActivated(nonNullAssert(console), source);
	}
	
	void showConsole(final NIConsole console, final boolean activate) {
		this.showConsoleViewJob.schedule(nonNullAssert(console), activate);
	}
	
	void showConsoleExplicitly(final NIConsole console, final boolean pin) {
		this.showConsoleViewJob.cancel();
		new ShowConsoleViewJob(0) {
			@Override
			protected void finish(final IConsoleView view) {
				if (pin) {
					view.setPinned(true);
				}
			}
		}.schedule(console, true);
	}
	
	
	private void notifyActiveToolSessionChanged(final @Nullable IViewPart source) {
		final var sessionData= new WorkbenchToolSessionData(this.activeProcess, this.activeConsole,
				this.page, source );
		if (ToolRegistry.DEBUG) {
			System.out.println("[tool registry] tool session activated: " + sessionData.toString());
		}
		
		final ActiveToolEvent toolEvent= new ActiveToolEvent(ActiveToolEvent.TOOL_ACTIVATED,
				sessionData.getTool() );
		for (final var listener : this.listeners) {
			try {
				if (listener instanceof WorkbenchToolRegistryListener) {
					((WorkbenchToolRegistryListener)listener).toolSessionActivated(sessionData);
				}
				else if (listener instanceof ActiveToolListener) {
					((ActiveToolListener)listener).onToolChanged(toolEvent);
				}
			}
			catch (final Exception e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, NicoUI.BUNDLE_ID, -1,
						"An error occurred when handling tool activation.", e ));
			}
		}
	}
	
	private void notifyToolTerminated() {
		final var sessionData= new WorkbenchToolSessionData(this.activeProcess, this.activeConsole, 
				this.page, null );
		if (ToolRegistry.DEBUG) {
			System.out.println("[tool registry] activate tool terminated: " + sessionData.toString());
		}
		
		final ActiveToolEvent toolEvent= new ActiveToolEvent(ActiveToolEvent.TOOL_TERMINATED,
				sessionData.getTool() );
		for (final var listener : this.listeners) {
			try {
				if (listener instanceof WorkbenchToolRegistryListener) {
					((WorkbenchToolRegistryListener)listener).toolTerminated(sessionData);
				}
				else if (listener instanceof ActiveToolListener) {
					((ActiveToolListener)listener).onToolChanged(toolEvent);
				}
			}
			catch (final Exception e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, NicoUI.BUNDLE_ID, -1,
						"An error occurred when handling tool termination.", e ));
			}
		}
	}
	
	private List<IConsoleView> getConsoleViews() {
		final List<IConsoleView> consoleViews= new ArrayList<>();
		
		final IViewReference[] allReferences= this.page.getViewReferences();
		for (final IViewReference reference : allReferences) {
			if (reference.getId().equals(IConsoleConstants.ID_CONSOLE_VIEW)) {
				final IViewPart view= reference.getView(true);
				if (view != null) {
					final IConsoleView consoleView= (IConsoleView)view;
					if (!consoleView.isPinned()) {
						consoleViews.add(consoleView);
					}
					else if (consoleView.getConsole() instanceof NIConsole) {
						consoleViews.add(0, consoleView);
					}
				}
			}
		}
		return consoleViews;
	}
	
	/**
	 * Searches best next console (tool)
	 * 
	 * Must be called only in UI thread
	 */
	private @Nullable NIConsole searchConsole(final List<ToolProcess> exclude) {
		// Search NIConsole in
		// 1. active part
		// 2. visible part
		// 3. all
		NIConsole nico= null;
		final IWorkbenchPart part= this.page.getActivePart();
		if (part instanceof IConsoleView) {
			final IConsole console= ((IConsoleView)part).getConsole();
			if (console instanceof NIConsole && !exclude.contains((nico= (NIConsole)console).getProcess())) {
				return nico;
			}
		}
		
		final List<IConsoleView> consoleViews= getConsoleViews();
		NIConsole secondChoice= null;
		for (final IConsoleView view : consoleViews) {
			final IConsole console= view.getConsole();
			if (console instanceof NIConsole && !exclude.contains((nico= (NIConsole)console).getProcess())) {
				if (this.page.isPartVisible(view)) {
					return nico;
				}
				else if (secondChoice == null) {
					secondChoice= nico;
				}
			}
		}
		return secondChoice;
	}
	
	/**
	 * Searches the best console view for the specified console (tool)
	 * 
	 * @param console
	 * @return 
	 */
	private @Nullable IConsoleView searchView(final NIConsole console) {
		// Search the console view
		final List<IConsoleView> views= getConsoleViews();
		
		final IConsoleView[] preferedView= new IConsoleView[10];
		for (final IConsoleView view : views) {
			final IConsole consoleInView= view.getConsole();
			if (consoleInView == console) {
				if (this.page.isPartVisible(view)) { // already visible
					preferedView[view.isPinned() ? 0 : 1]= view;
					continue;
				}
				else {								// already selected
					preferedView[view.isPinned() ? 2 : 3]= view;
					continue;
				}
			}
			if (consoleInView == null) {
				if (this.page.isPartVisible(view)) {
					preferedView[4]= view;
					continue;
				}
				else {
					preferedView[5]= view;
					continue;
				}
			}
			if (!view.isPinned()) {					// for same type created view
				final String secId= view.getViewSite().getSecondaryId();
				if (secId != null && secId.startsWith(console.getType())) {
					preferedView[6]= view;
					continue;
				}
				if (this.page.isPartVisible(view)) { // visible views
					preferedView[7]= view;
					continue;
				}
				else {								// other views
					preferedView[8]= view;
					continue;
				}
			}
		}
		for (int i= 0; i < preferedView.length; i++) {
			if (preferedView[i] != null) {
				return preferedView[i];
			}
		}
		return null;
	}
	
	
	@Override
	public @Nullable Tool getTool() {
		return this.activeProcess;
	}
	
	@Override
	public void addToolListener(final ActiveToolListener listener) {
		this.listeners.add(nonNullAssert(listener));
		if (this.isClosed) {
			this.listeners.clear();
		}
	}
	
	@Override
	public void removeToolListener(final ActiveToolListener listener) {
		this.listeners.remove(listener);
	}
	
}
