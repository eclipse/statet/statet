/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui;

import static org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolRegistry.ACTIVE_TOOL_SOURCE_NAME;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.ui.AbstractSourceProvider;
import org.eclipse.ui.ISources;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolRegistryListener;
import org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolSessionData;


public class ToolSourceProvider extends AbstractSourceProvider implements IWindowListener {
	
	
	private class RegistryListerner implements WorkbenchToolRegistryListener {
		
		private final IWorkbenchWindow window;
		
		public RegistryListerner(final IWorkbenchWindow window) {
			this.window= window;
		}
		
		@Override
		public void toolSessionActivated(final WorkbenchToolSessionData info) {
			if (ToolSourceProvider.this.activeWindow == this.window) {
				handleActivated(info.getTool());
			}
		}
		
		@Override
		public void toolTerminated(final WorkbenchToolSessionData sessionData) {
			if (ToolSourceProvider.this.activeWindow == this.window) {
				handleTerminated(sessionData.getTool());
			}
		}
	}
	
	
	private final ToolRegistry registry;
	private final List<RegistryListerner> createdListeners;
	private IWorkbenchWindow activeWindow;
	private Tool currentTool;
	
	
	public ToolSourceProvider() {
		this.createdListeners= new ArrayList<>();
		this.registry= NicoUIPlugin.getInstance().getToolRegistry();
		PlatformUI.getWorkbench().addWindowListener(this);
		for (final IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows()) {
			windowOpened(window);
		}
	}
	
	
	@Override
	public void dispose() {
		synchronized (this.createdListeners) {
			final Iterator<RegistryListerner> iter= this.createdListeners.iterator();
			while (iter.hasNext()) {
				this.registry.removeListener(iter.next());
				iter.remove();
			}
			this.createdListeners.clear();
		}
	}
	
	
	@Override
	public void windowOpened(final IWorkbenchWindow window) {
		final RegistryListerner listener= new RegistryListerner(window);
		synchronized (this.createdListeners) {
			this.createdListeners.add(listener);
		}
		this.registry.addListener(listener, window.getActivePage());
	}
	
	@Override
	public void windowClosed(final IWorkbenchWindow window) {
		synchronized (this.createdListeners) {
			final Iterator<RegistryListerner> iter= this.createdListeners.iterator();
			while (iter.hasNext()) {
				if (iter.next().window == window) {
					iter.remove();
				}
			}
		}
		this.activeWindow= null;
	}
	
	@Override
	public void windowActivated(final IWorkbenchWindow window) {
		this.activeWindow= window;
		handleActivated(this.registry.getActiveToolSession(window.getActivePage()).getTool());
	}
	
	@Override
	public void windowDeactivated(final IWorkbenchWindow window) {
	}
	
	
	private void handleActivated(final Tool tool) {
		synchronized (this) {
			if (this.currentTool == tool || (this.currentTool == null && tool == null)) {
				return;
			}
			this.currentTool= tool;
		}
		if (DEBUG) {
			System.out.println("[tool source] changed:" + (tool != null ? tool.getLabel(Tool.LONG_LABEL) : "-"));
		}
		final Object value= (tool != null) ? tool : IEvaluationContext.UNDEFINED_VARIABLE;
		fireSourceChanged(ISources.WORKBENCH, ACTIVE_TOOL_SOURCE_NAME, value);
	}
	
	private void handleTerminated(final Tool tool) {
		synchronized (this) {
			if (this.currentTool == tool) {
				if (DEBUG) {
					System.out.println("[tool source] terminated:" + (tool != null ? tool.getLabel(Tool.LONG_LABEL) : "-"));
				}
				fireSourceChanged(ISources.WORKBENCH, ACTIVE_TOOL_SOURCE_NAME, tool);
			}
		}
	}
	
	@Override
	public String[] getProvidedSourceNames() {
		return new String[] { ACTIVE_TOOL_SOURCE_NAME };
	}
	
	@Override
	public Map getCurrentState() {
		final Map<String, Object> map= new HashMap<>();
		Object tool= null;
		if (this.activeWindow != null) {
			tool= this.registry.getActiveToolSession(this.activeWindow.getActivePage()).getTool();
		}
		if (tool == null) {
			tool= IEvaluationContext.UNDEFINED_VARIABLE;
		}
		map.put(ACTIVE_TOOL_SOURCE_NAME, tool);
		return map;
	}
	
}
