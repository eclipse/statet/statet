/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI;
import org.eclipse.statet.ecommons.ui.actions.AbstractScopeHandler;
import org.eclipse.statet.ecommons.ui.actions.WorkbenchScopingHandler;

import org.eclipse.statet.internal.nico.ui.Messages;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.nico.ui.actions.AbstractToolScopeHandler;
import org.eclipse.statet.nico.ui.console.NIConsole;


@NonNullByDefault
public class TerminateToolHandler extends AbstractToolScopeHandler<ToolProcess> {
	
	
	public TerminateToolHandler(final IWorkbenchWindow scope,
			final @Nullable String commandId) {
		super(scope, commandId);
	}
	
	public TerminateToolHandler(final IPageSite scope, final ToolProvider toolProvider,
			final @Nullable String commandId) {
		super(scope, toolProvider, commandId);
	}
	
	
	@Override
	protected boolean isSupported(final Tool tool) {
		return (tool instanceof ToolProcess);
	}
	
	@Override
	protected boolean evaluateIsEnabled(final ToolProcess tool) {
		return (tool.canTerminate());
	}
	
	
	@Override
	protected @Nullable Object execute(final ExecutionEvent event,
			final ToolProcess tool, final IEvaluationContext evalContext)
			throws ExecutionException {
		try {
			tool.terminate();
			
			final IWorkbenchPage page= getWorkbenchWindow().getActivePage();
			final IConsole console= DebugUITools.getConsole(tool);
			if (console instanceof NIConsole) {
				NicoUITools.showConsole((NIConsole)console, page, true);
			}
		}
		catch (final DebugException e) {
			final int severity= e.getStatus().getSeverity();
			StatusManager.getManager().handle(new Status(severity, NicoUI.BUNDLE_ID, -1, Messages.TerminateToolAction_error_message, e),
					(severity >= IStatus.ERROR) ? StatusManager.LOG | StatusManager.SHOW : StatusManager.LOG);
		}
		return null;
	}
	
	
	public static class WorkbenchHandler extends WorkbenchScopingHandler {
		
		
		/** For instantiation via plugin.xml */
		public WorkbenchHandler() {
			super(ECommonsDebugUI.TERMINATE_COMMAND_ID);
		}
		
		
		@Override
		protected AbstractScopeHandler createScopeHandler(final Object scope) {
			return new TerminateToolHandler((IWorkbenchWindow)scope, getCommandId());
		}
		
		
	}
	
}
