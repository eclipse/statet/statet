/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.preferences;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.util.FixDocumentPartitioner;

import org.eclipse.statet.nico.ui.NicoUIPreferences;


@NonNullByDefault
class ConsoleTextStylesPreviewPartitioner extends FixDocumentPartitioner {
	
	
	public static final String PARTITIONING= "org.eclipse.statet.ConsolePreview";
	
	
	public static final ImList<String> CONTENT_TYPES= ImCollections.newList(
			NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY,
			NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY,
			NicoUIPreferences.OUTPUT_STD_OUTPUT_ROOT_KEY,
			NicoUIPreferences.OUTPUT_STD_ERROR_STREAM_ROOT_KEY,
			NicoUIPreferences.OUTPUT_SYSTEM_OUTPUT_STREAM_ROOT_KEY );
	
	
	public ConsoleTextStylesPreviewPartitioner() {
		super(CONTENT_TYPES);
		append(NicoUIPreferences.OUTPUT_STD_OUTPUT_ROOT_KEY, 23 + 22 + 40 + 1);
		append(NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, 2);
		append(NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, 17-2);
		append(NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, 2);
		append(NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, 4-2);
		append(NicoUIPreferences.OUTPUT_STD_OUTPUT_ROOT_KEY, 65 + 65);
		append(NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, 2);
		append(NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, 13-2);
		append(NicoUIPreferences.OUTPUT_STD_OUTPUT_ROOT_KEY, 49 + 48);
		append(NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, 2);
		append(NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, 5-2);
		append(NicoUIPreferences.OUTPUT_STD_ERROR_STREAM_ROOT_KEY, 29);
		append(NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, 2);
		append(NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, 3-2);
		append(NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, 2);
		append(NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, 15-2);
		append(NicoUIPreferences.OUTPUT_SYSTEM_OUTPUT_STREAM_ROOT_KEY, 41 + 41 + 11);
		append(NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, 2);
		append(NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, 6-2);
		append(NicoUIPreferences.OUTPUT_INFO_STREAM_ROOT_KEY, 31);
		append(NicoUIPreferences.OUTPUT_STD_INPUT_STREAM_ROOT_KEY, 33-31);
	}
	
}
