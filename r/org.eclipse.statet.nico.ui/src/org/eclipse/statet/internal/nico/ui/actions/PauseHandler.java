/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.actions.AbstractScopeHandler;
import org.eclipse.statet.ecommons.ui.actions.WorkbenchScopingHandler;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.core.runtime.Queue;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.actions.AbstractToolScopeHandler;


/**
 * Handler to toggle pause state of engine
 */
@NonNullByDefault
public class PauseHandler extends AbstractToolScopeHandler<ToolProcess>
		implements IElementUpdater, IDebugEventSetListener {
	
	
	private boolean isChecked;
	
	
	public PauseHandler(final IWorkbenchWindow scope,
			final @Nullable String commandId) {
		super(scope, commandId);
		
		DebugPlugin.getDefault().addDebugEventListener(this);
	}
	
	@Override
	public void dispose() {
		final DebugPlugin debugManager= DebugPlugin.getDefault();
		if (debugManager != null) {
			debugManager.removeDebugEventListener(this);
		}
		
		super.dispose();
	}
	
	
	private boolean setChecked(final boolean isChecked) {
		if (isChecked != this.isChecked) {
			this.isChecked= isChecked;
			return true;
		}
		return false;
	}
	
	@Override
	protected boolean updateState(final @Nullable ToolProcess tool) {
		boolean refresh= false;
		refresh|= setChecked(tool != null && tool.getQueue().isRequested(Queue.PAUSED_STATE));
		
		refresh|= super.updateState(tool);
		return refresh;
	}
	
	protected void updateChecked(final boolean isChecked) {
		if (setChecked(isChecked)) {
			refreshCommandElements();
		}
	}
	
	private void updateChecked(final ToolProcess tool, final boolean isChecked) {
		UIAccess.getDisplay().asyncExec(() -> {
			if (getCurrentTool(null) != tool) {
				return;
			}
			updateChecked(isChecked);
		});
	}
	
	
	@Override
	public void handleDebugEvents(final @NonNull DebugEvent[] events) {
		final var tool= getCurrentTool(null);
		if (tool == null) {
			return;
		}
		Boolean paused= null;
		ITER_EVENTS: for (final DebugEvent event : events) {
			if (event.getSource() == tool.getQueue()) {
				if (Queue.isStateRequest(event)) {
					final Queue.StateDelta delta= (Queue.StateDelta) event.getData();
					if (delta.newState == Queue.PAUSED_STATE) {
						paused= true;
					}
					else {
						paused= false;
					}
				}
				else if (Queue.isStateChange(event)) {
					final Queue.StateDelta delta= (Queue.StateDelta) event.getData();
					if (delta.newState == Queue.PAUSED_STATE) {
						paused= true;
					}
				}
				continue ITER_EVENTS;
			}
		}
		if (paused != null) {
			updateChecked(tool, paused);
		}
	}
	
	
	@Override
	public void updateCommandElement(final UIElement element, final Map<String, ?> parameters) {
		element.setChecked(this.isChecked);
	}
	
	
	@Override
	protected @Nullable Object execute(final ExecutionEvent event,
			final ToolProcess tool, final IEvaluationContext evalContext) {
		boolean isPaused= tool.getQueue().isRequested(Queue.PAUSED_STATE);
		final boolean success= (isPaused) ?
				tool.getQueue().resume() :
				tool.getQueue().pause();
		isPaused^= success;
		updateChecked(success && isPaused);
		
		return null;
	}
	
	
	public static class WorkbenchHandler extends WorkbenchScopingHandler
			implements IElementUpdater {
		
		
		/** For instantiation via plugin.xml */
		public WorkbenchHandler() {
			super(NicoUI.PAUSE_COMMAND_ID);
		}
		
		
		@Override
		protected AbstractScopeHandler createScopeHandler(final Object scope) {
			return new PauseHandler((IWorkbenchWindow)scope, getCommandId());
		}
		
		
	}
	
}
