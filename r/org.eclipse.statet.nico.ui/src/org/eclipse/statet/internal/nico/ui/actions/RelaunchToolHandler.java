/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.nico.ui.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandImageService;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.progress.IProgressConstants;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.debug.core.util.OverlayLaunchConfiguration;
import org.eclipse.statet.ecommons.ui.actions.AbstractScopeHandler;
import org.eclipse.statet.ecommons.ui.actions.WorkbenchScopingHandler;

import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;
import org.eclipse.statet.nico.ui.actions.AbstractToolScopeHandler;


@NonNullByDefault
public class RelaunchToolHandler extends AbstractToolScopeHandler<ToolProcess> {
	
	
	private static class RelaunchJob extends Job {
		
		
		private final ToolProcess process;
		
		
		public RelaunchJob(final ToolProcess process) {
			super(NLS.bind("Relaunch {0}", process.getLabel(ToolProcess.DEFAULT_LABEL)));
			setUser(false);
			setPriority(SHORT);
			
			final var commandImageService= nonNullAssert(PlatformUI.getWorkbench().getService(ICommandImageService.class));
			final var imageDescriptor= commandImageService.getImageDescriptor(NicoUI.RELAUNCH_TOOL_COMMAND_ID);
			if (imageDescriptor != null) {
				setProperty(IProgressConstants.ICON_PROPERTY, imageDescriptor);
			}
			
			this.process= process;
		}
		
		
		@Override
		public boolean belongsTo(final @Nullable Object family) {
			return (family == RelaunchJob.class);
		}
		
		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			final SubMonitor m= SubMonitor.convert(monitor, 3 + 10);
			try {
				m.worked(1);
				
				if (!this.process.isTerminated()) {
					m.setBlocked(new Status(IStatus.INFO, NicoUI.BUNDLE_ID,
							"Waiting for termination..." ));
					try {
						while (!this.process.isTerminated()) {
							try {
								Thread.sleep(100);
								
								if (m.isCanceled()) {
									return Status.CANCEL_STATUS;
								}
							}
							catch (final InterruptedException e) {
							}
						}
					}
					finally {
						m.clearBlocked();
					}
				}
				m.worked(1);
				
				final IFileStore workspaceDir= this.process.getWorkspace().getWorkspaceDir();
				
				final var originalLaunch= this.process.getLaunch();
				ILaunchConfiguration originalConfig= originalLaunch.getLaunchConfiguration();
				if (originalConfig instanceof OverlayLaunchConfiguration) {
					originalConfig= ((OverlayLaunchConfiguration)originalConfig).getOriginal();
				}
				
				final Map<String, Object> add= new HashMap<>();
				if (workspaceDir != null && workspaceDir.fetchInfo().exists()) {
					add.put("org.eclipse.statet.r/renv/WorkingDirectory", workspaceDir.toString());
				}
				m.worked(1);
				if (m.isCanceled()) {
					return Status.CANCEL_STATUS;
				}
				
				final ILaunchConfiguration relaunchConfig= new OverlayLaunchConfiguration(originalConfig, add);
				@SuppressWarnings("unused")
				final ILaunch relaunchLaunch= relaunchConfig.launch(
							originalLaunch.getLaunchMode(), m.newChild(10), false );
				
				return Status.OK_STATUS;
			}
			catch (final CoreException e) {
				return e.getStatus();
			}
			finally {
				m.done();
			}
		}
		
	}
	
	
	public RelaunchToolHandler(final IWorkbenchWindow scope,
			final @Nullable String commandId) {
		super(scope, commandId);
	}
	
	public RelaunchToolHandler(final IPageSite scope, final ToolProvider toolProvider,
			final @Nullable String commandId) {
		super(scope, toolProvider, commandId);
	}
	
	
	@Override
	protected boolean isSupported(final Tool tool) {
		return (tool instanceof ToolProcess);
	}
	
	@Override
	protected boolean evaluateIsEnabled(final ToolProcess tool) {
		return true;
	}
	
	
	@Override
	protected @Nullable Object execute(final ExecutionEvent event,
			final ToolProcess tool, final IEvaluationContext evalContext)
			throws ExecutionException {
		try {
			tool.terminate();
			
			for (final Job job : Job.getJobManager().find(RelaunchJob.class)) {
				if (((RelaunchJob)job).process == tool) {
					return null;
				}
			}
			new RelaunchJob(tool).schedule();
		}
		catch (final DebugException e) {
		}
		return null;
	}
	
	
	public static class WorkbenchHandler extends WorkbenchScopingHandler {
		
		
		/** For instantiation via plugin.xml */
		public WorkbenchHandler() {
			super(NicoUI.RELAUNCH_TOOL_COMMAND_ID);
		}
		
		
		@Override
		protected AbstractScopeHandler createScopeHandler(final Object scope) {
			return new RelaunchToolHandler((IWorkbenchWindow)scope, getCommandId());
		}
		
		
	}
	
}
