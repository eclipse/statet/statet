/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.ecommons.ui.actions.SimpleContributionItem;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.nico.core.NicoCore;
import org.eclipse.statet.nico.core.runtime.ITrack;
import org.eclipse.statet.nico.core.runtime.ToolProcess;


/**
 * Contribution item creating menu items to open track files.
 */
public class OpenTrackingFilesContributionItem extends CompoundContributionItem {
	
	
	private static class RefreshFileJob extends Job {
		
		
		private final IFile fFile;
		
		
		public RefreshFileJob(final String name, final IFile file) {
			super("Refresh track file of '"+name+"'");
			
			fFile = file;
			
			setSystem(true);
			setUser(false);
			final IResourceRuleFactory ruleFactory = file.getWorkspace().getRuleFactory();
			setRule(ruleFactory.refreshRule(file));
		}
		
		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			try {
				fFile.refreshLocal(IResource.DEPTH_ONE, monitor);
			}
			catch (final CoreException e) {
			}
			return Status.OK_STATUS;
		}
		
	}
	
	public static void open(final String name, final IFileStore fileStore) {
		try {
			final IFile workspaceFile = FileUtil.getAsWorkspaceFile(fileStore.toURI());
			if (workspaceFile != null) {
				new RefreshFileJob(name, workspaceFile).schedule();
				try {
					Thread.sleep(50);
				}
				catch (final InterruptedException e) {}
				IDE.openEditor(UIAccess.getActiveWorkbenchPage(true), workspaceFile);
			}
			else {
				IDE.openEditorOnFileStore(UIAccess.getActiveWorkbenchPage(true), fileStore);
			}
		}
		catch (final PartInitException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, NicoCore.BUNDLE_ID, -1,
					"An error occurred when opening the editor on the track file of "+name+".", e));
		}
	}
	
	
	private final ToolProcess fTool;
	
	
	public OpenTrackingFilesContributionItem(final ToolProcess tool) {
		if (tool == null) {
			throw new NullPointerException();
		}
		fTool = tool;
	}
	
	
	@Override
	protected IContributionItem[] getContributionItems() {
		final List<? extends ITrack> tracks = fTool.getTracks();
		final List<IContributionItem> items= new ArrayList<>(tracks.size());
		for (int i = 0; i < tracks.size(); i++) {
			final ITrack track = tracks.get(i);
			final IFileStore file = track.getFile();
			if (file != null) {
				final SimpleContributionItem item = new SimpleContributionItem(
						track.getName(), null) {
					@Override
					protected void execute() throws ExecutionException {
						track.flush();
						open(track.getName(), track.getFile());
					}
				};
				items.add(item);
			}
		}
		return items.toArray(new IContributionItem[items.size()]);
	}
}
