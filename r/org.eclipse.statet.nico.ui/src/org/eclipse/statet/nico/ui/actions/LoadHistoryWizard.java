/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;

import org.eclipse.statet.internal.nico.ui.LoadHistoryPage;
import org.eclipse.statet.internal.nico.ui.NicoUIPlugin;
import org.eclipse.statet.nico.core.runtime.History;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUIMessages;


/**
 * 
 */
public class LoadHistoryWizard extends Wizard {
	
	
	static final String STORE_SECTION = "tools/LoadSaveHistoryWizard"; //$NON-NLS-1$ shared with save
	
	
	private final ToolProcess fProcess;
	private LoadHistoryPage fPage;
	
	
	public LoadHistoryWizard(final ToolProcess process) {
		super();
		
		this.fProcess = process;
		
		setDialogSettings(DialogUtils.getDialogSettings(NicoUIPlugin.getInstance(), STORE_SECTION));
		setWindowTitle(NicoUIMessages.LoadHistory_title);
//		setDefaultPageImageDescriptor();
		setNeedsProgressMonitor(true);
	}
	
	
	@Override
	public void addPages() {
		this.fPage = new LoadHistoryPage(this.fProcess);
		addPage(this.fPage);
	}
	
	@Override
	public boolean performFinish() {
		this.fPage.saveSettings();
		
		try {
			final History history = this.fProcess.getHistory();
			final Object file = this.fPage.getFile();
			final var charset= this.fPage.getCharset();
			
			assert (history != null);
			assert (file != null);
			
			getContainer().run(true, true, new IRunnableWithProgress() {
				@Override
				public void run(final IProgressMonitor monitor) throws InvocationTargetException {
					final Status status= history.load(file, charset, false,
							EStatusUtils.convert(monitor) );
					if (status.getSeverity() == Status.ERROR) {
						throw new InvocationTargetException(
								new CoreException(EStatusUtils.convert(status)) );
					}
				}
			});
			return true;
		}
		catch (final OperationCanceledException e) {
			return false;
		}
		catch (final Exception e) {
			if (e instanceof InvocationTargetException) {
				final Throwable cause = ((InvocationTargetException) e).getTargetException();
				if (cause instanceof CoreException) {
					StatusManager.getManager().handle(((CoreException) cause).getStatus(),
						StatusManager.LOG | StatusManager.SHOW);
					return false;
				}
			}
			NicoUIPlugin.logError(NicoUIPlugin.INTERNAL_ERROR, "Error of unexpected type occured, when performing load history.", e); //$NON-NLS-1$
			return false;
		}
	}
	
}
