/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.nico.core.runtime.ToolProcess;


/**
 * A dialog that has a title area for displaying a title and an image as well as a common area for
 * displaying a description, a message, or an error message (top) and a tool information area
 * (bottom).
 */
public abstract class ToolDialog extends TitleAreaDialog {
	
	
	private final String dialogTitle;
	private final Image dialogImage;
	
	private final ToolProcess tool;
	
	private final int toolInfoFlags;
	
	
	public ToolDialog(final ToolProcess tool, final Shell parentShell,
			final Image dialogImage, final String dialogTitle) {
		this(tool, parentShell, dialogImage, dialogTitle, 0);
	}
	
	public ToolDialog(final ToolProcess tool, final Shell parentShell,
			final Image dialogImage, final String dialogTitle, final int toolInfoFlags) {
		super(parentShell);
		
		this.dialogImage= dialogImage;
		this.dialogTitle= dialogTitle;
		
		this.tool= tool;
		this.toolInfoFlags= toolInfoFlags;
	}
	
	
	@Override
	protected boolean isResizable() {
		return true;
	}
	
	@Override
	protected void configureShell(final Shell shell) {
		super.configureShell(shell);
		if (this.dialogImage != null) {
			shell.setImage(this.dialogImage);
		}
		if (this.dialogTitle != null) {
			shell.setText(this.dialogTitle);
		}
	}
	
	protected ToolProcess getTool() {
		return this.tool;
	}
	
	@Override
	protected IDialogSettings getDialogBoundsSettings() {
		return getDialogSettings();
	}
	
	
	protected IDialogSettings getDialogSettings() {
		return null;
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite dialogArea= (Composite) super.createDialogArea(parent);
		
		final Composite composite= new Composite(dialogArea, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(LayoutUtils.newDialogGrid(1));
		
		final Control content= createDialogContent(composite);
		content.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		addToolInfo(composite);
		
		return dialogArea;
	}
	
	protected abstract Control createDialogContent(Composite parent);
	
	protected void addToolInfo(final Composite parent) {
		LayoutUtils.addSmallFiller(parent, false);
		
		final ToolInfoGroup info= new ToolInfoGroup(parent, this.toolInfoFlags, this.tool);
		info.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
	}
	
}
