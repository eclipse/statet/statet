/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.nico.core.runtime.ToolProcess;


public class ToolMessageDialog extends MessageDialog {
	
	
	public static boolean openConfirm(final ToolProcess tool, final Shell parent, final String title, final String message,
			final String okLabel) {
		final ToolMessageDialog dialog= new ToolMessageDialog(tool,
				parent, title, null,
				message, QUESTION,
				new String[] { okLabel, IDialogConstants.CANCEL_LABEL }, 0 );
		return (dialog.open() == 0);
	}
	
	public static boolean openQuestion(final ToolProcess tool, final Shell parent, final String title, final String message) {
		final ToolMessageDialog dialog= new ToolMessageDialog(tool,
				parent, title, null,
				message, QUESTION,
				new String[] { IDialogConstants.YES_LABEL, IDialogConstants.NO_LABEL }, 0 );
		return (dialog.open() == 0);
	}
	
	
	private final ToolProcess tool;
	
	
	/**
	 * @see MessageDialog
	 */
	public ToolMessageDialog(final ToolProcess tool, final Shell parentShell, 
			final String dialogTitle, final Image dialogTitleImage,
			final String dialogMessage, final int dialogImageType,
			final String[] dialogButtonLabels, final int defaultIndex) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, dialogButtonLabels, defaultIndex);
		this.tool= tool;
	}
	
	
	protected ToolProcess getTool() {
		return this.tool;
	}
	
	@Override
	protected Control createCustomArea(final Composite parent) {
		LayoutUtils.addSmallFiller(parent, true);
		
		final ToolInfoGroup info= new ToolInfoGroup(parent, this.tool);
		info.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		applyDialogFont(parent);
		return parent;
	}
	
}
