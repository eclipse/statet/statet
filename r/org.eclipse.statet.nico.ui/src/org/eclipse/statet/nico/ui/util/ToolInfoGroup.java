/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ViewForm;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.ecommons.ui.components.ShortedLabel;

import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUITools;


/**
 * Control group showing information about a NICO tool.
 */
public class ToolInfoGroup {
	
	
	public static int WIDE=                                0x00000001;
	
	
	private final ToolProcess process;
	
	private ViewForm form;
	
	private final int flags;
	
	
	public ToolInfoGroup(final Composite parent, final ToolProcess process) {
		this(parent, 0, process);
	}
	
	public ToolInfoGroup(final Composite parent, final int flags, final ToolProcess process) {
		this.process= process;
		this.flags= flags;
		createControls(parent);
	}
	
	
	private void createControls(final Composite parent) {
		this.form= new ViewForm(parent, SWT.BORDER | SWT.FLAT);
		final Composite info= new Composite(this.form, SWT.NONE);
		final GridLayout layout= new GridLayout();
		layout.numColumns= ((this.flags & WIDE) != 0) ? 3 : 2;
		layout.verticalSpacing= ((this.flags & WIDE) != 0) ? 1 : 2;
		info.setLayout(layout);
		this.form.setContent(info);
		
		final Label text= new Label(info, SWT.NONE);
		final Image image= NicoUITools.getImage(this.process);
		if (image != null) {
			text.setImage(image);
		}
		else {
			text.setText("(i)"); //$NON-NLS-1$
		}
		final GridData gd= new GridData(SWT.TOP, SWT.LEFT, false, false);
		gd.horizontalSpan= 1;
		gd.verticalSpan= 2;
		text.setLayoutData(gd);
		
		final ShortedLabel detail1= new ShortedLabel(info, SWT.NONE);
		detail1.setText(this.process.getLabel(Tool.LONG_LABEL));
		detail1.getControl().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		final ShortedLabel detail2= new ShortedLabel(info, SWT.NONE);
		final String wd= FileUtil.toString(this.process.getWorkspace().getWorkspaceDir());
		detail2.setText((wd != null) ? (" ∙  " + wd) : "                                                  ");
		detail2.getControl().setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
	}
	
	public Control getControl() {
		return this.form;
	}
	
}
