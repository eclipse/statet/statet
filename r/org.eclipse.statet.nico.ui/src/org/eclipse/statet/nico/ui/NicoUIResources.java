/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.UIResources;

import org.eclipse.statet.internal.nico.ui.NicoUIPlugin;


@NonNullByDefault
public class NicoUIResources extends UIResources {
	
	
	private static final String NS= "org.eclipse.statet.nico"; //$NON-NLS-1$
	
	
	public static final String OBJ_TASK_CONSOLECOMMAND_IMAGE_ID= NS + "/images/obj/task.consolecommand"; //$NON-NLS-1$
	public static final String OBJ_TASK_DUMMY_IMAGE_ID=         NS + "/images/obj/task.commanddummy"; //$NON-NLS-1$
	
	public static final String OBJ_CONSOLECOMMAND_IMAGE_ID=     NS + "/images/obj/consolecommand"; //$NON-NLS-1$
	
	
	static final NicoUIResources INSTANCE= new NicoUIResources();
	
	
	private NicoUIResources() {
		super(NicoUIPlugin.getInstance().getImageRegistry());
	}
	
}
