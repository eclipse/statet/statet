/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.ui.dialogs.ExtStatusDialog;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.nico.core.util.TrackingConfiguration;


public class TrackingConfigurationDialog extends ExtStatusDialog {
	
	
	private final TrackingConfiguration config;
	
	private TrackingConfigurationComposite configComposite;
	
	
	public TrackingConfigurationDialog(final Shell parent, final TrackingConfiguration config, final boolean isNew) {
		super(parent, (isNew) ? WITH_DATABINDING_CONTEXT :
				(WITH_DATABINDING_CONTEXT | SHOW_INITIAL_STATUS) );
		setTitle(isNew ? "New Tracking Configuration" : "Edit Tracking Configuration");
		
		this.config= config;
	}
	
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite area= new Composite(parent, SWT.NONE);
		area.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true));
		area.setLayout(LayoutUtils.newDialogGrid(2));
		
		this.configComposite= createConfigComposite(area);
		this.configComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true));
		
		this.configComposite.setInput(this.config);
		
		applyDialogFont(area);
		
		return area;
	}
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		this.configComposite.addBindings(db);
	}
	
	protected TrackingConfigurationComposite createConfigComposite(final Composite parent) {
		return new TrackingConfigurationComposite(parent);
	}
	
	protected TrackingConfigurationComposite getConfigComposite() {
		return this.configComposite;
	}
	
}
