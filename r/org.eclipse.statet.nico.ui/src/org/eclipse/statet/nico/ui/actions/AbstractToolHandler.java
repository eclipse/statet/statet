/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.actions;

import static org.eclipse.statet.ecommons.ts.ui.workbench.WorkbenchToolRegistry.ACTIVE_TOOL_SOURCE_NAME;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.ActiveToolListener;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


@NonNullByDefault
public abstract class AbstractToolHandler<TTool extends Tool> extends AbstractHandler
		implements ActiveToolListener {
	
	
	protected static final byte S_INITIALIZING=              0;
	protected static final byte S_INITIALIZED=               1;
	protected static final byte S_DISPOSED=                 -1;
	
	
	/** internal state of this handler */
	private byte state;
	
	private final @Nullable String requiredMainType;
	private final @Nullable String requiredFeatureSet;
	
	private final @Nullable ToolProvider toolProvider;
	
	private final @Nullable IServiceLocator serviceLocator;
	
	private @Nullable TTool activeTool;
	
	
	protected AbstractToolHandler(final @Nullable String mainType, final @Nullable String featureSet,
			final @Nullable ToolProvider toolProvider, final @Nullable IServiceLocator serviceLocator) {
		this.requiredMainType= mainType;
		this.requiredFeatureSet= featureSet;
		
		this.toolProvider= toolProvider;
		this.serviceLocator= serviceLocator;
		
		this.state= S_INITIALIZING;
	}
	
	protected AbstractToolHandler(final @Nullable String mainType, final @Nullable String featureSet) {
		this(mainType, featureSet, null, null);
	}
	
	protected AbstractToolHandler(final @Nullable String mainType) {
		this(mainType, null, null, null);
	}
	
	
	protected final int getState() {
		return this.state;
	}
	
	/**
	 * Must be call at the end of the constructor to finish initialization.
	 */
	protected void init() {
		if (this.toolProvider != null) {
			this.toolProvider.addToolListener(this);
		}
		this.state= S_INITIALIZED;
	}
	
	@Override
	public void dispose() {
		if (this.state == S_DISPOSED) {
			return;
		}
		synchronized (this) {
			this.state= S_DISPOSED;
		}
		
		if (this.toolProvider != null) {
			this.toolProvider.removeToolListener(this);
		}
		
		super.dispose();
	}
	
	
	protected @Nullable Tool getTool(final @Nullable Object evaluationContext) {
		if (this.toolProvider != null) {
			return this.toolProvider.getTool();
		}
		return (Tool)HandlerUtil.getVariable(evaluationContext, ACTIVE_TOOL_SOURCE_NAME);
	}
	
	@Override
	public void onToolChanged(final ActiveToolEvent event) {
		if (this.state == S_DISPOSED) {
			return;
		}
		
		setEnabled(null);
	}
	
	
	protected boolean isValid(final @Nullable Tool tool) {
		return (tool != null
				&& (this.requiredMainType == null || tool.getMainType() == this.requiredMainType)
				&& (this.requiredFeatureSet == null || tool.isProvidingFeatureSet(this.requiredFeatureSet)));
	}
	
	
	protected boolean evaluateIsEnabled(final TTool tool, final @Nullable Object evaluationContext) {
		return (!tool.isTerminated());
	}
	
	@Override
	public void setEnabled(final @Nullable Object evaluationContext) {
		final Tool tool= getTool(evaluationContext);
		this.activeTool= isValid(tool) ? (TTool) tool : null;
		setBaseEnabled(this.activeTool != null
				&& evaluateIsEnabled(this.activeTool, evaluationContext) );
	}
	
	protected @Nullable TTool getActiveTool() {
		return this.activeTool;
	}
	
	
	protected IServiceLocator getServiceLocator(final @Nullable Object evaluationContext) {
		if (this.serviceLocator != null) {
			return this.serviceLocator;
		}
		return WorkbenchUIUtils.getServiceLocator(evaluationContext);
	}
	
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final Tool tool= getTool(event.getApplicationContext());
		final TTool execTool;
		if ((tool == this.activeTool || isValid(tool))
				&& evaluateIsEnabled(execTool= (TTool) tool, event.getApplicationContext()) ) {
			return execute(execTool, event);
		}
		return null;
	}
	
	
	protected abstract @Nullable Object execute(TTool tool,
			ExecutionEvent event) throws ExecutionException;
	
}
