/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.part.IPageSite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.ActiveToolListener;
import org.eclipse.statet.jcommons.ts.core.ActiveToolListener.ActiveToolEvent;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;

import org.eclipse.statet.ecommons.ui.actions.AbstractScopeHandler;

import org.eclipse.statet.nico.ui.NicoUI;


@NonNullByDefault
public abstract class AbstractToolScopeHandler<TTool extends Tool> extends AbstractScopeHandler {
	
	
	private final ActiveToolListener toolListener= this::onToolChanged;
	
	private @Nullable ToolProvider toolProvider;
	
	private @Nullable TTool currentTool;
	
	
	protected AbstractToolScopeHandler(final IWorkbenchWindow scope,
			final @Nullable String commandId) {
		super();
		init(scope, null, commandId);
	}
	
	protected AbstractToolScopeHandler(final IPageSite scope, final ToolProvider toolProvider,
			final @Nullable String commandId) {
		super();
		init(scope, toolProvider, commandId);
	}
	
	protected void init(final Object scope, @Nullable ToolProvider toolProvider,
			final @Nullable String commandId) {
		super.init(scope, commandId);
		
		if (toolProvider == null) {
			if (scope instanceof ToolProvider) {
				toolProvider= (ToolProvider)scope;
			}
			else if (scope instanceof IWorkbenchWindow) {
				final var workbenchPage= ((IWorkbenchWindow)scope).getActivePage();
				if (workbenchPage != null) {
					toolProvider= NicoUI.getToolRegistry().getToolProvider(workbenchPage);
				}
			}
		}
		
		if (toolProvider != null) {
			this.toolProvider= toolProvider;
			toolProvider.addToolListener(this.toolListener);
		}
	}
	
	@Override
	public void dispose() {
		final var toolProvider= this.toolProvider;
		if (toolProvider != null) {
			this.toolProvider= null;
			toolProvider.removeToolListener(this.toolListener);
		}
		
		super.dispose();
	}
	
	
	@Override
	protected IWorkbenchWindow getWorkbenchWindow() {
		return nonNullAssert(super.getWorkbenchWindow());
	}
	
	protected @Nullable Tool getTool(final @Nullable IEvaluationContext evalContext) {
		final var toolProvider= this.toolProvider;
		if (toolProvider != null) {
			return toolProvider.getTool();
		}
		
		return null;
	}
	
	private void onToolChanged(final ActiveToolEvent event) {
		if (this.toolProvider != null) {
			setEnabled(null);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected @Nullable TTool getCurrentTool(final @Nullable IEvaluationContext evalContext) {
		@Nullable TTool currentTool= this.currentTool;
		final Tool tool= getTool(evalContext);
		if (tool != currentTool) {
			currentTool= (tool != null && isSupported(tool)) ? (TTool)tool : null;
			this.currentTool= currentTool;
		}
		return currentTool;
	}
	
	protected boolean isSupported(final Tool tool) {
		return true;
	}
	
	
	@Override
	public void setEnabled(final @Nullable IEvaluationContext context) {
		final var currentTool= getCurrentTool(context);
		final boolean refresh= updateState(currentTool);
		if (refresh) {
			refreshCommandElements();
		}
	}
	
	protected boolean updateState(final @Nullable TTool tool) {
		setBaseEnabled(tool != null && evaluateIsEnabled(tool));
		
		return false;
	}
	
	protected boolean evaluateIsEnabled(final TTool tool) {
		return (!tool.isTerminated());
	}
	
	
	@Override
	protected @Nullable Object execute(final ExecutionEvent event, final IEvaluationContext evalContext)
			throws ExecutionException {
		final var currentTool= getCurrentTool(evalContext);
		if (currentTool != null && evaluateIsEnabled(currentTool)) {
			return execute(event, currentTool, evalContext);
		}
		return null;
	}
	
	protected abstract @Nullable Object execute(final ExecutionEvent event,
			final TTool tool, final IEvaluationContext evalContext)
			throws ExecutionException;
	
	
}
