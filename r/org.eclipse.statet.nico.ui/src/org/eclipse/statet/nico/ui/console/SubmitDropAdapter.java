/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.console;

import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;

import org.eclipse.statet.internal.nico.ui.LocalTaskTransfer;
import org.eclipse.statet.internal.nico.ui.LocalTaskTransfer.Data;
import org.eclipse.statet.nico.core.runtime.ToolController;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUITools;


public class SubmitDropAdapter implements DropTargetListener {
	
	
	public static class TaskSet {
		public ToolProcess process;
	}
	
	
	private final NIConsolePage page;
	
	
	public SubmitDropAdapter(final NIConsolePage page) {
		this.page= page;
	}
	
	
	@Override
	public void dragEnter(final DropTargetEvent event) {
		validate(event);
	}
	
	@Override
	public void dragLeave(final DropTargetEvent event) {
	}
	
	@Override
	public void dragOperationChanged(final DropTargetEvent event) {
		validate(event);
	}
	
	@Override
	public void dragOver(final DropTargetEvent event) {
		event.feedback= DND.FEEDBACK_NONE;
	}
	
	@Override
	public void dropAccept(final DropTargetEvent event) {
		validate(event);
	}
	
	@Override
	public void drop(final DropTargetEvent event) {
		if (LocalTaskTransfer.getTransfer().isSupportedType(event.currentDataType)) {
			final LocalTaskTransfer.Data data= (Data) event.data;
			final ToolProcess process= this.page.getConsole().getProcess();
			if (data == null || data.process == null || data.runnables == null
					|| process.isTerminated()) {
				return;
			}
			data.process.getQueue().move(data.runnables, process.getQueue());
			return;
		}
		if (TextTransfer.getInstance().isSupportedType(event.currentDataType)) {
			final String text= (String) event.data;
			final ToolController controller= this.page.getConsole().getProcess().getController();
			
			if (text == null || controller == null) {
				return;
			}
			
			final IRunnableWithProgress runnable= SubmitPasteAction.createRunnable(controller, text);
			NicoUITools.runSubmitInBackground(controller.getTool(), runnable, this.page.getSite().getShell());
			return;
		}
	}
	
	
	private void validate(final DropTargetEvent event) {
		final ToolProcess process= this.page.getConsole().getProcess();
		if (LocalTaskTransfer.getTransfer().isSupportedType(event.currentDataType)) {
			if (( (event.operations & DND.DROP_MOVE) == DND.DROP_MOVE)
					&& process.getMainType().equals(LocalTaskTransfer.getTransfer().getMainType())
					&& !process.isTerminated() ) {
				event.detail= DND.DROP_MOVE;
				return;
			}
		}
		if (TextTransfer.getInstance().isSupportedType(event.currentDataType)) {
			if (( (event.operations & DND.DROP_COPY) == DND.DROP_COPY) 
					&& !this.page.getConsole().getProcess().isTerminated() ) {
				event.detail= DND.DROP_COPY;
				return;
			}
		}
		event.detail= DND.DROP_NONE;
	}
	
}
