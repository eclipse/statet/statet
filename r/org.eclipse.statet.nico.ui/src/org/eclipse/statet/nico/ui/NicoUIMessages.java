/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui;

import org.eclipse.osgi.util.NLS;


public class NicoUIMessages extends NLS {
	
	
	public static String SubmitAction_name;
	public static String PasteSubmitAction_name;
	public static String SubmitTask_name;
	public static String Submit_error_message;
	
	public static String LoadHistory_title;
	public static String LoadHistoryAction_name;
	public static String LoadHistoryAction_tooltip;
	public static String SaveHistory_title;
	public static String SaveHistoryAction_name;
	public static String SaveHistoryAction_tooltip;
	
	
	static {
		NLS.initializeMessages(NicoUIMessages.class.getName(), NicoUIMessages.class);
	}
	private NicoUIMessages() {}
	
}
