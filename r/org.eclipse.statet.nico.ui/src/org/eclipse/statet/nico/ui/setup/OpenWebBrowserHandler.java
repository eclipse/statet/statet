/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.nico.ui.setup;

import static org.eclipse.statet.jcommons.io.UriUtils.FILE_SCHEME;
import static org.eclipse.statet.jcommons.net.CommonsNet.HTTP_SCHEME;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;

import org.eclipse.swt.program.Program;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;

import org.eclipse.statet.jcommons.io.UriUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.CommonsNet;
import org.eclipse.statet.jcommons.net.Port;
import org.eclipse.statet.jcommons.net.UnknownSchemeException;
import org.eclipse.statet.jcommons.net.core.RSAccessClientSession;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.ToolCommandData;
import org.eclipse.statet.jcommons.ts.core.ToolCommandHandler;
import org.eclipse.statet.jcommons.ts.core.ToolService;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.nico.ui.NicoUIPlugin;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUI;


@NonNullByDefault
public class OpenWebBrowserHandler implements ToolCommandHandler {
	
	
	public static final String OPEN_WEB_BROWSER_COMMAND_ID= "common/openWebBrowser"; //$NON-NLS-1$
	
	
	private final boolean tunnelRemote;
	
	
	public OpenWebBrowserHandler(final boolean tunnelRemote) {
		this.tunnelRemote= tunnelRemote;
	}
	
	public OpenWebBrowserHandler() {
		this(false);
	}
	
	
	@Override
	public Status execute(final String id, final ToolService service, final ToolCommandData data,
			final ProgressMonitor m) throws StatusException {
		switch (id) {
		case OPEN_WEB_BROWSER_COMMAND_ID:
			{	final URL urlUrl;
				final var urlString= data.getString("url"); //$NON-NLS-1$
				try {
					final URI url;
					if (urlString == null || urlString.isEmpty() || urlString.equals("about:blank")) {
						url= null;
					}
					else {
						url= toLocalUrl(urlString, service, m);
					}
					urlUrl= (url != null) ? url.toURL() : null;
				}
				catch (final URISyntaxException | UnknownSchemeException | MalformedURLException e) {
					throw new StatusException(new ErrorStatus(NicoUI.BUNDLE_ID, 0,
							String.format("The specified string '%1$s' is not a valid URL.", urlString),
							e ));
				}
				final var browserSupport= PlatformUI.getWorkbench().getBrowserSupport();
				
				UIAccess.getDisplay().asyncExec(() -> {
					try {
						final var workbenchPage= NicoUI.getToolRegistry().findWorkbenchPage(service.getTool());
						openWebBrowser(urlUrl, browserSupport, workbenchPage);
					}
					catch (final Exception e) {
						NicoUIPlugin.logError(0, "An error occurred when opening the web browser.", e);
					}
				});
				
				return Status.OK_STATUS;
			}
		default:
			throw new UnsupportedOperationException();
		}
	}
	
	protected @Nullable URI toLocalUrl(final String urlString, final ToolService service,
			final ProgressMonitor m) throws StatusException, URISyntaxException, UnknownSchemeException {
		URI url= new URI(urlString);
		if (url.getScheme() == null) {
			url= new URI((url.getHost() != null) ? HTTP_SCHEME : FILE_SCHEME,
					url.getSchemeSpecificPart(), url.getFragment() );
		}
		if (this.tunnelRemote
				&& (service.getTool() instanceof final ToolProcess toolProcess) ) {
			final var toolWorkspace= toolProcess.getWorkspace();
			if (toolWorkspace.isRemote()) {
				String host;
				if (UriUtils.isFileUrl(url)) {
					final String toolPath= url.getPath();
					if (toolPath == null) {
						return null;
					}
					final Path path= toolWorkspace.toSystemPath(toolPath);
					return path.toUri();
				}
				else if ((host= url.getHost()) != null && CommonsNet.isCommonLoopback(host)) {
					host= toolWorkspace.getHost();
					final RSAccessClientSession remoteNetSession;
					if ((remoteNetSession= toolWorkspace.getRemoteNetSession(m)) != null) {
						final var portForwarding= remoteNetSession.allocatePortForwardingL(
								(url.getPort() != -1) ? new Port(url.getPort()) : CommonsNet.getDefaultPort(url.getScheme()));
						url= new URI(url.getScheme(), url.getUserInfo(),
								portForwarding.getLocalAddress().getHostName(), portForwarding.getLocalAddress().getPort(),
								url.getPath(), url.getQuery(), url.getFragment() );
						toolProcess.addTerminationListener(() -> {
							try {
								remoteNetSession.releasePortForwarding(portForwarding);
							}
							catch (final StatusException e) {
								NicoUIPlugin.logError(0, "An error occurred when releasing port forwarding.", e);
							}
						});
					}
					else if (!host.equals(url.getHost())) {
						url= new URI(url.getScheme(), url.getUserInfo(), host, url.getPort(),
								url.getPath(), url.getQuery(), url.getFragment() );
					}
				}
			}
		}
		return url;
	}
	
	protected void openWebBrowser(final @Nullable URL url, final IWorkbenchBrowserSupport browserSupport,
			final IWorkbenchPage workbenchPage) throws PartInitException {
		final IWebBrowser browser= browserSupport.getExternalBrowser();
		if (url != null) {
			browser.openURL(url);
		}
		else { // null is only supported by some web browser implementations
			String browserClass= browser.getClass().getName();
			if (browserClass.startsWith("org.eclipse.ui.internal.browser.")) { //$NON-NLS-1$
				browserClass= browserClass.substring(32);
				if (browserClass.startsWith("ExternalBrowser") //$NON-NLS-1$
						|| browserClass.startsWith("InternalBrowserEditor") //$NON-NLS-1$
						|| browserClass.startsWith(".browsers.MozillaBrowser")) { //$NON-NLS-1$
					browser.openURL(null);
					return;
				}
				else if (browserClass.startsWith("SystemBrowser")) { //$NON-NLS-1$
					final Program program= Program.findProgram("html"); //$NON-NLS-1$
					if (program != null) {
						if (program.execute("about:blank")) { //$NON-NLS-1$
							return;
						}
					}
					Program.launch(HTTP_SCHEME + ':');
					return;
				}
			}
			else {
				try {
					browser.openURL(null);
					return;
				}
				catch (final NullPointerException e) {}
			}
			try {
				browser.openURL(new URI(HTTP_SCHEME, null, null).toURL());
			}
			catch (final URISyntaxException | MalformedURLException e) {}
		}
	}
	
}
