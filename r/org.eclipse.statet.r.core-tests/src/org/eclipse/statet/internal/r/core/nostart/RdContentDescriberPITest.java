/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.nostart;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.internal.r.core.nostart.RContentDescriberPITest.assertContentType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.util.ArrayUtils;


@NonNullByDefault
public class RdContentDescriberPITest {
	
	
	private static final String CONTENT_TYPE_ID= "org.eclipse.statet.r.contentTypes.Rd"; //$NON-NLS-1$
	
	private static final String CONTENT_1= "\\name{rj.GD}\n\\title{Create a new RJ graphics device}\n\\description{\nFoo bar\n}\n";
	
	
	public RdContentDescriberPITest() {
	}
	
	
	protected IContentTypeManager getContentTypeManager() {
		return nonNullAssert(Platform.getContentTypeManager());
	}
	
	
	@Test
	public void ContentType() {
		final var contentTypeManager= getContentTypeManager();
		
		final IContentType contentType= contentTypeManager.getContentType(CONTENT_TYPE_ID);
		assertNotNull(contentType);
		assertEquals(CONTENT_TYPE_ID, contentType.getId());
		assertNull(contentType.getDefaultCharset());
	}
	
	
	@Test
	public void describeByteStream_default() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(
						CONTENT_1.getBytes(StandardCharsets.US_ASCII) ),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertNull(description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(new byte[0]),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertNull(description.getCharset());
	}
	
	@Test
	public void describeByteStream_specified() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(
						("\\encoding{UTF-8}\n" + CONTENT_1).getBytes(StandardCharsets.UTF_8) ),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-8", description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(
						("\\encoding{ UTF-8 }\t\n" + CONTENT_1).getBytes(StandardCharsets.UTF_8) ),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-8", description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(
						("\\encoding{ UTF-8 }% TODO update\n" + CONTENT_1).getBytes(StandardCharsets.UTF_8) ),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-8", description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(
						("% 2020-02-23\n \n\\encoding{ UTF-8 }\n" + CONTENT_1).getBytes(StandardCharsets.UTF_8) ),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-8", description.getCharset());
	}
	
	@Test
	public void describeByteStream_specifiedUtf16() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		final byte[] content= ArrayUtils.concat(IContentDescription.BOM_UTF_16LE,
				("\\encoding{ UTF-16LE }\n" + CONTENT_1).getBytes(StandardCharsets.UTF_16LE) );
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(content),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-16LE", description.getCharset());
	}
	
	
	@Test
	public void describeCharStream_default() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new StringReader(CONTENT_1),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertNull(description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new StringReader(""),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertNull(description.getCharset());
	}
	
	@Test
	public void describeCharStream_specified() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new StringReader("\\encoding{ UTF-8 }\n" + CONTENT_1),
				"foo.Rd",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-8", description.getCharset());
	}
	
	
}
