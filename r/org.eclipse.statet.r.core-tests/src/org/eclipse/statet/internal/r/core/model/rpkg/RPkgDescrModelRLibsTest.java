/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import static org.junit.jupiter.api.Assertions.assertFalse;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.internal.r.core.nostart.RPkgDescriptionContentDescriber;
import org.eclipse.statet.internal.r.core.rmodel.SourceModelTests;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.Asts;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.Task;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.ltk.test.core.TestContentDescription;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.source.RSourceConfig;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_R_LIBS", matches= ".+")
@NonNullByDefault
public class RPkgDescrModelRLibsTest {
	
	
	private static final RPkgDescriptionContentDescriber CONTENT_DESCRIPTER= new RPkgDescriptionContentDescriber();
	
	
	private final RPkgDescrDcfParser parser= new RPkgDescrDcfParser();
	private final RPkgDescrSourceAnalyzer sourceAnalyzer= new RPkgDescrSourceAnalyzer();
	
	private final RPkgDescrIssueReporter reporter= new RPkgDescrIssueReporter();
	
	
	
	public RPkgDescrModelRLibsTest() {
	}
	
	
	public static List<Path> allRPkgPaths() throws IOException {
		@NonNull
		final String[] libs= nonNullAssert(System.getenv("STATET_TEST_R_LIBS"))
				.split("\\" + File.pathSeparatorChar);
		final var list= new ArrayList<Path>();
		for (final String libPathString : libs) {
			final Path libPath= Paths.get(libPathString);
			try (final var libDirStream= Files.newDirectoryStream(libPath)) {
				for (final Path path : libDirStream) {
					if (Files.isDirectory(path)) {
						list.add(path);
					}
				}
			}
		}
		return list;
	}
	
	
	@ParameterizedTest
	@MethodSource("allRPkgPaths")
	public void scanLib(final Path pkgPath) throws IOException {
		final String source= readDescriptionFile(pkgPath);
		check(source);
	}
	
	private String readDescriptionFile(final Path pkgPath) throws IOException {
		final Path file= pkgPath.resolve("DESCRIPTION");
		final byte[] bytes= Files.readAllBytes(file);
		final TestContentDescription contentDescription= new TestContentDescription();
		CONTENT_DESCRIPTER.describe(new ByteArrayInputStream(bytes), contentDescription);
		final String charsetName= contentDescription.getCharset();
		final Charset charset= (charsetName != null) ? Charset.forName(charsetName) : StandardCharsets.US_ASCII;
		return charset.newDecoder().decode(ByteBuffer.wrap(bytes)).toString();
	}
	
	private void check(final String source) {
		final RSourceUnit sourceUnit= SourceModelTests.createRPkgDescrSourceUnit(source);
		final RSourceConfig rSourceConfig= RSourceConfig.DEFAULT_CONFIG;
		final SourceContent sourceContent= sourceUnit.getContent(new NullProgressMonitor());
		final BasicSourceModelStamp stamp= new BasicSourceModelStamp(sourceContent.getStamp(),
				ImCollections.newList(rSourceConfig) );
		final SourceComponent sourceNode= this.parser.parseSourceUnit(
				sourceContent.getString() );
		final AstInfo ast= new AstInfo(1, stamp, sourceNode, ImCollections.emptyIdentitySet());
		
		final var modelInfo= this.sourceAnalyzer.createModel(sourceUnit, ast);
		
		final List<Problem> collectedProblems= new ArrayList<>();
		this.reporter.run(sourceUnit, modelInfo, sourceContent, new IssueRequestor() {
			@Override
			public boolean isInterestedInProblems(final String categoryId) {
				return true;
			}
			@Override
			public void acceptProblems(final String categoryId, final List<Problem> problems) {
				collectedProblems.addAll(problems);
			}
			@Override
			public void acceptProblems(final Problem problem) {
				collectedProblems.add(problem);
			}
			@Override
			public boolean isInterestedInTasks() {
				return false;
			}
			@Override
			public void acceptTask(final Task task) {
			}
			@Override
			public void finish() {
			}
		}, ModelManager.MODEL_FILE);
		if (!collectedProblems.isEmpty()) {
			final var sb= new ToStringBuilder("problems found:");
			for (int i= 0; i < collectedProblems.size(); i++) {
				sb.addProp(String.format("[%1$02d]", i), collectedProblems.get(i).toString());
			}
			throw new org.opentest4j.AssertionFailedError(sb.toString());
		}
		assertFalse(Asts.hasWarnings(sourceNode));
	}
	
}
