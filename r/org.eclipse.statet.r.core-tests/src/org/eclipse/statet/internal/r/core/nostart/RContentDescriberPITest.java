/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.nostart;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RContentDescriberPITest {
	
	
	private static final String CONTENT_TYPE_ID= "org.eclipse.statet.r.contentTypes.R"; //$NON-NLS-1$
	
	private static final String CONTENT_1= "x <- rnorm(1000);\n"; //$NON-NLS-1$
	
	
	public RContentDescriberPITest() {
	}
	
	
	protected IContentTypeManager getContentTypeManager() {
		return nonNullAssert(Platform.getContentTypeManager());
	}
	
	
	@Test
	public void ContentType() {
		final var contentTypeManager= getContentTypeManager();
		
		final IContentType contentType= contentTypeManager.getContentType(CONTENT_TYPE_ID);
		assertNotNull(contentType);
		assertEquals(CONTENT_TYPE_ID, contentType.getId());
		assertNull(contentType.getDefaultCharset());
	}
	
	
	@Test
	public void describeByteStream_default() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(
						CONTENT_1.getBytes(StandardCharsets.US_ASCII) ),
				"foo.R",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertNull(description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(new byte[0]),
				"foo.R",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertNull(description.getCharset());
	}
	
	
	@Test
	public void describeCharStream_default() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new StringReader(CONTENT_1),
				"foo.R",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertNull(description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new StringReader(""),
				"foo.R",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertNull(description.getCharset());
	}
	
	
	static void assertContentType(final String contentTypeId, final @Nullable IContentType actual) {
		assertNotNull(actual);
		assertEquals(contentTypeId, actual.getId());
	}
	
	
}
