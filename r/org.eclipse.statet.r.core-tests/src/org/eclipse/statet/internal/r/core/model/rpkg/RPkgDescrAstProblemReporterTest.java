/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants;
import org.eclipse.statet.dsl.dcf.core.source.ast.DcfAstProblemReporter;
import org.eclipse.statet.internal.r.core.rmodel.SourceModelTests;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.Task;
import org.eclipse.statet.r.core.model.RSourceUnit;


@NonNullByDefault
public class RPkgDescrAstProblemReporterTest {
	
	
	private final RPkgDescrDcfParser parser= new RPkgDescrDcfParser();
	
	private final DcfAstProblemReporter reporter= new DcfAstProblemReporter("DcfR");
	
	
	public RPkgDescrAstProblemReporterTest() {
	}
	
	
	@Test
	public void Field_missingName() {
		List<Problem> problems;
		
		problems= collectProblems(": Value");
		assertEquals(1, problems.size());
		assertProblem(Problem.SEVERITY_ERROR,
				DcfSourceConstants.TYPE12_SYNTAX_NODE_MISSING | DcfSourceConstants.CTX1_NAME,
				"Syntax Error/Missing Name: name of field is missing.",
				0, 1,
				problems.get(0) );
	}
	
	
	private List<Problem> collectProblems(final String source) {
		final RSourceUnit sourceUnit= SourceModelTests.createRPkgDescrSourceUnit(source);
		final SourceContent sourceContent= sourceUnit.getContent(new NullProgressMonitor());
		final SourceComponent sourceNode= this.parser.parseSourceUnit(
				sourceContent.getString() );
		
		final List<Problem> collectedProblems= new ArrayList<>();
		this.reporter.run(sourceNode, sourceContent, new IssueRequestor() {
			@Override
			public boolean isInterestedInProblems(final String categoryId) {
				return true;
			}
			@Override
			public void acceptProblems(final String categoryId, final List<Problem> problems) {
				collectedProblems.addAll(problems);
			}
			@Override
			public void acceptProblems(final Problem problem) {
				collectedProblems.add(problem);
			}
			@Override
			public boolean isInterestedInTasks() {
				return false;
			}
			@Override
			public void acceptTask(final Task task) {
			}
			@Override
			public void finish() {
			}
		});
		return collectedProblems;
	}
	
	private void assertProblem(final int expectedSeverity, final int expectedCode, final String expectedMessage,
			final int expectedStartOffset, final int expectedLength,
			final Problem actual) {
		assertEquals(expectedSeverity, actual.getSeverity(), "severity");
		assertEquals(String.format("0x%1$08X", expectedCode), String.format("0x%1$08X", actual.getCode()), "code");
		assertEquals(expectedMessage, actual.getMessage(), "message");
		assertEquals(expectedStartOffset, actual.getSourceStartOffset(), "startOffset");
		assertEquals(expectedStartOffset + expectedLength, actual.getSourceEndOffset(), "endOffset");
	}
	
}
