/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.nostart;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.internal.r.core.nostart.RContentDescriberPITest.assertContentType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.util.ArrayUtils;


@NonNullByDefault
public class RPkgDescriptionContentDescriberPITest {
	
	
	private static final String CONTENT_TYPE_ID= "org.eclipse.statet.r.contentTypes.RPkgDescription"; //$NON-NLS-1$
	
	private static final String CONTENT_1= "Title: Test Package\nPackage: test1\nDescription: Foo bar.\n"; //$NON-NLS-1$
	
	private static final String ASCII_NAME= "US-ASCII"; //$NON-NLS-1$
	
	
	public RPkgDescriptionContentDescriberPITest() {
	}
	
	
	protected IContentTypeManager getContentTypeManager() {
		return nonNullAssert(Platform.getContentTypeManager());
	}
	
	
	@Test
	public void ContentType() {
		final var contentTypeManager= getContentTypeManager();
		
		final IContentType contentType= contentTypeManager.getContentType(CONTENT_TYPE_ID);
		assertNotNull(contentType);
		assertEquals(CONTENT_TYPE_ID, contentType.getId());
		assertEquals(ASCII_NAME, contentType.getDefaultCharset());
	}
	
	
	@Test
	public void describeByteStream_default() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(
						CONTENT_1.getBytes(StandardCharsets.US_ASCII) ),
				"DESCRIPTION",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals(ASCII_NAME, description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(new byte[0]),
				"DESCRIPTION",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals(ASCII_NAME, description.getCharset());
	}
	
	@Test
	public void describeByteStream_specified() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(
						("Encoding: UTF-8\n" + CONTENT_1).getBytes(StandardCharsets.UTF_8) ),
				"DESCRIPTION",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-8", description.getCharset());
	}
	
	@Test
	public void describeByteStream_specifiedUtf16() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new ByteArrayInputStream(ArrayUtils.concat(
						IContentDescription.BOM_UTF_16LE,
						("Encoding: UTF-16LE\n" + CONTENT_1).getBytes(StandardCharsets.UTF_16LE) )),
				"DESCRIPTION",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-16LE", description.getCharset());
	}
	
	
	@Test
	public void describeCharStream_default() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new StringReader(CONTENT_1),
				"DESCRIPTION",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals(ASCII_NAME, description.getCharset());
		
		description= contentTypeManager.getDescriptionFor(
				new StringReader(""),
				"DESCRIPTION",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals(ASCII_NAME, description.getCharset());
	}
	
	@Test
	public void describeCharStream_specified() throws IOException {
		final var contentTypeManager= getContentTypeManager();
		IContentDescription description;
		
		description= contentTypeManager.getDescriptionFor(
				new StringReader("Encoding: UTF-8\n" + CONTENT_1),
				"DESCRIPTION",
				new QualifiedName[] { IContentDescription.CHARSET });
		assertContentType(CONTENT_TYPE_ID, description.getContentType());
		assertEquals("UTF-8", description.getCharset());
	}
	
	
}
