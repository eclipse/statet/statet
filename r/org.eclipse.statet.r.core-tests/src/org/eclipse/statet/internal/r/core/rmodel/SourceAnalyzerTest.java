/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.rmodel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static org.eclipse.statet.internal.r.core.rmodel.SourceModelTests.assertElementName;
import static org.eclipse.statet.internal.r.core.rmodel.SourceModelTests.assertRegion;

import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.internal.r.core.rmodel.RSrcStrElementByElementAccess.RMethod;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.r.core.model.RElement;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.core.model.RModel;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.RSourceUnitModelInfo;
import org.eclipse.statet.r.core.model.rlang.RLangPackageLoad;
import org.eclipse.statet.r.core.model.rlang.RLangSrcStrElement;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.ast.RParser;
import org.eclipse.statet.r.core.source.ast.SourceComponent;


@NonNullByDefault
public class SourceAnalyzerTest {
	
	
	private final RParser rParser= new RParser(ModelManager.MODEL_FILE);
	
	private final SourceAnalyzer rSourceAnalyzer= new SourceAnalyzer();
	
	
	public SourceAnalyzerTest() {
	}
	
	
	@Test
	public void PackageLoad_base_require() {
		RSourceUnitModelInfo modelInfo;
		List<? extends RLangSrcStrElement> elements;
		
		modelInfo= createModel("require(MASS)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 13, 8, elements.get(0));
		
		modelInfo= createModel("require(MASS, character.only= TRUE)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(0, elements.size());
		
		modelInfo= createModel("require(\"MASS\")\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 15, 9, elements.get(0));
		
		modelInfo= createModel("require(\"MASS\", character.only= TRUE)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 37, 9, elements.get(0));
	}
	
	@Test
	public void PackageLoad_base_library() {
		RSourceUnitModelInfo modelInfo;
		List<? extends RLangSrcStrElement> elements;
		
		modelInfo= createModel("library(MASS)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 13, 8, elements.get(0));
		
		modelInfo= createModel("library(MASS, character.only= TRUE)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(0, elements.size());
		
		modelInfo= createModel("library(\"MASS\")\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 15, 9, elements.get(0));
		
		modelInfo= createModel("library(\"MASS\", character.only= TRUE)\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertPackageLoad(0, 37, 9, elements.get(0));
	}
	
	
	@Test
	public void assign_arrowLeftS() {
		RSourceUnitModelInfo modelInfo;
		List<? extends RLangSrcStrElement> elements;
		
		modelInfo= createModel("f1 <- (function(a) { a })\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertFDef(0, 25, 0, elements.get(0));
	}
	
	@Test
	public void assign_arrowRightS() {
		RSourceUnitModelInfo modelInfo;
		List<? extends RLangSrcStrElement> elements;
		
		modelInfo= createModel("(function(a) { a }) -> f1\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertFDef(0, 25, 23, elements.get(0));
	}
	
	@Test
	public void assign_base_assign() {
		RSourceUnitModelInfo modelInfo;
		List<? extends RLangSrcStrElement> elements;
		
		modelInfo= createModel("assign(\"f1\", function(a) { a })\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertFDef(0, 31, 8, elements.get(0));
	}
	
	@Test
	public void assign_pipeForward_base_assign() {
		RSourceUnitModelInfo modelInfo;
		List<? extends RLangSrcStrElement> elements;
		
		modelInfo= createModel("(function(a) { a }) |> assign(x=\"f1\")\n");
		elements= modelInfo.getSourceElement().getSourceChildren(null);
		assertEquals(1, elements.size());
		assertFDef(0, 37, 33, elements.get(0));
	}
	
	
	protected RLangSrcStrElement assertRElement(final int expectedType,
			final int expectedStartOffset, final int expectedLength,
			final SourceStructElement<?, ?> actual) {
		assertEquals(RModel.R_TYPE_ID, actual.getModelTypeId());
		assertEquals(expectedType, (actual.getElementType() & LtkModelElement.MASK_C123));
		assertRegion(expectedStartOffset, expectedLength, actual.getSourceRange(), "sourceRange");
		assertInstanceOf(RLangSrcStrElement.class, actual);
		final RLangSrcStrElement rElement= (RLangSrcStrElement)actual;
		return rElement;
	}
	
	protected void assertPackageLoad(final int expectedStartOffset, final int expectedLength,
			final int expectedNameStartOffset,
			final RLangSrcStrElement actual) {
		assertRElement(RElement.R_PACKAGE_LOAD,
				expectedStartOffset, expectedLength,
				actual );
		assertInstanceOf(RLangPackageLoad.class, actual);
		assertElementName(RElementName.SCOPE_PACKAGE, "MASS",
				actual.getElementName() );
		assertRegion(expectedNameStartOffset, 4, actual.getNameSourceRange(), "name");
	}
	
	protected void assertFDef(final int expectedStartOffset, final int expectedLength,
			final int expectedNameStartOffset,
			final RLangSrcStrElement actual) {
		assertRElement(RElement.R_COMMON_FUNCTION,
				expectedStartOffset, expectedLength,
				actual );
		assertInstanceOf(RMethod.class, actual);
		assertElementName(RElementName.MAIN_DEFAULT, "f1",
				actual.getElementName() );
		assertRegion(expectedNameStartOffset, 2, actual.getNameSourceRange(), "name");
	}
	
	
	protected RSourceUnitModelInfo createModel(final String code) {
		final RSourceUnit sourceUnit= SourceModelTests.createRSourceUnit(code);
		
		final RSourceConfig rSourceConfig= RSourceConfig.DEFAULT_CONFIG;
		this.rParser.setRSourceConfig(rSourceConfig);
		
		final SourceContent sourceContent= sourceUnit.getContent(new NullProgressMonitor());
		final SourceComponent sourceComponent= this.rParser.parseSourceUnit(
				new StringParserInput(sourceContent.getString()).init() );
		final var ast= new AstInfo(ModelManager.MODEL_FILE, new BasicSourceModelStamp(0),
				sourceComponent );
		
		final var model= this.rSourceAnalyzer.createModel(sourceUnit, ast);
		assertNotNull(model);
		return model;
	}
	
	
}
