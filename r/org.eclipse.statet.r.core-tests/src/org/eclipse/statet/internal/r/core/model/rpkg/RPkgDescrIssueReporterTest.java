/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.core.model.rpkg;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.internal.r.core.rmodel.SourceModelTests;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.LtkCore;
import org.eclipse.statet.ltk.core.StatusCodes;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.Task;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.r.core.model.RPkgDescrFieldDefinition;
import org.eclipse.statet.r.core.model.RPkgDescriptions;
import org.eclipse.statet.r.core.model.RSourceUnit;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceConstants;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceStructElement;
import org.eclipse.statet.r.core.model.rpkg.RPkgDescrSourceUnitModelInfo;
import org.eclipse.statet.r.core.source.RSourceConfig;


@NonNullByDefault
public class RPkgDescrIssueReporterTest {
	
	
	private record ReconcileResult(
			RPkgDescrSourceUnitModelInfo modelInfo,
			List<Problem> problems) {
	}
	
	
	private final RPkgDescrDcfParser parser= new RPkgDescrDcfParser();
	private final RPkgDescrSourceAnalyzer sourceAnalyzer= new RPkgDescrSourceAnalyzer();
	
	private final RPkgDescrIssueReporter reporter= new RPkgDescrIssueReporter();
	
	
	public RPkgDescrIssueReporterTest() {
	}
	
	
	private static final String S_MIN= """
			Package: test.package
			Version: 1.0-123
			License: LGPL-3
			Title: R Pkg Test Package
			Description: Test StatET
			Author: Jane Doe [aut, cre]
			Maintainer: Jane Doe <jane.doe@example.com>
			""";
	
	
	@Test
	public void OK_min() {
		ReconcileResult result;
		
		result= reconcile(S_MIN);
		assertEquals(0, result.problems().size());
	}
	
	@Test
	public void OK_min_AuthorsR() {
		ReconcileResult result;
		
		result= reconcile("""
				Package: test.package.2
				Version: 1.0-123
				License: LGPL-3
				Title: R Pkg Test Package
				Description: Test StatET
				Authors@R: c(person("Jane", "Doe", role= c("aut", "cre"), email= "jane.doe@example.com"))
				""" );
		assertEquals(0, result.problems().size());
		for (final var field : result.modelInfo.getFields()) {
			assertModelStatus(StatusCodes.TYPE1_OK, field);
		}
	}
	
	@Test
	public void OK_common() {
		ReconcileResult result;
		
		result= reconcile("""
				Package: test.package.3
				Version: 1.0-123
				Date: 2023-01-06
				License: GPL (>= 2) | BSD_3_clause + file LICENSE
				Title: R Pkg Test Package
				Description: Test the parser and source analyzer for R package description file
				 in Eclipse StatET
				Author: Jane Doe [aut, cre],
				  Max Mustermann [aut]
				Maintainer: Jane Doe <jane.doe@example.com>
				Depends: R (>= 4.1.0), nlme
				Suggests: MASS
				URL: https://example.com/jane.doe/r
				""" );
		assertEquals(0, result.problems().size());
		assertModelStatus(StatusCodes.TYPE1_OK, result.modelInfo.getSourceElement());
		for (final var field : result.modelInfo.getFields()) {
			assertModelStatus(StatusCodes.TYPE1_OK, field);
		}
	}
	
	@Test
	public void OK_max() {
		ReconcileResult result;
		
		result= reconcile("""
				Encoding: UTF-8
				Type: Package
				Package: test.package
				Version: 1.0-123
				Date: 2023-01-06
				License: GPL (>= 2) | BSD_3_clause + file LICENSE
				Title: R Pkg Test Package
				Description: Test the parser and source analyzer for R package description file
				 in Eclipse StatET
				Author: Jane Doe [aut, cre],
				  Max Mustermann [aut]
				Maintainer: Jane Doe <jane.doe@example.com>
				Depends: R (>= 4.1.0), nlme
				Suggests: MASS
				URL: https://example.com/jane.doe/r
				""" );
		assertEquals(0, result.problems().size());
		assertModelStatus(StatusCodes.TYPE1_OK, result.modelInfo.getSourceElement());
		for (final var field : result.modelInfo.getFields()) {
			assertModelStatus(StatusCodes.TYPE1_OK, field);
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.model.RPkgDescriptions#getRequiredFieldDefinitions")
	public void Field_requiredMissing(final RPkgDescrFieldDefinition fieldDef) {
		ReconcileResult result;
		final var source= new StringBuilder();
		
		{	source.setLength(0);
			final int fieldStart= S_MIN.indexOf(fieldDef.getName() + ':');
			final int fieldEnd= S_MIN.indexOf('\n', fieldStart) + 1;
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_REQUIRED_MISSING,
				"Missing Field: the mandatory field '" + fieldDef.getName() + "' is missing.",
				LtkCore.NA_OFFSET, 0,
				result.problems().get(0) );
	}
	
	@Test
	public void Field_requiredMissing_multiple() {
		ReconcileResult result;
		String fieldName;
		final var source= new StringBuilder();
		
		{	source.setLength(0);
			final int end= S_MIN.indexOf("Description:");
			source.append(S_MIN, 0, end);
		}
		final var missingFields= List.of("Description", "Author", "Maintainer");
		result= reconcile(source.toString());
		assertEquals(missingFields.size(), result.problems().size());
		for (int i= 0; i < missingFields.size(); i++) {
			fieldName= missingFields.get(i);
			assertProblem(Problem.SEVERITY_ERROR,
					RPkgDescrSourceConstants.TYPE12_FIELD_REQUIRED_MISSING,
					"Missing Field: the mandatory field '" + fieldName + "' is missing.",
					LtkCore.NA_OFFSET, 0,
					result.problems().get(i) );
		}
	}
	
	@Test
	public void Field_duplicate() {
		ReconcileResult result;
		String fieldName;
		final var source= new StringBuilder();
		
		{	source.setLength(0);
			fieldName= "Package";
			source.append(S_MIN);
			source.append("Package: duplicate\n");
		}
		final int[] offsets= { 0, S_MIN.length() };
		result= reconcile(source.toString());
		assertEquals(2, result.problems().size());
		for (int i= 0; i < 2; i++) {
			assertProblem(Problem.SEVERITY_WARNING,
					RPkgDescrSourceConstants.TYPE12_FIELD_DUPLICATE,
					"Duplicate Field: the field '" + fieldName + "' is specified multiple times.",
					offsets[i], fieldName.length(),
					result.problems().get(i) );
		}
	}
	
	@Test
	public void Value_Encoding() {
		ReconcileResult result;
		final var source= new StringBuilder();
		
		{	source.setLength(0);
			source.append("Encoding: \n");
			source.append(S_MIN);
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				"Missing Value for 'Encoding': the value for that field must not be empty.",
				9, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				result.modelInfo.getField("Encoding") );
		
		{	source.setLength(0);
			source.append("Encoding:\n");
			source.append(S_MIN);
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				"Missing Value for 'Encoding': the value for that field must not be empty.",
				8, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				result.modelInfo.getField("Encoding") );
	}
	
	@Test
	public void Value_Type() {
		ReconcileResult result;
		final var source= new StringBuilder();
		
		{	source.setLength(0);
			source.append("Type: \n");
			source.append(S_MIN);
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				"Missing Value for 'Type': the value for that field must not be empty.",
				5, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				result.modelInfo.getField("Type") );
	}
	
	@Test
	public void Value_Package() {
		ReconcileResult result;
		final var source= new StringBuilder();
		
		final int fieldStart= S_MIN.indexOf("Package: ");
		final int fieldEnd= S_MIN.indexOf('\n', fieldStart) + 1;
		{	source.setLength(0);
			source.append("Package: \n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				"Missing Value for 'Package': the value for that field must not be empty.",
				8, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: test_package\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › test_package ‹ is invalid; it must contain only ASCII letters, digits or dot.",
				9 + 4, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: _test_package\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › _test_package ‹ is invalid; it must contain only ASCII letters, digits or dot.",
				9 + 0, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: test.päckage\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › test.päckage ‹ is invalid; it must contain only ASCII letters, digits or dot.",
				9 + 6, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: 0test.package\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › 0test.package ‹ is invalid; it must start with a letter.",
				9 + 0, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: .test\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › .test ‹ is invalid; it must start with a letter.",
				9 + 0, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: t\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › t ‹ is invalid; it must be at least two characters long.",
				9, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: test.\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › test. ‹ is invalid; it must not end with a dot.",
				9 + 4, 1,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: test.package\n .line\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › test.package❬¶❭.line ‹ is invalid; it must contain only ASCII letters, digits or dot.",
				9 + 11, 3,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
		
		{	source.setLength(0);
			source.append("Package: test.package  \n .line\n");
			source.append(S_MIN, 0, fieldStart);
			source.append(S_MIN, fieldEnd, S_MIN.length());
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Package Name: the package name › test.package❬¶❭.line ‹ is invalid; it must contain only ASCII letters, digits or dot.",
				9 + 12, 4,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField("Package") );
	}
	
	static List<RPkgDescrFieldDefinition> getLogicalValueFieldDefs() {
		return RPkgDescriptions.getAllFieldDefinitions().stream()
				.filter((field) -> (field.getDataType() == RPkgDescrFieldDefinition.LOGICAL))
				.toList();
	}
	
	@ParameterizedTest
	@MethodSource("getLogicalValueFieldDefs")
	public void Value_LogicalValues(final RPkgDescrFieldDefinition fieldDef) {
		ReconcileResult result;
		final var source= new StringBuilder();
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ": yes\n");
		}
		result= reconcile(source.toString());
		assertEquals(0, result.problems().size());
		assertModelStatus(StatusCodes.TYPE1_OK,
				result.modelInfo.getField(fieldDef.getName()) );
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ": false\n");
		}
		result= reconcile(source.toString());
		assertEquals(0, result.problems().size());
		assertModelStatus(StatusCodes.TYPE1_OK,
				result.modelInfo.getField(fieldDef.getName()) );
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ":  \n");
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				"Missing Value for '" + fieldDef.getName() + "': the value for that field must not be empty.",
				S_MIN.length() + fieldDef.getName().length() + 1, 2,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				result.modelInfo.getField(fieldDef.getName()) );
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ": on\n");
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_ERROR,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Value for '" + fieldDef.getName() + "': a boolean literal (› yes ‹/› no ‹ or › true ‹/› false ‹) is expected for that field.",
				S_MIN.length() + fieldDef.getName().length() + 2, 2,
				result.problems().get(0) );
		assertModelStatus(RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				result.modelInfo.getField(fieldDef.getName()) );
	}
	
	static List<RPkgDescrFieldDefinition> getUrlValueFieldDefs() {
		return RPkgDescriptions.getAllFieldDefinitions().stream()
				.filter((field) -> (field.getDataType() == RPkgDescrFieldDefinition.URL))
				.toList();
	}
	
	@ParameterizedTest
	@MethodSource("getUrlValueFieldDefs")
	public void Value_UrlValues(final RPkgDescrFieldDefinition fieldDef) {
		ReconcileResult result;
		final var source= new StringBuilder();
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ": https://example.com/org/project\n");
		}
		result= reconcile(source.toString());
		assertEquals(0, result.problems().size());
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ": https://example.com/org/project/report%20bugs.html\n");
		}
		result= reconcile(source.toString());
		assertEquals(0, result.problems().size());
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ":  \n");
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_WARNING,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_MISSING,
				"Missing Value for '" + fieldDef.getName() +"': the value for that field must not be empty.",
				S_MIN.length() + fieldDef.getName().length() + 1, 2,
				result.problems().get(0) );
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ": https://example.com/org/project https://example.com/second\n");
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_WARNING,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Value for '" + fieldDef.getName() +"': a single URL is expected for that field.",
				S_MIN.length() + fieldDef.getName().length() + 2, 58,
				result.problems().get(0) );
		
		{	source.setLength(0);
			source.append(S_MIN);
			source.append(fieldDef.getName() + ": https://example.com/org/project/report bugs.html\n");
		}
		result= reconcile(source.toString());
		assertEquals(1, result.problems().size());
		assertProblem(Problem.SEVERITY_WARNING,
				RPkgDescrSourceConstants.TYPE12_FIELD_VALUE_INVALID,
				"Invalid Value for '" + fieldDef.getName() + "': a single URL is expected for that field. › https://example.com/org/project/report bugs.html ‹ is no valid URL, illegal character in path.",
				S_MIN.length() + fieldDef.getName().length() + 2 + 38, 1,
				result.problems().get(0) );
	}
	
	
	private ReconcileResult reconcile(final String source) {
		final RSourceUnit sourceUnit= SourceModelTests.createRPkgDescrSourceUnit(source);
		final RSourceConfig rSourceConfig= RSourceConfig.DEFAULT_CONFIG;
		final SourceContent sourceContent= sourceUnit.getContent(new NullProgressMonitor());
		final BasicSourceModelStamp stamp= new BasicSourceModelStamp(sourceContent.getStamp(),
				ImCollections.newList(rSourceConfig) );
		final SourceComponent sourceNode= this.parser.parseSourceUnit(
				sourceContent.getString() );
		final AstInfo ast= new AstInfo(1, stamp, sourceNode, ImCollections.emptyIdentitySet());
		final var modelInfo= this.sourceAnalyzer.createModel(sourceUnit, ast);
		
		final List<Problem> collectedProblems= new ArrayList<>();
		this.reporter.run(sourceUnit, modelInfo, sourceContent, new IssueRequestor() {
			@Override
			public boolean isInterestedInProblems(final String categoryId) {
				return true;
			}
			@Override
			public void acceptProblems(final String categoryId, final List<Problem> problems) {
				collectedProblems.addAll(problems);
			}
			@Override
			public void acceptProblems(final Problem problem) {
				collectedProblems.add(problem);
			}
			@Override
			public boolean isInterestedInTasks() {
				return false;
			}
			@Override
			public void acceptTask(final Task task) {
			}
			@Override
			public void finish() {
			}
		}, ModelManager.MODEL_FILE);
		return new ReconcileResult(modelInfo, collectedProblems);
	}
	
	private void assertProblem(final int expectedSeverity, final int expectedCode, final String expectedMessage,
			final int expectedStartOffset, final int expectedLength,
			final Problem actual) {
		assertEquals(expectedSeverity, actual.getSeverity(), "severity");
		assertEquals(String.format("0x%1$08X", expectedCode), String.format("0x%1$08X", actual.getCode()), "code");
		assertEquals(expectedMessage, actual.getMessage(), "message");
		assertEquals(expectedStartOffset, actual.getSourceStartOffset(), "startOffset");
		assertEquals(expectedStartOffset + expectedLength, actual.getSourceEndOffset(), "endOffset");
	}
	
	private void assertModelStatus(final int expectedCode, final RPkgDescrSourceStructElement actual) {
		assertEquals(String.format("0x%1$08X", expectedCode), String.format("0x%1$08X", actual.getModelStatusCode()), "statusCode");
	}
	
}
