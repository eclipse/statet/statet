/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CharArrayString;
import org.eclipse.statet.jcommons.string.StringFactory;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.core.source.StatusDetail;


@NonNullByDefault
public class RSourceTests {
	
	
	public static final StringFactory STRING_FACTORY= new StringFactory( ) {
		
		@Override
		public String get(final int codepoint) {
			return Character.toString(codepoint);
		}
		
		@Override
		public String get(final char c) {
			return Character.toString(c);
		}
		
		@Override
		public String get(final String s) {
			return s;
		}
		
		@Override
		public String get(final CharArrayString s) {
			return s.toString();
		}
		
		@Override
		public String get(final CharSequence s) {
			return s.toString();
		}
		
	};
	
	public static final String IGNORE_TEXT= new String("--IGNORE--");
	
	
	public static List<RSourceConfig> RSourceConfigs_all() {
		return RSourceConstants.LANG_VERSIONS
				.map((langVersion) -> new RSourceConfig(langVersion))
				.toList();
	}
	
	public static List<RSourceConfig> RSourceConfigs_before_4_1() {
		return RSourceConstants.getLangVersionsBefore(RSourceConstants.LANG_VERSION_4_1)
				.map((langVersion) -> new RSourceConfig(langVersion))
				.toList();
	}
	
	public static List<RSourceConfig> RSourceConfigs_since_4_1() {
		return RSourceConstants.getLangVersionsSince(RSourceConstants.LANG_VERSION_4_1)
				.map((langVersion) -> new RSourceConfig(langVersion))
				.toList();
	}
	
	public static List<RSourceConfig> RSourceConfigs_before_4_2() {
		return RSourceConstants.getLangVersionsBefore(RSourceConstants.LANG_VERSION_4_2)
				.map((langVersion) -> new RSourceConfig(langVersion))
				.toList();
	}
	
	public static List<RSourceConfig> RSourceConfigs_since_4_2() {
		return RSourceConstants.getLangVersionsSince(RSourceConstants.LANG_VERSION_4_2)
				.map((langVersion) -> new RSourceConfig(langVersion))
				.toList();
	}
	
	public static List<RSourceConfig> RSourceConfigs_before_4_3() {
		return RSourceConstants.getLangVersionsBefore(RSourceConstants.LANG_VERSION_4_3)
				.map((langVersion) -> new RSourceConfig(langVersion))
				.toList();
	}
	
	public static List<RSourceConfig> RSourceConfigs_since_4_3() {
		return RSourceConstants.getLangVersionsSince(RSourceConstants.LANG_VERSION_4_3)
				.map((langVersion) -> new RSourceConfig(langVersion))
				.toList();
	}
	
	
	public static void assertNextToken(final RTerminal expectedType, final int expectedFlags,
			final int expectedOffset, final int expectedLength,
			final @Nullable String expectedText,
			final RLexer lexer) {
		assertEquals(expectedType, lexer.next(), "type (next)");
		assertEquals(expectedType, lexer.getType(), "type");
		assertEquals(expectedFlags, lexer.getFlags(), "flags");
		assertEquals(expectedOffset, lexer.getOffset(), "offset");
		assertEquals(expectedLength, lexer.getLength(), "length");
		if (expectedFlags == 0) {
			assertNull(lexer.getStatusDetail(), "statusDetail");
		}
		if (expectedText != IGNORE_TEXT) {
			assertEquals(expectedText, lexer.getText(), "text");
			assertEquals(expectedText, lexer.getText(STRING_FACTORY), "text (StringFactory)");
		}
	}
	
	public static void assertNextToken(final RTerminal expectedType,
			final int expectedOffset, final int expectedLength,
			final RLexer lexer) {
		assertNextToken(expectedType, 0,
				expectedOffset, expectedLength,
				expectedType.text,
				lexer );
	}
	
	public static void assertEndTokens(final String lineEnd, int offset,
			final RLexer lexer) {
		switch (lineEnd) {
		case "":
		case " ":
			break;
		case "\n":
		case "\r":
		case "\r\n":
			assertNextToken(RTerminal.LINEBREAK, offset, lineEnd.length(), lexer);
			break;
		case "+":
			assertNextToken(RTerminal.PLUS, offset, lineEnd.length(), lexer);
			break;
		}
		offset+= lineEnd.length();
		
		assertNextToken(RTerminal.EOF, 0, offset, 0, null, lexer);
	}
	
	
	public static void assertRegion(final int startOffset, final int length,
			final @Nullable TextRegion actual) {
		assertRegionStartEnd(startOffset, startOffset + length, actual);
	}
	
	public static void assertRegionStartEnd(final int startOffset, final int endOffset,
			final @Nullable TextRegion actual) {
		assertNotNull(actual);
		assertEquals(startOffset, actual.getStartOffset(), "textRegion.startOffset");
		assertEquals(endOffset, actual.getEndOffset(), "textRegion.endOffset");
	}
	
	public static void assertDetail(final int startOffset, final int length,
			final @Nullable String text,
			final @Nullable StatusDetail actual) {
		assertNotNull(actual);
		assertEquals(startOffset, actual.getStartOffset(), "detail.startOffset");
		assertEquals(startOffset + length, actual.getEndOffset(), "detail.endOffset");
		assertEquals(text, actual.getText(), "detail.text");
	}
	
	
	private RSourceTests() {
	}
	
}
