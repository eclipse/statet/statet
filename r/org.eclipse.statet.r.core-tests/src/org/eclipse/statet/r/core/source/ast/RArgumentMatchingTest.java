/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import static org.eclipse.statet.jcommons.collections.ImCollections.newIntList;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.r.core.model.RFunctionSpec;
import org.eclipse.statet.r.core.model.RFunctionSpec.Parameter;
import org.eclipse.statet.r.core.source.RTerminal;
import org.eclipse.statet.r.core.source.ast.RAsts.FCallArgMatch;


/**
 * Tests for
 *   {@link RAsts#matchArgs(FCall, RFunctionSpec)}
 *   {@link RAsts#matchArgs(FCall.Args, RFunctionSpec, ImList)}
 */
@NonNullByDefault
public class RArgumentMatchingTest {
	
	
	private static enum CallType {
		DEFAULT,
		PIPE_TARGET_INSERT,
		PIPE_TARGET_PLACEHOLDER
	}
	
	/** no match (other) */
	private static final int N= -1;
	/** DEFAULT match */
	private static final int D= 0;
	/** ELLIPSIS match */
	private static final int E= 1;
	
	
	private final RParser rParser= new RParser(ModelManager.MODEL_FILE);
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RArgumentMatchingTest() {
	}
	
	
	@Test
	public void Default_match_simple() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b");
		
		assertArgs(fSpec, "1, 2",				newIntList(0, 1));
		assertArgs(fSpec, "a=1, 2",				newIntList(0, 1));
		assertArgs(fSpec, "1, a=2",				newIntList(1, 0));
		assertArgs(fSpec, "b=1, 2",				newIntList(1, 0));
		assertArgs(fSpec, "a=1, b=2",			newIntList(0, 1));
		assertArgs(fSpec, "b=1, a=2",			newIntList(1, 0));
	}
	
	@Test
	public void Default_match_lessThanDef() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b");
		
		assertArgs(fSpec, "",					newIntList());
		assertArgs(fSpec, "1",					newIntList(0));
		assertArgs(fSpec, "a=1",				newIntList(0));
		assertArgs(fSpec, "b=2",				newIntList(1));
	}
	
	@Test
	public void Default_match_emptyArg1() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b");
		
		assertArgs(fSpec, ", 2",				newIntList(0, 1));
		assertArgs(fSpec, "a=, 2",				newIntList(0, 1));
		assertArgs(fSpec, ", a=2",				newIntList(1, 0));
		assertArgs(fSpec, "b=, 2",				newIntList(1, 0));
		assertArgs(fSpec, "a=, b=2",			newIntList(0, 1));
		assertArgs(fSpec, "b=, a=2",			newIntList(1, 0));
	}
	
	@Test
	public void Default_match_emptyArg2() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b");
		
		assertArgs(fSpec, "1, ",				newIntList(0, 1));
		assertArgs(fSpec, "a=1, ",				newIntList(0, 1));
		assertArgs(fSpec, "1, a=",				newIntList(1, 0));
		assertArgs(fSpec, "b=1, ",				newIntList(1, 0));
		assertArgs(fSpec, "a=1, b=",			newIntList(0, 1));
		assertArgs(fSpec, "b=1, a=",			newIntList(1, 0));
	}
	
	@Test
	public void Default_match_partialMatch() {
		final RFunctionSpec fSpec= new RFunctionSpec("aaa", "bbb", "bb");
		
		assertArgs(fSpec, "bbb=1, b=2, 3",		newIntList(1, 2, 0));
		assertArgs(fSpec, "bbb=1, b=2, b=3",	newIntList(1, 2, -1));
		assertArgs(fSpec, "b=1, b=2, bbb=3",	newIntList(2, -1, 1));
		assertArgs(fSpec, "bb=1, 2",			newIntList(2, 0));
		assertArgs(fSpec, "b=1, 2",				newIntList(-1, 0));
	}
	
	@Test
	public void Default_match_ellipsisAtEnd() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "...");
		
		assertArgs(fSpec, CallType.DEFAULT,
				"a=1, b=2, 3",		newIntList(0, 1, 2),	newIntList(D, D, E) );
		assertArgs(fSpec, CallType.DEFAULT,
				"a=1, b=2, 3, 4",	newIntList(0, 1, 2, 2), newIntList(D, D, E, E) );
		assertArgs(fSpec, CallType.DEFAULT,
				"a=1, 2, 3",		newIntList(0, 1, 2),	newIntList(D, D, E) );
		assertArgs(fSpec, CallType.DEFAULT,
				"1, b=2, 3",		newIntList(0, 1, 2), 	newIntList(D, D, E) );
		assertArgs(fSpec, CallType.DEFAULT,
				"",					newIntList(),			newIntList() );
		assertArgs(fSpec, CallType.DEFAULT,
				"a=1",				newIntList(0),			newIntList(D) );
		assertArgs(fSpec, CallType.DEFAULT,
				"x=1, y=2",			newIntList(2, 2),		newIntList(E, E) );
	}
	
	@Test
	public void Default_match_ellipsisAt0() {
		final RFunctionSpec fSpec= new RFunctionSpec("...", "b");
		
		assertArgs(fSpec, CallType.DEFAULT,
				"a=1, b=2, 3",		newIntList(0, 1, 0),	newIntList(E, D, E) );
		assertArgs(fSpec, CallType.DEFAULT,
				"1, b=2, 3",		newIntList(0, 1, 0), 	newIntList(E, D, E) );
		assertArgs(fSpec, CallType.DEFAULT,
				"1, 2, b=2",		newIntList(0, 0, 1), 	newIntList(E, E, D) );
		assertArgs(fSpec, CallType.DEFAULT,
				"a=1, 2, 3",		newIntList(0, 0, 0),	newIntList(E, E, E) );
		assertArgs(fSpec, CallType.DEFAULT,
				"",					newIntList(),			newIntList() );
		assertArgs(fSpec, CallType.DEFAULT,
				"x=1, y=2",			newIntList(0, 0),		newIntList(E, E) );
	}
	
	
	@Test
	public void PipeTarget_match_simple() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "c");
		
		assertArgs_PipeTargetInsert(fSpec, "1, 2",				newIntList(0, 1, 2));
		assertArgs_PipeTargetInsert(fSpec, "b=1, c=2",			newIntList(0, 1, 2));
		assertArgs_PipeTargetInsert(fSpec, "a=1, 2",			newIntList(1, 0, 2));
		assertArgs_PipeTargetInsert(fSpec, "1, a=2",			newIntList(1, 2, 0));
		assertArgs_PipeTargetInsert(fSpec, "1, b=2",			newIntList(0, 2, 1));
		assertArgs_PipeTargetInsert(fSpec, "b=1, 2",			newIntList(0, 1, 2));
		assertArgs_PipeTargetInsert(fSpec, "c=1, 2",			newIntList(0, 2, 1));
		assertArgs_PipeTargetInsert(fSpec, "a=1, b=2",			newIntList(2, 0, 1));
		assertArgs_PipeTargetInsert(fSpec, "b=1, a=2",			newIntList(2, 1, 0));
		assertArgs_PipeTargetInsert(fSpec, "c=1, b=2",			newIntList(0, 2, 1));
	}
	
	@Test
	public void PipeTarget_match_simple_withPlaceholder() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "c");
		
		assertArgs_PipeTargetPlaceholder(fSpec, "a=_, 2, 3",	newIntList(0, 1, 2));
		assertArgs_PipeTargetPlaceholder(fSpec, "b=1, c=2, a=_", newIntList(1, 2, 0));
		assertArgs_PipeTargetPlaceholder(fSpec, "a=1, 2, _",	newIntList(0, 1, 2)); // invalid use
	}
	
	@Test
	public void PipeTarget_match_lessThanDef() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "c");
		
		assertArgs_PipeTargetInsert(fSpec, "",					newIntList(0));
		assertArgs_PipeTargetInsert(fSpec, "1",					newIntList(0, 1));
		assertArgs_PipeTargetInsert(fSpec, "b=1",				newIntList(0, 1));
		assertArgs_PipeTargetInsert(fSpec, "a=1",				newIntList(1, 0));
		assertArgs_PipeTargetInsert(fSpec, "c=1",				newIntList(0, 2));
	}
	
	@Test
	public void PipeTarget_match_lessThanDef_withPlaceholder() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "c");
		
		assertArgs_PipeTargetPlaceholder(fSpec, "a=_",			newIntList(0));
		assertArgs_PipeTargetPlaceholder(fSpec, "b=_",			newIntList(1));
		assertArgs_PipeTargetPlaceholder(fSpec, "a=_, b=2",		newIntList(0, 1));
		assertArgs_PipeTargetPlaceholder(fSpec, "a=_, 2",		newIntList(0, 1));
		assertArgs_PipeTargetPlaceholder(fSpec, "1, a=_",		newIntList(1, 0));
		assertArgs_PipeTargetPlaceholder(fSpec, "b=1, c=_",		newIntList(1, 2));
		assertArgs_PipeTargetPlaceholder(fSpec, "c=_, a=2",		newIntList(2, 0));
	}
	
	@Test
	public void PipeTarget_match_emptyArg1() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "c");
		
		assertArgs_PipeTargetInsert(fSpec, ", 2",				newIntList(0, 1, 2));
		assertArgs_PipeTargetInsert(fSpec, "b=, 2",				newIntList(0, 1, 2));
		assertArgs_PipeTargetInsert(fSpec, "a=, 2",				newIntList(1, 0, 2));
		assertArgs_PipeTargetInsert(fSpec, ", a=2",				newIntList(1, 2, 0));
		assertArgs_PipeTargetInsert(fSpec, "c=, 2",				newIntList(0, 2, 1));
		assertArgs_PipeTargetInsert(fSpec, "a=, b=2",			newIntList(2, 0, 1));
		assertArgs_PipeTargetInsert(fSpec, "b=, a=2",			newIntList(2, 1, 0));
	}
	
	@Test
	public void PipeTarget_match_emptyArg2() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "c");
		
		assertArgs_PipeTargetInsert(fSpec, "1, ",				newIntList(0, 1, 2));
		assertArgs_PipeTargetInsert(fSpec, "b=1, ",				newIntList(0, 1, 2));
		assertArgs_PipeTargetInsert(fSpec, "a=1, ",				newIntList(1, 0, 2));
		assertArgs_PipeTargetInsert(fSpec, "1, a=",				newIntList(1, 2, 0));
		assertArgs_PipeTargetInsert(fSpec, "b=1, ",				newIntList(0, 1, 2));
		assertArgs_PipeTargetInsert(fSpec, "c=1, ",				newIntList(0, 2, 1));
		assertArgs_PipeTargetInsert(fSpec, "a=1, b=",			newIntList(2, 0, 1));
		assertArgs_PipeTargetInsert(fSpec, "b=1, a=",			newIntList(2, 1, 0));
		assertArgs_PipeTargetInsert(fSpec, "c=1, a=",			newIntList(1, 2, 0));
	}
	
	@Test
	public void PipeTarget_match_partialMatch() {
		final RFunctionSpec fSpec= new RFunctionSpec("aaa", "bbb", "bb", "c");
		
		assertArgs_PipeTargetInsert(fSpec, "bbb=1, b=2, 3",		newIntList(0, 1, 2, 3));
		assertArgs_PipeTargetInsert(fSpec, "bbb=1, b=2, b=3",	newIntList(0, 1, 2, -1));
		assertArgs_PipeTargetInsert(fSpec, "b=1, b=2, bbb=3",	newIntList(0, 2, -1, 1));
		assertArgs_PipeTargetInsert(fSpec, "b=1, 2",			newIntList(0, -1, 1));
		assertArgs_PipeTargetInsert(fSpec, "a=1, 2",			newIntList(1, 0, 2));
		assertArgs_PipeTargetInsert(fSpec, "a=1, b=2",			newIntList(1, 0, -1));
	}
	
	@Test
	public void PipeTarget_match_partialMatch_withPlaceholder() {
		final RFunctionSpec fSpec= new RFunctionSpec("aaa", "bbb", "bb");
		
		assertArgs_PipeTargetPlaceholder(fSpec, "bbb=1, b=_, 3",	newIntList(1, 2, 0));
		assertArgs_PipeTargetPlaceholder(fSpec, "bb=_, 2",			newIntList(2, 0));
		assertArgs_PipeTargetPlaceholder(fSpec, "b=_, 2",			newIntList(-1, 0));
	}
	
	@Test
	public void PipeTarget_match_ellipsisAtEnd() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "...");
		
		assertArgs(fSpec, CallType.PIPE_TARGET_INSERT,
				"a=1, b=2, 3",		newIntList(2, 0, 1, 2),	newIntList(E, D, D, E) );
		assertArgs(fSpec, CallType.PIPE_TARGET_INSERT,
				"a=1, b=2, 3, 4",	newIntList(2, 0, 1, 2, 2), newIntList(E, D, D, E, E) );
		assertArgs(fSpec, CallType.PIPE_TARGET_INSERT,
				"a=1, 2, 3",		newIntList(1, 0, 2, 2),	newIntList(D, D, E, E) );
		assertArgs(fSpec, CallType.PIPE_TARGET_INSERT,
				"1, b=2, 3", 		newIntList(0, 2, 1, 2),	newIntList(D, E, D, E) );
		assertArgs(fSpec, CallType.PIPE_TARGET_INSERT,
				"", 				newIntList(0),			newIntList(D) );
		assertArgs(fSpec, CallType.PIPE_TARGET_INSERT,
				"a=1",				newIntList(1, 0),		newIntList(D, D) );
		assertArgs(fSpec, CallType.PIPE_TARGET_INSERT,
				"x=1, y=2",			newIntList(0, 2, 2),	newIntList(D, E, E) );
	}
	
	@Test
	public void PipeTarget_match_ellipsisAtEnd_withPlaceholder() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "...");
		
		assertArgs(fSpec, CallType.PIPE_TARGET_PLACEHOLDER,
				"a=1, b=2, x=_",	newIntList(0, 1, 2),	newIntList(D, D, E) );
		assertArgs(fSpec, CallType.PIPE_TARGET_PLACEHOLDER,
				"a=1, b=2, x=_, 4", newIntList(0, 1, 2, 2), newIntList(D, D, E, E) );
		assertArgs(fSpec, CallType.PIPE_TARGET_PLACEHOLDER,
				"a=1, b=2, 3, y=_", newIntList(0, 1, 2, 2), newIntList(D, D, E, E) );
		assertArgs(fSpec, CallType.PIPE_TARGET_PLACEHOLDER,
				"x=2, y=_, z=", 	newIntList(2, 2, 2), 	newIntList(E, E, E) );
		assertArgs(fSpec, CallType.PIPE_TARGET_PLACEHOLDER,
				"a=1, b=_",			newIntList(0, 1),		newIntList(D, D) );
	}
	
	
	@Test
	public void ReplCall_match_simple() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "value", "b");
		
		assertArgs_ReplCall(fSpec, "1, 2",				newIntList(0, 2));
		assertArgs_ReplCall(fSpec, "a=1, 2",			newIntList(0, 2));
		assertArgs_ReplCall(fSpec, "1, a=2",			newIntList(2, 0));
		assertArgs_ReplCall(fSpec, "b=1, 2",			newIntList(0, 2));
		assertArgs_ReplCall(fSpec, "a=1, b=2",			newIntList(0, 2));
		assertArgs_ReplCall(fSpec, "b=1, a=2",			newIntList(2, 0));
	}
	
	@Test
	public void ReplCall_match_noValueArg() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b");
		
		assertArgs_ReplCall(fSpec, "1, 2",				newIntList(0, 1), N);
		assertArgs_ReplCall(fSpec, "a=1, 2",			newIntList(0, 1), N);
		assertArgs_ReplCall(fSpec, "1, a=2",			newIntList(1, 0), N);
		assertArgs_ReplCall(fSpec, "b=1, 2",			newIntList(0, 1), N);
		assertArgs_ReplCall(fSpec, "a=1, b=2",			newIntList(0, 1), N);
		assertArgs_ReplCall(fSpec, "b=1, a=2",			newIntList(1, 0), N);
	}
	
	@Test
	public void ReplCall_match_lessThanDef() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "value");
		
		assertArgs_ReplCall(fSpec, "1",					newIntList(0));
		assertArgs_ReplCall(fSpec, "a=1",				newIntList(0));
		assertArgs_ReplCall(fSpec, "b=2",				newIntList(0));
	}
	
	@Test
	public void ReplCall_match_moreThanDef() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "value");
		
		assertArgs_ReplCall(fSpec, "1, 2",				newIntList(0, -1),		newIntList(D, N));
	}
	
	@Test
	public void ReplCall_match_emptyArg1() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "value");
		
		assertArgs_ReplCall(fSpec, ", 2",				newIntList(0, 1));
		assertArgs_ReplCall(fSpec, "a=, 2",				newIntList(0, 1));
		assertArgs_ReplCall(fSpec, ", a=2",				newIntList(1, 0));
		assertArgs_ReplCall(fSpec, "b=, 2",				newIntList(0, 1));
		assertArgs_ReplCall(fSpec, "a=, b=2",			newIntList(0, 1));
		assertArgs_ReplCall(fSpec, "b=, a=2",			newIntList(1, 0));
	}
	
	@Test
	public void ReplCall_match_emptyArg2() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "value");
		
		assertArgs_ReplCall(fSpec, "1, ",				newIntList(0, 1));
		assertArgs_ReplCall(fSpec, "a=1, ",				newIntList(0, 1));
		assertArgs_ReplCall(fSpec, "1, a=",				newIntList(1, 0));
		assertArgs_ReplCall(fSpec, "b=1, ",				newIntList(0, 1));
		assertArgs_ReplCall(fSpec, "a=1, b=",			newIntList(0, 1));
		assertArgs_ReplCall(fSpec, "b=1, a=",			newIntList(1, 0));
	}
	
	@Test
	public void ReplCall_match_partialMatch() {
		final RFunctionSpec fSpec= new RFunctionSpec("aaa", "a", "value", "b");
		
		assertArgs_ReplCall(fSpec, "a=1, aa=2, 3",	newIntList(1, 0, 3));
	}
	
	@Test
	public void ReplCall_match_ellipsisAtEnd() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "value", "b", "...");
		
		assertArgs_ReplCall(fSpec, "a=1, b=2, 3",		newIntList(0, 2, 3),	newIntList(D, D, E));
		assertArgs_ReplCall(fSpec, "a=1, b=2, 3, 4",	newIntList(0, 2, 3, 3), newIntList(D, D, E, E));
		assertArgs_ReplCall(fSpec, "a=1, 2, 3",			newIntList(0, 2, 3),	newIntList(D, D, E));
		assertArgs_ReplCall(fSpec, "1, b=2, 3",			newIntList(0, 2, 3), 	newIntList(D, D, E));
		assertArgs_ReplCall(fSpec, "",					newIntList(),			newIntList());
		assertArgs_ReplCall(fSpec, "a=1",				newIntList(0),			newIntList(D));
		assertArgs_ReplCall(fSpec, "x=1, y=2",			newIntList(0, 3),		newIntList(D, E));
	}
	
	@Test
	public void ReplCall_match_ellipsisAtEnd_noValueArg() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "...");
		
		assertArgs_ReplCall(fSpec, "a=1, b=2, 3",		newIntList(0, 1, 2),	newIntList(D, D, E), E);
		assertArgs_ReplCall(fSpec, "a=1, b=2, 3, 4",	newIntList(0, 1, 2, 2), newIntList(D, D, E, E), E);
		assertArgs_ReplCall(fSpec, "a=1, 2, 3",			newIntList(0, 1, 2),	newIntList(D, D, E), E);
		assertArgs_ReplCall(fSpec, "1, b=2, 3",			newIntList(0, 1, 2), 	newIntList(D, D, E), E);
		assertArgs_ReplCall(fSpec, "",					newIntList(),			newIntList(), E);
		assertArgs_ReplCall(fSpec, "a=1",				newIntList(0),			newIntList(D), E);
		assertArgs_ReplCall(fSpec, "x=1, y=2",			newIntList(0, 2),		newIntList(D, E), E);
	}
	
	
	@Test
	public void AutoContext_match() {
		final RFunctionSpec fSpec= new RFunctionSpec("a", "b", "...");
		final RFunctionSpec assignFSpec = new RFunctionSpec("a", "b", "value", "...");
		
		assertArgs_AutoContext(fSpec, (FCall)parseExpr("f(c= 3, 1)"), newIntList(2, 0));
		assertArgs_AutoContext(fSpec, (FCall)parseExpr("x <- f(c= 3, 1)").getChild(1), newIntList(2, 0));
		
		assertArgs_AutoContext(fSpec, (FCall)parseExpr("0 |> f(c= 3, 1)").getChild(1), newIntList(0, 2, 1));
		assertArgs_AutoContext(fSpec, (FCall)parseExpr("0 |> (\\(a, b, ...){})(c= 3, 1)").getChild(1), newIntList(0, 2, 1));
		assertArgs_AutoContext(fSpec, (FCall)parseExpr("0 |> f(3, d=_ )").getChild(1), newIntList(0, 2));
		
		assertArgs_AutoContext(fSpec, (FCall)parseExpr("f(x, c= 3, 1) <- 1").getChild(0), newIntList(0, 2, 1));
		assertArgs_AutoContext(assignFSpec, (FCall)parseExpr("f(x, c= 3, 1) <- 1").getChild(0), newIntList(0, 3, 1));
		assertArgs_AutoContext(assignFSpec, (FCall)parseExpr("f(x, c= 3, 1) = 1").getChild(0), newIntList(0, 3, 1));
		assertArgs_AutoContext(assignFSpec, (FCall)parseExpr("1 -> f(x, c= 3, 1)").getChild(1), newIntList(0, 3, 1));
	}
	
	
	private FCall.Args parseArgs(final String code) {
		return nonNullAssert(this.rParser.parseFCallArgs(this.input.reset(code).init(), true));
	}
	
	private RAstNode parseExpr(final String code) {
		return nonNullAssert(this.rParser.parseExpr(this.input.reset(code).init()));
	}
	
	private ImIntList createMatchTypes(final ImIntList expectedDefIdx) {
		final int[] matchTypes= new int[expectedDefIdx.size()];
		for (int i= 0; i < matchTypes.length; i++) {
			matchTypes[i]= (expectedDefIdx.getAt(i) >= 0) ? D : N;
		}
		return ImCollections.newIntList(matchTypes);
	}
	
	private void assertArgs(final RFunctionSpec fSpec,
			final CallType callType, final String code,
			final ImIntList expectedDefIdxs, final ImIntList expectedMatchTypes) {
		final FCall.Args callArgs= parseArgs(code);
		final RAstNode sourceNode= new NumberConst(RTerminal.NUM_INT);
		final FCallArgMatch matchedArgs;
		int callArgIdxOffset= 0;
		int placeholderCallArgIdx= -1;
		switch (callType) {
		case PIPE_TARGET_INSERT:
			matchedArgs= RAsts.matchArgs(callArgs, fSpec,
					null, ImCollections.newList(sourceNode) );
			callArgIdxOffset= 1;
			break;
		case PIPE_TARGET_PLACEHOLDER:
			matchedArgs= RAsts.matchArgs(callArgs, fSpec,
					null, ImCollections.newList(sourceNode) );
			for (final FCall.Arg arg : callArgs.getArgChildren()) {
				final RAstNode argValueNode= arg.getValueChild();
				if (argValueNode != null && argValueNode.getNodeType() == NodeType.PLACEHOLDER) {
					placeholderCallArgIdx= callArgs.getChildIndex(arg);
					break;
				}
			}
			break;
		default:
			matchedArgs= RAsts.matchArgs(callArgs, fSpec,
					null, ImCollections.emptyList() );
			break;
		}
		
		assertEquals(fSpec, matchedArgs.getFunctionSpec());
		
		assertArrayEquals(expectedDefIdxs.toArray(), matchedArgs.argsNode2paramIdx());
		
		// methods by callArgIdx
		int ellipsisCount= 0;
		int nonmatchingCount= 0;
		for (int callArgIdx= 0; callArgIdx < expectedDefIdxs.size(); callArgIdx++) {
			final Parameter expectedParameter;
			final FCall.Arg actualArg;
			final int expectedDefArgIdx= expectedDefIdxs.getAt(callArgIdx);
			if (expectedDefArgIdx >= 0) {
				expectedParameter= fSpec.getParam(expectedDefArgIdx);
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					continue;
				case E:
					actualArg= matchedArgs.ellipsisArgs[ellipsisCount++];
					break;
				default:
					fail();
					continue;
				}
			}
			else {
				expectedParameter= null;
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case N:
					actualArg= matchedArgs.otherArgs[nonmatchingCount++];
					break;
				default:
					fail();
					continue;
				}
			}
			
			if (callType == CallType.PIPE_TARGET_INSERT && callArgIdx == 0) {
				assertEquals(expectedParameter, matchedArgs.getParamForInject(0));
				assertEquals(sourceNode, actualArg.getValueChild());
				continue;
			}
			assertEquals(expectedParameter, matchedArgs.getParamForFCall(callArgIdx - callArgIdxOffset));
			final FCall.Arg expectedArg= callArgs.getChild(callArgIdx - callArgIdxOffset);
			if (callType == CallType.PIPE_TARGET_PLACEHOLDER && callArgIdx == placeholderCallArgIdx) {
				assertEquals(expectedArg.getNameChild(), actualArg.getNameChild());
				assertEquals(sourceNode, actualArg.getValueChild());
				continue;
			}
			assertEquals(expectedArg, actualArg);
			continue;
		}
		
		assertNull(matchedArgs.getParamForInject(-1));
		assertNull(matchedArgs.getParamForFCall(-1));
		
		assertEquals(ellipsisCount, matchedArgs.ellipsisArgs.length);
		assertEquals(nonmatchingCount, matchedArgs.otherArgs.length);
		
		// methods by paramIdx
		for (int paramIdx= 0; paramIdx < fSpec.getParamCount(); paramIdx++) {
			final int callArgIdx= expectedDefIdxs.indexOf(paramIdx);
			if (callArgIdx >= 0){
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					if (callType == CallType.PIPE_TARGET_INSERT && callArgIdx == 0) {
						assertNull(matchedArgs.getArgNode(paramIdx).getNameChild());
						assertEquals(sourceNode, matchedArgs.getArgNode(paramIdx).getValueChild());
						assertEquals(sourceNode, matchedArgs.getArgValueNode(paramIdx));
						break;
					}
					final FCall.Arg expectedArg= callArgs.getChild(callArgIdx - callArgIdxOffset);
					if (callType == CallType.PIPE_TARGET_PLACEHOLDER && callArgIdx == placeholderCallArgIdx) {
						assertEquals(expectedArg.getNameChild(), matchedArgs.getArgNode(paramIdx).getNameChild());
						assertEquals(sourceNode, matchedArgs.getArgNode(paramIdx).getValueChild());
						assertEquals(sourceNode, matchedArgs.getArgValueNode(paramIdx));
						break;
					}
					assertEquals(expectedArg, matchedArgs.getArgNode(paramIdx));
					assertEquals(expectedArg.getValueChild(), matchedArgs.getArgValueNode(paramIdx));
					continue;
				case E:
					assertNull(matchedArgs.getArgNode(paramIdx));
					assertNull(matchedArgs.getArgValueNode(paramIdx));
					continue;
				default:
					fail();
					continue;
				}
			}
			else {
				assertNull(matchedArgs.getArgNode(paramIdx));
				assertNull(matchedArgs.getArgValueNode(paramIdx));
				continue;
			}
		}
		
		assertNull(matchedArgs.getArgNode(-1));
		assertNull(matchedArgs.getArgValueNode(-1));
	}
	
	private void assertArgs(final RFunctionSpec fSpec, final String code,
			final ImIntList expectedDefIdxs) {
		assertArgs(fSpec, CallType.DEFAULT, code,
				expectedDefIdxs, createMatchTypes(expectedDefIdxs) );
	}
	
	private void assertArgs_PipeTargetInsert(final RFunctionSpec fSpec, final String code,
			final ImIntList expectedDefIdxs) {
		assertArgs(fSpec, CallType.PIPE_TARGET_INSERT, code,
				expectedDefIdxs, createMatchTypes(expectedDefIdxs) );
	}
	
	private void assertArgs_PipeTargetPlaceholder(final RFunctionSpec fSpec, final String code,
			final ImIntList expectedDefIdxs) {
		assertArgs(fSpec, CallType.PIPE_TARGET_PLACEHOLDER, code,
				expectedDefIdxs, createMatchTypes(expectedDefIdxs) );
	}
	
	private void assertArgs_ReplCall(final RFunctionSpec fSpec, final String code,
			final ImIntList expectedDefIdxs, final ImIntList expectedMatchTypes, final int expectedValueType) {
		final FCall.Args callArgs= parseArgs(code);
		final RAstNode replValueNode= new NumberConst(RTerminal.NUM_INT);
		final FCallArgMatch matchedArgs= RAsts.matchArgs(callArgs, fSpec,
				replValueNode, ImCollections.emptyList() );
		final int valueParamIdx= fSpec.indexOfParam("value");
		
		assertEquals(fSpec, matchedArgs.getFunctionSpec());
		
		assertArrayEquals(expectedDefIdxs.toArray(), matchedArgs.argsNode2paramIdx());
		
		// methods by callArgIdx
		int ellipsisCount= 0;
		int nonmatchingCount= 0;
		for (int callArgIdx= 0; callArgIdx < expectedDefIdxs.size(); callArgIdx++) {
			final FCall.Arg expectedArg= callArgs.getChild(callArgIdx);
			final int expectedDefArgIdx= expectedDefIdxs.getAt(callArgIdx);
			if (expectedDefArgIdx >= 0) {
				assertEquals(fSpec.getParam(expectedDefArgIdx), matchedArgs.getParamForFCall(callArgIdx));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case D:
					break;
				case E:
					assertEquals(expectedArg, matchedArgs.ellipsisArgs[ellipsisCount++]);
					break;
				default:
					fail();
					break;
				}
			}
			else {
				assertNull(matchedArgs.getParamForFCall(callArgIdx));
				
				switch (expectedMatchTypes.getAt(callArgIdx)) {
				case N:
					assertEquals(expectedArg, matchedArgs.otherArgs[nonmatchingCount++]);
					break;
				default:
					fail();
					break;
				}
			}
		}
		switch (expectedValueType) {
		case E:
			ellipsisCount++;
			break;
		case N:
			nonmatchingCount++;
			break;
		}
		
		assertNull(matchedArgs.getParamForInject(-1));
		assertNull(matchedArgs.getParamForFCall(-1));
		
		assertEquals(ellipsisCount, matchedArgs.ellipsisArgs.length, "ellipsisCount");
		assertEquals(nonmatchingCount, matchedArgs.otherArgs.length, "failCount");
		
		// methods by paramIdx
		for (int paramIdx= 0; paramIdx < fSpec.getParamCount(); paramIdx++) {
			final int expectedDefArgIdx= expectedDefIdxs.indexOf(paramIdx);
			if (expectedDefArgIdx >= 0){
				final FCall.Arg expectedArg= callArgs.getChild(expectedDefArgIdx);
				switch (expectedMatchTypes.getAt(expectedDefArgIdx)) {
				case D:
					assertEquals(expectedArg, matchedArgs.getArgNode(paramIdx));
					assertEquals(expectedArg.getValueChild(), matchedArgs.getArgValueNode(paramIdx));
					break;
				case E:
					assertNull(matchedArgs.getArgNode(paramIdx));
					assertNull(matchedArgs.getArgValueNode(paramIdx));
					break;
				default:
					fail();
					break;
				}
			}
			else if (paramIdx == valueParamIdx) {
				assertEquals(replValueNode, matchedArgs.getArgValueNode(paramIdx));
			}
			else {
				assertNull(matchedArgs.getArgNode(paramIdx));
				assertNull(matchedArgs.getArgValueNode(paramIdx));
			}
		}
		
		switch (expectedValueType) {
		case E:
			assertEquals(replValueNode, matchedArgs.ellipsisArgs[ellipsisCount - 1].getValueChild());
			break;
		case N:
			assertEquals(replValueNode, matchedArgs.otherArgs[nonmatchingCount - 1].getValueChild());
			break;
		default:
			break;
		}
		
		assertNull(matchedArgs.getArgNode(-1));
		assertNull(matchedArgs.getArgValueNode(-1));
	}
	
	private void assertArgs_ReplCall(final RFunctionSpec fSpec, final String code,
			final ImIntList expectedDefIdxs, final ImIntList expectedMatchTypes) {
		assertArgs_ReplCall(fSpec, code,
				expectedDefIdxs, expectedMatchTypes, D );
	}
	
	private void assertArgs_ReplCall(final RFunctionSpec fSpec, final String code,
			final ImIntList expectedDefIdxs, final int expectedValueType) {
		assertArgs_ReplCall(fSpec, code,
				expectedDefIdxs, createMatchTypes(expectedDefIdxs), expectedValueType );
	}
	
	private void assertArgs_ReplCall(final RFunctionSpec fSpec, final String code,
			final ImIntList expectedParamIdxs) {
		assertArgs_ReplCall(fSpec, code,
				expectedParamIdxs, createMatchTypes(expectedParamIdxs), D );
	}
	
	private void assertArgs_AutoContext(final RFunctionSpec fSpec, final FCall fCallNode,
			final ImIntList expectedDefIdxs) {
		final FCallArgMatch matchedArgs= RAsts.matchArgs(fCallNode, fSpec);
		
		assertArrayEquals(expectedDefIdxs.toArray(), matchedArgs.argsNode2paramIdx());
	}
	
}
