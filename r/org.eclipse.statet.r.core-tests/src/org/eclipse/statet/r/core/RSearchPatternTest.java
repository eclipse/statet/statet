/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.jcommons.text.core.SearchPattern.PREFIX_MATCH;
import static org.eclipse.statet.jcommons.text.core.SearchPattern.SUBSTRING_MATCH;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.SearchPattern;


@NonNullByDefault
public class RSearchPatternTest {
	// Copy of org.eclipse.statet.jcommons.text.core.SearchPatternTests
	
	
	public RSearchPatternTest() {
	}
	
	
	protected RSearchPattern createPattern(final int rules) {
		return new RSearchPattern(rules, "");
	}
	
	
	@Test
	public void matches_Prefix() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("a"));
		assertEquals(0, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
	}
	
	@Test
	public void matches_Prefix_CaseInsensitive() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(0, pattern.matches("xAbcxabc"));
		
		pattern.setPattern("A");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(0, pattern.matches("xAbcxAbc"));
		assertEquals(0, pattern.matches("xABcxABc"));
		assertEquals(0, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("Ab");
		assertEquals(0, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xAbcxAbc"));
		assertEquals(0, pattern.matches("xABcxABc"));
		assertEquals(0, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("aB");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xAbcxAbc"));
		assertEquals(0, pattern.matches("xABcxABc"));
		assertEquals(0, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("Abc"));
		assertEquals(0, pattern.matches("aBc"));
		assertEquals(0, pattern.matches("abC"));
		assertEquals(0, pattern.matches("xAbcxAbc"));
		assertEquals(0, pattern.matches("xaBcxaBc"));
		assertEquals(0, pattern.matches("xabCxabC"));
	}
	
	@Test
	public void matches_Prefix_StartingDot() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("");
		assertEquals(0, pattern.matches(".a"));
		
		pattern.setPattern(".");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("."));
		assertEquals(PREFIX_MATCH, pattern.matches(".a"));
		
		pattern.setPattern("a");
		assertEquals(0, pattern.matches(".a"));
		assertEquals(0, pattern.matches(".abc"));
		assertEquals(0, pattern.matches(".xabcxabc"));
		assertEquals(0, pattern.matches(".xbc"));
		
		pattern.setPattern(".a");
		assertEquals(0, pattern.matches("a"));
		assertEquals(0, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		assertEquals(PREFIX_MATCH, pattern.matches(".a"));
		assertEquals(PREFIX_MATCH, pattern.matches(".abc"));
		assertEquals(0, pattern.matches(".xabcxabc"));
		assertEquals(0, pattern.matches(".xbc"));
		assertEquals(0, pattern.matches("a."));
		assertEquals(0, pattern.matches("a.bc"));
		assertEquals(0, pattern.matches("x.abcxabc"));
		assertEquals(0, pattern.matches("x.bc"));
	}
	
	@Test
	public void matches_Prefix_InnerDotAndUnderscore() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a.bc"));
		assertEquals(0, pattern.matches("x.abcxabc"));
		assertEquals(0, pattern.matches("x.bc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_bc"));
		assertEquals(0, pattern.matches("x_abcxabc"));
		assertEquals(0, pattern.matches("x_bc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a.bc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ab.c"));
		assertEquals(0, pattern.matches("x.abcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		assertEquals(0, pattern.matches("a_"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_bc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ab_c"));
		assertEquals(0, pattern.matches("x_abcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("a"));
		assertEquals(0, pattern.matches("a.bc"));
		assertEquals(0, pattern.matches("a.b.c"));
		assertEquals(0, pattern.matches("x.a.b.c.x.a.b.c"));
		assertEquals(0, pattern.matches("xbc"));
		assertEquals(0, pattern.matches("a_bc"));
		assertEquals(0, pattern.matches("a_b.c"));
		assertEquals(0, pattern.matches("x_a_b_c_x_a_b_c"));
		assertEquals(0, pattern.matches("xbc"));
	}
	
	@Test
	public void matches_Prefix_EndingDotAndUnderscore() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("");
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a_"));
		
		pattern.setPattern("a.");
		assertEquals(0, pattern.matches(".a"));
		assertEquals(0, pattern.matches(".abc"));
		assertEquals(0, pattern.matches(".xabcxabc"));
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a.bc"));
		assertEquals(0, pattern.matches("xa.bcxabc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_bc"));
		assertEquals(0, pattern.matches("xa_bcxabc"));
		
		pattern.setPattern("a_");
		assertEquals(0, pattern.matches(".a"));
		assertEquals(0, pattern.matches(".abc"));
		assertEquals(0, pattern.matches(".xabcxabc"));
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a.bc"));
		assertEquals(0, pattern.matches("xa.bcxabc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_bc"));
		assertEquals(0, pattern.matches("xa_bcxabc"));
	}
	
	@Test
	public void matches_Substring() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xbc"));
		assertEquals(0, pattern.matches("xbxc"));
	}
	
	@Test
	public void matches_Substring_CaseInsensitive() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxabc"));
		
		pattern.setPattern("A");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxAbc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xABcxABc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("Ab");
		assertEquals(0, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxAbc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xABcxABc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("aB");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxAbc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xABcxABc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("bc");
		assertEquals(SUBSTRING_MATCH, pattern.matches("Abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("aBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("abC"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxAbc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xaBcxaBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabCxabC"));
	}
	
	@Test
	public void matches_Substring_StartingDot() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("");
		assertEquals(0, pattern.matches(".a"));
		
		pattern.setPattern(".");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("."));
		assertEquals(PREFIX_MATCH, pattern.matches(".a"));
		
		pattern.setPattern("a");
		assertEquals(SUBSTRING_MATCH, pattern.matches(".a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches(".abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches(".xabcxabc"));
		assertEquals(0, pattern.matches(".xbc"));
		
		pattern.setPattern(".a");
		assertEquals(0, pattern.matches("a"));
		assertEquals(0, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		assertEquals(PREFIX_MATCH, pattern.matches(".a"));
		assertEquals(PREFIX_MATCH, pattern.matches(".abc"));
		assertEquals(0, pattern.matches(".xabcxabc"));
		assertEquals(0, pattern.matches(".xbc"));
		assertEquals(0, pattern.matches("a."));
		assertEquals(0, pattern.matches("a.bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("x.abcxabc"));
		assertEquals(0, pattern.matches("x.bc"));
	}
	
	@Test
	public void matches_Substring_InnerDotAndUnderscore() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a.bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("x.abcxabc"));
		assertEquals(0, pattern.matches("x.bc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("x_abcxabc"));
		assertEquals(0, pattern.matches("x_bc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a.bc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ab.c"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("x.abcx.abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa.bcxa.bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xab.cxab.c"));
		assertEquals(0, pattern.matches("xbc"));
		assertEquals(0, pattern.matches("a_"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_bc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ab_c"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("x_abcx_abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa_bcxa_bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xab_cxab_c"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("a.bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("a.b.c"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("x.a.b.c."));
		assertEquals(SUBSTRING_MATCH, pattern.matches("a_bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("a_b.c"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("x_a_b_c_"));
	}
	
	@Test
	public void matches_Substring_EndingDotAndUnderscore() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("");
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a_"));
		
		pattern.setPattern("a.");
		assertEquals(0, pattern.matches(".a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches(".abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches(".xabcxabc"));
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a.bc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_bc"));
		assertEquals(0, pattern.matches("xa"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa."));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa.bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa_"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa_bc"));
		
		pattern.setPattern("a_");
		assertEquals(0, pattern.matches(".a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches(".abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches(".xabcxabc"));
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a."));
		assertEquals(PREFIX_MATCH, pattern.matches("a.bc"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_"));
		assertEquals(PREFIX_MATCH, pattern.matches("a_bc"));
		assertEquals(0, pattern.matches("xa"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa."));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa.bc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa_"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xa_bc"));
	}
	
	
	@Test
	public void getMatchingRegions_Prefix() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("");
		assertNull(
				pattern.getMatchingRegions("a", PREFIX_MATCH) );
		assertNull(
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Prefix_CaseInsensitive() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("A", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("Abc", PREFIX_MATCH) );
		
		pattern.setPattern("A");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("Abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("aBc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("ABc", PREFIX_MATCH) );
		
		pattern.setPattern("Ab");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("aBc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("ABc", PREFIX_MATCH) );
		
		pattern.setPattern("aB");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("Abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("ABc", PREFIX_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Prefix_StartingDot() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern(".");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions(".", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions(".a", PREFIX_MATCH) );
		
		pattern.setPattern(".a");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions(".a", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions(".abc", PREFIX_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Prefix_InnerDotAndUnderscore() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a.", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a.bc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a_", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a_bc", PREFIX_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 0, 3 },
				pattern.getMatchingRegions("a.bc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("ab.c", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 3 },
				pattern.getMatchingRegions("a_bc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("ab_c", PREFIX_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Prefix_EndingDotAndUnderscore() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("");
		assertNull(
				pattern.getMatchingRegions("a.", PREFIX_MATCH) );
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a.", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a_", PREFIX_MATCH) );
		
		pattern.setPattern("a.");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("a.", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("a.bc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a_", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a_bc", PREFIX_MATCH) );
		
		pattern.setPattern("a_");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a.", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a.bc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("a_", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("a_bc", PREFIX_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Substring() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("bc");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 6, 8 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xbc", SUBSTRING_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Substring_CaseInsensitive() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xAbcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxAbc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xAbcxAbc", SUBSTRING_MATCH) );
		
		pattern.setPattern("A");
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xAbcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxAbc", SUBSTRING_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xAbcxaBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xABcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("Ab");
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xAbcxaBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xABcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("aB");
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xAbcxaBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xABcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("bc");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 6, 8 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xbc", SUBSTRING_MATCH) );
		
		pattern.setPattern("Bc");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("abC", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 6, 8 },
				pattern.getMatchingRegions("xabcxabC", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xbC", SUBSTRING_MATCH) );
		
		pattern.setPattern("bC");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("aBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 6, 8 },
				pattern.getMatchingRegions("xabcxaBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xBc", SUBSTRING_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Substring_StartingDot() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 1, 2 },
				pattern.getMatchingRegions(".a", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2 },
				pattern.getMatchingRegions(".abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 3, 6, 7 },
				pattern.getMatchingRegions(".xabcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern(".a");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("x.abcxabc", SUBSTRING_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Substring_InnerDotAndUnderscore() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 2, 3, 6, 7 },
				pattern.getMatchingRegions("x.abcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 3, 6, 7 },
				pattern.getMatchingRegions("x_abcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 2, 4, 7, 9 },
				pattern.getMatchingRegions("x.abcx.abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 4, 6, 9 },
				pattern.getMatchingRegions("xa.bcxa.bc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3, 6, 8 },
				pattern.getMatchingRegions("xab.cxab.c", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 7, 9 },
				pattern.getMatchingRegions("x_abcx_abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 4, 6, 9 },
				pattern.getMatchingRegions("xa_bcxa_bc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3, 6, 8 },
				pattern.getMatchingRegions("xab_cxab_c", SUBSTRING_MATCH) );
		
		pattern.setPattern("bc");
		assertArrayEquals(new int[] { 2, 4 },
				pattern.getMatchingRegions("a.bc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 5 },
				pattern.getMatchingRegions("a.b.c", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 4, 7 },
				pattern.getMatchingRegions("x.a.b.c.", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4 },
				pattern.getMatchingRegions("a_bc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 5 },
				pattern.getMatchingRegions("a_b.c", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 4, 7 },
				pattern.getMatchingRegions("x_a_b_c_", SUBSTRING_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Substring_EndingDotAndUnderscore() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a.");
		assertArrayEquals(new int[] { 1, 2 },
				pattern.getMatchingRegions(".abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 3, 6, 7 },
				pattern.getMatchingRegions(".xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xa.", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xa.bc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2 },
				pattern.getMatchingRegions("xa_", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2 },
				pattern.getMatchingRegions("xa_bc", SUBSTRING_MATCH) );
		
		pattern.setPattern("a_");
		assertArrayEquals(new int[] { 1, 2 },
				pattern.getMatchingRegions(".abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 3, 6, 7 },
				pattern.getMatchingRegions(".xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2 },
				pattern.getMatchingRegions("xa.", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2 },
				pattern.getMatchingRegions("xa.bc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xa_", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xa_bc", SUBSTRING_MATCH) );
	}
	
}
