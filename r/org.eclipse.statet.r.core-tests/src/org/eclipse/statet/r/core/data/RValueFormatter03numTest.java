/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Locale;

import org.junit.jupiter.api.Test;


public class RValueFormatter03numTest {
	
	
	private final RValueFormatter formatter= new RValueFormatter();
	
	
	public RValueFormatter03numTest() {
	}
	
	
	@Test
	public void print() {
		this.formatter.clear();
		this.formatter.appendNum(0);
		assertEquals("0.0", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(1);
		assertEquals("1.0", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(10);
		assertEquals("10.0", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(10000);
		assertEquals("10000.0", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(100000);
		assertEquals("1.0e+5", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(1e40);
		assertEquals("1.0e+40", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(0.1);
		assertEquals("0.1", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(0.001);
		assertEquals("0.001", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(0.0001);
		assertEquals("0.0001", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(0.00001);
		assertEquals("1.0e-5", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(1e-40);
		assertEquals("1.0e-40", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(0.00009999999999999);
		assertEquals("9.999999999999e-5", this.formatter.getString());
	}
	
	@Test
	public void printNegative() {
		this.formatter.clear();
		this.formatter.appendNum(-0.0);
		assertEquals("-0.0", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(-1);
		assertEquals("-1.0", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(-1e15);
		assertEquals("-1.0e+15", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(-1e-15);
		assertEquals("-1.0e-15", this.formatter.getString());
	}
	
	@Test
	public void checkPrecision() {
		final double[] values= new double[] {
				0.000000000000001,
				1.0000000000000011,
				1.0000000000000012,
				1.0000000000000013,
				4.0/3,
				Double.MIN_VALUE
		};
		for (int i= 0; i < values.length; i++) {
			final int index0= i;
			
			this.formatter.clear();
			this.formatter.appendNum(values[i]);
			final String expected= Double.toString(values[i]).toLowerCase(Locale.ROOT);
			assertEquals(expected, this.formatter.getString(), () -> String.format("formatted strings differ for sample [%1$s]", index0));
		}
	}
	
	@Test
	public void printSpecial() {
		this.formatter.clear();
		this.formatter.appendNum(Double.NaN);
		assertEquals("NaN", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(Double.POSITIVE_INFINITY);
		assertEquals("Inf", this.formatter.getString());
		
		this.formatter.clear();
		this.formatter.appendNum(Double.NEGATIVE_INFINITY);
		assertEquals("-Inf", this.formatter.getString());
	}
	
}
