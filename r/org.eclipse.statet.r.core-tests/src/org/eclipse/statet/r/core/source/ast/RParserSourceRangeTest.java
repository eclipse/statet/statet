/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ltk.model.core.ModelManager;


@NonNullByDefault
public class RParserSourceRangeTest {
	
	
	private final RParser rParser= new RParser(ModelManager.MODEL_FILE);
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RParserSourceRangeTest() {
	}
	
	
	@Test
	public void scan() {
		assertAst("x", 0, 1, false);
		assertAst(" x ", 1, 2, false);
		assertAst("\n\n x\n\n", 3, 4, false);
		assertAst("  ", 0, 0, false);
	}
	
	@Test
	public void scan_expand() {
		assertAst("x", 0, 1, true);
		assertAst(" x ", 0, 3, true);
		assertAst("\tx ", 0, 3, true);
		assertAst("\n\n x\n\n", 0, 6, true);
		assertAst("  ", 0, 2, true);
	}
	
	private void assertAst(final String code, final int startOffset, final int endOffset,
			final boolean expand) {
		final SourceComponent root= this.rParser.parseSourceFragment(this.input.reset(code).init(),
				null, expand );
		assertEquals(startOffset, root.getStartOffset(), "root.startOffset");
		assertEquals(endOffset, root.getEndOffset(), "root.endOffset");
	}
	
}
