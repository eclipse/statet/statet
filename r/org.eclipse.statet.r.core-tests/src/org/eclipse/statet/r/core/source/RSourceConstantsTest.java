/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.util.Version;


@NonNullByDefault
public class RSourceConstantsTest {
	
	
	public RSourceConstantsTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void getLangVersionsSince_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			RSourceConstants.getLangVersionsSince(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			RSourceConstants.getLangVersionsSince(new Version("100.0.0"));
		});
	}
	
	@Test
	public void getLangVersionsSince() {
		final var limit= RSourceConstants.LANG_VERSION_4_1;
		
		final var expected= new ArrayList<Version>();
		for (final Version version : RSourceConstants.LANG_VERSIONS) {
			if (version.compareTo(limit) >= 0) {
				expected.add(version);
			}
		}
		assertEquals(expected, RSourceConstants.getLangVersionsSince(limit));
	}
	
	@Test
	@SuppressWarnings("null")
	public void getLangVersionsBefore_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			RSourceConstants.getLangVersionsSince(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			RSourceConstants.getLangVersionsSince(new Version("100.0.0"));
		});
	}
	
	@Test
	public void getLangVersionsBefore() {
		final var limit= RSourceConstants.LANG_VERSION_4_2;
		
		final var expected= new ArrayList<Version>();
		for (final Version version : RSourceConstants.LANG_VERSIONS) {
			if (version.compareTo(limit) < 0) {
				expected.add(version);
			}
		}
		assertEquals(expected, RSourceConstants.getLangVersionsBefore(limit));
	}
	
}
