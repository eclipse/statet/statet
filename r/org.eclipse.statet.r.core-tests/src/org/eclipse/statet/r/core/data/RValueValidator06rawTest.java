/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.data;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;


public class RValueValidator06rawTest extends AbstractRValueValidatorTest {
	
	
	public RValueValidator06rawTest() {
		super(DefaultRObjectFactory.RAW_STRUCT_DUMMY);
	}
	
	
	@Override
	protected boolean isValidSource(final byte type) {
		return (type == RStore.LOGICAL);
	}
	
	@Override
	protected boolean isValidSourceNA(final byte type) {
		return false;
	}
	
	
	@Test
	@Override
	public void parseInt() {
		testValidate(true, "0L");
		testValidate(true, "255L");
		testValidate(false, "-1L");
		testValidate(false, "256L");
		testValidate(false, "NA_integer_");
	}
	
	
	@Test
	@Override
	public void parseNum() {
		testValidate(true, "0");
		testValidate(true, "a0");
		testValidate(true, "FF");
		testValidate(false, "100");
		testValidate(false, "255");
		testValidate(false, "-1");
		testValidate(true, "0x00");
		testValidate(true, "0x7F");
		testValidate(false, "0x100");
		testValidate(false, "0x7FFFFFF");
		testValidate(false, "0x7FFFFFFFFL");
		testValidate(false, "-1.55");
		testValidate(false, "-1.0e-5");
		testValidate(false, "NA_real_");
	}
	
	
}
