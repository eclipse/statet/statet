/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.r.core.source.RSourceTests.assertRegionStartEnd;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ADD_MINUS;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ADD_PLUS;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ASSIGN_EQUALS;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ASSIGN_LEFT_D;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ASSIGN_LEFT_S;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ASSIGN_RIGHT_D;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ASSIGN_RIGHT_S;
import static org.eclipse.statet.r.core.source.ast.RAstTests.HELP_QUESTIONMARK;
import static org.eclipse.statet.r.core.source.ast.RAstTests.MULT_DIV;
import static org.eclipse.statet.r.core.source.ast.RAstTests.MULT_MULT;
import static org.eclipse.statet.r.core.source.ast.RAstTests.PIPE_FORWARD;
import static org.eclipse.statet.r.core.source.ast.RAstTests.STRING_D;
import static org.eclipse.statet.r.core.source.ast.RAstTests.STRING_RAW;
import static org.eclipse.statet.r.core.source.ast.RAstTests.STRING_S;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SYMBOL_G;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SYMBOL_STD;
import static org.eclipse.statet.r.core.source.ast.RAstTests.assertExpr0Node;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.ast.RAstTests.NodeDescr;


@NonNullByDefault
public class RParserBasicTest extends AbstractAstNodeTest {
	
	
	static final ImList<NodeDescr> COMMON_OPS= ImCollections.newList(
				ADD_PLUS, ADD_MINUS,
				MULT_MULT, MULT_DIV,
				PIPE_FORWARD );
	
	static final ImList<NodeDescr> ASSIGN_OPS= ImCollections.newList(
				ASSIGN_LEFT_S, ASSIGN_LEFT_D,
				ASSIGN_RIGHT_S, ASSIGN_RIGHT_S,
				ASSIGN_EQUALS );
	
	
	public RParserBasicTest() {
	}
	
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void SYMBOL(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("abc",
						 0,  3, SYMBOL_STD,	0, "abc" );
		assertRegionStartEnd(0,  3, expr0.getTextRegion());
		
		expr0= assertExpr("`abc\\ndef`",
						 0, 10, SYMBOL_G,	0, "abc\ndef" );
		assertRegionStartEnd(1,  9, expr0.getTextRegion());
	}
	
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void STRING(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("\"abc\\ndef\"",
						 0, 10, STRING_D,	0, "abc\ndef" );
		assertRegionStartEnd(1,  9, expr0.getTextRegion());
		
		expr0= assertExpr("\'abc\\ndef\'",
						 0, 10, STRING_S,	0, "abc\ndef" );
		assertRegionStartEnd(1,  9, expr0.getTextRegion());
		
		expr0= assertExpr("r\"(abc\ndef)\"",
						 0, 12, STRING_RAW,	0, "abc\ndef" );
		assertRegionStartEnd(3, 10, expr0.getTextRegion());
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void STRING_withError(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("r\"--", 			  0,  4, STRING_RAW);
		assertNull(expr0.getText());
		assertNull(expr0.getTextRegion());
	}
	
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void ADD(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x + y",
						 0,  5, ADD_PLUS,	0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 4,  5, SYMBOL_STD,	0, "y",		expr0, new int[] { 1 });
		
		expr0= assertExpr("x - y",
						 0,  5, ADD_MINUS,	0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 4,  5, SYMBOL_STD,	0, "y",		expr0, new int[] { 1 });
		
		expr0= assertExpr("x + y + z",
						 0,  9, ADD_PLUS,	0, null);
		assertExpr0Node( 0,  5, ADD_PLUS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 4,  5, SYMBOL_STD,	0, "y",		expr0, new int[] { 0, 1 });
		assertExpr0Node( 8,  9, SYMBOL_STD,	0, "z",		expr0, new int[] { 1 });
		
		expr0= assertExpr("x + y - z",
						 0,  9, ADD_MINUS,	0, null );
		assertExpr0Node( 0,  5, ADD_PLUS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 4,  5, SYMBOL_STD,	0, "y",		expr0, new int[] { 0, 1 });
		assertExpr0Node( 8,  9, SYMBOL_STD,	0, "z",		expr0, new int[] { 1 });
		
		expr0= assertExpr("x - y - z",
						 0,  9, ADD_MINUS,	0, null );
		assertExpr0Node( 0,  5, ADD_MINUS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 4,  5, SYMBOL_STD,	0, "y",		expr0, new int[] { 0, 1 });
		assertExpr0Node( 8,  9, SYMBOL_STD,	0, "z",		expr0, new int[] { 1 });
		
		expr0= assertExpr("x - y + z",
						 0,  9, ADD_PLUS,	0, null );
		assertExpr0Node( 0,  5, ADD_MINUS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 4,  5, SYMBOL_STD,	0, "y",		expr0, new int[] { 0, 1 });
		assertExpr0Node( 8,  9, SYMBOL_STD,	0, "z",		expr0, new int[] { 1 });
		
		assertTwoOps_differentPrio(ADD_PLUS, MULT_MULT);
		assertTwoOps_differentPrio(ADD_PLUS, MULT_DIV);
		assertTwoOps_differentPrio(ADD_MINUS, MULT_MULT);
		assertTwoOps_differentPrio(ADD_MINUS, MULT_DIV);
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void ADD_withErrorInChild(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("`\\u` + x",
						 0,  8, ADD_PLUS,	ERROR_IN_CHILD, null );
		
		expr0= assertExpr("x + `\\u`",
						 0,  8, ADD_PLUS,	ERROR_IN_CHILD, null );
	}
	
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void HELP(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("? y",
						 0,  3, HELP_QUESTIONMARK, 0, null );
		assertEquals(1, expr0.getChildCount());
		assertExpr0Node( 2,  3, SYMBOL_STD,	0, "y",		expr0, new int[] { 0 });
		
		expr0= assertExpr("? y + z",
						 0,  7, HELP_QUESTIONMARK, 0, null );
		assertEquals(1, expr0.getChildCount());
		assertExpr0Node( 2,  7, ADD_PLUS,	0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 2,  3, SYMBOL_STD,	0, "y",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 6,  7, SYMBOL_STD,	0, "z",		expr0, new int[] { 0, 1 });
		
		expr0= assertExpr("x ? y",
						 0,  5, HELP_QUESTIONMARK, 0, null );
		assertEquals(2, expr0.getChildCount());
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 4,  5, SYMBOL_STD,	0, "y",		expr0, new int[] { 1 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void HELP_commonOps(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		for (final var opDescr : COMMON_OPS) {
			assertTwoOps_differentPrio(HELP_QUESTIONMARK, opDescr);
		}
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void HELP_assignOps(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		for (final var opDescr : ASSIGN_OPS) {
			assertTwoOps_differentPrio(HELP_QUESTIONMARK, opDescr);
		}
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void ASSIGN_commonOps(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		for (final var opDescr : COMMON_OPS) {
			assertTwoOps_differentPrio(ASSIGN_LEFT_S, opDescr);
			assertTwoOps_differentPrio(ASSIGN_LEFT_D, opDescr);
			assertTwoOps_differentPrio(ASSIGN_RIGHT_S, opDescr);
			assertTwoOps_differentPrio(ASSIGN_RIGHT_D, opDescr);
			assertTwoOps_differentPrio(ASSIGN_EQUALS, opDescr);
		}
	}
	
}
