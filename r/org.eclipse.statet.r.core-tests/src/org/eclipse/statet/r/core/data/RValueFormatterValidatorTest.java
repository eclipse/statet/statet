/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;

import com.ibm.icu.lang.UCharacter;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RComplexStore;
import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RLogicalStore;
import org.eclipse.statet.rj.data.RNumericStore;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RRawStore;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;


public class RValueFormatterValidatorTest {
	
	
	private final RObjectFactory rObjectFactory= DefaultRObjectFactory.INSTANCE;
	
	private final RValueFormatter formatter= new RValueFormatter();
	
	private final RValueValidator validator= new RValueValidator();
	
	
	public RValueFormatterValidatorTest() {
	}
	
	
	@Test
	public void reparse_01logi() {
		final RLogicalStore store= this.rObjectFactory.createLogiData(2);
		store.setLogi(0, true);
		store.setLogi(1, false);
		
		for (int i= 0; i < store.getLength(); i++) {
			final int index0= i;
			final String fString= this.formatter.format(store, i);
//			this.formatter.clear();
//			this.formatter.appendLogi(store.getNum(i));
//			final String aString= this.formatter.getString();
			
			final RStore<?> vData= this.validator.toRData(store, fString);
			
			assertNotNull(vData, () -> String.format("null for sample [%1$s]", index0));
			assertEquals(false, vData.isNA(0), () -> String.format("isNA differ for sample [%1$s]", index0));
			assertEquals(store.getLogi(i), vData.getLogi(0), () -> String.format("logi values differ for sample [%1$s]", index0));
			
//			assertEquals(fString, aString, () -> String.format("formatted strings differ for sample [%1$s]", index0));
		}
	}
	
	@Test
	public void reparse_01logi_NA() {
		final RLogicalStore store= this.rObjectFactory.createLogiData(1);
		store.setNA(0);
		
		final String fString= this.formatter.format(store, 0);
		this.formatter.clear();
		this.formatter.appendNA();
		final String aString= this.formatter.getString();
		
		final RStore<?> vData= this.validator.toRData(store, fString);
		assertEquals(true, vData.isNA(0));
		
		assertEquals(fString, aString);
	}
	
	@Test
	public void reparse_04int() {
		final RIntegerStore store= this.rObjectFactory.createIntData(new int[] {
				0,
				1,
				-1,
				0xFFFF,
				Integer.MAX_VALUE,
				Integer.MIN_VALUE + 1,
		});
		
		for (int i= 0; i < store.getLength(); i++) {
			final int index0= i;
			final String fString= this.formatter.format(store, i);
//			this.formatter.clear();
//			this.formatter.appendInt(store.getNum(i));
//			final String aString= this.formatter.getString();
			
			final RStore<?> vData= this.validator.toRData(store, fString);
			
			assertNotNull(vData, () -> String.format("null for sample [%1$s]", index0));
			assertEquals(false, vData.isNA(0), () -> String.format("isNA differ for sample [%1$s]", index0));
			assertEquals(store.getNum(i), vData.getNum(0), 0, () -> String.format("num values differ for sample [%1$s]", index0));
			
//			assertEquals(fString, aString);
			
//			assertEquals(fString, aString, () -> String.format("formatted strings differ for sample [%1$s]", index0));
		}
	}
	
	@Test
	public void reparse_02int_NA() {
		final RIntegerStore store= this.rObjectFactory.createIntData(1);
		store.setNA(0);
		
		final String fString= this.formatter.format(store, 0);
		this.formatter.clear();
		this.formatter.appendNA();
		final String aString= this.formatter.getString();
		
		final RStore<?> vData= this.validator.toRData(store, fString);
		assertEquals(true, vData.isNA(0));
		
		assertEquals(fString, aString);
	}
	
	@Test
	public void reparse_03num() {
		final RNumericStore store= this.rObjectFactory.createNumData(new double[] {
				0.0,
				-0.0,
				1.0,
				1e15,
				1e-15,
				-1e+15,
				-1e-15,
				1234567890.0,
				0.000000000000001,
				1.0000000000000011,
				1.0000000000000012,
				1.0000000000000013,
				4.0/3,
				Double.MAX_VALUE,
				Double.MIN_VALUE,
				Double.POSITIVE_INFINITY,
				Double.NEGATIVE_INFINITY,
				Double.NaN,
		});
		
		for (int i= 0; i < store.getLength(); i++) {
			final int index0= i;
			final String fString= this.formatter.format(store, i);
			this.formatter.clear();
			this.formatter.appendNum(store.getNum(i));
			final String aString= this.formatter.getString();
			
			final RStore<?> vData= this.validator.toRData(store, fString);
			
			assertNotNull(vData, () -> String.format("null for sample [%1$s]", index0));
			assertEquals(false, vData.isNA(0), () -> String.format("isNA differ for sample [%1$s]", index0));
			assertEquals(store.getNum(i), vData.getNum(0), 0, () -> String.format("num values differ for sample [%1$s]", index0));
			
			assertEquals(fString, aString, () -> String.format("formatted strings differ for sample [%1$s]", index0));
		}
	}
	
	@Test
	public void reparse_03num_NA() {
		final RNumericStore store= this.rObjectFactory.createNumData(1);
		store.setNA(0);
		
		final String fString= this.formatter.format(store, 0);
		this.formatter.clear();
		this.formatter.appendNA();
		final String aString= this.formatter.getString();
		
		final RStore<?> vData= this.validator.toRData(store, fString);
		assertEquals(true, vData.isNA(0));
		
		assertEquals(fString, aString);
	}
	
	@Test
	public void reparse_04num_NA() {
		final RComplexStore store= this.rObjectFactory.createCplxData(1);
		store.setNA(0);
		
		final String fString= this.formatter.format(store, 0);
		this.formatter.clear();
		this.formatter.appendNA();
		final String aString= this.formatter.getString();
		
		final RStore<?> vData= this.validator.toRData(store, fString);
		assertEquals(true, vData.isNA(0));
		
		assertEquals(fString, aString);
	}
	
	@Test
	public void reparse_05chr() {
		final char[] chars= new char[0xFFFF + 4];
		Arrays.fill(chars, 'X');
		chars[0]= 'X';
		for (int i= 1; i < 0xFFFF; i++) {
			if (UCharacter.isBMP(i)) {
				chars[i]= (char) i;
			}
			else {
				chars[i]= 'X';
			}
		}
		chars[0xFFFF + 0]= '\uD834';
		chars[0xFFFF + 1]= '\uDD73';
		chars[0xFFFF + 2]= '\uD834';
		chars[0xFFFF + 3]= '\uDD1E';
		final String s= new String(chars);
		
		final RCharacterStore store= this.rObjectFactory.createCharData(1);
		store.setChar(0, s);
		final String fString= this.formatter.format(store, 0);
		this.formatter.clear();
		this.formatter.appendStringD(s);
		final String aString= this.formatter.getString();
		
		final RStore<?> vData= this.validator.toRData(DefaultRObjectFactory.CHR_STRUCT_DUMMY, fString);
		assertEquals(false, vData.isNA(0));
		assertEquals(s, vData.getChar(0));
		
		assertEquals(fString, aString);
	}
	
	@Test
	public void reparse_05chr_NA() {
		final RCharacterStore store= this.rObjectFactory.createCharData(1);
		store.setNA(0);
		
		final String fString= this.formatter.format(store, 0);
		this.formatter.clear();
		this.formatter.appendNA();
		final String aString= this.formatter.getString();
		
		final RStore<?> vData= this.validator.toRData(store, fString);
		assertEquals(true, vData.isNA(0));
		
		assertEquals(fString, aString);
	}
	
	@Test
	public void reparse_06raw() {
		final RRawStore store= this.rObjectFactory.createRawData(1);
		for (int i= 0; i <= 0xFF; i++) {
			final int index0= i;
			final byte b= (byte) i;
			store.setRaw(0, b);
			final String fString= this.formatter.format(store, 0);
			this.formatter.clear();
			this.formatter.appendRaw(b);
			final String aString= this.formatter.getString();
			
			final RStore<?> vData= this.validator.toRData(DefaultRObjectFactory.RAW_STRUCT_DUMMY, fString);
			
			assertNotNull(vData, () -> String.format("null for sample [%1$s]", index0));
			assertEquals(false, vData.isNA(0), () -> String.format("isNA differ for sample [%1$s]", index0));
			assertEquals(b, vData.getRaw(0), () -> String.format("raw values differ for sample [%1$s]", index0));
			
			assertEquals(fString, aString, () -> String.format("formatted strings differ for sample [%1$s]", index0));
		}
	}
	
}
