/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.SUBSEQUENT;
import static org.eclipse.statet.r.core.source.RSourceConstants.CTX12_PIPE;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TOKEN_MULTI_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TOKEN_UNNAMED_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ADD_PLUS;
import static org.eclipse.statet.r.core.source.ast.RAstTests.ERROR_TERM;
import static org.eclipse.statet.r.core.source.ast.RAstTests.FCALL;
import static org.eclipse.statet.r.core.source.ast.RAstTests.FCALL_ARG;
import static org.eclipse.statet.r.core.source.ast.RAstTests.MULT_DIV;
import static org.eclipse.statet.r.core.source.ast.RAstTests.MULT_MULT;
import static org.eclipse.statet.r.core.source.ast.RAstTests.NUM_NUM;
import static org.eclipse.statet.r.core.source.ast.RAstTests.PIPE_FORWARD;
import static org.eclipse.statet.r.core.source.ast.RAstTests.PIPE_PLACEHOLDER;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SEQ;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SUB_INDEXED_ARG;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SUB_INDEXED_D;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SUB_INDEXED_S;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SUB_NAMED_PART;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SUB_NAMED_SLOT;
import static org.eclipse.statet.r.core.source.ast.RAstTests.SYMBOL_STD;
import static org.eclipse.statet.r.core.source.ast.RAstTests.assertExpr0Node;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.RSourceConstants;


@NonNullByDefault
public class RParserPipeTest extends AbstractAstNodeTest {
	
	
	public RParserPipeTest() {
	}
	
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_1")
	public void PipeForward_basic(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> f()",
						 0,  8, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		
		expr0= assertExpr("x |> f1() |> f2()",
						 0, 17, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  9, PIPE_FORWARD, 0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 5,  9, FCALL,		0, null,	expr0, new int[] { 0, 1 });
		assertExpr0Node( 5,  7, SYMBOL_STD,	0, "f1",	expr0, new int[] { 0, 1, 0 });
		assertExpr0Node(13, 17, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node(13, 15, SYMBOL_STD,	0, "f2",	expr0, new int[] { 1, 0 });
		
		expr0= assertExpr("f1() |> f2()",
						 0, 12, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  4, FCALL,		0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  2, SYMBOL_STD,	0, "f1",	expr0, new int[] { 0, 0 });
		assertExpr0Node( 8, 12, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 8, 10, SYMBOL_STD,	0, "f2",	expr0, new int[] { 1, 0 });
		
		assertTwoOps_differentPrio(PIPE_FORWARD, SEQ);
		assertTwoOps_differentPrio(MULT_MULT, PIPE_FORWARD);
		assertTwoOps_differentPrio(MULT_DIV, PIPE_FORWARD);
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_1")
	public void PipeForward_withArgs(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> f(b= 1)",
						 0, 12, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, FCALL_ARG,	0, null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, NUM_NUM,	0, "1",		expr0, new int[] { 1, 1, 0, 1 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_2")
	public void PipeForward_withPlaceholder_FCallArg(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> f(b= _)",
						 0, 12, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, FCALL_ARG,	0, null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 0, 1 }, new int[] { 0 });
		
		expr0= assertExpr("x |> f(b= 1, c= _)",
						 0, 18, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 18, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, FCALL_ARG,	0, null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node(13, 17, FCALL_ARG,	0, null,	expr0, new int[] { 1, 1, 1 });
		assertExpr0Node(13, 14, SYMBOL_STD,	0, "c",		expr0, new int[] { 1, 1, 1, 0 });
		assertExpr0Node(16, 17, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 1, 1, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 1, 1 }, new int[] { 0 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_2")
	public void PipeForward_withPlaceholder_SubArg(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> y[b= _]",
						 0, 12, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, SUB_INDEXED_S, 0,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "y",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, SUB_INDEXED_ARG, 0,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 0, 1 }, new int[] { 0 });
		
		expr0= assertExpr("x |> y[b= 1, c= _]",
						 0, 18, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 18, SUB_INDEXED_S, 0,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "y",		expr0, new int[] { 1, 0 });
		assertExpr0Node(13, 17, SUB_INDEXED_ARG, 0,
											   null,	expr0, new int[] { 1, 1, 1 });
		assertExpr0Node(13, 14, SYMBOL_STD,	0, "c",		expr0, new int[] { 1, 1, 1, 0 });
		assertExpr0Node(16, 17, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 1, 1, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 1, 1 }, new int[] { 0 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_3")
	public void PipeForward_withPlaceholder_SubRef(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> _[1]",
						 0,  9, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  9, SUB_INDEXED_S, 0,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SUB_INDEXED_ARG, 0,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, NUM_NUM,	0, "1",		expr0, new int[] { 1, 1, 0, 0 });
		assertPlaceholderDetail(expr0, new int[] { 1, 0 }, new int[] { 0 });
		
		expr0= assertExpr("x |> _[[1]]",
						 0, 11, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 11, SUB_INDEXED_D, 0,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 8,  9, NUM_NUM,	0, "1",		expr0, new int[] { 1, 1, 0, 0 });
		assertPlaceholderDetail(expr0, new int[] { 1, 0 }, new int[] { 0 });
		
		expr0= assertExpr("x |> _$a",
						 0,  8, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, SUB_NAMED_PART, 0,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "a",		expr0, new int[] { 1, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 0 }, new int[] { 0 });
		
		expr0= assertExpr("x |> _@a",
						 0,  8, PIPE_FORWARD, 0, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, SUB_NAMED_SLOT, 0,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "a",		expr0, new int[] { 1, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 0 }, new int[] { 0 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_1")
	public void PipeForward_missingTarget(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> ;",
				0,  4, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 4,  4, ERROR_TERM,	TYPE12_SYNTAX_FCALL_AFTER_OP_MISSING | CTX12_PIPE,
											null,		expr0, new int[] { 1 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_1")
	public void PipeForward_invalidTarget(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> f",
				0,  6, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE,
											   "f",		expr0, new int[] { 1 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_before_4_1")
	public void PipeForward_incompatible(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> f()",
						 0,  8, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
								null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		
		expr0= assertExpr("x |> f1() |> f2()",
						 0, 17, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | ERROR_IN_CHILD,
								null );
		assertExpr0Node( 0,  9, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
								null,					expr0, new int[] { 0 });
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0, 0 });
		assertExpr0Node( 5,  9, FCALL,		0, null,	expr0, new int[] { 0, 1 });
		assertExpr0Node( 5,  7, SYMBOL_STD,	0, "f1",	expr0, new int[] { 0, 1, 0 });
		assertExpr0Node(13, 17, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node(13, 15, SYMBOL_STD,	0, "f2",	expr0, new int[] { 1, 0 });
		
		expr0= assertExpr("f1() |> f2()",
						 0, 12, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
								null );
		assertExpr0Node( 0,  4, FCALL,		0, null,	expr0, new int[] { 0 });
		assertExpr0Node( 0,  2, SYMBOL_STD,	0, "f1",	expr0, new int[] { 0, 0 });
		assertExpr0Node( 8, 12, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 8, 10, SYMBOL_STD,	0, "f2",	expr0, new int[] { 1, 0 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_2")
	public void PipePlaceholder_invalidUse(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("1 + _",
						 0,  5, ADD_PLUS,	ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, NUM_NUM,	0, "1",		expr0, new int[] { 0 });
		assertExpr0Node( 4,  5, PIPE_PLACEHOLDER, TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1 });
		assertPlaceholderDetail(expr0, new int[] { 1 }, null);
		
		expr0= assertExpr("f(b= _)",
						 0,  7, FCALL, 		ERROR_IN_CHILD, null );
		assertExpr0Node( 2,  6, FCALL_ARG,	ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 2,  3, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 0, 0 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 0, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 0, 1 }, null);
		
		expr0= assertExpr("y[b= _]",
						 0,  7, SUB_INDEXED_S, ERROR_IN_CHILD, null );
		assertExpr0Node( 2,  6, SUB_INDEXED_ARG, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 2,  3, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 0, 0 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 0, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 0, 1 }, null);
		
		expr0= assertExpr("x |> f(_)",
						 0,  9, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  9, FCALL,		ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, FCALL_ARG,	ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, PIPE_PLACEHOLDER, TYPE123_SYNTAX_TOKEN_UNNAMED_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 0, 0 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 0, 0 }, new int[] { 0 });
		
		expr0= assertExpr("x |> f(b= g(c= _))",
						 0, 18, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 18, FCALL,		ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 17, FCALL_ARG,	ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(12, 13, SYMBOL_STD,	0, "c",		expr0, new int[] { 1, 1, 0, 1, 1, 0, 0 });
		assertExpr0Node(15, 16, PIPE_PLACEHOLDER, TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 0, 1, 1, 0, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 0, 1, 1, 0, 1 }, null);
		
		expr0= assertExpr("x |> f(b= _, c= _)",
						 0, 18, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 18, FCALL,		ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, FCALL_ARG,	0, null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		assertExpr0Node(13, 17, FCALL_ARG,	ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 1 });
		assertExpr0Node(13, 14, SYMBOL_STD,	0, "c",		expr0, new int[] { 1, 1, 1, 0 });
		assertExpr0Node(16, 17, PIPE_PLACEHOLDER, TYPE123_SYNTAX_TOKEN_MULTI_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 1, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 0, 1 }, new int[] { 0 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 1, 1 }, null);
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_since_4_3")
	public void PipePlaceholder_invalidUse_since_4_3(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> _[b= _]",
						 0, 12, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, SUB_INDEXED_S, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, 0,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, SUB_INDEXED_ARG, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, TYPE123_SYNTAX_TOKEN_MULTI_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		assertPlaceholderDetail(expr0, new int[] { 1, 0 }, new int[] { 0 });
		assertPlaceholderDetail(expr0, new int[] { 1, 1, 0, 1 }, null);
	}
	
	@Test
	public void PipePlaceholder_incompatible_in_4_2() {
		this.rParser.setRSourceConfig(new RSourceConfig(RSourceConstants.LANG_VERSION_4_2));
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> _[1]",
						 0,  9, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  9, SUB_INDEXED_S, TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE | ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SUB_INDEXED_ARG, 0,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, NUM_NUM,	0, "1",		expr0, new int[] { 1, 1, 0, 0 });
		
		expr0= assertExpr("x |> _$a",
						 0,  8, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, SUB_NAMED_PART, TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE | ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "a",		expr0, new int[] { 1, 1 });
	}
	
	@Test
	public void PipePlaceholder_incompatible_in_4_1() {
		this.rParser.setRSourceConfig(new RSourceConfig(RSourceConstants.LANG_VERSION_4_1));
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> f(b= _)",
						 0, 12, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, FCALL,		ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, FCALL_ARG,	ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		
		expr0= assertExpr("x |> f(b= _, c= _)",
						 0, 18, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 18, FCALL,		ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, FCALL_ARG,	ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		assertExpr0Node(13, 17, FCALL_ARG,	ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 1 });
		assertExpr0Node(13, 14, SYMBOL_STD,	0, "c",		expr0, new int[] { 1, 1, 1, 0 });
		assertExpr0Node(16, 17, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 1, 1 });
		
		expr0= assertExpr("x |> _[1]",
						 0,  9, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  9, SUB_INDEXED_S, TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE | ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SUB_INDEXED_ARG, 0,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, NUM_NUM,	0, "1",		expr0, new int[] { 1, 1, 0, 0 });
		
		expr0= assertExpr("x |> _$a",
						 0,  8, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, SUB_NAMED_PART, TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE | ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "a",		expr0, new int[] { 1, 1 });
		
		expr0= assertExpr("x |> y[b= _]",
						 0, 12, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, SUB_INDEXED_S, TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE | ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "y",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, SUB_INDEXED_ARG, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		
		expr0= assertExpr("x |> y[b= 1, c= _]",
						 0, 18, PIPE_FORWARD, ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 18, SUB_INDEXED_S, TYPE12_SYNTAX_FCALL_AFTER_OP_EXPR_UNEXPECTED | CTX12_PIPE | ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "y",		expr0, new int[] { 1, 0 });
		assertExpr0Node(13, 17, SUB_INDEXED_ARG, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 1 });
		assertExpr0Node(13, 14, SYMBOL_STD,	0, "c",		expr0, new int[] { 1, 1, 1, 0 });
		assertExpr0Node(16, 17, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE,
											   null,	expr0, new int[] { 1, 1, 1, 1 });
	}
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_before_4_1")
	public void PipePlaceholder_incompatible_pre_4_1(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		RAstNode expr0;
		
		expr0= assertExpr("x |> f(b= 1)",
						 0, 12, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, FCALL,		0, null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, FCALL_ARG,	0, null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, NUM_NUM, 	0, "1",		expr0, new int[] { 1, 1, 0, 1 });
		
		expr0= assertExpr("x |> f(b= _)",
						 0, 12, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, FCALL,		ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "f",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, FCALL_ARG,	ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | SUBSEQUENT,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		
		expr0= assertExpr("x |> _[1]",
						 0,  9, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  9, SUB_INDEXED_S, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | SUBSEQUENT,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SUB_INDEXED_ARG, 0,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, NUM_NUM,	0, "1",		expr0, new int[] { 1, 1, 0, 0 });
		
		expr0= assertExpr("x |> _$a",
						 0,  8, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5,  8, SUB_NAMED_PART, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | SUBSEQUENT,
											   null,	expr0, new int[] { 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "a",		expr0, new int[] { 1, 1 });
		
		expr0= assertExpr("x |> y[b= _]",
						 0, 12, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 12, SUB_INDEXED_S, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "y",		expr0, new int[] { 1, 0 });
		assertExpr0Node( 7, 11, SUB_INDEXED_ARG, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 0 });
		assertExpr0Node( 7,  8, SYMBOL_STD,	0, "b",		expr0, new int[] { 1, 1, 0, 0 });
		assertExpr0Node(10, 11, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | SUBSEQUENT,
											   null,	expr0, new int[] { 1, 1, 0, 1 });
		
		expr0= assertExpr("x |> y[b= 1, c= _]",
						 0, 18, PIPE_FORWARD, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | ERROR_IN_CHILD, null );
		assertExpr0Node( 0,  1, SYMBOL_STD,	0, "x",		expr0, new int[] { 0 });
		assertExpr0Node( 5, 18, SUB_INDEXED_S, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1 });
		assertExpr0Node( 5,  6, SYMBOL_STD,	0, "y",		expr0, new int[] { 1, 0 });
		assertExpr0Node(13, 17, SUB_INDEXED_ARG, ERROR_IN_CHILD,
											   null,	expr0, new int[] { 1, 1, 1 });
		assertExpr0Node(13, 14, SYMBOL_STD,	0, "c",		expr0, new int[] { 1, 1, 1, 0 });
		assertExpr0Node(16, 17, PIPE_PLACEHOLDER, TYPE12_SYNTAX_UNSUPPORTED_IN_LANG_VERSION | CTX12_PIPE | SUBSEQUENT,
											   null,	expr0, new int[] { 1, 1, 1, 1 });
	}
	
	
	protected void assertPlaceholderDetail(final RAstNode expr0,
			final int [] expectedPlaceholderNodeIndex,
			final int @Nullable [] expectedSubstituteNodeIndex) {
		final Placeholder expectedPlaceholder= RAstTests.getExpr0Node(expr0,
				expectedPlaceholderNodeIndex, Placeholder.class );
		final RAstNode expectedSubstitute= (expectedSubstituteNodeIndex != null) ?
				RAstTests.getExpr0Node(expr0, expectedSubstituteNodeIndex, RAstNode.class) :
				null;
		
		final var attachments= expectedPlaceholder.getAttachments();
		int count= 0;
		for (int i= 0; i < attachments.size(); i++) {
			final var attachment= attachments.get(i);
			if (attachment instanceof final PlaceholderDetail detail) {
				assertSame(expectedPlaceholder, detail.getPlaceholder());
				assertSame(expectedSubstitute, detail.getSubstitute());
				count++;
			}
		}
		assertEquals((expectedSubstitute != null) ? 1 : 0, count);
	}
	
}
