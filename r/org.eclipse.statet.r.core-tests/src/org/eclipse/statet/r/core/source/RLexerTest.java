/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.junit.jupiter.api.Assertions.assertSame;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceTests.assertEndTokens;
import static org.eclipse.statet.r.core.source.RSourceTests.assertNextToken;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;


@NonNullByDefault
public class RLexerTest {
	
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RLexerTest() {
	}
	
	
	protected int getConfig() {
		return 0;
	}
	
	
	static ImList<String> lineEndings() {
		return ImCollections.newList("", "\n", "\r", "\r\n");
	}
	
	static ImList<String> opSeparators() {
		return ImCollections.newList(" ", "", "\n", "\r", "\r\n");
	}
	
	static ImList<String> sSeparators() {
		return ImCollections.newList(" ", "+", "", "\n", "\r", "\r\n");
	}
	
	
	@Test
	public void getInput() {
		final RLexer lexer= new RLexer();
		
		lexer.reset(this.input.reset("x <- rnorm(100);").init());
		assertSame(this.input, lexer.getInput());
	}
	
	
	@Test
	public void empty() {
		final RLexer lexer= new RLexer(getConfig());
		
		lexer.reset(this.input.reset("").init());
		assertNextToken(RTerminal.EOF, 0,
				0, 0,
				null,
				lexer );
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void matchWhitespace(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig());
		
		lexer.reset(this.input.reset("  \t" + lineEnd).init());
		assertNextToken(RTerminal.BLANK, 0, 3, lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void skipWhitespace(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset("  \t" + lineEnd).init());
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@Test
	public void matchLinebreak() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" \n ").init());
		assertNextToken(RTerminal.LINEBREAK, 1, 1, lexer);
		assertNextToken(RTerminal.EOF, 0, 3, 0, null, lexer);
	}
	
	@Test
	public void matchLinebreakWin() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" \r\n ").init());
		assertNextToken(RTerminal.LINEBREAK, 1, 2, lexer);
		assertNextToken(RTerminal.EOF, 0, 4, 0, null, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void matchComment(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset("  # comment" + lineEnd).init());
		assertNextToken(RTerminal.COMMENT, 2, 9, lexer);
		assertEndTokens(lineEnd, 11, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void matchRoxygenComment(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" #' comment" + lineEnd).init());
		assertNextToken(RTerminal.ROXYGEN_COMMENT, 1, 10, lexer);
		assertEndTokens(lineEnd, 11, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void skipComment(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE | RLexer.SKIP_COMMENT);
		
		lexer.reset(this.input.reset("  # comment" + lineEnd).init());
		assertEndTokens(lineEnd, 11, lexer);
		
		lexer.reset(this.input.reset(" #' comment" + lineEnd).init());
		assertEndTokens(lineEnd, 11, lexer);
	}
	
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchBlockOpen(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" {" + lineEnd).init());
		assertNextToken(RTerminal.BLOCK_OPEN, 1, RTerminal.S_BLOCK_OPEN.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchBlockClose(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" }" + lineEnd).init());
		assertNextToken(RTerminal.BLOCK_CLOSE, 1, RTerminal.S_BLOCK_CLOSE.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchGroupOpen(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" (" + lineEnd).init());
		assertNextToken(RTerminal.GROUP_OPEN, 1, RTerminal.S_GROUP_OPEN.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchGroupClose(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" )" + lineEnd).init());
		assertNextToken(RTerminal.GROUP_CLOSE, 1, RTerminal.S_GROUP_CLOSE.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSubIndexedSOpen(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" [" + lineEnd).init());
		assertNextToken(RTerminal.SUB_INDEXED_S_OPEN, 1, RTerminal.S_SUB_INDEXED_OPEN.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSubIndexedDOpen(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" [[" + lineEnd).init());
		assertNextToken(RTerminal.SUB_INDEXED_D_OPEN, 1, RTerminal.S_SUB_INDEXED_D_OPEN.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSubIndexedClose(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ]" + lineEnd).init());
		assertNextToken(RTerminal.SUB_INDEXED_CLOSE, 1, RTerminal.S_SUB_INDEXED_CLOSE.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSubNamedPart(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" $" + lineEnd).init());
		assertNextToken(RTerminal.SUB_NAMED_PART, 1, RTerminal.S_SUB_NAMED.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSubSlotPart(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" @" + lineEnd).init());
		assertNextToken(RTerminal.SUB_NAMED_SLOT, 1, RTerminal.S_SUB_AT.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchNsGet(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ::" + lineEnd).init());
		assertNextToken(RTerminal.NS_GET, 1, RTerminal.S_NS_GET.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchNsGetInt(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" :::" + lineEnd).init());
		assertNextToken(RTerminal.NS_GET_INT, 1, RTerminal.S_NS_GET_INT.length(), lexer);
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchPlus(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" +" + lineEnd).init());
		assertNextToken(RTerminal.PLUS, 1, RTerminal.S_PLUS.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchMinus(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" -" + lineEnd).init());
		assertNextToken(RTerminal.MINUS, 1, RTerminal.S_MINUS.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchMult(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" *" + lineEnd).init());
		assertNextToken(RTerminal.MULT, 1, RTerminal.S_MULT.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchDiv(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" /" + lineEnd).init());
		assertNextToken(RTerminal.DIV, 1, RTerminal.S_DIV.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchOr(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" |" + lineEnd).init());
		assertNextToken(RTerminal.OR, 1, RTerminal.S_OR.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchOrD(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ||" + lineEnd).init());
		assertNextToken(RTerminal.OR_D, 1, RTerminal.S_OR_D.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchAnd(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" &" + lineEnd).init());
		assertNextToken(RTerminal.AND, 1, RTerminal.S_AND.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchAndD(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" &&" + lineEnd).init());
		assertNextToken(RTerminal.AND_D, 1, RTerminal.S_AND_D.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchNot(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" !" + lineEnd).init());
		assertNextToken(RTerminal.NOT, 1, RTerminal.S_NOT.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchPower(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ^" + lineEnd).init());
		assertNextToken(RTerminal.POWER, 1, RTerminal.S_POWER.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSeq(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" :" + lineEnd).init());
		assertNextToken(RTerminal.SEQ, 1, RTerminal.S_COLON.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSpecial(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" %/%" + lineEnd).init());
		assertNextToken(RTerminal.SPECIAL, 0,
				1, 3,
				"/",
				lexer );
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSpecial_empty(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" %%" + lineEnd).init());
		assertNextToken(RTerminal.SPECIAL, 0,
				1, 2,
				"",
				lexer );
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@Test
	public void matchSpecial_notClosed() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" %/ ").init());
		assertNextToken(RTerminal.SPECIAL, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 3,
				"/ ",
				lexer );
		assertEndTokens("", 4, lexer);
		
		lexer.reset(this.input.reset(" %/ \n%").init());
		assertNextToken(RTerminal.SPECIAL, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 3,
				"/ ",
				lexer );
		assertNextToken(RTerminal.LINEBREAK, 4, 1, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchQuestionmark(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ?" + lineEnd).init());
		assertNextToken(RTerminal.QUESTIONMARK, 1, RTerminal.S_QUESTIONMARK.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchComma(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ," + lineEnd).init());
		assertNextToken(RTerminal.COMMA, 1, RTerminal.S_COMMA.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchSemicolon(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ;" + lineEnd).init());
		assertNextToken(RTerminal.SEMICOLON, 1, RTerminal.S_SEMICOLON.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchPipeRight(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" |>" + lineEnd).init());
		assertNextToken(RTerminal.PIPE_RIGHT, 1, RTerminal.S_PIPE_RIGHT.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchArrowLeftS(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" <-" + lineEnd).init());
		assertNextToken(RTerminal.ARROW_LEFT_S, 1, RTerminal.S_ARROW_LEFT.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchArrowLeftD(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" <<-" + lineEnd).init());
		assertNextToken(RTerminal.ARROW_LEFT_D, 1, RTerminal.S_ARROW_LEFT_D.length(), lexer);
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchArrowRightS(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ->" + lineEnd).init());
		assertNextToken(RTerminal.ARROW_RIGHT_S, 1, RTerminal.S_ARROW_RIGHT.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchArrowRightD(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ->>" + lineEnd).init());
		assertNextToken(RTerminal.ARROW_RIGHT_D, 1, RTerminal.S_ARROW_RIGHT_D.length(), lexer);
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchEqual(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" =" + lineEnd).init());
		assertNextToken(RTerminal.EQUAL, 1, RTerminal.S_EQUAL.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchColonEqual(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE | RLexer.ENABLE_COLON_EQUAL);
		
		lexer.reset(this.input.reset(" :=" + lineEnd).init());
		assertNextToken(RTerminal.COLON_EQUAL, 1, 2, lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@Test
	public void matchColonEqual_disabled() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" := ").init());
		assertNextToken(RTerminal.SEQ, 1, 1, lexer);
		assertNextToken(RTerminal.EQUAL, 2, 1, lexer);
		assertEndTokens(" ", 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchTilde(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ~" + lineEnd).init());
		assertNextToken(RTerminal.TILDE, 1, RTerminal.S_TILDE.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchRelNE(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" !=" + lineEnd).init());
		assertNextToken(RTerminal.REL_NE, 1, RTerminal.S_REL_NE.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchRelEQ(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ==" + lineEnd).init());
		assertNextToken(RTerminal.REL_EQ, 1, RTerminal.S_REL_EQ.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchRelLT(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" <" + lineEnd).init());
		assertNextToken(RTerminal.REL_LT, 1, RTerminal.S_REL_LT.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
		
		lexer.reset(this.input.reset(" <<" + lineEnd).init());
		assertNextToken(RTerminal.REL_LT, 1, RTerminal.S_REL_LT.length(), lexer);
		assertNextToken(RTerminal.REL_LT, 2, RTerminal.S_REL_LT.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchRelLE(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" <=" + lineEnd).init());
		assertNextToken(RTerminal.REL_LE, 1, RTerminal.S_REL_LE.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchRelGT(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" >" + lineEnd).init());
		assertNextToken(RTerminal.REL_GT, 1, RTerminal.S_REL_GT.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
		
		lexer.reset(this.input.reset(" >>" + lineEnd).init());
		assertNextToken(RTerminal.REL_GT, 1, RTerminal.S_REL_GT.length(), lexer);
		assertNextToken(RTerminal.REL_GT, 2, RTerminal.S_REL_GT.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchRelGE(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" >=" + lineEnd).init());
		assertNextToken(RTerminal.REL_GE, 1, RTerminal.S_REL_GE.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_if(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" if" + lineEnd).init());
		assertNextToken(RTerminal.IF,1, RTerminal.S_IF.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_else(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" else" + lineEnd).init());
		assertNextToken(RTerminal.ELSE, 1, RTerminal.S_ELSE.length(), lexer);
		assertEndTokens(lineEnd, 5, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_for(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" for" + lineEnd).init());
		assertNextToken(RTerminal.FOR, 1, RTerminal.S_FOR.length(), lexer);
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_in(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" in" + lineEnd).init());
		assertNextToken(RTerminal.IN, 1, RTerminal.S_IN.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_while(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" while" + lineEnd).init());
		assertNextToken(RTerminal.WHILE, 1, RTerminal.S_WHILE.length(), lexer);
		assertEndTokens(lineEnd, 6, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_repeat(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" repeat" + lineEnd).init());
		assertNextToken(RTerminal.REPEAT, 1, RTerminal.S_REPEAT.length(), lexer);
		assertEndTokens(lineEnd, 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_next(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" next" + lineEnd).init());
		assertNextToken(RTerminal.NEXT, 1, RTerminal.S_NEXT.length(), lexer);
		assertEndTokens(lineEnd, 5, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_break(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" break" + lineEnd).init());
		assertNextToken(RTerminal.BREAK, 1, RTerminal.S_BREAK.length(), lexer);
		assertEndTokens(lineEnd, 6, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_function(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" function" + lineEnd).init());
		assertNextToken(RTerminal.FUNCTION, 1, RTerminal.S_FUNCTION.length(), lexer);
		assertEndTokens(lineEnd, 9, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("opSeparators")
	public void matchKey_function_B(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" \\" + lineEnd).init());
		assertNextToken(RTerminal.FUNCTION_B, 1, RTerminal.S_BACKSLASH.length(), lexer);
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_TRUE(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" TRUE" + lineEnd).init());
		assertNextToken(RTerminal.TRUE, 1, RTerminal.S_TRUE.length(), lexer);
		assertEndTokens(lineEnd, 5, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_FALSE(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" FALSE" + lineEnd).init());
		assertNextToken(RTerminal.FALSE, 1, RTerminal.S_FALSE.length(), lexer);
		assertEndTokens(lineEnd, 6, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_NA(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" NA" + lineEnd).init());
		assertNextToken(RTerminal.NA, 1, RTerminal.S_NA.length(), lexer);
		assertEndTokens(lineEnd, 3, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_NA_real(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" NA_real_" + lineEnd).init());
		assertNextToken(RTerminal.NA_REAL, 1, RTerminal.S_NA_REAL.length(), lexer);
		assertEndTokens(lineEnd, 9, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_NA_integer(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" NA_integer_" + lineEnd).init());
		
		assertNextToken(RTerminal.NA_INT, 1, RTerminal.S_NA_INT.length(), lexer);
		assertEndTokens(lineEnd, 12, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_NA_complex(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" NA_complex_" + lineEnd).init());
		assertNextToken(RTerminal.NA_CPLX, 1, RTerminal.S_NA_CPLX.length(), lexer);
		assertEndTokens(lineEnd, 12, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_NA_character(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" NA_character_" + lineEnd).init());
		assertNextToken(RTerminal.NA_CHAR, 1, RTerminal.S_NA_CHAR.length(), lexer);
		assertEndTokens(lineEnd, 14, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_NULL(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" NULL" + lineEnd).init());
		assertNextToken(RTerminal.NULL, 1, RTerminal.S_NULL.length(), lexer);
		assertEndTokens(lineEnd, 5, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_NaN(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" NaN" + lineEnd).init());
		assertNextToken(RTerminal.NAN, 1, RTerminal.S_NAN.length(), lexer);
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchKey_Inf(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" Inf" + lineEnd).init());
		assertNextToken(RTerminal.INF, 1, RTerminal.S_INF.length(), lexer);
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	
}
