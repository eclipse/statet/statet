/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.r.core.model.RFunctionSpec.SER_V1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.core.model.RFunctionSpec.Parameter;
import org.eclipse.statet.r.core.model.RFunctionSpec.ReturnValue;


@NonNullByDefault
public class RFunctionSpecTest {
	
	
	public static class TestCase {
		
		final String label;
		final RFunctionSpec spec;
		final byte[] serV1;
		
		public TestCase(final String label,
				final RFunctionSpec spec, final String serV1) {
			this.label= label;
			this.spec= spec;
			this.serV1= Base64.getDecoder().decode(serV1);
		}
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	private static final TestCase CASE_EMPTY= new TestCase("empty",
			new RFunctionSpecBuilder()
					.build(),
			"AAAAAAAAAAA=" );
	private static final TestCase CASE_EXAMPLE_1= new TestCase("withParams",
			new RFunctionSpecBuilder()
					.addParam("arg1")
					.addParam("arg2", RFunctionSpec.CLASS_NAME | RFunctionSpec.AS_SYMBOL)
					.addParam("arg3", RFunctionSpec.OTHER_SPECIFIC_OBJ, "MyClass")
					.addParam(null)
					.build(),
			"AAAABP////xhcmcxAAAAAIAAAAD////8YXJnMgEACACAAAAA/////GFyZzMAAABA////+U15Q2xhc3OAAAAAAAAAAIAAAAAAAAAA" );
	
	private static final TestCase CASE_RETURNS= new TestCase("withReturn",
			new RFunctionSpecBuilder()
					.addParam("...", RFunctionSpec.CLASS_NAME | RFunctionSpec.AS_SYMBOL)
					.returns(ImCollections.newList(
						new ReturnValue(RFunctionSpec.RELATOR_CODE | RFunctionSpec.AS_STRING, null),
						new ReturnValue(RFunctionSpec.OTHER_SPECIFIC_OBJ, "MyCode") ))
					.build(),
			"AAAAAf////0uLi4BAAgAgAAAAAAAAAICEAAAgAAAAAAAAED////6TXlDb2Rl" );
	
	
	public static List<TestCase> allCases() {
		return ImCollections.newList(CASE_EMPTY, CASE_EXAMPLE_1, CASE_RETURNS);
	}
	
	
	public RFunctionSpecTest() {
	}
	
	
	@Test
	public void Spec_noParam() {
		RFunctionSpec spec;
		
		spec= new RFunctionSpec();
		assertEquals(0, spec.getParamCount());
		assertEquals(ImCollections.emptyList(), spec.getReturns());
		
		spec= new RFunctionSpecBuilder()
				.build();
		assertEquals(0, spec.getParamCount());
		assertEquals(ImCollections.emptyList(), spec.getReturns());
		
		spec= new RFunctionSpecBuilder()
				.returns(ImCollections.emptyList())
				.build();
		assertEquals(0, spec.getParamCount());
		assertEquals(ImCollections.emptyList(), spec.getReturns());
	}
	
	@Test
	public void Spec_withParamNamesOnly() {
		RFunctionSpec spec;
		
		spec= new RFunctionSpec("arg1", "arg2");
		assertEquals(2, spec.getParamCount());
		assertParam(0, "arg1", 0, spec.getParam(0));
		assertParam(1, "arg2", 0, spec.getParam(1));
		assertEquals(ImCollections.emptyList(), spec.getReturns());
		
		spec= new RFunctionSpecBuilder()
				.addParam("arg1")
				.addParam("arg2")
				.build();
		assertEquals(2, spec.getParamCount());
		assertParam(0, "arg1", 0, spec.getParam(0));
		assertParam(1, "arg2", 0, spec.getParam(1));
		assertEquals(ImCollections.emptyList(), spec.getReturns());
		
		spec= new RFunctionSpecBuilder()
				.addParams("arg1", null)
				.build();
		assertEquals(2, spec.getParamCount());
		assertParam(0, "arg1", 0, spec.getParam(0));
		assertParam(1, null, 0, spec.getParam(1));
		assertEquals(ImCollections.emptyList(), spec.getReturns());
	}
	
	public void Spec_withParamMissingName() {
		RFunctionSpec spec;
		
		spec= new RFunctionSpec("arg1", null);
		assertEquals(2, spec.getParamCount());
		assertParam(0, "arg1", 0, spec.getParam(0));
		assertParam(1, null, 0, spec.getParam(1));
		assertEquals(ImCollections.emptyList(), spec.getReturns());
		
		spec= new RFunctionSpecBuilder()
				.addParam("arg1")
				.addParam(null)
				.build();
		assertEquals(2, spec.getParamCount());
		assertParam(0, "arg1", 0, spec.getParam(0));
		assertParam(1, null, 0, spec.getParam(1));
		assertEquals(ImCollections.emptyList(), spec.getReturns());
		
		spec= new RFunctionSpecBuilder()
				.addParams("arg1", null)
				.build();
		assertEquals(2, spec.getParamCount());
		assertParam(0, "arg1", 0, spec.getParam(0));
		assertParam(1, null, 0, spec.getParam(1));
		assertEquals(ImCollections.emptyList(), spec.getReturns());
	}
	
	@Test
	public void Spec_withParamTypes() {
		RFunctionSpec spec;
		
		spec= CASE_EXAMPLE_1.spec;
		assertEquals(4, spec.getParamCount());
		assertParam(0, "arg1", 0, spec.getParam(0));
		assertParam(1, "arg2", RFunctionSpec.CLASS_NAME | RFunctionSpec.AS_SYMBOL, spec.getParam(1));
		assertParam(2, "arg3", RFunctionSpec.OTHER_SPECIFIC_OBJ, "MyClass", spec.getParam(2));
		assertParam(3, null, 0, spec.getParam(3));
		assertEquals(ImCollections.emptyList(), spec.getReturns());
	}
	
	
	@Test
	public void Spec_withReturnTypes() {
		RFunctionSpec spec;
		
		spec= CASE_RETURNS.spec;
		final var returns= spec.getReturns();
		assertEquals(2, returns.size());
		assertReturn(RFunctionSpec.RELATOR_CODE | RFunctionSpec.AS_STRING, null, returns.get(0));
		assertReturn(RFunctionSpec.OTHER_SPECIFIC_OBJ, "MyCode", returns.get(1));
	}
	
	
//	@ParameterizedTest
//	@MethodSource("allCases")
	public void Spec_printSer(final TestCase c) throws IOException {
		System.out.print(c.label + "= ");
		System.out.println(Base64.getEncoder().encodeToString(
				writeSer(c.spec, SER_V1) ));
	}
	
	@ParameterizedTest
	@MethodSource("allCases")
	public void Spec_writeSer_V1(final TestCase c) throws IOException {
		assertArrayEquals(c.serV1,
				writeSer(c.spec, SER_V1) );
	}
	
	@ParameterizedTest
	@MethodSource("allCases")
	public void Spec_readSer_V1(final TestCase c) throws IOException {
		assertEquals(c.spec,
				readSer(c.serV1, SER_V1) );
	}
	
	
	private byte[] writeSer(final RFunctionSpec spec, final byte v) throws IOException {
		final var byteArrayOut= new ByteArrayOutputStream();
		final DataStream out= DataStream.get(byteArrayOut);
		spec.writeTo(out, v);
		out.close();
		return byteArrayOut.toByteArray();
	}
	
	private RFunctionSpec readSer(final byte[] byteArray, final byte v) throws IOException {
		final var byteArrayIn= new ByteArrayInputStream(byteArray);
		final DataStream in= DataStream.get(byteArrayIn);
		final RFunctionSpec spec= new RFunctionSpec(in, v);
		assertEquals(0, byteArrayIn.available());
		in.close();
		return spec;
	}
	
	private void assertParam(final int expectedIndex, final @Nullable String expectedName,
			final int expectedType, final @Nullable String expectedClassName,
			final Parameter actual) {
		assertEquals(expectedIndex, actual.index);
		assertEquals(expectedName, actual.getName());
		assertEquals(expectedType, actual.getType());
		assertEquals(expectedClassName, actual.getClassName());
	}
	
	private void assertParam(final int expectedIndex, final @Nullable String expectedName,
			final int expectedType,
			final Parameter actual) {
		assertParam(expectedIndex, expectedName, expectedType, null, actual);
	}
	
	private void assertReturn(final int expectedType, final @Nullable String expectedClassName,
			final ReturnValue actual) {
		assertEquals(expectedType, actual.getType());
		assertEquals(expectedClassName, actual.getClassName());
	}
	
}
