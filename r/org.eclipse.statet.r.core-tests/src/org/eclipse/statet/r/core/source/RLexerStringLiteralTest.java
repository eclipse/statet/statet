/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_SYMBOL_START_INVALID;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE12_SYNTAX_TOKEN_OPENING_INCOMPLETE;
import static org.eclipse.statet.r.core.source.RSourceTests.IGNORE_TEXT;
import static org.eclipse.statet.r.core.source.RSourceTests.assertDetail;
import static org.eclipse.statet.r.core.source.RSourceTests.assertEndTokens;
import static org.eclipse.statet.r.core.source.RSourceTests.assertNextToken;
import static org.eclipse.statet.r.core.source.RSourceTests.assertRegion;
import static org.eclipse.statet.r.core.source.RSourceTests.assertRegionStartEnd;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;


@NonNullByDefault
public class RLexerStringLiteralTest {
	
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RLexerStringLiteralTest() {
	}
	
	
	protected int getConfig() {
		return 0;
	}
	
	protected String requireDecoding(final String text) {
		return ((getConfig() & RLexer.ENABLE_QUICK_CHECK) != 0) ? IGNORE_TEXT : text;
	}
	
	
	static ImList<String> sSeparators() {
		return RLexerTest.sSeparators();
	}
	
	static ImList<String> lineBreaks() {
		return ImCollections.newList("\n", "\r", "\r\n");
	}
	
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchStringD(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" \"abc\"" + lineEnd).init());
		assertNextToken(RTerminal.STRING_D, 0,
				1, 5,
				"abc",
				lexer );
		assertRegion(1 + 1, 3, lexer.getTextRegion());
		assertEndTokens(lineEnd, 6, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchStringD_withEscape(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" \"abc\\\"\"" + lineEnd).init());
		assertNextToken(RTerminal.STRING_D, 0,
				1, 7,
				requireDecoding("abc\""),
				lexer );
		assertRegion(1 + 1, 5, lexer.getTextRegion());
		assertEndTokens(lineEnd, 8, lexer);
	}
	
	@Test
	public void matchStringD_notClosed() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" \"abc ").init());
		assertNextToken(RTerminal.STRING_D, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 5,
				"abc ",
				lexer );
		assertRegionStartEnd(1 + 1, 6, lexer.getTextRegion());
		assertEndTokens("", 6, lexer);
		
		lexer.reset(this.input.reset(" \"abc\\").init());
		assertNextToken(RTerminal.STRING_D, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 5,
				"abc\\",
				lexer );
		assertRegionStartEnd(1 + 1, 6, lexer.getTextRegion());
		assertEndTokens("", 6, lexer);
		
		lexer.reset(this.input.reset(" \"abc\\\"").init());
		assertNextToken(RTerminal.STRING_D, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 6,
				requireDecoding("abc\""),
				lexer );
		assertRegionStartEnd(1 + 1, 7, lexer.getTextRegion());
		assertEndTokens("", 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void matchStringD_with_Linebreak(final String linebreak) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= "abc" + linebreak + "efg";
		lexer.reset(this.input.reset("\"" + text + "\"").init());
		assertNextToken(RTerminal.STRING_D, 0,
				0, 2 + text.length(),
				text,
				lexer );
		assertEndTokens("", 2 + text.length(), lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchStringS(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc'" + lineEnd).init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 5,
				"abc",
				lexer );
		assertRegion(1 + 1, 3, lexer.getTextRegion());
		assertEndTokens(lineEnd, 6, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchStringS_withEscape(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" \'abc\\\'\'" + lineEnd).init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 7,
				requireDecoding("abc\'"),
				lexer );
		assertRegion(1 + 1, 5, lexer.getTextRegion());
		assertEndTokens(lineEnd, 8, lexer);
	}
	
	@Test
	public void matchStringS_notClosed() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc ").init());
		assertNextToken(RTerminal.STRING_S, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 5,
				"abc ",
				lexer );
		assertRegionStartEnd(1 + 1, 6, lexer.getTextRegion());
		assertEndTokens("", 6, lexer);
		
		lexer.reset(this.input.reset(" 'abc\\").init());
		assertNextToken(RTerminal.STRING_S, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 5,
				"abc\\",
				lexer );
		assertRegionStartEnd(1 + 1, 6, lexer.getTextRegion());
		assertEndTokens("", 6, lexer);
		
		lexer.reset(this.input.reset(" 'abc\\\"").init());
		assertNextToken(RTerminal.STRING_S, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 6,
				requireDecoding("abc\""),
				lexer );
		assertRegionStartEnd(1 + 1, 7, lexer.getTextRegion());
		assertEndTokens("", 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void matchStringS_with_Linebreak(final String linebreak) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= "abc" + linebreak + "efg";
		lexer.reset(this.input.reset("\'" + text + "\'").init());
		assertNextToken(RTerminal.STRING_S, 0,
				0, 2 + text.length(),
				text,
				lexer );
		assertEndTokens("", 2 + text.length(), lexer);
	}
	
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" abc" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 3,
				"abc",
				lexer );
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_Dot(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ." + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 1,
				".",
				lexer );
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_contains_Dot(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" abc.efg" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 7,
				"abc.efg",
				lexer );
		assertEndTokens(lineEnd, 8, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_Ellipsis(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ..." + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 3,
				"...",
				lexer );
		assertEndTokens(lineEnd, 4, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_contains_Dots(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" abc...efg" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 9,
				"abc...efg",
				lexer );
		assertEndTokens(lineEnd, 10, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_beginsWith_Dots(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" ...abc" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 6,
				"...abc",
				lexer );
		assertEndTokens(lineEnd, 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_endsWith_Dots(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" abc..." + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 6,
				"abc...",
				lexer );
		assertEndTokens(lineEnd, 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_beginWith_Underscore(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" _abc" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, TYPE123_SYNTAX_SYMBOL_START_INVALID,
				1, 4,
				"_abc",
				lexer );
		assertDetail(1, 1, "_", lexer.getStatusDetail());
		assertEndTokens(lineEnd, 5, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_contains_Underscore(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" abc_efg" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 7,
				"abc_efg",
				lexer );
		assertEndTokens(lineEnd, 8, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_contains_Underscores(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" abc___efg" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 9,
				"abc___efg",
				lexer );
		assertEndTokens(lineEnd, 10, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_endsWith_Underscores(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" abc___" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL, 0,
				1, 6,
				"abc___",
				lexer );
		assertEndTokens(lineEnd, 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_nonAscii(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		for (final String symbol : ImCollections.newList(
				"\u039Ebc",
				"a\u039Ec",
				"ab\u039E" )) {
			lexer.reset(this.input.reset(symbol + lineEnd).init());
			assertNextToken(RTerminal.SYMBOL, 0,
					0, symbol.length(),
					symbol,
					lexer );
			assertEndTokens(lineEnd, symbol.length(), lexer);
		}
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbol_noKeyword(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		final var sb= new StringBuilder();
		String symbol;
		
		for (final String keyword : ImCollections.newList(
				RTerminal.S_BREAK,
				RTerminal.S_ELSE,
				RTerminal.S_FOR,
				RTerminal.S_FUNCTION,
				RTerminal.S_IF,
				RTerminal.S_IN,
				RTerminal.S_NEXT,
				RTerminal.S_REPEAT,
				RTerminal.S_WHILE,
				RTerminal.S_INF,
				RTerminal.S_NA,
				RTerminal.S_NA_CHAR,
				RTerminal.S_NA_CPLX,
				RTerminal.S_NA_INT,
				RTerminal.S_NA_REAL,
				RTerminal.S_NULL,
				RTerminal.S_FALSE,
				RTerminal.S_TRUE )) {
			sb.setLength(0);
			sb.append(keyword);
			sb.setCharAt(1, 'X');
			symbol= sb.toString();
			lexer.reset(this.input.reset(symbol + lineEnd).init());
			assertNextToken(RTerminal.SYMBOL, 0,
					0, symbol.length(),
					symbol,
					lexer );
			assertEndTokens(lineEnd, symbol.length(), lexer);
			
			sb.setLength(0);
			sb.append(keyword);
			sb.setCharAt(keyword.length() - 1, 'a');
			symbol= sb.toString();
			lexer.reset(this.input.reset(symbol + lineEnd).init());
			assertNextToken(RTerminal.SYMBOL, 0,
					0, symbol.length(),
					symbol,
					lexer );
			assertEndTokens(lineEnd, symbol.length(), lexer);
			
			sb.setLength(0);
			sb.append(keyword);
			sb.append('0');
			symbol= sb.toString();
			lexer.reset(this.input.reset(symbol + lineEnd).init());
			assertNextToken(RTerminal.SYMBOL, 0,
					0, symbol.length(),
					symbol,
					lexer );
			assertEndTokens(lineEnd, symbol.length(), lexer);
		}
		
		for (final String c : ImCollections.newList("r", "R")) {
			symbol= c;
			lexer.reset(this.input.reset(symbol + lineEnd).init());
			assertNextToken(RTerminal.SYMBOL, 0,
					0, symbol.length(),
					symbol,
					lexer );
			assertEndTokens(lineEnd, symbol.length(), lexer);
			
			symbol= c + '0';
			lexer.reset(this.input.reset(symbol + lineEnd).init());
			assertNextToken(RTerminal.SYMBOL, 0,
					0, symbol.length(),
					symbol,
					lexer );
			assertEndTokens(lineEnd, symbol.length(), lexer);
		}
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchPipePlaceholder(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" _" + lineEnd).init());
		assertNextToken(RTerminal.PIPE_PLACEHOLDER, 0,
				1, 1,
				"_",
				lexer );
		assertNull(lexer.getTextRegion());
		assertEndTokens(lineEnd, 2, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("sSeparators")
	public void matchSymbolG(final String lineEnd) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc`" + lineEnd).init());
		assertNextToken(RTerminal.SYMBOL_G, 0,
				1, 5,
				"abc",
				lexer );
		assertRegion(1 + 1, 3, lexer.getTextRegion());
		assertEndTokens(lineEnd, 6, lexer);
	}
	
	@Test
	public void matchSymbolG_notClosed() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 5,
				"abc ",
				lexer );
		assertRegionStartEnd(1 + 1, 6, lexer.getTextRegion());
		assertEndTokens("", 6, lexer);
		
		lexer.reset(this.input.reset(" `abc\\").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 5,
				"abc\\",
				lexer );
		assertRegionStartEnd(1 + 1, 6, lexer.getTextRegion());
		assertEndTokens("", 6, lexer);
		
		lexer.reset(this.input.reset(" `abc\\t").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, 6,
				requireDecoding("abc\t"),
				lexer );
		assertRegionStartEnd(1 + 1, 7, lexer.getTextRegion());
		assertEndTokens("", 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void matchSymbolG_with_Linebreak(final String linebreak) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= "abc" + linebreak + "efg";
		lexer.reset(this.input.reset("`" + text + "`").init());
		assertNextToken(RTerminal.SYMBOL_G, 0,
				0, 2 + text.length(),
				text,
				lexer );
		assertEndTokens("", 2 + text.length(), lexer);
	}
	
	
	static List<Arguments> generateStringRDelimiterCombinationArguments() {
		final List<Arguments> combinations= new ArrayList<>();
		for (final char c0 : new char[] { 'r', 'R' }) {
			for (final char cQuote : new char[] { '\"', '\'' }) {
				for (final String bracketPair : new String[] { "()", "[]", "{}" }) {
					combinations.add(Arguments.arguments(c0, cQuote, bracketPair.toCharArray()));
				}
			}
		}
		return combinations;
	}
	
	@ParameterizedTest
	@MethodSource("generateStringRDelimiterCombinationArguments")
	public void matchStringR(final char c0, final char cQuote, final char[] bracketPair) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= " " + c0 + cQuote + bracketPair[0] + "abc" + bracketPair[1] + cQuote + " ";
		lexer.reset(this.input.reset(text).init());
		assertNextToken(RTerminal.STRING_R, 0,
				1, 8,
				"abc",
				lexer );
		assertRegion(1 + 3, 3, lexer.getTextRegion());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "-", "---", "------------------------" })
	public void matchStringR_withDashes(final String dashes) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= " r\"" + dashes + "(abc)" + dashes + "\" ";
		lexer.reset(this.input.reset(text).init());
		assertNextToken(RTerminal.STRING_R, 0,
				1, 8 + 2 * dashes.length(),
				"abc",
				lexer );
		assertRegion(1 + 3 + dashes.length(), 3, lexer.getTextRegion());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringR_incompleteOpening() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" r\" ").init());
		assertNextToken(RTerminal.STRING_R, TYPE12_SYNTAX_TOKEN_OPENING_INCOMPLETE,
				1, 2,
				null,
				lexer );
		assertNull(lexer.getTextRegion());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringR_incompleteOpening_withDashes() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" r\"-ab-").init());
		assertNextToken(RTerminal.STRING_R, TYPE12_SYNTAX_TOKEN_OPENING_INCOMPLETE,
				1, 3,
				null,
				lexer );
		assertNull(lexer.getTextRegion());
		assertEquals(RTerminal.SYMBOL, lexer.next());
	}
	
	@ParameterizedTest
	@ValueSource(strings= {
			"r\"(abc ",
			"r\"(abc) ",
			"r\"(abc\" ",
			"r\"(abc]\" ",
	})
	public void matchStringR_notClosed(final String string) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= " " + string;
		lexer.reset(this.input.reset(text).init());
		assertNextToken(RTerminal.STRING_R, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, text.length() - 1,
				text.substring(1 + 3),
				lexer );
		assertRegionStartEnd(1 + 3, text.length(), lexer.getTextRegion());
		assertDetail(1, text.length() - 1, ")\"", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@ParameterizedTest
	@ValueSource(strings= {
			"r\"(abc \u0000 ",
			"r\"(abc \u0000 ) \"",
	})
	public void matchStringR_notClosed_withTextError(final String string) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= " " + string;
		lexer.reset(this.input.reset(text).init());
		assertNextToken(RTerminal.STRING_R, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, text.length() - 1,
				text.substring(1 + 3),
				lexer );
		assertRegionStartEnd(1 + 3, text.length(), lexer.getTextRegion());
		assertDetail(1, text.length() - 1, ")\"", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@ParameterizedTest
	@ValueSource(strings= {
			"r\"---(abc",
			"r\"---(abc]---\"",
			"r\"---(abc)-- \"",
			"r\"---(abc)----\"",
			"r\"---(abc)---\'",
			"r\"---(abc)---",
	})
	public void matchStringR_notClosed_withDashes(final String string) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= " " + string;
		lexer.reset(this.input.reset(text).init());
		assertNextToken(RTerminal.STRING_R, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, text.length() - 1,
				text.substring(1 + 6),
				lexer );
		assertRegionStartEnd(1 + 6, text.length(), lexer.getTextRegion());
		assertDetail(1, text.length() - 1, ")---\"", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@ParameterizedTest
	@ValueSource(strings= {
			" r\"---(abc \u0000",
			" r\"---(abc \u0000 }---\"",
			" r\"---(abc \u0000 )-- \"",
			" r\"---(abc \u0000 )----\"",
			" r\"---(abc \u0000 )---\'",
			" r\"---(abc \u0000 )---",
	})
	public void matchStringR_notClosed_withDashes_withTextError(final String text) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(text).init());
		assertNextToken(RTerminal.STRING_R, TYPE12_SYNTAX_TOKEN_NOT_CLOSED,
				1, text.length() - 1,
				text.substring(1 + 6),
				lexer );
		assertRegionStartEnd(1 + 6, text.length(), lexer.getTextRegion());
		assertDetail(1, text.length() - 1, ")---\"", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void matchStringR_with_Linebreak(final String linebreak) {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		
		final String text= " r\"(abc" + linebreak + "efg)\" ";
		lexer.reset(this.input.reset(text).init());
		assertNextToken(RTerminal.STRING_R, 0,
				1, text.length() - 2,
				text.substring(1 + 3, text.length() - 2 - 1),
				lexer );
		assertRegion(1 + 3, 6 + linebreak.length(), lexer.getTextRegion());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	
}
