/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_HEX_DIGIT_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_INT_WITH_DEC_POINT;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;


@NonNullByDefault
public class RLexerNumericLiteralTest {
	
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RLexerNumericLiteralTest() {
	}
	
	
	protected int getConfig() {
		return 0;
	}
	
	
	@Test
	public void matchNumReal_Dec() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 01234567890 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("01234567890", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Dec_1_0() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 1 2 3 4 5 6 7 8 9 0 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("1", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("2", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("3", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("4", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("5", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("6", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("7", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("8", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("9", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Dec_with_Point() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123.456 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123.456", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Dec_startsWith_Point() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" .123 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(".123", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Dec_endsWith_Point() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123. ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123.", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Dec_withExp() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123.4e0 123.e10 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123.4e0", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123.e10", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Dec_withExp_withSign() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123.4e+1 123.4e-1 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123.4e+1", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123.4e-1", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Dec_withExp_missingExpDigit() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123.4e 123.4e+ 123e- ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Hex() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x01234567890 0xabcdef 0xABCDEF ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x01234567890", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xabcdef", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xABCDEF", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Hex_0_F() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x0 0x1 0x2 0x3 0x4 0x5 0x6 0x7 0x8 0x9 0xA 0xB 0xC 0xD 0xE 0xF 0xa 0xb 0xc 0xd 0xe 0xf ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x0", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x1", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x2", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x3", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x4", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x5", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x6", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x7", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x8", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x9", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xA", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xB", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xC", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xD", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xE", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xF", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xa", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xb", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xc", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xd", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xe", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0xf", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_Hex_missingDigit() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x 0xp ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_DIGIT_MISSING, lexer.getFlags());
	}
	
	@Test
	public void matchNumReal_HexFloat_withoutPoint() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123ABCp10 0x0FFp01 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x123ABCp10", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x0FFp01", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_HexFloat_with_Point() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123.456p0 0x.ABCDEFp10 0x123.456 0xA.A ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x123.456p0", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x.ABCDEFp10", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING, lexer.getFlags());
		assertEquals("0x123.456", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING, lexer.getFlags());
		assertEquals("0xA.A", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_HexFloat_startsWith_Point() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x.123p0 0x.123 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x.123p0", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING, lexer.getFlags());
		assertEquals("0x.123", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_HexFloat_endsWith_Point() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123.p0 0x123. ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x123.p0", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING, lexer.getFlags());
		assertEquals("0x123.", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_HexFloat_withExpSign() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123.4p+10 0x123.4p-01 ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x123.4p+10", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x123.4p-01", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumReal_HexFloat_missingExpDigit() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123.4p 0x123.4p+ 0x123p- 0x123.4pA ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.SYMBOL, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("A", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	
	@Test
	public void matchNumInt_Dec() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123L", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Dec_check() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 2147483647L 2147483648L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Dec_with_Point() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 2147483647.0L .1L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_INT_WITH_DEC_POINT, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Dec_withExp() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 1e9L 1.5e9L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("1e9L", lexer.getText());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("1.5e9L", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Dec_withExpSign() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0.5e+9L 100e-1L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0.5e+9L", lexer.getText());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("100e-1L", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Dec_withExp_check() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 2147483647e0L 2147483647.0e0L 2147483648.0e0L 1e10L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Dec_missingExpDigit() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123.4eL 123e+L 123e-L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Hex() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x0123ABCL ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x0123ABCL", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Hex_check() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x7FFFFFFFL 0x8FFFFFFFL 0xfffffffffffffffffffffffffffffffffffffffL ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_Hex_missingDigit() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0xL ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_HexFloat() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123p0L 0x.Ap10L 0x123.456L 0xA.AL ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x123p0L", lexer.getText());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x.Ap10L", lexer.getText());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING, lexer.getFlags());
		assertEquals("0x123.456L", lexer.getText());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING, lexer.getFlags());
		assertEquals("0xA.AL", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_HexFloat_check() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123.4p0L 0x.ABCEp10L ").init());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L, lexer.getFlags());
		assertEquals("0x123.4p0L", lexer.getText());
		
		assertEquals(RTerminal.NUM_NUM, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_NON_INT_WITH_L, lexer.getFlags());
		assertEquals("0x.ABCEp10L", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_HexFloat_withExpSign() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123.4p+10L 0x1200p-01L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x123.4p+10L", lexer.getText());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x1200p-01L", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumInt_HexFloat_missingExpDigit() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x123.4pL 0x123.4p+L 0x123p-L ").init());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.NUM_INT, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_EXP_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	
	@Test
	public void matchNumCplx() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123i ").init());
		
		assertEquals(RTerminal.NUM_CPLX, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123i", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumCplx_Dec() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 123.456i .456i 123.i").init());
		
		assertEquals(RTerminal.NUM_CPLX, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123.456i", lexer.getText());
		
		assertEquals(RTerminal.NUM_CPLX, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals(".456i", lexer.getText());
		
		assertEquals(RTerminal.NUM_CPLX, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("123.i", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumCplx_Hex() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x0123ABCi ").init());
		
		assertEquals(RTerminal.NUM_CPLX, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x0123ABCi", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumCplx_Hex_missingDigit() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0xi ").init());
		
		assertEquals(RTerminal.NUM_CPLX, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_DIGIT_MISSING, lexer.getFlags());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumCplx_HexFloat() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x0123A.BCp0i ").init());
		
		assertEquals(RTerminal.NUM_CPLX, lexer.next());
		assertEquals(0, lexer.getFlags());
		assertEquals("0x0123A.BCp0i", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchNumCplx_HexFloat_missingExp() {
		final RLexer lexer= new RLexer(getConfig() | RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 0x0123A.BCi ").init());
		
		assertEquals(RTerminal.NUM_CPLX, lexer.next());
		assertEquals(TYPE123_SYNTAX_NUMBER_HEX_FLOAT_EXP_MISSING, lexer.getFlags());
		assertEquals("0x0123A.BCi", lexer.getText());
		
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
}
