/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.model;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.io.DataStream;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class RElementNameTest {
	
	
	public static class TestCase {
		
		final String label;
		final @Nullable RElementName elementName;
		final byte[] serV1;
		
		public TestCase(final String label,
				final @Nullable RElementName elementName, final String serV1) {
			this.label= label;
			this.elementName= elementName;
			this.serV1= Base64.getDecoder().decode(serV1);
		}
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	public static final TestCase CASE_NULL= new TestCase("null",
			null,
			"AA==" );
	public static final TestCase CASE_SIMPLE= new TestCase("Simple",
			RElementName.create(RElementName.MAIN_DEFAULT, "element.name"),
			"AQAAABEA////9GVsZW1lbnQubmFtZQA=" );
	public static final TestCase CASE_NULL_NAME= new TestCase("segmentName= <missing>",
			RElementName.create(RElementName.MAIN_DEFAULT, null),
			"AQAAABEAgAAAAAA=" );
	public static final TestCase CASE_SCOPE= new TestCase("withScope",
			RElementName.addScope(
					RElementName.create(RElementName.MAIN_DEFAULT, "element.name"),
					RElementName.create(RElementName.SCOPE_NS, "my.pkg") ),
			"AQAAABEBAAAAIf////pteS5wa2cA////9GVsZW1lbnQubmFtZQA=" );
	public static final TestCase CASE_NEXT= new TestCase("withSub",
			RElementName.create(ImCollections.newList(
					RElementName.create(RElementName.MAIN_DEFAULT, "element.name"),
					RElementName.create(RElementName.SUB_NAMEDPART, "entry") )),
			"AQAAABEA////9GVsZW1lbnQubmFtZQEAAAAa////+2VudHJ5AA==" );
	public static final TestCase CASE_INDEXED= new TestCase("withSubIndexed",
			RElementName.create(ImCollections.newList(
					RElementName.create(RElementName.MAIN_DEFAULT, "element.name"),
					RElementName.create(RElementName.SUB_INDEXED_D, "20", 20) )),
			"AQAAABEA////9GVsZW1lbnQubmFtZQIAAAAc/////jIwAAAAFAA=" );
	public static final TestCase CASE_COMB= new TestCase("Combined",
			RElementName.create(ImCollections.newList(
					RElementName.create(RElementName.SCOPE_NS, "my.pkg"),
					RElementName.create(RElementName.MAIN_DEFAULT, "element.name"),
					RElementName.create(RElementName.SUB_NAMEDPART, "item", 1),
					RElementName.create(RElementName.SUB_NAMEDSLOT, null),
					RElementName.create(RElementName.SUB_INDEXED_S, "filter") )),
			"AQAAABEBAAAAIf////pteS5wa2cA////9GVsZW1lbnQubmFtZQIAAAAa/////Gl0ZW0AAAABAQAAABmAAAAAAQAAABv////6ZmlsdGVyAA==" );
	
	
	public static List<TestCase> allCases() {
		return ImCollections.newList(CASE_NULL, CASE_SIMPLE, CASE_NULL_NAME,
				CASE_SCOPE, CASE_NEXT, CASE_INDEXED,
				CASE_COMB );
	}
	
	
	public RElementNameTest() {
	}
	
	
//	@ParameterizedTest
//	@MethodSource("allCases")
	public void Name_printSer(final TestCase c) throws IOException {
		System.out.print(c.label + "= ");
		System.out.println(Base64.getEncoder().encodeToString(
				writeSer(c.elementName) ));
	}
	
	@ParameterizedTest
	@MethodSource("allCases")
	public void Name_writeSer(final TestCase c) throws IOException {
		assertArrayEquals(c.serV1,
				writeSer(c.elementName) );
	}
	
	@ParameterizedTest
	@MethodSource("allCases")
	public void Name_readSer_V1(final TestCase c) throws IOException {
		assertEquals(c.elementName,
				readSer(c.serV1) );
	}
	
	
	private byte[] writeSer(final @Nullable RElementName name) throws IOException {
		final var byteArrayOut= new ByteArrayOutputStream();
		try (final DataStream out= DataStream.get(byteArrayOut)) {
			RElementName.write(name, out);
		}
		return byteArrayOut.toByteArray();
	}
	
	private @Nullable RElementName readSer(final byte[] byteArray) throws IOException {
		final var byteArrayIn= new ByteArrayInputStream(byteArray);
		try (final DataStream in= DataStream.get(byteArrayIn)) {
			final RElementName name= RElementName.read(in);
			assertEquals(0, byteArrayIn.available());
			return name;
		}
	}
	
}
