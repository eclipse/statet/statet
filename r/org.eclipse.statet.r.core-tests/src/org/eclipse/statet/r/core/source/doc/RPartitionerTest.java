/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.doc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TypedRegion;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartition;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitioner;


@NonNullByDefault
public class RPartitionerTest {
	
	
	private final RDocumentSetupParticipant setup= new RDocumentSetupParticipant();
	
	private AbstractDocument document;
	
	
	@SuppressWarnings("null")
	public RPartitionerTest() {
	}
	
	
	@BeforeEach
	public void createDocument() {
		this.document= new Document();
		this.setup.setup(this.document);
	}
	
	protected void assertPartitions(final ITypedRegion... expected) {
		final TreePartitioner partitioner= nonNullAssert((TreePartitioner)this.document.getDocumentPartitioner(
				this.setup.getPartitioningId() ));
		final TreePartition[] actual= partitioner.computePartitioning(0, this.document.getLength(), true);
		assertEquals(expected.length, actual.length);
		for (int i= 0; i < actual.length; i++) {
			final int index0= i;
			assertEquals(expected[index0].getType(), actual[index0].getType(), () -> String.format("partitions differ at [%1$s] in type", index0));
			assertEquals(expected[index0].getOffset(), actual[index0].getOffset(), () -> String.format("partitions differ at [%1$s] in offset", index0));
			assertEquals(expected[index0].getLength(), actual[index0].getLength(), () -> String.format("partitions differ at [%1$s] in length", index0));
		}
	}
	
	
	@Test
	public void empty() {
		this.document.set("");
		assertPartitions(
				new TypedRegion(0, 0, RDocumentConstants.R_DEFAULT_CONTENT_TYPE) );
		
		this.document.set(" \t\n");
		assertPartitions(
				new TypedRegion(0, 3, RDocumentConstants.R_DEFAULT_CONTENT_TYPE) );
	}
	
	@Test
	public void matchComment() {
		this.document.set("  # comment\n");
		assertPartitions(
				new TypedRegion(0, 2, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(2, 9, RDocumentConstants.R_COMMENT_CONTENT_TYPE),
				new TypedRegion(11, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	
	@Test
	public void matchCode() {
		this.document.set(" { x }");
		assertPartitions(
				new TypedRegion(0, 6, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	@Test
	public void matchStringD() {
		this.document.set(" \"abc\"\n");
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, 5, RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(6, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	@Test
	public void matchStringS() {
		this.document.set(" \'abc\'\n");
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, 5, RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(6, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	
	@Test
	public void matchSymbolG() {
		this.document.set(" `abc` ");
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, 5, RDocumentConstants.R_QUOTED_SYMBOL_CONTENT_TYPE),
				new TypedRegion(6, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	
	static List<Arguments> generateStringRDelimiterCombinationArguments() {
		final List<Arguments> combinations= new ArrayList<>();
		for (final char c0 : new char[] { 'r', 'R' }) {
			for (final char cQuote : new char[] { '\"', '\'' }) {
				for (final String bracketPair : new String[] { "()", "[]", "{}" }) {
					combinations.add(arguments(c0, cQuote, bracketPair.toCharArray()));
				}
			}
		}
		return combinations;
	}
	
	@ParameterizedTest
	@MethodSource("generateStringRDelimiterCombinationArguments")
	public void matchStringR(final char c0, final char cQuote, final char[] bracketPair) {
		final String text= " " + c0 + cQuote + bracketPair[0] + "abc" + bracketPair[1] + cQuote + " ";
		this.document.set(text);
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, 8, RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(9, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "-", "---", "------------------------" })
	public void matchStringR_withDashes(final String dashes) {
		final String text= " r\"" + dashes + "(abc)" + dashes + "\" ";
		this.document.set(text);
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, 8 + 2 * dashes.length(), RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(9 + 2 * dashes.length(), 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "r\"", "r\"abc", "r\"\n()" })
	public void matchStringR_incompleteOpening(final String string) {
		final String text= " " + string;
		this.document.set(text);
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, 2, RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(3, text.length() - 3, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	@Test
	public void matchStringR_incompleteOpening_withDashes() {
		this.document.set(" r\"-ab-");
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, 3, RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(4, 3, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	@ParameterizedTest
	@ValueSource(strings= {
			"r\"(abc ",
			"r\"(abc) ",
			"r\"(abc\" ",
			"r\"(abc]\" ",
	})
	public void matchStringR_notClosed(final String string) {
		final String text= " " + string;
		this.document.set(text);
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, text.length() - 1, RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(text.length(), 0, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	@ParameterizedTest
	@ValueSource(strings= {
			"r\"---(abc",
			"r\"---(abc]---\"",
			"r\"---(abc)-- \"",
			"r\"---(abc)----\"",
			"r\"---(abc)---\'",
			"r\"---(abc)---",
	})
	public void matchStringR_notClosed_withDashes(final String string) {
		final String text= " " + string;
		this.document.set(text);
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, text.length() - 1, RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(text.length(), 0, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "\n", "\r", "\r\n" })
	public void matchStringR_with_Linebreak(final String lineSeparator) {
		final String text= " r\"(abc" + lineSeparator + "efg)\" ";
		this.document.set(text);
		assertPartitions(
				new TypedRegion(0, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE),
				new TypedRegion(1, text.length() - 2, RDocumentConstants.R_STRING_CONTENT_TYPE),
				new TypedRegion(text.length() - 1, 1, RDocumentConstants.R_DEFAULT_CONTENT_TYPE));
	}
	
}
