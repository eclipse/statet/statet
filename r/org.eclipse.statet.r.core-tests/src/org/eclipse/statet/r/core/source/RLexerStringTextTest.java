/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_CODEPOINT_INVALID;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_NOT_CLOSED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNKOWN;
import static org.eclipse.statet.r.core.source.RSourceConstants.TYPE123_SYNTAX_TEXT_NULLCHAR;
import static org.eclipse.statet.r.core.source.RSourceTests.assertDetail;
import static org.eclipse.statet.r.core.source.RSourceTests.assertNextToken;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;


@NonNullByDefault
public class RLexerStringTextTest {
	
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RLexerStringTextTest() {
	}
	
	
	@Test
	public void matchStringS_with_SpecialCharEscape() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\\' \\\" \\` \\a \\b \\f \\n \\r \\t \\v \\\\ \\\n e' ").init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 43,
				"abc \' \" ` \u0007 \u0008 \f \n \r \t \u000B \\ \n e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_OctalEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\1 \\02 \\003 \\777 \\7777 e' ").init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 30,
				"abc \u0001 \u0002 \u0003 \u01FF \u01FF7 e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_xHexEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\x1 \\x02 \\xAA \\xff \\x0ff e' ").init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 32,
				"abc \u0001 \u0002 \u00AA \u00FF \u000Ff e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_xHexEscapeSequence_missingDigit() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\x e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING,
				1, 10,
				"abc \\x e",
				lexer );
		assertDetail(6, 2, "\\x", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_uHexEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\u1 \\u02 \\u003 \\uAAAA \\ufffa \\u0fffa e' ").init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 44,
				"abc \u0001 \u0002 \u0003 \uAAAA \uFFFa \u0FFFa e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_uHexEscapeSequence_missingDigit() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\u e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING,
				1, 10,
				"abc \\u e",
				lexer );
		assertDetail(6, 2, "\\uxxxx", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_uHexBracketEscapeSequence() {
		
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" 'abc \\u{1} \\u{02} \\u{003} \\u{AAAA} \\u{FFF0} e' ").init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 46,
				"abc \u0001 \u0002 \u0003 \uAAAA \uFFF0 e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_uHexBracketEscapeSequence_missingDigit() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\u{} e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING,
				1, 12,
				"abc \\u{} e",
				lexer );
		assertDetail(6, 4, "\\u{xxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_uHexBracketEscapeSequence_notClosed() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\u{1 e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_NOT_CLOSED,
				1, 12,
				"abc \\u{1 e",
				lexer );
		assertDetail(6, 4, "\\u{xxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\u{FFFFF} e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_NOT_CLOSED,
				1, 17,
				"abc \\u{FFFFF} e",
				lexer );
		assertDetail(6, 7, "\\u{xxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_UHexEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\U1 \\U02 \\U003 \\UAAAA \\UFFF0 \\U1D11E \\U24F5C \\U00000aaaa e' ").init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 64,
				"abc \u0001 \u0002 \u0003 \uAAAA \uFFF0 \uD834\uDD1E \uD853\uDF5C \u0AAAa e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_UHexEscapeSequence_missingDigit() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\U e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING,
				1, 10,
				"abc \\U e",
				lexer );
		assertDetail(6, 2, "\\Uxxxxxxxx", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_UHexBracketEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\U{1} \\U{02} \\U{003} \\U{AAAA} \\U{FFF0} \\U{1D11E} \\U{24F5C} e' ").init());
		assertNextToken(RTerminal.STRING_S, 0,
				1, 66,
				"abc \u0001 \u0002 \u0003 \uAAAA \uFFF0 \uD834\uDD1E \uD853\uDF5C e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_UHexBracketEscapeSequence_missingDigit() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\U{} e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_HEX_DIGIT_MISSING,
				1, 12,
				"abc \\U{} e",
				lexer );
		assertDetail(6, 4, "\\U{xxxxxxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_UHexBracketEscapeSequence_notClosed() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\U{1 e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_NOT_CLOSED,
				1, 12,
				"abc \\U{1 e",
				lexer );
		assertDetail(6, 4, "\\U{xxxxxxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\U{FFFFFFFFF} e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_NOT_CLOSED,
				1, 21,
				"abc \\U{FFFFFFFFF} e",
				lexer );
		assertDetail(6, 11, "\\U{xxxxxxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_NullChar() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \u0000 e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 9,
				"abc \u0000 e",
				lexer );
		assertDetail(6, 1, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\0 e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 10,
				"abc \\0 e",
				lexer );
		assertDetail(6, 2, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\x00 e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 12,
				"abc \\x00 e",
				lexer );
		assertDetail(6, 4, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\u0 e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 11,
				"abc \\u0 e",
				lexer );
		assertDetail(6, 3, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\u{0} e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 13,
				"abc \\u{0} e",
				lexer );
		assertDetail(6, 5, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\U0 e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 11,
				"abc \\U0 e",
				lexer );
		assertDetail(6, 3, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\U{0000} e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 16,
				"abc \\U{0000} e",
				lexer );
		assertDetail(6, 8, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_InvalidCodepoint() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\U110000 e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_CODEPOINT_INVALID,
				1, 16,
				"abc \\U110000 e",
				lexer );
		assertDetail(6, 8, "U+110000", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" 'abc \\U{110000} e' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_CODEPOINT_INVALID,
				1, 18,
				"abc \\U{110000} e",
				lexer );
		assertDetail(6, 10, "U+110000", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringS_with_Escape_unknown() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" 'abc \\c' ").init());
		assertNextToken(RTerminal.STRING_S, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNKOWN,
				1, 8,
				"abc \\c",
				lexer );
		assertDetail(6, 2, "\\c", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	
	@Test
	public void matchStringD_with_SpecialCharEscape() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" \"abc \\\' \\\" \\` \\a \\b \\f \\n \\r \\t \\v \\\\ \\\n e\" ").init());
		assertNextToken(RTerminal.STRING_D, 0,
				1, 43,
				"abc \' \" ` \u0007 \u0008 \f \n \r \t \u000B \\ \n e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	
	@Test
	public void matchSymbolG_with_SingleCharEscape() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\\' \\\" \\` \\a \\b \\f \\n \\r \\t \\v \\\\ \\\n e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, 0,
				1, 43,
				"abc \' \" ` \u0007 \u0008 \f \n \r \t \u000B \\ \n e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_OctalEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\1 \\02 \\003 \\777 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, 0,
				1, 24,
				"abc \u0001 \u0002 \u0003 \u01FF e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_xHexEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\x1 \\x02 \\xAA \\xff e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, 0,
				1, 26,
				"abc \u0001 \u0002 \u00AA \u00FF e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_uHexEscapeSequence_unexpected() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\u1 \\u02 \\u003 \\uAAAA \\uFFF0 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 36,
				"abc \\u1 \\u02 \\u003 \\uAAAA \\uFFF0 e",
				lexer );
		assertDetail(6, 3, "\\uxxxx", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_uHexBracketEscapeSequence_unexpected() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\u{1} \\u{02} \\u{003} \\u{AAAA} \\u{FFF0} e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 46,
				"abc \\u{1} \\u{02} \\u{003} \\u{AAAA} \\u{FFF0} e",
				lexer );
		assertDetail(6, 5, "\\u{xxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_uHexBracketEscapeSequence_notClosed() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\u{1 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 12,
				"abc \\u{1 e",
				lexer );
		assertDetail(6, 4, "\\u{xxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" `abc \\u{FFFFF} e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 17,
				"abc \\u{FFFFF} e",
				lexer );
		assertDetail(6, 7, "\\u{xxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_UHexEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\U1 \\U02 \\U003 \\UAAAA \\UFFF0 \\U1D11E \\U24F5C e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 52,
				"abc \\U1 \\U02 \\U003 \\UAAAA \\UFFF0 \\U1D11E \\U24F5C e",
				lexer );
		assertDetail(6, 3, "\\Uxxxxxxxx", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_UHexBracketEscapeSequence() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\U{1} \\U{02} \\U{003} \\U{AAAA} \\U{FFF0} \\U{1D11E} \\U{24F5C} e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 66,
				"abc \\U{1} \\U{02} \\U{003} \\U{AAAA} \\U{FFF0} \\U{1D11E} \\U{24F5C} e",
				lexer );
		assertDetail(6, 5, "\\U{xxxxxxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_UHexBracketEscapeSequence_notClosed() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\U{1 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 12,
				"abc \\U{1 e",
				lexer );
		assertDetail(6, 4, "\\U{xxxxxxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" `abc \\U{FFFFFFFFF} e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 21,
				"abc \\U{FFFFFFFFF} e",
				lexer );
		assertDetail(6, 11, "\\U{xxxxxxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_NullChar() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \u0000 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 9,
				"abc \u0000 e",
				lexer );
		assertDetail(6, 1, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" `abc \\0 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 10,
				"abc \\0 e",
				lexer );
		assertDetail(6, 2, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" `abc \\x00 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 12,
				"abc \\x00 e",
				lexer );
		assertDetail(6, 4, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" `abc \\u0 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 11,
				"abc \\u0 e",
				lexer );
		assertDetail(6, 3, "\\uxxxx", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" `abc \\u{0} e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 13,
				"abc \\u{0} e",
				lexer );
		assertDetail(6, 5, "\\u{xxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" `abc \\U0 e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 11,
				"abc \\U0 e",
				lexer );
		assertDetail(6, 3, "\\Uxxxxxxxx", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
		
		lexer.reset(this.input.reset(" `abc \\U{0000} e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 16,
				"abc \\U{0000} e",
				lexer );
		assertDetail(6, 8, "\\U{xxxxxxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_InvalidCodepoint() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\U{110000} e` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNEXPECTED,
				1, 18,
				"abc \\U{110000} e",
				lexer );
		assertDetail(6, 10, "\\U{xxxxxxxx}", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchSymbolG_with_Escape_unknown() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		
		lexer.reset(this.input.reset(" `abc \\c` ").init());
		assertNextToken(RTerminal.SYMBOL_G, TYPE123_SYNTAX_TEXT_ESCAPE_SEQ_UNKOWN,
				1, 8,
				"abc \\c",
				lexer );
		assertDetail(6, 2, "\\c", lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	
	@Test
	public void matchStringR_with_NullChar() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" r'(abc \u0000 e)' ").init());
		
		assertNextToken(RTerminal.STRING_R, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, 12,
				"abc \u0000 e",
				lexer );
		assertDetail(8, 1, null, lexer.getStatusDetail());
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@ParameterizedTest
	@ValueSource(strings= {
			" r'(abc \u0000 efg)' ",
			" r\"(abc \u0000\nefg )\" ",
			" r\"(abc \u0000\u0000efg )\" ",
	})
	public void matchStringR_with_NullChar(final String text) {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(text).init());
		
		assertNextToken(RTerminal.STRING_R, TYPE123_SYNTAX_TEXT_NULLCHAR,
				1, text.length() - 2, text.substring(1 + 3, text.length() - 2 - 1),
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
	@Test
	public void matchStringR_with_Escape() {
		final RLexer lexer= new RLexer(RLexer.SKIP_WHITESPACE);
		lexer.reset(this.input.reset(" r\"(abc \\\' \\\" \\` \\a \\b \\f \\n \\r \\t \\v \\\\ \\\n e)\" ").init());
		
		assertNextToken(RTerminal.STRING_R, 0,
				1, 46,
				"abc \\\' \\\" \\` \\a \\b \\f \\n \\r \\t \\v \\\\ \\\n e",
				lexer );
		assertEquals(RTerminal.EOF, lexer.next());
	}
	
}
