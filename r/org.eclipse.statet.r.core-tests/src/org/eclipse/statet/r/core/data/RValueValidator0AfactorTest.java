/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.data;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.RFactor32Store;


public class RValueValidator0AfactorTest extends AbstractRValueValidatorTest {
	
	
	public RValueValidator0AfactorTest() {
		super(new RFactor32Store(0, false, new String[] { "A", "B", null }));
	}
	
	
	@Override
	protected boolean isValidSource(final byte type) {
		return false;
	}
	
	@Override
	protected boolean isValidSourceNA(final byte type) {
		return (type == RStore.LOGICAL);
	}
	
	
	@Test
	public void parseLevel() {
		testValidate(true, "A");
		testValidate(true, "B");
		testValidate(false, "C");
		testValidate(true, "<NA>");
	}
	
	@Test
	public void parseLevelTrim() {
		testValidate(true, " A");
		testValidate(true, "B ");
		testValidate(false, "C ");
		testValidate(true, " <NA> ");
	}
	
}
