/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.r.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.r.core.source.RSourceConfig;
import org.eclipse.statet.r.core.source.ast.FCall.Args;


@NonNullByDefault
public class RParserFCallArgsTest {
	
	
	private final RParser rParser= new RParser(ModelManager.MODEL_FILE);
	
	private final StringParserInput input= new StringParserInput();
	
	
	public RParserFCallArgsTest() {
	}
	
	
	@ParameterizedTest
	@MethodSource("org.eclipse.statet.r.core.source.RSourceTests#RSourceConfigs_all")
	public void scan(final RSourceConfig rSourceConfig) {
		this.rParser.setRSourceConfig(rSourceConfig);
		
		assertArgs("a, b= 123", 0, 9, false);
		assertArgs("  a, b= 123   ", 2, 2 + 9, false);
		assertArgs("  a, b= 123,   ", 2, 2 + 10, false);
		assertArgs("  a, b= 123,   )", 2, 2 + 10, false);
		assertArgs("  a, b= 123,   \n   ", 2, 2 + 10, false);
		assertArgs("  a, b= 123,#comment\n   )", 2, 2 + 10, false);
		assertArgs("  a, b= 123,#comment\n#comment\n   )", 2, 2 + 10, false);
	}
	
	@Test
	public void scan_expand() {
		assertArgs("a, b= 123", 0, 9, true);
		assertArgs("  a, b= 123   ", 0, 2 + 9 + 3, true);
		assertArgs("  a, b= 123,   ", 0, 2 + 10 + 3, true);
		assertArgs("  a, b= 123,   )", 0, 2 + 10 + 3, true);
		assertArgs("  a, b= 123,   \n   ", 0, 2 + 10 + 4 + 3, true);
		assertArgs("  a, b= 123,#comment\n   )", 0, 2 + 10 + 9 + 3, true);
		assertArgs("  a, b= 123,#comment\n#comment\n   )", 0, 2 + 10 + 2*9 + 3, true);
	}
	
	
	private void assertArgs(final String code, final int startOffset, final int endOffset,
			final boolean expand) {
		final Args callArgs= this.rParser.parseFCallArgs(this.input.reset(code).init(), expand);
		assertEquals(startOffset, callArgs.getStartOffset(), "args.startOffset");
		assertEquals(endOffset, callArgs.getEndOffset(), "args.endOffset");
	}
	
}
