/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IExpressionManager;
import org.eclipse.debug.core.model.IDebugElement;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.debug.core.model.IWatchExpression;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.debug.ui.actions.IWatchExpressionFactoryAdapter;
import org.eclipse.debug.ui.actions.IWatchExpressionFactoryAdapterExtension;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.debug.core.RElementVariable;
import org.eclipse.statet.r.debug.core.RVariable;
import org.eclipse.statet.r.ui.editors.RSourceEditor;


public class WatchHandler extends AbstractDebugHandler
		implements IWatchExpressionFactoryAdapter, IWatchExpressionFactoryAdapterExtension {
	
	
	public WatchHandler() {
	}
	
	
	@Override
	public void setEnabled(final Object evaluationContext) {
		final IWorkbenchPart part= WorkbenchUIUtils.getActivePart(evaluationContext);
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(evaluationContext);
		if (part != null && selection != null) {
			if (selection instanceof final IStructuredSelection structSelection) {
				if (structSelection.isEmpty()) {
					setBaseEnabled(false);
					return;
				}
				for (final Object obj : structSelection.toList()) {
					if (obj instanceof RVariable && canCreateWatchExpression((RVariable)obj)) {
						continue;
					}
					setBaseEnabled(false);
					return;
				}
				setBaseEnabled(true);
				return;
			}
			else if (selection instanceof ITextSelection && part instanceof RSourceEditor) {
//				final SourceEditor sourceEditor= (SourceEditor) part;
//				final ITextSelection textSelection= (ITextSelection) selection;
				setBaseEnabled(true);
				return;
			}
		}
		setBaseEnabled(false);
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPart part= WorkbenchUIUtils.getActivePart(event.getApplicationContext());
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		if (part != null && selection != null) {
			int count= 0;
			final IDebugElement contextElement= getContextElement(getViewInput(part), part);
			if (selection instanceof final IStructuredSelection structSelection) {
				for (final Object obj : structSelection.toList()) {
					if (obj instanceof RVariable) {
						final String expression= createWatchExpression((RVariable)obj);
						if (expression != null) {
							installWatchExpression(expression, contextElement);
							count++;
						}
					}
				}
			}
			else if (selection instanceof ITextSelection && part instanceof RSourceEditor) {
				final SourceEditor sourceEditor= (SourceEditor) part;
				final ITextSelection textSelection= (ITextSelection) selection;
				final String expression= getExpressionText(textSelection, sourceEditor);
				if (expression != null) {
					installWatchExpression(expression, contextElement);
					count++;
				}
			}
			if (count > 0) {
				showView(part, IDebugUIConstants.ID_EXPRESSION_VIEW);
			}
		}
		return null;
	}
	
	
	private boolean canCreateWatchExpression(final RVariable rVariable) {
		final RElementVariable elementVariable= getElementVariable(rVariable);
		return (elementVariable != null && elementVariable.getFQElementName() != null);
	}
	
	private String createWatchExpression(final RVariable rVariable) {
		final RElementVariable elementVariable= getElementVariable(rVariable);
		if (elementVariable != null) {
			final RElementName elementName= elementVariable.getFQElementName();
			if (elementName != null) {
				return addIndex(
						elementName.getDisplayName(RElementName.DISPLAY_EXACT),
						getVariableItemIndex(rVariable) );
			}
		}
		return null;
	}
	
	
	@Override
	public boolean canCreateWatchExpression(final IVariable variable) {
		if (variable instanceof RVariable) {
			return canCreateWatchExpression((RVariable)variable);
		}
		return false;
	}
	
	@Override
	public String createWatchExpression(final IVariable variable) throws CoreException {
		if (variable instanceof RVariable) {
			return createWatchExpression((RVariable)variable);
		}
		return null;
	}
	
	
	private void installWatchExpression(final String snippet, final IDebugElement contextElement) {
		final IExpressionManager expressionManager= DebugPlugin.getDefault().getExpressionManager();
		final IWatchExpression expression= expressionManager.newWatchExpression(snippet);
		expressionManager.addExpression(expression);
		
		if (contextElement != null) {
			expression.setExpressionContext(contextElement);
		}
	}
	
}
