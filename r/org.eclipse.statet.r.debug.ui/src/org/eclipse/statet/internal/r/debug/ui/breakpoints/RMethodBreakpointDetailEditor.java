/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.breakpoints;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IPropertyListener;

import org.eclipse.statet.jcommons.collections.IdentitySet;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.r.debug.core.breakpoints.RMethodBreakpoint;


public class RMethodBreakpointDetailEditor extends RLineBreakpointDetailEditor {
	
	
	private RMethodBreakpoint breakpoint;
	
	private Button entryControl;
	private Button exitControl;
	
	private IObservableValue<Boolean> entryValue;
	private IObservableValue<Boolean> exitValue;
	
	
	public RMethodBreakpointDetailEditor(final boolean mnemonics, final boolean autosave,
			final IdentitySet<IPropertyListener> listeners) {
		super(mnemonics, autosave, listeners);
	}
	
	
	@Override
	protected void addContent(final Composite composite) {
		{	final Composite methodOptions= createMethodOptions(composite);
			methodOptions.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		
		super.addContent(composite);
	}
	
	protected Composite createMethodOptions(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newCompositeGrid(3));
		
		this.entryControl= new Button(composite, SWT.CHECK);
		this.entryControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		this.entryControl.setText(checkLabel("&Entry"));
		
		this.exitControl= new Button(composite, SWT.CHECK);
		this.exitControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		this.exitControl.setText(checkLabel("&Exit"));
		
		return composite;
	}
	
	@Override
	protected void addBindings(final DataBindingContext dbc, final Realm realm) {
		super.addBindings(dbc, realm);
		
		this.entryValue= new WritableValue<>(realm, Boolean.FALSE, Boolean.class);
		this.exitValue= new WritableValue<>(realm, Boolean.FALSE, Boolean.class);
		
		enableAutosave(dbc.bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.entryControl),
				this.entryValue ));
		enableAutosave(dbc.bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.exitControl),
				this.exitValue ));
	}
	
	
	@Override
	public void doSetInput(final Object input) {
		super.doSetInput(input);
		
		this.breakpoint= null;
		boolean control= false;
		boolean entry= false;
		boolean exit= false;
		
		if (input instanceof RMethodBreakpoint) {
			this.breakpoint= (RMethodBreakpoint)input;
			
			try {
				entry= this.breakpoint.isEntry();
				exit= this.breakpoint.isExit();
			}
			catch (final CoreException e) {
				logLoadError(e);
			}
			
			control= true;
		}
		
		this.entryControl.setEnabled(control);
		this.exitControl.setEnabled(control);
		
		this.entryValue.setValue(entry);
		this.exitValue.setValue(exit);
	}
	
	@Override
	public void doSave() {
		super.doSave();
		
		if (this.breakpoint != null) {
			try {
				this.breakpoint.setEntry(this.entryValue.getValue());
				this.breakpoint.setExit(this.exitValue.getValue());
			}
			catch (final CoreException e) {
				logSaveError(e);
			}
		}
	}
	
}
