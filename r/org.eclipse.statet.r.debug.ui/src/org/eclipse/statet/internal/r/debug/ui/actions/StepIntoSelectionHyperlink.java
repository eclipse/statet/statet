/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;

import org.eclipse.statet.internal.r.debug.ui.Messages;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.nico.AbstractRDbgController;


public class StepIntoSelectionHyperlink implements IHyperlink {
	
	
	private final IRegion fRegion;
	
	private final SourceEditor fEditor;
	private final AbstractDocument fDocument;
	private final RElementAccess fAccess;
	
	private final AbstractRDbgController fController;
	
	
	public StepIntoSelectionHyperlink(final SourceEditor editor, final RElementAccess access,
			final AbstractRDbgController controller) {
		assert (editor != null);
		assert (access != null);
		assert (controller != null);
		
		fRegion = JFaceTextRegion.toJFaceRegion(access.getNameNode());
		fEditor = editor;
		fDocument = (AbstractDocument) editor.getViewer().getDocument();
		fAccess = access;
		
		fController = controller;
	}
	
	
	@Override
	public String getTypeLabel() {
		return null;
	}
	
	@Override
	public IRegion getHyperlinkRegion() {
		return fRegion;
	}
	
	@Override
	public String getHyperlinkText() {
		return Messages.Hyperlink_StepInto_label;
	}
	
	@Override
	public void open() {
		StepIntoSelectionHandler.exec(fController, fDocument, fAccess, fEditor.getWorkbenchPart());
	}
	
}
