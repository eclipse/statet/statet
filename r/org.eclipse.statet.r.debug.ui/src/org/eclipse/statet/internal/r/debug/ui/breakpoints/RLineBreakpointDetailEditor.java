/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.breakpoints;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IPropertyListener;

import org.eclipse.statet.jcommons.collections.IdentitySet;

import org.eclipse.statet.ecommons.debug.ui.breakpoints.AbstractBreakpointDetailEditor;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.ltk.ui.sourceediting.SnippetEditor;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.debug.core.breakpoints.RLineBreakpoint;
import org.eclipse.statet.r.ui.sourceediting.RSourceViewerConfiguration;
import org.eclipse.statet.r.ui.sourceediting.RSourceViewerConfigurator;


public class RLineBreakpointDetailEditor extends AbstractBreakpointDetailEditor {
	
	
	private RLineBreakpoint breakpoint;
	
	private Button conditionEnabledControl;
	private SnippetEditor conditionCodeEditor;
	
	private IObservableValue<Boolean> conditionEnabledValue;
	private IObservableValue<String> conditionCodeValue;
	
	
	public RLineBreakpointDetailEditor(final boolean mnemonics, final boolean autosave,
			final IdentitySet<IPropertyListener> listeners) {
		super(mnemonics, autosave, listeners);
	}
	
	
	@Override
	protected void addContent(final Composite composite) {
		{	final Composite options= createConditionOptions(composite);
			options.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		}
	}
	
	protected Composite createConditionOptions(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newCompositeGrid(1));
		
		this.conditionEnabledControl= new Button(composite, SWT.CHECK);
		this.conditionEnabledControl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		this.conditionEnabledControl.setText(checkLabel("&Conditional / Expression:"));
		
		final RSourceViewerConfigurator viewerConfigurator= new RSourceViewerConfigurator(
				RCore.getWorkbenchAccess(),
				new RSourceViewerConfiguration(0, null) );
		this.conditionCodeEditor= new SnippetEditor(viewerConfigurator);
		this.conditionCodeEditor.create(composite, SWT.BORDER | SWT.H_SCROLL | SWT.MULTI | SWT.LEFT_TO_RIGHT);
		this.conditionCodeEditor.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		return composite;
	}
	
	@Override
	protected void addBindings(final DataBindingContext dbc, final Realm realm) {
		super.addBindings(dbc, realm);
		
		this.conditionEnabledValue= new WritableValue<>(realm, Boolean.FALSE, Boolean.class);
		this.conditionCodeValue= new WritableValue<>(realm, "", String.class);
		
		enableAutosave(dbc.bindValue(
				WidgetProperties.buttonSelection()
						.observe(this.conditionEnabledControl),
				this.conditionEnabledValue ));
		dbc.bindValue(
				WidgetProperties.text(SWT.Modify)
						.observe(this.conditionCodeEditor.getTextControl()),
				this.conditionCodeValue );
		dbc.bindValue(
				WidgetProperties.enabled()
						.observe(this.conditionCodeEditor.getTextControl()),
				this.conditionEnabledValue );
	}
	
	
	@Override
	public void doSetInput(final Object input) {
		super.doSetInput(input);
		
		this.breakpoint= null;
		boolean control= false;
		boolean enabled= false;
		String code= "";
		if (input instanceof RLineBreakpoint) {
			this.breakpoint= (RLineBreakpoint) input;
			
			int type= RLineBreakpoint.R_TOPLEVEL_COMMAND_ELEMENT_TYPE;
			try {
				type= this.breakpoint.getElementType();
				enabled= this.breakpoint.isConditionEnabled();
				code= this.breakpoint.getConditionExpr();
				if (code == null) {
					code= "";
				}
			}
			catch (final CoreException e) {
				logLoadError(e);
			}
			
			if (type == RLineBreakpoint.R_TOPLEVEL_COMMAND_ELEMENT_TYPE) {
				enabled= false;
				control= false;
			}
			else {
				control= true;
			}
		}
		
		this.conditionEnabledControl.setEnabled(control);
		
		this.conditionEnabledValue.setValue(enabled);
		this.conditionCodeValue.setValue(code);
	}
	
	@Override
	public void doSave() {
		super.doSave();
		
		if (this.breakpoint != null) {
			try {
				this.breakpoint.setConditionEnabled(this.conditionEnabledValue.getValue());
				this.breakpoint.setConditionExpr(this.conditionCodeValue.getValue());
			}
			catch (final CoreException e) {
				logSaveError(e);
			}
		}
	}
	
}
