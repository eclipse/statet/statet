/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.hyperlink.AbstractHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.r.debug.ui.RDebugUIUtils;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.nico.core.runtime.ToolStatus;
import org.eclipse.statet.r.core.model.RElementAccess;
import org.eclipse.statet.r.nico.AbstractRDbgController;


@NonNullByDefault
public class StepIntoSelectionHyperlinkDetector extends AbstractHyperlinkDetector {
	
	
	public StepIntoSelectionHyperlinkDetector() {
	}
	
	
	@Override
	public @NonNull IHyperlink @Nullable [] detectHyperlinks(final ITextViewer textViewer,
			final IRegion region, final boolean canShowMultipleHyperlinks) {
		final SourceEditor editor = getAdapter(SourceEditor.class);
		if (editor == null) {
			return null;
		}
		final AbstractRDbgController controller = RDebugUIUtils.getRDbgController(editor);
		if (controller == null || controller.getStatus() != ToolStatus.STARTED_SUSPENDED) {
			return null;
		}
		final RElementAccess access = StepIntoSelectionHandler.searchAccess(editor, region);
		if (access != null) {
			return new @NonNull IHyperlink[] {
					new StepIntoSelectionHyperlink(editor, access, controller) };
		}
		return null;
	}
	
}
