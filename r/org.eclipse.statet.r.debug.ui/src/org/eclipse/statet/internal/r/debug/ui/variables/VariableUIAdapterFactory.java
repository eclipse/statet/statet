/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.variables;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.debug.internal.ui.viewers.model.provisional.IElementEditor;
import org.eclipse.debug.internal.ui.viewers.model.provisional.IElementLabelProvider;
import org.eclipse.debug.ui.actions.IWatchExpressionFactoryAdapter;

import org.eclipse.statet.internal.r.debug.ui.actions.WatchHandler;
import org.eclipse.statet.r.debug.core.RVariable;


public class VariableUIAdapterFactory implements IAdapterFactory {
	
	
	private static final Class<?>[] ADAPTERS= new Class<?>[] {
		IElementLabelProvider.class,
		IElementEditor.class,
		IWatchExpressionFactoryAdapter.class,
	};
	
	
	private final IElementLabelProvider labelProvider= new RVariableLabelProvider();
	private final IElementEditor variableEditor= new RVariableEditor();
	
	private final IWatchExpressionFactoryAdapter watchExprFactory= new WatchHandler();
	
	
	public VariableUIAdapterFactory() {
	}
	
	
	@Override
	public Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == IElementLabelProvider.class) {
			if (adaptableObject instanceof RVariable) {
				return (T) this.labelProvider;
			}
		}
		else if (adapterType == IElementEditor.class) {
			if (adaptableObject instanceof RVariable) {
				return (T) this.variableEditor;
			}
		}
		else if (adapterType == IWatchExpressionFactoryAdapter.class) {
			if (adaptableObject instanceof RVariable) {
				return (T) this.watchExprFactory;
			}
		}
		return null;
	}
	
}
