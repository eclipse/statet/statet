/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.r.debug.ui.RDebugUIUtils;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InfoHover;
import org.eclipse.statet.nico.core.runtime.ToolStatus;
import org.eclipse.statet.r.console.core.IRDataAdapter;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.r.core.model.RElementName;
import org.eclipse.statet.r.debug.core.RStackFrame;
import org.eclipse.statet.r.ui.rtool.RElementInfoHoverCreator;
import org.eclipse.statet.r.ui.rtool.RElementInfoTask;
import org.eclipse.statet.r.ui.sourceediting.RAssistInvocationContext;


@NonNullByDefault
public class RDebugHover implements InfoHover {
	
	
	private final int mode;
	
	private @Nullable IInformationControlCreator controlCreator;
	
	
	public RDebugHover() {
		this.mode= MODE_TOOLTIP;
	}
	
	
	@Override
	public @Nullable Object getHoverInfo(final AssistInvocationContext context,
			final ProgressMonitor m) {
		m.beginSubTask("Looking up element info from R console...");
		
		final IWorkbenchPart workbenchPart= context.getEditor().getWorkbenchPart();
		final RProcess process= RDebugUIUtils.getRProcess(workbenchPart);
		if (process == null || !(context instanceof RAssistInvocationContext)) {
			return null;
		}
		final RElementName name= ((RAssistInvocationContext) context).getNameSelection();
		if (name != null) {
			final RElementInfoTask info= new RElementInfoTask(name) {
				@Override
				protected int getFramePosition(final IRDataAdapter r,
						final ProgressMonitor m) throws StatusException {
					if (process.getToolStatus() == ToolStatus.STARTED_SUSPENDED) {
						final RStackFrame frame= RDebugUIUtils.getFrame(workbenchPart, process);
						if (frame != null && frame.getPosition() > 0) {
							return frame.getPosition();
						}
					}
					return 0;
				}
			};
			if (info.preCheck()) {
				return info.load(process, context.getSourceViewer().getTextWidget(), workbenchPart);
			}
		}
		return null;
	}
	
	@Override
	public @Nullable IInformationControlCreator getHoverControlCreator() {
		if (this.controlCreator == null) {
			this.controlCreator= new RElementInfoHoverCreator(this.mode);
		}
		return this.controlCreator;
	}
	
}
