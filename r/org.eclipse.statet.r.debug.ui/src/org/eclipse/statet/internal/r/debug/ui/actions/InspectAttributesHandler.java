/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class InspectAttributesHandler extends InspectHandler {
	
	
	public InspectAttributesHandler() {
	}
	
	
	@Override
	protected String getCommandId() {
		return "org.eclipse.statet.r.commands.InspectAttributes"; //$NON-NLS-1$
	}
	
	@Override
	protected String toCommandExpression(final String expression) {
		return "attributes(" + expression + ')'; //$NON-NLS-1$
	}
	
}
