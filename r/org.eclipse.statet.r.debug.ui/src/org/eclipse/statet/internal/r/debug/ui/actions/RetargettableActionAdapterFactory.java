/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.actions;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.debug.ui.actions.IToggleBreakpointsTarget;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.r.debug.ui.actions.RToggleBreakpointAdapter;


@NonNullByDefault
public class RetargettableActionAdapterFactory implements IAdapterFactory {
	
	
	private static final @NonNull Class<?>[] ADAPTERS= new @NonNull Class<?>[] {
				IToggleBreakpointsTarget.class
			};
	
	
	private @Nullable IToggleBreakpointsTarget breakpointAdapter;
	
	
	public RetargettableActionAdapterFactory() {
	}
	
	
	@Override
	public @NonNull Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == IToggleBreakpointsTarget.class) {
			synchronized (this) {
				var adapter= this.breakpointAdapter;
				if (adapter == null) {
					adapter= new RToggleBreakpointAdapter();
					this.breakpointAdapter= adapter;
				}
				return (T)adapter;
			}
		} 
		return null;
	}
	
}
