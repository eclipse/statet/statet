/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.variables;

import org.eclipse.debug.internal.ui.model.elements.VariableEditor;
import org.eclipse.debug.internal.ui.viewers.model.provisional.IPresentationContext;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.swt.widgets.Composite;


public class RVariableEditor extends VariableEditor {
	
	
	public RVariableEditor() {
	}
	
	
	@Override
	public CellEditor getCellEditor(final IPresentationContext context, final String columnId,
			final Object element, final Composite parent) {
		return super.getCellEditor(context, columnId, element, parent);
	}
	
	@Override
	public ICellModifier getCellModifier(final IPresentationContext context,
			final Object element) {
		return new RVariableCellModifier();
	}
	
}
