/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui;

import org.eclipse.jface.resource.ImageDescriptor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ltk.ui.input.BasicSourceFragmentEditorInput;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.r.debug.core.sourcelookup.RRuntimeSourceFragment;


@NonNullByDefault
public class RRuntimeSourceEditorInput extends BasicSourceFragmentEditorInput<RRuntimeSourceFragment> {
	
	
	public RRuntimeSourceEditorInput(final RRuntimeSourceFragment fragment) {
		super(fragment);
	}
	
	
	@Override
	public boolean exists() {
		return !this.fragment.getProcess().isTerminated();
	}
	
	
	@Override
	public @Nullable ImageDescriptor getImageDescriptor() {
		return RDebugUIPlugin.getInstance().getImageRegistry().getDescriptor(
				RDebugUIPlugin.IMG_OBJ_R_SOURCE_FROM_RUNTIME );
	}
	
	@Override
	public String getToolTipText() {
		return this.fragment.getFullName() + '\n' + this.fragment.getProcess().getLabel(Tool.LONG_LABEL);
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == ToolProcess.class) {
			return (T)this.fragment.getProcess();
		}
		return null;
	}
	
	
	@Override
	public String toString() {
		return this.fragment.getFullName() + " - " + this.fragment.getProcess().getLabel(Tool.LONG_LABEL); //$NON-NLS-1$
	}
	
}
