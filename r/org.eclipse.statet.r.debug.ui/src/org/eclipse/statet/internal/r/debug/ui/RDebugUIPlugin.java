/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.ui.jface.resource.ImageDescriptorRegistry;
import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.internal.r.debug.ui.breakpoints.BreakpointsHelper;


public class RDebugUIPlugin extends AbstractUIPlugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.r.debug.ui"; //$NON-NLS-1$
	
	
	private static final String R_IMAGES_ID= "org.eclipse.statet.r.images"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_R_BREAKPOINT= R_IMAGES_ID + "/obj/r_breakpoint"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_BREAKPOINT_DISABLED= R_IMAGES_ID + "/obj/r_breakpoint.disabled"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_TOPLEVEL_BREAKPOINT= R_IMAGES_ID + "/obj/r_toplevel_breakpoint"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_TOPLEVEL_BREAKPOINT_DISABLED= R_IMAGES_ID + "/obj/r_toplevel_breakpoint.disabled"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_EXCEPTION_BREAKPOINT= R_IMAGES_ID + "/obj/r_exception"; //$NON-NLS-1$
	public static final String IMG_OBJ_R_EXCEPTION_BREAKPOINT_DISABLED= R_IMAGES_ID + "/obj/r_exception.disabled"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_R_INSPECT_EXPRESSION= R_IMAGES_ID + "/obj/r_inspect_expression"; //$NON-NLS-1$
	
	public static final String IMG_OBJ_R_SOURCE_FROM_RUNTIME= R_IMAGES_ID + "/obj/r_source.runtime"; //$NON-NLS-1$
	
	
	private static RDebugUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static RDebugUIPlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private List<Disposable> disposables;
	
	private ImageDescriptorRegistry imageDescriptorRegistry;
	private BreakpointsHelper breakpointsHelper;
	
	
	public RDebugUIPlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.disposables= new ArrayList<>();
		
		this.breakpointsHelper= new BreakpointsHelper();
		this.disposables.add(this.breakpointsHelper);
		
		synchronized (this) {
			this.started= true;
		}
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
				this.imageDescriptorRegistry= null;
				this.breakpointsHelper= null;
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					log(new Status(IStatus.ERROR, RDebugUIPlugin.BUNDLE_ID,
							"Error occured while disposing a module.", //$NON-NLS-1$
							e ));
				}
			}
			this.disposables= null;
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(IMG_OBJ_R_BREAKPOINT, ImageRegistryUtil.T_OBJ, "r_breakpoint.png"); //$NON-NLS-1$
		util.register(IMG_OBJ_R_BREAKPOINT_DISABLED, ImageRegistryUtil.T_OBJ, "r_breakpoint-disabled.png"); //$NON-NLS-1$
		util.register(IMG_OBJ_R_TOPLEVEL_BREAKPOINT, ImageRegistryUtil.T_OBJ, "r_toplevel_breakpoint.png"); //$NON-NLS-1$
		util.register(IMG_OBJ_R_TOPLEVEL_BREAKPOINT_DISABLED, ImageRegistryUtil.T_OBJ, "r_toplevel_breakpoint-disabled.png"); //$NON-NLS-1$
		util.register(IMG_OBJ_R_EXCEPTION_BREAKPOINT, ImageRegistryUtil.T_OBJ, "r_exception.png"); //$NON-NLS-1$
		util.register(IMG_OBJ_R_EXCEPTION_BREAKPOINT_DISABLED, ImageRegistryUtil.T_OBJ, "r_exception-disabled.png"); //$NON-NLS-1$
		
		util.register(IMG_OBJ_R_INSPECT_EXPRESSION, ImageRegistryUtil.T_OBJ, "r_insp_expression.png"); //$NON-NLS-1$
		
		util.register(IMG_OBJ_R_SOURCE_FROM_RUNTIME, ImageRegistryUtil.T_OBJ, "r_source-runtime.png"); //$NON-NLS-1$
	}
	
	public synchronized ImageDescriptorRegistry getImageDescriptorRegistry() {
		if (this.imageDescriptorRegistry == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.imageDescriptorRegistry= new ImageDescriptorRegistry();
		}
		return this.imageDescriptorRegistry;
	}
	
}
