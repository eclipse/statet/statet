/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.variables;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.internal.ui.DebugUIPlugin;
import org.eclipse.debug.internal.ui.actions.variables.details.DetailPaneAssignValueAction;
import org.eclipse.debug.internal.ui.elements.adapters.DefaultVariableCellModifier;
import org.eclipse.debug.ui.IDebugUIConstants;

import org.eclipse.statet.r.debug.core.RVariable;


public class RVariableCellModifier extends DefaultVariableCellModifier {
	
	
	public RVariableCellModifier() {
	}
	
	
//	@Override
//	public boolean canModify(Object element, String property) {
//		return super.canModify(element, property);
//	}
	
	@Override
	public Object getValue(final Object element, final String property) {
		if (IDebugUIConstants.COLUMN_ID_VARIABLE_VALUE.equals(property)) {
			if (element instanceof final RVariable variable) {
				try {
					return variable.getValue().getValueString();
				}
				catch (final DebugException e) {
					DebugUIPlugin.log(e);
				}
				return null;
			}
		}
		return super.getValue(element, property);
	}
	
	@Override
	public void modify(final Object element, final String property, final Object value) {
		if (IDebugUIConstants.COLUMN_ID_VARIABLE_VALUE.equals(property)) {
			if (element instanceof final RVariable variable) {
				final Object oldValue= getValue(element, property);
				if (!value.equals(oldValue)) {
					if (value instanceof String) {
						DetailPaneAssignValueAction.assignValue(DebugUIPlugin.getShell(), variable,
								(String) value );
					}
				}
				return;
			}
		}
		super.modify(element, property, value);
	}
	
}
