/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.contexts.IDebugContextService;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.nico.core.runtime.ToolProcess;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.r.console.core.RConsoleTool;
import org.eclipse.statet.r.console.core.RProcess;
import org.eclipse.statet.r.debug.core.RStackFrame;
import org.eclipse.statet.r.nico.AbstractRDbgController;


@NonNullByDefault
public class RDebugUIUtils {
	
	
	public static @Nullable RProcess getRProcess(final IWorkbenchPart workbenchPart) {
		final Tool tool= NicoUITools.getTool(workbenchPart);
		return (tool != null && tool.getMainType() == RConsoleTool.TYPE
						&& tool instanceof RProcess) ?
				(RProcess)tool : null;
	}
	
	public static @Nullable AbstractRDbgController getRDbgController(final SourceEditor editor) {
		final IWorkbenchPart workbenchPart= editor.getWorkbenchPart();
		if (editor == null || workbenchPart == null) {
			return null;
		}
		final RProcess process= getRProcess(workbenchPart);
		if (process == null
				|| !(process.getController() instanceof AbstractRDbgController) ) {
			return null;
		}
		return (AbstractRDbgController)process.getController();
	}
	
	public static @Nullable RStackFrame getFrame(final IWorkbenchPart part, final ToolProcess process) {
		return UIAccess.syncExecGet(() -> {
			final IWorkbenchPartSite site= part.getSite();
			if (site == null) {
				return null;
			}
			final IDebugContextService contextService= DebugUITools.getDebugContextManager()
					.getContextService(site.getWorkbenchWindow());
			final ISelection selection= contextService.getActiveContext();
			if (selection instanceof IStructuredSelection) {
				final var firstElement= ((IStructuredSelection)selection).getFirstElement();
				if (firstElement instanceof IAdaptable) {
					return ((IAdaptable)firstElement).getAdapter(RStackFrame.class);
				}
			}
			return null;
		});
	}
	
}
