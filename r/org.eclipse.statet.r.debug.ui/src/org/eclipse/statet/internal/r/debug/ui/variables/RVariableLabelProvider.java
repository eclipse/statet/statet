/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.r.debug.ui.variables;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.debug.internal.ui.model.elements.VariableLabelProvider;
import org.eclipse.debug.internal.ui.viewers.model.provisional.IPresentationContext;

import org.eclipse.statet.r.debug.core.RVariable;


public class RVariableLabelProvider extends VariableLabelProvider {
	
	
	public RVariableLabelProvider() {
	}
	
	
	@Override
	protected String getVariableName(final IVariable variable,
			final IPresentationContext context) throws CoreException {
		if (variable instanceof RVariable) {
			return variable.getName();
		}
		return super.getVariableName(variable, context);
	}
	
	@Override
	protected String getValueText(final IVariable variable, final IValue value,
			final IPresentationContext context) throws CoreException {
		if (variable instanceof RVariable) {
			return value.getValueString();
		}
		return super.getValueText(variable, value, context);
	}
	
}
