/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.e.srvext;

import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.rj.server.srvext.ServerRuntimePlugin;


/**
 * Creates a display for native widgets ('native' depends on SWT in classpath).
 */
public class SWTPlugin implements ServerRuntimePlugin {
	
	
	private Display display;
	
	
	public SWTPlugin() {
	}
	
	
	@Override
	public String getSymbolicName() {
		return "swt";
	}
	
	@Override
	public void rjIdle() throws Exception {
		if (this.display == null) {
			Display.setAppName("R");
			this.display= new Display();
		}
		this.display.readAndDispatch();
	}
	
	@Override
	public void rjStop(final int regular) throws Exception {
		if (this.display != null) {
			this.display.dispose();
			this.display= null;
		}
	}
	
}
