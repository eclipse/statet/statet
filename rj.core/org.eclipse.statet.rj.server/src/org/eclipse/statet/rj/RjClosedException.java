/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj;


/**
 * Exception indicating that the used connection (to R) is already closed
 */
public class RjClosedException extends RjException {
	
	
	private static final long serialVersionUID= 5199233227969338152L;
	
	
	public RjClosedException(final String message) {
		super(message);
	}
	
	public RjClosedException(final String message, final Throwable cause) {
		super(message, cause);
	}
	
}
