/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.srv.engine;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.server.Server;
import org.eclipse.statet.rj.server.srvext.ServerRuntimePlugin;


/**
 * Interface for {@link Server} for local (none-exported) methods
 */
@NonNullByDefault
public interface SrvEnginePluginExtension {
	
	
	public void addPlugin(ServerRuntimePlugin plugin);
	
	public void removePlugin(ServerRuntimePlugin plugin);
	
}
