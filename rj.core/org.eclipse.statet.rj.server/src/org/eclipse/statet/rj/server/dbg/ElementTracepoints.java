/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class ElementTracepoints implements RJIOExternalizable {
	
	
	private final SrcfileData fileInfo;
	
	private final String elementId;
	private final int @Nullable [] elementSrcref;
	
	private final List<TracepointPosition> positions;
	
	
	public ElementTracepoints(final SrcfileData fileInfo,
			final String elementId, final int @Nullable [] elementSrcref,
			final List<TracepointPosition> positions) {
		if (fileInfo == null) {
			throw new NullPointerException("fileInfo"); //$NON-NLS-1$
		}
		if (elementId == null) {
			throw new NullPointerException("elementId"); //$NON-NLS-1$
		}
		this.fileInfo= fileInfo;
		this.elementId= elementId;
		this.elementSrcref= elementSrcref;
		this.positions= positions;
	}

	public ElementTracepoints(final SrcfileData fileInfo,
			final String elementId, final int @Nullable [] elementSrcref) {
		this(fileInfo, elementId, elementSrcref, new ArrayList<>(4));
	}
	
	public ElementTracepoints(final RJIO io) throws IOException {
		this.fileInfo= new SrcfileData(io);
		this.elementId= io.readString();
		this.elementSrcref= io.readIntArray();
		final int l= io.readInt();
		this.positions= new ArrayList<>(l);
		for (int i= 0; i < l; i++) {
			this.positions.add(new TracepointPosition(io));
		}
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		this.fileInfo.writeExternal(io);
		io.writeString(this.elementId);
		io.writeIntArray(this.elementSrcref, (this.elementSrcref != null) ? 6 : -1);
		final int l= this.positions.size();
		io.writeInt(l);
		for (int i= 0; i < l; i++) {
			this.positions.get(i).writeExternal(io);
		}
	}
	
	
	public SrcfileData getSrcfile() {
		return this.fileInfo;
	}
	
	public String getElementId() {
		return this.elementId;
	}
	
	public int @Nullable [] getElementSrcref() {
		return this.elementSrcref;
	}
	
	public List<? extends TracepointPosition> getPositions() {
		return this.positions;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder("ElementTracepoints"); //$NON-NLS-1$
		sb.append(" for ").append(this.elementId); //$NON-NLS-1$
		sb.append("\n").append("list (count= ").append(this.positions.size()).append("):"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		for (int i= 0; i < this.positions.size(); i++) {
			sb.append("\n").append(this.positions.get(i).toString()); //$NON-NLS-1$
		}
		return sb.toString();
	}
	
}
