/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class TracepointState implements Tracepoint, Comparable<TracepointState>, RJIOExternalizable {
	
	
	static final List<TracepointState> readList(final RJIO io) throws IOException {
		final int l= io.readInt();
		final List<TracepointState> list= new ArrayList<>(l);
		String currentFilePath= null;
		String currentElementId= null;
		for (int i= 0; i < l; i++) {
			final TracepointState state= new TracepointState();
			list.add(state);
			state.type= io.readInt();
			state.id= io.readLong();
			state.filePath= io.readString();
			if (state.filePath != null) {
				currentFilePath= state.filePath;
			}
			else {
				state.filePath= currentFilePath;
			}
			if (state.type == TYPE_DELETED) {
				continue;
			}
			state.elementId= io.readString();
			if (state.elementId != null) {
				currentElementId= state.elementId;
			}
			else {
				state.elementId= currentElementId;
			}
			state.index= io.readIntArray();
			state.elementLabel= io.readString();
			state.flags= io.readInt();
			state.expr= io.readString();
		}
		return list;
	}
	
	static final void writeList(final List<TracepointState> list, final RJIO io) throws IOException {
		final int l= list.size();
		io.writeInt(l);
		String currentFilePath= null;
		String currentElementId= null;
		for (int i= 0; i < l; i++) {
			final TracepointState state= list.get(i);
			io.writeInt(state.type);
			io.writeLong(state.id);
			if (state.filePath.equals(currentFilePath)) {
				io.writeString(null);
			}
			else {
				currentFilePath= state.filePath;
				io.writeString(currentFilePath);
			}
			if (state.type == TYPE_DELETED) {
				continue;
			}
			if (state.elementId.equals(currentElementId)) {
				io.writeString(null);
			}
			else {
				currentElementId= state.elementId;
				io.writeString(currentElementId);
			}
			io.writeIntArray(state.index, state.index.length);
			io.writeString(state.elementLabel);
			io.writeInt(state.flags);
			io.writeString(state.expr);
		}
	}
	
	
	public static final int FLAG_ENABLED=                  0x00000001;
	
	public static final int FLAG_MB_ENTRY=                 0x00010000;
	public static final int FLAG_MB_EXIT=                  0x00020000;
	
	public static final int FLAG_EXPR_INVALID=             0x01000000;
	public static final int FLAG_EXPR_EVAL_FAILED=         0x02000000;
	
	public static final String EB_FILEPATH= "exception"; //$NON-NLS-1$
	
	
	private static final int[] NO_IDX= new int[0];
	
	
	private int type;
	
	private String filePath;
	private long id;
	
	private String elementId;
	private int[] index;
	
	protected @Nullable String elementLabel;
	protected int flags;
	protected @Nullable String expr;
	
	
	public TracepointState(final int type,
			final String filePath, final long id, final String elementId, final int[] index,
			final @Nullable String elementLabel, final int flags, final @Nullable String expr) {
		if (filePath == null || elementId == null || index == null) {
			throw new NullPointerException();
		}
		this.type= type;
		this.filePath= filePath;
		this.id= id;
		this.elementId= elementId;
		this.index= index;
		this.elementLabel= elementLabel;
		this.flags= flags;
		this.expr= expr;
	}
	
	public TracepointState(final int type,
			final String filePath, final long id, final String elementId,
			final @Nullable String elementLabel, final int flags, final @Nullable String expr) {
		if (filePath == null || elementId == null) {
			throw new NullPointerException();
		}
		this.type= type;
		this.filePath= filePath;
		this.id= id;
		this.elementId= elementId;
		this.index= NO_IDX;
		this.elementLabel= elementLabel;
		this.flags= flags;
		this.expr= expr;
	}
	
	@SuppressWarnings("null")
	public TracepointState(final int type,
			final String filePath, final long id) {
		if (type != TYPE_DELETED) {
			throw new IllegalArgumentException("type= " + type); //$NON-NLS-1$
		}
		this.type= type;
		this.filePath= filePath;
		this.id= id;
	}
	
	@SuppressWarnings("null")
	private TracepointState() {
	}
	
	public TracepointState(final RJIO io) throws IOException {
		this.type= io.readInt();
		this.id= io.readLong();
		this.filePath= io.readString();
		if (this.type == TYPE_DELETED) {
			return;
		}
		this.elementId= io.readString();
		this.index= io.readIntArray();
		this.elementLabel= io.readString();
		this.flags= io.readInt();
		this.expr= io.readString();
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeInt(this.type);
		io.writeLong(this.id);
		io.writeString(this.filePath);
		if (this.type == TYPE_DELETED) {
			return;
		}
		io.writeString(this.elementId);
		io.writeIntArray(this.index, this.index.length);
		io.writeString(this.elementLabel);
		io.writeInt(this.flags);
		io.writeString(this.expr);
	}
	
	@Override
	public TracepointState clone() {
		return new TracepointState(this.type,
				this.filePath, this.id, this.elementId, this.index,
				this.elementLabel, this.flags, this.expr );
	}
	
	
	@Override
	public int getType() {
		return this.type;
	}
	
	public String getFilePath() {
		return this.filePath;
	}
	
	@Override
	public long getId() {
		return this.id;
	}
	
	public String getElementId() {
		return this.elementId;
	}
	
	public int[] getIndex() {
		return this.index;
	}
	
	public boolean isEnabled() {
		return ((this.flags & FLAG_ENABLED) != 0);
	}
	
	public @Nullable String getElementLabel() {
		return this.elementLabel;
	}
	
	public int getFlags() {
		return this.flags;
	}
	
	public @Nullable String getExpr() {
		return this.expr;
	}
	
	
	@Override
	public int compareTo(final TracepointState other) {
		{	final int diff= this.filePath.compareTo(other.filePath);
			if (diff != 0) {
				return diff;
			}
		}
		if (this.elementId != null) {
			if (other.elementId != null) {
				final int diff= this.elementId.compareTo(other.elementId);
				if (diff != 0) {
					return diff;
				}
			}
			else {
				return 0x1000;
			}
		}
		else if (other.elementId != null) {
			return -0x1000;
		}
		return (this.id < other.id) ? -0x1 : 0x1;
	}
	
	@Override
	public int hashCode() {
		return (int)(this.filePath.hashCode() * this.id);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final TracepointState other
						&& this.id == other.id
						&& this.filePath.equals(other.filePath) ));
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(TracepointState.class);
		Tracepoints.append(this.id, sb.getStringBuilder());
		sb.append(" in ");
		sb.append('\'', this.filePath, '\'');
		sb.addProp("type", Tracepoints.getTypeName(this.type));
		if (this.type != TYPE_DELETED) {
			sb.addProp("elementId", this.elementId);
			sb.addProp("index", Arrays.toString(this.index));
		}
		return sb.toString();
	}
	
}
