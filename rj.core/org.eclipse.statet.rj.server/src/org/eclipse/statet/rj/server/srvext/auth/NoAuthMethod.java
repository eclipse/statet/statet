/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.srvext.auth;

import javax.security.auth.callback.Callback;
import javax.security.auth.login.LoginException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.server.srvext.ServerAuthMethod;


/**
 * Authentication method 'none'
 * without any authentication mechanism.
 */
@NonNullByDefault
public class NoAuthMethod extends ServerAuthMethod {
	
	
	public NoAuthMethod() {
		super("none", false);
	}
	
	public NoAuthMethod(final String client) {
		super("none", false);
		setExpliciteClient(client);
		try {
			init("");
		}
		catch (final RjException e) {}
	}
	
	
	@Override
	public void doInit(final @Nullable String arg) throws RjException {
	}
	
	@Override
	protected ImList<Callback> doCreateLogin() throws RjException {
		return ImCollections.emptyList();
	}
	
	@Override
	protected String doPerformLogin(final ImList<Callback> callbacks) throws LoginException, RjException {
		return "-";
	}
	
}
