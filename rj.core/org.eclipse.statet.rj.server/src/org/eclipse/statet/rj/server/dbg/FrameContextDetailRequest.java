/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class FrameContextDetailRequest implements RJIOExternalizable {
	
	
	private final int position;
	
	
	public FrameContextDetailRequest(final int position) {
		this.position= position;
	}
	
	public FrameContextDetailRequest(final RJIO io) throws IOException {
		this.position= io.readInt();
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeInt(this.position);
	}
	
	
	public int getPosition() {
		return this.position;
	}
	
}
