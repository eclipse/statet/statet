/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;


/**
 * Command for main loop console output.
 */
@NonNullByDefault
public final class ConsoleWriteCmdItem extends MainCmdItem {
	
	
	public static final byte R_OUTPUT=                      1;
	public static final byte R_ERROR=                       2;
	public static final byte SYS_OUTPUT=                    5;
	
	
	private final byte streamId;
	
	private final String text;
	
	
	public ConsoleWriteCmdItem(final byte streamId, final String text) {
		assert (text != null);
		this.streamId= streamId;
		this.text= text;
	}
	
	public ConsoleWriteCmdItem(final byte streamId, final byte[] text) {
		this.streamId= streamId;
		this.text= new String(text, StandardCharsets.UTF_8);
	}
	
	/**
	 * Constructor for deserialization
	 */
	public ConsoleWriteCmdItem(final RJIO in) throws IOException {
		this.streamId= in.readByte();
		this.text= in.readString();
	}
	
	@Override
	public void writeExternal(final RJIO out) throws IOException {
		out.writeByte(this.streamId);
		out.writeString(this.text);
	}
	
	
	@Override
	public byte getCmdType() {
		return T_CONSOLE_WRITE_ITEM;
	}
	
	@Override
	public byte getOp() {
		return this.streamId;
	}
	
	
	@Override
	public void setAnswer(final RjsStatus status) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public boolean isOK() {
		return true;
	}
	
	@Override
	public @Nullable RjsStatus getStatus() {
		return null;
	}
	
	@Override
	public String getDataText() {
		return this.text;
	}
	
	@Override
	public byte [] getDataTextUtf8() {
		return this.text.getBytes(StandardCharsets.UTF_8);
	}
	
	
	@Override
	public boolean testEquals(final MainCmdItem other) {
		return (other instanceof final ConsoleWriteCmdItem otherItem
				&& this.options == otherItem.options
				&& this.streamId == otherItem.streamId
				&& this.text.equals(otherItem.getDataText()) );
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(128);
		sb.append("ConsoleWriteCmdItem (").append(this.streamId).append(")");
		sb.append("\n\t").append("options= 0x").append(Integer.toHexString(this.options));
		sb.append("\n<TEXT>\n");
		sb.append(this.text);
		sb.append("\n</TEXT>");
		return sb.toString();
	}
	
}
