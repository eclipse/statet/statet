/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Command for simple ping. Should be answered directly with a {@link RjsStatus}.
 */
public final class RjsPing implements RjsComObject, Externalizable {
	
	
	public static final RjsPing INSTANCE= new RjsPing();
	
	
	public RjsPing() {
	}
	
	
	@Override
	public void writeExternal(final ObjectOutput out) throws IOException {
	}
	
	@Override
	public void readExternal(final ObjectInput in) throws IOException, ClassNotFoundException {
	}
	
	
	@Override
	public int getComType() {
		return RjsComObject.T_PING;
	}
	
	
	@Override
	public int hashCode() {
		return 1;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj instanceof RjsPing);
	}
	
	
	@Override
	public String toString() {
		return "RjsPing";
	}
	
}
