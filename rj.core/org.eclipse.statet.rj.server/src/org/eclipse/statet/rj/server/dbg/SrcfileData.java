/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class SrcfileData implements RJIOExternalizable {
	
	
	private final @Nullable String path;
	private final @Nullable String name;
	private final long timestamp;
	
	
	public SrcfileData(final @Nullable String path, final @Nullable String name,
			final long timestamp) {
		this.path= path;
		this.name= name;
		this.timestamp= timestamp;
	}
	
	public SrcfileData(final RJIO io) throws IOException {
		this.path= io.readString();
		this.name= io.readString();
		this.timestamp= io.readLong();
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeString(this.path);
		io.writeString(this.name);
		io.writeLong(this.timestamp);
	}
	
	
	/**
	 * Returns the file path in the application (e.g. path in Eclipse workspace)
	 * @return the path or <code>null</code>
	 */
	public @Nullable String getPath() {
		return this.path;
	}
	
	/**
	 * Returns the complete standardized file path usually compatible with 'filename' properties in
	 * R.
	 * @return the path or <code>null</code>
	 */
	public @Nullable String getName() {
		return this.name;
	}
	
	/**
	 * Returns the modification timestamp of the file (milliseconds since 1970-01-01T00:00).
	 * 
	 * @return the timestamp or <code>0</code>
	 */
	public long getTimestamp() {
		return this.timestamp;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hashCode(this.path) * 17
				^ Objects.hashCode(this.name)
				^ (int)(this.timestamp ^ (this.timestamp >>> 32));
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final SrcfileData other
					&& Objects.equals(this.path, other.path)
					&& Objects.equals(this.name, other.name)
					&& this.timestamp == other.timestamp ));
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder("SrcfileData"); //$NON-NLS-1$
		sb.append("\n\t" + "path= ").append(this.path); //$NON-NLS-1$ //$NON-NLS-2$
		sb.append("\n\t" + "name= ").append(this.name); //$NON-NLS-1$ //$NON-NLS-2$
		sb.append("\n\t" + "timestamp= ").append(this.timestamp); //$NON-NLS-1$ //$NON-NLS-2$
		return sb.toString();
	}
	
}
