/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public abstract class MainCmdItem implements RJIOExternalizable {
	
	
	public static final byte T_NONE= 0;
	
	/**
	 * {@link ConsoleReadCmdItem}
	 */
	public static final byte T_CONSOLE_READ_ITEM=           0x01;
	
	/**
	 * {@link ConsoleWriteCmdItem}
	 */
	public static final byte T_CONSOLE_WRITE_ITEM=          0x02;
	
	
	/**
	 * {@link ConsoleMessageCmdItem}
	 */
	public static final byte T_MESSAGE_ITEM=                0x04;
	
	/**
	 * {@link ExtClientCmdItem}
	 */
	public static final byte T_EXT_CLIENT_ITEM=             0x05;
	
	/**
	 * {@link GDCmdItem}
	 */
	public static final byte T_GRAPH_ITEM=                  0x07;
	
	/**
	 * T_id <  => initiated by server
	 * T_id >  => initiated by client
	 */
	public static final byte T_S2C_C2S= 9;
	
	/**
	 * {@link MainCtrlCmdItem}
	 */
	public static final byte T_MAIN_CTRL_ITEM=              0x10;
	
	/**
	 * {@link DataCmdItem}
	 */
	public static final byte T_DATA_ITEM=                   0x11;
	
	/**
	 * {@link GraOpCmdItem}
	 */
	public static final byte T_GRAPHICS_OP_ITEM=            0x12;
	
	/**
	 * {@link DbgCmdItem}
	 */
	public static final byte T_DBG_ITEM=                    0x14;
	
	
	/**
	 * Inside server only
	 */
	public static final byte T_SRV_ITEM=                    0x20;
	
	
	protected static final int OM_STATUS=                  0x00f00000; // 0xf << OS_STATUS
	protected static final int OS_STATUS=                  20;
	
	protected static final int OM_WITH=                    0x0f000000;
	
	public static final int OV_ANSWER=                     0x40000000;
	protected static final int OM_ANSWER=                  OV_ANSWER;
	public static final int OV_WAITFORCLIENT=              0x80000000;
	protected static final int OM_WAITFORCLIENT=           OV_WAITFORCLIENT;
	protected static final int OC_WAITFORCLIENT=           ~(OM_WAITFORCLIENT);
	
	public static final int OM_CUSTOM=                     0x0000ffff;
	
	protected static final int OM_CLEARFORANSWER=          ~(OM_STATUS | OM_WITH);
	
	
	protected int options;
	
	public @Nullable MainCmdItem next;
	
	public int requestId;
	public byte slot;
	
	
	public abstract byte getCmdType();
	
	public abstract byte getOp();
	
	public final boolean waitForClient() {
		return ((this.options & OM_WAITFORCLIENT) != 0);
	}
	
	public final boolean isAnswer() {
		return ((this.options & OM_ANSWER) != 0);
	}
	
	public final int getCmdOption() {
		return ((this.options & OM_CUSTOM));
	}
	
	public abstract void setAnswer(RjsStatus status);
	
	public abstract boolean isOK();
	public abstract @Nullable RjsStatus getStatus();
	public abstract @Nullable String getDataText();
	public byte @Nullable [] getDataTextUtf8() {
		final String text= getDataText();
		return (text != null) ? text.getBytes(StandardCharsets.UTF_8) : null;
	}
	
	@Override
	public abstract void writeExternal(RJIO io) throws IOException;
	
	public abstract boolean testEquals(MainCmdItem other);
	
}
