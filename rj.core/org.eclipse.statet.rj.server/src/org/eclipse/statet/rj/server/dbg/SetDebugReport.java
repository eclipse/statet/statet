/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class SetDebugReport implements RJIOExternalizable {
	
	
	private static final int CHANGED=                      0x000000001;
	
	
	private final int resultCode;
	
	
	public SetDebugReport(final boolean changed) {
		this.resultCode= (changed) ? CHANGED : 0;
	}
	
	public SetDebugReport(final RJIO io) throws IOException {
		this.resultCode= io.readInt();
	}
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeInt(this.resultCode);
	}
	
	
	public boolean isChanged() {
		return ((this.resultCode & CHANGED) != 0);
	}
	
}
