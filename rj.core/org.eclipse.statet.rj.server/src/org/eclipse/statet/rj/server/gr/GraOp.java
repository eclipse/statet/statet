/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.gr;


/**
 * Graphic operations
 */
public interface GraOp {
	
	
	// -- C2S sync
	byte OP_CLOSE=                     0x01;
	
	byte OP_REQUEST_RESIZE=            0x08;
	
	byte OP_CONVERT_DEV2USER=          0x10;
	byte OP_CONVERT_USER2DEV=          0x11;
	
}
