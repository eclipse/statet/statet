/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.io.Serializable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ServerInfo implements Serializable {
	
	
	private static final long serialVersionUID= -5411479269748201535L;
	
	public static final String USER_OWNER= "owner";
	public static final String USER_CONSOLE= "console";
	public static final String USER_RSERVI= "rservi";
	
	
	private final String name;
	private final @NonNull String[] userTypes;
	private final @NonNull String[] userNames;
	private final String directory;
	private final long timestamp;
	private final int state;
	
	
	public ServerInfo(final String name, final String directory, final long timestamp,
			final @NonNull String[] userTypes, final @NonNull String[] userNames,
			final int state) {
		this.name= name;
		this.userTypes= userTypes;
		this.userNames= userNames;
		this.directory= directory;
		this.timestamp= timestamp;
		this.state= state;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	public @Nullable String getUsername(final String type) {
		for (int i= 0; i < this.userTypes.length; i++) {
			if (this.userTypes[i].equals(type)) {
				return this.userNames[i];
			}
		}
		return null;
	}
	
	public String getDirectory() {
		return this.directory;
	}
	
	public long getTimestamp() {
		return this.timestamp;
	}
	
	public int getState() {
		return this.state;
	}
	
}
