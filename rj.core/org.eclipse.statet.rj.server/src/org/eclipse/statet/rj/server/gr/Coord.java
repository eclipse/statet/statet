/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.gr;

import java.io.IOException;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


/**
 * Single R graphic coordinate
 */
public class Coord implements RJIOExternalizable {
	
	
	private double x;
	private double y;
	
	
	public Coord(final double x, final double y) {
		this.x= x;
		this.y= y;
	}
	
	public Coord(final RJIO in) throws IOException {
		this.x= in.readDouble();
		this.y= in.readDouble();
	}
	
	
	@Override
	public void writeExternal(final RJIO out) throws IOException {
		out.writeDouble(this.x);
		out.writeDouble(this.y);
	}
	
	
	public void setX(final double x) {
		this.x= x;
	}
	
	public void setY(final double y) {
		this.y= y;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
}
