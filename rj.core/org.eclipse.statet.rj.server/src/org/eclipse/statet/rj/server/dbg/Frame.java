/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class Frame {
	
	
	private final int position;
	private final @Nullable String call;
	
	protected long handle;
	
	protected @Nullable String fileName;
	protected long fileTimestamp;
	
	protected int @Nullable [] exprSrcref;
	
	protected int flags;
	
	
	public Frame(final int position, final @Nullable String call, final long handle,
			final @Nullable String fileName, final long fileTimestamp,
			final int @Nullable [] exprSrcref) {
		this.position= position;
		this.call= call;
		this.handle= handle;
		this.fileName= fileName;
		this.fileTimestamp= fileTimestamp;
		this.exprSrcref= exprSrcref;
	}
	
	protected Frame(final int position, final @Nullable String call) {
		this.position= position;
		this.call= call;
	}
	
	
	public int getPosition() {
		return this.position;
	}
	
	public @Nullable String getCall() {
		return this.call;
	}
	
	public long getHandle() {
		return this.handle;
	}
	
	public @Nullable String getFileName() {
		return this.fileName;
	}
	
	public long getFileTimestamp() {
		return this.fileTimestamp;
	}
	
	public int @Nullable [] getExprSrcref() {
		return this.exprSrcref;
	}
	
	
	public int getFlags() {
		return this.flags;
	}
	
	public void addFlags(final int flags) {
		this.flags |= flags;
	}
	
	/** top frame */
	public boolean isTopFrame() {
		return ((this.flags & CallStack.FLAG_TOPFRAME) != 0);
	}
	
	/** frame of top level command */
	public boolean isTopLevelCommand() {
		return (this.position == 3 && (this.flags & 0xff) == (CallStack.FLAG_COMMAND | 2));
	}
	
}
