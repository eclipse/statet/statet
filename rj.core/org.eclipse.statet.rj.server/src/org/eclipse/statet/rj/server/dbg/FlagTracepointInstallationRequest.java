/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;


@NonNullByDefault
public class FlagTracepointInstallationRequest extends TracepointInstallationRequest {
	
	
	private final byte[] types;
	private final int[] flags;
	
	
	public FlagTracepointInstallationRequest(final byte[] types, final int[] flags) {
		if (types.length != flags.length) {
			throw new IllegalArgumentException("types.length != flags.length");
		}
		this.types= types;
		this.flags= flags;
	}
	
	public FlagTracepointInstallationRequest(final RJIO in) throws IOException {
		final int l= in.readInt();
		this.types= in.readByteData(new byte[l], l);
		this.flags= in.readIntData(new int[l], l);
	}
	
	@Override
	public void writeExternal(final RJIO out) throws IOException {
		final int l= this.types.length;
		out.writeInt(l);
		out.writeByteData(this.types, l);
		out.writeIntData(this.flags, l);
	}
	
	
	public byte[] getTypes() {
		return this.types;
	}
	
	public int[] getFlags() {
		return this.flags;
	}
	
}
