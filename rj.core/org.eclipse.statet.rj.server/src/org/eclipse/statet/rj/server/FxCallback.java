/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.io.Serializable;

import javax.security.auth.callback.Callback;


/**
 * Callback for authentication using file permission
 */
public class FxCallback implements Callback, Serializable {
	
	
	private static final long serialVersionUID= 3559656299612581181L;
	
	
	private final String filename;
	private byte[] content;
	
	
	public FxCallback(final String filename, final byte[] pendingKey) {
		this.filename= filename;
		this.content= pendingKey;
	}
	
	
	public String getFilename() {
		return this.filename;
	}
	
	public byte[] createContent(final byte[] clientKey) {
		if (clientKey == null) {
			throw new NullPointerException();
		}
		final byte[] newContent= new byte[this.content.length+clientKey.length];
		System.arraycopy(this.content, 0, newContent, 0, this.content.length);
		System.arraycopy(clientKey, 0, newContent, this.content.length, clientKey.length);
		this.content= clientKey;
		return newContent;
	}
	
	public byte[] getContent() {
		return this.content;
	}
	
}
