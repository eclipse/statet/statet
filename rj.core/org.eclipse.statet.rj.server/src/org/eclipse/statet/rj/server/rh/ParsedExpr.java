/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.rh;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.server.dbg.TracepointState;


@NonNullByDefault
public class ParsedExpr implements RhDisposable {
	
	
	private static final int FLAG_DISPOSED= 1 << 0;
	
	
	private final String source;
	
	private int flags;
	
	private @Nullable Handle parsedExpr;
	
	
	public ParsedExpr(final String source) {
		this.source= source;
	}
	
	
	public final String getSource() {
		return this.source;
	}
	
	public final int getFlags() {
		return (this.flags & 0xFFFFFFF);
	}
	
	
	public @Nullable Handle getParsedExpr() {
		return this.parsedExpr;
	}
	
	public synchronized void setParsedExpr(final @Nullable Handle parsed, final RhEngine engine) {
		if ((this.flags & FLAG_DISPOSED) != 0) {
			return;
		}
		
		if (parsed == null) {
			this.flags|= TracepointState.FLAG_EXPR_INVALID;
		}
		else {
			engine.preserve(parsed);
		}
	}
	
	@Override
	public void dispose(final RhEngine engine) {
		synchronized (this) {
			this.flags|= FLAG_DISPOSED;
		}
		if ((this.flags) != 0 && this.parsedExpr != null) {
			engine.releasePreserved(this.parsedExpr);
			this.parsedExpr= null;
		}
	}
	
}
