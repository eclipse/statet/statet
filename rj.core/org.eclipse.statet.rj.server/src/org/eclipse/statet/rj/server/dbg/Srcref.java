/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class Srcref {
	
	
	public static final int BEGIN_LINE= 0;
	public static final int BEGIN_COLUMN= 4;
	public static final int BEGIN_BYTE= 1;
	
	public static final int END_LINE= 2;
	public static final int END_COLUMN= 5;
	public static final int END_BYTE= 3;
	
	public static final int NA= Integer.MIN_VALUE;
	
	
	public static final int[] copy(final int[] srcref) {
		final int[] clone= new int[6];
		System.arraycopy(srcref, 0, clone, 0, 6);
		return clone;
	}
	
	public static final int addLine(final int main, final int ref) {
		if (main > 0 && ref > 0) {
			return main + ref - 1;
		}
		else {
			return NA;
		}
	}
	
	public static final int @Nullable [] substract(final int[] main, final int[] ref) {
		final int[] result= new int[6];
		
		if (main[BEGIN_LINE] > 0 && ref[BEGIN_LINE] > 0) {
			result[BEGIN_LINE]= 1 + main[BEGIN_LINE] - ref[BEGIN_LINE];
			if (result[BEGIN_LINE] <= 0) {
				return null;
			}
		}
		else {
			result[BEGIN_LINE]= NA;
		}
		if (result[BEGIN_LINE] == 1) {
			if (main[BEGIN_COLUMN] > 0 && ref[BEGIN_COLUMN] > 0) {
				result[BEGIN_COLUMN]= 1 + main[BEGIN_COLUMN] - ref[BEGIN_COLUMN];
				if (result[BEGIN_COLUMN] <= 0) {
					return null;
				}
			}
			else {
				result[BEGIN_COLUMN]= NA;
			}
			if (main[BEGIN_BYTE] > 0 && ref[BEGIN_BYTE] > 0) {
				result[BEGIN_BYTE]= 1 + main[BEGIN_BYTE] - ref[BEGIN_BYTE];
				if (result[BEGIN_BYTE] <= 0) {
					result[BEGIN_BYTE]= NA;
				}
			}
			else {
				result[BEGIN_BYTE]= NA;
			}
		}
		else {
			result[BEGIN_COLUMN]= main[BEGIN_COLUMN];
			result[BEGIN_BYTE]= NA;
		}
		
		if (main[END_LINE] > 0 && ref[BEGIN_LINE] > 0) {
			result[END_LINE]= 1 + main[END_LINE] - ref[BEGIN_LINE];
			if (result[END_LINE] <= 0) {
				return null;
			}
		}
		else {
			result[END_LINE]= NA;
		}
		if (result[END_LINE] == 1) {
			if (main[END_COLUMN] > 0 && ref[BEGIN_COLUMN] > 0) {
				result[END_COLUMN]= 1 + main[END_COLUMN] - ref[BEGIN_COLUMN];
				if (result[END_COLUMN] <= 0) {
					return null;
				}
			}
			else {
				result[END_COLUMN]= NA;
			}
			if (main[END_BYTE] > 0 && ref[BEGIN_BYTE] > 0) {
				result[END_BYTE]= 1 + main[END_BYTE] - ref[BEGIN_BYTE];
				if (result[END_BYTE] <= 0) {
					result[END_BYTE]= NA;
				}
			}
			else {
				result[END_BYTE]= NA;
			}
		}
		else {
			result[END_COLUMN]= main[END_COLUMN];
			result[END_BYTE]= NA;
		}
		
		return result;
	}
	
	public static final int[] add(final int[] main, final int[] ref) {
		final int[] result= new int[6];
		
		result[BEGIN_LINE]= addLine(main[BEGIN_LINE], ref[BEGIN_LINE]);
		if (main[BEGIN_LINE] == 1) {
			if (main[BEGIN_COLUMN] > 0 && ref[BEGIN_COLUMN] > 0) {
				result[BEGIN_COLUMN]= main[BEGIN_COLUMN] + ref[BEGIN_COLUMN] - 1;
			}
			else {
				result[BEGIN_COLUMN]= NA;
			}
			if (main[BEGIN_BYTE] > 0 && ref[BEGIN_BYTE] > 0) {
				result[BEGIN_BYTE]= main[BEGIN_BYTE] + ref[BEGIN_BYTE] - 1;
			}
			else {
				result[BEGIN_BYTE]= NA;
			}
		}
		else {
			result[BEGIN_COLUMN]= main[BEGIN_COLUMN];
			result[BEGIN_BYTE]= main[BEGIN_BYTE];
		}
		
		result[END_LINE]= addLine(main[END_LINE], ref[BEGIN_LINE]);
		if (main[END_LINE] == 1) {
			if (main[BEGIN_COLUMN] > 0 && ref[END_COLUMN] > 0) {
				result[END_COLUMN]= main[BEGIN_COLUMN] + ref[END_COLUMN] - 1;
			}
			else {
				result[END_COLUMN]= NA;
			}
			if (main[BEGIN_BYTE] > 0 && ref[END_BYTE] > 0) {
				result[END_BYTE]= main[BEGIN_BYTE] + ref[END_BYTE] - 1;
			}
			else {
				result[END_BYTE]= NA;
			}
		}
		else {
			result[END_COLUMN]= main[END_COLUMN];
			result[END_BYTE]= main[END_BYTE];
		}
		
		return result;
	}
	
	
	public static final int[] merge(final int[] main, final int[] last) {
		final int[] result= copy(main);
		if (last != main && last[END_LINE] > 0) {
			if (last[END_LINE] == result[END_LINE]) {
				if (last[END_COLUMN] > 0 && last[END_COLUMN] > result[END_COLUMN]) {
					result[END_COLUMN]= last[END_COLUMN];
					result[END_BYTE]= last[END_BYTE];
				}
			}
			else if (last[END_LINE] > result[END_LINE]) {
				result[END_LINE]= last[END_LINE];
				result[END_COLUMN]= last[END_COLUMN];
				result[END_BYTE]= last[END_BYTE];
			}
		}
		return result;
	}
	
	public static final boolean equalsStart(final int[] srcref1, final int[] srcref2) {
		return (srcref1[BEGIN_LINE] == srcref2[BEGIN_LINE]
				&& srcref1[BEGIN_COLUMN] == srcref2[BEGIN_COLUMN]);
	}
	
	public static final boolean contains(final int[] srcref1, final int[] srcref2) {
		return (((srcref1[BEGIN_LINE] == srcref2[BEGIN_LINE]) ?
						(srcref1[BEGIN_COLUMN] <= srcref2[BEGIN_COLUMN]) :
						(srcref1[BEGIN_LINE] < srcref2[BEGIN_LINE]) )
				&& ((srcref1[END_LINE] == srcref2[END_LINE]) ?
						(srcref1[END_COLUMN] >= srcref2[END_COLUMN]) :
						(srcref1[END_LINE] > srcref2[END_LINE]) ) );
	}
	
	public static final boolean equalsDim(final int[] srcref1, final int[] srcref2) {
		return (srcref1[END_LINE] - srcref1[BEGIN_LINE] == srcref2[Srcref.END_LINE] - srcref2[BEGIN_LINE]
				&& srcref1[END_COLUMN] - srcref1[BEGIN_COLUMN] == srcref2[Srcref.END_COLUMN] - srcref2[BEGIN_COLUMN]);
	}
	
}
