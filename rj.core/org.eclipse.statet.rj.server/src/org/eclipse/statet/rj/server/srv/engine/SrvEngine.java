/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.srv.engine;

import java.rmi.RemoteException;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.server.RjsComObject;
import org.eclipse.statet.rj.server.Server;
import org.eclipse.statet.rj.server.srv.RMIServerControl;
import org.eclipse.statet.rj.server.srvext.Client;


@NonNullByDefault
public interface SrvEngine {
	
	
	void init(RMIServerControl control, Server publicServer,
			Map<String, ? extends @NonNull Object> more)
			throws Exception;
	
	int[] getVersion();
	
	int getState();
	Client getCurrentClient();
	Map<String, Object> getPlatformData();
	
	Object start(Client client, Map<String, ? extends @Nullable Object> properties)
			throws RemoteException;
	Object connect(Client client, Map<String, ? extends @Nullable Object> properties)
			throws RemoteException;
	void disconnect(Client client)
			throws RemoteException;
	void setProperties(Client client, Map<String, ? extends @Nullable Object> properties)
			throws RemoteException;
	
	RjsComObject runMainLoop(Client client, RjsComObject com)
			throws RemoteException;
	RjsComObject runAsync(Client client, RjsComObject com)
			throws RemoteException;
	
}
