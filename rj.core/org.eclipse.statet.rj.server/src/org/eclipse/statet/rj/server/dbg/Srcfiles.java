/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class Srcfiles {
	
	
	public static boolean equalsTimestamp(final long suTimestamp, final long rTimestamp) {
		final long diff;
		return (suTimestamp == rTimestamp
				|| (diff= Math.abs(suTimestamp - rTimestamp) + 500 / 1000) == 0
				|| diff == 3600 );
	}
	
	
	private Srcfiles() {}
	
}
