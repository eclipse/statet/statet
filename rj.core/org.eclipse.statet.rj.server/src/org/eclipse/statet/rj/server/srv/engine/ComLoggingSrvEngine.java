/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.srv.engine;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.rj.server.RjsComObject;
import org.eclipse.statet.rj.server.Server;
import org.eclipse.statet.rj.server.srv.RMIServerControl;
import org.eclipse.statet.rj.server.srvext.Client;


@NonNullByDefault
public class ComLoggingSrvEngine implements SrvEngine {
	
	
	private static class ComLogRecord extends LogRecord {
		
		private static final long serialVersionUID= 1L;
		
		private final transient Client client;
		private final long callId;
		
		public ComLogRecord(final Level level, final String methodName, final Client client) {
			super(level, null);
			this.client= client;
			this.callId= getSequenceNumber();
			
			setSourceClassName("SrvEngine");
			setSourceMethodName(methodName);
		}
		
		public ComLogRecord(final Level level, final ComLogRecord ref) {
			super(level, null);
			this.client= ref.client;
			this.callId= ref.callId;
			
			setSourceClassName("SrvEngine");
			setSourceMethodName(ref.getSourceMethodName());
		}
		
		
		public ToStringBuilder entering(final @Nullable String detail) {
			final ToStringBuilder sb= new ToStringBuilder("ENTRY #");
			sb.append(this.callId);
			if (detail != null) {
				sb.append(' ', detail);
			}
			sb.addProp("client", this.client);
			return sb;
		}
		
		public ToStringBuilder exiting(final @Nullable String detail) {
			final ToStringBuilder sb= new ToStringBuilder("EXIT #");
			sb.append(this.callId);
			if (detail != null) {
				sb.append(' ', detail);
			}
			sb.addProp("client", this.client);
			return sb;
		}
		
	}
	
	
	private final SrvEngine engine;
	
	private final Logger logger;
	
	
	public ComLoggingSrvEngine(final SrvEngine engine, final Logger logger) {
		this.engine= nonNullAssert(engine);
		this.logger= nonNullAssert(logger);
	}
	
	
	@Override
	public void init(final RMIServerControl control, final Server publicServer,
			final Map<String, ? extends @NonNull Object> more) throws Exception {
		this.engine.init(control, publicServer, more);
	}
	
	@Override
	public int[] getVersion() {
		return this.engine.getVersion();
	}
	
	@Override
	public int getState() {
		return this.engine.getState();
	}
	
	@Override
	public Client getCurrentClient() {
		return this.engine.getCurrentClient();
	}
	
	@Override
	public Map<String, Object> getPlatformData() {
		return this.engine.getPlatformData();
	}
	
	
	@Override
	public Object start(final Client client, final Map<String, ? extends @NonNull Object> properties)
			throws RemoteException {
		final ComLogRecord ref= entering("start", client, properties);
		try {
			final var result= this.engine.start(client, properties);
			exiting(ref, result);
			return result;
		}
		catch (final RemoteException | RuntimeException e) {
			exiting(ref, e);
			throw e;
		}
	}
	
	@Override
	public Object connect(final Client client, final Map<String, ? extends @NonNull Object> properties)
			throws RemoteException {
		final ComLogRecord ref= entering("connect", client, properties);
		try {
			final var result= this.engine.connect(client, properties);
			exiting(ref, result);
			return result;
		}
		catch (final RemoteException | RuntimeException e) {
			exiting(ref, e);
			throw e;
		}
	}
	
	@Override
	public void disconnect(final Client client)
			throws RemoteException {
		final ComLogRecord ref= entering("disconnect", client);
		try {
			this.engine.disconnect(client);
			exiting(ref);
		}
		catch (final RemoteException | RuntimeException e) {
			exiting(ref, e);
			throw e;
		}
	}
	
	@Override
	public void setProperties(final Client client, final Map<String, ? extends @NonNull Object> properties)
			throws RemoteException {
		final ComLogRecord ref= entering("setProperties", client, properties);
		try {
			this.engine.setProperties(client, properties);
			exiting(ref);
		}
		catch (final RemoteException | RuntimeException e) {
			exiting(ref, e);
			throw e;
		}
	}
	
	
	@Override
	public RjsComObject runMainLoop(final Client client, final RjsComObject com)
			throws RemoteException {
		final ComLogRecord ref= entering("runMainLoop", client, com);
		try {
			final var result= this.engine.runMainLoop(client, com);
			exiting(ref, result);
			return result;
		}
		catch (final RemoteException | RuntimeException e) {
			exiting(ref, e);
			throw e;
		}
	}
	
	@Override
	public RjsComObject runAsync(final Client client, final RjsComObject com)
			throws RemoteException {
		final ComLogRecord ref= entering("runAsync", client, com);
		try {
			final var result= this.engine.runAsync(client, com);
			exiting(ref, result);
			return result;
		}
		catch (final RemoteException | RuntimeException e) {
			exiting(ref, e);
			throw e;
		}
	}
	
	
	private ComLogRecord entering(final String methodName, final Client client,
			final Object param0) {
		final var record= new ComLogRecord(Level.FINER, methodName, client);
		final ToStringBuilder sb= record.entering(null);
		sb.addProp(">>", param0);
		record.setMessage(sb.toString());
		this.logger.log(record);
		return record;
	}
	
	private ComLogRecord entering(final String methodName, final Client client) {
		final var record= new ComLogRecord(Level.FINER, methodName, client);
		final ToStringBuilder sb= record.entering(null);
		record.setMessage(sb.toString());
		this.logger.log(record);
		return record;
	}
	
	private void exiting(final ComLogRecord ref, final Object result) {
		final var record= new ComLogRecord(Level.FINER, ref);
		final ToStringBuilder sb= record.exiting(null);
		sb.addProp("<<", result);
		record.setMessage(sb.toString());
		this.logger.log(record);
	}
	
	private void exiting(final ComLogRecord ref) {
		final var record= new ComLogRecord(Level.FINER, ref);
		final ToStringBuilder sb= record.exiting(null);
		record.setMessage(sb.toString());
		this.logger.log(record);
	}
	
	private void exiting(final ComLogRecord ref, final Throwable e) {
		final var record= new ComLogRecord(Level.FINER, ref);
		final ToStringBuilder sb= record.exiting("with exception");
		record.setMessage(sb.toString());
		record.setThrown(e);
		this.logger.log(record);
	}
	
}
