/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;


@NonNullByDefault
public class ElementTracepointInstallationRequest extends TracepointInstallationRequest {
	
	
	private final static int RESET=                         0x00000001;
	
	
	private final List<? extends ElementTracepoints> requests;
	
	private int properties;
	
	
	public ElementTracepointInstallationRequest(
			final List<? extends ElementTracepoints> checkedList,
			final boolean reset) {
		this.requests= checkedList;
		this.properties= (reset) ? RESET : 0;
	}
	
	public ElementTracepointInstallationRequest(final RJIO io) throws IOException {
		final int l= io.readInt();
		final List<ElementTracepoints> list= new ArrayList<>(l);
		for (int i= 0; i < l; i++) {
			list.add(new ElementTracepoints(io));
		}
		this.requests= list;
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		final int l= this.requests.size();
		io.writeInt(l);
		for (int i= 0; i < l; i++) {
			this.requests.get(i).writeExternal(io);
		}
	}
	
	
	public boolean getReset() {
		return ((this.properties & RESET) != 0);
	}
	
	public List<? extends ElementTracepoints> getRequests() {
		return this.requests;
	}
	
}
