/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.rh;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class RhWeakRef extends RhRef implements RhDisposable {
	
	
	private volatile boolean isValid= true;
	
	private @Nullable RhRefListener listener;
	
	
	public RhWeakRef(final Handle handle, final @Nullable RhRefListener listener) {
		super(handle);
		this.listener= listener;
	}
	
	
	public boolean isValid() {
		return this.isValid;
	}
	
	
	protected void onFinalized() {
		this.isValid= false;
		
		if (this.listener != null) {
			this.listener.onFinalized(this);
			this.listener= null;
		}
	}
	
	
	@Override
	public void dispose(final RhEngine engine) {
		this.isValid= false;
	}
	
}
