/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class TracepointEvent implements Tracepoint, RJIOExternalizable {
	
	
	public static final byte KIND_ABOUT_TO_HIT=            0x01;
	
	public static final byte KIND_INSTALLED=               0x10;
	public static final byte KIND_UNINSTALLED=             0x20;
	
	
	private final byte kind;
	
	private final int type;
	
	private final @Nullable String filePath;
	private final long id;
	private final String elementId;
	
	private final @Nullable String label;
	private final int flags;
	
	
	public TracepointEvent(final byte kind, final int type,
			final @Nullable String filePath, final long id,
			final String elementId, final @Nullable String label, final int flags) {
		this.kind= kind;
		this.type= type;
		this.filePath= filePath;
		this.id= id;
		this.elementId= elementId;
		this.label= label;
		this.flags= flags;
	}
	
	public TracepointEvent(final RJIO io) throws IOException {
		this.kind= io.readByte();
		this.type= io.readInt();
		this.filePath= io.readString();
		this.id= io.readLong();
		this.elementId= io.readString();
		this.label= io.readString();
		this.flags= io.readInt();
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeByte(this.kind);
		io.writeInt(this.type);
		io.writeString(this.filePath);
		io.writeLong(this.id);
		io.writeString(this.elementId);
		io.writeString(this.label);
		io.writeInt(this.flags);
	}
	
	
	public byte getKind() {
		return this.kind;
	}
	
	@Override
	public int getType() {
		return this.type;
	}
	
	public @Nullable String getFilePath() {
		return this.filePath;
	}
	
	@Override
	public long getId() {
		return this.id;
	}
	
	public String getElementId() {
		return this.elementId;
	}
	
	public @Nullable String getLabel() {
		return this.label;
	}
	
	public int getFlags() {
		return this.flags;
	}
	
}
