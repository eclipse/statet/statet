/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;


/**
 * Command for main loop console prompt/input.
 */
@NonNullByDefault
public final class MainCtrlCmdItem extends MainCmdItem {
	
	
	public static final byte OP_FINISH_TASK= 02;
	
	
	private final byte op;
	
	
	public MainCtrlCmdItem(final byte op, final int options) {
		this.op= op;
		this.options= (options | (OM_WAITFORCLIENT));
	}
	
	/**
	 * Constructor for deserialization
	 */
	public MainCtrlCmdItem(final RJIO in) throws IOException {
		this.requestId= in.readInt();
		this.options= in.readInt();
		this.op= in.readByte();
	}
	
	@Override
	public void writeExternal(final RJIO out) throws IOException {
		out.writeInt(this.requestId);
		out.writeInt(this.options);
		out.writeByte(this.op);
	}
	
	
	@Override
	public byte getCmdType() {
		return T_MAIN_CTRL_ITEM;
	}
	
	@Override
	public byte getOp() {
		return this.op;
	}
	
	
	@Override
	public void setAnswer(final RjsStatus status) {
		this.options= (this.options & OM_CLEARFORANSWER) | (status.getSeverity() << OS_STATUS);
	}
	
	
	@Override
	public boolean isOK() {
		return ((this.options & OM_STATUS) == RjsStatus.OK);
	}
	
	@Override
	public @Nullable RjsStatus getStatus() {
		return null;
	}
	
	@Override
	public @Nullable String getDataText() {
		return null;
	}
	
	@Override
	public byte @Nullable [] getDataTextUtf8() {
		return null;
	}
	
	
	@Override
	public boolean testEquals(final MainCmdItem other) {
		return (other instanceof final MainCtrlCmdItem otherItem
				&& getOp() == otherItem.getOp()
				&& this.options == otherItem.options );
	}
	
	
	@Override
	public String toString() {
		final StringBuffer sb= new StringBuffer(128);
		sb.append("MainCtrlCmdItem ");
		switch (this.op) {
		case OP_FINISH_TASK:
			sb.append("FINISH_TASK");
			break;
		default:
			sb.append(this.op);
			break;
		}
		sb.append("\n\t").append("options= 0x").append(Integer.toHexString(this.options));
		return sb.toString();
	}
	
}
