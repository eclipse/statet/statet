/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


@NonNullByDefault
public class CtrlReport implements RJIOExternalizable {
	
	
	private final static int REQUEST_EXECUTED=              0x01000000;
	
	private final static int ENGINE_EXECUTING=              0;
	private final static int ENGINE_SUSPENDED=              0x02000000;
	
	private final static int RESET_PROMPT=                  0x00100000;
	
	
	public static CtrlReport createRequestExecuted(final byte type) {
		switch (type) {
		case DbgRequest.RESUME:
		case DbgRequest.STEP_INTO:
		case DbgRequest.STEP_OVER:
		case DbgRequest.STEP_RETURN:
			return new CtrlReport(type | REQUEST_EXECUTED | ENGINE_EXECUTING);
		default:
			throw new IllegalArgumentException("type= " + type); //$NON-NLS-1$
		}
	}
	
	public static CtrlReport createRequestNotApplicable(final boolean isEngineSuspended) {
		return new CtrlReport((isEngineSuspended) ? ENGINE_SUSPENDED : ENGINE_EXECUTING);
	}
	
	public static CtrlReport createRequestNotSupported(final boolean isEngineSuspended) {
		return new CtrlReport((isEngineSuspended) ? ENGINE_SUSPENDED : ENGINE_EXECUTING);
	}
	
	
	private final int code;
	
	
	private CtrlReport(final int code) {
		this.code= code;
	}
	
	public CtrlReport(final RJIO io) throws IOException {
		this.code= io.readInt();
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeInt(this.code);
	}
	
	
	public byte getOp() {
		return (byte) (this.code & 0xff);
	}
	
	public boolean isRequestExecuted() {
		return ((this.code & REQUEST_EXECUTED) != 0);
	}
	
	public boolean isEngineSuspended() {
		return ((this.code & ENGINE_SUSPENDED) != 0);
	}
	
}
