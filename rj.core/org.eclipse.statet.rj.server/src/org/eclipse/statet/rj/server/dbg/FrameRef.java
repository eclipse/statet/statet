/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dbg;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RJIOExternalizable;


/**
 * References a frame of the callstack.
 */
@NonNullByDefault
public abstract class FrameRef implements RJIOExternalizable {
	
	
	public static final class ByPosition extends FrameRef {
		
		
		private final int position;
		
		
		public ByPosition(final int position) {
			this.position= position;
		}
		
		public ByPosition(final RJIO in) throws IOException {
			this.position= in.readInt();
		}
		
		@Override
		public void writeExternal(final RJIO out) throws IOException {
			out.writeInt(this.position);
		}
		
		
		public int getPosition() {
			return this.position;
		}
		
	}
	
	public static final class ByHandle extends FrameRef {
		
		
		private final long handle;
		
		
		public ByHandle(final long handle) {
			this.handle= handle;
		}
		
		public ByHandle(final RJIO in) throws IOException {
			this.handle= in.readLong();
		}
		
		@Override
		public void writeExternal(final RJIO out) throws IOException {
			out.writeLong(this.handle);
		}
		
		
		public long getHandle() {
			return this.handle;
		}
	}
	
}
