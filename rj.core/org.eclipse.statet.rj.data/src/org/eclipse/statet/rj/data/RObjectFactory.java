/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RObjectFactory {
	
	/** Flag to fetch only the structure but not the data (store) of the objects */
	int F_ONLY_STRUCT=                  1 << 0;
	
	/** XXX: Not yet implemented */
	int F_WITH_ATTR=                    1 << 1;
	
	/** Flag to fetch additional dbg detail */
	int F_WITH_DBG=                     1 << 2;
	
	/** Flag to load environments directly instead of the reference only */
	int F_LOAD_ENVIRONMENT=             1 << 4;
	
	/** Flag to eval all promises directly */
	int F_LOAD_PROMISE=                 1 << 5;
	
	
	int O_LENGTHGRADE_MASK= 7; // 3 bits
	
	int O_WITH_ATTR= 1 << 3;
	
	int O_CLASS_NAME= 1 << 4;
	
	int O_NO_CHILDREN= 1 << 5;
	
	int O_WITH_NAMES= 1 << 6;
	
	
//	RArgument createArgument(String name, String defaultSource);
	
//	RFunction createFunction(RArgument[] argument);
	
	/**
	 * Creates an R vector with the given R data store and R class name.
	 * 
	 * @param data the data store
	 * @param className the R class name
	 * @return the R vector
	 * 
	 * @since 4.8
	 */
	public <TData extends RStore<?>> RVector<TData> createVector(TData data, String className);
	
	/**
	 * Creates an R vector with the given R data store.
	 * 
	 * <p>The vector has the default R class name for data type of the given store.</p>
	 * 
	 * @param data the data store
	 * @return the R vector
	 */
	public default <TData extends RStore<?>> RVector<TData> createVector(final TData data) {
		return createVector(data, data.getBaseVectorRClassName());
	}
	
	
	/**
	 * Creates an R array with the given R data store, dimension and R class name.
	 * 
	 * @param data the data store
	 * @param dim the dimension of the array
	 * @param className the R class name
	 * @return the R array
	 * 
	 * @since 4.8
	 */
	public <TData extends RStore<?>> RArray<TData> createArray(TData data,
			int[] dim, String className);
	
	public default <TData extends RStore<?>> RArray<TData> createArray(final TData data,
			final int[] dim) {
		return createArray(data,
				dim, (dim.length == 2) ? RObject.CLASSNAME_MATRIX : RObject.CLASSNAME_ARRAY );
	}
	
	/**
	 * Creates an R matrix with the given R data store and dimension.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * @param data the data store
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R matrix
	 * 
	 * @since 4.8
	 */
	public default <TData extends RStore<?>> RArray<TData> createMatrix(final TData data,
			final int rowCount, final int colCount) {
		return createArray(data,
				new int[] { rowCount, colCount }, RObject.CLASSNAME_MATRIX );
	}
	
	
	/**
	 * Creates an R list (generic vector).
	 * 
	 * <p>Note that the R object / its data stores may use the arrays directly.</p>
	 * 
	 * @param components the list components
	 * @param names the names of the list components, optional: Java strings or {@code null} for NA
	 * @param className the R class name
	 * @return the R list
	 * 
	 * @since 4.8
	 */
	public RList createList(RObject [] components,
			@Nullable String @Nullable [] names,
			String className);
	
	/**
	 * Creates an R list (generic vector).
	 * 
	 * <p>The list has the default R class name 'list'.</p>
	 * 
	 * <p>Note that the R object / its data stores may use the arrays directly.</p>
	 * 
	 * @param components the list components
	 * @param names the names of the list components, optional: Java strings or {@code null} for NA
	 * @return the R list
	 */
	public default RList createList(final RObject [] components,
			final @Nullable String @Nullable [] names) {
		return createList(components, names, RObject.CLASSNAME_LIST);
	}
	
	
	/**
	 * Creates an R data frame.
	 * 
	 * <p>Note that the R object / its data stores may use the arrays directly.</p>
	 * 
	 * @return the R data frame
	 * 
	 * @since 4.8
	 */
	public RDataFrame createDataFrame(@NonNull RStore<?> [] colDatas,
			@NonNull String [] colNames,
			@NonNull String @Nullable [] rowNames,
			String className);
	
	/**
	 * Creates an R data frame.
	 * 
	 * <p>The data frame has the default R class name 'data.frame'.</p>
	 * 
	 * <p>Note that the R object / its data stores may use the arrays directly.</p>
	 * 
	 * @return the R data frame
	 * 
	 * @since 4.8
	 */
	public default RDataFrame createDataFrame(@NonNull final RStore<?> [] colDatas,
			final @NonNull String [] colNames,
			final @NonNull String @Nullable [] rowNames) {
		return createDataFrame(colDatas, colNames, rowNames, RObject.CLASSNAME_DATAFRAME);
	}
	
	/**
	 * Creates an R data frame.
	 * 
	 * <p>The data frame has the default R class name 'data.frame'.</p>
	 * 
	 * <p>Note that the R object / its data stores may use the arrays directly.</p>
	 * 
	 * @return the R data frame
	 * 
	 * @since 4.8
	 */
	public default RDataFrame createDataFrame(final @NonNull RStore<?> [] colDatas,
			final @NonNull String [] colNames) {
		return createDataFrame(colDatas, colNames, null, RObject.CLASSNAME_DATAFRAME);
	}
	
	
	public RLanguage createName(String name);
	
	public RLanguage createExpression(String expr);
	
	
/*[ Data/RStore ]==============================================================*/
	
	public RLogicalStore createLogiData(boolean [] logiValues);
	
	/**
	 * Creates an R data stores of the type {@link RStore#LOGICAL} and the specified length.
	 * 
	 * <p>The store is initialized with value {@code FALSE}.</p>
	 * 
	 * @param length the length
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public RLogicalStore createLogiData(long length);
	
	
	public RIntegerStore createIntData(int [] intValues);
	
	/**
	 * Creates an R data stores of the type {@link RStore#INTEGER} and the specified length.
	 * 
	 * <p>The store is initialized with value {@code 0}.</p>
	 * 
	 * @param length the length
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public RIntegerStore createIntData(long length);
	
	
	public RNumericStore createNumData(double [] numValues);
	
	/**
	 * Creates an R data stores of the type {@link RStore#NUMERIC} and the specified length.
	 * 
	 * <p>The store is initialized with value {@code 0.0}.</p>
	 * 
	 * @param length the length
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public RNumericStore createNumData(long length);
	
	
	public RComplexStore createCplxData(double [] reValues, double [] imValues);
	
	/**
	 * Creates an R data stores of the type {@link RStore#COMPLEX} and the specified length.
	 * 
	 * <p>The store is initialized with value {@code 0.0+0.0i}.</p>
	 * 
	 * @param length the length
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public RComplexStore createCplxData(long length);
	
	
	/**
	 * Creates an R data stores of the type {@link RStore#CHARACTER} with the given initial values.
	 * 
	 * <p>Note that the R data store may use the array directly.</p>
	 * 
	 * @param charValues an array with the initial values: Java strings or {@code null} for NA
	 * @return the R data store
	 */
	public RCharacterStore createCharData(@Nullable String [] charValues);
	
	/**
	 * Creates an R data stores of the type {@link RStore#CHARACTER} with the given initial values.
	 * 
	 * @param charValues an array with the initial values: Java strings or {@code null} for NA
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public default RCharacterStore createCharData(final List<? extends @Nullable String> charValues) {
		return createCharData(charValues.toArray(new @Nullable String[charValues.size()]));
	}
	
	/**
	 * Creates an R data stores of the type {@link RStore#CHARACTER} with the given initial values.
	 * 
	 * <p>Note that the R data store may use the array directly.</p>
	 * 
	 * @param charValues an array with the initial values: UTF-8 encoded character strings or {@code null} for NA
	 * @return the R data store
	 * 
	 * @since 4.6
	 */
	public default RCharacterStore createCharDataUtf8(final byte [] @Nullable [] charValues) {
		final int length= charValues.length;
		final @Nullable String [] charValuesJava= new @Nullable String [length];
		for (int i= 0; i < length; i++) {
			final byte[] charValue= charValues[i];
			if (charValue != null) {
				charValuesJava[i]= new String(charValue, StandardCharsets.UTF_8);
			}
		}
		return createCharData(charValuesJava);
	}
	
	/**
	 * Creates an R data stores of the type {@link RStore#CHARACTER} and the specified length.
	 * 
	 * <p>The store is initialized with value {@code ""} (empty character string).</p>
	 * 
	 * @param length the length
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public RCharacterStore createCharData(long length);
	
	
	public RRawStore createRawData(byte [] rawValues);
	
	/**
	 * Creates an R data stores of the type {@link RStore#RAW} and the specified length.
	 * 
	 * <p>The store is initialized with value {@code 0x00}.</p>
	 * 
	 * @param length the length
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public RRawStore createRawData(long length);
	
	
	public RFactorStore createUnorderedFactorData(int[] codes, @Nullable String [] levels);
	@Deprecated(since= "4.8")
	public default RFactorStore createFactorData(final int[] codes, final @Nullable String [] levels) {
		return createUnorderedFactorData(codes, levels);
	}
	
	/**
	 * Creates an R data stores of the type {@link RStore#FACTOR} and the specified length
	 * with the given unordered levels.
	 * 
	 * <p>The store is initialized with NA.</p>
	 * 
	 * @param length the length
	 * @param levels the factor levels
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public RFactorStore createUnorderedFactorData(long length, @Nullable String [] levels);
	
	public RFactorStore createOrderedFactorData(int[] codes, @Nullable String [] levels);
	
	/**
	 * Creates an R data stores of the type {@link RStore#FACTOR} and the specified length
	 * with the given ordered levels.
	 * 
	 * <p>The store is initialized with NA.</p>
	 * 
	 * @param length the length
	 * @param levels the factor levels
	 * @return the R data store
	 * 
	 * @since 4.8
	 */
	public RFactorStore createOrderedFactorData(long length, @Nullable String [] levels);
	
	
/*[ Shortcuts RObject+Data/RStore ]============================================*/
	
	/**
	 * Creates an R logical vector with values from the given Java boolean array.
	 * 
	 * <p>The vector has the default R class name 'logical'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param logiValues an array with the initial values
	 * @return the R logical vector
	 * 
	 * @see #createLogiData(boolean[])
	 * @since 4.8
	 */
	public default RVector<RLogicalStore> createLogiVector(final boolean [] logiValues) {
		return createVector(createLogiData(logiValues), RObject.CLASSNAME_LOGICAL);
	}
	
	/**
	 * Creates an R logical vector of the specified length.
	 * 
	 * <p>The vector has the default R class name 'logical'.</p>
	 * 
	 * <p>The function works analog to the R function <code>logical(length)</code>;
	 * the store of the vector is initialized with value {@code FALSE}.</p>
	 * 
	 * @param length the length of the vector
	 * @return the R logical vector
	 * 
	 * @see #createLogiData(long)
	 * @since 4.8
	 */
	public default RVector<RLogicalStore> createLogiVector(final long length) {
		return createVector(createLogiData(length), RObject.CLASSNAME_LOGICAL);
	}
	
	/**
	 * Creates an R logical array with values from the given Java boolean array.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param logiValues an array with the initial values
	 * @param dim the dimension of the array
	 * @return the R logical array
	 * 
	 * @see #createLogiData(boolean[])
	 * @since 4.8
	 */
	public default RArray<RLogicalStore> createLogiArray(final boolean [] logiValues,
			final int[] dim) {
		return createArray(createLogiData(logiValues), dim);
	}
	
	/**
	 * Creates an R logical array of the specified dimension.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>The store of the array is initialized with value {@code FALSE}.</p>
	 * 
	 * @param dim the dimension of the array
	 * @return the R logical array
	 * 
	 * @see #createLogiData(long)
	 * @since 4.8
	 */
	public default RArray<RLogicalStore> createLogiArray(final int[] dim) {
		return createArray(createLogiData(RDataUtils.computeLengthFromDim(dim)), dim);
	}
	
	/**
	 * Creates an R logical matrix with values from the given Java boolean array.
	 * 
	 * <p>The array has the default R class name 'matrix'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param logiValues an array with the initial values
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R logical matrix
	 * 
	 * @see #createLogiData(boolean[])
	 * @since 4.8
	 */
	public default RArray<RLogicalStore> createLogiMatrix(final boolean [] logiValues,
			final int rowCount, final int colCount) {
		return createMatrix(createLogiData(logiValues), rowCount, colCount);
	}
	
	/**
	 * Creates an R logical matrix of the specified dimension.
	 * 
	 * <p>The array has the default R class name 'matrix'.</p>
	 * 
	 * <p>The store of the matrix is initialized with value {@code FALSE}.</p>
	 * 
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R logical matrix
	 * 
	 * @see #createLogiData(long)
	 * @since 4.8
	 */
	public default RArray<RLogicalStore> createLogiMatrix(final int rowCount, final int colCount) {
		return createMatrix(createLogiData(rowCount * colCount), rowCount, colCount);
	}
	
	
	/**
	 * Creates an R integer vector with values from the given Java integer array.
	 * 
	 * <p>The vector has the default R class name 'integer'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param intValues an array with the initial values
	 * @return the R integer vector
	 * 
	 * @see #createIntData(int[])
	 * @since 4.8
	 */
	public default RVector<RIntegerStore> createIntVector(final int [] intValues) {
		return createVector(createIntData(intValues), RObject.CLASSNAME_INTEGER);
	}
	
	/**
	 * Creates an R integer vector of the specified length.
	 * 
	 * <p>The vector has the default R class name 'integer'.</p>
	 * 
	 * <p>The function works analog to the R function <code>integer(length)</code>;
	 * the store of the vector is initialized with value {@code 0}.</p>
	 * 
	 * @param length the length of the vector
	 * @return the R integer vector
	 * 
	 * @see #createIntData(long)
	 * @since 4.8
	 */
	public default RVector<RIntegerStore> createIntVector(final long length) {
		return createVector(createIntData(length), RObject.CLASSNAME_INTEGER);
	}
	
	/**
	 * Creates an R integer array with values from the given Java integer array.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param intValues an array with the initial values
	 * @param dim the dimension of the array
	 * @return the R integer array
	 * 
	 * @see #createIntData(int[])
	 * @since 4.8
	 */
	public default RArray<RIntegerStore> createIntArray(final int [] intValues,
			final int[] dim) {
		return createArray(createIntData(intValues), dim);
	}
	
	/**
	 * Creates an R integer array of the specified dimension.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>The store of the array is initialized with value {@code 0}.</p>
	 * 
	 * @param dim the dimension of the array
	 * @return the R integer array
	 * 
	 * @see #createIntData(long)
	 * @since 4.8
	 */
	public default RArray<RIntegerStore> createIntArray(final int[] dim) {
		return createArray(createIntData(RDataUtils.computeLengthFromDim(dim)), dim);
	}
	
	/**
	 * Creates an R integer vector with values from the given Java integer array.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param intValues an array with the initial values
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R integer matrix
	 * 
	 * @see #createIntData(int[])
	 * @since 4.8
	 */
	public default RArray<RIntegerStore> createIntMatrix(final int [] intValues,
			final int rowCount, final int colCount) {
		return createMatrix(createIntData(intValues), rowCount, colCount);
	}
	
	/**
	 * Creates an R integer matrix of the specified dimension.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>The store of the matrix is initialized with value {@code 0}.</p>
	 * 
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R integer matrix
	 * 
	 * @see #createIntData(long)
	 * @since 4.8
	 */
	public default RArray<RIntegerStore> createIntMatrix(final int rowCount, final int colCount) {
		return createMatrix(createIntData(rowCount * colCount), rowCount, colCount);
	}
	
	
	/**
	 * Creates an R numeric vector with values from the given Java double array.
	 * 
	 * <p>The vector has the default R class name 'numeric'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param numValues an array with the initial values
	 * @return the R numeric vector
	 * 
	 * @see #createNumData(double[])
	 * @since 4.8
	 */
	public default RVector<RNumericStore> createNumVector(final double [] numValues) {
		return createVector(createNumData(numValues), RObject.CLASSNAME_NUMERIC);
	}
	
	/**
	 * Creates an R numeric vector of the specified length.
	 * 
	 * <p>The vector has the default R class name 'numeric'.</p>
	 * 
	 * <p>The function works analog to the R function <code>numeric(length)</code>;
	 * the store is initialized with value {@code 0.0}.</p>
	 * 
	 * @param length the length of the vector
	 * @return the R numeric vector
	 * 
	 * @see #createNumData(long)
	 * @since 4.8
	 */
	public default RVector<RNumericStore> createNumVector(final long length) {
		return createVector(createNumData(length), RObject.CLASSNAME_NUMERIC);
	}
	
	/**
	 * Creates an R numeric array with values from the given Java double array.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param numValues an array with the initial values
	 * @param dim the dimension of the array
	 * @return the R numeric array
	 * 
	 * @see #createNumData(double[])
	 * @since 4.8
	 */
	public default RArray<RNumericStore> createNumArray(final double [] numValues,
			final int[] dim) {
		return createArray(createNumData(numValues), dim);
	}
	
	/**
	 * Creates an R numeric array of the specified dimension.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>The function works analog to the R function <code>numeric(length)</code>;
	 * the store is initialized with value {@code 0.0}.</p>
	 * 
	 * @param dim the dimension of the array
	 * @return the R numeric array
	 * 
	 * @see #createNumData(long)
	 * @since 4.8
	 */
	public default RArray<RNumericStore> createNumArray(final int[] dim) {
		return createArray(createNumData(RDataUtils.computeLengthFromDim(dim)), dim);
	}
	
	/**
	 * Creates an R numeric matrix with values from the given Java double array.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param numValues an array with the initial values
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R numeric matrix
	 * 
	 * @see #createNumData(double[])
	 * @since 4.8
	 */
	public default RArray<RNumericStore> createNumMatrix(final double [] numValues,
			final int rowCount, final int colCount) {
		return createMatrix(createNumData(numValues), rowCount, colCount);
	}
	
	/**
	 * Creates an R numeric matrix of the specified dimension.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>The store is initialized with value {@code 0.0}.</p>
	 * 
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R numeric matrix
	 * 
	 * @see #createNumData(long)
	 * @since 4.8
	 */
	public default RArray<RNumericStore> createNumMatrix(final int rowCount, final int colCount) {
		return createMatrix(createNumData(rowCount * colCount), rowCount, colCount);
	}
	
	
	/**
	 * Creates an R complex vector of the specified length.
	 * 
	 * <p>The vector has the default R class name 'complex'.</p>
	 * 
	 * <p>The function works analog to the R function <code>complex(length)</code>;
	 * the store is initialized with value {@code 0.0+0.0i}.</p>
	 * 
	 * @param length the length of the vector
	 * @return the R complex vector
	 * 
	 * @see #createCplxData(long)
	 * @since 4.8
	 */
	public default RVector<RComplexStore> createCplxVector(final long length) {
		return createVector(createCplxData(length), RObject.CLASSNAME_COMPLEX);
	}
	
	
	/**
	 * Creates an R character vector with values from the given Java string array.
	 * 
	 * <p>The vector has the default R class name 'character'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param charValues an array with the initial values: Java strings or {@code null} for NA
	 * @return the R character vector
	 * 
	 * @see #createCharData(String[])
	 * @since 4.8
	 */
	public default RVector<RCharacterStore> createCharVector(final @Nullable String [] charValues) {
		return createVector(createCharData(charValues), RObject.CLASSNAME_CHARACTER);
	}
	
	/**
	 * Creates an R character vector with values from the given list with Java strings.
	 * 
	 * <p>The vector has the default R class name 'character'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param charValues a list with the initial values: Java strings or {@code null} for NA
	 * @return the R character vector
	 * 
	 * @see #createCharData(List)
	 * @since 4.8
	 */
	public default RVector<RCharacterStore> createCharVector(final List<? extends @Nullable String> charValues) {
		return createVector(createCharData(charValues), RObject.CLASSNAME_CHARACTER);
	}
	
	/**
	 * Creates an R character vector of the specified length.
	 * 
	 * <p>The vector has the default R class name 'character'.</p>
	 * 
	 * <p>The function works analog to the R function <code>character(length)</code>;
	 * the store is initialized with value {@code ""} (empty character string).</p>
	 * 
	 * @param length the length of the vector
	 * @return the R character vector
	 * 
	 * @see #createCharData(long)
	 * @since 4.8
	 */
	public default RVector<RCharacterStore> createCharVector(final long length) {
		return createVector(createCharData(length), RObject.CLASSNAME_CHARACTER);
	}
	
	/**
	 * Creates an R character array with values from the given Java string array.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param charValues an array with the initial values: Java strings or {@code null} for NA
	 * @param dim the dimension of the array
	 * @return the R character array
	 * 
	 * @see #createCharData(String[])
	 * @since 4.8
	 */
	public default RArray<RCharacterStore> createCharArray(final @Nullable String [] charValues,
			final int[] dim) {
		return createArray(createCharData(charValues), dim);
	}
	
	/**
	 * Creates an R character array with values from the given list with Java strings.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param charValues a list with the initial values: Java strings or {@code null} for NA
	 * @param dim the dimension of the array
	 * @return the R character array
	 * 
	 * @see #createCharData(List)
	 * @since 4.8
	 */
	public default RArray<RCharacterStore> createCharArray(final List<? extends @Nullable String> charValues,
			final int[] dim) {
		return createArray(createCharData(charValues), dim);
	}
	
	/**
	 * Creates an R character array of the specified dimension.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>The store is initialized with value {@code ""} (empty character string).</p>
	 * 
	 * @param dim the dimension of the array
	 * @return the R character array
	 * 
	 * @see #createCharData(long)
	 * @since 4.8
	 */
	public default RArray<RCharacterStore> createCharArray(final int[] dim) {
		return createArray(createCharData(RDataUtils.computeLengthFromDim(dim)), dim);
	}
	
	/**
	 * Creates an R character matrix with values from the given Java string array.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param charValues an array with the initial values: Java strings or {@code null} for NA
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R character matrix
	 * 
	 * @see #createCharData(String[])
	 * @since 4.8
	 */
	public default RArray<RCharacterStore> createCharMatrix(final @Nullable String [] charValues,
			final int rowCount, final int colCount) {
		return createMatrix(createCharData(charValues), rowCount, colCount);
	}
	
	/**
	 * Creates an R character matrix with values from the given list with Java strings.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param charValues a list with the initial values: Java strings or {@code null} for NA
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R character matrix
	 * 
	 * @see #createCharData(List)
	 * @since 4.8
	 */
	public default RArray<RCharacterStore> createCharMatrix(final List<? extends @Nullable String> charValues,
			final int rowCount, final int colCount) {
		return createMatrix(createCharData(charValues), rowCount, colCount);
	}
	
	/**
	 * Creates an R character matrix of the specified dimension.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>The store is initialized with value {@code ""} (empty character string).</p>
	 * 
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R character matrix
	 * 
	 * @see #createCharData(long)
	 * @since 4.8
	 */
	public default RArray<RCharacterStore> createCharMatrix(final int rowCount, final int colCount) {
		return createMatrix(createCharData(rowCount * colCount), rowCount, colCount);
	}
	
	
	/**
	 * Creates an R raw vector with values from the given Java byte array.
	 * 
	 * <p>The vector has the default R class name 'raw'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param rawValues an array with the initial values
	 * @return the R raw vector
	 * 
	 * @see #createRawData(byte[])
	 * @since 4.8
	 */
	public default RVector<RRawStore> createRawVector(final byte[] rawValues) {
		return createVector(createRawData(rawValues), RObject.CLASSNAME_RAW);
	}
	
	/**
	 * Creates an R raw vector of the specified length.
	 * 
	 * <p>The vector has the default R class name 'raw'.</p>
	 * 
	 * <p>The function works analog to the R function {@code raw(length)};
	 * the store is initialized with value {@code 0x00}.</p>
	 * 
	 * @param length the length of the vector
	 * @return the R raw vector
	 * 
	 * @see #createRawData(long)
	 * @since 4.8
	 */
	public default RVector<RRawStore> createRawVector(final long length) {
		return createVector(createRawData(length), RObject.CLASSNAME_RAW);
	}
	
	/**
	 * Creates an R raw array with values from the given Java byte array.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param rawValues an array with the initial values
	 * @param dim the dimension of the array
	 * @return the R raw array
	 * 
	 * @see #createRawData(byte[])
	 * @since 4.8
	 */
	public default RArray<RRawStore> createRawArray(final byte [] rawValues,
			final int[] dim) {
		return createArray(createRawData(rawValues), dim);
	}
	
	/**
	 * Creates an R raw array of the specified dimension.
	 * 
	 * <p>The array has the default R class name 'matrix' (for 2 dims) or 'array'.</p>
	 * 
	 * <p>The store is initialized with value {@code 0x00}.</p>
	 * 
	 * @param dim the dimension of the array
	 * @return the R raw array
	 * 
	 * @see #createRawData(long)
	 * @since 4.8
	 */
	public default RArray<RRawStore> createRawArray(final int[] dim) {
		return createArray(createRawData(RDataUtils.computeLengthFromDim(dim)), dim);
	}
	
	/**
	 * Creates an R raw matrix with values from the given Java byte array.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param rawValues an array with the initial values
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R raw matrix
	 * 
	 * @see #createRawData(byte[])
	 * @since 4.8
	 */
	public default RArray<RRawStore> createRawMatrix(final byte [] rawValues,
			final int rowCount, final int colCount) {
		return createMatrix(createRawData(rawValues), rowCount, colCount);
	}
	
	/**
	 * Creates an R raw matrix of the specified dimension.
	 * 
	 * <p>The matrix has the default R class name 'matrix'.</p>
	 * 
	 * <p>The store is initialized with value {@code 0x00}.</p>
	 * 
	 * @param rowCount the number of rows of the matrix
	 * @param colCount the number of columns of the matrix
	 * @return the R raw matrix
	 * 
	 * @see #createRawData(long)
	 * @since 4.8
	 */
	public default RArray<RRawStore> createRawMatrix(final int rowCount, final int colCount) {
		return createMatrix(createRawData(rowCount * colCount), rowCount, colCount);
	}
	
	
	/**
	 * Creates an R (unordered) factor vector with level codes from the given Java integer array.
	 * 
	 * <p>The vector has the default R class name 'factor'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param codes the coded levels
	 * @param levels the unordered levels
	 * @return the R factor vector
	 * 
	 * @see #createUnorderedFactorData(int[], String[])
	 * @since 4.8
	 */
	public default RVector<RFactorStore> createUnorderedFactorVector(final int [] codes,
			final @Nullable String [] levels) {
		return createVector(createUnorderedFactorData(codes, levels), RObject.CLASSNAME_FACTOR);
	}
	
	/**
	 * Creates an R (unordered) factor vector of the specified length.
	 * 
	 * <p>The vector has the default R class name 'factor'.</p>
	 * 
	 * @param length the length of the vector
	 * @param levels the unordered levels
	 * @return the R factor vector
	 * 
	 * @see #createUnorderedFactorData(long, String[])
	 * @since 4.8
	 */
	public default RVector<RFactorStore> createUnorderedFactorVector(final long length,
			final @Nullable String [] levels) {
		return createVector(createUnorderedFactorData(length, levels), RObject.CLASSNAME_FACTOR);
	}
	
	/**
	 * Creates an R ordered factor vector with level codes from the given Java integer array.
	 * 
	 * <p>The vector has the default R class name 'ordered'.</p>
	 * 
	 * <p>Note that the R object / its data store may use the array directly.</p>
	 * 
	 * @param codes the coded levels
	 * @param levels the ordered levels
	 * @return the R factor vector
	 * 
	 * @see #createOrderedFactorData(int[], String[])
	 * @since 4.8
	 */
	public default RVector<RFactorStore> createOrderedFactorVector(final int [] codes,
			final @Nullable String [] levels) {
		return createVector(createOrderedFactorData(codes, levels), RObject.CLASSNAME_ORDERED);
	}
	
	/**
	 * Creates an R ordered factor vector of the specified length.
	 * 
	 * <p>The vector has the default R class name 'ordered'.</p>
	 * 
	 * @param length the length of the vector
	 * @param levels the ordered levels
	 * @return the R factor vector
	 * 
	 * @see #createOrderedFactorData(long, String[])
	 * @since 4.8
	 */
	public default RVector<RFactorStore> createOrderedFactorVector(final long length,
			final @Nullable String [] levels) {
		return createVector(createOrderedFactorData(length, levels), RObject.CLASSNAME_ORDERED);
	}
	
	
/*[ RJ-IO ]====================================================================*/
	
	void writeObject(RObject object, RJIO io) throws IOException;
	RObject readObject(RJIO io) throws IOException;
	
	void writeStore(RStore<?> data, RJIO io) throws IOException;
	RStore<?> readStore(RJIO io, long length) throws IOException;
	
	void writeAttributeList(RList list, RJIO io) throws IOException;
	RList readAttributeList(RJIO io) throws IOException;
	
	void writeNames(@Nullable RStore<?> names, RJIO io) throws IOException;
	@Nullable RStore<?> readNames(RJIO io, long length) throws IOException;
	
}
