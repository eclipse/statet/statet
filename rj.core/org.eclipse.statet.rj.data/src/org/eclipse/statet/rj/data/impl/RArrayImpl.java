/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class RArrayImpl<TData extends RStore<?>> extends AbstractRObject
		implements RArray<TData>, ExternalizableRObject {
	
	
	private long length;
	private TData data;
	
	private String className1;
	private RInteger32Store dimAttribute;
	private @Nullable SimpleRList<RStore<?>> dimnamesAttribute;
	
	
	public RArrayImpl(final TData data, final String className1, final int[] dim) {
		if (data == null || className1 == null || dim == null) {
			throw new NullPointerException();
		}
		this.length= RDataUtils.computeLengthFromDim(dim);
		if (data.getLength() >= 0 && data.getLength() != this.length) {
			throw new IllegalArgumentException("dim");
		}
		this.data= data;
		this.className1= className1;
		this.dimAttribute= new RInteger32Store(dim);
	}
	
	@SuppressWarnings("null")
	public RArrayImpl(final RJIO io, final RObjectFactory factory) throws IOException {
		readExternal(io, factory);
	}
	
	public void readExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		//-- options
		final int options= io.readInt();
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			this.className1= io.readString();
		}
		this.length= io.readVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK));
		final int[] dim= io.readIntArray();
		this.dimAttribute= new RInteger32Store(dim);
		if ((options & RObjectFactory.O_WITH_NAMES) != 0) {
			final RCharacter32Store names0= new RCharacter32Store(io, dim.length);
			final RStore<?>[] names1= new RStore[dim.length];
			for (int i= 0; i < dim.length; i++) {
				names1[i]= factory.readNames(io, dim[i]);
			}
			this.dimnamesAttribute= new SimpleRList<>(names1, names0);
		}
		//-- data
		this.data= (TData) factory.readStore(io, this.length);
		
		if ((options & RObjectFactory.O_CLASS_NAME) == 0) {
			this.className1= (dim.length == 2) ? RObject.CLASSNAME_MATRIX : RObject.CLASSNAME_ARRAY;
		}
		//-- attributes
		if ((options & RObjectFactory.O_WITH_ATTR) != 0) {
			setAttributes(factory.readAttributeList(io));
		}
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		final int n= this.dimAttribute.length();
		//-- options
		int options= io.getVULongGrade(this.length);
		if (!this.className1.equals((n == 2) ?
				RObject.CLASSNAME_MATRIX : RObject.CLASSNAME_ARRAY )) {
			options |= RObjectFactory.O_CLASS_NAME;
		}
		if ((io.flags & RObjectFactory.F_ONLY_STRUCT) == 0 && this.dimnamesAttribute != null) {
			options |= RObjectFactory.O_WITH_NAMES;
		}
		final RList attributes= ((io.flags & RObjectFactory.F_WITH_ATTR) != 0) ? getAttributes() : null;
		if (attributes != null) {
			options |= RObjectFactory.O_WITH_ATTR;
		}
		io.writeInt(options);
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			io.writeString(this.className1);
		}
		io.writeVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK), this.length);
		io.writeInt(n);
		this.dimAttribute.writeExternal(io);
		if ((options & RObjectFactory.O_WITH_NAMES) != 0) {
			((ExternalizableRStore) this.dimnamesAttribute.getNames()).writeExternal(io);
			for (int i= 0; i < n; i++) {
				factory.writeNames(this.dimnamesAttribute.get(i), io);
			}
		}
		//-- data
		factory.writeStore(this.data, io);
		//-- attributes
		if ((options & RObjectFactory.O_WITH_ATTR) != 0) {
			factory.writeAttributeList(attributes, io);
		}
	}
	
	
	@Override
	public final byte getRObjectType() {
		return TYPE_ARRAY;
	}
	
	@Override
	public String getRClassName() {
		return this.className1;
	}
	
	@Override
	public long getLength() {
		return this.length;
	}
	
	@Override
	public RIntegerStore getDim() {
		return this.dimAttribute;
	}
	
	@Override
	public @Nullable RCharacterStore getDimNames() {
		if (this.dimnamesAttribute != null) {
			return this.dimnamesAttribute.getNames();
		}
		return null;
	}
	
	@Override
	public @Nullable RStore<?> getNames(final int dim) {
		if (this.dimnamesAttribute != null) {
			return this.dimnamesAttribute.get(dim);
		}
		return null;
	}
	
	
	@Override
	public TData getData() {
		return this.data;
	}
	
	
//	protected int[] getDataInsertIdxs(final int dim, final int idx) {
//		if (dim >= this.dimAttribute.length || idx >= this.dimAttribute.intValues[dim]) {
//			throw new IllegalArgumentException();
//		}
//		final int size= this.data.getLength() / this.dimAttribute.intValues[dim];
//		final int[] dataIdxs= new int[size];
//		int step= 1;
//		int stepCount= 1;
//		for (int currentDimI= 0; currentDimI < this.dimAttribute.length; currentDimI++) {
//			final int currentDimLength= this.dimAttribute.intValues[currentDimI];
//			if (currentDimI == dim) {
//				final int add= step*idx;
//				for (int i= 0; i < size; i++) {
//					dataIdxs[i]+= add;
//				}
//			}
//			else {
//				if (currentDimI > 0) {
//					int temp= 0;
//					for (int i= 0; i < size; ) {
//						final int add= step*temp;
//						for (int j= 0; j < stepCount && i < size; i++,j++) {
//							dataIdxs[i]+= add;
//						}
//						temp++;
//						if (temp == currentDimLength) {
//							temp= 0;
//						}
//					}
//				}
//				stepCount *= currentDimLength;
//			}
//			step *= currentDimLength;
//		}
//		return dataIdxs;
//	}
//	
//	public void setDim(final int[] dim) {
//		checkDim(getLength(), dim);
//		this.dimAttribute= dim;
//	}
//	
//	public void setDimNames(final List<RCharacterStore> list) {
//	}
	
//	public void insert(final int dim, final int idx) {
//		((RDataResizeExtension) this.data).insertNA(getDataInsertIdxs(dim, idx));
//		this.dimAttribute[dim]++;
//		if (this.dimnamesAttribute != null) {
//			final RVector<RCharacterStore> names= (RVector<RCharacterStore>) this.dimnamesAttribute.get(dim);
//			if (names != null) {
//				names.insert(idx);
//			}
//		}
//	}
//	
//	public void remove(final int dim, final int idx) {
//		((RDataResizeExtension) this.data).remove(RDataUtils.getDataIdxs(this.dimAttribute, dim, idx));
//		this.dimAttribute[dim]--;
//		if (this.dimnamesAttribute != null) {
//			final RVector<RCharacterStore> names= (RVector<RCharacterStore>) this.dimnamesAttribute.get(dim);
//			if (names != null) {
//				names.remove(idx);
//			}
//		}
//	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("RObject type=RArray, class=").append(getRClassName());
		sb.append("\n\tlength=").append(getLength());
		sb.append("\n\tdim=");
		this.dimAttribute.appendTo(sb);
		sb.append("\n\tdata: ");
		sb.append(this.data.toString());
		return sb.toString();
	}
	
}
