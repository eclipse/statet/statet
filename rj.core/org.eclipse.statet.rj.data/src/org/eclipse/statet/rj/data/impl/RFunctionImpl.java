/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RFunction;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class RFunctionImpl extends AbstractRObject
		implements RFunction, ExternalizableRObject {
	
	
	private @Nullable String headerSource;
	private @Nullable String bodySource;
	
	
	public RFunctionImpl(final @Nullable String header) {
		this.headerSource= header;
	}
	
	@SuppressWarnings("null")
	public RFunctionImpl(final RJIO io, final RObjectFactory factory) throws IOException {
		readExternal(io, factory);
	}
	
	public void readExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		/*final int options =*/ io.readInt();
		this.headerSource= io.readString();
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		io.writeInt(/*options*/ 0);
		io.writeString(this.headerSource);
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_FUNCTION;
	}
	
	@Override
	public String getRClassName() {
		return "function";
	}
	
	
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public @Nullable String getHeaderSource() {
		return this.headerSource;
	}
	
	@Override
	public @Nullable String getBodySource() {
		return this.bodySource;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
}
