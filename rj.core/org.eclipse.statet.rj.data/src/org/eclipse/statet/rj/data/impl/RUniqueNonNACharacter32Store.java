/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;


@NonNullByDefault
public class RUniqueNonNACharacter32Store extends RUniqueCharacter32Store {
	
	
	public RUniqueNonNACharacter32Store() {
		super();
	}
	
	@SuppressWarnings("null")
	public RUniqueNonNACharacter32Store(final @NonNull String [] initialValues) {
		super(initialValues);
	}
	
	RUniqueNonNACharacter32Store(final RCharacter32Store source, final boolean reuse) {
		super(source, reuse);
	}
	
	public RUniqueNonNACharacter32Store(final RJIO io, final int length) throws IOException {
		super(io, length);
	}
	
	
	@Override
	public boolean isNA(final long idx) {
		return false;
	}
	
	@Override
	public boolean isNA(final int idx) {
		return false;
	}
	
	@Override
	public void setNA(final int idx) {
		throw new IllegalArgumentException();
	}
	
	@Override
	public void setNA(final long idx) {
		throw new IllegalArgumentException();
	}
	
	@Override
	public void insertNA(final int idx) {
		throw new IllegalArgumentException();
	}
	
	@Override
	public void insertNA(final int[] idxs) {
		throw new IllegalArgumentException();
	}
	
	
	@Override
	public boolean containsNA() {
		return false;
	}
	
	@Override
	public long indexOfNA(final long fromIdx) {
		return -1;
	}
	
	@Override
	public int indexOfNA(final int fromIdx) {
		return -1;
	}
	
	
}
