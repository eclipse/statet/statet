/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RReference;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class RReferenceImpl implements RReference, ExternalizableRObject {
	
	
	private long handle;
	private byte type;
	private String baseClassName;
	
	
	public RReferenceImpl(final long handler, final byte type, final String baseClass) {
		this.handle= handler;
		this.type= type;
		this.baseClassName= baseClass;
	}
	
	@SuppressWarnings("null")
	public RReferenceImpl(final RJIO io, final RObjectFactory factory) throws IOException {
		readExternal(io, factory);
	}
	
	public void readExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		this.handle= io.readLong();
		this.type= io.readByte();
		this.baseClassName= io.readString();
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		io.writeLong(this.handle);
		io.writeByte(this.type);
		io.writeString(this.baseClassName);
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_REFERENCE;
	}
	
	@Override
	public byte getReferencedRObjectType() {
		return this.type;
	}
	
	@Override
	public String getRClassName() {
		return this.baseClassName;
	}
	
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public long getHandle() {
		return this.handle;
	}
	
	@Override
	public @Nullable RObject getResolvedRObject() {
		return null;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
	@Override
	public @Nullable RList getAttributes() {
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return (int)this.handle;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof RReference
						&& this.handle == ((RReference)obj).getHandle() ));
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("RObjectReference type= ").append(RDataUtils.getObjectTypeName(this.type));
		sb.append(", class= ").append(getRClassName());
		return sb.toString();
	}
	
}
