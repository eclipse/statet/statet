/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;


/**
 * Default implementation of an R object of type {@link RObject#TYPE_PROMISE PROMISE}.
 * 
 * @since de.walware.rj.data 0.6
 */
@NonNullByDefault
public class RPromiseImpl implements RObject, ExternalizableRObject {
	
	
	public static final RPromiseImpl INSTANCE= new RPromiseImpl();
	
	
	private final @Nullable RObject expression;
	private final @Nullable RReferenceImpl environment;
	
	
	public RPromiseImpl() {
		this.expression= null;
		this.environment= null;
	}
	
	public RPromiseImpl(final RObject expression, final RReferenceImpl environment) {
		this.expression= expression;
		this.environment= environment;
	}
	
	public RPromiseImpl(final RJIO io, final RObjectFactory factory) throws IOException {
		final int options= io.readInt();
		if ((options & RObjectFactory.O_NO_CHILDREN) == 0) {
			this.expression= factory.readObject(io);
			this.environment= new RReferenceImpl(io, factory);
		}
		else {
			this.expression= null;
			this.environment= null;
		}
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		if ((io.flags & RObjectFactory.F_WITH_DBG) == 0) {
			return;
		}
		int options= 0;
		if (this.expression == null) {
			options|= RObjectFactory.O_NO_CHILDREN;
		}
		io.writeInt(options);
		
		if ((options & RObjectFactory.O_NO_CHILDREN) == 0) {
			factory.writeObject(this.expression, io);
			this.environment.writeExternal(io, factory);
		}
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_PROMISE;
	}
	
	@Override
	public String getRClassName() {
		return "<promise>";
	}
	
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
	@Override
	public @Nullable RList getAttributes() {
		return null;
	}
	
	
	public @Nullable RObject getExpression() {
		return this.expression;
	}
	
	public @Nullable RObject getEnvironment() {
		if (this.environment == null) {
			return null;
		}
		return (this.environment.getReferencedRObjectType() == RObject.TYPE_ENVIRONMENT) ?
				this.environment : RNullImpl.INSTANCE;
	}
	
}
