/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Interface for R data stores of type {@link RStore#RAW}.
 * <p>
 * An R data store implements this interface if the R function
 * <code>typeof(object)</code> returns 'raw'.</p>
 */
@NonNullByDefault
public interface RRawStore extends RStore<Byte> {
	
	
	byte MIN_BYTE= 0;
	byte MAX_BYTE= (byte)0xFF;
	
	int MIN_INT= (MIN_BYTE & 0xFF);
	int MAX_INT= (MAX_BYTE & 0xFF);
	
	
	byte[] toRawArray();
	
	@Override
	String getChar(final int idx);
	@Override
	String getChar(final long idx);
	
	
	@Override
	Byte get(int idx);
	@Override
	Byte get(long idx);
	
	@Override
	@SuppressWarnings("null")
	@NonNull Byte [] toArray();
	
}
