/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class RComplexStructStore extends AbstractComplexStore {
	
	
	public RComplexStructStore() {
	}
	
	
	@Override
	protected final boolean isStructOnly() {
		return true;
	}
	
	
	@Override
	public final long getLength() {
		return -1;
	}
	
	@Override
	public boolean isNA(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean isNA(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean isNaN(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean isNaN(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean isMissing(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean isMissing(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public double getCplxRe(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public double getCplxRe(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public double getCplxIm(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public double getCplxIm(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public Complex get(final long idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Complex get(final int idx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public @Nullable Complex [] toArray() {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public long indexOfNA(final long fromIdx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public long indexOf(final int integer, final long fromIdx) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public long indexOf(final String character, final long fromIdx) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public boolean allEqual(final RStore<?> other) {
		return (COMPLEX == other.getStoreType()
				&& -1 == other.getLength() );
	}
	
}
