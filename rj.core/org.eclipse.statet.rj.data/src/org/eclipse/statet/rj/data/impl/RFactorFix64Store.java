/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;
import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RFactorStore;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RStore;


/**
 * {@link RFactorStore} supporting long length.
 */
@NonNullByDefault
public class RFactorFix64Store extends AbstractFactorStore
		implements ExternalizableRStore {
	
	
	public static final int SEGMENT_LENGTH= DEFAULT_LONG_DATA_SEGMENT_LENGTH;
	
	
	private final long length;
	
	private final int [] @NonNull [] codes;
	
	private final RCharacter32Store codeLabels;
	
	
	public RFactorFix64Store(final long length,
			final boolean isOrdered, final @Nullable String [] levelLabels) {
		this.length= length;
		this.isOrdered= isOrdered;
		this.codes= new2dIntArray(length, SEGMENT_LENGTH);
		Arrays.fill(this.codes, NA_integer_INT);
		this.codeLabels= new RUniqueCharacter32Store(levelLabels);
	}
	
	public RFactorFix64Store(final int [] @NonNull [] codes,
			final boolean isOrdered, final @Nullable String [] levelLabels) {
		this.length= check2dArrayLength(codes, SEGMENT_LENGTH);
		this.isOrdered= isOrdered;
		this.codes= codes;
		this.codeLabels= new RUniqueCharacter32Store(levelLabels);
	}
	
	public RFactorFix64Store(final int [] @NonNull [] codes,
			final boolean @Nullable [] @NonNull [] isNAs,
			final boolean isOrdered, final @Nullable String [] levelLabels) {
		this.length= check2dArrayLength(codes, SEGMENT_LENGTH);
		this.isOrdered= isOrdered;
		this.codes= codes;
		this.codeLabels= new RUniqueCharacter32Store(levelLabels);
		if (isNAs != null) {
			if (check2dArrayLength(isNAs, SEGMENT_LENGTH) != this.length) {
				throw new IllegalArgumentException();
			}
			for (int i= 0; i < isNAs.length; i++) {
				final boolean[] isNAi= isNAs[i];
				for (int j= 0; j < isNAi.length; j++) {
					if (isNAi[j]) {
						this.codes[i][j]= NA_integer_INT;
					}
				}
			}
		}
	}
	
	
	public RFactorFix64Store(final RJIO io, final long length) throws IOException {
		this.length= length;
		this.isOrdered= io.readBoolean();
		this.codes= new2dIntArray(length, SEGMENT_LENGTH);
		for (int i= 0; i < this.codes.length; i++) {
			io.readIntData(this.codes[i], this.codes[i].length);
		}
		this.codeLabels= readLabels(io, io.readInt());
	}
	protected RCharacter32Store readLabels(final RJIO io, final int l) throws IOException {
		return new RUniqueCharacter32Store(io, l);
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeBoolean(this.isOrdered);
		for (int i= 0; i < this.codes.length; i++) {
			io.writeIntData(this.codes[i], this.codes[i].length);
		}
		io.writeInt(this.codeLabels.length());
		this.codeLabels.writeExternal(io);
	}
	
	
	@Override
	protected final boolean isStructOnly() {
		return false;
	}
	
	
	@Override
	public final long getLength() {
		return this.length;
	}
	
	@Override
	public boolean isNA(final int idx) {
		return (this.codes[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] <= 0);
	}
	
	@Override
	public boolean isNA(final long idx) {
		return (this.codes[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] <= 0);
	}
	
	@Override
	public void setNA(final int idx) {
		this.codes[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] =
				NA_integer_INT;
	}
	
	@Override
	public void setNA(final long idx) {
		this.codes[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] =
				NA_integer_INT;
	}
	
	@Override
	public boolean isMissing(final int idx) {
		return (this.codes[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH] <= 0);
	}
	
	@Override
	public boolean isMissing(final long idx) {
		return (this.codes[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)] <= 0);
	}
	
	@Override
	public int getInt(final int idx) {
		return this.codes[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH];
	}
	
	@Override
	public int getInt(final long idx) {
		return this.codes[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)];
	}
	
	@Override
	public void setInt(final int idx, final int code) {
		if (code <= 0 || code > this.codeLabels.length()) {
			throw new IllegalArgumentException();
		}
		this.codes[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH]= code;
	}
	
	@Override
	public void setInt(final long idx, final int code) {
		if (code <= 0 || code > this.codeLabels.length()) {
			throw new IllegalArgumentException();
		}
		this.codes[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)]= code;
	}
	
	@Override
	public @Nullable String getChar(final int idx) {
		final int code= this.codes[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH];
		return (code > 0) ? this.codeLabels.getChar(code - 1): null;
	}
	
	@Override
	public @Nullable String getChar(final long idx) {
		final int code= this.codes[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)];
		return (code > 0) ? this.codeLabels.getChar(code - 1): null;
	}
	
	@Override
	public void setChar(final int idx, final String data) {
		final int code= this.codeLabels.indexOf(data, 0) + 1;
		if (code <= 0) {
			throw new IllegalArgumentException();
		}
		this.codes[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH]= code;
	}
	
	@Override
	public void setChar(final long idx, final String data) {
		final int code= this.codeLabels.indexOf(data, 0) + 1;
		if (code <= 0) {
			throw new IllegalArgumentException();
		}
		this.codes[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)]= code;
	}
	
	
	@Override
	public RCharacterStore getLevels() {
		return this.codeLabels;
	}
	
	@Override
	public int getLevelCount() {
		return this.codeLabels.length();
	}
	
	@Override
	public RCharacterStore toCharacterData() {
		final @Nullable String [] @NonNull [] data= new2dStringArray(this.length, SEGMENT_LENGTH);
		for (int i= 0; i < data.length; i++) {
			final var chars= data[i];
			final int[] ints= this.codes[i];
			for (int j= 0; j < ints.length; j++) {
				final int code= ints[j];
				if (code > 0) {
					chars[j]= this.codeLabels.getChar(code - 1);
				}
			}
		}
		return new RCharacterFix64Store(data);
	}
	
	
	@Override
	public @Nullable Integer get(final int idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		final int code= this.codes[idx / SEGMENT_LENGTH][idx % SEGMENT_LENGTH];
		return (code > 0) ?
				Integer.valueOf(code) :
				null;
	}
	
	@Override
	public @Nullable Integer get(final long idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		final int code= this.codes[(int)(idx / SEGMENT_LENGTH)][(int)(idx % SEGMENT_LENGTH)];
		return (code > 0) ?
				Integer.valueOf(code) :
				null;
	}
	
	@Override
	public @Nullable Integer [] toArray() {
		final int l= checkToArrayLength();
		final var array= new @Nullable Integer [l];
		for (int i= 0, destIdx= 0; i < this.codes.length; i++) {
			final int[] ints= this.codes[i];
			for (int j= 0; j < ints.length; j++, destIdx++) {
				final int code= ints[j];
				if (code > 0) {
					array[destIdx]= Integer.valueOf(code);
				}
			}
		}
		return array;
	}
	
	
	@Override
	public long indexOfNA(long fromIdx) {
		if (fromIdx < 0) {
			fromIdx= 0;
		}
		int i= (int)(fromIdx / SEGMENT_LENGTH);
		int j= (int)(fromIdx % SEGMENT_LENGTH);
		while (i < this.codes.length) {
			final int[] ints= this.codes[i];
			while (j < ints.length) {
				if (ints[i] == NA_integer_INT) {
					return (i * (long)SEGMENT_LENGTH) + j;
				}
			}
			i++;
			j= 0;
		}
		return -1;
	}
	
	@Override
	public long indexOf(final int code, long fromIdx) {
		if (code <= 0 || code > this.codeLabels.length()) {
			return -1;
		}
		if (fromIdx < 0) {
			fromIdx= 0;
		}
		int i= (int)(fromIdx / SEGMENT_LENGTH);
		int j= (int)(fromIdx % SEGMENT_LENGTH);
		while (i < this.codes.length) {
			final int[] ints= this.codes[i];
			while (j < ints.length) {
				if (ints[i] == code) {
					return (i * (long)SEGMENT_LENGTH) + j;
				}
			}
			i++;
			j= 0;
		}
		return -1;
	}
	
	@Override
	public long indexOf(final @Nullable String character, final long fromIdx) {
		final int code= ((character != null) ?
				this.codeLabels.indexOf(character, 0) :
				this.codeLabels.indexOfNA(0) ) + 1;
		return indexOf(code, fromIdx);
	}
	
	
	@Override
	public boolean allEqual(final RStore<?> other) {
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
}
