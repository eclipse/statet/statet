/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public class RNullImpl implements RObject {
	
	
	public static final RNullImpl INSTANCE= new RNullImpl();
	
	
	public RNullImpl() {
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_NULL;
	}
	
	@Override
	public String getRClassName() {
		return CLASSNAME_NULL;
	}
	
	/**
	 * Returns the length of the object. The length of {@link RObject#TYPE_NULL NULL}
	 * is always zero.
	 * 
	 * @return the length
	 */
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
	@Override
	public @Nullable RList getAttributes() {
		return null;
	}
	
	@Override
	public int hashCode() {
		return 15677;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj ||
				(obj instanceof RObject
						&& ((RObject)obj).getRObjectType() == RObject.TYPE_NULL ));
	}
	
	
	@Override
	public String toString() {
		return "RObject type=RNull";
	}
	
}
