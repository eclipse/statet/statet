/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import java.io.IOException;
import java.io.ObjectInput;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RComplexStore;
import org.eclipse.statet.rj.data.RDataFrame;
import org.eclipse.statet.rj.data.RFactorStore;
import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RLogicalStore;
import org.eclipse.statet.rj.data.RNumericStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RRawStore;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.RVector;


@NonNullByDefault
public class DefaultRObjectFactory implements RObjectFactory {
	
	
	public static final DefaultRObjectFactory INSTANCE= new DefaultRObjectFactory();
	
	public static final RNumericStore NUM_STRUCT_DUMMY= new RNumericStructStore();
	public static final RComplexStructStore CPLX_STRUCT_DUMMY= new RComplexStructStore();
	public static final RIntegerStructStore INT_STRUCT_DUMMY= new RIntegerStructStore();
	public static final RLogicalStructStore LOGI_STRUCT_DUMMY= new RLogicalStructStore();
	public static final RRawStructStore RAW_STRUCT_DUMMY= new RRawStructStore();
	public static final RCharacterStructStore CHR_STRUCT_DUMMY= new RCharacterStructStore();
	
	
	private final long storeLengthFixLong= AbstractRStore.DEFAULT_LONG_DATA_SEGMENT_LENGTH;
	
	
	public DefaultRObjectFactory() {
	}
	
	
	/*-- Vector --*/
	
	@Override
	public <TData extends RStore<?>> RVector<TData> createVector(final TData data, final String className) {
		return new RVectorImpl<>(data, className);
	}
	
	
	@Deprecated(since= "4.8")
	public RVector<RLogicalStore> createLogiVector(final int length) {
		return createLogiVector((long)length);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RIntegerStore> createIntVector(final int length) {
		return createIntVector((long)length);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RNumericStore> createNumVector(final int length) {
		return createNumVector((long)length);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RComplexStore> createCplxVector(final int length) {
		return createCplxVector((long)length);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RCharacterStore> createCharVector(final int length) {
		return createCharVector((long)length);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RRawStore> createRawVector(final int length) {
		return createRawVector((long)length);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RFactorStore> createFactorVector(final int [] codes, final @Nullable String [] levels) {
		return createUnorderedFactorVector(codes, levels);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RFactorStore> createFactorVector(final int length, final @Nullable String [] levels) {
		return createUnorderedFactorVector(length, levels);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RFactorStore> createOrderedVector(final int [] codes, final @Nullable String [] levels) {
		return createOrderedFactorVector(codes, levels);
	}
	
	@Deprecated(since= "4.8")
	public RVector<RFactorStore> createOrderedVector(final int length, final @Nullable String [] levels) {
		return createOrderedFactorVector(length, levels);
	}
	
	
	/*-- Array/Matrix --*/
	
	@Override
	public <TData extends RStore<?>> RArray<TData> createArray(final TData data, final int[] dim,
			final String className) {
		return new RArrayImpl<>(data, className, dim);
	}
	
	
	/*-- DataFrame --*/
	
	@Override
	public RDataFrame createDataFrame(final @NonNull RStore<?> [] colDatas,
			final @NonNull String [] colNames,
			final @NonNull String @Nullable [] rowNames,
			final String className) {
		final @NonNull RObject [] colVectors= new @NonNull RObject[colDatas.length];
		for (int i= 0; i < colVectors.length; i++) {
			colVectors[i]= createVector(colDatas[i]);
		}
		return createDataFrame(colVectors, colNames, rowNames, className);
	}
	
	public RDataFrame createDataFrame(final @NonNull RObject [] colVectors,
			final @NonNull String [] colNames,
			final @NonNull String @Nullable [] rowNames,
			final String className) {
		return new RDataFrame32Impl(colVectors, className, colNames, rowNames);
	}
	
	
	@Override
	public RList createList(final RObject [] components, final @Nullable String @Nullable [] names,
			final String className) {
		return new RList32Impl(components, className, names);
	}
	
	
	/*-- Language --*/
	
	@Override
	public RLanguage createName(final String name) {
		return new RLanguageImpl(RLanguage.NAME, name, RObject.CLASSNAME_NAME);
	}
	
	@Override
	public RLanguage createExpression(final String expr) {
		return new RLanguageImpl(RLanguage.EXPRESSION, expr, RObject.CLASSNAME_EXPRESSION);
	}
	
	
	/*-- Data/RStore --*/
	
	@Override
	public RLogicalStore createLogiData(final boolean [] logiValues) {
		return new RLogicalByte32Store(logiValues);
	}
	
	@Override
	public RLogicalStore createLogiData(final long length) {
		return (length <= this.storeLengthFixLong) ?
				new RLogicalByte32Store((int)length) :
				new RLogicalByteFix64Store(length);
	}
	
	@Override
	public RIntegerStore createIntData(final int [] intValues) {
		return new RInteger32Store(intValues);
	}
	
	@Override
	public RIntegerStore createIntData(final long length) {
		return (length <= this.storeLengthFixLong) ?
				new RInteger32Store((int)length) :
				new RIntegerFix64Store(length);
	}
	
	@Override
	public RNumericStore createNumData(final double [] numValues) {
		return new RNumericB32Store(numValues);
	}
	
	@Override
	public RNumericStore createNumData(final long length) {
		return (length <= this.storeLengthFixLong) ?
				new RNumericB32Store((int)length) :
				new RNumericBFix64Store(length);
	}
	
	@Override
	public RComplexStore createCplxData(final double [] reValues, final double [] imValues) {
		return new RComplexB32Store(reValues, imValues, (int[])null);
	}
	
	@Override
	public RComplexStore createCplxData(final long length) {
		return (length <= this.storeLengthFixLong) ?
				new RComplexB32Store((int)length) :
				new RComplexBFix64Store(length);
	}
	
	@Override
	public RCharacterStore createCharData(final @Nullable String [] charValues) {
		return new RCharacter32Store(charValues);
	}
	
	@Override
	public RCharacterStore createCharDataUtf8(final byte [] @Nullable [] charValues) {
		return new RCharacter32Utf8Store(charValues);
	}
	
	@Override
	public RCharacterStore createCharData(final long length) {
		return (length <= this.storeLengthFixLong) ?
				new RCharacter32Store((int)length) :
				new RCharacterFix64Store(length);
	}
	
	@Override
	public RRawStore createRawData(final byte [] rawValues) {
		return new RRaw32Store(rawValues);
	}
	
	@Override
	public RRawStore createRawData(final long length) {
		return (length <= this.storeLengthFixLong) ?
				new RRaw32Store((int)length) :
				new RRawFix64Store(length);
	}
	
	@Override
	public RFactorStore createUnorderedFactorData(final int [] codes, final @Nullable String [] levels) {
		return new RFactor32Store(codes, false, levels);
	}
	
	@Override
	public RFactorStore createUnorderedFactorData(final long length, final @Nullable String [] levels) {
		if (length > this.storeLengthFixLong) {
			throw new UnsupportedOperationException("long length");
		}
		return new RFactor32Store((int)length, false, levels);
	}
	@Deprecated
	public RFactorStore createFactorData(final int length, final @Nullable String [] levels) {
		return createUnorderedFactorData(length, levels);
	}
	
	@Override
	public RFactorStore createOrderedFactorData(final int [] codes, final @Nullable String [] levels) {
		return new RFactor32Store(codes, true, levels);
	}
	@Deprecated
	public RFactorStore createOrderedData(final int [] codes, final @Nullable String [] levels) {
		return createOrderedFactorData(codes, levels);
	}
	
	@Override
	public RFactorStore createOrderedFactorData(final long length, final @Nullable String [] levels) {
		if (length > this.storeLengthFixLong) {
			throw new UnsupportedOperationException("long length");
		}
		return new RFactor32Store((int)length, true, levels);
	}
	@Deprecated
	public RFactorStore createOrderedData(final int length, final @Nullable String [] levels) {
		return createOrderedFactorData(length, levels);
	}
	
	
	/*-- Streaming --*/
	
	@Override
	@SuppressWarnings("rawtypes")
	public RObject readObject(final RJIO io) throws IOException {
		final byte type= io.readByte();
		int options;
		switch (type) {
		case -1:
			return null;
		case RObject.TYPE_NULL:
			return RNullImpl.INSTANCE;
		case RObject.TYPE_VECTOR: {
			return new RVectorImpl(io, this); }
		case RObject.TYPE_ARRAY:
			return new RArrayImpl(io, this);
		case RObject.TYPE_LIST:
			options= io.readInt();
			return ((options & O_LENGTHGRADE_MASK) <= 3) ?
					new RList32Impl(io, this, options) :
					new RListFix64Impl(io, this, options);
		case RObject.TYPE_DATAFRAME:
			options= io.readInt();
			return ((options & O_LENGTHGRADE_MASK) <= 3) ?
					new RDataFrame32Impl(io, this, options) :
					new RListFix64Impl(io, this, options);
		case RObject.TYPE_ENVIRONMENT:
			return new REnvironmentImpl(io, this);
		case RObject.TYPE_LANGUAGE:
			return new RLanguageImpl(io, this);
		case RObject.TYPE_FUNCTION:
			return new RFunctionImpl(io, this);
		case RObject.TYPE_REFERENCE:
			return new RReferenceImpl(io, this);
		case RObject.TYPE_S4OBJECT:
			return new RS4ObjectImpl(io, this);
		case RObject.TYPE_OTHER:
			return new ROtherImpl(io, this);
		case RObject.TYPE_MISSING:
			return RMissingImpl.INSTANCE;
		case RObject.TYPE_PROMISE:
			if ((io.flags & F_WITH_DBG) != 0) {
				return new RPromiseImpl(io, this);
			}
			return RPromiseImpl.INSTANCE;
		default:
			throw new IOException("object type= " + type);
		}
	}
	
	@Override
	public void writeObject(final RObject robject, final RJIO io) throws IOException {
		if (robject == null) {
			io.writeByte(-1);
			return;
		}
		final byte type= robject.getRObjectType();
		io.writeByte(type);
		switch (type) {
		case RObject.TYPE_NULL:
		case RObject.TYPE_MISSING:
			return;
		case RObject.TYPE_VECTOR:
		case RObject.TYPE_ARRAY:
		case RObject.TYPE_LIST:
		case RObject.TYPE_DATAFRAME:
		case RObject.TYPE_ENVIRONMENT:
		case RObject.TYPE_LANGUAGE:
		case RObject.TYPE_FUNCTION:
		case RObject.TYPE_REFERENCE:
		case RObject.TYPE_S4OBJECT:
		case RObject.TYPE_OTHER:
		case RObject.TYPE_PROMISE:
			((ExternalizableRObject) robject).writeExternal(io, this);
			return;
		default:
			throw new IOException("object type= " + type);
		}
	}
	
	@Override
	public RStore<?> readStore(final RJIO io, final long length) throws IOException {
		if ((io.flags & F_ONLY_STRUCT) == 0) {
			final byte storeType= io.readByte();
			if (length <= Integer.MAX_VALUE) {
				switch (storeType) {
				case RStore.LOGICAL:
					return new RLogicalByte32Store(io, (int)length);
				case RStore.INTEGER:
					return new RInteger32Store(io, (int)length);
				case RStore.NUMERIC:
					return new RNumericB32Store(io, (int)length);
				case RStore.COMPLEX:
					return new RComplexB32Store(io, (int)length);
				case RStore.CHARACTER:
					return new RCharacter32Store(io, (int)length);
				case RStore.RAW:
					return new RRaw32Store(io, (int)length);
				case RStore.FACTOR:
					return new RFactor32Store(io, (int)length);
				default:
					throw new IOException("store type= " + storeType);
				}
			}
			else {
				switch (storeType) {
				case RStore.LOGICAL:
					return new RLogicalByteFix64Store(io, length);
				case RStore.INTEGER:
					return new RIntegerFix64Store(io, length);
				case RStore.NUMERIC:
					return new RNumericBFix64Store(io, length);
				case RStore.COMPLEX:
					return new RComplexBFix64Store(io, length);
				case RStore.CHARACTER:
					return new RCharacterFix64Store(io, length);
				case RStore.RAW:
					return new RRawFix64Store(io, length);
				case RStore.FACTOR:
					return new RFactorFix64Store(io, length);
				default:
					throw new IOException("store type= " + storeType);
				}
			}
		}
		else {
			final byte storeType= io.readByte();
			switch (storeType) {
			case RStore.LOGICAL:
				return LOGI_STRUCT_DUMMY;
			case RStore.INTEGER:
				return INT_STRUCT_DUMMY;
			case RStore.NUMERIC:
				return NUM_STRUCT_DUMMY;
			case RStore.COMPLEX:
				return CPLX_STRUCT_DUMMY;
			case RStore.CHARACTER:
				return CHR_STRUCT_DUMMY;
			case RStore.RAW:
				return RAW_STRUCT_DUMMY;
			case RStore.FACTOR:
				return new RFactorStructStore(io.readBoolean(), io.readInt());
			default:
				throw new IOException("store type= " + storeType);
			}
		}
	}
	
	@Override
	public void writeStore(final RStore<?> data, final RJIO io) throws IOException {
		if ((io.flags & F_ONLY_STRUCT) == 0) {
			io.writeByte(data.getStoreType());
			((ExternalizableRStore) data).writeExternal(io);
		}
		else {
			final byte storeType= data.getStoreType();
			io.writeByte(storeType);
			if (storeType == RStore.FACTOR) {
				final RFactorStore factor= (RFactorStore) data;
				io.writeBoolean(factor.isOrdered());
				io.writeInt(factor.getLevelCount());
			}
		}
	}
	
	@Override
	public RList readAttributeList(final RJIO io) throws IOException {
		return new RList32Impl(io, this, io.readInt());
	}
	
	@Override
	public void writeAttributeList(final RList list, final RJIO io) throws IOException {
		((ExternalizableRObject) list).writeExternal(io, this);
	}
	
	protected final int[] readDim(final ObjectInput in) throws IOException {
		final int length= in.readInt();
		final int[] dim= new int[length];
		for (int i= 0; i < length; i++) {
			dim[i]= in.readInt();
		}
		return dim;
	}
	
	@Override
	public @Nullable RStore<?> readNames(final RJIO io, final long length) throws IOException {
		final byte type= io.readByte();
		if (type == RStore.CHARACTER) {
			return (length <= Integer.MAX_VALUE) ?
					new RCharacter32Store(io, (int)length) :
					new RCharacterFix64Store(io, length);
		}
		if (type == 0) {
			return null;
		}
		throw new IOException();
	}
	
	@Override
	public void writeNames(final @Nullable RStore<?> names, final RJIO io) throws IOException {
		if (names != null) {
			final byte type= names.getStoreType();
			if (type == RStore.CHARACTER) {
				io.writeByte(type);
				((ExternalizableRStore) names).writeExternal(io);
				return;
			}
		}
		io.writeByte(0);
	}
	
}
