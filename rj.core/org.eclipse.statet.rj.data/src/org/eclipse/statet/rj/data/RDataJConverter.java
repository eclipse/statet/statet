/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;



/**
 * Converts R data objects to classic Java objects and the other way around.
 */
@NonNullByDefault
public class RDataJConverter {
	
	
	private boolean keepArray1;
	
	private RObjectFactory rObjectFactory;
	
	
	public RDataJConverter(final RObjectFactory rObjectFactory) {
		this.rObjectFactory= nonNullAssert(rObjectFactory);
	}
	
	public RDataJConverter() {
		this(DefaultRObjectFactory.INSTANCE);
	}
	
	
	public void setKeepArray1(final boolean enable) {
		this.keepArray1= enable;
	}
	
	public void setRObjectFactory(final RObjectFactory factory) {
		this.rObjectFactory= nonNullAssert(factory);
	}
	
	public RObjectFactory getRObjectFactory() {
		return this.rObjectFactory;
	}
	
	
	public @Nullable Object toJava(final RObject rObject) {
		switch (rObject.getRObjectType()) {
		case RObject.TYPE_VECTOR:
			return toJava(nonNullAssert(rObject.getData()));
		}
		return null;
	}
	
	public @Nullable Object toJava(final RStore<?> rData) {
		final Object[] array= rData.toArray();
		if (!this.keepArray1 && array != null && array.length == 1) {
			return array[0];
		}
		return array;
	}
	
	
	
	public @Nullable RObject toRJ(final @Nullable Object javaObj) {
		if (javaObj instanceof Boolean) {
			return this.rObjectFactory.createVector(
					this.rObjectFactory.createLogiData(new boolean[] {
							((Boolean)javaObj).booleanValue() }));
		}
		if (javaObj instanceof String) {
			return this.rObjectFactory.createVector(
					this.rObjectFactory.createCharData(new @Nullable String [] {
							(String)javaObj }));
		}
		if (javaObj instanceof String[]) {
			return this.rObjectFactory.createVector(
					this.rObjectFactory.createCharData((@Nullable String [])javaObj) );
		}
		if (javaObj instanceof Map) {
			final Map<?, ?> map= (Map<?, ?>)javaObj;
			final var names= new @Nullable String[map.size()];
			final var components= new RObject[map.size()];
			int i= 0;
			for (final Entry<?, ?> entry : map.entrySet()) {
				names[i]= (String) entry.getKey();
				components[i]= toRJ(entry.getValue());
				i++;
			}
			return this.rObjectFactory.createList(components, names);
		}
		return null;
	}
	
}
