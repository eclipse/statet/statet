/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;


/**
 * This implementation is limited to length of 2<sup>31</sup>-1.
 */
@NonNullByDefault
public class RCharacter32Utf8Store extends AbstractCharacterStore
		implements ExternalizableRStore, Externalizable {
	
	
	private int length;
	
	private byte [] @Nullable [] charValues;
	
	
	@SuppressWarnings("null")
	public RCharacter32Utf8Store() {
		this.length= 0;
		this.charValues= EMPTY_BYTE_2dARRAY;
	}
	
	public RCharacter32Utf8Store(final int length, final byte @Nullable [] initialValue) {
		this.length= length;
		this.charValues= new byte [length] @Nullable [];
		if (initialValue != null) {
			Arrays.fill(this.charValues, initialValue);
		}
	}
	
	public RCharacter32Utf8Store(final int length, final @Nullable String initialValue) {
		this(length, (initialValue != null) ? initialValue.getBytes() : null);
	}
	
	public RCharacter32Utf8Store(final int length) {
		this(length, EMPTY_BYTE_ARRAY);
	}
	
	protected RCharacter32Utf8Store(final byte [] @Nullable [] charValues) {
		this.length= charValues.length;
		this.charValues= charValues;
	}
	
	public RCharacter32Utf8Store(final @Nullable String [] values,
			final int startIndex, final int endIndex) {
		final int length= endIndex - startIndex;
		final var utf8Values= new byte [length] @Nullable [];
		for (int i= 0; i < length; i++) {
			final String s= values[i];
			if (s != null) {
				utf8Values[i]= s.getBytes(StandardCharsets.UTF_8);
			}
		}
		this.length= length;
		this.charValues= utf8Values;
	}
	
	public RCharacter32Utf8Store(final @Nullable String [] values) {
		this(values, 0, values.length);
	}
	
	public RCharacter32Utf8Store(final byte [] @Nullable [] values,
			final int startIndex, final int endIndex) {
		this.length= endIndex - startIndex;
		this.charValues= nonNullAssert(values);
	}
	
	
	public RCharacter32Utf8Store(final RJIO io, final int length) throws IOException {
		this.length= length;
		this.charValues= new byte [length] @Nullable [];
		io.readStringDataUtf8(this.charValues, length);
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
		io.writeStringDataUtf8(this.charValues, this.length);
	}
	
	@Override
	public void readExternal(final ObjectInput in) throws IOException {
		this.length= in.readInt();
		this.charValues= new byte [this.length] @Nullable [];
		for (int i= 0; i < this.length; i++) {
			final int l= in.readInt();
			if (l >= 0) {
				final char[] c= new char[l];
				for (int j= 0; j < l; j++) {
					c[j]= in.readChar();
				}
				this.charValues[i]= new String(c).getBytes(StandardCharsets.UTF_8);
			}
//			else {
//				this.charValues[i]= null;
//			}
		}
	}
	
	@Override
	public void writeExternal(final ObjectOutput out) throws IOException {
		out.writeInt(this.length);
		for (int i= 0; i < this.length; i++) {
			final byte[] value= this.charValues[i];
			if (value != null) {
				final String s= new String(value, StandardCharsets.UTF_8);
				out.writeInt(s.length());
				out.writeChars(s);
			}
			else {
				out.writeInt(-1);
			}
		}
	}
	
	
	protected final byte[] @Nullable [] getCharValuesUtf8Array() {
		return this.charValues;
	}
	
	
	@Override
	protected final boolean isStructOnly() {
		return false;
	}
	
	
	public final int length() {
		return this.length;
	}
	
	@Override
	public final long getLength() {
		return this.length;
	}
	
	
	@Override
	public boolean isNA(final int idx) {
		return (this.charValues[idx] == null);
	}
	
	@Override
	public boolean isNA(final long idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		return (this.charValues[(int)idx] == null);
	}
	
	@Override
	public void setNA(final int idx) {
//		if (this.charValues[idx] == null) {
//			return;
//		}
		this.charValues[idx]= null;
	}
	
	@Override
	public void setNA(final long idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
//		if (this.charValues[(int)idx] == null) {
//			return;
//		}
		this.charValues[(int)idx]= null;
	}
	
	@Override
	public boolean isMissing(final int idx) {
		return (this.charValues[idx] == null);
	}
	
	@Override
	public boolean isMissing(final long idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		return (this.charValues[(int)idx] == null);
	}
	
	
	@Override
	@SuppressWarnings("null")
	public String getChar(final int idx) {
		return new String(this.charValues[idx], StandardCharsets.UTF_8);
	}
	
	@Override
	@SuppressWarnings("null")
	public String getChar(final long idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		return new String(this.charValues[(int)idx], StandardCharsets.UTF_8);
	}
	
	@Override
	public void setChar(final int idx, final String value) {
//		assert (value != null);
		this.charValues[idx]= value.getBytes(StandardCharsets.UTF_8);
	}
	
	@Override
	public void setChar(final long idx, final String value) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
//		assert (value != null);
		this.charValues[(int)idx]= value.getBytes(StandardCharsets.UTF_8);
	}
	
	
//	private void prepareInsert(final int[] idxs) {
//		this.charValues= prepareInsert(this.charValues, this.length, idxs);
//		this.length+= idxs.length;
//	}
//	
//	public void insertChar(final int idx, final String value) {
////		assert (value != null);
//		prepareInsert(new int[] { idx });
//		this.charValues[idx]= value.getBytes(StandardCharsets.UTF_8);
//	}
//	
//	@Override
//	public void insertNA(final int idx) {
//		prepareInsert(new int[] { idx });
//		this.charValues[idx]= null;
//	}
//	
//	@Override
//	public void insertNA(final int[] idxs) {
//		if (idxs.length == 0) {
//			return;
//		}
//		prepareInsert(idxs);
//		for (int i= 0; i < idxs.length; i++) {
//			this.charValues[idxs[i]]= null;
//		}
//	}
//	
//	@Override
//	public void remove(final int idx) {
//		this.charValues= remove(this.charValues, this.length, new int[] { idx });
//		this.length--;
//	}
//	
//	@Override
//	public void remove(final int[] idxs) {
//		this.charValues= remove(this.charValues, this.length, idxs);
//		this.length-= idxs.length;
//	}
	
	
	@Override
	public @Nullable String get(final int idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		final byte[] value= this.charValues[idx];
		return (value != null) ? new String(value, StandardCharsets.UTF_8) : null;
	}
	
	@Override
	public @Nullable String get(final long idx) {
		if (idx < 0 || idx >= this.length) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		final byte[] value= this.charValues[(int)idx];
		return (value != null) ? new String(value, StandardCharsets.UTF_8) : null;
	}
	
	@Override
	public @Nullable String [] toArray() {
		final int length= this.length;
		final var values= this.charValues;
		final var array= new @Nullable String [length];
		for (int i= 0; i < length; i++) {
			final byte[] value= values[i];
			if (value != null) {
				array[i]= new String(value, StandardCharsets.UTF_8);
			}
		}
		return array;
	}
	
	
	@Override
	public long indexOfNA(long fromIdx) {
		if (fromIdx >= Integer.MAX_VALUE) {
			return -1;
		}
		if (fromIdx < 0) {
			fromIdx= 0;
		}
		final int l= this.length;
		final var values= this.charValues;
		for (int i= (int)fromIdx; i < l; i++) {
			if (values[i] == null) {
				return i;
			}
		}
		return -1;
	}
	
	public int indexOfNA(int fromIdx) {
		if (fromIdx < 0) {
			fromIdx= 0;
		}
		final int l= this.length;
		final var values= this.charValues;
		for (int i= fromIdx; i < l; i++) {
			if (values[i] == null) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public long indexOf(final String character, long fromIdx) {
		final int l= this.length;
		if (fromIdx >= l) {
			return -1;
		}
		if (fromIdx < 0) {
			fromIdx= 0;
		}
		final byte[] bytes= character.getBytes(StandardCharsets.UTF_8);
		final var values= this.charValues;
		for (int i= (int)fromIdx; i < l; i++) {
			if (Arrays.equals(values[i], bytes)) {
				return i;
			}
		}
		return -1;
	}
	
	public int indexOf(final String character, int fromIdx) {
		final int l= this.length;
		if (fromIdx >= l) {
			return -1;
		}
		if (fromIdx < 0) {
			fromIdx= 0;
		}
		final byte[] bytes= character.getBytes(StandardCharsets.UTF_8);
		final var values= this.charValues;
		while (fromIdx < l) {
			if (Arrays.equals(values[fromIdx], bytes)) {
				return fromIdx;
			}
			fromIdx++;
		}
		return -1;
	}
	
}
