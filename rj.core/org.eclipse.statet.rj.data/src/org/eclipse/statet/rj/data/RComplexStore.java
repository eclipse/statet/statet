/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RComplexStore.Complex;


/**
 * Interface for R data stores of type {@link RStore#COMPLEX}.
 * <p>
 * An R data store implements this interface if the R function
 * <code>typeof(object)</code> returns 'complex'.</p>
 */
@NonNullByDefault
public interface RComplexStore extends RStore<Complex> {
	
	
	final class Complex {
		
		
		private final double realValue;
		private final double imaginaryValue;
		
		
		public Complex(final double real, final double imaginary) {
			this.realValue= real;
			this.imaginaryValue= imaginary;
		}
		
		
		public double getRe() {
			return this.realValue;
		}
		
		public double getIm() {
			return this.imaginaryValue;
		}
		
		
		@Override
		public int hashCode() {
			final long realBits= Double.doubleToLongBits(this.realValue);
			final long imaginaryBits= Double.doubleToLongBits(this.imaginaryValue) + 1;
			return (int)((realBits ^ (realBits >>> 32)) | (imaginaryBits ^ (imaginaryBits >>> 32)));
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			if (!(obj instanceof Complex)) {
				return false;
			}
			final Complex other= (Complex) obj;
			
			return (Double.doubleToLongBits(this.realValue) == Double.doubleToLongBits(other.realValue)
					&& Double.doubleToLongBits(this.imaginaryValue) == Double.doubleToLongBits(other.imaginaryValue) );
		}
		
		@Override
		public String toString() {
			final StringBuilder sb= new StringBuilder(32);
			sb.append(Double.toString(this.realValue));
			if (this.imaginaryValue >= 0.0) {
				sb.append('+');
			}
			sb.append(Double.toString(this.imaginaryValue));
			sb.append('i');
			return sb.toString();
		}
		
	}
	
	
	boolean isNaN(int idx);
	boolean isNaN(long idx);
	
	
	@Override
	String getChar(final int idx);
	@Override
	String getChar(final long idx);
	
	
	@Override
	@Nullable Complex get(int idx);
	@Override
	@Nullable Complex get(long idx);
	
	@Override
	@Nullable Complex [] toArray();
	
}
