/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * An R object is of the type {@link RObject#TYPE_ENVIRONMENT environment}, if the
 * object is an R environment.  Such an R environment object is represented
 * by an instance of this interface.
 * <p>
 * The R function <code>typeof(object)</code> returns 'environment' for objects
 * of this type.</p>
 * <p>
 * Even the interface extends {@link RList}, the objects are not a list in R!
 * The inheritance is only for a uniform API.</p>
 */
@NonNullByDefault
public interface REnvironment extends RList {
	
	/**
	 * Environment name of the global environment
	 * (<code>.GlobalEnv</code>)
	 * 
	 * Name: {@value}
	 */
	String ENVNAME_GLOBAL= "R_GlobalEnv";
	
	/**
	 * Environment name of the empty environment
	 * (<code>emptyenv()</code>)
	 * 
	 * Name: {@value}
	 */
	String ENVNAME_EMPTY= "R_EmptyEnv";
	
	/**
	 * Environment name of the base environment
	 * (<code>baseenv()</code>)
	 * 
	 * Name: {@value}
	 */
	String ENVNAME_BASE= "base";
	
	/**
	 * Environment name of the Autoloads environment
	 * (<code>.AutoloadEnv</code>)
	 * 
	 * Name: {@value}
	 */
	String ENVNAME_AUTOLOADS= "Autoloads";
	
	
	byte ENVTYPE_BASE=                                      0x01;
	byte ENVTYPE_AUTOLOADS=                                 0x02;
	byte ENVTYPE_PACKAGE=                                   0x05;
	byte ENVTYPE_GLOBAL=                                    0x07;
	
	byte ENVTYPE_EMTPY=                                     0x09;
	
	/**
	 * @since de.walware.rj.data 2.1
	 */
	byte ENVTYPE_NAMESPACE=                                 0x0B;
	/**
	 * @since de.walware.rj.data 2.1
	 */
	byte ENVTYPE_NAMESPACE_EXPORTS=                         0x0C;
	
	
	/**
	 * Returns the length of the object. The length of an {@link RObject#TYPE_ENVIRONMENT environment}
	 * is the count the objects in the environment.
	 * 
	 * At moment, the length of an {@link RObject#TYPE_ENVIRONMENT environment} is always &le; 2<sup>31</sup>-1
	 * (representable by Java int).
	 * 
	 * @return the length
	 */
	@Override
	long getLength();
	
	/**
	 * Indicates a special environment type (> 0)
	 * see <code>ENVTYPE_</code> constants defined in {@link REnvironment}.
	 * 
	 * @return the type constant or &lt;= 0
	 */
	int getSpecialType();
	
	/**
	 * Returns the environment name of the environment.  This is the return value
	 * of the R command <code>environmentName(object)</code>.
	 * 
	 * @return the environment name
	 */
	String getEnvironmentName();
	
	long getHandle();
	
	
	@Override
	RCharacterStore getNames();
	
	@Override
	String getName(long idx);
	
	@Override
	String getName(int idx);
	
	
}
