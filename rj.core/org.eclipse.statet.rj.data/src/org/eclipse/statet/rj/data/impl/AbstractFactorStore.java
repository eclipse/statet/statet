/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RFactorStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public abstract class AbstractFactorStore extends AbstractRStore<Integer>
		implements RFactorStore {
	
	
	protected boolean isOrdered;
	
	
	@Override
	public final byte getStoreType() {
		return RStore.FACTOR;
	}
	
	@Override
	public final String getBaseVectorRClassName() {
		return (this.isOrdered) ? RObject.CLASSNAME_ORDERED : RObject.CLASSNAME_FACTOR;
	}
	
	@Override
	public final boolean isOrdered() {
		return this.isOrdered;
	}
	
	
	@Override
	public final boolean getLogi(final long idx) {
		return (getInt(idx) != 0);
	}
	
	@Override
	public final boolean getLogi(final int idx) {
		return (getInt(idx) != 0);
	}
	
	@Override
	public final void setLogi(final long idx, final boolean logi) {
		setInt(idx, logi ? 1 : 0);
	}
	
	@Override
	public final void setLogi(final int idx, final boolean logi) {
		setInt(idx, logi ? 1 : 0);
	}
	
	@Override
	public abstract int getInt(long idx);
	
	@Override
	public abstract int getInt(int idx);
	
	@Override
	public final double getNum(final long idx) {
		return getInt(idx);
	}
	
	@Override
	public final double getNum(final int idx) {
		return getInt(idx);
	}
	
	@Override
	public final void setNum(final long idx, final double real) {
		setInt(idx, (int)real);
	}
	
	@Override
	public final void setNum(final int idx, final double real) {
		setInt(idx, (int)real);
	}
	
	@Override
	@SuppressWarnings("null")
	public abstract @Nullable String getChar(long idx);
	
	@Override
	@SuppressWarnings("null")
	public abstract @Nullable String getChar(int idx);
	
	@Override
	public byte getRaw(final long idx) {
		return AbstractIntegerStore.toRaw(getInt(idx));
	}
	
	@Override
	public byte getRaw(final int idx) {
		return AbstractIntegerStore.toRaw(getInt(idx));
	}
	
	
	@Override
	public abstract @Nullable Integer get(long idx);
	
	@Override
	public abstract @Nullable Integer get(int idx);
	
	@Override
	public abstract @Nullable Integer [] toArray();
	
	
}
