/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


/**
 * Code generation for AdbSymbol
 * 
 * http://unicode.org/Public/MAPPINGS/VENDORS/ADOBE/symbol.txt
 */
class CodeGenAdbSymbol {
	
	
	public static void main(final String[] args) throws Exception {
		final File file= new File("target/classes/" + CodeGenAdbSymbol.class.getPackage().getName().replace('.', '/') + "/AdbSymbol.txt");
		final BufferedReader reader= new BufferedReader(new FileReader(file));
		String line;
		while ((line= reader.readLine()) != null) {
			if (line.isEmpty() || line.startsWith("#")) {
				continue;
			}
			final String[] columns= line.split("\t");
			if (columns.length != 4) {
				throw new IOException(line);
			}
			
			System.out.print("MAPPING[0x");
			System.out.print(columns[0]);
			System.out.print("]= 0x");
			System.out.print(columns[1]);
			System.out.print("; // ");
			System.out.print(columns[2].substring(2));
			
			final int codepoint= Integer.parseInt(columns[0], 16);
			if (new String(new int[] { codepoint }, 0, 1).length() != 1) {
				throw new IOException("Warning: multichar codepoint");
			}
			
			System.out.println();
		}
	}
	
}
