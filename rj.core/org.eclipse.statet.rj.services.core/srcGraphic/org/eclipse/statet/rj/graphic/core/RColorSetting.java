/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Configures paint color.
 */
@NonNullByDefault
public class RColorSetting extends RPaintSetting {
	
	
	public final int color;
	
	
	/**
	 * Creates a new color setting.
	 * 
	 * @param color color
	 */
	public RColorSetting(final int color) {
		this.color= color;
	}
	
	
	@Override
	public final byte getInstructionType() {
		return SET_COLOR;
	}
	
	
	public final int getAlpha() {
		return ((this.color >> 24) & 255);
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("ColorSetting[(");
		sb.append(this.color & 255);
		sb.append(", ");
		sb.append((this.color >> 8) & 255);
		sb.append(", ");
		sb.append((this.color >> 16) & 255);
		sb.append("), ");
		sb.append((this.color >> 24) & 255);
		sb.append("]");
		return sb.toString();
	}
	
}
