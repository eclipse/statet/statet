/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Text at position <code>(x, y)</code>, horizontal adjusted by <code>hAdj</code>
 * [0, 1] and rotated by <code>rDeg</code>.
 */
@NonNullByDefault
public class RText extends RGraphicElement {
	
	/**
	 * The text to draw.
	 */
	public final String text;
	/**
	 * The position where to draw the text.
	 */
	public final double x, y;
	/**
	 * The factor for the horizontal adjustment of the text.
	 */
	public final double horizontalAdjust;
	/**
	 * The degrees for rotation of the text.
	 */
	public final double rotateDegree;
	
	
	/**
	 * Creates a new text element.
	 * 
	 * @param x {@link #x}
	 * @param y {@link #y}
	 * @param hAdj {@link #horizontalAdjust}
	 * @param rDeg {@link #rotateDegree}
	 * @param text {@link #text}
	 */
	public RText(final String text,
			final double x, final double y, final double rDeg, final double hAdj) {
		this.text= text;
		this.x= x;
		this.y= y;
		this.horizontalAdjust= hAdj;
		this.rotateDegree= rDeg;
	}
	
	
	@Override
	public final byte getInstructionType() {
		return DRAW_TEXT;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(50 + this.text.length());
		sb.append("Text[(");
		sb.append(this.x);
		sb.append(",");
		sb.append(this.y);
		sb.append("), ");
		sb.append(this.horizontalAdjust);
		sb.append(", ");
		sb.append(this.rotateDegree);
		sb.append(", \"");
		sb.append(this.text);
		sb.append("\"]");
		return sb.toString();
	}
	
}
