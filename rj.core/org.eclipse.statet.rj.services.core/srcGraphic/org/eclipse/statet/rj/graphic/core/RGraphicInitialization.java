/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * First instruction of an R graphic.
 */
@NonNullByDefault
public class RGraphicInitialization implements RGraphicInstruction {
	
	
	/**
	 * The width the graphic is created for in R
	 */
	public final double width;
	
	/**
	 * The height the graphic is created for in R
	 */
	public final double height;
	
	/**
	 * The color of the device background
	 */
	public final int canvasColor;
	
	
	public RGraphicInitialization(final double width, final double height, final int canvasColor) {
		this.width= width;
		this.height= height;
		this.canvasColor= canvasColor;
	}
	
	
	@Override
	public final byte getInstructionType() {
		return INIT;
	}
	
}
