/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Circle with center <code>(x, y)</code> and radius <code>r</code>.
 */
@NonNullByDefault
public class RCircle extends RGraphicElement {
	
	
	public final double x;
	public final double y;
	public final double r;
	
	
	/**
	 * Creates a new circle.
	 * 
	 * @param x x coordinate of the center
	 * @param y y coordinate of the center
	 * @param r radius
	 */
	public RCircle(final double x, final double y, final double r) {
		this.x= x;
		this.y= y;
		this.r= r;
	}
	
	
	@Override
	public final byte getInstructionType() {
		return DRAW_CIRCLE;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(40);
		sb.append("Circle[(");
		sb.append(this.x);
		sb.append(",");
		sb.append(this.y);
		sb.append("), ");
		sb.append(this.r);
		sb.append("]");
		return sb.toString();
	}
	
}
