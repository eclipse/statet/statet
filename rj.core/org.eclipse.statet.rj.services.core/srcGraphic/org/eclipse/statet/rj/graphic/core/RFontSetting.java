/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Configures the text font.
 */
@NonNullByDefault
public class RFontSetting extends RPaintSetting {
	
	/**
	 * Font family name, ~R: <code>par(familiy)</code>.
	 */
	public final String family;
	
	/**
	 * Font face constant, ~R: <code>par(font)</code>.
	 * <p>
	 * Values:
	 *     1= default,
	 *     2= bold,
	 *     3= italic,
	 *     4= bold and italic,
	 *     5= symbol.</p>
	 */
	public final int face;
	
	/**
	 * Font size in points, ~R: ps * cex.
	 */
	public final float pointSize;
	
	/**
	 * The line height factor, ~R: <code>par(lheight)</code>
	 */
	public final double lineHeight;
	
	
	/**
	 * Creates a new font setting.
	 * 
	 * @param family font family name
	 * @param type face font face constant
	 * @param pointSize font size in points
	 * @param lineHeight line height factor
	 */
	public RFontSetting(final String family, final int face, final float pointSize,
			final double lineHeight) {
		this.family= family;
		this.face= face;
		this.pointSize= pointSize;
		this.lineHeight= lineHeight;
	}
	
	
	@Override
	public final byte getInstructionType() {
		return SET_FONT;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(40 + this.family.length());
		sb.append("FontSetting[");
		sb.append(this.family);
		sb.append(", ");
		sb.append(this.face);
		sb.append(", ");
		sb.append(this.pointSize);
		sb.append(", ");
		sb.append(this.lineHeight);
		sb.append("]");
		return sb.toString();
	}
	
}
