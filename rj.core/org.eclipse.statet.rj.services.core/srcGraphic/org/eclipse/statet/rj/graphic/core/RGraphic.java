/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * An RGraphic represents the client side of a graphic (device)
 * in R.
 * <p>
 * The attributes devId ({@link #getDevId()}) and active ({@link #isActive()},
 * {@link Listener#activated()}, {@link Listener#deactivated()}) are equivalent
 * to the device id and active device in R.</p>
 * <p>
 * A graphic has a sequence of graphic instructions describing how to paint the
 * graphic ({@link #getInstructions()}). If not empty, the first instruction is
 * of the type {@link RGraphicInstruction#INIT INIT}, followed by setting and
 * drawing instructions. Settings are valid for all following drawing instructions
 * until it is changed by a new setting instruction of the same type.</p>
 */
@NonNullByDefault
public interface RGraphic {
	
	
	interface Listener {
		
		void activated();
		void deactivated();
		void drawingStarted();
		void drawingStopped();
		
	}
	
	/**
	 * Returns the device id of the graphic in R. Note that the device number presented in public
	 * R functions is this device id + 1.
	 * 
	 * @return the device id
	 */
	int getDevId();
	
	/**
	 * Returns if graphic device of this graphic is the active device in R.
	 * 
	 * @return <code>true</code> if it is active, otherwise <code>false</code>
	 */
	boolean isActive();
	
	List<? extends RGraphicInstruction> getInstructions();
	
	void addListener(Listener listener);
	
	void removeListener(Listener listener);
	
}
