/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Polygon with vertices <code>(x[1], y[1])</code>, ..., <code>(x[n-1], y[n-1])</code>.
 */
@NonNullByDefault
public class RPolygon extends RGraphicElement {
	
	
	/**
	 * Coordinates of the vertices.
	 */
	public final double[] x, y;
	
	
	/**
	 * Creates a new polygon
	 * 
	 * @param x {@link #x}
	 * @param y {@link #y}
	 */
	public RPolygon(final double[] x, final double[] y) {
		this.x= x;
		this.y= y;
	}
	
	
	@Override
	public final byte getInstructionType() {
		return DRAW_POLYGON;
	}
	
	
	@Override
	public String toString() {
		final int n= this.x.length;
		if (n == 0) {
			return "Polygon[]";
		}
		final StringBuilder sb= new StringBuilder(14 + this.x.length*20);
		sb.append("Polygon[(");
		sb.append(this.x[0]);
		sb.append(",");
		sb.append(this.y[0]);
		for (int i= 1; i < n; i++) {
			sb.append("), (");
			sb.append(this.x[i]);
			sb.append(",");
			sb.append(this.y[i]);
		}
		sb.append(")]");
		return sb.toString();
	}
	
}
