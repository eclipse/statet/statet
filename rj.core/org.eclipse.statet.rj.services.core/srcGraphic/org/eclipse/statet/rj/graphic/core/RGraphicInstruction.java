/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Instruction to paint a R graphic.
 * 
 * An instruction has one of the instruction type defined in this interface.
 * 
 * @see RGraphic
 */
@NonNullByDefault
public interface RGraphicInstruction {
	
	
	byte INIT=                          0;
	
	byte SET_CLIP=                      1;
	byte SET_COLOR=                     2;
	byte SET_FILL=                      3;
	byte SET_LINE=                      4;
	byte SET_FONT=                      5;
	
	byte DRAW_LINE=                     6;
	byte DRAW_RECTANGLE=                7;
	byte DRAW_POLYLINE=                 8;
	byte DRAW_POLYGON=                  9;
	byte DRAW_CIRCLE=                  10;
	byte DRAW_TEXT=                    11;
	byte DRAW_RASTER=                  12;
	byte DRAW_PATH=                    13;
	
	
	/**
	 * Gets the type of this instruction.
	 * 
	 * @return the instruction type
	 * @see RGraphicInstruction
	 */
	byte getInstructionType();
	
}
