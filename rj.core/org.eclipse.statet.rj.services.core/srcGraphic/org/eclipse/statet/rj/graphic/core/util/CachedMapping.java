/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core.util;


public final class CachedMapping implements CharMapping {
	
	
	private final CharMapping mapping;
	
	private String lastString;
	private String lastEncoded;
	
	
	public CachedMapping(final CharMapping mapping) {
		this.mapping= mapping;
	}
	
	
	@Override
	public int encode(final int codepoint) {
		return this.mapping.encode(codepoint);
	}
	
	@Override
	public String encode(final String s) {
		if (s.equals(this.lastString)) {
			return this.lastEncoded;
		}
		return (this.lastEncoded= this.mapping.encode(s));
	}
	
}
