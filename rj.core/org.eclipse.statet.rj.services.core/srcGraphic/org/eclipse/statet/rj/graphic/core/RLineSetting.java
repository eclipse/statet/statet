/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.graphic.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Configures line width and type.
 */
@NonNullByDefault
public class RLineSetting extends RPaintSetting {
	
	
	public static final int TYPE_BLANK= -1;
	public static final int TYPE_SOLID= 0;
	
	public static final byte CAP_ROUND= 1;
	public static final byte CAP_BUTT= 2;
	public static final byte CAP_SQUARE= 3;
	
	public static final byte JOIN_ROUND= 1;
	public static final byte JOIN_MITER= 2;
	public static final byte JOIN_BEVEL= 3;
	
	
	/**
	 * Line type, ~R: <code>par(lty)</code>
	 * <p>
	 * Values (not the predefined R constants, see line style encoding):
	 *     -1= blank,
	 *     0= solid (default),
	 *     encoded style
	 */
	public final int type;
	
	public final float width;
	
	public final byte cap;
	
	public final byte join;
	public final float joinMiterLimit;
	
	
	/**
	 * Creates a new line setting.
	 * 
	 * @param type line type
	 * @param width line width
	 */
	public RLineSetting(final int type, final float width,
			final byte cap, final byte join, final float joinMiterLimit) {
		this.type= type;
		this.width= width;
		this.cap= cap;
		this.join= join;
		this.joinMiterLimit= joinMiterLimit;
	}
	
	
	@Override
	public final byte getInstructionType() {
		return SET_LINE;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(30);
		sb.append("LineSetting[");
		sb.append(this.type);
		sb.append(", ");
		sb.append(this.width);
		sb.append(", ");
		sb.append(this.cap);
		sb.append(", ");
		sb.append(this.join);
		sb.append(", ");
		sb.append(this.joinMiterLimit);
		sb.append("]");
		return sb.toString();
	}
	
}
