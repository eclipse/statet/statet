/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RObject;


/**
 * Fully qualified reference to an R object.
 * 
 * @since 2.1
 */
@NonNullByDefault
public interface FQRObjectRef<THandle> {
	
	
	/**
	 * Handle to the R instance.
	 * 
	 * @return handle to R.
	 */
	THandle getRHandle();
	
	/**
	 * The environment in R, specified by a call or reference.
	 * 
	 * @return a reference to the environment
	 */
	RObject getEnv();
	
	/**
	 * The name, relative to the environment, specified by a symbol or call.
	 * 
	 * @return the name
	 */
	RObject getName();
	
}
