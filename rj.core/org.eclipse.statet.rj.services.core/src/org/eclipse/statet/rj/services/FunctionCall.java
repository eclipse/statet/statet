/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;


/**
 * A function call provides a comfortable way to build and execute
 * R function call.
 * 
 * <p>A function call builder for a given function can be created by
 * {@link RService#createFunctionCall(String)}.</p>
 * 
 * <p>The builder mainly provides methods to add arguments and to
 * finally evaluate the resulting function call in R.</p>
 * 
 * <p>Arguments are added by the <code>#add...()</code> methods. The
 * order they are added is exactly the order they are send to R. There
 * are add methods for a symbol referring to data existing in R,
 * for given R data objects and for single data values which are
 * transformed automatically from java primitives into R data objects.
 * All methods are available in a variant with and without a parameter
 * for the R argument name. An unnamed argument can be specified by
 * using the variant without the argument name parameter or by specifying
 * the name as <code>null</code>.</p>
 * 
 * <p>The common guidelines in {@link RService} (like concurrency) are effective
 * for all evaluation methods in this interface.</p>
 */
@NonNullByDefault
public interface FunctionCall {
	
	
	/**
	 * Adds an argument with the given R expression as value.
	 * 
	 * <p>The expression is parsed and lazily evaluated as known from function calls in R source
	 * code.</p>
	 * 
	 * @param arg the name of the argument or <code>null</code> for unnamed
	 * @param expression a single valid expression
	 * @return a reference to this object
	 * 
	 * @see #addChar(String, String) to add a string/character value directly
	 */
	FunctionCall add(final @Nullable String arg, final String expression);
	
	/**
	 * Adds a unnamed argument with the given R expression as value.
	 * 
	 * <p>The expression is parsed and lazily evaluated as known from function calls in R source
	 * code.</p>
	 * 
	 * @param expression a single valid expression
	 * @return a reference to this object
	 * 
	 * @see #addChar(String) to add a string/character value directly
	 */
	FunctionCall add(final String expression);
	
	/**
	 * Adds a argument with the given R data object as value.
	 * 
	 * @param arg the name of the argument or <code>null</code> for unnamed
	 * @param data an R data object
	 * @return a reference to this object
	 */
	FunctionCall add(final @Nullable String arg, final RObject data);
	
	/**
	 * Adds an unnamed argument with the given R data object as value.
	 * 
	 * @param arg the name of the argument
	 * @param data an R data object
	 * @return a reference to this object
	 */
	FunctionCall add(final RObject data);
	
	
	/**
	 * Adds an argument with the given boolean/logical as value.
	 * 
	 * <p>The Java boolean value is transformed into an R data object
	 * of type {@link RStore#LOGICAL logical}.</p>
	 * 
	 * @param arg the name of the argument or <code>null</code> for unnamed
	 * @param logical the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addLogi(final @Nullable String arg, final boolean logical);
	
	/**
	 * Adds an unnamed argument with the given boolean/logical as value.
	 * 
	 * <p>The Java boolean value is transformed into an R data object
	 * of type {@link RStore#LOGICAL logical}.</p>
	 * 
	 * @param logical the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addLogi(final boolean logical);
	
	/**
	 * Adds an argument with the given integer as value.
	 * 
	 * <p>The Java integer value is transformed into an R data object
	 * of type {@link RStore#INTEGER integer}.</p>
	 * 
	 * @param arg the name of the argument or <code>null</code> for unnamed
	 * @param integer the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addInt(final @Nullable String arg, final int integer);
	
	/**
	 * Adds an unnamed argument with the given integer as value.
	 * 
	 * <p>The Java integer value is transformed into an R data object
	 * of type {@link RStore#INTEGER integer}.</p>
	 * 
	 * @param integer the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addInt(final int integer);
	
	/**
	 * Adds an argument with the given double/numeric as value.
	 * 
	 * <p>The Java double value is transformed into an R data object
	 * of type {@link RStore#NUMERIC numeric}, also called real.</p>
	 * 
	 * @param arg the name of the argument or <code>null</code> for unnamed
	 * @param numeric the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addNum(final @Nullable String arg, final double numeric);
	
	/**
	 * Adds an unnamed argument with the given double/numeric as value.
	 * 
	 * <p>The Java double value is transformed into an R data object
	 * of type {@link RStore#NUMERIC numeric}, also called real.</p>
	 * 
	 * @param arg the name of the argument
	 * @param numeric the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addNum(final double numeric);
	
	/**
	 * Adds an argument with the given string/character as value.
	 * 
	 * <p>The Java String value is transformed into an R data object
	 * of type {@link RStore#CHARACTER character}.</p>
	 * 
	 * @param arg the name of the argument or <code>null</code> for unnamed
	 * @param character the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addCplx(final @Nullable String arg, final double real, final double imaginary);
	
	/**
	 * Adds an unnamed argument with the given string/character as value.
	 * 
	 * <p>The Java String value is transformed into an R data object
	 * of type {@link RStore#CHARACTER character}.</p>
	 * 
	 * @param character the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addCplx(final double real, final double imaginary);
	
	/**
	 * Adds an argument with the given string/character as value.
	 * 
	 * <p>The Java String value is transformed into an R data object
	 * of type {@link RStore#CHARACTER character}.</p>
	 * 
	 * @param arg the name of the argument or <code>null</code> for unnamed
	 * @param character the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addChar(final @Nullable String arg, final String character);
	
	/**
	 * Adds an unnamed argument with the given string/character as value.
	 * 
	 * <p>The Java String value is transformed into an R data object
	 * of type {@link RStore#CHARACTER character}.</p>
	 * 
	 * @param character the value of the argument
	 * @return a reference to this object
	 */
	FunctionCall addChar(final String character);
	
	/**
	 * Adds an argument with the given NULL as value.
	 * 
	 * <p>The Java String value is transformed into an R data
	 * {@link RObject#NULL NULL} object.</p>
	 * 
	 * @param arg the name of the argument or <code>null</code> for unnamed
	 * @return a reference to this object
	 */
	FunctionCall addNull(final @Nullable String arg);
	
	/**
	 * Adds an unnamed argument with the given NULL as value.
	 * 
	 * <p>The Java String value is transformed into an R data
	 * {@link RObject#NULL NULL} object.</p>
	 * 
	 * @return a reference to this object
	 */
	FunctionCall addNull();
	
	
	/**
	 * Performs the evaluation of this function call in R without returning a value.
	 * The method returns after the evaluation is finished.
	 * 
	 * <p>The evaluation is performed in the global environment of R.</p>
	 * 
	 * @param m a progress monitor to catch cancellation and provide progress feedback.
	 * @throws StatusException if the operation was canceled or failed; the status
	 *     of the exception contains detail about the cause
	 */
	void evalVoid(final ProgressMonitor m) throws StatusException;
	
	/**
	 * Performs the evaluation of this function call in R without returning a value.
	 * The method returns after the evaluation is finished.
	 * 
	 * <p>This method allows advanced configuration for the evaluation.</p>
	 * 
	 * @param envir the environment where to perform the evaluation; specified by an reference
	 *     or language object, or <code>null</code> for the global environment
	 * @param m a progress monitor to catch cancellation and provide progress feedback.
	 * @throws StatusException if the operation was canceled or failed; the status
	 *     of the exception contains detail about the cause
	 * 
	 * @since de.walware.rj.services 2.1
	 */
	void evalVoid(final @Nullable RObject envir,
			final ProgressMonitor m) throws StatusException;
	
	/**
	 * Performs the evaluation of the this function call in R and returns its value as R data object.
	 * The method returns after the evaluation is finished.
	 * 
	 * <p>This is a short version of {@link #evalData(String, int, int, ProgressMonitor)}
	 * sufficient for most purpose. The returned R data objects are created by the default factory
	 * with no limit in the object tree depth.</p>
	 * 
	 * <p>The evaluation is performed in the global environment of R.</p>
	 * 
	 * @param m a progress monitor to catch cancellation and provide progress feedback
	 * @return the evaluated value as R data object
	 * @throws StatusException if the operation was canceled or failed; the status
	 *     of the exception contains detail about the cause
	 */
	RObject evalData(final ProgressMonitor m) throws StatusException;
	
	/**
	 * Performs the evaluation of the this function call in R and returns its value as R data object.
	 * The method returns after the evaluation is finished.
	 * 
	 * <p>This method allows advanced configuration for the returned R data object.</p>
	 * 
	 * <p>The evaluation is performed in the global environment of R.</p>
	 * 
	 * @param factoryId the id of the factory to use when creating the RObject in this VM.
	 * @param options 0
	 * @param depth object tree depth for the created return value
	 * @param m a progress monitor to catch cancellation and provide progress feedback
	 * @return the evaluated value as R data object
	 * @throws StatusException if the operation was canceled or failed; the status
	 *     of the exception contains detail about the cause
	 */
	RObject evalData(final @Nullable String factoryId, final int options, final int depth,
			final ProgressMonitor m) throws StatusException;
	
	/**
	 * Performs the evaluation of the this function call in R and returns its value as R data object.
	 * The method returns after the evaluation is finished.
	 * 
	 * <p>This method allows advanced configuration for the evaluation and the returned R data
	 * object.</p>
	 * 
	 * @param envir the environment where to perform the evaluation; specified by an reference
	 *     or language object, or <code>null</code> for the global environment
	 * @param factoryId the id of the factory to use when creating the RObject in this VM.
	 * @param options 0
	 * @param depth object tree depth for the created return value
	 * @param m a progress monitor to catch cancellation and provide progress feedback
	 * @return the evaluated value as R data object
	 * @throws StatusException if the operation was canceled or failed; the status
	 *     of the exception contains detail about the cause
	 * 
	 * @since de.walware.rj.services 2.1
	 */
	RObject evalData(final @Nullable RObject envir,
			final @Nullable String factoryId, final int options, final int depth,
			final ProgressMonitor m) throws StatusException;
	
	/**
	 * Performs the evaluation of the this function call in R and assign its return value to
	 * the specified expression.
	 * 
	 * <p>The target have to be a valid target expression for a R <code>&lt;-</code> assignment
	 * operation.  A single symbol like <code>x</code> or <code>`x-y`</code>, a path in an object
	 * tree like <code>xlist$item1</code> or <code>xobj@slotName</code> is valid as well as
	 * special function calls which supports assignments like <code>dim(x)</code>.</p>
	 * 
	 * <p>The evaluation and assignment is performed in the global environment of R.</p>
	 * 
	 * @param target a single valid expression to assign the result to
	 * @param m a progress monitor to catch cancellation and provide progress feedback
	 * @throws StatusException if the operation was canceled or failed; the status
	 *     of the exception contains detail about the cause
	 * 
	 * @since de.walware.rj.services 2.0
	 */
	void evalAssign(final String target,
			final ProgressMonitor m) throws StatusException;
	
}
