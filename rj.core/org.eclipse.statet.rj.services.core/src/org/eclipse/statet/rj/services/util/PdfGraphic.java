/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RService;


public class PdfGraphic extends Graphic {
	
	
	public PdfGraphic() {
	}
	
	
	@Override
	public void setSize(final double width, final double height, final String unit) {
		if (UNIT_PX.equals(unit)) {
			throw new IllegalArgumentException("Pixel not supported by PDF graphic.");
		}
		super.setSize(width, height, unit);
	}
	
	@Override
	protected void prepare(final String filename,
			final RService service, final ProgressMonitor m) throws StatusException {
		final FunctionCall png= service.createFunctionCall("pdf"); //$NON-NLS-1$
		png.addChar("file", filename); //$NON-NLS-1$
		if (this.resolution > 0) {
			png.addInt("res", this.resolution); //$NON-NLS-1$
		}
		if (this.sizeUnit != null) {
			if (this.sizeUnit.equals(UNIT_IN)) {
				png.addNum("width", this.sizeWidth); //$NON-NLS-1$
				png.addNum("height", this.sizeHeight); //$NON-NLS-1$
			}
			else if (this.sizeUnit.equals(UNIT_CM)) {
				png.addNum("width", this.sizeWidth/2.54); //$NON-NLS-1$
				png.addNum("height", this.sizeHeight/2.54); //$NON-NLS-1$
			}
			else if (this.sizeUnit.equals(UNIT_MM)) {
				png.addNum("width", this.sizeWidth/25.4); //$NON-NLS-1$
				png.addNum("height", this.sizeHeight/25.4); //$NON-NLS-1$
			}
		}
		png.evalVoid(m);
	}
	
}
