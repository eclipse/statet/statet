/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util;

import java.io.OutputStream;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RService;


public abstract class Graphic {
	
	
	public static final String UNIT_PX= "px";
	public static final String UNIT_IN= "in";
	public static final String UNIT_CM= "cm";
	public static final String UNIT_MM= "mm";
	
	
	String sizeUnit;
	double sizeWidth;
	double sizeHeight;
	
	int resolution= -1;
	
	
	protected Graphic() {
	}
	
	
	/**
	 * Sets the size of the graphic.
	 * 
	 * The unit can be one of the constants with prefix UNIT_ .
	 * The default is pixel for raster graphic images (png) and inch for vector images (pdf).
	 * 
	 * @param width the width in the given unit
	 * @param height the height in the given unit
	 * @param unit the unit of width and height arguments
	 */
	public void setSize(final double width, final double height, final String unit) {
		this.sizeWidth= width;
		this.sizeHeight= height;
		this.sizeUnit= unit;
	}
	
	/**
	 * Sets the nominal resolution in dpi of the graphic.
	 * 
	 * @param resolution the resolution in dpi
	 */
	public void setResolution(final int resolution) {
		this.resolution= resolution;
	}
	
	
	public byte[] create(final FunctionCall plot,
			final RService service, final ProgressMonitor m) throws StatusException {
		final String filename= "plot-"+System.nanoTime()+".plot";
		prepare(filename, service, m);
		plot.evalVoid(m);
		service.evalVoid("dev.off()", m);
		return service.downloadFile(filename, 0, m);
	}
	
	public void create(final FunctionCall plot, final OutputStream out,
			final RService service, final ProgressMonitor m) throws StatusException {
		final String filename= "plot-"+System.nanoTime()+".plot";
		prepare(filename, service, m);
		plot.evalVoid(m);
		service.evalVoid("dev.off()", m);
		service.downloadFile(out, filename, 0, m);
	}
	
	public byte[] create(final String plotCommand,
			final RService service, final ProgressMonitor m) throws StatusException {
		final String filename= "plot-"+System.nanoTime()+".plot";
		prepare(filename, service, m);
		service.evalVoid(plotCommand, m);
		service.evalVoid("dev.off()", m);
		return service.downloadFile(filename, 0, m);
	}
	
	public void create(final String plotCommand, final OutputStream out,
			final RService service, final ProgressMonitor m) throws StatusException {
		final String filename= "plot-"+System.nanoTime()+".plot";
		prepare(filename, service, m);
		service.evalVoid(plotCommand, m);
		service.evalVoid("dev.off()", m);
		service.downloadFile(out, filename, 0, m);
	}
	
	protected abstract void prepare(final String filename,
			final RService service, final ProgressMonitor m) throws StatusException;
	
}
