/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util.dataaccess;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore.Fragment;


/**
 * Data adapter for {@link RVector} objects.
 * 
 * @since 2.0 (provisional)
 */
public class RVectorDataAdapter extends AbstractRDataAdapter<RVector<?>, RVector<?>> {
	
	
	@Override
	public RVector<?> validate(final RObject rObject) throws UnexpectedRDataException {
		return RDataUtils.checkRVector(rObject);
	}
	
	@Override
	public RVector<?> validate(final RObject rObject, final RVector<?> referenceObject,
			final int flags) throws UnexpectedRDataException {
		final RVector<?> vector= RDataUtils.checkRVector(rObject);
		if ((flags & ROW_COUNT) != 0) {
			RDataUtils.checkLengthEqual(vector, referenceObject.getLength());
		}
		if ((flags & STORE_TYPE) != 0) {
			RDataUtils.checkData(vector.getData(), referenceObject.getData().getStoreType());
		}
		return vector;
	}
	
	@Override
	public long getRowCount(final RVector<?> rObject) {
		return rObject.getLength();
	}
	
	@Override
	public long getColumnCount(final RVector<?> rObject) {
		return 1;
	}
	
	
	@Override
	protected String getLoadDataFName() {
		return API_R_PREFIX + ".getDataVectorValues"; //$NON-NLS-1$
	}
	
	@Override
	protected String getSetDataFName() {
		return API_R_PREFIX + ".setDataVectorValues"; //$NON-NLS-1$
	}
	
	@Override
	protected RVector<?> validateData(final RObject rObject, final RVector<?> referenceObject,
			final Fragment<RVector<?>> fragment) throws UnexpectedRDataException {
		final RVector<?> vector= RDataUtils.checkRVector(rObject);
		RDataUtils.checkLengthEqual(vector, fragment.getRowCount());
		
		RDataUtils.checkData(rObject.getData(), referenceObject.getData().getStoreType());
		
		return vector;
	}
	
	@Override
	protected String getLoadRowNamesFName() {
		return API_R_PREFIX + ".getDataVectorRowNames"; //$NON-NLS-1$
	}
	
}
