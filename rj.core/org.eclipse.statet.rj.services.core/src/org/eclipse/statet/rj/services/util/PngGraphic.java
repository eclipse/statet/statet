/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RPlatform;
import org.eclipse.statet.rj.services.RService;


public class PngGraphic extends Graphic {
	
	
	public PngGraphic() {
	}
	
	
	@Override
	protected void prepare(final String filename,
			final RService service, final ProgressMonitor m) throws StatusException {
		final FunctionCall png= service.createFunctionCall("png");
		png.addChar("filename", filename);
		if (this.resolution > 0) {
			png.addInt("res", this.resolution);
		}
		if (this.sizeUnit != null) {
			png.addNum("width", this.sizeWidth);
			png.addNum("height", this.sizeHeight);
			png.addChar("unit", this.sizeUnit);
		}
		if (service.getPlatform().getOsType().equals(RPlatform.OS_WINDOWS)) {
			png.addLogi("restoreConsole", false);
		}
		png.evalVoid(m);
	}
	
}
