/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RReference;


@NonNullByDefault
public class BasicFQRObjectRef<THandle> implements FQRObjectRef<THandle> {
	
	
	private static boolean isValidEnvObject(final RObject env) {
		switch (env.getRObjectType()) {
		case RObject.TYPE_REFERENCE:
			return (((RReference) env).getReferencedRObjectType() == RObject.TYPE_ENVIRONMENT);
		case RObject.TYPE_LANGUAGE:
			return (((RLanguage) env).getLanguageType() == RLanguage.CALL);
		default:
			return false;
		}
	}
	
	private static boolean isValidNameObject(final RObject env) {
		switch (env.getRObjectType()) {
		case RObject.TYPE_LANGUAGE:
			switch (((RLanguage) env).getLanguageType()) {
			case RLanguage.NAME:
			case RLanguage.CALL:
				return true;
			default:
				return false;
			}
		default:
			return false;
		}
	}
	
	
	private final THandle rHandle;
	
	private final RObject env;
	
	private final RObject name;
	
	
	public BasicFQRObjectRef(final THandle rHandle, final RObject env, final RObject name) {
		if (env == null) {
			throw new NullPointerException("env"); //$NON-NLS-1$
		}
		if (!isValidEnvObject(env)) {
			throw new IllegalArgumentException("env"); //$NON-NLS-1$
		}
		if (name == null) {
			throw new NullPointerException("name"); //$NON-NLS-1$
		}
		if (!isValidNameObject(name)) {
			throw new IllegalArgumentException("name"); //$NON-NLS-1$
		}
		
		this.rHandle= rHandle;
		this.env= env;
		this.name= name;
	}
	
	
	@Override
	public THandle getRHandle() {
		return this.rHandle;
	}
	
	@Override
	public RObject getEnv() {
		return this.env;
	}
	
	@Override
	public RObject getName() {
		return this.name;
	}
	
	
	@Override
	public String toString() {
		return this.env + "\n" + this.name.toString();
	}
	
}
