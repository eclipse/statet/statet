/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.REnvironment;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.impl.RLanguageImpl;


@NonNullByDefault
public class BasicFQRObject<THandle> implements FQRObject<THandle> {
	
	
	private final THandle rHandle;
	
	private final REnvironment env;
	
	private final RObject name;
	
	private final RObject object;
	
	
	public BasicFQRObject(final THandle rHandle, final REnvironment env, final RObject name, final RObject object) {
		this.rHandle= rHandle;
		this.env= nonNullAssert(env);
		this.name= nonNullAssert(name);
		this.object= nonNullAssert(object);
	}
	
	public BasicFQRObject(final THandle rHandle, final REnvironment env, final String name, final RObject object) {
		this.rHandle= rHandle;
		this.env= nonNullAssert(env);
		this.name= new RLanguageImpl(RLanguage.NAME, nonNullAssert(name), null);
		this.object= nonNullAssert(object);
	}
	
	
	@Override
	public THandle getRHandle() {
		return this.rHandle;
	}
	
	@Override
	public REnvironment getEnv() {
		return this.env;
	}
	
	@Override
	public RObject getName() {
		return this.name;
	}
	
	@Override
	public RObject getObject() {
		return this.object;
	}
	
}
