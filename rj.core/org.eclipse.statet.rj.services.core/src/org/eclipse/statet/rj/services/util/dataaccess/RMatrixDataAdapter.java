/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util.dataaccess;

import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore.Fragment;


/**
 * Data adapter for two-dimensional {@link RArray} objects.
 * 
 * @since 2.0 (provisional)
 */
public class RMatrixDataAdapter extends AbstractRDataAdapter<RArray<?>, RArray<?>> {
	
	
	@Override
	public RArray<?> validate(final RObject rObject) throws UnexpectedRDataException {
		return RDataUtils.checkRArray(rObject, 2);
	}
	
	@Override
	public RArray<?> validate(final RObject rObject, final RArray<?> referenceObject,
			final int flags) throws UnexpectedRDataException {
		final RArray<?> array= RDataUtils.checkRArray(rObject, 2);
		RDataUtils.checkColumnCountEqual(array, RDataUtils.getColumnCount(referenceObject));
		if ((flags & ROW_COUNT) != 0) {
			RDataUtils.checkLengthEqual(array, referenceObject.getLength());
		}
		if ((flags & STORE_TYPE) != 0) {
			RDataUtils.checkData(array.getData(), referenceObject.getData().getStoreType());
		}
		return array;
	}
	
	@Override
	public long getRowCount(final RArray<?> rObject) {
		return rObject.getDim().getInt(0);
	}
	
	@Override
	public long getColumnCount(final RArray<?> rObject) {
		return rObject.getDim().getInt(1);
	}
	
	
	@Override
	protected String getLoadDataFName() {
		return API_R_PREFIX + ".getDataMatrixValues"; //$NON-NLS-1$
	}
	
	@Override
	protected String getSetDataFName() {
		return "rj:::.setDataMatrixValues";
	}
	
	@Override
	protected RArray<?> validateData(final RObject rObject, final RArray<?> referenceObject,
			final Fragment<RArray<?>> fragment)
			throws UnexpectedRDataException {
		final RArray<?> array= RDataUtils.checkRArray(rObject, 2);
		RDataUtils.checkColumnCountEqual(array, RDataUtils.checkIntLength(fragment.getColumnCount()));
		RDataUtils.checkRowCountEqual(array, RDataUtils.checkIntLength(fragment.getRowCount()));
		
		RDataUtils.checkData(rObject.getData(), referenceObject.getData().getStoreType());
		
		return array;
	}
	
	@Override
	protected String getLoadRowNamesFName() {
		return API_R_PREFIX + ".getDataMatrixRowNames"; //$NON-NLS-1$
	}
	
}
