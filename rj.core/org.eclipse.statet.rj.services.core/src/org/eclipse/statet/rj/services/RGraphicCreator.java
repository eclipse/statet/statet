/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.graphic.core.RGraphic;


/**
 * Controls the creation of {@link RGraphic}.
 * 
 * The creator can be access by {@link RService#createRGraphicCreator(int)}.
 * A creator can be used multiple times for different graphics. Properties
 * are reused and not reseted.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @since de.walware.rj.services 0.5
 */
public interface RGraphicCreator {
	
	/**
	 * Sets the size in pixel of the graphic to create.
	 * 
	 * @param width width in pixel
	 * @param height height in pixel
	 */
	void setSize(final double width, final double height);
	
	
	RGraphic create(final String expression,
			final ProgressMonitor m) throws StatusException;
	
	RGraphic create(final FunctionCall fcall,
			final ProgressMonitor m) throws StatusException;
	
}
