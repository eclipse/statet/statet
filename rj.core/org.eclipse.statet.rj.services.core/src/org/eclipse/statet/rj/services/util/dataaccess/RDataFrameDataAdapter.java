/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util.dataaccess;

import org.eclipse.statet.rj.data.RDataFrame;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.util.dataaccess.LazyRStore.Fragment;


/**
 * Data adapter for {@link RDataFrame} objects.
 * 
 * @since 2.0 (provisional)
 */
public class RDataFrameDataAdapter extends AbstractRDataAdapter<RDataFrame, RDataFrame> {
	
	
	@Override
	public RDataFrame validate(final RObject rObject) throws UnexpectedRDataException {
		return RDataUtils.checkRDataFrame(rObject);
	}
	
	@Override
	public RDataFrame validate(final RObject rObject, final RDataFrame referenceObject,
			final int flags) throws UnexpectedRDataException {
		final RDataFrame dataframe= RDataUtils.checkRDataFrame(rObject, referenceObject.getColumnCount());
		if ((flags & ROW_COUNT) != 0) {
			RDataUtils.checkRowCountEqual(dataframe, referenceObject.getRowCount());
		}
		if ((flags & STORE_TYPE) != 0) {
			for (int i= 0; i < dataframe.getColumnCount(); i++) {
				RDataUtils.checkData(dataframe.getColumn(i), referenceObject.getColumn(i).getStoreType());
			}
		}
		return dataframe;
	}
	
	@Override
	public long getRowCount(final RDataFrame rObject) {
		return rObject.getRowCount();
	}
	
	@Override
	public long getColumnCount(final RDataFrame rObject) {
		return rObject.getColumnCount();
	}
	
	
	@Override
	protected String getLoadDataFName() {
		return API_R_PREFIX + ".getDataFrameValues"; //$NON-NLS-1$
	}
	
	@Override
	protected String getSetDataFName() {
		return "rj:::.setDataFrameValues"; //$NON-NLS-1$
	}
	
	@Override
	protected RDataFrame validateData(final RObject rObject, final RDataFrame referenceObject,
			final Fragment<RDataFrame> fragment) throws UnexpectedRDataException {
		final RDataFrame dataframe= RDataUtils.checkRDataFrame(rObject, fragment.getColumnCount());
		RDataUtils.checkRowCountEqual(dataframe, fragment.getRowCount());
		
		for (int i= 0; i < fragment.getColumnCount(); i++) {
			RDataUtils.checkData(dataframe.getColumn(i),
					referenceObject.getColumn(fragment.getColumnBeginIdx() + i).getStoreType() );
		}
		
		return dataframe;
	}
	
	@Override
	protected String getLoadRowNamesFName() {
		return API_R_PREFIX + ".getDataFrameRowNames"; //$NON-NLS-1$
	}
	
}
