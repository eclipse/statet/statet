/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RCodeBuilder {
	
	
	/**
	 * Appends the specified string as double quoted R string to the string builder.
	 * If required, the characters of the string are escaped according to the R language specification.
	 * 
	 * @param sb
	 * @param s the string to append
	 */
	public static void appendStringD(final StringBuilder sb, final CharSequence s) {
		sb.ensureCapacity(sb.length() + 2);
		sb.append('"');
		int doneIndex= 0;
		final int endIndex= s.length();
		for (int index= 0; index < endIndex; ) {
			final char c= s.charAt(index);
			switch (c) {
			case '\\':
				if (doneIndex < index) {
					sb.append(s, doneIndex, index);
				}
				sb.append("\\\\"); //$NON-NLS-1$
				doneIndex= ++index;
				continue;
			case '"':
				if (doneIndex < index) {
					sb.append(s, doneIndex, index);
				}
				sb.append("\\\""); //$NON-NLS-1$
				doneIndex= ++index;
				continue;
			default:
				index++;
				continue;
			}
		}
		if (doneIndex < endIndex) {
			sb.append(s, doneIndex, endIndex);
		}
		sb.append('"');
	}
	
	
	protected final StringBuilder sb;
	
	
	public RCodeBuilder() {
		this.sb= new StringBuilder();
	}
	
	public RCodeBuilder(final int initialCapacity) {
		this.sb= new StringBuilder(initialCapacity);
	}
	
	
	public RCodeBuilder clear() {
		this.sb.setLength(0);
		return this;
	}
	
	
	/**
	 * Appends the specified code.
	 * 
	 * @param code the code to append
	 */
	public RCodeBuilder append(final CharSequence code) {
		this.sb.append(nonNullAssert(code));
		return this;
	}
	
	/**
	 * Appends the specified code.
	 * 
	 * @param code the code to append
	 */
	public RCodeBuilder append(final String code) {
		this.sb.append(nonNullAssert(code));
		return this;
	}
	
	public RCodeBuilder append(final CharSequence code, final int startIndex, final int endIndex) {
		this.sb.append(nonNullAssert(code), startIndex, endIndex);
		return this;
	}
	
	public RCodeBuilder append(final char c) {
		this.sb.append(c);
		return this;
	}
	
	public RCodeBuilder insert(final int index, final char c) {
		this.sb.insert(index, c);
		return this;
	}
	
	public RCodeBuilder deleteEnd(final int length) {
		final int endIndex= this.sb.length();
		this.sb.delete(endIndex - length, endIndex);
		return this;
	}
	
	
	/**
	 * Appends the specified string as R string.
	 * 
	 * @param string the string to append
	 */
	public RCodeBuilder appendChar(final CharSequence s) {
		appendStringD(this.sb, s);
		return this;
	}
	
	
	public String build() {
		return this.sb.toString();
	}
	
	
	@Override
	public String toString() {
		return this.sb.toString();
	}
	
}
