/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.util;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.util.Version;


@NonNullByDefault
public class RVersion extends Version {
	
	
	public RVersion(final int major, final int minor, final int micro,
			final @Nullable String qualifier) {
		super(major, minor, micro, qualifier);
	}
	
	public RVersion(final int major, final int minor, final int micro) {
		super(major, minor, micro);
	}
	
	public RVersion(final Version version) {
		super(version);
	}
	
	public RVersion(final String version) {
		super(version);
	}
	
	
}
