/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.renv.core;

import java.util.List;

import com.ibm.icu.text.Collator;
import com.ibm.icu.util.ULocale;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.Status;

import org.eclipse.statet.rj.renv.core.RLibGroup;
import org.eclipse.statet.rj.renv.core.RLibLocation;


@NonNullByDefault
public class REnvCoreInternals {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.rj.services"; //$NON-NLS-1$
	
	
	public static void log(final Status status) {
		CommonsRuntime.log(status);
	}
	
	
	public static final Collator R_NAMES_COLLATOR;
	static {
		final ULocale locale= ULocale.forLanguageTag("en-u-ks-identic-kf-upper"); //$NON-NLS-1$
		final Collator collator= Collator.getInstance(locale);
		if (collator.getStrength() != Collator.IDENTICAL) {
			throw new IllegalStateException("Collator.strength= " + collator.getStrength()); //$NON-NLS-1$
		}
		R_NAMES_COLLATOR= collator;
	}
	
	
	public static ImList<? extends RLibLocation> listRLibLocations(
			final ImList<? extends RLibGroup> libGroups) {
		final List<? extends RLibLocation>[] listArray= new @NonNull List[libGroups.size()];
		for (int i= 0; i < listArray.length; i++) {
			listArray[i]= libGroups.get(i).getLibLocations();
		}
		return ImCollections.concatList(listArray);
	}
	
	public static @Nullable String getRLibGroupLabel(final String groupId) {
		switch (groupId) {
		case RLibGroup.R_DEFAULT:
			return "Default Library";
		case RLibGroup.R_SITE:
			return "Site Libraries (R_LIBS_SITE)";
		case RLibGroup.R_USER:
			return "User Libraries (R_LIBS_USER)";
		case RLibGroup.R_OTHER:
			return "Additional Libraries (R_LIBS)";
		default:
			return null;
		}
	}
	
	
	private REnvCoreInternals() {}
	
}
