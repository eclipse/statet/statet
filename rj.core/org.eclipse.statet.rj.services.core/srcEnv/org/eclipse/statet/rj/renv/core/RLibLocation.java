/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import java.nio.file.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * A single R library.
 * 
 * It usually points to a directory in a file system with
 * subdirectories containing the R packages.
 */
@NonNullByDefault
public interface RLibLocation {
	
	
	/** Source type constant */
	String R= "r"; //$NON-NLS-1$
	/** Source type constant */
	String EPLUGIN= "eplugin"; //$NON-NLS-1$
	/** Source type constant */
	String USER= "user"; //$NON-NLS-1$
	
	String MISSING= "missing"; //$NON-NLS-1$
	
	
	String getSource();
	
	@Nullable String getLabel();
	
	/**
	 * Returns the directory as specified (may contain variables).
	 * 
	 * @return the directory path string
	 */
	String getDirectory();
	/**
	 * Returns the directory as local NIO path.
	 * 
	 * @return the directory path; or <code>null</code> if the specified path is not local or
	 *     cannot be resolved
	 */
	@Nullable Path getDirectoryPath();
	
}
