/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicRLibGroup implements RLibGroup {
	
	
	private final String id;
	
	private final String label;
	
	private final List<? extends RLibLocation> libLocations;
	
	
	public BasicRLibGroup(final String id, final String label,
			final List<? extends RLibLocation> libLocations) {
		this.id= id;
		this.label= label;
		this.libLocations= libLocations;
	}
	
	
	@Override
	public String getId() {
		return this.id;
	}
	
	@Override
	public String getLabel() {
		return this.label;
	}
	
	@Override
	public List<? extends RLibLocation> getLibLocations() {
		return this.libLocations;
	}
	
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RLibGroup other
						&& this.id.equals(other.getId())
						&& this.label.equals(other.getLabel())
						&& this.libLocations.equals(other.getLibLocations()) ));
	}
	
}
