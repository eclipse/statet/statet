/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Basic immutable R package, implementation of {@link RPkg}.
 */
@NonNullByDefault
public class BasicRPkg implements RPkg {
	
	
	private final String name;
	
	private final RNumVersion version;
	
	
	public BasicRPkg(final String name, final RNumVersion version) {
		if (name == null) {
			throw new NullPointerException("name"); //$NON-NLS-1$
		}
		if (version == null) {
			throw new NullPointerException("version"); //$NON-NLS-1$
		}
		this.name= name;
		this.version= version;
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public RNumVersion getVersion() {
		return this.version;
	}
	
	
	@Override
	public int hashCode() {
		return this.name.hashCode() + this.version.hashCode() * 7;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RPkg other
						&& this.name.equals(other.getName())
						&& this.version.equals(other.getVersion()) ));
	}
	
	
	@Override
	public String toString() {
		if (this.version == RNumVersion.NONE) {
			return this.name;
		}
		final StringBuilder sb= new StringBuilder(this.name.length() + this.version.toString().length() + 12);
		sb.append(this.name);
		sb.append(" (" + "version= ").append(this.version.toString());
		sb.append(')');
		return sb.toString();
	}
	
}
