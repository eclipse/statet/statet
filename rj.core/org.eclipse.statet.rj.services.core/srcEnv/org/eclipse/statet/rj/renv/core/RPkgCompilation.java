/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RPkgCompilation<T extends RPkg> {
	
	
	/**
	 * Returns the sorted set with names of all packages in this collection.
	 * 
	 * @return the package names
	 */
	List<String> getNames();
	
	boolean contains(String name);
	List<T> get(String name);
	@Nullable T getFirst(String name);
	
	List<String> getSources();
	@Nullable RPkgList<T> getBySource(String source);
	
	List<? extends RPkgList<T>> getAll();
	
}
