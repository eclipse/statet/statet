/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RPkgBuilt extends RPkg {
	
	
	static boolean equalsBuilt(final @Nullable RPkgBuilt pkg1, final @Nullable RPkgBuilt pkg2) {
		return (pkg1 == pkg2
				|| (pkg1 != null && pkg2 != null
						&& pkg1.getName().equals(pkg2.getName())
						&& pkg1.getVersion().equals(pkg2.getVersion())
						&& pkg1.getBuilt().equals(pkg2.getBuilt()) ));
	}
	
	
	String getTitle();
	
	String getBuilt();
	
	RLibLocation getLibLocation();
	
}
