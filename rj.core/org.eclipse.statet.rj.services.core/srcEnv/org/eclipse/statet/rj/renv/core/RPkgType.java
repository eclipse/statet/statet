/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Type ({@link #SOURCE source} or {@link #BINARY binary}) of R packages.
 */
@NonNullByDefault
public enum RPkgType {
	
	
	SOURCE("Source"),
	BINARY("Binary");
	
	
	private final String label;
	
	
	private RPkgType(final String label) {
		if (label == null) {
			throw new NullPointerException("label"); //$NON-NLS-1$
		}
		this.label= label;
	}
	
	
	public String getLabel() {
		return this.label;
	}
	
	
	@Override
	public String toString() {
		return getLabel();
	}
	
}
