/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.runtime;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
public interface RPkgManager {
	
	
	int NONE=                           0;
	
	int INSTALLED=                      1 << 0;
	int AVAILABLE_REPOS=                1 << 4;
	int AVAILABLE_PKGS=                 1 << 5;
	int AVAILABLE= AVAILABLE_REPOS | AVAILABLE_PKGS;
	
	int REFRESH_INSTALLED_PKGS=         1 << 10;
	int REFRESH_AVAILABLE_REPOS=        1 << 14;
	int REFRESH_AVAILABLE_PKGS=         1 << 15;
	
	int INITIAL=                        1 << 20;
	int RESET=                          1 << 21;
	
	
	int OK=                             0;
	int REQUIRES_CONFIG=                1 << 0;
	int REQUIRES_UPDATE=                1 << 1;
	
	
	static int expandFlags(int flags) {
		if (((REFRESH_AVAILABLE_REPOS) & flags) != 0) {
			flags |= AVAILABLE_REPOS;
		}
		if (((REFRESH_AVAILABLE_PKGS) & flags) != 0) {
			flags |= AVAILABLE_REPOS | AVAILABLE_PKGS;
		}
		if ((REFRESH_INSTALLED_PKGS & flags) != 0) {
			flags |= INSTALLED;
		}
		return flags;
	}
	
	
	REnv getREnv();
	
	
	/**
	 * Performs regular checks, e.g. checks for changes in R lib paths.
	 * 
	 * @param flags {@link #NONE}, {@link #INITIAL}
	 * @param r an R service of the R environment
	 * @param monitor
	 * @throws StatusException
	 */
	void check(int flags, RService r, ProgressMonitor m) throws StatusException;
	
	/**
	 * Returns the dataset providing the specified features.
	 * 
	 * @param request feature flags
	 * @return {@link #OK}, {@link #REQUIRES_CONFIG} or {@link #REQUIRES_UPDATE}
	 */
	int request(int request);
	
	void update(RService r, ProgressMonitor m) throws StatusException;
	
	/**
	 * Returns the current dataset.
	 * 
	 * @return
	 */
	@Nullable RPkgManagerDataset getDataset();
	
	/**
	 * Returns the dataset providing the specified features.
	 * 
	 * @param request feature flags
	 * @return
	 */
	default RPkgManagerDataset getDataset(final int request,
			final RService r, final ProgressMonitor m) throws StatusException {
		if (request(request) != OK) {
			update(r, m);
		}
		return nonNullAssert(getDataset());
	}
	
}
