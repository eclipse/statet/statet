/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.rj.renv.core.REnvCoreInternals;


@NonNullByDefault
public class BasicRLibPaths implements RLibPaths {
	
	
	private final ImList<? extends RLibGroup> libGroups;
	private final ImList<? extends RLibLocation> libLocations;
	
	
	public BasicRLibPaths(final ImList<? extends RLibGroup> libGroups,
			final ImList<? extends RLibLocation> libLocations) {
		this.libGroups= nonNullAssert(libGroups);
		this.libLocations= nonNullAssert(libLocations);
	}
	
	public BasicRLibPaths(final List<? extends RLibGroup> libGroups) {
		this.libGroups= ImCollections.toList(libGroups);
		this.libLocations= REnvCoreInternals.listRLibLocations(this.libGroups);
	}
	
	
	@Override
	public ImList<? extends RLibGroup> getRLibGroups() {
		return this.libGroups;
	}
	
	@Override
	public ImList<? extends RLibLocation> getRLibLocations() {
		return this.libLocations;
	}
	
	
}
