/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface REnvManager {
	
	/**
	 * Returns a list containing all current R environments.
	 * 
	 * @return a list containing the handle of all R environments
	 */
	ImList<? extends REnv> list();
	
	/**
	 * Returns the R environment with the specified id or name.
	 * 
	 * <p>The name is only considered, if the id is not specified (<code>null</code>) or there is
	 * no environment with the specified id.</p>
	 * 
	 * @param id the id of the R environment
	 * @param name the name of the R environment
	 * @return the handle of the R environment or <code>null</code>
	 */
	@Nullable REnv get(final @Nullable String id, final @Nullable String name);
	
	/**
	 * Adds the specified R environment.
	 * 
	 * @param rEnvConfig the configuration of the R environment to add
	 * 
	 * @throws UnsupportedOperationException if add/delete is not supported for the specified
	 *     R environment
	 */
	void add(final REnvConfiguration rEnvConfig);
	
	/**
	 * Deletes the specified R environment.
	 * 
	 * @param rEnv the handle of the R environment
	 * 
	 * @throws UnsupportedOperationException if delete is not supported for the specified
	 *     R environment
	 */
	void delete(final REnv rEnv);
	
}
