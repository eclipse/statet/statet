/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import java.io.IOException;
import java.nio.file.Path;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface RLibPaths {
	
	
	ImList<? extends RLibGroup> getRLibGroups();
	
	default @Nullable RLibGroup getRLibGroup(final String id) {
		for (final RLibGroup group : getRLibGroups()) {
			if (group.getId() == id) {
				return group;
			}
		}
		return null;
	}
	
	ImList<? extends RLibLocation> getRLibLocations();
	
	default @Nullable RLibLocation getRLibLocationByDirectory(final String directory) {
		for (final RLibLocation libLocation : getRLibLocations()) {
			if (directory.equals(libLocation.getDirectory())) {
				return libLocation;
			}
		}
		return null;
	}
	
	default @Nullable RLibLocation getRLibLocationByDirectoryPath(final Path directory) {
		for (final RLibLocation libLocation : getRLibLocations()) {
			try {
				final Path libLocationPath= libLocation.getDirectoryPath();
				if (libLocationPath != null
						&& (directory.equals(libLocationPath)
								|| directory.equals(libLocationPath.toRealPath() ))) {
					return libLocation;
				}
			}
			catch (final IOException | SecurityException e) {}
		}
		return null;
	}
	
}
