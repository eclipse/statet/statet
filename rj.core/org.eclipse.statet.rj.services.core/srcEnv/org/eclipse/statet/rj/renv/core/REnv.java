/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class REnv {
	
	
	private final String id;
	
	
	public REnv(final String id) {
		this.id= id;
	}
	
	
	/**
	 * The id of the R environment.
	 * 
	 * The id of an instance of {@link REnv} and its configurations never changes.
	 * 
	 * @return the id
	 */
	public final String getId() {
		return this.id;
	}
	
	/**
	 * The current name of the R environment.
	 * 
	 * @return the name
	 */
	public abstract String getName();
	
	/**
	 * Resolves virtual references (like workbench default) to references of a real configuration.
	 * 
	 * A virtual reference can return <code>null</code>, if it doesn't currently point to a valid
	 * configuration. A reference of a real configuration always return itself.
	 * 
	 * @return the final reference, if available, otherwise <code>null</code>.
	 */
	public abstract @Nullable REnv resolve();
	
	
	/**
	 * Resolves finally the reference and returns the configuration, if available.
	 * 
	 * @return the configuration if available, otherwise <code>null</code>.
	 */
	public abstract <T> @Nullable T get(Class<T> type);
	
	
	public boolean isDeleted() {
		return false;
	}
	
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof REnv && this.id.equals(((REnv) obj).getId())) );
	}
	
}
