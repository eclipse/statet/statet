/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import java.util.Locale;

import com.ibm.icu.text.Collator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rj.renv.core.REnvCoreInternals;
import org.eclipse.statet.rj.services.RJServices;
import org.eclipse.statet.rj.services.RPlatform;


@NonNullByDefault
public class RPkgUtils {
	
	
	public static final Collator NAMES_COLLATOR= REnvCoreInternals.R_NAMES_COLLATOR;
	
	
	private static boolean isWin(final RPlatform rPlatform) {
		return rPlatform.getOsType().equals(RPlatform.OS_WINDOWS);
	}
	
	private static boolean isMac(final RPlatform rPlatform) {
		return rPlatform.getOSName().regionMatches(true, 0, "Mac OS", 0, 6); //$NON-NLS-1$
	}
	
	public static @Nullable RPkgType getPkgType(final String fileName, final RPlatform rPlatform) {
		if (fileName.endsWith(".tar.gz")) { //$NON-NLS-1$
			return RPkgType.SOURCE;
		}
		if (isWin(rPlatform)) {
			if (fileName.endsWith(".zip") || fileName.toLowerCase(Locale.ROOT).endsWith(".zip")) { //$NON-NLS-1$ //$NON-NLS-2$
				return RPkgType.BINARY;
			}
		}
		else if (isMac(rPlatform)) {
			if (fileName.endsWith(".tgz")) { //$NON-NLS-1$
				return RPkgType.BINARY;
			}
		}
		return null;
	}
	
	public static RPkgType checkPkgType(final String fileName, final RPlatform rPlatform)
			throws StatusException {
		final RPkgType pkgType= getPkgType(fileName, rPlatform);
		if (pkgType == null) {
			throw new StatusException(new ErrorStatus(RJServices.BUNDLE_ID,
					String.format("Invalid file name '%1$s' (unsupported extension) for R package on %2$s.",
							fileName, rPlatform.getOSName() )));
		}
		return pkgType;
	}
	
	/**
	 * Checks if the given file name has the standard format <code>name_version.extension</code>.
	 * 
	 * @param fileName the file name to check
	 * @return a R package object with the detected name and version.
	 * @throws StatusException if the file name has not the standard format.
	 */
	public static RPkg checkPkgFileName(final String fileName)
			throws StatusException {
		final int extIdx;
		if (fileName.endsWith(".tar.gz")) { //$NON-NLS-1$
			extIdx= fileName.length() - 7;
		}
		else if (fileName.endsWith(".zip") || fileName.endsWith(".tgz")) { //$NON-NLS-1$ //$NON-NLS-2$
			extIdx= fileName.length() - 4;
		}
		else {
			throw new StatusException(new ErrorStatus(RJServices.BUNDLE_ID,
					String.format("Invalid file name '%1$s' (unsupported extension) for R package.",
							fileName )));
		}
		
		final int versionIdx= fileName.indexOf('_');
		if (versionIdx < 0) {
			throw new StatusException(new ErrorStatus(RJServices.BUNDLE_ID,
					String.format("Invalid file name '%1$s' (missing version) for R package.",
							fileName )));
		}
		return new BasicRPkg(fileName.substring(0, versionIdx),
				RNumVersion.create(fileName.substring(versionIdx + 1, extIdx)) );
	}
	
	public static @Nullable String getPkgTypeInstallKey(final RPlatform rPlatform, final RPkgType pkgType) {
		if (pkgType == RPkgType.SOURCE) {
			return "source"; //$NON-NLS-1$
		}
		if (pkgType == RPkgType.BINARY) {
			if (isWin(rPlatform)) {
				return "win.binary"; //$NON-NLS-1$
			}
			else if (isMac(rPlatform)) {
				return "mac.binary.leopard"; //$NON-NLS-1$
			}
		}
		return null;
	}
	
	
}
