/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Basic immutable R package description, implementation {@link RPkgDescription}.
 */
@NonNullByDefault
public class BasicRPkgDescription extends BasicRPkg implements RPkgDescription {
	
	
	private final String title;
	private final String description;
	
	private final @Nullable String author;
	private final @Nullable String maintainer;
	private final ImList<String> urls;
	
	private final String built;
	
	private final RLibLocation libLocation;
	
	
	public BasicRPkgDescription(final String name, final RNumVersion version,
			final String title, final String description,
			final @Nullable String author, final @Nullable String maintainer,
			final ImList<String> urls,
			final String built, final RLibLocation libLocation) {
		super(name, version);
		this.title= title;
		this.description= description;
		this.author= author;
		this.maintainer= maintainer;
		this.urls= urls;
		this.built= built;
		this.libLocation= libLocation;
	}
	
	public BasicRPkgDescription(final RPkgDescription template, final RLibLocation libLocation) {
		super(template.getName(), template.getVersion());
		this.title= template.getTitle();
		this.description= template.getDescription();
		this.author= template.getAuthor();
		this.maintainer= template.getMaintainer();
		this.urls= template.getUrls();
		this.built= template.getBuilt();
		this.libLocation= libLocation;
	}
	
	
	@Override
	public String getTitle() {
		return this.title;
	}
	
	@Override
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public @Nullable String getAuthor() {
		return this.author;
	}
	
	@Override
	public @Nullable String getMaintainer() {
		return this.maintainer;
	}
	
	@Override
	public ImList<String> getUrls() {
		return this.urls;
	}
	
	@Override
	public String getBuilt() {
		return this.built;
	}
	
	@Override
	public RLibLocation getLibLocation() {
		return this.libLocation;
	}
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj instanceof RPkgDescription && super.equals(obj));
	}
	
}
