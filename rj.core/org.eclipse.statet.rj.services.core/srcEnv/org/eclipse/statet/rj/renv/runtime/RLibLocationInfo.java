/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.runtime;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.rj.renv.core.RLibLocation;


@NonNullByDefault
public class RLibLocationInfo {
	
	
	private final RLibLocation location;
	
	private final String directoryRPath;
	
	private final boolean directoryExists;
	private final boolean directoryAccess;
	private final long lastModified;
	
	private final int libPathsIndex;
	
	
	public RLibLocationInfo(final RLibLocation location, final String directoryRPath,
			final boolean isDirectoryExists, final boolean isDirectoryWritable,
			final long lastModified, final int libPathsIndex) {
		this.location= location;
		this.directoryRPath= directoryRPath;
		this.directoryExists= isDirectoryExists;
		this.directoryAccess= isDirectoryWritable;
		this.lastModified= lastModified;
		this.libPathsIndex= libPathsIndex;
	}
	
	
	public RLibLocation getLibLocation() {
		return this.location;
	}
	
	public String getDirectoryRPath() {
		return this.directoryRPath;
	}
	
	public boolean isDirectoryExists() {
		return this.directoryExists;
	}
	
	public boolean isWritable() {
		return this.directoryAccess;
	}
	
	public long getLastModified() {
		return this.lastModified;
	}
	
	public int getLibPathsIndex() {
		return this.libPathsIndex;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(this.location.toString());
		sb.addProp("directoryRPath", this.directoryRPath); //$NON-NLS-1$
		sb.addProp("directoryExists", this.directoryExists); //$NON-NLS-1$
		sb.addProp("lastModified", this.lastModified); //$NON-NLS-1$
		return sb.toString();
	}
	
	
}
