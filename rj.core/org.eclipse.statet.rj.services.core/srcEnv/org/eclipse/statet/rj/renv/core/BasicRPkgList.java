/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Comparator;

import org.eclipse.statet.jcommons.collections.SortedArraySet;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class BasicRPkgList<T extends @NonNull RPkg> extends SortedArraySet<T>
		implements RPkgList<T> {
	
	
	private static final Comparator<RPkg> RPKG_BY_NAME_COMPARATOR= new Comparator<>() {
		@Override
		public
		final int compare(final RPkg pkg1, final RPkg pkg2) {
			return RPkgUtils.NAMES_COLLATOR.compare(pkg1.getName(), pkg2.getName());
		}
	};
	
	
	private static final long serialVersionUID= -3022375551268568786L;
	
	
	@SuppressWarnings("unchecked")
	public BasicRPkgList(final int capacity) {
		super((T[])new RPkg[capacity], 0, RPKG_BY_NAME_COMPARATOR);
	}
	
	
	@Override
	public int indexOf(final @Nullable String name) {
		if (name == null) {
			return -1;
		}
		int low= 0;
		int high= super.size() - 1;
		while (low <= high) {
			final int mid= (low + high) >>> 1;
			final int diff= RPkgUtils.NAMES_COLLATOR.compare(super.get(mid).getName(), name);
			if (diff < 0) {
				low= mid + 1;
			}
			else if (diff > 0) {
				high= mid - 1;
			}
			else {
				return mid;
			}
		}
		return -(low + 1);
	}
	
	public int indexOf(final String name, int low) {
		int high= super.size() - 1;
		while (low <= high) {
			final int mid= (low + high) >>> 1;
			final int diff= RPkgUtils.NAMES_COLLATOR.compare(super.get(mid).getName(), name);
			if (diff < 0) {
				low= mid + 1;
			}
			else if (diff > 0) {
				high= mid - 1;
			}
			else {
				return mid;
			}
		}
		return -(low + 1);
	}
	
	
	@Override
	public int indexOfE(final T pkg) {
		return indexOf(pkg.getName());
	}
	
	@Override
	public boolean contains(final @Nullable String name) {
		return (indexOf(name) >= 0);
	}
	
	@Override
	public @Nullable T get(final @Nullable String name) {
		final int idx= indexOf(name);
		return (idx >= 0) ? super.get(idx) : null;
	}
	
	@Override
	public int addE(final T pkg) {
		return addIndex(indexOf(pkg.getName()), pkg);
	}
	
	@Override
	public void add(final int index, final T pkg) {
		super.add(index, nonNullAssert(pkg));
	}
	
	public void remove(final String name) {
		final int idx= indexOf(name);
		if (idx >= 0) {
			remove(idx);
		}
	}
	
}
