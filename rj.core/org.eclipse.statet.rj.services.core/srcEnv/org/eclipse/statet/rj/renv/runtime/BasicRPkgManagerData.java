/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.renv.runtime;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.RLibPaths;
import org.eclipse.statet.rj.renv.core.RPkgBuilt;
import org.eclipse.statet.rj.renv.core.RPkgCompilation;


@NonNullByDefault
public class BasicRPkgManagerData<TInstalled extends RPkgBuilt> implements RPkgManagerDataset {
	
	
	private final REnv rEnv;
	
	private final int flags;
	
	private final RLibPaths rLibPaths;
	
	private final RPkgCompilation<? extends TInstalled> installed;
	
	
	public BasicRPkgManagerData(final REnv rEnv, final int flags, final RLibPaths rLibPaths,
			final RPkgCompilation<? extends TInstalled> installed) {
		this.rEnv= rEnv;
		this.flags= flags;
		this.rLibPaths= rLibPaths;
		this.installed= installed;
	}
	
	
	@Override
	public REnv getREnv() {
		return this.rEnv;
	}
	
	@Override
	public int getProviding() {
		return this.flags;
	}
	
	@Override
	public RLibPaths getRLibPaths() {
		return this.rLibPaths;
	}
	
	@Override
	public RPkgCompilation<? extends TInstalled> getInstalled() {
		return this.installed;
	}
	
}
