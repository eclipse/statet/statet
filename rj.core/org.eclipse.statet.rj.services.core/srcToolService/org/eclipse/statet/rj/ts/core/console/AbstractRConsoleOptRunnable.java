/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.ts.core.console;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.rj.ts.core.RTool;
import org.eclipse.statet.rj.ts.core.RToolService;
import org.eclipse.statet.rj.ts.core.util.RToolServiceWrapper;


@NonNullByDefault
public class AbstractRConsoleOptRunnable extends AbstractRConsoleRunnable {
	
	
	private static class ThisService extends RToolServiceWrapper implements RConsoleService {
		
		
		public ThisService(final RToolService service) {
			super(service);
		}
		
		
		@Override
		public boolean acceptNewConsoleCommand() {
			return true;
		}
		
		@Override
		public void submitToConsole(final String input,
				final ProgressMonitor m) throws StatusException {
			this.service.evalVoid("{\n" + input + "\n}", m); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
		@Override
		public void briefAboutToChange() {
		}
		
		@Override
		public void briefChanged(final int o) {
		}
		
	}
	
	
	/**
	 * @param typeId
	 * @param label
	 */
	public AbstractRConsoleOptRunnable(final String typeId, final String label) {
		super(typeId, label);
	}
	
	
	@Override
	public boolean canRunIn(final Tool tool) {
		return tool.isProvidingFeatureSet(RTool.R_SERVICE_FEATURE_ID);
	}
	
	
	@Override
	protected void run(final RToolService r, final ProgressMonitor m) throws StatusException {
		if (r instanceof RConsoleService) {
			run((RConsoleService)r, m);
		}
		else {
			run(new ThisService(r), m);
		}
	}
	
	
}
