/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.rj.renv.core.REnv;


@NonNullByDefault
public interface RTool extends Tool {
	
	
	/**
	 * Tool type of R instances.
	 * 
	 * @see Tool#getMainType()
	 */
	String TYPE= "R"; //$NON-NLS-1$
	
	
	String R_SERVICE_FEATURE_ID= "org.eclipse.statet.rj.services.RService"; //$NON-NLS-1$
	
	
	@Nullable REnv getREnv();
	
}
