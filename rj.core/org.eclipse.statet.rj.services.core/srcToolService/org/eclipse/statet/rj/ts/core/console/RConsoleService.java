/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.ts.core.console;

import static org.eclipse.statet.jcommons.util.StringUtils.U_LINEBREAK_PATTERN;

import java.util.List;
import java.util.regex.Matcher;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.console.ConsoleService;

import org.eclipse.statet.rj.services.RJServices;
import org.eclipse.statet.rj.ts.core.RToolService;
import org.eclipse.statet.rj.util.RCodeBuilder;


@NonNullByDefault
public interface RConsoleService extends RToolService, ConsoleService {
	
	
	int AUTO_CHANGE=                    0b0_0000_0000_0000_0001;
	
//	int DATA_CHANGE=                    0b0_0000_0000_0000_1000;
	
	int PACKAGE_CHANGE=                 0b0_0000_0000_0001_0000;
	
	
	/**
	 * Reports a status to the user
	 * 
	 * @param status the status to handle
	 * @param m the progress monitor of the current run (or a child)
	 */
	@Override
	default void handleStatus(final Status status, final ProgressMonitor m) {
		if (status == null || status.getSeverity() == Status.OK) {
			return;
		}
		try {
			final String br= "\n"; //$NON-NLS-1$
			final String br1= br + "    "; //$NON-NLS-1$
			final String br2= br1 + "    "; //$NON-NLS-1$
			final StringBuilder msg= new StringBuilder(br);
			boolean details= false;
			
			switch (status.getSeverity()) {
			case Status.INFO:
				msg.append("[INFO   ] "); //$NON-NLS-1$
				break;
			case Status.WARNING:
				msg.append("[WARNING] "); //$NON-NLS-1$
				break;
			case Status.ERROR:
				msg.append("[ERROR  ] "); //$NON-NLS-1$
				break;
			case Status.CANCEL:
				msg.append("[CANCEL ] "); //$NON-NLS-1$
				break;
			}
			msg.append(status.getMessage());
			
			List<Status> subList= null;
			if (status.isMultiStatus()) {
				details|= (status.getException() != null);
				subList= status.getChildren();
			}
			else {
				final Throwable exception= status.getException();
				if (exception instanceof StatusException) {
					subList= ImCollections.newList(((StatusException) exception).getStatus());
				}
				else if (exception != null) {
					details= true;
				}
			}
			if (subList != null && !subList.isEmpty()) {
				final Matcher linebreakMatcher= U_LINEBREAK_PATTERN.matcher(""); //$NON-NLS-1$
				for (final Status subStatus : subList) {
					msg.append(br1);
					msg.append(linebreakMatcher.reset(subStatus.getMessage()).replaceAll(br2));
					details|= (subStatus.isMultiStatus() || subStatus.getException() != null);
				}
			}
			if (details) {
//				msg.append(br);
//				msg.append("(more details available in application log)");
			}
			
			msg.append(br);
			
			submitToConsole(new RCodeBuilder().append("cat(").appendChar(msg).append(")").build(), //$NON-NLS-1$ //$NON-NLS-2$
					m );
		}
		catch (final Exception e) {
			CommonsRuntime.log(new ErrorStatus(RJServices.BUNDLE_ID,
					"An error occured when reporting status to R console.", e)); //$NON-NLS-1$
		}
	}
	
	
	void briefAboutToChange();
	void briefChanged(int flags);
	
	
}
