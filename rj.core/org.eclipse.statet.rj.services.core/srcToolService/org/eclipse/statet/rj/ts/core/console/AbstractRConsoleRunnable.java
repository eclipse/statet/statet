/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.ts.core.console;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.rj.services.RJServices;
import org.eclipse.statet.rj.ts.core.AbstractRToolRunnable;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class AbstractRConsoleRunnable extends AbstractRToolRunnable {
	
	
	/**
	 * @param typeId
	 * @param label
	 */
	public AbstractRConsoleRunnable(final String typeId, final String label) {
		super(typeId, label);
	}
	
	
	@Override
	public boolean canRunIn(final Tool tool) {
		return super.canRunIn(tool); // TODO add console feature check
	}
	
	
	@Override
	protected void run(final RToolService r, final ProgressMonitor m) throws StatusException {
		run((RConsoleService)r, m);
	}
	
	protected void run(final RConsoleService r,
			final ProgressMonitor m) throws StatusException {
	}
	
	
	protected static void checkNewCommand(final RConsoleService r,
			final ProgressMonitor m) throws StatusException {
		if (!r.acceptNewConsoleCommand()) {
			throw new StatusException(new ErrorStatus(RJServices.BUNDLE_ID,
					"Operation cancelled because another command is already started" +
					"in the console.", null ));
		}
	}
	
}
