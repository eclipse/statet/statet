/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.ts.core.util;

import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RReference;
import org.eclipse.statet.rj.services.FQRObject;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RGraphicCreator;
import org.eclipse.statet.rj.services.RPlatform;
import org.eclipse.statet.rj.ts.core.RTool;
import org.eclipse.statet.rj.ts.core.RToolService;


@NonNullByDefault
public class RToolServiceWrapper implements RToolService {
	
	
	protected final RToolService service;
	
	
	public RToolServiceWrapper(final RToolService service) {
		this.service= service;
	}
	
	
	@Override
	public RTool getTool() {
		return this.service.getTool();
	}
	
	@Override
	public RPlatform getPlatform() {
		return this.service.getPlatform();
	}
	
	
	@Override
	public void evalVoid(final String expression,
			final ProgressMonitor m) throws StatusException {
		this.service.evalVoid(expression, m);
	}
	
	@Override
	public void evalVoid(final String expression, final @Nullable RObject envir,
			final ProgressMonitor m) throws StatusException {
		this.service.evalVoid(expression, envir, m);
	}
	
	@Override
	public RObject evalData(final String expression,
			final ProgressMonitor m) throws StatusException {
		return this.service.evalData(expression, m);
	}
	
	@Override
	public RObject evalData(final String expression,
			final @Nullable String factoryId, final int options, final int depth,
			final ProgressMonitor m) throws StatusException {
		return this.service.evalData(expression, factoryId, options, depth, m);
	}
	
	@Override
	public RObject evalData(final String expression, final @Nullable RObject envir,
			final @Nullable String factoryId, final int options, final int depth,
			final ProgressMonitor m) throws StatusException {
		return this.service.evalData(expression, envir, factoryId, options, depth, m);
	}
	
	@Override
	public RObject evalData(final RReference reference,
			final ProgressMonitor m) throws StatusException {
		return this.service.evalData(reference, m);
	}
	
	@Override
	public RObject evalData(final RReference reference,
			final @Nullable String factoryId, final int options, final int depth,
			final ProgressMonitor m) throws StatusException {
		return this.service.evalData(reference, factoryId, options, depth, m);
	}
	
	@Override
	public @Nullable FQRObject<? extends RTool> findData(final String symbol, final @Nullable RObject env, final boolean inherits,
			final @Nullable String factoryId, final int options, final int depth,
			final ProgressMonitor m) throws StatusException {
		return this.service.findData(symbol, env, inherits, factoryId, options, depth, m);
	}
	
	@Override
	public void assignData(final String target, final RObject data,
			final ProgressMonitor m) throws StatusException {
		this.service.assignData(target, data, m);
	}
	
	
	@Override
	public void uploadFile(final InputStream in, final long length, final String fileName, final int options,
			final ProgressMonitor m) throws StatusException {
		this.service.uploadFile(in, length, fileName, options, m);
	}
	
	@Override
	public void downloadFile(final OutputStream out, final String fileName, final int options,
			final ProgressMonitor m) throws StatusException {
		this.service.downloadFile(out, fileName, options, m);
	}
	
	@Override
	public byte[] downloadFile(final String fileName, final int options,
			final ProgressMonitor m) throws StatusException {
		return this.service.downloadFile(fileName, options, m);
	}
	
	
	@Override
	public FunctionCall createFunctionCall(final String name) throws StatusException {
		return this.service.createFunctionCall(name);
	}
	
	@Override
	public RGraphicCreator createRGraphicCreator(final int options) throws StatusException {
		return this.service.createRGraphicCreator(options);
	}
	
	
}
