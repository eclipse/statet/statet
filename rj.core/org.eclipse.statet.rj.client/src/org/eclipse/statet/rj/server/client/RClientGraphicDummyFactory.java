/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.client;

import java.util.Collections;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.server.client.RClientGraphic.InitConfig;


@NonNullByDefault
public class RClientGraphicDummyFactory implements RClientGraphicFactory {
	
	
	public RClientGraphicDummyFactory() {
	}
	
	
	@Override
	public Map<String, ? extends @NonNull Object> getInitServerProperties() {
		return Collections.emptyMap();
	}
	
	@Override
	public RClientGraphic newGraphic(final int devId, final double w, final double h,
			final InitConfig config,
			final boolean active, final @Nullable RClientGraphicActions actions, final int options) {
		return new RClientGraphicDummy(devId, w, h);
	}
	
	@Override
	public void closeGraphic(final RClientGraphic graphic) {
	}
	
}
