/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.client;

import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.rj.graphic.core.RCircle;
import org.eclipse.statet.rj.graphic.core.RClipSetting;
import org.eclipse.statet.rj.graphic.core.RColorSetting;
import org.eclipse.statet.rj.graphic.core.RFillSetting;
import org.eclipse.statet.rj.graphic.core.RFontSetting;
import org.eclipse.statet.rj.graphic.core.RLine;
import org.eclipse.statet.rj.graphic.core.RLineSetting;
import org.eclipse.statet.rj.graphic.core.RPath;
import org.eclipse.statet.rj.graphic.core.RPolygon;
import org.eclipse.statet.rj.graphic.core.RPolyline;
import org.eclipse.statet.rj.graphic.core.RRaster;
import org.eclipse.statet.rj.graphic.core.RRect;
import org.eclipse.statet.rj.graphic.core.RText;
import org.eclipse.statet.rj.services.RService;


/**
 * Interface for the R side of (RJ) graphics at client layer.
 * 
 * The methods are called by the client protocol handler
 * (e.g. {@link AbstractRJComClient}) and must not be called at
 * another place.
 */
public interface RClientGraphic {
	
	
	class InitConfig {
		
		public int canvasColor;
		
	}
	
	
	/**
	 * Mask for fill rule
	 * <ul>
	 *   <li>{@link #FILL_WIND_EVEN_ODD}</li>
	 *   <li>{@link #FILL_WIND_NON_ZERO}</li>
	 * </ul>
	 * 
	 * @since 2.0
	 */
	int MASK_FILL_RULE= 0x3;
	
	/**
	 * Constant for "even-odd" winding rule for filling elements
	 * 
	 * @since 2.0
	 */
	int FILL_WIND_EVEN_ODD= 0x0;
	
	/**
	 * Constant for "non-zero" winding rule for filling elements
	 * 
	 * @since 2.0
	 */
	int FILL_WIND_NON_ZERO= 0x1;
	
	
	/**
	 * Returns the device id of R
	 * 
	 * @return the id
	 */
	int getDevId();
	
	/**
	 * 
	 * @param w the width of the graphic
	 * @param h the height of the graphic
	 * @param config initialization configuration
	 */
	void reset(double w, double h, InitConfig config);
	
	/**
	 * Sets if the graphic is the active device in R.
	 * 
	 * @param active <code>true</code> if active, otherwise <code>false</code>
	 */
	void setActive(boolean active);
	
	/**
	 * Returns if the graphic is the active device in R.
	 * 
	 * @return <code>true</code> if active, otherwise <code>false</code>
	 */
	boolean isActive();
	
	/**
	 * Sets the current mode signaled by R.
	 * <p>
	 * Values:
	 *     0= R stopped drawing
	 *     1= R started drawing
	 *     2= graphical input exists
	 * 
	 * @param mode the value constant
	 */
	void setMode(int mode);
	
	double[] computeSize();
	double[] computeFontMetric(int ch);
	double[] computeStringWidth(String txt);
	
	/**
	 * @see RClipSetting#RClipSetting(double, double, double, double)
	 */
	void addSetClip(double x0, double y0, double x1, double y1);
	/**
	 * @see RColorSetting#RColorSetting(int)
	 */
	void addSetColor(int color);
	/**
	 * @see RFillSetting#RFillSetting(int)
	 */
	void addSetFill(int color);
	/**
	 * @see RLineSetting#RLineSetting(int, double, byte, byte, float)
	 */
	void addSetLine(int type, float width, byte cap, byte join, float joinMiterLimit);
	/**
	 * @see RFontSetting#RFontSetting(String, int, double, double, double)
	 */
	void addSetFont(String family, int face, float pointSize, float lineHeight);
	/**
	 * @see RLine#RLine(double, double, double, double)
	 */
	void addDrawLine(double x0, double y0, double x1, double y1);
	/**
	 * @see RRect#RRect(double, double, double, double)
	 */
	void addDrawRect(double x0, double y0, double x1, double y1);
	/**
	 * @see RPolyline
	 */
	void addDrawPolyline(double[] x, double[] y);
	/**
	 * @see RPolygon
	 */
	void addDrawPolygon(double[] x, double[] y);
	/**
	 * @see RPath
	 */
	void addDrawPath(int[] n, double[] x, double[] y, int winding);
	/**
	 * @see RCircle#RCircle(double, double, double)
	 */
	void addDrawCircle(double x, double y, double r);
	/**
	 * @see RText
	 */
	void addDrawText(String txt, double x, double y, double rDeg, double hAdj);
	/**
	 * @see RRaster
	 */
	void addDrawRaster(byte[] imgData, boolean imgAlpha, int imgWidth, int imgHeight,
			double x, double y, double w, double h, double rDeg, boolean interpolate);
	
	byte[] capture(int width, int height);
	
	
	double[] runRLocator(RService r, ProgressMonitor m);
	
}
