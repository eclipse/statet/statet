/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.client;

import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.server.client.RClientGraphic.InitConfig;


/**
 * Factory interface used to create (and close) {@link RClientGraphic} objects.
 * 
 * The methods are called by the client protocol handler
 * ({@link AbstractRJComClient}) and must not be called at
 * another place.
 */
@NonNullByDefault
public interface RClientGraphicFactory {
	
	
	/**
	 * Indicates explicitly that the graphic will be added to a global graphic
	 * collection.
	 * 
	 * Typically that means, the graphic is shown in the default graphic view/window.
	 */
	int MANAGED_ON= 1 << 1;
	
	/**
	 * Indicates that the graphic will not be added to a global graphic collection.
	 * 
	 * Typically that means, the graphic isn't shown in the default graphic view/window.
	 */
	int MANAGED_OFF= 1 << 2;
	
	/**
	 * Indicates that the graphic will be disposed if closed in R.
	 */
	int R_CLOSE_ON= 1 << 3;
	
	/**
	 * Indicates that the graphic will not be disposed if closed in R.
	 */
	int R_CLOSE_OFF= 1 << 4;
	
	/**
	 * Returns a map with properties configuring the server
	 * The values must be serializable.
	 * 
	 * @return a map with keys and values of the properties
	 */
	Map<String, ? extends @NonNull Object> getInitServerProperties();
	
	/**
	 * Called if a new graphic is created in R (<code>dev.new()</code>).
	 * 
	 * @param devId
	 * @param w
	 * @param h
	 * @param config initialization configuration
	 * @param active
	 * @param actions
	 * @return a new graphic object
	 */
	@Nullable RClientGraphic newGraphic(int devId, double w, double h, final InitConfig config,
			boolean active, final @Nullable RClientGraphicActions actions, int options);
	
	/**
	 * Called if the graphic is closed by the R (<code>dev.off()</code>
	 * or R closed).
	 * 
	 * @param graphic the closed graphic
	 */
	void closeGraphic(final RClientGraphic graphic);
	
}
