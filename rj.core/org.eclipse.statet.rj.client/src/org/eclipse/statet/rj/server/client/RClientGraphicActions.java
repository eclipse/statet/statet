/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.client;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * Interface for (RJ) graphics offering actions which can be used
 * in graphic views/windows.
 */
public interface RClientGraphicActions {
	
	
	Object getRHandle();
	
	String getRLabel();
	
	
	Status resizeGraphic(final int devId, final Runnable beforeResize);
	
	Status closeGraphic(final int devId);
	
	
	void copy(final int devId, final String toDev, final String toDevFile, final String toDevArgs,
			final ProgressMonitor m) throws StatusException;
	
	double[] convertGraphic2User(final int devId, final double[] xy,
			final ProgressMonitor m) throws StatusException;
	double[] convertUser2Graphic(final int devId, final double[] xy,
			final ProgressMonitor m) throws StatusException;
	
}
