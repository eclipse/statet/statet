/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.client;

import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.rj.services.RService;


/**
 * A minimal {@link RClientGraphic} implementation.
 */
public class RClientGraphicDummy implements RClientGraphic {
	
	
	private final int devId;
	
	private boolean isActive;
	
	private double[] size;
	
	
	public RClientGraphicDummy(final int devId, final double w, final double h) {
		this.devId= devId;
		this.size= new double[] { w, h };
	}
	
	
	@Override
	public int getDevId() {
		return this.devId;
	}
	
	@Override
	public void reset(final double w, final double h, final InitConfig config) {
		this.size= new double[] { w, h };
	}
	
	@Override
	public void setMode(final int mode) {
	}
	
	@Override
	public void setActive(final boolean active) {
		this.isActive= active;
	}
	
	@Override
	public boolean isActive() {
		return this.isActive;
	}
	
	@Override
	public double[] computeSize() {
		return this.size;
	}
	
	@Override
	public double[] computeFontMetric(final int ch) {
		return null;
	}
	
	@Override
	public double[] computeStringWidth(final String txt) {
		return null;
	}
	
	@Override
	public void addSetClip(final double x0, final double y0, final double x1, final double y1) {
	}
	
	@Override
	public void addSetColor(final int color) {
	}
	
	@Override
	public void addSetFill(final int color) {
	}
	
	@Override
	public void addSetLine(final int type, final float width,
			final byte cap, final byte join, final float joinMiterLimit) {
	}
	
	@Override
	public void addSetFont(final String family, final int face, final float pointSize, final float lineHeight) {
	}
	
	@Override
	public void addDrawLine(final double x0, final double y0, final double x1, final double y1) {
	}
	
	@Override
	public void addDrawRect(final double x0, final double y0, final double x1, final double y1) {
	}
	
	@Override
	public void addDrawPolyline(final double[] x, final double[] y) {
	}
	
	@Override
	public void addDrawPolygon(final double[] x, final double[] y) {
	}
	
	@Override
	public void addDrawPath(final int[] n, final double[] x, final double[] y, final int winding) {
	}
	
	@Override
	public void addDrawCircle(final double x, final double y, final double r) {
	}
	
	@Override
	public void addDrawText(final String text,
			final double x, final double y, final double rDeg, final double hAdj) {
	}
	
	@Override
	public void addDrawRaster(final byte[] imgData, final boolean imgAlpha, final int imgWidth, final int imgHeight,
			final double x, final double y, final double w, final double h,
			final double rDeg, final boolean interpolate) {
	}
	
	@Override
	public byte[] capture(final int width, final int height) {
		return null;
	}
	
	
	@Override
	public double[] runRLocator(final RService r, final ProgressMonitor m) {
		return null;
	}
	
}
