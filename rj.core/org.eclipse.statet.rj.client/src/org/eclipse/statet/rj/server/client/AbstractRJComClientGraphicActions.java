/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.client;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.server.gr.Coord;
import org.eclipse.statet.rj.server.gr.GraOp;


public abstract class AbstractRJComClientGraphicActions implements RClientGraphicActions {
	
	
	public static interface Factory {
		
		
		AbstractRJComClientGraphicActions create(AbstractRJComClient rjs, Object rHandle);
		
	}
	
	
	protected final AbstractRJComClient rjs;
	
	
	protected AbstractRJComClientGraphicActions(final AbstractRJComClient rjs) {
		if (rjs == null) {
			throw new NullPointerException("rjs");
		}
		this.rjs= rjs;
	}
	
	
	public void doResizeGraphic(final int devId,
			final ProgressMonitor m) throws StatusException {
		this.rjs.execSyncGraphicOp(devId, GraOp.OP_REQUEST_RESIZE, m);
	}
	
	public void doCloseGraphic(final int devId,
			final ProgressMonitor m) throws StatusException {
		this.rjs.execSyncGraphicOp(devId, GraOp.OP_CLOSE, m);
	}
	
	
	@Override
	public void copy(final int devId, final String toDev, final String toDevFile, final String toDevArgs,
			final ProgressMonitor m) throws StatusException {
		final StringBuilder sb= new StringBuilder(64);
		sb.append("rj:::gr.copyGraphic(");
		sb.append("devNr=").append((devId + 1)).append("L,");
		sb.append("device=").append(toDev).append(",");
		if (toDevFile != null) {
			sb.append("file=\"").append(toDevFile).append("\",");
		}
		if (toDevArgs != null && toDevArgs.length() > 0) {
			sb.append(toDevArgs);
			sb.append(",");
		}
		sb.replace(sb.length()-1, sb.length(), ")");
		this.rjs.evalVoid(sb.toString(), null, m);
	}
	
	@Override
	public double[] convertGraphic2User(final int devId, final double[] xy,
			final ProgressMonitor m) throws StatusException {
		if (xy == null) {
			throw new NullPointerException("xy");
		}
		if (xy.length != 2) {
			throw new IllegalArgumentException("length of xy");
		}
		final Coord coord= (Coord) this.rjs.execSyncGraphicOp(devId, GraOp.OP_CONVERT_DEV2USER,
				new Coord(xy[0], xy[1]), m );
		if (coord != null && !Double.isNaN(coord.getX()) && !Double.isNaN(coord.getY())) {
			return new double[] { coord.getX(), coord.getY() };
		}
		return null;
	}
	
	@Override
	public double[] convertUser2Graphic(final int devId, final double[] xy,
			final ProgressMonitor m) throws StatusException {
		if (xy == null) {
			throw new NullPointerException("xy");
		}
		if (xy.length != 2) {
			throw new IllegalArgumentException("length of xy");
		}
		final Coord coord= (Coord) this.rjs.execSyncGraphicOp(devId, GraOp.OP_CONVERT_USER2DEV,
				new Coord(xy[0], xy[1]), m );
		if (coord != null && !Double.isNaN(coord.getX()) && !Double.isNaN(coord.getY())) {
			return new double[] { coord.getX(), coord.getY() };
		}
		return null;
	}
	
}
