/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.client;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


class DaemonThreadFactory implements ThreadFactory {
	
	
	final AtomicInteger threadNumber= new AtomicInteger(1);
	final String namePrefix;
	final String nameSuffix= "]";
	
	
	public DaemonThreadFactory(final String name) {
		this.namePrefix= name + " [Thread-";
	}
	
	
	@Override
	public Thread newThread(final Runnable r) {
		final Thread t= new Thread(r,
				this.namePrefix + this.threadNumber.getAndIncrement() + this.nameSuffix);
		
		t.setDaemon(true);
		t.setPriority(Thread.NORM_PRIORITY);
		return t;
	}
	
}
