 #=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#


## RJ Client (UI)

#' Custom Client Command
#' 
#' Sends an custom command to the client which executes the command, if it is supported
#' ("ext client command").
#' 
#' @param commandId the id (\code{character}) of the command.
#' @param args a list with command arguments (the meaning depends on command).
#' @param wait if R must wait until the command is executed (necessary if a return value is wanted).
#' @return The answer of the executed command, if \code{wait} is \code{TRUE} and the command could
#'     be executed, otherwise \code{NULL}.
#' 
#' @examplesIf rj:::inRJ
#'     # Execute the custom action "mynamespace/runAction1" registered in the client app:
#'     .client.execCommand("mynamespace/runAction1", list(arg1= 1), wait= FALSE) # always returns NULL
#'     
#'     # See also source of functions like 'showHelp' or 'chooseFile' from this package.
#' 
#' @export
.client.execCommand <- function(commandId, args= list(), wait= TRUE) {
	if (missing(commandId)) {
		stop("Missing param: commandId");
	}
	if (!is.list(args)) {
		stop("Illegal argument: args must be a list");
	}
	if (length(args) > 0) {
		args.names <- names(args);
		if (is.null(args.names) || any(is.na(args.names))) {
			stop("Illegal argument: args must be named");
		}
	}
	
	options <- 0L;
	if (wait) {
		options <- options + 1L;
	}
	.Call(C_rj_execJCommand, paste("ext", commandId, sep= ":"), args, options);
}

