 #=============================================================================#
 # Copyright (c) 2009, 2024 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#

import(utils, methods, graphics, grDevices)

export(.rj.tmp)
export(.client.execCommand)
export(.console.loadHistory, .console.saveHistory, .console.addtoHistory)
export(rj.GD, .rj.initGD)
export(rj.pager)
export(statet.help, statet.help.start, .statet.initDebug, .statet.initHelp)
export(statet.showHistory)
export(openInEditor, openWebBrowser, openPackageManager, chooseFile, showTextFiles)
export(dbg.updateTracepoints)
export(.renv.createConfigProperties)
