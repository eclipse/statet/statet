/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.services.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.util.RCodeBuilder;


@NonNullByDefault
public class RCodeBuilderTest {
	
	
	@Test
	public void reset() {
		assertEquals("",
				new RCodeBuilder()
					.append("ABC")
					.clear()
					.build() );
	}
	
	@Test
	@SuppressWarnings("null")
	public void append_CharSequence_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			new RCodeBuilder().append((CharSequence)null);
		});
	}
	
	@Test
	public void append_CharSequence() {
		assertEquals("ABC",
				new RCodeBuilder()
					.append((CharSequence)"ABC")
					.build() );
	}
	
	@Test
	@SuppressWarnings("null")
	public void append_String_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			new RCodeBuilder().append((String)null);
		});
	}
	
	@Test
	public void append_String() {
		assertEquals("ABC",
				new RCodeBuilder()
					.append("ABC")
					.build() );
		assertEquals("ABC\"XYZ\"",
				new RCodeBuilder()
					.append("ABC")
					.append("\"XYZ\"")
					.build() );
	}
	
	@Test
	@SuppressWarnings("null")
	public void append_CharSequenceStartEnd_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			new RCodeBuilder().append((CharSequence)null, 0, 0);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			new RCodeBuilder().append(new StringBuilder("ABC"), -1, 2);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			new RCodeBuilder().append(new StringBuilder("ABC"), 0, 4);
		});
	}
	
	@Test
	public void append_CharSequenceStartEnd() {
		assertEquals("AB2",
				new RCodeBuilder()
					.append("ABC", 0, 2)
					.append("0123456", 2, 3)
					.append("XYZ", 3, 3)
					.build() );
	}
	
	@Test
	public void append_Char() {
		assertEquals("a\u0247",
				new RCodeBuilder()
					.append('a')
					.append('\u0247')
					.build() );
	}
	
	@Test
	public void insert_Char_argCheck() {
		assertThrows(IndexOutOfBoundsException.class, () -> {
			new RCodeBuilder().insert(-1, 'A');
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			new RCodeBuilder().append("ABC").insert(4, 'A');
		});
	}
	
	@Test
	public void insert_Char() {
		assertEquals("1A2BC0",
				new RCodeBuilder()
					.insert(0, '1')
					.append("ABC")
					.insert(2, '2')
					.insert(5, '0')
					.build() );
	}
	
	@Test
	public void deleteEnd() {
		assertEquals("A",
				new RCodeBuilder()
					.append("ABC")
					.deleteEnd(2)
					.build() );
		assertEquals("",
				new RCodeBuilder()
					.deleteEnd(0)
					.build() );
	}
	
	
	@Test
	public void appendChar() {
		assertEquals("\"ABC\"",
				new RCodeBuilder()
					.appendChar("ABC")
					.build() );
		assertEquals("\"\\\"ABC\\\"\"",
				new RCodeBuilder()
					.appendChar("\"ABC\"")
					.build() );
		assertEquals("\"ABC\\\\\\\"'\nXYZ\"",
				new RCodeBuilder()
					.appendChar("ABC\\\"\'\nXYZ")
					.build() );
	}
	
	
	@Test
	public void toString_() {
		assertEquals("ABC",
				new RCodeBuilder()
					.append("ABC")
					.toString() );
	}
	
}
