/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import static org.eclipse.statet.rj.data.impl.RCharacterFix64Store.SEGMENT_LENGTH;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RCharacterStore;


@NonNullByDefault
public class RCharacterFix64StoreTest extends RCharacterStoreTest {
	
	
	public RCharacterFix64StoreTest() {
	}
	
	
	@Override
	protected RCharacterStore createStore(final CharCaseData data) {
		final int nSegments= getSegmentCount(data, SEGMENT_LENGTH);
		final var values= new @Nullable String [nSegments] @NonNull[];
		for (int i= 0, start= 0; i < values.length; i++) {
			final int end= Math.min(start + SEGMENT_LENGTH, data.length);
			final String[] encoded= Arrays.copyOfRange(data.values, start, end);
			for (int j= 0; j < encoded.length; j++) {
				if (data.nas[start + j]) {
					encoded[j]= null;
				}
			}
			values[i]= encoded;
			start= end;
		}
		return new RCharacterFix64Store(values);
	}
	
}
