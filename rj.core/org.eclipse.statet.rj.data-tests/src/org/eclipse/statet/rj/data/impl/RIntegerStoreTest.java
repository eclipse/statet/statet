/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.rj.data.impl.AbstractRStore.DEFAULT_LONG_DATA_SEGMENT_LENGTH;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;


@NonNullByDefault
public abstract class RIntegerStoreTest extends AbstractRStoreTest {
	
	
	static class IntCaseData extends CaseData<RIntegerStore> {
		
		final static Function<IntCaseData, RIntegerStore> REF_CONSTRUCTOR= new RInteger32StoreTest()::createStore;
		
		final int[] values;
		
		
		public IntCaseData(final String label, final int[] values, final boolean[] nas) {
			super(label, nas);
			assert (values.length == this.length);
			assert (nas.length == this.length);
			this.values= values;
			
			setReference(REF_CONSTRUCTOR.apply(this));
		}
		
		public IntCaseData(final String label, final int[] values) {
			super(label, values.length);
			assert (values.length == this.length);
			this.values= values;
			
			setReference(REF_CONSTRUCTOR.apply(this));
		}
		
	}
	
	protected static final List<IntCaseData> DEFAULT_DATA_SOURCES;
	static {
		final var datas= new ArrayList<IntCaseData>();
		
		datas.add(new IntCaseData("empty", new int[0]));
		
		datas.add(new IntCaseData("single-0", new int[] { 0 }));
		datas.add(new IntCaseData("single-1", new int[] { 1 }));
		datas.add(new IntCaseData("single-MIN", new int[] { RIntegerStore.MIN_INT }));
		datas.add(new IntCaseData("single-MAX", new int[] { RIntegerStore.MAX_INT }));
		datas.add(new IntCaseData("single-NA", new int[] { 0 }, new boolean[] { true }));
		
		{	final int[] values= new int[0x1FF];
			for (int i= 0; i < values.length; i++) {
				values[i]= i - 0xFF;
			}
			datas.add(new IntCaseData("seq", values));
		}
		{	final int[] values= new int[0x0FF];
			final boolean[] nas= new boolean[values.length];
			int i= 0;
			values[i++]= 0;
			values[i++]= RIntegerStore.MIN_INT;
			values[i++]= RIntegerStore.MAX_INT;
			nas[i++]= true;
			datas.add(new IntCaseData("special", values, nas));
		}
		
		{	final Random rand= new Random(16857);
			final int[] values= new int[100000];
			for (int i= 0; i < values.length; i++) {
				values[i]= rand.nextInt();
				if (values[i] == Integer.MIN_VALUE) {
					values[i]= 0;
				}
			}
			datas.add(new IntCaseData("rand100000", values));
		}
		if (isBigDataEnabled(4)) {
			final Random rand= new Random(46);
			final int[] values= new int[DEFAULT_LONG_DATA_SEGMENT_LENGTH * 2 + 13];
			for (int i= 0; i < values.length; i++) {
				values[i]= rand.nextInt();
				if (values[i] == Integer.MIN_VALUE) {
					values[i]= 0;
				}
			}
			datas.add(new IntCaseData("randMultiSeg", values));
		}
		
		DEFAULT_DATA_SOURCES= datas;
	}
	
	
	public RIntegerStoreTest() {
	}
	
	
	public static List<IntCaseData> provideCaseDatas() {
		return new ArrayList<>(DEFAULT_DATA_SOURCES);
	}
	
	protected abstract RIntegerStore createStore(final IntCaseData data);
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getStoreType(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		assertEquals(RStore.INTEGER, store.getStoreType());
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getBaseVectorRClassName(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		assertEquals(RObject.CLASSNAME_INTEGER, store.getBaseVectorRClassName());
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void length(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		checkLength(data, store);
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void isNA(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		checkIsNA(data, store);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void isMissing(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		checkIsMissingNonNum(data, store);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	@SuppressWarnings("boxing")
	public void getLogi(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final int i0= i;
			if (data.nas[i0]) {
				// undefined
			}
			else {
				final Boolean expected= Boolean.valueOf(data.values[i0] != 0);
				assertEquals(expected, store.getLogi(i0));
				assertEquals(expected, store.getLogi((long)i0));
			}
		}
		assertIndexOutOfBounds(data, store::getLogi, store::getLogi);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getInt(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final int i0= i;
			if (data.nas[i0]) {
				// undefined
			}
			else {
				final int expected= data.values[i0];
				assertEquals(expected, store.getInt(i0));
				assertEquals(expected, store.getInt((long)i0));
			}
		}
		assertIndexOutOfBounds(data, store::getInt, store::getInt);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getNum(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final int i0= i;
			if (data.nas[i0]) {
				// undefined
			}
			else {
				final double expected= data.values[i0];
				assertEquals(expected, store.getNum(i0));
				assertEquals(expected, store.getNum((long)i0));
			}
		}
		assertIndexOutOfBounds(data, store::getNum, store::getNum);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getChar(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final int i0= i;
			if (data.nas[i0]) {
				// undefined
			}
			else {
				final String expected= Integer.toString(data.values[i0]);
				assertEquals(expected, store.getChar(i0));
				assertEquals(expected, store.getChar((long)i0));
			}
		}
		assertIndexOutOfBounds(data, store::getChar, store::getChar);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void getRaw(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final int i0= i;
			if (data.nas[i0]) {
				// undefined
			}
			else if (data.values[i0] >= 0 && data.values[i0] < 0xFF) {
				final byte expected= (byte)(data.values[i0] & 0xFF);
				assertEquals(expected, store.getRaw(i0));
				assertEquals(expected, store.getRaw((long)i0));
			}
		}
		assertIndexOutOfBounds(data, store::getRaw, store::getRaw);
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void get(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		for (int i= 0; i < data.length; i++) {
			final int i0= i;
			if (data.nas[i0]) {
				assertNull(store.get(i0), storeDiffersAt(i0));
			}
			else {
				final Integer expected= Integer.valueOf(data.values[i0]);
				assertEquals(expected, store.get(i0));
				assertEquals(expected, store.get((long)i0));
			}
		}
		assertIndexOutOfBounds(data, store::get, store::get);
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void toArray(final IntCaseData data) {
		final RIntegerStore store= createStore(data);
		
		final @Nullable Integer[] array= store.toArray();
		assertEquals(data.values.length, array.length);
		for (int i= 0; i < data.length; i++) {
			final int i0= i;
			if (data.nas[i0]) {
				assertNull(store.get(i0), storeDiffersAt(i0));
			}
			else {
				final Integer expected= Integer.valueOf(data.values[i]);
				assertEquals(expected, array[i], arrayDiffersAt(i));
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void writeExternal(final IntCaseData data) throws IOException {
		final RIntegerStore store= createStore(data);
		
		if (store instanceof ExternalizableRStore) {
			final byte[] ser= writeExternal(store);
			assertArrayEquals(data.getExternalBytes(), ser);
		}
	}
	
}
