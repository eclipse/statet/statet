/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import static org.eclipse.statet.rj.data.impl.RComplexBFix64Store.SEGMENT_LENGTH;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RComplexStore;


@NonNullByDefault
public class RComplexBFix64StoreTest extends RComplexStoreTest {
	
	
	public RComplexBFix64StoreTest() {
	}
	
	
	@Override
	protected RComplexStore createStore(final CplxCaseData data) {
		final int nSegments= getSegmentCount(data, SEGMENT_LENGTH);
		final var reValues= new double [nSegments] @NonNull[];
		final var imValues= new double [nSegments] @NonNull[];
		final var nas= new boolean [nSegments] @NonNull[];
		for (int i= 0, start= 0; i < reValues.length; i++) {
			final int end= Math.min(start + SEGMENT_LENGTH, data.length);
			reValues[i]= Arrays.copyOfRange(data.realValues, start, end);
			imValues[i]= Arrays.copyOfRange(data.imaginaryValues, start, end);
			nas[i]= Arrays.copyOfRange(data.nas, start, end);
			start= end;
		}
		return new RComplexBFix64Store(reValues, imValues, nas);
	}
	
}
