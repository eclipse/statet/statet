/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import static org.eclipse.statet.rj.data.impl.RRawFix64Store.SEGMENT_LENGTH;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RRawStore;


@NonNullByDefault
public class RRawFix64StoreTest extends RRawStoreTest {
	
	
	public RRawFix64StoreTest() {
	}
	
	
	@Override
	protected RRawStore createStore(final RawCaseData data) {
		final int segments= getSegmentCount(data, SEGMENT_LENGTH);
		final var values= new byte [segments] @NonNull[];
		for (int i= 0, start= 0; i < values.length; i++) {
			final int end= Math.min(start + SEGMENT_LENGTH, data.length);
			values[i]= Arrays.copyOfRange(data.values, start, end);
			start= end;
		}
		return new RRawFix64Store(values);
	}
	
}
