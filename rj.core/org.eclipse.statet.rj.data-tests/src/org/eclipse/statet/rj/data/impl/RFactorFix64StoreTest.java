/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import static org.eclipse.statet.rj.data.impl.RIntegerFix64Store.SEGMENT_LENGTH;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RFactorStore;


@NonNullByDefault
public class RFactorFix64StoreTest extends RFactorStoreTest {
	
	
	public RFactorFix64StoreTest() {
	}
	
	
	@Override
	protected RFactorStore createStore(final FactorCaseData data) {
		final int nSegments= getSegmentCount(data, SEGMENT_LENGTH);
		final var values= new int [nSegments] @NonNull[];
		final var nas= new boolean [nSegments] @NonNull[];
		for (int i= 0, start= 0; i < values.length; i++) {
			final int end= Math.min(start + SEGMENT_LENGTH, data.length);
			values[i]= Arrays.copyOfRange(data.codes, start, end);
			nas[i]= Arrays.copyOfRange(data.nas, start, end);
			start= end;
		}
		return new RFactorFix64Store(values, nas,
				data.ordered,
				Arrays.copyOf(data.levels, data.levels.length) );
	}
	
}
