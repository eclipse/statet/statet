/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.data.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RFactorStore;


@NonNullByDefault
public class RFactor32StoreReadTest extends RFactorStoreTest {
	
	
	public RFactor32StoreReadTest() {
	}
	
	
	@Override
	protected RFactorStore createStore(final FactorCaseData data) {
		return readExternal(RFactor32Store::new, data.getExternalBytes());
	}
	
}
