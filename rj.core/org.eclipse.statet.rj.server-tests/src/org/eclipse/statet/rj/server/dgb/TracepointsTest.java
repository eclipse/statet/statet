/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.server.dgb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.server.dbg.Tracepoint;
import org.eclipse.statet.rj.server.dbg.Tracepoints;


@NonNullByDefault
public class TracepointsTest {
	
	
	public TracepointsTest() {
	}
	
	
	@ParameterizedTest
	@ValueSource(longs= { 0, 1234, 5938098098L, -1L })
	public void appendId(final long id) {
		final StringBuilder sb= new StringBuilder();
		Tracepoints.append(id, sb);
		assertEquals(String.format("%1$016X", id), sb.toString());
	}
	
	@Test
	public void toTypeName() {
		assertEquals("FB", Tracepoints.getTypeName(Tracepoint.TYPE_FB));
		assertEquals("LB", Tracepoints.getTypeName(Tracepoint.TYPE_LB));
		assertEquals("TB", Tracepoints.getTypeName(Tracepoint.TYPE_TB));
		assertEquals("EB", Tracepoints.getTypeName(Tracepoint.TYPE_EB));
		
		assertEquals("DELETED", Tracepoints.getTypeName(Tracepoint.TYPE_DELETED));
		
		assertEquals("unknown: 0x00001234", Tracepoints.getTypeName(0x0_0000_1234));
	}
	
	
}
