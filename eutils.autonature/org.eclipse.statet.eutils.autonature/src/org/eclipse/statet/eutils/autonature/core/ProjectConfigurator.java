/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.eutils.autonature.core;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;


public interface ProjectConfigurator {
	
	
	byte NOT_CONFIGURABLE= 0;
	byte ALREADY_CONFIGURED= 1;
	byte CONFIGURABLE= 2;
	
	
	byte check(IProject project, IProgressMonitor monitor);
	
	void configure(IProject project, IProgressMonitor monitor);
	
}
