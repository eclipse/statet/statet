/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ContentTypeConfig extends AutoConfig {
	
	
	private final String contentTypeId;
	
	private @Nullable String label;
	
	
	public ContentTypeConfig(final String contentTypeId, final ImList<Task> tasks) {
		super(ConfigManager.ON_FILE_CONTENT_CONTRIB + ':' + contentTypeId, tasks);
		this.contentTypeId= contentTypeId;
	}
	
	
	public String getContentTypeId() {
		return this.contentTypeId;
	}
	
	
	@Override
	public @Nullable String getLabel() {
		var label= this.label;
		if (label == null) {
			final IContentType contentType= Platform.getContentTypeManager().getContentType(this.contentTypeId);
			if (contentType != null) {
				label= contentType.getName();
				this.label= label;
			}
		}
		return label;
	}
	
}
