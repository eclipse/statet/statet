/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.eutils.autonature.core.ProjectConfigurator;


@NonNullByDefault
public class ConfiguratorTask extends Task {
	
	
	private final String label;
	
	private final @Nullable String natureId;
	
	private @Nullable IConfigurationElement configElement;
	
	private @Nullable ProjectConfigurator configurator;
	
	
	public ConfiguratorTask(final String label, final @Nullable String natureId,
			final IConfigurationElement configElement) {
		this.label= label;
		this.natureId= natureId;
		this.configElement= configElement;
	}
	
	
	private synchronized @Nullable ProjectConfigurator getProjectConfigurator() throws CoreException {
		final var configElement= this.configElement;
		if (configElement != null) {
			this.configElement= null;
			this.configurator= (ProjectConfigurator)configElement
					.createExecutableExtension(ConfigManager.CLASS_ATTR_NAME);
		}
		return this.configurator;
	}
	
	@Override
	public String getLabel() {
		return this.label;
	}
	
	@Override
	public boolean isAvailable() {
		return true;
	}
	
	@Override
	public boolean isSupported(final byte mode) {
		return (mode == ConfigManager.MANUAL_MODE);
	}
	
	@Override
	public byte check(final IProject project, final int flags, final SubMonitor m) throws CoreException {
		if (this.natureId != null) {
			try {
				if (project.hasNature(this.natureId)) {
					return ProjectConfigurator.ALREADY_CONFIGURED;
				}
			}
			catch (final CoreException e) {}
		}
		if ((flags & CONTENT_MATCH) == 0) {
			return ProjectConfigurator.NOT_CONFIGURABLE;
		}
		final ProjectConfigurator configurator= getProjectConfigurator();
		if (configurator == null) {
			return 0;
		}
		return configurator.check(project, m);
	}
	
	public void configure(final IProject project, final SubMonitor m) {
		final ProjectConfigurator configurator= this.configurator;
		if (configurator == null) {
			throw new RuntimeException();
		}
		configurator.configure(project, m);
	}
	
	
	@Override
	public String toString() {
		return "ConfiguratorTask '" + this.label + "'"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
