/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.eutils.autonature.core.ProjectConfigurator;


@NonNullByDefault
public class NatureTask extends Task {
	
	
	private static @Nullable NatureTask get(final List<NatureTask> tasks, final String natureId) {
		final int size= tasks.size();
		for (int i= 0; i < size; i++) {
			final NatureTask task= tasks.get(i);
			if (task.getNatureId() == natureId) {
				return task;
			}
		}
		return null;
	}
	
	public static class Missing extends NatureTask {
		
		
		public Missing(final String natureId) {
			super(natureId, null, ImCollections.emptyList());
		}
		
		
		@Override
		public boolean isAvailable() {
			return false;
		}
		
	}
	
	
	private final String natureId;
	
	private final String label;
	
	private final ImList<String> required;
	
	private final List<NatureTask> prev= new ArrayList<>(4);
	private final List<NatureTask> prevFlat= new ArrayList<>(8);
	
	
	public NatureTask(final String natureId, final @Nullable String label, final ImList<String> required) {
		this.natureId= natureId;
		this.label= (label != null && !label.isEmpty()) ? label : natureId;
		this.required= required;
	}
	
	
	public String getNatureId() {
		return this.natureId;
	}
	
	@Override
	public String getLabel() {
		return this.label;
	}
	
	@Override
	public boolean isAvailable() {
		return true;
	}
	
	
	public List<String> getRequiredNatureIds() {
		return this.required;
	}
	
	void addPrev(final NatureTask task) {
		if (!this.prev.contains(task)) {
			this.prev.add(task);
		}
	}
	
	void finish() {
		this.prevFlat.clear();
		for (int i= 0; i < this.prev.size(); i++) {
			this.prevFlat.add(this.prev.get(i));
		}
		for (int i= 0; i < this.prevFlat.size(); i++) {
			final List<NatureTask> tasks= this.prevFlat.get(i).prev;
			for (int j= 0; j < tasks.size(); j++) {
				final NatureTask task= tasks.get(j);
				if (task != this && !this.prevFlat.contains(task)) {
					this.prevFlat.add(task);
				}
			}
		}
	}
	
	boolean isSubsequentTo(final String natureId) {
		final NatureTask task= get(this.prevFlat, natureId);
		return (task != null && get(task.prevFlat, this.natureId) == null);
	}
	
	@Override
	public byte check(final IProject project, final int flags, final SubMonitor m) throws CoreException {
		return (project.hasNature(this.natureId)) ?
				ProjectConfigurator.ALREADY_CONFIGURED :
				ProjectConfigurator.CONFIGURABLE;
	}
	
	
	@Override
	public String toString() {
		return "NatureTask '" + this.natureId + "'"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
