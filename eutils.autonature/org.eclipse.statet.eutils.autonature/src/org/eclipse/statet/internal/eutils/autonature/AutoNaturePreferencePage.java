/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.osgi.service.prefs.BackingStoreException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils.CheckboxTableComposite;


@NonNullByDefault
public class AutoNaturePreferencePage extends PreferencePage implements IWorkbenchPreferencePage {
	
	
	private List<AutoConfig> configs;
	
	private Button enableButton;
	
	private CheckboxTableViewer entryViewer;
	
	
	@SuppressWarnings("null")
	public AutoNaturePreferencePage() {
	}
	
	
	@Override
	public void init(final @Nullable IWorkbench workbench) {
	}
	
	@Override
	protected Control createContents(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newCompositeGrid(1));
		
		{	this.enableButton= new Button(composite, SWT.CHECK);
			this.enableButton.setText("Enable automatic project &configuration.");
			this.enableButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		}
		
		LayoutUtils.addSmallFiller(composite, false);
		
		{	final Label label= new Label(composite, SWT.NONE);
			label.setText("Enable project configuration for:");
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			
			final CheckboxTableComposite tableComposite= UIUtils.createContentTypeTable(composite, 1);
			this.entryViewer= tableComposite.viewer;
		}
		
		Dialog.applyDialogFont(composite);
		
		loadConfigs();
		loadPrefs();
		
		return composite;
	}
	
	private void loadConfigs() {
		final ConfigManager configManager= Activator.getInstance().getConfigManager();
		final var configs= configManager.getConfigs(ConfigManager.AUTO_MODE);
		UIUtils.sortConfigs(configs);
		this.configs= configs;
		this.entryViewer.setInput(configs);
	}
	
	private void loadPrefs() {
		final IPreferencesService preferences= Platform.getPreferencesService();
		
		this.enableButton.setSelection(preferences.getBoolean(
				Activator.BUNDLE_ID, Activator.ENABLED_PREF_KEY, true, null ));
		
		final var checked= new ArrayList<AutoConfig>();
		for (final AutoConfig config : this.configs) {
			if (preferences.getBoolean(
					ConfigManager.PREF_QUALIFIER, config.getEnabledPrefKey(), false, null )) {
				checked.add(config);
			}
		}
		this.entryViewer.setCheckedElements(checked.toArray());
	}
	
	private void savePrefs(final boolean flush) {
		final var bundleNode= InstanceScope.INSTANCE.getNode(Activator.BUNDLE_ID);
		
		bundleNode.putBoolean(Activator.ENABLED_PREF_KEY, this.enableButton.getSelection());
		
		final var configsNode= InstanceScope.INSTANCE.getNode(ConfigManager.PREF_QUALIFIER);
		final List<Object> enabled= Arrays.asList(this.entryViewer.getCheckedElements());
		for (final AutoConfig config : this.configs) {
			configsNode.putBoolean(config.getEnabledPrefKey(), enabled.contains(config));
		}
		
		if (flush) {
			try {
				bundleNode.flush();
			}
			catch (final BackingStoreException e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.BUNDLE_ID, -1, 
						"An error occured when saving the autorun launch configuration.", e));
			}
		}
	}
	
	
	@Override
	protected void performDefaults() {
		this.enableButton.setSelection(true);
		
		final var configsNode= DefaultScope.INSTANCE.getNode(ConfigManager.PREF_QUALIFIER);
		final List<AutoConfig> checked= new ArrayList<>();
		for (final AutoConfig config : this.configs) {
			if (configsNode.getBoolean(config.getEnabledPrefKey(), false)) {
				checked.add(config);
			}
		}
		this.entryViewer.setCheckedElements(checked.toArray());
	}
	
	@Override
	protected void performApply() {
		savePrefs(true);
	}
	
	@Override
	public boolean performOk() {
		savePrefs(false);
		return true;
	}
	
}
