/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.eutils.autonature;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AutoConfig {
	
	
	static class Dummy extends AutoConfig {
		
		public Dummy(final String id) {
			super(id, ImCollections.emptyList());
		}
		
		@Override
		public @Nullable String getLabel() {
			return null;
		}
		
	}
	
	
	private final String id;
	
	private final String enabledPrefKey;
	
	private final ImList<Task> tasks;
	
	
	public AutoConfig(final String id, final ImList<Task> tasks) {
		this.id= id;
		this.enabledPrefKey= id + ".enabled";
		this.tasks= tasks;
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public String getEnabledPrefKey() {
		return this.enabledPrefKey;
	}
	
	public boolean isAvailable() {
		if (getLabel() == null) {
			return false;
		}
		for (final Task task : this.tasks) {
			if (!task.isAvailable()) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isSupported(final byte mode) {
		for (final Task task : this.tasks) {
			if (!task.isSupported(mode)) {
				return false;
			}
		}
		return true;
	}
	
	public abstract @Nullable String getLabel();
	
	public ImList<Task> getTasks() {
		return this.tasks;
	}
	
	
	@Override
	public String toString() {
		return this.id;
	}
	
}
