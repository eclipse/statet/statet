/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.springbootapp.rservi;

import java.util.NoSuchElementException;

import javax.security.auth.login.LoginException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rj.example.springbootapp.rservi.RServiConfiguration.ServeMode;
import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.servi.RServiUtils;


/**
 * RServiProvider for {@link ServeMode#EXTERN_POOL}.
 */
@NonNullByDefault
public class ExternPoolRServiProvider implements RServiProvider {
	
	
	private final String poolAddress;
	
	
	public ExternPoolRServiProvider(final RServiConfiguration config) {
		this.poolAddress= "//rservi-pool"; // TODO add config
	}
	
	
	@Override
	public RServi getRServi(final String task,
			final ProgressMonitor m) throws StatusException, NoSuchElementException, LoginException {
		return RServiUtils.getRServi(this.poolAddress, task);
	}
	
}
