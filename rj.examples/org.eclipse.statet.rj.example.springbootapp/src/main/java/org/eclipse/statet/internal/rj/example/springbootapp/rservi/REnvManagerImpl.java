/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.springbootapp.rservi;

import static org.eclipse.statet.rj.example.springbootapp.RjExampleApplication.BUNDLE_ID;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;

import org.eclipse.statet.rj.renv.core.BasicREnvConfiguration;
import org.eclipse.statet.rj.renv.core.BasicREnvManager;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.renv.core.REnvManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * This implementation loads only a single R environment configuration with the id 'default'.
 */
@Component
@NonNullByDefault
public class REnvManagerImpl extends BasicREnvManager implements REnvManager {
	
	
	private static  class REnvConfigurationImpl extends BasicREnvConfiguration {
	
		public REnvConfigurationImpl(final REnv rEnv, final Path stateDataRootDirectoryPath)
				throws IOException {
			super(rEnv, stateDataRootDirectoryPath);
			setFlags((LOCAL | SPEC_SETUP));
			
			final Properties p= new Properties();
			final Path propertiesFile= stateDataRootDirectoryPath.resolve("renv.properties");
			try (final var reader= Files.newBufferedReader(propertiesFile, StandardCharsets.UTF_8)) {
				p.load(reader);
			}
			load(p);
			resolvePaths();
		}
	
	}
	
	
	private final RServiConfiguration configuration;
	
	
	@Autowired
	public REnvManagerImpl(final RServiConfiguration configuration) {
		this.configuration= configuration;
		
		final List<REnvConfiguration> configs= new ArrayList<>();
		try {
			final Path dir= this.configuration.getRootDirectory();
			
			{	// Only single environment
				final REnvConfiguration config= readConfig(dir);
				if (config != null) {
					configs.add(config);
				}
			}
		}
		catch (final Exception e) {
			throw new RuntimeException("Failed to initialize R environment.", e);
		}
		
		synchronized (this) {
			final List<ActualREnv> envs= new ArrayList<>(configs.size());
			for (final REnvConfiguration config : configs) {
				final ActualREnv env= (ActualREnv)config.getREnv();
				updateEnv(env, config);
				envs.add(env);
			}
			setActualEnvs(ImCollections.toList(envs));
		}
	}
	
	private @Nullable REnvConfiguration readConfig(final Path path) {
		final String id= "default"; //$NON-NLS-1$
		try {
			final ActualREnv env= newEnv(id);
			final var config= new REnvConfigurationImpl(env, path);
			return config;
		}
		catch (final Exception e) {
			CommonsRuntime.log(new ErrorStatus(BUNDLE_ID,
					String.format("An error occurred when loading R environment configuration '%1$s'.", id),
					e ));
			return null;
		}
	}
	
	@Override
	protected ActualREnv newEnv(final String id) {
		if (!isValidId(id)) {
			throw new IllegalArgumentException("id=" + id);
		}
		return new ActualREnv(id.intern());
	}
	
}
