/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.springbootapp.rservi;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.rj.example.springbootapp.RjExampleApplication.BUNDLE_ID;

import org.eclipse.statet.jcommons.io.UriUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.rmi.RMIRegistry;
import org.eclipse.statet.jcommons.rmi.RMIRegistryManager;
import org.eclipse.statet.jcommons.runtime.ClassLoaderUtils;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.RjInvalidConfigurationException;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.renv.core.REnvConfiguration;
import org.eclipse.statet.rj.server.util.RJContext;
import org.eclipse.statet.rj.servi.node.RServiImpl;
import org.eclipse.statet.rj.servi.node.RServiNodeConfig;
import org.eclipse.statet.rj.servi.node.RServiNodeFactory;


@NonNullByDefault
public class LocalREnvConfiguration {
	
	
	private static interface NodeConfigureExt {
		
		void configure(RServiNodeConfig rServiConfig) throws StatusException;
		
	}
	
	private static class NoopConfigurator implements NodeConfigureExt {
		
		public NoopConfigurator() {
		}
		
		@Override
		public void configure(final RServiNodeConfig rServiConfig) throws StatusException {
		}
		
	}
	
	private static class ProtocolHandlerConfigurator implements NodeConfigureExt {
		
		private final String pkg;
		private final String classPathEntry;
		
		public ProtocolHandlerConfigurator(final String pkg) throws Exception {
			final Class<?> handlerClass= Class.forName(pkg + ".jar.Handler");
			final String url= ClassLoaderUtils.getClassLocationUrlString(handlerClass);
			this.pkg= pkg;
			this.classPathEntry= ClassLoaderUtils.toJClassPathEntryString(url);
		}
		
		@Override
		public void configure(final RServiNodeConfig rServiConfig) throws StatusException {
			rServiConfig.addToClasspath(this.classPathEntry);
			rServiConfig.setJavaArgs("-Djava.protocol.handler.pkgs=" + this.pkg //$NON-NLS-1$
					+ " -Dsun.misc.URLClassPath.disableJarChecking=true"); //$NON-NLS-1$
		}
		
	}
	
	private static final Object CLASSPATH_STATIC_LOCK= new Object();
	private static @Nullable NodeConfigureExt classPathConfigurator;
	
	
	private final REnv rEnv;
	
	private volatile @Nullable RServiNodeFactory nodeFactory;
	
	
	public LocalREnvConfiguration(final REnv rEnv) {
		this.rEnv= nonNullAssert(rEnv);
	}
	
	
	public REnv getREnv() {
		return this.rEnv;
	}
	
	
	private RServiNodeConfig createNodeConfig() throws StatusException {
		final REnvConfiguration rEnvConfig= this.rEnv.get(REnvConfiguration.class);
		if (rEnvConfig == null) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					String.format("The configuration for the R environment '%1$s' is missing.", this.rEnv.getId()) ));
		}
		
		final RServiNodeConfig rServiConfig= new RServiNodeConfig();
		rServiConfig.load(rEnvConfig);
		
		configureClassPath(rServiConfig);
		
		return rServiConfig;
	}
	
	private void configureClassPath(final RServiNodeConfig rServiConfig) throws StatusException {
		NodeConfigureExt configurator;
		synchronized (CLASSPATH_STATIC_LOCK) {
			configurator= classPathConfigurator;
			if (configurator == null) {
				try {
					configurator= new ProtocolHandlerConfigurator("org.springframework.boot.loader.net.protocol"); //$NON-NLS-1$
				}
				catch (final Exception e) {
					final String url= ClassLoaderUtils.getClassLocationUrlString(LocalPerTaskRServiProvider.class);
					if (UriUtils.isFileUrl(url)) {
						configurator= new NoopConfigurator();
					}
					else {
						try {
							// try classic loader
							configurator= new ProtocolHandlerConfigurator("org.springframework.boot.loader"); //$NON-NLS-1$
						}
						catch (final Exception ignore) {
							throw new StatusException(new ErrorStatus(BUNDLE_ID,
									"An error occurred when configuring protocol handler for fat jar.",
									e ));
						}
					}
				}
				classPathConfigurator= configurator;
			}
		}
		configurator.configure(rServiConfig);
	}
	
	
	protected RServiNodeFactory getNodeFactory(final ProgressMonitor m) throws RjException, StatusException {
		var nodeFactory= this.nodeFactory;
		if (nodeFactory == null) {
			nodeFactory= createNodeFactory(m);
			this.nodeFactory= nodeFactory;
		}
		return nodeFactory;
	}
	
	private RServiNodeFactory createNodeFactory(
			final ProgressMonitor m) throws StatusException, RjInvalidConfigurationException {
		final RJContext context= new RJContext();
		
		final RMIRegistry registry= RMIRegistryManager.INSTANCE.getEmbeddedPrivateRegistry(m);
		
		final RServiNodeFactory nodeFactory= RServiImpl.createLocalNodeFactory(this.rEnv.getId(), context);
		nodeFactory.setRegistry(registry);
		nodeFactory.setConfig(createNodeConfig());
		return nodeFactory;
	}
	
}
