/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.example.springbootapp;

import jakarta.annotation.PreDestroy;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.BasicAppEnvironment;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.runtime.bundle.Bundles;
import org.eclipse.statet.jcommons.status.InfoStatus;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.util.ACommonsLoggingStatusLogger;

import org.eclipse.statet.internal.rj.example.springbootapp.RjExampleInternals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackageClasses= { RjExampleInternals.class })
@ServletComponentScan(basePackageClasses= { RjExampleInternals.class })
@NonNullByDefault
public class RjExampleApplication extends BasicAppEnvironment {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.rj.example.springbootapp"; //$NON-NLS-1$
	
	
	public static void main(final @NonNull String[] args) {
		final SpringApplication app= new SpringApplication(RjExampleApplication.class);
		
		app.run(args);
	}
	
	
	@Autowired
	@SuppressWarnings("unused")
	private ApplicationContext appContext;
	
	
	public RjExampleApplication() throws StatusException {
		super(BUNDLE_ID, new ACommonsLoggingStatusLogger(), Bundles.createResolver());
		CommonsRuntime.init(this);
		
		log(new InfoStatus(BUNDLE_ID, "Application started."));
	}
	
	@PreDestroy
	protected void dispose() {
		onAppStopping();
		
		log(new InfoStatus(BUNDLE_ID, "Application stopped."));
	}
	
}
