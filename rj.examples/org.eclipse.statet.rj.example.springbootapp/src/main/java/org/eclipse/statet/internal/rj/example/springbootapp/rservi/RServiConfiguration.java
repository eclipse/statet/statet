/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.springbootapp.rservi;

import java.nio.file.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.annotation.Configuration;

import org.eclipse.statet.rj.example.springbootapp.RjExampleApplication;


@Configuration
@ConfigurationProperties(prefix= "rconfig")
@NonNullByDefault
public class RServiConfiguration {
	
	
	public static enum ServeMode {
		
		/**
		 * Mode to start an local R node when an RServi is requested.
		 */
		TASK_INSTANCE,
		
		/**
		 * Mode to use an internal RServi pool with local R nodes.
		 */
		INTERN_POOL,
		
		/**
		 * Mode to use an external RServi pool.
		 */
		REMOTE_POOL;
		
	}
	
	
	private @Nullable Path rootDir;
	
	private ServeMode serveMode= ServeMode.TASK_INSTANCE;
	
	
	public void setServeMode(final ServeMode mode) {
		this.serveMode= mode;
	}
	
	public ServeMode getServeMode() {
		return this.serveMode;
	}
	
	public void setPath(final @Nullable String path) {
		this.rootDir= (path != null) ? Path.of(path) : null;
	}
	
	public Path getRootDirectory() {
		var dir= this.rootDir;
		if (dir != null) {
			return dir;
		}
		else {
			final ApplicationHome applicationHome= new ApplicationHome(RjExampleApplication.class);
			dir= applicationHome.getDir().toPath();
			return dir.resolve("rconfig"); //$NON-NLS-1$
		}
	}
	
}
