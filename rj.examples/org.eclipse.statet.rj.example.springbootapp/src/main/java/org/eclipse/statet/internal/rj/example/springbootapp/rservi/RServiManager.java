/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.springbootapp.rservi;

import static org.eclipse.statet.rj.example.springbootapp.RjExampleApplication.BUNDLE_ID;

import java.util.NoSuchElementException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.rmi.RMIRegistryManager;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.renv.core.REnvManager;
import org.eclipse.statet.rj.server.RjsComConfig;
import org.eclipse.statet.rj.server.client.RClientGraphicDummyFactory;
import org.eclipse.statet.rj.servi.RServi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.eclipse.statet.rj.example.springbootapp.RjExampleApplication;


@Component
@NonNullByDefault
public class RServiManager {
	
	
	private final RServiConfiguration.ServeMode serveMode;
	private final RServiConfiguration config;
	
	private final REnvManager rEnvManager;
	
	private @Nullable RServiProvider rServiProvider;
	
	
	@Autowired
	public RServiManager(final RServiConfiguration config,
			final REnvManager rEnvManager,
			final RjExampleApplication app) {
		this.serveMode= config.getServeMode();
		this.config= config;
		this.rEnvManager= rEnvManager;
		
		app.addStoppingListener(this::dispose);
		init();
	}
	
	private void init() {
		RMIRegistryManager.INSTANCE.setEmbeddedPrivateMode(false);
		RjsComConfig.setProperty("rj.servi.graphicFactory", new RClientGraphicDummyFactory());
		
		switch (this.serveMode) {
		case TASK_INSTANCE:
			this.rServiProvider= new LocalPerTaskRServiProvider(createLocalRConfig());
			break;
		case INTERN_POOL:  {
				final var pool= new InternPoolRServiProvider(this.config, createLocalRConfig());
				pool.start(new NullProgressMonitor());
				this.rServiProvider= pool;
			}
			break;
		case REMOTE_POOL:
			this.rServiProvider= new ExternPoolRServiProvider(this.config);
			break;
		default:
			throw new RuntimeException("mode= " + this.serveMode.name());
		}
	}
	
	private synchronized void dispose() {
		final var rServiSupplier= this.rServiProvider;
		if (rServiSupplier != null) {
			this.rServiProvider= null;
			rServiSupplier.dispose();
		}
	}
	
	
	private LocalREnvConfiguration createLocalRConfig() {
		return new LocalREnvConfiguration(this.rEnvManager.get("default", null));
	}
	
	
	public RServi getRServi(final String task, final ProgressMonitor m) throws StatusException {
		final var rServiSupplier= this.rServiProvider;
		if (rServiSupplier == null) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"R service is not available." ));
		}
		try {
			return rServiSupplier.getRServi(task, m);
		}
		catch (final NoSuchElementException e) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"R currently not available, please try again later.", e ));
		}
		catch (final Exception e) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"R not available, please check the configuration.", e ));
		}
	}
	
}
