/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.springbootapp;

import java.io.IOException;
import java.util.Arrays;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rj.example.springbootapp.rservi.RServiManager;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RNumericStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.servi.RServi;

import org.springframework.beans.factory.annotation.Autowired;


@WebServlet(urlPatterns= { "/demo1" })
@NonNullByDefault
public class Demo1Servlet extends HttpServlet {
	
	
	private static final long serialVersionUID= 1L;
	
	
	@Autowired
	private RServiManager rServiManager;
	
	
	public Demo1Servlet() {
	}
	
	
	@Override
	protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			final RNumericStore numbers;
			final var m= new NullProgressMonitor();
			try (RServi r= this.rServiManager.getRServi("demo1", m)) {
				final RObject data= r.createFunctionCall("rnorm").addInt(100)
						.evalData(m);
				numbers= RDataUtils.checkRNumVector(data).getData();
			}
			
			resp.setContentType("text/html;charset=UTF-8"); //$NON-NLS-1$
			resp.setHeader("Cache-Control", "no-cache, must-revalidate"); //$NON-NLS-1$ //$NON-NLS-2$
			final var writer= resp.getWriter();
			writer.write("""
					<html><body>
					<h1>StatET RJ - Demo 1</h1>
					<h4>Random Numbers:</h4>
					""" );
			writer.write(Arrays.toString(numbers.toArray()));
			writer.write("""
					</body></html>
					""" );
		}
		catch (final StatusException | UnexpectedRDataException e) {
			throw new ServletException(e);
		}
	}
	
}
