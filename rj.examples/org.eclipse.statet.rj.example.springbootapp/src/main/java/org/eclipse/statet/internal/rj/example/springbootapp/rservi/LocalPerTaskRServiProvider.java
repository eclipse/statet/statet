/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.springbootapp.rservi;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.concurrent.atomic.AtomicLong;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rj.example.springbootapp.rservi.RServiConfiguration.ServeMode;
import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.servi.RServiUtils;
import org.eclipse.statet.rj.servi.node.RServiImpl;
import org.eclipse.statet.rj.servi.node.RServiNodeFactory;
import org.eclipse.statet.rj.servi.node.RServiNodeManager;
import org.eclipse.statet.rj.services.util.RServiceWrapper;


/**
 * RServiProvider for {@link ServeMode#TASK_INSTANCE}.
 */
@NonNullByDefault
public class LocalPerTaskRServiProvider implements RServiProvider {
	
	
	private static class RInstanceServi extends RServiceWrapper implements RServi {
		
		
		private final RServiNodeManager localR;
		
		
		public RInstanceServi(final RServiNodeManager localR, final RServi rServi) {
			super(rServi);
			this.localR= localR;
		}
		
		
		@Override
		public boolean isClosed() {
			return ((RServi)this.service).isClosed();
		}
		
		@Override
		public void close() throws StatusException {
			try {
				((RServi)this.service).close();
			}
			finally {
				this.localR.stop();
			}
		}
		
	}
	
	private static final AtomicLong COUNTER= new AtomicLong(1);
	
	
	private final LocalREnvConfiguration rConfig;
	
	private final String id;
	
	
	public LocalPerTaskRServiProvider(final LocalREnvConfiguration config) {
		this.rConfig= config;
		this.id= config.getREnv().getId();
	}
	
	
	private RServiNodeManager createR(final ProgressMonitor m) throws StatusException, RjException {
		final RServiNodeFactory nodeFactory= this.rConfig.getNodeFactory(m);
		
		return RServiImpl.createNodeManager(this.id + '-' + COUNTER.getAndIncrement(),
				nonNullAssert(nodeFactory.getRegistry()), nodeFactory );
	}
	
	@Override
	public RServi getRServi(final String task,
			final ProgressMonitor m) throws StatusException, RjException {
		final String key= this.id + "->" + task;
		
		final RServiNodeManager localR= createR(m);
		final RServi rServi= RServiUtils.getRServi(localR, key);
		return new RInstanceServi(localR, rServi);
	}
	
}
