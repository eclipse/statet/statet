/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.example.springbootapp.rservi;

import static org.eclipse.statet.rj.example.springbootapp.RjExampleApplication.BUNDLE_ID;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.rmi.RemoteException;
import java.util.NoSuchElementException;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rj.example.springbootapp.rservi.RServiConfiguration.ServeMode;
import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.servi.node.RServiImpl;
import org.eclipse.statet.rj.servi.pool.PoolConfig;
import org.eclipse.statet.rj.servi.pool.RServiPoolManager;


/**
 * RServiProvider for {@link ServeMode#INTERN_POOL}.
 */
@NonNullByDefault
public class InternPoolRServiProvider implements RServiProvider {
	
	
	private final String id;
	
	private final RServiConfiguration config;
	private final LocalREnvConfiguration localRConfig;
	
	private final RServiPoolManager poolManager;
	
	
	public InternPoolRServiProvider(final RServiConfiguration config, final LocalREnvConfiguration rConfig) {
		this.config= config;
		this.id= rConfig.getREnv().getId();
		this.localRConfig= rConfig;
		
		this.poolManager= RServiImpl.createPool(this.id, null);
	}
	
	
	private PoolConfig getPoolConfig() {
		final var configFile= this.config.getRootDirectory().resolve("pool.config");
		final PoolConfig poolConfig= new PoolConfig();
		try {
			if (Files.isReadable(configFile)) {
				final Properties p= new Properties();
				try (final var reader= Files.newBufferedReader(configFile, StandardCharsets.UTF_8)) {
					p.load(reader);
				}
				poolConfig.load(p);
			}
		}
		catch (final IOException e) {
			CommonsRuntime.log(new ErrorStatus(BUNDLE_ID, "An error occurred when loading the pool configuration.", e));
		}
		return poolConfig;
	}
	
	public void start(final ProgressMonitor m) {
		try {
			this.poolManager.setConfig(getPoolConfig());
			this.poolManager.addNodeFactory(this.localRConfig.getNodeFactory(m));
			
			this.poolManager.init();
		}
		catch (final RjException | StatusException e) {
			CommonsRuntime.log(new ErrorStatus(BUNDLE_ID, "An error occurred when starting the RServi pool manager.", e));
		}
	}
	
	@Override
	public void dispose() {
		try {
			this.poolManager.stop(0);
		}
		catch (final RjException e) {
			CommonsRuntime.log(new ErrorStatus(BUNDLE_ID, "An error occurred when stopping the RServi pool manager.", e));
		}
	}
	
	
	@Override
	public RServi getRServi(final String task,
			final ProgressMonitor m) throws NoSuchElementException, LoginException, RemoteException, RjException {
		return this.poolManager.getPool().getRServi(task, null);
	}
	
}
