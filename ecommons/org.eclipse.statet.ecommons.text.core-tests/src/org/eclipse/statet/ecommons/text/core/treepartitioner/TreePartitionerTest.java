/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import static org.eclipse.statet.ecommons.text.core.treepartitioner.TestPartitionNodeType.DEFAULT_ROOT;
import static org.eclipse.statet.ecommons.text.core.treepartitioner.TestPartitionNodeType.T1;
import static org.eclipse.statet.ecommons.text.core.treepartitioner.TestPartitionNodeType.T2;
import static org.eclipse.statet.ecommons.text.core.treepartitioner.TestPartitionNodeType.T3;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class TreePartitionerTest {
	
	protected static void assertTypedRegion(final int offset, final int end, final String type, final ITypedRegion actual) {
		assertEquals(offset, actual.getOffset(), "offset");
		assertEquals(end - offset, actual.getLength(), "length");
		assertEquals(type, actual.getType(), "type");
	}
	
	protected static String format(final @Nullable String elementLabel, final String propertyLabel) {
		if (elementLabel == null) {
			return propertyLabel;
		}
		return elementLabel + '.' + propertyLabel;
	}
	
	protected static void assertTypedRegion(final ITypedRegion expected, final ITypedRegion actual,
			final @Nullable String elementLabel) {
		assertEquals(expected.getOffset(), actual.getOffset(),
				() -> format(elementLabel, "offset") );
		assertEquals(expected.getLength(), actual.getLength(),
				() -> format(elementLabel, "length") );
		assertEquals(expected.getType(), actual.getType(),
				() -> format(elementLabel, "type") );
	}
	
	protected static void assertTypedRegions(final ImList<? extends ITypedRegion> expected, final @NonNull ITypedRegion[] actual) {
		assertEquals(expected.size(), actual.length, "partitions.length");
		
		for (int i= 0; i < expected.size(); i++) {
			final int i0= i;
			assertTypedRegion(expected.get(i0), actual[i0],
					String.format("partitions[%1$s]", i0) );
		}
	}
	
	
	private static class TestSet {
		
		final String content;
		
		final ImList<ExpectedPartition> partitions;
		
		final ImList<ExpectedPartition> zeroLengthPartitions;
		
		
		public TestSet(final String content,
				final ImList<ExpectedPartition> partitions, final ImList<ExpectedPartition> zeroLengthPartitions) {
			this.content= content;
			this.partitions= partitions;
			this.zeroLengthPartitions= zeroLengthPartitions;
		}
		
		
	}
	
	
	private final TestSet TEST_1= new TestSet("docu     ment{type 1} [type 2](type 3)docu     ment",
			ImCollections.newList(
					new ExpectedPartition(0, 13, DEFAULT_ROOT),
					new ExpectedPartition(13, 8, T1),
					new ExpectedPartition(21, 1, DEFAULT_ROOT),
					new ExpectedPartition(22, 8, T2),
					new ExpectedPartition(30, 8, T3),
					new ExpectedPartition(38, 13, DEFAULT_ROOT) ),
			ImCollections.newList(
					new ExpectedPartition(0, 13, DEFAULT_ROOT, true, true),
					new ExpectedPartition(13, 8, T1, false, false),
					new ExpectedPartition(21, 1, DEFAULT_ROOT, true, true),
					new ExpectedPartition(22, 8, T2, false, false),
					new ExpectedPartition(30, 0, DEFAULT_ROOT, true, true),
					new ExpectedPartition(30, 8, T3, false, false),
					new ExpectedPartition(38, 13, DEFAULT_ROOT, true, true) )
	);
	
	private final TestSet TEST_NESTED= new TestSet("docu     ment{type 1[type 2(type 3)(type 3)]type 1}",
			ImCollections.newList(
					new ExpectedPartition(0, 13, DEFAULT_ROOT),
					new ExpectedPartition(13, 7, T1),
					new ExpectedPartition(20, 7, T2),
					new ExpectedPartition(27, 8, T3),
					new ExpectedPartition(35, 8, T3),
					new ExpectedPartition(43, 1, T2),
					new ExpectedPartition(44, 7, T1) ),
			ImCollections.newList(
					new ExpectedPartition(0, 13, DEFAULT_ROOT, true, true),
					new ExpectedPartition(13, 7, T1, false, true),
					new ExpectedPartition(20, 7, T2, false, true),
					new ExpectedPartition(27, 8, T3, false, false),
					new ExpectedPartition(35, 0, T2, true, true),
					new ExpectedPartition(35, 8, T3, false, false),
					new ExpectedPartition(43, 1, T2, true, false),
					new ExpectedPartition(44, 7, T1, true, false),
					new ExpectedPartition(51, 0, DEFAULT_ROOT, true, true) )
	);
	
	private final TestSet TEST_START_TYPE= new TestSet("type 1} [type 2](type 3)docu     ment",
			ImCollections.newList(
					new ExpectedPartition(0, 7, T1),
					new ExpectedPartition(7, 1, DEFAULT_ROOT),
					new ExpectedPartition(8, 8, T2),
					new ExpectedPartition(16, 8, T3),
					new ExpectedPartition(24, 13, DEFAULT_ROOT) ),
			ImCollections.newList(
					new ExpectedPartition(0, 0, DEFAULT_ROOT, true, true), // TODO
					new ExpectedPartition(0, 7, T1, false, false),
					new ExpectedPartition(7, 1, DEFAULT_ROOT, true, true),
					new ExpectedPartition(8, 8, T2, false, false),
					new ExpectedPartition(16, 0, DEFAULT_ROOT, true, true),
					new ExpectedPartition(16, 8, T3, false, false),
					new ExpectedPartition(24, 13, DEFAULT_ROOT, true, true) )
	);
	
	
	protected IDocument doc= nonNullLateInit();
	protected TreePartitioner partitioner= nonNullLateInit();
	
	
	@BeforeEach
	public void setUp() {
		this.doc= new Document();
		this.partitioner= new TreePartitioner("default",
				new TestPartitionNodeScanner(),
				TestPartitionNodeType.CONTENT_TYPES_IDS );
		this.doc.setDocumentPartitioner(this.partitioner);
		this.partitioner.connect(this.doc);
	}
	
	
	@Test
	public void computePartitioning_Test1() throws BadLocationException {
		this.doc.set(this.TEST_1.content);
		assertTypedRegions(this.TEST_1.partitions,
				this.partitioner.computePartitioning(0, this.doc.getLength(), false) );
	}
	
	@Test
	public void computePartitioningZeroLength_Test1() throws BadLocationException {
		this.doc.set(this.TEST_1.content);
		assertTypedRegions(this.TEST_1.zeroLengthPartitions,
				this.partitioner.computePartitioning(0, this.doc.getLength(), true) );
	}
	
	@Test
	public void getPartition_Test1() throws BadLocationException {
		this.doc.set(this.TEST_1.content);
		assertGetPartition(this.TEST_1.partitions, false);
	}
	
	@Test
	public void getPartitionZeroLength_Test1() throws BadLocationException {
		this.doc.set(this.TEST_1.content);
		assertGetPartition(this.TEST_1.zeroLengthPartitions, true);
	}
	
	
	@Test
	public void computePartitiong_Nested() {
		this.doc.set(this.TEST_NESTED.content);
		assertTypedRegions(this.TEST_NESTED.partitions,
				this.partitioner.computePartitioning(0, this.doc.getLength(), false) );
	}
	
	@Test
	public void computePartitiongZeroLength_Nested() {
		this.doc.set(this.TEST_NESTED.content);
		assertTypedRegions(this.TEST_NESTED.zeroLengthPartitions,
				this.partitioner.computePartitioning(0, this.doc.getLength(), true) );
	}
	
	@Test
	public void getPartition_Nested() throws BadLocationException {
		this.doc.set(this.TEST_NESTED.content);
		assertGetPartition(this.TEST_NESTED.partitions, false);
	}
	
	@Test
	public void getPartitionZeroLength_Nested() throws BadLocationException {
		this.doc.set(this.TEST_NESTED.content);
		assertGetPartition(this.TEST_NESTED.zeroLengthPartitions, true);
	}
	
	
	@Test
	public void computePartitiong_StartType() {
		this.partitioner.setStartType(TestPartitionNodeType.T1);
		this.doc.set(this.TEST_START_TYPE.content);
		assertTypedRegions(this.TEST_START_TYPE.partitions,
				this.partitioner.computePartitioning(0, this.doc.getLength(), false) );
	}
	
	@Test
	public void computePartitiongZeroLength_StartType() {
		this.partitioner.setStartType(TestPartitionNodeType.T1);
		this.doc.set(this.TEST_START_TYPE.content);
		assertTypedRegions(this.TEST_START_TYPE.zeroLengthPartitions,
				this.partitioner.computePartitioning(0, this.doc.getLength(), true) );
	}
	
	@Test
	public void getPartition_StartType() throws BadLocationException {
		this.partitioner.setStartType(TestPartitionNodeType.T1);
		this.doc.set(this.TEST_START_TYPE.content);
		assertGetPartition(this.TEST_START_TYPE.partitions, false);
	}
	
	@Test
	public void getPartitionZeroLength_StartType() throws BadLocationException {
		this.partitioner.setStartType(TestPartitionNodeType.T1);
		this.doc.set(this.TEST_START_TYPE.content);
		assertGetPartition(this.TEST_START_TYPE.zeroLengthPartitions, true);
	}
	
	protected void assertGetPartition(final ImList<? extends ExpectedPartition> expected, final boolean zeroLength) {
		for (int i= 0; i < expected.size(); i++) {
			final ExpectedPartition iPartition= expected.get(i);
			final int start= (iPartition.prefereAtStart()) ?
					iPartition.getOffset() :
					(iPartition.getOffset() + 1);
			final int end= (iPartition.prefereAtEnd()) ?
					(iPartition.getOffset() + iPartition.getLength() + 1) :
					(iPartition.getOffset() + iPartition.getLength());
			assertGetPartition(iPartition, start, end, zeroLength);
		}
	}
	
	protected void assertGetPartition(final ITypedRegion expected,
			final int startOffset, final int endOffset, final boolean zeroLength) {
		for (int offset= startOffset; offset < endOffset; offset++) {
			assertTypedRegion(expected, this.partitioner.getPartition(offset, zeroLength),
					String.format("partition at offset= %1$s ", offset) );
		}
	}
	
}
