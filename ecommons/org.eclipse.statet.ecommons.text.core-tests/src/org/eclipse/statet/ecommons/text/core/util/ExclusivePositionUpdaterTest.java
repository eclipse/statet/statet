/*=============================================================================#
 # Copyright (c) 2005, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jface.text: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IPositionUpdater;
import org.eclipse.jface.text.Position;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ExclusivePositionUpdaterTest {
	
	private static final String CATEGORY= "testcategory";
	
	
	private IPositionUpdater updater= nonNullLateInit();
	private Position pos= nonNullLateInit();
	private IDocument doc= nonNullLateInit();
	
	
	public ExclusivePositionUpdaterTest() {
	}
	
	
	@BeforeEach
	public void setUp() throws Exception {
		this.updater= new ExclusivePositionUpdater(CATEGORY);
		this.doc= new Document("ccccccccccccccccccccccccccccccccccccccccccccc");
		this.pos= new Position(5, 5);
		this.doc.addPositionUpdater(this.updater);
		this.doc.addPositionCategory(CATEGORY);
		this.doc.addPosition(CATEGORY, this.pos);
	}
	
	@AfterEach
	public void tearDown() throws Exception {
		this.doc.removePositionUpdater(this.updater);
		this.doc.removePositionCategory(CATEGORY);
	}
	
	
	// Delete, ascending by offset, length:
	
	@Test
	public void deleteBefore() throws BadLocationException {
		this.doc.replace(2, 2, "");
		assertEquals(3, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteRightBefore() throws BadLocationException {
		this.doc.replace(3, 2, "");
		assertEquals(3, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteOverLeftBorder() throws BadLocationException {
		this.doc.replace(3, 6, "");
		assertEquals(3, this.pos.offset);
		assertEquals(1, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteOverLeftBorderTillRight() throws BadLocationException {
		this.doc.replace(4, 6, "");
		assertTrue(this.pos.isDeleted);
	}
	
	@Test
	public void deleted() throws BadLocationException {
		this.doc.replace(4, 7, "");
		assertTrue(this.pos.isDeleted);
	}
	
	@Test
	public void deleteAtOffset() throws BadLocationException {
		this.doc.replace(5, 1, "");
		assertEquals(5, this.pos.offset);
		assertEquals(4, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteAtOffset2() throws BadLocationException {
		this.doc.replace(5, 2, "");
		assertEquals(5, this.pos.offset);
		assertEquals(3, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteAtOffsetTillRight() throws BadLocationException {
		this.doc.replace(5, 5, "");
		assertEquals(5, this.pos.offset);
		assertEquals(0, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteAtOffsetOverRightBorder() throws BadLocationException {
		this.doc.replace(5, 6, "");
		assertTrue(this.pos.isDeleted);
	}
	
	@Test
	public void deleteWithin() throws BadLocationException {
		this.doc.replace(6, 2, "");
		assertEquals(5, this.pos.offset);
		assertEquals(3, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteAtRight() throws BadLocationException {
		this.doc.replace(8, 2, "");
		assertEquals(5, this.pos.offset);
		assertEquals(3, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteOverRightBorder() throws BadLocationException {
		this.doc.replace(9, 2, "");
		assertEquals(5, this.pos.offset);
		assertEquals(4, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteRightAfter() throws BadLocationException {
		this.doc.replace(10, 2, "");
		assertEquals(5, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void deleteAfter() throws BadLocationException {
		this.doc.replace(20, 2, "");
		assertEquals(5, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	
	// Add, ascending by offset:
	
	@Test
	public void addBefore() throws BadLocationException {
		this.doc.replace(2, 0, "yy");
		assertEquals(7, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void addRightBefore() throws BadLocationException {
		this.doc.replace(5, 0, "yy");
		assertEquals(7, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void addWithin() throws BadLocationException {
		this.doc.replace(6, 0, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(7, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void addWithin2() throws BadLocationException {
		this.doc.replace(9, 0, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(7, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void addRightAfter() throws BadLocationException {
		this.doc.replace(10, 0, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void addAfter() throws BadLocationException {
		this.doc.replace(20, 0, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	
	// Replace, ascending by offset, length:
	
	@Test
	public void replaceBefore() throws BadLocationException {
		this.doc.replace(2, 2, "y");
		assertEquals(4, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceRightBefore() throws BadLocationException {
		this.doc.replace(2, 3, "y");
		assertEquals(3, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceLeftBorder() throws BadLocationException {
		this.doc.replace(4, 2, "yy");
		assertEquals(6, this.pos.offset);
		assertEquals(4, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceLeftBorderTillRight() throws BadLocationException {
		this.doc.replace(4, 6, "yy");
		assertTrue(this.pos.isDeleted);
	}
	
	@Test
	public void replaced1() throws BadLocationException {
		this.doc.replace(4, 7, "yy");
		assertTrue(this.pos.isDeleted);
	}
	
	@Test
	public void replaced2() throws BadLocationException {
		this.doc.replace(4, 7, "yyyyyyy");
		assertTrue(this.pos.isDeleted);
	}
	
	@Test
	public void replaced3() throws BadLocationException {
		this.doc.replace(4, 7, "yyyyyyyy");
		assertTrue(this.pos.isDeleted);
	}
	
	@Test
	public void replaceAtOffset1() throws BadLocationException {
		this.doc.replace(5, 1, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(6, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceAtOffset2() throws BadLocationException {
		this.doc.replace(5, 4, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(3, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceAtOffsetTillRight() throws BadLocationException {
		this.doc.replace(5, 5, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(2, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceAtRight() throws BadLocationException {
		this.doc.replace(6, 4, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(3, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceRightBorder() throws BadLocationException {
		this.doc.replace(9, 2, "yy");
		assertEquals(5, this.pos.offset);
		assertEquals(4, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceRightAfter() throws BadLocationException {
		this.doc.replace(10, 2, "y");
		assertEquals(5, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
	@Test
	public void replaceAfter() throws BadLocationException {
		this.doc.replace(20, 2, "y");
		assertEquals(5, this.pos.offset);
		assertEquals(5, this.pos.length);
		assertFalse(this.pos.isDeleted);
	}
	
}
