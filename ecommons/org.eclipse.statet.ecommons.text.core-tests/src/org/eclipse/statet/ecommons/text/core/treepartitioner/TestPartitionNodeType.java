/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class TestPartitionNodeType extends BasicPartitionNodeType {
	
	
	public static final String DEFAULT_ID= "DEFAULT";
	public static final String T1_ID= "{}";
	public static final String T2_ID= "[]";
	public static final String T3_ID= "()";
	
	
	public static ImList<String> CONTENT_TYPES_IDS= ImCollections.newList(
			DEFAULT_ID,
			T1_ID, T2_ID, T3_ID
	);
	
	
	public static final TestPartitionNodeType DEFAULT_ROOT= new TestPartitionNodeType(DEFAULT_ID, 0);
	public static final TestPartitionNodeType T1= new TestPartitionNodeType(T1_ID, 1);
	public static final TestPartitionNodeType T2= new TestPartitionNodeType(T2_ID, 2);
	public static final TestPartitionNodeType T3= new TestPartitionNodeType(T3_ID, 3);
	
	
	private final String type;
	
	
	public TestPartitionNodeType(final String type, final int priority) {
		this.type= type;
	}
	
	@Override
	public String getPartitionType() {
		return this.type;
	}
	
}
