/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;


@NonNullByDefault
public class DocumentParserInputTest {
	
	
	private IDocument doc= nonNullLateInit();
	private DocumentParserInput input;
	
	
	@SuppressWarnings("null")
	public DocumentParserInputTest() {
	}
	
	
	@BeforeEach
	public void setUp() {
		this.doc= new Document();
	}
	
	
	@Test
	public void init() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.doc.set(s);
		this.input= new DocumentParserInput(this.doc);
		this.input.init();
		
		assertEquals(0, this.input.getStartIndex());
		assertEquals(s.length(), this.input.getStopIndex());
	}
	
	@Test
	public void initRegion() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.doc.set(s);
		this.input= new DocumentParserInput(this.doc);
		this.input.init(100, 200);
		
		assertEquals(100, this.input.getStartIndex());
		assertEquals(200, this.input.getStopIndex());
	}
	
	@Test
	public void initRegionIllegalStart() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.doc.set(s);
		this.input= new DocumentParserInput(this.doc);
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.input.init(-1, 400);
		});
	}
	
	@Test
	public void initRegionIllegalStop() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.doc.set(s);
		this.input= new DocumentParserInput(this.doc);
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.input.init(0, 801);
		});
	}
	
	@Test
	public void initRegionIllegalLength() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.doc.set(s);
		this.input= new DocumentParserInput(this.doc);
		assertThrows(IllegalArgumentException.class, () -> {
			this.input.init(800, 400);
		});
	}
	
	@Test
	public void read() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.doc.set(s);
		this.input= new DocumentParserInput(this.doc);
		this.input.init();
		
		assertChars(s);
		
		assertEquals(TextParserInput.EOF, this.input.get(s.length()));
		
		assertEquals(s.length(), Utils.getBufferLength(this.input));
	}
	
	@Test
	public void readRegion() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.doc.set(s);
		this.input= new DocumentParserInput(this.doc);
		this.input.init(100, 200);
		
		assertChars(s, 100, 200);
		
		assertEquals(TextParserInput.EOF, this.input.get(200));
		
		assertEquals(s.length(), Utils.getBufferLength(this.input));
	}
	
	@Test
	public void updateBuffer() {
		final String s= Utils.COUNTER_STRING;
		this.input= new DocumentParserInput();
		this.input.reset(this.doc);
		this.doc.set(s);
		this.input.init();
		
		readConsume(s, 0, s.length(), 100);
		
		assertEquals(TextParserInput.EOF, this.input.get(0));
		
		assertEquals(Utils.DEFAULT_BUFFER_SIZE, Utils.getBufferLength(this.input));
	}
	
	@Test
	public void increaseBuffer() {
		final String s= Utils.COUNTER_STRING;
		this.input= new DocumentParserInput();
		this.input.reset(this.doc);
		this.doc.set(s);
		this.input.init();
		
		assertChars(s);
		
		assertEquals(TextParserInput.EOF, this.input.get(s.length()));
	}
	
	@Test
	public void combined() {
		final String s= Utils.COUNTER_STRING;
		this.input= new DocumentParserInput();
		this.input.reset(this.doc);
		this.doc.set(s);
		this.input.init();
		
		readConsume(s, 0, s.length(), 2351);
		
		assertEquals(TextParserInput.EOF, this.input.get(0));
		
		assertTrue(0x1000 >= Utils.getBufferLength(this.input));
	}
	
	@Test
	public void empty() {
		final String s= "";
		this.input= new DocumentParserInput();
		this.input.reset(this.doc);
		this.doc.set(s);
		this.input.init();
		
		assertEquals(TextParserInput.EOF, this.input.get(0));
		
		assertTrue(0x1000 >= Utils.getBufferLength(this.input));
	}
	
	
	protected void readConsume(final String expected, int begin, final int end, final int consume) {
		while (begin < end) {
			final int l= Math.min(end - begin, consume);
			assertEquals(begin, this.input.getIndex());
			assertEquals(0, this.input.getLengthInSource(0));
			assertChars(expected, begin, begin + l);
			assertEquals(l, this.input.getLengthInSource(l));
			this.input.consume(l);
			begin+= l;
		}
	}
	
	protected void assertChars(final String expected) {
		assertChars(expected, 0, expected.length());
	}
	
	protected void assertChars(final String expected, final int begin, final int end) {
		for (int n= 0, index= begin; index < end; n++, index++) {
			final int index0= index;
			final char eChar= expected.charAt(index);
			final int actual= this.input.get(n);
			assertEquals(eChar, actual,
					() -> String.format("char at offset= %1$s", index0) );
		}
	}
	
}
