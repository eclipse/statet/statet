/*=============================================================================#
 # Copyright (c) 2000, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #     IBM Corporation - org.eclipse.jface.text: initial API and implementation of DefaultPartitionerZeroLengthTest
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionerTest.assertTypedRegion;

import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Equivalent to JFace's DefaultPartitionerZeroLengthTest (prefereOpen= true)
 */
@NonNullByDefault
public class PartitionerJFaceZeroLengthTest extends PartitionerJFaceTest {
	
	
	public PartitionerJFaceZeroLengthTest() {
	}
	
	
	@Override
	protected void assertGetContentType(final int[] offsets) {
		TestPartitionNodeType type= TestPartitionNodeType.DEFAULT_ROOT;
		int previousOffset= 0;
		for (int i= 0; i <= offsets.length; i++) {
			final int offset= (i == offsets.length) ? this.doc.getLength() : offsets[i];
			assertEqualPartitionType(previousOffset, offset, type);
			
			// advance
			type= nextType(type);
			previousOffset= offset;
		}
	}
	
	@Override
	protected void assertGetPartition(final int[] offsets) {
		TestPartitionNodeType type= TestPartitionNodeType.DEFAULT_ROOT;
		int previousOffset= 0;
		for (int i= 0; i <= offsets.length; i++) {
			final int offset= (i == offsets.length) ? this.doc.getLength() : offsets[i];
			assertEqualPartition(previousOffset, offset, type);
			
			// advance
			type= nextType(type);
			previousOffset= offset;
		}
	}
	
	@Override
	protected void assertComputePartitioning(final int startOffset, final int endOffset, final int[] offsets,
			final TestPartitionNodeType startType) {
		final var regions= this.partitioner.computePartitioning(startOffset, endOffset - startOffset, true);
		
		TestPartitionNodeType type= startType;
		int previousOffset= startOffset;
		
//		assertEquals(offsets.length + 1, regions.length);
		for (int i= 0; i <= offsets.length; i++) {
			final int currentOffset= (i == offsets.length) ? endOffset : offsets[i];
			final ITypedRegion region= regions[i];
			
			assertTypedRegion(previousOffset, currentOffset, type.getPartitionType(),
					region );
			
			// advance
			type= nextType(type);
			previousOffset= currentOffset;
		}
	}
	
	private boolean isOpenType(final TreePartitionNodeType type) {
		return (type == TestPartitionNodeType.DEFAULT_ROOT);
	}
	
	@Override
	protected void assertEqualPartition(final int offset, final int inclusiveEnd, final TreePartitionNodeType type) {
		final int from= isOpenType(type) ? offset : offset + 1;
		final int to= isOpenType(type) ? inclusiveEnd : inclusiveEnd - 1;
		for (int i= from; i <= to; i++) {
			assertTypedRegion(offset, inclusiveEnd, type.getPartitionType(),
					this.partitioner.getPartition(i, true) );
		}
	}
	
	@Override
	protected void assertEqualPartitionType(final int offset, final int end, final TreePartitionNodeType type) {
		final int from= isOpenType(type) ? offset : offset + 1;
		final int to= isOpenType(type) ? end : end - 1;
		for (int i= from; i <= to; i++) {
			assertEquals(type.getPartitionType(),
					this.partitioner.getContentType(i, true) );
		}
	}
	
}
