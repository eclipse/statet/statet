/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.Region;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ExpectedPartition extends Region implements ITypedRegion {
	
	
	private final TestPartitionNodeType type;
	
	private final boolean prefereAtStart;
	private final boolean prefereAtEnd;
	
	
	public ExpectedPartition(final int offset, final int length, final TestPartitionNodeType type) {
		this(offset, length, type, true, false);
	}
	
	public ExpectedPartition(final int offset, final int length, final TestPartitionNodeType type,
			final boolean prefereAtStart, final boolean prefereAtEnd) {
		super(offset, length);
		
		this.type= type;
		this.prefereAtStart= prefereAtStart;
		this.prefereAtEnd= prefereAtEnd;
	}
	
	
	public TestPartitionNodeType getNodeType() {
		return this.type;
	}
	
	@Override
	public String getType() {
		return this.type.getPartitionType();
	}
	
	public boolean prefereAtStart() {
		return this.prefereAtStart;
	}
	
	public boolean prefereAtEnd() {
		return this.prefereAtEnd;
	}
	
}
