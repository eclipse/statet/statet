/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.input;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;


@NonNullByDefault
class Utils {
	
	
	public static final String COUNTER_STRING;
	static {
		final char[] test= new char[0x4000];
		for (int i= 0; i < test.length; i++) {
			test[i]= (char) i;
		}
		COUNTER_STRING= new String(test);
	}
	
	
	public static final int DEFAULT_BUFFER_SIZE;
	static {
		try {
			final Field field= TextParserInput.class.getDeclaredField("DEFAULT_BUFFER_SIZE");
			field.setAccessible(true);
			DEFAULT_BUFFER_SIZE= field.getInt(null);
		}
		catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static int getBufferLength(final TextParserInput input) {
		try {
			final Method method= TextParserInput.class.getDeclaredMethod("getBuffer");
			method.setAccessible(true);
			final char[] buffer= (char[]) method.invoke(input);
			return buffer.length;
		}
		catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
