/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.core.util;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService;


@NonNullByDefault
public class ResumeJob extends Job {
	
	
	private final PreferenceSetService service;
	
	private final String sourceId;
	
	
	public ResumeJob(final PreferenceSetService service, final String sourceId) {
		super("Resume PreferenceSetService");
		this.service= service;
		this.sourceId= sourceId;
		
		setSystem(true);
		setPriority(SHORT);
	}
	
	
	@Override
	protected IStatus run(final IProgressMonitor monitor) {
		this.service.resume(this.sourceId);
		return Status.OK_STATUS;
	}
	
}
