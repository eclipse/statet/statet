/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.core;

import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface PreferenceSetService {
	
	
	static interface ChangeEvent {
		
		
		boolean contains(String qualifier);
		
		boolean contains(String qualifier, String key);
		boolean contains(Preference<?> pref);
		
		ImList<String> getKeys(String qualifier);
		
	}
	
	static interface ChangeListener {
		
		static final byte PRIORITY_HIGHEST= 10;
		static final byte PRIORITY_DEFAULT= 5;
		static final byte PRIORITY_LOWEST= 0;
		
		
		default byte getNotificationPriority() {
			return PRIORITY_DEFAULT;
		}
		
		void onPreferenceChanged(ChangeEvent event);
		
	}
	
	
	boolean pause(String sourceId);
	void resume(String sourceId);
	void addResumeListener(Runnable listener);
	
	void addChangeListener(ChangeListener listener,
			ImList<IScopeContext> contexts, ImSet<String> qualifiers);
	void removeChangeListener(ChangeListener listener);
	
}
