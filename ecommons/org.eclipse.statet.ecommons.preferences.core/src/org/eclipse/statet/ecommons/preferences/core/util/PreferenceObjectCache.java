/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.core.util;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceObjectAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceObjectDef;


@NonNullByDefault
public abstract class PreferenceObjectCache implements PreferenceObjectAccess {
	
	
	private static class TypeItem<TObject> {
		
		private final Class<TObject> type;
		
		private @Nullable
		final PreferenceObjectController<TObject> controller;
		
		
		public TypeItem(final Class<TObject> type,
				final @Nullable PreferenceObjectController<TObject> controller) {
			this.type= type;
			this.controller= controller;
		}
		
		
		@SuppressWarnings("null")
		public @Nullable TObject get() {
			return (this.controller != null) ? this.controller.get() : null;
		}
		
		
		@Override
		public String toString() {
			return this.type.toString();
		}
		
	}
	
	
	private final Map<String, TypeItem<?>> map= new HashMap<>();
	
	
	public PreferenceObjectCache() {
	}
	
	
	public abstract PreferenceAccess getPrefs();
	
	
	protected <TObject> @Nullable PreferenceObjectController<TObject> createPreferenceObjectController(
			final Class<TObject> type) {
		return null;
	}
	
	protected <TObject> PreferenceObjectController<TObject> createPreferenceObjectController(
			final Class<TObject> type, final PreferenceObjectDef<TObject> def) {
		return new PreferenceObjectController<>(type, getPrefs(), def.getQualifiers()) {
			@Override
			protected TObject createObject(final PreferenceAccess prefs) {
				return def.create(prefs);
			}
		};
	}
	
	private <TObject> TypeItem<TObject> getTypeItem(final Class<TObject> type,
			final @Nullable PreferenceObjectDef<TObject> def) {
		final String typeName= type.getName();
		synchronized (this.map) {
			@SuppressWarnings("unchecked")
			var typeItem= (TypeItem<TObject>)this.map.get(typeName);
			if (typeItem == null) {
				typeItem= new TypeItem<>(type, (def != null) ?
						createPreferenceObjectController(type, def) :
						createPreferenceObjectController(type) );
				this.map.put(typeName, typeItem);
			}
			return typeItem;
		}
	}
	
	@Override
	public <TObject> @Nullable TObject getPreferenceObject(final Class<TObject> type) {
		return getTypeItem(type, null).get();
	}
	
	@Override
	public <TObject> TObject getPreferenceObject(final PreferenceObjectDef<TObject> def) {
		return getTypeItem(def.getType(), def).get();
	}
	
	public <TObject> void addPreferenceObject(final PreferenceObjectDef<TObject> def) {
		getTypeItem(def.getType(), def);
	}
	
}
