/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;
import java.util.Map;

import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.core.runtime.preferences.InstanceScope;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.ecommons.preferences.core.ECommonsPreferencesCorePlugin;


@NonNullByDefault
public class PreferenceUtils {
	
	
	public static final int FLUSH_SYNC= 1 << 0;
	public static final int FLUSH_ASYNC= 1 << 1;
	
	
	public static <T> T getPrefValue(final List<IScopeContext> contexts, final Preference<T> pref) {
		String storeValue= null;
		for (int i= 0; i < contexts.size() && storeValue == null; i++) {
			try {
				final var node= contexts.get(i).getNode(pref.getQualifier());
				if (node != null) {
					storeValue= node.get(pref.getKey(), null);
				}
			}
			catch (final IllegalStateException e) {
			}
		}
		return pref.store2Usage(storeValue);
	}
	
	public static <T> T getPrefValue(final IScopeContext context, final Preference<T> pref) {
		final IEclipsePreferences node= context.getNode(pref.getQualifier());
		if (node == null) {
			return pref.store2Usage(null);
		}
		return getPrefValue(node, pref);
	}
	
	public static <T> T getPrefValue(final IEclipsePreferences node, final Preference<T> pref) {
		final String storeValue= node.get(pref.getKey(), (String)null);
		return pref.store2Usage(storeValue);
	}
	
	public static <T> void setPrefValue(final IScopeContext context,
			final Preference<T> pref, final T value) {
		final IEclipsePreferences node= context.getNode(pref.getQualifier());
		setPrefValue(node, pref, value);
	}
	
	public static <T> void setPrefValue(final IScopeContext context,
			final Preference<T> pref, final T value, final int flags) {
		final IEclipsePreferences node= context.getNode(pref.getQualifier());
		setPrefValue(node, pref, value);
		
		if ((flags & FLUSH_SYNC) != 0) {
			flush(node);
		}
		else if ((flags & FLUSH_ASYNC) != 0) {
			final Job job= new Job("Save Preferences") {
				@Override
				protected IStatus run(final IProgressMonitor monitor) {
					flush(node);
					return Status.OK_STATUS;
				}
			};
			job.setSystem(true);
			job.setUser(false);
			job.setPriority(Job.SHORT);
			job.schedule();
		}
	}
	
	private static void flush(final Preferences node) {
		try {
			node.flush();
		}
		catch (final BackingStoreException e) {
			ECommonsPreferencesCorePlugin.log(new Status(IStatus.ERROR,
					ECommonsPreferencesCorePlugin.BUNDLE_ID,
					"An error occurred when saving preferences.", //$NON-NLS-1$
					e ));
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void setPrefValues(final IScopeContext context,
			final Map<Preference<?>, Object> preferencesMap) {
		for (final Map.Entry<Preference<?>, Object> pref : preferencesMap.entrySet()) {
			setPrefValue(context, (Preference)pref.getKey(), pref.getValue());
		}
	}
	
	public static <T> void setPrefValue(final IEclipsePreferences node,
			final Preference<T> pref, final T value) {
		final String storeValue;
		if (value == null
				|| (storeValue= pref.usage2Store(value)) == null) {
			node.remove(pref.getKey());
			return;
		}
		node.put(pref.getKey(), storeValue);
	}
	
	
	public static <TObject> TObject getPreferenceObject(final PreferenceAccess prefs,
			final PreferenceObjectDef<TObject> def) {
		if (prefs instanceof PreferenceObjectAccess) {
			return ((PreferenceObjectAccess)prefs).getPreferenceObject(def);
		}
		return def.create(prefs);
	}
	
	
	private static final String MIGRATED_KEY= "migrated"; //$NON-NLS-1$
	
	public static boolean mergeNode(final String fromQualifier, final String toQualifier,
			final boolean deleteFinally)
			throws BackingStoreException {
		final IScopeContext scope= InstanceScope.INSTANCE;
		IEclipsePreferences fromNode;
		if (!scope.getNode("").nodeExists(fromQualifier)
				|| (fromNode= scope.getNode(fromQualifier)) == null ) {
			return false;
		}
		if (!deleteFinally && fromNode.getBoolean(MIGRATED_KEY, false)) {
			return false;
		}
		final var toNode= scope.getNode(toQualifier);
		for (final String key : fromNode.keys()) {
			if (key.equals(MIGRATED_KEY)) {
				continue;
			}
			final String value= fromNode.get(key, null);
			if (value != null && toNode.get(key, null) == null) {
				toNode.put(key, value);
			}
		}
		
		toNode.flush();
		
		if (deleteFinally) {
			final var parent= nonNullAssert(fromNode.parent());
			fromNode.removeNode();
			parent.flush();
		}
		else {
			fromNode.putBoolean(MIGRATED_KEY, true);
			fromNode.flush();
		}
		return true;
	}
	
	
	private PreferenceUtils() {
	}
	
}
