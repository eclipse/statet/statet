/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.core;

import java.util.function.Function;

import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Definition for preference object type with
 * <ul>
 *   <li>information about relevant preferences</li>
 *   <li>{@link #create(PreferenceAccess) factory method}</li>
 * </ul>
 * 
 * @param <TObject> the type of the preference objects
 */
@NonNullByDefault
public abstract class PreferenceObjectDef<TObject> {
	
	
	private final Class<TObject> type;
	
	private final ImSet<String> qualifiers;
	
	
	public PreferenceObjectDef(final Class<TObject> type, final ImSet<String> qualifiers) {
		this.type= type;
		this.qualifiers= qualifiers;
	}
	
	
	public Class<TObject> getType() {
		return this.type;
	}
	
	public ImSet<String> getQualifiers() {
		return this.qualifiers;
	}
	
	public boolean isRelevant(final PreferenceSetService.ChangeEvent event) {
		return true;
	}
	
	
	public abstract TObject create(final PreferenceAccess prefs);
	
	
	public static <TObject> PreferenceObjectDef<TObject> createFactory(
			final Class<TObject> type, final ImSet<String> qualifiers,
			final Function<PreferenceAccess, TObject> objectFactory) {
		return new PreferenceObjectDef<>(type, qualifiers) {
			@Override
			public TObject create(final PreferenceAccess prefs) {
				return objectFactory.apply(prefs);
			}
		};
	}
	
}
