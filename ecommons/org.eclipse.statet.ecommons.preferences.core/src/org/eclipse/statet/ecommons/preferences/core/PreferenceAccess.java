/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.core;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.IPreferenceChangeListener;
import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Interface to access preferences using <code>Preference</code>.
 * <p>
 * In most cases, you can take the Objects from <code>PreferencesUtil</code>.
 */
@NonNullByDefault
public interface PreferenceAccess {
	
	
	/**
	 * Returns the scopes used by this agent.
	 * 
	 * @return array with all scopes used for lookup.
	 */
	ImList<IScopeContext> getPreferenceContexts();
	
	/**
	 * Returns the preference value of the specified <code>Preference</code>
	 * 
	 * @param <T> type for which the <code>Preference</code> is designed.
	 * @param pref
	 * @return value
	 */
	<T> T getPreferenceValue(Preference<T> pref);
	
	/**
	 * Register the given listener at the node in all scopes
	 * (which can changed).
	 * @see IEclipsePreferences#addPreferenceChangeListener(IPreferenceChangeListener)
	 * 
	 * @param nodeQualifier the qualifier of the node
	 * @param listener the listener
	 */
	void addPreferenceNodeListener(String nodeQualifier, IPreferenceChangeListener listener);
	
	/**
	 * Remove the given listener from the node in all scopes
	 * (registered with {@link #addPreferenceNodeListener(String, IPreferenceChangeListener)}).
	 * @see IEclipsePreferences#removePreferenceChangeListener(IPreferenceChangeListener)
	 * 
	 * @param nodeQualifier the qualifier of the node
	 * @param listener the listener
	 */
	void removePreferenceNodeListener(String nodeQualifier, IPreferenceChangeListener listener);
	
	/**
	 * Subscribes the given preference changeset listener for all scopes of this preference access
	 * and the specified preference node qualifiers.
	 * 
	 * @see PreferenceSetService#addChangeListener(PreferenceSetService.ChangeListener)
	 * 
	 * @param listener the listener
	 * @param qualifiers list of preference node qualifiers
	 */
	void addPreferenceSetListener(PreferenceSetService.ChangeListener listener,
			ImSet<String> qualifiers);
	
	/**
	 * Unsubscribes the given preference changeset listener completely
	 * (registered with {@link #addPreferenceSetListener(PreferenceSetService.ChangeListener)}).
	 * 
	 * @see PreferenceSetService#removeChangeListener(PreferenceSetService.ChangeListener)
	 * 
	 * @param listener the listener
	 */
	void removePreferenceSetListener(PreferenceSetService.ChangeListener listener);
	
}
