/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.core.util;

import java.util.function.Supplier;

import org.eclipse.statet.jcommons.collections.ImSet;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceObjectDef;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService.ChangeEvent;


@NonNullByDefault
public abstract class PreferenceObjectController<TObject> implements Supplier<TObject>,
		PreferenceSetService.ChangeListener {
	
	private static final byte STARTED= 0;
	private static final byte INITIALIZED= 1;
	private static final byte UPTODATE= 2;
	private static final byte DISPOSED= 3;
	
	
	private final Class<TObject> type;
	
	private final PreferenceAccess prefs;
	private final ImSet<String> qualifiers;
	
	private volatile byte state;
	
	private @Nullable TObject object;
	
	
	public PreferenceObjectController(final Class<TObject> type,
			final PreferenceAccess prefs, final ImSet<String> qualifiers) {
		this.type= type;
		
		this.prefs= prefs;
		this.qualifiers= qualifiers;
	}
	
	public synchronized void dispose() {
		final var state= this.state;
		if (state == DISPOSED) {
			return;
		}
		
		this.state= DISPOSED;
		if (state > STARTED) {
			this.prefs.removePreferenceSetListener(this);
		}
	}
	
	
	public final Class<TObject> getType() {
		return this.type;
	}
	
	
	@Override
	public byte getNotificationPriority() {
		return PRIORITY_HIGHEST - 1;
	}
	
	@Override
	public final void onPreferenceChanged(final ChangeEvent event) {
		if (this.state == UPTODATE && isUpdateRequired(event)) {
			disposeObject();
		}
	}
	
	protected boolean isUpdateRequired(final PreferenceSetService.ChangeEvent event) {
		return true;
	}
	
	
	protected final synchronized void disposeObject() {
		final var state= this.state;
		if (state <= INITIALIZED) {
			return;
		}
		
		this.state= INITIALIZED;
		final @Nullable TObject object= this.object;
		this.object= null;
		if (object instanceof Disposable) {
			((Disposable)object).dispose();
		}
	}
	
	protected abstract TObject createObject(PreferenceAccess prefs);
//		Adapters.adapt(this.prefs, this.type);
	
	
	@Override
	@SuppressWarnings("null")
	public synchronized TObject get() {
		final var state= this.state;
		if (state >= UPTODATE) {
			return this.object;
		}
		
		this.state= UPTODATE;
		if (state == STARTED) {
			this.prefs.addPreferenceSetListener(this, this.qualifiers);
		}
		return this.object= createObject(this.prefs);
	}
	
	
	@Override
	public String toString() {
		return this.type.getName();
	}
	
	
	public static <TObject> PreferenceObjectController<TObject> createController(final Class<TObject> type,
			final PreferenceAccess prefs, final PreferenceObjectDef<TObject> factory) {
		return new PreferenceObjectController<>(type, prefs, factory.getQualifiers()) {
			@Override
			protected TObject createObject(final PreferenceAccess prefs) {
				return factory.create(prefs);
			}
		};
	}
	
}
