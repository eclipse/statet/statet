/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.text.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.IdentityHashMap;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.DocumentPartitioningChangedEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioningListener;
import org.eclipse.jface.text.IDocumentPartitioningListenerExtension2;
import org.eclipse.jface.text.Position;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.DocumentEnhancement;
import org.eclipse.statet.ecommons.text.core.util.TextUtils;


@NonNullByDefault
public class DocumentEnhancementImpl extends Position
			implements DocumentEnhancement, IDocumentPartitioningListener, IDocumentPartitioningListenerExtension2 {
	
	
	private static final String POSITION_CATEGORY= "org.eclipse.statet.ecommons.text.DocumentEnhancement"; //$NON-NLS-1$
	
	
	private static DocumentEnhancementImpl doSetup(final IDocument document) throws BadLocationException, BadPositionCategoryException {
		document.addPositionCategory(POSITION_CATEGORY);
		
		final DocumentEnhancementImpl documentEnh= new DocumentEnhancementImpl();
		document.addPosition(POSITION_CATEGORY, documentEnh);
		document.addDocumentPartitioningListener(documentEnh);
		
		return documentEnh;
	}
	
	public static void setup(final IDocument document) {
		synchronized (TextUtils.getLockObject(document)) {
			try {
				if (!document.containsPositionCategory(POSITION_CATEGORY)) {
					doSetup(document);
				}
			}
			catch (final BadPositionCategoryException | BadLocationException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public static DocumentEnhancementImpl get(final IDocument document) {
		synchronized (TextUtils.getLockObject(document)) {
			try {
				if (document.containsPositionCategory(POSITION_CATEGORY)) {
					return (DocumentEnhancementImpl)document.getPositions(POSITION_CATEGORY)[0];
				}
				return doSetup(document);
			}
			catch (final BadPositionCategoryException | BadLocationException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	
	private final CopyOnWriteIdentityListSet<IDocumentPartitioningListenerExtension2> partitioningListeners= new CopyOnWriteIdentityListSet<>();
	
	private @Nullable IdentityHashMap<String, Object> data;
	
	
	@Override
	public void addPrePartitioningListener(final IDocumentPartitioningListenerExtension2 listener) {
		this.partitioningListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removePrePartitioningListener(final IDocumentPartitioningListenerExtension2 listener) {
		this.partitioningListeners.remove(listener);
	}
	
	
	@Override
	public void documentPartitioningChanged(final IDocument document) {
	}
	
	@Override
	public void documentPartitioningChanged(final DocumentPartitioningChangedEvent event) {
		for (final IDocumentPartitioningListenerExtension2 listener : this.partitioningListeners) {
			listener.documentPartitioningChanged(event);
		}
	}
	
	
	@Override
	public @Nullable Object getData(final String key) {
		final IdentityHashMap<String, Object> data= this.data;
		return (data != null) ? data.get(key) : null;
	}
	
	@Override
	public void setData(final String key, final @Nullable Object value) {
		IdentityHashMap<String, Object> data= this.data;
		if (data == null) {
			data= new IdentityHashMap<>();
			this.data= data;
		}
		if (value != null) {
			data.put(key, value);
		}
		else {
			data.remove(key);
		}
	}
	
}
