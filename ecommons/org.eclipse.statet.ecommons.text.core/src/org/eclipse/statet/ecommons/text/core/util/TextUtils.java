/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.util;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.ISynchronizable;
import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;


/**
 * @since de.walware.ecommons.text 1.0
 */
@NonNullByDefault
public final class TextUtils {
	
	
	public static Object getLockObject(final Object o) {
		final Object lockObject= (o instanceof ISynchronizable) ?
				((ISynchronizable)o).getLockObject() : null;
		return (lockObject != null) ? lockObject : o;
	}
	
	
	public static String getContentType(final IDocument document, final String partitioning,
			final int offset, final boolean preferOpenPartition)
			throws BadPartitioningException, BadLocationException {
		return ((IDocumentExtension3)document).getContentType(partitioning,
				offset, preferOpenPartition );
	}
	
	public static ITypedRegion getPartition(final IDocument document, final String partitioning,
			final int offset, final boolean preferOpenPartition)
			throws BadPartitioningException, BadLocationException {
		return ((IDocumentExtension3)document).getPartition(partitioning,
				offset, preferOpenPartition );
	}
	
	public static String getContentType(final IDocument document, final DocContentSections contentInfo,
			final int offset, final boolean preferOpenPartition)
			throws BadPartitioningException, BadLocationException {
		return ((IDocumentExtension3)document).getContentType(contentInfo.getPartitioning(),
				offset, preferOpenPartition );
	}
	
	public static ITypedRegion getPartition(final IDocument document, final DocContentSections contentInfo,
			final int offset, final boolean preferOpenPartition)
			throws BadPartitioningException, BadLocationException {
		return ((IDocumentExtension3)document).getPartition(contentInfo.getPartitioning(),
				offset, preferOpenPartition );
	}
	
	
	public static final boolean isCommonWord2Part(final int cp) {
		return switch (cp) {
		case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
					'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
					'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'.', '_', '-', '+', '@' ->
				true;
		case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
					' ', '!', '#', '$', '%', '&', '*', ',', ';', 
					'"', '\'', '`',
					'/', '\\', ':', '<', '=', '>', '?',
					'(', ')', '[', ']', '{', '|', '}',
					'^', '~' ->
				false;
		default ->
			switch (Character.getType(cp)) {
			case Character.UPPERCASE_LETTER, Character.LOWERCASE_LETTER, Character.TITLECASE_LETTER,
						Character.MODIFIER_LETTER, Character.OTHER_LETTER,
						Character.NON_SPACING_MARK, Character.ENCLOSING_MARK, Character.COMBINING_SPACING_MARK,
						Character.DECIMAL_DIGIT_NUMBER, Character.LETTER_NUMBER, Character.OTHER_NUMBER,
						Character.DASH_PUNCTUATION, Character.CONNECTOR_PUNCTUATION,
						Character.MODIFIER_SYMBOL ->
					true;
			default ->
					false;
			};
		};
	}
	
	public static final int findCommonWord2Start(final IDocument document, int offset,
			final int bound) throws BadLocationException {
		while (--offset >= bound
				&& isCommonWord2Part(document.getChar(offset)) ) {
		}
		return offset + 1;
	}
	
	public static final int findCommonWord2End(final IDocument document, int offset,
			final int bound) throws BadLocationException {
		while (++offset < bound
				&& isCommonWord2Part(document.getChar(offset)) ) {
		}
		return offset;
	}
	
}
