/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;


@NonNullByDefault
public class TreePartition extends JFaceTextRegion implements ITypedRegion {
	
	
	private final TreePartitionNode node;
	
	
	/**
	 * Creates a typed region based on the given specification.
	 * 
	 * @param startOffset the starting offset, inclusive.
	 * @param endOffset the ending offset, exclusive.
	 * @param type the type of the region.
	 */
	public TreePartition(final int startOffset, final int endOffset, final TreePartitionNode node) {
		super(startOffset, endOffset);
		
		this.node= nonNullAssert(node);
	}
	
	
	@Override
	public String getType() {
		return this.node.getType().getPartitionType();
	}
	
	public TreePartitionNode getTreeNode() {
		return this.node;
	}
	
	
	@Override
	public int hashCode() {
		return super.hashCode() | getType().hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final TreePartition other
						&& super.equals(other)
						&& getType().equals(other.getType()) ));
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(getType());
		sb.append(": "); //$NON-NLS-1$
		appendIntervalString(sb);
		return sb.toString();
	}
	
}
