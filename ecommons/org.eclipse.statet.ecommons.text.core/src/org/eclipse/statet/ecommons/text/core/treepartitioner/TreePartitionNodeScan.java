/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface TreePartitionNodeScan {
	
	
	class State {
		
		public int offset;
		
		public TreePartitionNode node;
		
		
		public State(final int offset, final TreePartitionNode node) {
			this.offset= offset;
			this.node= node;
		}
		
		@SuppressWarnings("null")
		State(final int offset) {
			this.offset= offset;
		}
		
	}
	
	
	class BreakException extends RuntimeException {
		
		private static final long serialVersionUID= 1L;
		
		
		public BreakException() {
			super("BreakScan", null, true, false);
		}
		
	}
	
	
	IDocument getDocument();
	
	int getStartOffset();
	
	int getEndOffset();
	
	TreePartitionNode getBeginNode();
	
	
	boolean isAutoBreakEnabled();
	
	void setAutoBreakEnabled(final boolean enable);
	
	void checkBreak() throws BreakException;
	
	
	TreePartitionNode add(final TreePartitionNodeType type,
			final TreePartitionNode parent, final int offset, final int flags);
	
	void expand(final TreePartitionNode node,
			final int endOffset, final int flags, final boolean close);
	
	void markDirtyEnd(int offset);
	
}
