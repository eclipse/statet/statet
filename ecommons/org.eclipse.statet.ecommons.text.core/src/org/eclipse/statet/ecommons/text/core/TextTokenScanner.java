/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.string.CharPair;


@NonNullByDefault
public interface TextTokenScanner {
	
	/**
	 * Returned by all methods when the requested position could not be found, or if a
	 * {@link BadLocationException} was thrown while scanning.
	 */
	public static final int NOT_FOUND= -1;
	
	/**
	 * Special bound parameter that means either -1 (backward scanning) or
	 * <code>document.getLength()</code> (forward scanning).
	 */
	public static final int UNBOUND= -2;
	
	public static final char NO_CHAR= (char)0;
	
	
	/**
	 * @param document the document to scan
	 * @param partition the partition to scan in
	 */
	public void configure(IDocument document, String partition);
	
//	/**
//	 * @param document the document to scan
//	 * @param offset offset, where looking for the partition to scan in
//	 * @throws BadLocationException 
//	 */
//	public void configure(IDocument document, int offset) throws BadLocationException;
	
	/**
	 * Returns the position of the closing peer character (forward search). Any scopes introduced by opening peers
	 * are skipped. All peers accounted for must reside in the default partition.
	 * 
	 * <p>Note that <code>start</code> must not point to the opening peer, but to the first
	 * character being searched.</p>
	 * 
	 * @param offset the start position
	 * @param pair pair with the opening and closing peer character
	 * @param escapeChar char escaping the charaters in parameter <code>pair</code>
	 * @return the matching peer character position, or <code>NOT_FOUND</code>
	 * @throws BadLocationException 
	 */
	public int findClosingPeer(int offset, CharPair pair, char escapeChar) throws BadLocationException;
	
	/**
	 * Returns the position of the opening peer character (backward search). Any scopes introduced by closing peers
	 * are skipped. All peers accounted for must reside in the default partition.
	 * 
	 * <p>Note that <code>start</code> must not point to the closing peer, but to the first
	 * character being searched.</p>
	 * 
	 * @param offset the start position
	 * @param pair pair with the opening and closing peer character
	 * @param escapeChar char escaping the charaters in parameter <code>pair</code>
	 * @return the matching peer character position, or <code>NOT_FOUND</code>
	 */
	public int findOpeningPeer(int offset, CharPair pair, char escapeChar) throws BadLocationException;
	
}
