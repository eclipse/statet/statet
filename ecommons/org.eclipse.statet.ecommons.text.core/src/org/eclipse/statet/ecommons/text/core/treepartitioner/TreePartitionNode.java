/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;


@NonNullByDefault
public interface TreePartitionNode extends TextRegion {
	
	
	/**
	 * Flag indicating that the partition is not closed.
	 */
	int END_UNCLOSED=                   1 << 8;
	
	
	TreePartitionNodeType getType();
	
	
	/**
	 * {@inheritDoc}
	 * 
	 * <p><b>Warning:</b> region of the tree partition node is mutable due to document changes.
	 * </p>
	 */
	@Override
	int getStartOffset();
	
	/**
	 * {@inheritDoc}
	 * 
	 * <p><b>Warning:</b> region of the tree partition node is mutable due to document changes.
	 * </p>
	 */
	@Override
	int getEndOffset();
	
	/**
	 * {@inheritDoc}
	 * 
	 * <p><b>Warning:</b> region of the tree partition node is mutable due to document changes.
	 * </p>
	 */
	@Override
	int getLength();
	
	
	@Nullable TreePartitionNode getParent();
	
	int getChildCount();
	
	TreePartitionNode getChild(final int idx);
	
	int indexOfChild(final int offset);
	int indexOfChild(final TreePartitionNode child);
	
	int getFlags();
	
	void addAttachment(final Object data);
	void removeAttachment(final Object data);
	ImList<Object> getAttachments();
	
}
