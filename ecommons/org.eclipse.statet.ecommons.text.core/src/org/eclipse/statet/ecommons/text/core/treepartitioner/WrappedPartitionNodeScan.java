/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class WrappedPartitionNodeScan implements TreePartitionNodeScan {
	
	
	private final TreePartitionNodeScan parent;
	
	private int startOffset;
	private int endOffset;
	
	private TreePartitionNode beginNode;
	
	private boolean wasAutoBreakEnabled;
	
	
	public WrappedPartitionNodeScan(final TreePartitionNodeScan parent) {
		if (parent == null) {
			throw new NullPointerException("parent"); //$NON-NLS-1$
		}
		this.parent= parent;
	}
	
	
	public void init(final int startOffset, final int endOffset, final TreePartitionNode beginNode) {
		if (beginNode == null) {
			throw new NullPointerException("beginNode"); //$NON-NLS-1$
		}
		this.startOffset= startOffset;
		this.endOffset= endOffset;
		this.beginNode= beginNode;
		
		this.wasAutoBreakEnabled= this.parent.isAutoBreakEnabled();
		if (!this.wasAutoBreakEnabled) {
			this.parent.setAutoBreakEnabled(true);
		}
	}
	
	public void exit() {
		if (this.wasAutoBreakEnabled != this.parent.isAutoBreakEnabled()) {
			this.parent.setAutoBreakEnabled(this.wasAutoBreakEnabled);
			if (this.wasAutoBreakEnabled) {
				this.parent.checkBreak();
			}
		}
		
		this.beginNode= null;
	}
	
	
	@Override
	public IDocument getDocument() {
		return this.parent.getDocument();
	}
	
	@Override
	public int getStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public int getEndOffset() {
		return this.endOffset;
	}
	
	@Override
	public TreePartitionNode getBeginNode() {
		return this.beginNode;
	}
	
	
	@Override
	public void setAutoBreakEnabled(final boolean enable) {
		this.parent.setAutoBreakEnabled(enable);
	}
	
	@Override
	public boolean isAutoBreakEnabled() {
		return this.parent.isAutoBreakEnabled();
	}
	
	@Override
	public void checkBreak() throws BreakException {
		this.parent.checkBreak();
	}
	
	@Override
	public TreePartitionNode add(final TreePartitionNodeType type,
			final TreePartitionNode parent, final int offset, final int flags) {
		return this.parent.add(type, parent, offset, flags);
	}
	
	@Override
	public void expand(final TreePartitionNode node,
			final int endOffset, final int flags,
			final boolean close) {
		this.parent.expand(node, endOffset, flags, close);
	}
	
	@Override
	public void markDirtyEnd(final int offset) {
		this.parent.markDirtyEnd(offset);
	}
	
}
