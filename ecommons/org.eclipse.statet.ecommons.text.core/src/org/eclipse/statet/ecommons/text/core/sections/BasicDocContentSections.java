/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.sections;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextUtilities;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class BasicDocContentSections implements DocContentSections {
	
	
	private final String partitioning;
	
	private final ImList<String> allTypes;
	private final String primaryType;
	
	
	/**
	 * Creates a new description from the specified list of content types, the first in the list is the
	 * primary content type.
	 * 
	 * @param partitioning the partitioning id
	 * @param allTypes a list of ids of the supported section content types
	 */
	protected BasicDocContentSections(final String partitioning, final ImList<String> allTypes) {
		this.partitioning= partitioning;
		this.allTypes= allTypes;
		this.primaryType= allTypes.getFirst();
	}
	
	/**
	 * Creates a new description with a single content type.
	 * 
	 * @param partitioning the partitioning id
	 * @param primaryType the id of the primary section content type
	 */
	protected BasicDocContentSections(final String partitioning, final String primaryType) {
		this.partitioning= partitioning;
		this.allTypes= ImCollections.newList(primaryType);
		this.primaryType= primaryType;
	}
	
	
	@Override
	public final String getPartitioning() {
		return this.partitioning;
	}
	
	@Override
	public final ImList<String> getAllTypes() {
		return this.allTypes;
	}
	
	@Override
	public final String getPrimaryType() {
		return this.primaryType;
	}
	
	
	@Override
	public String getType(final IDocument document, final int offset) {
		try {
			return getTypeByPartition(
					TextUtilities.getPartition(document, getPartitioning(), offset, true).getType()
					);
		}
		catch (final BadLocationException e) {
			return ERROR;
		}
	}
	
	@Override
	public abstract String getTypeByPartition(final String contentType);
	
}
