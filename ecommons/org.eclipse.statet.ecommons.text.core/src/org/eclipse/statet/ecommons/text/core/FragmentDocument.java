/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.TextRegion;


/**
 * Document representing a fragment of a master document.
 * 
 * @since de.walware.ecommons.text 1.1
 */
@NonNullByDefault
public interface FragmentDocument extends IDocument {
	
	
	AbstractDocument getMasterDocument();
	
	int getOffsetInMasterDocument();
	TextRegion getRegionInMasterDocument();
	
}
