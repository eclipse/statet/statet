/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.util;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultLineTracker;
import org.eclipse.jface.text.ITextStore;

import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Immutable document.
 * 
 * @since de.walware.ecommons.text 1.1
 */
@NonNullByDefault
public class ImmutableDocument extends AbstractDocument implements Immutable {
	
	
	private static class StringTextStore implements ITextStore {
		
		private final String content;
		
		/**
		 * Creates a new string text store with the given content.
		 *
		 * @param content the content
		 */
		public StringTextStore(final String content) {
			if (content == null) {
				throw new NullPointerException();
			}
			this.content= content;
		}
		
		@Override
		public char get(final int offset) {
			return this.content.charAt(offset);
		}
		
		@Override
		public String get(final int offset, final int length) {
			return this.content.substring(offset, offset + length);
		}
		
		@Override
		public int getLength() {
			return this.content.length();
		}
		
		@Override
		public void replace(final int offset, final int length, final String text) {
		}
		
		@Override
		public void set(final String text) {
		}
		
	}
	
	
	/**
	 * Creates a new read-only document with the given content.
	 *
	 * @param content the content
	 * @param lineDelimiters the line delimiters
	 */
	public ImmutableDocument(final String content, final long timestamp) {
		super();
		setTextStore(new StringTextStore(content));
		setLineTracker(new DefaultLineTracker());
		completeInitialization();
		super.set(content, timestamp);
	}
	
	
	@Override
	public void set(final String text, final long modificationStamp) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void replace(final int pos, final int length, final String text)
			throws BadLocationException {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void replace(final int pos, final int length, final String text,
			final long modificationStamp) throws BadLocationException {
		throw new UnsupportedOperationException();
	}
	
	
}
