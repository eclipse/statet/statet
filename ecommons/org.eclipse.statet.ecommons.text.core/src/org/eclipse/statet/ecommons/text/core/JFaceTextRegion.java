/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core;

import org.eclipse.jface.text.IRegion;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;


/**
 * TextRegion also implementing JFace IRegion
 */
@NonNullByDefault
public class JFaceTextRegion extends BasicTextRegion implements IRegion {
	
	
	public static final JFaceTextRegion toJFaceRegion(final TextRegion region) {
		return (region instanceof JFaceTextRegion) ?
				(JFaceTextRegion) region :
				new JFaceTextRegion(region);
	}
	
	public static final JFaceTextRegion toTextRegion(final IRegion region) {
		return (region instanceof JFaceTextRegion) ?
				(JFaceTextRegion) region :
				new JFaceTextRegion(region.getOffset(), region.getOffset() + region.getLength());
	}
	
	public static final JFaceTextRegion newByStartLength(final int startOffset, final int length) {
		return new JFaceTextRegion(startOffset, startOffset + length);
	}
	
	public static final JFaceTextRegion newByStartEnd(final int startOffset, final int endOffset) {
		return new JFaceTextRegion(startOffset, endOffset);
	}
	
	
	protected JFaceTextRegion(final int startOffset, final int endOffset) {
		super(startOffset, endOffset);
	}
	
	protected JFaceTextRegion(final TextRegion region) {
		super(region);
	}
	
	
	@Override
	public final int getOffset() {
		return getStartOffset();
	}
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof TextRegion) {
			return super.equals(obj);
		}
		if (obj instanceof final IRegion other) {
			return (getOffset() == other.getOffset()
					&& getLength() == other.getLength() );
		}
		return false;
	}
	
}
