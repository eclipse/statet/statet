/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.core.treepartitioner;

import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface TreePartitionNodeType {
	
	/**
	 * Returns the partition content type.
	 * 
	 * @return the identifier of the type
	 */
	String getPartitionType();
	
	/**
	 * Returns if the this partition should be preferred to the parent at the begin of the position.
	 * 
	 * @return <code>true</code> to prefere this partition, otherwise false
	 */
	boolean prefereAtBegin(final TreePartitionNode node, final IDocument document);
	
	/**
	 * Returns if the this partition should be preferred to the parent at the end of the position.
	 * 
	 * @return <code>true</code> to prefere this partition, otherwise false
	 */
	boolean prefereAtEnd(final TreePartitionNode node, final IDocument document);
	
}
