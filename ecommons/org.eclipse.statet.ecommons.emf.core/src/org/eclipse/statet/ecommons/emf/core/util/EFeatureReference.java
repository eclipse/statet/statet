/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.core.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class EFeatureReference {
	
	
	private final EObject eObject;
	private final EStructuralFeature eFeature;
	
	
	public EFeatureReference(final EObject eObject, final EStructuralFeature eFeature) {
		this.eObject= eObject;
		this.eFeature= eFeature;
	}
	
	
	public EObject getEObject() {
		return this.eObject;
	}
	
	public EStructuralFeature getEFeature() {
		return this.eFeature;
	}
	
	public @Nullable Object getValue() {
		return this.eObject.eGet(this.eFeature);
	}
	
	
	@Override
	public int hashCode() {
		return this.eObject.hashCode() + this.eFeature.hashCode() * 17;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final EFeatureReference other
						&& this.eObject == other.eObject
						&& this.eFeature == other.eFeature ));
	}
	
	
	@Override
	public String toString() {
		return this.eObject.toString() + " # " + this.eFeature.toString(); //$NON-NLS-1$
	}
	
}
