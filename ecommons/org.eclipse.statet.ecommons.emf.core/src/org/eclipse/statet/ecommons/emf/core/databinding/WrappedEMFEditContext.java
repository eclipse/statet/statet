/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.core.databinding;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.edit.domain.EditingDomain;


public class WrappedEMFEditContext implements IEMFEditContext {
	
	
	private IEMFEditContext parent;
	
	
	public WrappedEMFEditContext(final IEMFEditContext parent) {
		setParent(parent);
	}
	
	
	protected void setParent(final IEMFEditContext parent) {
		if (parent == null) {
			throw new NullPointerException("parent");
		}
		this.parent= parent;
	}
	
	@Override
	public DataBindingContext getDataBindingContext() {
		return this.parent.getDataBindingContext();
	}
	
	@Override
	public Realm getRealm() {
		return this.parent.getRealm();
	}
	
	@Override
	public IObservableValue getBaseObservable() {
		return this.parent.getBaseObservable();
	}
	
	@Override
	public EditingDomain getEditingDomain() {
		return this.parent.getEditingDomain();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Class<T> adapterType) {
		if (adapterType.isInstance(this)) {
			return (T) this;
		}
		return this.parent.getAdapter(adapterType);
	}
	
}
