/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.variables.core;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String Validation_Syntax_DollorInvalidChar_message;
	public static String Validation_Syntax_DollarEnd_message;
	public static String Validation_Syntax_VarMissingName_message;
	public static String Validation_Syntax_VarInvalidChar_message;
	public static String Validation_Syntax_VarNotClosed_message;
	public static String Validation_Ref_VarNotDefined_message;
	public static String Validation_Ref_VarNoArgs_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
