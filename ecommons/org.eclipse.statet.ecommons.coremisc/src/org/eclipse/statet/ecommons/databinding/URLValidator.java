/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;


/**
 * Validator for URLs.
 */
public class URLValidator implements IValidator {
	
	
	private final String label;
	
	
	public URLValidator(final String label) {
		this.label= label;
	}
	
	
	@Override
	public IStatus validate(final Object value) {
		if (value instanceof String) {
			try {
				new URL((String) value);
				return ValidationStatus.ok();
			}
			catch (final MalformedURLException e) {
				return ValidationStatus.error(this.label + " is invalid: " + e.getLocalizedMessage());
			}
		}
		throw new IllegalStateException("Unsupported value type: " + value.getClass().toString());
	}
	
}
