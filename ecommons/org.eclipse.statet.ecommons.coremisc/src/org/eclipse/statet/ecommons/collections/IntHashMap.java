/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.collections;

import java.nio.channels.UnsupportedAddressTypeException;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * An object that maps integer keys to values using the integer value itself as hash value.
 * 
 * @param <V> type of the values
 * @since 1.0
 */
@NonNullByDefault
public final class IntHashMap<V> implements IntMap<V> {
	
	
	private static final float DEFAULT_LOAD_FACTOR= 0.75f;
	
	private static class Entry<V> implements java.util.Map.Entry<Integer, V>, IntEntry<V> {
		
		final int key;
		V value;
		Entry<V> next;
		
		public Entry(final int key, final V value, final Entry<V> next) {
			this.key= key;
			this.value= value;
			this.next= next;
		}
		
		
		@Override
		public int getIntKey() {
			return this.key;
		}
		
		@Override
		public Integer getKey() {
			return Integer.valueOf(this.key);
		}
		
		@Override
		public V getValue() {
			return this.value;
		}
		
		@Override
		public V setValue(final V value) {
			final V oldValue= this.value;
			this.value= value;
			return oldValue;
		}
		
		@Override
		public int hashCode() {
			return this.key + Objects.hashCode(this.value);
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			return (this == obj
					|| (obj instanceof final IntEntry<?> other
							&& this.key == other.getIntKey()
							&& Objects.equals(this.value, other.getValue()) ));
		}
		
	}
	
	private class IntEntryIterator implements Iterator<IntEntry<V>> {
		
		private @Nullable Entry<V> currentEntry;
		private @Nullable Entry<V> nextEntry;
		private int nextEntryIdx;
		
		public IntEntryIterator() {
			for (int idx= 0; idx < IntHashMap.this.entries.length; idx++) {
				if (IntHashMap.this.entries[idx] != null) {
					this.nextEntry= IntHashMap.this.entries[this.nextEntryIdx= idx];
					break;
				}
			}
		}
		
		@Override
		public boolean hasNext() {
			return (this.nextEntry != null);
		}
		
		@Override
		public Entry<V> next() {
			this.currentEntry= this.nextEntry;
			if (this.currentEntry == null) {
				throw new NoSuchElementException();
			}
			if (this.currentEntry.next != null) {
				this.nextEntry= this.currentEntry.next;
			}
			else {
				this.nextEntry= null;
				for (int idx= this.nextEntryIdx+1; idx < IntHashMap.this.entries.length; idx++) {
					if (IntHashMap.this.entries[idx] != null) {
						this.nextEntry= IntHashMap.this.entries[this.nextEntryIdx= idx];
						break;
					}
				}
			}
			return this.currentEntry;
		}
		
		@Override
		public void remove() {
			if (this.currentEntry == null) {
				throw new IllegalStateException();
			}
			IntHashMap.this.remove(this.currentEntry.key);
			this.currentEntry= null;
		}
		
	}
	
	
	private Entry<V>[] entries;
	
	private int size;
	
	private int threshold;
	
	private final float loadFactor;
	
	private volatile Set<IntEntry<V>> entryIntSet;
	
	
	public IntHashMap() {
		this(16, DEFAULT_LOAD_FACTOR);
	}
	
	public IntHashMap(final int initialCapacity) {
		this(initialCapacity, DEFAULT_LOAD_FACTOR);
	}
	
	public IntHashMap(int initialCapacity, final float loadFactor) {
		if (initialCapacity < 0) {
			throw new IllegalArgumentException("initialCapacity: " + initialCapacity);
		}
		if (loadFactor <= 0) {
			throw new IllegalArgumentException("loadFactor: " + loadFactor);
		}
		if (initialCapacity == 0) {
			initialCapacity= 1;
		}
		
		this.loadFactor= loadFactor;
		this.threshold= (int) (initialCapacity * this.loadFactor);
		this.entries= new Entry[initialCapacity];
	}
	
	
	private int idxFor(final int key) {
		final int compr= key ^ (key >>> 23) ^ (key >>> 11);
		return ((compr ^ (compr >>> 7)) & 0x7fffffff) % this.entries.length;
	}
	
	@Override
	public boolean isEmpty() {
		return (this.size == 0);
	}
	
	@Override
	public int size() {
		return this.size;
	}
	
	@Override
	public boolean containsKey(final int key) {
		for (Entry<V> e= this.entries[idxFor(key)]; e != null; e= e.next) {
			if (e.key == key) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean containsKey(final @Nullable Object key) {
		return (key instanceof Integer
				&& containsKey(((Integer)key).intValue()) );
	}
	
	@Override
	public boolean containsValue(final @Nullable Object value) {
		for (int i= this.entries.length-1; i >= 0; i--) {
			for (Entry<V> e= this.entries[i]; e != null; e= e.next) {
				if (e.value.equals(value)) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public @Nullable V get(final int key) {
		for (Entry<V> e= this.entries[idxFor(key)]; e != null; e= e.next) {
			if (e.key == key) {
				return e.value;
			}
		}
		return null;
	}
	
	@Override
	public @Nullable V get(final Object key) {
		return (key instanceof Integer) ? get(((Integer)key).intValue()) : null;
	}
	
	
	private void increase() {
		final Entry<V>[] oldEntries= this.entries;
		this.entries= new Entry[oldEntries.length * 2 + 1];
		for (int i= oldEntries.length-1; i >= 0; i--) {
			for (Entry<V> next= oldEntries[i]; next != null;) {
				final Entry<V> e= next;
				next= next.next;
				
				final int idx= idxFor(e.key);
				e.next= this.entries[idx];
				this.entries[idx]= e;
			}
		}
		this.threshold= (int)(this.entries.length * this.loadFactor);
	}
	
	@Override
	public @Nullable V put(final int key, final V value) {
		for (Entry<V> e= this.entries[idxFor(key)]; e != null; e= e.next) {
			if (e.key == key) {
				final V old= e.value;
				e.value= value;
				return old;
			}
		}
		if (this.size >= this.threshold) {
			increase();
		}
		{	final int idx= idxFor(key);
			this.entries[idx]= new Entry<>(key, value, this.entries[idx]);
			this.size++;
			return null;
		}
	}
	
	@Override
	public @Nullable V put(final Integer key, final V value) {
		return put(key.intValue(), value);
	}
	
	@Override
	public void putAll(final Map<? extends Integer, ? extends V> t) {
		for (final var entry : t.entrySet()) {
			put(entry.getKey().intValue(), entry.getValue());
		}
	}
	
	public @Nullable V remove(final int key) {
		final int idx= idxFor(key);
		for (Entry<V> e= this.entries[idx], prev= null; e != null; prev= e, e= e.next) {
			if (e.key == key) {
				if (prev == null) {
					this.entries[idx]= e.next;
				}
				else {
					prev.next= e.next;
				}
				this.size--;
				final V oldValue= e.value;
				e.value= null;
				return oldValue;
			}
		}
		return null;
	}
	
	@Override
	public @Nullable V remove(final @Nullable Object key) {
		return (key instanceof Integer) ? remove(((Integer)key).intValue()) : null;
	}
	
	@Override
	public void clear() {
		Arrays.fill(this.entries, null);
		this.size= 0;
	}
	
	
	public Set<IntEntry<V>> entryIntSet() {
		final Set<IntEntry<V>> entries= this.entryIntSet;
		return (entries != null) ? entries : (this.entryIntSet= new AbstractSet<>() {
			@Override
			public int size() {
				return IntHashMap.this.size;
			}
			@Override
			public boolean contains(final Object o) {
				return (o instanceof IntEntry
						&& o.equals(IntHashMap.this.get(((IntEntry<?>) o).getIntKey())));
			}
			@Override
			public Iterator<IntEntry<V>> iterator() {
				return new IntEntryIterator();
			}
			@Override
			public void clear() {
				IntHashMap.this.clear();
			}
		});
	}
	
	@Override
	public Set<Integer> keySet() {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Collection<V> values() {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Set<java.util.Map.Entry<Integer, V>> entrySet() {
		throw new UnsupportedAddressTypeException();
	}
	
}
