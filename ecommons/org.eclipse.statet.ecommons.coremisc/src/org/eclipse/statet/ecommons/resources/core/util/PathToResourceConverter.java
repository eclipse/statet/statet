/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.resources.core.util;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.IPath;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.core.conversion.ClassTypedConverter;


@NonNullByDefault
public abstract class PathToResourceConverter<TResource extends IResource> implements ClassTypedConverter<IPath, TResource> {
	
	
	public static class ToContainer extends PathToResourceConverter<IContainer> {
		
		
		public ToContainer(final IContainer baseContainer) {
			super(IResource.PROJECT | IResource.FOLDER, baseContainer);
		}
		
		
		@Override
		public Class<IContainer> getToType() {
			return IContainer.class;
		}
		
	}
	
	public static class ToFolder extends PathToResourceConverter<IFolder> {
		
		
		public ToFolder(final IContainer baseContainer) {
			super(IResource.FOLDER, baseContainer);
		}
		
		
		@Override
		public Class<IFolder> getToType() {
			return IFolder.class;
		}
		
	}
	
	public static class ToFile extends PathToResourceConverter<IFile> {
		
		
		public ToFile(final IContainer baseContainer) {
			super(IResource.FILE, baseContainer);
		}
		
		
		@Override
		public Class<IFile> getToType() {
			return IFile.class;
		}
		
	}
	
	
	private final int resourceType;
	
	private IContainer baseContainer;
	
	
	public PathToResourceConverter(final int resourceType, final IContainer baseContainer) {
		this.resourceType= resourceType;
		this.baseContainer= baseContainer;
	}
	
	
	public void setBaseContainer(final IContainer container) {
		this.baseContainer= container;
	}
	
	public IContainer getBaseContainer() {
		return this.baseContainer;
	}
	
	
	@Override
	public Class<IPath> getFromType() {
		return IPath.class;
	}
	
	@Override
	public abstract Class<TResource> getToType();
	
	@Override
	@SuppressWarnings("unchecked")
	public TResource convert(final IPath path) {
		IContainer base= getBaseContainer();
		if (path.isAbsolute()) {
			base= base.getWorkspace().getRoot();
		}
		
		if (path.segmentCount() == 0) {
			checkResourceType(base.getType(), path);
			return (TResource)base;
		}
		if (path.segmentCount() == 1 && base.getType() == IResource.ROOT) {
			checkResourceType(IResource.PROJECT, path);
			return (TResource)((IWorkspaceRoot)base).getProject(path.segment(0));
		}
		if ((this.resourceType & IResource.FILE) == 0 || path.hasTrailingSeparator()) {
			checkResourceType(IResource.FOLDER, path);
			return (TResource)base.getFolder(path);
		}
		{	checkResourceType(IResource.FILE, path);
			return (TResource)base.getFile(path);
		}
	}
	
	protected void checkResourceType(final int type, final IPath path) {
		if (this.resourceType != 0 && (this.resourceType & type) == 0) {
			throw new IllegalArgumentException("path= " + path.toString()); //$NON-NLS-1$
		}
	}
	
}
