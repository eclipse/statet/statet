/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.resources.core.util;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.ecommons.databinding.core.validation.ValidationUtils;
import org.eclipse.statet.ecommons.io.internal.Messages;


public class RelativePathValidator implements IValidator<Object> {
	
	
	private final int resourceType;
	
	private IContainer baseContainer;
	
	private final String resourceLabel;
	
	private int onEmpty;
	
	
	public RelativePathValidator(final int resourceType, final IContainer container,
			final String resourceLabel) {
		this.resourceType= resourceType;
		this.baseContainer= container;
		this.resourceLabel= resourceLabel;
	}
	
	
	public void setBaseContainer(final IContainer container) {
		this.baseContainer= container;
	}
	
	public IContainer getBaseContainer() {
		return this.baseContainer;
	}
	
	
	@Override
	public IStatus validate(final Object value) {
		if (value instanceof String) {
			final String s= ((String) value).trim();
			if (s.isEmpty()) {
				return onEmtpy();
			}
			return validatePath(new Path(s));
		}
		if (value instanceof IPath) {
			return validatePath((IPath) value);
		}
		
		throw new IllegalArgumentException();
	}
	
	protected IStatus validatePath(final IPath path) {
		if (path.isAbsolute() || path.getDevice() != null) {
			return onAbsolute();
		}
		if (path.isEmpty()) {
			return onEmtpy();
		}
		
		final IContainer baseContainer= getBaseContainer();
		if (baseContainer == null) {
			throw new IllegalStateException("baseContainer is missing."); //$NON-NLS-1$
		}
		final IWorkspace workspace= baseContainer.getWorkspace();
		for (int i= 0; i < path.segmentCount(); i++) {
			final IStatus status= workspace.validateName(path.segment(i), IResource.FOLDER);
			if (!status.isOK()) {
				return status;
			}
		}
		
		return ValidationStatus.ok();
	}
	
	protected IStatus onEmtpy() {
		if (this.onEmpty == IStatus.OK) {
			return ValidationStatus.ok();
		}
		return ValidationUtils.newStatus(this.onEmpty, (this.resourceLabel != null) ?
				NLS.bind(Messages.Resource_error_NoInput_message, this.resourceLabel) :
				Messages.Resource_error_NoInput_message_0 );
	}
	
	protected IStatus onAbsolute() {
		return ValidationStatus.error((this.resourceLabel != null) ?
				NLS.bind(Messages.Resource_error_NotRelative_message, this.resourceLabel) :
				Messages.Resource_error_NotRelative_message_0 );
	}
	
}
