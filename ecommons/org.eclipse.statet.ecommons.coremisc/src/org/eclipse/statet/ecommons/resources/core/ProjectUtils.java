/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.resources.core;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProjectDescription;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ProjectUtils {
	
	
	private static int indexOf(final String[] ids, final String id) {
		for (int idx= 0; idx < ids.length; idx++) {
			if (ids[idx].equals(id)) {
				return idx;
			}
		}
		return -1;
	}
	
	private static int indexOf(final ICommand[] commands, final String id) {
		for (int idx= 0; idx < commands.length; idx++) {
			if (commands[idx].getBuilderName().equals(id)) {
				return idx;
			}
		}
		return -1;
	}
	
	/**
	 * Adds the specified project nature to the project description.
	 * 
	 * @param description
	 * @param natureId id of the nature to add
	 * @return <code>true</code> if the project description is changed by this method
	 */
	public static boolean addNature(final IProjectDescription description,
			final String natureId) {
		final String[] prevNatures= description.getNatureIds();
		final int idx= indexOf(prevNatures, natureId);
		
		if (idx == -1) {
			final String[] newNatures= new @NonNull String[prevNatures.length + 1];
			System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
			newNatures[prevNatures.length]= natureId;
			description.setNatureIds(newNatures);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Removes the specified project nature to the project description.
	 * 
	 * @param description
	 * @param natureId id of the nature to remove
	 * @return <code>true</code> if the project description is changed by this method
	 */
	public static boolean removeNature(final IProjectDescription description,
			final String natureId) {
		final String[] prevNatures= description.getNatureIds();
		final int idx= indexOf(prevNatures, natureId);
		
		if (idx >= 0) {
			final String[] newNatures= new @NonNull String[prevNatures.length - 1];
			System.arraycopy(prevNatures, 0, newNatures, 0, idx);
			System.arraycopy(prevNatures, idx + 1, newNatures, idx, prevNatures.length - idx - 1);
			description.setNatureIds(newNatures);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Adds the specified project builder to the project description.
	 * 
	 * @param description
	 * @param builderId the id of the builder to add
	 * @return <code>true</code> if the project description is changed by this method
	 */
	public static boolean addBuilder(final IProjectDescription description,
			final String builderId) {
		final ICommand[] prevCommands= description.getBuildSpec();
		final int idx= indexOf(prevCommands, builderId);
		
		if (idx == -1) {
			final ICommand newCommand= description.newCommand();
			newCommand.setBuilderName(builderId);
			final ICommand[] newCommands= new @NonNull ICommand[prevCommands.length+1];
			System.arraycopy(prevCommands, 0, newCommands, 0, prevCommands.length);
			newCommands[prevCommands.length]= newCommand;
			description.setBuildSpec(newCommands);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Adds the specified project builder to the project description.
	 * 
	 * @param description
	 * @param builderId the id of the builder to add
	 * @return <code>true</code> if the project description is changed by this method
	 */
	public static boolean removeBuilder(final IProjectDescription description,
			final String builderId) {
		final ICommand[] prevCommands= description.getBuildSpec();
		final int idx= indexOf(prevCommands, builderId);
		
		if (idx >= 0) {
			final ICommand[] newCommands= new @NonNull ICommand[prevCommands.length - 1];
			System.arraycopy(prevCommands, 0, newCommands, 0, idx);
			System.arraycopy(prevCommands, idx + 1, newCommands, idx, prevCommands.length - idx - 1);
			description.setBuildSpec(newCommands);
			return true;
		}
		
		return false;
	}
	
	
	private ProjectUtils() {}
	
}
