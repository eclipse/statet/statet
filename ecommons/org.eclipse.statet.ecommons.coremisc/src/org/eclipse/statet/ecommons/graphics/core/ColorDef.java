/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.graphics.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ColorDef {
	
	
	public static final @Nullable ColorDef parseRGBHex(final String s) {
		try {
			return new ColorDef(
					Integer.parseInt(s.substring(0, 2), 16),
					Integer.parseInt(s.substring(2, 4), 16),
					Integer.parseInt(s.substring(4, 6), 16) );
		}
		catch (final NumberFormatException e) {
			return null;
		}
	}
	
	
	protected int red;
	protected int green;
	protected int blue;
	
	
	protected ColorDef() {
	}
	
	public ColorDef(final int red, final int green, final int blue) {
		if (red < 0 || red > 255) {
			throw new IllegalArgumentException("red"); //$NON-NLS-1$
		}
		if (green < 0 || green > 255) {
			throw new IllegalArgumentException("green"); //$NON-NLS-1$
		}
		if (blue < 0 || blue > 255) {
			throw new IllegalArgumentException("blue"); //$NON-NLS-1$
		}
		
		this.red= red;
		this.green= green;
		this.blue= blue;
	}
	
	public ColorDef(final ColorDef def) {
		this.red= def.red;
		this.green= def.green;
		this.blue= def.blue;
	}
	
	
	public String getType() {
		return "rgb"; //$NON-NLS-1$
	}
	
	
	public final int getRed() {
		return this.red;
	}
	
	public final int getGreen() {
		return this.green;
	}
	
	public final int getBlue() {
		return this.blue;
	}
	
	
	@Override
	public int hashCode() {
		return (this.red << 16 | this.green << 8 | this.blue);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj instanceof ColorDef && equalsRGB((ColorDef)obj));
	}
	
	public final boolean equalsRGB(final ColorDef other) {
		return (other != null
				&& this.red == other.red && this.green == other.green && this.blue == other.blue);
	}
	
	
	public final void printRGBHex(final StringBuilder sb) {
		if (this.red < 0x10) {
			sb.append('0');
		}
		sb.append(Integer.toHexString(this.red));
		if (this.green < 0x10) {
			sb.append('0');
		}
		sb.append(Integer.toHexString(this.green));
		if (this.blue < 0x10) {
			sb.append('0');
		}
		sb.append(Integer.toHexString(this.blue));
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(7);
		sb.append('#');
		printRGBHex(sb);
		return sb.toString();
	}
	
}
