/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.graphics.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class NumberedRefColorDef extends ColorRefDef {
	
	
	private final int number;
	
	
	public NumberedRefColorDef(final int number, final ColorDef ref) {
		super(ref);
		this.number= number;
	}
	
	
	public int getNumber() {
		return this.number;
	}
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final NumberedRefColorDef other
						&& this.number == other.number
						&& getRef().equals(other.getRef()) ));
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("Id: "); //$NON-NLS-1$
		sb.append(this.number);
		sb.append(" / "); //$NON-NLS-1$
		sb.append(getRef().toString());
		return sb.toString();
	}
	
}
