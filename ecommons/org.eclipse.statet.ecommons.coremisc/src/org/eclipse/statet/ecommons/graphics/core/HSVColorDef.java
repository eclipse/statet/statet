/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.graphics.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class HSVColorDef extends ColorDef {
	
	
	protected float hue;
	protected float saturation;
	protected float value;
	
	
	public HSVColorDef(final float hue, final float saturation, final float value) {
		if (hue < 0f || hue > 1f) {
			throw new IllegalArgumentException("hue"); //$NON-NLS-1$
		}
		if (saturation < 0f || saturation > 1f) {
			throw new IllegalArgumentException("saturation"); //$NON-NLS-1$
		}
		if (value < 0f || value > 1f) {
			throw new IllegalArgumentException("value"); //$NON-NLS-1$
		}
		
		setHSV(hue, saturation, value);
		
		HSVtoRGB();
	}
	
	public HSVColorDef(final ColorDef def) {
		super(def);
		
		if (def instanceof final HSVColorDef other) {
			this.hue= other.hue;
			this.saturation= other.saturation;
			this.value= other.value;
		}
		else {
			RGBtoHSV();
		}
	}
	
	
	@Override
	public String getType() {
		return "hsv"; //$NON-NLS-1$
	}
	
	protected void setHSV(final float hue, final float saturation, final float value) {
		this.hue= Math.round(hue * 1000f) / 1000f;
		this.saturation= Math.round(saturation * 1000f) / 1000f;
		this.value= Math.round(value * 1000f) / 1000f;
	}
	
	private void HSVtoRGB() {
		float hue= this.hue;
		final float saturation= this.saturation;
		final float value= this.value;
		
		float r, g, b;
		if (saturation == 0) {
			r= g= b= value; 
		}
		else {
			if (hue == 1) {
				hue= 0;
			}
			hue *= 6;	
			final int i= (int) hue;
			final float f= hue - i;
			final float p= value * (1 - saturation);
			final float q= value * (1 - saturation * f);
			final float t= value * (1 - saturation * (1 - f));
			switch(i) {
				case 0:
					r= value;
					g= t;
					b= p;
					break;
				case 1:
					r= q;
					g= value;
					b= p;
					break;
				case 2:
					r= p;
					g= value;
					b= t;
					break;
				case 3:
					r= p;
					g= q;
					b= value;
					break;
				case 4:
					r= t;
					g= p;
					b= value;
					break;
				case 5:
				default:
					r= value;
					g= p;
					b= q;
					break;
			}
		}
		this.red= (int) (r * 255 + 0.5);
		this.green= (int) (g * 255 + 0.5);
		this.blue= (int) (b * 255 + 0.5);	
	}
	
	private void RGBtoHSV() {
		final float r= this.red / 255f;
		final float g= this.green / 255f;
		final float b= this.blue / 255f;
		final float max= Math.max(Math.max(r, g), b);
		final float min= Math.min(Math.min(r, g), b);
		final float delta= max - min;
		float hue= 0;
		if (delta != 0) {
			if (r == max) {
				hue= (g  - b) / delta;
			}
			else {
				if (g == max) {
					hue= 2 + (b - r) / delta;	
				} else {
					hue= 4 + (r - g) / delta;
				}
			}
			hue /= 6;
			if (hue < 0) {
				hue += 1;
			}
		}
		setHSV(hue, (max == 0) ? 0 : (max - min) / max, max);
	}
	
	
	public float getHue() {
		return this.hue;
	}
	
	public float getSaturation() {
		return this.saturation;
	}
	
	public float getValue() {
		return this.value;
	}
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final HSVColorDef other
						&& this.hue == other.hue && this.saturation == other.saturation && this.value == other.value ));
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("HSV: "); //$NON-NLS-1$
		sb.append(this.hue);
		sb.append(", "); //$NON-NLS-1$
		sb.append(this.saturation);
		sb.append(", "); //$NON-NLS-1$
		sb.append(this.value);
		sb.append(" (#"); //$NON-NLS-1$
		printRGBHex(sb);
		sb.append(")"); //$NON-NLS-1$
		return sb.toString();
	}
	
}
