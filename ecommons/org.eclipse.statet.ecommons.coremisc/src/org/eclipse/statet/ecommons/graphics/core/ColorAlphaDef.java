/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.graphics.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ColorAlphaDef extends ColorRefDef {
	
	
	private float alpha;
	
	
	public ColorAlphaDef(final ColorDef ref, final float alpha) {
		super(ref);
		
		if (alpha < 0 || alpha > 1) {
			throw new IllegalArgumentException("alpha"); //$NON-NLS-1$
		}
		setAlpha(alpha);
	}
	
	public ColorAlphaDef(final ColorDef ref, final int alpha255) {
		super(ref);
		
		if (alpha255 < 0 || alpha255 > 255) {
			throw new IllegalArgumentException("alpha"); //$NON-NLS-1$
		}
		setAlpha((alpha255) / 255f);
	}
	
	
	protected void setAlpha(final float alpha) {
		this.alpha= Math.round(alpha * 1000f) / 1000f;
	}
	
	public float getAlpha() {
		return this.alpha;
	}
	
	public int getAlpha255() {
		return Math.round(this.alpha * 255f);
	}
	
	public final void printRGBAHex(final StringBuilder sb) {
		printRGBHex(sb);
		final int alpha255= getAlpha255();
		if (alpha255 < 0x10) {
			sb.append('0');
		}
		sb.append(Integer.toHexString(alpha255));
	}
	
	
	@Override
	public int hashCode() {
		return super.hashCode() * (int) (235 * (1f + this.alpha));
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final ColorAlphaDef other
						&& this.alpha == other.alpha
						&& getRef().equals(other.getRef()) ));
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append(getRef().toString());
		sb.append(" × \u03B1: "); //$NON-NLS-1$
		sb.append(this.alpha);
		return sb.toString();
	}
	
}
