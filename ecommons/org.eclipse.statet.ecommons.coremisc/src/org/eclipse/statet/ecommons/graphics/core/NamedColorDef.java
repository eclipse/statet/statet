/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.graphics.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class NamedColorDef extends ColorDef {
	
	
	private final String name;
	
	
	public NamedColorDef(final String name, final int red, final int green, final int blue) {
		super(red, green, blue);
		this.name= name;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	
	@Override
	public String getType() {
		return "rgb-"; //$NON-NLS-1$
	}
	
	
	@Override
	public int hashCode() {
		return this.name.hashCode() + super.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final NamedColorDef other
						&& this.name.equals(other.name)
						&& equalsRGB(other) ));
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder(18);
		sb.append(this.name);
		sb.append(" (#"); //$NON-NLS-1$
		printRGBHex(sb);
		sb.append(')');
		return sb.toString();
	}
	
}