/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.io;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.AbstractObservableValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.variables.core.ObservableVariable;


@NonNullByDefault
public class ObservableFileValidator extends FileValidator {
	
	
	private class ResourceObservableValue extends AbstractObservableValue<@Nullable IResource> {
		
		
		private @Nullable IResource value;
		
		
		public ResourceObservableValue(final Realm realm) {
			super(realm);
		}
		
		
		@Override
		public Object getValueType() {
			return IResource.class;
		}
		
		@Override
		protected @Nullable IResource doGetValue() {
			return this.value;
		}
		
		@Override
		protected void doSetValue(final @Nullable IResource value) {
			setExplicit(value);
			checkExplicit();
		}
		
		void update(final @Nullable IResource newValue) {
			if ((newValue != null) ? !newValue.equals(this.value) : null != this.value) {
				fireValueChange(Diffs.createValueDiff(this.value, this.value= newValue));
			}
		}
		
	}
	
	private class FileStoreObservableValue extends AbstractObservableValue<@Nullable IFileStore> {
		
		
		private @Nullable IFileStore value;
		
		
		public FileStoreObservableValue(final Realm realm) {
			super(realm);
		}
		
		
		@Override
		public Object getValueType() {
			return IFileStore.class;
		}
		
		@Override
		protected @Nullable IFileStore doGetValue() {
			return this.value;
		}
		
		@Override
		protected void doSetValue(final @Nullable IFileStore value) {
			setExplicit(value);
			checkExplicit();
		}
		
		void update(final @Nullable IFileStore newValue) {
			if ((newValue != null) ? !newValue.equals(this.value) : null != this.value) {
				fireValueChange(Diffs.createValueDiff(this.value, this.value= newValue));
			}
		}
		
	}
	
	
	private final Realm realm;
	
	private @Nullable IChangeListener observableListener;
	
	private @Nullable ResourceObservableValue resourceObservable;
	private @Nullable FileStoreObservableValue fileStoreObservable;
	
	
	public ObservableFileValidator(final Realm realm) {
		this.realm= realm;
	}
	
	
	@Override
	void checkVariable(final IStringVariable variable) {
		if (variable instanceof ObservableVariable) {
			IChangeListener listener= this.observableListener;
			if (listener == null) {
				listener= new IChangeListener() {
					@Override
					public void handleChange(final ChangeEvent event) {
						updateVariableResolution();
					}
				};
				this.observableListener= listener;
			}
			((ObservableVariable) variable).addChangeListener(listener);
		}
	}
	
	@Override
	protected void setStatus(final @Nullable IStatus status) {
		super.setStatus(status);
		
		{	final ResourceObservableValue observable= this.resourceObservable;
			if (observable != null) {
				observable.update(getWorkspaceResource());
			}
		}
		{	final FileStoreObservableValue observable= this.fileStoreObservable;
			if (observable != null) {
				observable.update(getFileStore());
			}
		}
	}
	
	public IObservableValue<@Nullable IResource> getWorkspaceResourceObservable() {
		ResourceObservableValue observable= this.resourceObservable;
		if (observable == null) {
			observable= new ResourceObservableValue(this.realm);
			observable.update(getWorkspaceResource());
			this.resourceObservable= observable;
		}
		return observable;
	}
	
	public IObservableValue<@Nullable IFileStore> getFileStoreObservable() {
		FileStoreObservableValue observable= this.fileStoreObservable;
		if (observable == null) {
			observable= new FileStoreObservableValue(this.realm);
			observable.update(getFileStore());
			this.fileStoreObservable= observable;
		}
		return observable;
	}
	
}
