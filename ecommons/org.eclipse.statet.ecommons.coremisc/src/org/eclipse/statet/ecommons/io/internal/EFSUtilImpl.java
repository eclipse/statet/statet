/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.io.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URI;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.filesystem.IFileSystem;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.io.FileUtil;
import org.eclipse.statet.internal.ecommons.coreutils.CoreMiscellanyPlugin;


/**
 * impl for {@link EFS} / {@link IFileStore}
 */
@NonNullByDefault
public class EFSUtilImpl extends FileUtil {
	
	
	private static final String LABEL_2_LOCALFILE= "' (" + Messages.FileType_Local_name + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	
	
	private final IFileStore file;
	
	
	public EFSUtilImpl(final IFileStore file) {
		this.file= file;
	}
	
	
	@Override
	public String getLabel() {
		final IFileSystem system= this.file.getFileSystem();
		if (system.equals(EFS.getLocalFileSystem())) {
			return "'" + this.file.toString() + LABEL_2_LOCALFILE; //$NON-NLS-1$
		}
		return "'" + this.file.toURI().toString() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	@Override
	public URI getURI() {
		return this.file.toURI();
	}
	
	@Override
	public long getTimeStamp(final IProgressMonitor monitor) throws CoreException {
		return this.file.fetchInfo(EFS.NONE, monitor).getLastModified();
	}
	
	
	@Override
	public ReadTextFileOperation createReadTextFileOp(final ReaderAction action) {
		return new ReadTextFileOperation() {
			
			@Override
			protected FileInput getInput(final IProgressMonitor monitor) throws CoreException, IOException {
				try {
					final InputStream raw= EFSUtilImpl.this.file.openInputStream(EFS.NONE, monitor);
					return new FileInput(raw, null);
				}
				finally {
					monitor.done();
				}
			}
			
			@Override
			protected ReaderAction getAction() {
				return action;
			}
			
		};
	}
	
	@Override
	public WriteTextFileOperation createWriteTextFileOp(final String content) {
		return new WriteTextFileOperation() {
			
			@Override
			protected void writeImpl(final SubMonitor m) throws CoreException, IOException {
				final boolean exists= EFSUtilImpl.this.file.fetchInfo(EFS.NONE,  m.newChild(5)).exists();
				if (exists && (this.mode & (EFS.OVERWRITE | EFS.APPEND)) == 0) {
					throw new CoreException(new Status(IStatus.ERROR, CoreMiscellanyPlugin.BUNDLE_ID, 0,
							NLS.bind(Messages.Resource_error_AlreadyExists_message, " "+getLabel()+" "), null)); //$NON-NLS-1$ //$NON-NLS-2$
				}
				if (exists && (this.mode & EFS.APPEND) != 0 && !this.forceCharset) {
					try {
						final InputStream raw= EFSUtilImpl.this.file.openInputStream(EFS.NONE, m.newChild(5));
						final FileInput fi= new FileInput(raw, null);
						fi.close();
						final var defaultCharset= fi.getDefaultCharset();
						if (defaultCharset != null) {
							this.charset= defaultCharset;
						}
					}
					catch (final IOException e) { }
					finally {
						m.worked(5);
					}
				}
				else {
					m.worked(10);
				}
				try (final var writer= new OutputStreamWriter(EFSUtilImpl.this.file.openOutputStream(this.mode, m.newChild(5)), this.charset)) {
					writer.write(content);
					m.worked(75);
					writer.flush();
				}
			}
			
		};
	}
	
}
