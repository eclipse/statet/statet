/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.io.internal;

import org.eclipse.osgi.util.NLS;


public class Messages {
	
	
	public static String CoreUtility_Build_Job_title;
	public static String CoreUtility_Build_AllTask_name;
	public static String CoreUtility_Build_ProjectTask_name;
	public static String CoreUtility_Clean_Job_title;
	public static String CoreUtility_Clean_AllTask_name;
	public static String CoreUtility_Clean_ProjectTask_name;
	
	public static String Resource_error_NoInput_message;
	public static String Resource_error_NoInput_message_0;
	public static String Resource_error_DoesNotExists_message;
	public static String Resource_error_DoesNotExists_message_0;
	public static String Resource_error_AlreadyExists_message;
	public static String Resource_error_AlreadyExists_message_0;
	public static String Resource_error_NoValidSpecification_message;
	public static String Resource_error_NoValidSpecification_message_0;
	public static String Resource_error_NotRelative_message;
	public static String Resource_error_NotRelative_message_0;
	public static String Resource_error_Other_message;
	public static String Resource_error_Other_message_0;
	public static String Resource_error_IsFile_message;
	public static String Resource_error_IsFile_message_0;
	public static String Resource_error_IsDirectory_message;
	public static String Resource_error_IsDirectory_message_0;
	public static String Resource_error_NotLocal_message;
	public static String Resource_error_NotLocal_message_0;
	public static String Resource_error_NotInWorkspace_message;
	public static String Resource_error_NotInWorkspace_message_0;
	
	public static String FileType_Local_name;
	public static String FileType_Workspace_name;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
