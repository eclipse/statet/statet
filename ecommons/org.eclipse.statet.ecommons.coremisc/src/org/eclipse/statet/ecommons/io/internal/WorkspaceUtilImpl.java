/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.io.internal;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.Charset;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.io.FileUtil;


/**
 * Impl of FileUtil for Eclipse workspace files.
 */
@NonNullByDefault
public class WorkspaceUtilImpl extends FileUtil {
	
	private static final InputStream EMPTY_INPUT= new ByteArrayInputStream(new byte[0]);
	private static final String LABEL_2_WORKSPACE= "' (" + Messages.FileType_Workspace_name + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	
	
	private final IFile file;
	
	
	public WorkspaceUtilImpl(final IFile file) {
		this.file= file;
	}
	
	
	@Override
	public String getLabel() {
		return "'" + this.file.getFullPath().makeAbsolute().toString() + LABEL_2_WORKSPACE;  //$NON-NLS-1$
	}
	
	@Override
	public @Nullable URI getURI() {
		return this.file.getLocationURI();
	}
	
	@Override
	public long getTimeStamp(final IProgressMonitor monitor) throws CoreException {
		final long stamp= this.file.getLocalTimeStamp();
		return stamp;
	}
	
	
	@Override
	public ReadTextFileOperation createReadTextFileOp(final ReaderAction action) {
		return new ReadTextFileOperation() {
			
			@Override
			protected FileInput getInput(final IProgressMonitor monitor) throws CoreException, IOException {
				try {
					final InputStream raw= WorkspaceUtilImpl.this.file.getContents(true);
					final var charset= Charset.forName(WorkspaceUtilImpl.this.file.getCharset(true));
					return new FileInput(raw, charset);
				}
				finally {
					monitor.done();
				}
			}
			
			@Override
			protected ReaderAction getAction() {
				return action;
			}
			
			@Override
			public void doOperation(final IProgressMonitor monitor) throws CoreException, OperationCanceledException {
				runAsWorkspaceRunnable(monitor, null);
			}
			
		};
	}
	
	@Override
	public WriteTextFileOperation createWriteTextFileOp(final String content) {
		return new WriteTextFileOperation() {
			
			@Override
			public void doOperation(final IProgressMonitor monitor) throws CoreException, OperationCanceledException {
				final ISchedulingRule rule= (WorkspaceUtilImpl.this.file.exists()) ? 
						WorkspaceUtilImpl.this.file.getWorkspace().getRuleFactory().modifyRule(WorkspaceUtilImpl.this.file) :
						WorkspaceUtilImpl.this.file.getWorkspace().getRuleFactory().createRule(WorkspaceUtilImpl.this.file);
				runAsWorkspaceRunnable(monitor, rule);
			}
			
			@Override
			protected void writeImpl(final SubMonitor m) throws CoreException, UnsupportedEncodingException {
				final boolean exists= WorkspaceUtilImpl.this.file.exists();
				if (exists && ((this.mode & EFS.APPEND) != 0)) {
					if (this.forceCharset) {
						WorkspaceUtilImpl.this.file.setCharset(this.charset.name(), m.newChild(20));
					}
					else {
						this.charset= Charset.forName(WorkspaceUtilImpl.this.file.getCharset(true));
						m.worked(20);
					}
					
					WorkspaceUtilImpl.this.file.appendContents(new ByteArrayInputStream(content.getBytes(this.charset)),
							(IResource.FORCE | IResource.KEEP_HISTORY),
							m.newChild(80) );
				}
				else {
					if (exists && ((this.mode & EFS.OVERWRITE) != 0)) {
						WorkspaceUtilImpl.this.file.setContents(EMPTY_INPUT, IResource.FORCE | IResource.KEEP_HISTORY,
								m.newChild(15) );
					}
					else {
						WorkspaceUtilImpl.this.file.create(EMPTY_INPUT, IResource.FORCE, m.newChild(15));
					}
					if (this.forceCharset || !this.charset.equals(
							Charset.forName(WorkspaceUtilImpl.this.file.getCharset(true)) )) {
						WorkspaceUtilImpl.this.file.setCharset(this.charset.name(), m.newChild(5));
					}
					else {
						m.worked(5);
					}
					WorkspaceUtilImpl.this.file.setContents(new ByteArrayInputStream(content.getBytes(this.charset)),
							IResource.NONE, m.newChild(80) );
				}
			}
			
		};
	}
	
}
