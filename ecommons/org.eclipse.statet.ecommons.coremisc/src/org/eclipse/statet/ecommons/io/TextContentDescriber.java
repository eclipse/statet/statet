/*=============================================================================#
 # Copyright (c) 2004, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.platform: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.ITextContentDescriber;

import org.eclipse.statet.jcommons.io.ByteOrderMark;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * This class provides basis for text-based content describers. 
 */
@NonNullByDefault
public class TextContentDescriber implements ITextContentDescriber {
	
	
	private final static @NonNull QualifiedName[] SUPPORTED_OPTIONS= new @NonNull QualifiedName[] {
			IContentDescription.BYTE_ORDER_MARK
	};
	
	
	@Override
	public @NonNull QualifiedName[] getSupportedOptions() {
		return SUPPORTED_OPTIONS;
	}
	
	@Override
	public int describe(final Reader contents, final @Nullable IContentDescription description) throws IOException {
		// we want to be pretty loose on detecting the text content type  
		return INDETERMINATE;
	}
	
	@Override
	public int describe(final InputStream contents, final @Nullable IContentDescription description) throws IOException {
		if (description != null && description.isRequested(IContentDescription.BYTE_ORDER_MARK)) {
			checkByteOrderMark(contents, description);
		}
		
		// we want to be pretty loose on detecting the text content type
		return INDETERMINATE;
	}
	
	protected @Nullable ByteOrderMark checkByteOrderMark(final InputStream in,
			final @Nullable IContentDescription description) throws IOException {
		final ByteOrderMark bom= ByteOrderMark.read(in);
		if (bom != null && description != null) {
			description.setProperty(IContentDescription.BYTE_ORDER_MARK,
					switch (bom) {
					case UTF_8 -> IContentDescription.BOM_UTF_8;
					case UTF_16BE -> IContentDescription.BOM_UTF_16BE;
					case UTF_16LE -> IContentDescription.BOM_UTF_16LE;
					});
		}
		return bom;
	}
	
}
