/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.variables.core;

import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ObservableValueVariable<TValue> extends DynamicVariable implements ObservableVariable {
	
	
	private final IObservableValue<TValue> observable;
	
	
	public ObservableValueVariable(final String name, final @Nullable String description,
			final IObservableValue<TValue> observable) {
		super(name, description, false);
		
		if (observable == null) {
			throw new NullPointerException("observable"); //$NON-NLS-1$
		}
		
		this.observable= observable;
	}
	
	public ObservableValueVariable(final IStringVariable variable,
			final IObservableValue<TValue> observable) {
		this(variable.getName(), variable.getDescription(), observable);
	}
	
	
	public IObservableValue<TValue> getObservable() {
		return this.observable;
	}
	
	@Override
	public void addChangeListener(final IChangeListener listener) {
		this.observable.addChangeListener(listener);
	}
	
	@Override
	public void removeChangeListener(final IChangeListener listener) {
		this.observable.removeChangeListener(listener);
	}
	
	
	@Override
	public @Nullable String getValue(final @Nullable String argument) throws CoreException {
		final String value= toVariableValue(this.observable.getValue());
		if (value == null) {
			return super.getValue(null);
		}
		return value.toString();
	}
	
	protected @Nullable String toVariableValue(final TValue value) {
		return (value != null) ? value.toString() : null;
	}
	
}
