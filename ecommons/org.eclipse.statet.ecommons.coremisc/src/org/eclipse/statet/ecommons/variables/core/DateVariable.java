/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.variables.core;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Dynamic variable providing the formatted date. 
 * The variable supports a formatting pattern as optional argument.
 * 
 * It is recommended to overwrite {@link #getTimestamp()} to provide a 
 * constant or special timestamp.
 * 
 * @see SimpleDateFormat
 */
@NonNullByDefault
public class DateVariable extends DynamicVariable {
	
	
	public DateVariable(final String name, final String description) {
		super(name, description, true);
	}
	
	public DateVariable(final IStringVariable variable) {
		super(variable);
	}
	
	
	protected long getTimestamp() {
		return System.currentTimeMillis();
	}
	
	@Override
	public @Nullable String getValue(final @Nullable String argument) throws CoreException {
		if (argument == null) {
			return DateFormat.getDateInstance().format(getTimestamp());
		}
		return new SimpleDateFormat(argument).format(getTimestamp());
	}
	
}
