/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.variables.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IDynamicVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Wraps a dynamic variable.
 * 
 * Can be used to overwrite a single method (usually {@link #getValue(String)})
 * for a given instance of a dynamic variable.
 */
@NonNullByDefault
public class WrappedDynamicVariable implements IDynamicVariable {
	
	
	private final IDynamicVariable variable;
	
	
	public WrappedDynamicVariable(final IDynamicVariable variable) {
		this.variable= nonNullAssert(variable);
	}
	
	
	@Override
	public String getName() {
		return this.variable.getName();
	}
	
	@Override
	public @Nullable String getDescription() {
		return this.variable.getDescription();
	}
	
	@Override
	public boolean supportsArgument() {
		return this.variable.supportsArgument();
	}
	
	@Override
	public @Nullable String getValue(final @Nullable String argument) throws CoreException {
		return this.variable.getValue(argument);
	}
	
	
	@Override
	public int hashCode() {
		return this.variable.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| this.variable.equals(
						(obj instanceof WrappedDynamicVariable) ? ((WrappedDynamicVariable)obj).variable : obj ));
	}
	
}
