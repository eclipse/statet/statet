/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.variables.core;

import java.util.Collection;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.core.variables.IDynamicVariableResolver;
import org.eclipse.core.variables.IStringVariable;
import org.eclipse.core.variables.IValueVariable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class VariableUtils {
	
	
	public static <TVariable extends IStringVariable> void add(
			final Map<String, ? super TVariable> variables,
			final TVariable variableToAdd) {
		variables.put(variableToAdd.getName(), variableToAdd);
	}
	
	public static <TVariable extends IStringVariable> void add(
			final Map<String, ? super TVariable> variables,
			final Collection<TVariable> variablesToAdd) {
		for (final TVariable variable : variablesToAdd) {
			variables.put(variable.getName(), variable);
		}
	}
	
	public static void add(final Map<String, ? super IDynamicVariable> variables,
			final Collection<IDynamicVariable> variablesToAdd, final IDynamicVariableResolver resolver) {
		for (final IDynamicVariable variable : variablesToAdd) {
			variables.put(variable.getName(),
					new DynamicVariable.ResolverVariable(variable, resolver) );
		}
	}
	
	public static <TVariable extends IStringVariable> TVariable getChecked(
			final Map<@NonNull String, ? extends TVariable> variables,
			final String name) {
		final @Nullable TVariable variable= variables.get(name);
		if (variable == null) {
			throw new RuntimeException(String.format("Required variable '%1$s' is missing", name));
		}
		return variable;
	}
	
	
	public static @Nullable String getValue(final IStringVariable variable) throws CoreException {
		if (variable instanceof IValueVariable) {
			return ((IValueVariable) variable).getValue();
		}
		else /*(variable instanceof IDynamicVariable)*/ {
			return ((IDynamicVariable) variable).getValue(null);
		}
	}
	
	
	public static IDynamicVariable toStaticVariable(final IStringVariable variable) {
		return toStaticVariable(variable, variable);
	}
	
	public static IDynamicVariable toStaticVariable(final IStringVariable defVariable,
			final IStringVariable valueVariable) {
		try {
			final String value= getValue(valueVariable);
			return new StaticVariable(defVariable, value);
		}
		catch (final CoreException exception) {
			return new UnresolvedVariable(defVariable, exception);
		}
	}
	
}
