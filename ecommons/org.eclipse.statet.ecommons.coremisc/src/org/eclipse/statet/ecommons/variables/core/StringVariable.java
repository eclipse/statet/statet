/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.variables.core;

import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Simple string value e.g. as additional entry in variable selection dialogs.
 */
@NonNullByDefault
public class StringVariable implements IStringVariable {
	
	
	private final String name;
	
	private final @Nullable String description;
	
	
	/**
	 * Create a new variable.
	 * 
	 * @param name the name of the variable within <code>${}</code>
	 * @param description
	 */
	public StringVariable(final String name, final @Nullable String description) {
		this.name= name;
		this.description= description;
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public @Nullable String getDescription() {
		return this.description;
	}
	
}
