/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.variables.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.core.variables.IDynamicVariableResolver;
import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Dynamic variable to provide easily variable resolving for a given string variable.
 */
@NonNullByDefault
public class DynamicVariable extends StringVariable implements IDynamicVariable {
	
	
	@Deprecated
	public static abstract class LocationVariable extends DynamicVariable implements ILocationVariable {
		
		
		public LocationVariable(final IStringVariable variable) {
			super(variable);
		}
		
	}
	
	public static class ResolverVariable extends DynamicVariable {
		
		
		private final IDynamicVariableResolver resolver;
		
		
		public ResolverVariable(final String name, final @Nullable String description,
				final boolean supportsArgument, final IDynamicVariableResolver resolver) {
			super(name, description, supportsArgument);
			
			this.resolver= nonNullAssert(resolver);
		}
		
		public ResolverVariable(final IStringVariable variable,
				final IDynamicVariableResolver resolver) {
			super(variable);
			
			this.resolver= nonNullAssert(resolver);
		}
		
		
		@Override
		public @Nullable String getValue(final @Nullable String argument) throws CoreException {
			return this.resolver.resolveValue(this, argument);
		}
		
	}
	
	
	private final boolean isArgumentSupported;
	
	
	public DynamicVariable(final String name, final @Nullable String description,
			final boolean supportsArgument) {
		super(name, description);
		
		this.isArgumentSupported= supportsArgument;
	}
	
	public DynamicVariable(final IStringVariable variable) {
		super(variable.getName(), variable.getDescription());
		
		this.isArgumentSupported= (variable instanceof IDynamicVariable
				&& ((IDynamicVariable) variable).supportsArgument() );
	}
	
	
	@Override
	public boolean supportsArgument() {
		return this.isArgumentSupported;
	}
	
	@Override
	public @Nullable String getValue(final @Nullable String argument) throws CoreException {
		throw new CoreException(new Status(IStatus.ERROR, ECommonsVariablesCore.BUNDLE_ID,
				"At the moment not resolvable." ));
	}
	
}
