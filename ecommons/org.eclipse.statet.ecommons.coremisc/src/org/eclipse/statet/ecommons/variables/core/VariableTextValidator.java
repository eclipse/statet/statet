/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.variables.core;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.variables.core.VariableText2.Severities;


@NonNullByDefault
public class VariableTextValidator implements IValidator<String> {
	
	
	private final VariableText2 variableResolver;
	
	private final String message;
	
	
	public VariableTextValidator(final VariableText2 variableResolver, final String message) {
		this.variableResolver= variableResolver;
		this.message= message;
	}
	
	
	@Override
	public IStatus validate(final String value) {
		try {
			this.variableResolver.validate(value, Severities.CHECK_SYNTAX, null);
			return ValidationStatus.ok();
		}
		catch (final CoreException e) {
			final IStatus status= e.getStatus();
			return new Status(status.getSeverity(), status.getPlugin(),
					NLS.bind(this.message, status.getMessage()) );
		}
	}
	
}
