/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.variables.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class StaticVariable extends StringVariable implements IDynamicVariable {
	
	
	private final @Nullable String value;
	
	
	public StaticVariable(final String name, final @Nullable String description,
			final @Nullable String value) {
		super(name, description);
		
		this.value= value;
	}
	
	public StaticVariable(final IStringVariable variable,
			final @Nullable String value) {
		this(variable.getName(), variable.getDescription(), value);
	}
	
	
	@Override
	public boolean supportsArgument() {
		return false;
	}
	
	@Override
	public @Nullable String getValue(final @Nullable String argument) throws CoreException {
		return this.value;
	}
	
}
