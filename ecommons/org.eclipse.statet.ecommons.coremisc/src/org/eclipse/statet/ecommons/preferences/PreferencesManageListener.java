/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences;

import java.util.Set;
import java.util.concurrent.locks.Lock;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;


/**
 * Keeps a preferences model up-to-date listening to changes of in the specified settings group.
 * 
 * The listener be disposed, if no longer required.
 */
public class PreferencesManageListener implements SettingsChangeNotifier.ManageListener {
	
	
	private AbstractPreferencesModelObject fModel;
	private PreferenceAccess fPrefAccess;
	private final String[] fGroupIds;
	
	
	/**
	 * Creates a new listener for a single group id.
	 */
	public PreferencesManageListener(final AbstractPreferencesModelObject model, final PreferenceAccess prefs, final String groupId) {
		this(model, prefs, new String[] { groupId });
	}
	
	/**
	 * Creates a new listener for multiple group ids.
	 */
	public PreferencesManageListener(final AbstractPreferencesModelObject model, final PreferenceAccess prefs, final String[] groupIds) {
		fModel = model;
		fPrefAccess = prefs;
		fGroupIds = groupIds;
		final Lock lock = fModel.getWriteLock();
		lock.lock();
		try {
			PreferencesUtil.getSettingsChangeNotifier().addManageListener(this);
			fModel.load(fPrefAccess);
		}
		finally {
			lock.unlock();
		}
	}
	
	@Override
	public void beforeSettingsChangeNotification(final Set<String> groupIds) {
		for (final String id : fGroupIds) {
			if (groupIds.contains(id)) {
				final Lock lock = fModel.getWriteLock();
				lock.lock();
				try {
					fModel.load(fPrefAccess);
				}
				finally {
					lock.unlock();
				}
				return;
			}
		}
	}
	
	@Override
	public void afterSettingsChangeNotification(final Set<String> groupIds) {
		fModel.resetDirty();
	}
	
	public void dispose() {
		PreferencesUtil.getSettingsChangeNotifier().removeManageListener(this);
		fModel = null;
		fPrefAccess = null;
	}
	
}
