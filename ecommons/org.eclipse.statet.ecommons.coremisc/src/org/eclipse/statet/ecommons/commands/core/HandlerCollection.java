/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.commands.core;

import org.eclipse.core.commands.IHandler2;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Utility to manage the command handler of a service layer (view, dialog, etc.).
 */
@NonNullByDefault
public interface HandlerCollection extends Disposable {
	
	
	void add(String commandId, IHandler2 handler, int flags);
	void add(String commandId, IHandler2 handler);
	
	@Nullable IHandler2 get(String commandId);
	
	void update(@Nullable Object evaluationContext);
	void update(@Nullable Object evaluationContext, int flags);
	
	
	@Override
	void dispose();
	
}
