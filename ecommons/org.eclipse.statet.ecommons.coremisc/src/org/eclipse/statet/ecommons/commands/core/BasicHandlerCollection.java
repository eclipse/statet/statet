/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.commands.core;

import org.eclipse.core.commands.IHandler2;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Utility to manage the command handler of a service layer (view, dialog, etc.).
 */
@NonNullByDefault
public class BasicHandlerCollection extends AbstractHandlerCollection<AbstractHandlerCollection.CommandRecord> {
	
	
	public BasicHandlerCollection() {
	}
	
	
	@Override
	protected CommandRecord createCommandRecord(final String commandId, final IHandler2 handler, final int flags) {
		return new CommandRecord(handler, flags);
	}
	
}
