/*=============================================================================#
 # Copyright (c) 2004, 2025 TeXlipse-Project (texlipse.sf.net) and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     Tor Arne Vestbø - initial API and implementation
 #=============================================================================*/

#include <jni.h>
/* Header for class org_eclipse_statet_ecommons_io_win_DDEClient */

#ifndef _Included_org_eclipse_statet_ecommons_io_win_DDEClient
#define _Included_org_eclipse_statet_ecommons_io_win_DDEClient
#ifdef __cplusplus
extern "C" {
#endif


JNIEXPORT jint JNICALL Java_org_eclipse_statet_ecommons_io_win_DDEClient_ddeExecute(
		JNIEnv *, jclass, jstring, jstring, jstring);

#ifdef __cplusplus
}
#endif
#endif
