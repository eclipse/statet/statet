/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.ObservableEvent;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.core.AggregateValidationStatus;
import org.eclipse.statet.ecommons.databinding.core.DataStatus;
import org.eclipse.statet.ecommons.databinding.core.util.DirtyTracker;
import org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI;


/**
 * Abstract LaunchConfigurationTab with support of a DataBindingContext.
 * <p>You have to implement:
 * <ul>
 *   <li>{@link #addBindings(DataBindingContext, Realm)} (add binding to the context)</li>
 *   <li>{@link #doInitialize(ILaunchConfiguration)} (load values from config to the model)</li>
 *   <li>{@link #doSave(ILaunchConfigurationWorkingCopy)} (save values from model to config)</li>
 * </ul></p>
 * <p>Validation status with severity WARNING are handled like errors, but can be saved.
 * </p>
 */
@NonNullByDefault
public abstract class LaunchConfigTabWithDbc extends AbstractLaunchConfigurationTab {
	
	
	private DataBindingContext dbc;
	
	private AggregateValidationStatus aggregateStatus;
	private @Nullable IStatus currentStatus;
	
	private boolean initializing;
	
	
	protected LaunchConfigTabWithDbc() {
	}
	
	public String getValidationErrorAttr() {
		return getId() + "/validation.hasError"; //$NON-NLS-1$
	}
	
	protected void updateDialogState() {
		if (!isInitializing()) {
			switch (DataStatus.getInfoSeverity(this.currentStatus)) {
			case DataStatus.ERROR:
			case DataStatus.UPDATEABLE_ERROR:
				setMessage(null);
				setWarningMessage(null);
				setErrorMessage(this.currentStatus.getMessage());
				break;
			case IStatus.WARNING:
				setMessage(null);
				setWarningMessage(this.currentStatus.getMessage());
				setErrorMessage(null);
				break;
			case IStatus.INFO:
				setMessage(this.currentStatus.getMessage());
				setWarningMessage(null);
				setErrorMessage(null);
				break;
			default:
				setMessage(null);
				setWarningMessage(null);
				setErrorMessage(null);
				break;
			}
			updateLaunchConfigurationDialog();
		}
	}
	
	protected Realm getRealm() {
		return Realm.getDefault();
	}
	
	protected void initBindings() {
		this.dbc= new DataBindingContext(getRealm());
		
		addBindings(this.dbc);
		
		this.aggregateStatus= new AggregateValidationStatus(this.dbc, AggregateValidationStatus.MAX_INFO_SEVERITY);
		this.aggregateStatus.addValueChangeListener(new IValueChangeListener<IStatus>() {
			@Override
			public void handleValueChange(final ValueChangeEvent<? extends IStatus> event) {
				LaunchConfigTabWithDbc.this.currentStatus= event.diff.getNewValue();
				updateDialogState();
			}
		});
		this.currentStatus= ValidationStatus.ok();
		
		new DirtyTracker(this.dbc) {
			@Override
			public void handleChange(final ObservableEvent event) {
				if (!isDirty()) {
					LaunchConfigTabWithDbc.this.currentStatus= LaunchConfigTabWithDbc.this.aggregateStatus.getValue();
					setDirty(true);
					updateDialogState();
				}
			}
		};
		
		getControl().addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				disposeBindings();
			}
		});
	}
	
	private void disposeBindings() {
		if (this.aggregateStatus != null) {
			this.aggregateStatus.dispose();
			this.aggregateStatus= null;
		}
		if (this.dbc != null) {
			this.dbc.dispose();
			this.dbc= null;
		}
	}
	
	protected DataBindingContext getDataBindingContext() {
		return this.dbc;
	}
	
	protected void addBindings(final DataBindingContext dbc) {
	}
	
	@Override
	public void dispose() {
		super.dispose();
		disposeBindings();
	}
	
	
	@SuppressWarnings("null")
	protected String readAttribute(final ILaunchConfiguration configuration,
			final String attributeName, final String defaultValue) {
		String value= nonNullAssert(defaultValue);
		try {
			value= configuration.getAttribute(attributeName, value);
		}
		catch (final CoreException e) {
			logReadingError(e);
		}
		return value;
	}
	
	protected boolean readAttribute(final ILaunchConfiguration configuration,
			final String attributeName, final boolean defaultValue) {
		boolean value= defaultValue;
		try {
			value= configuration.getAttribute(attributeName, value);
		}
		catch (final CoreException e) {
			logReadingError(e);
		}
		return value;
	}
	
	protected int readAttribute(final ILaunchConfiguration configuration,
			final String attributeName, final int defaultValue) {
		int value= defaultValue;
		try {
			value= configuration.getAttribute(attributeName, value);
		}
		catch (final CoreException e) {
			logReadingError(e);
		}
		return value;
	}
	
	@SuppressWarnings("null")
	protected List<String> readAttribute(final ILaunchConfiguration configuration,
			final String attributeName, final List<String> defaultValue) {
		List<String> value= nonNullAssert(defaultValue);
		try {
			value= configuration.getAttribute(attributeName, value);
		}
		catch (final CoreException e) {
			logReadingError(e);
		}
		return value;
	}
	
	protected void logReadingError(final CoreException e) {
		StatusManager.getManager().handle(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID, 0,
				NLS.bind("An error occurred while reading launch configuration (name: ''{0}'', id: ''{1}'')", //$NON-NLS-1$
						getName(), getId()),
				e ));
	}
	
	
	@Override
	public void initializeFrom(final ILaunchConfiguration configuration) {
		this.initializing= true;
		doInitialize(configuration);
		setDirty(false);
		for (final Object obj : this.dbc.getBindings()) {
			((Binding) obj).validateTargetToModel();
		}
		this.currentStatus= this.aggregateStatus.getValue();
		this.initializing= false;
		updateDialogState();
	}
	
	@Override
	public void activated(final ILaunchConfigurationWorkingCopy workingCopy) {
		updateDialogState();
	}
	
	@Override
	public void deactivated(final ILaunchConfigurationWorkingCopy workingCopy) {
	}
	
	@Override
	public void performApply(final ILaunchConfigurationWorkingCopy configuration) {
		if (!canSave()) {
			configuration.setAttribute(getValidationErrorAttr(), true); // To enable the revert button
			return;
		}
		configuration.removeAttribute(getValidationErrorAttr());
		if (isDirty()) {
			doSave(configuration);
			setDirty(false);
		}
	}
	
	protected final boolean isInitializing() {
		return this.initializing;
	}
	
	protected abstract void doInitialize(ILaunchConfiguration configuration);
	protected abstract void doSave(ILaunchConfigurationWorkingCopy configuration);
	
	@Override
	public boolean isValid(final ILaunchConfiguration launchConfig) {
		return (DataStatus.getInfoSeverity(this.currentStatus) <= IStatus.WARNING);
	}
	
	@Override
	public boolean canSave() {
		return (this.currentStatus.getSeverity() < IStatus.ERROR);
	}
	
}
