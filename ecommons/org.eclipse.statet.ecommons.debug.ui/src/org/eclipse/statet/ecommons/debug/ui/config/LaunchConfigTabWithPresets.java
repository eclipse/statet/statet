/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config;

import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI;
import org.eclipse.statet.ecommons.ui.components.DropDownButton;
import org.eclipse.statet.internal.ecommons.debug.ui.Messages;


@NonNullByDefault
public abstract class LaunchConfigTabWithPresets extends LaunchConfigTabWithDbc {
	
	
	private ImList<ILaunchConfiguration> presets= ImCollections.emptyList();
	
	private @Nullable Map<String, Object> presetToLoad;
	private @Nullable ILaunchConfiguration presetLoaded;
	
	
	protected LaunchConfigTabWithPresets() {
	}
	
	
	protected void setPresets(final LaunchConfigPresets presets) {
		this.presets= presets.toList();
	}
	
	
	protected DropDownButton addPresetsButton(final Composite parent) {
		final DropDownButton button= new DropDownButton(parent, SWT.SINGLE);
		button.setText(Messages.ConfigTab_LoadPreset_label);
		final GridData gd= new GridData(SWT.RIGHT, SWT.FILL, true, false);
		button.setLayoutData(gd);
		
		final Menu menu= button.getDropDownMenu();
		final SelectionListener selectionListener= new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				loadPreset((ILaunchConfiguration)e.widget.getData());
			}
		};
		for (final ILaunchConfiguration preset : this.presets) {
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(LaunchConfigPresets.getName(preset));
			item.setData(preset);
			item.addSelectionListener(selectionListener);
		}
		
		if (this.presets.isEmpty()) {
			button.setEnabled(false);
		}
		
		return button;
	}
	
	
	protected ImList<ILaunchConfigurationTab> getPresetTabs(final ILaunchConfiguration config) {
		final ILaunchConfigurationTab[] tabs= getLaunchConfigurationDialog().getTabs();
		if (tabs != null && tabs[tabs.length - 1] instanceof CommonTab) {
			return ImCollections.newList(tabs, 0, tabs.length - 1);
		}
		else {
			return ImCollections.newList();
		}
	}
	
	protected void loadPreset(final ILaunchConfiguration preset) {
		try {
			this.presetToLoad= preset.getAttributes();
			setDirty(true);
			updateLaunchConfigurationDialog();
			
			final ILaunchConfiguration config= this.presetLoaded;
			if (config != null) {
				final List<ILaunchConfigurationTab> tabs= getPresetTabs(config);
				
				for (final ILaunchConfigurationTab tab : tabs) {
					tab.initializeFrom(config);
				}
			}
		}
		catch (final Exception e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID, 0,
						"An error occurred while loading the launch configuration preset.", e ),
					(StatusManager.LOG | StatusManager.SHOW) );
		}
		finally {
			this.presetToLoad= null;
			this.presetLoaded= null;
		}
	}
	
	
	@Override
	protected void doInitialize(final ILaunchConfiguration configuration) {
	}
	
	@Override
	public void performApply(final ILaunchConfigurationWorkingCopy configuration) {
		final Map<String, Object> presetToLoad= this.presetToLoad;
		if (presetToLoad != null) {
			this.presetToLoad= null;
			
			configuration.removeAttribute(getValidationErrorAttr());
			
			LaunchConfigPresets.apply(presetToLoad, configuration);
			
			this.presetLoaded= configuration;
			return;
		}
		
		super.performApply(configuration);
	}
	
}
