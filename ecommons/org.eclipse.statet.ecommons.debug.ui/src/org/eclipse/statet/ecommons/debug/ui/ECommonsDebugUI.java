/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui;


public final class ECommonsDebugUI {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.ecommons.debug.ui"; //$NON-NLS-1$
	
	
	public static final String TERMINATE_COMMAND_ID= "org.eclipse.debug.ui.commands.Terminate"; //$NON-NLS-1$
	
	public static final String TERMINATE_RELAUNCH_COMMAND_ID= "org.eclipse.debug.ui.commands.TerminateAndRelaunch"; //$NON-NLS-1$
	
	
	public static final String WATCH_COMMAND_ID= "org.eclipse.jdt.debug.ui.commands.Watch"; //$NON-NLS-1$
	
	public static final String INSPECT_COMMAND_ID= "org.eclipse.jdt.debug.ui.commands.Inspect"; //$NON-NLS-1$
	
}
