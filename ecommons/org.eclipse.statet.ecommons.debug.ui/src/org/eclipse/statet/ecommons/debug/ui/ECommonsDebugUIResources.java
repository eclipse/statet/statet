/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.UIResources;
import org.eclipse.statet.internal.ecommons.debug.ui.ECommonsDebugUIPlugin;


@NonNullByDefault
public class ECommonsDebugUIResources extends UIResources {
	
	
	private static final String NS= "org.eclipse.statet.ecommons.debug"; //$NON-NLS-1$
	
	
	public static final String OBJ_VARIABLE_PARTITION=          NS + "/images/obj/VariablePartition"; //$NON-NLS-1$
	public static final String OBJ_VARIABLE_ITEM=               NS + "/images/obj/VariableItem"; //$NON-NLS-1$
	public static final String OBJ_VARIABLE_DIM=                NS + "/images/obj/VariableDim"; //$NON-NLS-1$
	
	public static final String OVR_BREAKPOINT_INSTALLED=        NS + "/images/ovr/Breakpoint.Installed"; //$NON-NLS-1$
	public static final String OVR_BREAKPOINT_INSTALLED_DISABLED= NS + "/images/ovr/Breakpoint.Installed.disabled"; //$NON-NLS-1$
	
	public static final String OVR_BREAKPOINT_CONDITIONAL=      NS + "/images/ovr/Breakpoint.Conditional"; //$NON-NLS-1$
	public static final String OVR_BREAKPOINT_CONDITIONAL_DISABLED= NS + "/images/ovr/Breakpoint.Conditional.disabled"; //$NON-NLS-1$
	
	public static final String OVR_METHOD_BREAKPOINT_ENTRY=     NS + "/images/ovr/Breakpoint.MethodEntry"; //$NON-NLS-1$
	public static final String OVR_METHOD_BREAKPOINT_ENTRY_DISABLED= NS + "/images/ovr/Breakpoint.MethodEntry.disabled"; //$NON-NLS-1$
	public static final String OVR_METHOD_BREAKPOINT_EXIT=      NS + "/images/ovr/Breakpoint.MethodExit"; //$NON-NLS-1$
	public static final String OVR_METHOD_BREAKPOINT_EXIT_DISABLED= NS + "/images/ovr/Breakpoint.MethodExit.disabled"; //$NON-NLS-1$
	
	
	public static final ECommonsDebugUIResources INSTANCE= new ECommonsDebugUIResources();
	
	
	private ECommonsDebugUIResources() {
		super(ECommonsDebugUIPlugin.getInstance().getImageRegistry());
	}
	
	
}
