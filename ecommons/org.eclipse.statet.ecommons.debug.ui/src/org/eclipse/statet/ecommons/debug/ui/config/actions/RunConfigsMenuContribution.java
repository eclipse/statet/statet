/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config.actions;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.bindings.TriggerSequence;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.help.IWorkbenchHelpSystem;
import org.eclipse.ui.keys.IBindingService;
import org.eclipse.ui.menus.IWorkbenchContribution;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.collections.IdentitySet;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;
import org.eclipse.statet.ecommons.ui.actions.ListContributionItem;
import org.eclipse.statet.ecommons.ui.actions.SimpleContributionItem;
import org.eclipse.statet.ecommons.ui.actions.SubMenuContributionItem;
import org.eclipse.statet.ecommons.ui.util.MessageUtils;
import org.eclipse.statet.internal.ecommons.debug.ui.Messages;


@NonNullByDefault
public class RunConfigsMenuContribution<TElement> extends ListContributionItem
		implements IWorkbenchContribution, IExecutableExtension {
	
	
	private class Data {
		
		public final IWorkbenchWindow window;
		
		public final TElement element;
		
		public final LaunchConfigManager<TElement> manager;
		
		public final @Nullable ILaunchConfiguration activeConfig;
		
		public final @Nullable IBindingService bindingService;
		
		public final @Nullable IWorkbenchHelpSystem helpSystem;
		
		
		public Data(final IWorkbenchWindow window, final IServiceLocator serviceLocator,
				final TElement file, final LaunchConfigManager<TElement> manager) {
			this.window= window;
			this.element= file;
			this.manager= manager;
			this.activeConfig= manager.getActiveConfig();
			this.bindingService= (serviceLocator != null) ?
					(IBindingService) serviceLocator.getService(IBindingService.class) : null;
			this.helpSystem= PlatformUI.getWorkbench().getHelpSystem();
		}
		
	}
	
	protected class ConfigContribution extends SubMenuContributionItem implements SelectionListener {
		
		
		private static final String ACTIVATE= "activate"; //$NON-NLS-1$
		private static final String EDIT= "edit"; //$NON-NLS-1$
		
		
		private final String label;
		
		private final Image icon;
		
		private final ILaunchConfiguration config;
		
		private Data data;
		
		
		public ConfigContribution(final String label, final Image icon,
				final ILaunchConfiguration configuration) {
			super();
			
			this.label= label;
			this.icon= icon;
			this.config= configuration;
		}
		
		
		protected IWorkbenchWindow getWindow() {
			return this.data.window;
		}
		
		protected TElement getElement() {
			return this.data.element;
		}
		
		protected LaunchConfigManager<TElement> getManager() {
			return this.data.manager;
		}
		
		@Override
		protected String getLabel() {
			return this.label;
		}
		
		@Override
		protected Image getImage() {
			return this.icon;
		}
		
		public ILaunchConfiguration getConfiguration() {
			return this.config;
		}
		
		protected boolean isActive() {
			return (this.config == this.data.activeConfig);
		}
		
		
		@Override
		protected void fillMenu(final Menu menu) {
			addLaunchItem(menu, ImCollections.newIdentitySet(), null, true, null, null);
			
			addActivateItem(menu, null);
			addEditItem(menu, null);
		}
		
		protected void addActivateItem(final Menu menu, final String helpContextId) {
			final MenuItem item= new MenuItem(menu, SWT.RADIO);
			item.setText(Messages.RunAction_ActivateConfig_label);
			item.setData(ACTIVATE);
			item.addSelectionListener(this);
			item.setSelection(isActive());
			if (this.data.helpSystem != null && helpContextId != null) {
				this.data.helpSystem.setHelp(item, helpContextId);
			}
		}
		
		protected void addEditItem(final Menu menu, final String helpContextId) {
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(Messages.RunAction_EditConfig_label);
			item.setData(EDIT);
			item.addSelectionListener(this);
			if (this.data.helpSystem != null && helpContextId != null) {
				this.data.helpSystem.setHelp(item, helpContextId);
			}
		}
		
		protected void addLaunchItem(final Menu menu,				final ImIdentitySet<String> launchFlags,
				final @Nullable String actionDetailInfo, final boolean enabled,
				@Nullable String commandId, final @Nullable String helpContextId) {
			if (getMode() != ActionUtil.ACTIVE_EDITOR_MODE) {
				commandId= null;
			}
			
			final StringBuilder label= getStringBuilder();
			label.append(this.data.manager.getActionLabel(launchFlags));
			if (actionDetailInfo != null) {
				label.append(actionDetailInfo);
			}
			if (isActive() && commandId != null && this.data.bindingService != null) {
				final TriggerSequence binding= this.data.bindingService.getBestActiveBindingFor(commandId);
				if (binding != null) {
					label.append('\t');
					label.append(binding.format());
				}
			}
			
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(label.toString());
			item.setImage(this.data.manager.getActionImage(launchFlags));
			item.setData(launchFlags);
			item.addSelectionListener(this);
			item.setEnabled(enabled);
			if (this.data.helpSystem != null && helpContextId != null) {
				this.data.helpSystem.setHelp(item, helpContextId);
			}
			
		}
		
		
		@Override
		public void widgetDefaultSelected(final SelectionEvent e) {
		}
		
		@Override
		public void widgetSelected(final SelectionEvent e) {
			final LaunchConfigManager<TElement> manager= getManager();
			
			final Object data= e.widget.getData();
			if (data instanceof ImIdentitySet) {
				manager.launch(this.config, getElement(), (ImIdentitySet<String>) data);
				return;
			}
			if (data == ACTIVATE) {
				manager.setActiveConfig(this.config);
				return;
			}
			if (data == EDIT) {
				final IWorkbenchWindow window= getWindow();
				if (getMode() == ActionUtil.ACTIVE_EDITOR_MODE) {
					ActionUtil.activateActiveEditor(window);
				}
				manager.openConfigurationDialog(window.getShell(),
						new StructuredSelection(this.config) );
				return;
			}
		}
		
		
	}
	
	private class ConfigureContribution extends SimpleContributionItem {
		
		
		private final Data data;
		
		
		public ConfigureContribution(final Data data) {
			super(Messages.RunAction_CreateEditConfigs_label, null);
			
			this.data= data;
		}
		
		
		protected IWorkbenchWindow getWindow() {
			return this.data.window;
		}
		
		protected LaunchConfigManager<TElement> getManager() {
			return this.data.manager;
		}
		
		
		@Override
		protected void execute(final Event event) throws ExecutionException {
			final IWorkbenchWindow window= getWindow();
			if (getMode() == ActionUtil.ACTIVE_EDITOR_MODE) {
				ActionUtil.activateActiveEditor(window);
			}
			getManager().openConfigurationDialog(window.getShell(), null);
		}
		
	}
	
	private class ShortcutContribution extends SimpleContributionItem {
		
		
		private Data data;
		
		private final IdentitySet<String> launchFlags;
		
		
		public ShortcutContribution(final String label, final @Nullable ImageDescriptor icon,
				final IdentitySet<String> launchFlags) {
			super(label, null, icon, null);
			
			this.launchFlags= launchFlags;
		}
		
		
		protected LaunchConfigManager<TElement> getManager() {
			return this.data.manager;
		}
		
		protected TElement getElement() {
			return this.data.element;
		}
		
		protected IdentitySet<String> getLaunchFlags() {
			return this.launchFlags;
		}
		
		
		@Override
		protected void execute(final Event event) throws ExecutionException {
			getManager().launch(this.data.activeConfig, getElement(), getLaunchFlags());
		}
		
	}
	
	
	private final ActionUtil<TElement> util;
	private IServiceLocator serviceLocator;
	
	private final StringBuilder sBuilder= new StringBuilder(32);
	
	
	public RunConfigsMenuContribution(final ActionUtil<TElement> util) {
		this.util= util;
	}
	
	
	protected ActionUtil<TElement> getUtil() {
		return this.util;
	}
	
	@Override
	public void setInitializationData(final IConfigurationElement config,
			final String propertyName, final @Nullable Object data) throws CoreException {
		if (data instanceof String) {
			switch ((String) data) {
			case "activeEditor": //$NON-NLS-1$
				setMode(ActionUtil.ACTIVE_EDITOR_MODE);
				break;
			case "activeMenuSelection": //$NON-NLS-1$
				setMode(ActionUtil.ACTIVE_MENU_SELECTION_MODE);
				break;
			default:
				break;
			}
		}
	}
	
	protected void setMode(final byte mode) {
		this.util.setMode(mode);
	}
	
	protected byte getMode() {
		return this.util.getMode();
	}
	
	@Override
	public void initialize(final IServiceLocator serviceLocator) {
		this.serviceLocator= serviceLocator;
	}
	
	
	protected StringBuilder getStringBuilder() {
		this.sBuilder.setLength(0);
		return this.sBuilder;
	}
	
	
	@Override
	protected void createContributionItems(final List<IContributionItem> items) {
		final IWorkbenchWindow window= ActionUtil.getWindow(this.serviceLocator);
		final @Nullable TElement element= this.util.getLaunchElement(window);
		if (element == null) {
			return;
		}
		final LaunchConfigManager<TElement> manager= this.util.getManager(window, element);
		if (manager == null) {
			return;
		}
		
		final ImList<ILaunchConfiguration> configs= manager.getAvailableConfigs();
		final Data data= new Data(window,
				(this.serviceLocator != null) ? this.serviceLocator : window,
				element, manager );
		
		if (getMode() != ActionUtil.ACTIVE_EDITOR_MODE && data.activeConfig != null) {
			final List<ImIdentitySet<String>> shortcuts= getContextShortcutLaunchFlags();
			if (!shortcuts.isEmpty()) {
				for (final ImIdentitySet<String> launchFlags : shortcuts) {
					items.add(createContextShortcut(data, launchFlags));
				}
				items.add(new Separator());
			}
		}
		
		int i= 0;
		for (int num= 1; i < configs.size(); i++, num++) {
			final ILaunchConfiguration configuration= configs.get(i);
			
			final StringBuilder label= getStringBuilder();
			String mnemonic= null;
			if (num > 0 && num <= 10) {
				mnemonic= Integer.toString((num % 10));
				label.append('&');
				label.append(mnemonic);
				label.append(' ');
			}
			label.append(MessageUtils.escapeForMenu(configuration.getName()));
			final Image image= manager.getImage(configuration);
			
			final ConfigContribution item= createConfigContribution(label, image, configuration);
			item.data= data;
			
			items.add(item);
		}
		
		if (getMode() == ActionUtil.ACTIVE_EDITOR_MODE || configs.isEmpty()) {
			items.add(new ConfigureContribution(data));
		}
	}
	
	protected List<ImIdentitySet<String>> getContextShortcutLaunchFlags() {
		return ImCollections.emptyList();
	}
	
	private ShortcutContribution createContextShortcut(final Data data,
			final IdentitySet<String> launchFlags) {
		final Image icon= data.manager.getActionImage(launchFlags);
		final String label= data.manager.getLabel(data.activeConfig, launchFlags, false);
		return new ShortcutContribution(label,
				(icon != null) ? ImageDescriptor.createFromImage(icon) : null,
				launchFlags );
	}
	
	protected ConfigContribution createConfigContribution(final StringBuilder label,
			final Image icon,			final ILaunchConfiguration configuration) {
		return new ConfigContribution(label.toString(), icon, configuration);
	}
	
}
