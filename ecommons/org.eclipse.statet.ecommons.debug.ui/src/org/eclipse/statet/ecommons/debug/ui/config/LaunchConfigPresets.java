/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class LaunchConfigPresets {
	
	
	public static final String UNDEFINED_VALUE= "!UNDEFINED!"; //$NON-NLS-1$
	
	
	private static final String NAME_ATTR_NAME= "Preset.name"; //$NON-NLS-1$
	
	
	static boolean isInternalArgument(final String name) {
		return (name.startsWith("Preset.")); //$NON-NLS-1$
	}
	
	static String getName(final ILaunchConfiguration preset) {
		try {
			return nonNullAssert(preset.getAttribute(NAME_ATTR_NAME, (String) null));
		}
		catch (final CoreException e) {
			throw new RuntimeException(e);
		}
	}
	
	static void apply(final Map<String, Object> preset,
			final ILaunchConfigurationWorkingCopy config) {
		for (final Entry<String, Object> entry : preset.entrySet()) {
			final String name= entry.getKey();
			if (LaunchConfigPresets.isInternalArgument(name)) {
				continue;
			}
			final Object value= entry.getValue();
			if (value instanceof String) {
				if (value.equals(LaunchConfigPresets.UNDEFINED_VALUE)) {
					config.removeAttribute(name);
				}
				else {
					config.setAttribute(name, (String) value);
				}
			}
			else if (value instanceof Integer) {
				config.setAttribute(name, value);
			}
			else if (value instanceof Boolean) {
				config.setAttribute(name, value);
			}
			else if (value instanceof List) {
				config.setAttribute(name, (List) value);
			}
			else if (value instanceof Map) {
				config.setAttribute(name, (Map) value);
			}
		}
	}
	
	public static void apply(final ILaunchConfiguration preset,
			final ILaunchConfigurationWorkingCopy config) {
		try {
			apply(preset.getAttributes(), config);
		}
		catch (final CoreException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private final ILaunchConfigurationType type;
	
	private final List<ILaunchConfiguration> presets= new ArrayList<>();
	
	
	public LaunchConfigPresets(final String typeId) {
		this.type= nonNullAssert(DebugPlugin.getDefault().getLaunchManager()
				.getLaunchConfigurationType(typeId) );
	}
	
	
	public ILaunchConfigurationWorkingCopy add(final String name) {
		if (name == null) {
			throw new NullPointerException("name"); //$NON-NLS-1$
		}
		try {
			final ILaunchConfigurationWorkingCopy preset= this.type.newInstance(null, "template").getWorkingCopy(); //$NON-NLS-1$
			preset.setAttribute(NAME_ATTR_NAME, name);
			
			this.presets.add(preset);
			
			return preset;
		}
		catch (final CoreException e) {
			throw new RuntimeException(e);
		}
	}
	
	public @Nullable ILaunchConfiguration get(final String name) {
		for (final ILaunchConfiguration config : this.presets) {
			if (name.equals(getName(config))) {
				return config;
			}
		}
		return null;
	}
	
	public ImList<ILaunchConfiguration> toList() {
		return ImCollections.toList(this.presets);
	}
	
}
