/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config.actions;

import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.menus.IWorkbenchContribution;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.collections.IdentityCollection;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI;
import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;
import org.eclipse.statet.ecommons.ui.actions.ListContributionItem;
import org.eclipse.statet.ecommons.ui.actions.SimpleContributionItem;
import org.eclipse.statet.ecommons.ui.util.MessageUtils;


@NonNullByDefault
public class RunConfigsDropdownContribution<TElement> extends ListContributionItem
		implements IWorkbenchContribution, IExecutableExtension {
	
	
	private class Data {
		
		
		private final LaunchConfigManager<TElement> manager;
		
		private final TElement element;
		
		
		public Data(final LaunchConfigManager<TElement> manager, final TElement file) {
			this.manager= manager;
			this.element= file;
		}
		
	}
	
	
	protected class ConfigContribution extends SimpleContributionItem {
		
		
		private Data data;
		
		private final ILaunchConfiguration configuration;
		
		
		@SuppressWarnings("null")
		public ConfigContribution(final String label, final @Nullable String mnemonic,
				final ImageDescriptor icon,
				final ILaunchConfiguration configuration) {
			super(label, mnemonic, icon, null);
			
			this.configuration= configuration;
		}
		
		
		protected Data getData() {
			return this.data;
		}
		
		@Override
		protected void execute(final Event event) throws ExecutionException {
			final Data data= getData();
			data.manager.setActiveConfig(this.configuration);
			data.manager.launch(this.configuration, data.element, getLaunchFlags());
		}
		
	}
	
	
	private final ActionUtil<TElement> util;
	private @Nullable IServiceLocator serviceLocator;
	
	private ImIdentitySet<String> launchFlags;
	
	private final StringBuilder sBuilder= new StringBuilder(32);
	
	
	public RunConfigsDropdownContribution(final IContentType contentType,
			final ActionUtil<TElement> util,
			final IdentityCollection<String> launchFlags) {
		this.util= util;
		this.launchFlags= ImCollections.toIdentitySet(launchFlags);
	}
	
	/** plugin.xml */
	@SuppressWarnings("null")
	public RunConfigsDropdownContribution(final ActionUtil<TElement> util) {
		this.util= util;
	}
	
	
	protected ActionUtil<TElement> getUtil() {
		return this.util;
	}
	
	@Override
	public void initialize(final IServiceLocator serviceLocator) {
		this.serviceLocator= serviceLocator;
	}
	
	@Override
	public void setInitializationData(final IConfigurationElement config,
			final String propertyName, final @Nullable Object data) throws CoreException {
		if (data instanceof Map) {
			try {
				configure((Map<String, String>) data);
			}
			catch (final IllegalArgumentException e) {
				throw new CoreException(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID, 0,
						NLS.bind("Invalid declaration of contribution by ''{0}''.", //$NON-NLS-1$
								config.getContributor().getName() ),
						e ));
			}
		}
	}
	
	protected void configure(final Map<String, @Nullable String> parameters) {
		{	final String s= parameters.get(ActionUtil.LAUNCH_FLAGS_PAR_NAME);
			if (s != null) {
				this.launchFlags= CollectionUtils.toIdentifierSet(s.split(";")); //$NON-NLS-1$
			}
			else {
				this.launchFlags= ImCollections.newIdentitySet();
			}
		}
	}
	
	protected ImIdentitySet<String> getLaunchFlags() {
		return this.launchFlags;
	}
	
	
	private StringBuilder getStringBuilder() {
		this.sBuilder.setLength(0);
		return this.sBuilder;
	}
	
	
	@Override
	protected void createContributionItems(final List<IContributionItem> items) {
		final IWorkbenchWindow window= ActionUtil.getWindow(this.serviceLocator);
		final @Nullable TElement element= this.util.getLaunchElement(window);
		if (element == null) {
			return;
		}
		final LaunchConfigManager<TElement> manager= this.util.getManager(window, element);
		if (manager == null) {
			return;
		}
		
		final ImList<ILaunchConfiguration> configs= manager.getAvailableConfigs();
		final Data data= new Data(manager, element);
		
		int i= 0;
		for (int num= 1; i < configs.size(); i++, num++) {
			final ILaunchConfiguration configuration= configs.get(i);
			
			final StringBuilder label= getStringBuilder();
			String mnemonic= null;
			if (num > 0 && num <= 10) {
				mnemonic= Integer.toString((num % 10));
				label.append(mnemonic);
				label.append(' ');
			}
			label.append(MessageUtils.escapeForMenu(configuration.getName()));
			final ImageDescriptor icon= manager.getImageDescriptor(configuration);
			
			final ConfigContribution item= createConfigContribution(label, mnemonic, icon,
					configuration );
			item.data= data;
			
			items.add(item);
		}
	}
	
	
	protected ConfigContribution createConfigContribution(
			final StringBuilder label, final @Nullable String mnemonic,
			final ImageDescriptor icon,
			final ILaunchConfiguration configuration) {
		return new ConfigContribution(label.toString(), mnemonic, icon, configuration);
	}
	
}
