/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config;

import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.IPrototypeAttributesLabelProvider;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.debug.ui.ILaunchConfigurationTab2;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class WrappedLaunchConfigurationTab implements ILaunchConfigurationTab2, IPrototypeAttributesLabelProvider {
	
	
	private final ILaunchConfigurationTab2 tab;
	
	
	public WrappedLaunchConfigurationTab(final ILaunchConfigurationTab2 tab) {
		this.tab= tab;
	}
	
	
	protected ILaunchConfigurationTab getBaseTab() {
		return this.tab;
	}
	
	
	@Override
	public @Nullable Image getImage() {
		return this.tab.getImage();
	}
	
	@Override
	public String getName() {
		return this.tab.getName();
	}
	
	
	@Override
	public @Nullable String getAttributeLabel(final String attribute) {
		if (this.tab instanceof IPrototypeAttributesLabelProvider) {
			return ((IPrototypeAttributesLabelProvider)this.tab).getAttributeLabel(attribute);
		}
		return null;
	}
	
	
	@Override
	public void setLaunchConfigurationDialog(final @Nullable ILaunchConfigurationDialog dialog) {
		this.tab.setLaunchConfigurationDialog(dialog);
	}
	
	@Override
	public void createControl(final Composite parent) {
		this.tab.createControl(parent);
	}
	
	@Override
	public @Nullable Control getControl() {
		return this.tab.getControl();
	}
	
	@Override
	public void dispose() {
		this.tab.dispose();
	}
	
	
	@Override
	public void activated(final ILaunchConfigurationWorkingCopy workingCopy) {
		this.tab.activated(workingCopy);
	}
	
	@Override
	public void deactivated(final ILaunchConfigurationWorkingCopy workingCopy) {
		this.tab.deactivated(workingCopy);
	}
	
	
	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
		this.tab.setDefaults(configuration);
	}
	
	@Override
	public void initializeFrom(final ILaunchConfiguration configuration) {
		this.tab.initializeFrom(configuration);
	}
	
	@Override
	public void performApply(@NonNull final ILaunchConfigurationWorkingCopy configuration) {
		this.tab.performApply(configuration);
	}
	
	
	@Override
	public boolean canSave() {
		return this.tab.canSave();
	}
	
	@Override
	public @Nullable String getErrorMessage() {
		return this.tab.getErrorMessage();
	}
	
	@Override
	public @Nullable String getWarningMessage() {
		return this.tab.getWarningMessage();
	}
	
	@Override
	public @Nullable String getMessage() {
		return this.tab.getMessage();
	}
	
	
	@Override
	public boolean isValid(final ILaunchConfiguration launchConfig) {
		return this.tab.isValid(launchConfig);
	}
	
	@Override
	@SuppressWarnings("deprecation")
	public void launched(final ILaunch launch) {
		this.tab.launched(launch);
	}
	
	
}
