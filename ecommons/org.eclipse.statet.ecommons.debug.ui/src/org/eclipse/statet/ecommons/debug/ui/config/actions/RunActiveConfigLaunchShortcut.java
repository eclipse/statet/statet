/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config.actions;

import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;


@NonNullByDefault
public class RunActiveConfigLaunchShortcut<TElement> implements ILaunchShortcut {
	
	
	private final ActionUtil<TElement> util;
	
	private final ImIdentitySet<String> launchFlags;
	
	
	public RunActiveConfigLaunchShortcut(final ActionUtil<TElement> util,
			final ImIdentitySet<String> launchFlags) {
		this.util= util;
		this.launchFlags= launchFlags;
	}
	
	public RunActiveConfigLaunchShortcut(final ActionUtil<TElement> util) {
		this(util, ImCollections.newIdentitySet());
	}
	
	
	protected ActionUtil<TElement> getUtil() {
		return this.util;
	}
	
	protected ImIdentitySet<String> getLaunchFlags() {
		return this.launchFlags;
	}
	
	
	@Override
	public void launch(final ISelection selection, final String mode) {
		final IWorkbenchWindow window= ActionUtil.getWindow(null);
		final @Nullable TElement element= this.util.getLaunchElement(selection);
		if (element == null) {
			return;
		}
		final LaunchConfigManager<TElement> manager= this.util.getManager(window, element);
		if (manager == null) {
			return;
		}
		
		this.util.launchActive(window, manager, element, this.launchFlags);
	}
	
	@Override
	public void launch(final IEditorPart editor, final String mode) {
		final IWorkbenchWindow window= editor.getEditorSite().getWorkbenchWindow();
		final @Nullable TElement element= this.util.getLaunchElement(editor);
		if (element == null) {
			return;
		}
		final LaunchConfigManager<TElement> manager= this.util.getManager(window, element);
		if (manager == null) {
			return;
		}
		
		this.util.launchActive(window, manager, element, getLaunchFlags());
	}
	
}
