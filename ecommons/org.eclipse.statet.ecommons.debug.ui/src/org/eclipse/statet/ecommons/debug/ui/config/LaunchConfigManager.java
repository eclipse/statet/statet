/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.ui.config;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationListener;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.ILaunchGroup;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.collections.IdentityCollection;
import org.eclipse.statet.jcommons.collections.IdentitySet;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.core.util.OverlayLaunchConfiguration;
import org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUI;
import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;
import org.eclipse.statet.ecommons.ui.util.MessageUtils;
import org.eclipse.statet.internal.ecommons.debug.ui.ECommonsDebugUIPlugin;
import org.eclipse.statet.internal.ecommons.debug.ui.Messages;


/**
 * Manages configuration of a specific launch configuration type.
 */
@NonNullByDefault
public abstract class LaunchConfigManager<TElement>
		implements ILaunchConfigurationListener, Disposable {
	
	
	private static final Comparator<ILaunchConfiguration> CONFIG_COMPARATOR= new LaunchConfigUtils.LaunchConfigurationComparator();
	
	private static final String ACTIVE_CONFIG_KEY= "activeConfig"; //$NON-NLS-1$
	
	
	public static interface Listener {
		
		void activeConfigChanged(ILaunchConfiguration config);
		void availableConfigChanged(ImList<ILaunchConfiguration> configs);
		
	}
	
	
	private static @Nullable ILaunchGroup getLaunchGroup(final ILaunchConfigurationType type, final String mode) {
		// Copy of org.eclipse.debug.internal.ui.launchConfigurations.LaunchConfigurationManager.getLaunchGroup(ILaunchConfigurationType, String)
		if (!type.supportsMode(mode)) {
			return null;
		}
		final String category= type.getCategory();
		final ILaunchGroup[] groups= DebugUITools.getLaunchGroups();
		ILaunchGroup extension= null;
		for (int i= 0; i < groups.length; i++) {
			extension= groups[i];
			if (category == null) {
				if (extension.getCategory() == null && extension.getMode().equals(mode)) {
					return extension;
				}
			}
			else if (category.equals(extension.getCategory())) {
				if (extension.getMode().equals(mode)) {
					return extension;
				}
			}
		}
		return null;
	}
	
	
	private String contentTypeId;
	
	private final CopyOnWriteIdentityListSet<Listener> listeners= new CopyOnWriteIdentityListSet<>();
	
	private ILaunchConfigurationType configType;
	private @Nullable ImList<ILaunchConfiguration> currentConfigs;
	private @Nullable ILaunchConfiguration activeConfig;
	
	private String configImageKey;
	private String activeConfigImageKey;
	
	
	@SuppressWarnings("null")
	public LaunchConfigManager(final String contentTypeId, final String configTypeId) {
		init(contentTypeId, configTypeId);
	}
	
	@SuppressWarnings("null")
	protected LaunchConfigManager() {
	}
	
	protected void init(final String contentTypeId, final String configTypeId) {
		final ILaunchManager launchManager= DebugPlugin.getDefault().getLaunchManager();
		final ILaunchConfigurationType configType= launchManager.getLaunchConfigurationType(configTypeId);
		if (configType == null) {
			throw new IllegalArgumentException("configTypeId= " + configTypeId); //$NON-NLS-1$
		}
		this.contentTypeId= contentTypeId;
		this.configType= configType;
		
		launchManager.addLaunchConfigurationListener(this);
		
		final IDialogSettings settings= getDialogSettings();
		final String s= settings.get(ACTIVE_CONFIG_KEY);
		if (s != null && !s.isEmpty()) {
			for (final ILaunchConfiguration config : getAvailableConfigs()) {
				if (s.equals(config.getName())) {
					setActiveConfig(config);
					break;
				}
			}
		}
		
		initImages();
	}
	
	
	private void initImages() {
		this.configImageKey= this.configType.getIdentifier();
		this.activeConfigImageKey= this.configImageKey + "_ActiveConfig"; //$NON-NLS-1$
		
		final ImageRegistry imageRegistry= ECommonsDebugUIPlugin.getInstance().getImageRegistry();
		
		final Image image= DebugUITools.getImage(this.configType.getIdentifier());
		
		if (imageRegistry.getDescriptor(this.configImageKey) == null) {
			imageRegistry.put(this.configImageKey, image);
		}
		
		if (imageRegistry.getDescriptor(this.activeConfigImageKey) == null) {
			final ImageDescriptor activeConfigImageDescriptor= new DecorationOverlayIcon(image,
					new ImageDescriptor[] {
							null, null, null, SharedUIResources.getImages().getDescriptor(SharedUIResources.OVR_DEFAULT_MARKER_IMAGE_ID),
							null },
					new Point(image.getBounds().width, image.getBounds().height) );
			imageRegistry.put(this.activeConfigImageKey, activeConfigImageDescriptor);
		}
	}
	
	
	public String getContentTypeId() {
		return this.contentTypeId;
	}
	
	protected IDialogSettings getDialogSettings() {
		return DialogUtils.getDialogSettings(ECommonsDebugUIPlugin.getInstance(), this.contentTypeId);
	}
	
	@Override
	public void dispose() {
		final ILaunchManager launchManager= DebugPlugin.getDefault().getLaunchManager();
		if (launchManager != null) {
			launchManager.removeLaunchConfigurationListener(this);
		}
		
		final IDialogSettings settings= getDialogSettings();
		settings.put(ACTIVE_CONFIG_KEY, (this.activeConfig != null) ? this.activeConfig.getName() : null);
	}
	
	
	protected byte getBits(final IdentityCollection<String> flags) {
		return 0;
	}
	
	
	@Override
	public void launchConfigurationAdded(final ILaunchConfiguration configuration) {
		try {
			if (configuration.getType() == this.configType) {
				if (DebugPlugin.getDefault().getLaunchManager().getMovedFrom(configuration) == this.activeConfig) {
					update(true, true, configuration);
				}
				else {
					update(true, false, null);
				}
			}
		} catch (final CoreException e) {
		}
	}
	
	@Override
	public void launchConfigurationChanged(final ILaunchConfiguration configuration) {
		try {
			if (configuration.getType() == this.configType && !configuration.isWorkingCopy()) {
				if (DebugPlugin.getDefault().getLaunchManager().getMovedFrom(configuration) == this.activeConfig) {
					update(true, true, configuration);
				}
				else {
					update(true, false, null);
				}
			}
		} catch (final CoreException e) {
		}
	}
	
	@Override
	public void launchConfigurationRemoved(final ILaunchConfiguration configuration) {
		try {
			// no possible to test for type (exception)
			if (configuration == this.activeConfig) {
				update(true, true, null);
			}
			else {
				final ImList<ILaunchConfiguration> configs= this.currentConfigs;
				if (configs != null) {
					for (final ILaunchConfiguration config : configs) {
						if (config == configuration) {
							update(true, false, null);
							break;
						}
					}
				}
			}
		} catch (final CoreException e) {
		}
	}
	
	private synchronized void update(final boolean updateList, boolean updateActive,
			final @Nullable ILaunchConfiguration newActive) throws CoreException {
		if (updateActive && this.activeConfig == newActive) {
			updateActive= false;
		}
		
		final ImList<Listener> listeners= this.listeners.toList();
		if (updateActive) {
			this.activeConfig= newActive;
		}
		if (updateList) {
			if (!listeners.isEmpty()) {
				final ImList<ILaunchConfiguration> configs= updateAvailableConfigs();
				for (final Listener listener : listeners) {
					try {
						listener.availableConfigChanged(configs);
					}
					catch (final Exception e) {
						ECommonsDebugUIPlugin.log(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID, 0,
								"An error occurred while notifying a listener.", e ));
					}
				}
			}
			else {
				this.currentConfigs= null;
			}
		}
		if (updateActive) {
			for (final Listener listener : listeners) {
				try {
					listener.activeConfigChanged(newActive);
				}
				catch (final Exception e) {
					ECommonsDebugUIPlugin.log(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID, 0,
							"An error occurred while notifying a listener.", e ));
				}
			}
		}
	}
	
	private ImList<ILaunchConfiguration> updateAvailableConfigs() throws CoreException {
		return this.currentConfigs= ImCollections.newList(
				DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations(this.configType),
				CONFIG_COMPARATOR );
	}
	
	public void addListener(final Listener listener) {
		this.listeners.add(nonNullAssert(listener));
	}
	
	public void removeListener(final Listener listener) {
		this.listeners.remove(listener);
	}
	
	public ILaunchConfigurationType getConfigurationType() {
		return this.configType;
	}
	
	public ImList<ILaunchConfiguration> getAvailableConfigs() {
		ImList<ILaunchConfiguration> configs= this.currentConfigs;
		if (configs == null) {
			try {
				configs= updateAvailableConfigs();
			}
			catch (final CoreException e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID,
						-1, "Loading available configurations failed.", e)); //$NON-NLS-1$
				configs= ImCollections.emptyList();
			}
		}
		return configs;
	}
	
	public void setActiveConfig(final ILaunchConfiguration configuration) {
		try {
			update(false, true, configuration);
		}
		catch (final CoreException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID,
					-1, "Setting configuration as default failed.", e)); //$NON-NLS-1$
		}
	}
	
	public @Nullable ILaunchConfiguration getActiveConfig() {
		return this.activeConfig;
	}
	
	
	public void openConfigurationDialog(final Shell shell, @Nullable IStructuredSelection selection) {
		if (selection == null || selection.isEmpty()) {
			selection= new StructuredSelection(this.configType);
			
			String name;
			if (getAvailableConfigs().isEmpty() && (name= getAutogenConfigName()) != null) {
				try {
					name= DebugPlugin.getDefault().getLaunchManager().generateLaunchConfigurationName(name);
					final ILaunchConfigurationWorkingCopy config= this.configType.newInstance(null, name);
					initAutogenConfig(config);
					selection= new StructuredSelection(config.doSave());
				}
				catch (final CoreException e) {
					ECommonsDebugUIPlugin.log(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID,
							NLS.bind("An error occurred when generating launch configuration for ''{0}''.",
									this.configType.getIdentifier() ),
							e ));
				}
			}
		}
		
		final String groupId= getLaunchGroup(this.configType, ILaunchManager.RUN_MODE).getIdentifier();
		DebugUITools.openLaunchConfigurationDialogOnGroup(shell, selection, groupId);
	}
	
	public void launch(final ILaunchConfiguration configuration, final TElement element,
			final IdentitySet<String> flags) {
		final String label= getLabel(configuration, flags, true);
		final IRunnableWithProgress runnable= new IRunnableWithProgress() {
			@Override
			public void run(final IProgressMonitor monitor) throws InvocationTargetException {
				final SubMonitor m= SubMonitor.convert(monitor, 1);
				try {
					final ILaunchConfiguration config= new OverlayLaunchConfiguration(configuration,
							createRunAttributes(element, flags) );
					final String mode= ILaunchManager.RUN_MODE;
					final byte bits= getBits(flags);
					config.launch(mode, m.newChild(1), getBuildBeforeLaunch(bits), false);
				}
				catch (final CoreException e) {
					throw new InvocationTargetException(e);
				}
				finally {
					m.done();
				}
			}
		};
		try {
			PlatformUI.getWorkbench().getProgressService().busyCursorWhile(runnable);
		}
		catch (final InvocationTargetException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, ECommonsDebugUI.BUNDLE_ID, 0,
					String.format(Messages.Run_Launch_error_message, label), e.getTargetException()),
					StatusManager.LOG | StatusManager.SHOW);
		}
		catch (final InterruptedException e) {
		}
	}
	
	protected abstract Map<String, Object> createRunAttributes(final TElement element,
			final IdentitySet<String> flags);
	
	protected boolean getBuildBeforeLaunch(final byte bits) {
		return true;
	}
	
	
	public ImageDescriptor getImageDescriptor(final ILaunchConfiguration configuration) {
		return ECommonsDebugUIPlugin.getInstance().getImageRegistry().getDescriptor(
				this.activeConfig == configuration ?
						this.activeConfigImageKey :
						this.configImageKey );
	}
	
	public Image getImage(final ILaunchConfiguration configuration) {
		return ECommonsDebugUIPlugin.getInstance().getImageRegistry().get(
				(this.activeConfig == configuration) ?
						this.activeConfigImageKey :
						this.configImageKey );
	}
	
	public String getLabel(final @Nullable ILaunchConfiguration configuration,
			final IdentityCollection<String> flags, final boolean noMnemonics) {
		String label= getActionLabel(flags);
		if (configuration != null) {
			label+= " '" + configuration.getName() + "'"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		return (noMnemonics) ? MessageUtils.removeMnemonics(label) : label;
	}
	
	public @Nullable Image getActionImage(final IdentityCollection<String> flags) {
		return getActionImage(getBits(flags));
	}
	
	protected @Nullable Image getActionImage(final byte bits) {
		switch (bits) {
		case 0:
			return ECommonsDebugUIPlugin.getInstance().getImageRegistry().get(this.configImageKey);
		default:
			return null;
		}
	}
	
	public String getActionLabel(final IdentityCollection<String> flags) {
		return getActionLabel(getBits(flags));
	}
	
	protected abstract String getActionLabel(byte bits);
	
	
	protected @Nullable String getAutogenConfigName() {
		return null;
	}
	
	protected void initAutogenConfig(final ILaunchConfigurationWorkingCopy config) {
	}
	
}
