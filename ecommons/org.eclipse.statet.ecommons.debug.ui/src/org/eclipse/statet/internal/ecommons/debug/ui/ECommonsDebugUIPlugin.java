/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.debug.ui;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.ECommonsDebugUIResources;
import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;


@NonNullByDefault
public class ECommonsDebugUIPlugin extends AbstractUIPlugin {
	
	
	private static @Nullable ECommonsDebugUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static @Nullable ECommonsDebugUIPlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	
	public ECommonsDebugUIPlugin() {
	}
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		synchronized (this) {
			this.started= true;
		}
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	private void checkStarted() {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
	}
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(ECommonsDebugUIResources.OBJ_VARIABLE_PARTITION, ImageRegistryUtil.T_OBJ, "variable_partition.png"); //$NON-NLS-1$
		util.register(ECommonsDebugUIResources.OBJ_VARIABLE_ITEM, ImageRegistryUtil.T_OBJ, "variable_item.png"); //$NON-NLS-1$
		util.register(ECommonsDebugUIResources.OBJ_VARIABLE_DIM, ImageRegistryUtil.T_OBJ, "variable_dim.png"); //$NON-NLS-1$
		
		util.register(ECommonsDebugUIResources.OVR_BREAKPOINT_INSTALLED, ImageRegistryUtil.T_OVR, "installed.png"); //$NON-NLS-1$
		util.register(ECommonsDebugUIResources.OVR_BREAKPOINT_INSTALLED_DISABLED, ImageRegistryUtil.T_OVR, "installed-disabled.png"); //$NON-NLS-1$
		
		util.register(ECommonsDebugUIResources.OVR_BREAKPOINT_CONDITIONAL, ImageRegistryUtil.T_OVR, "conditional.png"); //$NON-NLS-1$
		util.register(ECommonsDebugUIResources.OVR_BREAKPOINT_CONDITIONAL_DISABLED, ImageRegistryUtil.T_OVR, "conditional-disabled.png"); //$NON-NLS-1$
		
		util.register(ECommonsDebugUIResources.OVR_METHOD_BREAKPOINT_ENTRY, ImageRegistryUtil.T_OVR, "entry.png"); //$NON-NLS-1$
		util.register(ECommonsDebugUIResources.OVR_METHOD_BREAKPOINT_ENTRY_DISABLED, ImageRegistryUtil.T_OVR, "entry-disabled.png"); //$NON-NLS-1$
		util.register(ECommonsDebugUIResources.OVR_METHOD_BREAKPOINT_EXIT, ImageRegistryUtil.T_OVR, "exit.png"); //$NON-NLS-1$
		util.register(ECommonsDebugUIResources.OVR_METHOD_BREAKPOINT_EXIT_DISABLED, ImageRegistryUtil.T_OVR, "exit-disabled.png"); //$NON-NLS-1$
	}
	
}
