/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ts.ui.workbench;

import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.ts.core.Tool;


@NonNullByDefault
public class WorkbenchToolSessionData {
	
	
	private final @Nullable Tool tool;
	private final @Nullable Object console;
	private final @Nullable IWorkbenchPage page;
	private final @Nullable IViewPart source;
	
	
	public WorkbenchToolSessionData(final @Nullable Tool tool, final @Nullable Object console,
			final @Nullable IWorkbenchPage page, final @Nullable IViewPart source) {
		this.tool= tool;
		this.console= console;
		this.page= page;
		this.source= source;
	}
	
	
	public @Nullable Tool getTool() {
		return this.tool;
	}
	
	public @Nullable Object getConsole() {
		return this.console;
	}
	
	public @Nullable IWorkbenchPage getPage() {
		return this.page;
	}
	
	public @Nullable IViewPart getSource() {
		return this.source;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder("WorkbenchToolSessionData");
		sb.addProp("tool", this.tool);
		sb.addProp("console", this.console);
		sb.addProp("page", this.page);
		sb.addProp("source", this.source);
		return sb.toString();
	}
	
}
