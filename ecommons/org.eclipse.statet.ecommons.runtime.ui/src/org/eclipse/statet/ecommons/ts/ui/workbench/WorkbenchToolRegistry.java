/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ts.ui.workbench;

import org.eclipse.ui.IWorkbenchPage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.ts.core.Tool;
import org.eclipse.statet.jcommons.ts.core.ToolProvider;


/**
 * The tool registry tracks activation of tools. One Eclipse workbench page has
 * one active tool session.
 */
@NonNullByDefault
public interface WorkbenchToolRegistry {
	
	
	/**
	 * The variable name for the active tool.
	 * 
	 * @see org.eclipse.ui.ISourceProvider
	 */
	String ACTIVE_TOOL_SOURCE_NAME= "org.eclipse.statet.activeTool"; //$NON-NLS-1$
	
	
	/**
	 * Adds the given listener.
	 * 
	 * @param listener the listener to register
	 * @param page the workbench page or <code>null</code> if register to all pages
	 */
	void addListener(final WorkbenchToolRegistryListener listener,
			final @Nullable IWorkbenchPage page);
	
	/**
	 * Removes the given listener.
	 * 
	 * @param listener the listener to remove
	 */
	void removeListener(final WorkbenchToolRegistryListener listener);
	
	
	/**
	 * Returns the active tool session.
	 * 
	 * @return the session data, the fields can be <code>null</code>.
	 */
	WorkbenchToolSessionData getActiveToolSession(final IWorkbenchPage page);
	
	/**
	 * Finds the best workbench page for the given tool.
	 * 
	 * @return the workbench page
	 */
	IWorkbenchPage findWorkbenchPage(final Tool process);
	
	
	/**
	 * Returns a tool provider for the specified workbench page.
	 * 
	 * @param page the workbench page
	 * @return a tool provider, or {@code null} if not available
	 */
	@Nullable ToolProvider getToolProvider(final IWorkbenchPage page);
	
}
