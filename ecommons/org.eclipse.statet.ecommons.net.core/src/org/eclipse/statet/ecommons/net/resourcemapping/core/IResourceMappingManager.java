/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.net.resourcemapping.core;

import java.util.List;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.IPath;

import org.eclipse.statet.jcommons.lang.Nullable;


public interface IResourceMappingManager {
	
	
	/**
	 * Returns all configured resource mappings for the given host.
	 * 
	 * @param hostAddress the address of the remote system
	 * @param order the order of the mappings in the returned list
	 * @return a list of resource mappings, an empty list if no mappings exists
	 */
	List<IResourceMapping> getResourceMappingsFor(String hostAddress, ResourceMappingOrder order);
	
	/**
	 * Maps a remote resource path to a local file store.
	 * 
	 * @param hostAddress the address of the remote system the remote resource path belongs to
	 * @param remotePath the remote resource path
	 * @param relativeBasePath optional relative path
	 * @return a file store or <code>null</code> if no mapping was found
	 */
	@Nullable IFileStore mapRemoteResourceToFileStore(String hostAddress, IPath remotePath, IPath relativeBasePath);
	
	/**
	 * Maps a local file store to a remote resource path.
	 * 
	 * @param hostAddress the address of the remote system the remote resource path shall belong to
	 * @param fileStore the file store
	 * @return a path or <code>null</code> if no mapping was found
	 */
	@Nullable IPath mapFileStoreToRemoteResource(String hostAddress, IFileStore fileStore);
	
}
