/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.ecommons.net.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.net.core.RSAccessSessionService;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.ecommons.net.core.ECommonsNetCorePlugin;


@NonNullByDefault
public class ECommonsNetCore {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.ecommons.net.core"; //$NON-NLS-1$
	
	
	public static RSAccessSessionService getSessionManager() throws StatusException {
		return ECommonsNetCorePlugin.getInstanceRunning().getNetSessionManager();
	}
	
	
	private ECommonsNetCore() {
	}
	
}
