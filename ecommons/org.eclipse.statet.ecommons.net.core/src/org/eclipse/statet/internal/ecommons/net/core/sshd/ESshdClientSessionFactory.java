/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.net.core.sshd;

import java.nio.file.Path;
import java.util.List;

import org.eclipse.core.net.proxy.IProxyService;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.sshd.KeyPasswordProvider;
import org.eclipse.jgit.transport.sshd.agent.ConnectorFactory;

import org.eclipse.statet.jcommons.io.FileUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.core.ssh.sshd.SshdClientSessionFactory;

import org.eclipse.statet.internal.ecommons.net.core.ESshSettings;


@NonNullByDefault
public class ESshdClientSessionFactory extends SshdClientSessionFactory {
	
	
	private final ESshSettings sshSettings;
	
	
	public ESshdClientSessionFactory(final ESshSettings settings,
			final @Nullable IProxyService proxyService) {
		super((proxyService != null) ? new EProxyDataFactory(proxyService) : null);
		this.sshSettings= settings;
	}
	
	
	@Override
	protected Path getSshDataDirectory() {
		final var directory= this.sshSettings.getSshDataDirectory();
		if (directory != null) {
			return directory;
		}
		return super.getSshDataDirectory();
	}
	
	@Override
	protected List<Path> getDefaultIdentities(final ConfigBase configBase) {
		final var identityNames= this.sshSettings.getDefaultIdentityNames();
		if (identityNames != null) {
			return FileUtils.getRegularFiles(configBase.getSshDataDir(), identityNames);
		}
		return super.getDefaultIdentities(configBase);
	}
	
	@Override
	protected @Nullable String getDefaultPreferredAuthenticationMethodsString() {
		return this.sshSettings.getPreferredAuthenticationMethodsString();
	}
	
	@Override
	protected @Nullable KeyPasswordProvider createKeyPasswordProvider(final @Nullable CredentialsProvider credentialsProvider) {
		return new EIdentityPasswordProvider(credentialsProvider, this.sshSettings);
	}
	
	@Override
	protected @Nullable ConnectorFactory getConnectorFactory() {
		if (this.sshSettings.isUseIdentityAgentEnabled()) {
			final var connectorFactory= super.getConnectorFactory();
			if (connectorFactory != null) {
				return new EConnectoryFactory(connectorFactory, this.sshSettings);
			}
		}
		return null;
	}
	
	
}
