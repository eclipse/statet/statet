/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.net.core.jsch;

import static org.eclipse.statet.jcommons.util.TimeUtils.toMillisInt;

import static org.eclipse.statet.ecommons.net.core.ECommonsNetCore.BUNDLE_ID;

import java.time.Duration;

import org.osgi.framework.Bundle;
import org.osgi.framework.Version;
import org.osgi.util.tracker.ServiceTracker;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jsch.core.IJSchLocation;
import org.eclipse.jsch.core.IJSchService;

import com.jcraft.jsch.Session;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.core.ssh.BasicSshClientSessionFactory;
import org.eclipse.statet.jcommons.net.core.ssh.SshClientSession;
import org.eclipse.statet.jcommons.net.core.ssh.SshTarget;
import org.eclipse.statet.jcommons.net.core.ssh.jsch.JSchClientSession;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;

import org.eclipse.statet.internal.ecommons.net.core.ECommonsNetCorePlugin;


@NonNullByDefault
public class EJSchClientSessionFactory extends BasicSshClientSessionFactory implements Disposable {
	
	
	private static final Version REQ_JSCH_VERSION= new Version(0, 1, 49);
	
	
	private final ServiceTracker<IJSchService, ?> sshJSchTracker;
	
	
	public EJSchClientSessionFactory() throws StatusException {
		
		final var context= ECommonsNetCorePlugin.getInstanceRunning().getBundle()
				.getBundleContext();
		this.sshJSchTracker= new ServiceTracker<>(context, IJSchService.class, null);
		this.sshJSchTracker.open();
		
		{	// Check if JSch is compatible/up-to-date
			final Bundle bundle= Platform.getBundle("com.jcraft.jsch"); //$NON-NLS-1$
			if (bundle != null && bundle.getVersion().compareTo(REQ_JSCH_VERSION) < 0) {
				throw new StatusException(new ErrorStatus(BUNDLE_ID, 0,
						"The installed version of the Java Secure Channel Library 'JSch' by JCraft is outdated.\n" +
						"Please install version " + REQ_JSCH_VERSION + " or newer, " +
						"for example from the update-site of Eclipse StatET." ));
			}
		}
	}
	
	@Override
	public void dispose() {
		this.sshJSchTracker.close();
	}
	
	
	protected IJSchService getJSchService() throws StatusException {
		return (IJSchService)this.sshJSchTracker.getService();
	}
	
	
	@Override
	protected SshClientSession createSession(final SshTarget target,
			final @Nullable Duration timeout, final ProgressMonitor m) throws StatusException {
		m.setWorkRemaining(1 + 3);
		
		final IJSchService jschService= getJSchService();
		final IJSchLocation jschLocation= jschService.getLocation(
				target.getUsername(), target.getHost(), target.getPortNum() );
		final JSchClientSession session= new JSchClientSession(target, timeout) {
			@Override
			protected Session connect(
					final @Nullable Duration timeout, final ProgressMonitor m) throws Exception {
				final Session jschSession= jschService.createSession(jschLocation, null);
				try {
					jschService.connect(jschSession,
							toMillisInt(timeout, 0), EStatusUtils.convert(m) );
					return jschSession;
				}
				catch (final Exception e) {
					close(jschSession, e);
					throw e;
				}
			}
		};
		m.addWorked(1);
		
		session.connect(m);
		return session;
	}
	
	
	public static void close(final @Nullable Session toClose, final Throwable exception) {
		if (toClose != null) {
			try {
				toClose.disconnect();
			}
			catch (final Exception e) {
				exception.addSuppressed(e);
			}
		}
	}
	
}
