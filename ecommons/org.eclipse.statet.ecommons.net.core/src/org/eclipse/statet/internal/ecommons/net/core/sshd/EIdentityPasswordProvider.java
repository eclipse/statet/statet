/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.net.core.sshd;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.net.core.ECommonsNetCore.BUNDLE_ID;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.egit.core.credentials.CredentialsStore;
import org.eclipse.egit.core.credentials.UserPasswordCredentials;
import  org.eclipse.equinox.security.storage.StorageException;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.sshd.IdentityPasswordProvider;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;

import org.eclipse.statet.internal.ecommons.net.core.ESshSettings;
import org.eclipse.statet.internal.ecommons.net.core.Messages;


@NonNullByDefault
public class EIdentityPasswordProvider extends IdentityPasswordProvider {
	
	
	private final ESshSettings sshSettings;
	
	private boolean useSecureStore;
	
	
	public EIdentityPasswordProvider(final @Nullable CredentialsProvider credentialsProvider,
			final ESshSettings settings) {
		super(credentialsProvider);
		this.sshSettings= settings;
	}
	
	
	private CredentialsStore getCredentialsStore() {
		return nonNullAssert(org.eclipse.egit.core.Activator.getDefault().getCredentialsStore());
	}
	
	
	@Override
	protected char @Nullable [] getPassword(final URIish uri, final int attempt, final State state)
			throws IOException {
		if (attempt == 0) {
			this.useSecureStore= this.sshSettings.getSaveCredentialsInSecureStore();
			// Obtain a password from secure store and return it if successful
			if (this.useSecureStore) {
				try {
					final UserPasswordCredentials credentials= getCredentialsStore().getCredentials(uri);
					if (credentials != null) {
						final String password= credentials.getPassword();
						if (password != null) {
							final char[] pass= password.toCharArray();
							state.setPassword(pass);
							// Don't increment the count; this attempt shall not count against the 
							// limit, and we rely on count still being zero when we handle the result.
							return pass;
						}
					}
				}
				catch (final StorageException e) {
					if (e.getErrorCode() == StorageException.NO_PASSWORD) {
						// User canceled dialog: don't try to use the secure storage anymore
						this.useSecureStore= false;
						this.sshSettings.setSaveCredentialsInSecureStore(false);
					}
					else {
						logError(e);
					}
				}
				catch (final Exception e) {
					logError(e);
				}
			}
		}
		return super.getPassword(uri, attempt, state);
	}
	
	@Override
	protected char @Nullable [] getPassword(final URIish uri, final @Nullable String message) {
		final var credentialsProvider= getCredentialsProvider();
		if (credentialsProvider == null) {
			return null;
		}
		final List<CredentialItem> items= new ArrayList<>(3);
		if (message != null && !message.isEmpty()) {
			items.add(new CredentialItem.InformationalMessage(message));
		}
		final var passwordItem= new CredentialItem.Password(Messages.SshSession_IdentityKey_Password_prompt);
		items.add(passwordItem);
		final var storeValueItem= new CredentialItem.YesNoType(Messages.SshSession_IdentityKey_Password_SaveInStore_label);
		storeValueItem.setValue(this.useSecureStore);
		items.add(storeValueItem);
		try {
			if (!credentialsProvider.get(uri, items)) {
				cancelAuthentication();
			}
			final boolean shouldStore= storeValueItem.getValue();
			if (this.useSecureStore != shouldStore) {
				this.useSecureStore= shouldStore;
				this.sshSettings.setSaveCredentialsInSecureStore(shouldStore);
			}
			final char[] pass= passwordItem.getValue();
			return (pass != null) ? pass.clone() : null;
		}
		finally {
			passwordItem.clear();
		}
	}
	
	@Override
	protected boolean keyLoaded(final URIish uri, final @Nullable State state,
			final char @Nullable [] password,
			final @Nullable Exception err) throws IOException, GeneralSecurityException {
		if (state != null && password != null) {
			if (state.getCount() == 0) { // We tried the secure store.
				if (err != null) {
					// Clear the secure store entry for this resource -- it didn't work. On the 
					// next round we'll not find a password in the secure store, increment the count,
					// and go through the CredentialsProvider.
					try {
						getCredentialsStore().clearCredentials(uri);
					}
					catch (final Exception e) {
						logError(e);
					}
					return true; // Re-try
				}
			}
			else if (err == null) {
				if (this.useSecureStore) {
					// A user-entered password worked: store it in the secure store. We need a dummy
					// user name to go with it.
					final UserPasswordCredentials credentials= new UserPasswordCredentials(
							"egit:ssh:resource", new String(password)); //$NON-NLS-1$
					try {
						getCredentialsStore().putCredentials(uri, credentials);
					}
					catch (final StorageException e) {
						if (e.getErrorCode() == StorageException.NO_PASSWORD) {
							// User canceled dialog: don't try to use the secure storage anymore
							this.useSecureStore= false;
							this.sshSettings.setSaveCredentialsInSecureStore(false);
						}
						else {
							logError(e);
						}
					}
					catch (final Exception e) {
						logError(e);
					}
				}
			}
		}
		return super.keyLoaded(uri, state, password, err);
	}
	
	
	protected void logError(final Exception e) {
		CommonsRuntime.log(new ErrorStatus(BUNDLE_ID,
				"An error occurred when accessing the credentials store.",
				e ));
	}
	
}
