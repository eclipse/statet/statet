/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.net.core.sshd;

import static org.eclipse.statet.ecommons.net.core.ECommonsNetCore.BUNDLE_ID;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import org.eclipse.jgit.transport.sshd.agent.Connector;
import org.eclipse.jgit.transport.sshd.agent.ConnectorFactory;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.collections.CopyOnWriteListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.WarningStatus;

import org.eclipse.statet.internal.ecommons.net.core.ESshSettings;
import org.eclipse.statet.internal.ecommons.net.core.Messages;


@NonNullByDefault
public class EConnectoryFactory implements ConnectorFactory {
	
	
	private static final Set<String> WARNED_AGENTS= new CopyOnWriteListSet<>();
	
	
	private final ConnectorFactory delegate;
	
	private final ESshSettings sshSettings;
	
	
	public EConnectoryFactory(final ConnectorFactory delegate, final ESshSettings settings) {
		this.delegate= delegate;
		this.sshSettings= settings;
	}
	
	
	@Override
	public boolean isSupported() {
		return this.delegate.isSupported();
	}
	
	@Override
	public String getName() {
		return this.delegate.getName();
	}
	
	@Override
	public Collection<ConnectorDescriptor> getSupportedConnectors() {
		return this.delegate.getSupportedConnectors();
	}
	
	public @Nullable ConnectorDescriptor getConnector(final String identityAgent) {
		for (final var connector : getSupportedConnectors()) {
			if (identityAgent.equals(connector.getIdentityAgent())) {
				return connector;
			}
		}
		return null;
	}
	
	@Override
	public ConnectorDescriptor getDefaultConnector() {
		final String identityAgent= this.sshSettings.getDefaultIdentityAgent();
		if (identityAgent != null) {
			final var connector= getConnector(identityAgent);
			if (connector != null) {
				return connector;
			}
			else if (WARNED_AGENTS.add(identityAgent)) {
				CommonsRuntime.log(new WarningStatus(BUNDLE_ID,
						NLS.bind(Messages.SshSession_IdentityAgent_error_Unknown_message, identityAgent) ));
			}
		}
		return this.delegate.getDefaultConnector();
	}
	
	@Override
	public Connector create(final @Nullable String identityAgent, final File homeDir) throws IOException {
		return this.delegate.create(identityAgent, homeDir);
	}
	
}
