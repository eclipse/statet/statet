/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.net.core;

import java.util.concurrent.CopyOnWriteArraySet;

import org.osgi.framework.BundleContext;

import org.eclipse.core.net.proxy.IProxyService;
import org.eclipse.core.runtime.Plugin;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.core.BasicRSAccessSessionService;
import org.eclipse.statet.jcommons.net.core.RSAccessSessionService;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.internal.ecommons.net.core.jsch.EJSchClientSessionFactory;
import org.eclipse.statet.internal.ecommons.net.core.sshd.ESshdClientSessionFactory;


/**
 * The activator class controls the plug-in life cycle
 */
@NonNullByDefault
public final class ECommonsNetCorePlugin extends Plugin {
	
	
	private static @Nullable ECommonsNetCorePlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static @Nullable ECommonsNetCorePlugin getInstance() {
		return instance;
	}
	
	public static ECommonsNetCorePlugin getInstanceRunning() {
		final var instance= ECommonsNetCorePlugin.instance;
		if (instance == null) {
			throw new IllegalStateException("The plug-in is not running.");
		}
		return instance;
	}
	
	
	private boolean started;
	
	private final CopyOnWriteArraySet<Disposable> disposables= new CopyOnWriteArraySet<>();
	
	private @Nullable BasicRSAccessSessionService sshService;
	
	
	/**
	 * The constructor
	 */
	public ECommonsNetCorePlugin() {
		instance= this;
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
				this.sshService= null;
			}
			
			try {
				for (final Disposable listener : this.disposables) {
					listener.dispose();
				}
			}
			finally {
				this.disposables.clear();
			}
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	public void addStopListener(final Disposable listener) {
		this.disposables.add(listener);
	}
	
	public void removeStopListener(final Disposable listener) {
		this.disposables.remove(listener);
	}
	
	
	private @Nullable IProxyService getProxyService() {
		final BundleContext context= getBundle().getBundleContext();
		final var reference= context.getServiceReference(IProxyService.class);
		if (reference == null) {
			return null;
		}
		return context.getService(reference);
	}
	
	public synchronized RSAccessSessionService getNetSessionManager() throws StatusException {
		BasicRSAccessSessionService service= this.sshService;
		if (service == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			final String implId= System.getProperty("org.eclipse.statet.jcommons.net.core.ssh.Service"); //$NON-NLS-1$
			if (implId != null && implId.equals("jsch-0")) { //$NON-NLS-1$
				service= new BasicRSAccessSessionService(
						new EJSchClientSessionFactory() );
			}
			else {
				final var sshSettings= new ESshSettings(EPreferences.getInstancePrefs());
				service= new BasicRSAccessSessionService(
						new ESshdClientSessionFactory(sshSettings, getProxyService()) );
			}
			addStopListener(service);
			this.sshService= service;
		}
		return service;
	}
	
}
