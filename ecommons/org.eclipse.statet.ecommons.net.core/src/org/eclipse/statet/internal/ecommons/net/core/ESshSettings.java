/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.net.core;

import static org.eclipse.statet.ecommons.net.core.ECommonsNetCore.BUNDLE_ID;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.List;

import org.eclipse.core.runtime.preferences.IEclipsePreferences.IPreferenceChangeListener;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
import org.eclipse.egit.core.GitCorePreferences;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.WarningStatus;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.NullableStringPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.StringListPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;


@NonNullByDefault
public class ESshSettings implements Disposable {
	
	
	private static final String EJSCH_QUALIFIER= "org.eclipse.jsch.core"; //$NON-NLS-1$
	
	private static final Preference<@Nullable String> SSH_DATA_DIRECTORY_PATH_PREF= new NullableStringPref(
			EJSCH_QUALIFIER, "SSH2HOME" ); //$NON-NLS-1$
	
	private static final Preference<List<String>> DEFAULT_IDENTITY_NAMES_PREF= new StringListPref(
			EJSCH_QUALIFIER, "PRIVATEKEY", ',' ); //$NON-NLS-1$
	
	private static final Preference<@Nullable String> PREFERRED_AUTHENTICATION_METHODS_LIST_STRING_PREF= new NullableStringPref(
			EJSCH_QUALIFIER, "CVSSSH2PreferencePage.PREF_AUTH_METHODS" ); //$NON-NLS-1$
	
	
	private static final String EGIT_QUALIFIER= org.eclipse.egit.core.Activator.PLUGIN_ID;
	
	private static final BooleanPref IDENTITY_AGENT_ENABLED_PREF= new BooleanPref(
			EGIT_QUALIFIER, GitCorePreferences.core_sshAgent,
			true );
	
	private static final Preference<@Nullable String> IDENTITY_AGENT_DEFAULT_PREF= new NullableStringPref(
			EGIT_QUALIFIER, GitCorePreferences.core_sshDefaultAgent );
	
	private static final BooleanPref SAVE_CREDITALS_IN_SECURE_STORE_ENABLED_PREF= new BooleanPref(
			EGIT_QUALIFIER, GitCorePreferences.core_saveCredentialsInSecureStore,
			true );
	
	
	private final PreferenceAccess prefs;
	
	private final IPreferenceChangeListener prefsListener;
	
	private @Nullable Path sshDataDirectory;
	
	private @Nullable ImList<String> defaultIdentityNames;
	
	private @Nullable String preferredAuthenticationMethodsString;
	
	
	public ESshSettings(final PreferenceAccess prefs) {
		this.prefs= prefs;
		this.prefsListener= this::preferenceChange;
		this.prefs.addPreferenceNodeListener(EJSCH_QUALIFIER, this.prefsListener);
		reload();
	}
	
	@Override
	public void dispose() {
		this.prefs.removePreferenceNodeListener(EJSCH_QUALIFIER, this.prefsListener);
	}
	
	
	protected void preferenceChange(final PreferenceChangeEvent event) {
		reload();
	}
	
	protected void reload() {
		{	final var value= this.prefs.getPreferenceValue(SSH_DATA_DIRECTORY_PATH_PREF);
			Path path= null;
			if (value != null && !value.isEmpty()) {
				try {
					path= Path.of(value);
				}
				catch (final InvalidPathException e) {
					CommonsRuntime.log(new WarningStatus(BUNDLE_ID,
							NLS.bind(Messages.SshSession_SshDataDir_error_Invalid_message, value),
							e ));
				}
			}
			this.sshDataDirectory= path;
		}
		{	final var value= this.prefs.getPreferenceValue(DEFAULT_IDENTITY_NAMES_PREF);
			ImList<String> names= null;
			if (!value.isEmpty()) {
				final var checked= new @NonNull String[value.size()];
				int n= 0;
				for (String name : value) {
					name= name.trim();
					if (!name.isEmpty()) {
						try {
							Path.of(name);
							checked[n++]= name;
						}
						catch (final InvalidPathException e) {
							CommonsRuntime.log(new WarningStatus(BUNDLE_ID,
									NLS.bind(Messages.SshSession_IdentityKey_error_Invalid_message, value),
									e ));
						}
					}
				}
				names= ImCollections.newList(checked, 0, n);
			}
			this.defaultIdentityNames= names;
		}
		{	final var value= this.prefs.getPreferenceValue(PREFERRED_AUTHENTICATION_METHODS_LIST_STRING_PREF);
			String string= null;
			if (value != null && !value.isEmpty()) {
				string= value;
			}
			this.preferredAuthenticationMethodsString= string;
		}
	}
	
	
	public @Nullable Path getSshDataDirectory() {
		return this.sshDataDirectory;
	}
	
	public @Nullable ImList<String> getDefaultIdentityNames() {
		return this.defaultIdentityNames;
	}
	
	public @Nullable String getPreferredAuthenticationMethodsString() {
		return this.preferredAuthenticationMethodsString;
	}
	
	
	public boolean isUseIdentityAgentEnabled() {
		return this.prefs.getPreferenceValue(IDENTITY_AGENT_ENABLED_PREF);
	}
	
	public @Nullable String getDefaultIdentityAgent() {
		return this.prefs.getPreferenceValue(IDENTITY_AGENT_DEFAULT_PREF);
	}
	
	
	public boolean getSaveCredentialsInSecureStore() {
		return this.prefs.getPreferenceValue(SAVE_CREDITALS_IN_SECURE_STORE_ENABLED_PREF);
	}
	
	public void setSaveCredentialsInSecureStore(final boolean enabled) {
		PreferenceUtils.setPrefValue(this.prefs.getPreferenceContexts().getFirst(),
				SAVE_CREDITALS_IN_SECURE_STORE_ENABLED_PREF, enabled,
				PreferenceUtils.FLUSH_SYNC | PreferenceUtils.FLUSH_ASYNC );
	}
	
}
