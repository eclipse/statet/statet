/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.net.core.sshd;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.eclipse.core.net.proxy.IProxyData;
import org.eclipse.core.net.proxy.IProxyService;
import org.eclipse.jgit.transport.sshd.ProxyData;
import org.eclipse.jgit.transport.sshd.ProxyDataFactory;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class EProxyDataFactory implements ProxyDataFactory {
	
	
	private final IProxyService proxyService;
	
	
	public EProxyDataFactory(final IProxyService proxyService) {
		this.proxyService= proxyService;
	}
	
	
	@Override
	public @Nullable ProxyData get(final InetSocketAddress remoteAddress) {
		try {
			@NonNull IProxyData[] eData= this.proxyService.select(
					new URI(IProxyData.SOCKS_PROXY_TYPE, "//" + remoteAddress.getHostString(), null) ); //$NON-NLS-1$
			if (eData == null || eData.length == 0) {
				eData= this.proxyService.select(
						new URI(IProxyData.HTTP_PROXY_TYPE, "//" + remoteAddress.getHostString(), null) ); //$NON-NLS-1$
				if (eData == null || eData.length == 0) {
					return null;
				}
			}
			return toJGitData(eData[0]);
		}
		catch (final URISyntaxException e) {
			return null;
		}
	}
	
	private @Nullable ProxyData toJGitData(final IProxyData data) {
		char[] password= null;
		try {
			final InetSocketAddress proxyAddress= new InetSocketAddress(data.getHost(), data.getPort());
			password= (data.getPassword() != null) ? data.getPassword().toCharArray() : null;
			Proxy proxy;
			switch (data.getType()) {
			case IProxyData.HTTP_PROXY_TYPE:
				proxy= new Proxy(Proxy.Type.HTTP, proxyAddress);
				return new ProxyData(proxy, data.getUserId(), password);
			case IProxyData.SOCKS_PROXY_TYPE:
				proxy= new Proxy(Proxy.Type.SOCKS, proxyAddress);
				return new ProxyData(proxy, data.getUserId(), password);
			default:
				return null;
			}
		}
		finally {
			if (password != null) {
				Arrays.fill(password, '\000');
			}
		}
	}
	
}
