/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.debug.core;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.core.model.IDebugElement;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class DebugElementPropertyTester extends PropertyTester {
	
	
	public DebugElementPropertyTester() {
	}
	
	
	@Override
	public boolean test(final Object receiver, final String property,
			final @NonNull Object[] args, final @Nullable Object expectedValue) {
		final IDebugElement element;
		if (receiver instanceof IDebugElement) {
			element= (IDebugElement)receiver;
		}
		else if (receiver instanceof IAdaptable) {
			element= ((IAdaptable)receiver).getAdapter(IDebugElement.class);
		}
		else {
			element= null;
		}
		if (element != null) {
			if (property.equals("equalsModelIdentifier")) { //$NON-NLS-1$
				if (expectedValue instanceof String) {
					return (expectedValue.equals(element.getModelIdentifier()));
				}
				return false;
			}
		}
		return false;
	}
	
}
