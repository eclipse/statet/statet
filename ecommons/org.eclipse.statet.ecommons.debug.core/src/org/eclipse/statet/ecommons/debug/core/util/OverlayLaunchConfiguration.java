/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.core.util;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchDelegate;
import org.eclipse.debug.internal.core.LaunchConfiguration;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Launch configuration which allows to adds temporary additional or overwrite existing
 * attributes to a launch configuration.
 */
@NonNullByDefault
@SuppressWarnings({ "restriction" })
public class OverlayLaunchConfiguration extends LaunchConfiguration {
	
	
	private final ILaunchConfiguration launchConfiguration;
	
	private final Map<String, Object> additionalAttributes;
	
	
	public OverlayLaunchConfiguration(final ILaunchConfiguration orginal, final Map<String, Object> additional) {
		super(orginal.getName(), null);
		this.launchConfiguration= orginal;
		this.additionalAttributes= additional;
	}
	
	
	public ILaunchConfiguration getOriginal() {
		return this.launchConfiguration;
	}
	
	
	@Override
	public boolean contentsEqual(final @Nullable ILaunchConfiguration configuration) {
		return (configuration != null
				&& this.launchConfiguration.contentsEqual(configuration) );
	}
	
	@Override
	public ILaunchConfigurationWorkingCopy copy(final String name) throws CoreException {
		return this.launchConfiguration.copy(name);
	}
	
	@Override
	public void delete() throws CoreException {
		this.launchConfiguration.delete();
	}
	
	@Override
	public boolean exists() {
		return this.launchConfiguration.exists();
	}
	
	@Override
	public boolean hasAttribute(final String attributeName) throws CoreException {
		if (this.additionalAttributes.containsKey(attributeName)) {
			return true;
		}
		return this.launchConfiguration.hasAttribute(attributeName);
	}
	
	@Override
	public boolean getAttribute(final String attributeName, final boolean defaultValue) throws CoreException {
		final Object value= this.additionalAttributes.get(attributeName);
		if (value instanceof Boolean) {
			return ((Boolean)value).booleanValue();
		}
		return this.launchConfiguration.getAttribute(attributeName, defaultValue);
	}
	
	@Override
	public int getAttribute(final String attributeName, final int defaultValue) throws CoreException {
		final Object value= this.additionalAttributes.get(attributeName);
		if (value instanceof Integer) {
			return ((Integer)value).intValue();
		}
		return this.launchConfiguration.getAttribute(attributeName, defaultValue);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public @Nullable List<String> getAttribute(final String attributeName, final @Nullable List<String> defaultValue) throws CoreException {
		final Object value= this.additionalAttributes.get(attributeName);
		if (value instanceof List) {
			return (List<String>)value;
		}
		return this.launchConfiguration.getAttribute(attributeName, defaultValue);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public @Nullable Set<String> getAttribute(final String attributeName, final @Nullable Set<String> defaultValue) throws CoreException {
		final Object value= this.additionalAttributes.get(attributeName);
		if (value instanceof Set) {
			return (Set<String>)value;
		}
		return this.launchConfiguration.getAttribute(attributeName, defaultValue);
	}
	
	@Override
	public @Nullable Map getAttribute(final String attributeName, final @Nullable Map defaultValue) throws CoreException {
		final Object value= this.additionalAttributes.get(attributeName);
		if (value instanceof Map) {
			return (Map)value;
		}
		return this.launchConfiguration.getAttribute(attributeName, defaultValue);
	}
	
	@Override
	public @Nullable String getAttribute(final String attributeName, final @Nullable String defaultValue) throws CoreException {
		final Object value= this.additionalAttributes.get(attributeName);
		if (value instanceof String) {
			return (String)value;
		}
		return this.launchConfiguration.getAttribute(attributeName, defaultValue);
	}
	
	@Override
	public Map<String, Object> getAttributes() throws CoreException {
		return this.launchConfiguration.getAttributes();
	}
	
	@Override
	public @Nullable String getCategory() throws CoreException {
		return this.launchConfiguration.getCategory();
	}
	
	@Override
	public @Nullable IFile getFile() {
		return this.launchConfiguration.getFile();
	}
	
	@Override
	@Deprecated
	public @Nullable IPath getLocation() {
		return this.launchConfiguration.getLocation();
	}
	
	@Override
	public @Nullable IFileStore getFileStore() throws CoreException {
		if (this.launchConfiguration instanceof LaunchConfiguration) {
			return ((LaunchConfiguration)this.launchConfiguration).getFileStore();
		}
		return super.getFileStore();
	}
	
	@Override
	public @NonNull IResource @Nullable [] getMappedResources() throws CoreException {
		return this.launchConfiguration.getMappedResources();
	}
	
	@Override
	public @Nullable String getMemento() throws CoreException {
		return this.launchConfiguration.getMemento();
	}
	
	@Override
	public Set<String> getModes() throws CoreException {
		return this.launchConfiguration.getModes();
	}
	
	@Override
	public String getName() {
		return this.launchConfiguration.getName();
	}
	
	@Override
	public @Nullable ILaunchDelegate getPreferredDelegate(final Set<String> modes) throws CoreException {
		return this.launchConfiguration.getPreferredDelegate(modes);
	}
	
	@Override
	public ILaunchConfigurationType getType() throws CoreException {
		return this.launchConfiguration.getType();
	}
	
	@Override
	public ILaunchConfigurationWorkingCopy getWorkingCopy() throws CoreException {
		return this.launchConfiguration.getWorkingCopy();
	}
	
	@Override
	public boolean isLocal() {
		return this.launchConfiguration.isLocal();
	}
	
	@Override
	public boolean isMigrationCandidate() throws CoreException {
		return this.launchConfiguration.isMigrationCandidate();
	}
	
	@Override
	public boolean isReadOnly() {
		return this.launchConfiguration.isReadOnly();
	}
	
	@Override
	public boolean isWorkingCopy() {
		return false;
	}
	
	
	@Override
	public void migrate() throws CoreException {
		this.launchConfiguration.migrate();
	}
	
	@Override
	public boolean supportsMode(final String mode) throws CoreException {
		return this.launchConfiguration.supportsMode(mode);
	}
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return this.launchConfiguration.getAdapter(adapterType);
	}
	
}
