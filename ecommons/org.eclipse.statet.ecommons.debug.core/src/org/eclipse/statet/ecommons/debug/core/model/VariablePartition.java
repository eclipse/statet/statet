/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.core.model;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.DebugElement;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Supports long collections and avoids widows.
 */
@NonNullByDefault
public class VariablePartition<TValue extends IndexedValue> extends DebugElement implements IndexedVariablePartition, IValue {
	
	
	protected final TValue value;
	
	private final VariablePartitionFactory<TValue>.PartitionHandle partition;
	
	
	public VariablePartition(final TValue value, final VariablePartitionFactory<TValue>.PartitionHandle partition) {
		super(value.getDebugTarget());
		this.value= value;
		this.partition= partition;
	}
	
	
	@Override
	public String getModelIdentifier() {
		return this.value.getModelIdentifier();
	}
	
	
	@Override
	public final long getPartitionStart() {
		return this.partition.getStart();
	}
	
	@Override
	public final long getPartitionLength() {
		return this.partition.getLength();
	}
	
	protected int getNameIndexBase() {
		return 0;
	}
	
	protected @Nullable String getNameIndexLabel(final long idx) {
		return null;
	}
	
	
	@Override
	public String getName() {
		final StringBuilder sb= new StringBuilder();
		{	final long startIndex= getPartitionStart() + getNameIndexBase();
			sb.append('[');
			sb.append(startIndex);
			sb.append(" ... "); //$NON-NLS-1$
			sb.append(startIndex + (getPartitionLength() - 1));
			sb.append(']');
		}
		{	String label= getNameIndexLabel(getPartitionStart());
			if (label != null) {
				sb.append("  "); //$NON-NLS-1$
				sb.append(label);
				sb.append(" ... "); //$NON-NLS-1$
				label= getNameIndexLabel(getPartitionStart() + (getPartitionLength() - 1));
				if (label != null) {
					sb.append(label);
				}
			}
		}
		return sb.toString();
	}
	
	@Override
	public IValue getValue() throws DebugException {
		return this;
	}
	
	@Override
	public boolean isAllocated() throws DebugException {
		return true;
	}
	
	@Override
	public String getReferenceTypeName() throws DebugException {
		return ""; //$NON-NLS-1$
	}
	
	@Override
	public boolean hasValueChanged() throws DebugException {
		return false;
	}
	
	@Override
	public String getValueString() throws DebugException {
		return ""; //$NON-NLS-1$
	}
	
	@Override
	public boolean hasVariables() throws DebugException {
		return true;
	}
	
	@Override
	public @NonNull IVariable[] getVariables() throws DebugException {
		try {
			return this.partition.getElements(this.value);
		}
		catch (final UnsupportedOperationException e) {
			throw VariablePartitionFactory.newNotSupported(e);
		}
		catch (final IllegalArgumentException e) {
			throw VariablePartitionFactory.newRequestInvalidFailed(e);
		}
	}
	
	
	@Override
	public boolean supportsValueModification() {
		return false;
	}
	
	@Override
	public boolean verifyValue(final String expression) throws DebugException {
		throw VariablePartitionFactory.newNotSupported();
	}
	
	@Override
	public void setValue(final String expression) throws DebugException {
		throw VariablePartitionFactory.newNotSupported();
	}
	
	@Override
	public boolean verifyValue(final IValue value) throws DebugException {
		throw VariablePartitionFactory.newNotSupported();
	}
	
	@Override
	public void setValue(final IValue value) throws DebugException {
		throw VariablePartitionFactory.newNotSupported();
	}
	
	
	@Override
	public int hashCode() {
		return this.value.hashCode() + this.partition.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final VariablePartition<?> other
						&& this.value.equals(other.value) && this.partition.equals(other.partition) ));
	}
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
//		final Object adapter= this.value.getAdapter(required);
//		if (adapter != null) {
//			return adapter;
//		}
		return super.getAdapter(adapterType);
	}
	
}
