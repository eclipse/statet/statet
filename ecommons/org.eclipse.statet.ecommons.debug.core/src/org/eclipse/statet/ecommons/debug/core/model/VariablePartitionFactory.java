/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.debug.core.model;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IVariable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.core.ECommonsDebugCore;
import org.eclipse.statet.ecommons.models.core.util.ElementPartitionFactory;


@NonNullByDefault
public class VariablePartitionFactory<T extends IndexedValue>
		extends ElementPartitionFactory<IVariable, T> {
	
	
	static DebugException newNotSupported() {
		return new DebugException(new Status(IStatus.ERROR, ECommonsDebugCore.BUNDLE_ID,
				DebugException.NOT_SUPPORTED, "Not supported for partitions.", null ));
	}
	
	static DebugException newNotSupported(final Throwable cause) {
		return new DebugException(new Status(IStatus.ERROR, ECommonsDebugCore.BUNDLE_ID,
				DebugException.NOT_SUPPORTED, "Not supported for partitions.", cause ));
	}
	
	static DebugException newRequestInvalidFailed(final Throwable cause) {
		return new DebugException(new Status(IStatus.ERROR, ECommonsDebugCore.BUNDLE_ID,
				DebugException.REQUEST_FAILED, "Request failed.", cause));
	}
	
	
	public VariablePartitionFactory() {
		super(IVariable.class, DEFAULT_PART_SIZE);
	}
	
	
	public @NonNull IVariable[] getVariables(final T value) throws DebugException {
		try {
			return getElements(value, value.getSize());
		}
		catch (final UnsupportedOperationException e) {
			throw newNotSupported(e);
		}
		catch (final IllegalArgumentException e) {
			throw newRequestInvalidFailed(e);
		}
	}
	
	@Override
	protected IVariable createPartition(final T value, final PartitionHandle partition) {
		return new VariablePartition<>(value, partition);
	}
	
	@Override
	protected @NonNull IVariable[] getChildren(final T value, final long start, final int length) {
		return value.getVariables(start, length);
	}
	
}
