/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.resources.core.variables;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.variables.IStringVariable;

import org.eclipse.statet.ecommons.variables.core.ObservableValueVariable;


public class ObservableResourcePathVariable<TValue extends IResource> extends ObservableValueVariable<TValue> {
	
	
	public ObservableResourcePathVariable(final String name, final String description,
			final IObservableValue<TValue> observable) {
		super(name, description, observable);
	}
	
	public ObservableResourcePathVariable(final IStringVariable variable,
			final IObservableValue<TValue> observable) {
		super(variable, observable);
	}
	
	
	@Override
	protected String toVariableValue(final TValue value) {
		return (value != null) ? value.getFullPath().toString() : null;
	}
	
}
