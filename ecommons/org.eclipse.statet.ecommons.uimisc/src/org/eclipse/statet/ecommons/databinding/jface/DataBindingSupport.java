/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.jface;

import org.eclipse.core.databinding.AggregateValidationStatus;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.ObservableEvent;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.core.util.DirtyTracker;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.ui.SharedUIResources;


/**
 * 
 */
@NonNullByDefault
public class DataBindingSupport {
	
	private static final int STARTED= 1;
	private static final int DISPOSING= 2;
	private static final int DISPOSED= 3;
	
	
	private Realm realm;
	private DataBindingContext dbc;
	
	private @Nullable DirtyTracker tracker;
	
	private int state;
	
	
	public DataBindingSupport(final Control rootControl) {
		this.realm= Realm.getDefault();
		this.dbc= createContext(this.realm);
		
		rootControl.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				dispose();
			}
		});
		
		this.state= STARTED;
	}
	
	
	protected DataBindingContext createContext(final Realm realm) {
		return new DataBindingContext(realm);
	}
	
	public DataBindingContext getContext() {
		return this.dbc;
	}
	
	public Realm getRealm() {
		return this.realm;
	}
	
	private void dispose() {
		final var dbc= this.dbc;
		if (dbc != null) {
			this.state= DISPOSING;
			try {
				dbc.dispose();
			}
			catch (final Throwable e) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, SharedUIResources.BUNDLE_ID,
						"An error occurend when dispose databinding", e)); //$NON-NLS-1$
			}
			this.state= DISPOSED;
			this.dbc= null;
			this.realm= null;
		}
	}
	
	@SuppressWarnings("null")
	public void installStatusListener(final StatusChangeListener listener) {
		if (this.state > STARTED) {
			throw new IllegalStateException();
		}
		final AggregateValidationStatus validationStatus= new AggregateValidationStatus(this.dbc, AggregateValidationStatus.MAX_SEVERITY);
		validationStatus.addValueChangeListener(new IValueChangeListener<IStatus>() {
			@Override
			public void handleValueChange(final ValueChangeEvent<? extends IStatus> event) {
				if (DataBindingSupport.this.state == STARTED) {
					final IStatus status= event.diff.getNewValue();
					listener.statusChanged(status);
				}
			}
		});
		
		listener.statusChanged(validationStatus.getValue());
		this.tracker= new DirtyTracker(this.dbc) { // sets initial status on first change again, because initial errors are suppressed
			@Override
			public void handleChange(final @Nullable ObservableEvent event) {
				if (!isDirty()) {
					if (DataBindingSupport.this.state == STARTED) {
						listener.statusChanged(validationStatus.getValue());
					}
					super.handleChange(event);
				}
			}
		};
	}
	
	public void updateStatus() {
		final DirtyTracker tracker= this.tracker;
		if (tracker != null) {
			tracker.resetDirty();
			tracker.handleChange(null);
		}
	}
	
}
