/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.jface;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.AbstractObservableValue;
import org.eclipse.jface.databinding.swt.DisplayRealm;
import org.eclipse.jface.databinding.swt.ISWTObservableValue;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Widget;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class AbstractSWTObservableValue<T> extends AbstractObservableValue<T> implements ISWTObservableValue<T> {
	
	
	private final Widget widget;
	
	
	protected AbstractSWTObservableValue(final Widget widget) {
		this(DisplayRealm.getRealm(widget.getDisplay()), widget);
	}
	
	protected AbstractSWTObservableValue(final Realm realm, final Widget widget) {
		super(realm);
		
		this.widget= widget;
		
		widget.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				AbstractSWTObservableValue.this.dispose();
			}
		});
	}
	
	
	@Override
	public Widget getWidget() {
		return this.widget;
	}
	
}
