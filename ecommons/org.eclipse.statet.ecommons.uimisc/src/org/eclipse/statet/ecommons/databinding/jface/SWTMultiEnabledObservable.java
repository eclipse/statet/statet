/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.jface;

import java.util.List;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;


@NonNullByDefault
public class SWTMultiEnabledObservable extends AbstractSWTObservableValue<@Nullable Object> {
	
	
	private final List<? extends Control> controls;
	private final @Nullable List<? extends Control> exceptions;
	
	
	public SWTMultiEnabledObservable(final Realm realm,
			final List<? extends Control> controls,
			final @Nullable List<? extends Control> exceptions) {
		super(realm, controls.get(0));
		this.controls = controls;
		this.exceptions = exceptions;
	}
	
	public SWTMultiEnabledObservable(
			final List<? extends Control> controls,
			final @Nullable List<? extends Control> exceptions) {
		super(controls.get(0));
		this.controls = controls;
		this.exceptions = exceptions;
	}
	
	
	@Override
	protected void doSetValue(final @Nullable Object value) {
		if (value instanceof Boolean) {
			DialogUtils.setEnabled(this.controls, this.exceptions, ((Boolean)value).booleanValue());
		}
	}
	
	@Override
	protected @Nullable Object doGetValue() {
		return null;
	}
	
	@Override
	public @Nullable Object getValueType() {
		return null;
	}
	
}
