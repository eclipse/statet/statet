/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.jface;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.Realm;

import org.eclipse.statet.ecommons.ui.components.IIntValueListener;
import org.eclipse.statet.ecommons.ui.components.IIntValueWidget;
import org.eclipse.statet.ecommons.ui.components.IntValueEvent;


public class IntValueObservable extends AbstractSWTObservableValue implements IIntValueListener {
	
	
	private final IIntValueWidget fWidget;
	
	private final int fValueIdx;
	
	
	public IntValueObservable(final IIntValueWidget widget, final int idx) {
		this(Realm.getDefault(), widget, idx);
	}
	
	public IntValueObservable(final Realm realm, final IIntValueWidget widget, final int idx) {
		super(realm, widget.getControl());
		
		fWidget = widget;
		fValueIdx = idx;
		
		fWidget.addValueListener(this);
	}
	
	
	@Override
	public Object getValueType() {
		return Integer.TYPE;
	}
	
	@Override
	protected void doSetValue(final Object value) {
		final int newValue = ((Integer) value).intValue();
		final int oldValue = fWidget.getValue(fValueIdx);
		if (newValue != oldValue) {
			fWidget.setValue(fValueIdx, newValue);
			fireValueChange(Diffs.createValueDiff(oldValue, newValue));
		}
	}
	
	@Override
	protected Object doGetValue() {
		return fWidget.getValue(fValueIdx);
	}
	
	@Override
	public void valueAboutToChange(final IntValueEvent event) {
	}
	
	@Override
	public void valueChanged(final IntValueEvent event) {
		setValue(event.newValue);
	}
	
}
