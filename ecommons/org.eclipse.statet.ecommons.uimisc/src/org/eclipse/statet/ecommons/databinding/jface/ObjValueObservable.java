/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.jface;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.ValueDiff;

import org.eclipse.statet.ecommons.ui.components.IObjValueListener;
import org.eclipse.statet.ecommons.ui.components.IObjValueWidget;
import org.eclipse.statet.ecommons.ui.components.ObjValueEvent;


public class ObjValueObservable<T> extends AbstractSWTObservableValue implements IObjValueListener<T> {
	
	
	private final IObjValueWidget<T> fWidget;
	
	private final int fValueIdx;
	
	
	public ObjValueObservable(final Realm realm, final IObjValueWidget<T> widget) {
		this(realm, widget, 0);
	}
	
	public ObjValueObservable(final Realm realm, final IObjValueWidget<T> widget, final int idx) {
		super(realm, widget.getControl());
		
		fWidget = widget;
		
		fValueIdx = idx;
		
		fWidget.addValueListener(this);
	}
	
	
	@Override
	public Object getValueType() {
		return fWidget.getValueType();
	}
	
	@Override
	protected void doSetValue(final Object value) {
		fWidget.setValue(fValueIdx,(T) value);
	}
	
	@Override
	protected Object doGetValue() {
		return fWidget.getValue(fValueIdx);
	}
	
	@Override
	public void valueAboutToChange(final ObjValueEvent<T> event) {
	}
	
	@Override
	public void valueChanged(final ObjValueEvent<T> event) {
		if (event.valueIdx != fValueIdx) {
			return;
		}
		final ValueDiff diff = Diffs.createValueDiff(event.oldValue, event.newValue);
		fireValueChange(diff);
	}
	
}
