/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.jface;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;


/**
 * Observable for a radio button group
 */
public class RadioGroupObservable extends AbstractSWTObservableValue implements SelectionListener {
	
	
	private final Button[] fButtons;
	
	private int fIdx;
	
	
	public RadioGroupObservable(final Realm realm, final Button[] buttons) {
		super(realm, buttons[0]);
		fButtons = buttons;
		
		for (final Button button : buttons) {
			button.addSelectionListener(this);
		}
		fIdx = -1;
	}
	
	
	@Override
	public Object getValueType() {
		return Integer.class;
	}
	
	@Override
	protected Object doGetValue() {
		return fIdx;
	}
	
	@Override
	protected void doSetValue(final Object value) {
		int idx = ((Integer) value).intValue();
		if (idx < 0 || idx > fButtons.length) {
			idx = 0;
		}
		fIdx = idx;
		for (int i = 0; i < fButtons.length; i++) {
			fButtons[i].setSelection(idx == i);
		}
	}
	
	
	@Override
	public void widgetDefaultSelected(final SelectionEvent e) {
	}
	
	@Override
	public void widgetSelected(final SelectionEvent e) {
		if (((Button) e.widget).getSelection()) {
			for (int i = 0; i < fButtons.length; i++) {
				if (e.widget == fButtons[i]) {
					final int old = fIdx;
					if (i != old) {
						fIdx = i;
						fireValueChange(Diffs.createValueDiff(old, i));
					}
					return;
				}
			}
		}
	}
	
}
