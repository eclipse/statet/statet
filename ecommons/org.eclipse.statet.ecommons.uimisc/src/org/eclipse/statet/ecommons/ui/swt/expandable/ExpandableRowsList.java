/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.swt.expandable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.FormColors;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.events.IExpansionListener;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.SharedScrolledComposite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ExpandableRowsList extends SharedScrolledComposite {
	
	
	private static @Nullable FormToolkit gDialogsFormToolkit;
	
	private static FormToolkit getViewFormToolkit() {
		FormToolkit toolkit= gDialogsFormToolkit;
		if (toolkit == null) {
			final FormColors colors= new FormColors(Display.getCurrent());
			colors.setBackground(null);
			colors.setForeground(null);
			toolkit= new FormToolkit(colors);
			gDialogsFormToolkit= toolkit;
		}
		return toolkit;
	}
	
	
	private final FormToolkit toolkit;
	
	private @Nullable IExpansionListener expansionListener;
	
	private int delayReflowCounter;
	
	
	public ExpandableRowsList(final Composite parent) {
		this(parent, SWT.H_SCROLL | SWT.V_SCROLL);
	}
	
	public ExpandableRowsList(final Composite parent, final int style) {
		super(parent, style);
		
		if ((style & SWT.H_SCROLL) == 0) {
			setExpandHorizontal(true);
		}
		
		setFont(parent.getFont());
		
		this.toolkit= getViewFormToolkit();
		
		setExpandHorizontal(true);
		setExpandVertical(true);
		
		final Composite body= new Composite(this, SWT.NONE);
		setContent(body);
		
		setFont(parent.getFont());
		setBackgroundMode(SWT.INHERIT_FORCE);
		setBackground(getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		setForeground(getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND));
	}
	
	
	@Override
	@SuppressWarnings("null")
	public Composite getContent() {
		return (Composite)super.getContent();
	}
	
	@Override
	public void setDelayedReflow(final boolean delayedReflow) {
		if (delayedReflow) {
			this.delayReflowCounter++;
		}
		else {
			this.delayReflowCounter--;
		}
		super.setDelayedReflow(this.delayReflowCounter > 0);
	}
	
	public void adaptChild(final Control childControl) {
		if (childControl instanceof final ExpandableComposite expandableComposite) {
			this.toolkit.adapt(expandableComposite, true, false);
			var expansionListener= this.expansionListener;
			if (expansionListener == null) {
				expansionListener= new ExpansionAdapter() {
					@Override
					public void expansionStateChanged(final ExpansionEvent e) {
						expandedStateChanged();
					}
				};
				this.expansionListener= expansionListener;
			}
			expandableComposite.addExpansionListener(expansionListener);
		}
		else if (childControl instanceof final Composite composite) {
			this.toolkit.adapt(composite, false, false);
			for (final Control child : composite.getChildren()) {
				this.toolkit.adapt(child, true, false);
			}
		}
		else {
			this.toolkit.adapt(childControl, true, false);
		}
	}
	
	protected void expandedStateChanged() {
		reflow(true);
	}
	
}
