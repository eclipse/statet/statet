/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.ui.IWorkbenchCommandConstants;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * Handler for command {@link IWorkbenchCommandConstants#NAVIGATE_COLLAPSE_ALL}
 * for a single tree viewer.
 */
@NonNullByDefault
public class CollapseAllHandler extends AbstractHandler {
	
	
	private @Nullable AbstractTreeViewer viewer;
	
	
	public CollapseAllHandler(final AbstractTreeViewer viewer) {
		this.viewer= nonNullAssert(viewer);
	}
	
	@Override
	public void dispose() {
		this.viewer= null;
		super.dispose();
	}
	
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final AbstractTreeViewer viewer= this.viewer;
		if (viewer != null && UIAccess.isOkToUse(viewer.getControl())) {
			viewer.collapseAll();
		}
		return null;
	}
	
}
