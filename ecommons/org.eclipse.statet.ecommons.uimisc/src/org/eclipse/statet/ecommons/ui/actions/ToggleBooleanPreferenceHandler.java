/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.actions;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.IPreferenceChangeListener;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


/**
 * Handler toggling and caching the state of a boolean preference value.
 */
@NonNullByDefault
public class ToggleBooleanPreferenceHandler extends AbstractHandler implements IElementUpdater, IPreferenceChangeListener {
	
	
	private final Preference<Boolean> pref;
	private final PreferenceAccess prefAccess;
	
	private boolean isPrefEnabled;
	
	private final @Nullable String commandId;
	
	
	public ToggleBooleanPreferenceHandler(final Preference<@NonNull Boolean> pref,
			final @Nullable String commandId) {
		this(pref, EPreferences.getInstancePrefs(), commandId);
	}
	
	public ToggleBooleanPreferenceHandler(final Preference<@NonNull Boolean> pref, final PreferenceAccess access,
			final @Nullable String commandId) {
		super();
		this.pref= pref;
		this.prefAccess= access;
		this.commandId= commandId;
		this.prefAccess.addPreferenceNodeListener(this.pref.getQualifier(), this);
		this.isPrefEnabled= this.prefAccess.getPreferenceValue(this.pref);
		setBaseEnabled(true);
	}
	
	
	protected final boolean isPrefEnabled() {
		return this.isPrefEnabled;
	}
	
	@Override
	public void preferenceChange(final PreferenceChangeEvent event) {
		if (this.pref.getKey().equals(event.getKey())) {
			final boolean isEnabled= this.prefAccess.getPreferenceValue(this.pref);
			if (isEnabled != this.isPrefEnabled) {
				this.isPrefEnabled= isEnabled;
				handleToggled(isEnabled);
				
				final ICommandService commandService= PlatformUI.getWorkbench().getService(ICommandService.class);
				if (this.commandId != null && commandService != null) {
					WorkbenchUIUtils.refreshCommandElements(this.commandId, this, null);
				}
			}
		}
	}
	
	protected void handleToggled(final boolean isEnabled) {
	}
	
	@Override
	public void dispose() {
		this.prefAccess.removePreferenceNodeListener(this.pref.getQualifier(), this);
		super.dispose();
	}
	
	@Override
	public void updateElement(final UIElement element, final Map parameters) {
		WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
		try {
			element.setChecked(this.isPrefEnabled);
		}
		finally {
			WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
		}
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent arg0) throws ExecutionException {
		PreferenceUtils.setPrefValue(this.prefAccess.getPreferenceContexts().getFirst(), this.pref,
				!this.isPrefEnabled,
				PreferenceUtils.FLUSH_ASYNC );
		return null;
	}
	
}
