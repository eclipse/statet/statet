/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Replacement for CLabel.
 * 
 * While CLabel is inflexible, use this class instead. Features:
 *  - No border (customisable)
 *  - Better algorithm (faster and prettier).
 *  - Hover text.
 */
@NonNullByDefault
public class ShortedLabel {
	
	private static final Pattern LINEBREAK_PATTERN= Pattern.compile("\\r[\\n]?|\\n"); //$NON-NLS-1$
	
	
	private String text;
	private String checkedText;
	
	private final Label label;
	private String lineBreakReplacement= " "; //$NON-NLS-1$
	
	
	public ShortedLabel(final Composite parent, final int style) {
		this.label= new Label(parent, style);
		this.label.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				updateShortening();
			}
		});
	}
	
	public Label getControl() {
		return this.label;
	}
	
	public void setText(final String label) {
		if (label == null) {
			throw new NullPointerException();
		}
		
		if (label.equals(this.text)) {
			return;
		}
		this.text= label;
		updateChecking();
		updateShortening();
	}
	
	public void setLineBreakReplacement(final String s) {
		this.lineBreakReplacement= nonNullAssert(s);
		if (this.text != null) {
			updateChecking();
		}
	}
	
	private void updateChecking() {
		this.checkedText= LINEBREAK_PATTERN.matcher(this.text).replaceAll(this.lineBreakReplacement);
	}
	
	private void updateShortening() {
		final Shorter shorter= new Shorter(this.label);
		try {
			final String text= shorter.shorten(this.checkedText);
			this.label.setText(text);
			this.label.setToolTipText((text == this.checkedText) ? null : this.text);
		}
		finally {
			shorter.dispose();
		}
	}
	
	
	private static class Shorter {
		
		private static final String ELLIPSIS= " ... "; //$NON-NLS-1$
		private static final int DRAW_FLAGS= SWT.DRAW_TAB;
		
		private final Control control;
		private GC gc;
		private int maxWidth;
		
		private String text;
		
		
		public Shorter(final Control control) {
			this.control= control;
		}
		
		public void dispose() {
			final var gc= this.gc;
			if (gc != null) {
				this.gc= null;
				gc.dispose();
			}
		}
		
		
		public String shorten(final String text) {
			if (text == null || text.isEmpty()) {
				return text;
			}
			
			if (this.gc == null) {
				this.gc= new GC(this.control);
				this.maxWidth= this.control.getBounds().width;
			}
			if (this.gc.textExtent(text, DRAW_FLAGS).x <= this.maxWidth) {
				return text;
			}
			
			this.text= text;
			final String shortedText= doShorten();
			this.text= null;
			return shortedText;
		}
		
		private String doShorten() {
			final double avgCharWidth= this.gc.getFontMetrics().getAverageCharacterWidth();
			final int textLength= this.text.length();
			
			final int ellipsisWidth= this.gc.textExtent(ELLIPSIS, DRAW_FLAGS).x;
			
			int max2= (this.maxWidth-ellipsisWidth) * 42 / 100;
			if (max2 < avgCharWidth * 3) {
				max2= 0;
			}
			int e= Math.max(textLength - (int)(max2 / avgCharWidth), 0);
			int w2= measurePart2(e);
			while (w2 > max2 && e < textLength) {
				w2= measurePart2(e++);
			}
			while (e > 0) {
				final int test= measurePart2(e-1);
				if (test <= max2) {
					e--;
					w2= test;
					continue;
				}
				else {
					break;
				}
			}
			
			final int max1= this.maxWidth-ellipsisWidth-w2;
			int s= Math.min((int)(max2 / avgCharWidth), textLength);
			int w1= measurePart1(s);
			while (w1 > max1 && s > 3) {
				w1= measurePart1(s--);
			}
			while (s < textLength) {
				final int test= measurePart1(s + 1);
				if (test <= max1) {
					s++;
					w1= test;
					continue;
				}
				else {
					break;
				}
			}
			
			return this.text.substring(0, s) + ELLIPSIS + this.text.substring(e, textLength);
		}
		
		private int measurePart1(final int end) {
			return this.gc.textExtent(this.text.substring(0, end), DRAW_FLAGS).x; 
		}
		
		private int measurePart2(final int start) {
			return this.gc.textExtent(this.text.substring(start), DRAW_FLAGS).x;
		}
	}
	
}
