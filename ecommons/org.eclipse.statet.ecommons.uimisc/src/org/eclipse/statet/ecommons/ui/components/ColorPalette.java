/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.LineAttributes;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.graphics.core.ColorDef;
import org.eclipse.statet.ecommons.graphics.core.ColorRefDef;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


@NonNullByDefault
public class ColorPalette extends Canvas implements IObjValueWidget<@Nullable ColorDef> {
	
	private static final LineAttributes SELECTION1= new LineAttributes(1f, SWT.CAP_FLAT, SWT.JOIN_MITER, SWT.LINE_CUSTOM, new float[] { 1f, 1f }, 0f, 10f);
	private static final LineAttributes SELECTION2= new LineAttributes(1f, SWT.CAP_FLAT, SWT.JOIN_MITER, SWT.LINE_CUSTOM, new float[] { 1f, 1f }, 1f, 10f);
	
	
	private class SWTListener implements Listener, PaintListener {
		
		private boolean doubleClick;
		
		@Override
		public void handleEvent(final Event event) {
			int idx;
			switch (event.type) {
			case SWT.Resize:
				updateScroll();
				checkCursor();
				return;
			case SWT.FocusIn:
				ColorPalette.this.hasFocus= true;
				redraw();
				return;
			case SWT.FocusOut:
				ColorPalette.this.hasFocus= false;
				redraw();
				return;
			case SWT.Selection:
				redraw();
				return;
			case SWT.MouseHover:
				idx= getColorIdx(event.x, event.y);
				if (idx >= 0) {
					setToolTipText(ColorPalette.this.colors.get(idx).toString());
				}
				else {
					setToolTipText(""); //$NON-NLS-1$
				}
				return;
			case SWT.MouseDown:
				this.doubleClick= false;
				idx= getColorIdx(event.x, event.y);
				if (idx >= 0) {
					ColorPalette.this.cursorIdx= idx;
					if (!doSetColor(idx, event.time, 0)) {
						this.doubleClick= true;
						redraw();
					}
				}
				return;
			case SWT.MouseDoubleClick:
				if (this.doubleClick) {
					this.doubleClick= false;
					idx= getColorIdx(event.x, event.y);
					if (idx >= 0 && idx == ColorPalette.this.selectionIdx) {
						ColorPalette.this.cursorIdx= idx;
						if (!doSetColor(idx, event.time, ObjValueEvent.DEFAULT_SELECTION)) {
							redraw();
						}
					}
				}
				return;
			case SWT.KeyDown:
				switch (event.keyCode) {
				case SWT.ARROW_LEFT:
					if (event.stateMask == 0) {
						ColorPalette.this.cursorIdx--;
						checkCursor();
						redraw();
					}
					return;
				case SWT.ARROW_RIGHT:
					if (event.stateMask == 0) {
						ColorPalette.this.cursorIdx++;
						checkCursor();
						redraw();
					}
					return;
				case SWT.ARROW_UP:
					if (event.stateMask == 0 && ColorPalette.this.cursorIdx >= ColorPalette.this.columnCount) {
						ColorPalette.this.cursorIdx -= ColorPalette.this.columnCount;
						checkCursor();
						redraw();
					}
					return;
				case SWT.ARROW_DOWN:
					if (event.stateMask == 0 && ColorPalette.this.cursorIdx < ColorPalette.this.colors.size() - ColorPalette.this.columnCount) {
						ColorPalette.this.cursorIdx+= ColorPalette.this.columnCount;
						checkCursor();
						redraw();
					}
					return;
				case SWT.CR:
					doSetColor(ColorPalette.this.cursorIdx, event.time, ObjValueEvent.DEFAULT_SELECTION);
					return;
				case ' ':
					doSetColor(ColorPalette.this.cursorIdx, event.time, 0);
					return;
				default:
					return;
				}
			case SWT.Traverse:
				switch (event.detail) {
				case SWT.TRAVERSE_PAGE_NEXT:
				case SWT.TRAVERSE_PAGE_PREVIOUS:
				case SWT.TRAVERSE_ARROW_NEXT:
				case SWT.TRAVERSE_ARROW_PREVIOUS:
				case SWT.TRAVERSE_RETURN:
					event.doit= false;
					return;
				}
				event.doit= true;
			}
		}
		
		@Override
		public void paintControl(final PaintEvent e) {
			final int count= ColorPalette.this.colors.size();
			if (count == 0 || ColorPalette.this.columnCount == 0) {
				return;
			}
			final ScrollBar bar= getScrollBar();
			final Rectangle clientArea= getClientArea();
			int idx= bar.getSelection() * ColorPalette.this.columnCount;
			
			final GC gc= e.gc;
			final Display display= getDisplay();
			int column= 0;
			int x= 1, y= 1;
			while (idx < count) {
				final ColorDef colorDef= ColorPalette.this.colors.get(idx);
				gc.setBackground(
						new Color(colorDef.getRed(), colorDef.getGreen(), colorDef.getBlue()) );
				gc.fillRectangle(x, y, ColorPalette.this.size - 1, ColorPalette.this.size - 1);
				
				if (idx == ColorPalette.this.selectionIdx) {
					gc.setLineStyle(SWT.LINE_SOLID);
					gc.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
					gc.drawRectangle(x - 1, y - 1, ColorPalette.this.size, ColorPalette.this.size);
					gc.setForeground(getBackground());
					gc.drawRectangle(x, y, ColorPalette.this.size - 2, ColorPalette.this.size - 2);
				}
				else if (idx == count - 1
						&& Math.abs(colorDef.getRed() - ColorPalette.this.backgroundColor.getRed()) < 8
						&& Math.abs(colorDef.getGreen() - ColorPalette.this.backgroundColor.getGreen()) < 8
						&& Math.abs(colorDef.getBlue() - ColorPalette.this.backgroundColor.getBlue()) < 8) {
					gc.setLineStyle(SWT.LINE_SOLID);
					gc.setForeground(display.getSystemColor(SWT.COLOR_DARK_GRAY));
					gc.drawRectangle(x - 1, y - 1, ColorPalette.this.size, ColorPalette.this.size);
					gc.setForeground(getBackground());
					gc.drawRectangle(x, y, ColorPalette.this.size - 2, ColorPalette.this.size - 2);
				}
				if (idx == ColorPalette.this.cursorIdx && ColorPalette.this.hasFocus) {
					gc.setLineAttributes(SELECTION1);
					gc.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
					gc.drawRectangle(x, y, ColorPalette.this.size - 2, ColorPalette.this.size - 2);
					gc.setLineAttributes(SELECTION2);
					gc.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
					gc.drawRectangle(x, y, ColorPalette.this.size - 2, ColorPalette.this.size - 2);
				}
				
				idx++;
				column++;
				if (column < ColorPalette.this.columnCount) {
					x+= ColorPalette.this.size;
				}
				else {
					column= 0;
					x= 0;
					y+= ColorPalette.this.size;
					if (y > clientArea.height) {
						break;
					}
				}
			}
		}
		
	}
	
	
	private final int size;
	
	private boolean hasFocus;
	
	private List<? extends ColorDef> colors= Collections.emptyList();
	
	private int selectionIdx= -1;
	private int cursorIdx= 0;
	
	private int columnCount;
	private int visibleRowCount;
	
	private final CopyOnWriteIdentityListSet<IObjValueListener<@Nullable ColorDef>> valueListeners= new CopyOnWriteIdentityListSet<>();
	
	private final Color backgroundColor;
	
	
	public ColorPalette(final Composite parent, final Color backgroundColor) {
		super(parent, SWT.V_SCROLL);
		
		this.backgroundColor= backgroundColor;
		
		final ScrollBar bar= getScrollBar();
		this.size= 8 + LayoutUtils.defaultHSpacing() * 2;
		bar.setVisible(true);
		
		final SWTListener listener= new SWTListener();
		addPaintListener(listener);
		addListener(SWT.Resize, listener);
		addListener(SWT.FocusIn, listener);
		addListener(SWT.FocusOut, listener);
		bar.addListener(SWT.Selection, listener);
		addListener(SWT.MouseVerticalWheel, listener);
		addListener(SWT.MouseDoubleClick, listener);
		addListener(SWT.MouseDown, listener);
		addListener(SWT.MouseHover, listener);
		addListener(SWT.KeyDown, listener);
		addListener(SWT.Traverse, listener);
		updateScroll();
	}
	
	
	public void setColors(final List<? extends ColorDef> colors) {
		this.colors= colors;
		updateScroll();
		checkCursor();
	}
	
	
	@SuppressWarnings("null")
	protected final ScrollBar getScrollBar() {
		return getVerticalBar();
	}
	
	private void checkCursor() {
		if (this.colors.isEmpty()) {
			this.cursorIdx= -1;
			return;
		}
		final ScrollBar bar= getScrollBar();
		if (this.cursorIdx < 0) {
			this.cursorIdx= bar.getSelection() * this.columnCount;
		}
		if (this.cursorIdx >= this.colors.size()) {
			this.cursorIdx= this.colors.size() - 1;
		}
		if (this.columnCount == 0) {
			return;
		}
		final int row= this.cursorIdx / this.columnCount;
		final int topRow= bar.getSelection();
		if (row < topRow) {
			bar.setSelection(row);
		}
		else if (row >= topRow + this.visibleRowCount) {
			bar.setSelection(row - this.visibleRowCount + 1);
		}
	}
	
	private void updateScroll() {
		final ScrollBar bar= getScrollBar();
		final int count= this.colors.size();
		final Rectangle clientArea= getClientArea();
		this.columnCount= (clientArea.width - 1) / this.size;
		if (count == 0 || this.columnCount == 0) {
			bar.setEnabled(false);
			bar.setValues(0, 0, 1, 1, 1, 1);
			return;
		}
		final int rows= (count + this.columnCount - 1) / this.columnCount;
		this.visibleRowCount= (clientArea.height - 1) / this.size;
		if (rows <= this.visibleRowCount) {
			bar.setEnabled(false);
			bar.setValues(0, 0, 1, 1, 1, 1);
			return;
		}
		bar.setEnabled(true);
		bar.setValues(0, 0, rows, this.visibleRowCount, 1, this.visibleRowCount);
	}
	
	@Override
	public Point computeSize(final int wHint, final int hHint, final boolean changed) {
		checkWidget();
		final int width= (wHint >= 0) ? wHint : (1 + this.size * 10);
		final int height= (hHint >= 0) ? hHint : (1 + this.size * 9);
		final Rectangle trimmed= computeTrim(0, 0, width, height);
		return new Point(trimmed.width, trimmed.height);
	}
	
	@Override
	public boolean setFocus() {
		return forceFocus();
	}
	
	public int getColorIdx(final int x, final int y) {
		final int count= this.colors.size();
		if (count == 0 || this.columnCount == 0) {
			return -1;
		}
		final ScrollBar bar= getScrollBar();
		int idx= (bar.getSelection() + ((y - 1) / this.size)) * this.columnCount;
		idx+= ((x - 1) / this.size);
		return (idx < count) ? idx : -1;
	}
	
	@Override
	public Control getControl() {
		return this;
	}
	
	@Override
	@SuppressWarnings("null")
	public Class<@Nullable ColorDef> getValueType() {
		return ColorDef.class;
	}
	
	@Override
	public void addValueListener(final IObjValueListener<@Nullable ColorDef> listener) {
		this.valueListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeValueListener(final IObjValueListener<@Nullable ColorDef> listener) {
		this.valueListeners.remove(listener);
	}
	
	private boolean doSetColor(final int idx, final int time, final int flags) {
		final ColorDef oldValue= (this.selectionIdx >= 0) ? this.colors.get(this.selectionIdx) : null;
		final ColorDef newValue= (idx >= 0) ? this.colors.get(idx) : null;
		if (oldValue == newValue && flags == 0) {
			return false;
		}
		
		this.selectionIdx= idx;
		
		final var event= new ObjValueEvent<>(this, time, 0, oldValue, newValue, flags);
		for (final var listener : this.valueListeners) {
			event.newValue= newValue;
			listener.valueChanged(event);
		}
		
		if (!isDisposed()) {
			redraw();
		}
		
		return true;
	}
	
	@Override
	public @Nullable ColorDef getValue(final int idx) {
		if (idx != 0) {
			throw new IllegalArgumentException("idx: " + idx); //$NON-NLS-1$
		}
		return (this.selectionIdx >= 0) ? this.colors.get(this.selectionIdx) : null;
	}
	
	@Override
	public void setValue(final int idx, final @Nullable ColorDef value) {
		if (idx != 0) {
			throw new IllegalArgumentException("idx: " + idx); //$NON-NLS-1$
		}
		if (value != null) {
			for (int i= 0; i < this.colors.size(); i++) {
				final ColorDef c= this.colors.get(i);
				if (c.equals(value)) {
					setValue(i);
					return;
				}
			}
			if (value instanceof ColorRefDef) {
				final ColorDef ref= ((ColorRefDef) value).getRef();
				for (int i= 0; i < this.colors.size(); i++) {
					final ColorDef c= this.colors.get(i);
					if (c.equals(ref)) {
						setValue(i);
						return;
					}
				}
			}
			for (int i= 0; i < this.colors.size(); i++) {
				final ColorDef c= this.colors.get(i);
				if (c.equalsRGB(value)) {
					setValue(i);
					return;
				}
			}
		}
		setValue(-1);
	}
	
	public void setValue(final int idx) {
		this.cursorIdx= idx;
		checkCursor();
		if (this.selectionIdx != idx) {
			if (doSetColor(idx, 0, 0)) {
				return;
			}
		}
		redraw();
	}
	
	public void setCursor(final int idx, final ColorDef value) {
		if (idx != 0) {
			throw new IllegalArgumentException("idx: " + idx); //$NON-NLS-1$
		}
		int colorIdx= -1;
		for (int i= 0; i < this.colors.size(); i++) {
			final ColorDef c= this.colors.get(i);
			if (c.equals(value)) {
				colorIdx= i;
				break;
			}
			if (colorIdx == -1 && c.equalsRGB(value)) {
				colorIdx= i;
			}
		}
		if (colorIdx >= 0) {
			this.cursorIdx= colorIdx;
			checkCursor();
			redraw();
		}
	}
	
}
