/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import static org.eclipse.statet.jcommons.util.Units.MILLI_FACTOR;
import static org.eclipse.statet.jcommons.util.Units.MILLI_NANO;
import static org.eclipse.statet.jcommons.util.Units.NANO_FACTOR;

import static org.eclipse.statet.ecommons.ui.util.UIAccess.SCHEDULE_TOLERANCE_NANOS;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.ecommons.ui.components.StatusInfo;


/**
 * Utility to manage messages for the status line.
 * 
 * Uses status objects (e.g. {@link StatusInfo}) for messages and provides support for <ul>
 *     <li>Default messages (selection message)</li>
 *     <li>Error messages</li>
 *     <li>Timeout of messages</li>
 * </ul>
 */
public class StatusLineMessageManager {
	
	
	private static final long NO_CLEAR= 0;
	
	private static final long toClear(final long nano) {
		return (nano != NO_CLEAR) ? nano : 1;
	}
	
	
	private class Handler implements Runnable {
		
		/**
		 * Current status
		 */
		private IStatus status;
		
		private IStatus defaultStatus;
		
		/**
		 * Timestamp to clear status
		 * #NO_CLEAR for no clear
		 */
		private long clearTime;
		
		private long scheduledTime;
		
		
		public Handler() {
		}
		
		
		private void doSetMessage(final Image icon, final String message) {
			if (this == StatusLineMessageManager.this.errorHandler) {
				getStatusLine().setErrorMessage(icon, message);
			}
			else {
				getStatusLine().setMessage(icon, message);
			}
		}
		
		public void setMessage(final IStatus status, final int timeout) {
			final Image icon = getIcon(status.getSeverity());
			final String message = status.getMessage();
			
			this.status = status;
			if (timeout > 0) {
				this.clearTime= toClear(System.nanoTime() + (long)timeout * NANO_FACTOR);
				if (this.scheduledTime > this.clearTime) {
					this.scheduledTime= NO_CLEAR;
					StatusLineMessageManager.this.display.timerExec(-1, this);
				}
				if (this.scheduledTime == NO_CLEAR) {
					this.scheduledTime= this.clearTime;
					StatusLineMessageManager.this.display.timerExec(timeout * MILLI_FACTOR, this);
				}
			}
			else {
				this.clearTime= NO_CLEAR;
			}
			
			doSetMessage(icon, message);
		}
		
		public void setDefaultMessage(IStatus status) {
			if (status != null) {
				this.defaultStatus = status;
				setMessage(status, 0);
			}
			else {
				status = this.defaultStatus;
				this.defaultStatus = null;
				clear(status);
			}
		}
		
		public void clear(final IStatus status) {
			if (this.status == status) {
				clear();
			}
		}
		
		public void clear() {
			if (this.defaultStatus != null) {
				setMessage(this.defaultStatus, 0);
			}
			else {
				this.status = null;
				this.clearTime= NO_CLEAR;
				doSetMessage(null, null);
			}
		}
		
		@Override
		public void run() {
			if (this.clearTime != NO_CLEAR) {
				final long remaining= this.clearTime - System.nanoTime();
				if (remaining > SCHEDULE_TOLERANCE_NANOS) {
					this.scheduledTime= this.clearTime;
					StatusLineMessageManager.this.display.timerExec((int)((remaining + 800_000) / MILLI_NANO), this);
					return;
				}
				
				clear();
			}
			
			this.scheduledTime= NO_CLEAR;
		}
		
	}
	
	
	private final IStatusLineManager statusLineManager;
	
	private final Display display;
	
	private final boolean showIcons;
	
	private final Handler infoHandler = new Handler();
	private final Handler errorHandler = new Handler();
	
	
	public StatusLineMessageManager(final IStatusLineManager statusLineManager) {
		this(statusLineManager, true);
	}
	
	public StatusLineMessageManager(final IStatusLineManager statusLineManager,
			final boolean showIcons) {
		this.statusLineManager = statusLineManager;
		this.display = UIAccess.getDisplay();
		this.showIcons = showIcons;
	}
	
	
	protected IStatusLineManager getStatusLine() {
		return this.statusLineManager;
	}
	
	public void setMessage(final IStatus status) {
		setMessage(status, 30);
	}
	
	/**
	 * Sets the specified message to the status line.
	 * 
	 * @param status the status to show
	 * @param timeout timeout for the status in seconds
	 */
	public void setMessage(final IStatus status, final int timeout) {
		getHandler(status).setMessage(status, timeout);
	}
	
	public void setSelectionMessage(final IStatus status) {
		this.infoHandler.setDefaultMessage(status);
	}
	
	public void clear(final IStatus status) {
		getHandler(status).clear(status);
	}
	
	public void clearAll() {
		this.infoHandler.clear();
		this.errorHandler.clear();
	}
	
	
	private Handler getHandler(final IStatus status) {
		return (status.getSeverity() == IStatus.ERROR) ? this.errorHandler : this.infoHandler;
	}
	
	private Image getIcon(final int severity) {
		if (!this.showIcons) {
			return null;
		}
		switch (severity) {
		case IStatus.INFO:
			return JFaceResources.getImage(Dialog.DLG_IMG_MESSAGE_INFO);
		case IStatus.WARNING:
			return JFaceResources.getImage(Dialog.DLG_IMG_MESSAGE_WARNING);
		case IStatus.ERROR:
			return JFaceResources.getImage(Dialog.DLG_IMG_MESSAGE_ERROR);
		default:
			return null;
		}
	}
	
}
