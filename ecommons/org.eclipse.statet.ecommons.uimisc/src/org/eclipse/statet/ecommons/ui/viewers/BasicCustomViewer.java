/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.jface.util.OpenStrategy;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.swt.ScheduledDisplayRunnable;


@NonNullByDefault
public abstract class BasicCustomViewer<TSelection extends ISelection> extends Viewer
		implements IPostSelectionProvider {
	
	
	public static enum UpdateType {
		DIRECT,
		DEFAULT,
		DEFAULT_DELAYED_POST;
	}
	
	
	private final Display display;
	
	private TSelection selection;
	
	private final CopyOnWriteIdentityListSet<ISelectionChangedListener> selectionListeners=
			new CopyOnWriteIdentityListSet<>();
	
	private final CopyOnWriteIdentityListSet<ISelectionChangedListener> postSelectionListeners=
			new CopyOnWriteIdentityListSet<>();
	
	private long updateSelectionDefaultAsyncDelayNanos;
	private SelectionRunnable updateSelectionRunnable= nonNullLateInit();
	private PostSelectionRunnable postSelectionRunnable= nonNullLateInit();
	
	
	public BasicCustomViewer(final Display display, final TSelection initalSelection) {
		this.display= display;
		
		this.selection= initalSelection;
	}
	
	
	protected boolean isDisposed() {
		return getControl().isDisposed();
	}
	
	
	@Override
	public TSelection getSelection() {
		return this.selection;
	}
	
	@Override
	public void setSelection(final ISelection selection, final boolean reveal) {
		final long t= System.nanoTime();
		this.selection= (TSelection)selection;
		
		handleSelectionChanged(t, UpdateType.DIRECT);
	}
	
	
//-- AbstractPostSelectionProvider ----
	
	@Override
	public void addSelectionChangedListener(final ISelectionChangedListener listener) {
		this.selectionListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeSelectionChangedListener(final ISelectionChangedListener listener) {
		this.selectionListeners.remove(listener);
	}
	
	
	@Override
	public void addPostSelectionChangedListener(final ISelectionChangedListener listener) {
		this.postSelectionListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removePostSelectionChangedListener(final ISelectionChangedListener listener) {
		this.postSelectionListeners.remove(listener);
	}
	
	
	@Override
	protected void fireSelectionChanged(final SelectionChangedEvent event) {
		for (final ISelectionChangedListener listener : this.selectionListeners) {
			SafeRunnable.run(() -> listener.selectionChanged(event));
		}
	}
	
	protected void firePostSelectionChanged(final SelectionChangedEvent event) {
		for (final ISelectionChangedListener listener : this.postSelectionListeners) {
			SafeRunnable.run(() -> listener.selectionChanged(event));
		}
	}
	
	
//-- Selection Controller ----
	
	private class SelectionRunnable extends ScheduledDisplayRunnable {
		
		private final Object updateExchLock= new Object();
		
		private int updateFlags;
		
		private @Nullable UpdateType updateRequest= null;
		private long updateRequestStamp;
		
		
		public SelectionRunnable() {
			super(BasicCustomViewer.this.display);
		}
		
		public void schedule(final int flags, final long stamp,
				final UpdateType updateType, final boolean async) {
			final long scheduleTime;
			synchronized (this.updateExchLock) {
				this.updateFlags|= flags;
				
				if (updateType == UpdateType.DIRECT) {
					this.updateRequest= UpdateType.DIRECT;
					this.updateRequestStamp= stamp;
					scheduleTime= stamp;
				}
				else if (this.updateRequest != UpdateType.DEFAULT) {
					this.updateRequest= updateType;
					this.updateRequestStamp= stamp;
					scheduleTime= (async) ?
							stamp + BasicCustomViewer.this.updateSelectionDefaultAsyncDelayNanos :
							stamp;
				}
				else {
					return;
				}
			}
			if (async) {
				scheduleFor(scheduleTime);
			}
			else {
				runNow();
			}
		}
		
		@Override
		protected void execute() {
			final UpdateType type;
			final long stamp;
			final int flags;
			synchronized (this.updateExchLock) {
				flags= this.updateFlags;
				this.updateFlags= 0;
				
				type= this.updateRequest;
				this.updateRequest= null;
				stamp= this.updateRequestStamp;
			}
			if (type == null || isDisposed()) {
				return;
			}
			
			updateSelection(type, stamp, flags);
		}
		
	}
	
	private class PostSelectionRunnable extends ScheduledDisplayRunnable {
		
		private @Nullable SelectionChangedEvent event;
		
		public PostSelectionRunnable() {
			super(BasicCustomViewer.this.display);
		}
		
		public void schedule(final SelectionChangedEvent event, final long stamp,
				final UpdateType updateType) {
			this.event= event;
			if (updateType == UpdateType.DEFAULT_DELAYED_POST) {
				scheduleFor(stamp + OpenStrategy.getPostSelectionDelay(), 1);
			}
			else {
				runNow();
			}
		}
		
		@Override
		public void cancel() {
			super.cancel();
			this.event= null;
		}
		
		@Override
		protected void execute() {
			final SelectionChangedEvent event= this.event;
			this.event= null;
			if (event == null || isDisposed()) {
				return;
			}
			firePostSelectionChanged(event);
		}
		
	}
	
	
	protected void initSelectionController(final TSelection initialSelection,
			final long updateDefaultAsyncDelayNanos) {
		this.selection= initialSelection;
		this.updateSelectionDefaultAsyncDelayNanos= updateDefaultAsyncDelayNanos;
		this.updateSelectionRunnable= new SelectionRunnable();
		this.postSelectionRunnable= new PostSelectionRunnable();
	}
	
	/**
	 * Initiates the refresh of the selection (fetched from widget).
	 * 
	 * @param flags viewer specific flags passed to {@link #fetchSelectionFromWidget(int, ISelection)}
	 * @param type the type of the selection update
	 * @param async if the update should be executed asynchronously ({@code true}, or synchronously
	 *     ({@code false}
	 */
	protected void refreshSelection(final int flags, final UpdateType type, final boolean async) {
		final long stamp= System.nanoTime();
		this.updateSelectionRunnable.schedule(flags, stamp, type, async);
	}
	
	protected @Nullable TSelection fetchSelectionFromWidget(final int flags,
			final TSelection prevSelection) {
		return null;
	}
	
	private void updateSelection(final UpdateType type, final long stamp, final int flags) {
		final TSelection newSelection= fetchSelectionFromWidget(flags, this.selection);
		if (newSelection == null || newSelection.equals(this.selection)) {
			return;
		}
		
		this.selection= newSelection;
		
		handleSelectionChanged(stamp, type);
	}
	
	private void handleSelectionChanged(final long stamp, final UpdateType type) {
		final SelectionChangedEvent event= new SelectionChangedEvent(this, this.selection);
		fireSelectionChanged(event);
		
		this.postSelectionRunnable.schedule(event, stamp, type);
	}
	
	
}
