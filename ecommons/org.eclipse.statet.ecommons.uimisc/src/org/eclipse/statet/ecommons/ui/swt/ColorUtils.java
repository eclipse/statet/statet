/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.swt;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ColorUtils {
	
	
	private static int blend(final int v1, final int v2, final float ratio) {
		final int b= Math.round(ratio * v1 + (1 - ratio) * v2);
		return Math.min(255, b);
	}
	
	/**
	 * Blends the two colors according to the specified ratio.
	 *
	 * @param c1 the first color
	 * @param c2 the second color
	 * @param ratio of the first color in the blend (0-1)
	 * @return the RGB value of the blended color
	 */
	public static RGB blend(final RGB c1, final RGB c2, final float ratio) {
		return new RGB(
				blend(c1.red, c2.red, ratio),
				blend(c1.green, c2.green, ratio),
				blend(c1.blue, c2.blue, ratio) );
	}
	
	/**
	 * Blends the two colors according to the specified ratio.
	 *
	 * @param c1 the first color
	 * @param c2 the second color
	 * @param ratio of the first color in the blend (0-1)
	 * @return the blended color
	 */
	public static Color blend(final Color c1, final Color c2, final float ratio) {
		return new Color(
				blend(c1.getRed(), c2.getRed(), ratio),
				blend(c1.getGreen(), c2.getGreen(), ratio),
				blend(c1.getBlue(), c2.getBlue(), ratio) );
	}
	
	
	private ColorUtils() {
	}
	
}
