/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;


public class StructuredSelectionProxy implements IStructuredSelection {
	
	
	protected final IStructuredSelection fSelection;
	
	
	public StructuredSelectionProxy(final IStructuredSelection selection) {
		fSelection = selection;
	}
	
	
	@Override
	public boolean isEmpty() {
		return fSelection.isEmpty();
	}
	
	@Override
	public int size() {
		return fSelection.size();
	}
	
	@Override
	public Object getFirstElement() {
		return fSelection.getFirstElement();
	}
	
	@Override
	public Iterator iterator() {
		return fSelection.iterator();
	}
	
	@Override
	public List toList() {
		return fSelection.toList();
	}
	
	@Override
	public Object[] toArray() {
		return fSelection.toArray();
	}
	
}
