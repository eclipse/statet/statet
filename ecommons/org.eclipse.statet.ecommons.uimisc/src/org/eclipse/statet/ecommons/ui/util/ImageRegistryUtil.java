/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;


public class ImageRegistryUtil {
	
	/** Icons of Views */
	public static final String T_VIEW=          "view_16"; //$NON-NLS-1$
	/** Icons for global tools */
	public static final String T_TOOL=          "tool_16"; //$NON-NLS-1$
	/** Icons for deactivated global tools */
	public static final String T_TOOLD=         "tool_16_d"; //$NON-NLS-1$
	/** Icons for object, e.g. files or model objects. */
	public static final String T_OBJ=           "obj_16"; //$NON-NLS-1$
	/** Icon overlays */
	public static final String T_OVR=           "ovr_16"; //$NON-NLS-1$
	/** Icons in the banners of wizards */
	public static final String T_WIZBAN=        "wizban"; //$NON-NLS-1$
	/** Icons in local tools */
	public static final String T_LOCTOOL=       "loctool_16"; //$NON-NLS-1$
	/** Icons of deactivated local tools */
	public static final String T_LOCTOOL_D=     "loctool_16_d"; //$NON-NLS-1$
	
	
	private final AbstractUIPlugin plugin;
	
	private final URL iconBaseURL;
	
	
	public ImageRegistryUtil(final AbstractUIPlugin plugin) {
		this.plugin= plugin;
		this.iconBaseURL= plugin.getBundle().getEntry("/icons/"); //$NON-NLS-1$
	}
	
	
	public void register(final String key, final String prefix, final String name) {
		ImageDescriptor descriptor;
		try {
			final var url= createIconFileURL(prefix + '/' + name);
			descriptor= createDescriptor(url);
		}
		catch (final MalformedURLException | RuntimeException e) {
			this.plugin.getLog().log(new Status(IStatus.ERROR, this.plugin.getBundle().getSymbolicName(), 0,
					String.format("Error occured while loading an image descriptor (key= %1$s).", key), //$NON-NLS-1$
					e ));
			descriptor= ImageDescriptor.getMissingImageDescriptor();
		}
		this.plugin.getImageRegistry().put(key, descriptor);
	}
	
	public void register(final String key, final String name) {
		ImageDescriptor descriptor;
		try {
			final int typeStartIdx= key.indexOf("/images/") + 8; //$NON-NLS-1$
			final int typeEndIdx= key.indexOf('/', typeStartIdx);
			final String type= key.substring(typeStartIdx, typeEndIdx);
			final var path= new StringBuilder();
			path.append(switch (type) {
					case "obj" -> T_OBJ + '/'; //$NON-NLS-1$
					case "tool" -> T_TOOL + '/'; //$NON-NLS-1$
					case "ovr" -> T_OVR + '/'; //$NON-NLS-1$
					case "loctool" -> T_LOCTOOL + '/'; //$NON-NLS-1$
					default -> throw new IllegalArgumentException(key);
					});
			if (name.charAt(0) == '.') {
				final int endIdx= key.length();
				int idx= typeEndIdx + 1;
				int doneIdx= idx;
				boolean lc= false;
				while (idx < endIdx) {
					final char c= key.charAt(idx);
					switch (c) {
					case '-':
					case '_':
					case '$':
					case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
						++idx;
						lc= false;
						continue;
					case '.':
						if (doneIdx < idx) {
							path.append(key, doneIdx, idx);
						}
						path.append('-');
						doneIdx= ++idx;
						lc= false;
						continue;
					case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
						if (doneIdx < idx) {
							path.append(key, doneIdx, idx);
						}
						if (lc) {
							path.append('_');
						}
						path.append((char)(c + 32));
						doneIdx= ++idx;
						lc= false;
						continue;
					case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
						++idx;
						lc= true;
						continue;
					default:
						throw new IllegalArgumentException(key);
					}
				}
				if (doneIdx < endIdx) {
					path.append(key, doneIdx, endIdx);
				}
			}
			path.append(name);
			
			final var url= createIconFileURL(path.toString());
			descriptor= createDescriptor(url);
		}
		catch (final MalformedURLException | RuntimeException e) {
			this.plugin.getLog().log(new Status(IStatus.ERROR, this.plugin.getBundle().getSymbolicName(), 0,
					String.format("Error occured while loading an image descriptor (key= %1$s).", key), //$NON-NLS-1$
					e ));
			descriptor= ImageDescriptor.getMissingImageDescriptor();
		}
		this.plugin.getImageRegistry().put(key, descriptor);
	}
	
	public void register(final String key) {
		register(key, ".png"); //$NON-NLS-1$
	}
	
	
	protected URL createIconFileURL(final String path) throws MalformedURLException {
		if (this.iconBaseURL == null) {
			throw new MalformedURLException();
		}
		final var url= new URL(this.iconBaseURL, path);
//		try (var stream= url.openStream()) {
//			if (stream.read() == -1) {
//				throw new Exception();
//			}
//		}
//		catch (final Exception e) {
//			System.err.println(String.format("ERROR Failed to read image url= '%1$s'.", url));
//			e.printStackTrace();
//		}
		return url;
	}
	
	protected ImageDescriptor createDescriptor(final URL url) {
		return ImageDescriptor.createFromURL(url);
	}
	
}
