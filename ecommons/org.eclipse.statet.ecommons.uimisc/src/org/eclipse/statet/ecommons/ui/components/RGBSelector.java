/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.graphics.core.ColorDef;
import org.eclipse.statet.ecommons.graphics.core.HSVColorDef;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


@NonNullByDefault
public class RGBSelector extends Canvas implements IObjValueWidget<ColorDef> {
	
	
	private static final byte RED= 0x0;
	private static final byte GREEN= 0x1;
	private static final byte BLUE= 0x2;
	
	private static final int INITIAL= 1 << 0;
	
	
	private static final class Data {
		
		private final int size;
		private final float factor;
		
		private final Color backgroundColor;
		
		private final int primX0;
		private final int primX1;
		
		private final int rectX0;
		private final int rectX1;
		
		private final int y0;
		private final int y1;
		
		
		public Data(final int size, final Color backgroundColor) {
			this.size= size;
			this.factor= size - 1;
			
			this.backgroundColor= backgroundColor;
			
			this.primX0= 1;
			this.primX1= this.primX0 + Math.round(size * 0.15f);
			
			this.rectX0= this.primX1 + LayoutUtils.defaultHSpacing();
			this.rectX1= this.rectX0 + size;
			
			this.y0= 1;
			this.y1= this.y0 + size;
		}
		
		
		public int prim_y_255(final int y) {
			final int v= 255 - Math.round(((y - this.y0) / this.factor) * 255f);
			if (v <= 0) {
				return 0;
			}
			if (v >= 255) {
				return 255;
			}
			return v;
		}
		
		public int rect_x_255(final int x) {
			final int v= Math.round(((x - this.rectX0) / this.factor) * 255f);
			if (v <= 0) {
				return 0;
			}
			if (v >= 255) {
				return 255;
			}
			return v;
		}
		
		public int rect_y_255(final int y) {
			final int v= 255 - Math.round(((y - this.y0) / this.factor) * 255f);
			if (v <= 0) {
				return 0;
			}
			if (v >= 255) {
				return 255;
			}
			return v;
		}
		
		public int prim_255_y(final int v) {
			return this.y0 + Math.round(((255 - v) / 255f) * this.factor);
		}
		
		public int rect_255_x(final int v) {
			return this.rectX0 + Math.round((v / 255f) * this.factor);
		}
		
		public int rect_255_y(final int v) {
			return this.y0 + Math.round(((255 - v) / 255f) * this.factor);
		}
		
		public Image createImage(final Display display, final byte primColor, final int primValue, final byte xColor, final byte yColor) {
			final Image image= new Image(display, this.rectX1 + 1, this.y1 + 1);
			final GC gc= new GC(image);
			try {
				gc.setAdvanced(false);
				
				gc.setBackground(this.backgroundColor);
				gc.fillRectangle(0, 0, image.getImageData().width, image.getImageData().height);
				
				// prim
				final int[] rgb= new int[3];
				{	final int x1= this.primX1 - 1;
					for (int y= this.y0; y < this.y1; y++) {
						rgb[primColor]= prim_y_255(y);
						gc.setForeground(new Color(rgb[0], rgb[1], rgb[2]));
						gc.drawLine(this.primX0, y, x1, y);
					}
				}
				// rect
				rgb[primColor]= primValue;
				for (int y= this.y0; y < this.y1; y++) {
					rgb[yColor]= rect_y_255(y);
					for (int x= this.rectX0; x < this.rectX1; x++) {
						rgb[xColor]= rect_x_255(x);
						gc.setForeground(new Color(rgb[0], rgb[1], rgb[2]));
						gc.drawPoint(x, y);
					}
				}
			}
			finally {
				gc.dispose();
			}
			return image;
		}
		
	}
	
	
	private class SWTListener implements PaintListener, Listener {
		
		
		private static final int TRACK_PRIM= 1;
		private static final int TRACK_RECT= 2;
		
		private @Nullable Data data;
		
		private @Nullable Image image;
		private int imagePrim;
		private int imagePrimValue;
		
		private int mouseState;
		
		
		@Override
		public void handleEvent(final Event event) {
			final var data= this.data;
			switch (event.type) {
			case SWT.MouseDown:
				if (data != null && event.y >= data.y0 && event.y < data.y1) {
					if (event.x >= data.primX0 && event.x < data.primX1) {
						doSetValue(createColor(RGBSelector.this.currentPrim, data.prim_y_255(event.y)),
								event.time, 0 );
						this.mouseState= TRACK_PRIM;
					}
					else if (event.x >= data.rectX0 && event.x < data.rectX1) {
						doSetValue(createColor(RGBSelector.this.currentRectX, data.rect_x_255(event.x),
								RGBSelector.this.currentRectY, data.rect_y_255(event.y)),
								event.time, 0 );
						this.mouseState= TRACK_RECT;
					}
				}
				return;
			case SWT.MouseUp:
				this.mouseState= 0;
				return;
			case SWT.MouseMove:
				if (data != null) {
					switch (this.mouseState) {
					case TRACK_PRIM:
						doSetValue(createColor(RGBSelector.this.currentPrim, data.prim_y_255(event.y)),
								event.time, 0 );
						break;
					case TRACK_RECT:
						doSetValue(createColor(RGBSelector.this.currentRectX, data.rect_x_255(event.x),
								RGBSelector.this.currentRectY, data.rect_y_255(event.y)),
								event.time, 0 );
						break;
					}
				}
				return;
			case SWT.Dispose:
				if (this.image != null) {
					this.image.dispose();
					this.image= null;
				}
			}
		}
		
		private int computeSize(int width, final int height) {
			width -= LayoutUtils.defaultHSpacing();
			width= Math.round(width / 1.15f);
			return Math.min(width - 2, height - 2);
		}
		
		@Override
		public void paintControl(final PaintEvent e) {
			final Rectangle clientArea= getClientArea();
			int size= computeSize(clientArea.width, clientArea.height);
			if (RGBSelector.this.size > 0 && RGBSelector.this.size < size) {
				size= RGBSelector.this.size;
			}
			
			var data= this.data;
			var image= this.image;
			if (data == null || data.size != size) {
				data= new Data(size, RGBSelector.this.backgroundColor);
				this.data= data;
			}
			
			final GC gc= e.gc;
			
			gc.setAdvanced(true);
			gc.setAntialias(SWT.OFF);
			final int currentAlpha= 255;
			gc.setAlpha(currentAlpha);
			
			gc.setBackground(RGBSelector.this.backgroundColor);
			gc.fillRectangle(clientArea);
			
			final int primValue= getComponent(RGBSelector.this.currentPrim);
			if (image == null || this.imagePrim != RGBSelector.this.currentPrim || this.imagePrimValue != primValue) {
				if (image != null) {
					this.image= null;
					image.dispose();
					image= null;
				}
				image= data.createImage(e.display, RGBSelector.this.currentPrim, primValue,
						RGBSelector.this.currentRectX, RGBSelector.this.currentRectY );
				this.image= image;
				this.imagePrim= RGBSelector.this.currentPrim;
				this.imagePrimValue= primValue;
			}
			gc.drawImage(image, 0, 0);
			
			gc.setLineWidth(1);
			gc.setAdvanced(true);
			gc.setAntialias(SWT.ON);
			
			{	final int y= data.prim_255_y(primValue);
				gc.setForeground(e.display.getSystemColor(SWT.COLOR_BLACK));
				gc.drawLine(data.primX0 - 1, y, data.primX1, y);
				if (primValue < 127) {
					gc.setForeground(e.display.getSystemColor(SWT.COLOR_WHITE));
					gc.drawLine(data.primX0, y, data.primX1 - 1, y);
				}
			}
			{	final int x= data.rect_255_x(getComponent(RGBSelector.this.currentRectX));
				final int y= data.rect_255_y(getComponent(RGBSelector.this.currentRectY));
				gc.setForeground(e.display.getSystemColor(
						(Math.max(Math.max(RGBSelector.this.value.getRed(), RGBSelector.this.value.getGreen()), RGBSelector.this.value.getBlue()) < 127) ? SWT.COLOR_WHITE : SWT.COLOR_BLACK ));
				gc.drawOval(x - 1, y - 1, 3, 3);
			}
		}
		
	}
	
	
	private int size= 8 + LayoutUtils.defaultHSpacing() * 30;
	
	private ColorDef value= new HSVColorDef(1, 0, 0);
	
	private int flags= INITIAL;
	
	private byte currentPrim;
	private byte currentRectX;
	private byte currentRectY;
	
	private final CopyOnWriteIdentityListSet<IObjValueListener<ColorDef>> valueListeners= new CopyOnWriteIdentityListSet<>();
	
	private Color backgroundColor;
	
	
	public RGBSelector(final Composite parent, final Color backgroundColor) {
		super(parent, SWT.DOUBLE_BUFFERED);
		
		this.backgroundColor= backgroundColor;
		
		setPrimary(RED);
		
		final SWTListener listener= new SWTListener();
		addPaintListener(listener);
		addListener(SWT.MouseDown, listener);
		addListener(SWT.MouseUp, listener);
		addListener(SWT.MouseMove, listener);
		addListener(SWT.Dispose, listener);
	}
	
	
	public void setSize(final int size) {
		this.size= size;
	}
	
	private int getComponent(final byte c) {
		switch (c) {
		case RED:
			return this.value.getRed();
		case GREEN:
			return this.value.getGreen();
		case BLUE:
			return this.value.getBlue();
		default:
			throw new IllegalArgumentException();
		}
	}
	
	private ColorDef createColor(final byte color, final int value) {
		final int[] rgb= new int[] { this.value.getRed(), this.value.getGreen(), this.value.getBlue() };
		rgb[color]= value;
		return new ColorDef(rgb[0], rgb[1], rgb[2]);
	}
	
	private ColorDef createColor(final byte change1, final int value1, final byte change2, final int value2) {
		final int[] rgb= new int[] { this.value.getRed(), this.value.getGreen(), this.value.getBlue() };
		rgb[change1]= value1;
		rgb[change2]= value2;
		return new ColorDef(rgb[0], rgb[1], rgb[2]);
	}
	
	private boolean doSetValue(final ColorDef newValue, final int time, final int flags) {
		final ColorDef oldValue= this.value;
		if (oldValue.equals(newValue) && (flags | (this.flags & INITIAL)) == 0) {
			return false;
		}
		
		this.value= newValue;
		this.flags&= ~INITIAL;
		
		final var event= new ObjValueEvent<>(this, time, 0, oldValue, newValue, flags);
		for (final var listener : this.valueListeners) {
			event.newValue= newValue;
			listener.valueChanged(event);
		}
		
		if (!isDisposed()) {
			redraw();
		}
		
		return true;
	}
	
	
	@Override
	public Point computeSize(final int wHint, final int hHint, final boolean changed) {
		int width= 2 + Math.round(this.size * 1.15f) + LayoutUtils.defaultHSpacing();
		int height= 2 + this.size;
		final int border= getBorderWidth();
		width+= border * 2;
		height+= border * 2;
		return new Point(width, height);
	}
	
	
	@Override
	public Control getControl() {
		return this;
	}
	
	@Override
	public Class<ColorDef> getValueType() {
		return ColorDef.class;
	}
	
	@Override
	public void addValueListener(final IObjValueListener<ColorDef> listener) {
		this.valueListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeValueListener(final IObjValueListener<ColorDef> listener) {
		this.valueListeners.remove(listener);
	}
	
	@Override
	public ColorDef getValue(final int idx) {
		if (idx != 0) {
			throw new IllegalArgumentException("idx: " + idx); //$NON-NLS-1$
		}
		return this.value;
	}
	
	@Override
	public void setValue(final int idx, final ColorDef value) {
		if (idx != 0) {
			throw new IllegalArgumentException("idx: " + idx); //$NON-NLS-1$
		}
		if (value.getType() == "rgb") { //$NON-NLS-1$
			doSetValue(value, 0, 0);
		}
		else {
			doSetValue(new ColorDef(value), 0, 0);
		}
	}
	
	public void setPrimary(final int color) {
		switch (color) {
		case RED:
			this.currentPrim= RED;
			this.currentRectX= GREEN;
			this.currentRectY= BLUE;
			break;
		case GREEN:
			this.currentPrim= GREEN;
			this.currentRectX= BLUE;
			this.currentRectY= RED;
			break;
		case BLUE:
			this.currentPrim= BLUE;
			this.currentRectX= RED;
			this.currentRectY= GREEN;
			break;
		default:
			throw new IllegalArgumentException();
		}
		redraw();
	}
	
}
