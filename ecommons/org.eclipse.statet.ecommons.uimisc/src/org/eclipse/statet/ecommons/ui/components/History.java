/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.ui.IWorkbenchCommandConstants.NAVIGATE_BACKWARD_HISTORY;
import static org.eclipse.ui.IWorkbenchCommandConstants.NAVIGATE_FORWARD_HISTORY;

import java.util.ArrayList;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler2;
import org.eclipse.jface.action.IMenuListener2;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.menus.UIElement;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.ecommons.ui.SharedMessages;
import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;
import org.eclipse.statet.ecommons.ui.workbench.ContextHandlers;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


public class History<E> {
	
	
	private static final int HISTORY_SIZE= 100;
	
	private static final int HISTORY_SHOWN= 10;
	
	
	private class NavigateHandler extends AbstractHandler implements IElementUpdater {
		
		private final int relPos;
		
		public NavigateHandler(final int relPos) {
			this.relPos= relPos;
		}
		
		@Override
		public void setEnabled(final Object evaluationContext) {
			final int pos= getPosition(this.relPos);
			setBaseEnabled(pos >= 0 && pos < History.this.list.size());
		}
		
		@Override
		public Object execute(final ExecutionEvent event) throws ExecutionException {
			select(this.relPos);
			return null;
		}
		
		@Override
		public void updateElement(final UIElement element, final Map parameters) {
			WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
			try {
				final int pos= getPosition(this.relPos);
				if (pos >= 0 && pos < History.this.list.size()) {
					final String label= getLabel(History.this.list.get(pos));
					setBaseEnabled(true);
					element.setText(label);
					element.setTooltip(NLS.bind((this.relPos <= 0) ?
							SharedMessages.NavigateBack_1_tooltip :
								SharedMessages.NavigateForward_1_tooltip,
								label ));
				}
				else {
					setBaseEnabled(false);
					element.setText(null);
					element.setTooltip(null);
				}
			}
			finally {
				WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
			}
		}
		
	}
	
	private abstract class NavigateDropdownContribItem extends HandlerContributionItem {
		
		public NavigateDropdownContribItem(
				final CommandContributionItemParameter contributionParameters,
				final IHandler2 handler) {
			super(contributionParameters, handler);
		}
		
		
		@Override
		protected void initDropDownMenu(final MenuManager menuManager) {
			menuManager.addMenuListener(new IMenuListener2() {
				@Override
				public void menuAboutToShow(final IMenuManager manager) {
					addItems(menuManager);
				}
				@Override
				public void menuAboutToHide(final IMenuManager manager) {
					NavigateDropdownContribItem.this.display.asyncExec(new Runnable() {
						@Override
						public void run() {
							menuManager.dispose();
						}
					});
				}
			});
		}
		
		protected abstract void addItems(IMenuManager menuManager);
		
		protected void addItem(final IMenuManager menuManager, final int relPos) {
			menuManager.add(new HandlerContributionItem(new CommandContributionItemParameter(
					getServiceLocator(), null, NO_COMMAND_ID,
					HandlerContributionItem.STYLE_PUSH) ,
					new NavigateHandler(relPos) ));
		}
		
	}
	
	
	private final ArrayList<E> list= new ArrayList<>(HISTORY_SIZE);
	
	private int currentPosition= -1;
	private boolean currentPositionSelected;
	
	private IServiceLocator serviceLocator;
	private ContextHandlers handlers;
	
	private ISelectionProvider selectionProvider;
	
	
	public History() {
	}
	
	
	public void setSelectionProvider(final ISelectionProvider provider) {
		this.selectionProvider= provider;
	}
	
	public void addActions(final IServiceLocator serviceLocator, final ContextHandlers handlers,
			final ToolBarManager toolBar) {
		this.serviceLocator= serviceLocator;
		this.handlers= handlers;
		
		final ISharedImages sharedImages= PlatformUI.getWorkbench().getSharedImages();
		
		{	final NavigateHandler handler= new NavigateHandler(-1);
			toolBar.add(new NavigateDropdownContribItem(new CommandContributionItemParameter(
					serviceLocator, null, NAVIGATE_BACKWARD_HISTORY, null,
					sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_BACK),
					sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_BACK_DISABLED), null,
					null, null, null,
					HandlerContributionItem.STYLE_PULLDOWN,
					null, false), handler ) {
				@Override
				protected void addItems(final IMenuManager menuManager) {
					if (History.this.currentPosition < 0) {
						return;
					}
					int last= History.this.currentPosition;
					if (!History.this.currentPositionSelected) {
						last++;
					}
					if (last > HISTORY_SHOWN) {
						last= HISTORY_SHOWN;
					}
					last= -last;
					for (int relPos= -1; relPos >= last; relPos--) {
						addItem(menuManager, relPos);
					}
				}
			});
			this.handlers.addActivate(NAVIGATE_BACKWARD_HISTORY, handler);
		}
		{	final NavigateHandler handler= new NavigateHandler(1);
			toolBar.add(new NavigateDropdownContribItem(new CommandContributionItemParameter(
					serviceLocator, null, NAVIGATE_FORWARD_HISTORY, null,
					sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_FORWARD),
					sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_FORWARD_DISABLED), null,
					null, null, null,
					HandlerContributionItem.STYLE_PULLDOWN,
					null, false), handler ) {
				@Override
				protected void addItems(final IMenuManager menuManager) {
					if (History.this.currentPosition < 0) {
						return;
					}
					int last= History.this.list.size() - History.this.currentPosition - 1;
					if (last > HISTORY_SHOWN) {
						last= HISTORY_SHOWN;
					}
					for (int relPos= 1; relPos <= last; relPos++) {
						menuManager.add(new HandlerContributionItem(new CommandContributionItemParameter(
								getServiceLocator(), null, NO_COMMAND_ID,
								HandlerContributionItem.STYLE_PUSH) ,
								new NavigateHandler(relPos) ));
					}
				}
			});
			this.handlers.addActivate(NAVIGATE_FORWARD_HISTORY, handler);
		}
	}	
	
	public void selected(final E entry) {
		if (entry == null) {
			this.currentPositionSelected= false;
		}
		else if (this.currentPosition < 0 || !this.list.get(this.currentPosition).equals(entry)) {
			this.list.subList(this.currentPosition + 1, this.list.size()).clear();
			
			if (this.list.size() >= HISTORY_SIZE) {
				this.list.removeFirst();
				this.currentPosition--;
			}
			this.list.add(entry);
			this.currentPosition++;
			this.currentPositionSelected= true;
		}
		updateControls();
	}
	
	protected int getPosition(final int relPos) {
		int pos= this.currentPosition + relPos;
		if (!this.currentPositionSelected && relPos < 0) {
			pos++;
		}
		return pos;
	}
	
	public void select(final int relPos) {
		final int pos= getPosition(relPos);
		if (pos >= 0 && pos < this.list.size()) {
			this.currentPosition= pos;
			updateControls();
			select(this.list.get(pos));
		}
	}
	
	protected void select(final E entry) {
		this.selectionProvider.setSelection(new StructuredSelection(entry));
	}
	
	protected String getLabel(final E entry) {
		return entry.toString();
	}
	
	protected void updateControls() {
//		for (final IHandler2 handler : fHandler) {
//			handler.setEnabled(null);
//		}
		if (this.serviceLocator != null) {
			WorkbenchUIUtils.refreshCommandElements(NAVIGATE_BACKWARD_HISTORY,
					this.handlers.get(NAVIGATE_BACKWARD_HISTORY), null );
			WorkbenchUIUtils.refreshCommandElements(NAVIGATE_FORWARD_HISTORY,
					this.handlers.get(NAVIGATE_FORWARD_HISTORY), null );
		}
	}
	
}
