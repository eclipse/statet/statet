/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.actions;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;


/**
 * Handler toggling and caching the state of a boolean preference value.
 */
@Deprecated
public class TogglePreferenceEnablementHandler extends ToggleBooleanPreferenceHandler {
	
	
	public TogglePreferenceEnablementHandler(final Preference<Boolean> pref,
			final String commandId) {
		super(pref, commandId);
	}
	
	public TogglePreferenceEnablementHandler(final Preference<Boolean> pref, final PreferenceAccess access,
			final String commandId) {
		super(pref, access, commandId);
	}
	
	
}
