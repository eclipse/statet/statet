/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.ui.actions.CompoundContributionItem;

import org.eclipse.statet.ecommons.ui.actions.SimpleContributionItem;


public class ShowBookmarksDropdownContribution extends CompoundContributionItem {
	
	
	public static class OpenBookmarkContributionItem extends SimpleContributionItem {
		
		
		private final PageBookBrowserView view;
		private final BrowserBookmark bookmark;
		
		
		public OpenBookmarkContributionItem(final PageBookBrowserView view, final BrowserBookmark bookmark) {
			super(bookmark.getLabel(), null);
			this.view= view;
			this.bookmark= bookmark;
		}
		
		public OpenBookmarkContributionItem(final PageBookBrowserView view, final BrowserBookmark bookmark,
				final String label, final String mnemonic) {
			super((label != null) ? label : bookmark.getLabel(), mnemonic);
			this.view= view;
			this.bookmark= bookmark;
		}
		
		
		@Override
		protected void execute() throws ExecutionException {
			this.view.openBookmark(this.bookmark, this.view.getCurrentSession());
		}
		
	}
	
	
	private final PageBookBrowserView view;
	
	
	public ShowBookmarksDropdownContribution(final PageBookBrowserView view) {
		this.view= view;
	}
	
	
	@Override
	protected IContributionItem[] getContributionItems() {
		final List<IContributionItem> list= new ArrayList<>();
		
		final List<BrowserBookmark> bookmarks= this.view.getBookmarks();
		for (int i= 0; i < bookmarks.size(); i++) {
			list.add(createPageContribution(bookmarks.get(i), i+1));
		}
		
		return list.toArray(new IContributionItem[list.size()]);
	}
	
	private ContributionItem createPageContribution(final BrowserBookmark bookmark, final int num) {
		String label= bookmark.getLabel();
		String mnemonic= null;
		if (num < 10) {
			mnemonic= Integer.toString(num);
			label= mnemonic + " " + label;
		}
		return new OpenBookmarkContributionItem(this.view, bookmark, label, mnemonic);
	}
	
}
