/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class CssUtils {
	
	
	public static final void appendCssFontFamily(final StringBuilder sb,
			final @Nullable FontData fontData, final String fallback) {
		if (fontData != null) {
			sb.append("'");
			sb.append(fontData.getName());
			sb.append("', ");
		}
		sb.append(fallback);
	}
	
	public static final void appendCssFontWeight(final StringBuilder sb,
			final @Nullable FontData fontData) {
		if (fontData != null && (fontData.getStyle() & SWT.BOLD) != 0) {
			sb.append("bold");
		}
		else {
			sb.append("normal");
		}
	}
	
	public static final void appendCssFontStyle(final StringBuilder sb,
			final @Nullable FontData fontData) {
		if (fontData != null && (fontData.getStyle() & SWT.ITALIC) != 0) {
			sb.append("italic");
		}
		else {
			sb.append("normal");
		}
	}
	
	public static final void appendCssFontSize(final StringBuilder sb,
			final @Nullable FontData fontData, final int fallback) {
		if (fontData != null) {
			sb.append(fontData.getHeight());
		}
		else {
			sb.append(fallback);
		}
		sb.append("pt");
	}
	
	
	private static final byte ROOT= 1;
	private static final byte RULESET= 2;
	
	public static class StylesheetBuilder {
		
		
		private final StringBuilder sb= new StringBuilder();
		
		private final RulesetBuilder rulesetBuilder= new RulesetBuilder();
		
		private byte state;
		
		
		public StylesheetBuilder() {
			this.state= ROOT;
		}
		
		
		private void setRoot() {
			switch (this.state) {
			case RULESET:
				this.sb.append("}\n");
				this.state= ROOT;
				return;
			case ROOT:
				return;
			default:
				throw new IllegalStateException();
			}
		}
		
		
		public StylesheetBuilder append(final String s) {
			setRoot();
			this.sb.append(s);
			return this;
		}
		
		
		public RulesetBuilder addRuleset(final String selectors) {
			setRoot();
			this.sb.append(selectors);
			this.sb.append(" { ");
			this.state= RULESET;
			return this.rulesetBuilder;
		}
		
		
		public String build() {
			setRoot();
			return this.sb.toString();
		}
		
		@Override
		public String toString() {
			return this.sb.toString();
		}
		
		
		public class RulesetBuilder {
			
			
			RulesetBuilder() {
			}
			
			
			public RulesetBuilder add(final String property, final String value) {
				StylesheetBuilder.this.sb.append(property);
				StylesheetBuilder.this.sb.append(": ");
				StylesheetBuilder.this.sb.append(value);
				StylesheetBuilder.this.sb.append("; ");
				return this;
			}
			
			public RulesetBuilder append(final String s) {
				StylesheetBuilder.this.sb.append(s);
				return this;
			}
			
			
			public RulesetBuilder addFontFamily(final @Nullable FontData fontData, final String fallback) {
				StylesheetBuilder.this.sb.append("font-family: ");
				appendCssFontFamily(StylesheetBuilder.this.sb, fontData, fallback);
				StylesheetBuilder.this.sb.append("; ");
				return this;
			}
			
			public RulesetBuilder addFontWeight(final @Nullable FontData fontData) {
				StylesheetBuilder.this.sb.append("font-weight: ");
				appendCssFontWeight(StylesheetBuilder.this.sb, fontData);
				StylesheetBuilder.this.sb.append("; ");
				return this;
			}
			
			public RulesetBuilder addFontStyle(final @Nullable FontData fontData) {
				StylesheetBuilder.this.sb.append("font-style: ");
				appendCssFontStyle(StylesheetBuilder.this.sb, fontData);
				StylesheetBuilder.this.sb.append("; ");
				return this;
			}
			
			public RulesetBuilder addFontSize(final @Nullable FontData fontData, final int fallback) {
				StylesheetBuilder.this.sb.append("font-size: ");
				appendCssFontSize(StylesheetBuilder.this.sb, fontData, fallback);
				StylesheetBuilder.this.sb.append("; ");
				return this;
			}
			
			public RulesetBuilder addFontSize(final String value) {
				add("font-size", value);
				return this;
			}
			
			
			public StylesheetBuilder close() {
				setRoot();
				return StylesheetBuilder.this;
			}
			
		}
		
	}
	
	
	private CssUtils() {
	}
	
}
