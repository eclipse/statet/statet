/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.swt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Helper class to save the enable/disable state of controls including their children.
 */
@NonNullByDefault
public class ControlEnableStates {
	
	
	public static ControlEnableStates disable(final Collection<Control> controls,
			final Collection<Control> exceptions) {
		return new ControlEnableStates(
				ImCollections.toList(controls), ImCollections.toIdentityList(exceptions),
				false );
	}
	
	public static ControlEnableStates disable(final Collection<Control> controls) {
		return new ControlEnableStates(
				ImCollections.toList(controls), ImCollections.emptyIdentityList(),
				false );
	}
	
	public static ControlEnableStates disable(final Control control,
			final Collection<Control> exceptions) {
		return new ControlEnableStates(
				ImCollections.newList(control), ImCollections.toIdentityList(exceptions),
				false );
	}
	
	public static ControlEnableStates disable(final Control control) {
		return new ControlEnableStates(
				ImCollections.newList(control), ImCollections.emptyIdentityList(),
				false );
	}
	
	public static ControlEnableStates enable(final Collection<Control> controls,
			final Collection<Control> exceptions) {
		return new ControlEnableStates(
				ImCollections.toList(controls), ImCollections.toIdentityList(exceptions),
				true );
	}
	
	public static ControlEnableStates enable(final Collection<Control> controls) {
		return new ControlEnableStates(
				ImCollections.toList(controls), ImCollections.emptyIdentityList(),
				true );
	}
	
	public static ControlEnableStates enable(final Control control,
			final Collection<Control> exceptions) {
		return new ControlEnableStates(
				ImCollections.newList(control), ImCollections.toIdentityList(exceptions),
				true );
	}
	
	public static ControlEnableStates enable(final Control control) {
		return new ControlEnableStates(
				ImCollections.newList(control), ImCollections.emptyIdentityList(),
				true );
	}
	
	
	private static class State {
		
		protected final Control control;
		
		protected boolean enabled;
		
		public State(final Control control, final boolean enabled) {
			this.control= control;
			this.enabled= enabled;
		}
		
	}
	
	
	private final ImIdentityList<Control> exceptions;
	
	private final ImList<State> states;
	
	
	private ControlEnableStates(
			final ImList<Control> controls, final ImIdentityList<Control> exceptions,
			final boolean enabled) {
		this.exceptions= exceptions;
		final List<State> states= new ArrayList<>();
		init(controls, enabled, states);
		this.states= ImCollections.toList(states);
	}
	
	
	private void init(
			final ImList<? extends Control> controls, final boolean enabled,
			final List<State> states) {
		for (final var control : controls) {
			if (this.exceptions.contains(control)) {
				continue;
			}
			if (control instanceof final Composite composite) {
				init(ImCollections.newList(composite.getChildren()), enabled, states);
			}
			states.add(new State(control, control.getEnabled()));
			control.setEnabled(enabled);
		}
	}
	
	private @Nullable State getState(final Control control) {
		for (final var state : this.states) {
			if (state.control == control) {
				return state;
			}
		}
		return null;
	}
	
	
	public void updateState(final ImCollection<? extends Control> controls, final boolean recursively,
			final boolean enabled) {
		for (final var control : controls) {
			if (this.exceptions.contains(control)) {
				continue;
			}
			final var state= getState(control);
			if (state == null) {
				continue;
			}
			if (recursively && control instanceof final Composite composite) {
				updateState(ImCollections.newList(composite.getChildren()), recursively, enabled);
			}
			state.enabled= enabled;
		}
	}
	
	public void updateState(final Control control, final boolean recursively,
			final boolean enabled) {
		updateState(ImCollections.newList(control), recursively, enabled);
	}
	
	
	public void restore() {
		for (final var state : this.states) {
			if (state.control.isDisposed()) {
				continue;
			}
			state.control.setEnabled(state.enabled);
		}
	}
	
}
