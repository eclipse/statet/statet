/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.variables.IStringVariable;
import org.eclipse.core.variables.IStringVariableManager;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.ui.StringVariableSelectionDialog.VariableFilter;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.FilteredResourcesSelectionDialog;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.io.ObservableFileValidator;
import org.eclipse.statet.ecommons.ui.SharedMessages;
import org.eclipse.statet.ecommons.ui.components.CustomizableVariableSelectionDialog;
import org.eclipse.statet.ecommons.ui.components.WidgetToolsButton;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.util.MessageUtils;
import org.eclipse.statet.ecommons.ui.workbench.workspace.ResourceContainerSelectionDialog;
import org.eclipse.statet.ecommons.ui.workbench.workspace.ResourceFileSelectionDialog;
import org.eclipse.statet.internal.ecommons.ui.Messages;


/**
 * Composite with text/combo and browse buttons.
 * 
 * Configurable for files and directories, new or existing resources.
 * 
 * XXX: Not yet all combinations are tested!
 */
public class ResourceInputComposite extends Composite implements IValueChangeListener<Object> {
	
	
	private static final String VAR_WORKSPACE_LOC= "workspace_loc"; //$NON-NLS-1$
	
	
	private static class SearchResourceDialog extends FilteredResourcesSelectionDialog {
		
		public SearchResourceDialog(final Shell shell, final boolean multi,
				final IContainer container, final int typesMask) {
			super(shell, multi, container, typesMask);
			setTitle(Messages.ResourceSelectionDialog_title);
		}
	}
	
	
	public static final int MODE_FILE=                      1 << 0;
	public static final int MODE_DIRECTORY=                 1 << 1;
	public static final int MODE_SAVE=                      1 << 2;
	public static final int MODE_OPEN=                      1 << 3;
	public static final int MODE_WS_ONLY=                   1 << 4;
	
	public static final int STYLE_TEXT=                     0;
	public static final int STYLE_COMBO=                    1 << 0;
	public static final int STYLE_GROUP=                    1 << 1;
	public static final int STYLE_LABEL=                    1 << 2;
	
	
	private String resourceLabel;
	
	private final int style;
	private final boolean asCombo;
	
	private boolean forDirectory;
	private boolean forFile;
	private boolean doOpen;
	private boolean wsOnly;
	private boolean controlledChange;
	
	private final ObservableFileValidator validator;
	
	private Text locationTextField;
	private Combo locationComboField;
	
	private Label label;
	private WidgetToolsButton tools;
	private boolean showInsertVariable;
	private ImList<? extends VariableFilter> showInsertVariableFilters;
	private ImList<? extends IStringVariable> showInsertVariableAdditionals;
	
	private String defaultFilesystemPath;
	private String[] fileFilters;
	private String[] fileFilterNames;
	
	
	public ResourceInputComposite(final Composite parent, final int style,
			final int mode, final String resourceLabel) {
		super(parent, SWT.NONE);
		
		this.validator= new ObservableFileValidator(Realm.getDefault());
		setMode(mode);
		
		this.style= style;
		this.asCombo= (this.style & STYLE_COMBO) == STYLE_COMBO;
		this.controlledChange= false;
		setResourceLabel(resourceLabel);
		createContent();
	}
	
	
	public void setHistory(final String[] history) {
		if (history != null && this.asCombo) {
			this.locationComboField.setItems(history);
		}
	}
	
	public void setMode(final int mode) {
		assert ((mode & (MODE_DIRECTORY | MODE_FILE)) != 0);
		if ((mode & MODE_DIRECTORY) == MODE_DIRECTORY) {
			this.forDirectory= true;
			this.validator.setOnDirectory(IStatus.OK);
		}
		else {
			this.forDirectory= false;
			this.validator.setOnDirectory(IStatus.ERROR);
		}
		if ((mode & MODE_FILE) == MODE_FILE) {
			this.forFile= true;
			this.validator.setOnFile(IStatus.OK);
		}
		else {
			this.forFile= false;
			this.validator.setOnFile(IStatus.ERROR);
		}
		
		this.doOpen= (mode & MODE_OPEN) == MODE_OPEN;
		this.validator.setDefaultMode(this.doOpen);
		
		this.wsOnly= (mode & MODE_WS_ONLY) == MODE_WS_ONLY;
		this.validator.setRequireWorkspace(this.wsOnly, this.wsOnly);
		
		if (this.wsOnly) {
			this.validator.getWorkspaceResourceObservable().addValueChangeListener(this);
		}
		else {
			this.validator.getFileStoreObservable().addValueChangeListener(this);
		}
		
		if (this.tools != null) {
			this.tools.resetMenu();
		}
	}
	
	public void setResourceLabel(final String label) {
		this.resourceLabel= label;
		if (this.label != null) {
			this.label.setText(this.resourceLabel + ':');
		}
		this.validator.setResourceLabel(MessageUtils.removeMnemonics(label));
	}
	
	protected String getTaskLabel() {
		return NLS.bind(Messages.ChooseResource_Task_description, this.resourceLabel);
	}
	
	public void setShowInsertVariable(final boolean enable,
			final List<VariableFilter> filters, final List<? extends IStringVariable> additionals) {
		this.showInsertVariable= enable;
		this.showInsertVariableFilters= (filters != null) ? ImCollections.toList(filters) : null;
		if (this.showInsertVariableAdditionals != null) {
			for (final IStringVariable variable : this.showInsertVariableAdditionals) {
				final String name= variable.getName();
				final Pattern pattern= Pattern.compile("\\Q${"+name+"\\E[\\}\\:]");  //$NON-NLS-1$//$NON-NLS-2$
				this.validator.setOnPattern(pattern, -1);
			}
		}
		this.showInsertVariableAdditionals= (additionals != null) ? ImCollections.toList(additionals) : null;
		if (this.showInsertVariableAdditionals != null) {
			for (final IStringVariable variable : this.showInsertVariableAdditionals) {
				final String name= variable.getName();
				final Pattern pattern= Pattern.compile("\\Q${"+name+"\\E[\\}\\:]"); //$NON-NLS-1$ //$NON-NLS-2$
				this.validator.setOnPattern(pattern, IStatus.OK);
			}
		}
		if (this.tools != null) {
			this.tools.resetMenu();
		}
	}
	
	/**
	 * Sets the default filesystem path for file and directory selection dialogs, used if
	 * no resource is specified.
	 * 
	 * @param path the filesystem path
	 */
	public void setDefaultFilesystemPath(final String path) {
		this.defaultFilesystemPath= (path != null && !path.isEmpty()) ? path : null;
	}
	
	/**
	 * Sets the file filters for file selection dialogs.
	 * 
	 * A filter is a string array with:<br/>
	 *     [0]= filter, e.g. "*.txt"<br/>
	 *     [1]= name, e.g. "Text Files"
	 * 
	 * @param filters list of filters 
	 */
	public void setFileFilters(final List<String[]> filters) {
		String[] extensions0;
		String[] names;
		if (filters != null) {
			final int n= filters.size() + 1;
			extensions0= new String[n];
			names= new String[n];
			for (int i= 0; i < n - 1; i++) {
				final String[] strings= filters.get(i);
				extensions0[i]= strings[0];
				names[i]= strings[1] + " ("+strings[0]+")"; //$NON-NLS-1$ //$NON-NLS-2$
			}
			extensions0[n - 1]= "*.*"; //$NON-NLS-1$
			names[n - 1]= Messages.ChooseResource_AllFiles_name + " (*.*)"; //$NON-NLS-1$
		}
		else {
			extensions0= null;
			names= null;
		}
		this.fileFilters= extensions0;
		this.fileFilterNames= names;
	}
	
	
	public Control getTextControl() {
		if (this.asCombo) {
			return this.locationComboField;
		}
		else {
			return this.locationTextField;
		}
	}
	
	protected String escapeText(final String text) {
		if (this.validator.getVariableResolver() != null) {
			return this.validator.getVariableResolver().escapeText(text);
		}
		return text;
	}
	
	protected void setText(final String text) {
		setText(text, true);
	}
	
	private void setText(final String s, final boolean validate) {
		if (!validate) {
			this.controlledChange= true;
		}
		if (this.asCombo) {
			this.locationComboField.setText(s);
		}
		else {
			this.locationTextField.setText(s);
		}
		if (!validate) {
			this.controlledChange= false;
		}
	}
	
	protected void insertText(final String s) {
		if (this.asCombo) {
			//
		}
		else {
			this.locationTextField.insert(s);
		}
	}
	
	protected String getText() {
		if (this.asCombo) {
			return this.locationComboField.getText();
		}
		else {
			return this.locationTextField.getText();
		}
	}
	
	@Override
	public void setEnabled(final boolean enabled) {
		super.setEnabled(enabled);
		
		boolean editable= true;
		if (this.label != null) {
			this.label.setEnabled(enabled);
		}
		if (this.locationTextField != null) {
			this.locationTextField.setEnabled(enabled);
			editable= this.locationTextField.getEditable();
		}
		if (this.locationComboField != null) {
			this.locationComboField.setEnabled(enabled);
		}
		if (this.tools != null) {
			this.tools.setEnabled(enabled && editable);
		}
	}
	
	public void setEditable(final boolean editable) {
		boolean enabled= true;
		this.locationTextField.setEditable(editable);
		enabled= this.locationTextField.isEnabled();
		if (this.tools != null) {
			this.tools.setEnabled(enabled && editable);
		}
	}
	
	
	private void createContent() {
		Composite content;
		final GridLayout layout;
		if ((this.style & STYLE_GROUP) == STYLE_GROUP) {
			setLayout(new FillLayout());
			final Group group= new Group(this, SWT.NONE);
			group.setText(this.resourceLabel + ':');
			content= group;
			layout= LayoutUtils.newGroupGrid(2);
		}
		else {
			content= this;
			layout= LayoutUtils.newCompositeGrid(2);
		}
		layout.horizontalSpacing= 0;
		content.setLayout(layout);
		
		if ((this.style & STYLE_LABEL) != 0) {
			this.label= new Label(content, SWT.LEFT);
			this.label.setText(this.resourceLabel + ':');
			this.label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		}
		
		if (this.asCombo) {
			this.locationComboField= new Combo(content, SWT.DROP_DOWN);
		}
		else {
			this.locationTextField= new Text(content, SWT.BORDER | SWT.SINGLE);
		}
		final Control inputField= getTextControl();
		inputField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				if (!ResourceInputComposite.this.controlledChange) {
					ResourceInputComposite.this.validator.setExplicit(getText());
//					fValidator.getStatus();
				}
			}
		});
		
		this.tools= new WidgetToolsButton(inputField) {
			@Override
			protected void fillMenu(final Menu menu) {
				ResourceInputComposite.this.fillMenu(menu);
			}
		};
		this.tools.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
	}
	
	protected void fillMenu(final Menu menu) {
		final boolean both= (this.forFile && this.forDirectory);
		{
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(Messages.SearchWorkspace_label);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					beforeMenuAction();
					handleSearchWorkspaceButton();
					getTextControl().setFocus();
					afterMenuAction();
				}
			});
		}
		if (this.forFile) {
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(both ? Messages.BrowseWorkspace_ForFile_label : Messages.BrowseWorkspace_label);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					beforeMenuAction();
					handleBrowseWorkspaceButton(MODE_FILE);
					getTextControl().setFocus();
					afterMenuAction();
				}
			});
		}
		if (this.forDirectory) {
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(both ? Messages.BrowseWorkspace_ForDir_label : Messages.BrowseWorkspace_label);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					beforeMenuAction();
					handleBrowseWorkspaceButton(MODE_DIRECTORY);
					getTextControl().setFocus();
					afterMenuAction();
				}
			});
		}	
		if (this.forFile && !this.wsOnly) {
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(both ? Messages.BrowseFilesystem_ForFile_label : Messages.BrowseFilesystem_label);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					beforeMenuAction();
					handleBrowseFilesystemButton(MODE_FILE);
					getTextControl().setFocus();
					afterMenuAction();
				}
			});
		}
		if (this.forDirectory && !this.wsOnly) {
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(both ? Messages.BrowseFilesystem_ForDir_label : Messages.BrowseFilesystem_label);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					beforeMenuAction();
					handleBrowseFilesystemButton(MODE_DIRECTORY);
					getTextControl().setFocus();
					afterMenuAction();
				}
			});
		}
		
		if (this.showInsertVariable) {
			new MenuItem(menu, SWT.SEPARATOR);
		}
		
		if (this.showInsertVariable) {
			final MenuItem item= new MenuItem(menu, SWT.PUSH);
			item.setText(SharedMessages.InsertVariable_label);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {
					beforeMenuAction();
					handleVariablesButton();
					getTextControl().setFocus();
					afterMenuAction();
				}
			});
		}
		
	}
	
	protected void handleSearchWorkspaceButton() {
		int resourceMode= 0;
		if (this.forFile) {
			resourceMode |= IResource.FILE;
		}
		if (this.forDirectory ) {
			resourceMode |= IResource.FOLDER;
		}
		final IWorkspaceRoot container= ResourcesPlugin.getWorkspace().getRoot();
		final SearchResourceDialog dialog= new SearchResourceDialog(getShell(), false, container, resourceMode);
		String initial= ""; //$NON-NLS-1$
		final IFileStore store= this.validator.getFileStore();
		if (store != null) {
			initial= store.getName();
		}
		else {
			final String current= getText();
			final int idx= current.lastIndexOf('/');
			if (idx >= 0) {
				initial= current.substring(idx+1);
			}
			else {
				initial= current;
			}
		}
		dialog.setInitialPattern(initial);
		dialog.open();
		final Object[] results= dialog.getResult();
		if (results == null || results.length < 1) {
			return;
		}
		IResource resource= (IResource) results[0];
		if (!this.forFile && resource.getType() == IResource.FILE) {
			resource= resource.getParent();
		}
		final String wsPath= resource.getFullPath().toString();
		
		this.validator.setExplicit(resource);
		setText(newVariableExpression(VAR_WORKSPACE_LOC, escapeText(wsPath)), false); 
	}
	
	protected void handleBrowseWorkspaceButton(final int mode) {
		IResource res= this.validator.getWorkspaceResource();
		if (res == null) {
			res= ResourcesPlugin.getWorkspace().getRoot();
		}
		
		String wsPath;
		String appendPath;
		IResource resource= null;
		if (mode == MODE_DIRECTORY) {
			if (res instanceof IFile) {
				res= res.getParent();
			}
			final ResourceContainerSelectionDialog dialog= new ResourceContainerSelectionDialog(getShell(),
					getTaskLabel());
			dialog.setInitialElementSelection(res);
			dialog.open();
			resource= dialog.getFirstResult();
			if (resource == null) {
				return;
			}
			wsPath= res.getFullPath().toString();
			appendPath= ""; //$NON-NLS-1$
		}
		else {
			final ResourceFileSelectionDialog dialog= new ResourceFileSelectionDialog(getShell(), getTaskLabel());
			dialog.setInitialElementSelection(res);
			dialog.setAllowNewResources(this.validator.getOnNotExisting() != IStatus.ERROR);
			dialog.open();
			resource= dialog.getFirstResult();
			if (resource == null) {
				return;
			}
			res= resource.getParent();
			final StringBuilder path= new StringBuilder(resource.getName());
			path.insert(0, '/');
			while (!res.exists()) {
				res= res.getParent();
				path.insert(0, res.getName());
				path.insert(0, '/');
			}
			wsPath= res.getFullPath().toString();
			appendPath= path.toString();
		}
		
		this.validator.setExplicit(resource);
		if (this.wsOnly) {
			setText(escapeText(wsPath + appendPath), false);
		}
		else {
			setText(newVariableExpression(VAR_WORKSPACE_LOC, escapeText(wsPath)) + escapeText(appendPath), false);
		}
	}
	
	protected void handleBrowseFilesystemButton(final int mode) {
		String path= null;
		try {
			if (this.validator.isLocalFile()) {
				path= URIUtil.toPath(this.validator.getFileStore().toURI()).toOSString();
			}
			else if (getText().isEmpty()) {
				path= this.defaultFilesystemPath;
			}
		}
		catch (final Exception e) {
		}
		if (mode == MODE_DIRECTORY) {
			final DirectoryDialog dialog= new DirectoryDialog(getShell());
			dialog.setText(MessageUtils.removeMnemonics(getTaskLabel()));
			dialog.setFilterPath(path);
			path= dialog.open();
		}
		else {
			final FileDialog dialog= new FileDialog(getShell(), (this.doOpen) ? SWT.OPEN: SWT.SAVE);
			dialog.setText(MessageUtils.removeMnemonics(getTaskLabel()));
			dialog.setFilterPath(path);
			if (this.fileFilters != null) {
				dialog.setFilterExtensions(this.fileFilters);
				dialog.setFilterNames(this.fileFilterNames);
			}
			path= dialog.open();
		}
		if (path == null) {
			return;
		}
		
		path= escapeText(path);
		this.validator.setExplicit(path);
		setText(path, false);
	}
	
	protected void handleVariablesButton() {
		final CustomizableVariableSelectionDialog dialog= new CustomizableVariableSelectionDialog(getShell());
		if (this.showInsertVariableFilters != null) {
			for (final VariableFilter filter : this.showInsertVariableFilters) {
				dialog.addVariableFilter(filter);
			}
		}
		if (this.showInsertVariableAdditionals != null) {
			dialog.setAdditionals(this.showInsertVariableAdditionals);
		}
		else if (this.validator.getVariableResolver() != null) {
			final Map<String, IStringVariable> extraVariables= this.validator.getVariableResolver()
					.getExtraVariables();
			if (extraVariables != null) {
				dialog.setAdditionals(extraVariables.values());
			}
		}
		if (dialog.open() != Dialog.OK) {
			return;
		}
		final String variable= dialog.getVariableExpression();
		if (variable == null) {
			return;
		}
		insertText(variable);
	}
	
	@Override
	public void handleValueChange(final ValueChangeEvent<?> event) {
		String path= null;
		if (this.wsOnly) {
			final IResource resource= this.validator.getWorkspaceResource();
			if (resource != null) {
				path= resource.getFullPath().toString();
			}
		}
		else {
			final IFileStore fileStore= this.validator.getFileStore();
			if (fileStore != null) {
				try {
					if (this.validator.isLocalFile()) {
						path= URIUtil.toPath(fileStore.toURI()).toOSString();
					}
					else {
						path= fileStore.toURI().toString();
					}
				}
				catch (final Exception e) {
				}
			}
		}
		if (path != null && !path.isEmpty()) {
//			path= "\u21FF " + path; //$NON-NLS-1$
		}
		else {
			path= ""; //$NON-NLS-1$
		}
		getTextControl().setToolTipText(path);
	}
	
	
	public String getResourceString() {
		return getText();
	}
	
	
	public void addModifyListener(final ModifyListener listener) {
		if (this.asCombo) {
			this.locationComboField.addModifyListener(listener);
		}
		else {
			this.locationTextField.addModifyListener(listener);
		}
	}
	
	public IResource getResourceAsWorkspaceResource() {
		return this.validator.getWorkspaceResource();
	}
	
	/**
	 * You must call validate, before this method can return a file
	 * @return a file handler or null.
	 */
	public IFileStore getResourceAsFileStore() {
		return this.validator.getFileStore();
	}
	
	public IObservableValue<String> getObservable() {
		if (this.asCombo) {
			return WidgetProperties.text()
					.observe(this.locationComboField);
		}
		else {
			return WidgetProperties.text(SWT.Modify)
					.observe(this.locationTextField);
		}
	}
	
	public ObservableFileValidator getValidator() {
		return this.validator;
	}
	
	/**
	 * Returns a new variable expression with the given variable and the given
	 * argument.
	 * 
	 * @see IStringVariableManager#generateVariableExpression(String, String)
	 */
	protected String newVariableExpression(final String varName, final String arg) {
		return VariablesPlugin.getDefault().getStringVariableManager().generateVariableExpression(varName, arg);
	}
	
	/**
	 * Is called before a menu action is executed.
	 */
	protected void beforeMenuAction() {
	}
	
	/**
	 * Is called after a menu action is finish.
	 */
	protected void afterMenuAction() {
	}
	
}
