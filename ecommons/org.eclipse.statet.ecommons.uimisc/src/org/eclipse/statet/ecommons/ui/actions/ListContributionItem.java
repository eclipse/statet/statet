/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.ui.actions.CompoundContributionItem;


/**
 * CompoundContributionItem with public access to contribution items
 */
public abstract class ListContributionItem extends CompoundContributionItem {
	
	
	public ListContributionItem() {
	}
	
	public ListContributionItem(final String id) {
		super(id);
	}
	
	
	@Override
	protected IContributionItem[] getContributionItems() {
		final List<IContributionItem> items= new ArrayList<>();
		
		createContributionItems(items);
		
		return items.toArray(new IContributionItem[items.size()]);
	}
	
	protected abstract void createContributionItems(List<IContributionItem> items);
	
	
	/**
	 * Delegates the creation of contribution items to the specified item.
	 */
	protected void createContributionItems(final List<IContributionItem> items,
			final ListContributionItem item) {
		item.createContributionItems(items);
	}
	
}
