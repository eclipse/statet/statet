/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.core.databinding.observable.IObservableCollection;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.viewers.ITreeContentProvider;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class DataAdapter<TItem> {
	
	
	public static class ListAdapter<TItem> extends DataAdapter<TItem> {
		
		
		final IObservableCollection<TItem> list;
		
		
		public ListAdapter(final IObservableCollection<TItem> list, final IObservableValue<@Nullable TItem> defaultValue) {
			super(defaultValue);
			this.list= nonNullAssert(list);
		}
		
		
		@Override
		public Object getAddParent(final Object element) {
			return this.list;
		}
		
		@Override
		public Object getParent(final Object element) {
			return this.list;
		}
		
		@Override
		protected Collection<? super TItem> getContainerFor(final Object element) {
			return this.list;
		}
		
		@Override
		public boolean isMoveAllowed(final Object element, final int direction) {
			if (!super.isMoveAllowed(element, direction)) {
				return false;
			}
			if (this.list instanceof List) {
				final int oldIdx= ((List<?>)this.list).indexOf(element);
				final int newIdx= oldIdx + direction;
				return (oldIdx >= 0 && newIdx >= 0 & newIdx < this.list.size());
			}
			return false;
		}
		
		@Override
		public void delete(final List<? extends @NonNull Object> elements) {
			setDirty(true);
			deleteDefault(elements);
			
			this.list.removeAll(elements);
			
			deleteChecked(elements);
		}
		
		@Override
		public void move(final Object item, final int direction) {
			final int oldIdx= ((IObservableList<TItem>)this.list).indexOf(item);
			final int newIdx= oldIdx + direction;
			if (oldIdx < 0 || newIdx < 0 || newIdx >= this.list.size()) {
				return;
			}
			moveByIdx(oldIdx, newIdx);
		}
		
		protected void moveByIdx(final int oldIdx, final int newIdx) {
			((IObservableList<TItem>)this.list).move(oldIdx, newIdx);
		}
		
		@Override
		protected void changeDefault(final @Nullable TItem oldItem, final TItem newItem) {
			if (oldItem == null) {
				if (this.defaultValue != null && this.list.isEmpty()) {
					this.defaultValue.setValue(newItem);
				}
				return;
			}
			super.changeDefault(oldItem, newItem);
		}
		
	}
	
	public static class TreeAdapter<TItem> extends DataAdapter<TItem> {
		
		
		private final ITreeContentProvider contentProvider;
		
		public TreeAdapter(final ITreeContentProvider contentProvider,
				final IObservableValue<? extends @Nullable Object> defaultValue) {
			super(defaultValue);
			this.contentProvider= nonNullAssert(contentProvider);
		}
		
		
		@Override
		public @Nullable Object getAddParent(final Object element) {
			if (isContentItem(element)) {
				return getParent(element);
			}
			return element;
		}
		
		@Override
		public @Nullable Object getParent(final Object element) {
			return this.contentProvider.getParent(element);
		}
		
		@Override
		public @NonNull Object[] getChildren(final Object element) {
			return this.contentProvider.getChildren(element);
		}
		
	}
	
	
	private Set<TItem> checkedSet;
	
	protected final IObservableValue defaultValue;
	
	private boolean isDirty;
	
	
	public DataAdapter(final IObservableValue<? extends @Nullable Object> defaultValue) {
		this.defaultValue= defaultValue;
	}
	
	
	public @Nullable Object getAddParent(final Object element) {
		return null;
	}
	
	public @Nullable Object getParent(final Object element) {
		return null;
	}
	
	public @NonNull Object @Nullable [] getChildren(final Object element) {
		return null;
	}
	
	public boolean isAddAllowed(final Object element) {
		return true;
	}
	
	public boolean isContentItem(final @Nullable Object element) {
		return true;
	}
	
	public boolean isModifyAllowed(final Object element) {
		return isContentItem(element);
	}
	
	public boolean isMoveAllowed(final Object element, final int direction) {
		return isModifyAllowed(element);
	}
	
	public boolean isDeleteAllowed(final Object element) {
		return isModifyAllowed(element);
	}
	
	public TItem getModelItem(final Object element) {
		return (TItem)element;
	}
	
	public Object getViewerElement(final TItem item, final @Nullable Object parent) {
		return item;
	}
	
	
	public void setCheckedModel(final Set<TItem> set) {
		this.checkedSet= set;
	}
	
	protected @Nullable Object getContainerFor(final Object element) {
		return null;
	}
	
	protected @Nullable IObservableValue getDefaultFor(final TItem item) {
		return this.defaultValue;
	}
	
	public void setDefault(final TItem item) {
		final var observable= getDefaultFor(item);
		if (observable == null) {
			return;
		}
		this.isDirty= true;
		if (item != null) {
			observable.setValue(getDefaultValue(item));
		}
	}
	
	public Object change(final @Nullable TItem oldItem, final TItem newItem,
			final Object parent, final @Nullable Object container ) {
		if (container instanceof Collection) {
			final var list= (Collection<? super @NonNull TItem>)container;
			setDirty(true);
			changeDefault(oldItem, newItem);
			if (oldItem == null) {
				list.add(newItem);
			}
			else {
				if (oldItem != newItem) { // can be directly manipulated or replaced)
					if (list instanceof List) {
						final int idx= ((List<? super @NonNull TItem>)list).indexOf(oldItem);
						((List<? super @NonNull TItem>)list).set(idx, newItem);
					}
					else {
						list.remove(oldItem);
						list.add(newItem);
					}
				}
			}
			final Object editElement= getViewerElement(newItem, parent);
			
			changeChecked(oldItem, newItem);
			return editElement;
		}
		else {
			throw new UnsupportedOperationException();
		}
	}
	
	protected Object getDefaultValue(final @Nullable TItem item) {
		return item;
	}
	
	protected void changeDefault(final @Nullable TItem oldItem, final @Nullable TItem newItem) {
		if (oldItem == null) {
			return;
		}
		final var observable= getDefaultFor(oldItem);
		if (observable == null) {
			return;
		}
		final var oldValue= getDefaultValue(oldItem);
		final var newValue= getDefaultValue(newItem);
		if (oldValue != newValue && oldValue.equals(observable.getValue())) {
			observable.setValue(newValue);
		}
	}
	
	protected void changeChecked(final TItem oldItem, final TItem newItem) {
		if (this.checkedSet != null) {
			if (oldItem == null) {
				this.checkedSet.add(newItem);
			}
			else {
				if (this.checkedSet.remove(oldItem)) {
					this.checkedSet.add(newItem);
				}
			}
		}
	}
	
	public void delete(final List<? extends @NonNull Object> elements) {
		setDirty(true);
		deleteDefault(elements);
		
		for (final var element : elements) {
			delete(getModelItem(element), getContainerFor(element));
		}
		
		deleteChecked(elements);
	}
	
	protected void delete(final TItem item, final @Nullable Object container) {
		if (container instanceof Collection) {
			((Collection<?>)container).remove(item);
		}
		else {
			throw new UnsupportedOperationException();
		}
	}
	
	protected void deleteDefault(final List<? extends @NonNull Object> elements) {
		if (elements.isEmpty()) {
			return;
		}
		for (final Object element : elements) {
			final TItem item= getModelItem(element);
			if (item == null) {
				continue;
			}
			final var observable= getDefaultFor(item);
			if (observable == null) {
				continue;
			}
			final var itemValue= getDefaultValue(item);
			if (itemValue != null && itemValue.equals(observable.getValue())) {
				observable.setValue(null);
				return;
			}
		}
	}
	
	protected void deleteChecked(final List<? extends @NonNull Object> elements) {
		if (this.checkedSet != null) {
			this.checkedSet.removeAll(elements);
		}
	}
	
	public void move(final Object item, final int direction) {
		throw new UnsupportedOperationException();
	}
	
	
	public void setDirty(final boolean isDirty) {
		this.isDirty= isDirty;
	}
	
	public boolean isDirty() {
		return this.isDirty;
	}
	
}
