/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench.workspace;

import java.util.Iterator;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.resources.core.variables.ResourceVariableResolver;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


@NonNullByDefault
public class ResourceVariableUtil implements ResourceVariableResolver.Context {
	
	
	/**
	 * Returns the currently selected resource in the active workbench window, or <code>null</code>
	 * if none.  If an editor is active, the resource adapter associated with the editor is
	 * returned, if any.
	 * 
	 * @return selected resource or <code>null</code>
	 */
	public static @Nullable IResource fetchSelectedResource() {
		return fetchSelectedResource(null);
	}
	
	private static @Nullable IResource fetchSelectedResource(final @Nullable IWorkbenchPart part) {
		// Compatible with DebugUITools.getSelectedResource
		final Display display= UIAccess.getDisplay();
		if (Thread.currentThread() == display.getThread()) {
			return getSelectedResource0(part);
		}
		else {
			class DisplayRunnable implements Runnable {
				
				@Nullable IResource resource;
				
				@Override
				public void run() {
					this.resource= getSelectedResource0(part);
				}
				
			}
			final DisplayRunnable runnable= new DisplayRunnable();
			display.syncExec(runnable);
			return runnable.resource;
		}
	}
	
	private static @Nullable IResource getSelectedResource0(@Nullable IWorkbenchPart part) {
		IResource resource= null;
		if (part == null) {
			final IWorkbenchPage page= UIAccess.getActiveWorkbenchPage(true);
			if (page != null) {
				part= page.getActivePart();
			}
		}
		if (part != null) {
			if (part instanceof final IEditorPart epart) {
				resource= epart.getEditorInput().getAdapter(IResource.class);
			}
			if (resource == null) {
				final IWorkbenchPartSite site= part.getSite();
				if (site != null) {
					final ISelectionProvider provider= site.getSelectionProvider();
					if (provider != null) {
						final ISelection selection= provider.getSelection();
						if (selection instanceof final IStructuredSelection structuredSelection) {
							if (!structuredSelection.isEmpty()) {
								final Iterator<?> iterator= structuredSelection.iterator();
								final IAdapterManager adapterManager= Platform.getAdapterManager();
								while (resource == null && iterator.hasNext()) {
									final Object element= iterator.next();
									resource= adapterManager.getAdapter(element, IResource.class);
								}
							}
						}
					}
				}
			}
		}
		return resource;
	}
	
	
	protected static final byte S_RESOURCE_FETCHED=         0b0_0000_0001;
	
	
	private final IWorkbenchPage workbenchPage;
	
	private final @Nullable IWorkbenchPart part;
	
	private byte state;
	
	private @Nullable IResource resource;
	
	
	
	public ResourceVariableUtil() {
		this(UIAccess.getActiveWorkbenchPage(true));
	}
	
	public ResourceVariableUtil(final IWorkbenchPage page) {
		this.workbenchPage= page;
		this.part= this.workbenchPage.getActivePart();
	}
	
	public ResourceVariableUtil(final IWorkbenchPart part) {
		this.workbenchPage= part.getSite().getPage();
		this.part= part;
	}
	
	public ResourceVariableUtil(final @Nullable IResource resource) {
		this();
		this.state|= S_RESOURCE_FETCHED;
		this.resource= resource;
	}
	
	public ResourceVariableUtil(final ResourceVariableUtil location, final @Nullable IResource resource) {
		this.workbenchPage= location.getWorkbenchPage();
		this.part= location.getWorkbenchPart();
		this.state|= S_RESOURCE_FETCHED;
		this.resource= resource;
	}
	
	
	public IWorkbenchPage getWorkbenchPage() {
		return this.workbenchPage;
	}
	
	public @Nullable IWorkbenchPart getWorkbenchPart() {
		return this.part;
	}
	
	@Override
	public @Nullable IResource getResource() {
		if ((this.state & S_RESOURCE_FETCHED) == 0) {
			this.state|= S_RESOURCE_FETCHED;
			this.resource= fetchResource();
		}
		return this.resource;
	}
	
	protected void resetResource() {
		this.state&= ~S_RESOURCE_FETCHED;
		this.resource= null;
	}
	
	protected @Nullable IResource fetchResource() {
		return fetchSelectedResource(getWorkbenchPart());
	}
	
}
