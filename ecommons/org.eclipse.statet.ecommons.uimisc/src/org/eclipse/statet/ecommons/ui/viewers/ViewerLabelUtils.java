/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ViewerLabelUtils {
	
	
	/**
	 * Sets the given <code>styler</code> to use for the specified <code>regions</code>.
	 * 
	 * @param styledText the styled text
	 * @param regions the regions specified by start and end index
	 * @param offset the offset of regions in styledText
	 * @param styler the styler to set
	 */
	public static void setStyle(final StyledString styledText, final int @Nullable [] regions,
			final Styler styler) {
		if (regions == null) {
			return;
		}
		for (int i= 0; i + 1 < regions.length; ) {
			final int begin= regions[i];
			int end= regions[i + 1];
			i+= 2;
			while (i < regions.length && regions[i] == end) {
				end= regions[i + 1];
				i+= 2;
			}
			
			styledText.setStyle(begin, end - begin, styler);
		}
	}
	
	
	private ViewerLabelUtils() {}
	
}
