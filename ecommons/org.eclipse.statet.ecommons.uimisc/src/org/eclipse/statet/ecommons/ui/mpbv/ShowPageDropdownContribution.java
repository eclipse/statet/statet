/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.actions.CompoundContributionItem;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.actions.SimpleContributionItem;


@NonNullByDefault
public class ShowPageDropdownContribution<S extends PageBookSession> extends CompoundContributionItem {
	
	
	private final ManagedPageBookView<S> view;
	
	
	public ShowPageDropdownContribution(final ManagedPageBookView<S> view) {
		this.view= view;
	}
	
	
	@Override
	protected IContributionItem[] getContributionItems() {
		final List<IContributionItem> list= new ArrayList<>();
		
		final List<S> sessions= this.view.getSessions();
		for (int i= 0; i < sessions.size(); i++) {
			list.add(createPageContribution(sessions.get(i), i+1));
		}
		
		return list.toArray(new @NonNull IContributionItem[list.size()]);
	}
	
	private ContributionItem createPageContribution(final S session, final int num) {
		final ImageDescriptor imageDescriptor= session.getImageDescriptor();
		String label= session.getLabel();
		String mnemonic= null;
		if (num < 10) {
			mnemonic= Integer.toString(num);
			label= mnemonic + " " + label;
		}
		final SimpleContributionItem item= new SimpleContributionItem(
				label, mnemonic, imageDescriptor, null,
				SimpleContributionItem.STYLE_RADIO ) {
			@Override
			protected void execute() throws ExecutionException {
				ShowPageDropdownContribution.this.view.showPage(session);
			}
		};
		if (this.view.getCurrentSession() == session) {
			item.setChecked(true);
		}
		return item;
	}
	
}
