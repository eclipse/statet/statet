/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs;

import java.util.LinkedHashSet;
import java.util.List;

import org.osgi.framework.Bundle;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.util.Geometry;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.ui.PlatformUI;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.swt.ControlEnableStates;


/**
 * Util methods for dialogs
 */
@NonNullByDefault
public class DialogUtils {
	
	
	public static final int HISTORY_MAX= 25;
	
	private static final @NonNull String[] EMPTY_ARRAY_SETTING= new @NonNull String[0];
	
	
	public static IDialogSettings getDialogSettings(final Bundle bundle, final String dialogId) {
		final String sectionName= dialogId;
		final IDialogSettings settings= PlatformUI.getDialogSettingsProvider(bundle)
				.getDialogSettings();
		IDialogSettings section= settings.getSection(sectionName);
		if (section == null) {
			section= settings.addNewSection(sectionName);
		}
		return section;
	}
	
	public static IDialogSettings getDialogSettings(final Plugin plugin, final String dialogId) {
		final Bundle bundle= plugin.getBundle();
		if (bundle == null) {
			throw new RuntimeException("plugin is not started.");
		}
		return getDialogSettings(bundle, dialogId);
	}
	
	public static IDialogSettings getSection(final IDialogSettings settings, final String sectionName) {
		IDialogSettings section= settings.getSection(sectionName);
		if (section == null) {
			section= settings.addNewSection(sectionName);
		}
		return section;
	}
	
	/**
	 * Combines existing items and new item of a history setting
	 * and saves it to the dialog settings section
	 * 
	 * @param settings settings section
	 * @param key settings key
	 * @param newItem optional new item
	 */
	public static void saveHistorySettings(final IDialogSettings settings, final String key,
			final String newItem) {
		final @NonNull String[] items= combineHistoryItems(settings.getArray(key), newItem);
		settings.put(key, items);
	}
	
	/**
	 * Combines existing items and new item of a history setting
	 * 
	 * @param existingItems optional array of existing items
	 * @param newItem optional new item
	 */
	public static @NonNull String[] combineHistoryItems(
			final @NonNull String @Nullable [] existingItems, final @Nullable String newItem) {
		final LinkedHashSet<String> history= new LinkedHashSet<>(HISTORY_MAX);
		if (newItem != null && newItem.length() > 0) {
			history.add(newItem);
		}
		if (existingItems != null) {
			for (int i= 0; i < existingItems.length && history.size() < HISTORY_MAX; i++) {
				history.add(existingItems[i]);
			}
		}
		return history.toArray(new @NonNull String[history.size()]);
	}
	
	public static @NonNull String[] noNull(final @NonNull String @Nullable [] array) {
		return (array != null) ? array : EMPTY_ARRAY_SETTING;
	}
	
	
	/**
	 * Recursively enables/disables all controls and their children.
	 * {@link Control#setEnabled(boolean)}
	 * 
	 * See {@link ControlEnableStates} if saving state is required.
	 * 
	 * @param control list of controls
	 * @param exceptions
	 * @param enable
	 */
	public static void setEnabled(final List<? extends Control> controls,
			final @Nullable List<? extends Control> exceptions, final boolean enable) {
		for (final var control : controls) {
			if ((exceptions != null && exceptions.contains(control))) {
				continue;
			}
			control.setEnabled(enable);
			if (control instanceof final Composite composite) {
				setEnabled(ImCollections.newList(composite.getChildren()), exceptions, enable);
			}
		}
	}
	
	/**
	 * Recursively enables/disables all controls and their children.
	 * {@link Control#setEnabled(boolean)}
	 * 
	 * See {@link ControlEnableStates} if saving state is required.
	 * 
	 * @param control array of controls
	 * @param exceptions
	 * @param enable
	 */
	public static void setEnabled(final @NonNull Control[] controls,
			final @Nullable List<? extends Control> exceptions, final boolean enable) {
		setEnabled(ImCollections.newList(controls), exceptions, enable);
	}
	
	/**
	 * Recursively enables/disables all controls and their children.
	 * {@link Control#setEnabled(boolean)}
	 * 
	 * See {@link ControlEnableStates} if saving state is required.
	 * 
	 * @param control a control
	 * @param exceptions
	 * @param enable
	 */
	public static void setEnabled(final Control control,
			final @Nullable List<? extends Control> exceptions, final boolean enable) {
		setEnabled(ImCollections.newList(control), exceptions, enable);
	}
	
	
	/**
	 * Recursively sets visible/invisible to all controls and their children.
	 * {@link Control#setVisible(boolean)}
	 * 
	 * @param control list of controls
	 * @param exceptions
	 * @param enable
	 */
	public static void setVisible(final List<? extends Control> controls,
			final @Nullable List<? extends Control> exceptions, final boolean enable) {
		for (final var control : controls) {
			if ((exceptions != null && exceptions.contains(control))) {
				continue;
			}
			control.setVisible(enable);
			if (control instanceof final Composite composite) {
				setVisible(ImCollections.newList(composite.getChildren()), exceptions, enable);
			}
		}
	}
	
	/**
	 * Recursively sets visible/invisible to the control and its children.
	 * {@link Control#setVisible(boolean)}
	 * 
	 * @param control a control
	 * @param exceptions
	 * @param enable
	 */
	public static void setVisible(final Control control,
			final @Nullable List<? extends Control> exceptions, final boolean enable) {
		setVisible(ImCollections.newList(control), exceptions, enable);
	}
	
	
	public static Monitor getClosestMonitor(final Display toSearch, final Rectangle rectangle) {
		int closest= Integer.MAX_VALUE;
		
		final Point toFind= Geometry.centerPoint(rectangle);
		final var monitors= toSearch.getMonitors();
		Monitor result= monitors[0];
		
		for (int idx= 0; idx < monitors.length; idx++) {
			final Monitor current= monitors[idx];
			
			final Rectangle clientArea= current.getClientArea();
			
			if (clientArea.contains(toFind)) {
				return current;
			}
			
			final int distance= Geometry.distanceSquared(Geometry.centerPoint(clientArea), toFind);
			if (distance < closest) {
				closest= distance;
				result= current;
			}
		}
		
		return result;
	}
	
	
	private DialogUtils() {}
	
}
