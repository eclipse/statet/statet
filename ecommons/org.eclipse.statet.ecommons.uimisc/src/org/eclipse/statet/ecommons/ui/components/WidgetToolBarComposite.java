/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolBar;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


/**
 * Composite adding a toolbar at the top of a widget.
 */
public class WidgetToolBarComposite extends Composite {
	
	
	private final Listener listener;
	
	private ToolBar leftToolBar;
	private ToolBar rightToolBar;
	
	
	public WidgetToolBarComposite(final Composite parent, final int style) {
		super(parent, style);
		
		{	final GridLayout layout= LayoutUtils.newCompositeGrid(2);
			layout.verticalSpacing= 2;
			setLayout(layout);
		}
		
		this.listener= new Listener() {
			@Override
			public void handleEvent(final Event event) {
				switch (event.type) {
				case SWT.Paint:
					onPaint(event);
					break;
				default:
					break;
				}
			}
		};
		
		createToolBars();
	}
	
	
	private void createToolBars() {
		this.leftToolBar= new ToolBar(this, SWT.FLAT);
		this.leftToolBar.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, true, false));
		
		this.rightToolBar= new ToolBar(this, SWT.FLAT | SWT.RIGHT);
		this.rightToolBar.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false));
		
		addListener(SWT.Paint, this.listener);
	}
	
	
	public ToolBar getLeftToolBar() {
		return this.leftToolBar;
	}
	
	public ToolBar getRightToolBar() {
		return this.rightToolBar;
	}
	
	
	private void onPaint(final Event event) {
		final GC gc= event.gc;
		
		final Rectangle toolBarBounds= this.leftToolBar.getBounds();
		gc.setForeground(gc.getDevice().getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		final int y= toolBarBounds.y + toolBarBounds.height + 1;
		gc.drawLine(event.x, y, event.x + event.width, y);
	}
	
	public GridData getContentLayoutData() {
		return new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
	}
	
}
