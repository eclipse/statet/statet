/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.swt.SWT;
import org.eclipse.swt.accessibility.AccessibleAdapter;
import org.eclipse.swt.accessibility.AccessibleEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Search text custom widget (with clear button)
 */
@NonNullByDefault
public class SearchText extends Composite {
	// see org.eclipse.ui.dialogs.FilteredTree
	
	
	public static interface Listener {
		
		void okPressed();
		
		void downPressed();
		
		void textChanged(final boolean user);
		
	}
	
	
	private class SWTListener implements org.eclipse.swt.widgets.Listener {
		
		@Override
		public void handleEvent(final Event event) {
			switch (event.type) {
			case SWT.Resize:
				updateLayout();
				return;
			}
		}
		
	}
	
	
	private final Text textControl;
	
	private final CopyOnWriteIdentityListSet<Listener> listeners= new CopyOnWriteIdentityListSet<>();
	
	private boolean typingChange= true;
	
	
	public SearchText(final Composite parent) {
		this(parent, null, SWT.BORDER);
	}
	
	public SearchText(final Composite parent, final @Nullable String initialText,
			final int textStyle) {
		super(parent, SWT.NONE);
		
		this.textControl= createText(this, textStyle);
		createClearTextButtonSupport(this);
		
		if (initialText != null) {
			setText(initialText);
			this.textControl.selectAll();
		}
		
		final SWTListener swtListener= new SWTListener();
		addListener(SWT.Resize, swtListener);
	}
	
	
	public void addListener(final Listener listener) {
		this.listeners.add(nonNullAssert(listener));
	}
	
	public void removeListener(final Listener listener) {
		this.listeners.remove(listener);
	}
	
	
	private void textChanged0() {
		final boolean typingChange= this.typingChange;
		this.typingChange= true;
		for (final Listener listener : this.listeners) {
			listener.textChanged(typingChange);
		}
	}
	
	private void okPressed0() {
		for (final Listener listener : this.listeners) {
			listener.okPressed();
		}
	}
	
	private void downPressed0() {
		for (final Listener listener : this.listeners) {
			listener.downPressed();
		}
	}
	
	
	@Override
	public boolean setFocus() {
		return this.textControl.setFocus();
	}
	
	public void setMessage(final String text) {
		this.textControl.setMessage(text);
	}
	
	@Override
	public void setToolTipText(final @Nullable String text) {
		this.textControl.setToolTipText(text);
	}
	
	public void setText(final @Nullable String text) {
		this.typingChange= false;
		this.textControl.setText((text != null) ? text : ""); //$NON-NLS-1$
	}
	
	public String getText() {
		return this.textControl.getText();
	}
	
	public Text getTextControl() {
		return this.textControl;
	}
	
	public void clearText() {
		setText(null);
		if (isVisible()) {
			this.textControl.setFocus();
		}
	}
	
	/**
	 * Create the text widget.
	 * 
	 * @param parent parent <code>Composite</code> of toolbar button
	 * @param style additional SWT style for text constant
	 * @return 
	 */
	private Text createText(final Composite parent, int style) {
		style |= (SWT.LEFT | SWT.SINGLE | SWT.ICON_CANCEL);
		if ((style & SWT.BORDER) != 0) {
			style |= SWT.SEARCH;
		}
		final Text textControl= new Text(this, style);
		
		textControl.getAccessible().addAccessibleListener(
				new AccessibleAdapter() {
					@Override
					public void getName(final AccessibleEvent e) {
						e.result= getAccessibleMessage();
					}
				});
		textControl.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.keyCode == SWT.ARROW_DOWN) {
					downPressed0();
					e.doit= false;
					return;
				}
				if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
					okPressed0();
					e.doit= false;
					return;
				}
			}
		});
		textControl.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(final ModifyEvent e) {
				textChanged0();
			}
		});
		return textControl;
	}
	
	protected String getAccessibleMessage() {
		return this.textControl.getText();
	}
	
	/**
	 * Create the button that clears the text.
	 * 
	 * @param parent parent <code>Composite</code> of toolbar button
	 */
	private void createClearTextButtonSupport(final Composite parent) {
		this.textControl.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.keyCode == SWT.ESC) {
					// allows other top level actions if field was already empty
					final boolean alreadyClear= (SearchText.this.textControl.getText().isEmpty());
					setText(null);
					e.doit= alreadyClear;
					return;
				}
			}
		});
		this.textControl.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				if (e.detail == SWT.ICON_CANCEL) {
					clearText();
				}
			}
		});
	}
	
	@Override
	public Point computeSize(final int wHint, final int hHint, final boolean changed) {
		final Point textSize= this.textControl.computeSize(wHint, hHint, changed);
		final Rectangle trim= computeTrim(0, 0, textSize.x, textSize.y);
		return new Point(trim.width, trim.height);
	}
	
	private void updateLayout() {
		final Rectangle clientArea= getClientArea();
		this.textControl.setBounds(clientArea);
	}
	
}
