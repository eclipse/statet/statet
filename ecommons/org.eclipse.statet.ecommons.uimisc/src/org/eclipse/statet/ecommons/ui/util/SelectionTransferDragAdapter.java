/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.util.TransferDragSourceListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;


public class SelectionTransferDragAdapter extends DragSourceAdapter implements TransferDragSourceListener {
	
	
	private final ISelectionProvider fSelectionProvider;
	
	
	public SelectionTransferDragAdapter(final ISelectionProvider selectionProvider) {
		assert (selectionProvider != null);
		fSelectionProvider = selectionProvider;
	}
	
	
	@Override
	public Transfer getTransfer() {
		return LocalSelectionTransfer.getTransfer();
	}
	
	@Override
	public void dragStart(final DragSourceEvent event) {
		final ISelection selection = fSelectionProvider.getSelection();
		final LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
		transfer.setSelection(selection);
		transfer.setSelectionSetTime(event.time & 0xFFFFFFFFL);
		
		if (selection.isEmpty()) {
			event.doit = false;
			return;
		}
	}
	
	@Override
	public void dragSetData(final DragSourceEvent event) {
		event.data = LocalSelectionTransfer.getTransfer().getSelection();
	}
	
	@Override
	public void dragFinished(final DragSourceEvent event) {
		final LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
		transfer.setSelection(null);
		transfer.setSelectionSetTime(0);
	}
	
}
