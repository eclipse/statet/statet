/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.graphics.core.ColorDef;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


@NonNullByDefault
public class AlphaSelector extends Canvas implements IObjValueWidget<Float> {
	
	
	private static final ColorDef DEFAULT_BASE= new ColorDef(0, 0, 0);
	
	private static final int INITIAL= 1 << 0;
	
	
	private static final class Data {
		
		private final int size;
		private final float factor;
		
		private final Color backgroundColor;
		
		private final ColorDef baseColor;
		
		private final int alphaX0;
		private final int alphaX1;
		
		private final int y0;
		private final int y1;
		
		
		public Data(final int size, final Color backgroundColor, final ColorDef baseColor) {
			this.size= size;
			this.backgroundColor= backgroundColor;
			this.baseColor= baseColor;
			this.factor= size - 1;
			
			this.alphaX0= 1;
			this.alphaX1= this.alphaX0 + Math.round(size * 0.15f);
			
			this.y0= 1;
			this.y1= this.y0 + size;
		}
		
		
		public int alpha_y_255(final int y) {
			final int v= 255 - Math.round(((y - this.y0) / this.factor) * 255f);
			if (v <= 0) {
				return 0;
			}
			if (v >= 255) {
				return 255;
			}
			return v;
		}
		
		public int alpha_255_y(final int v) {
			return this.y0 + Math.round((v / 255f) * this.factor);
		}
		
		public float alpha_y_01(final int y) {
			final float v= 1f - ((y - this.y0) / this.factor);
			if (v <= 0f) {
				return 0f;
			}
			if (v >= 1f) {
				return 1f;
			}
			return v;
		}
		
		public int alpha_01_y(final float v) {
			return this.y0 + Math.round(((1f - v) * this.factor));
		}
		
		public int getBaseMax() {
			return Math.max(this.baseColor.getRed(), Math.max(this.baseColor.getGreen(), this.baseColor.getBlue()));
		}
		
		public Image createImage(final Display display) {
			final Image image= new Image(display, this.alphaX1 + 1, this.y1 + 1);
			final GC gc= new GC(image);
			try {
				gc.setAdvanced(false);
				
				gc.setBackground(this.backgroundColor);
				gc.fillRectangle(0, 0, image.getImageData().width, image.getImageData().height);
				
				// prim
				if (this.baseColor.equalsRGB(DEFAULT_BASE)) {
					final int x1= this.alphaX1 - 1;
					for (int y= this.y0; y < this.y1; y++) {
						final int alpha255= 255 - alpha_y_255(y);
						gc.setForeground(new Color(alpha255, alpha255, alpha255));
						gc.drawLine(this.alphaX0, y, x1, y);
					}
				}
				else {
					final int x1= this.alphaX1 - 1;
					for (int y= this.y0; y < this.y1; y++) {
						final int alpha255= alpha_y_255(y);
						final int white= 255 - alpha255;
						gc.setForeground(new Color(
								white + (this.baseColor.getRed() * alpha255) / 255,
								white + (this.baseColor.getGreen() * alpha255) / 255,
								white + (this.baseColor.getBlue() * alpha255) / 255 ));
						gc.drawLine(this.alphaX0, y, x1, y);
					}
				}
			}
			finally {
				gc.dispose();
			}
			return image;
		}
		
	}
	
	
	private class SWTListener implements PaintListener, Listener {
		
		
		private static final int TRACK_ALPHA= 1;
		
		private @Nullable Data data;
		
		private @Nullable Image image;
		
		private int mouseState;
		
		
		@Override
		public void handleEvent(final Event event) {
			final var data= this.data;
			switch (event.type) {
			case SWT.MouseDown:
				if (data != null && event.y >= data.y0 && event.y < data.y1
						&& event.x >= data.alphaX0 && event.x < data.alphaX1) {
					doSetValue(Float.valueOf(data.alpha_y_01(event.y)), event.time, 0);
					this.mouseState= TRACK_ALPHA;
				}
				return;
			case SWT.MouseUp:
				this.mouseState= 0;
				return;
			case SWT.MouseMove:
				if (data != null) {
					switch (this.mouseState) {
					case TRACK_ALPHA:
						doSetValue(Float.valueOf(data.alpha_y_01(event.y)), event.time, 0);
						break;
					}
				}
				return;
			case SWT.Dispose:
				if (this.image != null) {
					this.image.dispose();
					this.image= null;
				}
			}
		}
		
		private int computeSize(int width, final int height) {
			width= Math.round(width / 0.15f);
			return Math.min(width - 2, height - 2);
		}
		
		@Override
		public void paintControl(final PaintEvent e) {
			final Rectangle clientArea= getClientArea();
			int size= computeSize(clientArea.width, clientArea.height);
			if (AlphaSelector.this.size > 0 && AlphaSelector.this.size < size) {
				size= AlphaSelector.this.size;
			}
			
			var data= this.data;
			var image= this.image;
			if (data == null || data.size != size || !data.baseColor.equalsRGB(AlphaSelector.this.baseColor)) {
				if (image != null) {
					this.image= null;
					image.dispose();
					image= null;
				}
				data= new Data(size, AlphaSelector.this.backgroundColor, AlphaSelector.this.baseColor);
				this.data= data;
			}
			
			final GC gc= e.gc;
			
			gc.setAdvanced(false);
			
			gc.setBackground(AlphaSelector.this.backgroundColor);
			gc.fillRectangle(clientArea);
			
//			if (fImage == null) {
				if (image != null) {
					image.dispose();
					image= null;
				}
				image= data.createImage(e.display);
				this.image= image;
//			}
			gc.drawImage(image, 0, 0);
			
			gc.setLineWidth(1);
			gc.setAdvanced(true);
			gc.setAntialias(SWT.ON);
			
			{	final float alpha= AlphaSelector.this.value.floatValue();
				final int y= data.alpha_01_y(alpha);
				gc.setForeground(e.display.getSystemColor(SWT.COLOR_BLACK));
				gc.drawLine(data.alphaX0 - 1, y, data.alphaX1, y);
				if (255 * (1f - alpha) + (data.getBaseMax() * alpha) < 127) {
					gc.setForeground(e.display.getSystemColor(SWT.COLOR_WHITE));
					gc.drawLine(data.alphaX0, y, data.alphaX1 - 1, y);
				}
			}
		}
		
	}
	
	
	private int size= 8 + LayoutUtils.defaultHSpacing() * 30;
	
	private Float value= Float.valueOf(1f);
	
	private int flags= INITIAL;
	
	private final CopyOnWriteIdentityListSet<IObjValueListener<Float>> valueListeners= new CopyOnWriteIdentityListSet<>();
	
	private ColorDef baseColor= DEFAULT_BASE;
	
	private Color backgroundColor;
	
	
	public AlphaSelector(final Composite parent, final Color backgroundColor) {
		super(parent, SWT.DOUBLE_BUFFERED);
		
		this.backgroundColor= backgroundColor;
		
		final SWTListener listener= new SWTListener();
		addPaintListener(listener);
		addListener(SWT.MouseDown, listener);
		addListener(SWT.MouseUp, listener);
		addListener(SWT.MouseMove, listener);
		addListener(SWT.Dispose, listener);
	}
	
	
	public void setSize(final int size) {
		this.size= size;
	}
	
	public void setBaseColor(final ColorDef color) {
		this.baseColor= (color != null) ? color : DEFAULT_BASE;
		redraw();
	}
	
	private boolean doSetValue(final Float newValue, final int time, final int flags) {
		final Float oldValue= this.value; 
		if (oldValue.equals(newValue) && (flags | (this.flags & INITIAL)) == 0) {
			return false;
		}
		
		this.value= newValue;
		
		final var event= new ObjValueEvent<>(this, time, 0, oldValue, newValue, flags);
		for (final var listener : this.valueListeners.toList()) {
			event.newValue= newValue;
			listener.valueChanged(event);
		}
		
		if (!isDisposed()) {
			redraw();
		}
		
		return true;
	}
	
	
	@Override
	public Point computeSize(final int wHint, final int hHint, final boolean changed) {
		int width= 2 + Math.round(this.size * 0.15f);
		int height= 2 + this.size;
		final int border= getBorderWidth();
		width+= border * 2;
		height+= border * 2;
		return new Point(width, height);
	}
	
	
	@Override
	public Control getControl() {
		return this;
	}
	
	@Override
	public Class<Float> getValueType() {
		return Float.class;
	}
	
	@Override
	public void addValueListener(final IObjValueListener<Float> listener) {
		this.valueListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeValueListener(final IObjValueListener<Float> listener) {
		this.valueListeners.remove(listener);
	}
	
	@Override
	public Float getValue(final int idx) {
		if (idx != 0) {
			throw new IllegalArgumentException("idx: " + idx); //$NON-NLS-1$
		}
		return this.value;
	}
	
	@Override
	public void setValue(final int idx, final Float value) {
		if (idx != 0) {
			throw new IllegalArgumentException("idx: " + idx); //$NON-NLS-1$
		}
		if (value == null) {
			throw new NullPointerException("value"); //$NON-NLS-1$
		}
		doSetValue(value, 0, 0);
	}
	
}
