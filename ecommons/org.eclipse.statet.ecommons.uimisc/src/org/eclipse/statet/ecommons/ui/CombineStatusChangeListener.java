/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;


public class CombineStatusChangeListener implements StatusChangeListener {
	
	
	private class StackListeners implements StatusChangeListener {
		
		private IStatus status = Status.OK_STATUS;
		
		@Override
		public void statusChanged(final IStatus status) {
			this.status = status;
			update();
		}
		
	}
	
	
	private final StatusChangeListener fParent;
	
	private IStatus fBaseStatus = Status.OK_STATUS;
	
	private final List<StackListeners> fStack= new ArrayList<>();
	
	
	public CombineStatusChangeListener(final StatusChangeListener statusListener) {
		fParent = statusListener;
	}
	
	
	@Override
	public void statusChanged(final IStatus status) {
		fBaseStatus = status;
		update();
	}
	
	private void update() {
		if (fStack.isEmpty()) {
			fParent.statusChanged(fBaseStatus);
			return;
		}
		int severity = 0;
		for (int i = 0; i < fStack.size(); i++) {
			final int s2 = fStack.get(i).status.getSeverity();
			if (s2 > severity) {
				severity = s2;
			}
		}
		System.out.println(severity);
		fParent.statusChanged(new OverlayStatus(fStack.get(fStack.size()-1).status, severity));
	}
	
	public StatusChangeListener newListener() {
		final StackListeners listener = new StackListeners();
		fStack.add(listener);
		return listener;
	}
	
	public void removeListener(final StatusChangeListener listener) {
		fStack.remove(listener);
		update();
	}
	
}
