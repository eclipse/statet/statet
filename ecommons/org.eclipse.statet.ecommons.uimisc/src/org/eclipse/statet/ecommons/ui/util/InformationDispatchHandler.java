/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.bindings.keys.SWTKeySupport;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.keys.IBindingService;

import org.eclipse.statet.internal.ecommons.ui.Messages;


public class InformationDispatchHandler extends AbstractHandler {
	
	
	public static final String COMMAND_ID = "org.eclipse.ui.edit.text.showInformation"; //$NON-NLS-1$
	
	
	public static final int MODE_TOOLTIP=                   1 << 0;
	public static final int MODE_PROPOSAL_INFO=             1 << 1;
	
	public static final int MODE_FOCUS=                     1 << 4;
	
	
	public static final String getTooltipAffordanceString() {
		final IBindingService bindingService= PlatformUI.getWorkbench().getAdapter(IBindingService.class);
		if (bindingService == null) {
			return null;
		}
		
		final String keySequence= bindingService.getBestActiveBindingFormattedFor(InformationDispatchHandler.COMMAND_ID);
		if (keySequence == null) {
			return ""; //$NON-NLS-1$
		}
		
		return NLS.bind(Messages.Hover_TooltipFocusAffordance_message, keySequence);
	}
	
	public static final String getAdditionalInfoAffordanceString() {
		final String keySequence= SWTKeySupport.getKeyFormatterForPlatform().format(SWT.TAB);
		
		return NLS.bind(Messages.Hover_ProposalInfoFocusAffordance_message, keySequence);
	}
	
	public static final String getAffordanceString(final int mode) {
		switch (mode & 0xff) {
		case MODE_TOOLTIP:
			return getTooltipAffordanceString();
		case MODE_PROPOSAL_INFO:
			return getAdditionalInfoAffordanceString();
		default:
			return ""; //$NON-NLS-1$
		}
	}
	
	
	private final ColumnWidgetTokenOwner tokenOwner;
	
	
	/**
	 * Creates a dispatch action.
	 */
	public InformationDispatchHandler(final ColumnWidgetTokenOwner owner) {
		this.tokenOwner= owner;
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		if (this.tokenOwner != null) {
			if (this.tokenOwner.moveFocusToWidgetToken()) {
				return null;
			}
		}
		
		return showInformation();
	}
	
	protected Object showInformation() {
		return null;
	}
	
}
