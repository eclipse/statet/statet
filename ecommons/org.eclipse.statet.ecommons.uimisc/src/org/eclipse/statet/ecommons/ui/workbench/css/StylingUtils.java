/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench.css;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.e4.ui.services.IStylingEngine;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class StylingUtils {
	
	
	public static enum ThemeType {
		
		DARK,
		LIGHT,
		OTHER;
		
	}
	
	
	/**
	 * Key value for setting and getting the CSS ID of a widget.
	 * 
	 * @see org.eclipse.swt.widgets.Widget#getData(String)
	 * @see org.eclipse.swt.widgets.Widget#setData(String, Object)
	 */
	/* org.eclipse.e4.ui.css.swt.CSSSWTConstants.CSS_ID_KEY */
	public static final String WIDGET_CSS_ID_KEY= "org.eclipse.e4.ui.css.id"; //$NON-NLS-1$
	
	
	private static final String SYSTEM_THEME_ID= "org.eclipse.e4.ui.css.theme.e4_system"; //$NON-NLS-1$
	private static final String DEFAULT_THEME_ID= "org.eclipse.e4.ui.css.theme.e4_default"; //$NON-NLS-1$
	private static final String DARK_THEME_ID= "org.eclipse.e4.ui.css.theme.e4_dark"; //$NON-NLS-1$
	
	private static final String NONE_THEME_ID= "none"; //$NON-NLS-1$
	
	
	public static boolean isStylingSupported() {
		return (PlatformUI.getWorkbench().getService(IStylingEngine.class) != null);
	}
	
	public static IStylingEngine getStylingEngine() {
		return nonNullAssert(PlatformUI.getWorkbench().getService(IStylingEngine.class));
	}
	
	
	@SuppressWarnings("restriction")
	private static String getCurrentCssThemeId() {
		try {
			final var engine= PlatformUI.getWorkbench().getService(
					org.eclipse.e4.ui.css.swt.theme.IThemeEngine.class );
			if (engine != null) {
				final var theme= engine.getActiveTheme();
				if (theme != null) {
					return theme.getId();
				}
			}
		}
		catch (final Exception e) {}
		
		return NONE_THEME_ID;
	}
	
	public static ThemeType getCurrentThemeType(final Display display) {
		final var themeId= getCurrentCssThemeId();
		switch (themeId) {
		case SYSTEM_THEME_ID:
		case NONE_THEME_ID:
			return (Display.isSystemDarkTheme()) ? ThemeType.DARK : ThemeType.LIGHT;
		case DARK_THEME_ID:
			return ThemeType.DARK;
		case DEFAULT_THEME_ID:
			return ThemeType.LIGHT;
		default:
			if (themeId.contains("dark")) { //$NON-NLS-1$
				return ThemeType.DARK;
			}
			if (themeId.contains("light")) { //$NON-NLS-1$
				return ThemeType.LIGHT;
			}
			return ThemeType.OTHER;
		}
	}
	
	public static boolean isCurrentThemeMatchingSystem(final Display display) {
		final var themeId= getCurrentCssThemeId();
		switch (themeId) {
		case SYSTEM_THEME_ID:
		case NONE_THEME_ID:
			return true;
		case DEFAULT_THEME_ID:
			return !Display.isSystemDarkTheme();
		default:
			if (themeId.contains("dark")) { //$NON-NLS-1$
				return Display.isSystemDarkTheme();
			}
			return false;
		}
	}
	
	
	public static void disableStyling(final Control control) {
		control.setData("org.eclipse.e4.ui.css.disabled", Boolean.TRUE); //$NON-NLS-1$
	}
	
	
	private StylingUtils() {}
	
}
