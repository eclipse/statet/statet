/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.ecommons.ui.Messages;


@NonNullByDefault
public class DNDUtils {
	
	
	public static abstract class SimpleTextDropAdapter extends DropTargetAdapter {
		
		protected abstract StyledText getTextWidget();
		
		@Override
		public void dragEnter(final DropTargetEvent e) {
			if (e.detail == DND.DROP_DEFAULT && (e.operations & DND.DROP_COPY) != 0) {
				e.detail= DND.DROP_COPY;
			}
		}
		
		@Override
		public void dragOperationChanged(final DropTargetEvent e) {
			if (e.detail == DND.DROP_DEFAULT && (e.operations & DND.DROP_COPY) != 0) {
				e.detail= DND.DROP_COPY;
			}
		}
		
		@Override
		public void dragOver(final DropTargetEvent event) {
			event.feedback= DND.FEEDBACK_SCROLL | DND.FEEDBACK_SELECT;
		}
		
		@Override
		public void drop(final DropTargetEvent e) {
			getTextWidget().insert((String)e.data);
		}
	}
	
	
	public static boolean setContent(final Clipboard clipboard, final Object[] datas,
			final @NonNull Transfer[] tranfers) {
		while (true) {
			try {
				clipboard.setContents(datas, tranfers);
				return true;
			}
			catch (final SWTError e) {
				if (e.code != DND.ERROR_CANNOT_SET_CLIPBOARD) {
					throw e;
				}
				
				if (!MessageDialog.openQuestion(
						UIAccess.getActiveWorkbenchShell(true),
						Messages.CopyToClipboard_error_title,
						Messages.CopyToClipboard_error_message )) {
					return false;
				}
			}
		}
	}
	
	/**
	 * for common options (DEFAULT, MOVE, COPY).
	 * 
	 * @param viewer
	 * @param listener
	 * @param transferTypes
	 */
	public static void addDropSupport(final Control control, final DropTargetListener listener,
			final @NonNull Transfer[] transferTypes) {
		addDropSupport(control, ImCollections.newList(listener),
				DND.DROP_DEFAULT | DND.DROP_MOVE | DND.DROP_COPY,
				transferTypes );
	}
	
	public static void addDropSupport(final Control control, final ImList<DropTargetListener> listeners,
			final int operations, final @NonNull Transfer[] transferTypes) {
		final DropTarget dropTarget= new DropTarget(control, operations);
		dropTarget.setTransfer(transferTypes);
		for (final DropTargetListener listener : listeners) {
			dropTarget.addDropListener(listener);
		}
	}
	
	
	private DNDUtils() {}
	
}
