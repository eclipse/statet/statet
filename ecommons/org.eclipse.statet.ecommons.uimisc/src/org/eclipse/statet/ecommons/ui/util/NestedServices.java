/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.internal.contexts.NestableContextService;
import org.eclipse.ui.internal.handlers.LegacyHandlerService;
import org.eclipse.ui.internal.services.IServiceLocatorCreator;
import org.eclipse.ui.internal.services.IWorkbenchLocationService;
import org.eclipse.ui.internal.services.ServiceLocator;
import org.eclipse.ui.internal.services.WorkbenchLocationService;
import org.eclipse.ui.services.IDisposable;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.ui.services.IServiceScopes;


/**
 * Util to create a nested service locator with nested services, e.g. for dialogs.
 * 
 * Workaround for E-bug #142226
 */
public class NestedServices implements Listener {
	
	
	public static class Dialog extends NestedServices {
		
		public Dialog(final Shell shell) {
			super("Dialog", PlatformUI.getWorkbench());
			
			registerService(IWorkbenchLocationService.class, new WorkbenchLocationService(
					IServiceScopes.DIALOG_SCOPE,
					PlatformUI.getWorkbench(), null, null, null, null, 1 ));
			
			initializeDefaultServices();
			
			final IContextService contextService= getLocator()
					.getService(IContextService.class);
			contextService.registerShell(shell, IContextService.TYPE_DIALOG);
			
			bindTo(shell);
		}
		
	}
	
	
	private final String name;
	
	private IEclipseContext context;
	private ServiceLocator serviceLocator;
	
	private boolean isActivated;
	
	
	public NestedServices(final IServiceLocator parent, final String name) {
		this(name, parent);
		
		initializeDefaultServices();
	}
	
	protected NestedServices(final String name, final IServiceLocator parent) {
		this.name= name;
		final IServiceLocatorCreator slc= parent.getService(IServiceLocatorCreator.class);
		final IEclipseContext parentContext= parent.getService(IEclipseContext.class);
		this.context= parentContext.createChild(name);
		this.serviceLocator= (ServiceLocator) slc.createServiceLocator(parent, null,
				new IDisposable() {
					@Override
					public void dispose() {
						clear();
					}
				}, this.context);
		if (this.serviceLocator == null) {
			throw new RuntimeException("Could not create nested service locator.");
		}
	}
	
	
	protected <T> void registerService(final Class<T> api, final T service) {
		this.serviceLocator.registerService(api, service);
	}
	
	protected void initializeDefaultServices() {
		registerService(IContextService.class, new NestableContextService(
				this.context.getParent().get(IContextService.class), null ));
		registerService(IHandlerService.class, new LegacyHandlerService(
				this.context, null ));
	}
	
	public void dispose() {
		if (this.serviceLocator != null) {
			this.serviceLocator.dispose();
			clear();
		}
	}
	
	private void clear() {
		if (this.serviceLocator != null) {
			this.serviceLocator= null;
			this.context.dispose();
			this.context= null;
		}
	}
	
	
	public IServiceLocator getLocator() {
		return this.serviceLocator;
	}
	
	public IEclipseContext getContext() {
		return this.context;
	}
	
	/**
	 * Binds the services to the specified composite of controls.
	 */
	public void bindTo(final Control control) {
		control.addListener(SWT.Activate, this);
		control.addListener(SWT.FocusIn, this);
		control.addListener(SWT.Deactivate, this);
		control.addListener(SWT.Dispose, this);
	}
	
	/**
	 * Binds the services to the specified focus control.
	 */
	public void bindToFocus(final Control control) {
		control.addListener(SWT.Activate, this);
		control.addListener(SWT.FocusIn, this);
		control.addListener(SWT.FocusOut, this);
		control.addListener(SWT.Dispose, this);
	}
	
	@Override
	@SuppressWarnings("restriction")
	public void handleEvent(final Event event) {
		switch (event.type) {
		case SWT.Activate:
		case SWT.FocusIn:
			if (this.serviceLocator != null && !this.isActivated) {
				activate(event);
			}
			break;
		case SWT.Deactivate:
		case SWT.FocusOut:
			if (this.serviceLocator != null && this.isActivated) {
				deactivate(event);
			}
			break;
		case SWT.Dispose:
			dispose();
			break;
		default:
			break;
		}
	}
	
	
	protected void activate(final Event event) {
		this.isActivated= true;
		this.context.activate();
		event.display.asyncExec(new Runnable() {
			@Override
			public void run() {
				if (NestedServices.this.serviceLocator != null && NestedServices.this.isActivated) {
					NestedServices.this.context.processWaiting();
					NestedServices.this.serviceLocator.activate();
				}
			}
		});
	}
	
	protected void deactivate(final Event event) {
		this.isActivated= false;
		this.serviceLocator.deactivate();
		this.context.deactivate();
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder("NestedServices");
		sb.append(" '").append(this.name).append("'");
		if (this.serviceLocator == null) {
			sb.append(" (disposed)");
		}
		return sb.toString();
	}
	
}
