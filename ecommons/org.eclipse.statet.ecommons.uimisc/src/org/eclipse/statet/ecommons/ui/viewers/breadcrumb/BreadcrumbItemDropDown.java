/*=============================================================================#
 # Copyright (c) 2008, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers.breadcrumb;

import java.util.Objects;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.util.OpenStrategy;
import org.eclipse.jface.viewers.IOpenListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.OpenEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.accessibility.AccessibleAdapter;
import org.eclipse.swt.accessibility.AccessibleEvent;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.swt.AccessibleArrowImage;
import org.eclipse.statet.ecommons.ui.workbench.css.StylingUtils;


/**
 * The part of the breadcrumb item with the drop down menu.
 */
@NonNullByDefault
class BreadcrumbItemDropDown {
	
	/**
	 * Tells whether this class is in debug mode.
	 */
	private static boolean DEBUG= "true".equalsIgnoreCase(Platform.getDebugOption("org.eclipse.jdt.ui/debug/BreadcrumbItemDropDown")); //$NON-NLS-1$//$NON-NLS-2$
	
	private static final boolean IS_MAC_WORKAROUND= "carbon".equals(SWT.getPlatform()); //$NON-NLS-1$
	
	private static final int DROP_DOWN_HIGHT= 300;
	private static final int DROP_DOWN_WIDTH= 500;
	
	private final BreadcrumbItem parent;
	private final Composite parentComposite;
	private final ToolBar toolBar;
	
	private boolean isMenuShown;
	private boolean isEnabled;
	private Shell shell;
	private TreeViewer dropDownViewer;
	
	private MenuManager dropDownMenuManager;
	
	
	public BreadcrumbItemDropDown(final BreadcrumbItem parent, final Composite composite) {
		this.parent= parent;
		this.parentComposite= composite;
		this.isMenuShown= false;
		this.isEnabled= true;
		
		{	final var toolBar= new ToolBar(composite, SWT.FLAT);
			
			toolBar.setData(StylingUtils.WIDGET_CSS_ID_KEY, "BreadcrumbItemDropDownToolBar"); //$NON-NLS-1$
			
			toolBar.getAccessible().addAccessibleListener(new AccessibleAdapter() {
				@Override
				public void getName(final AccessibleEvent e) {
					e.result= BreadcrumbMessages.BreadcrumbItemDropDown_showDropDownMenu_action_tooltip;
				}
			});
			
			toolBar.setLayoutData(new GridData(SWT.END, SWT.CENTER, false, false));
			this.toolBar= toolBar;
		}
		final ToolBarManager manager= new ToolBarManager(this.toolBar);
		
		final Action showDropDownMenuAction= new Action(null, SWT.NONE) {
			@Override
			public void run() {
				Shell shell= BreadcrumbItemDropDown.this.parent.getDropDownShell();
				if (shell != null) {
					return;
				}
				
				shell= BreadcrumbItemDropDown.this.parent.getViewer().getDropDownShell();
				if (shell != null) {
					shell.close();
				}
				
				showMenu();
				
				BreadcrumbItemDropDown.this.shell.setFocus();
			}
		};
		
		showDropDownMenuAction.setImageDescriptor(new AccessibleArrowImage(
				isLTR() ? SWT.RIGHT : SWT.LEFT, AccessibleArrowImage.DEFAULT_SIZE, 
				composite.getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND).getRGB(),
				composite.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND).getRGB() ));
		showDropDownMenuAction.setToolTipText(BreadcrumbMessages.BreadcrumbItemDropDown_showDropDownMenu_action_tooltip);
		manager.add(showDropDownMenuAction);
		
		manager.update(true);
		if (IS_MAC_WORKAROUND) {
			manager.getControl().addMouseListener(new MouseAdapter() {
				// see also BreadcrumbItemDetails#addElementListener(Control)
				@Override
				public void mouseDown(final MouseEvent e) {
					showDropDownMenuAction.run();
				}
			});
		}
		
		this.dropDownMenuManager= new MenuManager();
		this.dropDownMenuManager.setRemoveAllWhenShown(true);
		this.dropDownMenuManager.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(final IMenuManager manager) {
				final IStructuredSelection selection= (IStructuredSelection) BreadcrumbItemDropDown.this.dropDownViewer.getSelection();
				if (!selection.isEmpty()) {
					BreadcrumbItemDropDown.this.parent.getViewer().fillDropDownContextMenu(manager, selection.getFirstElement());
				}
			}
		});
	}
	
	
	/**
	 * Return the width of this element.
	 * 
	 * @return the width of this element
	 */
	public int getWidth() {
		return this.toolBar.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
	}
	
	/**
	 * Set whether the drop down menu is available.
	 * 
	 * @param enabled true if available
	 */
	public void setEnabled(final boolean enabled) {
		this.isEnabled= enabled;
		
		this.toolBar.setVisible(enabled);
	}
	
	/**
	 * Tells whether the menu is shown.
	 *
	 * @return true if the menu is open
	 */
	public boolean isMenuShown() {
		return this.isMenuShown;
	}
	
	/**
	 * Returns the shell used for the drop down menu if it is shown.
	 * 
	 * @return the drop down shell or <code>null</code>
	 */
	public @Nullable Shell getDropDownShell() {
		if (!isMenuShown()) {
			return null;
		}
		
		return this.shell;
	}
	
	/**
	 * Returns the drop down selection provider.
	 * 
	 * @return the selection provider of the drop down if {@link #isMenuShown()}, <code>null</code>
	 *     otherwise
	 */
	public @Nullable ISelectionProvider getDropDownSelectionProvider() {
		if (!this.isMenuShown) {
			return null;
		}
		
		return this.dropDownViewer;
	}
	
	/**
	 * Opens the drop down menu.
	 */
	public void showMenu() {
		if (DEBUG) {
			System.out.println("BreadcrumbItemDropDown.showMenu()"); //$NON-NLS-1$
		}
		
		if (!this.isEnabled || this.isMenuShown) {
			return;
		}
		
		this.shell= new Shell(this.toolBar.getShell(), SWT.RESIZE | SWT.TOOL | SWT.ON_TOP);
		if (DEBUG) {
			System.out.println("	creating new shell"); //$NON-NLS-1$
		}
		
		try {
			this.isMenuShown= true;
			
			final GridLayout layout= new GridLayout(1, false);
			layout.marginHeight= 0;
			layout.marginWidth= 0;
			this.shell.setLayout(layout);
			
			final Composite composite= new Composite(this.shell, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			final GridLayout gridLayout= new GridLayout(1, false);
			gridLayout.marginHeight= 0;
			gridLayout.marginWidth= 0;
			composite.setLayout(gridLayout);
			
			this.dropDownViewer= createViewer(composite);
			this.dropDownViewer.setUseHashlookup(true);
			
			final Tree tree= (Tree) this.dropDownViewer.getControl();
			tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			
			final Object input= this.parent.getElement();
			this.parent.getViewer().configureDropDownViewer(this.dropDownViewer, input);
			this.dropDownViewer.setInput(input);
			
			setShellBounds(this.shell);
			
			this.dropDownViewer.addOpenListener(new IOpenListener() {
				
				@Override
				public void open(final OpenEvent event) {
					if (DEBUG) {
						System.out.println("BreadcrumbItemDropDown.showMenu()$treeViewer>open"); //$NON-NLS-1$
					}
					
					final ISelection selection= event.getSelection();
					if (!(selection instanceof IStructuredSelection)) {
						return;
					}
					
					final Object element= ((IStructuredSelection) selection).getFirstElement();
					if (element == null) {
						return;
					}
					
					openElement(element);
				}
				
			});
			
			tree.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseUp(final MouseEvent e) {
					if (DEBUG) {
						System.out.println("BreadcrumbItemDropDown.showMenu()$treeViewer>mouseUp"); //$NON-NLS-1$
					}
					
					if (e.button != 1) {
						return;
					}
					
					if ((OpenStrategy.getOpenMethod() & OpenStrategy.SINGLE_CLICK) != 0) {
						return;
					}
					
					final var item= tree.getItem(new Point(e.x, e.y));
					if (item == null) {
						return;
					}
					
					openElement(item.getData());
				}
				
				@Override
				public void mouseDown(final MouseEvent e) {
				}
				
				@Override
				public void mouseDoubleClick(final MouseEvent e) {
				}
				
			});
			
			tree.addMouseMoveListener(new MouseMoveListener() {
				
				@Nullable TreeItem lastItem= null;
				
				@Override
				public void mouseMove(final MouseEvent e) {
					if (tree.equals(e.getSource())) {
						final TreeItem currentItem= tree.getItem(new Point(e.x, e.y));
						if (!Objects.equals(currentItem, this.lastItem)) {
							final boolean showHandPointer= (currentItem != null
									&& BreadcrumbItemDropDown.this.parent.getViewer().canOpen(currentItem));
							tree.setCursor((showHandPointer) ?
									tree.getDisplay().getSystemCursor(SWT.CURSOR_HAND) :
									null );
						}
						if (currentItem != null) {
							final Rectangle clientArea= tree.getClientArea();
							if (!currentItem.equals(this.lastItem)) {
								this.lastItem= currentItem;
								tree.setSelection(new TreeItem[] { this.lastItem });
							}
							else if (e.y - clientArea.y < tree.getItemHeight() / 4) {
								// Scroll up
								if (currentItem.getParentItem() == null) {
									final int index= tree.indexOf(currentItem);
									if (index < 1) {
										return;
									}
									
									this.lastItem= tree.getItem(index - 1);
									tree.setSelection(new TreeItem[] { this.lastItem });
								}
								else {
									final Point p= tree.toDisplay(e.x, e.y);
									final Item item= BreadcrumbItemDropDown.this.dropDownViewer.scrollUp(p.x, p.y);
									if (item instanceof TreeItem) {
										this.lastItem= (TreeItem)item;
										tree.setSelection(new TreeItem[] { this.lastItem });
									}
								}
							}
							else if (clientArea.y + clientArea.height - e.y < tree.getItemHeight() / 4) {
								// Scroll down
								if (currentItem.getParentItem() == null) {
									final int index= tree.indexOf(currentItem);
									if (index >= tree.getItemCount() - 1) {
										return;
									}
									
									this.lastItem= tree.getItem(index + 1);
									tree.setSelection(new TreeItem[] { this.lastItem });
								}
								else {
									final Point p= tree.toDisplay(e.x, e.y);
									final Item item= BreadcrumbItemDropDown.this.dropDownViewer.scrollDown(p.x, p.y);
									if (item instanceof TreeItem) {
										this.lastItem= (TreeItem)item;
										tree.setSelection(new TreeItem[] { this.lastItem });
									}
								}
							}
						}
						else {
							this.lastItem= null;
						}
					}
				}
				
			});
			
			tree.addKeyListener(new KeyListener() {
				
				@Override
				public void keyPressed(final KeyEvent e) {
					if (e.keyCode == SWT.ARROW_UP) {
						final TreeItem[] selection= tree.getSelection();
						if (selection.length != 1) {
							return;
						}
						
						final int selectionIndex= tree.indexOf(selection[0]);
						if (selectionIndex != 0) {
							return;
						}
						
						BreadcrumbItemDropDown.this.shell.close();
					}
				}
				
				@Override
				public void keyReleased(final KeyEvent e) {
				}
				
			});
			
			tree.setMenu(this.dropDownMenuManager.createContextMenu(tree));
			
			this.dropDownViewer.addTreeListener(new ITreeViewerListener() {
				
				@Override
				public void treeCollapsed(final TreeExpansionEvent event) {
				}
				
				@Override
				public void treeExpanded(final TreeExpansionEvent event) {
					tree.setRedraw(false);
					BreadcrumbItemDropDown.this.shell.getDisplay().asyncExec(new Runnable() {
						@Override
						public void run() {
							if (BreadcrumbItemDropDown.this.shell.isDisposed()) {
								return;
							}
							
							try {
								resizeShell(BreadcrumbItemDropDown.this.shell);
							}
							finally {
								tree.setRedraw(true);
							}
						}
					});
				}
				
			});
			
			final int index= this.parent.getViewer().getIndexOfItem(this.parent);
			if (index < this.parent.getViewer().getItemCount() - 1) {
				final var childItem= this.parent.getViewer().getItem(index + 1);
				final Object childElement= childItem.getElement();
				
				this.dropDownViewer.setSelection(new StructuredSelection(childElement), true);
				
				final TreeItem[] selection= tree.getSelection();
				if (selection.length > 0) {
					tree.setTopItem(selection[0]);
				}
			}
			
			this.shell.setVisible(true);
			installCloser(this.shell);
		}
		catch (final RuntimeException e) {
			this.isMenuShown= false;
			this.shell.close();
			throw e;
		}
	}
	
	protected TreeViewer createViewer(final Composite composite) {
		return new TreeViewer(composite, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
	}
	
	private void openElement(final @Nullable Object data) {
		if (data == null) {
			return;
		}
		
		// This might or might not open an editor
		this.parent.getViewer().fireMenuSelection(data);
		
		final boolean treeHasFocus= !this.shell.isDisposed() && this.dropDownViewer.getTree().isFocusControl();
		
		if (DEBUG) {
			System.out.println("	isDisposed: " + this.shell.isDisposed()); //$NON-NLS-1$
			System.out.println("	shell hasFocus: " + (!this.shell.isDisposed() && this.shell.isFocusControl())); //$NON-NLS-1$
			System.out.println("	tree hasFocus: " + treeHasFocus); //$NON-NLS-1$
		}
		
		if (this.shell.isDisposed()) {
			return;
		}
		
		if (!treeHasFocus) {
			this.shell.close();
			return;
		}
		
		toggleExpansionState(data);
	}
	
	private void toggleExpansionState(final Object element) {
		final Tree tree= this.dropDownViewer.getTree();
		if (this.dropDownViewer.getExpandedState(element)) {
			this.dropDownViewer.collapseToLevel(element, 1);
		}
		else {
			tree.setRedraw(false);
			try {
				this.dropDownViewer.expandToLevel(element, 1);
				resizeShell(this.shell);
			}
			finally {
				tree.setRedraw(true);
			}
		}
	}
	
	/**
	 * The closer closes the given shell when the focus is lost.
	 * 
	 * @param shell the shell to install the closer to
	 */
	private void installCloser(final Shell shell) {
		final Listener focusListener= new Listener() {
			@Override
			public void handleEvent(final Event event) {
				final Widget focusElement= event.widget;
				final boolean isFocusBreadcrumbTreeFocusWidget= focusElement == shell || focusElement instanceof Tree && ((Tree)focusElement).getShell() == shell;
				final boolean isFocusWidgetParentShell= focusElement instanceof Control && ((Control)focusElement).getShell().getParent() == shell;
				
				switch (event.type) {
					case SWT.FocusIn:
						if (DEBUG) {
							System.out.println("focusIn - is breadcrumb tree: " + isFocusBreadcrumbTreeFocusWidget); //$NON-NLS-1$
						}
						
						if (!isFocusBreadcrumbTreeFocusWidget && !isFocusWidgetParentShell) {
							if (DEBUG) {
								System.out.println("==> closing shell since focus in other widget"); //$NON-NLS-1$
							}
							shell.close();
						}
						break;
						
					case SWT.FocusOut:
						if (DEBUG) {
							System.out.println("focusOut - is breadcrumb tree: " + isFocusBreadcrumbTreeFocusWidget); //$NON-NLS-1$
						}
						
						if (event.display.getActiveShell() == null) {
							if (DEBUG) {
								System.out.println("==> closing shell since event.display.getActiveShell() == null"); //$NON-NLS-1$
							}
							shell.close();
						}
						break;
						
					default:
						Assert.isTrue(false);
				}
			}
		};
		
		final Display display= shell.getDisplay();
		display.addFilter(SWT.FocusIn, focusListener);
		display.addFilter(SWT.FocusOut, focusListener);
		
		final ControlListener controlListener= new ControlListener() {
			
			@Override
			public void controlMoved(final ControlEvent e) {
				shell.close();
			}
			
			@Override
			public void controlResized(final ControlEvent e) {
				shell.close();
			}
			
		};
		this.toolBar.getShell().addControlListener(controlListener);
		
		shell.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				if (DEBUG) {
					System.out.println("==> shell disposed"); //$NON-NLS-1$
				}
				
				display.removeFilter(SWT.FocusIn, focusListener);
				display.removeFilter(SWT.FocusOut, focusListener);
				
				if (!BreadcrumbItemDropDown.this.toolBar.isDisposed()) {
					BreadcrumbItemDropDown.this.toolBar.getShell().removeControlListener(controlListener);
				}
			}
			
		});
		shell.addShellListener(new ShellListener() {
			
			@Override
			public void shellActivated(final ShellEvent e) {
			}
			
			@Override
			public void shellClosed(final ShellEvent e) {
				if (DEBUG) {
					System.out.println("==> shellClosed"); //$NON-NLS-1$
				}
				
				if (!BreadcrumbItemDropDown.this.isMenuShown) {
					return;
				}
				
				BreadcrumbItemDropDown.this.isMenuShown= false;
				BreadcrumbItemDropDown.this.dropDownViewer= null;
			}
			
			@Override
			public void shellDeactivated(final ShellEvent e) {
			}
			
			@Override
			public void shellDeiconified(final ShellEvent e) {
			}
			
			@Override
			public void shellIconified(final ShellEvent e) {
			}
			
		});
	}
	
	/**
	 * Calculates a useful size for the given shell.
	 * 
	 * @param shell the shell to calculate the size for.
	 */
	private void setShellBounds(final Shell shell) {
		final Rectangle rect= this.parentComposite.getBounds();
		final Rectangle toolbarBounds= this.toolBar.getBounds();
		
		shell.pack();
		final Point size= shell.getSize();
		final int height= Math.min(size.y, DROP_DOWN_HIGHT);
		final int width= Math.max(Math.min(size.x, DROP_DOWN_WIDTH), 250);
		
		int imageBoundsX= 0;
		if (this.dropDownViewer.getTree().getItemCount() > 0) {
			final TreeItem item= this.dropDownViewer.getTree().getItem(0);
			imageBoundsX= item.getImageBounds(0).x;
		}
		
		final Rectangle trim= this.shell.computeTrim(0, 0, width, height);
		int x= toolbarBounds.x + toolbarBounds.width + 2 + trim.x - imageBoundsX;
		if (!isLTR()) {
			x+= width;
		}
		
		Point pt= new Point(x, rect.y + rect.height);
		pt= this.parentComposite.toDisplay(pt);
		
		final Rectangle monitor= org.eclipse.jface.util.Util.getClosestMonitor(
				shell.getDisplay(), pt ).getClientArea();
		final int overlap= (pt.x + width) - (monitor.x + monitor.width);
		if (overlap > 0) {
			pt.x-= overlap;
		}
		if (pt.x < monitor.x) {
			pt.x= monitor.x;
		}
		
		shell.setLocation(pt);
		shell.setSize(width, height);
	}
	
	/**
	 * Set the size of the given shell such that more content can be shown. The shell size does not
	 * exceed {@link #DROP_DOWN_HIGHT} and {@link #DROP_DOWN_WIDTH}.
	 * 
	 * @param shell the shell to resize
	 */
	private void resizeShell(final Shell shell) {
		final Point size= shell.getSize();
		final int currentWidth= size.x;
		final int currentHeight= size.y;
		
		if (currentHeight >= DROP_DOWN_HIGHT && currentWidth >= DROP_DOWN_WIDTH) {
			return;
		}
		
		final Point preferedSize= shell.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
		
		int newWidth;
		if (currentWidth >= DROP_DOWN_WIDTH) {
			newWidth= currentWidth;
		}
		else {
			newWidth= Math.min(Math.max(preferedSize.x, currentWidth), DROP_DOWN_WIDTH);
		}
		int newHeight;
		if (currentHeight >= DROP_DOWN_HIGHT) {
			newHeight= currentHeight;
		}
		else {
			newHeight= Math.min(Math.max(preferedSize.y, currentHeight), DROP_DOWN_HIGHT);
		}
		
		if (newHeight != currentHeight || newWidth != currentWidth) {
			shell.setRedraw(false);
			try {
				shell.setSize(newWidth, newHeight);
				if (!isLTR()) {
					final Point location= shell.getLocation();
					shell.setLocation(location.x - (newWidth - currentWidth), location.y);
				}
			}
			finally {
				shell.setRedraw(true);
			}
		}
	}
	
	/**
	 * Tells whether this the breadcrumb is in LTR or RTL mode.
	 * 
	 * @return <code>true</code> if the breadcrumb in left-to-right mode, <code>false</code>
	 *         otherwise
	 */
	private boolean isLTR() {
		return (this.parentComposite.getStyle() & SWT.RIGHT_TO_LEFT) == 0;
	}
	
}
