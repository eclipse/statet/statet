/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import java.util.List;

import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.ui.components.ButtonGroup;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils;


public class ManageBookmarksDialog extends TrayDialog {
	
	
	private TableViewer tableViewer;
	
	private ButtonGroup<BrowserBookmark> buttons;
	
	private final PageBookBrowserView view;
	private final List<BrowserBookmark> bookmarks;
	
	
	protected ManageBookmarksDialog(final PageBookBrowserView view) {
		super(view.getViewSite().getShell());
		this.view= view;
		this.bookmarks= view.getBookmarks();
		
		create();
	}
	
	
	@Override
	protected void configureShell(final Shell shell) {
		shell.setText("Bookmarks");
		
		super.configureShell(shell);
	}
	
	@Override
	public boolean isHelpAvailable() {
		return false;
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}
	
	@Override
	protected Control createContents(final Composite parent) {
		final Control control= super.createContents(parent);
		
		this.buttons.updateState();
		
		return control;
	}
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(LayoutUtils.newDialogGrid(2));
		
		{	this.tableViewer= new TableViewer(composite);
			this.tableViewer.setUseHashlookup(true);
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
			gd.widthHint= LayoutUtils.hintWidth(this.tableViewer.getTable(), 60);
			gd.heightHint= LayoutUtils.hintHeight(this.tableViewer.getTable(), 20);
			this.tableViewer.getControl().setLayoutData(gd);
			
			this.tableViewer.setLabelProvider(new LabelProvider());
		}
		
		this.buttons= new ButtonGroup<>(composite) {
			@Override
			protected BrowserBookmark edit1(final BrowserBookmark item, final boolean newItem, final Object parent) {
				final EditBookmarkDialog dialog= new EditBookmarkDialog(getShell(), item);
				if (dialog.open() == Dialog.OK) {
					return dialog.getBookmark();
				}
				return null;
			}
			@Override
			public void updateState() {
				super.updateState();
				getButton(IDialogConstants.OPEN_ID).setEnabled(
						((IStructuredSelection) ManageBookmarksDialog.this.tableViewer.getSelection()).size() == 1);
			}
		};
		this.buttons.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		this.buttons.addAddButton(null);
		this.buttons.addEditButton(null);
		this.buttons.addDeleteButton(null);
		this.buttons.addSeparator();
		this.buttons.addUpButton(null);
		this.buttons.addDownButton(null);
		
		final IObservableList<BrowserBookmark> writableList= new WritableList<>(this.bookmarks, BrowserBookmark.class);
		this.tableViewer.setContentProvider(new ObservableListContentProvider());
		this.tableViewer.setInput(writableList);
		this.buttons.connectTo(this.tableViewer, writableList, null);
		ViewerUtils.scheduleStandardSelection(this.tableViewer);
		
		applyDialogFont(composite);
		return composite;
	}
	
	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		createButton(parent, IDialogConstants.OPEN_ID, IDialogConstants.OPEN_LABEL, false);
		createButton(parent, IDialogConstants.CLOSE_ID, IDialogConstants.CLOSE_LABEL, true);
	}
	
	@Override
	protected void buttonPressed(final int buttonId) {
		if (buttonId == IDialogConstants.OPEN_ID) {
			final BrowserBookmark bookmark= (BrowserBookmark)
					((IStructuredSelection) this.tableViewer.getSelection()).getFirstElement();
			this.view.openBookmark(bookmark, this.view.getCurrentSession());
			close();
		}
		if (buttonId == IDialogConstants.CLOSE_ID) {
			close();
		}
	}
	
}
