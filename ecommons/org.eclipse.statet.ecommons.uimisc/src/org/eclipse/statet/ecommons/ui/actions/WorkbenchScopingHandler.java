/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.actions;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.HandlerEvent;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.UIElement;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


@NonNullByDefault
public abstract class WorkbenchScopingHandler extends AbstractHandler
		implements IWindowListener, IHandlerListener, IElementUpdater {
	
	
	private @Nullable String commandId;
	
	private Map<Object, AbstractScopeHandler> scopeHandlers= new IdentityHashMap<>();
	
	private @Nullable AbstractScopeHandler activeHandler;
	
	
	public WorkbenchScopingHandler() {
		init();
	}
	
	public WorkbenchScopingHandler(final String commandId) {
		init();
		
		this.commandId= commandId;
	}
	
	
	private void init() {
		final IWorkbench workbench= PlatformUI.getWorkbench();
		workbench.addWindowListener(this);
	}
	
	/** @see IExecutableExtension#setInitializationData(IConfigurationElement, String, Object) */
	public void setInitializationData(final IConfigurationElement config,
			final String propertyName, final @Nullable Object data) throws CoreException {
		{	final String s= config.getAttribute("commandId"); //$NON-NLS-1$
			if (s != null) {
				this.commandId= s.intern();
			}
		}
	}
	
	@Override
	public void dispose() {
		final IWorkbench workbench= PlatformUI.getWorkbench();
		workbench.removeWindowListener(this);
		
		final Map<Object, AbstractScopeHandler> handlers;
		synchronized (this.scopeHandlers) {
			handlers= this.scopeHandlers;
			this.scopeHandlers= Collections.emptyMap();
		}
		for (final Entry<Object, AbstractScopeHandler> entry : handlers.entrySet()) {
			disposeScope(entry.getKey(), entry.getValue());
		}
		
		super.dispose();
	}
	
	
	@Override
	public void windowOpened(final IWorkbenchWindow window) {
	}
	
	@Override
	public void windowClosed(final IWorkbenchWindow window) {
		removeScope(window);
	}
	
	@Override
	public void windowActivated(final IWorkbenchWindow window) {
	}
	
	@Override
	public void windowDeactivated(final IWorkbenchWindow window) {
	}
	
	
	protected @Nullable String getCommandId() {
		return this.commandId;
	}
	
	protected @Nullable AbstractScopeHandler getActiveHandler() {
		return this.activeHandler;
	}
	
	
	protected @Nullable Object getScope(final @Nullable IServiceLocator serviceLocator) {
		if (serviceLocator != null) {
			return serviceLocator.getService(IWorkbenchWindow.class);
		}
		return null;
	}
	
	protected @Nullable Object getScope(final IEvaluationContext context) {
		Object o= context.getVariable(IWorkbenchWindow.class.getName());
		if (o == null || o == IEvaluationContext.UNDEFINED_VARIABLE) {
			o= PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		}
		if (o instanceof IWorkbenchWindow) {
			return o;
		}
		return null;
	}
	
	private @Nullable AbstractScopeHandler getScopeHandler(final @Nullable Object scope) {
		AbstractScopeHandler handler= null;
		if (scope != null) {
			synchronized (this.scopeHandlers) {
				handler= this.scopeHandlers.get(scope);
				if (handler == null) {
					handler= createScopeHandler(scope);
					handler.addHandlerListener(this);
					this.scopeHandlers.put(scope, handler);
				}
			}
		}
		return handler;
	}
	
	private void removeScope(final Object scope) {
		final AbstractScopeHandler handler;
		synchronized (this.scopeHandlers) {
			handler= this.scopeHandlers.remove(scope);
		}
		if (handler != null) {
			handler.dispose();
		}
	}
	
	protected abstract AbstractScopeHandler createScopeHandler(Object scope);
	
	protected void disposeScope(final Object scope, final AbstractScopeHandler handler) {
		handler.dispose();
	}
	
	
	@Override
	public synchronized void setEnabled(final @Nullable Object appContext) {
		if (appContext instanceof final IEvaluationContext evalContext) {
			final AbstractScopeHandler handler= getScopeHandler(getScope(evalContext));
			this.activeHandler= handler;
			if (handler != null) {
				handler.setEnabled(evalContext);
				setBaseEnabled(handler.isEnabled());
				return;
			}
		}
		
		setBaseEnabled(false);
	}
	
	@Override
	public synchronized void handlerChanged(final HandlerEvent handlerEvent) {
		final AbstractScopeHandler handler= (AbstractScopeHandler)handlerEvent.getHandler();
		synchronized (this) {
			if (handler == this.activeHandler) {
				setBaseEnabled(handler.isEnabled());
				return;
			}
		}
		
		handler.refreshCommandElements();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void updateElement(final UIElement element,
			@SuppressWarnings("rawtypes") final Map parameters) {
		final AbstractScopeHandler handler= getScopeHandler(getScope(element.getServiceLocator()));
		if (handler instanceof IElementUpdater) {
			WorkbenchUIUtils.aboutToUpdateCommandsElements(this, handler, element);
			try {
				handler.updateCommandElement(element, parameters);
			}
			finally {
				WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
			}
		}
	}
	
	@Override
	public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
		final Object appContext= event.getApplicationContext();
		if (appContext instanceof final IEvaluationContext evalContext) {
			final AbstractScopeHandler handler= getScopeHandler(getScope(evalContext));
			if (handler != null) {
				synchronized (this) {
					handler.setEnabled(evalContext);
					if (!handler.isEnabled()) {
						return null;
					}
				}
				return handler.execute(event, evalContext);
			}
		}
		return null;
	}
	
}
