/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.Geometry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


public class ToolPopup {
	
	
	private class SWTListener implements Listener {
		
		@Override
		public void handleEvent(final Event event) {
			switch (event.type) {
			case SWT.Deactivate:
				if (ToolPopup.this.ignoreActivation == 0) {
					close();
				}
				return;
			case SWT.Dispose:
				dispose();
				return;
			case SWT.Selection:
				if (event.widget == ToolPopup.this.okButton) {
					onOK();
					close();
					return;
				}
				if (event.widget == ToolPopup.this.cancelButton) {
					close();
					return;
				}
				if (event.widget == ToolPopup.this.tabFolder) {
					tabSelected(getTab(ToolPopup.this.tabFolder.getSelection()));
					return;
				}
			}
		}
		
	}
	
	public static class ToolTab {
		
		
		private final String key;
		
		private final ToolPopup parent;
		private final CTabItem tabItem;
		private Composite composite;
		
		
		public ToolTab(final String key, final ToolPopup parent,
				final String name, final String tooltip) {
			this.key= key;
			this.parent= parent;
			this.tabItem= new CTabItem(parent.tabFolder, SWT.NONE);
			this.tabItem.setText(name);
			this.tabItem.setToolTipText(tooltip);
			parent.toolTabs.add(this);
		}
		
		
		public ToolPopup getParent() {
			return this.parent;
		}
		public CTabItem getTabItem() {
			return this.tabItem;
		}
		
		protected Composite create() {
			final Composite composite= new Composite(this.parent.getTabFolder(), SWT.NONE);
			this.tabItem.setControl(composite);
			composite.setBackground(getParent().getGraphicBackgroundColor());
			return composite;
		}
		
		protected void activated() {
		}
		
		protected void performOK() {
			this.parent.onOK();
			this.parent.close();
		}
		
	}
	
	protected static abstract class PreviewCanvas extends Canvas implements PaintListener {
		
		
		private static final int DEFAULT_WIDTH= 50;
		
		
		public PreviewCanvas(final Composite parent) {
			super(parent, SWT.NONE);
			
			addPaintListener(this);
			setLayoutData(createGD());
		}
		
		@Override
		public void paintControl(final PaintEvent e) {
			final GC gc= e.gc;
			gc.setForeground(e.display.getSystemColor(SWT.COLOR_DARK_GRAY));
			final Rectangle size= getClientArea();
			final int width= Math.min(DEFAULT_WIDTH, size.width / 2);
			final int height= size.height - 7;
			final int x0= size.x;
			final int y0= size.y + (size.height - height) / 2;
			
			gc.drawRectangle(x0, y0, width, height);
			gc.drawRectangle(x0 + width, y0, width, height);
			
			drawPreview(gc, 0, x0 + 1, y0 + 1, width - 2, height - 2);
			drawPreview(gc, 1, x0 + width + 1, y0 + 1, width - 2, height - 2);
		}
		
		public GridData createGD() {
			return new GridData(SWT.FILL, SWT.FILL, true, false);
		}
		
		@Override
		public Point computeSize(final int wHint, final int hHint, final boolean changed) {
			int width= 1 + DEFAULT_WIDTH * 2;
			if (wHint != -1 && wHint < width) {
				width= Math.max(width / 2, wHint);
			}
			final int height= (hHint != -1) ? hHint : (4 + LayoutUtils.defaultHSpacing());
			
			return new Point(width, height);
		}
		
		protected abstract void drawPreview(GC gc, int idx, int x, int y, int width, int height);
		
	}
	
	
	private Shell shell;
	
	private CTabFolder tabFolder;
	
	private Button okButton;
	private Button cancelButton;
	
	private final List<ToolTab> toolTabs= new ArrayList<>();
	
	private int ignoreActivation;
	
	private Color graphicBackgroundColor;
	
	
	public ToolPopup() {
	}
	
	
	protected void open(final Shell parent, final Rectangle position) {
		create(parent);
		
		final Point size= this.shell.getSize();
		final Display display= this.shell.getDisplay();
		final Monitor monitor= DialogUtils.getClosestMonitor(display, position);
		final Rectangle clientArea= monitor.getClientArea();
		
		final Rectangle bounds= new Rectangle(position.x , position.y - size.y, size.x, size.y);
		if (bounds.y < 0) {
			bounds.y= position.y + position.height;
		}
		Geometry.moveInside(bounds, clientArea);
		
		this.shell.setBounds(bounds);
		
		selectTab(getBestTab());
		
		this.shell.open();
	}
	
	
	public boolean isActive() {
		return (UIAccess.isOkToUse(this.shell) && this.shell.isVisible());
	}
	
	
	public void close() {
		if (UIAccess.isOkToUse(this.shell)) {
			this.shell.close();
		}
		dispose();
	}
	
	public void dispose() {
		if (this.shell != null) {
			if (!this.shell.isDisposed()) {
				this.shell.dispose();
			}
			onDispose();
			this.shell= null;
		}
	}
	
	private void create(final Shell parent) {
		if (UIAccess.isOkToUse(this.shell)) {
			if (this.shell.getParent() == parent) {
				return;
			}
			dispose();
		}
		
		this.toolTabs.clear();
		
		this.shell= new Shell(parent, SWT.ON_TOP | SWT.TOOL); // SWT.RESIZE
		this.shell.setText("Color");
		this.shell.setFont(JFaceResources.getDialogFont());
		this.shell.setSize(320, 300);
		
		{	final GridLayout gl= new GridLayout();
			gl.marginHeight= 0;
			gl.marginWidth= 0;
			gl.horizontalSpacing= 0;
			gl.verticalSpacing= 0;
			this.shell.setLayout(gl);
		}
		final SWTListener listener= new SWTListener();
		parent.addListener(SWT.Dispose, listener);
		this.shell.addListener(SWT.Deactivate, listener);
		
		this.shell.setBackground(this.shell.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		this.shell.setBackgroundMode(SWT.INHERIT_FORCE);
		
		this.graphicBackgroundColor= this.shell.getDisplay().getSystemColor(SWT.COLOR_GRAY);
		
		this.tabFolder= new CTabFolder(this.shell, SWT.BOTTOM | SWT.FLAT);
		this.tabFolder.setSimple(true);
		this.tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		this.tabFolder.setSelectionBackground(this.graphicBackgroundColor);
		
		addTabs(this.tabFolder);
		
		final Composite commonBar= new Composite(this.shell, SWT.NONE);
		commonBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		commonBar.setLayout(LayoutUtils.newContentGrid(3));
		
//		final Composite status= new Composite(commonBar, SWT.NONE);
//		status.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		addStatusControls(commonBar);
		
		this.okButton= new Button(commonBar, SWT.PUSH | SWT.FLAT);
		this.okButton.setText(IDialogConstants.OK_LABEL);
		this.okButton.setFont(this.shell.getFont());
		this.okButton.addListener(SWT.Selection, listener);
		
		this.cancelButton= new Button(commonBar, SWT.PUSH | SWT.FLAT);
		this.cancelButton.setText(IDialogConstants.CANCEL_LABEL);
		this.cancelButton.setFont(this.shell.getFont());
		this.cancelButton.addListener(SWT.Selection, listener);
		
		{	final Point size= this.okButton.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
			size.x= Math.max(size.x, this.cancelButton.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
			{	final GridData gd= new GridData(SWT.FILL, SWT.FILL);
				gd.widthHint= size.x;
				gd.heightHint= size.y - 2;
				this.okButton.setLayoutData(gd);
			}
			{	final GridData gd= new GridData(SWT.FILL, SWT.FILL);
				gd.widthHint= size.x;
				gd.heightHint= size.y - 2;
				this.cancelButton.setLayoutData(gd);
			}
		}
		
		this.tabFolder.addListener(SWT.Selection, listener);
		this.shell.setDefaultButton(this.okButton);
		
		this.shell.pack();
	}
	
	public Shell getShell() {
		return this.shell;
	}
	
	public Color getGraphicBackgroundColor() {
		return this.graphicBackgroundColor;
	}
	
	protected CTabFolder getTabFolder() {
		return this.tabFolder;
	}
	
	protected ToolTab getTab(final String key) {
		for (final ToolTab tab : this.toolTabs) {
			if (tab.key == key) {
				return tab;
			}
		}
		return null;
	}
	
	protected ToolTab getTab(final CTabItem item) {
		for (final ToolTab tab : this.toolTabs) {
			if (tab.tabItem == item) {
				return tab;
			}
		}
		return null;
	}
	
	protected void addStatusControls(final Composite composite) {
	}
	
	protected void addTabs(final CTabFolder tabFolder) {
	}
	
	protected ToolTab getBestTab() {
		return null;
	}
	
	protected void selectTab(final ToolTab tab) {
		if (tab != null) {
			final CTabItem tabItem= tab.getTabItem();
			this.tabFolder.setSelection(tabItem);
			tabSelected(tab);
			
			final Display display= this.shell.getDisplay();
			final Control focusControl= display.getFocusControl();
			display.asyncExec(new Runnable() {
				@Override
				public void run() {
					if (UIAccess.isOkToUse(ToolPopup.this.tabFolder)
							&& ToolPopup.this.tabFolder.getSelection() == tabItem
							&& display.getFocusControl() == focusControl) {
						tabItem.getControl().setFocus();
					}
				}
			});
		}
	}
	
	protected void tabSelected(final ToolTab tab) {
		if (tab != null) {
			tab.activated();
			tab.getTabItem().getControl().setFocus();
		}
	}
	
	public void beginIgnoreActivation() {
		this.ignoreActivation++;
	}
	
	public void endIgnoreActivation() {
		this.ignoreActivation--;
	}
	
	protected void onDispose() {
		this.tabFolder= null;
		this.toolTabs.clear();
	}
	
	protected void onOK() {
	}
	
}
