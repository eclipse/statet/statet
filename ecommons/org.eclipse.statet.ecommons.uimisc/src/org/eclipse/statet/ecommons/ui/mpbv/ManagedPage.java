/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.ui.workbench.ContextHandlers;


@NonNullByDefault
public abstract class ManagedPage<TView extends ManagedPageBookView> extends Page {
	
	
	private final TView view;
	
	private ContextHandlers handlers;
	
	
	public ManagedPage(final TView view) {
		this.view= view;
	}
	
	@Override
	public void dispose() {
		if (this.handlers != null) {
			this.handlers.dispose();
			this.handlers= null;
		}
		
		super.dispose();
	}
	
	
	public TView getView() {
		return this.view;
	}
	
	
	protected ContextHandlers getHandlers() {
		return this.handlers;
	}
	
	protected void updateState(final @Nullable Object evaluationContext) {
		this.handlers.update(evaluationContext);
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		final IPageSite site= getSite();
		this.handlers= new ContextHandlers(site);
		initActions(site, this.handlers);
		contributeToActionBars(site, site.getActionBars(), this.handlers);
	}
	
	protected void initActions(final IServiceLocator serviceLocator,
			final ContextHandlers handlers) {
	}
	
	protected void contributeToActionBars(final IServiceLocator serviceLocator,
			final IActionBars actionBars, final HandlerCollection handlers) {
	}
	
}
