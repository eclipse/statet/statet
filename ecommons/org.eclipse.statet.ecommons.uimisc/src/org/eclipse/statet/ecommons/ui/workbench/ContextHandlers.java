/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;

import org.eclipse.core.commands.IHandler2;
import org.eclipse.core.expressions.Expression;
import org.eclipse.ui.handlers.IHandlerActivation;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.commands.core.AbstractHandlerCollection;
import org.eclipse.statet.ecommons.commands.core.HandlerCollection;


@NonNullByDefault
public class ContextHandlers extends AbstractHandlerCollection<ContextHandlers.ContextCommandRecord>
		implements HandlerCollection {
	
	
	static class ContextCommandRecord extends AbstractHandlerCollection.CommandRecord {
		
		
		@Nullable IHandlerActivation activation;
		
		
		public ContextCommandRecord(final IHandler2 handler, final int flags) {
			super(handler, flags);
		}
		
		
	}
	
	
	private IHandlerService handlerService;
	
	private @Nullable Expression defaultActivationExpression;
	
	private boolean deactivateOnDisposal;
	
	
	public ContextHandlers(final IHandlerService handlerService) {
		this.handlerService= handlerService;
	}
	
	public ContextHandlers(final IServiceLocator serviceLocator) {
		this.handlerService= nonNullAssert(serviceLocator.getService(IHandlerService.class));
	}
	
	
	public final IHandlerService getHandlerService() {
		return this.handlerService;
	}
	
	public void setDefaultActivationExpression(final Expression expression) {
		this.defaultActivationExpression= expression;
	}
	
	public void setDeactivateOnDisposal(final boolean enabled) {
		this.deactivateOnDisposal= enabled;
	}
	
	
	@Override
	protected ContextCommandRecord createCommandRecord(final String commandId,
			final IHandler2 handler, final int flags) {
		return new ContextCommandRecord(handler, flags);
	}
	
	
	public void addActivate(final String commandId, final IHandler2 handler, final int flags) {
		final var record= addCommandRecord(commandId, handler, flags);
		record.activation= this.handlerService.activateHandler(commandId, handler,
				this.defaultActivationExpression, false );
	}
	
	public void addActivate(final String commandId, final IHandler2 handler) {
		final var record= addCommandRecord(commandId, handler, 0);
		record.activation= this.handlerService.activateHandler(commandId, handler,
				this.defaultActivationExpression, false );
	}
	
	
	public void deactivate(final int flags) {
		final var records= getCommandRecords();
		final var activations= new ArrayList<IHandlerActivation>(records.size());
		for (final var record : records) {
			if ((record.getFlags() & flags) != 0) {
				final var activation= record.activation;
				if (activation != null) {
					record.activation= null;
					activations.add(activation);
				}
			}
		}
		this.handlerService.deactivateHandlers(activations);
	}
	
	public void deactivateAll() {
		final var records= getCommandRecords();
		final var activations= new ArrayList<IHandlerActivation>(records.size());
		for (final var record : records) {
			final var activation= record.activation;
			if (activation != null) {
				record.activation= null;
				activations.add(activation);
			}
		}
		this.handlerService.deactivateHandlers(activations);
	}
	
	
	@Override
	@SuppressWarnings("null")
	public void dispose() {
		if (this.deactivateOnDisposal) {
			deactivateAll();
		}
		this.handlerService= null;
		
		super.dispose();
	}
	
}
