/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.content;

import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import org.eclipse.statet.ecommons.models.core.util.ElementSourceProvider;
import org.eclipse.statet.ecommons.ui.util.PostSelectionProviderProxy;
import org.eclipse.statet.ecommons.ui.util.StructuredSelectionProxy;


public class ElementSourceSelectionProvider extends PostSelectionProviderProxy {
	
	
	protected static class StructuredSelection extends StructuredSelectionProxy
			implements ElementSourceProvider {
		
		
		private final Object fSource;
		
		
		public StructuredSelection(final IStructuredSelection selection, final Object source) {
			super(selection);
			
			fSource = source;
		}
		
		
		@Override
		public Object getElementSource() {
			return fSource;
		}
		
	}
	
	
	private final Object fSource;
	
	
	public ElementSourceSelectionProvider(final IPostSelectionProvider selectionProvider, final Object source) {
		super(selectionProvider);
		
		fSource = source;
	}
	
	
	@Override
	protected ISelection getSelection(final ISelection originalSelection) {
		return new StructuredSelection((IStructuredSelection) originalSelection, fSource);
	}
	
}
