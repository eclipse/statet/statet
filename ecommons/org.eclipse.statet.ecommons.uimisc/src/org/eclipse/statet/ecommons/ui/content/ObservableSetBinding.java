/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.content;

import java.util.Collection;

import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.ISetChangeListener;
import org.eclipse.core.databinding.observable.set.SetChangeEvent;

import org.eclipse.statet.jcommons.collections.ImCollections;


public class ObservableSetBinding implements ISetChangeListener<Object> {
	
	
	private final TableFilterController controller;
	
	private final SetElementFilter filter;
	
	
	public ObservableSetBinding(final TableFilterController controller, final IObservableSet<?> set,
			final SetElementFilter filter) {
		this.controller= controller;
		this.filter= filter;
		set.addSetChangeListener(this);
	}
	
	
	protected Collection<?> getAll() {
		return null;
	}
	
	protected Collection<?> createFilterSet(final Collection<?> set) {
		return ImCollections.toList(set);
	}
	
	@Override
	public void handleSetChange(final SetChangeEvent<?> event) {
		final IObservableSet<?> set= event.getObservableSet();
		final Collection<?> copy;
		final Collection<?> all;
		if (set.isEmpty() || ((all= getAll()) != null && set.containsAll(all))) {
			copy= null;
		}
		else {
			copy= createFilterSet(set);
		}
		if (this.filter.setSet(copy)) {
			this.controller.refresh(true);
		}
	}
	
}
