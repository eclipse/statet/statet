/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench.workspace;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.views.navigator.ResourceComparator;

import org.eclipse.statet.ecommons.ui.components.TreeAndListGroup;
import org.eclipse.statet.internal.ecommons.ui.Messages;


public class ResourceFileSelectionDialog extends AbstractResourceSelectionDialog<IFile> {
	
	
	private final static int SIZING_SELECTION_WIDGET_WIDTH= 100;
	private final static int SIZING_SELECTION_WIDGET_HEIGHT= 20;
	
	
	// the visual selection widget group
	private TreeAndListGroup selectionGroup;
	
	
	/**
	 * Creates a resource selection dialog.
	 * 
	 * @param parent the parent shell
	 * @param message the message to be displayed at the top of this dialog, or
	 *     <code>null</code> to display a default message
	 */
	public ResourceFileSelectionDialog(final Shell parent, final String message) {
		super(parent);
		
		setTitle(Messages.ResourceSelectionDialog_title);
		setMessage(message);
	}
	
	
	@Override
	protected String getDefaultMessage() {
		return Messages.ResourceSelectionDialog_SelectContainer_message;
	}
	
	@Override
	protected int getResourceTypes() {
		return IResource.FILE;
	}
	
	public void addFileFilter(final String label, final String fileNamePattern) {
		// TODO (enh) add filter
	}
	
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		// page group
		final Composite composite= (Composite) super.createDialogArea(parent);
		
		createMessageArea(composite);
		
		{	final TreeAndListGroup group= new TreeAndListGroup(composite, getRootElement(),
					new ResourceContentProvider(IResource.FOLDER | IResource.PROJECT),
					WorkbenchLabelProvider.getDecoratingWorkbenchLabelProvider(),
					new ResourceContentProvider(IResource.FILE),
					WorkbenchLabelProvider.getDecoratingWorkbenchLabelProvider(),
					new ResourceComparator(ResourceComparator.NAME), false );
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
			gd.widthHint= convertWidthInCharsToPixels(SIZING_SELECTION_WIDGET_WIDTH);
			gd.heightHint= convertHeightInCharsToPixels(SIZING_SELECTION_WIDGET_HEIGHT);
			group.getControl().setLayoutData(gd);
			
			this.selectionGroup= group;
		}
		
		this.selectionGroup.addSelectionChangedListener(createSelectionChangeListener());
		this.selectionGroup.addDoubleClickListener(createSelectionDoubleClickListener());
		
		createTextField(composite);
		
		return composite;
	}
	
	@Override
	protected void initSelection(final List<IResource> checkedResources) {
		final IResource resource= checkedResources.get(0);
		if (resource instanceof IFile) {
			this.selectionGroup.selectListElement(resource);
		}
		else {
			this.selectionGroup.selectTreeElement(resource);
		}
	}
	
	@Override
	protected void initDialog() {
		this.selectionGroup.initFields();
		
		super.initDialog();
	}
	
}
