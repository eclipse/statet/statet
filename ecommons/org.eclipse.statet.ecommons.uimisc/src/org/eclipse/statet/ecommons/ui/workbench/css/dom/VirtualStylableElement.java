/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench.css.dom;

import org.eclipse.e4.ui.css.core.dom.CSSStylableElement;
import org.eclipse.e4.ui.css.core.dom.ElementAdapter;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.e4.ui.css.core.utils.ClassUtils;

import org.w3c.dom.NodeList;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.ecommons.ui.swt.css.dom.VirtualWidget;


@NonNullByDefault
@SuppressWarnings("restriction")
public abstract class VirtualStylableElement<TParent extends CSSStylableElement>
		extends ElementAdapter implements NodeList {
	
	
	private final TParent parent;
	
	private final String name;
	private final @Nullable String cssClass;
	
	private ImList<? extends VirtualStylableElement<? extends VirtualStylableElement<?>>> children= ImCollections.emptyList();
	
	
	public VirtualStylableElement(final TParent parent,
			final String name, final @Nullable String cssClass,
			final CSSEngine engine) {
		super(new VirtualWidget(), engine);
		((VirtualWidget)getNativeWidget()).setElement(this);
		this.parent= parent;
		this.name= name;
		this.cssClass= cssClass;
	}
	
	protected void init(final ImList<? extends VirtualStylableElement<? extends VirtualStylableElement<TParent>>> children) {
		this.children= children;
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		for (final VirtualStylableElement<?> child : this.children) {
			child.dispose();
		}
	}
	
	
	@Override
	public TParent getParentNode() {
		return this.parent;
	}
	
	@Override
	public NodeList getChildNodes() {
		return this;
	}
	
	@Override
	public int getLength() {
		return this.children.size();
	}
	
	@Override
	public @Nullable VirtualStylableElement<? extends VirtualStylableElement<?>> item(final int index) {
		if (index >= 0 && index < this.children.size()) {
			return this.children.get(index);
		}
		return null;
	}
	
	
	@Override
	public @Nullable String getNamespaceURI() {
		return ClassUtils.getPackageName(VirtualCompositeStylableElement.class);
	}
	
	@Override
	public String getLocalName() {
		return this.name;
	}
	
	@Override
	public @Nullable String getCSSId() {
		return null;
	}
	
	@Override
	public @Nullable String getCSSClass() {
		return this.cssClass;
	}
	
	@Override
	public @Nullable String getCSSStyle() {
		return null;
	}
	
	@Override
	public String getAttribute(final String name) {
		return ""; //$NON-NLS-1$
	}
	
	
	@Override
	public String toString() {
		final var sb= new StringBuilder();
		appendNameRec(sb, this);
		return sb.toString();
	}
	
	static void appendNameRec(final StringBuilder sb, final VirtualStylableElement<?> element) {
		final var parentNode= element.getParentNode();
		if (parentNode instanceof VirtualStylableElement<?>) {
			appendNameRec(sb, (VirtualStylableElement<?>)parentNode);
			sb.append(' ');
		}
		else {
			appendName(sb, parentNode, true);
			sb.append(' ');
		}
		appendName(sb, element, false);
	}
	
	static void appendName(final StringBuilder sb, final CSSStylableElement element, final boolean id) {
		sb.append(element.getLocalName());
		final String cssClass= element.getCSSClass();
		if (cssClass != null) {
			sb.append('.').append(cssClass);
		}
		if (id) {
			sb.append('#').append(System.identityHashCode(element));
		}
	}
	
}
