/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.math.RoundingMode;
import java.util.Locale;
import java.util.Objects;

import com.ibm.icu.text.DecimalFormat;
import com.ibm.icu.text.DecimalFormatSymbols;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils;


@NonNullByDefault
public class DoubleText implements IObjValueWidget<@Nullable Double> {
	
	
	public static DecimalFormat createFormat(final int maxDigits) {
		final DecimalFormat format= new DecimalFormat("0.0"); //$NON-NLS-1$
		format.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		format.setMaximumFractionDigits(maxDigits);
		format.setRoundingMode(RoundingMode.HALF_DOWN.ordinal());
		return format;
	}
	
	private class SWTListener implements Listener {
		
		@Override
		public void handleEvent(final Event event) {
			switch (event.type) {
			case SWT.Modify:
				updateValue(event.time);
				return;
			case SWT.KeyDown:
				switch (event.keyCode) {
				case SWT.ARROW_UP:
					incrementValue(DoubleText.this.increment);
					event.doit= false;
					return;
				case SWT.ARROW_DOWN:
					incrementValue(-DoubleText.this.increment);
					event.doit= false;
					return;
				case SWT.PAGE_UP:
					incrementValue(DoubleText.this.increment * 10);
					event.doit= false;
					return;
				case SWT.PAGE_DOWN:
					incrementValue(-DoubleText.this.increment * 10);
					event.doit= false;
					return;
				}
			}
		}
		
	}
	
	
	private final Text text;
	
	private @Nullable Double value;
	
	private final CopyOnWriteIdentityListSet<IObjValueListener<@Nullable Double>> valueListeners= new CopyOnWriteIdentityListSet<>();
	
	private double increment;
	private double min;
	private double max= Double.NaN;
	
	private @Nullable DecimalFormat format;
	
	
	public DoubleText(final Composite parent, final int flags) {
		this.text= new Text(parent, SWT.LEFT | SWT.SINGLE | flags);
		final SWTListener swtListener= new SWTListener();
		this.text.addListener(SWT.Modify, swtListener);
		this.text.addListener(SWT.KeyDown, swtListener);
	}
	
	
	@Override
	public Text getControl() {
		return this.text;
	}
	
	@Override
	public Class<@Nullable Double> getValueType() {
		return ObjectUtils.Nullable_Double_TYPE;
	}
	
	public void setIncrement(final double v) {
		this.increment= v;
	}
	
	public void setMinMax(final double min, final double max) {
		if (Double.isNaN(min) || Double.isNaN(max)) {
			this.min= 0;
			this.max= Double.NaN;
		}
		else {
			this.min= min;
			this.max= max;
		}
	}
	
	public void setFormat(final @Nullable DecimalFormat format) {
		this.format= format;
	}
	
	@Override
	public void addValueListener(final IObjValueListener<@Nullable Double> listener) {
		this.valueListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeValueListener(final IObjValueListener<@Nullable Double> listener) {
		this.valueListeners.remove(listener);
	}
	
	@Override
	public @Nullable Double getValue(final int idx) {
		return this.value;
	}
	
	@Override
	public void setValue(final int idx, final @Nullable Double value) {
		this.text.setText(formatValue(value));
	}
	
	
	private void incrementValue(final double increment) {
		final Double oldValue= this.value;
		double newValue= (oldValue != null) ? (oldValue.doubleValue() + increment) : this.min;
		if (!Double.isNaN(this.max)) {
			if (newValue < this.min) {
				newValue= this.min;
			}
			else if (newValue > this.max) {
				newValue= this.max;
			}
		}
		this.text.setText(formatValue(newValue));
	}
	
	protected @Nullable Double parseValue(final String text) {
		try {
			return Double.valueOf(text);
		}
		catch (final NumberFormatException e) {
			return null;
		}
	}
	
	protected String formatValue(final @Nullable Double value) {
		if (value == null) {
			return ""; //$NON-NLS-1$
		}
		final var format= this.format;
		return (format != null) ?
				format.format(value.doubleValue()) :
				value.toString();
	}
	
	private void updateValue(final int time) {
		final Double oldValue= this.value;
		final Double newValue= parseValue(this.text.getText());
		if (Objects.equals(oldValue, newValue)) {
			return;
		}
		
		this.value= newValue;
		
		final var event= new ObjValueEvent<>(this, time, 0, oldValue, newValue, 0);
		for (final var listener : this.valueListeners) {
			event.newValue= newValue;
			listener.valueChanged(event);
		}
	}
	
}
