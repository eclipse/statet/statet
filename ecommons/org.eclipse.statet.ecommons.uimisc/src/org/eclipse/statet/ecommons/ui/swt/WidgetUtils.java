/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Widget;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class WidgetUtils {
	
	
	public static @Nullable Display getDisplay(final @Nullable Widget widget) {
		if (widget != null && !widget.isDisposed()) {
			try {
				return widget.getDisplay();
			}
			catch (final SWTException e) {}
		}
		return null;
	}
	
	
	public static void setSelection(final StyledText styledText, final int start, final int end,
			final int time) {
		final Point prevSelection= styledText.getSelection();
		styledText.setSelection(start, end);
		final Point newSelection= styledText.getSelection();
		if (!newSelection.equals(prevSelection)) {
			final Event event= new Event();
			event.display= styledText.getDisplay();
			event.widget= styledText;
			event.type= SWT.Selection;
			event.time= time;
			event.x= newSelection.x;
			event.y= newSelection.y;
			styledText.notifyListeners(SWT.Selection, event);
		}
	}
	
	public static void setSelection(final StyledText styledText, final int offset,
			final int time) {
		setSelection(styledText, offset, offset, time);
	}
	
	
	private WidgetUtils() {
	}
	
}
