/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.wizard.WizardPage;


public class CellEditorWizardStatusUpdater implements ICellEditorListener {
	
	
	private static final String NO_ERROR = ""; //$NON-NLS-1$
	
	
	private final CellEditor fCellEditor;
	private final WizardPage fPage;
	
	private String fRestore;
	
	
	public CellEditorWizardStatusUpdater(final CellEditor editor, final WizardPage page) {
		fPage = page;
		fCellEditor = editor;
		fCellEditor.addListener(this);
	}
	
	
	@Override
	public void editorValueChanged(final boolean oldValidState, final boolean newValidState) {
		if (fRestore == null) {
			fRestore = fPage.getErrorMessage();
			if (fRestore == null) {
				fRestore = NO_ERROR;
			}
		}
		if (!newValidState) {
			fPage.setErrorMessage(fCellEditor.getErrorMessage());
		}
		else {
			fPage.setErrorMessage(null);
		}
	}
	
	@Override
	public void applyEditorValue() {
		if (fRestore != null) {
			fPage.setErrorMessage((fRestore != NO_ERROR) ? fRestore : null);
			fRestore = null;
		}
	}
	
	@Override
	public void cancelEditor() {
		if (fRestore != null) {
			fPage.setErrorMessage((fRestore != NO_ERROR) ? fRestore : null);
			fRestore = null;
		}
	}
	
}
