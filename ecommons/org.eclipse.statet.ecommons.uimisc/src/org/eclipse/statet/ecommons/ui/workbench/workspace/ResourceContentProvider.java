/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench.workspace;

import java.util.ArrayList;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.model.WorkbenchContentProvider;


/**
 * Content provider for <code>IResource</code>s that returns
 * only children of the given resource types.
 */
public final class ResourceContentProvider extends WorkbenchContentProvider {
	
	
	private final int resourceType;
	
	
	public ResourceContentProvider(final int resourceType) {
		this.resourceType= resourceType;
	}
	
	
	@Override
	public Object[] getElements(final Object element) {
		if (element instanceof IResource[]) {
			return (IResource[]) element;
		}
		return getChildren(element);
	}
	
	@Override
	public boolean hasChildren(final Object element) {
		try {
			if (element instanceof IContainer) {
				final IResource[] members= ((IContainer) element).members();
				
				//filter out the desired resource types
				for (int i= 0; i < members.length; i++) {
					if (((members[i].getType() & this.resourceType) != 0)
							&& members[i].isAccessible() ) {
						return true;
					}
				}
			}
		}
		catch (final CoreException e) {
		}
		return false;
	}
	
	@Override
	public Object[] getChildren(final Object element) {
		try {
			if (element instanceof IContainer) {
				final IResource[] members= ((IContainer) element).members();
				
				//filter out the desired resource types
				final ArrayList<IResource> results= new ArrayList<>(members.length);
				for (int i= 0; i < members.length; i++) {
					if (((members[i].getType() & this.resourceType) != 0)
							&& members[i].isAccessible() ) {
						results.add(members[i]);
					}
				}
				return results.toArray();
			}
		}
		catch (final CoreException e) {
		}
		return new Object[0];
	}
	
}
