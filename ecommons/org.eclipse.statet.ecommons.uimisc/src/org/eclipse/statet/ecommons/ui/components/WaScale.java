/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.ui.swt.AutoDisposeReference.autoDispose;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Region;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.swt.ColorUtils;


/**
 * Scale slider widget supporting multiple knobs.
 * 
 * By default it has two knobs to adjust an interval (range).
 */
@NonNullByDefault
public class WaScale extends Composite implements IIntValueWidget {
	
	
	private static class StyleData {
		
		
		final int minWidth;
		final int defaultWidth;
		final int height;
		
		final int linePadding= 1;
		
		final int knobHeight;
		final int knobWidth;
		final int knobScaleOffset;
		
		final Image knobLine;
		
		
		public StyleData(final Display display, final int style) {
			final Shell shell= new Shell(display, SWT.NO_TRIM);
			try {
				shell.setSize(200, 200);
				shell.setFont(display.getSystemFont());
				
				try (final var managedGC= autoDispose(new GC(shell))) {
					int width= (int)(managedGC.get().getFontMetrics().getAverageCharacterWidth() * 2);
					if (width % 2 == 0) {
						width++;
					}
					if (width < 11) {
						width= 11;
					}
					
					final Button button= new Button(shell, SWT.PUSH | style);
					final Point size= button.computeSize(width, (int)(width * 1.333));
					width= size.x;
					if (width % 2 == 0) {
						width++;
					}
					this.knobWidth= width;
					this.knobHeight= size.y;
					this.knobScaleOffset= this.knobWidth / 2;
					this.height= this.knobHeight + 9;
					this.minWidth= 4 * this.knobWidth + 2 * this.knobScaleOffset;
					this.defaultWidth= 100 + 2 * this.knobScaleOffset;
				}
				
				this.knobLine= createKnobLine(display, this.knobHeight / 2);
			}
			finally {
				shell.dispose();
			}
		}
		
		private Image createKnobLine(final Display display, final int height) {
			final ImageData imageData;
			try (final var managedImage= autoDispose(new Image(display, 3, height))) {
				final GC gc= new GC(managedImage.get());
				try {
					gc.setForeground(display.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW));
					gc.drawLine(0, 0, 0, height);
					gc.drawPoint(1, 0);
					gc.drawLine(2, 0, 2, height);
					
					gc.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
					gc.drawLine(1, 1, 1, height);
				}
				finally {
					gc.dispose();
				}
				
				imageData= managedImage.get().getImageData();
			}
			
			final byte[] alphaData= new byte[3 * imageData.height];
			byte alpha= 127;
			int line= 0;
			int i= 0;
			while (line++ < 4) {
				alphaData[i++]= alpha;
				alphaData[i++]= alpha;
				alphaData[i++]= alpha;
				alpha += 16;
			}
			while (line++ < imageData.height) {
				alphaData[i++]= alpha;
				alphaData[i++]= alpha;
				alphaData[i++]= alpha;
			}
			imageData.alphaData= alphaData;
			
			return new Image(display, imageData);
		}
		
	}
	
	private static @Nullable StyleData gDefaultStyleData;
	private static @Nullable StyleData gFlatStyleData;
	
	private static int checkSWTStyle(int style) {
		style &= ~(SWT.HORIZONTAL | SWT.VERTICAL);
//		style |= SWT.DOUBLE_BUFFERED | SWT.NO_BACKGROUND;
		return style;
	}
	
	private static int checkThisStyle(int style, final int swtStyle) {
		style &= (SWT.HORIZONTAL | SWT.VERTICAL);
		if ((style & SWT.HORIZONTAL) != 0) {
			style &= ~SWT.VERTICAL;
		}
		style |= (swtStyle & SWT.FLAT);
		return style;
	}
	
	
	protected class Knob {
		
		
		private int idx;
		
		private int value;
		
		private Button button;
		
		private String toolTipText;
		
		private boolean savedEnabled= true;
		
		
		/**
		 * Create a new know
		 * 
		 * @param value The initial value of the knob
		 */
		public Knob(final int value) {
			this.value= value;
		}
		
		
		public void setToolTipText(final String text) {
			this.toolTipText= text;
			if (this.button != null) {
				this.button.setToolTipText(text);
			}
		}
		
		/**
		 * Returns the index of the knob in the list of all knobs ({@link WaScale#getKnobs()}).
		 * 
		 * @return The index of the knob
		 */
		public final int getIdx() {
			return this.idx;
		}
		
		/**
		 * Returns the value of the knob.
		 * 
		 * @return The value of the knob
		 */
		public final int getValue() {
			return this.value;
		}
		
		/**
		 * Sets the value of the knob.
		 * 
		 * The method checks if the value is between minimum and maximum of the scale and adjust
		 * it if required. Sub classes can add additional checks by overwriting the method.
		 * 
		 * @param value the new value of the knob
		 */
		protected int checkValue(int value) {
			if (value < WaScale.this.minimum) {
				value= WaScale.this.minimum;
			}
			else if (value > WaScale.this.maximum) {
				value= WaScale.this.maximum;
			}
			return value;
		}
		
		/**
		 * Sets the value of the knob and notifies all listeners if the value changed.
		 * 
		 * @param time The time stamp of the event
		 * @param value The new value of the knob
		 */
		public boolean setValue(final int time, int value) {
			final int oldValue= this.value;
			value= checkValue(value);
			value= fireAboutToChange(time, this.idx, oldValue, value);
			value= checkValue(value);
			if (oldValue == value) {
				return false;
			}
			this.value= value;
			fireChanged(time, this.idx, oldValue, this.value);
			return true;
		}
		
	}
	
	protected class OrderedKnob extends Knob {
		
		public OrderedKnob(final int value) {
			super(value);
		}
		
		public OrderedKnob(final int value, final String toolTipText) {
			super(value);
			setToolTipText(toolTipText);
		}
		
		protected int getMinDistance() {
			return 1;
		}
		
		@Override
		protected int checkValue(int value) {
			final List<Knob> knobs= getKnobs();
			final int idx= getIdx();
			final int distance= getMinDistance();
			if (idx + 1 < knobs.size()) {
				final int bound= knobs.get(idx + 1).getValue();
				if (bound - value < distance) {
					value= bound - distance;
				}
			}
			if (idx > 0) {
				final int bound= knobs.get(idx - 1).getValue();
				if (value - bound < distance) {
					value= bound + distance;
				}
			}
			return super.checkValue(value);
		}
		
	}
	
	
	private final int thisStyle;
	private final StyleData styleData;
	
	private Color color1;
	private Color color2;
	private Color rangeColor;
	private Color rangeFocusColor;
	private Color tickColor;
	
	private Rectangle scaleArea;
	
	private int minimum= 1;
	private int maximum= 100;
	
	private int increment= 1;
	private int pageIncrement= 10;
	
	private ImList<Knob> knobs;
	
	private final CopyOnWriteIdentityListSet<IIntValueListener> selectionListeners= new CopyOnWriteIdentityListSet<>();
	
	private boolean isActive;
	
	private int opInProgress;
	private int opButton;
	private int opSavedValue;
	private int opOffset;
	
	
	private final FocusListener buttonFocusListener= new FocusListener() {
		@Override
		public void focusGained(final FocusEvent event) {
			final Control[] children= getChildren();
			if (children[0] == event.widget) {
				return;
			}
			((Button)event.widget).moveAbove(children[0]);
		}
		@Override
		public void focusLost(final FocusEvent event) {
		}
	};
	private final PaintListener buttonPaintListener= new PaintListener() {
		@Override
		public void paintControl(final PaintEvent event) {
			paintKnob(event);
		}
	};
	private final Listener buttonKnobMoveListener= new Listener() {
		@Override
		public void handleEvent(final Event event) {
			if (event.type == SWT.Traverse) {
				switch (event.keyCode) {
				case SWT.ARROW_LEFT:
				case SWT.ARROW_DOWN:
				case SWT.ARROW_RIGHT:
				case SWT.ARROW_UP:
					event.doit= false;
					return;
				default:
					return;
				}
			}
			
			moveKnob(event);
		}
	};
	
	
	public WaScale(final Composite parent, final int style) {
		super(parent, checkSWTStyle(style));
		
		this.thisStyle= checkThisStyle(style, getStyle());
		this.styleData= createStyle();
		this.knobs= ImCollections.emptyList();
		
		updateColors();
		
		addPaintListener(new PaintListener() {
			@Override
			public void paintControl(final PaintEvent event) {
				paint(event);
			}
		});
		final Listener listener= new Listener() {
			@Override
			public void handleEvent(final Event event) {
				switch (event.type) {
				case SWT.Activate:
					WaScale.this.isActive= true;
					redraw();
					return;
				case SWT.Deactivate:
					WaScale.this.isActive= false;
					redraw();
					return;
				case SWT.Resize:
					updateLayout();
					return;
				case SWT.Dispose:
					onDispose();
					return;
				default:
					return;
				}
			}
		};
		addListener(SWT.Activate, listener);
		addListener(SWT.Deactivate, listener);
		addListener(SWT.Resize, listener);
		addListener(SWT.Dispose, listener);
		
		initKnobs();
	}
	
	
	private StyleData createStyle() {
		StyleData data;
		if ((this.thisStyle & SWT.FLAT) != 0) {
			data= gFlatStyleData;
			if (data == null) {
				data= new StyleData(getDisplay(), SWT.FLAT);
				gFlatStyleData= data;
			}
		}
		else {
			data= gDefaultStyleData;
			if (data == null) {
				data= new StyleData(getDisplay(), SWT.NONE);
				gDefaultStyleData= data;
			}
		}
		return data;
	}
	
	@Override
	public Control getControl() {
		return this;
	}
	
	protected void initKnobs() {
//		setKnobs(new ConstList<Knob>(
//				new DefaultKnob(getMinimum(), "Lower Bound"),
//				new DefaultKnob(getMaximum(), "Upper Bound") ));
		setKnobs(ImCollections.newList(
				new Knob(getMinimum()),
				new Knob(getMaximum()) ));
	}
	
	protected void setKnobs(final ImList<Knob> knobs) {
		stopOperations(0);
		
		if (this.knobs != null) {
			for (final Knob knob : knobs) {
				if (knob.button != null) {
					knob.button.dispose();
					knob.button= null;
				}
			}
		}
		
		final var tabList= new @NonNull Control[knobs.size()];
		final boolean enabled= getEnabled();
		for (int idx= 0; idx < knobs.size(); idx++) {
			final Knob knob= knobs.get(idx);
			knobs.get(idx).idx= idx;
			knob.button= createKnobControl(knob);
			if (!enabled) {
				knob.button.setEnabled(false);
			}
			tabList[idx]= knob.button;
		}
		this.knobs= knobs;
		setTabList(tabList);
		
		recheckKnobValues();
		updateLayout();
	}
	
	protected List<Knob> getKnobs() {
		return this.knobs;
	}
	
	private void recheckKnobValues() {
		final List<Knob> knobs= this.knobs;
		for (int i= 0; i < knobs.size(); i++) {
			final Knob knob= knobs.get(i);
			knob.setValue(0, knob.getValue());
		}
	}
	
	
	public int getMinimum() {
		checkWidget();
		return this.minimum;
	}
	
	public void setMinimum(final int value) {
		checkWidget();
		this.minimum= value;
	}
	
	public int getMaximum() {
		checkWidget();
		return this.maximum;
	}
	
	public void setMaximum(final int value) {
		checkWidget();
		this.maximum= value;
	}
	
	public int getPageIncrement() {
		checkWidget();
		return this.pageIncrement;
	}
	
	public void setPageIncrement(final int increment) {
		checkWidget();
		this.pageIncrement= increment;
	}
	
	public int getIncrement() {
		checkWidget();
		return this.increment;
	}
	
	public void setIncrement(final int increment) {
		checkWidget();
		this.increment= increment;
	}
	
	@Override
	public int getValue(final int knobIdx) {
		return this.knobs.get(knobIdx).getValue();
	}
	
	@Override
	public void setValue(final int knobIdx, final int value) {
		this.knobs.get(knobIdx).setValue(0, value);
		updateButtonPositions();
		redraw();
	}
	
	@Override
	public void addValueListener(final IIntValueListener listener) {
		this.selectionListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeValueListener(final IIntValueListener listener) {
		this.selectionListeners.remove(listener);
	}
	
	private int fireAboutToChange(final int time, final int knobIdx, final int oldValue, final int newValue) {
		if (time == 0) {
			return newValue;
		}
		final IntValueEvent scaleEvent= new IntValueEvent(this, time, knobIdx, oldValue, newValue);
		for (final var listener : this.selectionListeners) {
			listener.valueAboutToChange(scaleEvent);
		}
		return scaleEvent.newValue;
	}
	
	private void fireChanged(final int time, final int knobIdx, final int oldValue, final int newValue) {
		if (time == 0) {
			return;
		}
		final IntValueEvent scaleEvent= new IntValueEvent(this, time, knobIdx, oldValue, newValue);
		for (final var listener : this.selectionListeners) {
			scaleEvent.newValue= newValue;
			listener.valueChanged(scaleEvent);
		}
	}
	
	
	@Override
	public void setEnabled(final boolean enabled) {
		super.setEnabled(enabled);
		if (getEnabled() == enabled) {
			return;
		}
		
		final List<Knob> knobs= this.knobs;
		if (!enabled) {
			for (int idx= 0; idx < knobs.size(); idx++) {
				final Knob knob= knobs.get(idx);
				knob.savedEnabled= knob.button.getEnabled();
				knob.button.setEnabled(false);
			}
		}
		else {
			for (int idx= 0; idx < knobs.size(); idx++) {
				final Knob knob= knobs.get(idx);
				knob.button.setEnabled(knob.savedEnabled);
			}
		}
	}
	
	protected int convertClient2Value(final int coord) {
		final Rectangle scaleArea= this.scaleArea;
		double tmp= coord - scaleArea.x;
		tmp /= scaleArea.width;
		tmp *= ((double)this.maximum - this.minimum);
		tmp += this.minimum;
		return (int)tmp;
	}
	
	protected int convertValue2Client(final int value) {
		final Rectangle scaleArea= this.scaleArea;
		double tmp= value - this.minimum;
		tmp /= ((double)this.maximum - this.minimum);
		tmp *= scaleArea.width;
		int scale= (int)tmp;
		if (tmp < 0) {
			scale= 0;
		}
		if (tmp > scaleArea.width) {
			scale= scaleArea.width;
		}
		return scale + scaleArea.x;
	}
	
	
	protected final int getCoord(final Event event) {
		return ((this.thisStyle & SWT.VERTICAL) != 0) ? event.y : event.x;
	}
	
	protected final int getCoord(final Point location) {
		return ((this.thisStyle & SWT.VERTICAL) != 0) ? location.y : location.x;
	}
	
	@Override
	public Point computeSize(int wHint, final int hHint, final boolean changed) {
		if (wHint < 0) {
			wHint= this.styleData.defaultWidth;
		}
		else if (wHint < this.styleData.minWidth) {
			wHint= this.styleData.minWidth;
		}
		final Rectangle trim= computeTrim (0, 0, wHint, this.styleData.height);
		return new Point (trim.width, trim.height);
	}
	
	@Override
	public void setBackground(final Color color) {
		super.setBackground(color);
		updateColors();
	}
	
	protected void updateColors() {
		this.color1= computeColor1();
		this.color2= computeColor2();
		this.rangeColor= computeRangeColor(false);
		this.rangeFocusColor= computeRangeColor(true);
		this.tickColor= computeTickColor();
	}
	
	protected Color computeColor1() {
		Color color;
		final Display display= getDisplay();
		color= display.getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW);
		if (color.equals(getBackground())) {
			color= display.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW);
		}
		return color;
	}
	
	protected Color getColor1() {
		return this.color1;
	}
	
	protected Color computeColor2() {
		Color color;
		final Display display= getDisplay();
		color= display.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW);
		if (color.equals(getBackground())) {
			color= display.getSystemColor(SWT.COLOR_WIDGET_HIGHLIGHT_SHADOW);
		}
		return color;
	}
	
	protected Color getColor2() {
		return this.color2;
	}
	
	protected Color computeRangeColor(final boolean focus) {
		Color color;
		final Display display= getDisplay();
		if (focus) {
			color= ColorUtils.blend(getBackground(),
					display.getSystemColor(SWT.COLOR_LIST_SELECTION), 0.5f );
		}
		else {
			color= display.getSystemColor(SWT.COLOR_GRAY);
			if (color.equals(getBackground())) {
				color= display.getSystemColor(SWT.COLOR_DARK_GRAY);
			}
		}
		return color;
	}
	
	protected Color getRangeColor(final boolean focus) {
		return (focus) ? this.rangeFocusColor : this.rangeColor;
	}
	
	protected Color computeTickColor() {
		return computeColor1();
	}
	
	protected Color getTickColor() {
		return this.tickColor;
	}
	
	
	private Button createKnobControl(final Knob knob) {
		final Button button= new Button(this, SWT.PUSH | (this.thisStyle & SWT.FLAT));
		button.setSize(this.styleData.knobWidth, this.styleData.knobHeight);
		button.addFocusListener(this.buttonFocusListener);
		button.addPaintListener(this.buttonPaintListener);
		button.addListener(SWT.Traverse, this.buttonKnobMoveListener);
		button.addListener(SWT.KeyDown, this.buttonKnobMoveListener);
		button.addListener(SWT.MouseWheel, this.buttonKnobMoveListener);
		button.addListener(SWT.MouseDown, this.buttonKnobMoveListener);
		button.addListener(SWT.MouseMove, this.buttonKnobMoveListener);
		button.addListener(SWT.MouseUp, this.buttonKnobMoveListener);
		button.setData(knob);
		button.setToolTipText(knob.toolTipText);
		final Region shape= new Region(button.getDisplay());
		shape.add(0, 0, this.styleData.knobWidth, this.styleData.knobHeight - 1);
		button.setRegion(shape);
		return button;
	}
	
	private void updateLayout() {
		final Rectangle scaleArea= getClientArea();
		if ((this.thisStyle & SWT.VERTICAL) != 0) {
			scaleArea.x= scaleArea.y;
			scaleArea.width= scaleArea.height;
		}
		scaleArea.x += this.styleData.knobScaleOffset;
		scaleArea.width -= 2* this.styleData.knobScaleOffset;
		this.scaleArea= scaleArea;
		
		updateButtonPositions();
	}
	
	private void updateButtonPositions() {
		final List<Knob> knobs= this.knobs;
		if ((this.thisStyle & SWT.VERTICAL) != 0) {
			for (int idx= 0; idx < knobs.size(); idx++) {
				final Knob knob= knobs.get(idx);
				final int y= convertValue2Client(knob.getValue()) - this.styleData.knobScaleOffset;
				knob.button.setLocation(0, y);
			}
		}
		else {
			for (int idx= 0; idx < knobs.size(); idx++) {
				final Knob knob= knobs.get(idx);
				final int x= convertValue2Client(knob.getValue()) - this.styleData.knobScaleOffset;
				knob.button.setLocation(x, 0);
			}
		}
	}
	
	private void paint(final PaintEvent e) {
		final Rectangle clientArea= getClientArea();
		if (clientArea.width == 0 || clientArea.height == 0) {
			return;
		}
		
		final GC gc= e.gc;
		
		gc.setBackground(getBackground());
		gc.fillRectangle(clientArea);
		
		if (clientArea.height < this.styleData.height || clientArea.width < this.styleData.knobWidth) {
			return;
		}
		
		// Line
		gc.setLineWidth(1);
		gc.setForeground(getColor1());
		int x= clientArea.x + this.styleData.linePadding;
		int y= clientArea.y + this.styleData.knobHeight - 1;
		int width= clientArea.width - 2 * this.styleData.linePadding;
		gc.drawLine(x, y, x + width, y);
		y ++;
		gc.drawPoint(x, y);
//		e.gc.setForeground(getBackground());
//		e.gc.drawLine(x + 1, y, x + width - 2, y);
		gc.setForeground(getColor2());
		gc.drawPoint(x + width, y);
		y ++;
		gc.drawLine(x, y, x + width, y);
		y ++;
		
		// Ticks
		final Rectangle scaleArea= this.scaleArea;
		width= scaleArea.width;
		y += 3;
		gc.setForeground(getTickColor());
		final int count= 4;
		for (int i= 0; i <= count; i++) {
			x= scaleArea.x + (width * i) / count;
			gc.drawLine(x, y, x, y + 3);
		}
		
		y= clientArea.y + this.styleData.knobHeight;
		paintRanges(gc, y);
		
		gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
		for (int i= 0; i < this.knobs.size(); i++) {
			x= convertValue2Client(this.knobs.get(i).getValue());
			gc.drawPoint(x, y);
		}
	}
	
	
	protected void stopOperations(final int time) {
		if (this.opInProgress == 0) {
			return;
		}
		this.opInProgress= 0;
		this.knobs.get(this.opButton).setValue(time, this.opSavedValue);
		
		updateButtonPositions();
		redraw();
	}
	
	protected boolean isInOperation() {
		return false;
	}
	
	private void paintKnob(final PaintEvent e) {
		final GC gc= e.gc;
		
		gc.drawImage(this.styleData.knobLine, this.styleData.knobScaleOffset - 1, (1 + this.styleData.knobHeight) / 2);
	}
	
	protected void paintRanges(final GC gc, final int y) {
		if (this.knobs.size() == 2) {
			final Knob knob0= this.knobs.getFirst();
			final Knob knob1= this.knobs.getLast();
			gc.setForeground(getRangeColor(this.isActive &&
					(knob0.button.isFocusControl() || knob1.button.isFocusControl())));
			gc.drawLine(convertValue2Client(this.knobs.getFirst().getValue()), y,
					convertValue2Client(this.knobs.getLast().getValue()), y);
		}
	}
	
	
	private void moveKnob(final Event event) {
		final Knob knob= nonNullAssert((Knob)event.widget.getData());
		
		int newValue;
		
		EVENT: switch (event.type) {
		case SWT.KeyDown:
			stopOperations(event.time);
			
			switch (event.keyCode) {
			case SWT.ARROW_LEFT:
				newValue= knob.getValue() - this.increment;
				break EVENT;
			case SWT.ARROW_DOWN:
				if ((this.thisStyle & SWT.VERTICAL) != 0) {
					newValue= knob.getValue() + this.increment;
					break EVENT;
				}
				newValue= knob.getValue() - this.increment;
				break EVENT;
			case SWT.ARROW_RIGHT:
				newValue= knob.getValue() + this.increment;
				break EVENT;
			case SWT.ARROW_UP:
				if ((this.thisStyle & SWT.VERTICAL) != 0) {
					newValue= knob.getValue() - this.increment;
					break EVENT;
				}
				newValue= knob.getValue() + this.increment;
				break EVENT;
			case SWT.PAGE_DOWN:
				newValue= knob.getValue() - this.pageIncrement;
				break EVENT;
			case SWT.PAGE_UP:
				newValue= knob.getValue() + this.pageIncrement;
				break EVENT;
			case SWT.HOME:
				newValue= this.minimum;
				break EVENT;
			case SWT.END:
				newValue= this.maximum;
				break EVENT;
			default:
				return;
			}
		
		case SWT.MouseWheel:
			if (isInOperation()) {
				return;
			}
			
			newValue= knob.getValue() + this.increment * event.count;
			
			break EVENT;
		
		case SWT.MouseDown:
			stopOperations(event.time);
			
			this.opInProgress= 1;
			this.opButton= knob.getIdx();
			this.opSavedValue= knob.getValue();
			this.opOffset= getCoord(event) - this.styleData.knobScaleOffset;
			
			return;
			
		case SWT.MouseMove:
			if (this.opInProgress != 1) {
				return;
			}
			
			newValue= convertClient2Value(getCoord(knob.button.getLocation()) + getCoord(event) - this.opOffset);
			
			break EVENT;
			
		case SWT.MouseUp:
			if (this.opInProgress != 1) {
				return;
			}
			
			this.opInProgress= 0;
			
			return;
			
		default:
			return;
		}
		
		event.doit= false;
		knob.setValue(event.time, newValue);
		
		updateButtonPositions();
		redraw();
	}
	
	protected void onDispose() {
	}
	
}
