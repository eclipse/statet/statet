/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Patrick Chuong (Texas Instruments) - org.eclipse.jdt: Bug 292411
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.jface.resource;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * A registry that maps <code>ImageDescriptors</code> to <code>Image</code>.
 */
@NonNullByDefault
public class ImageDescriptorRegistry {
	
	
	private final Map<ImageDescriptor, Image> registry= new HashMap<>();
	
	private final Display display;
	
	private boolean disposed;
	
	
	/**
	 * Creates a new image descriptor registry for the given display.
	 * 
	 * <p>All images managed by this registry will be disposed when the display gets disposed.</p>
	 * 
	 * @param display the display the images managed by this registry are allocated for
	 */
	public ImageDescriptorRegistry(final Display display) {
		this.display= nonNullAssert(display);
		
		this.display.asyncExec(() -> {
			ImageDescriptorRegistry.this.display.disposeExec(
					ImageDescriptorRegistry.this::dispose );
		});
	}
	
	/**
	 * Creates a new image descriptor registry for the current or default display, respectively.
	 */
	public ImageDescriptorRegistry() {
		this(UIAccess.getDisplay());
	}
	
	protected void dispose() {
		synchronized (this.registry) {
			this.disposed= true;
			try {
				for (final Image image : this.registry.values()) {
					image.dispose();
				}
			}
			finally {
				this.registry.clear();
			}
		}
	}
	
	
	/**
	 * Returns the image associated with the given image descriptor.
	 * 
	 * @param descriptor the image descriptor for which the registry manages an image
	 * @return the image associated with the image descriptor or <code>null</code>
	 *    if the image descriptor can't create the requested image.
	 */
	public @Nullable Image get(ImageDescriptor descriptor) {
		if (descriptor == null) {
			descriptor= ImageDescriptor.getMissingImageDescriptor();
		}
		synchronized (this.registry) {
			Image image= this.registry.get(descriptor);
			if (image == null && !this.disposed) {
				image= descriptor.createImage();
				if (image != null) {
					this.registry.put(descriptor, image);
				}
			}
			return image;
		}
	}
	
}
