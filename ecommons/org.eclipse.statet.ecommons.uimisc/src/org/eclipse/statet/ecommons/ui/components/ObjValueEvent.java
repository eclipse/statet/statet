/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import org.eclipse.swt.events.TypedEvent;


public class ObjValueEvent<T> extends TypedEvent {
	
	private static final long serialVersionUID = 7509242153124380586L;
	
	public static final int DEFAULT_SELECTION = 1 << 0;
	
	
	public final int valueIdx;
	
	public final T oldValue;
	
	public T newValue;
	
	public final int flags;
	
	
	public ObjValueEvent(final IObjValueWidget<T> source, final int time, final int idx,
			final T oldValue, final T newValue, final int flags) {
		super(source);
		
		this.display = source.getControl().getDisplay();
		this.widget = source.getControl();
		this.time = time;
		
		this.valueIdx = idx;
		this.oldValue = oldValue;
		this.newValue = newValue;
		
		this.flags = flags;
	}
	
	
}
