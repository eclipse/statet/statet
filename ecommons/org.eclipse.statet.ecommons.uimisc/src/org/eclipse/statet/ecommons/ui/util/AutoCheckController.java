/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler2;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;


/**
 * Enables check by selection for checkbox viewers
 */
public class AutoCheckController<TElement> implements Listener {
	
	
	private final CheckboxTableViewer viewer;
	
	private final IObservableSet<TElement> set;
	
	private int ignoreTime;
	
	
	public AutoCheckController(final CheckboxTableViewer viewer, final IObservableSet<TElement> set) {
		this.viewer= viewer;
		this.set= set;
		
		viewer.getTable().addListener(SWT.Selection, this);
	}
	
	
	public IObservableSet<TElement> getChecked() {
		return this.set;
	}
	
	@Override
	public void handleEvent(final Event event) {
		if (event.type == SWT.Selection) {
			if (event.item == null) {
				return;
			}
			if (event.detail == SWT.CHECK) {
				this.ignoreTime= event.time;
			}
			else if (event.detail == 0 && event.time != this.ignoreTime
					&& ((event.stateMask & SWT.BUTTON_MASK) == 0
							|| (event.stateMask & SWT.BUTTON_MASK) == SWT.BUTTON1 )) {
				event.display.asyncExec(new Runnable() {
					@Override
					public void run() {
						if (event.time == AutoCheckController.this.ignoreTime) {
							return;
						}
						if ((event.stateMask & SWT.MOD2) != 0) {
							final TableItem[] selection= AutoCheckController.this.viewer.getTable().getSelection();
							final List<TElement> list= new ArrayList<>();
							for (final TableItem item : selection) {
								@SuppressWarnings("unchecked")
								final TElement element= (TElement) item.getData();
								if (element != null) {
									list.add(element);
								}
							}
							AutoCheckController.this.set.retainAll(list);
							if (AutoCheckController.this.set.size() != list.size()) {
								AutoCheckController.this.set.addAll(list);
							}
						}
						else if ((event.stateMask & SWT.MOD1) != 0) {
							@SuppressWarnings("unchecked")
							final TElement element= (TElement) event.item.getData();
							if (element != null) {
								AutoCheckController.this.set.add(element);
							}
						}
						else {
							@SuppressWarnings("unchecked")
							final TElement element= (TElement) event.item.getData();
							if (element != null) {
								AutoCheckController.this.set.retainAll(Collections.singleton(element));
								if (AutoCheckController.this.set.isEmpty()) {
									AutoCheckController.this.set.add(element);
								}
							}
						}
					}
				});
			}
			return;
		}
	}
	
	
	public IHandler2 createSelectAllHandler() {
		return new AbstractHandler() {
			@Override
			public Object execute(final ExecutionEvent event) throws ExecutionException {
				AutoCheckController.this.viewer.getTable().selectAll();
				AutoCheckController.this.set.addAll(
						((IStructuredSelection) AutoCheckController.this.viewer.getSelection()).toList() );
				return null;
			}
		};
	}
	
}
