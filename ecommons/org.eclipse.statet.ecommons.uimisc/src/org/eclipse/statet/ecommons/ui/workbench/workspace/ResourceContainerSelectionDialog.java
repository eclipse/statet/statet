/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench.workspace;

import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.views.navigator.ResourceComparator;

import org.eclipse.statet.internal.ecommons.ui.Messages;


public class ResourceContainerSelectionDialog extends AbstractResourceSelectionDialog<IContainer> {
	
	
	private final static int SIZING_SELECTION_WIDGET_WIDTH= 75;
	private final static int SIZING_SELECTION_WIDGET_HEIGHT= 20;
	
	
	private TreeViewer selectionViewer;
	
	
	public ResourceContainerSelectionDialog(final Shell parent, final String message) {
		super(parent);
		
//		super(shell, WorkbenchLabelProvider.getDecoratingWorkbenchLabelProvider(),
//				new ResourceContentProvider(IResource.FOLDER | IResource.PROJECT) );
//		setComparator(new ResourceComparator(ResourceComparator.NAME));
		setTitle(Messages.ResourceSelectionDialog_title);
		setMessage(message);
	}
	
	
	@Override
	protected String getDefaultMessage() {
		return Messages.ResourceSelectionDialog_SelectContainer_message;
	}
	
	@Override
	protected int getResourceTypes() {
		return IResource.PROJECT | IResource.FOLDER;
	}
	
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		// page group
		final Composite composite= (Composite) super.createDialogArea(parent);
		
		createMessageArea(composite);
		
		{	final TreeViewer viewer= new TreeViewer(composite, SWT.SINGLE | SWT.BORDER);
			viewer.setContentProvider(new ResourceContentProvider(IResource.FOLDER | IResource.PROJECT));
			viewer.setComparator(new ResourceComparator(ResourceComparator.NAME));
			viewer.setLabelProvider(WorkbenchLabelProvider.getDecoratingWorkbenchLabelProvider());
			viewer.setInput(getRootElement());
			
			final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
			gd.widthHint= convertWidthInCharsToPixels(SIZING_SELECTION_WIDGET_WIDTH);
			gd.heightHint= convertHeightInCharsToPixels(SIZING_SELECTION_WIDGET_HEIGHT);
			viewer.getControl().setLayoutData(gd);
			
			this.selectionViewer= viewer;
		}
		
		this.selectionViewer.addSelectionChangedListener(createSelectionChangeListener());
		this.selectionViewer.addDoubleClickListener(createSelectionDoubleClickListener());
		
		createTextField(composite);
		
		return composite;
	}
	
	@Override
	protected void initSelection(final List<IResource> checkedResources) {
		this.selectionViewer.setSelection(new StructuredSelection(checkedResources), true);
	}
	
}
