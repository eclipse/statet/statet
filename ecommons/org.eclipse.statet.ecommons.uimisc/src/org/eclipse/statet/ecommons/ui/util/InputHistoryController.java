/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.observable.list.ListChangeEvent;
import org.eclipse.jface.dialogs.IDialogSettings;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;


@NonNullByDefault
public abstract class InputHistoryController<E> {
	
	
	public static class ForString extends InputHistoryController<String> {
		
		
		public ForString(final IDialogSettings settings, final String settingsHistoryKey,
				final int maxLength) {
			super(settings, settingsHistoryKey, maxLength);
		}
		
		public ForString(final IDialogSettings settings, final String settingsHistoryKey) {
			super(settings, settingsHistoryKey);
		}
		
		
		@Override
		protected @Nullable String createElement(final String serialized) {
			return serialized;
		}
		
		@Override
		protected String serialize(final String element) {
			return element;
		}
		
	}
	
	
	private final IDialogSettings settings;
	private final String settingsHistoryKey;
	
	private final InputHistory<@NonNull E> history;
	
	private boolean isLoading;
	
	
	public InputHistoryController(final IDialogSettings settings, final String settingsHistoryKey,
			final int maxLength) {
		this.settings= settings;
		this.settingsHistoryKey= settingsHistoryKey;
		
		this.history= createList(maxLength);
		load();
		this.history.addListChangeListener(
				(final ListChangeEvent<? extends E> event) -> {
					if (!this.isLoading) {
						save();
					}
				});
	}
	
	public InputHistoryController(final IDialogSettings settings, final String settingsHistoryKey) {
		this(settings, settingsHistoryKey,
				DialogUtils.HISTORY_MAX );
	}
	
	
	protected InputHistory<@NonNull E> createList(final int maxLength) {
		return new InputHistory<>(maxLength);
	}
	
	public InputHistory<E> getList() {
		return this.history;
	}
	
	
	protected void load() {
		this.isLoading= true;
		try {
			final IDialogSettings settings= this.settings;
			
			final String[] serialized= settings.getArray(this.settingsHistoryKey);
			final List<@NonNull E> elements;
			if (serialized != null) {
				elements= new ArrayList<>(serialized.length);
				for (int i= 0; i < serialized.length; i++) {
					@Nullable
					final E element= createElement(serialized[i]);
					if (element != null && !elements.contains(element)) {
						elements.add(element);
					}
				}
			}
			else {
				elements= ImCollections.emptyList();
			}
			this.history.clear();
			this.history.addAll(elements);
		}
		finally {
			this.isLoading= false;
		}
	}
	
	protected void save() {
		final IDialogSettings settings= this.settings;
		
		final String[] serialized= new @NonNull String[this.history.size()];
		for (int i= 0; i < serialized.length; i++) {
			serialized[i]= serialize(this.history.get(i));
		}
		
		settings.put(this.settingsHistoryKey, serialized);
	}
	
	protected abstract @Nullable E createElement(final String serialized);
	
	protected abstract String serialize(final @NonNull E element);
	
}
