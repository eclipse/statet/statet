/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.jface.resource.ImageDescriptorRegistry;
import org.eclipse.statet.ecommons.ui.util.UIResources;
import org.eclipse.statet.internal.ecommons.ui.UIMiscellanyPlugin;


@NonNullByDefault
public class SharedUIResources extends UIResources {
	
	
	public static final String BUNDLE_ID= UIMiscellanyPlugin.BUNDLE_ID;
	
	
	public static final String EDIT_COPYPASTE_MENU_ID= "edit.copypaste"; //$NON-NLS-1$
	
	
	public static final String NEW_PAGE_COMMAND_ID = "org.eclipse.statet.workbench.commands.NewPage"; //$NON-NLS-1$
	public static final String CLOSE_PAGE_COMMAND_ID = "org.eclipse.statet.workbench.commands.ClosePage"; //$NON-NLS-1$
	public static final String CLOSE_ALL_PAGES_COMMAND_ID = "org.eclipse.statet.workbench.commands.CloseAllPages"; //$NON-NLS-1$
	
	/** equal to org.eclipse.ui.texteditor.IWorkbenchActionDefinitionIds#FIND_NEXT */
	public static final String FIND_NEXT_COMMAND_ID = "org.eclipse.ui.edit.findNext"; //$NON-NLS-1$
	/** equal to org.eclipse.ui.texteditor.IWorkbenchActionDefinitionIds#FIND_PREVIOUS */
	public static final String FIND_PREVIOUS_COMMAND_ID = "org.eclipse.ui.edit.findPrevious"; //$NON-NLS-1$
	
	
	private static final String NS= UIMiscellanyPlugin.BUNDLE_ID;
	
	public static final String PLACEHOLDER_IMAGE_ID=            NS + "/images/obj/dummy"; //$NON-NLS-1$
	
	public static final String OBJ_USER_IMAGE_ID=               NS + "/images/obj/user"; //$NON-NLS-1$
	
	public static final String OBJ_LINE_MATCH_IMAGE_ID=         NS + "/images/obj/line_match"; //$NON-NLS-1$
	
	public static final String OBJ_MAIN_TAB_ID=                 NS + "/images/obj/main_tab"; //$NON-NLS-1$
	
	public static final String OVR_DEFAULT_MARKER_IMAGE_ID=     NS + "/images/ovr/default_marker"; //$NON-NLS-1$
	public static final String OVR_GREEN_LIGHT_IMAGE_ID=        NS + "/images/ovr/light-green"; //$NON-NLS-1$
	public static final String OVR_YELLOW_LIGHT_IMAGE_ID=       NS + "/images/ovr/light-yellow"; //$NON-NLS-1$
	
	public static final String OVR_INFO_IMAGE_ID=               NS + "/images/ovr/info"; //$NON-NLS-1$
	public static final String OVR_WARNING_IMAGE_ID=            NS + "/images/ovr/warning"; //$NON-NLS-1$
	public static final String OVR_ERROR_IMAGE_ID=              NS + "/images/ovr/error"; //$NON-NLS-1$
	public static final String OVR_IGNORE_OPTIONAL_PROBLEMS_IMAGE_ID= NS + "/images/ovr/ignore-optional_problems"; //$NON-NLS-1$
	
	public static final String OVR_DEPRECATED_IMAGE_ID=         NS + "/images/ovr/deprecated"; //$NON-NLS-1$
	
	public static final String LOCTOOL_FILTER_IMAGE_ID=         NS + "/images/loctool/filter_view"; //$NON-NLS-1$
	public static final String LOCTOOLD_FILTER_IMAGE_ID=        NS + "/images/loctoold/filter_view"; //$NON-NLS-1$
	public static final String LOCTOOL_DISABLE_FILTER_IMAGE_ID= NS + "/images/loctool/disable-filter"; //$NON-NLS-1$
	
	public static final String LOCTOOL_SORT_ALPHA_IMAGE_ID=     NS + "/images/loctool/sort-alpha"; //$NON-NLS-1$
	public static final String LOCTOOL_SORT_SCORE_IMAGE_ID=     NS + "/images/loctool/sort-score"; //$NON-NLS-1$
	
	public static final String LOCTOOL_CASESENSITIVE_IMAGE_ID=  NS + "/images/loctool/casesensitive"; //$NON-NLS-1$
	
	public static final String LOCTOOL_EXPANDALL_IMAGE_ID=      NS + "/images/loctool/expand.all"; //$NON-NLS-1$
	public static final String LOCTOOL_COLLAPSEALL_IMAGE_ID=    NS + "/images/loctool/collapse.all"; //$NON-NLS-1$
	
	public static final String TOOL_SCROLLLOCK_IMAGE_ID=        NS + "/images/tool/scrolllock"; //$NON-NLS-1$
	public static final String TOOL_SCROLLLOCK_DISABLED_IMAGE_ID= NS + "/images/tool/scrolllock$d"; //$NON-NLS-1$
	
	public static final String LOCTOOL_SYNC_EDITOR_IMAGE_ID=    NS + "/images/loctool/sync-editor"; //$NON-NLS-1$
	
	public static final String LOCTOOL_FAVORITES_IMAGE_ID=      NS + "/images/loctool/favorites"; //$NON-NLS-1$
	
	public static final String LOCTOOL_CLOSETRAY_IMAGE_ID=      NS + "/images/loctool/close"; //$NON-NLS-1$
	public static final String LOCTOOL_CLOSETRAY_H_IMAGE_ID=    NS + "/images/loctoolh/close"; //$NON-NLS-1$
	
	public static final String LOCTOOL_LEFT_IMAGE_ID=           NS + "/images/loctool/left"; //$NON-NLS-1$
	public static final String LOCTOOL_LEFT_H_IMAGE_ID=         NS + "/images/loctoolh/left"; //$NON-NLS-1$
	public static final String LOCTOOL_RIGHT_IMAGE_ID=          NS + "/images/loctool/right"; //$NON-NLS-1$
	public static final String LOCTOOL_RIGHT_H_IMAGE_ID=        NS + "/images/loctoolh/right"; //$NON-NLS-1$
	
	public static final String LOCTOOL_UP_IMAGE_ID=             NS + "/images/loctool/up"; //$NON-NLS-1$
	public static final String LOCTOOL_UP_H_IMAGE_ID=           NS + "/images/loctoolh/up"; //$NON-NLS-1$
	public static final String LOCTOOL_DOWN_IMAGE_ID=           NS + "/images/loctool/down"; //$NON-NLS-1$
	public static final String LOCTOOL_DOWN_H_IMAGE_ID=         NS + "/images/loctoolh/down"; //$NON-NLS-1$
	
	public static final String LOCTOOL_CHANGE_PAGE_IMAGE_ID=    NS + "/images/loctool/change_page"; //$NON-NLS-1$
	public static final String LOCTOOL_PIN_PAGE_IMAGE_ID=       NS + "/images/loctool/pin_page"; //$NON-NLS-1$
	public static final String LOCTOOLD_PIN_PAGE_IMAGE_ID=      NS + "/images/loctoold/pin_page"; //$NON-NLS-1$
	
	
	/**
	 * The image registry of ECommonsUI
	 * 
	 * @return the image registry
	 */
	public static ImageRegistry getImages() {
		return UIMiscellanyPlugin.getInstance().getImageRegistry();
	}
	
	
	public static final SharedUIResources INSTANCE= new SharedUIResources();
	
	public static SharedUIResources getInstance() {
		return INSTANCE;
	}
	
	
	private final Point iconDefaultSize= new Point(16, 16);
	
	private final ImageDescriptorRegistry imageDescriptorRegistry= new ImageDescriptorRegistry();
	
	
	private SharedUIResources() {
		super(UIMiscellanyPlugin.getInstance().getImageRegistry());
	}
	
	
	public Point getIconDefaultSize() {
		return this.iconDefaultSize;
	}
	
	
	public ImageDescriptorRegistry getImageDescriptorRegistry() {
		return this.imageDescriptorRegistry;
	}
	
}
