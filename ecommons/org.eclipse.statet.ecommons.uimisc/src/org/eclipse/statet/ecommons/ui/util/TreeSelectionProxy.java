/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;


public class TreeSelectionProxy extends StructuredSelectionProxy implements ITreeSelection {
	
	
	public TreeSelectionProxy(final ITreeSelection selection) {
		super(selection);
	}
	
	
	@Override
	public TreePath[] getPathsFor(final Object element) {
		return ((ITreeSelection) fSelection).getPathsFor(element);
	}
	
	@Override
	public TreePath[] getPaths() {
		return ((ITreeSelection) fSelection).getPaths();
	}
	
}
