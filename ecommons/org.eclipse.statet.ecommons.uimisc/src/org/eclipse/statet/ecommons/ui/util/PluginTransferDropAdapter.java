/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.part.PluginDropAdapter;
import org.eclipse.ui.part.PluginTransfer;


public class PluginTransferDropAdapter extends PluginDropAdapter implements TransferDropTargetListener {
	
	
	public PluginTransferDropAdapter(final StructuredViewer viewer) {
		super(viewer);
		setFeedbackEnabled(false);
	}
	
	
	@Override
	public Transfer getTransfer() {
		return PluginTransfer.getInstance();
	}
	
	@Override
	public boolean isEnabled(final DropTargetEvent event) {
		return PluginTransfer.getInstance().isSupportedType(event.currentDataType);
	}
	
}
