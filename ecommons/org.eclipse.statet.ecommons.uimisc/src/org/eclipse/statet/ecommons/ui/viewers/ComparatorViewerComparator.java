/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers;

import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;


/**
 * Viewer Comparator using a common comparator to compare the elements.
 */
public class ComparatorViewerComparator extends ViewerComparator {
	
	
	private final Comparator comparator;
	
	
	public ComparatorViewerComparator(final Comparator comparator) {
		this.comparator= comparator;
	}
	
	
	@Override
	public int compare(final Viewer viewer, final Object e1, final Object e2) {
		return this.comparator.compare(e1, e2);
	}
	
	@Override
	public void sort(final Viewer viewer, final Object[] elements) {
		Arrays.sort(elements, this.comparator);
	}
	
}
