/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class AbstractPostSelectionProvider implements IPostSelectionProvider {
	
	
	private final CopyOnWriteIdentityListSet<ISelectionChangedListener> selectionListeners=
			new CopyOnWriteIdentityListSet<>();
	
	private final CopyOnWriteIdentityListSet<ISelectionChangedListener> postSelectionListeners=
			new CopyOnWriteIdentityListSet<>();
	
	
	public AbstractPostSelectionProvider() {
	}
	
	
	@Override
	public void addSelectionChangedListener(final ISelectionChangedListener listener) {
		this.selectionListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeSelectionChangedListener(final ISelectionChangedListener listener) {
		this.selectionListeners.remove(listener);
	}
	
	
	@Override
	public void addPostSelectionChangedListener(final ISelectionChangedListener listener) {
		this.postSelectionListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removePostSelectionChangedListener(final ISelectionChangedListener listener) {
		this.postSelectionListeners.remove(listener);
	}
	
	
	protected void fireSelectionChanged(final SelectionChangedEvent event) {
		for (final ISelectionChangedListener listener : this.selectionListeners) {
			SafeRunnable.run(() -> listener.selectionChanged(event));
		}
	}
	
	protected void firePostSelectionChanged(final SelectionChangedEvent event) {
		for (final ISelectionChangedListener listener : this.postSelectionListeners) {
			SafeRunnable.run(() -> listener.selectionChanged(event));
		}
	}
	
}
