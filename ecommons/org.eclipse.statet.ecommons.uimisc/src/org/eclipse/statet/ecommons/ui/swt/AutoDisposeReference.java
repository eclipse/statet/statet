/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.swt;

import org.eclipse.swt.graphics.Resource;
import org.eclipse.swt.widgets.Widget;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class AutoDisposeReference<TResource> implements AutoCloseable {
	
	
	public static <T extends Resource> AutoDisposeReference<T> autoDispose(final T resource) {
		return new AutoDisposeReference<>(resource) {
			@Override
			public void close() {
				get().dispose();
			}
		};
	}
	
	public static <T extends Widget> AutoDisposeReference<T> autoDispose(final T resource) {
		return new AutoDisposeReference<>(resource) {
			@Override
			public void close() {
				get().dispose();
			}
		};
	}
	
	
	private final TResource resource;
	
	
	private AutoDisposeReference(final TResource resource) {
		this.resource= resource;
	}
	
	
	public final TResource get() {
		return this.resource;
	}
	
	@Override
	public abstract void close();
	
}
