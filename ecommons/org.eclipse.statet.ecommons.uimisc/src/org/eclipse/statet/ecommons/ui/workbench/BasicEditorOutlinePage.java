/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench;

import static org.eclipse.ui.IWorkbenchCommandConstants.NAVIGATE_COLLAPSE_ALL;
import static org.eclipse.ui.IWorkbenchCommandConstants.NAVIGATE_EXPAND_ALL;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.ui.actions.UIActions.VIEW_EXPAND_GROUP_ID;
import static org.eclipse.statet.ecommons.ui.actions.UIActions.VIEW_FILTER_GROUP_ID;
import static org.eclipse.statet.ecommons.ui.actions.UIActions.VIEW_SORT_GROUP_ID;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler2;
import org.eclipse.core.expressions.EvaluationContext;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISources;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.menus.UIElement;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.services.IServiceLocator;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.ui.actions.CollapseAllHandler;
import org.eclipse.statet.ecommons.ui.actions.ExpandAllHandler;
import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * Abstract content outline page.
 */
@NonNullByDefault
public abstract class BasicEditorOutlinePage extends Page
		implements IContentOutlinePage, IAdaptable, IPostSelectionProvider {
	
	
	private static class SelectionChangeNotify extends SafeRunnable implements ISelectionChangedListener {
		
		
		private final CopyOnWriteIdentityListSet<ISelectionChangedListener> selectionListeners;
		
		private SelectionChangedEvent currentEvent;
		private ISelectionChangedListener currentListener;
		
		
		public SelectionChangeNotify(final CopyOnWriteIdentityListSet<ISelectionChangedListener> listenerList) {
			this.selectionListeners= listenerList;
		}
		
		
		@Override
		public void selectionChanged(final SelectionChangedEvent event) {
			this.currentEvent= event;
			for (final ISelectionChangedListener listener : this.selectionListeners.toList()) {
				this.currentListener= listener;
				SafeRunner.run(this);
			}
			this.currentEvent= null;
			this.currentListener= null;
		}
		
		@Override
		public void run() {
			this.currentListener.selectionChanged(this.currentEvent);
		}
		
	}
	
	private class DefaultSelectionListener implements ISelectionChangedListener {
		
		@Override
		public void selectionChanged(final SelectionChangedEvent event) {
			if (BasicEditorOutlinePage.this.ignoreSelection == 0) {
				selectInEditor(event.getSelection());
			}
		}
		
	}
	
	protected abstract class AbstractToggleHandler extends AbstractHandler implements IElementUpdater {
		
		private final String settingsKey;
		private final String commandId;
		private final int time;
		
		private boolean isSettingEnabled;
		
		public AbstractToggleHandler(final String checkSettingsKey, final boolean checkSettingsDefault, 
				final String commandId, final int expensive) {
			this.settingsKey= checkSettingsKey;
			this.commandId= commandId;
			this.time= expensive;
			
			final IDialogSettings settings= getDialogSettings();
			final boolean on= (settings.get(this.settingsKey) == null) ?
					checkSettingsDefault : getDialogSettings().getBoolean(this.settingsKey);
			this.isSettingEnabled= on;
			apply(on);
		}
		
		protected void init() {
		}
		
		@Override
		public void updateElement(final UIElement element, final Map parameters) {
			WorkbenchUIUtils.aboutToUpdateCommandsElements(this, element);
			try {
				element.setChecked(isChecked());
			}
			finally {
				WorkbenchUIUtils.finalizeUpdateCommandsElements(this);
			}
		}
		
		public boolean isChecked() {
			return this.isSettingEnabled;
		}
		
		@Override
		public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
			final boolean on= !this.isSettingEnabled;
			this.isSettingEnabled= on;
			final Runnable runnable= new Runnable() {
				@Override
				public void run() {
					apply(on);
					getDialogSettings().put(AbstractToggleHandler.this.settingsKey, on);
				}
			};
			if (this.time == 0) {
				runnable.run();
			}
			else {
				BusyIndicator.showWhile(Display.getCurrent(), runnable);
			}
			if (this.commandId != null) {
				WorkbenchUIUtils.refreshCommandElements(this.commandId, this, null);
			}
			return null;
		}
		
		protected abstract void apply(final boolean on);
		
	}
	
	
	private @Nullable TreeViewer treeViewer;
	private @Nullable ISelection currentSelection;
	
	private final CopyOnWriteIdentityListSet<ISelectionChangedListener> selectionListeners= new CopyOnWriteIdentityListSet<>();
	private final ISelectionChangedListener selectionListener= new SelectionChangeNotify(this.selectionListeners);
	private final CopyOnWriteIdentityListSet<ISelectionChangedListener> postSelectionListeners= new CopyOnWriteIdentityListSet<>();
	private final ISelectionChangedListener postSelectionListener= new SelectionChangeNotify(this.postSelectionListeners);
	private int ignoreSelection;
	
	private final String contextMenuID;
	private @Nullable Menu contextMenu;
	
	private ContextHandlers handlers;
	private final CopyOnWriteIdentityListSet<IHandler2> handlersToUpdate= new CopyOnWriteIdentityListSet<>();
	
	
	public BasicEditorOutlinePage(final String contextMenuId) {
		this.contextMenuID= contextMenuId;
	}
	
	
	@Override
	public void init(final IPageSite pageSite) {
		super.init(pageSite);
		pageSite.setSelectionProvider(this);
	}
	
	protected abstract IDialogSettings getDialogSettings();
	
	
	protected @Nullable StructuredViewer getViewer() {
		return this.treeViewer;
	}
	
	@Override
	public void createControl(final Composite parent) {
		final TreeViewer viewer= new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		
		viewer.setUseHashlookup(true);
		configureViewer(viewer);
		ColumnViewerToolTipSupport.enableFor(viewer);
		
		this.treeViewer= viewer;
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				BasicEditorOutlinePage.this.currentSelection= event.getSelection();
			}
		});
		final IPageSite site= getSite();
		this.handlers= new ContextHandlers(site);
		initActions(site, this.handlers);
		this.selectionListeners.add(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				final Control control= getControl();
				if (control != null && control.isVisible()) {
					final EvaluationContext evaluationContext= new EvaluationContext(null, event.getSelection());
					evaluationContext.addVariable(ISources.ACTIVE_SITE_NAME, site);
					evaluationContext.addVariable(ISources.ACTIVE_CURRENT_SELECTION_NAME, event.getSelection());
					for (final IHandler2 handler : BasicEditorOutlinePage.this.handlersToUpdate.toList()) {
						handler.setEnabled(evaluationContext);
					}
				}
			}
		});
		
		contributeToActionBars(site, site.getActionBars(), this.handlers);
		hookContextMenu();
		
		init();
	}
	
	private void hookContextMenu() {
		final MenuManager menuManager= new MenuManager(this.contextMenuID, this.contextMenuID);
		menuManager.setRemoveAllWhenShown(true);
		menuManager.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(final IMenuManager m) {
				contextMenuAboutToShow(m);
			}
		});
		final StructuredViewer viewer= nonNullAssert(getViewer());
		final Menu contextMenu= menuManager.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(contextMenu);
		this.contextMenu= contextMenu;
		getSite().registerContextMenu(this.contextMenuID, menuManager, viewer);
	}
	
	@Override
	public @Nullable Control getControl() {
		final TreeViewer viewer= this.treeViewer;
		if (viewer != null) {
			return viewer.getControl();
		}
		return null;
	}
	
	@Override
	public void setFocus() {
		final StructuredViewer viewer= getViewer();
		if (viewer == null || !UIAccess.isOkToUse(viewer.getControl())) {
			return;
		}
		viewer.getControl().setFocus();
	}
	
	protected abstract void configureViewer(TreeViewer viewer);
	
	protected void init() {
	}
	
	protected void beginIgnoreSelection() {
		this.ignoreSelection++;
	}
	
	protected void endIgnoreSelection(final boolean async) {
		if (async) {
			Display.getCurrent().asyncExec(new Runnable() {
				@Override
				public void run() {
					BasicEditorOutlinePage.this.ignoreSelection--;
				}
			});
		}
		else {
			this.ignoreSelection--;
		}
	}
	
	protected void initActions(final IServiceLocator serviceLocator,
			final ContextHandlers handlers) {
		final StructuredViewer viewer= nonNullAssert(getViewer());
		this.postSelectionListeners.add(new DefaultSelectionListener());
		viewer.addSelectionChangedListener(this.selectionListener);
		viewer.addPostSelectionChangedListener(this.postSelectionListener);
		
		if (viewer instanceof final AbstractTreeViewer treeViewer) {
			{	final CollapseAllHandler handler= new CollapseAllHandler(treeViewer) {
					@Override
					public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
						beginIgnoreSelection();
						try {
							return super.execute(event);
						}
						finally {
							endIgnoreSelection(true);
						}
					}
				};
				handlers.addActivate(NAVIGATE_COLLAPSE_ALL, handler);
			}
			{	final ExpandAllHandler handler= new ExpandAllHandler(treeViewer);
				handlers.addActivate(NAVIGATE_EXPAND_ALL, handler);
			}
		}
	}
	
	protected void contributeToActionBars(final IServiceLocator serviceLocator,
			final IActionBars actionBars, final HandlerCollection handlers) {
		final IToolBarManager toolBarManager= actionBars.getToolBarManager();
		
		toolBarManager.add(new Separator(VIEW_EXPAND_GROUP_ID));
		toolBarManager.appendToGroup(VIEW_EXPAND_GROUP_ID, new HandlerContributionItem(
				new CommandContributionItemParameter(serviceLocator,
						null, NAVIGATE_COLLAPSE_ALL, HandlerContributionItem.STYLE_PUSH ),
				handlers ));
		toolBarManager.add(new Separator(VIEW_SORT_GROUP_ID));
		final Separator viewFilter= new Separator(VIEW_FILTER_GROUP_ID);
		viewFilter.setVisible(false);
		toolBarManager.add(viewFilter);
	}
	
	protected void contextMenuAboutToShow(final IMenuManager m) {
	}
	
	
	@Override
	public void dispose() {
		this.handlers.dispose();
		this.handlersToUpdate.clear();
		this.postSelectionListeners.clear();
		
		final Menu contextMenu= this.contextMenu;
		if (contextMenu != null && !contextMenu.isDisposed()) {
			this.contextMenu= null;
			contextMenu.dispose();
		}
	}
	
	
	protected abstract void selectInEditor(final ISelection selection);
	
	protected void registerHandlerToUpdate(final IHandler2 handler) {
		this.handlersToUpdate.add(handler);
	}
	
	
	@Override
	public void setSelection(final ISelection selection) {
		final StructuredViewer viewer= getViewer();
		if (viewer == null || !UIAccess.isOkToUse(viewer.getControl())) {
			return;
		}
		viewer.setSelection(selection);
	}
	
	@Override
	public ISelection getSelection() {
		final ISelection selection= this.currentSelection;
		if (selection != null) {
			return selection;
		}
		final TreeViewer viewer= this.treeViewer;
		if (viewer != null) {
			return viewer.getSelection();
		}
		return TreeSelection.EMPTY;
	}
	
	
	@Override
	public void addSelectionChangedListener(final ISelectionChangedListener listener) {
		this.selectionListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeSelectionChangedListener(final ISelectionChangedListener listener) {
		this.selectionListeners.remove(listener);
	}
	
	@Override
	public void addPostSelectionChangedListener(final ISelectionChangedListener listener) {
		this.postSelectionListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removePostSelectionChangedListener(final ISelectionChangedListener listener) {
		this.postSelectionListeners.remove(listener);
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return null;
	}
	
}
