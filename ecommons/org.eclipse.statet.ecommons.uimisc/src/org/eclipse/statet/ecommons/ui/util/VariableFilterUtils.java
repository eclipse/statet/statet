/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.debug.ui.StringVariableSelectionDialog.VariableFilter;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;


public class VariableFilterUtils {
	
	/**
	 * Variable filter excluding known variables, which requires interaction from the user,
	 * including selection in the UI.
	 */
	public static final VariableFilter EXCLUDE_INTERACTIVE_FILTER= new VariableFilter() {
		@Override
		public boolean isFiltered(final IDynamicVariable variable) {
			final String variableName= variable.getName();
			return (variableName.startsWith("selected_") //$NON-NLS-1$
					|| variableName.endsWith("_prompt") ); //$NON-NLS-1$
		}
	};
	
	/**
	 * Variable filter excluding known variables from Eclipse Development Tool for Java like JDT.
	 */
	public static final VariableFilter EXCLUDE_JAVA_FILTER= new VariableFilter() {
		@Override
		public boolean isFiltered(final IDynamicVariable variable) {
			final String variableName= variable.getName();
			return (variableName.startsWith("java_") //$NON-NLS-1$
					|| variableName.startsWith("target_home") //$NON-NLS-1$
					|| variableName.startsWith("tptp_junit") ); //$NON-NLS-1$
		}
	};
	
	/**
	 * Variable filter excluding known variables, which are only valid in builds.
	 */
	public static final VariableFilter EXCLUDE_BUILD_FILTER= new VariableFilter() {
		@Override
		public boolean isFiltered(final IDynamicVariable variable) {
			final String variableName= variable.getName();
			return (variableName.startsWith("build_")); //$NON-NLS-1$
		}
	};
	
	/**
	 * Variable filter excluding known variables for path location.
	 */
	public static final VariableFilter EXCLUDE_LOC_FILTER= new VariableFilter() {
		@Override
		public boolean isFiltered(final IDynamicVariable variable) {
			final String variableName= variable.getName();
			return (variableName.endsWith("_loc")); //$NON-NLS-1$
		}
	};
	
	
	/**
	 * Common set of filters for use cases, in which user interaction is possible.
	 */
	public static final ImList<VariableFilter> DEFAULT_INTERACTIVE_FILTERS= ImCollections.newList(
			EXCLUDE_JAVA_FILTER,
			EXCLUDE_BUILD_FILTER );
	
	/**
	 * Common set of filters for use cases, in which user interaction is possible, for workspace resources.
	 */
	public static final ImList<VariableFilter> DEFAULT_INTERACTIVE_RESOURCE_FILTERS= ImCollections.newList(
			EXCLUDE_JAVA_FILTER,
			EXCLUDE_BUILD_FILTER,
			EXCLUDE_LOC_FILTER );
	
	/**
	 * Common set of filters for use cases, in which user interaction is not possible.
	 */
	public static final ImList<VariableFilter> DEFAULT_NON_ITERACTIVE_FILTERS= ImCollections.newList(
			EXCLUDE_JAVA_FILTER,
			EXCLUDE_BUILD_FILTER,
			EXCLUDE_INTERACTIVE_FILTER );
	
	
	private VariableFilterUtils() {}
	
}
