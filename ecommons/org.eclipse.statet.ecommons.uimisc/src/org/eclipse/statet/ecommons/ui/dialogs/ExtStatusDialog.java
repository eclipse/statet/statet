/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.dialogs;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.StatusDialog;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.operation.ModalContext;
import org.eclipse.jface.util.Policy;
import org.eclipse.jface.util.Util;
import org.eclipse.jface.wizard.ProgressMonitorPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.ui.swt.ControlEnableStates;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


@NonNullByDefault
public class ExtStatusDialog extends StatusDialog implements IRunnableContext {
	
	
	protected static final int WITH_RUNNABLE_CONTEXT= 1 << 0;
	protected static final int WITH_DATABINDING_CONTEXT= 1 << 1;
	protected static final int SHOW_INITIAL_STATUS= 1 << 2;
	
	private static final IStatus NO_STATUS= new Status(IStatus.OK, Policy.JFACE, IStatus.OK,
			Util.ZERO_LENGTH_STRING, null );
	
	
	public class StatusUpdater implements StatusChangeListener {
		
		@Override
		public void statusChanged(final IStatus status) {
			updateStatus(status);
		}
		
	}
	
	
	private final int options;
	
	
	/*- WITH_RUNNABLE_CONTEXT -*/
	
	private Composite progressComposite= nonNullLateInit();
	private ProgressMonitorPart progressMonitorPart= nonNullLateInit();
	
	private int activeRunningOperations;
	
	private @Nullable Control progressLastFocusControl;
	private @Nullable ControlEnableStates progressLastContentEnableState;
	
	
	/*- WITH_DATABINDING_CONTEXT -*/
	
	private DataBindingSupport dataBinding= nonNullLateInit();
	
	
	/**
	 * @see StatusDialog#StatusDialog(Shell)
	 */
	public ExtStatusDialog(final @Nullable Shell parent) {
		this(parent, 0);
	}
	
	/**
	 * @see StatusDialog#StatusDialog(Shell)
	 * 
	 * @param options
	 * @see #WITH_RUNNABLE_CONTEXT
	 * @see #WITH_DATABINDING_CONTEXT
	 * @see #SHOW_INITIAL_STATUS
	 */
	public ExtStatusDialog(final @Nullable Shell parent, final int options) {
		super(parent);
		this.options= options;
	}
	
	
	@Override
	protected boolean isResizable() {
		return true;
	}
	
	@Override
	protected Point getInitialSize() {
		final Point savedSize= super.getInitialSize();
		final Point minSize= getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT);
		return new Point(Math.max(savedSize.x, minSize.x), Math.max(savedSize.y, minSize.y));
	}
	
	@Override
	public void create() {
		// E-3.6 Eclipse bug fixed?
		super.create();
		final Button button= getButton(IDialogConstants.OK_ID);
		final Shell shell= getShell();
		if (button != null && shell != null && !shell.isDisposed()) {
			shell.setDefaultButton(button);
		}
		
		if ((this.options & WITH_DATABINDING_CONTEXT) != 0) {
			initBindings();
		}
	}
	
	protected void initBindings() {
		final DataBindingSupport databinding= new DataBindingSupport(getDialogArea());
		addBindings(databinding);
		databinding.installStatusListener(new StatusUpdater());
		if ((this.options & SHOW_INITIAL_STATUS) == 0) {
			final IStatus status= getStatus();
			updateStatus(Status.OK_STATUS);
			updateButtonsEnableState(status);
		}
		this.dataBinding= databinding;
	}
	
	protected void addBindings(final DataBindingSupport db) {
	}
	
	protected DataBindingSupport getDataBinding() {
		return this.dataBinding;
	}
	
	@Override
	protected Control createButtonBar(final Composite parent) {
		final Composite composite= (Composite)super.createButtonBar(parent);
		final GridLayout layout= (GridLayout)composite.getLayout();
		layout.verticalSpacing= 0;
		
		if ((this.options & WITH_RUNNABLE_CONTEXT) != 0) {
			final Composite monitorComposite= createMonitorComposite(composite);
			final Control[] children= composite.getChildren();
			layout.numColumns= 3;
			((GridData)children[0].getLayoutData()).horizontalSpan++;
			monitorComposite.moveBelow(children[1]);
			monitorComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		}
		
		return composite;
	}
	
	private Composite createMonitorComposite(final Composite parent) {
		this.progressComposite= new Composite(parent, SWT.NULL);
		final GridLayout layout= LayoutUtils.newCompositeGrid(2);
		layout.marginLeft= LayoutUtils.defaultHMargin();
		this.progressComposite.setLayout(layout);
		
		this.progressMonitorPart= new ProgressMonitorPart(this.progressComposite, null, true);
		this.progressMonitorPart.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		
		Dialog.applyDialogFont(this.progressComposite);
		this.progressComposite.setVisible(false);
		return this.progressComposite;
	}
	
	@Override
	public void run(final boolean fork, final boolean cancelable,
			final IRunnableWithProgress runnable) throws InvocationTargetException, InterruptedException {
		if ((this.options & WITH_RUNNABLE_CONTEXT) == 0) {
			throw new UnsupportedOperationException();
		}
		if (getShell() != null && getShell().isVisible()) {
			if (this.activeRunningOperations == 0) {
				// Save control state
				Control focusControl= getShell().getDisplay().getFocusControl();
				if (focusControl != null && focusControl.getShell() != getShell()) {
					focusControl= null;
				}
				this.progressLastFocusControl= focusControl;
				
				final List<Control> disable= new ArrayList<>();
				disable.add(nonNullAssert(getDialogArea()));
				for (final Control child : getButton(IDialogConstants.OK_ID).getParent().getChildren()) {
					if (child instanceof Button) {
						disable.add(child);
					}
				}
				this.progressLastContentEnableState= ControlEnableStates.disable(disable);
				
				// Enable monitor
				this.progressMonitorPart.attachToCancelComponent(null);
				this.progressComposite.setVisible(true);
			}
			
			this.activeRunningOperations++;
			try {
				ModalContext.run(runnable, fork, this.progressMonitorPart, getShell().getDisplay());
			} 
			finally {
				this.activeRunningOperations--;
				
				if (this.activeRunningOperations == 0 && getShell() != null) {
					this.progressComposite.setVisible(false);
					this.progressMonitorPart.removeFromCancelComponent(null);
					
					final ControlEnableStates contentEnableState= this.progressLastContentEnableState;
					if (contentEnableState != null) {
						this.progressLastContentEnableState= null;
						contentEnableState.restore();
					}
					
					final Control focusControl= this.progressLastFocusControl;
					if (focusControl != null) {
						this.progressLastFocusControl= null;
						focusControl.setFocus();
					}
				}
			}
		}
		else {
			PlatformUI.getWorkbench().getProgressService().run(fork, cancelable, runnable);
		}
	}
	
	protected boolean isOperationRunning() {
		return (this.activeRunningOperations > 0);
	}
	
	
	protected void clearStatus() {
		updateStatus(NO_STATUS);
	}
	
	@Override
	protected void updateButtonsEnableState(final IStatus status) {
		if (isOperationRunning()) {
			final ControlEnableStates contentEnableState= this.progressLastContentEnableState;
			final Button okButton= getButton(IDialogConstants.OK_ID);
			if (contentEnableState != null
					&& okButton != null && !okButton.isDisposed() ) {
				contentEnableState.updateState(okButton, false, !status.matches(IStatus.ERROR));
			}
		}
		else {
			super.updateButtonsEnableState(status);
		}
	}
	
}
