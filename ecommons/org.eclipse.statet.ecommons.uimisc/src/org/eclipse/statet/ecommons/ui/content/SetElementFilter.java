/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.content;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.statet.jcommons.collections.ImCollections;


public class SetElementFilter implements IElementFilter {
	
	
	private class Filter implements IFinalFilter {
		
		
		private final Collection<?> set;
		
		
		Filter(final Collection<?> set) {
			this.set= set;
		}
		
		
		@Override
		public boolean select(final Object element) {
			return SetElementFilter.this.select(this.set, element);
		}
		
		@Override
		public boolean isSubOf(final IFinalFilter other) {
			return (other == this || other == null || ((Filter) other).set.containsAll(this.set));
		}
		
		@Override
		public boolean isEqualTo(final IFinalFilter other) {
			return (other == this || ((other instanceof Filter)
					&& this.set.equals((((Filter) other).set)) ));
		}
		
	}
	
	
	private Collection<?> set;
	private boolean changed;
	
	private Filter currentFilter;
	
	
	public SetElementFilter() {
		this.set= Collections.EMPTY_LIST;
	}
	
	
	public synchronized boolean setSet(/*Im*/Collection<?> set) {
		if (set == null || set.isEmpty()) {
			set= ImCollections.emptySet();
		}
		this.changed|= !this.set.equals(set);
		this.set= set;
		return this.changed;
	}
	
	@Override
	public synchronized IFinalFilter getFinal(final boolean newData) {
		if (this.set.isEmpty()) {
			this.currentFilter= null;
		}
		else if (this.currentFilter == null
				|| (newData && this.currentFilter.set != this.set)
				|| (this.changed && this.currentFilter.set != this.set) ) {
			this.currentFilter= new Filter(this.set);
		}
		this.changed= false;
		return this.currentFilter;
	}
	
	
	protected boolean select(final Collection<?> set, final Object element) {
		return set.contains(element.toString());
	}
	
}
