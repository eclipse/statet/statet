/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.views.navigator.ResourceComparator;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.SharedMessages;
import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.components.StatusInfo;
import org.eclipse.statet.ecommons.ui.components.WidgetToolBarComposite;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils;
import org.eclipse.statet.internal.ecommons.ui.Messages;


/**
 * Workbench-level composite for choosing a container.
 */
public class ContainerSelectionComposite extends Composite {
	
	
	private static final int SIZING_SELECTION_PANE_WIDTH= 320;
//	private static final int SIZING_SELECTION_PANE_HEIGHT= 300;
	
	
	public static abstract class ContainerFilter extends ViewerFilter {
		
		private IPath excludePath;
		
		@Override
		public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
			if (!(element instanceof IContainer)) {
				return true; // never
			}
			
			final IContainer container= (IContainer) element;
			if (this.excludePath != null) {
				if (container.getFullPath().isPrefixOf(this.excludePath)) {
					return true;
				}
			}
			
			return select(container);
		}
		
		public abstract boolean select(IContainer container);
		
	}
	
	
	private class CollapseAllAction extends Action {
		
		CollapseAllAction() {
			super();
			setText(SharedMessages.CollapseAllAction_label); 
			setDescription(SharedMessages.CollapseAllAction_description); 
			setToolTipText(SharedMessages.CollapseAllAction_tooltip); 
			setImageDescriptor(SharedUIResources.getImages().getDescriptor(SharedUIResources.LOCTOOL_COLLAPSEALL_IMAGE_ID));
		}
		
		@Override
		public void run() {
			ContainerSelectionComposite.this.treeViewer.collapseAll();
		}
		
	}
	
	private class ExpandAllAction extends Action {
		
		ExpandAllAction() {
			super();
			setText(SharedMessages.ExpandAllAction_label); 
			setDescription(SharedMessages.ExpandAllAction_description); 
			setToolTipText(SharedMessages.ExpandAllAction_tooltip); 
			setImageDescriptor(SharedUIResources.getImages().getDescriptor(SharedUIResources.LOCTOOL_EXPANDALL_IMAGE_ID));
		}
		
		@Override
		public void run() {
			ContainerSelectionComposite.this.treeViewer.expandAll();
		}
		
	}
	
	private class ToggleFilterAction extends Action {
		
		ToggleFilterAction() {
			super();
			setText(Messages.FilterFavouredContainersAction_label); 
			setDescription(Messages.FilterFavouredContainersAction_description); 
			setToolTipText(Messages.FilterFavouredContainersAction_tooltip); 
			setImageDescriptor(SharedUIResources.getImages().getDescriptor(SharedUIResources.LOCTOOL_FILTER_IMAGE_ID));
			setDisabledImageDescriptor(SharedUIResources.getImages().getDescriptor(SharedUIResources.LOCTOOLD_FILTER_IMAGE_ID));
			setChecked(false);
		}
		
		@Override
		public void run() {
			final boolean enable= isChecked();
			doToggleFilter(enable);
			ContainerSelectionComposite.this.isToggleFilterActivated= enable;
		}
		
		void doToggleFilter(final boolean enable) {
			if (enable) {
				ContainerSelectionComposite.this.treeViewer.addFilter(ContainerSelectionComposite.this.toggledFilter);
			}
			else {
				ContainerSelectionComposite.this.treeViewer.removeFilter(ContainerSelectionComposite.this.toggledFilter);
			}
		}
		
	}
	
	
	// Enable user to type in new container name
	private boolean allowNewContainerName= true;
	
	private boolean showClosedProjects= true;
	
	private IContainer selectedContainer;
	
	private Text containerNameField;
	private TreeViewer treeViewer;
	private ToolBarManager rightToolBarMgr;
	
	private boolean isToggleFilterActivated;
	private ContainerFilter toggledFilter;
	
	// The listener to notify of events
	private Listener listener;
	
	
//	/**
//	 * Creates a new instance of the widget.
//	 * 
//	 * @param parent The parent widget of the group.
//	 * @param allowNewContainerName Enable the user to type in a new container
//	 *     name instead of just selecting from the existing ones.
//	 * @param message The text to present to the user.
//	 * @param showClosedProjects Whether or not to show closed projects.
//	 */
//	public ContainerSelectionControl(final Composite parent,
//			final boolean allowNewContainerName, final boolean showClosedProjects,
//			final String message) {
//		this(parent, allowNewContainerName, showClosedProjects, message,
//				SIZING_SELECTION_PANE_HEIGHT );
//	}
	
	/**
	 * Creates a new instance of the widget.
	 * 
	 * @param parent The parent widget of the group.
	 * @param allowNewContainerName Enable the user to type in a new container
	 *     name instead of just selecting from the existing ones.
	 * @param message The text to present to the user.
	 * @param showClosedProjects Whether or not to show closed projects.
	 * @param heightHint height hint for the drill down composite
	 */
	public ContainerSelectionComposite(final Composite parent,
			final boolean allowNewContainerName, final boolean showClosedProjects,
			@Nullable String message,
			final int heightHint) {
		super(parent, SWT.NONE);
		this.allowNewContainerName= allowNewContainerName;
		this.showClosedProjects= showClosedProjects;
		if (message == null) {
			if (allowNewContainerName) {
				message= Messages.ContainerSelectionControl_label_EnterOrSelectFolder;
			}
			else {
				message= Messages.ContainerSelectionControl_label_SelectFolder;
			}
		}
		createContents(message, heightHint);
	}
	
	/**
	 * Creates the contents of the composite.
	 * 
	 * @param heightHint height hint for the drill down composite
	 */
	protected void createContents(final String message, final int heightHint) {
		setLayout(LayoutUtils.newCompositeGrid());
		
		final Label label= new Label(this, SWT.WRAP);
		label.setText(message);
		
		if (this.allowNewContainerName) {
			this.containerNameField= new Text(this, SWT.SINGLE | SWT.BORDER);
			this.containerNameField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			this.containerNameField.setFont(getFont());
		}
		else {
			// filler - required?
			new Label(this, SWT.NONE);
		}
		
		createTreeViewer(heightHint);
		
		Dialog.applyDialogFont(this);
		
		this.treeViewer.setInput(ResourcesPlugin.getWorkspace());
	}
	
	/**
	 * Returns a new drill down viewer for this dialog.
	 * 
	 * @param heightHint height hint for the drill down composite
	 * @return a new drill down viewer
	 */
	protected void createTreeViewer(final int heightHint) {
		final WidgetToolBarComposite treeGroup= new WidgetToolBarComposite(this, SWT.BORDER);
		{	final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, true);
			gd.widthHint= SIZING_SELECTION_PANE_WIDTH;
			gd.heightHint= heightHint;
			treeGroup.setLayoutData(gd);
		}
		
		final ToolBarManager leftToolBarMgr= new ToolBarManager(treeGroup.getLeftToolBar());
		
		this.rightToolBarMgr= new ToolBarManager(treeGroup.getRightToolBar());
		
		this.treeViewer= new TreeViewer(treeGroup, SWT.NONE);
		this.treeViewer.getTree().setLayoutData(treeGroup.getContentLayoutData());
		
		// toolbars
		final DrillDownAdapter adapter= new DrillDownAdapter(this.treeViewer);
		adapter.addNavigationActions(leftToolBarMgr);
		
		this.rightToolBarMgr.add(new CollapseAllAction());
		this.rightToolBarMgr.add(new ExpandAllAction());
		
		leftToolBarMgr.update(true);
		this.rightToolBarMgr.update(true);
		
		// layout group
		treeGroup.layout();
		
		this.treeViewer.setUseHashlookup(true);
		final ContainerContentProvider cp= new ContainerContentProvider();
		cp.showClosedProjects(this.showClosedProjects);
		this.treeViewer.setContentProvider(cp);
		this.treeViewer.setComparator(new ResourceComparator(ResourceComparator.NAME));
		this.treeViewer.setLabelProvider(WorkbenchLabelProvider.getDecoratingWorkbenchLabelProvider());
		this.treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				final IStructuredSelection selection= (IStructuredSelection) event.getSelection();
				containerSelectionChanged((IContainer) selection.getFirstElement());
			}
		});
		ViewerUtils.addDoubleClickExpansion(this.treeViewer);
	}
	
	public void setToggleFilter(final ContainerFilter filter, final boolean initialEnabled) {
		this.toggledFilter= filter;
		this.isToggleFilterActivated= initialEnabled;
		final ToggleFilterAction action= new ToggleFilterAction();
		this.rightToolBarMgr.add(new Separator());
		this.rightToolBarMgr.add(action);
		
		// check and init
		action.doToggleFilter(true);
		if (this.treeViewer.getTree().getItemCount() == 0) {
			action.doToggleFilter(false);
			action.setChecked(false);
			action.setEnabled(false);
		} 
		else {
			action.setChecked(initialEnabled);
			if (!initialEnabled) {
				action.doToggleFilter(false);
			}
		}
		
		this.rightToolBarMgr.update(true);
		this.rightToolBarMgr.getControl().getParent().layout();
	}
	
	public boolean getToggleFilterSetting() {
		return this.isToggleFilterActivated;
	}
	
	
	/**
	 * Listener will be notified, if container selection changed.
	 * 
	 * @param listener
	 */
	public void setListener(final Listener listener) {
		this.listener= listener;
		if (this.containerNameField != null) {
			this.containerNameField.addListener(SWT.Modify, this.listener);
		}
	}
	
	
	/**
	 * Gives focus to one of the widgets in the group, as determined by the group.
	 */
	public void setInitialFocus() {
		if (this.allowNewContainerName) {
			this.containerNameField.setFocus();
		}
		else {
			this.treeViewer.getTree().setFocus();
		}
	}
	
	/**
	 * The container selection has changed in the
	 * tree view. Update the container name field
	 * value and notify all listeners.
	 */
	protected void containerSelectionChanged(final @Nullable IContainer container) {
		if (this.allowNewContainerName) {
			if (container != null) {
				this.selectedContainer= container;
				this.containerNameField.setText(container.getFullPath().makeRelative().toString());
			}
		}
		else {
			this.selectedContainer= container;
			// fire an event so the parent can update its controls
			if (this.listener != null) {
				final Event changeEvent= new Event();
				changeEvent.type= SWT.Selection;
				changeEvent.widget= this;
				this.listener.handleEvent(changeEvent);
			}
		}
	}
	
	
	/**
	 * Returns the currently entered container name.
	 * <p>
	 * Note that the container may not exist yet if the user
	 * entered a new container name in the field.</p>
	 * 
	 * @return path of container, <code>null</code> if the field is empty.
	 */
	public @Nullable IPath getContainerFullPath() {
		if (this.allowNewContainerName) {
			final String pathName= this.containerNameField.getText();
			if (pathName == null || pathName.length() < 1) {
				return null;
			}
			else {
				//The user may not have made this absolute so do it for them
				return (new Path(pathName)).makeAbsolute();
			}
		}
		else {
			if (this.selectedContainer == null) {
				return null;
			}
			else {
				return this.selectedContainer.getFullPath();
			}
		}
	}
	
	public void selectContainer(final IContainer container) {
		this.selectedContainer= container;
		
		//expand to and select the specified container
		final List<IContainer> itemsToExpand= new ArrayList<>();
		IContainer parent= container.getParent();
		while (parent != null) {
			itemsToExpand.add(0, parent);
			parent= parent.getParent();
		}
		// update filter
		if (this.toggledFilter != null) {
			this.toggledFilter.excludePath= container.getFullPath();
			this.treeViewer.refresh();
		}
		// update selection
		this.treeViewer.setExpandedElements(itemsToExpand.toArray());
		this.treeViewer.setSelection(new StructuredSelection(container), true);
	}
	
	public void selectContainer(final IPath path) {
		IResource initial= ResourcesPlugin.getWorkspace().getRoot().findMember(path);
		if (initial != null) {
			if (!(initial instanceof IContainer)) {
				initial= initial.getParent();
			}
			selectContainer((IContainer) initial);
		}
	}
	
	
	public static IStatus validate(@Nullable IPath path) {
		// validate Container
		if (path == null || path.isEmpty()) {
			return new StatusInfo(IStatus.ERROR, Messages.ContainerSelectionControl_error_FolderEmpty);
		}
		final IWorkspaceRoot root= ResourcesPlugin.getWorkspace().getRoot();
		final String projectName= path.segment(0);
		if (projectName == null || !root.getProject(projectName).exists()) {
			return new StatusInfo(IStatus.ERROR, Messages.ContainerSelectionControl_error_ProjectNotExists);
		}
		//path is invalid if any prefix is occupied by a file
		while (path.segmentCount() > 1) {
			if (root.getFile(path).exists()) {
				return new StatusInfo(IStatus.ERROR, NLS.bind(
						Messages.ContainerSelectionControl_error_PathOccupied, 
						path.makeRelative() ));
			}
			path= path.removeLastSegments(1);
		}
		return new StatusInfo();
	}
	
}
