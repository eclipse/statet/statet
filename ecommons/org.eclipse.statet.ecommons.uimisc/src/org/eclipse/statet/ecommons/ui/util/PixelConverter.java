/*=============================================================================#
 # Copyright (c) 2000, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.platform: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class PixelConverter {
	
	
	// TODO: check if still required
	private final static double FONT_WS_FACTOR;
	static {
		final String ws = Platform.getWS();
		if (ws.equals(Platform.WS_WIN32)) {
			FONT_WS_FACTOR = 0.95;
		}
		else if (ws.equals(Platform.WS_GTK)) {
			FONT_WS_FACTOR = 1.15;
		}
		else {
			FONT_WS_FACTOR = 1.00;
		}
	}
	
	
	private final FontMetrics fontMetrics;
	private final boolean monospace;
	
	
	public PixelConverter(final Control control) {
		final GC gc = new GC(control);
		try {
			gc.setFont(control.getFont());
			this.fontMetrics= gc.getFontMetrics();
			this.monospace = (gc.stringExtent("i").x == gc.stringExtent("W").x); //$NON-NLS-1$ //$NON-NLS-2$
		}
		finally {
			gc.dispose();
		}
	}
	
	
	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#convertHeightInCharsToPixels(int)
	 */
	public int convertHeightInCharsToPixels(final int chars) {
		return Dialog.convertHeightInCharsToPixels(this.fontMetrics, chars);
	}
	
	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#convertHorizontalDLUsToPixels(int)
	 */
	public int convertHorizontalDLUsToPixels(final int dlus) {
		return Dialog.convertHorizontalDLUsToPixels(this.fontMetrics, dlus);
	}
	
	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#convertVerticalDLUsToPixels(int)
	 */
	public int convertVerticalDLUsToPixels(final int dlus) {
		return Dialog.convertVerticalDLUsToPixels(this.fontMetrics, dlus);
	}
	
	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#convertWidthInCharsToPixels(int)
	 */
	public int convertWidthInCharsToPixels(final int chars) {
		if (this.monospace) {
			return Dialog.convertWidthInCharsToPixels(this.fontMetrics, chars);
		}
		else {
			return (int)(FONT_WS_FACTOR * Dialog.convertWidthInCharsToPixels(this.fontMetrics, chars));
		}
	}	
	
}
