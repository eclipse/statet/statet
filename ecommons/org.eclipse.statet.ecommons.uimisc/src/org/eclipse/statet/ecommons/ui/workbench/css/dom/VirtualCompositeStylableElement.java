/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench.css.dom;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.e4.ui.css.swt.dom.WidgetElement;
import org.eclipse.swt.widgets.Control;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.workbench.css.VirtualComposite;
import org.eclipse.statet.internal.ecommons.ui.swt.css.dom.VirtualCompositeSkinListener;


@NonNullByDefault
@SuppressWarnings("restriction")
public abstract class VirtualCompositeStylableElement<TControl extends Control & VirtualComposite>
		extends WidgetElement implements NodeList {
	
	
	private final ImList<? extends VirtualStylableElement<? extends VirtualCompositeStylableElement<TControl>>> children;
	
	
	public VirtualCompositeStylableElement(final TControl control, final CSSEngine engine) {
		super(control, engine);
		
		this.children= createChildren(control);
		
		control.addDisposeListener((event) -> dispose());
		
		VirtualCompositeSkinListener.enable(control.getDisplay(), engine);
	}
	
	protected abstract ImList<? extends VirtualStylableElement<? extends VirtualCompositeStylableElement<TControl>>> createChildren(
			final TControl control);
	
	@Override
	public void dispose() {
		super.dispose();
		
		for (final var child : this.children) {
			this.engine.handleWidgetDisposed(child);
			child.dispose();
		}
	}
	
	
	@Override
	public NodeList getChildNodes() {
		return this;
	}
	
	@Override
	public int getLength() {
		return this.children.size();
	}
	
	@Override
	public @Nullable Node item(final int index) {
		if (index >= 0 && index < this.children.size()) {
			return this.children.get(index);
		}
		return null;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public TControl getNativeWidget() {
		return (TControl)super.getNativeWidget();
	}
	
	
	@Override
	public String toString() {
		final var sb= new StringBuilder();
		VirtualStylableElement.appendName(sb, this, true);
		return sb.toString();
	}
	
}
