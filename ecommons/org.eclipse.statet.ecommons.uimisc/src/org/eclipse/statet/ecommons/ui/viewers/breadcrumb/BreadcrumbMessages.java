/*=============================================================================#
 # Copyright (c) 2008, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers.breadcrumb;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class BreadcrumbMessages extends NLS {
	
	
	public static String BreadcrumbItemDropDown_showDropDownMenu_action_tooltip;
	
	
	static {
		NLS.initializeMessages(BreadcrumbMessages.class.getName(), BreadcrumbMessages.class);
	}
	private BreadcrumbMessages() {}
	
}
