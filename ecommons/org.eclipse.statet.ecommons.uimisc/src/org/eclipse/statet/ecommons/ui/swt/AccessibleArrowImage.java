/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.swt;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ecommons.ui.swt.AutoDisposeReference.autoDispose;

import java.util.Arrays;

import org.eclipse.jface.resource.CompositeImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageDataProvider;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * An arrow image descriptor, e.g. for drop down buttons.
 * 
 * <p>Using the foreground and background colors of the widget makes the arrow visible even in high
 * contrast too.</p>
 */
@NonNullByDefault
public final class AccessibleArrowImage extends CompositeImageDescriptor {
	
	
	public final static int DEFAULT_SIZE= 5;
	
	
	private final int direction;
	
	private final int size;
	
	private final RGB foregroundColor;
	private final RGB backgroundColor;
	
	
	public AccessibleArrowImage(final int direction, final int size,
			final RGB foregroundColor, final RGB backgroundColor) {
		switch (direction) {
		case SWT.UP:
		case SWT.DOWN:
		case SWT.LEFT:
		case SWT.RIGHT:
			this.direction= direction;
			break;
		default:
			throw new IllegalArgumentException("direction: " + direction); //$NON-NLS-1$
		}
		
		this.foregroundColor= nonNullAssert(foregroundColor);
		this.backgroundColor= nonNullAssert(backgroundColor);
		this.size= (size == SWT.DEFAULT) ? DEFAULT_SIZE : size;
	}
	
	
	@Override
	protected void drawCompositeImage(final int width, final int height) {
		final int xOffset;
		final int yOffset;
		final int imgWidth;
		final int imgHeight;
		switch (this.direction) {
		case SWT.UP:
			xOffset= 0;
			yOffset= 0;
			imgWidth= this.size * 2 - 1;
			imgHeight= this.size;
			break;
		case SWT.DOWN:
			xOffset= 0;
			yOffset= this.size % 2;
			imgWidth= this.size * 2 - 1;
			imgHeight= this.size;
			break;
		case SWT.LEFT:
			xOffset= 0;
			yOffset= 0;
			imgWidth= this.size;
			imgHeight= this.size * 2 - 1;
			break;
		case SWT.RIGHT:
			xOffset= this.size % 2;
			yOffset= 0;
			imgWidth= this.size;
			imgHeight= this.size * 2 - 1;
			break;
		default:
			throw new IllegalStateException();
		}
		
		final ImageDataProvider imageProvider= (final int zoom) -> {
			final Display display= Display.getCurrent();
			
			final ImageData imageData;
			try (final var managedImage= autoDispose(new Image(display, imgWidth, imgHeight))) {
				imageData= managedImage.get().getImageData(zoom);
			}
			
			if (imageData.alphaData == null) {
				imageData.alphaData= new byte[imageData.width * imageData.height];
			}
			else if (imageData.alphaData[0] != 0) {
				Arrays.fill(imageData.alphaData, (byte)0);
			}
			
			final int foreground= imageData.palette.getPixel(
					ColorUtils.blend(this.foregroundColor, this.backgroundColor, 0.80f) );
			final int size1;
			final int size2;
			switch (this.direction) {
			case SWT.UP:
				size1= (imageData.width) / 2 + imageData.width % 2 - 1;
				size2= size1 * 2;
				for (int i= 0; i <= size1; i++) {
					final int fix1= size1 - i;
					final int last2= size2 - i;
					imageData.setPixel(i, fix1, foreground);
					imageData.setAlpha(i, fix1, 191);
					for (int j= i + 1; j < last2; j++) {
						imageData.setPixel(j, fix1, foreground);
						imageData.setAlpha(j, fix1, 255);
					}
					imageData.setPixel(last2, fix1, foreground);
					imageData.setAlpha(last2, fix1, 191);
				}
				break;
			case SWT.DOWN:
				size1= (imageData.width) / 2 + imageData.width % 2 - 1;
				size2= size1 * 2;
				for (int i= 0; i <= size1; i++) {
					final int fix1= i;
					final int last2= size2 - i;
					imageData.setPixel(i, fix1, foreground);
					imageData.setAlpha(i, fix1, 191);
					for (int j= i + 1; j < last2; j++) {
						imageData.setPixel(j, fix1, foreground);
						imageData.setAlpha(j, fix1, 255);
					}
					imageData.setPixel(last2, fix1, foreground);
					imageData.setAlpha(last2, fix1, 191);
				}
				break;
			case SWT.LEFT:
				size1= (imageData.height) / 2 + imageData.height % 2 - 1;
				size2= size1 * 2;
				for (int i= 0; i <= size1; i++) {
					final int fix1= size1 - i;
					final int last2= size2 - i;
					imageData.setPixel(fix1, i, foreground);
					imageData.setAlpha(fix1, i, 191);
					for (int j= i + 1; j < last2; j++) {
						imageData.setPixel(fix1, j, foreground);
						imageData.setAlpha(fix1, j, 255);
					}
					imageData.setPixel(fix1, last2, foreground);
					imageData.setAlpha(fix1, last2, 191);
				}
				break;
			case SWT.RIGHT:
				size1= (imageData.height) / 2 + imageData.height % 2 - 1;
				size2= size1 * 2;
				for (int i= 0; i <= size1; i++) {
					final int fix1= i;
					final int last2= size2 - i;
					imageData.setPixel(fix1, i, foreground);
					imageData.setAlpha(fix1, i, 191);
					for (int j= i + 1; j < last2; j++) {
						imageData.setPixel(fix1, j, foreground);
						imageData.setAlpha(fix1, j, 255);
					}
					imageData.setPixel(fix1, last2, foreground);
					imageData.setAlpha(fix1, last2, 191);
				}
				break;
			}
			
			return imageData;
		};
		
		drawImage(imageProvider,
				xOffset + (width - imgWidth) / 2,
				yOffset + (height - imgHeight) / 2 );
	}

	@Override
	protected Point getSize() {
		final int corr= ((this.size % 2) == 0) ? -1 : 0;
		switch (this.direction) {
		case SWT.UP:
		case SWT.DOWN:
			return new Point(this.size * 3 + corr, this.size * 2);
		case SWT.LEFT:
		case SWT.RIGHT:
			return new Point(this.size * 2, this.size * 3 + corr);
		default:
			throw new IllegalStateException();
		}
	}
	
	
	@Override
	public int hashCode() {
		return this.direction * 7
				+ this.size * 3460
				+ this.foregroundColor.hashCode() * 343629
				+ this.backgroundColor.hashCode() * 987972;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final AccessibleArrowImage other
						&& this.direction == other.direction
						&& this.size == other.size
						&& this.foregroundColor.equals(other.foregroundColor)
						&& this.backgroundColor.equals(other.backgroundColor) ));
	}
	
}
