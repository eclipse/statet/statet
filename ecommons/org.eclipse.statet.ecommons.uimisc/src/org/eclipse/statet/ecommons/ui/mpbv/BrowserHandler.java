/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.mpbv;

import java.net.URL;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.browser.Browser;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.components.StatusInfo;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * Abstract command handler which can be bind to a browser.
 */
@NonNullByDefault
public abstract class BrowserHandler extends AbstractHandler {
	
	
	public static interface IBrowserProvider {
		
		@Nullable Browser getBrowser();
		
		void showMessage(IStatus status);
		
	}
	
	
	public static class NavigateBackHandler extends BrowserHandler {
		
		
		public NavigateBackHandler(final Browser browser) {
			super(browser);
		}
		
		public NavigateBackHandler(final IBrowserProvider browser) {
			super(browser);
		}
		
		
		@Override
		public void setEnabled(final @Nullable Object evaluationContext) {
			final Browser browser= getBrowser();
			setBaseEnabled(UIAccess.isOkToUse(browser) && browser.isBackEnabled());
		}
		
		@Override
		public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
			final Browser browser= getBrowser();
			if (UIAccess.isOkToUse(browser)) {
				browser.back();
			}
			return null;
		}
		
	}
	
	public static class NavigateForwardHandler extends BrowserHandler {
		
		
		public NavigateForwardHandler(final Browser browser) {
			super(browser);
		}
		
		public NavigateForwardHandler(final IBrowserProvider browser) {
			super(browser);
		}
		
		
		@Override
		public void setEnabled(final @Nullable Object evaluationContext) {
			final Browser browser= getBrowser();
			setBaseEnabled(UIAccess.isOkToUse(browser) && browser.isForwardEnabled());
		}
		
		@Override
		public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
			final Browser browser= getBrowser();
			if (UIAccess.isOkToUse(browser)) {
				browser.forward();
			}
			return null;
		}
		
	}
	
	public static class CancelHandler extends BrowserHandler {
		
		
		public CancelHandler(final Browser browser) {
			super(browser);
		}
		
		public CancelHandler(final IBrowserProvider browser) {
			super(browser);
		}
		
		
		@Override
		public void setEnabled(final @Nullable Object evaluationContext) {
			final Browser browser= getBrowser();
			setBaseEnabled(UIAccess.isOkToUse(browser));
		}
		
		@Override
		public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
			final Browser browser= getBrowser();
			if (UIAccess.isOkToUse(browser)) {
				browser.stop();
			}
			return null;
		}
		
	}
	
	public static class OpenExternalHandler extends BrowserHandler {
		
		
		public OpenExternalHandler(final Browser browser) {
			super(browser);
		}
		
		public OpenExternalHandler(final IBrowserProvider browser) {
			super(browser);
		}
		
		
		@Override
		public void setEnabled(final @Nullable Object evaluationContext) {
			final Browser browser= getBrowser();
			setBaseEnabled(browser != null
					&& browser.getUrl().length() > 0);
		}
		
		@Override
		public @Nullable Object execute(final ExecutionEvent event) throws ExecutionException {
			final Browser browser= getBrowser();
			if (browser != null) {
				final IWorkbenchBrowserSupport browserSupport= PlatformUI.getWorkbench().getBrowserSupport();
				try {
					open(browser.getUrl(), browserSupport.getExternalBrowser());
				}
				catch (final Exception e) {
					StatusManager.getManager().handle(new Status(IStatus.ERROR, SharedUIResources.BUNDLE_ID, -1,
							"An error occurred when opening the page in an external browser.", e));
					showMessage(IStatus.ERROR, "Open external browser failed");
				}
			}
			return null;
		}
		
		protected void open(final String url, final IWebBrowser webBrowser) throws Exception {
			webBrowser.openURL(new URL(url));
		}
		
	}
	
	
	private IBrowserProvider browserProvider;
	
	
	public BrowserHandler() {
	}
	
	public BrowserHandler(final Browser browser) {
		this.browserProvider= new IBrowserProvider() {
			@Override
			public Browser getBrowser() {
				return browser;
			}
			@Override
			public void showMessage(final IStatus status) {
			}
		};
	}
	
	public BrowserHandler(final IBrowserProvider browser) {
		this.browserProvider= browser;
	}
	
	
	public @Nullable Browser getBrowser() {
		return this.browserProvider.getBrowser();
	}
	
	protected void showMessage(final int severity, final String message) {
		this.browserProvider.showMessage(new StatusInfo(severity, message));
	}
	
}
