/*=============================================================================#
 # Copyright (c) 2007, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.viewers.breadcrumb;

import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerRow;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Widget;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * A viewer row for the breadcrumb viewer.
 */
@NonNullByDefault
class BreadcrumbViewerRow extends ViewerRow {
	
	
	private final BreadcrumbViewer viewer;
	private final BreadcrumbItem item;
	
	private @Nullable Color foreground;
	private @Nullable Font font;
	private @Nullable Color background;
	
	
	public BreadcrumbViewerRow(final BreadcrumbViewer viewer, final BreadcrumbItem item) {
		this.viewer= viewer;
		this.item= item;
	}
	
	
	@Override
	public Object clone() {
		return new BreadcrumbViewerRow(this.viewer, this.item);
	}
	
	@Override
	public @Nullable Color getBackground(final int columnIndex) {
		return this.background;
	}
	
	@Override
	public Rectangle getBounds(final int columnIndex) {
		return getBounds();
	}
	
	@Override
	public Rectangle getBounds() {
		return this.item.getBounds();
	}
	
	@Override
	public int getColumnCount() {
		return 1;
	}
	
	@Override
	public Control getControl() {
		return this.viewer.getControl();
	}
	
	@Override
	public Object getElement() {
		return this.item.getElement();
	}
	
	@Override
	public @Nullable Font getFont(final int columnIndex) {
		return this.font;
	}
	
	@Override
	public @Nullable Color getForeground(final int columnIndex) {
		return this.foreground;
	}
	
	@Override
	public Image getImage(final int columnIndex) {
		return this.item.getImage();
	}
	
	@Override
	public Widget getItem() {
		return this.item;
	}
	
	@Override
	public @Nullable ViewerRow getNeighbor(final int direction, final boolean sameLevel) {
		return null;
	}
	
	@Override
	public String getText(final int columnIndex) {
		return this.item.getText();
	}
	
	@Override
	public TreePath getTreePath() {
		return new TreePath(new @NonNull Object[] { getElement() });
	}
	
	@Override
	public void setBackground(final int columnIndex, final @Nullable Color color) {
		this.background= color;
	}
	
	@Override
	public void setFont(final int columnIndex, final @Nullable Font font) {
		this.font= font;
	}
	
	@Override
	public void setForeground(final int columnIndex, final @Nullable Color color) {
		this.foreground= color;
	}
	
	@Override
	public void setImage(final int columnIndex, final @Nullable Image image) {
		this.item.setImage(image);
	}
	
	@Override
	public void setText(final int columnIndex, final String text) {
		this.item.setText(text);
	}
	
}
