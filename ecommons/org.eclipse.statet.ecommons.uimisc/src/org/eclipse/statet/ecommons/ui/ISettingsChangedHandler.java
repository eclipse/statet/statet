/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui;

import java.util.Map;
import java.util.Set;


/**
 * 
 */
public interface ISettingsChangedHandler {
	
	
	public static final String VIEWER_KEY = "context.Viewer"; //$NON-NLS-1$
	
	
	public void handleSettingsChanged(Set<String> groupIds, Map<String, Object> options);
	
}
