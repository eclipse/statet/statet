/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.TableItem;


public class WaComboViewer extends TableViewer {
	
	
	private final WaCombo fCombo;
	
	
	public WaComboViewer(final WaCombo combo) {
		super(combo.getList());
		
		fCombo = combo;
	}
	
	
	@Override
	public void setSelection(final ISelection selection, final boolean reveal) {
		super.setSelection(selection, reveal);
		final Object object = ((IStructuredSelection) getSelection()).getFirstElement();
		fCombo.updateText((TableItem) ((object != null) ? findItem(object) : null));
	}
	
}
