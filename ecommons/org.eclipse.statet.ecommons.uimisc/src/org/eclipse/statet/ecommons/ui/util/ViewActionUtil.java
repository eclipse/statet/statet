/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPart;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ViewActionUtil {
	
	
	private final IWorkbenchPart part;
	
	private final Control control;
	
	private final ISelectionProvider selectionProvider;
	
	private @Nullable Clipboard clipboard;
	
	private final StatusLineMessageManager statusLine;
	
	
	public ViewActionUtil(final IViewPart part) {
		this.part= part;
		this.control= nonNullAssert(part.getAdapter(Control.class));
		
		final IViewSite site= part.getViewSite();
		this.selectionProvider= nonNullAssert(site.getSelectionProvider());
		this.statusLine= new StatusLineMessageManager(site.getActionBars().getStatusLineManager());
	}
	
	
	public IWorkbenchPart getWorkbenchPart() {
		return this.part;
	}
	
	public ISelectionProvider getSelectionProvider() {
		return this.selectionProvider;
	}
	
	public Control getControl() {
		return this.control;
	}
	
	public Clipboard getClipboard() {
		Clipboard clipboard= this.clipboard;
		if (clipboard == null) {
			this.clipboard= clipboard= new Clipboard(getControl().getDisplay());
		}
		return clipboard;
	}
	
	public StatusLineMessageManager getStatusLine() {
		return this.statusLine;
	}
	
}
