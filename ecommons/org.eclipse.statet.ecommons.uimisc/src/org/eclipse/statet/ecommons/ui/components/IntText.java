/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.components;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Objects;

import com.ibm.icu.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils;


@NonNullByDefault
public class IntText implements IObjValueWidget<@Nullable Integer> {
	
	
	private class SWTListener implements Listener {
		
		@Override
		public void handleEvent(final Event event) {
			switch (event.type) {
			case SWT.Modify:
				updateValue(event.time);
				return;
			case SWT.KeyDown:
				switch (event.keyCode) {
				case SWT.ARROW_UP:
					incrementValue(IntText.this.increment);
					event.doit= false;
					return;
				case SWT.ARROW_DOWN:
					incrementValue(-IntText.this.increment);
					event.doit= false;
					return;
				case SWT.PAGE_UP:
					incrementValue(IntText.this.increment * 10);
					event.doit= false;
					return;
				case SWT.PAGE_DOWN:
					incrementValue(-IntText.this.increment * 10);
					event.doit= false;
					return;
				}
			}
		}
		
	}
	
	
	private final Text text;
	
	private @Nullable Integer value;
	
	private final CopyOnWriteIdentityListSet<IObjValueListener<@Nullable Integer>> valueListeners= new CopyOnWriteIdentityListSet<>();
	
	private int increment;
	private int min= Integer.MIN_VALUE;
	private int max= Integer.MAX_VALUE;
	
	private @Nullable DecimalFormat format;
	
	
	public IntText(final Composite parent, final int flags) {
		this.text= new Text(parent, SWT.LEFT | SWT.SINGLE | flags);
		final SWTListener swtListener= new SWTListener();
		this.text.addListener(SWT.Modify, swtListener);
		this.text.addListener(SWT.KeyDown, swtListener);
	}
	
	
	@Override
	public Text getControl() {
		return this.text;
	}
	
	@Override
	public Class<@Nullable Integer> getValueType() {
		return ObjectUtils.Nullable_Integer_TYPE;
	}
	
	public void setIncrement(final int v) {
		this.increment= v;
	}
	
	public void setMinMax(final int min, final int max) {
		this.min= min;
		this.max= max;
	}
	
	public void setFormat(final DecimalFormat format) {
		this.format= format;
	}
	
	@Override
	public void addValueListener(final IObjValueListener<@Nullable Integer> listener) {
		this.valueListeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeValueListener(final IObjValueListener<@Nullable Integer> listener) {
		this.valueListeners.remove(listener);
	}
	
	@Override
	public @Nullable Integer getValue(final int idx) {
		return this.value;
	}
	
	@Override
	public void setValue(final int idx, final @Nullable Integer value) {
		this.text.setText(formatValue(value));
	}
	
	
	private void incrementValue(final int increment) {
		final var oldValue= this.value;
		int newValue= (oldValue != null) ? (oldValue.intValue() + increment) : this.min;
		if (newValue < this.min) {
			newValue= this.min;
		}
		else if (newValue > this.max) {
			newValue= this.max;
		}
		this.text.setText(formatValue(newValue));
	}
	
	protected @Nullable Integer parseValue(final String text) {
		try {
			return Integer.valueOf(text);
		}
		catch (final NumberFormatException e) {
			return null;
		}
	}
	
	protected String formatValue(final @Nullable Integer value) {
		if (value == null) {
			return ""; //$NON-NLS-1$
		}
		final var format= this.format;
		if (format != null) {
			return format.format(value.intValue());
		}
		return value.toString();
	}
	
	private void updateValue(final int time) {
		final Integer oldValue= this.value;
		final Integer newValue= parseValue(this.text.getText());
		if (Objects.equals(oldValue, newValue)) {
			return;
		}
		
		this.value= newValue;
		
		final var event= new ObjValueEvent<>(this, time, 0, oldValue, newValue, 0);
		for (final var listener : this.valueListeners) {
			event.newValue= newValue;
			listener.valueChanged(event);
		}
	}
	
}
