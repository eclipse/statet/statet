/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.ui.workbench;

import org.eclipse.ui.IPageLayout;


public interface IWorkbenchPerspectiveElements {
	
	
	String PROJECT_EXPLORER_VIEW = IPageLayout.ID_PROJECT_EXPLORER;
	
	String TASKS_VIEW = IPageLayout.ID_TASK_LIST;
	String PROBLEM_VIEW = IPageLayout.ID_PROBLEM_VIEW;
	String BOOKMARKS_VIEW = IPageLayout.ID_BOOKMARKS;
	String PROGRESS_VIEW = IPageLayout.ID_PROGRESS_VIEW;
	String PROPERTIES_VIEW = IPageLayout.ID_PROP_SHEET;
	
	String OUTLINE_VIEW = IPageLayout.ID_OUTLINE;
	String TEMPLATES_VIEW = "org.eclipse.ui.texteditor.TemplatesView"; // TemplatesView.ID //$NON-NLS-1$
	String FILTERS_VIEW = "org.eclipse.statet.workbench.views.ContentFilter"; //$NON-NLS-1$
	
	String SEARCH_VIEW = "org.eclipse.search.ui.views.SearchView"; // NewSearchUI.SEARCH_VIEW_ID //$NON-NLS-1$
	String CONSOLE_VIEW = "org.eclipse.ui.console.ConsoleView"; // IConsoleConstants.ID_CONSOLE_VIEW //$NON-NLS-1$
	
	String NICO_CMDHISTORY_VIEW = "org.eclipse.statet.nico.views.HistoryView"; //$NON-NLS-1$
	String NICO_OBJECTBROWSER_VIEW = "org.eclipse.statet.nico.views.ObjectBrowser"; //$NON-NLS-1$
	
	
	String LAUNCH_ACTION_SET = "org.eclipse.debug.ui.launchActionSet"; //$NON-NLS-1$
	String BREAKPOINT_ACTION_SET = "org.eclipse.debug.ui.breakpointActionSet"; //$NON-NLS-1$
	String NAVIGATE_ACTION_SET = IPageLayout.ID_NAVIGATE_ACTION_SET;
	
	
	String NEW_FOLDER_WIZARD = "org.eclipse.ui.wizards.new.folder"; //$NON-NLS-1$
	String NEW_TEXTFILE_WIZARD = "org.eclipse.ui.wizards.new.file"; //$NON-NLS-1$
	String NEW_UNTITLED_TEXTFILE_WIZARD = "org.eclipse.ui.editors.wizards.UntitledTextFileWizard"; //$NON-NLS-1$
	
	
	String DEBUG_PERSPECTIVE = "org.eclipse.debug.ui.DebugPerspective"; //$NON-NLS-1$
	
}
