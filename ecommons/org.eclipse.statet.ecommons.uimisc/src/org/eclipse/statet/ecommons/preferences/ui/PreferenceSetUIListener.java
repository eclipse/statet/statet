/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import org.eclipse.statet.jcommons.collections.ImSet;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService.ChangeEvent;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


public abstract class PreferenceSetUIListener implements PreferenceSetService.ChangeListener, Listener {
	
	
	public static PreferenceSetUIListener subscribe(
			final PreferenceSetService.ChangeListener listener,
			final PreferenceAccess prefAccess, final ImSet<String> qualifiers,
			final Control control) {
		final PreferenceSetUIListener uiListener= new PreferenceSetUIListener(prefAccess, control) {
			@Override
			protected void handlePreferenceChanged(final ChangeEvent event) {
				listener.onPreferenceChanged(event);
			}
		};
		uiListener.subscribe(qualifiers);
		return uiListener;
	}
	
	
	private final PreferenceAccess prefAccess;
	
	private final Control control;
	
	
	public PreferenceSetUIListener(final PreferenceAccess prefAccess, final Control control) {
		this.prefAccess= prefAccess;
		this.control= control;
		
		this.control.addListener(SWT.Dispose, this);
		
	}
	
	
	public void subscribe(final ImSet<String> qualifiers) {
		this.prefAccess.addPreferenceSetListener(this, qualifiers);
	}
	
	public void unsubscribe() {
		this.prefAccess.removePreferenceSetListener(this);
	}
	
	@Override
	public void handleEvent(final Event event) {
		if (event.type == SWT.Dispose) {
			this.prefAccess.removePreferenceSetListener(this);
		}
	}
	
	@Override
	public void onPreferenceChanged(final ChangeEvent event) {
		UIAccess.getDisplay().syncExec(new Runnable() {
			@Override
			public void run() {
				if (UIAccess.isOkToUse(PreferenceSetUIListener.this.control)) {
					handlePreferenceChanged(event);
				}
			}
		});
	}
	
	protected abstract void handlePreferenceChanged(final ChangeEvent event);
	
}
