/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.jface.preference.ColorSelector;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.RGB;

import org.eclipse.statet.ecommons.databinding.jface.AbstractSWTObservableValue;


/**
 * ObservableValue for a JFace ColorSelector.
 */
public class ColorSelectorObservableValue extends AbstractSWTObservableValue<RGB> {
	
	
	private final ColorSelector selector;
	
	private RGB value;
	
	private final IPropertyChangeListener updateListener= new IPropertyChangeListener() {
		@Override
		public void propertyChange(final PropertyChangeEvent event) {
			final RGB oldValue= (RGB)event.getOldValue();
			final RGB newValue= (RGB)event.getNewValue();
			ColorSelectorObservableValue.this.value= newValue;
			fireValueChange(Diffs.createValueDiff(oldValue, newValue));
		}
	};
	
	
	/**
	 * @param selector
	 */
	public ColorSelectorObservableValue(final ColorSelector selector) {
		super(selector.getButton());
		this.selector= selector;
		
		this.selector.addListener(this.updateListener);
	}
	
	
	@Override
	public synchronized void dispose() {
		this.selector.removeListener(this.updateListener);
		super.dispose();
	}
	
	
	@Override
	public Object getValueType() {
		return RGB.class;
	}
	
	@Override
	public void doSetValue(final RGB value) {
		final RGB oldValue= this.value;
		this.value= value;
		this.selector.setColorValue(this.value != null ? this.value : new RGB(0,0,0));
		fireValueChange(Diffs.createValueDiff(oldValue, this.value));
	}
	
	@Override
	public RGB doGetValue() {
		return this.selector.getColorValue();
	}
	
}
