/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.ecommons.preferences.ui.Messages;


@NonNullByDefault
public final class PreferenceUIUtils {
	
	
	public static final class SeeAlsoBuilder {
		
		
		private final StringBuilder sb= new StringBuilder(0x80);
		
		
		public SeeAlsoBuilder(final String seeAlsoMessage) {
			this.sb.append(seeAlsoMessage);
		}
		
		
		private void addItem() {
			this.sb.append("\n   • "); //$NON-NLS-1$
		}
		
		public SeeAlsoBuilder add(final String text) {
			addItem();
			this.sb.append(text);
			return this;
		}
		
		public SeeAlsoBuilder add(final String text, final String... pageId) {
			addItem();
			int idxWritten= 0;
			for (int i= 0; i < pageId.length; i++) {
				final int idx= text.indexOf("<a>"); //$NON-NLS-1$
				if (idx < 0) {
					throw new IllegalArgumentException();
				}
				this.sb.append(text, idxWritten, idx);
				this.sb.append("<a href=\""); //$NON-NLS-1$
				this.sb.append(pageId[i]);
				this.sb.append("\">"); //$NON-NLS-1$
				idxWritten= idx + 3;
			}
			this.sb.append(text, idxWritten, text.length());
			return this;
		}
		
		
		@Override
		public String toString() {
			return this.sb.toString();
		}
		
	}
	
	public static SeeAlsoBuilder composeSeeAlsoPreferencePages() {
		return new SeeAlsoBuilder(Messages.SeeAlso_message);
	}
	
	
	private PreferenceUIUtils() {}
	
}
