/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.graphics.RGB;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.Preference;


/**
 * Preference of a color value as RGB (like in JFace).
 */
@NonNullByDefault
public class RGBPref extends Preference<@Nullable RGB> {
	
	
	public RGBPref(final String qualifier, final String key) {
		super(qualifier, key);
	}
	
	
	@Override
	public Class<RGB> getUsageType() {
		return RGB.class;
	}
	
	@Override
	public @Nullable RGB store2Usage(final @Nullable String storeValue) {
		if (storeValue != null) {
			return StringConverter.asRGB(storeValue);
		}
		return null;
	}
	
	@Override
	public @Nullable String usage2Store(final @Nullable RGB value) {
		if (value != null) {
			return StringConverter.asString(value);
		}
		return null;
	}
	
}
