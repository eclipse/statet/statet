/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.preferences.IWorkbenchPreferenceContainer;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceSetService;
import org.eclipse.statet.ecommons.preferences.core.util.ResumeJob;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.ui.IOverlayStatus;
import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.components.StatusInfo;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.internal.ecommons.preferences.ui.Messages;


/**
 * Abstract preference page which is used to wrap a
 * Configuration Block
 */
@NonNullByDefault
public abstract class ConfigurationBlockPreferencePage extends PreferencePage
		implements IWorkbenchPreferencePage {
	
	
	private ConfigurationBlock block;
	
	private Composite blockControl;
	
	private IStatus blockStatus;
	
	
	/**
	 * Creates a new preference page.
	 */
	public ConfigurationBlockPreferencePage() {
		this.blockStatus= new StatusInfo();
	}
	
	
	protected abstract ConfigurationBlock createConfigurationBlock() throws CoreException;
	
	@Override
	public void init(final IWorkbench workbench) {
		try {
			this.block= createConfigurationBlock();
		}
		catch (final CoreException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, SharedUIResources.BUNDLE_ID, -1,
							NLS.bind(Messages.ConfigurationPage_error_message, getTitle()),
							e ),
					StatusManager.LOG | StatusManager.SHOW );
		}
	}
	
	@Override
	public void dispose() {
		this.block.dispose();
		
		super.dispose();
	}
	
	
	protected ConfigurationBlock getBlock() {
		return this.block;
	}
	
	
	@Override
	protected Control createContents(final Composite parent) {
		this.blockControl= new Composite(parent, SWT.NONE);
		this.blockControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		final GridLayout layout= LayoutUtils.newCompositeGrid(1);
		layout.marginRight= LayoutUtils.defaultHSpacing();
		this.blockControl.setLayout(layout);
		this.blockControl.setBackgroundMode(SWT.INHERIT_FORCE);
		this.block.createContents(this.blockControl, getWorkbenchContainer(), getPreferenceStore());
		
		applyDialogFont(this.blockControl);
		
		final String explTitle= this.block.getTitle();
		if (explTitle != null) {
			setTitle(explTitle);
		}
		
		final String helpContext= getHelpContext();
		if (helpContext != null) {
			PlatformUI.getWorkbench().getHelpSystem().setHelp(this.blockControl, helpContext);
		}
		
		return this.blockControl;
	}
	
	protected Composite getBlockControl() {
		return this.blockControl;
	}
	
	/**
	 * Overwrite to enable a help context
	 * 
	 * @return the help context for the page or <code>null</code>
	 */
	protected @Nullable String getHelpContext() {
		return this.block.getHelpContext();
	}
	
	@Override
	public void applyData(final @Nullable Object data) {
		if (data != null) {
			this.block.applyData(data);
		}
	}
	
	
	private IWorkbenchPreferenceContainer getWorkbenchContainer() {
		return (IWorkbenchPreferenceContainer)getContainer();
	}
	
	@Override
	public boolean performOk() {
		final var block= this.block;
		if (block != null) {
			final PreferenceSetService preferenceSetService= EPreferences.getPreferenceSetService();
			final IWorkbenchPreferenceContainer container= getWorkbenchContainer();
			final String sourceId= "Obj" + System.identityHashCode((container != null) ? container : this); //$NON-NLS-1$
			final boolean resume= preferenceSetService.pause(sourceId);
			try {
				if (!block.performOk(0)) {
					return false;
				}
			}
			finally {
				if (resume) {
					if (container != null) {
						container.registerUpdateJob(new ResumeJob(preferenceSetService, sourceId));
					}
					else {
						preferenceSetService.resume(sourceId);
					}
				}
			}
		}
		return super.performOk();
	}
	
	@Override
	public void performApply() {
		final var block= this.block;
		if (block != null) {
			final PreferenceSetService preferenceSetService= EPreferences.getPreferenceSetService();
			final String sourceId= "Obj" + System.identityHashCode(this); //$NON-NLS-1$
			final boolean resume= preferenceSetService.pause(sourceId);
			try {
				block.performOk(ConfigurationBlock.SAVE_STORE);
			}
			finally {
				if (resume) {
					preferenceSetService.resume(sourceId);
				}
			}
		}
	}
	
	@Override
	public void performDefaults() {
		final var block= this.block;
		if (block != null) {
			block.performDefaults();
		}
		super.performDefaults();
	}
	
	@Override
	public boolean performCancel() {
		final var block= this.block;
		if (block != null) {
			block.performCancel();
		}
		return true;
	}
	
	
	/**
	 * Returns a new status change listener
	 * @return The new listener
	 */
	protected StatusChangeListener createStatusChangedListener() {
		return new StatusChangeListener() {
			@Override
			public void statusChanged(final IStatus status) {
				ConfigurationBlockPreferencePage.this.blockStatus= status;
				updateStatus();
			}
		};
	}
	
	protected void updateStatus() {
		updateStatus(this.blockStatus);
	}
	
	protected void updateStatus(final IStatus status) {
		if (status instanceof IOverlayStatus) {
			setValid(((IOverlayStatus) status).getCombinedSeverity() != IStatus.ERROR);
		}
		else {
			setValid(!status.matches(IStatus.ERROR));
		}
		StatusInfo.applyToStatusLine(this, status);
	}
	
	protected IStatus getBlockStatus() {
		return this.blockStatus;
	}
	
}
