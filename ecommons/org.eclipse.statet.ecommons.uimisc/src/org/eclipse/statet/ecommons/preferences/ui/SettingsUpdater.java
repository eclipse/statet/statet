/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.preferences.ui;

import java.util.HashMap;
import java.util.Set;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.preferences.SettingsChangeNotifier;
import org.eclipse.statet.ecommons.ui.ISettingsChangedHandler;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * Util for settings changes in UI components.
 * Propagates changes to handler in UI thread and disposes automatically.
 */
public class SettingsUpdater implements SettingsChangeNotifier.ChangeListener {
	
	
	private final ISettingsChangedHandler fHandler;
	private final Control fControl;
	private final DisposeListener fDisposeListener;
	
	private String[] fInterestingGroupIds;
	
	
	public SettingsUpdater(final ISettingsChangedHandler handler, final Control control) {
		this(handler, control, null);
	}
	
	public SettingsUpdater(final ISettingsChangedHandler handler, final Control control, final String[] groupIds) {
		fHandler = handler;
		fControl = control;
		setInterestingGroups(groupIds);
		PreferencesUtil.getSettingsChangeNotifier().addChangeListener(this);
		fDisposeListener = new DisposeListener() {
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				dispose();
			}
		};
		fControl.addDisposeListener(fDisposeListener);
	}
	
	
	public void setInterestingGroups(final String[] groupIds) {
		fInterestingGroupIds = groupIds;
	}
	
	@Override
	public void settingsChanged(final Set<String> groupIds) {
		if (fInterestingGroupIds == null) {
			runUpdate(groupIds);
			return;
		}
		for (final String id : fInterestingGroupIds) {
			if (groupIds.contains(id)) {
				runUpdate(groupIds);
				return;
			}
		}
	}
	
	private void runUpdate(final Set<String> groupIds) {
		final HashMap<String, Object> options= new HashMap<>();
		UIAccess.getDisplay().syncExec(new Runnable() {
			@Override
			public void run() {
				if (UIAccess.isOkToUse(fControl)) {
					fHandler.handleSettingsChanged(groupIds, options);
				}
			}
		});
	}
	
	public void dispose() {
		fControl.removeDisposeListener(fDisposeListener);
		PreferencesUtil.getSettingsChangeNotifier().removeChangeListener(this);
	}
	
}
