/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.preferences.ui;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String ConfigurationPage_error_message;
	
	public static String PropertyAndPreference_UseProjectSettings_label;
	public static String PropertyAndPreference_ShowProjectSpecificSettings_label;
	public static String PropertyAndPreference_ShowWorkspaceSettings_label;
	
	public static String ProjectSelectionDialog_title;
	public static String ProjectSelectionDialog_desciption;
	public static String ProjectSelectionDialog_filter;
	
	public static String SeeAlso_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
