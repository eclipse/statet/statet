/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.swt.AccessibleArrowImage;


@NonNullByDefault
public class AccessibleArrowImageTest {
	
	
	public static void main(final String[] args) {
		final int[] directions= new int[] { SWT.UP, SWT.DOWN, SWT.LEFT, SWT.RIGHT };
		final int size= AccessibleArrowImage.DEFAULT_SIZE;
		
		final Display display= new Display();
		final Shell shell= new Shell(display);
		shell.setLayout (new GridLayout(directions.length, false));
		
		for (int i= 0; i < directions.length; i++) {
			final AccessibleArrowImage image= new AccessibleArrowImage(directions[i], size,
					display.getSystemColor(SWT.COLOR_WIDGET_FOREGROUND).getRGB(),
					display.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND).getRGB() );
			final Button button= new Button(shell, SWT.PUSH);
			button.setBackground(display.getSystemColor(SWT.COLOR_GREEN));
			button.setImage(image.createImage());
		}
		shell.setSize((size + 8) * 8 + 100, (size + 8) * 2 + 100);
		shell.open();
		while (!shell.isDisposed ()) {
			if (!display.readAndDispatch ()) {
				display.sleep ();
			}
		}
		display.dispose ();
	}
	
}
