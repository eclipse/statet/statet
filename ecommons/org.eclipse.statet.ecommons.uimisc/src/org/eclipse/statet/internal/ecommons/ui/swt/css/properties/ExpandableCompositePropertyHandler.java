/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.ui.swt.css.properties;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.e4.ui.css.swt.properties.AbstractCSSPropertySWTHandler;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Control;

import org.w3c.dom.css.CSSValue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.swt.expandable.ExpandableComposite;


@NonNullByDefault
@SuppressWarnings("restriction")
public class ExpandableCompositePropertyHandler extends AbstractCSSPropertySWTHandler {
	
	
	private static final String TITLE_BAR_FOREGROUND= "swt-titlebar-color"; //$NON-NLS-1$
	
	
	@Override
	protected @Nullable String retrieveCSSProperty(final Control control, final String property,
			final @Nullable String pseudo, final CSSEngine engine) throws Exception {
		if (!(control instanceof ExpandableComposite)) {
			return null;
		}
		final var expandableComposite= (ExpandableComposite)control;
		
		switch (property) {
		case TITLE_BAR_FOREGROUND:
			if (pseudo == null) {
				return engine.convert(expandableComposite.getTitleBarForeground(),
						Color.class, null );
			}
			return null;
//		case CSSPropertyFormHandler.TB_TOGGLE:
//			if (pseudo == null) {
//				expandableComposite.setToggleColor(
//						(Color)engine.convert(value, Color.class, control.getDisplay()) );
//			}
//			else if (pseudo.equalsIgnoreCase("hover")) { //$NON-NLS-1$
//				expandableComposite.setToggleHoverColor(
//						(Color)engine.convert(value, Color.class, control.getDisplay()) );
//			}
//			return;
		default:
			break;
		}
		return null;
	}
	
	@Override
	protected void applyCSSProperty(final Control control, final String property,
			final CSSValue value,
			final @Nullable String pseudo, final CSSEngine engine) throws Exception {
		if (!(control instanceof ExpandableComposite)) {
			return;
		}
		final var expandableComposite= (ExpandableComposite)control;
		
		switch (property) {
		case TITLE_BAR_FOREGROUND:
			if (pseudo == null
					&& value.getCssValueType() == CSSValue.CSS_PRIMITIVE_VALUE) {
				expandableComposite.setTitleBarForeground(
						(Color)engine.convert(value, Color.class, control.getDisplay()) );
			}
			return;
//		case CSSPropertyFormHandler.TB_TOGGLE:
//			if (pseudo == null) {
//				expandableComposite.setToggleColor(
//						(Color)engine.convert(value, Color.class, control.getDisplay()) );
//			}
//			else if (pseudo.equalsIgnoreCase("hover")) { //$NON-NLS-1$
//				expandableComposite.setToggleHoverColor(
//						(Color)engine.convert(value, Color.class, control.getDisplay()) );
//			}
//			return;
		default:
			break;
		}
		return;
	}
	
}
