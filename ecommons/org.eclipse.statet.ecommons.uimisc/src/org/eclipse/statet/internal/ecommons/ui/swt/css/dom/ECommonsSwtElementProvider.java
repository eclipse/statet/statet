/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Eclipse Public License v1.0
 # which accompanies this distribution, and is available at
 # http://www.eclipse.org/legal/epl-v10.html
 #
 # Contributors:
 #     Stephan Wahlbrink <stephan.wahlbrink@walware.de> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.ui.swt.css.dom;

import org.eclipse.e4.ui.css.core.dom.IElementProvider;
import org.eclipse.e4.ui.css.core.engine.CSSEngine;

import org.w3c.dom.Element;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.swt.expandable.ExpandableComposite;


@NonNullByDefault
@SuppressWarnings("restriction")
public class ECommonsSwtElementProvider implements IElementProvider {
	
	
	public ECommonsSwtElementProvider() {
	}
	
	
	@Override
	public @Nullable Element getElement(final Object element, final CSSEngine engine) {
		if (element instanceof ExpandableComposite) {
			return new ExpandableCompositeElement((ExpandableComposite)element, engine);
		}
		if (element instanceof VirtualWidget) {
			return ((VirtualWidget)element).getElement();
		}
		return null;
	}
	
}
