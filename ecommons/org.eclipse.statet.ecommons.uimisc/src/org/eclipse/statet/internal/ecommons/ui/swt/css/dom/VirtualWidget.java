/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.ui.swt.css.dom;

import org.eclipse.e4.ui.css.core.dom.CSSStylableElement;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("restriction")
public class VirtualWidget {
	
	
	private CSSStylableElement element;
	
	
	@SuppressWarnings("null")
	public VirtualWidget() {
	}
	
	public void setElement(final CSSStylableElement element) {
		this.element= element;
	}
	
	
	public CSSStylableElement getElement() {
		return this.element;
	}
	
}
