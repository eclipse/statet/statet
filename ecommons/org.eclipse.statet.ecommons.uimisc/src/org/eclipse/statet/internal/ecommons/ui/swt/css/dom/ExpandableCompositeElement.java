/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Eclipse Public License v1.0
 # which accompanies this distribution, and is available at
 # http://www.eclipse.org/legal/epl-v10.html
 #
 # Contributors:
 #     Stephan Wahlbrink <stephan.wahlbrink@walware.de> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.ui.swt.css.dom;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.e4.ui.css.swt.dom.CompositeElement;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.swt.expandable.ExpandableComposite;


@NonNullByDefault
@SuppressWarnings("restriction")
public class ExpandableCompositeElement extends CompositeElement {
	
	
	public ExpandableCompositeElement(final ExpandableComposite composite, final CSSEngine engine) {
		super(composite, engine);
	}
	
	
	@Override
	protected String computeLocalName() {
		return "ExpandableComposite"; //$NON-NLS-1$
	}
	
	@Override
	public ExpandableComposite getComposite() {
		return (ExpandableComposite)getNativeWidget();
	}
	
	@Override
	public void reset() {
		super.reset();
		final var composite= getComposite();
		composite.setTitleBarForeground(null);
		composite.setToggleColor(null);
		composite.setToggleHoverColor(null);
	}
	
}
