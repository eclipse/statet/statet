/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.ui;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.CompositeImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;
import org.eclipse.statet.ecommons.ui.util.UIAccess;


public class UIMiscellanyPlugin extends AbstractUIPlugin {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.ecommons.uimisc"; //$NON-NLS-1$
	
	private static final String NS= BUNDLE_ID;
	
	public static final String LOCTOOL_GO_IMAGE_ID=             NS + "/images/loctool/go"; //$NON-NLS-1$
	public static final String LOCTOOL_GO_D_IMAGE_ID=           NS + "/images/loctoold/go"; //$NON-NLS-1$
	
	
	private static UIMiscellanyPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static UIMiscellanyPlugin getInstance() {
		return instance;
	}
	
	
	public static void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private final List<Disposable> disposables= new ArrayList<>();
	
	
	/**
	 * The default constructor
	 */
	public UIMiscellanyPlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.started = true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started = false;
				
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					log(new Status(IStatus.ERROR, BUNDLE_ID,
							"Error occured when dispose module", //$NON-NLS-1$
							e ));
				}
			}
			this.disposables.clear();
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	public void addStoppingListener(final Disposable listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.disposables.add(listener);
		}
	}
	
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(SharedUIResources.OBJ_USER_IMAGE_ID);
		util.register(SharedUIResources.OBJ_LINE_MATCH_IMAGE_ID, ".gif"); //$NON-NLS-1$
		util.register(SharedUIResources.OBJ_MAIN_TAB_ID, ImageRegistryUtil.T_OBJ, "main_tab.png"); //$NON-NLS-1$
		
		util.register(SharedUIResources.OVR_DEFAULT_MARKER_IMAGE_ID, ".gif"); //$NON-NLS-1$
		util.register(SharedUIResources.OVR_GREEN_LIGHT_IMAGE_ID);
		util.register(SharedUIResources.OVR_YELLOW_LIGHT_IMAGE_ID);
		
		util.register(SharedUIResources.OVR_INFO_IMAGE_ID);
		util.register(SharedUIResources.OVR_WARNING_IMAGE_ID);
		util.register(SharedUIResources.OVR_ERROR_IMAGE_ID);
		util.register(SharedUIResources.OVR_IGNORE_OPTIONAL_PROBLEMS_IMAGE_ID);
		
		util.register(SharedUIResources.OVR_DEPRECATED_IMAGE_ID);
		
		util.register(SharedUIResources.LOCTOOL_FILTER_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL, "filter_view.gif"); //$NON-NLS-1$
		util.register(SharedUIResources.LOCTOOLD_FILTER_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL_D, "filter_view.gif"); //$NON-NLS-1$
		util.register(SharedUIResources.LOCTOOL_DISABLE_FILTER_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL, "disable-filter.png"); //$NON-NLS-1$
		
		util.register(SharedUIResources.LOCTOOL_SORT_ALPHA_IMAGE_ID);
		util.register(SharedUIResources.LOCTOOL_SORT_SCORE_IMAGE_ID);
		
		util.register(SharedUIResources.LOCTOOL_CASESENSITIVE_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL, "casesensitive.png"); //$NON-NLS-1$
		
		util.register(SharedUIResources.LOCTOOL_EXPANDALL_IMAGE_ID);
		util.register(SharedUIResources.LOCTOOL_COLLAPSEALL_IMAGE_ID);
		
		util.register(SharedUIResources.TOOL_SCROLLLOCK_IMAGE_ID);
		util.register(SharedUIResources.TOOL_SCROLLLOCK_DISABLED_IMAGE_ID);
		
		util.register(SharedUIResources.LOCTOOL_SYNC_EDITOR_IMAGE_ID);
		
		util.register(SharedUIResources.LOCTOOL_FAVORITES_IMAGE_ID);
		
		util.register(SharedUIResources.LOCTOOL_CHANGE_PAGE_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL, "change_page.png"); //$NON-NLS-1$
		util.register(SharedUIResources.LOCTOOL_PIN_PAGE_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL, "pin_page.png"); //$NON-NLS-1$
		util.register(SharedUIResources.LOCTOOLD_PIN_PAGE_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL, "pin_page.png"); //$NON-NLS-1$
		
		util.register(LOCTOOL_GO_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL, "go.png"); //$NON-NLS-1$
		util.register(LOCTOOL_GO_D_IMAGE_ID, ImageRegistryUtil.T_LOCTOOL_D, "go.png"); //$NON-NLS-1$
		
		UIAccess.getDisplay().syncExec(new Runnable() {
			@Override
			public void run() {
				final Display display = Display.getCurrent();
				final int[] cross = new int[] { 
						 3,  3,  5,  3,  7,  5,  8,  5, 10,  3, 12,  3, 
						12,  5, 10,  7, 10,  8, 12, 10, 12, 12,
						10, 12,  8, 10,  7, 10,  5, 12,  3, 12,
						 3, 10,  5,  8,  5,  7,  3,  5,
				};
				final int[] right = new int[] { 
						 5,  3,  8,  3, 12,  7, 12,  8,  8, 12,  5, 12,
						 5, 11,  8,  8,  8,  7,  5,  4, 
				};
				final int[] left = new int[right.length];
				final int[] up = new int[right.length];
				final int[] down = new int[right.length];
				for (int i = 0; i < right.length; i = i+2) {
					final int j = i+1;
					final int x = right[i];
					final int y = right[j];
					left[i] = 16-x;
					left[j] = y;
					up[i] = y;
					up[j] = 16-x;
					down[i] = y;
					down[j] = x;
				}
				
				final Color border = display.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW);
				final Color background = display.getSystemColor(SWT.COLOR_LIST_BACKGROUND);
				final Color hotRed = new Color(252, 160, 160);
				final Color hotYellow = new Color(252, 232, 160);
				final Color transparent = display.getSystemColor(SWT.COLOR_MAGENTA);
				
				final int defaultIconSize= 16;
				final PaletteData palette = new PaletteData(transparent.getRGB(),
						border.getRGB(), background.getRGB(),
						hotRed.getRGB(), hotYellow.getRGB() );
				final ImageData data = new ImageData(defaultIconSize, defaultIconSize, 8, palette);
				data.transparentPixel = 0;
				
				reg.put(SharedUIResources.PLACEHOLDER_IMAGE_ID,
						new CompositeImageDescriptor() {
							@Override
							protected Point getSize() {
								return new Point(defaultIconSize, defaultIconSize);
							}
							@Override
							protected void drawCompositeImage(final int width, final int height) {
							}
						});
				{	// Close
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(background);
					gc.fillPolygon(cross);
					gc.setForeground(border);
					gc.drawPolygon(cross);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_CLOSETRAY_IMAGE_ID, image);
				}
				{	// Close hot
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(hotRed);
					gc.fillPolygon(cross);
					gc.setForeground(border);
					gc.drawPolygon(cross);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_CLOSETRAY_H_IMAGE_ID, image);
				}
				{	// Left
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(background);
					gc.fillPolygon(left);
					gc.setForeground(border);
					gc.drawPolygon(left);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_LEFT_IMAGE_ID, image);
				}
				{	// Left hot
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(hotYellow);
					gc.fillPolygon(left);
					gc.setForeground(border);
					gc.drawPolygon(left);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_LEFT_H_IMAGE_ID, image);
				}
				{	// Right
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(background);
					gc.fillPolygon(right);
					gc.setForeground(border);
					gc.drawPolygon(right);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_RIGHT_IMAGE_ID, image);
				}
				{	// Right hot
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(hotYellow);
					gc.fillPolygon(right);
					gc.setForeground(border);
					gc.drawPolygon(right);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_RIGHT_H_IMAGE_ID, image);
				}
				{	// Up
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(background);
					gc.fillPolygon(up);
					gc.setForeground(border);
					gc.drawPolygon(up);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_UP_IMAGE_ID, image);
				}
				{	// Up hot
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(hotYellow);
					gc.fillPolygon(up);
					gc.setForeground(border);
					gc.drawPolygon(up);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_UP_H_IMAGE_ID, image);
				}
				{	// Down
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(background);
					gc.fillPolygon(down);
					gc.setForeground(border);
					gc.drawPolygon(down);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_DOWN_IMAGE_ID, image);
				}
				{	// Down hot
					final Image image = new Image(display, data);
					image.setBackground(transparent);
					final GC gc = new GC(image);
					gc.setBackground(hotYellow);
					gc.fillPolygon(down);
					gc.setForeground(border);
					gc.drawPolygon(down);
					gc.dispose();
					
					reg.put(SharedUIResources.LOCTOOL_DOWN_H_IMAGE_ID, image);
				}
			}
		});
	}
	
}
