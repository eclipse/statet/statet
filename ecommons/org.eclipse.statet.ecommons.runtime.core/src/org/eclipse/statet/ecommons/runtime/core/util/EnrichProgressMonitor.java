/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.runtime.core.util;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ProgressMonitorWrapper;


public class EnrichProgressMonitor extends ProgressMonitorWrapper {
	
	
	private String prefix;
	
	
	public EnrichProgressMonitor(final IProgressMonitor monitor) {
		super(monitor);
	}
	
	
	public void setPrefix(final String prefix) {
		this.prefix= prefix;
	}
	
	@Override
	public void setTaskName(final String name) {
		final String prefix= this.prefix;
		if (prefix != null) {
			if (name != null) {
				super.setTaskName(prefix + name);
			}
			else {
				super.setTaskName(prefix);
			}
		}
		else {
			super.setTaskName(name);
		}
	}
	
}
