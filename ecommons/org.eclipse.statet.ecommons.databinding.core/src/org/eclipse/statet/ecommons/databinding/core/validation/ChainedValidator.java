/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.validation;

import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.RETURN_TYPE;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ChainedValidator<T> implements IValidator<T> {
	
	
	private final ImList<IValidator<? super T>> validators;
	
	
	public ChainedValidator(final ImList<IValidator<? super T>> validators) {
		this.validators= validators;
	}
	
	
	@Override
	@NonNullByDefault({ RETURN_TYPE })
	public IStatus validate(final T value) {
		@NonNull IStatus status= ValidationStatus.ok();
		for (final IValidator<? super T> validator : this.validators) {
			final IStatus current= validator.validate(value);
			if (current.getSeverity() > status.getSeverity()) {
				status= current;
				if (status.getSeverity() >= IStatus.ERROR) {
					break;
				}
			}
		}
		return status;
	}
	
}
