/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core;

import org.eclipse.core.databinding.util.Policy;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class DataStatus implements IStatus {
	
	
	public static final int UPDATEABLE_ERROR= 0x03;
	
	
	public static final DataStatus OK_STATUS= new DataStatus(OK, "ok"); //$NON-NLS-1$
	
	
	public static int getInfoSeverity(final IStatus status) {
		if (status instanceof DataStatus) {
			return ((DataStatus) status).getInfoSeverity();
		}
		final @NonNull IStatus[] children= status.getChildren();
		if (children.length > 0) {
			int maxSeverity= -1;
			for (int i= 0; i < children.length; i++) {
				final int severity= getInfoSeverity(children[i]);
				if (severity > maxSeverity) {
					maxSeverity= severity;
				}
			}
			return maxSeverity;
		}
		return status.getSeverity();
	}
	
	
	private static final @NonNull IStatus[] NO_CHILDREN= new @NonNull IStatus[0];
	
	
	private final int severity;
	
	private final String message;
	
	
	public DataStatus(final int severity, final @Nullable String message) {
		this.severity= severity;
		this.message= (message != null) ? message : ""; //$NON-NLS-1$
	}
	
	
	@Override
	public final int getSeverity() {
		return (this.severity == UPDATEABLE_ERROR) ? WARNING : this.severity;
	}
	
	public final int getInfoSeverity() {
		return (this.severity == UPDATEABLE_ERROR) ? ERROR : this.severity;
	}
	
	@Override
	public final boolean isOK() {
		return (this.severity == 0);
	}
	
	@Override
	public boolean matches(final int severityMask) {
		return ((getSeverity() & severityMask) != 0);
	}
	
	
	@Override
	public String getPlugin() {
		return Policy.JFACE_DATABINDING;
	}
	
	@Override
	public int getCode() {
		return 0;
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
	
	@Override
	public @Nullable Throwable getException() {
		return null;
	}
	
	
	@Override
	public boolean isMultiStatus() {
		return false;
	}
	
	@Override
	public @NonNull IStatus[] getChildren() {
		return NO_CHILDREN;
	}
	
	
	@Override
	public int hashCode() {
		return this.severity * 31 + this.message.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final DataStatus other
						&& this.severity == other.severity
						&& this.message.equals(other.message) ));
	}
	
}
