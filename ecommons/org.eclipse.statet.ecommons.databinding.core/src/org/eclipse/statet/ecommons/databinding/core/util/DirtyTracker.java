/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.util;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.ObservableEvent;
import org.eclipse.core.databinding.observable.list.IListChangeListener;
import org.eclipse.core.databinding.observable.list.ListChangeEvent;
import org.eclipse.core.databinding.observable.list.ListDiffEntry;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Tracker for observable changes
 */
@NonNullByDefault
public class DirtyTracker implements IValueChangeListener<Object>, IChangeListener {
	
	
	private boolean dirty;
	
	
	/**
	 * Tracker tracking automatically all bindings of a context
	 */
	public DirtyTracker(final DataBindingContext dbc) {
		for (final Object obj : dbc.getBindings()) {
			track((Binding)obj, true);
		}
		dbc.getBindings().addListChangeListener(new IListChangeListener<Binding>() {
			@Override
			public void handleListChange(final ListChangeEvent<? extends Binding> event) {
				for (final ListDiffEntry<? extends Binding> diff : event.diff.getDifferences()) {
					track(diff.getElement(), diff.isAddition());
				}
			}
		});
	}
	
	/**
	 * Tracker tracking bindings added manually
	 */
	public DirtyTracker() {
	}
	
	
	public void add(final Binding binding) {
		track(binding, true);
	}
	
	public void remove(final Binding binding) {
		track(binding, false);
	}
	
	private void track(final Binding binding, final boolean add) {
		final IObservable obs= binding.getModel();
		if (obs instanceof IObservableValue) {
			final IObservableValue<?> value= (IObservableValue<?>)obs;
			if (add) {
				value.addValueChangeListener(this);
			}
			else {
				value.removeValueChangeListener(this);
			}
			return;
		}
		else {
			if (add) {
				obs.addChangeListener(this);
			}
			else {
				obs.removeChangeListener(this);
			}
		}
	}
	
	@Override
	public void handleValueChange(final ValueChangeEvent<?> event) {
		handleChange(event);
	}
	
	@Override
	public void handleChange(final @Nullable ChangeEvent event) {
		handleChange((ObservableEvent)event);
	}
	
	/**
	 * Called if a change is detected
	 * 
	 * @param event the source event, if available
	 */
	public void handleChange(final @Nullable ObservableEvent event) {
		this.dirty= true;
	}
	
	public void resetDirty() {
		this.dirty= false;
	}
	
	public boolean isDirty() {
		return this.dirty;
	}
	
}
