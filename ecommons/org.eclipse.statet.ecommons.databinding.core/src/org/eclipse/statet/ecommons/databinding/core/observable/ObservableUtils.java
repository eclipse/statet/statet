/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.observable;

import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.PARAMETER;
import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.RETURN_TYPE;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.observable.value.ValueDiff;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ObservableUtils {
	
	
	@NonNullByDefault({ PARAMETER, RETURN_TYPE })
	@SuppressWarnings("unchecked")
	public static <T> ValueChangeEvent<T> typed(final ValueChangeEvent<?> event, final IObservableValue<T> ref) {
		return (ValueChangeEvent<T>)event;
	}
	
	@NonNullByDefault({ PARAMETER, RETURN_TYPE })
	@SuppressWarnings("unchecked")
	public static <T> ValueDiff<T> typed(final ValueDiff<?> diff, final IObservableValue<T> ref) {
		return (ValueDiff<T>)diff;
	}
	
	
	private ObservableUtils() {
	}

}
