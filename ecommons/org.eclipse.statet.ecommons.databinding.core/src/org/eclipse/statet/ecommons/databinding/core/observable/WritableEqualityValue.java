/*=============================================================================#
 # Copyright (c) 2006, 2025 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.databinding: initial API and implementation of WritableValue
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial WritableEqualityValue
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.observable;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.AbstractObservableValue;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.Nullable;


public class WritableEqualityValue<TValue> extends AbstractObservableValue<TValue> {
	
	
	private final @Nullable Object valueType;
	
	private TValue value= null;
	
	
	/**
	 * Constructs a new instance.
	 *
	 * @param realm
	 * @param initialValue
	 *            can be <code>null</code>
	 * @param valueType
	 *            can be <code>null</code>
	 */
	public WritableEqualityValue(final @NonNull Realm realm,
			final @Nullable TValue initialValue, final @Nullable Object valueType) {
		super(realm);
		this.valueType= valueType;
		this.value= initialValue;
	}
	
	/**
	 * Constructs a new instance with the default realm.
	 *
	 * @param initialValue
	 *            can be <code>null</code>
	 * @param valueType
	 *            can be <code>null</code>
	 */
	public WritableEqualityValue(final @Nullable TValue initialValue, final @Nullable Object valueType) {
		this(Realm.getDefault(), initialValue, valueType);
	}
	
	/**
	 * Constructs a new instance with the default realm, a <code>null</code>
	 * value type, and a <code>null</code> value.
	 */
	public WritableEqualityValue() {
		this(null, null);
	}
	
	/**
	 * Constructs a new instance with the provided <code>realm</code>, a
	 * <code>null</code> value type, and a <code>null</code> initial value.
	 *
	 * @param realm
	 */
	public WritableEqualityValue(final @NonNull Realm realm) {
		this(realm, null, null);
	}
	
	
	@Override
	public Object getValueType() {
		return this.valueType;
	}
	
	@Override
	public TValue doGetValue() {
		return this.value;
	}
	
	@Override
	public void doSetValue(final TValue value) {
		final TValue oldValue= doGetValue();
		if (value != oldValue && (value == null || !value.equals(oldValue))) {
			fireValueChange(Diffs.createValueDiff(this.value, this.value= value));
		}
	}
	
}
