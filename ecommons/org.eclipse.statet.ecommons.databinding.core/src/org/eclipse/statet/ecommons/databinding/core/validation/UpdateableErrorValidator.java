/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.validation;

import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.RETURN_TYPE;

import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.databinding.core.DataStatus;


@NonNullByDefault
public class UpdateableErrorValidator<T> implements IValidator<T> {
	
	
	private final IValidator<T> validator;
	
	
	public UpdateableErrorValidator(final IValidator<T> validator) {
		this.validator= validator;
	}
	
	
	@Override
	@NonNullByDefault({ RETURN_TYPE })
	public IStatus validate(final T value) {
		final @NonNull IStatus status= this.validator.validate(value);
		if (status.getSeverity() == IStatus.ERROR) {
			return new DataStatus(DataStatus.UPDATEABLE_ERROR, status.getMessage());
		}
		return status;
	}
	
}
