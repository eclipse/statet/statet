/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.conversion;

import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.PARAMETER;
import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.RETURN_TYPE;

import java.util.function.Function;

import org.eclipse.core.databinding.conversion.IConverter;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault({ PARAMETER, RETURN_TYPE })
public interface ClassTypedConverter<F, T> extends IConverter<F, T> {
	
	
	@Override
	Class<F> getFromType();
	
	@Override
	Class<T> getToType();
	
	
	static <F, T> ClassTypedConverter<F, T> create(final Class<F> fromType, final Class<T> toType,
			final Function<? super F, ? extends T> function) {
		return new ClassTypedConverter<>() {
			@Override
			public Class<F> getFromType() {
				return fromType;
			}
			@Override
			public Class<T> getToType() {
				return toType;
			}
			@Override
			public T convert(final F fromObject) {
				return function.apply(fromObject);
			}
		};
	}
	
	
	static <F, T> ClassTypedConverter<F, T> createIdentity(final Class<F> fromType, final Class<T> toType) {
		return new ClassTypedConverter<>() {
			@Override
			public Class<F> getFromType() {
				return fromType;
			}
			@Override
			public Class<T> getToType() {
				return toType;
			}
			@Override
			@SuppressWarnings("unchecked")
			public T convert(final F fromObject) {
				return (T)fromObject;
			}
		};
	}
	
	static <T> ClassTypedConverter<T, T> createIdentity(final Class<T> type) {
		return createIdentity(type, type);
	}
	
}
