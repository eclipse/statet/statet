/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.validation;

import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.FIELD;
import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.PARAMETER;
import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.RETURN_TYPE;

import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault({ PARAMETER, RETURN_TYPE, FIELD })
public class ConverterValidator<T> implements IValidator<T> {
	
	
	private final IConverter<T, ?> converter;
	
	
	public ConverterValidator(final IConverter<T, ?> converter) {
		this.converter= converter;
	}
	
	
	@Override
	public IStatus validate(final T value) {
		try {
			this.converter.convert(value);
			return ValidationStatus.ok();
		}
		catch (final IllegalArgumentException e) {
			return ValidationStatus.error(e.getLocalizedMessage());
		}
	}
	
}
