/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.databinding.core.observable;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.value.AbstractObservableValue;
import org.eclipse.core.databinding.observable.value.ComputedValue;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;


/**
 * Similar to {@link ComputedValue}, but using a fixed list of dependencies.
 */
public abstract class ComputedOnChangeValue<TValue> extends AbstractObservableValue<TValue>
		implements IChangeListener {
	
	
	private final Object valueType;
	
	private final ImList<IObservable> dependencies;
	
	private boolean setting;
	
	private TValue value;
	
	
	public ComputedOnChangeValue(final Object valueType, final IObservable... dependencies) {
		super(dependencies[0].getRealm());
		this.valueType= valueType;
		this.dependencies= ImCollections.newList(dependencies);
		for (final IObservable obs : dependencies) {
			obs.addChangeListener(this);
		}
	}
	
	@Override
	public synchronized void dispose() {
		for (final IObservable obs : this.dependencies) {
			obs.removeChangeListener(this);
		}
		super.dispose();
	}
	
	
	@Override
	public void handleChange(final ChangeEvent event) {
		if (!this.setting) {
			final TValue newValue= calculate();
			final TValue oldValue= this.value;
			if ((oldValue != null) ? !oldValue.equals(newValue) : null != newValue) {
				fireValueChange(Diffs.createValueDiff(oldValue, this.value= newValue));
			}
		}
	}
	
	@Override
	public Object getValueType() {
		return this.valueType;
	}
	
	@Override
	protected final TValue doGetValue() {
		return calculate();
	}
	
	@Override
	protected final void doSetValue(final TValue value) {
		this.setting= true;
		try {
			extractAndSet(value);
			this.value= value;
		}
		finally {
			this.setting= false;
		}
	}
	
	protected abstract TValue calculate();
	
	protected void extractAndSet(final Object value) {
		throw new UnsupportedOperationException();
	}
	
}
