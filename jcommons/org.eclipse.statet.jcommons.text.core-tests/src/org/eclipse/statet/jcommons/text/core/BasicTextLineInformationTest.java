/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class BasicTextLineInformationTest {
	
	
	public BasicTextLineInformationTest() {
	}
	
	
	private BasicTextLineInformation createEmpty() {
		return new BasicTextLineInformation(new int[] { 0, 0 });
	}
	
	private BasicTextLineInformation createSingleLine() {
		return new BasicTextLineInformation(new int[] { 0, 3 });
	}
	
	private BasicTextLineInformation createMultiLine() {
		return new BasicTextLineInformation(new int[] { 0, 3, 8, 40, 40 });
	}
	
	
	@Test
	public void getNumberOfLines_Empty() {
		final BasicTextLineInformation lines= createEmpty();
		
		assertEquals(1, lines.getNumberOfLines());
	}
	
	@Test
	public void getNumberOfLines_SingleLine() {
		final BasicTextLineInformation lines= createSingleLine();
		
		assertEquals(1, lines.getNumberOfLines());
	}
	
	@Test
	public void getNumberOfLines_MultiLines() {
		final BasicTextLineInformation lines= createMultiLine();
		
		assertEquals(4, lines.getNumberOfLines());
	}
	
	
	@Test
	public void getStartOffset_Empty() {
		final BasicTextLineInformation lines= createEmpty();
		
		assertEquals(0, lines.getStartOffset(0));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getStartOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getStartOffset(1));
	}
	
	@Test
	public void getStartOffset_SingleLine() {
		final BasicTextLineInformation lines= createSingleLine();
		
		assertEquals(0, lines.getStartOffset(0));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getStartOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getStartOffset(1));
	}
	
	@Test
	public void getStartOffset_MultiLines() {
		final BasicTextLineInformation lines= createMultiLine();
		
		assertEquals(0, lines.getStartOffset(0));
		assertEquals(3, lines.getStartOffset(1));
		assertEquals(8, lines.getStartOffset(2));
		assertEquals(40, lines.getStartOffset(3));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getStartOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getStartOffset(4));
	}
	
	
	@Test
	public void getEndOffset_Empty() {
		final BasicTextLineInformation lines= createEmpty();
		
		assertEquals(0, lines.getEndOffset(0));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getEndOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getEndOffset(1));
	}
	
	@Test
	public void getEndOffset_SingleLine() {
		final BasicTextLineInformation lines= createSingleLine();
		
		assertEquals(3, lines.getEndOffset(0));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getEndOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getEndOffset(1));
	}
	
	@Test
	public void getEndOffset_MultiLines() {
		final BasicTextLineInformation lines= createMultiLine();
		
		assertEquals(3, lines.getEndOffset(0));
		assertEquals(8, lines.getEndOffset(1));
		assertEquals(40, lines.getEndOffset(2));
		assertEquals(40, lines.getEndOffset(3));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getEndOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getEndOffset(4));
	}
	
	
	@Test
	public void getLength_Empty() {
		final BasicTextLineInformation lines= createEmpty();
		
		assertEquals(0, lines.getLength(0));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getLength(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getLength(1));
	}
	
	@Test
	public void getLength_SingleLine() {
		final BasicTextLineInformation lines= createSingleLine();
		
		assertEquals(3, lines.getLength(0));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getLength(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getLength(1));
	}
	
	@Test
	public void getLength_MultiLines() {
		final BasicTextLineInformation lines= createMultiLine();
		
		assertEquals(3, lines.getLength(0));
		assertEquals(5, lines.getLength(1));
		assertEquals(32, lines.getLength(2));
		assertEquals(0, lines.getLength(3));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getLength(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getLength(4));
	}
	
	
	@Test
	public void getLineOfOffset_Empty() {
		final BasicTextLineInformation lines= createEmpty();
		
		assertEquals(0, lines.getLineOfOffset(0));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getLineOfOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getLineOfOffset(1));
	}
	
	@Test
	public void getLineOfOffset_SingleLine() {
		final BasicTextLineInformation lines= createSingleLine();
		
		assertEquals(0, lines.getLineOfOffset(0));
		assertEquals(0, lines.getLineOfOffset(1));
		assertEquals(0, lines.getLineOfOffset(2));
		assertEquals(0, lines.getLineOfOffset(3));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getLineOfOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getLineOfOffset(4));
	}
	
	@Test
	public void getLineOfOffset_MultiLines() {
		final BasicTextLineInformation lines= createMultiLine();
		
		assertEquals(0, lines.getLineOfOffset(0));
		assertEquals(0, lines.getLineOfOffset(1));
		assertEquals(0, lines.getLineOfOffset(2));
		assertEquals(1, lines.getLineOfOffset(3));
		assertEquals(1, lines.getLineOfOffset(7));
		assertEquals(2, lines.getLineOfOffset(8));
		assertEquals(2, lines.getLineOfOffset(39));
		assertEquals(3, lines.getLineOfOffset(40));
		
		assertThrows(IllegalArgumentException.class, () -> lines.getLineOfOffset(-1));
		assertThrows(IllegalArgumentException.class, () -> lines.getLineOfOffset(41));
	}
	
}
