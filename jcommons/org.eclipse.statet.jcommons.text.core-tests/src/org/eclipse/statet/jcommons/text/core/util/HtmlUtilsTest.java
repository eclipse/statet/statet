/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class HtmlUtilsTest {
	
	
	@Test
	public void getEntityName_fromName() {
		assertEquals("amp", HtmlUtils.getEntityReference("amp"));
	}
	
	@Test
	public void getEntityName_fromHtmlEntity() {
		assertEquals("amp", HtmlUtils.getEntityReference("&amp;"));
		
		assertEquals("amp", HtmlUtils.getEntityReference("&amp"));
	}
	
	
	@Test
	public void appendContent() {
		final StringBuilder sb= new StringBuilder();
		
		sb.setLength(0);
		HtmlUtils.appendContent(sb, "foo bar");
		assertEquals("foo bar", sb.toString());
		
		sb.setLength(0);
		HtmlUtils.appendContent(sb, "$\"&'<>$");
		assertEquals("$&quot;&amp;&#39;&lt;&gt;$", sb.toString());
		
		sb.setLength(0);
		sb.append("<tag>");
		HtmlUtils.appendContent(sb, "\"foo bar\"");
		assertEquals("<tag>&quot;foo bar&quot;", sb.toString());
	}
	
	@Test
	@SuppressWarnings("null")
	public void appendContent_argCheck() {
		assertThrows(NullPointerException.class, () ->
				HtmlUtils.appendContent((StringBuilder)null, "abc") );
		assertThrows(NullPointerException.class, () ->
		HtmlUtils.appendContent(new StringBuilder(), null) );
	}
	
	@Test
	public void writeContent() {
		final var outputStream= new ByteArrayOutputStream();
		final PrintWriter writer= new PrintWriter(outputStream, false, StandardCharsets.UTF_8);
		
		outputStream.reset();
		HtmlUtils.writeContent(writer, "foo bar");
		writer.flush();
		assertEquals("foo bar", outputStream.toString(StandardCharsets.UTF_8));
		
		outputStream.reset();
		HtmlUtils.writeContent(writer, "$\"&'<>$");
		writer.flush();
		assertEquals("$&quot;&amp;&#39;&lt;&gt;$", outputStream.toString(StandardCharsets.UTF_8));
		
		outputStream.reset();
		writer.append("<tag>");
		HtmlUtils.writeContent(writer, "\"foo bar\"");
		writer.flush();
		assertEquals("<tag>&quot;foo bar&quot;", outputStream.toString(StandardCharsets.UTF_8));
	}
	
	@Test
	@SuppressWarnings("null")
	public void writeContent_argCheck() {
		final var outputStream= new ByteArrayOutputStream();
		final PrintWriter writer= new PrintWriter(outputStream, false, StandardCharsets.UTF_8);
		
		assertThrows(NullPointerException.class, () ->
				HtmlUtils.writeContent((PrintWriter)null, "abc") );
		assertThrows(NullPointerException.class, () ->
				HtmlUtils.writeContent(writer, null) );
	}
	
	
	@Test
	public void HtmlStringBuilder() {
		final var output= new HtmlUtils.HtmlStringBuilder();
		
		output.append("<tag>");
		output.appendContent("\"foo bar\"");
		output.append("for bar</tag> ", 7, 14);
		output.append(17);
		
		assertEquals("<tag>&quot;foo bar&quot;</tag> 17", output.toString());
	}
	
	@Test
	@SuppressWarnings("null")
	public void HtmlStringBuilder_argCheck() {
		assertThrows(IllegalArgumentException.class, () ->
				new HtmlUtils.HtmlStringBuilder(-1) );
		
		final var output= new HtmlUtils.HtmlStringBuilder(100);
		
		assertThrows(NullPointerException.class, () ->
				output.append((String)null) );
		assertThrows(NullPointerException.class, () ->
				output.append((String)null, 0, 0) );
		assertThrows(NullPointerException.class, () ->
				output.appendContent((String)null) );
	}
	
}
