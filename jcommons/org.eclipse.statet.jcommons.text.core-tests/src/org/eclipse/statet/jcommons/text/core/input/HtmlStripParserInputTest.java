/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.util.HtmlStripParserInput;
import org.eclipse.statet.jcommons.text.core.util.HtmlUtils;
import org.eclipse.statet.jcommons.text.core.util.HtmlUtils.Entity;


@NonNullByDefault
public class HtmlStripParserInputTest extends AbstractTextParserInputTest<HtmlStripParserInput> {
	
	
	public HtmlStripParserInputTest() {
	}
	
	
	@Test
	public void skipTag() {
		final String s= Utils.COUNTER_STRING;
		final StringBuilder sb= new StringBuilder(s);
		sb.insert(100, "<html>");
		sb.insert(200, "</html>");
		
		this.input= new HtmlStripParserInput(sb.toString());
		this.input.init();
		
		assertChars(s, 0, 100);
		assertEquals(100, this.input.getLengthInSource(100));
		this.input.consume(100);
		// <html>
		assertEquals(106, this.input.getIndex());
		assertChars(s, 100, s.length());
		assertEquals(sb.length() - 106, this.input.getLengthInSource(s.length() - 100));
		assertEquals(new BasicTextRegion(106 + 50, 106 + 150 + 7),
				this.input.getRegionInSource(50, 150) );
		this.input.consume(s.length() - 100);
		
		assertEquals(TextParserInput.EOF, this.input.get(0));
		assertEquals(0, this.input.getLengthInSource(0));
	}
	
	@Test
	public void skipTagAtStart() {
		final String s= Utils.COUNTER_STRING;
		final StringBuilder sb= new StringBuilder(s);
		sb.insert(0, "<html></html>");
		
		this.input= new HtmlStripParserInput(sb.toString());
		this.input.init();
		
		assertEquals(13, this.input.getIndex());
		assertChars(s);
		assertEquals(new BasicTextRegion(50 + 13, 150 + 13),
				this.input.getRegionInSource(50, 150) );
		
		assertEquals(TextParserInput.EOF, this.input.get(s.length()));
	}
	
	@Test
	public void skipTagWithAttr() {
		final String s= Utils.COUNTER_STRING;
		final StringBuilder sb= new StringBuilder(s);
		final String tag1;
		sb.insert(100, tag1= "<html attr=\"abc\">");
		sb.insert(200, "<html attr=\'abc\'>");
		
		this.input= new HtmlStripParserInput(sb.toString());
		this.input.init();
		
		assertChars(s);
		assertEquals(new BasicTextRegion(50, 150 + tag1.length()),
				this.input.getRegionInSource(50, 150) );
		
		assertEquals(TextParserInput.EOF, this.input.get(s.length()));
	}
	
	@Test
	public void skipTagWithAttrSpecial() {
		final String s= Utils.COUNTER_STRING;
		final StringBuilder sb= new StringBuilder(s);
		final String tag1, tag2, tag3;
		sb.insert(100, tag1= "<html attr=\"ab'c\">");
		sb.insert(200, tag2= "<html attr=\'ab\"c\'>");
		sb.insert(300, tag3= "<html attr=\"<ignore\">");
		
		this.input= new HtmlStripParserInput(sb.toString());
		this.input.init();
		
		assertChars(s);
		assertEquals(new BasicTextRegion(50, 150 + tag1.length()),
				this.input.getRegionInSource(50, 150) );
		assertEquals(new BasicTextRegion(150 + tag1.length(), 500 + tag1.length() + tag2.length() + tag3.length()),
				this.input.getRegionInSource(150, 500) );
		
		assertEquals(TextParserInput.EOF, this.input.get(s.length()));
	}
	
	@Test
	public void htmlDoc() {
		final String s= Utils.COUNTER_STRING;
		final StringBuilder sb= new StringBuilder(s);
		sb.insert(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" +
				"    \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" +
				"<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"de\">");
		sb.insert(1000, "<!-- comment -->");
		sb.insert(1100, "<!--->");
		sb.insert(4000, "<a long=\"tag " + Utils.COUNTER_STRING.substring(1000, 3000) + "\">");
		sb.append("</html>");
		
		this.input= new HtmlStripParserInput(sb.toString());
		this.input.init();
		
		assertChars(s);
		
		assertEquals(TextParserInput.EOF, this.input.get(s.length()));
		
		assertTrue(0x5000 > this.input.getBuffer().length);
	}
	
	private static int replaceByEntity(final StringBuilder sb, final Entity entity) {
		final int start= sb.indexOf(entity.getString());
		sb.replace(start, start + entity.getString().length(), '&' + entity.getName() + ';');
		return start;
	}
	
	@Test
	public void decodeEntity() {
		final String s= Utils.COUNTER_STRING;
		final StringBuilder sb= new StringBuilder(s);
		final int entityIdx= replaceByEntity(sb, nonNullAssert(HtmlUtils.getNamedEntity("angle")));
		
		this.input= new HtmlStripParserInput(sb.toString());
		this.input.init();
		
		assertChars(s);
		assertEquals(entityIdx, this.input.getLengthInSource(entityIdx));
		assertEquals(entityIdx + 7, this.input.getLengthInSource(entityIdx + 1));
		assertEquals(sb.length(), this.input.getLengthInSource(s.length()));
		
		assertEquals(TextParserInput.EOF, this.input.get(s.length()));
	}
	
}
