/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core.input;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class TextParserInputMatchTest {
	
	
	private StringParserInput input;
	
	
	@SuppressWarnings("null")
	public TextParserInputMatchTest() {
	}
	
	
	@Test
	public void get() {
		final String s= Utils.COUNTER_STRING;
		this.input= new StringParserInput(s);
		this.input.init();
		
		this.input.consume('a');
		assertTrue(this.input.get(0) == 'a');
	}
	
	@Test
	public void matches1() {
		final String s= Utils.COUNTER_STRING;
		this.input= new StringParserInput(s);
		this.input.init();
		
		this.input.consume('a');
		assertTrue(this.input.matches(0, 'a'));
		assertTrue(this.input.matches(1, 'b'));
		assertTrue(this.input.matches(2, 'c'));
		assertFalse(this.input.matches(0, 'x'));
		assertFalse(this.input.matches(3, 'c'));
	}
	
	@Test
	public void matches2() {
		final String s= Utils.COUNTER_STRING;
		this.input= new StringParserInput(s);
		this.input.init();
		
		this.input.consume('a');
		assertTrue(this.input.matches(0, 'a', 'b'));
		assertTrue(this.input.matches(1, 'b', 'c'));
		assertTrue(this.input.matches(2, 'c', 'd'));
		assertFalse(this.input.matches(0, 'x', 'b'));
		assertFalse(this.input.matches(3, 'c', 'e'));
		assertFalse(this.input.matches(3, 'd', 'd'));
	}
	
	@Test
	public void matches3() {
		final String s= Utils.COUNTER_STRING;
		this.input= new StringParserInput(s);
		this.input.init();
		
		this.input.consume('a');
		assertTrue(this.input.matches(0, 'a', 'b', 'c'));
		assertTrue(this.input.matches(1, 'b', 'c', 'd'));
		assertTrue(this.input.matches(2, 'c', 'd', 'e'));
		assertFalse(this.input.matches(0, 'x', 'b', 'c'));
		assertFalse(this.input.matches(3, 'c', 'e', 'f'));
		assertFalse(this.input.matches(3, 'd', 'e', 'e'));
	}
	
	@Test
	public void matchesArray() {
		final String s= Utils.COUNTER_STRING;
		this.input= new StringParserInput(s);
		this.input.init();
		
		this.input.consume('a');
		assertTrue(this.input.matches(0, new char[] { }));
		assertTrue(this.input.matches(0, new char[] { 'a' }));
		assertTrue(this.input.matches(0, new char[] { 'a', 'b', 'c', 'd' }));
		assertTrue(this.input.matches(1, new char[] { 'b' }));
		assertTrue(this.input.matches(1, new char[] { 'b', 'c', 'd', 'e' }));
		assertTrue(this.input.matches(2, new char[] { 'c', 'd', 'e', 'f' }));
		assertFalse(this.input.matches(0, new char[] { 'x', 'b', 'c', 'd' }));
		assertFalse(this.input.matches(3, new char[] { 'c', 'e', 'f', 'g' }));
		assertFalse(this.input.matches(3, new char[] { 'd', 'e', 'f', 'f' }));
	}
	
	
}
