/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;


@NonNullByDefault
public class RegionParserInputTest extends AbstractTextParserInputTest<RegionParserInput> {
	
	
	private static String createRegionString(final String s, final ImList<? extends TextRegion> regions) {
		final StringBuilder sb= new StringBuilder();
		for (final TextRegion region : regions) {
			sb.append(s, region.getStartOffset(), region.getEndOffset());
		}
		return sb.toString();
	}
	
	private static int[] createRegionIndexes(final ImList<? extends TextRegion> regions) {
		int l= 0;
		for (final TextRegion region : regions) {
			l+= region.getLength();
		}
		final int[] indexes= new int[l];
		int idx= 0;
		for (final TextRegion region : regions) {
			int index= region.getStartOffset();
			for (int i= 0; i < region.getLength(); i++) {
				indexes[idx++]= index++;
			}
		}
		return indexes;
	}
	
	
	public RegionParserInputTest() {
	}
	
	
	@Test
	public void init() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.input= new RegionParserInput(s, null);
		
		this.input.init();
		assertEquals(0, this.input.getStartIndex());
		assertEquals(800, this.input.getStopIndex());
	}
	
	@Test
	public void init_Region_argCheck() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.input= new RegionParserInput(s, null);
		
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.input.init(-1, 400);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.input.init(0, 801);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			this.input.init(800, 400);
		});
	}
	
	@Test
	public void init_Region() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.input= new RegionParserInput(s, ImCollections.newList(new BasicTextRegion(0, 800)));
		this.input.init(100, 200);
		
		assertEquals(100, this.input.getStartIndex());
		assertEquals(200, this.input.getStopIndex());
	}
	
	@Test
	public void read() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.input= new RegionParserInput(s, null);
		
		final ImList<? extends TextRegion> regions= ImCollections.newList(
				new BasicTextRegion(100, 200),
				new BasicTextRegion(300, 700) );
		this.input.reset(regions).init();
		final String rs= createRegionString(s, regions);
		
		assertChars(rs);
		
		assertEquals(TextParserInput.EOF, this.input.get(rs.length()));
		
		assertEquals(s.length(), Utils.getBufferLength(this.input));
	}
	
	@Test
	public void readRegion() {
		final String s= Utils.COUNTER_STRING.substring(0, 800);
		this.input= new RegionParserInput(s, null);
		
		final ImList<? extends TextRegion> regions= ImCollections.newList(
				new BasicTextRegion(100, 200),
				new BasicTextRegion(300, 700) );
		this.input.reset(regions).init(200, 500);
		
		assertChars(s, 300, 500);
		
		assertEquals(TextParserInput.EOF, this.input.get(200));
		
		assertEquals(s.length(), Utils.getBufferLength(this.input));
	}
	
	@Test
	public void updateBuffer() {
		final String s= Utils.COUNTER_STRING;
		this.input= new RegionParserInput(s, null);
		
		final ImList<? extends TextRegion> regions= ImCollections.newList(
				new BasicTextRegion(0, 1000),
				new BasicTextRegion(2000, 3000),
				new BasicTextRegion(4000, 5000),
				new BasicTextRegion(6000, 7000) );
		this.input.reset(regions).init();
		
		final String rs= createRegionString(s, regions);
		final int[] ri= createRegionIndexes(regions);
		readConsume(rs, ri, 0, rs.length(), 100);
		
		assertEquals(TextParserInput.EOF, this.input.get(0));
		
		assertEquals(Utils.DEFAULT_BUFFER_SIZE, Utils.getBufferLength(this.input));
	}
	
	@Test
	public void increaseBuffer() {
		final String s= Utils.COUNTER_STRING;
		this.input= new RegionParserInput(s, null);
		
		final ImList<? extends TextRegion> regions= ImCollections.newList(
				new BasicTextRegion(0, 1000),
				new BasicTextRegion(2000, 3000),
				new BasicTextRegion(4000, 5000),
				new BasicTextRegion(6000, 7000) );
		this.input.reset(regions).init();
		
		final String rs= createRegionString(s, regions);
		
		assertChars(rs);
		
		assertEquals(TextParserInput.EOF, this.input.get(rs.length()));
	}
	
	@Test
	public void consume1() {
		final String s= Utils.COUNTER_STRING;
		this.input= new RegionParserInput(s, null);
		
		final ImList<? extends TextRegion> regions= ImCollections.newList(
				new BasicTextRegion(0, 1000),
				new BasicTextRegion(2000, 3000),
				new BasicTextRegion(4000, 5000),
				new BasicTextRegion(6000, 7000) );
		this.input.reset(regions).init();
		
		final String rs= createRegionString(s, regions);
		final int[] ri= createRegionIndexes(regions);
		readConsume(rs, ri, 0, rs.length(), 1);
		
		assertEquals(TextParserInput.EOF, this.input.get(0));
	}
	
	@Test
	public void combined() {
		final String s= Utils.COUNTER_STRING;
		this.input= new RegionParserInput(s, null);
		
		final ImList<? extends TextRegion> regions= ImCollections.newList(
				new BasicTextRegion(5, 105),
				new BasicTextRegion(2000, 3000),
				new BasicTextRegion(4000, 4777),
				new BasicTextRegion(6000, 7111) );
		this.input.reset(regions).init();
		
		final String rs= createRegionString(s, regions);
		final int[] ri= createRegionIndexes(regions);
		readConsume(rs, ri, 0, rs.length(), 2351);
		
		assertEquals(TextParserInput.EOF, this.input.get(0));
		
		assertTrue(0x1000 >= Utils.getBufferLength(this.input));
	}
	
}
