/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class UriUtilsTest {
	
	
	public UriUtilsTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void isValidScheme_checkArgs() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.isValidScheme(null, 0, 4);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.isValidScheme("file", -1, 4);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.isValidScheme("file", 0, 5);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.isValidScheme("file", 1, 0);
		});
	}
	
	@Test
	public void isValidScheme() {
		assertTrue(UriUtils.isValidScheme("file://", 0, 4));
		assertTrue(UriUtils.isValidScheme(":file://", 1, 5));
		assertTrue(UriUtils.isValidScheme("a", 0, 1));
		assertTrue(UriUtils.isValidScheme("test+v1.2-3://", 0, 11));
		assertFalse(UriUtils.isValidScheme("file://", 0, 0));
		assertFalse(UriUtils.isValidScheme(":file://", 0, 5));
		assertFalse(UriUtils.isValidScheme("test+v1.2-3://", 0, 12));
		assertTrue(UriUtils.isValidScheme("test+v1.2-3"));
		assertFalse(UriUtils.isValidScheme("test+v1.2-3:"));
		assertFalse(UriUtils.isValidScheme("123"));
	}
	
	@Test
	@SuppressWarnings("null")
	public void normalizeScheme_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.normalizeScheme(null);
		});
		assertThrows(URISyntaxException.class, () -> {
			UriUtils.normalizeScheme("");
		});
		assertThrows(URISyntaxException.class, () -> {
			UriUtils.normalizeScheme("file_test");
		});
	}
	
	@Test
	public void normalizeScheme() throws URISyntaxException {
		assertEquals("file", UriUtils.normalizeScheme("file"));
		assertEquals("file", UriUtils.normalizeScheme("FILE"));
		assertEquals("file", UriUtils.normalizeScheme("File"));
		assertEquals("file", UriUtils.normalizeScheme("fIle"));
		assertEquals("file", UriUtils.normalizeScheme("filE"));
		assertEquals("test+v1.2-3", UriUtils.normalizeScheme("test+v1.2-3"));
		assertEquals("test+v1.2-3", UriUtils.normalizeScheme("test+V1.2-3"));
	}
	
	
	@Test
	public void isFileUrl() throws URISyntaxException {
		assertTrue(UriUtils.isFileUrl(new URI("file", "/test/file.jar", null)));
		assertTrue(UriUtils.isFileUrl(new URI("FILE", "/test/file.jar", null)));
		assertTrue(UriUtils.isFileUrl(new URI("File", "/test/file.jar", null)));
		
		assertFalse(UriUtils.isFileUrl(new URI(null, "", null)));
		assertFalse(UriUtils.isFileUrl(new URI(null, "file", null)));
		assertFalse(UriUtils.isFileUrl(new URI(null, "file/test/file.jar!/", null)));
		assertFalse(UriUtils.isFileUrl(new URI("filx", "/test/file.jar!/", null)));
		assertFalse(UriUtils.isFileUrl(new URI("filesystem", "/test/file.jar", null)));
		assertFalse(UriUtils.isFileUrl(new URI("jar", "/test/file.jar", null)));
	}
	
	@Test
	public void isFileUrlString() {
		assertTrue(UriUtils.isFileUrl("file:/test/file.jar"));
		assertTrue(UriUtils.isFileUrl("FILE:/test/file.jar"));
		assertTrue(UriUtils.isFileUrl("File:/test/file.jar"));
		
		assertFalse(UriUtils.isFileUrl(""));
		assertFalse(UriUtils.isFileUrl("file"));
		assertFalse(UriUtils.isFileUrl("file/test/file.jar"));
		assertFalse(UriUtils.isFileUrl("filÉ:/test/file.jar"));
		assertFalse(UriUtils.isFileUrl("filesystem:/test/file.jar"));
		assertFalse(UriUtils.isFileUrl("jar:/test/file.jar"));
	}
	
	
	@Test
	public void isJarUrl() throws URISyntaxException {
		assertTrue(UriUtils.isJarUrl(new URI("jar", "file:/test/file.jar!/", null)));
		assertTrue(UriUtils.isJarUrl(new URI("JAR", "file:/test/file.jar!/", null)));
		assertTrue(UriUtils.isJarUrl(new URI("Jar", "file:/test/file.jar!/", null)));
		
		assertTrue(UriUtils.isJarUrl(new URI("jar", "file:/test/file.jar!/p1/file.txt", null)));
		assertTrue(UriUtils.isJarUrl(new URI("jar", "nested:/test/file.jar/!p1/file.txt", null)));
		
		assertFalse(UriUtils.isJarUrl(new URI(null, "", null)));
		assertFalse(UriUtils.isJarUrl(new URI(null, "jar", null)));
		assertFalse(UriUtils.isJarUrl(new URI(null, "jar/test/file.jar!/", null)));
		assertFalse(UriUtils.isJarUrl(new URI("jarfile", "/test/file.jar!/", null)));
		assertFalse(UriUtils.isJarUrl(new URI("file", "/test/file.jar!/", null)));
	}
	
	@Test
	@SuppressWarnings("null")
	public void isJarUrlString_checkArgs() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.isJarUrl((String)null);
		});
	}
	
	@Test
	public void isJarUrlString() {
		assertTrue(UriUtils.isJarUrl("jar:file:/test/file.jar!/"));
		assertTrue(UriUtils.isJarUrl("JAR:file:/test/file.jar!/"));
		assertTrue(UriUtils.isJarUrl("Jar:file:/test/file.jar!/"));
		
		assertTrue(UriUtils.isJarUrl("jar:file:/test/file.jar!/p1/file.txt"));
		assertTrue(UriUtils.isJarUrl("jar:nested:/test/file.jar/!p1/file.txt"));
		
		assertFalse(UriUtils.isJarUrl(""));
		assertFalse(UriUtils.isJarUrl("jar"));
		assertFalse(UriUtils.isJarUrl("jar/test/file.jar!/"));
		assertFalse(UriUtils.isJarUrl("jax:/test/file.jar!/"));
		assertFalse(UriUtils.isJarUrl("jarfile:/test/file.jar!/"));
		assertFalse(UriUtils.isJarUrl("file:/test/file.jar!/"));
	}
	
	@Test
	@SuppressWarnings("null")
	public void getArchiveUrl_String_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.getArchiveUrl((String)null);
		});
	}
	
	@Test
	public void getArchiveUrl_String_JarFile() throws URISyntaxException {
		assertJarArchiveUrl(new URI("jar", "file:/test/file.jar!/", null),
				new URI("file", "/test/file.jar", null),
				new URI("file", "/test/file.jar", null), "",
				UriUtils.getArchiveUrl("jar:file:/test/file.jar!/") );
		
		assertJarArchiveUrl(new URI("jar", "file:/test/file.jar!/p1/file.txt", null),
				new URI("file", "/test/file.jar", null),
				new URI("file", "/test/file.jar", null), "p1/file.txt",
				UriUtils.getArchiveUrl("jar:file:/test/file.jar!/p1/file.txt") );
	}
	
	@Test
	public void getArchiveUrl_String_NestedJarNested() throws URISyntaxException {
		assertJarArchiveUrl(new URI("jar", "nested:/test/file.jar/!p1/nested.jar!/", null),
				new URI("file", "/test/file.jar", null),
				new URI("jar", "file:/test/file.jar!/p1/nested.jar", null), "",
				UriUtils.getArchiveUrl("jar:nested:/test/file.jar/!p1/nested.jar!/") );
		
		assertJarArchiveUrl(new URI("jar", "nested:/test/file.jar/!p1/nested.jar!/p2/file.txt", null),
				new URI("file", "/test/file.jar", null),
				new URI("jar", "file:/test/file.jar!/p1/nested.jar", null), "p2/file.txt",
				UriUtils.getArchiveUrl("jar:nested:/test/file.jar/!p1/nested.jar!/p2/file.txt") );
	}
	
	@Test
	public void getArchiveUrl_String_Other() throws URISyntaxException {
		assertNull(UriUtils.getArchiveUrl("other:/p1/file.txt"));
	}
	
	@Test
	public void getArchiveUrl_Uri_JarFile() throws URISyntaxException {
		assertJarArchiveUrl(new URI("jar", "file:/test/file.jar!/", null),
				new URI("file", "/test/file.jar", null),
				new URI("file", "/test/file.jar", null), "",
				UriUtils.getArchiveUrl(new URI("jar", "file:/test/file.jar!/", null)) );
		
		assertJarArchiveUrl(new URI("jar", "file:/test/file.jar!/p1/file.txt", null),
				new URI("file", "/test/file.jar", null),
				new URI("file", "/test/file.jar", null), "p1/file.txt",
				UriUtils.getArchiveUrl(new URI("jar", "file:/test/file.jar!/p1/file.txt", null)) );
	}
	
	@Test
	public void getArchiveUrl_Uri_NestedJarNested() throws URISyntaxException {
		assertJarArchiveUrl(new URI("jar", "nested:/test/file.jar/!p1/nested.jar!/", null),
				new URI("file", "/test/file.jar", null),
				new URI("jar", "file:/test/file.jar!/p1/nested.jar", null), "",
				UriUtils.getArchiveUrl(new URI("jar", "nested:/test/file.jar/!p1/nested.jar!/", null)) );
		
		assertJarArchiveUrl(new URI("jar", "nested:/test/file.jar/!p1/nested.jar!/p2/file.txt", null),
				new URI("file", "/test/file.jar", null),
				new URI("jar", "file:/test/file.jar!/p1/nested.jar", null), "p2/file.txt",
				UriUtils.getArchiveUrl(new URI("jar", "nested:/test/file.jar/!p1/nested.jar!/p2/file.txt", null)) );
	}
	
	@Test
	@SuppressWarnings("null")
	public void getArchiveUrl_Uri_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.getArchiveUrl((URI)null);
		});
	}
	
	@Test
	public void getArchiveUrl_Uri_Other() throws URISyntaxException {
		assertNull(UriUtils.getArchiveUrl(new URI("other", "/p1/file.txt", null)));
	}
	
	private void assertJarArchiveUrl(final URI expectedUrl,
			final URI expectedOuterUrl, final URI expectedInnerUrl, final String expectedPath,
			final @Nullable ArchiveUrl actualUrl) throws URISyntaxException {
		assertNotNull(actualUrl);
		
		assertEquals(expectedUrl, actualUrl.getUrl());
		assertEquals(expectedUrl.toString(), actualUrl.getUrlString());
		
		assertEquals(!expectedInnerUrl.equals(expectedOuterUrl), actualUrl.isNested());
		
		assertEquals("jar", actualUrl.getOuterArchiveType());
		assertEquals(expectedOuterUrl.getScheme(), actualUrl.getOuterArchiveScheme());
		assertEquals(expectedOuterUrl, actualUrl.getOuterArchiveUrl());
		assertEquals(expectedOuterUrl.toString(), actualUrl.getOuterArchiveUrlString());
		
		assertEquals("jar", actualUrl.getInnerArchiveType());
		assertEquals(expectedInnerUrl, actualUrl.getInnerArchiveUrl());
		assertEquals(expectedInnerUrl.toString(), actualUrl.getInnerArchiveUrlString());
		
		assertEquals(expectedPath, actualUrl.getInnerEntryPathString());
	}
	
	@Test
	@SuppressWarnings("null")
	public void getJarFileUrl_checkJarUrl() throws URISyntaxException {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.getJarFileUrl((URI)null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.getJarFileUrl(new URI("file:/test/file.jar"));
		});
		assertThrows(URISyntaxException.class, () -> {
			UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar"));
		});
	}
	
	@Test
	public void getJarFileUrl_Jar() throws URISyntaxException {
		assertEquals(new URI("file:/test/file.jar"),
				UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar!/")) );
		assertEquals(new URI("file:/test/file.jar"),
				UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar!/p1/file.txt")) );
	}
	@Test
	public void getJarFileUrl_NestedJarNested() throws URISyntaxException {
		assertEquals(new URI("jar:file:/test/file.jar!/p1/nested.jar"),
				UriUtils.getJarFileUrl(new URI("jar:nested:/test/file.jar/!p1/nested.jar!/")) );
		assertEquals(new URI("jar:file:/test/file.jar!/p1/nested.jar"),
				UriUtils.getJarFileUrl(new URI("jar:nested:/test/file.jar/!p1/nested.jar!/p2/file.txt")) );
	}
	@Test
	public void getJarFileUrl_NestedJarBoot2() throws URISyntaxException {
		assertEquals(new URI("jar:file:/test/file.jar!/p1/nested.jar"),
				UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar!/p1/nested.jar!/")) );
		assertEquals(new URI("jar:file:/test/file.jar!/p1/nested.jar"),
				UriUtils.getJarFileUrl(new URI("jar:file:/test/file.jar!/p1/nested.jar!/p2/file.txt")) );
	}
	
	@Test
	@SuppressWarnings("null")
	public void getJarEntryPath_checkJarUrl() throws URISyntaxException {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.getJarEntryPath((URI)null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.getJarEntryPath(new URI("file:/test/file.jar"));
		});
		assertThrows(URISyntaxException.class, () -> {
			UriUtils.getJarEntryPath(new URI("jar:file:/test/file.jar"));
		});
	}
	
	@Test
	public void getJarEntryPath_jar() throws URISyntaxException {
		assertEquals("",
				UriUtils.getJarEntryPath(new URI("jar:file:/test/file.jar!/")) );
	}
	@Test
	public void getJarEntryPath_jar_jarNested() throws URISyntaxException {
		assertEquals("",
				UriUtils.getJarEntryPath(new URI("jar:nested:/test/file.jar/!p1/nested.jar!/")) );
		assertEquals("p2/file.txt",
				UriUtils.getJarEntryPath(new URI("jar:nested:/test/file.jar/!p1/nested.jar!/p2/file.txt")) );
	}
	@Test
	public void getJarEntryPath_jar_jarBoot2() throws URISyntaxException {
		assertEquals("",
				UriUtils.getJarEntryPath(new URI("jar:file:/test/file.jar!/p1/nested.jar!/")) );
		assertEquals("p2/file.txt",
				UriUtils.getJarEntryPath(new URI("jar:file:/test/file.jar!/p1/nested.jar!/p2/file.txt")) );
	}
	
	@Test
	@SuppressWarnings("null")
	public void toJarUrl_checkJarUrl() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.toJarUrl((String)null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.toJarUrl("jar:nested:/test/file.jar/!p1/nested.jar!/");
		});
	}
	
	@Test
	public void toJarUrl() throws URISyntaxException {
		assertEquals(new URI("jar", "file:/test/file.jar!/", null),
				UriUtils.toJarUrl("file:/test/file.jar") );
		
		assertEquals(new URI("jar", "nested:/test/file.jar/!p1/nested.jar!/", null),
				UriUtils.toJarUrl("jar:file:/test/file.jar!/p1/nested.jar") );
	}
	
	@Test
	@SuppressWarnings("null")
	public void toJarUrlString_checkJarUrl() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.toJarUrlString((String)null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			UriUtils.toJarUrlString("jar:nested:/test/file.jar/!p1/nested.jar!/");
		});
	}
	
	@Test
	public void toJarUrlString() throws URISyntaxException {
		assertEquals("jar:file:/test/file.jar!/",
				UriUtils.toJarUrlString("file:/test/file.jar") );
		
		assertEquals("jar:nested:/test/file.jar/!p1/nested.jar!/",
				UriUtils.toJarUrlString("jar:file:/test/file.jar!/p1/nested.jar") );
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void encodePathSegment_checkArgs() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.encodePathSegment((String)null);
		});
	}
	
	@Test
	public void encodePathSegment() {
		assertEquals("path%3C%3Esegment",
				UriUtils.encodePathSegment("path<>segment") );
		
		assertEquals("%3C%3E",
				UriUtils.encodePathSegment("<>") );
		assertEquals("path%3C%3E",
				UriUtils.encodePathSegment("path<>") );
		assertEquals("%3C%3Esegment",
				UriUtils.encodePathSegment("<>segment") );
		assertEquals("pathsegment",
				UriUtils.encodePathSegment("pathsegment") );
		
		assertEquals("%25",
				UriUtils.encodePathSegment("%") );
		assertEquals("%2F",
				UriUtils.encodePathSegment("/") );
		assertEquals("%3F",
				UriUtils.encodePathSegment("?") );
		assertEquals("%23",
				UriUtils.encodePathSegment("#") );
		assertEquals("~",
				UriUtils.encodePathSegment("~") );
		assertEquals("%C3%A1%C4%81",
				UriUtils.encodePathSegment("áā") );
		assertEquals("%F0%90%86%90",
				UriUtils.encodePathSegment("\uD800\uDD90") );
	}
	
	@Test
	@SuppressWarnings("null")
	public void encodeFragment_checkArgs() {
		assertThrows(NullPointerException.class, () -> {
			UriUtils.encodeFragment((String)null);
		});
	}
	
	@Test
	public void encodeFragment() {
		assertEquals("fragment%3C%3E123",
				UriUtils.encodeFragment("fragment<>123") );
		
		assertEquals("%3C%3E",
				UriUtils.encodeFragment("<>") );
		assertEquals("fragment%3C%3E",
				UriUtils.encodeFragment("fragment<>") );
		assertEquals("%3C%3E123",
				UriUtils.encodeFragment("<>123") );
		assertEquals("fragment123",
				UriUtils.encodeFragment("fragment123") );
		
		assertEquals("%25",
				UriUtils.encodeFragment("%") );
		assertEquals("/",
				UriUtils.encodeFragment("/") );
		assertEquals("?",
				UriUtils.encodeFragment("?") );
		assertEquals("%23",
				UriUtils.encodeFragment("#") );
		assertEquals("~",
				UriUtils.encodeFragment("~") );
		assertEquals("%C3%A1%C4%81",
				UriUtils.encodeFragment("áā") );
		assertEquals("%F0%90%86%90",
				UriUtils.encodeFragment("\uD800\uDD90") );
	}
	
}
