/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeFalse;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class FileUtilsTest {
	
	
	public FileUtilsTest() {
	}
	
	
	@Test
	public void getUserWorkingDirectory() {
		assertEquals(FileUtils.getUserWorkingDirectory(),
				new File(System.getProperty("user.dir")).toPath().toAbsolutePath() );
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void requireParent_requireNotNull() {
		assertThrows(NullPointerException.class, () -> {
			FileUtils.requireParent(null);
		});
	}
	
	@Test
	public void requireParent() {
		assertEquals(FileUtils.requireParent(Path.of("dir/test")),
				Path.of("dir") );
		assertThrows(IllegalArgumentException.class, () -> {
			FileUtils.requireParent(Path.of("dir"));
		});
	}
	
	@Test
	@SuppressWarnings("null")
	public void requireFileName_requireNotNull() {
		assertThrows(NullPointerException.class, () -> {
			FileUtils.requireFileName(null);
		});
	}
	
	@Test
	public void requireFileName() {
		assertEquals(FileUtils.requireFileName(Path.of("dir/test")),
				Path.of("test") );
		assertThrows(IllegalArgumentException.class, () -> {
			FileUtils.requireFileName(Path.of("/"));
		});
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void deleteRecursively_requireNotNull() throws IOException {
		assertThrows(NullPointerException.class, () -> {
			FileUtils.deleteRecursively(null);
		});
	}
	
	@Test
	public void deleteRecursively() throws IOException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= Files.createDirectory(testDir.resolve("dir"));
		
		Files.writeString(dir.resolve("file"), "Hello");
		Files.writeString(dir.resolve("file_2"), "Hello");
		final Path subDir= Files.createDirectory(dir.resolve("sub"));
		Files.writeString(subDir.resolve("file"), "Hello");
		
		FileUtils.deleteRecursively(dir);
		
		assertTrue(Files.notExists(dir));
	}
	
	@Test
	public void deleteRecursively_notExisting() throws IOException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= testDir.resolve("dir");
		
		FileUtils.deleteRecursively(dir);
		
		assertTrue(Files.notExists(dir));
	}
	
	@Test
	public void deleteRecursively_SymbolicLinkFile() throws IOException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= Files.createDirectory(testDir.resolve("dir"));
		
		final Path targetFile= Files.writeString(testDir.resolve("file"), "Hello");
		
		Files.writeString(dir.resolve("file"), "Hello");
		try {
			Files.createSymbolicLink(dir.resolve("file-link"), targetFile);
		}
		catch (final UnsupportedOperationException | FileSystemException e) {
			assumeFalse(true, e.toString());
		}
		
		FileUtils.deleteRecursively(dir);
		
		assertTrue(Files.notExists(dir));
		assertTrue(Files.exists(targetFile));
	}
	
	@Test
	public void deleteRecursively_SymbolicLinkDir() throws IOException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= Files.createDirectory(testDir.resolve("dir"));
		
		final Path targetDir= Files.createDirectory(testDir.resolve("file"));
		final Path targetDirFile= Files.writeString(targetDir.resolve("file"), "Hello");
		
		Files.writeString(dir.resolve("file"), "Hello");
		try {
			Files.createSymbolicLink(dir.resolve("dir-link"), targetDir);
		}
		catch (final UnsupportedOperationException | FileSystemException e) {
			assumeFalse(true, e.toString());
		}
		
		FileUtils.deleteRecursively(dir);
		
		assertTrue(Files.notExists(dir));
		assertTrue(Files.exists(targetDir));
		assertTrue(Files.exists(targetDirFile));
	}
	
	@Test
	public void deleteRecursively_HardLinkFile() throws IOException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= Files.createDirectory(testDir.resolve("dir"));
		
		final Path targetFile= Files.writeString(testDir.resolve("file"), "Hello");
		
		Files.writeString(dir.resolve("file"), "Hello");
		try {
			Files.createLink(dir.resolve("file-link"), targetFile);
		}
		catch (final UnsupportedOperationException | FileSystemException e) {
			assumeFalse(true, e.toString());
		}
		
		FileUtils.deleteRecursively(dir);
		
		assertTrue(Files.notExists(dir));
		assertTrue(Files.exists(targetFile));
	}
	
	@Test
	@EnabledOnOs(OS.WINDOWS)
	public void deleteRecursively_WinJunctionDir() throws IOException, InterruptedException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= Files.createDirectory(testDir.resolve("dir"));
		
		final Path targetDir= Files.createDirectory(testDir.resolve("file"));
		final Path targetDirFile= Files.writeString(targetDir.resolve("file"), "Hello");
		
		Files.writeString(dir.resolve("file"), "Hello");
		try {
			final Path linkDir= dir.resolve("dir-link");
			final Process t= new ProcessBuilder("cmd", "/C", "mklink", "/J", linkDir.toString(), targetDir.toString())
					.directory(testDir.toFile())
					.redirectOutput(Redirect.DISCARD)
					.redirectErrorStream(true)
					.start();
			t.waitFor(5, TimeUnit.SECONDS);
			assertTrue(Files.isDirectory(linkDir));
		}
		catch (final UnsupportedOperationException | FileSystemException e) {
			assumeFalse(true, e.toString());
		}
		
		FileUtils.deleteRecursively(dir);
		
		assertTrue(Files.notExists(dir));
		assertTrue(Files.exists(targetDir));
		assertTrue(Files.exists(targetDirFile));
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void cleanDirectory_requireNotNull() throws IOException {
		assertThrows(NullPointerException.class, () -> {
			FileUtils.cleanDirectory(null);
		});
	}
	
	@Test
	public void cleanDirectory_requireDirectory() throws IOException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= testDir.resolve("dir");
		
		assertThrows(NoSuchFileException.class, () -> {
			FileUtils.cleanDirectory(dir);
		});
		
		Files.writeString(dir, "Hello");
		assertThrows(NotDirectoryException.class, () -> {
			FileUtils.cleanDirectory(dir);
		});
	}
	
	@Test
	public void cleanDirectory() throws IOException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= Files.createDirectory(testDir.resolve("dir"));
		
		final Path file1= Files.writeString(dir.resolve("file"), "Hello");
		final Path file2= Files.writeString(dir.resolve("file_2"), "Hello");
		final Path subDir= Files.createDirectory(dir.resolve("sub"));
		Files.writeString(subDir.resolve("file"), "Hello");
		
		FileUtils.cleanDirectory(dir);
		
		assertTrue(Files.exists(dir));
		assertTrue(Files.notExists(file1));
		assertTrue(Files.notExists(file2));
		assertTrue(Files.notExists(subDir));
	}
	
	@Test
	public void cleanDirectory_withFilter() throws IOException {
		final Path testDir= Files.createTempDirectory("FileUtilsTest").toAbsolutePath();
		final Path dir= Files.createDirectory(testDir.resolve("dir"));
		
		final Path file1= Files.writeString(dir.resolve("file"), "Hello");
		final Path file2= Files.writeString(dir.resolve("file_2"), "Hello");
		final Path subDir= Files.createDirectory(dir.resolve("sub"));
		Files.writeString(subDir.resolve("file"), "Hello");
		
		FileUtils.cleanDirectory(dir,
				(path) -> !nonNullAssert(path.getFileName()).toString().equals("file_2") );
		
		assertTrue(Files.exists(dir));
		assertTrue(Files.notExists(file1));
		assertTrue(Files.exists(file2));
		assertTrue(Files.notExists(subDir));
	}
	
}
