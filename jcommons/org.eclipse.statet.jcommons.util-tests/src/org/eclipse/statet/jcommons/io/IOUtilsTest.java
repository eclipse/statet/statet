/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.io.IOUtils.UTF_16BE_BOM;
import static org.eclipse.statet.jcommons.io.IOUtils.UTF_16LE_BOM;
import static org.eclipse.statet.jcommons.io.IOUtils.UTF_8_BOM;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.util.ArrayUtils;


@NonNullByDefault
public class IOUtilsTest {
	
	
	private static final String T1_STR= "Abc Ä\n\uD83D\uDE0A";
	private static final String T2_STR= "Abc Ä\n";
	
	private static final String BOM_STR= String.valueOf(IOUtils.BOM_CHAR);
	
	private static final String EXT= "in";
	
	
	@TempDir
	private static Path testDir= nonNullLateInit();
	
	@BeforeAll
	public static void initTestFiles() throws Exception {
		writeTestFiles(testDir);
	}
	
	private static Path getTestFileFolder() throws URISyntaxException {
		return testDir;
//		return FileUtils.getUserWorkingDirectory();
	}
	
	
	public IOUtilsTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void newStreamReader_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			IOUtils.newStreamContentReader(null, null, StandardCharsets.UTF_8);
		});
		assertThrows(NullPointerException.class, () -> {
			IOUtils.newStreamContentReader(new ByteArrayInputStream(new byte[0]), null, null);
		});
	}
	
	@Test
	public void newStreamReader() throws Exception {
		InputStreamReader reader;
		
		reader= IOUtils.newStreamContentReader(new ByteArrayInputStream(
				new byte[] { (byte)0x41 }),
				null, StandardCharsets.UTF_8 );
		assertEquals('A', reader.read());
		assertEquals(-1, reader.read());
		
		reader= IOUtils.newStreamContentReader(new ByteArrayInputStream(
				ArrayUtils.concat(UTF_8_BOM, new byte[] { (byte)0x41 }) ),
				ByteOrderMark.UTF_8, StandardCharsets.UTF_8 );
		assertEquals('A', reader.read());
		assertEquals(-1, reader.read());
		
		reader= IOUtils.newStreamContentReader(new ByteArrayInputStream(
				ArrayUtils.concat(UTF_16BE_BOM, new byte[] { (byte)0x00, (byte)0x41 }) ),
				ByteOrderMark.UTF_16BE, StandardCharsets.UTF_16BE );
		assertEquals('A', reader.read());
		assertEquals(-1, reader.read());
		
		reader= IOUtils.newStreamContentReader(new ByteArrayInputStream(
				ArrayUtils.concat(UTF_16LE_BOM, new byte[] { (byte)0x41, (byte)0x00 }) ),
				ByteOrderMark.UTF_16LE, StandardCharsets.UTF_16LE );
		assertEquals('A', reader.read());
		assertEquals(-1, reader.read());
	}
	
	@Test
	public void newStreamReader_empty() throws Exception {
		InputStreamReader reader;
		
		reader= IOUtils.newStreamContentReader(new ByteArrayInputStream(
				new byte[0] ),
				null, StandardCharsets.UTF_8 );
		assertEquals(-1, reader.read());
		
		reader= IOUtils.newStreamContentReader(new ByteArrayInputStream(
				UTF_8_BOM ),
				ByteOrderMark.UTF_8, StandardCharsets.UTF_8 );
		assertEquals(-1, reader.read());
		
		reader= IOUtils.newStreamContentReader(new ByteArrayInputStream(
				UTF_16BE_BOM ),
				ByteOrderMark.UTF_16BE, StandardCharsets.UTF_16BE );
		assertEquals(-1, reader.read());
		
		reader= IOUtils.newStreamContentReader(new ByteArrayInputStream(
				UTF_16LE_BOM ),
				ByteOrderMark.UTF_16LE, StandardCharsets.UTF_16LE );
		assertEquals(-1, reader.read());
	}
	
	
	@ParameterizedTest
	@ValueSource(strings= { "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void newBufferedReader(final String charsetName) throws Exception {
		final Path folder= getTestFileFolder();
		
		final Charset charset= Charset.forName(charsetName);
		testBufferedReader(T1_STR,
				folder.resolve("t1-" + charsetName + "-." + EXT), charset );
		testBufferedReader(T1_STR,
				folder.resolve("t1-" + charsetName + "-bom." + EXT), charset );
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void newBufferedReader_doubleBOM(final String charsetName) throws Exception {
		final Path folder= getTestFileFolder();
		
		final Charset charset= Charset.forName(charsetName);
		testBufferedReader(BOM_STR + T1_STR,
				folder.resolve("t1-" + charsetName + "-bom+bom." + EXT), charset );
	}
	
	private void testBufferedReader(final String expectedContent,
			final Path file, final Charset charset) throws Exception {
		final BufferedReader reader= IOUtils.newBufferedStreamContentReader(
				Files.newInputStream(file), charset, CodingErrorAction.REPORT );
		for (int i= 0; i < expectedContent.length(); i++) {
			final int actual= reader.read();
			if (expectedContent.charAt(i) != actual) {
				assertEquals(String.format("0x%1$04X", (int)expectedContent.charAt(i)), String.format("0x%1$04X", actual), String.format("[%1$s]", i));
			}
		}
		assertEquals(-1, reader.read());
	}
	
	
	@ParameterizedTest
	@ValueSource(strings= { "ISO-8859-1", "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void readFileContentString_InputStream(final String charsetName) throws Exception {
		final Path folder= getTestFileFolder();
		
		final Charset charset= Charset.forName(charsetName);
		if (charset.equals(StandardCharsets.ISO_8859_1)) {
			testReadInputStreamContentString(T2_STR,
					folder.resolve("t2-" + charsetName + "-." + EXT), charset );
		}
		else {
			testReadInputStreamContentString(T1_STR,
					folder.resolve("t1-" + charsetName + "-." + EXT), charset );
			testReadInputStreamContentString(T1_STR,
					folder.resolve("t1-" + charsetName + "-bom." + EXT), charset );
		}
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void readContentString_InputStream_doubleBOM(final String charsetName) throws Exception {
		final Path folder= getTestFileFolder();
		
		final Charset charset= Charset.forName(charsetName);
		testReadInputStreamContentString(BOM_STR + T1_STR,
				folder.resolve("t1-" + charsetName + "-bom+bom." + EXT), charset );
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "ISO-8859-1", "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void readContentString_InputStream_empty(final String charsetName) throws Exception {
		final Charset charset= Charset.forName(charsetName);
		final String s= IOUtils.readContentString(new ByteArrayInputStream(new byte[0]), charset);
		assertTrue(s.isEmpty());
	}
	
	private void testReadInputStreamContentString(final String expectedContent,
			final Path file, final Charset charset) throws Exception {
		final byte[] bytes= Files.readAllBytes(file);
		
		final String s= IOUtils.readContentString(new ByteArrayInputStream(bytes), charset);
		for (int i= 0; i < expectedContent.length(); i++) {
			final int actual= (i < s.length()) ? s.charAt(i) : -1;
			if (expectedContent.charAt(i) != actual) {
				assertEquals(String.format("0x%1$04X", (int)expectedContent.charAt(i)), String.format("0x%1$04X", actual), String.format("[%1$s]", i));
			}
		}
		assertEquals(expectedContent.length(), s.length());
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "ISO-8859-1", "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void readFileContentString_Path(final String charsetName) throws Exception {
		final Path folder= getTestFileFolder();
		
		final Charset charset= Charset.forName(charsetName);
		if (charset.equals(StandardCharsets.ISO_8859_1)) {
			testReadFileContentString(T2_STR,
					folder.resolve("t2-" + charsetName + "-." + EXT), charset );
		}
		else {
			testReadFileContentString(T1_STR,
					folder.resolve("t1-" + charsetName + "-." + EXT), charset );
			testReadFileContentString(T1_STR,
					folder.resolve("t1-" + charsetName + "-bom." + EXT), charset );
		}
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void readContentString_Path_doubleBOM(final String charsetName) throws Exception {
		final Path folder= getTestFileFolder();
		
		final Charset charset= Charset.forName(charsetName);
		testReadFileContentString(BOM_STR + T1_STR,
				folder.resolve("t1-" + charsetName + "-bom+bom." + EXT), charset );
	}
	
	@ParameterizedTest
	@ValueSource(strings= { "ISO-8859-1", "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void readContentString_Path_empty(final String charsetName) throws Exception {
		final Path folder= getTestFileFolder();
		
		final Charset charset= Charset.forName(charsetName);
		final String s= IOUtils.readContentString(folder.resolve("empty." + EXT), charset);
		assertTrue(s.isEmpty());
	}
	
	private void testReadFileContentString(final String expectedContent,
			final Path file, final Charset charset) throws Exception {
		final String s= IOUtils.readContentString(file, charset);
		for (int i= 0; i < expectedContent.length(); i++) {
			final int actual= (i < s.length()) ? s.charAt(i) : -1;
			if (expectedContent.charAt(i) != actual) {
				assertEquals(String.format("0x%1$04X", (int)expectedContent.charAt(i)), String.format("0x%1$04X", actual), String.format("[%1$s]", i));
			}
		}
		assertEquals(expectedContent.length(), s.length());
	}
	
	
	@ParameterizedTest
	@ValueSource(strings= { "ISO-8859-1", "UTF-8", "UTF-16BE", "UTF-16LE" })
	public void writeBOM_Charset(final String charsetName) throws Exception {
		final Charset charset= Charset.forName(charsetName);
		final var out= new ByteArrayOutputStream();
		IOUtils.writeBOM(out, charset);
		if (charset.equals(StandardCharsets.ISO_8859_1)) {
			assertEquals(0, out.size());
		}
		else {
			assertEquals(BOM_STR, out.toString(charset));
		}
	}
	
	
	private static void writeTestFiles(final Path folder) throws IOException {
		writeTestFiles("t1", T1_STR, folder);
		Files.write(folder.resolve("t2-ISO-8859-1-." + EXT), T2_STR.getBytes(StandardCharsets.ISO_8859_1));
		Files.write(folder.resolve("empty." + EXT), new byte[0]);
		
		System.out.println("[INFO] Test files written to: " + folder);
	}
	
	private static void writeTestFiles(final String id, final String text, final Path folder)
			throws IOException {
		byte[] content;
		
		content= text.getBytes(StandardCharsets.UTF_8);
		Files.write(folder.resolve(id + "-UTF-8-." + EXT), content);
		Files.write(folder.resolve(id + "-UTF-8-bom." + EXT),
				ArrayUtils.concat(UTF_8_BOM, content) );
		Files.write(folder.resolve(id + "-UTF-8-bom+bom." + EXT),
				ArrayUtils.concat(UTF_8_BOM, UTF_8_BOM, content) );
		
		content= text.getBytes(StandardCharsets.UTF_16BE);
		Files.write(folder.resolve(id + "-UTF-16BE-." + EXT), content);
		Files.write(folder.resolve(id + "-UTF-16BE-bom." + EXT),
				ArrayUtils.concat(UTF_16BE_BOM, content) );
		Files.write(folder.resolve(id + "-UTF-16BE-bom+bom." + EXT),
				ArrayUtils.concat(UTF_16BE_BOM, UTF_16BE_BOM, content) );
		
		content= text.getBytes(StandardCharsets.UTF_16LE);
		Files.write(folder.resolve(id + "-UTF-16LE-." + EXT), content);
		Files.write(folder.resolve(id + "-UTF-16LE-bom." + EXT),
				ArrayUtils.concat(UTF_16LE_BOM, content));
		Files.write(folder.resolve(id + "-UTF-16LE-bom+bom." + EXT),
				ArrayUtils.concat(UTF_16LE_BOM, UTF_16LE_BOM, content));
	}
	
	public static void main(final String[] args) throws Exception {
		writeTestFiles(FileUtils.getUserWorkingDirectory());
	}
	
}
