/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.eclipse.statet.jcommons.io.ByteOrderMark.UTF_16BE;
import static org.eclipse.statet.jcommons.io.ByteOrderMark.UTF_16LE;
import static org.eclipse.statet.jcommons.io.ByteOrderMark.UTF_8;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ByteMarkOrderTest {
	
	
	public ByteMarkOrderTest() {
	}
	
	
	@Test
	public void UTF_8() {
		final ByteOrderMark bom= UTF_8;
		
		assertArrayEquals(new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF }, bom.getBytes());
		assertEquals(3, bom.getLength());
		assertEquals("UTF-8", bom.getCharset().name());
	}
	
	@Test
	public void UTF_16BE() {
		final ByteOrderMark bom= UTF_16BE;
		
		assertArrayEquals(new byte[] { (byte)0xFE, (byte)0xFF }, bom.getBytes());
		assertEquals(2, bom.getLength());
		assertEquals("UTF-16BE", bom.getCharset().name());
	}
	
	@Test
	public void UTF_16LE() {
		final ByteOrderMark bom= UTF_16LE;
		
		assertArrayEquals(new byte[] { (byte)0xFF, (byte)0xFE }, bom.getBytes());
		assertEquals(2, bom.getLength());
		assertEquals("UTF-16LE", bom.getCharset().name());
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void of_Bytes_argCheck() throws IOException {
		assertThrows(NullPointerException.class, () -> {
			ByteOrderMark.of((byte[])null);
		});
	}
	
	@Test
	public void of_Bytes() throws IOException {
		assertNull(ByteOrderMark.of(
				new byte[] {} ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0x00 } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0x41} ));
		
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xEF } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xEF, (byte)0xB0 } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xEF, (byte)0xBB } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xB0 } ));
		assertSame(UTF_8, ByteOrderMark.of(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF, (byte)0x41 } ));
		
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xFE } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xFE, (byte)0xF0 } ));
		assertSame(UTF_16BE, ByteOrderMark.of(
				new byte[] { (byte)0xFE, (byte)0xFF } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xFE, (byte)0xFF, (byte)0x00, (byte)0x41 } ));
		
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xFF } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xFF, (byte)0xF0 } ));
		assertSame(UTF_16LE, ByteOrderMark.of(
				new byte[] { (byte)0xFF, (byte)0xFE } ));
		assertNull(ByteOrderMark.of(
				new byte[] { (byte)0xFF, (byte)0xFE, (byte)0x41, (byte)0x00 } ));
	}
	
	@Test
	public void ofStart_Bytes() throws IOException {
		assertNull(ByteOrderMark.ofStart(
				new byte[] {} ));
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0x00 } ));
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0x41} ));
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0x41, (byte)0xEF, (byte)0xBB, (byte)0xBF } ));
		
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0xEF } ));
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0xEF, (byte)0xB0 } ));
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0xEF, (byte)0xBB } ));
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xB0 } ));
		assertSame(UTF_8, ByteOrderMark.ofStart(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF } ));
		assertSame(UTF_8, ByteOrderMark.ofStart(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF, (byte)0x41 } ));
		
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0xFE } ));
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0xFE, (byte)0xF0 } ));
		assertSame(UTF_16BE, ByteOrderMark.ofStart(
				new byte[] { (byte)0xFE, (byte)0xFF } ));
		assertSame(UTF_16BE, ByteOrderMark.ofStart(
				new byte[] { (byte)0xFE, (byte)0xFF, (byte)0x00, (byte)0x41 } ));
		
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0xFF } ));
		assertNull(ByteOrderMark.ofStart(
				new byte[] { (byte)0xFF, (byte)0xF0 } ));
		assertSame(UTF_16LE, ByteOrderMark.ofStart(
				new byte[] { (byte)0xFF, (byte)0xFE } ));
		assertSame(UTF_16LE, ByteOrderMark.ofStart(
				new byte[] { (byte)0xFF, (byte)0xFE, (byte)0x41, (byte)0x00 } ));
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void read_InputStream_argCheck() throws IOException {
		assertThrows(NullPointerException.class, () -> {
			ByteOrderMark.read(null);
		});
	}
	
	@Test
	public void read_InputStream() throws IOException {
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] {} )));
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0x00 } )));
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0x41} )));
		
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xEF } )));
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xEF, (byte)0xB0 } )));
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xEF, (byte)0xBB } )));
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xB0 } )));
		assertSame(UTF_8, ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF } )));
		assertSame(UTF_8, ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF, (byte)0x41 } )));
		
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFE } )));
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFE, (byte)0xF0 } )));
		assertSame(UTF_16BE, ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFE, (byte)0xFF } )));
		assertSame(UTF_16BE, ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFE, (byte)0xFF, (byte)00 } )));
		assertSame(UTF_16BE, ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFE, (byte)0xFF, (byte)0x00, (byte)0x41 } )));
		
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFF } )));
		assertNull(ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFF, (byte)0xF0 } )));
		assertSame(UTF_16LE, ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFF, (byte)0xFE } )));
		assertSame(UTF_16LE, ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFF, (byte)0xFE, (byte)00 } )));
		assertSame(UTF_16LE, ByteOrderMark.read(new ByteArrayInputStream(
				new byte[] { (byte)0xFF, (byte)0xFE, (byte)0x41, (byte)0x00 } )));
	}
	
}
