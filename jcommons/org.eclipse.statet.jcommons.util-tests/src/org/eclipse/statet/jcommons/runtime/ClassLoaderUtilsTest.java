/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ClassLoaderUtilsTest {
	
	
	public ClassLoaderUtilsTest() {
	}
	
	
	@Test
	public void toJClassPathEntryString_Folder() {
		assertEquals("/test-env/bundle-locations/org.eclipse.statet.rhelp.server".replace('/', File.separatorChar),
				ClassLoaderUtils.toJClassPathEntryString("file:/test-env/bundle-locations/org.eclipse.statet.rhelp.server/") );
	}
	
	@Test
	public void toJClassPathEntryString_Jar() {
		assertEquals("/test-env/bundle-locations/org.eclipse.statet.rhelp.server.jar".replace('/', File.separatorChar),
				ClassLoaderUtils.toJClassPathEntryString("jar:file:/test-env/bundle-locations/org.eclipse.statet.rhelp.server.jar!/") );
	}
	
}
