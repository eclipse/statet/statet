/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime.bundle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.bundle.BundleEntryProvider.DevBinPathEntryProvider;
import org.eclipse.statet.jcommons.runtime.bundle.BundleEntryProvider.JarFilePathEntryProvider;


@NonNullByDefault
public class BundleEntryProviderTest {
	
	
	public BundleEntryProviderTest() {
	}
	
	
	@Test
	public void detectEntryProvider_DevFolder() throws Exception { // in IDE
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"file:/../org.eclipse.statet-rj/core/org.eclipse.statet.rj.server/target/classes/",
				null );
		
		assertEquals(DevBinPathEntryProvider.class, provider.getClass());
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntry_DevFolder() throws Exception { // in IDE
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntry entry= Bundles.detectEntry(
				"file:/" + folder.replace('\\', '/') + "/bundle-locations/DevFolder/org.eclipse.statet-rj/core/org.eclipse.statet.rj.server/target/classes/" );
		
		assertEquals("org.eclipse.statet.rj.server", entry.getBundleId());
	}
	
	
	@Test
	public void detectEntryProvider_SimpleServerJar() throws Exception { // e.g. console server
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:file:/../rserver/org.eclipse.statet.rj.server.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server.jar") );
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntry_SimpleServerJar() throws Exception { // in IDE
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntry entry= Bundles.detectEntry(
				"jar:file:/" + folder.replace('\\', '/') + "/bundle-locations/SimpleServerJar/rserver/org.eclipse.statet.rj.server.jar!/" );
		
		assertEquals("org.eclipse.statet.rj.server", entry.getBundleId());
	}
	
	
	@Test
	public void detectEntryProvider_TargetJar_Snapshot() throws Exception {
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:file:/../org.eclipse.statet-rj/core/org.eclipse.statet.rj.server/target/org.eclipse.statet.rj.server-4.0.0-SNAPSHOT.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0-SNAPSHOT.jar") );
		assertNull(
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0-SNAPSHOT-sources.jar") );
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntry_TargetJar_Snapshot() throws Exception { // in IDE
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntry entry= Bundles.detectEntry(
				"jar:file:/" + folder.replace('\\', '/') + "/bundle-locations/TargetJar_Snapshot/org.eclipse.statet-rj/core/org.eclipse.statet.rj.server/target/org.eclipse.statet.rj.server-4.0.0-SNAPSHOT.jar!/" );
		
		assertEquals("org.eclipse.statet.rj.server", entry.getBundleId());
		entry.getJClassPathString();
	}
	
	@Test
	public void detectEntryProvider_TargetJar_Release() throws Exception {
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:file:/../org.eclipse.statet-rj/core/org.eclipse.statet.rj.server/target/org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar") );
		assertNull(
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0.201906060800-r-sources.jar") );
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntry_TargetJar_Release() throws Exception { // in IDE
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntry entry= Bundles.detectEntry(
				"jar:file:/" + folder.replace('\\', '/') + "/bundle-locations/TargetJar_Release/org.eclipse.statet-rj/core/org.eclipse.statet.rj.server/target/org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar!/" );
		
		assertEquals("org.eclipse.statet.rj.server", entry.getBundleId());
	}
	
	@Test
	public void detectEntryProvider_NexusJar_Snapshot() throws Exception {
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:file:/../WEB-INF/lib/org.eclipse.statet.rj.server-4.0.0-20190529.154707-7.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0-20190529.154707-7.jar") );
		assertNull(
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0-20190529.154707-7-sources.jar") );
	}
	
	@Test
	public void detectEntryProvider_NexusJar_Release() throws Exception {
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:file:/../WEB-INF/lib/org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar") );
		assertNull(
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0.201906060800-r-sources.jar") );
	}
	
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntryProvider_NestedJarNested_Snapshot() throws Exception { // e.g. R help server
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:nested:/" + folder.replace('\\', '/') + "/bundle-locations/org.eclipse.statet.rhelp.server-4.0.0-SNAPSHOT.jar/!BOOT-INF/lib/org.eclipse.statet.rj.server-4.0.0-SNAPSHOT.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0-SNAPSHOT.jar") );
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntryProvider_NestedJarNested_Nexus() throws Exception { // e.g. R help server
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:nested:/" + folder.replace('\\', '/') + "/bundle-locations/org.eclipse.statet.rhelp.server-nexus.jar/!BOOT-INF/lib/org.eclipse.statet.rj.server-4.0.0-20190529.154707-7.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0-20190529.154707-7.jar") );
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntryProvider_NestedJarNested_Release() throws Exception { // e.g. R help server
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:nested:/" + folder.replace('\\', '/') + "/bundle-locations/org.eclipse.statet.rhelp.server.jar/!BOOT-INF/lib/org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar") );
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntryProvider_NestedJarBoot2_Snapshot() throws Exception { // e.g. R help server
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:file:/" + folder.replace('\\', '/') + "/bundle-locations/org.eclipse.statet.rhelp.server-4.0.0-SNAPSHOT.jar!/BOOT-INF/lib/org.eclipse.statet.rj.server-4.0.0-SNAPSHOT.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0-SNAPSHOT.jar") );
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntryProvider_NestedJarBoot2_Nexus() throws Exception { // e.g. R help server
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:file:/" + folder.replace('\\', '/') + "/bundle-locations/org.eclipse.statet.rhelp.server-nexus.jar!/BOOT-INF/lib/org.eclipse.statet.rj.server-4.0.0-20190529.154707-7.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0-20190529.154707-7.jar") );
	}
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void detectEntryProvider_NestedJarBoot2_Release() throws Exception { // e.g. R help server
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final BundleEntryProvider provider= Bundles.detectEntryProvider(
				"jar:file:/" + folder.replace('\\', '/') + "/bundle-locations/org.eclipse.statet.rhelp.server.jar!/BOOT-INF/lib/org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar!/",
				null );
		
		assertEquals(JarFilePathEntryProvider.class, provider.getClass());
		
		final JarFilePathEntryProvider jarProvider= (JarFilePathEntryProvider)provider;
		assertEquals("org.eclipse.statet.rj.server",
				jarProvider.getBundleId("org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar") );
	}
	
}
