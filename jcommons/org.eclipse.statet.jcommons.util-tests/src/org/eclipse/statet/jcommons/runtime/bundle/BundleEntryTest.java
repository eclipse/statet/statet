/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime.bundle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class BundleEntryTest {
	
	
	public BundleEntryTest() {
	}
	
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void Entry_DevFolder() throws Exception { // in IDE
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final Path path= Path.of(
				new URI("file:/" + folder.replace('\\', '/') + "/bundle-locations/DevFolder/org.eclipse.statet-rj/core/org.eclipse.statet.rj.server") );
		final BundleEntry entry= new BundleEntry.Dir("org.eclipse.statet.rj.server",
				path, path.resolve("target/classes") );
		
		assertServerBundle(path, path.resolve("target/classes"), entry);
	}
	
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void Entry_SimpleServerJar() throws Exception { // in IDE
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final Path path= Path.of(
				new URI("file:/" + folder.replace('\\', '/') + "/bundle-locations/SimpleServerJar/rserver/org.eclipse.statet.rj.server.jar") );
		final BundleEntry entry= new BundleEntry.Jar("org.eclipse.statet.rj.server", path);
		
		assertServerBundle(path, path, entry);
	}
	
	
	@Test
	@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
	public void EntryProvider_NestedJar() throws Exception { // e.g. R help server
		final String folder= nonNullAssert(System.getenv("STATET_TEST_FILES"));
		
		final Path path= Bundles.getPath(
				new URI("jar:file:/" + folder.replace('\\', '/') + "/bundle-locations/org.eclipse.statet.rhelp.server.jar!/BOOT-INF/lib/org.eclipse.statet.rj.server-4.0.0.201906060800-r.jar") );
		final BundleEntry entry= new BundleEntry.Jar("org.eclipse.statet.rj.server", path);
		
		assertServerBundle(path, path, entry);
	}
	
	
	private void assertServerBundle(final Path expectedPath, final Path expectedjClassPath,
			final BundleEntry entry) throws IOException {
		assertEquals("org.eclipse.statet.rj.server", entry.getBundleId());
		assertEquals(expectedPath, entry.getPath());
		assertEquals(expectedjClassPath, entry.getJClassPath());
		final Path policyPath= entry.getResourcePath("localhost.policy");
		assertNotNull(policyPath);
		assertTrue(Files.isReadable(policyPath));
		assertEquals(149, Files.readAllBytes(policyPath).length);
	}
	
}
