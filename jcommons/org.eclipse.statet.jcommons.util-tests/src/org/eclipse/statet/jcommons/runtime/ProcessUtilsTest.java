/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ProcessUtilsTest {
	
	
	public ProcessUtilsTest() {
	}
	
	
	@Test
	public void generateCommandLine() throws URISyntaxException {
		assertEquals("", ProcessUtils.generateCommandLine(
				ImCollections.emptyList() ));
		
		assertEquals("command.exe", ProcessUtils.generateCommandLine(
				ImCollections.newList("command.exe") ));
		
		assertEquals("command.exe arg1", ProcessUtils.generateCommandLine(
				ImCollections.newList("command.exe", "arg1") ));
		
		assertEquals("command.exe arg1 arg2", ProcessUtils.generateCommandLine(
				ImCollections.newList("command.exe", "arg1", "arg2") ));
	}
	
	@Test
	public void generateCommandLine_withSpecialChars() throws URISyntaxException {
		assertEquals("command.exe \"arg with space\"", ProcessUtils.generateCommandLine(
				ImCollections.newList("command.exe", "arg with space") ));
		
		assertEquals("command.exe arg-with-\\\"", ProcessUtils.generateCommandLine(
				ImCollections.newList("command.exe", "arg-with-\"") ));
		
		assertEquals("command.exe \"arg with space and \\\"\"", ProcessUtils.generateCommandLine(
				ImCollections.newList("command.exe", "arg with space and \"") ));
	}
	
}
