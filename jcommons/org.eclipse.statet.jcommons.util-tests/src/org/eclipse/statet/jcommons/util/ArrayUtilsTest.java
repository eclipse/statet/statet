/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.util;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ArrayUtilsTest {
	
	
	private static final byte[] BYTE_ARRAY_EMPTY= new byte[0];
	private static final byte[] BYTE_ARRAY_1= new byte[200];
	private static final int[] INT_ARRAY_EMPTY= new int[0];
	private static final int[] INT_ARRAY_1= new int[200];
	static {
		for (int i= 0, j= 3; i < 100; i++, j++) {
			BYTE_ARRAY_1[i]= (byte)j;
			INT_ARRAY_1[i]= j;
		}
		for (int i= 100, j= 4; i < 200; i++, j++) {
			BYTE_ARRAY_1[i]= (byte)j;
			INT_ARRAY_1[i]= j;
		}
	}
	
	
	public ArrayUtilsTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void indexOf_byteArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.indexOf((byte[])null, new byte[] {});
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.indexOf(BYTE_ARRAY_1, (byte[])null);
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.indexOf((byte[])null, (byte[])null);
		});
	}
	
	@Test
	public void indexOf_byteArray() {
		assertEquals(0, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] {}));
		
		assertEquals(0, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)3 }));
		assertEquals(2, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)5 }));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)0 }));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)104 }));
		
		assertEquals(0, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)3, (byte)4 }));
		assertEquals(2, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)5, (byte)6, (byte)7, (byte)8 }));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)6, (byte)6, (byte)7, (byte)8 }));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)5, (byte)6, (byte)7, (byte)9 }));
		assertEquals(98, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)101, (byte)102 }));
		assertEquals(197, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)101, (byte)102, (byte)103 }));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_1, new byte[] { (byte)101, (byte)102, (byte)103, (byte)104 }));
		
		assertEquals(0, ArrayUtils.indexOf(BYTE_ARRAY_1, BYTE_ARRAY_1));
		assertEquals(0, ArrayUtils.indexOf(BYTE_ARRAY_1, Arrays.copyOf(BYTE_ARRAY_1, BYTE_ARRAY_1.length)));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_1, Arrays.copyOf(BYTE_ARRAY_1, BYTE_ARRAY_1.length + 1)));
		
		assertEquals(0, ArrayUtils.indexOf(BYTE_ARRAY_EMPTY, new byte[] {}));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_EMPTY, new byte[] { (byte)0 }));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_EMPTY, new byte[] { (byte)3 }));
	}
	
	@Test
	@SuppressWarnings("null")
	public void indexOf_byte_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.indexOf((byte[])null, (byte)0);
		});
	}
	
	@Test
	public void indexOf_byte() {
		assertEquals(0, ArrayUtils.indexOf(BYTE_ARRAY_1, (byte)3));
		assertEquals(2, ArrayUtils.indexOf(BYTE_ARRAY_1, (byte)5));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_1, (byte)0));
		assertEquals(199, ArrayUtils.indexOf(BYTE_ARRAY_1, (byte)103));
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_1, (byte)104));
		
		assertEquals(-1, ArrayUtils.indexOf(BYTE_ARRAY_EMPTY, (byte)0));
	}
	
	@Test
	@SuppressWarnings("null")
	public void contains_byteArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.contains((byte[])null, new byte[] {});
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.contains(BYTE_ARRAY_1, (byte[])null);
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.contains((byte[])null, (byte[])null);
		});
	}
	
	@Test
	public void contains_byteArray() {
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] {}));
		
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)3 }));
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)5 }));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)0 }));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)104 }));
		
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)3, (byte)4 }));
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)5, (byte)6, (byte)7, (byte)8 }));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)6, (byte)6, (byte)7, (byte)8 }));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)5, (byte)6, (byte)7, (byte)9 }));
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)101, (byte)102 }));
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)101, (byte)102, (byte)103 }));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_1, new byte[] { (byte)101, (byte)102, (byte)103, (byte)104 }));
		
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, BYTE_ARRAY_1));
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, Arrays.copyOf(BYTE_ARRAY_1, BYTE_ARRAY_1.length)));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_1, Arrays.copyOf(BYTE_ARRAY_1, BYTE_ARRAY_1.length + 1)));
		
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_EMPTY, new byte[] {}));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_EMPTY, new byte[] { (byte)0 }));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_EMPTY, new byte[] { (byte)3 }));
	}
	
	@Test
	@SuppressWarnings("null")
	public void contains_byte_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.contains((byte[])null, (byte)0);
		});
	}
	
	@Test
	public void contains_byte() {
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, (byte)3));
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, (byte)5));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_1, (byte)0));
		assertTrue(ArrayUtils.contains(BYTE_ARRAY_1, (byte)103));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_1, (byte)104));
		
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_EMPTY, (byte)0));
		assertFalse(ArrayUtils.contains(BYTE_ARRAY_EMPTY, (byte)3));
	}
	
	@Test
	@SuppressWarnings("null")
	public void startsWith_byteArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.startsWith((byte[])null, new byte[] {});
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.startsWith(BYTE_ARRAY_1, (byte[])null);
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.startsWith((byte[])null, (byte[])null);
		});
	}
	
	@Test
	public void startsWith_byteArray() {
		assertTrue(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] {}));
		
		assertTrue(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] { (byte)3 }));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] { (byte)0 }));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] { (byte)5 }));
		
		assertTrue(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] { (byte)3, (byte)4 }));
		assertTrue(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] { (byte)3, (byte)4, (byte)5, (byte)6 }));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] { (byte)4, (byte)4, (byte)5, (byte)6 }));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] { (byte)3, (byte)4, (byte)5, (byte)7 }));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_1, new byte[] { (byte)101, (byte)102, (byte)103 }));
		
		assertTrue(ArrayUtils.startsWith(BYTE_ARRAY_1, BYTE_ARRAY_1));
		assertTrue(ArrayUtils.startsWith(BYTE_ARRAY_1, Arrays.copyOf(BYTE_ARRAY_1, BYTE_ARRAY_1.length)));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_1, Arrays.copyOf(BYTE_ARRAY_1, BYTE_ARRAY_1.length + 1)));
		
		assertTrue(ArrayUtils.startsWith(BYTE_ARRAY_EMPTY, new byte[] {}));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_EMPTY, new byte[] { (byte)0 }));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_EMPTY, new byte[] { (byte)3 }));
	}
	
	@Test
	@SuppressWarnings("null")
	public void startsWith_byte_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.startsWith((byte[])null, (byte)0);
		});
	}
	
	@Test
	public void startsWith_byte() {
		assertTrue(ArrayUtils.startsWith(BYTE_ARRAY_1, (byte)3));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_1, (byte)0));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_1, (byte)5));
		
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_EMPTY, (byte)0));
		assertFalse(ArrayUtils.startsWith(BYTE_ARRAY_EMPTY, (byte)3));
	}
	
	@Test
	@SuppressWarnings("null")
	public void concat_byteArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(null, new byte[] { (byte)21, (byte)22, (byte)23 });
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(new byte[] { (byte)11, (byte)12 }, null);
		});
	}
	
	@Test
	public void concat_byteArray() {
		assertArrayEquals(new byte[] { (byte)11, (byte)12, (byte)21, (byte)22, (byte)23 },
				ArrayUtils.concat(new byte[] { (byte)11, (byte)12 }, new byte[] { (byte)21, (byte)22, (byte)23 }));
		assertArrayEquals(new byte[] { (byte)21, (byte)22 },
				ArrayUtils.concat(BYTE_ARRAY_EMPTY, new byte[] { (byte)21, (byte)22 }));
		assertArrayEquals(new byte[] { (byte)11, (byte)12 },
				ArrayUtils.concat(new byte[] { (byte)11, (byte)12 }, BYTE_ARRAY_EMPTY));
	}
	
	@Test
	@SuppressWarnings("null")
	public void concat_byteArray3_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(null, new byte[] { (byte)21, (byte)22, (byte)23 }, new byte[] { (byte)31, (byte)32, (byte)33, (byte)34 });
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(new byte[] { (byte)11, (byte)12 }, null, new byte[] { (byte)31, (byte)32, (byte)33, (byte)34 });
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(new byte[] { (byte)11, (byte)12 }, new byte[] { (byte)21, (byte)22, (byte)23 }, null);
		});
	}
	
	@Test
	public void concat_byteArray3() {
		assertArrayEquals(new byte[] { (byte)11, (byte)12, (byte)21, (byte)22, (byte)23, (byte)31, (byte)32, (byte)33, (byte)34 },
				ArrayUtils.concat(new byte[] { (byte)11, (byte)12 }, new byte[] { (byte)21, (byte)22, (byte)23 }, new byte[] { (byte)31, (byte)32, (byte)33, (byte)34 }));
		assertArrayEquals(new byte[] { (byte)21, (byte)22, (byte)31, (byte)32, (byte)33, (byte)34 }, 
				ArrayUtils.concat(BYTE_ARRAY_EMPTY, new byte[] { (byte)21, (byte)22 }, new byte[] { (byte)31, (byte)32, (byte)33, (byte)34 }));
		assertArrayEquals(new byte[] { (byte)11, (byte)12, (byte)21, (byte)22, (byte)23 },
				ArrayUtils.concat(new byte[] { (byte)11, (byte)12 }, new byte[] { (byte)21, (byte)22, (byte)23 }, BYTE_ARRAY_EMPTY));
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void indexOf_intArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.indexOf((int[])null, new int[] {});
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.indexOf(INT_ARRAY_1, (int[])null);
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.indexOf((int[])null, (int[])null);
		});
	}
	
	@Test
	public void indexOf_intArray() {
		assertEquals(0, ArrayUtils.indexOf(INT_ARRAY_1, new int[] {}));
		
		assertEquals(0, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 3 }));
		assertEquals(2, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 5 }));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 0 }));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 104 }));
		
		assertEquals(0, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 3, 4 }));
		assertEquals(2, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 5, 6, 7, 8 }));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 6, 6, 7, 8 }));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 5, 6, 7, 9 }));
		assertEquals(98, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 101, 102 }));
		assertEquals(197, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 101, 102, 103 }));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_1, new int[] { 101, 102, 103, 104 }));
		
		assertEquals(0, ArrayUtils.indexOf(INT_ARRAY_1, INT_ARRAY_1));
		assertEquals(0, ArrayUtils.indexOf(INT_ARRAY_1, Arrays.copyOf(INT_ARRAY_1, INT_ARRAY_1.length)));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_1, Arrays.copyOf(INT_ARRAY_1, INT_ARRAY_1.length + 1)));
		
		assertEquals(0, ArrayUtils.indexOf(INT_ARRAY_EMPTY, new int[] {}));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_EMPTY, new int[] { 0 }));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_EMPTY, new int[] { 3 }));
	}
	
	@Test
	@SuppressWarnings("null")
	public void indexOf_int_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.indexOf((int[])null, 0);
		});
	}
	
	@Test
	public void indexOf_int() {
		assertEquals(0, ArrayUtils.indexOf(INT_ARRAY_1, 3));
		assertEquals(2, ArrayUtils.indexOf(INT_ARRAY_1, 5));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_1, 0));
		assertEquals(199, ArrayUtils.indexOf(INT_ARRAY_1, 103));
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_1, 104));
		
		assertEquals(-1, ArrayUtils.indexOf(INT_ARRAY_EMPTY, 0));
	}
	
	@Test
	@SuppressWarnings("null")
	public void contains_intArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.contains((int[])null, new int[] {});
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.contains(INT_ARRAY_1, (int[])null);
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.contains((int[])null, (int[])null);
		});
	}
	
	@Test
	public void contains_intArray() {
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, new int[] {}));
		
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, new int[] { 3 }));
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, new int[] { 5 }));
		assertFalse(ArrayUtils.contains(INT_ARRAY_1, new int[] { 0 }));
		assertFalse(ArrayUtils.contains(INT_ARRAY_1, new int[] { 104 }));
		
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, new int[] { 3, 4 }));
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, new int[] { 5, 6, 7, 8 }));
		assertFalse(ArrayUtils.contains(INT_ARRAY_1, new int[] { 6, 6, 7, 8 }));
		assertFalse(ArrayUtils.contains(INT_ARRAY_1, new int[] { 5, 6, 7, 9 }));
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, new int[] { 101, 102 }));
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, new int[] { 101, 102, 103 }));
		assertFalse(ArrayUtils.contains(INT_ARRAY_1, new int[] { 101, 102, 103, 104 }));
		
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, INT_ARRAY_1));
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, Arrays.copyOf(INT_ARRAY_1, INT_ARRAY_1.length)));
		assertFalse(ArrayUtils.contains(INT_ARRAY_1, Arrays.copyOf(INT_ARRAY_1, INT_ARRAY_1.length + 1)));
		
		assertTrue(ArrayUtils.contains(INT_ARRAY_EMPTY, new int[] {}));
		assertFalse(ArrayUtils.contains(INT_ARRAY_EMPTY, new int[] { 0 }));
		assertFalse(ArrayUtils.contains(INT_ARRAY_EMPTY, new int[] { 3 }));
	}
	
	@Test
	@SuppressWarnings("null")
	public void contains_int_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.contains((int[])null, 0);
		});
	}
	
	@Test
	public void contains_int() {
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, 3));
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, 5));
		assertFalse(ArrayUtils.contains(INT_ARRAY_1, 0));
		assertTrue(ArrayUtils.contains(INT_ARRAY_1, 103));
		assertFalse(ArrayUtils.contains(INT_ARRAY_1, 104));
		
		assertFalse(ArrayUtils.contains(INT_ARRAY_EMPTY, 0));
		assertFalse(ArrayUtils.contains(INT_ARRAY_EMPTY, 3));
	}
	
	@Test
	@SuppressWarnings("null")
	public void startsWith_intArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.startsWith((int[])null, new int[] {});
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.startsWith(INT_ARRAY_1, (int[])null);
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.startsWith((int[])null, (int[])null);
		});
	}
	
	@Test
	public void startsWith_intArray() {
		assertTrue(ArrayUtils.startsWith(INT_ARRAY_1, new int[] {}));
		
		assertTrue(ArrayUtils.startsWith(INT_ARRAY_1, new int[] { 3 }));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_1, new int[] { 0 }));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_1, new int[] { 5 }));
		
		assertTrue(ArrayUtils.startsWith(INT_ARRAY_1, new int[] { 3, 4 }));
		assertTrue(ArrayUtils.startsWith(INT_ARRAY_1, new int[] { 3, 4, 5, 6 }));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_1, new int[] { 4, 4, 5, 6 }));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_1, new int[] { 3, 4, 5, 7 }));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_1, new int[] { 101, 102, 103 }));
		
		assertTrue(ArrayUtils.startsWith(INT_ARRAY_1, INT_ARRAY_1));
		assertTrue(ArrayUtils.startsWith(INT_ARRAY_1, Arrays.copyOf(INT_ARRAY_1, INT_ARRAY_1.length)));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_1, Arrays.copyOf(INT_ARRAY_1, INT_ARRAY_1.length + 1)));
		
		assertTrue(ArrayUtils.startsWith(INT_ARRAY_EMPTY, new int[] {}));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_EMPTY, new int[] { 0 }));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_EMPTY, new int[] { 3 }));
	}
	
	@Test
	@SuppressWarnings("null")
	public void startsWith_int_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.startsWith((int[])null, 0);
		});
	}
	
	@Test
	public void startsWith_int() {
		assertTrue(ArrayUtils.startsWith(INT_ARRAY_1, 3));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_1, 0));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_1, 5));
		
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_EMPTY, 0));
		assertFalse(ArrayUtils.startsWith(INT_ARRAY_EMPTY, 3));
	}
	
	@Test
	@SuppressWarnings("null")
	public void concat_intArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(null, new int[] { 21, 22, 23 });
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(new int[] { 11, 12 }, null);
		});
	}
	
	@Test
	public void concat_intArray() {
		assertArrayEquals(new int[] { 11, 12, 21, 22, 23 },
				ArrayUtils.concat(new int[] { 11, 12 }, new int[] { 21, 22, 23 }));
		assertArrayEquals(new int[] { 21, 22, 23 },
				ArrayUtils.concat(INT_ARRAY_EMPTY, new int[] { 21, 22, 23 }));
		assertArrayEquals(new int[] { 11, 12 },
				ArrayUtils.concat(new int[] { 11, 12 }, INT_ARRAY_EMPTY));
	}
	
	@Test
	@SuppressWarnings("null")
	public void concat_intArray3_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(null, new int[] { 21, 22, 23 }, new int[] { 31, 32, 33, 34 });
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(new int[] { 11, 12 }, null, new int[] { 31, 32, 33, 34 });
		});
		assertThrows(NullPointerException.class, () -> {
			ArrayUtils.concat(new int[] { 11, 12 }, new int[] { 21, 22, 23 }, null);
		});
	}
	
	@Test
	public void concat_intArray3() {
		assertArrayEquals(new int[] { 11, 12, 21, 22, 23, 31, 32, 33, 34 },
				ArrayUtils.concat(new int[] { 11, 12 }, new int[] { 21, 22, 23 }, new int[] { 31, 32, 33, 34 }));
		assertArrayEquals(new int[] { 21, 22, 23, 31, 32, 33, 34 },
				ArrayUtils.concat(INT_ARRAY_EMPTY, new int[] { 21, 22, 23 }, new int[] { 31, 32, 33, 34 }));
		assertArrayEquals(new int[] { 11, 12, 21, 22, 23 },
				ArrayUtils.concat(new int[] { 11, 12 }, new int[] { 21, 22, 23 }, INT_ARRAY_EMPTY));
	}
	
	
	@Test
	public void toInt_byteArray() {
		assertArrayEquals(new int[0],
				ArrayUtils.toInt(new byte[0]) );
		assertArrayEquals(new int[] { 0x00, 0x01, 0x7F, -0x01, -0x80 },
				ArrayUtils.toInt(new byte[] { 0x00, 0x01, Byte.MAX_VALUE, -0x01, Byte.MIN_VALUE } ) );
	}
	
	@Test
	public void toIntUnsigned_byteArray() {
		assertArrayEquals(new int[0],
				ArrayUtils.toIntUnsigned(new byte[0]) );
		assertArrayEquals(new int[] { 0x00, 0x01, 0x7F, 0xFF, 0x80 },
				ArrayUtils.toIntUnsigned(new byte[] { 0x00, 0x01, Byte.MAX_VALUE, -0x01, Byte.MIN_VALUE } ) );
	}
	
}
