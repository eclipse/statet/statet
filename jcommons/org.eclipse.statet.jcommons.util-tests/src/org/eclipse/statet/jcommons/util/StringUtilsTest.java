/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.collections.ImCollections.emptyList;
import static org.eclipse.statet.jcommons.collections.ImCollections.newList;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class StringUtilsTest {
	
	
	@Test
	public void firstIndexOfNonTrim_StringFromToIndex() {
		assertEquals(0, StringUtils.firstIndexOfNonTrim("", 0, 0));
		assertEquals(1, StringUtils.firstIndexOfNonTrim(" ", 0, 1));
		assertEquals(2, StringUtils.firstIndexOfNonTrim(" \t", 0, 2));
		assertEquals(4, StringUtils.firstIndexOfNonTrim(" \n\t ", 0, 4));
		assertEquals(1, StringUtils.firstIndexOfNonTrim("\n", 0, 1));
		assertEquals(0, StringUtils.firstIndexOfNonTrim("abc", 0, 1));
		assertEquals(3, StringUtils.firstIndexOfNonTrim(" \n\ta ", 0, 5));
		
		assertEquals(1, StringUtils.firstIndexOfNonTrim(" ", 1, 1));
		assertEquals(2, StringUtils.firstIndexOfNonTrim(" \t", 1, 2));
		assertEquals(4, StringUtils.firstIndexOfNonTrim(" \n\t ", 2, 4));
		assertEquals(1, StringUtils.firstIndexOfNonTrim("abc", 1, 2));
		assertEquals(3, StringUtils.firstIndexOfNonTrim(" \n\ta ", 2, 5));
		
		assertEquals(0, StringUtils.firstIndexOfNonTrim(" ", 0, 0));
		assertEquals(1, StringUtils.firstIndexOfNonTrim(" \t", 0, 1));
		assertEquals(2, StringUtils.firstIndexOfNonTrim(" \n\t ", 0, 2));
		assertEquals(1, StringUtils.firstIndexOfNonTrim("abc", 1, 2));
		assertEquals(2, StringUtils.firstIndexOfNonTrim(" \na\t ", 1, 3));
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstIndexOfNonTrim_StringFromToIndex_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfNonTrim(null, 0, 4) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfNonTrim(null, 0, 0) );
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstIndexOfNonTrim("abc", -1, 0) );
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstIndexOfNonTrim("abc", 0, 4) );
	}
	
	@Test
	public void firstIndexOfNonTrim_String() {
		assertEquals(0, StringUtils.firstIndexOfNonTrim(""));
		assertEquals(1, StringUtils.firstIndexOfNonTrim(" "));
		assertEquals(2, StringUtils.firstIndexOfNonTrim(" \t"));
		assertEquals(4, StringUtils.firstIndexOfNonTrim(" \n\t "));
		assertEquals(1, StringUtils.firstIndexOfNonTrim("\n"));
		assertEquals(0, StringUtils.firstIndexOfNonTrim("abc"));
		assertEquals(3, StringUtils.firstIndexOfNonTrim(" \n\ta "));
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstIndexOfNonTrim_String_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfNonTrim(null) );
	}
	
	@Test
	public void lastIndexOfNonTrim_StringFromToIndex() {
		assertEquals(0, StringUtils.lastIndexOfNonTrim("", 0, 0));
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" ", 0, 1));
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" \t", 0, 2));
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" \n\t ", 0, 4));
		assertEquals(0, StringUtils.lastIndexOfNonTrim("\n", 0, 1));
		assertEquals(1, StringUtils.lastIndexOfNonTrim("abc", 0, 1));
		assertEquals(4, StringUtils.lastIndexOfNonTrim(" \n\ta ", 0, 5));
		
		assertEquals(1, StringUtils.lastIndexOfNonTrim(" ", 1, 1));
		assertEquals(1, StringUtils.lastIndexOfNonTrim(" \t", 1, 2));
		assertEquals(2, StringUtils.lastIndexOfNonTrim(" \n\t ", 2, 4));
		assertEquals(2, StringUtils.lastIndexOfNonTrim("abc", 1, 2));
		assertEquals(4, StringUtils.lastIndexOfNonTrim(" \n\ta ", 2, 5));
		
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" ", 0, 0));
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" \t", 0, 1));
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" \n\t ", 0, 2));
		assertEquals(2, StringUtils.lastIndexOfNonTrim("abc", 1, 2));
		assertEquals(3, StringUtils.lastIndexOfNonTrim(" \na\t ", 1, 3));
	}
	
	@Test
	@SuppressWarnings("null")
	public void lastIndexOfNonTrim_StringFromToIndex_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.lastIndexOfNonTrim(null, 0, 4) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.lastIndexOfNonTrim(null, 0, 0) );
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.lastIndexOfNonTrim("abc", -1, 0) );
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.lastIndexOfNonTrim("abc", 0, 4) );
	}
	
	@Test
	public void lastIndexOfNonTrim_String() {
		assertEquals(0, StringUtils.lastIndexOfNonTrim(""));
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" "));
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" \t"));
		assertEquals(0, StringUtils.lastIndexOfNonTrim(" \n\t "));
		assertEquals(0, StringUtils.lastIndexOfNonTrim("\n"));
		assertEquals(3, StringUtils.lastIndexOfNonTrim("abc"));
		assertEquals(4, StringUtils.lastIndexOfNonTrim(" \n\ta "));
	}
	
	@Test
	@SuppressWarnings("null")
	public void lastIndexOfNonTrim_String_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.lastIndexOfNonTrim(null) );
	}
	
	
	@Test
	public void trim_StringFromToIndex() {
		assertEquals("", StringUtils.trim("", 0, 0));
		assertEquals("", StringUtils.trim(" ", 0, 1));
		assertEquals("", StringUtils.trim(" \t", 0, 2));
		assertEquals("", StringUtils.trim(" \n\t ", 0, 4));
		assertEquals("", StringUtils.trim("\n", 0, 1));
		assertEquals("a", StringUtils.trim("abc", 0, 1));
		assertEquals("a", StringUtils.trim(" \n\ta ", 0, 5));
		
		assertEquals("", StringUtils.trim(" ", 1, 1));
		assertEquals("", StringUtils.trim(" \t", 1, 2));
		assertEquals("", StringUtils.trim(" \n\t ", 2, 4));
		assertEquals("b", StringUtils.trim("abc", 1, 2));
		assertEquals("a", StringUtils.trim(" \n\ta ", 2, 5));
		
		assertEquals("", StringUtils.trim(" ", 0, 0));
		assertEquals("", StringUtils.trim(" \t", 0, 1));
		assertEquals("", StringUtils.trim(" \n\t ", 0, 2));
		assertEquals("b", StringUtils.trim("abc", 1, 2));
		assertEquals("a", StringUtils.trim(" \na\t ", 1, 3));
		assertEquals("", StringUtils.trim(" \na\t ", 1, 2));
	}
	
	@Test
	public void isTrimEmpty_StringFromToIndex() {
		assertTrue(StringUtils.isTrimEmpty("", 0, 0));
		assertTrue(StringUtils.isTrimEmpty(" ", 0, 1));
		assertTrue(StringUtils.isTrimEmpty(" \t", 0, 2));
		assertTrue(StringUtils.isTrimEmpty(" \n\t ", 0, 4));
		assertTrue(StringUtils.isTrimEmpty("\n", 0, 1));
		assertFalse(StringUtils.isTrimEmpty("abc", 0, 1));
		assertFalse(StringUtils.isTrimEmpty(" \n\ta ", 0, 5));
		
		assertTrue(StringUtils.isTrimEmpty(" ", 1, 1));
		assertTrue(StringUtils.isTrimEmpty(" \t", 1, 2));
		assertTrue(StringUtils.isTrimEmpty(" \n\t ", 2, 4));
		assertFalse(StringUtils.isTrimEmpty("abc", 1, 2));
		assertFalse(StringUtils.isTrimEmpty(" \n\ta ", 2, 5));
		
		assertTrue(StringUtils.isTrimEmpty(" ", 0, 0));
		assertTrue(StringUtils.isTrimEmpty(" \t", 0, 1));
		assertTrue(StringUtils.isTrimEmpty(" \n\t ", 0, 2));
		assertFalse(StringUtils.isTrimEmpty("abc", 1, 2));
		assertFalse(StringUtils.isTrimEmpty(" \na\t ", 1, 3));
		assertTrue(StringUtils.isTrimEmpty(" \na\t ", 1, 2));
	}
	
	@Test
	@SuppressWarnings("null")
	public void isTrimEmpty_StringFromToIndex_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.isTrimEmpty(null, 0, 4) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.isTrimEmpty(null, 0, 0) );
	}
	
	@Test
	public void isTrimEmpty_String() {
		assertTrue(StringUtils.isTrimEmpty(""));
		assertTrue(StringUtils.isTrimEmpty(" "));
		assertTrue(StringUtils.isTrimEmpty(" \t"));
		assertTrue(StringUtils.isTrimEmpty(" \n\t "));
		assertTrue(StringUtils.isTrimEmpty("\n"));
		assertFalse(StringUtils.isTrimEmpty("a"));
		assertFalse(StringUtils.isTrimEmpty(" \n\ta "));
	}
	
	@Test
	@SuppressWarnings("null")
	public void isTrimEmpty_String_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.isTrimEmpty(null) );
	}
	
	
	@Test
	public void containsAny_String() {
		boolean match;
		
		match= StringUtils.containsAny("xxxxxxxxxx", emptyList());
		assertFalse(match);
		
		match= StringUtils.containsAny("xxxxxxxxxx", newList("a", "ab", "abc"));
		assertFalse(match);
		
		match= StringUtils.containsAny("foobarabcd", newList("a", "ab", "abc"));
		assertTrue(match);
		
		match= StringUtils.containsAny("foobarabcd", newList("ab", "abc"));
		assertTrue(match);
		
		match= StringUtils.containsAny("foobarabcd", newList("ab", "ab"));
		assertTrue(match);
		
		match= StringUtils.containsAny("foobarabcd", newList("", "ab", "abc"));
		assertTrue(match);
	}
	
	@Test
	@SuppressWarnings("null")
	public void containsAny_String_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.containsAny((String)null, newList("abc")) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.containsAny("foobarabcd", null) );
//		assertThrows(NullPointerException.class, () ->
//				StringUtils.containsAny("foobarabcd", newList("abc", null)) );
	}
	
	
	@Test
	public void containsAny_StringBuilder() {
		boolean match;
		
		match= StringUtils.containsAny(new StringBuilder("xxxxxxxxxx"), emptyList());
		assertFalse(match);
		
		match= StringUtils.containsAny(new StringBuilder("xxxxxxxxxx"), newList("a", "ab", "abc"));
		assertFalse(match);
		
		match= StringUtils.containsAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"));
		assertTrue(match);
		
		match= StringUtils.containsAny(new StringBuilder("foobarabcd"), newList("ab", "abc"));
		assertTrue(match);
		
		match= StringUtils.containsAny(new StringBuilder("foobarabcd"), newList("ab", "ab"));
		assertTrue(match);
		
		match= StringUtils.containsAny(new StringBuilder("foobarabcd"), newList("", "ab", "abc"));
		assertTrue(match);
	}
	
	@Test
	@SuppressWarnings("null")
	public void containsAny_StringBuilder_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.containsAny((StringBuilder)null, newList("abc")) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.containsAny(new StringBuilder("foobarabcd"), null) );
//		assertThrows(NullPointerException.class, () ->
//				StringUtils.containsAny(new StringBuilder("foobarabcd"), newList("abc", null)) );
	}
	
	
	@Test
	public void firstIndexOfAny_StringFromIndex() {
		int match;
		
		match= StringUtils.firstIndexOfAny("xxxxxxxxxx", emptyList(), 0);
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny("xxxxxxxxxx", newList("a", "ab", "abc"), 0);
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("a", "ab", "abc"), 0);
		assertEquals(4, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("ab", "ab"), 0);
		assertEquals(6, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("", "ab", "abc"), 0);
		assertEquals(0, match);
		
		match= StringUtils.firstIndexOfAny("xxxxxxxxxx", emptyList(), 5);
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny("xaxxabcxxx", newList("a", "ab", "abc"), 5);
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("a", "ab", "abc"), 5);
		assertEquals(6, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("", "ab", "abc"), 5);
		assertEquals(5, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("a", "ab", "abc"), "foobarabcd".length());
		assertEquals(-1, match);
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstIndexOfAny_StringFromIndex_argCheck() {
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstIndexOfAny("foobarabcd", newList("abc"), -5) );
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstIndexOfAny("foobarabcd", newList("abc"), 20) );
		
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny((String)null, newList("abc"), 5) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny("foobarabcd", null, 5) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny("foobarabcd", newList("abc", null), 5) );
	}
	
	@Test
	public void firstIndexOfAny_String() {
		int match;
		
		match= StringUtils.firstIndexOfAny("xxxxxxxxxx", emptyList());
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny("xxxxxxxxxx", newList("a", "ab", "abc"));
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("a", "ab", "abc"));
		assertEquals(4, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("ab", "abc"));
		assertEquals(6, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("ab", "ab"));
		assertEquals(6, match);
		
		match= StringUtils.firstIndexOfAny("foobarabcd", newList("", "ab", "abc"));
		assertEquals(0, match);
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstIndexOfAny_String_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny((String)null, newList("abc")) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny("foobarabcd", null) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny("foobarabcd", newList("abc", null)) );
	}
	
	
	@Test
	public void firstIndexOfAny_StringBuilderFromIndex() {
		int match;
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("xxxxxxxxxx"), emptyList(), 0);
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("xxxxxxxxxx"), newList("a", "ab", "abc"), 0);
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"), 0);
		assertEquals(4, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("ab", "ab"), 0);
		assertEquals(6, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("", "ab", "abc"), 0);
		assertEquals(0, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("xxxxxxxxxx"), emptyList(), 5);
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("xaxxabcxxx"), newList("a", "ab", "abc"), 5);
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"), 5);
		assertEquals(6, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("", "ab", "abc"), 5);
		assertEquals(5, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"), "foobarabcd".length());
		assertEquals(-1, match);
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstIndexOfAny_StringBuilderFromIndex_argCheck() {
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("abc"), -5) );
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("abc"), 20) );
		
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny((StringBuilder)null, newList("abc"), 5) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), null, 5) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("abc", null), 5) );
	}
	
	@Test
	public void firstIndexOfAny_StringBuilder() {
		int match;
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("xxxxxxxxxx"), emptyList());
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("xxxxxxxxxx"), newList("a", "ab", "abc"));
		assertEquals(-1, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"));
		assertEquals(4, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("ab", "abc"));
		assertEquals(6, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("ab", "ab"));
		assertEquals(6, match);
		
		match= StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("", "ab", "abc"));
		assertEquals(0, match);
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstIndexOfAny_StringBuilder_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny((StringBuilder)null, newList("abc")) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), null) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstIndexOfAny(new StringBuilder("foobarabcd"), newList("abc", null)) );
	}
	
	
	@Test
	public void Match() {
		StringUtils.Match match;
		
		match= new StringUtils.Match("abc", 2);
		assertEquals("abc", match.getString());
		assertEquals(2, match.getStartIndex());
		assertEquals(2 + 3, match.getEndIndex());
		assertEquals(3, match.getLength());
	}
	
	@Test
	@SuppressWarnings("null")
	public void Match_argCheck() {
		assertThrows(NullPointerException.class, () ->
			new StringUtils.Match(null, 2) );
	}
	
	
	@Test
	public void firstMatchOfAny_StringFromIndex() {
		StringUtils.Match match;
		
		match= StringUtils.firstMatchOfAny("xxxxxxxxxx", emptyList(), 0);
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny("xxxxxxxxxx", newList("a", "ab", "abc"), 0);
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("a", "ab", "abc"), 0);
		assertNotNull(match);
		assertEquals("a", match.getString());
		assertEquals(4, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("ab", "ab"), 0);
		assertNotNull(match);
		assertEquals("ab", match.getString());
		assertEquals(6, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("", "ab", "abc"), 0);
		assertNotNull(match);
		assertEquals("", match.getString());
		assertEquals(0, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny("xxxxxxxxxx", emptyList(), 5);
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny("xaxxabcxxx", newList("a", "ab", "abc"), 5);
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("a", "ab", "abc"), 5);
		assertNotNull(match);
		assertEquals("abc", match.getString());
		assertEquals(6, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("", "ab", "abc"), 5);
		assertNotNull(match);
		assertEquals("", match.getString());
		assertEquals(5, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("a", "ab", "abc"), "foobarabcd".length());
		assertNull(match);
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstMatchOfAny_StringFromIndex_argCheck() {
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstMatchOfAny("foobarabcd", newList("abc"), -5) );
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstMatchOfAny("foobarabcd", newList("abc"), 20) );
		
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny((String)null, newList("abc"), 5) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny("foobarabcd", null, 5) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny("foobarabcd", newList("abc", null), 5) );
	}
	
	@Test
	public void firstMatchOfAny_String() {
		StringUtils.Match match;
		
		match= StringUtils.firstMatchOfAny("xxxxxxxxxx", emptyList());
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny("xxxxxxxxxx", newList("a", "ab", "abc"));
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("a", "ab", "abc"));
		assertNotNull(match);
		assertEquals("a", match.getString());
		assertEquals(4, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("ab", "abc"));
		assertNotNull(match);
		assertEquals("abc", match.getString());
		assertEquals(6, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("ab", "ab"));
		assertNotNull(match);
		assertEquals("ab", match.getString());
		assertEquals(6, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny("foobarabcd", newList("", "ab", "abc"));
		assertNotNull(match);
		assertEquals("", match.getString());
		assertEquals(0, match.getStartIndex());
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstMatchOfAny_String_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny((String)null, newList("abc")) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny("foobarabcd", null) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny("foobarabcd", newList("abc", null)) );
	}
	
	
	@Test
	public void firstMatchOfAny_StringBuilderFromIndex() {
		StringUtils.Match match;
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("xxxxxxxxxx"), emptyList(), 0);
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("xxxxxxxxxx"), newList("a", "ab", "abc"), 0);
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"), 0);
		assertNotNull(match);
		assertEquals("a", match.getString());
		assertEquals(4, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("ab", "ab"), 0);
		assertNotNull(match);
		assertEquals("ab", match.getString());
		assertEquals(6, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("", "ab", "abc"), 0);
		assertNotNull(match);
		assertEquals("", match.getString());
		assertEquals(0, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("xxxxxxxxxx"), emptyList(), 5);
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("xaxxabcxxx"), newList("a", "ab", "abc"), 5);
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"), 5);
		assertNotNull(match);
		assertEquals("abc", match.getString());
		assertEquals(6, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("", "ab", "abc"), 5);
		assertNotNull(match);
		assertEquals("", match.getString());
		assertEquals(5, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"), "foobarabcd".length());
		assertNull(match);
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstMatchOfAny_StringBuilderFromIndex_argCheck() {
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("abc"), -5) );
		assertThrows(StringIndexOutOfBoundsException.class, () ->
				StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("abc"), 20) );
		
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny((StringBuilder)null, newList("abc"), 5) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), null, 5) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("abc", null), 5) );
	}
	
	@Test
	public void firstMatchOfAny_StringBuilder() {
		StringUtils.Match match;
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("xxxxxxxxxx"), emptyList());
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("xxxxxxxxxx"), newList("a", "ab", "abc"));
		assertNull(match);
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("a", "ab", "abc"));
		assertNotNull(match);
		assertEquals("a", match.getString());
		assertEquals(4, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("ab", "abc"));
		assertNotNull(match);
		assertEquals("abc", match.getString());
		assertEquals(6, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("ab", "ab"));
		assertNotNull(match);
		assertEquals("ab", match.getString());
		assertEquals(6, match.getStartIndex());
		
		match= StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("", "ab", "abc"));
		assertNotNull(match);
		assertEquals("", match.getString());
		assertEquals(0, match.getStartIndex());
	}
	
	@Test
	@SuppressWarnings("null")
	public void firstMatchOfAny_StringBuilder_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny((StringBuilder)null, newList("abc")) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), null) );
		assertThrows(NullPointerException.class, () ->
				StringUtils.firstMatchOfAny(new StringBuilder("foobarabcd"), newList("abc", null)) );
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void replace_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.replace(null, 0, 0, "XX") );
		assertThrows(NullPointerException.class, () ->
				StringUtils.replace("abcdef", 0, 0, (String)null) );
		assertThrows(IndexOutOfBoundsException.class, () ->
				StringUtils.replace("abcdef", -1, 0, "XX") );
		assertThrows(IndexOutOfBoundsException.class, () ->
				StringUtils.replace("abcdef", 6, 7, "XX") );
		assertThrows(IndexOutOfBoundsException.class, () ->
				StringUtils.replace("abcdef", -1, 3, "XX") );
		assertThrows(IndexOutOfBoundsException.class, () ->
				StringUtils.replace("abcdef", 3, 7, "XX") );
	}
	
	@Test
	public void replace() {
		assertEquals("XXabcdef",
				StringUtils.replace("abcdef", 0, 0, "XX") );
		assertEquals("aXXbcdef",
				StringUtils.replace("abcdef", 1, 1, "XX") );
		assertEquals("abcdefXX",
				StringUtils.replace("abcdef", 6, 6, "XX") );
		
		assertEquals("XXcdef",
				StringUtils.replace("abcdef", 0, 2, "XX") );
		assertEquals("aXXdef",
				StringUtils.replace("abcdef", 1, 3, "XX") );
		assertEquals("abcdXX",
				StringUtils.replace("abcdef", 4, 6, "XX") );
		
		assertEquals("XXef",
				StringUtils.replace("abcdef", 0, 4, "XX") );
		assertEquals("aXXf",
				StringUtils.replace("abcdef", 1, 5, "XX") );
		assertEquals("abXX",
				StringUtils.replace("abcdef", 2, 6, "XX") );
	}
	
	@Test
	@SuppressWarnings("null")
	public void insert_String_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.insert(null, 0, "XX") );
		assertThrows(NullPointerException.class, () ->
				StringUtils.insert("abcdef", 0, (String)null) );
		assertThrows(IndexOutOfBoundsException.class, () ->
				StringUtils.insert("abcdef", -1, "XX") );
		assertThrows(IndexOutOfBoundsException.class, () ->
				StringUtils.insert("abcdef", 7, "XX") );
	}
	
	@Test
	public void insert_String() {
		assertEquals("abcdef",
				StringUtils.insert("abcdef", 0, "") );
		assertEquals("abcdef",
				StringUtils.insert("abcdef", 1, "") );
		assertEquals("abcdef",
				StringUtils.insert("abcdef", 6, "") );
		
		assertEquals("XXabcdef",
				StringUtils.insert("abcdef", 0, "XX") );
		assertEquals("aXXbcdef",
				StringUtils.insert("abcdef", 1, "XX") );
		assertEquals("abcdefXX",
				StringUtils.insert("abcdef", 6, "XX") );
		
		assertEquals("XXXXabcdef",
				StringUtils.insert("abcdef", 0, "XXXX") );
		assertEquals("aXXXXbcdef",
				StringUtils.insert("abcdef", 1, "XXXX") );
		assertEquals("abcdefXXXX",
				StringUtils.insert("abcdef", 6, "XXXX") );
	}
	
	@Test
	@SuppressWarnings("null")
	public void insert_char_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.insert(null, 0, 'X') );
		assertThrows(IndexOutOfBoundsException.class, () ->
				StringUtils.insert("abcdef", -1, 'X') );
		assertThrows(IndexOutOfBoundsException.class, () ->
				StringUtils.insert("abcdef", 7, 'X') );
	}
	
	@Test
	public void insert_char() {
		assertEquals("Xabcdef",
				StringUtils.insert("abcdef", 0, 'X') );
		assertEquals("aXbcdef",
				StringUtils.insert("abcdef", 1, 'X') );
		assertEquals("abcdefX",
				StringUtils.insert("abcdef", 6, 'X') );
	}
	
	
	@Test
	public void appendSpaces() {
		final StringBuilder sb= new StringBuilder();
		
		sb.setLength(0);
		StringUtils.appendSpaces(sb, 0);
		assertEquals("", sb.toString());
		
		sb.setLength(0);
		StringUtils.appendSpaces(sb, 1);
		assertEquals(" ", sb.toString());
		
		sb.setLength(0);
		StringUtils.appendSpaces(sb.append("abc"), 4);
		assertEquals("abc    ", sb.toString());
		
		sb.setLength(0);
		StringUtils.appendSpaces(sb, 1234);
		assertEquals(" ".repeat(1234), sb.toString());
	}
	
	@Test
	public void insertSpaces() {
		final StringBuilder sb= new StringBuilder();
		
		sb.setLength(0);
		StringUtils.insertSpaces(sb, 0, 0);
		assertEquals("", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertSpaces(sb, 0, 1);
		assertEquals(" ", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertSpaces(sb.append("123"), 0, 1);
		assertEquals(" 123", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertSpaces(sb.append("123"), 1, 1);
		assertEquals("1 23", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertSpaces(sb.append("123"), 3, 1);
		assertEquals("123 ", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertSpaces(sb.append("123"), 2, 4);
		assertEquals("12    3", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertSpaces(sb.append("123"), 3, 1234);
		assertEquals("123" + " ".repeat(1234), sb.toString());
	}
	
	@Test
	public void appendTabs() {
		final StringBuilder sb= new StringBuilder();
		
		sb.setLength(0);
		StringUtils.appendTabs(sb, 0);
		assertEquals("", sb.toString());
		
		sb.setLength(0);
		StringUtils.appendTabs(sb, 1);
		assertEquals("\t", sb.toString());
		
		sb.setLength(0);
		StringUtils.appendTabs(sb.append("abc"), 4);
		assertEquals("abc\t\t\t\t", sb.toString());
		
		sb.setLength(0);
		StringUtils.appendTabs(sb, 1234);
		assertEquals("\t".repeat(1234), sb.toString());
	}
	
	@Test
	public void insertTabs() {
		final StringBuilder sb= new StringBuilder();
		
		sb.setLength(0);
		StringUtils.insertTabs(sb, 0, 0);
		assertEquals("", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertTabs(sb, 0, 1);
		assertEquals("\t", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertTabs(sb.append("123"), 0, 1);
		assertEquals("\t123", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertTabs(sb.append("123"), 1, 1);
		assertEquals("1\t23", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertTabs(sb.append("123"), 3, 1);
		assertEquals("123\t", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertTabs(sb.append("123"), 2, 4);
		assertEquals("12\t\t\t\t3", sb.toString());
		
		sb.setLength(0);
		StringUtils.insertTabs(sb.append("123"), 2, 1234);
		assertEquals("12" + "\t".repeat(1234) + "3", sb.toString());
	}
	
	
	@Test
	public void toSimpleSingleLine() {
		assertEquals("foobarabcd",
				StringUtils.toSimpleSingleLine("foobarabcd") );
		assertEquals("",
				StringUtils.toSimpleSingleLine("") );
		assertEquals("foobarabcd",
				StringUtils.toSimpleSingleLine("foobarabcd\n") );
		assertEquals("foobar  abcd",
				StringUtils.toSimpleSingleLine("foobar\tabcd") );
		assertEquals("foobar \u2014 abcd",
				StringUtils.toSimpleSingleLine("foobar\nabcd") );
		assertEquals("foobar",
				StringUtils.toSimpleSingleLine("foobar\fabcd") );
		assertEquals("foobarabcd",
				StringUtils.toSimpleSingleLine("foobar\u0000abcd") );
		
		assertEquals("foo  barabcd",
				StringUtils.toSimpleSingleLine("foo\tbar\u0000\u0000abcd\n") );
	}
	
	@Test
	@SuppressWarnings("null")
	public void toSimpleSingleLine_argCheck() {
		assertThrows(NullPointerException.class, () ->
				StringUtils.toSimpleSingleLine((String)null) );
	}
	
}
