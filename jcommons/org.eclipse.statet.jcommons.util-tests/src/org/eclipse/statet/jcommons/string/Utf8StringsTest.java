/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class Utf8StringsTest {
	
	
	public Utf8StringsTest() {
	}
	
	
	@Test
	public void isValidCodepoint() {
		assertFalse(Utf8Strings.isValidCodepoint(-1));
		assertTrue(Utf8Strings.isValidCodepoint(0));
		assertTrue(Utf8Strings.isValidCodepoint(1));
		assertTrue(Utf8Strings.isValidCodepoint('A'));
		assertTrue(Utf8Strings.isValidCodepoint(0x007F));
		assertTrue(Utf8Strings.isValidCodepoint(0x0080));
		assertTrue(Utf8Strings.isValidCodepoint(0x00FF));
		assertTrue(Utf8Strings.isValidCodepoint(0x0100));
		assertTrue(Utf8Strings.isValidCodepoint(0xD7FF));
		assertFalse(Utf8Strings.isValidCodepoint(0xD800));
		assertFalse(Utf8Strings.isValidCodepoint(0xDFFF));
		assertTrue(Utf8Strings.isValidCodepoint(0xE000));
		assertTrue(Utf8Strings.isValidCodepoint(0xFFFF));
		assertTrue(Utf8Strings.isValidCodepoint(0x010001));
		assertTrue(Utf8Strings.isValidCodepoint(0x10FFFF));
		assertFalse(Utf8Strings.isValidCodepoint(0x110000));
	}
	
	@Test
	@SuppressWarnings("null")
	public void getCodepoint_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			Utf8Strings.getCodepoint(null, 0, 0);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			Utf8Strings.getCodepoint(new byte[10], -1, 1);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			Utf8Strings.getCodepoint(new byte[10], 0, 11);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			Utf8Strings.getCodepoint(new byte[10], 1, 0);
		});
		
		assertThrows(NullPointerException.class, () -> {
			Utf8Strings.getCodepoint(null, 0);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			Utf8Strings.getCodepoint(new byte[10], -1);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			Utf8Strings.getCodepoint(new byte[10], 11);
		});
	}
	
	@Test
	public void getCodepoint() {
		for (int cp= 0x0000; cp <= Character.MAX_CODE_POINT; cp++) {
			if (Utf8Strings.isValidCodepoint(cp)) {
				final String s= Character.toString(cp);
				final byte[] bytes= s.getBytes(StandardCharsets.UTF_8);
				assertEquals(cp, Utf8Strings.getCodepoint(bytes, 0),
						Chars.formatCodePoint(cp) );
			}
		}
		
		{	final int[] cps= new int[] { 'A', 'B', 0, 0x0080, 0x0800, 0x1678, 0x10678 };
			final String s= new String(cps, 0, cps.length);
			final byte[] bytes= s.getBytes(StandardCharsets.UTF_8);
			for (int i= 0, offset= 0; i < cps.length; i++) {
				assertEquals(cps[i], Utf8Strings.getCodepoint(bytes, offset));
				offset+= Utf8Strings.getCodepointLength(cps[i]);
			}
		}
	}
	
	@Test
	public void getCodepoint_invalid() {
		final byte[] bytes= new byte[10];
		
		bytes[0]= (byte)0b10000000;
		bytes[1]= 'a';
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		
		// incomplete
		bytes[1]= 'a';
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0, 0));
		
		bytes[0]= (byte)0b11000000;
		bytes[1]= 'a';
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		bytes[1]= (byte)0b10000001;
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0, 1));
		
		bytes[0]= (byte)0b11100000;
		bytes[1]= 'a';
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		bytes[1]= (byte)0b10000001;
		bytes[2]= 'a';
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		bytes[2]= (byte)0b10000001;
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0, 2));
		
		bytes[0]= (byte)0b11110000;
		bytes[1]= 'a';
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		bytes[1]= (byte)0b10000001;
		bytes[2]= 'a';
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		bytes[2]= (byte)0b10000001;
		bytes[3]= 'a';
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		bytes[3]= (byte)0b10000001;
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0, 3));
		
		bytes[0]= (byte)0b11111000;
		bytes[1]= (byte)0b10000001;
		bytes[2]= (byte)0b10000001;
		bytes[3]= (byte)0b10000001;
		bytes[4]= (byte)0b10000001;
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		
		// surrogate
		bytes[0]= (byte)0b11101101;
		bytes[1]= (byte)0b10100000;
		bytes[2]= (byte)0b10000000;
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
		bytes[0]= (byte)0b11101101;
		bytes[1]= (byte)0b10111111;
		bytes[2]= (byte)0b10111111;
		assertEquals(-1, Utf8Strings.getCodepoint(bytes, 0));
	}
	
	@Test
	public void getCodepointLength() {
		for (int cp= 0x0000; cp <= Character.MAX_CODE_POINT; cp++) {
			if (Utf8Strings.isValidCodepoint(cp)) {
				final String s= Character.toString(cp);
				final byte[] bytes= s.getBytes(StandardCharsets.UTF_8);
				assertEquals(bytes.length, Utf8Strings.getCodepointLength(cp),
						Chars.formatCodePoint(cp) );
			}
		}
		
		assertEquals(0, Utf8Strings.getCodepointLength(-1));
	}
	
}
