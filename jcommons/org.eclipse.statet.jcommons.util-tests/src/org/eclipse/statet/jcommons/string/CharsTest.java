/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CharsTest {
	
	
	public CharsTest() {
	}
	
	
	@Test
	public void getAsciiControlAbbr_checkArgs() {
		assertThrows(IllegalArgumentException.class, () -> {
			Chars.getAsciiControlAbbr('A');
		});
	}
	
	@Test
	public void getAsciiControlAbbr() {
		assertEquals("NUL", Chars.getAsciiControlAbbr((char)0));
		assertEquals("HT", Chars.getAsciiControlAbbr('\t'));
		assertEquals("LF", Chars.getAsciiControlAbbr('\n'));
		assertEquals("SP", Chars.getAsciiControlAbbr(' '));
		assertEquals("DEL", Chars.getAsciiControlAbbr('\u007F'));
	}
	
	@Test
	public void getAsciiControlSymbol_checkArgs() {
		assertThrows(IllegalArgumentException.class, () -> {
			Chars.getAsciiControlSymbol('A');
		});
	}
	
	@Test
	public void getAsciiControlSymbol() {
		assertEquals(0x2400, Chars.getAsciiControlSymbol((char)0));
		assertEquals(0x2409, Chars.getAsciiControlSymbol('\t'));
		assertEquals(0x240A, Chars.getAsciiControlSymbol('\n'));
		assertEquals(0x2420, Chars.getAsciiControlSymbol(' '));
		assertEquals(0x2421, Chars.getAsciiControlSymbol('\u007F'));
	}
	
	@Test
	public void formatCodePoint() {
		assertEquals("U+0000", Chars.formatCodePoint(0)); // MIN
		assertEquals("U+0001", Chars.formatCodePoint(1));
		assertEquals("U+0041", Chars.formatCodePoint('A'));
		assertEquals("U+FFFF", Chars.formatCodePoint(0xFFFF));
		assertEquals("U+010000", Chars.formatCodePoint(0x10000));
		assertEquals("U+10FFFF", Chars.formatCodePoint(0x10FFFF)); // MAX
		assertEquals("U+FFFFFFFF", Chars.formatCodePoint(0xFFFFFFFF)); // <invalid>
	}
	
}
