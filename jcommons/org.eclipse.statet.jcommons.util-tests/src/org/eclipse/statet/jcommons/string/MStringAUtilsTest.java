/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class MStringAUtilsTest {
	
	
	public MStringAUtilsTest() {
	}
	
	
	static ImList<String> lineBreaks() {
		return ImCollections.newList("\n", "\r", "\r\n");
	}
	
	
	@Test
	public void isLineSeparator_Char() {
		assertTrue(MStringAUtils.isLineSeparator('\n'));
		assertTrue(MStringAUtils.isLineSeparator('\r'));
		assertFalse(MStringAUtils.isLineSeparator('\u0000'));
		assertFalse(MStringAUtils.isLineSeparator('\f'));
		assertFalse(MStringAUtils.isLineSeparator(' '));
		assertFalse(MStringAUtils.isLineSeparator('A'));
		assertFalse(MStringAUtils.isLineSeparator('z'));
		assertFalse(MStringAUtils.isLineSeparator('\u00FF'));
		assertFalse(MStringAUtils.isLineSeparator('\u0100'));
	}
	
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void replaceTabsWithSpaces_StringBuilder(final String nl) {
		final StringBuilder sb= new StringBuilder();
		
		sb.setLength(0);
		MStringAUtils.replaceTabsWithSpaces(sb.append("\tABC\tD" + nl + " \tABC\t  \t"), 4);
		assertEquals("    ABC D" + nl + "    ABC     ", sb.toString());
		
		sb.setLength(0);
		MStringAUtils.replaceTabsWithSpaces(sb.append("\t" + nl), 4);
		assertEquals("    " + nl, sb.toString());
	}
	
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void computeIndent_String(final String nl) {
		assertEquals(0, MStringAUtils.computeIndent("", 4));
		
		assertEquals(0, MStringAUtils.computeIndent("A1", 4));
		assertEquals(0, MStringAUtils.computeIndent("A1" + nl + "A2", 4));
		assertEquals(0, MStringAUtils.computeIndent(" A1" + nl + "A2", 4));
		assertEquals(0, MStringAUtils.computeIndent("  A1" + nl + "A2", 4));
		assertEquals(0, MStringAUtils.computeIndent("\tA1" + nl + "A2", 4));
		assertEquals(1, MStringAUtils.computeIndent(" A1", 4));
		assertEquals(1, MStringAUtils.computeIndent(" A1" + nl + " A2", 4));
		assertEquals(1, MStringAUtils.computeIndent(" A1" + nl + "  A2", 4));
		assertEquals(1, MStringAUtils.computeIndent("  A1" + nl + " A2", 4));
		assertEquals(1, MStringAUtils.computeIndent("\tA1" + nl + " A2", 4));
		assertEquals(2, MStringAUtils.computeIndent("\tA1" + nl + "  A2", 4));
		assertEquals(4, MStringAUtils.computeIndent("\tA1" + nl + "      A2", 4));
		assertEquals(4, MStringAUtils.computeIndent("   \tA1" + nl + "      A2", 4));
		
		assertEquals(0, MStringAUtils.computeIndent("A1" + nl, 4));
		assertEquals(0, MStringAUtils.computeIndent("A1" + nl + " ", 4));
		assertEquals(0, MStringAUtils.computeIndent("  A1" + nl + "A2" + nl + "", 4));
		assertEquals(2, MStringAUtils.computeIndent("  A1" + nl + "", 4));
		assertEquals(1, MStringAUtils.computeIndent("  A1" + nl + " ", 4));
		assertEquals(2, MStringAUtils.computeIndent("  A1" + nl + "  A2" + nl + "", 4));
		assertEquals(8, MStringAUtils.computeIndent("\t  \t" + nl, 4));
	}
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void computeIndent_StringSub(final String nl) {
		assertEquals(0, MStringAUtils.computeIndent("pre", 3, 3, 4));
		
		assertEquals(0, MStringAUtils.computeIndent("preA1", 3, 5, 4));
		assertEquals(0, MStringAUtils.computeIndent("preA1" + nl + "A2", 3, 7 + nl.length(), 4));
		assertEquals(0, MStringAUtils.computeIndent("preA1" + nl + "A2", 3, 5 + nl.length(), 4));
		assertEquals(0, MStringAUtils.computeIndent("pre A1" + nl + "A2", 3, 8 + nl.length(), 4));
		assertEquals(1, MStringAUtils.computeIndent("pre A1" + nl + "A2", 3, 6 + nl.length(), 4));
	}
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void computeIndent_StringBuilder(final String nl) {
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder(""), 4));
		
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("A1"), 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("A1" + nl + "A2"), 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder(" A1" + nl + "A2"), 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("  A1" + nl + "A2"), 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("\tA1" + nl + "A2"), 4));
		assertEquals(1, MStringAUtils.computeIndent(new StringBuilder(" A1"), 4));
		assertEquals(1, MStringAUtils.computeIndent(new StringBuilder(" A1" + nl + " A2"), 4));
		assertEquals(1, MStringAUtils.computeIndent(new StringBuilder(" A1" + nl + "  A2"), 4));
		assertEquals(1, MStringAUtils.computeIndent(new StringBuilder("  A1" + nl + " A2"), 4));
		assertEquals(1, MStringAUtils.computeIndent(new StringBuilder("\tA1" + nl + " A2"), 4));
		assertEquals(2, MStringAUtils.computeIndent(new StringBuilder("\tA1" + nl + "  A2"), 4));
		assertEquals(4, MStringAUtils.computeIndent(new StringBuilder("\tA1" + nl + "      A2"), 4));
		assertEquals(4, MStringAUtils.computeIndent(new StringBuilder("   \tA1" + nl + "      A2"), 4));
		
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("A1" + nl), 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("A1" + nl + " "), 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("  A1" + nl + "A2" + nl + ""), 4));
		assertEquals(2, MStringAUtils.computeIndent(new StringBuilder("  A1" + nl + ""), 4));
		assertEquals(1, MStringAUtils.computeIndent(new StringBuilder("  A1" + nl + " "), 4));
		assertEquals(2, MStringAUtils.computeIndent(new StringBuilder("  A1" + nl + "  A2" + nl + ""), 4));
		assertEquals(8, MStringAUtils.computeIndent(new StringBuilder("\t  \t" + nl), 4));
	}
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void computeIndent_StringBuilderSub(final String nl) {
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("pre"), 3, 3, 4));
		
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("preA1"), 3, 5, 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("preA1" + nl + "A2"), 3, 7 + nl.length(), 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("preA1" + nl + "A2"), 3, 5 + nl.length(), 4));
		assertEquals(0, MStringAUtils.computeIndent(new StringBuilder("pre A1" + nl + "A2"), 3, 8 + nl.length(), 4));
		assertEquals(1, MStringAUtils.computeIndent(new StringBuilder("pre A1" + nl + "A2"), 3, 6 + nl.length(), 4));
	}
	
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void collectLines(final String nl) {
		final var lines= new ArrayList<String>();
		
		lines.clear();
		MStringAUtils.collectLines("1234567890" + nl + "ABCDEFGHIJ" + nl + "abcdefghij", lines);
		assertArrayEquals(new String[] { "1234567890", "ABCDEFGHIJ", "abcdefghij" },
				lines.toArray() );
		
		lines.clear();
		MStringAUtils.collectLines("1234567890" + nl + nl + "abcdefghij", lines);
		assertArrayEquals(new String[] { "1234567890", "", "abcdefghij" },
				lines.toArray() );
		
		lines.clear();
		MStringAUtils.collectLines("" + nl + "ABCDEFGHIJ" + nl + "abcdefghij", lines);
		assertArrayEquals(new String[] { "", "ABCDEFGHIJ", "abcdefghij" },
				lines.toArray() );
		
		lines.clear();
		MStringAUtils.collectLines("1234567890" + nl + "ABCDEFGHIJ" + nl + "", lines);
		assertArrayEquals(new String[] { "1234567890", "ABCDEFGHIJ" },
				lines.toArray() );
	}
	
	@Test
	public void collectLines_empty() {
		final var lines= new ArrayList<String>();
		
		lines.clear();
		MStringAUtils.collectLines("", lines);
		assertArrayEquals(lines.toArray(),
				new String[] { } );
	}
	
	@Test
	@SuppressWarnings("null")
	public void collectLines_argCheck() {
		assertThrows(NullPointerException.class, () -> {
				MStringAUtils.collectLines(null, new ArrayList<>());
		});
	}
	
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void stripLineStart(final String nl) {
		final StringBuilder sb= new StringBuilder();
		
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb.append("    "), 0);
		assertEquals("    ", sb.toString());
		
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb, 4);
		assertEquals("", sb.toString());
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb.append("    "), 4);
		assertEquals("", sb.toString());
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb.append("    A1"), 4);
		assertEquals("A1", sb.toString());
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb.append("     A1"), 4);
		assertEquals(" A1", sb.toString());
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb.append("    " + nl), 4);
		assertEquals("" + nl, sb.toString());
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb.append("    A1" + nl + "        " + nl), 4);
		assertEquals("A1" + nl + "    " + nl, sb.toString());
		
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb.append("1" + nl), 1);
		assertEquals("" + nl, sb.toString());
		sb.setLength(0);
		MStringAUtils.stripLineStart(sb.append("1" + nl + "1234" + nl), 1);
		assertEquals(nl + "234" + nl, sb.toString());
	}
	
	@Test
	@SuppressWarnings("null")
	public void appendWithLineStart_argCheck() {
		final StringBuilder sb= new StringBuilder();
		
		assertThrows(NullPointerException.class, () -> {
				MStringAUtils.appendWithLineStart(sb, null, "PREFIX", true);
		});
		assertThrows(NullPointerException.class, () -> {
				MStringAUtils.appendWithLineStart(sb, "", null, true);
		});
	}
	
	@ParameterizedTest
	@MethodSource("lineBreaks")
	public void appendWithLineStart(final String nl) {
		final StringBuilder sb= new StringBuilder();
		
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "", "", true);
		assertEquals("", sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "", "PREFIX", true);
		assertEquals("", sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "line1", "PREFIX", true);
		assertEquals("PREFIXline1", sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, nl, "PREFIX", true);
		assertEquals("PREFIX" + nl, sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "line1" + nl, "PREFIX", true);
		assertEquals("PREFIXline1" + nl, sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "line1" + nl + "line2", "PREFIX", true);
		assertEquals("PREFIXline1" + nl + "PREFIXline2", sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "line1" + nl + "line2" + nl, "PREFIX", true);
		assertEquals("PREFIXline1" + nl + "PREFIXline2" + nl, sb.toString());
		
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "", "", false);
		assertEquals("", sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "", "PREFIX", false);
		assertEquals("", sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "line1", "PREFIX", false);
		assertEquals("line1", sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, nl, "PREFIX", false);
		assertEquals(nl, sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "line1" + nl, "PREFIX", false);
		assertEquals("line1" + nl, sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "line1" + nl + "line2", "PREFIX", false);
		assertEquals("line1" + nl + "PREFIXline2", sb.toString());
		sb.setLength(0);
		MStringAUtils.appendWithLineStart(sb, "line1" + nl + "line2" + nl, "PREFIX", false);
		assertEquals("line1" + nl + "PREFIXline2" + nl, sb.toString());
	}
	
	
}
