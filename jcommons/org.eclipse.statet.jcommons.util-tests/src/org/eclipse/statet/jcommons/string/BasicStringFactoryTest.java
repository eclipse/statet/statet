/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class BasicStringFactoryTest {
	
	
	protected StringFactory factory= nonNullLateInit();
	
	
	public BasicStringFactoryTest() {
	}
	
	
	protected StringFactory create() {
		return BasicStringFactory.INSTANCE;
	}
	
	
	@Test
	public void get_CharSequence_emtpy() {
		this.factory= create();
		String s;
		s= this.factory.get(new StringBuilder(""));
		assertSame("", s);
	}
	
	@Test
	public void get_CharSequence_latinChar() {
		this.factory= create();
		String s;
		s= this.factory.get(new StringBuilder().append('a'));
		assertSame("a", s);
		s= this.factory.get(new StringBuilder().append(new char[] { 0x00BB }));
		assertSame("\u00BB", s);
	}
	
	@Test
	public void get_CharSequence_latinPair() {
		this.factory= create();
		String s;
		s= this.factory.get(new StringBuilder().append(new char[] { 'a', 'b' }));
		assertSame("ab", s);
		s= this.factory.get(new StringBuilder().append(new char[] { '!', 'ö' }));
		assertSame("!ö", s);
	}
	
	@Test
	public void get_CharSequence_otherPair() {
		this.factory= create();
		String s;
		s= this.factory.get(new StringBuilder().append(new char[] { 'a', 0x0100 }));
		assertEquals("a\u0100", s);
		s= this.factory.get(new StringBuilder().append(new char[] { 0x2E31, 'ö' }));
		assertEquals("\u2E31ö", s);
	}
	
	@Test
	public void get_CharSequence_other() {
		this.factory= create();
		String s;
		s= this.factory.get(new StringBuilder().append(new char[] { 'a', 'b', 'c' }));
		assertEquals("abc", s);
		s= this.factory.get(new StringBuilder().append(new char[] { 0, 0x2E31, 'ö' }));
		assertEquals("\u0000\u2E31ö", s);
	}
	
	
	@Test
	public void get_CharArrayString_emtpy() {
		this.factory= create();
		String s;
		s= this.factory.get(new CharArrayString(""));
		assertSame("", s);
	}
	
	@Test
	public void get_CharArrayString_latinChar() {
		this.factory= create();
		String s;
		s= this.factory.get(new CharArrayString(new char[] { 'a' }));
		assertSame("a", s);
		s= this.factory.get(new CharArrayString(new char[] { 0x00BB }));
		assertSame("\u00BB", s);
	}
	
	@Test
	public void get_CharArrayString_latinPair() {
		this.factory= create();
		String s;
		s= this.factory.get(new CharArrayString(new char[] { 'a', 'b' }));
		assertSame("ab", s);
		s= this.factory.get(new CharArrayString(new char[] { '!', 'ö' }));
		assertSame("!ö", s);
	}
	
	@Test
	public void get_CharArrayString_otherPair() {
		this.factory= create();
		String s;
		s= this.factory.get(new CharArrayString(new char[] { 'a', 0x0100 }));
		assertEquals("a\u0100", s);
		s= this.factory.get(new CharArrayString(new char[] { 0x2E31, 'ö' }));
		assertEquals("\u2E31ö", s);
	}
	
	@Test
	public void get_CharArrayString_other() {
		this.factory= create();
		String s;
		s= this.factory.get(new CharArrayString(new char[] { 'a', 'b', 'c' }));
		assertEquals("abc", s);
		s= this.factory.get(new CharArrayString(new char[] { 0, 0x2E31, 'ö' }));
		assertEquals("\u0000\u2E31ö", s);
	}
	
	
	@Test
	public void get_String_emtpy() {
		this.factory= create();
		String s;
		s= this.factory.get(new String());
		assertSame("", s);
	}
	
	@Test
	public void get_String_latinChar() {
		this.factory= create();
		String s;
		s= this.factory.get(new String(new char[] { 'a' }));
		assertSame("a", s);
		s= this.factory.get(new String(new char[] { 0x00BB }));
		assertSame("\u00BB", s);
	}
	
	@Test
	public void get_String_otherChar() {
		this.factory= create();
		String s;
		s= this.factory.get(new String(new char[] { 0x0100 }));
		assertEquals("\u0100", s);
		s= this.factory.get(new String(new char[] { 0x2E31 }));
		assertEquals("\u2E31", s);
	}
	
	@Test
	public void get_String_latinPair() {
		this.factory= create();
		String s;
		s= this.factory.get(new String(new char[] { 'a', 'b' }));
		assertSame("ab", s);
		s= this.factory.get(new String(new char[] { '!', 'ö' }));
		assertSame("!ö", s);
	}
	
	@Test
	public void get_String_otherPair() {
		this.factory= create();
		String s;
		s= this.factory.get(new String(new char[] { 'a', 0x0100 }));
		assertEquals("a\u0100", s);
		s= this.factory.get(new String(new char[] { 0x2E31, 'ö' }));
		assertEquals("\u2E31ö", s);
	}
	
	@Test
	public void get_String_other() {
		this.factory= create();
		String s;
		s= this.factory.get(new String(new char[] { 'a', 'b', 'c' }));
		assertEquals("abc", s);
		s= this.factory.get(new String(new char[] { 0, 0x2E31, 'ö' }));
		assertEquals("\u0000\u2E31ö", s);
	}
	
	
	@Test
	public void get_Char_latinChar() {
		this.factory= create();
		String s;
		s= this.factory.get('a');
		assertSame("a", s);
		s= this.factory.get('ö');
		assertSame("ö", s);
	}
	
	@Test
	public void get_Char_otherChar() {
		this.factory= create();
		String s;
		s= this.factory.get((char)0x0100);
		assertEquals("\u0100", s);
		s= this.factory.get((char)0x2E31);
		assertEquals("\u2E31", s);
	}
	
	
	@Test
	public void get_Codepoint_latinChar() {
		this.factory= create();
		String s;
		s= this.factory.get((int)'a');
		assertSame("a", s);
		s= this.factory.get((int)'ö');
		assertSame("ö", s);
	}
	
	@Test
	public void get_Codepoint_otherChar() {
		this.factory= create();
		String s;
		s= this.factory.get(0x0100);
		assertEquals("\u0100", s);
		s= this.factory.get(0x2E31);
		assertEquals("\u2E31", s);
	}
	
}
