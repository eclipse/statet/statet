/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CachedStringFactoryTest extends BasicStringFactoryTest {
	
	
	@Override
	protected StringFactory create() {
		return new CacheStringFactory(10);
	}
	
	
	@Test
	public void get_CharSequence_otherChar_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new StringBuilder().append((char)0x0100));
		assertSame(s, this.factory.get("\u0100"));
		s= this.factory.get(new StringBuilder().append((char)0x2E31));
		assertSame(s, this.factory.get("\u2E31"));
	}
	
	@Test
	public void get_CharSequence_otherPair_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new StringBuilder().append(new char[] { 'a', 'b' }));
		assertSame(s, this.factory.get("ab"));
		s= this.factory.get(new StringBuilder().append(new char[] { '!', 'ö' }));
		assertSame(s, this.factory.get("!ö"));
	}
	
	@Test
	public void get_CharSequence_other_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new StringBuilder().append(new char[] { 'a', 'b', 'c' }));
		assertSame(s, this.factory.get(new StringBuilder("abc")));
		s= this.factory.get(new StringBuilder().append(new char[] { 0, 0x2E31, 'ö' }));
		assertSame(s, this.factory.get(new StringBuilder("\u0000\u2E31ö")));
	}
	
	
	@Test
	public void get_CharArrayString_otherChar_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new CharArrayString(new char[] { 0x0100 }));
		assertSame(s, this.factory.get("\u0100"));
		s= this.factory.get(new CharArrayString(new char[] { 0x2E31 }));
		assertSame(s, this.factory.get("\u2E31"));
	}
	
	@Test
	public void get_CharArrayString_otherPair_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new CharArrayString(new char[] { 'a', 'b' }));
		assertSame(s, this.factory.get("ab"));
		s= this.factory.get(new CharArrayString(new char[] { '!', 'ö' }));
		assertSame(s, this.factory.get("!ö"));
	}
	
	@Test
	public void get_CharArrayString_other_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new CharArrayString(new char[] { 'a', 'b', 'c' }));
		assertSame(s, this.factory.get(new CharArrayString("abc")));
		s= this.factory.get(new CharArrayString(new char[] { 0, 0x2E31, 'ö' }));
		assertSame(s, this.factory.get("\u0000\u2E31ö"));
	}
	
	
	@Test
	public void get_String_otherChar_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new String(new char[] { 0x0100 }));
		assertSame(s, this.factory.get("\u0100"));
		s= this.factory.get(new String(new char[] { 0x2E31 }));
		assertSame(s, this.factory.get("\u2E31"));
	}
	
	@Test
	public void get_String_otherPair_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new String(new char[] { 'a', 'b' }));
		assertSame(s, this.factory.get("ab"));
		s= this.factory.get(new String(new char[] { '!', 'ö' }));
		assertSame(s, this.factory.get("!ö"));
	}
	
	@Test
	public void get_String_other_cached() {
		this.factory= create();
		String s;
		s= this.factory.get(new String(new char[] { 'a', 'b', 'c' }));
		assertSame(s, this.factory.get(new String("abc")));
		s= this.factory.get(new String(new char[] { 0, 0x2E31, 'ö' }));
		assertSame(s, this.factory.get("\u0000\u2E31ö"));
	}
	
}
