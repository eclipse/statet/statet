/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.REMOTE_HOST_NAME/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.net;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.net.CommonsNet.LOCAL_LOOPBACK_STRING;
import static org.eclipse.statet.jcommons.net.CommonsNet.LOOPBACK_IPV4;
import static org.eclipse.statet.jcommons.net.CommonsNet.LOOPBACK_IPV6;
import static org.eclipse.statet.jcommons.net.CommonsNet.LOOPBACK_IPV6_SHORT;
import static org.eclipse.statet.jcommons.rmi.RMIAddress.PARSE_REGISTRY;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.util.List;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.rmi.RMIAddress;


@NonNullByDefault
public class RMIAddressTest {
	
	
	private static final InetAddress LOCAL_HOST_INET_ADDRESS= CommonsNet.getLocalLoopbackInetAddress();
	private static final String LOCAL_HOST_NAME= LOCAL_LOOPBACK_STRING;
	private static final String REMOTE_HOST_NAME= "www.eclipse.org";
	private static final String DEFAULT_HOST_NAME= LOOPBACK_IPV4;
	private static final String LOOPBACK_IPV6_B= '[' + LOOPBACK_IPV6 + ']';
	private static final Port DEFAULT_PORT= RMIAddress.DEFAULT_PORT;
	private static final int DEFAULT_PORT_NUM= DEFAULT_PORT.get();
	
	
	private static List<String> woSec(final String s0) {
		return List.of("rmi:" + s0, s0);
	}
	
	private static List<String> woSec(final String s0, final String s1) {
		return List.of("rmi:" + s0, s0, "rmi:" + s1, s1);
	}
	
	
	public RMIAddressTest() {
	}
	
	
	@Test
	public void new_String_Int_String() throws Exception {
		RMIAddress address;
		
		address= new RMIAddress("localhost", -1, "test1");
		assertRegularRMIAddress("localhost", DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
		
		address= new RMIAddress("localhost", 123, "test1");
		assertRegularRMIAddress("localhost", 123, "test1", address);
		assertResolved("localhost", address);
		
		address= new RMIAddress(REMOTE_HOST_NAME, 123, "test1");
		assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "test1", address);
		assertResolved(REMOTE_HOST_NAME, address);
		
		address= new RMIAddress(LOOPBACK_IPV4, -1, "test1");
		assertRegularRMIAddress(LOOPBACK_IPV4, DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
		
		address= new RMIAddress(LOOPBACK_IPV6, -1, "test1");
		assertRegularRMIAddress(LOOPBACK_IPV6_B, DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
		address= new RMIAddress(LOOPBACK_IPV6_B, -1, "test1");
		assertRegularRMIAddress(LOOPBACK_IPV6_B, DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
		address= new RMIAddress(LOOPBACK_IPV6_SHORT, -1, "test1");
		assertRegularRMIAddress('[' + LOOPBACK_IPV6_SHORT + ']', DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
		
		address= new RMIAddress(null, -1, "test1");
		assertRegularRMIAddress(DEFAULT_HOST_NAME, DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
		
		address= new RMIAddress("localhost", -1, null);
		assertRegularRMIAddress("localhost", DEFAULT_PORT_NUM, "", address);
		assertResolved("localhost", address);
		
		address= new RMIAddress(null, 123, null);
		assertRegularRMIAddress(DEFAULT_HOST_NAME, 123, "", address);
		assertResolved("localhost", address);
	}
	
	@Test
	@SuppressWarnings("null")
	public void new_InetAddress_Port_checkArgs() throws Exception {
		assertThrows(NullPointerException.class, () -> {
			new RMIAddress(null, null, null, "test1");
		});
		assertThrows(NullPointerException.class, () -> {
			new RMIAddress(null, new Port(123), null, null);
		});
	}
	
	@Test
	@SuppressWarnings("null")
	public void new_InetAddress_Port() throws Exception {
		RMIAddress address;
		
		address= new RMIAddress(null, RMIAddress.DEFAULT_PORT, null, "test1");
		assertRegularRMIAddress(LOOPBACK_IPV4, DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
	}
	
	@Test
	@SuppressWarnings("null")
	public void new_RMIAddress_String_checkArgs() throws Exception {
		RMIAddress registry;
		
		registry= new RMIAddress("localhost", -1, "");
		assertThrows(NullPointerException.class, () -> {
			new RMIAddress(null, "test1");
		});
		assertThrows(NullPointerException.class, () -> {
			new RMIAddress(registry, null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			new RMIAddress(registry, "test?1");
		});
	}
	
	@Test
	public void new_RMIAddress_String() throws Exception {
		RMIAddress registry, address;
		
		registry= new RMIAddress("localhost", -1, "");
		address= new RMIAddress(registry, "test1");
		assertRegularRMIAddress("localhost", DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
		
		registry= new RMIAddress("localhost", 123, "");
		address= new RMIAddress(registry, "test1");
		assertRegularRMIAddress("localhost", 123, "test1", address);
		assertResolved("localhost", address);
		
		registry= new RMIAddress(REMOTE_HOST_NAME, 123, "");
		address= new RMIAddress(registry, "test1");
		assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "test1", address);
		assertResolved(REMOTE_HOST_NAME, address);
		
		registry= new RMIAddress(LOOPBACK_IPV4, -1, "");
		address= new RMIAddress(registry, "test1");
		assertRegularRMIAddress(LOOPBACK_IPV4, DEFAULT_PORT_NUM, "test1", address);
		assertResolved("localhost", address);
		
		registry= new RMIAddress(LOCAL_HOST_INET_ADDRESS, DEFAULT_PORT, RMIAddress.REGISTRY_NAME);
		address= new RMIAddress(registry, "");
		assertRegularRMIAddress(LOCAL_HOST_NAME, DEFAULT_PORT_NUM, "", address);
		assertResolved(LOCAL_HOST_INET_ADDRESS, address);
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void parse_String_checkArgs() throws Exception {
		assertThrows(NullPointerException.class, () -> {
			RMIAddress.parse((String)null);
		});
		assertThrows(MalformedURLException.class, () -> {
			RMIAddress.parse("//[..1]/test1");
		});
		assertThrows(MalformedURLException.class, () -> {
			RMIAddress.parse("//[::1/test1");
		});
		assertThrows(MalformedURLException.class, () -> {
			RMIAddress.parse("//[::1]:a/test1");
		});
		assertThrows(MalformedURLException.class, () -> {
			RMIAddress.parse("//[::1]123/test1");
		});
		assertThrows(MalformedURLException.class, () -> {
			RMIAddress.parse("//[::1]/:123/test1");
		});
		assertThrows(MalformedURLException.class, () -> {
			RMIAddress.parse("//127.0.0.1/:123/test1");
		});
	}
	
	@Test
	public void parse_String() throws Exception {
		RMIAddress address;
		
		for (final String s : woSec("//localhost/test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("localhost", DEFAULT_PORT_NUM, "test1", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//localhost:123/test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("localhost", 123, "test1", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//" + REMOTE_HOST_NAME + ":123/test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "test1", address);
			assertResolved(REMOTE_HOST_NAME, address);
		}
		
		for (final String s : woSec("//127.0.0.1/test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("127.0.0.1", DEFAULT_PORT_NUM, "test1", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//[0:0:0:0:0:0:0:1]/test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("[0:0:0:0:0:0:0:1]", DEFAULT_PORT_NUM, "test1", address);
			assertResolved("localhost", address);
		}
		for (final String s : woSec("//[::1]/test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("[::1]", DEFAULT_PORT_NUM, "test1", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("/test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("127.0.0.1", DEFAULT_PORT_NUM, "test1", address);
		}
		for (final String s : woSec("///test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("127.0.0.1", DEFAULT_PORT_NUM, "test1", address);
		}
		
		for (final String s : woSec("//:123/test1")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("127.0.0.1", 123, "test1", address);
			assertResolved("localhost", address);
		}
	}
	
	@Test
	public void parse_String_caseRegistryAddress() throws Exception {
		RMIAddress address;
		
		for (final String s : woSec("//localhost/")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("localhost", DEFAULT_PORT_NUM, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//localhost:123/")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("localhost", 123, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//" + REMOTE_HOST_NAME + ":123/")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "", address);
			assertResolved(REMOTE_HOST_NAME, address);
		}
		
		for (final String s : woSec("//127.0.0.1:123/")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("127.0.0.1", 123, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//[::1]:123/")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress("[::1]", 123, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//:123/")) {
			address= RMIAddress.parse(s);
			assertRegularRMIAddress(DEFAULT_HOST_NAME, 123, "", address);
			assertResolved("localhost", address);
		}
	}
	
	@Test
	public void parseUnresolved_String() throws Exception {
		RMIAddress address;
		
		for (final String s: woSec("//" + REMOTE_HOST_NAME + "/test1")) {
			address= RMIAddress.parseUnresolved(s);
			assertRegularRMIAddress(REMOTE_HOST_NAME, DEFAULT_PORT_NUM, "test1", address);
			assertUnresolved(address);
		}
		
		for (final String s: woSec("//" + REMOTE_HOST_NAME + ":123/test1")) {
			address= RMIAddress.parseUnresolved(s);
			assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "test1", address);
			assertUnresolved(address);
		}
		
		for (final String s: woSec("//" + REMOTE_HOST_NAME + ":123/")) {
			address= RMIAddress.parseUnresolved(s);
			assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "", address);
			assertUnresolved(address);
		}
		
		for (final String s: woSec("//192.168.13.254:123/test1")) {
			address= RMIAddress.parseUnresolved(s);
			assertRegularRMIAddress("192.168.13.254", 123, "test1", address);
			assertUnresolved(address);
		}
	}
	
	@Test
	public void parse_String_PARSE_REGISTRY() throws Exception {
		RMIAddress address;
		
		for (final String s : woSec("//localhost/", "//localhost/test1")) {
			address= RMIAddress.parse(s, PARSE_REGISTRY);
			assertRegularRMIAddress("localhost", DEFAULT_PORT_NUM, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//localhost:123/", "//localhost:123/test1")) {
			address= RMIAddress.parse(s, PARSE_REGISTRY);
			assertRegularRMIAddress("localhost", 123, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//" + REMOTE_HOST_NAME + ":123/")) {
			address= RMIAddress.parse(s, PARSE_REGISTRY);
			assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "", address);
			assertResolved(REMOTE_HOST_NAME, address);
		}
		
		for (final String s : woSec("//127.0.0.1:123/")) {
			address= RMIAddress.parse(s, PARSE_REGISTRY);
			assertRegularRMIAddress("127.0.0.1", 123, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//:123/")) {
			address= RMIAddress.parse(s, PARSE_REGISTRY);
			assertRegularRMIAddress(DEFAULT_HOST_NAME, 123, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//localhost")) {
			address= RMIAddress.parse(s, PARSE_REGISTRY);
			assertRegularRMIAddress("localhost", DEFAULT_PORT_NUM, "", address);
			assertResolved("localhost", address);
		}
		
		for (final String s : woSec("//" + REMOTE_HOST_NAME)) {
			address= RMIAddress.parse(s, PARSE_REGISTRY);
			assertRegularRMIAddress(REMOTE_HOST_NAME, DEFAULT_PORT_NUM, "", address);
			assertResolved(REMOTE_HOST_NAME, address);
		}
	}
	
	
	@Test
	public void resolve() throws Exception {
		RMIAddress address, resolved;
		
		address= new RMIAddress(REMOTE_HOST_NAME, DEFAULT_PORT_NUM, "test1");
		assertResolved(REMOTE_HOST_NAME, address);
		resolved= address.resolve();
		assertSame(address, resolved);
		
		address= RMIAddress.parseUnresolved("rmi://" + REMOTE_HOST_NAME + "/test1");
		assertUnresolved(address);
		resolved= address.resolve();
		assertRegularRMIAddress(REMOTE_HOST_NAME, DEFAULT_PORT_NUM, "test1", resolved);
		assertResolved(REMOTE_HOST_NAME, resolved);
	}
	
	
	@Test
	public void isLocalHost() throws Exception {
		RMIAddress address;
		
		address= new RMIAddress(LOCAL_HOST_INET_ADDRESS, DEFAULT_PORT, "test1");
		assertTrue(address.isLocalHost());
		address= RMIAddress.parse("rmi://localhost/");
		assertTrue(address.isLocalHost());
		address= new RMIAddress("127.0.0.1", 123, "test1");
		assertTrue(address.isLocalHost());
		address= new RMIAddress(null, -1, "test1");
		assertTrue(address.isLocalHost());
		
		address= RMIAddress.parse("rmi://" + REMOTE_HOST_NAME + "/test1");
		assertFalse(address.isLocalHost());
		address= RMIAddress.parse("rmi://" + REMOTE_HOST_NAME + "/");
		assertFalse(address.isLocalHost());
	}
	
	@Test
	public void getRegistryAddress() throws Exception {
		RMIAddress address, registry;
		
		address= new RMIAddress(LOCAL_HOST_INET_ADDRESS, DEFAULT_PORT, "test1");
		registry= address.getRegistryAddress();
		assertRegularRMIAddress(LOCAL_HOST_NAME, DEFAULT_PORT_NUM, "", registry);
		assertResolved(LOCAL_HOST_INET_ADDRESS, registry);
		assertEquals(registry, registry.getRegistryAddress());
		
		address= RMIAddress.parse("rmi://" + REMOTE_HOST_NAME + ":123/test1");
		registry= address.getRegistryAddress();
		assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "", registry);
		assertResolved(REMOTE_HOST_NAME, registry);
		assertEquals(registry, registry.getRegistryAddress());
		
		address= RMIAddress.parseUnresolved("rmi://" + REMOTE_HOST_NAME + ":123/test1");
		registry= address.getRegistryAddress();
		assertRegularRMIAddress(REMOTE_HOST_NAME, 123, "", registry);
		assertUnresolved(registry);
		assertEquals(registry, registry.getRegistryAddress());
	}
	
	@Test
	public void getAddress() throws Exception {
		RMIAddress address;
		
		address= new RMIAddress(LOCAL_HOST_INET_ADDRESS, DEFAULT_PORT, "test1");
		assertEquals("rmi://" + LOCAL_HOST_NAME + "/test1", address.getAddress());
		
		address= new RMIAddress(null, DEFAULT_PORT, "test1");
		assertEquals("rmi:///test1", address.getAddress());
		
		address= new RMIAddress(LOOPBACK_IPV6, -1, "test1");
		assertEquals("rmi://" + LOOPBACK_IPV6_B + "/test1", address.getAddress());
		
		address= RMIAddress.parse("rmi://" + REMOTE_HOST_NAME + ":123/test1");
		assertEquals("rmi://" + REMOTE_HOST_NAME + ":123/test1", address.getAddress());
		
		address= RMIAddress.parseUnresolved("rmi://" + REMOTE_HOST_NAME + ":123/test1");
		assertEquals("rmi://" + REMOTE_HOST_NAME + ":123/test1", address.getAddress());
	}
	
	
	@Test
	@SuppressWarnings("unlikely-arg-type")
	public void equals() throws Exception {
		final var inetAddress= LOCAL_HOST_INET_ADDRESS;
		final var port= RMIAddress.DEFAULT_PORT;
		final var serviceName= "test1";
		final RMIAddress address= new RMIAddress(inetAddress, port, serviceName);
		assertTrue(address.equals(address));
		assertTrue(address.equals(new RMIAddress(inetAddress, port, "test1")));
		assertTrue(address.equals(new RMIAddress(inetAddress.getHostAddress(), -1, "test1")));
		assertTrue(address.equals(RMIAddress.parse("//127.0.0.1/test1")));
		assertFalse(address.equals("//127.0.0.1/test1"));
		assertFalse(address.equals("//localhost/test1"));
		assertFalse(address.equals(new RMIAddress(inetAddress, new Port(123), serviceName)));
		assertFalse(address.equals(new RMIAddress(inetAddress, port, RMIAddress.REGISTRY_NAME)));
		assertFalse(address.equals(new RMIAddress(inetAddress, port, "different")));
		assertFalse(address.equals(new RMIAddress("eclipse.org", -1, serviceName)));
		assertFalse(address.equals(new RMIAddress(inetAddress, port, RMIAddress.SSL, serviceName)));
	}
	
	@Test
	public void toString_andSerialization() throws Exception {
		RMIAddress address;
		String s;
		
		address= new RMIAddress(LOCAL_HOST_INET_ADDRESS, DEFAULT_PORT, "test1");
		s= address.toString();
		assertEquals("rmi://" + LOCAL_HOST_NAME + "/test1", s);
		assertEquals(address, RMIAddress.parse(s));
		
		address= new RMIAddress(null, DEFAULT_PORT, "test1");
		s= address.toString();
		assertEquals("rmi:///test1", s);
		assertEquals(address, RMIAddress.parse(s));
		
		address= new RMIAddress(LOOPBACK_IPV6, -1, "test1");
		s= address.toString();
		assertEquals("rmi://" + LOOPBACK_IPV6_B + "/test1", s);
		assertEquals(address, RMIAddress.parse(s));
		
		address= RMIAddress.parse("rmi://" + REMOTE_HOST_NAME + ":123/test1");
		s= address.toString();
		assertEquals("rmi://" + REMOTE_HOST_NAME + ":123/test1", s);
		assertEquals(address, RMIAddress.parse(s));
		
		address= RMIAddress.parseUnresolved("rmi://" + REMOTE_HOST_NAME + ":123/test1");
		s= address.toString();
		assertEquals("rmi://" + REMOTE_HOST_NAME + ":123/test1", s);
		assertEquals(address, RMIAddress.parseUnresolved(s));
	}
	
	
	protected void assertRegularRMIAddress(final String expectedHost, final int expectedPort,
			final String expectedServiceName,
			final RMIAddress actual) {
		assertNotNull(actual);
		assertEquals(expectedHost, actual.getHost());
		assertEquals(new Port(expectedPort), actual.getPort());
		assertEquals(expectedPort, actual.getPortNum());
		assertEquals(expectedServiceName, actual.getName());
		assertNull(actual.getSec());
	}
	
	protected void assertResolved(final String expectedHostName, final RMIAddress actual) {
		assertTrue(actual.isResolved());
		final InetAddress hostInetAddress= actual.getHostInetAddress();
		assertNotNull(hostInetAddress);
		assertEquals(expectedHostName, hostInetAddress.getHostName());
	}
	
	protected void assertResolved(final InetAddress expectedInetAddress, final RMIAddress actual) {
		assertTrue(actual.isResolved());
		final InetAddress hostInetAddress= actual.getHostInetAddress();
		assertNotNull(hostInetAddress);
		assertEquals(expectedInetAddress, hostInetAddress);
	}
	
	protected void assertUnresolved(final RMIAddress actual) {
		assertFalse(actual.isResolved());
		assertThrows(UnsupportedOperationException.class, () -> {
			actual.getHostInetAddress();
		});
	}
	
}
