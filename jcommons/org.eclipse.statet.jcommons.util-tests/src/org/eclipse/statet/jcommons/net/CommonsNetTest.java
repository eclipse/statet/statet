/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.net;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CommonsNetTest {
	
	
	public CommonsNetTest() {
	}
	
	
	@Test
	public void isIpV4Address() throws URISyntaxException {
		assertTrue(CommonsNet.isIpV4Address("0.0.0.0"));
		assertTrue(CommonsNet.isIpV4Address("1.1.1.1"));
		assertTrue(CommonsNet.isIpV4Address("10.10.10.10"));
		assertTrue(CommonsNet.isIpV4Address("100.100.100.100"));
		assertTrue(CommonsNet.isIpV4Address("192.168.1.1"));
		assertTrue(CommonsNet.isIpV4Address("255.255.255.255"));
		assertTrue(CommonsNet.isIpV4Address("255.0.0.0"));
		
		assertFalse(CommonsNet.isIpV4Address(""));
		assertFalse(CommonsNet.isIpV4Address("."));
		assertFalse(CommonsNet.isIpV4Address("..."));
		assertFalse(CommonsNet.isIpV4Address("1"));
		assertFalse(CommonsNet.isIpV4Address("1.1"));
		assertFalse(CommonsNet.isIpV4Address("1.1.1"));
		assertFalse(CommonsNet.isIpV4Address(".1.1.1"));
		assertFalse(CommonsNet.isIpV4Address("1.1.1.1."));
		assertFalse(CommonsNet.isIpV4Address("1.1.1.1.1"));
		assertFalse(CommonsNet.isIpV4Address("01.1.1.1"));
		assertFalse(CommonsNet.isIpV4Address("1.1.1.01"));
		assertFalse(CommonsNet.isIpV4Address("1.1.1.011"));
		assertFalse(CommonsNet.isIpV4Address("1.1.1.0255"));
		assertFalse(CommonsNet.isIpV4Address("255.255.255.256"));
		assertFalse(CommonsNet.isIpV4Address("255.255.255.-1"));
		assertFalse(CommonsNet.isIpV4Address("255.255.255.AA"));
		assertFalse(CommonsNet.isIpV4Address(" "));
		assertFalse(CommonsNet.isIpV4Address(" 1.1.1.1"));
		assertFalse(CommonsNet.isIpV4Address("1.1.1.1 "));
		assertFalse(CommonsNet.isIpV4Address("1 .1.1.1"));
		assertFalse(CommonsNet.isIpV4Address("1. 1.1.1"));
	}
	
	@Test
	public void isIpV6Address() throws URISyntaxException {
		assertTrue(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e:0370:7347"));
		assertTrue(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e:0370::"));
		assertTrue(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e::7347"));
		assertTrue(CommonsNet.isIpV6Address("1::1"));
		assertTrue(CommonsNet.isIpV6Address("::1"));
		assertTrue(CommonsNet.isIpV6Address("::"));
		assertTrue(CommonsNet.isIpV6Address("[2001:0db8:85a3:08d3:1319:8a2e:0370:7347]"));
		assertTrue(CommonsNet.isIpV6Address("[1::1]"));
		assertTrue(CommonsNet.isIpV6Address("[::]"));
		
		assertFalse(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e:0370:734G"));
		assertFalse(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e:0370:"));
		assertFalse(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e:0370"));
		assertFalse(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e:::7347"));
		assertFalse(CommonsNet.isIpV6Address("0.0.0.0"));
		assertFalse(CommonsNet.isIpV6Address(""));
		assertFalse(CommonsNet.isIpV6Address(":"));
		assertFalse(CommonsNet.isIpV6Address("1"));
		assertFalse(CommonsNet.isIpV6Address("1:1"));
		assertFalse(CommonsNet.isIpV6Address("a:b"));
		assertFalse(CommonsNet.isIpV6Address("a.b"));
		assertFalse(CommonsNet.isIpV6Address("x::1"));
		assertFalse(CommonsNet.isIpV6Address("0x::1"));
		assertFalse(CommonsNet.isIpV6Address(" "));
		assertFalse(CommonsNet.isIpV6Address(" 2001:0db8:85a3:08d3:1319:8a2e:0370:7347"));
		assertFalse(CommonsNet.isIpV6Address("2001 :0db8:85a3:08d3:1319:8a2e:0370:7347"));
		assertFalse(CommonsNet.isIpV6Address("2001: 0db8:85a3:08d3:1319:8a2e:0370:7347"));
		assertFalse(CommonsNet.isIpV6Address("2001:0d b8:85a3:08d3:1319:8a2e:0370:7347"));
		assertFalse(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e:0370:7347 "));
		assertFalse(CommonsNet.isIpV6Address("[a:b]"));
		assertFalse(CommonsNet.isIpV6Address("[a.b]"));
		assertFalse(CommonsNet.isIpV6Address("[x::1]"));
		assertFalse(CommonsNet.isIpV6Address("[0x::1]"));
		assertFalse(CommonsNet.isIpV6Address("[2001:0db8:85a3:08d3:1319:8a2e:0370:7347"));
		assertFalse(CommonsNet.isIpV6Address("2001:0db8:85a3:08d3:1319:8a2e:0370:7347]"));
	}
	
	@Test
	@SuppressWarnings("null")
	public void getDefaultPort_argCheck() throws UnknownSchemeException {
		assertThrows(NullPointerException.class, () -> {
			CommonsNet.getDefaultPort(null);
		});
		assertThrows(UnknownSchemeException.class, () -> {
			CommonsNet.getDefaultPort("unknown");
		});
	}
	
	@Test
	public void getDefaultPort() throws UnknownSchemeException {
		assertEquals(new Port(80), CommonsNet.getDefaultPort("http"));
		assertEquals(new Port(443), CommonsNet.getDefaultPort("https"));
	}
	
	@Test
	@SuppressWarnings("null")
	public void getDefaultPortNum_argCheck() throws UnknownSchemeException {
		assertThrows(NullPointerException.class, () -> {
			CommonsNet.getDefaultPortNum(null);
		});
		assertThrows(UnknownSchemeException.class, () -> {
			CommonsNet.getDefaultPortNum("unknown");
		});
	}
	
	@Test
	public void getDefaultPortNum() throws UnknownSchemeException {
		assertEquals(80, CommonsNet.getDefaultPortNum("http"));
		assertEquals(443, CommonsNet.getDefaultPortNum("https"));
	}
	
	
}
