/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.eclipse.statet.jcommons.collections.CollectionTests.assertListEquals;
import static org.eclipse.statet.jcommons.collections.ImCollections.emptyLongList;
import static org.eclipse.statet.jcommons.collections.ImCollections.newLongList;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ImLongCollectionsOperationsTest {
	
	
	public ImLongCollectionsOperationsTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void List_addElement_atIndex_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ImCollections.addElement((LongList)null, 0, 99999999999L);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ImCollections.addElement(emptyLongList(), -1, 99999999999L);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ImCollections.addElement(emptyLongList(), 1, 99999999999L);
		});
	}
	
	@Test
	public void List_addElement_ImList_atIndex() {
		final ImLongList list0= emptyLongList();
		final ImLongList list1= newLongList(1);
		final ImLongList list2= newLongList(1, 2);
		
		assertListEquals(
				new long[] { 99999999999L },
				ImCollections.addElement(list0, 0, 99999999999L) );
		assertListEquals(
				new long[] { 99999999999L, 1 },
				ImCollections.addElement(list1, 0, 99999999999L) );
		assertListEquals(
				new long[] { 1, 99999999999L },
				ImCollections.addElement(list1, 1, 99999999999L) );
		assertListEquals(
				new long[] { 99999999999L, 1, 2 },
				ImCollections.addElement(list2, 0, 99999999999L) );
		assertListEquals(
				new long[] { 1, 99999999999L, 2 },
				ImCollections.addElement(list2, 1, 99999999999L) );
		assertListEquals(
				new long[] { 1, 2, 99999999999L },
				ImCollections.addElement(list2, 2, 99999999999L) );
	}
	
//	@Test
//	public void List_addElement_ArrayList_atIndex() {
//		final LongArrayList list0= newArrayList();
//		final LongArrayList list1= newArrayList(1);
//		final LongArrayList list2= newArrayList(1, 2);
//		
//		assertListEquals(
//				new long[] { 99999999999L },
//				ImCollections.addElement(list0, 0, 99999999999L) );
//		assertListEquals(
//				new long[] { 99999999999L, 1 },
//				ImCollections.addElement(list1, 0, 99999999999L) );
//		assertListEquals(
//				new long[] { 1, 99999999999L },
//				ImCollections.addElement(list1, 1, 99999999999L) );
//		assertListEquals(
//				new long[] { 99999999999L, 1, 2 },
//				ImCollections.addElement(list2, 0, 99999999999L) );
//		assertListEquals(
//				new long[] { 1, 99999999999L, 2 },
//				ImCollections.addElement(list2, 1, 99999999999L) );
//		assertListEquals(
//				new long[] { 1, 2, 99999999999L },
//				ImCollections.addElement(list2, 2, 99999999999L) );
//	}
	
	@Test
	@SuppressWarnings("null")
	public void List_addElement_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ImCollections.addElement((LongList)null, 99999999999L);
		});
	}
	
	@Test
	public void List_addElement_ImList() {
		final ImLongList list0= emptyLongList();
		final ImLongList list1= newLongList(1);
		final ImLongList list2= newLongList(1, 2);
		
		assertListEquals(
				new long[] { 99999999999L },
				ImCollections.addElement(list0, 99999999999L) );
		assertListEquals(
				new long[] { 1, 99999999999L },
				ImCollections.addElement(list1, 99999999999L) );
		assertListEquals(
				new long[] { 1, 2, 99999999999L },
				ImCollections.addElement(list2, 99999999999L) );
		assertListEquals(
				new long[] { 1, 2, 2 },
				ImCollections.addElement(list2, 2) );
	}
	
//	@Test
//	public void List_addElement_ArrayList() {
//		final LongArrayList list0= newArrayList();
//		final LongArrayList list1= newArrayList(1);
//		final LongArrayList list2= newArrayList(1, 2);
//		
//		assertListEquals(
//				new long[] { 99999999999L },
//				ImCollections.addElement(list0, 99999999999L) );
//		assertListEquals(
//				new long[] { 1, 99999999999L },
//				ImCollections.addElement(list1, 99999999999L) );
//		assertListEquals(
//				new long[] { 1, 2, 99999999999L },
//				ImCollections.addElement(list2, 99999999999L) );
//		assertListEquals(
//				new long[] { 1, 2, 2 },
//				ImCollections.addElement(list2, 2) );
//	}
	
	
}
