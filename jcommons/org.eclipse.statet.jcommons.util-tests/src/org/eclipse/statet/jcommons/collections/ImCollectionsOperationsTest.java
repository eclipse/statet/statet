/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.eclipse.statet.jcommons.collections.CollectionTests.A_notIdentical;
import static org.eclipse.statet.jcommons.collections.CollectionTests.B_notIdentical;
import static org.eclipse.statet.jcommons.collections.CollectionTests.assertListEquals;
import static org.eclipse.statet.jcommons.collections.CollectionTests.assertListEqualsSameEntries;
import static org.eclipse.statet.jcommons.collections.CollectionTests.assertSetEquals;
import static org.eclipse.statet.jcommons.collections.ImCollections.emptyIdentityList;
import static org.eclipse.statet.jcommons.collections.ImCollections.emptyList;
import static org.eclipse.statet.jcommons.collections.ImCollections.emptySet;
import static org.eclipse.statet.jcommons.collections.ImCollections.newIdentityList;
import static org.eclipse.statet.jcommons.collections.ImCollections.newList;
import static org.eclipse.statet.jcommons.collections.ImCollections.newSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImCollectionsOperationsTest {
	
	
	public ImCollectionsOperationsTest() {
	}
	
	
	private static ArrayList<String> newArrayList(final @NonNull String... elements) {
		return new ArrayList<>(Arrays.asList(elements));
	}
	
	
	@Test
	public void List_concatList_2ImList() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list3= newList("A", "B", "C");
		
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.concatList(list0, emptyList()) );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.concatList(list1, emptyList()) );
		assertListEquals(
				new @Nullable String[] { "E" },
				ImCollections.concatList(list0, newList("E")) );
		assertListEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.concatList(list1, newList("E")) );
		assertListEquals(
				new @Nullable String[] { "A", "B", "C" },
				ImCollections.concatList(list3, emptyList()) );
		assertListEquals(
				new @Nullable String[] { "E", "F", "G" },
				ImCollections.concatList(list0, newList("E", "F", "G")) );
		assertListEquals(
				new @Nullable String[] { "A", "B", "C", "E", "F", "G" },
				ImCollections.concatList(list3, newList("E", "F", "G")) );
		
		assertSame(list1, ImCollections.concatList(list1, emptyList()));
		assertSame(list1, ImCollections.concatList(emptyList(), list1));
		assertSame(list3, ImCollections.concatList(list3, emptyList()));
		assertSame(list3, ImCollections.concatList(emptyList(), list3));
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void List_addElement_atIndex_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ImCollections.addElement((List<String>)null, 0, "E");
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ImCollections.addElement(emptyList(), -1, "E");
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ImCollections.addElement(emptyList(), 1, "E");
		});
	}
	
	@Test
	public void List_addElement_ImList_atIndex() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		
		assertListEquals(
				new @Nullable String[] { "E" },
				ImCollections.addElement(list0, 0, "E") );
		assertListEquals(
				new @Nullable String[] { "E", "A" },
				ImCollections.addElement(list1, 0, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElement(list1, 1, "E") );
		assertListEquals(
				new @Nullable String[] { "E", "A", "B" },
				ImCollections.addElement(list2, 0, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "E", "B" },
				ImCollections.addElement(list2, 1, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElement(list2, 2, "E") );
	}
	
	@Test
	public void List_addElement_ArrayList_atIndex() {
		final ArrayList<String> list0= newArrayList();
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		
		assertListEquals(
				new @Nullable String[] { "E" },
				ImCollections.addElement(list0, 0, "E") );
		assertListEquals(
				new @Nullable String[] { "E", "A" },
				ImCollections.addElement(list1, 0, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElement(list1, 1, "E") );
		assertListEquals(
				new @Nullable String[] { "E", "A", "B" },
				ImCollections.addElement(list2, 0, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "E", "B" },
				ImCollections.addElement(list2, 1, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElement(list2, 2, "E") );
	}
	
	@Test
	@SuppressWarnings("null")
	public void List_addElement_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ImCollections.addElement((List<String>)null, "E");
		});
	}
	
	@Test
	public void List_addElement_ImList() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		
		assertListEquals(
				new @Nullable String[] { "E" },
				ImCollections.addElement(list0, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElement(list1, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElement(list2, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "B", "B" },
				ImCollections.addElement(list2, "B") );
	}
	
	@Test
	public void List_addElement_ArrayList() {
		final ArrayList<String> list0= newArrayList();
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		
		assertListEquals(
				new @Nullable String[] { "E" },
				ImCollections.addElement(list0, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElement(list1, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElement(list2, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "B", "B" },
				ImCollections.addElement(list2, "B") );
	}
	
	@Test
	public void List_addElementIfAbsent_ImList() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElementIfAbsent(list0, "A") );
		assertListEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElementIfAbsent(list1, "E") );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElementIfAbsent(list1, "A") );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElementIfAbsent(list1, A_notIdentical) );
		assertListEquals(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElementIfAbsent(list2, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, "B") );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, B_notIdentical) );
		
		assertSame(list1, ImCollections.addElementIfAbsent(list1, "A") );
		assertSame(list2, ImCollections.addElementIfAbsent(list2, "A") );
	}
	
	@Test
	public void List_addElementIfAbsent_ArrayList() {
		final ArrayList<String> list0= newArrayList();
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElementIfAbsent(list0, "A") );
		assertListEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElementIfAbsent(list1, "E") );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElementIfAbsent(list1, "A") );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElementIfAbsent(list1, A_notIdentical) );
		assertListEquals(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElementIfAbsent(list2, "E") );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, "B") );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, B_notIdentical) );
	}
	
	@Test
	public void List_removeElement_ImList() {
		final ImList<String> list0= emptyList();
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		final ImList<String> list3= newList("A", "B", "C");
		
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.removeElement(list0, "A") );
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.removeElement(list1, "A") );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list1, "X") );
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.removeElement(list1, A_notIdentical) );
		assertListEquals(
				new @Nullable String[] { "B" },
				ImCollections.removeElement(list2, "A") );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list2, "B") );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list2, "X") );
		assertListEquals(
				new @Nullable String[] { "A", },
				ImCollections.removeElement(list2, B_notIdentical) );
		assertListEquals(
				new @Nullable String[] { "B", "C" },
				ImCollections.removeElement(list3, "A") );
		assertListEquals(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeElement(list3, "B") );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list3, "C") );
		assertListEquals(
				new @Nullable String[] { "A", "B", "C" },
				ImCollections.removeElement(list3, "X") );
		assertListEquals(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeElement(list3, B_notIdentical) );
	}
	
	@Test
	public void List_removeElement_ArrayList() {
		final ArrayList<String> list0= newArrayList();
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		final ArrayList<String> list3= newArrayList("A", "B", "C");
		
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.removeElement(list0, "A") );
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.removeElement(list1, "A") );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list1, "X") );
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.removeElement(list1, A_notIdentical) );
		assertListEquals(
				new @Nullable String[] { "B" },
				ImCollections.removeElement(list2, "A") );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list2, "B") );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list2, "X") );
		assertListEquals(
				new @Nullable String[] { "A", },
				ImCollections.removeElement(list2, B_notIdentical) );
		assertListEquals(
				new @Nullable String[] { "B", "C" },
				ImCollections.removeElement(list3, "A") );
		assertListEquals(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeElement(list3, "B") );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list3, "C") );
		assertListEquals(
				new @Nullable String[] { "A", "B", "C" },
				ImCollections.removeElement(list3, "X") );
		assertListEquals(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeElement(list3, B_notIdentical) );
	}
	
	@Test
	public void List_removeElement_ImList_atIndex() {
		final ImList<String> list1= newList("A");
		final ImList<String> list2= newList("A", "B");
		final ImList<String> list3= newList("A", "B", "C");
		
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.removeElement(list1, 0) );
		assertListEquals(
				new @Nullable String[] { "B" },
				ImCollections.removeElement(list2, 0) );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list2, 1) );
		assertListEquals(
				new @Nullable String[] { "B", "C" },
				ImCollections.removeElement(list3, 0) );
		assertListEquals(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeElement(list3, 1) );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list3, 2) );
	}
	
	@Test
	public void List_removeElement_ArrayList_atIndex() {
		final ArrayList<String> list1= newArrayList("A");
		final ArrayList<String> list2= newArrayList("A", "B");
		final ArrayList<String> list3= newArrayList("A", "B", "C");
		
		assertListEquals(
				new @Nullable String[] { },
				ImCollections.removeElement(list1, 0) );
		assertListEquals(
				new @Nullable String[] { "B" },
				ImCollections.removeElement(list2, 0) );
		assertListEquals(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list2, 1) );
		assertListEquals(
				new @Nullable String[] { "B", "C" },
				ImCollections.removeElement(list3, 0) );
		assertListEquals(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeElement(list3, 1) );
		assertListEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list3, 2) );
	}
	
	
	@Test
	public void IdentityList_concatList_2ImList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list3= newIdentityList("A", "B", "C");
		
		assertListEqualsSameEntries(
				new @Nullable String[] { },
				ImCollections.concatList(list0, emptyIdentityList()) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.concatList(list1, emptyIdentityList()) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "E" },
				ImCollections.concatList(list0, newIdentityList("E")) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "E" },
				ImCollections.concatList(list1, newIdentityList("E")) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "C" },
				ImCollections.concatList(list3, emptyIdentityList()) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "E", "F", "G" },
				ImCollections.concatList(list0, newIdentityList("E", "F", "G")) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "C", "E", "F", "G" },
				ImCollections.concatList(list3, newIdentityList("E", "F", "G")) );
		
		assertSame(list1, ImCollections.concatList(list1, list0));
		assertSame(list1, ImCollections.concatList(list0, list1));
		assertSame(list3, ImCollections.concatList(list3, list0));
		assertSame(list3, ImCollections.concatList(list0, list3));
	}
	
	@Test
	public void IdentityList_addElement_ImIdentityList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		
		assertListEqualsSameEntries(
				new @Nullable String[] { "E" },
				ImCollections.addElement(list0, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElement(list1, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElement(list2, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "B" },
				ImCollections.addElement(list2, "B") );
	}
	
	@Test
	public void IdentityList_addElement_ImIdentityList_atIndex() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		
		assertListEqualsSameEntries(
				new @Nullable String[] { "E" },
				ImCollections.addElement(list0, 0, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "E", "A" },
				ImCollections.addElement(list1, 0, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElement(list1, 1, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "E", "A", "B" },
				ImCollections.addElement(list2, 0, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "E", "B" },
				ImCollections.addElement(list2, 1, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElement(list2, 2, "E") );
	}
	
	@Test
	public void IdentityList_addElementIfAbsent_ImIdentityList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.addElementIfAbsent(list0, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElementIfAbsent(list1, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.addElementIfAbsent(list1, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", A_notIdentical },
				ImCollections.addElementIfAbsent(list1, A_notIdentical) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElementIfAbsent(list2, "E") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElementIfAbsent(list2, "B") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", B_notIdentical },
				ImCollections.addElementIfAbsent(list2, B_notIdentical) );
		
		assertSame(list1, ImCollections.addElementIfAbsent(list1, "A") );
		assertSame(list2, ImCollections.addElementIfAbsent(list2, "A") );
	}
	
	@Test
	public void IdentityList_removeElement_ImIdentityList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		final ImIdentityList<String> list3= newIdentityList("A", "B", "C");
		
		assertListEqualsSameEntries(
				new @Nullable String[] { },
				ImCollections.removeElement(list0, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { },
				ImCollections.removeElement(list1, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list1, "X") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list1, A_notIdentical) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "B" },
				ImCollections.removeElement(list2, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list2, "B") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list2, "X") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list2, B_notIdentical) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "B", "C" },
				ImCollections.removeElement(list3, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeElement(list3, "B") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list3, "C") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "C" },
				ImCollections.removeElement(list3, "X") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "C" },
				ImCollections.removeElement(list3, B_notIdentical) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "B", "A" },
				ImCollections.removeElement(newIdentityList("A", "B", "A"), "A") );
		
		assertSame(list1, ImCollections.removeElement(list1, "X") );
		assertSame(list2, ImCollections.removeElement(list2, "X") );
		assertSame(list3, ImCollections.removeElement(list3, "X") );
	}
	
	@Test
	public void IdentityList_removeLastElement_ImIdentityList() {
		final ImIdentityList<String> list0= emptyIdentityList();
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		final ImIdentityList<String> list3= newIdentityList("A", "B", "C");
		
		assertListEqualsSameEntries(
				new @Nullable String[] { },
				ImCollections.removeLastElement(list0, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { },
				ImCollections.removeLastElement(list1, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.removeLastElement(list1, "X") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.removeLastElement(list1, A_notIdentical) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "B" },
				ImCollections.removeLastElement(list2, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.removeLastElement(list2, "B") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeLastElement(list2, "X") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeLastElement(list2, B_notIdentical) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "B", "C" },
				ImCollections.removeLastElement(list3, "A") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeLastElement(list3, "B") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeLastElement(list3, "C") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "C" },
				ImCollections.removeLastElement(list3, "X") );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B", "C" },
				ImCollections.removeLastElement(list3, B_notIdentical) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeLastElement(newIdentityList("A", "B", "A"), "A") );
		
		assertSame(list1, ImCollections.removeLastElement(list1, "X") );
		assertSame(list2, ImCollections.removeLastElement(list2, "X") );
		assertSame(list3, ImCollections.removeLastElement(list3, "X") );
	}
	
	@Test
	public void IdentityList_removeElement_ImIdentityList_atIndex() {
		final ImIdentityList<String> list1= newIdentityList("A");
		final ImIdentityList<String> list2= newIdentityList("A", "B");
		final ImIdentityList<String> list3= newIdentityList("A", "B", "C");
		
		assertListEqualsSameEntries(
				new @Nullable String[] { },
				ImCollections.removeElement(list1, 0) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "B" },
				ImCollections.removeElement(list2, 0) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A" },
				ImCollections.removeElement(list2, 1) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "B", "C" },
				ImCollections.removeElement(list3, 0) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "C" },
				ImCollections.removeElement(list3, 1) );
		assertListEqualsSameEntries(
				new @Nullable String[] { "A", "B" },
				ImCollections.removeElement(list3, 2) );
	}
	
	
	@Test
	public void Set_addElement_ImSet() {
		final ImSet<String> list0= emptySet();
		final ImSet<String> list1= newSet("A");
		final ImSet<String> list2= newSet("A", "B");
		
		assertSetEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElement(list0, "A") );
		assertSetEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElement(list1, "E") );
		assertSetEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElement(list1, "A") );
		assertSetEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElement(list1, A_notIdentical) );
		assertSetEquals(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElement(list2, "E") );
		assertSetEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElement(list2, "B") );
		assertSetEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElement(list2, B_notIdentical) );
		
		assertSame(list1, ImCollections.addElement(list1, "A") );
		assertSame(list2, ImCollections.addElement(list2, "A") );
	}
	
	@Test
	public void Set_addElement_ImSeqedSet() {
		final ImSequencedSet<String> list0= emptySet();
		final ImSequencedSet<String> list1= newSet("A");
		final ImSequencedSet<String> list2= newSet("A", "B");
		
		assertSetEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElement(list0, "A") );
		assertSetEquals(
				new @Nullable String[] { "A", "E" },
				ImCollections.addElement(list1, "E") );
		assertSetEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElement(list1, "A") );
		assertSetEquals(
				new @Nullable String[] { "A" },
				ImCollections.addElement(list1, A_notIdentical) );
		assertSetEquals(
				new @Nullable String[] { "A", "B", "E" },
				ImCollections.addElement(list2, "E") );
		assertSetEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElement(list2, "B") );
		assertSetEquals(
				new @Nullable String[] { "A", "B" },
				ImCollections.addElement(list2, B_notIdentical) );
		
		assertSame(list1, ImCollections.addElement(list1, "A") );
		assertSame(list2, ImCollections.addElement(list2, "A") );
	}
	
}
