/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class AbstractImLongListTest {
	
	
	protected static final long VALUE_0= 1654234001111111L;
	
	
	protected ImLongList list= nonNullLateInit();
	
	
	public AbstractImLongListTest() {
	}
	
	
	protected abstract ImLongList createList(long value0);
	
	protected abstract int getExpectedSize();
	
	@BeforeEach
	public void init() {
		this.list= createList(VALUE_0);
	}
	
	
	@Test
	public void size() {
		assertEquals(getExpectedSize(), this.list.size());
	}
	
	@Test
	public void isEmpty() {
		if (getExpectedSize() == 0) {
			assertTrue(this.list.isEmpty());
		}
		else {
			assertFalse(this.list.isEmpty());
		}
	}
	
	@Test
	public void contains() {
		if (getExpectedSize() >= 1) {
			assertTrue(this.list.contains(VALUE_0));
		}
		assertFalse(this.list.contains(0));
	}
	
	@Test
	public void getAt() {
		if (getExpectedSize() >= 1) {
			assertEquals(VALUE_0, this.list.getAt(0));
		}
		if (getExpectedSize() > 3) {
			assertEquals(VALUE_0 + 3, this.list.getAt(3));
		}
		
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.list.getAt(getExpectedSize());
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.list.getAt(-1);
		});
	}
	
	@Test
	public void getFirst() {
		if (getExpectedSize() >= 1) {
			assertEquals(VALUE_0, this.list.getFirst());
		}
		else {
			assertThrows(NoSuchElementException.class, () -> {
				this.list.getFirst();
			});
		}
	}
	
	@Test
	public void getLast() {
		if (getExpectedSize() > 3) {
			assertEquals(VALUE_0 + getExpectedSize() - 1, this.list.getLast());
		}
		else if (getExpectedSize() >= 1) {
			assertEquals(VALUE_0, this.list.getLast());
		}
		else {
			assertThrows(NoSuchElementException.class, () -> {
				this.list.getLast();
			});
		}
	}
	
	@Test
	public void indexOf() {
		if (getExpectedSize() >= 1) {
			assertEquals(0, this.list.indexOf(VALUE_0));
		}
		assertEquals(-1, this.list.indexOf(0));
	}
	
	@Test
	public void lastIndexOf() {
		if (getExpectedSize() >= 1) {
			assertEquals(0, this.list.lastIndexOf(VALUE_0));
		}
		assertEquals(-1, this.list.lastIndexOf(0));
	}
	
	
	protected void assertNotModified() {
		assertEquals(getExpectedSize(), this.list.size());
		if (getExpectedSize() >= 1) {
			assertEquals(VALUE_0, this.list.getAt(0));
		}
	}
	
	@Test
	public void add() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.add(2);
		});
		assertNotModified();
	}
	
	@Test
	public void addAt() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.addAt(0, 2);
		});
		assertNotModified();
	}
	
	@Test
	public void setAt() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.setAt(0, 2);
		});
		assertNotModified();
	}
	
	@Test
	public void remove() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.remove(VALUE_0);
		});
		assertNotModified();
	}
	
	@Test
	public void removeAt() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.removeAt(0);
		});
		assertNotModified();
	}
	
	@Test
	public void clear() {
		assertThrows(UnsupportedOperationException.class, () -> {
			this.list.clear();
		});
		assertNotModified();
	}
	
	
	@Test
	public void toArray() {
		final long[] array= this.list.toArray();
		assertEquals(getExpectedSize(), array.length);
		if (getExpectedSize() >= 1) {
			assertEquals(VALUE_0, array[0]);
		}
	}
	
	@Test
	@SuppressWarnings("null")
	public void toArray_LongArray_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			this.list.toArray(null);
		});
		if (getExpectedSize() >= 1) {
			assertThrows(IndexOutOfBoundsException.class, () -> {
				this.list.toArray(new long[getExpectedSize() - 1]);
			});
		}
	}
	
	@Test
	public void toArray_LongArray() {
		final long[] array= new long[this.list.size()];
		this.list.toArray(array);
		if (getExpectedSize() >= 1) {
			assertEquals(VALUE_0, array[0]);
		}
	}
	
	
	@Test
	@SuppressWarnings("unlikely-arg-type")
	public void equals() {
		assertFalse(this.list.equals(null));
		assertFalse(this.list.equals("X"));
		
		assertTrue(this.list.equals(this.list));
		
		assertTrue(this.list.equals(createList(VALUE_0)));
		if (getExpectedSize() >= 1) {
			assertFalse(this.list.equals(createList(0)));
			assertFalse(this.list.equals(ImCollections.emptyLongList()));
		}
		assertFalse(this.list.equals(ImCollections.newLongList(VALUE_0, VALUE_0)));
	}
	
	
	@Test
	public void toArrayEquals() {
		assertTrue(this.list.equals(ImCollections.newLongList(this.list.toArray())));
	}
	
}
