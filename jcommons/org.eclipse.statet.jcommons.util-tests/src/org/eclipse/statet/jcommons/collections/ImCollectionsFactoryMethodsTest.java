/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.internal.jcommons.collections.ImIntEmptyList;
import org.eclipse.statet.internal.jcommons.collections.ImIntSeqList;
import org.eclipse.statet.internal.jcommons.collections.ImIntSingletonList;
import org.eclipse.statet.internal.jcommons.collections.ImLongEmptyList;
import org.eclipse.statet.internal.jcommons.collections.ImLongSeqList;
import org.eclipse.statet.internal.jcommons.collections.ImLongSingletonList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ImCollectionsFactoryMethodsTest {
	
	
	public ImCollectionsFactoryMethodsTest() {
	}
	
	
	private static ArrayList<String> newArrayList(final @NonNull String... elements) {
		return new ArrayList<>(Arrays.asList(elements));
	}
	
	
	@Test
	public void IntList_newIntSequence_argCheck() {
		assertThrows(IllegalArgumentException.class, () -> {
			ImCollections.newIntSequence(1, 0);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			ImCollections.newIntSequence(-1, Integer.MAX_VALUE);
		});
	}
	
	@Test
	public void IntList_newIntSequence_Empty() {
		final ImIntList list= ImCollections.newIntSequence(1, 1);
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());
		assertInstanceOf(ImIntEmptyList.class, list);
	}
	
	@Test
	public void IntList_newIntSequence_Singleton() {
		final ImIntList list= ImCollections.newIntSequence(1, 2);
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());
		assertInstanceOf(ImIntSingletonList.class, list);
		assertEquals(1, list.getAt(0));
	}
	
	@Test
	public void IntList_newIntSequence_Seq() {
		final ImIntList list= ImCollections.newIntSequence(1, 3);
		assertFalse(list.isEmpty());
		assertEquals(2, list.size());
		assertInstanceOf(ImIntSeqList.class, list);
		assertEquals(1, list.getAt(0));
		assertEquals(2, list.getAt(1));
	}
	
	
	@Test
	public void LongList_newLongSequence_argCheck() {
		assertThrows(IllegalArgumentException.class, () -> {
			ImCollections.newLongSequence(1, 0);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			ImCollections.newLongSequence(0, Integer.MAX_VALUE + 1);
		});
	}
	
	@Test
	public void LongList_newLongSequence_Empty() {
		final ImLongList list= ImCollections.newLongSequence(1, 1);
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());
		assertInstanceOf(ImLongEmptyList.class, list);
	}
	
	@Test
	public void LongList_newLongSequence_Singleton() {
		final ImLongList list= ImCollections.newLongSequence(1, 2);
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());
		assertInstanceOf(ImLongSingletonList.class, list);
		assertEquals(1, list.getAt(0));
	}
	
	@Test
	public void LongList_newLongSequence_Seq() {
		final ImLongList list= ImCollections.newLongSequence(1, 3);
		assertFalse(list.isEmpty());
		assertEquals(2, list.size());
		assertInstanceOf(ImLongSeqList.class, list);
		assertEquals(1, list.getAt(0));
		assertEquals(2, list.getAt(1));
	}
	
}
