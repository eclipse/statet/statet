/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.collections.CollectionTests.A_notIdentical;
import static org.eclipse.statet.jcommons.collections.CollectionTests.B_notIdentical;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractMutableListTest<T extends List<@Nullable String>> extends AbstractListTest<T> {
	
	
	public AbstractMutableListTest(final Class<?> type) {
		super(type);
	}
	
	
	protected abstract T createEmptyCollection();
	
	protected abstract T createCollection(final @Nullable String... elements);
	
	
	@Test
	public void add() {
		boolean r;
		
		this.c= createEmptyCollection();
		assertCollectionEmpty();
		
		r= this.c.add("A");
		assertCollectionEquals("A");
		assertTrue(r);
		
		r= this.c.add("B");
		assertCollectionEquals("A", "B");
		assertTrue(r);
		
		r= this.c.add("A");
		assertCollectionEquals("A", "B", "A");
		assertTrue(r);
	}
	
	@Test
	public void add_notIdentical() {
		boolean r;
		
		this.c= createCollection("A", "B");
		assertCollectionEquals("A", "B");
		
		r= this.c.add(A_notIdentical);
		assertCollectionEquals("A", "B", A_notIdentical);
		assertTrue(r);
	}
	
	@Test
	public void add_atIndex() {
		this.c= createEmptyCollection();
		this.c.add(0, "A");
		assertCollectionEquals("A");
		
		this.c.add(1, "B");
		assertCollectionEquals("A", "B");
		
		this.c.add(1, "C");
		assertCollectionEquals("A", "C", "B");
		
		this.c.add(0, "D");
		assertCollectionEquals("D", "A", "C", "B");
	}
	
	@Test
	public void addAll() {
		boolean r;
		
		this.c= createEmptyCollection();
		assertCollectionEmpty();
		
		r= this.c.addAll(Arrays.asList("A", "B"));
		assertCollectionEquals("A", "B");
		assertTrue(r);
		
		r= this.c.addAll(Arrays.asList("A", "B"));
		assertCollectionEquals("A", "B", "A", "B");
		assertTrue(r);
	}
	
	@Test
	public void addAll_notIdentical() {
		boolean r;
		
		this.c= createCollection("A", "B");
		
		r= this.c.addAll(Arrays.asList(A_notIdentical, B_notIdentical));
		assertCollectionEquals("A", "B", A_notIdentical, B_notIdentical);
		assertTrue(r);
	}
	
	@Test
	public void addAll_atIndex() {
		this.c= createEmptyCollection();
		this.c.addAll(0, Arrays.asList("A", "B"));
		assertCollectionEquals("A", "B");
		
		this.c.addAll(2, Arrays.asList("C", "D"));
		assertCollectionEquals("A", "B", "C", "D");
		
		this.c.addAll(2, Arrays.asList("E", "F", "G"));
		assertCollectionEquals("A", "B", "E", "F", "G", "C", "D");
		
		this.c.addAll(0, Arrays.asList("H"));
		assertCollectionEquals("H", "A", "B", "E", "F", "G", "C", "D");
	}
	
	@Test
	public void retainAll() {
		boolean r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList("B", "C", "D"));
		assertCollectionEquals("B", "C", "D");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList("X", "B", "C", "C", "D", "X"));
		assertCollectionEquals("B", "C", "D");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList("D", "C", "A"));
		assertCollectionEquals("A", "C", "D");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList("A"));
		assertCollectionEquals("A");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList("F"));
		assertCollectionEquals("F");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList("X"));
		assertCollectionEmpty();
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList());
		assertCollectionEmpty();
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList("F", "B", "C", "D", "E", "A"));
		assertCollectionEquals("A", "B", "C", "D", "E", "F");
		assertFalse(r);
	}
	
	@Test
	public void retainAll_notIdentical() {
		boolean r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.retainAll(Arrays.asList(A_notIdentical, "B", B_notIdentical, "D"));
		if (this.isIdentityCollection) {
			assertCollectionEquals("B", "D");
			assertTrue(r);
		}
		else {
			assertCollectionEquals("A", "B", "D");
			assertTrue(r);
		}
	}
	
	@Test
	public void remove() {
		boolean r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.remove("B");
		assertCollectionEquals("A", "C", "D", "E", "F");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.remove("A");
		assertCollectionEquals("B", "C", "D", "E", "F");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.remove("F");
		assertCollectionEquals("A", "B", "C", "D", "E");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.remove("X");
		assertCollectionEquals("A", "B", "C", "D", "E", "F");
		assertFalse(r);
		
		this.c= createCollection("A");
		r= this.c.remove("A");
		assertCollectionEmpty();
		assertTrue(r);
		
		r= this.c.remove("X");
		assertCollectionEmpty();
		assertFalse(r);
	}
	
	@Test
	public void remove_notIdentical() {
		boolean r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.remove(B_notIdentical);
		if (this.isIdentityCollection) {
			assertCollectionEquals("A", "B", "C", "D", "E", "F");
			assertFalse(r);
		}
		else {
			assertCollectionEquals("A", "C", "D", "E", "F");
			assertTrue(r);
		}
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.remove(A_notIdentical);
		if (this.isIdentityCollection) {
			assertCollectionEquals("A", "B", "C", "D", "E", "F");
			assertFalse(r);
		}
		else {
			assertCollectionEquals("B", "C", "D", "E", "F");
			assertTrue(r);
		}
	}
	
	@Test
	public void remove_atIndex() {
		String r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.remove(1);
		assertCollectionEquals("A", "C", "D", "E", "F");
		assertEquals("B", r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.remove(0);
		assertCollectionEquals("B", "C", "D", "E", "F");
		assertEquals("A", r);
		
		r= this.c.remove(4);
		assertCollectionEquals("B", "C", "D", "E");
		assertEquals("F", r);
		
		this.c= createCollection("A");
		r= this.c.remove(0);
		assertCollectionEmpty();
		assertEquals("A", r);
	}
	
	@Test
	public void removeAll() {
		boolean r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList("B", "C", "D"));
		assertCollectionEquals("A", "E", "F");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList("X", "B", "C", "C", "D", "X"));
		assertCollectionEquals("A", "E", "F");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList("D", "C", "A"));
		assertCollectionEquals("B", "E", "F");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList("A"));
		assertCollectionEquals("B", "C", "D", "E", "F");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList("F"));
		assertCollectionEquals("A", "B", "C", "D", "E");
		assertTrue(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList("X"));
		assertCollectionEquals("A", "B", "C", "D", "E", "F");
		assertFalse(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList());
		assertCollectionEquals("A", "B", "C", "D", "E", "F");
		assertFalse(r);
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList("A", "B", "C", "D", "E", "F"));
		assertCollectionEmpty();
		assertTrue(r);
		
		this.c= createCollection("A");
		r= this.c.removeAll(Arrays.asList("A", "B", "C", "D", "E", "F"));
		assertCollectionEmpty();
		assertTrue(r);
	}
	
	@Test
	public void removeAll_notIdentical() {
		boolean r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.removeAll(Arrays.asList(A_notIdentical, "B", B_notIdentical, "D"));
		if (this.isIdentityCollection) {
			assertCollectionEquals("A", "C", "E", "F");
			assertTrue(r);
		}
		else {
			assertCollectionEquals("C", "E", "F");
			assertTrue(r);
		}
	}
	
	@Test
	public void clear() {
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		this.c.clear();
		assertCollectionEmpty();
		
		this.c.clear();
		assertCollectionEmpty();
	}
	
}
