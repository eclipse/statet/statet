/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.statet.internal.jcommons.collections.ImArraySet;
import org.eclipse.statet.internal.jcommons.collections.ImEmptySet;
import org.eclipse.statet.internal.jcommons.collections.ImSingletonSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImSeqedEqualitySetTest extends AbstractImSeqSetTest {
	
	
	public ImSeqedEqualitySetTest() {
		super(ImCollections::newSet);
	}
	
	
	@Override
	@SuppressWarnings("cast")
	protected void assertExpectedType(final @Nullable TestCase<ImSequencedSet<@Nullable String>> testCase,
			final int variant) {
		assertTrue(this.c instanceof ImSet);
		assertTrue(this.c instanceof ImSequencedSet);
		assertFalse(this.c instanceof ImIdentitySet);
		assertFalse(this.c instanceof ImList);
		
		switch (this.c.size()) {
		case 0:
			assertSame(this.c, ImEmptySet.INSTANCE);
			break;
		case 1:
			assertSame(this.c.getClass(), ImSingletonSet.class);
			break;
		default:
			assertSame(this.c.getClass(), ImArraySet.class);
			break;
		}
	}
	
}
