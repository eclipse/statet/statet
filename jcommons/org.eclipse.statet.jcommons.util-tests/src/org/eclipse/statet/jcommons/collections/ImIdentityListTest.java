/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.statet.internal.jcommons.collections.ImArrayIdentityList;
import org.eclipse.statet.internal.jcommons.collections.ImArrayIdentitySub0List;
import org.eclipse.statet.internal.jcommons.collections.ImArrayIdentitySubList;
import org.eclipse.statet.internal.jcommons.collections.ImEmptyIdentityList;
import org.eclipse.statet.internal.jcommons.collections.ImSingletonIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImIdentityListTest extends AbstractImListTest {
	
	
	public ImIdentityListTest() {
		super(ImCollections::newIdentityList);
	}
	
	
	@Override
	@SuppressWarnings("cast")
	protected void assertExpectedType(final @Nullable TestCase<ImList<@Nullable String>> testCase,
			final int variant) {
		assertTrue(this.c instanceof ImList);
		assertTrue(this.c instanceof ImIdentityList);
		assertFalse(this.c instanceof ImSet);
		
		switch (this.c.size()) {
		case 0:
			assertSame(ImEmptyIdentityList.INSTANCE, this.c);
			break;
		case 1:
			assertSame(ImSingletonIdentityList.class, this.c.getClass());
			break;
		default:
			if ((testCase instanceof SubListCase || (variant & SUB) != 0)
					&& (variant & REV) == 0 ) {
				assertTrue(ImArrayIdentitySubList.class == this.c.getClass() 
						|| ImArrayIdentitySub0List.class == this.c.getClass() );
				break;
			}
			assertSame(ImArrayIdentityList.class, this.c.getClass());
			break;
		}
	}
	
}
