/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class CopyOnWriteIdentityListSetTest extends AbstractMutableSetTest<CopyOnWriteIdentityListSet<@Nullable String>> {
	
	
	public CopyOnWriteIdentityListSetTest() {
		super(CopyOnWriteIdentityListSet.class);
	}
	
	
	@Override
	protected CopyOnWriteIdentityListSet<@Nullable String> createEmptyCollection() {
		return new CopyOnWriteIdentityListSet<>();
	}
	
	@Override
	protected CopyOnWriteIdentityListSet<@Nullable String> createCollection(final @Nullable String... elements) {
		final var set= new HashSet<>(Arrays.asList(elements));
		return new CopyOnWriteIdentityListSet<>(set);
	}
	
	
	@Override
	protected void assertCollectionEquals(final @Nullable String... expected) {
		CollectionTests.assertListEqualsSameEntries(expected, this.c.toList());
	}
	
	
	@Test
	public void clearToList() {
		ImIdentityList<@Nullable String> r;
		
		this.c= createCollection("A", "B", "C", "D", "E", "F");
		r= this.c.clearToList();
		assertCollectionEmpty();
		CollectionTests.assertListEqualsSameEntries(new @Nullable String[] { "A", "B", "C", "D", "E", "F" }, r);
		
		r= this.c.clearToList();
		assertCollectionEmpty();
		CollectionTests.assertListEqualsSameEntries(new @Nullable String[] {}, r);
	}
	
}
