/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Set;

import org.junit.jupiter.api.Assertions;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class CollectionTests {
	
	
	public static final String A_notIdentical= new String("A");
	public static final String B_notIdentical= new String("B");
	
	
	public static final <E> void assertCollectionEmpty(final Collection<E> c) {
		assertTrue(c.isEmpty());
		Assertions.assertEquals(0, c.size());
	}
	
	public static final <E> void assertListEquals(final @Nullable String[] expected, final List<E> c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		for (int index= 0; index < expected.length; index++) {
			final var e= expected[index];
			assertEquals((e != null) ? new String(e) : null, c.get(index), String.format("[%1$s]", index));
		}
	}
	
	public static final <E> void assertListEqualsSameEntries(final E[] expected, final List<E> c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		for (int index= 0; index < expected.length; index++) {
			assertSameIndexed(expected[index], c.get(index), index);
		}
	}
	
	public static final <E> void assertSetEquals(final E[] expected, final Set<E> c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		for (int index= 0; index < expected.length; index++) {
			assertTrue(c.contains(expected[index]), String.format("[%1$s]", index));
		}
	}
	
	public static final <E> void assertSetEqualsSameEntries(final E[] expected, final ImSequencedSet<E> c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		assertIteratorSameEntries(expected, c.iterator());
	}
	
	
	public static final <E> void assertIteratorSameEntries(final E[] expected,
			final Iterator<E> iterator) {
		for (int index= 0; index < expected.length; index++) {
			assertTrue(iterator.hasNext());
			assertSameIndexed(expected[index], iterator.next(), index);
		}
		assertFalse(iterator.hasNext());
		assertThrows(NoSuchElementException.class, () -> {
			iterator.next();
		});
	}
	
	private static void assertIteratorState(final int index, final int size, final ListIterator<?> iterator) {
		final String s= String.format("[%1$s]", index);
		if (index < size) {
			assertTrue(iterator.hasNext(), s);
		}
		else {
			assertFalse(iterator.hasNext(), s);
			assertThrows(NoSuchElementException.class, () -> {
				iterator.next();
			});
		}
		assertEquals(index, iterator.nextIndex());
		
		if (index > 0) {
			assertTrue(iterator.hasPrevious());
		}
		else {
			assertFalse(iterator.hasPrevious());
			assertThrows(NoSuchElementException.class, () -> {
				iterator.previous();
			});
		}
		assertEquals(index - 1, iterator.previousIndex());
	}
	
	public static final <E> void assertListIteratorSameEntries(final E[] expected, final int initIndex,
			final ListIterator<E> iterator) {
		final int size= expected.length;
		int index= initIndex;
		while (index < size) {
			assertIteratorState(index, size, iterator);
			assertSameIndexed(expected[index], iterator.next(), index);
			index++;
		}
		// index == size
		assertIteratorState(index, size, iterator);
		assertFalse(iterator.hasNext());
		if (size >= 1) {
			index--;
			assertSameIndexed(expected[index], iterator.previous(), index);
			assertIteratorState(index, size, iterator);
			assertSameIndexed(expected[index], iterator.next(), index);
			index++;
			while (index > 0) {
				assertIteratorState(index, size, iterator);
				index--;
				assertSameIndexed(expected[index], iterator.previous(), index);
			}
		}
		// index == 0
		assertIteratorState(index, size, iterator);
		assertFalse(iterator.hasPrevious());
		if (size >= 1) {
			assertSameIndexed(expected[index], iterator.next(), index);
			index++;
			assertIteratorState(index, size, iterator);
		}
	}
	
	
	public static final <E> void assertCollectionEmpty(final IntList c) {
		assertTrue(c.isEmpty());
		Assertions.assertEquals(0, c.size());
	}
	
	public static final <E> void assertListEquals(final int[] expected, final IntList c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		for (int index= 0; index < expected.length; index++) {
			final var e= expected[index];
			assertEquals(e, c.getAt(index), String.format("[%1$s]", index));
		}
	}
	
	
	public static final <E> void assertCollectionEmpty(final LongList c) {
		assertTrue(c.isEmpty());
		Assertions.assertEquals(0, c.size());
	}
	
	public static final <E> void assertListEquals(final long[] expected, final LongList c) {
		if (expected.length == 0) {
			assertCollectionEmpty(c);
			return;
		}
		assertFalse(c.isEmpty());
		Assertions.assertEquals(expected.length, c.size());
		for (int index= 0; index < expected.length; index++) {
			final var e= expected[index];
			assertEquals(e, c.getAt(index), String.format("[%1$s]", index));
		}
	}
	
	
	public static void assertSameIndexed(final @Nullable Object expected, final @Nullable Object actual, final int index) {
		if (expected == actual) {
			return;
		}
		assertSame(expected, actual, String.format("[%1$s]", index));
	}
	
	public static <E, F> void assertArraySameEntries(final E[] expected, final F[] actual) {
		if (expected == actual) {
			return;
		}
		assertEquals(expected.length, actual.length);
		
		for (int index = 0; index < expected.length; index++) {
			assertSameIndexed(expected[index], actual[index], index);
		}
	}
	
	public static <E> E[] reversedCopy(final E[] array) {
		final E[] reversed= array.clone();
		Collections.reverse(Arrays.asList(reversed));
		return reversed;
	}
	
}
