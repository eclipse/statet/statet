/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class IntIntervalArraysTest {
	
	
	public IntIntervalArraysTest() {
	}
	
	
	@Test
	public void insertBlank() {
		testInsertBlank(new int[][] {
					{ 7, 8, 11, 12 },
					{ 7, 8, 11, 12 },
					{ 7, 8, 11, 12 },
					{ 7, 8, 11, 12 },
					{ 7, 8, 11, 12 },
					{ 7, 8, 11, 12 },
					{ 7, 8, 11, 12 },
					{ 6, 7, 11, 12 },
					{ 6, 7, 11, 12 },
					{ 6, 7, 11, 12 },
					{ 6, 7, 11, 12 },
					{ 6, 7, 10, 11 },
					{ 6, 7, 10, 11 },
				}, new int[]
					{ 6, 7, 10, 11 },
				1 );
		
		testInsertBlank(new int[][] {
					{ 17, 18, 21, 22 },
					{ 17, 18, 21, 22 },
					{ 17, 18, 21, 22 },
					{ 17, 18, 21, 22 },
					{ 17, 18, 21, 22 },
					{ 17, 18, 21, 22 },
					{ 17, 18, 21, 22 },
					{ 6, 7, 21, 22 },
					{ 6, 7, 21, 22 },
					{ 6, 7, 21, 22 },
					{ 6, 7, 21, 22 },
					{ 6, 7, 10, 11 },
					{ 6, 7, 10, 11 },
				}, new int[]
					{ 6, 7, 10, 11 },
				11 );
	}
	
	@Test
	public void insertBlank_start0() {
		testInsertBlank(new int[][] {
					{ 1, 6, 7, 8, 11, 12 },
					{ 0, 1, 2, 6, 7, 8, 11, 12 },
					{ 0, 2, 3, 6, 7, 8, 11, 12 },
					{ 0, 3, 4, 6, 7, 8, 11, 12 },
					{ 0, 4, 5, 6, 7, 8, 11, 12 },
					{ 0, 5, 7, 8, 11, 12 },
					{ 0, 5, 7, 8, 11, 12 },
					{ 0, 5, 6, 7, 11, 12 },
					{ 0, 5, 6, 7, 11, 12 },
					{ 0, 5, 6, 7, 11, 12 },
					{ 0, 5, 6, 7, 11, 12 },
					{ 0, 5, 6, 7, 10, 11 },
					{ 0, 5, 6, 7, 10, 11 },
				}, new int[]
					{ 0, 5, 6, 7, 10, 11 },
				1 );
		
		testInsertBlank(new int[][] {
					{ 11, 16, 17, 18, 21, 22 },
					{ 0, 1, 12, 16, 17, 18, 21, 22 },
					{ 0, 2, 13, 16, 17, 18, 21, 22 },
					{ 0, 3, 14, 16, 17, 18, 21, 22 },
					{ 0, 4, 15, 16, 17, 18, 21, 22 },
					{ 0, 5, 17, 18, 21, 22 },
					{ 0, 5, 17, 18, 21, 22 },
					{ 0, 5, 6, 7, 21, 22 },
					{ 0, 5, 6, 7, 21, 22 },
					{ 0, 5, 6, 7, 21, 22 },
					{ 0, 5, 6, 7, 21, 22 },
					{ 0, 5, 6, 7, 10, 11 },
					{ 0, 5, 6, 7, 10, 11 },
				}, new int[]
					{ 0, 5, 6, 7, 10, 11 },
				11 );
	}
	
	@Test
	public void insertBlank_empty() {
		testInsertBlank(new int[][] {
					{ },
					{ },
					{ },
				}, new int[]
					{ },
				1 );
		
		testInsertBlank(new int[][] {
					{ },
					{ },
					{ },
				}, new int[]
					{ },
				11 );
	}
	
	private void testInsertBlank(final int[][] expectedRegions,
			final int[] baseRegions, final int length) {
		for (int offset= 0; offset < expectedRegions.length; offset++) {
			final String id= String.format("[offset= %1$s, length= %2$s]", offset, length);
			final int[] actualRegions= IntIntervalArrays.insertBlank(baseRegions.clone(), offset, length);
			assertArrayEquals(expectedRegions[offset], actualRegions, id);
		}
	}
	
	
	@Test
	public void insertRegion() {
		testInsertRegion(new int[][] {
					{ 0, 1, 7, 8, 11, 12 },
					{ 1, 2, 7, 8, 11, 12 },
					{ 2, 3, 7, 8, 11, 12 },
					{ 3, 4, 7, 8, 11, 12 },
					{ 4, 5, 7, 8, 11, 12 },
					{ 5, 6, 7, 8, 11, 12 },
					{ 6, 8, 11, 12 },
					{ 6, 8, 11, 12 },
					{ 6, 7, 8, 9, 11, 12 },
					{ 6, 7, 9, 10, 11, 12 },
					{ 6, 7, 10, 12 },
					{ 6, 7, 10, 12 },
					{ 6, 7, 10, 11, 12, 13 },
				}, new int[]
					{ 6, 7, 10, 11 },
				1 );
		
		testInsertRegion(new int[][] {
					{ 0, 11, 17, 18, 21, 22 },
					{ 1, 12, 17, 18, 21, 22 },
					{ 2, 13, 17, 18, 21, 22 },
					{ 3, 14, 17, 18, 21, 22 },
					{ 4, 15, 17, 18, 21, 22 },
					{ 5, 16, 17, 18, 21, 22 },
					{ 6, 18, 21, 22 },
					{ 6, 18, 21, 22 },
					{ 6, 7, 8, 19, 21, 22 },
					{ 6, 7, 9, 20, 21, 22 },
					{ 6, 7, 10, 22 },
					{ 6, 7, 10, 22 },
					{ 6, 7, 10, 11, 12, 23 },
				}, new int[]
					{ 6, 7, 10, 11 },
				11 );
	}
	
	@Test
	public void insertRegion_start0() {
		testInsertRegion(new int[][] {
					{ 0, 6, 7, 8, 11, 12 },
					{ 0, 6, 7, 8, 11, 12 },
					{ 0, 6, 7, 8, 11, 12 },
					{ 0, 6, 7, 8, 11, 12 },
					{ 0, 6, 7, 8, 11, 12 },
					{ 0, 6, 7, 8, 11, 12 },
					{ 0, 5, 6, 8, 11, 12 },
					{ 0, 5, 6, 8, 11, 12 },
					{ 0, 5, 6, 7, 8, 9, 11, 12 },
					{ 0, 5, 6, 7, 9, 10, 11, 12 },
					{ 0, 5, 6, 7, 10, 12 },
					{ 0, 5, 6, 7, 10, 12 },
					{ 0, 5, 6, 7, 10, 11, 12, 13 },
				}, new int[]
					{ 0, 5, 6, 7, 10, 11 },
				1 );
		
		testInsertRegion(new int[][] {
					{ 0, 16, 17, 18, 21, 22 },
					{ 0, 16, 17, 18, 21, 22 },
					{ 0, 16, 17, 18, 21, 22 },
					{ 0, 16, 17, 18, 21, 22 },
					{ 0, 16, 17, 18, 21, 22 },
					{ 0, 16, 17, 18, 21, 22 },
					{ 0, 5, 6, 18, 21, 22 },
					{ 0, 5, 6, 18, 21, 22 },
					{ 0, 5, 6, 7, 8, 19, 21, 22 },
					{ 0, 5, 6, 7, 9, 20, 21, 22 },
					{ 0, 5, 6, 7, 10, 22 },
					{ 0, 5, 6, 7, 10, 22 },
					{ 0, 5, 6, 7, 10, 11, 12, 23 },
				}, new int[]
					{ 0, 5, 6, 7, 10, 11 },
				11 );
	}
	
	@Test
	public void insertRegion_empty() {
		testInsertRegion(new int[][] {
					{ 0, 1 },
					{ 1, 2 },
					{ 2, 3 },
				}, new int[]
					{ },
				1 );
		
		testInsertRegion(new int[][] {
					{ 0, 11 },
					{ 1, 12 },
					{ 2, 13 },
				}, new int[]
					{ },
				11 );
	}
	
	private void testInsertRegion(final int[][] expectedRegions,
			final int[] baseRegions, final int length) {
		for (int offset= 0; offset < expectedRegions.length; offset++) {
			final String id= String.format("[offset= %1$s, length= %2$s]", offset, length);
			final int[] actualRegions= IntIntervalArrays.insertRegion(baseRegions.clone(), offset, length);
			assertArrayEquals(expectedRegions[offset], actualRegions, id);
		}
	}
	
	
	@Test
	public void removeTail() {
		testRemoveTail(new int[][] {
					{ },
					{ 0, 1 },
					{ 0, 2 },
					{ 0, 3 },
					{ 0, 4 },
					{ 0, 5 },
					{ 0, 5 },
					{ 0, 5, 6, 7 },
					{ 0, 5, 6, 7 },
					{ 0, 5, 6, 7 },
					{ 0, 5, 6, 7 },
					{ 0, 5, 6, 7, 10, 11 },
					{ 0, 5, 6, 7, 10, 11 },
				}, new int[]
					{ 0, 5, 6, 7, 10, 11 } );
		testRemoveTail(new int[][] {
					{ },
					{ },
					{ },
					{ },
					{ },
					{ },
					{ },
					{ 6, 7 },
					{ 6, 7 },
				}, new int[]
					{ 6, 7, 10, 11 } );
	}
	
	private void testRemoveTail(final int[][] expectedRegions,
			final int[] baseRegions) {
		for (int offset= 0; offset < expectedRegions.length; offset++) {
			final String id= String.format("[offset= %1$s]", offset);
			final int[] actualRegions= IntIntervalArrays.removeTail(baseRegions.clone(), offset);
			assertArrayEquals(expectedRegions[offset], actualRegions, id);
		}
	}
	
}
