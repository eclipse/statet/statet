/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.eclipse.statet.jcommons.collections.CollectionTests.assertListEquals;
import static org.eclipse.statet.jcommons.collections.ImCollections.emptyIntList;
import static org.eclipse.statet.jcommons.collections.ImCollections.newIntList;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ImIntCollectionsOperationsTest {
	
	
	public ImIntCollectionsOperationsTest() {
	}
	
	
	private static IntArrayList newArrayList(final int... elements) {
		return new IntArrayList(elements);
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void List_addElement_atIndex_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ImCollections.addElement((IntList)null, 0, 999);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ImCollections.addElement(emptyIntList(), -1, 999);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			ImCollections.addElement(emptyIntList(), 1, 999);
		});
	}
	
	@Test
	public void List_addElement_ImList_atIndex() {
		final ImIntList list0= emptyIntList();
		final ImIntList list1= newIntList(1);
		final ImIntList list2= newIntList(1, 2);
		
		assertListEquals(
				new int[] { 999 },
				ImCollections.addElement(list0, 0, 999) );
		assertListEquals(
				new int[] { 999, 1 },
				ImCollections.addElement(list1, 0, 999) );
		assertListEquals(
				new int[] { 1, 999 },
				ImCollections.addElement(list1, 1, 999) );
		assertListEquals(
				new int[] { 999, 1, 2 },
				ImCollections.addElement(list2, 0, 999) );
		assertListEquals(
				new int[] { 1, 999, 2 },
				ImCollections.addElement(list2, 1, 999) );
		assertListEquals(
				new int[] { 1, 2, 999 },
				ImCollections.addElement(list2, 2, 999) );
	}
	
	@Test
	public void List_addElement_ArrayList_atIndex() {
		final IntArrayList list0= newArrayList();
		final IntArrayList list1= newArrayList(1);
		final IntArrayList list2= newArrayList(1, 2);
		
		assertListEquals(
				new int[] { 999 },
				ImCollections.addElement(list0, 0, 999) );
		assertListEquals(
				new int[] { 999, 1 },
				ImCollections.addElement(list1, 0, 999) );
		assertListEquals(
				new int[] { 1, 999 },
				ImCollections.addElement(list1, 1, 999) );
		assertListEquals(
				new int[] { 999, 1, 2 },
				ImCollections.addElement(list2, 0, 999) );
		assertListEquals(
				new int[] { 1, 999, 2 },
				ImCollections.addElement(list2, 1, 999) );
		assertListEquals(
				new int[] { 1, 2, 999 },
				ImCollections.addElement(list2, 2, 999) );
	}
	
	@Test
	@SuppressWarnings("null")
	public void List_addElement_argCheck() {
		assertThrows(NullPointerException.class, () -> {
			ImCollections.addElement((IntList)null, 999);
		});
	}
	
	@Test
	public void List_addElement_ImList() {
		final ImIntList list0= emptyIntList();
		final ImIntList list1= newIntList(1);
		final ImIntList list2= newIntList(1, 2);
		
		assertListEquals(
				new int[] { 999 },
				ImCollections.addElement(list0, 999) );
		assertListEquals(
				new int[] { 1, 999 },
				ImCollections.addElement(list1, 999) );
		assertListEquals(
				new int[] { 1, 2, 999 },
				ImCollections.addElement(list2, 999) );
		assertListEquals(
				new int[] { 1, 2, 2 },
				ImCollections.addElement(list2, 2) );
	}
	
	@Test
	public void List_addElement_ArrayList() {
		final IntArrayList list0= newArrayList();
		final IntArrayList list1= newArrayList(1);
		final IntArrayList list2= newArrayList(1, 2);
		
		assertListEquals(
				new int[] { 999 },
				ImCollections.addElement(list0, 999) );
		assertListEquals(
				new int[] { 1, 999 },
				ImCollections.addElement(list1, 999) );
		assertListEquals(
				new int[] { 1, 2, 999 },
				ImCollections.addElement(list2, 999) );
		assertListEquals(
				new int[] { 1, 2, 2 },
				ImCollections.addElement(list2, 2) );
	}
	
	
}
