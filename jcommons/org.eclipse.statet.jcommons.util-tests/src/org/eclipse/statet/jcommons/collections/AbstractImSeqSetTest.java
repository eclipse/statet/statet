/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractImSeqSetTest extends AbstractImCollectionTest<ImSequencedSet<@Nullable String>> {
	
	
	public AbstractImSeqSetTest(final Function<@Nullable String[], ImSequencedSet<@Nullable String>> factory) {
		super(factory);
	}
	
	
	public static List<TestCase<ImSet<@Nullable String>>> testCases() {
		return List.of(
				new TestCase<>("empty", new @Nullable String[] {}),
				new TestCase<>("single", new @Nullable String[] { "A" }),
				new TestCase<>("2", new @Nullable String[] { "A", "B" }),
				new TestCase<>("3", new @Nullable String[] { "A", "B", "C" }),
				new TestCase<>("n", new @Nullable String[] { "A", "B", "C", "D", "E", "F" }),
				new TestCase<>("single=null", new @Nullable String[] { null }),
				new TestCase<>("n+null", new @Nullable String[] { "A", "B", "C", null, "D", "E", "F" }) );
	}
	
	
	@Override
	protected Set<@Nullable String> createJavaImpl(final TestCase<ImSequencedSet<@Nullable String>> testCase) {
		return new HashSet<>(Arrays.asList(testCase.values));
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void getFirst(final TestCase<ImSequencedSet<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		if (testCase.size >= 1) {
			Assertions.assertSame(testCase.values[0], this.c.getFirst());
		}
		else {
			assertThrows(NoSuchElementException.class, () -> {
				this.c.getFirst();
			});
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void getLast(final TestCase<ImSequencedSet<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		if (testCase.size >= 1) {
			Assertions.assertSame(testCase.values[testCase.size - 1], this.c.getLast());
		}
		else {
			assertThrows(NoSuchElementException.class, () -> {
				this.c.getLast();
			});
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void reversed(final TestCase<ImSequencedSet<@Nullable String>> testCase) {
		final var parent= createCollection(testCase);
		
		this.c= parent.reversed();
		assertExpectedType(testCase, REV);
		CollectionTests.assertSetEqualsSameEntries(CollectionTests.reversedCopy(testCase.values), this.c);
	}
	
}
