/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.eclipse.statet.jcommons.collections.CollectionTests.A_notIdentical;
import static org.eclipse.statet.jcommons.collections.CollectionTests.B_notIdentical;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractImListTest extends AbstractImCollectionTest<ImList<@Nullable String>> {
	
	
	public AbstractImListTest(final Function<@Nullable String[], ImList<@Nullable String>> factory) {
		super(factory);
	}
	
	
	protected static class SubListCase extends TestCase<ImList<@Nullable String>> {
		
		private final @Nullable String[] parentValues;
		private final int startIndex;
		private final int endIndex;
		
		public SubListCase(final String label,
				final @Nullable String[] values, final int startIndex, final int endIndex) {
			super(String.format("%1$s sub=[%2$s, %3$s)", label, startIndex, endIndex),
					Arrays.copyOfRange(values, startIndex, endIndex) );
			this.parentValues= values;
			this.startIndex= startIndex;
			this.endIndex= endIndex;
		}
		
		@Override
		public ImList<@Nullable String> create(
				final Function<@Nullable String[], ImList<@Nullable String>> factory) {
			final var parent= factory.apply(this.parentValues);
			return parent.subList(this.startIndex, this.endIndex);
		}
		
	}
	
	public static List<TestCase<ImList<@Nullable String>>> testCases() {
		return List.of(
				new TestCase<>("empty", new @Nullable String[] {}),
				new TestCase<>("single", new @Nullable String[] { "A" }),
				new TestCase<>("2", new @Nullable String[] { "A", "B" }),
				new TestCase<>("3", new @Nullable String[] { "A", "B", "C" }),
				new TestCase<>("n", new @Nullable String[] { "A", "B", "C", "E", "D", "E", "F" }),
				
				new TestCase<>("single=null", new @Nullable String[] { null }),
				new TestCase<>("n+null", new @Nullable String[] { "A", "B", "C", null, "D", "E", "E", "F", null }),
				
				new SubListCase("2", new @Nullable String[] { "A", "B" }, 0, 1),
				new SubListCase("2", new @Nullable String[] { "A", "B" }, 1, 2),
				new SubListCase("n", new @Nullable String[] { "A", "B", "C", "E", "D", "E", "F" }, 0, 5),
				new SubListCase("n", new @Nullable String[] { "A", "B", "C", "E", "D", "E", "F" }, 1, 3),
				new SubListCase("n+null", new @Nullable String[] { "A", "B", "C", null, "D", "E", "E", "F", null }, 0, 6),
				new SubListCase("n+null", new @Nullable String[] { "A", "B", "C", null, "D", "E", "E", "F", null }, 3, 9) );
	}
	
	
	@Override
	protected List<@Nullable String> createJavaImpl(final TestCase<ImList<@Nullable String>> testCase) {
		return new ArrayList<>(Arrays.asList(testCase.values));
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void indexOf(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		for (int i= 0; i < Math.min(testCase.size, 3); i++) {
			assertEquals(i, this.c.indexOf(testCase.values[i]));
		}
		
		assertEquals(-1, this.c.indexOf("X"));
		if (testCase.nullFirstIdx == -1) {
			assertEquals(-1, this.c.indexOf(null));
		}
		
		if (this.c instanceof IdentityCollection) {
			assertEquals(-1, this.c.indexOf(A_notIdentical));
		}
		else if (testCase.size >= 1 && testCase.values[0] == "A") {
			assertEquals(0, this.c.indexOf(A_notIdentical));
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void lastIndexOf(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		for (int i= testCase.size - 1; i >= Math.max(testCase.size - 3, 0); i--) {
			assertEquals(i, this.c.lastIndexOf(testCase.values[i]));
		}
		
		assertEquals(-1, this.c.lastIndexOf("X"));
		if (testCase.nullFirstIdx == -1) {
			assertEquals(-1, this.c.lastIndexOf(null));
		}
		
		if (this.c instanceof IdentityCollection) {
			assertEquals(-1, this.c.lastIndexOf(A_notIdentical));
		}
		else if (testCase.size >= 1 && testCase.values[0] == "A") {
			assertEquals(0, this.c.lastIndexOf(A_notIdentical));
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void get_argCheck(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.c.get(-1);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.c.get(testCase.size);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void get(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		CollectionTests.assertListEqualsSameEntries(testCase.values, this.c);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void getFirst(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		if (testCase.size >= 1) {
			Assertions.assertSame(testCase.values[0], this.c.getFirst());
		}
		else {
			assertThrows(NoSuchElementException.class, () -> {
				this.c.getFirst();
			});
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void getLast(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		if (testCase.size >= 1) {
			Assertions.assertSame(testCase.values[testCase.size - 1], this.c.getLast());
		}
		else {
			assertThrows(NoSuchElementException.class, () -> {
				this.c.getLast();
			});
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void listIterator_atIndex_argCheck(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.c.listIterator(-1);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.c.listIterator(this.c.size() + 1);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void listIterator_atIndex(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		{	final ListIterator<@Nullable String> iterator= this.c.listIterator(0);
			assertNotNull(iterator);
			CollectionTests.assertListIteratorSameEntries(testCase.values, 0, iterator);
		}
		if (testCase.size > 1) {
			final ListIterator<@Nullable String> iterator= this.c.listIterator(1);
			assertNotNull(iterator);
			CollectionTests.assertListIteratorSameEntries(testCase.values, 1, iterator);
		}
		if (testCase.size >= 1) {
			final ListIterator<@Nullable String> iterator= this.c.listIterator(testCase.size);
			assertNotNull(iterator);
			CollectionTests.assertListIteratorSameEntries(testCase.values, testCase.size, iterator);
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void listIterator(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		{	final ListIterator<@Nullable String> iterator= this.c.listIterator();
			assertNotNull(iterator);
			CollectionTests.assertListIteratorSameEntries(testCase.values, 0, iterator);
		}
		{	final ListIterator<@Nullable String> iterator= this.c.listIterator();
			assertThrows(UnsupportedOperationException.class, () -> {
				iterator.add("A");
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				iterator.set("A");
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				iterator.remove();
			});
			if (testCase.size >= 1) {
				iterator.next();
				assertThrows(UnsupportedOperationException.class, () -> {
					iterator.add("A");
				});
				assertThrows(UnsupportedOperationException.class, () -> {
					iterator.set("A");
				});
				assertThrows(UnsupportedOperationException.class, () -> {
					iterator.remove();
				});
			}
			assertCollectionUnchanged(testCase);
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void set_atIndex(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.set(0, "A");
		});
		if (testCase.size >= 1) {
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.set(1, "A");
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.set(this.c.size() - 1, "A");
			});
		}
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void add_atIndex(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.add(0, "A");
		});
		if (testCase.size >= 1) {
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.add(1, "A");
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.add(this.c.size(), "A");
			});
		}
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void add_atIndex_notIdentical(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.add(0, A_notIdentical);
		});
		if (testCase.size >= 1) {
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.add(1, A_notIdentical);
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.add(this.c.size(), A_notIdentical);
			});
		}
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void addAll_atIndex(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(0, Arrays.asList());
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(0, Arrays.asList("A"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(0, Arrays.asList("A", "B"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(0, Arrays.asList("X"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(0, Arrays.asList("X", "Y"));
		});
		if (testCase.size >= 1) {
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(1, Arrays.asList());
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(1, Arrays.asList("A"));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(1, Arrays.asList("A", "B"));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(1, Arrays.asList("X"));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(1, Arrays.asList("X", "Y"));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(this.c.size(), Arrays.asList());
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(this.c.size(), Arrays.asList("A"));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(this.c.size(), Arrays.asList("A", "B"));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(this.c.size(), Arrays.asList("X"));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(this.c.size(), Arrays.asList("X", "Y"));
			});
		}
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void addAll_atIndex_notIdentical(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(0, Arrays.asList(A_notIdentical));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(0, Arrays.asList(A_notIdentical, B_notIdentical));
		});
		if (testCase.size >= 1) {
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(1, Arrays.asList(A_notIdentical));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(1, Arrays.asList(A_notIdentical, B_notIdentical));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(this.c.size(), Arrays.asList(A_notIdentical));
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.addAll(this.c.size(), Arrays.asList(A_notIdentical, B_notIdentical));
			});
		}
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void remove_atIndex(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.remove(0);
		});
		if (testCase.size >= 1) {
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.remove(1);
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				this.c.remove(this.c.size() - 1);
			});
		}
		assertCollectionUnchanged(testCase);
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void subList_argCheck(final TestCase<ImList<@Nullable String>> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(IllegalArgumentException.class, () -> {
			this.c.subList(0, -1);
		});
		if (testCase.size >= 1) {
			assertThrows(IllegalArgumentException.class, () -> {
				this.c.subList(1, 0);
			});
		}
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.c.subList(-1, 0);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.c.subList(0, Integer.MAX_VALUE);
		});
		assertThrows(IndexOutOfBoundsException.class, () -> {
			this.c.subList(Integer.MAX_VALUE, Integer.MAX_VALUE - 1);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void subList_empty(final TestCase<ImList<@Nullable String>> testCase) {
		final var parent= createCollection(testCase);
		
		this.c= parent.subList(0, 0);
		assertExpectedType(testCase, SUB);
		assertEquals(0, this.c.size());
		
		if (testCase.size >= 1) {
			this.c= parent.subList(1, 1);
			assertExpectedType(testCase, SUB);
			assertEquals(0, this.c.size());
		}
		
		this.c= parent.subList(testCase.size, testCase.size);
		assertExpectedType(testCase, SUB);
		assertEquals(0, this.c.size());
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void subList_singleton(final TestCase<ImList<@Nullable String>> testCase) {
		final var parent= createCollection(testCase);
		
		if (testCase.size >= 1) {
			this.c= parent.subList(0, 1);
			assertExpectedType(testCase, SUB);
			assertEquals(1, this.c.size());
			assertSame(testCase.values[0], this.c.get(0));
			
			this.c= parent.subList(testCase.size - 1, testCase.size);
			assertExpectedType(testCase, SUB);
			assertEquals(1, this.c.size());
			assertSame(testCase.values[testCase.size - 1], this.c.get(0));
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void subList_2(final TestCase<ImList<@Nullable String>> testCase) {
		final var parent= createCollection(testCase);
		
		if (testCase.size >= 3) {
			this.c= parent.subList(0, 2);
			assertExpectedType(testCase, SUB);
			assertEquals(2, this.c.size());
			assertSame(testCase.values[0], this.c.get(0));
			assertSame(testCase.values[1], this.c.get(1));
			
			this.c= parent.subList(testCase.size - 2, testCase.size);
			assertExpectedType(testCase, SUB);
			assertEquals(2, this.c.size());
			assertSame(testCase.values[testCase.size - 2], this.c.get(0));
			assertSame(testCase.values[testCase.size - 1], this.c.get(1));
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void subList_unchanged(final TestCase<ImList<@Nullable String>> testCase) {
		final var parent= createCollection(testCase);
		
		assertSame(parent, parent.subList(0, testCase.size));
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void reversed(final TestCase<ImList<@Nullable String>> testCase) {
		final var parent= createCollection(testCase);
		
		this.c= parent.reversed();
		assertExpectedType(testCase, REV);
		CollectionTests.assertListEqualsSameEntries(CollectionTests.reversedCopy(testCase.values), this.c);
	}
	
}
