/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.collections.CollectionTests.A_notIdentical;
import static org.eclipse.statet.jcommons.collections.CollectionTests.B_notIdentical;
import static org.eclipse.statet.jcommons.collections.CollectionTests.assertArraySameEntries;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractImCollectionTest<C extends ImCollection<@Nullable String>> {
	
	
	protected static final int BASE=    1 << 0;
	protected static final int SUB=     1 << 1;
	protected static final int REV=     1 << 2;
	
	
	protected static class TestCase<TCollection extends ImCollection<? extends @Nullable String>> {
		
		private final String label;
		
		final @Nullable String[] values;
		
		final int size;
		
		final int nullFirstIdx;
		
		
		public TestCase(final String label, final @Nullable String[] values) {
			this.label= label;
			this.values= values;
			this.size= values.length;
			this.nullFirstIdx= indexOf(values, null);
		}
		
		
		TCollection create(final Function<@Nullable String[], TCollection> factory) {
			return factory.apply(this.values);
		}
		
		
		@Override
		public String toString() {
			return String.format("%1$s: %2$s", this.label, Arrays.toString(this.values));
		}
		
	}
	
	
	protected final Function<@Nullable String[], C> factory;
	
	protected C c;
	
	
	@SuppressWarnings("null")
	protected AbstractImCollectionTest(final Function<@Nullable String[], C> factory) {
		this.factory= factory;
	}
	
	
	protected C createCollection(final TestCase<C> testCase) {
		return testCase.create(this.factory);
	}
	
	protected abstract Collection<@Nullable String> createJavaImpl(TestCase<C> testCase);
	
	
	protected void assertCollectionUnchanged(final TestCase<C> testCase) {
		assertEquals(testCase.size, this.c.size());
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("cast")
	public void type(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertNotNull(this.c);
		assertTrue(this.c instanceof ImCollection);
		assertExpectedType(testCase, BASE);
	}
	
	protected abstract void assertExpectedType(@Nullable TestCase<C> testCase, int variant);
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void size(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertEquals(testCase.size, this.c.size());
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void isEmpty(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertEquals(testCase.size == 0, this.c.isEmpty());
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void contains(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		for (int i= 0; i < testCase.values.length; i++) {
			assertTrue(this.c.contains(testCase.values[i]));
		}
		
		assertFalse(this.c.contains("X"));
		if (testCase.nullFirstIdx == -1) {
			assertFalse(this.c.contains(null));
		}
		
		if (this.c instanceof IdentityCollection) {
			assertFalse(this.c.contains(A_notIdentical));
		}
		else if (testCase.size >= 1 && testCase.values[0] == "A") {
			assertTrue(this.c.contains(A_notIdentical));
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void containsAll_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(NullPointerException.class, () -> {
			this.c.containsAll(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void containsAll(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		if (testCase.size >= 1) {
			assertTrue(this.c.containsAll(Arrays.asList(testCase.values[0])));
		}
		assertTrue(this.c.containsAll(Arrays.asList(testCase.values)));
		assertFalse(this.c.containsAll(Arrays.asList("X")));
		if (testCase.size >= 1) {
			assertFalse(this.c.containsAll(Arrays.asList(testCase.values[0], "X")));
		}
		assertFalse(this.c.containsAll(Arrays.asList("X", "Y")));
		
		assertFalse(this.c.containsAll(Set.of("X")));
		
		if (this.c instanceof IdentityCollection) {
			assertFalse(this.c.containsAll(Arrays.asList("A", A_notIdentical)));
		}
		else if (testCase.size >= 1 && testCase.values[0] == "A") {
			assertTrue(this.c.containsAll(Arrays.asList("A", A_notIdentical)));
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void iterator(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		if (this.c.iterator() instanceof ListIterator) {
			final ListIterator<@Nullable String> iterator= (ListIterator<@Nullable String>)this.c.iterator();
			assertNotNull(iterator);
			CollectionTests.assertListIteratorSameEntries(testCase.values, 0, iterator);
			
			assertThrows(UnsupportedOperationException.class, () -> {
				iterator.add("A");
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				iterator.set("A");
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				iterator.remove();
			});
		}
		else {
			final Iterator<@Nullable String> iterator= this.c.iterator();
			assertNotNull(iterator);
			CollectionTests.assertIteratorSameEntries(testCase.values, iterator);
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void stream(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		Stream<@Nullable String> stream;
		
		stream= this.c.stream();
		assertNotNull(stream);
		CollectionTests.assertIteratorSameEntries(testCase.values, stream.iterator());
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void add(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.add("A");
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void add_notIdentical(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.add(A_notIdentical);
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void addAll_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void addAll(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(Arrays.asList());
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(Arrays.asList("A"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(Arrays.asList("A", "B"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(Arrays.asList("X"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(Arrays.asList("X", "Y"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(Arrays.asList((String)null));
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void addAll_notIdentical(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(Arrays.asList(A_notIdentical));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.addAll(Arrays.asList(A_notIdentical, B_notIdentical));
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void remove(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.remove("A");
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.remove("B");
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.remove("F");
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.remove("X");
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void remove_notIdentical(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.remove(A_notIdentical);
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.remove(B_notIdentical);
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void removeIf_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.removeIf(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void removeIf(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.removeIf((o) -> false);
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.removeIf((o) -> true);
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void removeAll_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.removeAll(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void removeAll(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.removeAll(Arrays.asList("B"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.removeAll(Arrays.asList("B", "C", "D"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.removeAll(Arrays.asList("X", "Y"));
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void removeAll_notIdentical(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.removeAll(Arrays.asList(A_notIdentical, "B", B_notIdentical, "D"));
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void retainAll_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.retainAll(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void retainAll(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.retainAll(Arrays.asList("A"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.retainAll(Arrays.asList("B", "C", "F"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.retainAll(Arrays.asList("X", "B", "C", "X", "Y"));
		});
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.retainAll(Arrays.asList("X", "Y"));
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void retainAll_notIdentical(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.retainAll(Arrays.asList(A_notIdentical, "B", B_notIdentical, "D"));
		});
		assertCollectionUnchanged(testCase);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void clear(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(UnsupportedOperationException.class, () -> {
			this.c.clear();
		});
		assertCollectionUnchanged(testCase);
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void toArray(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		Object[] array;
		
		array= this.c.toArray();
		assertEquals(Object.class, array.getClass().getComponentType());
		assertArraySameEntries(testCase.values, array);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void toArray_Array_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(NullPointerException.class, () -> {
			this.c.toArray((Object[])null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void toArray_Array(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		Object[] arrayIn, array;
		
		arrayIn= new String[testCase.size];
		array= this.c.toArray(arrayIn);
		assertSame(arrayIn, array);
		assertArraySameEntries(testCase.values, array);
		
		arrayIn= new String[0];
		array= this.c.toArray(arrayIn);
		assertEquals(String.class, array.getClass().getComponentType());
		assertArraySameEntries(testCase.values, array);
		
		arrayIn= new Object[0];
		array= this.c.toArray(arrayIn);
		assertEquals(Object.class, array.getClass().getComponentType());
		assertArraySameEntries(testCase.values, array);
		
		arrayIn= new String[testCase.size + 10];
		Arrays.fill(arrayIn, "X");
		array= this.c.toArray(arrayIn);
		assertSame(arrayIn, array);
		assertArraySameEntries(testCase.values, Arrays.copyOfRange(array, 0, testCase.size));
		assertNull(array[testCase.size]);
		assertEquals("X", array[testCase.size + 1]);
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void toArray_Generator_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(NullPointerException.class, () -> {
			this.c.toArray((IntFunction<Object[]>)null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void toArray_Generator(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		Object[] array;
		
		array= this.c.toArray(String[]::new);
		assertEquals(String[].class, array.getClass());
		assertArraySameEntries(testCase.values, array);
		
		array= this.c.toArray(Object[]::new);
		assertEquals(Object[].class, array.getClass());
		assertArraySameEntries(testCase.values, array);
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void map_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(NullPointerException.class, () -> {
			this.c.map(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void map(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		final Function<@Nullable String, Integer> mapping= (s) -> Integer.valueOf(Objects.hash(s));
		final ImCollection.MappingResult<Integer> result= this.c.map(mapping);
		final var expectedArray= Arrays.stream(testCase.values).map(mapping).toArray();
		assertArrayEquals(expectedArray, result.toArray());
		
		final ImList<Integer> resultList= result.toList();
		assertNotNull(resultList);
		assertArrayEquals(expectedArray, resultList.toArray());
		
		final ImIdentityList<Integer> resultIdentityList= result.toIdentityList();
		assertNotNull(resultIdentityList);
		assertArrayEquals(expectedArray, resultIdentityList.toArray());
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void findFirst_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(NullPointerException.class, () -> {
			this.c.findFirst(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void findFirst(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertNull(this.c.findFirst((final String s) -> false));
		if (testCase.size >= 1) {
			assertEquals(testCase.values[0], this.c.findFirst((final String s) -> true));
		}
		else {
			assertNull(this.c.findFirst((final String s) -> true));
		}
		
		for (int i= 0; i < testCase.values.length; i++) {
			final var v= testCase.values[i];
			assertEquals(v, this.c.findFirst((s) -> (Objects.equals(s, v))));
		}
		assertNull(this.c.findFirst((s) -> (Objects.equals(s, "X"))));
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void anyMatch_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(NullPointerException.class, () -> {
			this.c.anyMatch(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void anyMatch(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertFalse(this.c.anyMatch((final String s) -> false));
		if (testCase.size >= 1) {
			assertTrue(this.c.anyMatch((final String s) -> true));
		}
		else {
			assertFalse(this.c.anyMatch((final String s) -> true));
		}
		
		for (int i= 0; i < testCase.values.length; i++) {
			final var v= testCase.values[i];
			assertTrue(this.c.anyMatch((s) -> (Objects.equals(s, v))));
		}
		assertFalse(this.c.anyMatch((s) -> (Objects.equals(s, "X"))));
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void allMatch_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(NullPointerException.class, () -> {
			this.c.allMatch(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void allMatch(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertTrue(this.c.allMatch((final String s) -> true));
		if (testCase.size >= 1) {
			assertFalse(this.c.allMatch((final String s) -> false));
		}
		else {
			assertTrue(this.c.allMatch((final String s) -> false));
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("null")
	public void noneMatch_argCheck(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertThrows(NullPointerException.class, () -> {
			this.c.noneMatch(null);
		});
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void noneMatch(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertTrue(this.c.noneMatch((final String s) -> false));
		if (testCase.size >= 1) {
			assertFalse(this.c.noneMatch((final String s) -> true));
		}
		else {
			assertTrue(this.c.noneMatch((final String s) -> true));
		}
		
		for (int i= 0; i < testCase.values.length; i++) {
			final var v= testCase.values[i];
			assertFalse(this.c.noneMatch((s) -> (Objects.equals(s, v))));
		}
		assertTrue(this.c.noneMatch((s) -> (Objects.equals(s, "X"))));
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void hashCode(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		final var expected= createJavaImpl(testCase);
		assertEquals(expected.hashCode(), this.c.hashCode());
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	@SuppressWarnings("unlikely-arg-type")
	public void equals(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertFalse(this.c.equals(null));
		assertFalse(this.c.equals("X"));
		
		assertTrue(this.c.equals(this.c));
		
		final var expected= createJavaImpl(testCase);
		assertTrue(this.c.equals(expected));
		
		// different size
		expected.add("X");
		assertFalse(this.c.equals(expected));
		
		// same size
		if (testCase.size >= 1) {
			expected.remove(testCase.values[0]);
			assertFalse(this.c.equals(expected));
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void toString(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		assertEquals(Arrays.toString(testCase.values), this.c.toString());
	}
	
	
	private static int indexOf(final String[] values, final @Nullable String value) {
		for (int i= 0; i < values.length; i++) {
			if (values[i] == value) {
				return i;
			}
		}
		return -1;
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void toList(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		final ImList<@Nullable String> list= ImCollections.toList(this.c);
		assertInstanceOf(ImList.class, list);
		assertFalse(list instanceof IdentityList);
		assertFalse(list instanceof Set);
		
		if (!(this.c instanceof IdentityList) && !(this.c instanceof Set)) {
			assertSame(this.c, list);
		}
		else {
			assertEquals(this.c.size(), list.size());
			assertArraySameEntries(this.c.toArray(), list.toArray());
		}
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void toIdentityList(final TestCase<C> testCase) {
		this.c= createCollection(testCase);
		
		final ImList<@Nullable String> list= ImCollections.toIdentityList(this.c);
		assertInstanceOf(ImList.class, list);
		assertInstanceOf(ImIdentityList.class, list);
		assertFalse(list instanceof Set);
		
		if (this.c instanceof IdentityList && !(this.c instanceof Set)) {
			assertSame(this.c, list);
		}
		else {
			assertEquals(this.c.size(), list.size());
			assertArraySameEntries(this.c.toArray(), list.toArray());
		}
	}
	
}
