/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.statet.internal.jcommons.collections.ImArrayIdentitySet;
import org.eclipse.statet.internal.jcommons.collections.ImEmptyIdentitySet;
import org.eclipse.statet.internal.jcommons.collections.ImSingletonIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImSeqedIdentitySetTest extends AbstractImSeqSetTest {
	
	
	public ImSeqedIdentitySetTest() {
		super(ImCollections::newIdentitySet);
	}
	
	
	@Override
	@SuppressWarnings("cast")
	protected void assertExpectedType(final @Nullable TestCase<ImSequencedSet<@Nullable String>> testCase,
			final int variant) {
		assertTrue(this.c instanceof ImSet);
		assertTrue(this.c instanceof ImSequencedSet);
		assertTrue(this.c instanceof ImIdentitySet);
		assertTrue(this.c instanceof ImSequencedIdentitySet);
		assertFalse(this.c instanceof ImList);
		
		switch (this.c.size()) {
		case 0:
			assertSame(this.c, ImEmptyIdentitySet.INSTANCE);
			break;
		case 1:
			assertSame(this.c.getClass(), ImSingletonIdentitySet.class);
			break;
		default:
			assertSame(this.c.getClass(), ImArrayIdentitySet.class);
			break;
		}
	}
	
}
