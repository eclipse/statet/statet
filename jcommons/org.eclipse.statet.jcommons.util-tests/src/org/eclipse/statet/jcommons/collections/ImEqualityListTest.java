/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import org.eclipse.statet.internal.jcommons.collections.ImArrayList;
import org.eclipse.statet.internal.jcommons.collections.ImArraySub0List;
import org.eclipse.statet.internal.jcommons.collections.ImArraySubList;
import org.eclipse.statet.internal.jcommons.collections.ImEmptyList;
import org.eclipse.statet.internal.jcommons.collections.ImSingletonList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImEqualityListTest extends AbstractImListTest {
	
	
	public ImEqualityListTest() {
		super(ImCollections::newList);
	}
	
	
	@Override
	protected void assertExpectedType(final @Nullable TestCase<ImList<@Nullable String>> testCase,
			final int variant) {
		assertInstanceOf(ImList.class, this.c);
		assertFalse(this.c instanceof IdentityList);
		assertFalse(this.c instanceof Set);
		
		switch (this.c.size()) {
		case 0:
			assertSame(ImEmptyList.INSTANCE, this.c);
			break;
		case 1:
			assertSame(ImSingletonList.class, this.c.getClass());
			break;
		default:
			if ((testCase instanceof SubListCase || (variant & SUB) != 0)
					&& (variant & REV) == 0 ) {
				assertTrue(ImArraySubList.class == this.c.getClass() 
						|| ImArraySub0List.class == this.c.getClass() );
				break;
			}
			assertSame(ImArrayList.class, this.c.getClass());
			break;
		}
	}
	
}
