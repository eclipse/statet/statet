/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Lines in a text (document)
 * 
 * All indexes are 0-based
 */
@NonNullByDefault
public interface TextLineInformation {
	
	
	/**
	 * Returns the number of lines.
	 * 
	 * @return count
	 */
	int getNumberOfLines();
	
	/**
	 * Returns the line of the specified offset.
	 * 
	 * @param offset 
	 * @return the line of the offset
	 * @throws IllegalArgumentException if offset is out of bounds
	 */
	int getLineOfOffset(final int offset);
	
	/**
	 * Returns the offset of the specified line.
	 * 
	 * @param line
	 * @return the offset of the line
	 * @throws IllegalArgumentException if line is out of bounds
	 */
	int getStartOffset(final int line);
	
	/**
	 * Returns the end offset of the specified line.
	 * 
	 * Equivalent to <code>getLineStartOffset(line) + getLineLength(line)</code>
	 * 
	 * @param line
	 * @return the end offset of the line
	 * @throws IllegalArgumentException if line is out of bounds
	 */
	int getEndOffset(final int line);
	
	/**
	 * Returns the length (including the line delimiters) of the specified line.
	 * 
	 * @param line
	 * @return the offset of the line
	 * @throws IllegalArgumentException if line is out of bounds
	 */
	int getLength(final int line);
	
	/**
	 * Returns the region (including the line delimiters) of the specified line.
	 * 
	 * @param line
	 * @return the region of the line
	 * @throws IllegalArgumentException if line is out of bounds
	 */
	TextRegion getRegion(final int line);
	
}
