/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface TextRegion {
	
	
	/**
	 * Returns the offset the region starts.
	 *
	 * @return the start offset.
	 */
	int getStartOffset();
	
	/**
	 * Returns the offset the region ends.
	 *
	 * @return the end offset, exclusive.
	 */
	int getEndOffset();
	
	/**
	 * Returns the length of the region.
	 *
	 * @return the length.
	 */
	int getLength();
	
	
	default boolean isEmpty() {
		return (getLength() == 0);
	}
	
	default boolean contains(final int offset) {
		return (offset >= getStartOffset() && offset < getEndOffset());
	}
	
	default boolean contains(final TextRegion region) {
		return (region.getStartOffset() >= getStartOffset()
				&& region.getEndOffset() <= getEndOffset() );
	}
	
	
	default TextRegion expansion(final TextRegion region) {
		final int thisStartOffset= getStartOffset();
		final int thisEndOffset= getEndOffset();
		final int startOffset= Math.min(thisStartOffset, region.getStartOffset());
		final int endOffset= Math.max(thisEndOffset, region.getEndOffset());
		return (startOffset == thisStartOffset && endOffset == thisEndOffset) ?
				this : new BasicTextRegion(startOffset, endOffset);
	}
	
	default boolean intersectsNonEmpty(final TextRegion region) {
		final int startOffset= Math.max(getStartOffset(), region.getStartOffset());
		final int endOffset= Math.min(getEndOffset(), region.getEndOffset());
		return (startOffset < endOffset);
	}
	
	default @Nullable TextRegion intersectionNonEmpty(final TextRegion region) {
		final int thisStartOffset= getStartOffset();
		final int thisEndOffset= getEndOffset();
		final int startOffset= Math.max(thisStartOffset, region.getStartOffset());
		final int endOffset= Math.min(thisEndOffset, region.getEndOffset());
		if (startOffset < endOffset) {
			return (startOffset == thisStartOffset && endOffset == thisEndOffset) ?
					this : new BasicTextRegion(startOffset, endOffset);
		}
		return null;
	}
	
}
