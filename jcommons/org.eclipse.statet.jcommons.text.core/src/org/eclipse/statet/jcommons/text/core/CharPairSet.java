/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core;

import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CharPair;


@NonNullByDefault
public class CharPairSet {
	
	
	public static abstract class CharMatch {
		
		
		protected final CharPair pair;
		
		private final int pairIndex;
		
		
		public CharMatch(final CharPair pair, final int pairIndex) {
			this.pair= pair;
			this.pairIndex= pairIndex;
		}
		
		
		public final CharPair getPair() {
			return this.pair;
		}
		
		public final int getPairIndex() {
			return this.pairIndex;
		}
		
		public abstract char getChar();
		
		public abstract boolean isOpening();
		public abstract boolean isClosing();
		
		public abstract char getMatchingChar();
		
	}
	
	private static final class OpeningMatch extends CharMatch {
		
		
		public OpeningMatch(final CharPair pair, final int pairIndex) {
			super(pair, pairIndex);
		}
		
		
		@Override
		public char getChar() {
			return this.pair.opening;
		}
		
		@Override
		public boolean isOpening() {
			return true;
		}
		
		@Override
		public boolean isClosing() {
			return false;
		}
		
		@Override
		public char getMatchingChar() {
			return this.pair.closing;
		}
		
	}
	
	private static final class ClosingMatch extends CharMatch {
		
		
		public ClosingMatch(final CharPair pair, final int pairIndex) {
			super(pair, pairIndex);
		}
		
		
		@Override
		public char getChar() {
			return this.pair.closing;
		}
		
		@Override
		public boolean isOpening() {
			return false;
		}
		
		@Override
		public boolean isClosing() {
			return true;
		}
		
		@Override
		public char getMatchingChar() {
			return this.pair.opening;
		}
		
	}
	
	
	private final ImIdentityList<CharPair> pairs;
	
	private final char escapeChar;
	
	private final @Nullable CharMatch[] chars= new @Nullable CharMatch[127];
	
	
	public CharPairSet(final ImIdentityList<CharPair> pairs, final char escapeChar) {
		this.pairs= pairs;
		this.escapeChar= escapeChar;
		
		for (int pairIndex= 0; pairIndex < pairs.size(); pairIndex++) {
			final CharPair pair= pairs.get(pairIndex);
			if (this.chars[pair.opening] != null || this.chars[pair.closing] != null) {
				throw new IllegalArgumentException();
			}
			this.chars[pair.opening]= new OpeningMatch(pair, pairIndex);
			this.chars[pair.closing]= new ClosingMatch(pair, pairIndex);
		}
	}
	
	public CharPairSet(final ImIdentityList<CharPair> pairs) {
		this(pairs, (char)0);
	}
	
	
	public final ImIdentityList<CharPair> getPairs() {
		return this.pairs;
	}
	
	public final int getPairCount() {
		return this.pairs.size();
	}
	
	public final int getPairIndex(final CharPair pair) {
		return this.pairs.indexOf(pair);
	}
	
	public final char getEscapeChar() {
		return this.escapeChar;
	}
	
	public @Nullable CharMatch getMatch(final int c) {
		return (c > 0 && c < 127) ? this.chars[c] : null;
	}
	
	
	@Override
	public int hashCode() {
		return this.pairs.hashCode() + this.escapeChar * 31;
	}
	
	@Override
	public boolean equals(@Nullable final Object obj) {
		return (this == obj
				|| (obj instanceof final CharPairSet other
						&& this.pairs.equals(other.pairs)
						&& this.escapeChar == other.escapeChar ));
	}
	
}
