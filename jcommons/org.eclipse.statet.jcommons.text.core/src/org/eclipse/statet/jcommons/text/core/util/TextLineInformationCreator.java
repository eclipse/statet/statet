/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.text.core.util;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.BasicTextLineInformation;
import org.eclipse.statet.jcommons.text.core.TextLineInformation;


@NonNullByDefault
public class TextLineInformationCreator {
	
	
	private int[] buffer= new int[2048];
	
	
	public TextLineInformationCreator() {
	}
	
	
	public TextLineInformation create(final String text) {
		int[] lines= this.buffer;
		lines[0]= 0;
		int n= 1;
		for (int offset= 0; offset < text.length(); ) {
			final int c= text.charAt(offset++);
			switch (c) {
			case '\r':
				if (offset < text.length() && text.charAt(offset) == '\n') {
					offset++;
				}
				break;
			case '\n':
				break;
			default:
				continue;
			}
			if (n >= lines.length) {
				this.buffer= lines= Arrays.copyOf(lines, lines.length + 2048);
			}
			lines[n++]= offset;
		}
		if (n >= lines.length) {
			this.buffer= lines= Arrays.copyOf(lines, lines.length + 2048);
		}
		lines[n++]= text.length();
		return new BasicTextLineInformation(Arrays.copyOf(lines, n));
	}
	
}
