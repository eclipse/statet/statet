/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core.ssh.jsch;

import static org.eclipse.statet.jcommons.net.CommonsNet.LOCAL_LOOPBACK_STRING;
import static org.eclipse.statet.jcommons.util.TimeUtils.toMillisInt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.InetSocketAddress;
import java.time.Duration;

import com.jcraft.jsch.ChannelDirectTCPIP;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import org.eclipse.statet.jcommons.io.IOUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.core.RemoteProcess;
import org.eclipse.statet.jcommons.net.core.TunnelClientSocketImpl;
import org.eclipse.statet.jcommons.net.core.ssh.BasicSshClientSession;
import org.eclipse.statet.jcommons.net.core.ssh.SshTarget;
import org.eclipse.statet.jcommons.runtime.ProcessConfig;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public abstract class JSchClientSession extends BasicSshClientSession<Session> {
	
	
	public JSchClientSession(final SshTarget target,
			final @Nullable Duration timeout) {
		super(target, timeout);
	}
	
	
	@Override
	protected boolean isConnected(final Session session) {
		return session.isConnected();
	}
	
	@Override
	protected void disconnect(final @Nullable Session session) {
		if (session != null) {
			session.disconnect();
		}
	}
	
	
	@Override
	protected RemoteProcess exec(final Session session,
			final ProcessConfig processConfig,
			final @Nullable Duration timeout, final ProgressMonitor m) throws Exception {
		final var channel= (ChannelExec)session.openChannel("exec"); //$NON-NLS-1$
		if (channel == null) {
			throw new IOException("Failed to create SSH 'exec' channel");
		}
		channel.setCommand(processConfig.getCommandString());
		for (final var entry : processConfig.getEnvironmentVars().entrySet()) {
			channel.setEnv(entry.getKey(), entry.getValue());
		}
		
		final InputStream in= processConfig.getInputStream();
		final OutputStream out= IOUtils.orNullStream(processConfig.getOutputStream());
		channel.setInputStream(in);
		channel.setOutputStream(out, false);
		channel.setErrStream(out, true);
		
		channel.connect(toMillisInt(timeout, 0));
		
		return new JSchExecProcess(processConfig.getCommandString(), channel);
	}
	
	@Override
	protected TunnelClientSocketImpl createDirectTcpIpSocketImpl(final Session session
			) throws Exception {
		return new JSchTunnelClientSocketImpl(session);
	}
	
	@Override
	protected InetSocketAddress startPortForwardingL(final Session session,
			final InetSocketAddress targetAddress,
			final @Nullable Duration timeout) throws Exception {
		final var localHostString= LOCAL_LOOPBACK_STRING;
		final var localPortNum= session.setPortForwardingL(localHostString, 0,
				targetAddress.getHostString(), targetAddress.getPort(),
				null, toMillisInt(timeout, 0) );
		return new InetSocketAddress(localHostString, localPortNum);
	}
	
	@Override
	protected void stopPortForwardingL(final Session session,
			final InetSocketAddress targetAddress, final InetSocketAddress localAddress,
			final @Nullable Duration timeout) throws Exception {
		session.delPortForwardingL(localAddress.getHostString(), localAddress.getPort());
	}
	
	
	private static class JSchExecProcess extends RemoteProcess {
		
		
		private final ChannelExec channel;
		
		
		public JSchExecProcess(final String command, final ChannelExec channel) {
			super(command);
			this.channel= channel;
		}
		
		
		@Override
		public boolean isAlive() {
			return (this.channel.isConnected()
					&& this.channel.getExitStatus() < 0 );
		}
		
		@Override
		public int getExitStatusCode() {
			final boolean closed= !this.channel.isConnected();
			final int statusCode= this.channel.getExitStatus();
			if (statusCode < 0) {
				if (closed) {
					return MISSING_EXIT_STATUS_CODE;
				}
				throw new IllegalThreadStateException("The process has not terminated");
			}
			return statusCode;
		}
		
		@Override
		public void close() {
			this.channel.disconnect();
		}
		
	}
	
	private static class JSchTunnelClientSocketImpl extends TunnelClientSocketImpl {
		
		
		private final Session session;
		
		private volatile @Nullable ChannelDirectTCPIP channel;
		
		
		public JSchTunnelClientSocketImpl(final Session session) throws IOException {
			this.session= session;
			this.tcpNoDelay= true;
		}
		
		
		@Override
		protected void connectRemote(final InetSocketAddress targetAddress,
				final @Nullable Duration timeout, final ProgressMonitor m) throws IOException, StatusException {
			try {
				final var channel= (ChannelDirectTCPIP)this.session.openChannel("direct-tcpip"); //$NON-NLS-1$
				if (channel == null) {
					throw new IOException("Failed to create SSH 'direct-tcpip' channel");
				}
				channel.setHost(targetAddress.getHostString());
				channel.setPort(targetAddress.getPort());
				
				final var inputStream= new PipedInputStream(RCVBUF_DEFAULT);
				channel.setOutputStream(new PipedOutputStream(inputStream));
				setInputStream(inputStream);
				setOutputStream(channel.getOutputStream());
				
				this.channel= channel;
				channel.connect(toMillisInt(timeout, 0));
			}
			catch (final JSchException e) {
				final IOException ioException= new IOException();
				ioException.initCause(e);
				throw ioException;
			}
		}
		
		@Override
		protected void close() throws IOException {
			super.close();
			final var channel= this.channel;
			if (channel == null) {
				return;
			}
			
			channel.disconnect();
		}
		
	}
	
}
