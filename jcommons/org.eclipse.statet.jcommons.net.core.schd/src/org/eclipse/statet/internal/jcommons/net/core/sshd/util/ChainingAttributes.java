/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.net.core.sshd.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Collection;

import org.apache.sshd.common.AttributeRepository;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * An {@link AttributeRepository} that chains together two other attribute sources in a hierarchy.
 */
@NonNullByDefault
public class ChainingAttributes implements AttributeRepository {
	
	
	private final AttributeRepository delegate;
	
	private final AttributeRepository parent;
	
	
	/**
	 * Create a new {@link ChainingAttributes} attribute source.
	 *
	 * @param delegate to search for attributes first
	 * @param parent to search for attributes if not found in {@code delegate}
	 */
	public ChainingAttributes(final AttributeRepository delegate, final AttributeRepository parent) {
		this.delegate= delegate;
		this.parent= parent;
	}
	
	
	@Override
	public int getAttributesCount() {
		return this.delegate.getAttributesCount();
	}
	
	@Override
	public Collection<AttributeKey<?>> attributeKeys() {
		return this.delegate.attributeKeys();
	}
	
	@Override
	public <T> @Nullable T getAttribute(final AttributeKey<T> key) {
		return this.delegate.getAttribute(nonNullAssert(key));
	}
	
	@Override
	public <T> @Nullable T resolveAttribute(final AttributeKey<T> key) {
		final var value= getAttribute(nonNullAssert(key));
		if (value == null) {
			return this.parent.getAttribute(key);
		}
		return value;
	}
	
}
