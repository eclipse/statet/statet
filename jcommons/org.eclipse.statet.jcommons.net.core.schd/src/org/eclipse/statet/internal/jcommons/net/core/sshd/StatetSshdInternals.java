/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.net.core.sshd;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.InfoStatus;
import org.eclipse.statet.jcommons.status.WarningStatus;


@NonNullByDefault
public class StatetSshdInternals {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.jcommons.net.core.ssh.apache"; //$NON-NLS-1$
	
	
	private static final boolean DEBUG= false;
	
	public static void logWarning(final String message) {
		CommonsRuntime.log(new WarningStatus(BUNDLE_ID, message));
	}
	
	public static void logWarning(final String message, final Throwable cause) {
		CommonsRuntime.log(new WarningStatus(BUNDLE_ID, message, cause));
	}
	
	public static void logError(final String message, final Throwable cause) {
		CommonsRuntime.log(new ErrorStatus(BUNDLE_ID, message, cause));
	}
	
	public static boolean isDebugEnabled() {
		return DEBUG;
	}
	
	public static void logDebug(final String message) {
		if (isDebugEnabled()) {
			CommonsRuntime.log(new InfoStatus(BUNDLE_ID, "DEBUG: " + message)); //$NON-NLS-1$
		}
	}
	
	
	private StatetSshdInternals() {
	}
	
}
