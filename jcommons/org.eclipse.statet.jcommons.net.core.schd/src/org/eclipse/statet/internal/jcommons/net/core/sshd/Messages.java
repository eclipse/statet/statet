/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.net.core.sshd;

import org.eclipse.jgit.nls.NLS;
import org.eclipse.jgit.nls.TranslationBundle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public final class Messages extends TranslationBundle {
	
	
	public static Messages get() {
		return NLS.getBundleFor(Messages.class);
	}
	
	
	public String configInvalidPath;
	public String configInvalidPattern;
	public String configInvalidPositive;
	public String configInvalidProxyJump;
	public String configNoKnownAlgorithms;
	public String configUnknownAlgorithm;
	
	public String loginDenied;
	
	public String proxyJumpAbort;
	
	public String sshProxySessionCloseFailed;
	
}
