/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.net.core.sshd;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.util.Units.MILLI_NANO;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyPair;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.jgit.internal.transport.sshd.AuthenticationLogger;
import org.eclipse.jgit.transport.SshConstants;

import org.apache.sshd.client.config.hosts.HostConfigEntry;
import org.apache.sshd.common.AttributeRepository;
import org.apache.sshd.common.future.DefaultCancellableSshFuture;
import org.apache.sshd.common.future.SshFuture;
import org.apache.sshd.common.future.SshFutureListener;
import org.apache.sshd.common.future.VerifiableFuture;
import org.apache.sshd.common.keyprovider.KeyIdentityProvider;
import org.apache.sshd.common.session.SessionContext;

import org.eclipse.statet.internal.jcommons.net.core.sshd.util.ChainingAttributes;
import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.core.ssh.OpenSshConfigUtils;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.util.function.CallableE;


@NonNullByDefault
public class SshdUtils {
	
	
	public static AttributeRepository chain(final @Nullable AttributeRepository self,
			final @Nullable AttributeRepository parent) {
		if (self == null) {
			return nonNullAssert(parent);
		}
		if (parent == null || parent == self) {
			return self;
		}
		return new ChainingAttributes(self, parent);
	}
	
	public static KeyIdentityProvider toKeyIdentityProvider(final Iterable<KeyPair> keys) {
		if (keys instanceof KeyIdentityProvider) {
			return (KeyIdentityProvider)keys;
		}
		return new KeyIdentityProvider() {
			@Override
			public Iterable<KeyPair> loadKeys(final @Nullable SessionContext session) {
				return keys;
			}
		};
	}
	
	public static List<URI> determineHops(final List<URI> currentHops,
			final HostConfigEntry hostConfig, final String host) throws IOException {
		if (currentHops.isEmpty()) {
			final String jumpHosts= hostConfig.getProperty(SshConstants.PROXY_JUMP);
			if (jumpHosts != null && !jumpHosts.isEmpty()
					&& !SshConstants.NONE.equals(jumpHosts) ) {
				try {
					return OpenSshConfigUtils.parseProxyJump(jumpHosts);
				}
				catch (final URISyntaxException e) {
					throw new IOException(MessageFormat.format(Messages.get().configInvalidProxyJump,
									host, jumpHosts ),
							e );
				}
			}
		}
		return currentHops;
	}
	
	public static String attachAuthLog(final String message,
			final @Nullable AuthenticationLogger authLog) {
		if (authLog != null) {
			final String log= CollectionUtils.toString(authLog.getLog(), "\n");
			if (!log.isEmpty()) {
				return message + "\n" + log;
			}
		}
		return message;
	}
	
	
	public static <T extends SshFuture<T>> T verify(final VerifiableFuture<T> future,
			final long startTime, final @Nullable Duration timeout,
			final ProgressMonitor m) throws IOException, StatusException {
		final long timeoutTime= startTime + ((timeout != null) ? timeout.toNanos() : Long.MAX_VALUE);
		if (future instanceof DefaultCancellableSshFuture) {
			final var f= (DefaultCancellableSshFuture<T>)future;
			class FutureListener implements SshFutureListener<T> {
				@Override
				public synchronized void operationComplete(final T future) {
					notifyAll();
				}
			}
			final var listener= new FutureListener();
			f.addListener(listener);
			try {
				while (!f.isDone()) {
					if (m.isCanceled()) {
						f.cancel();
						throw new StatusException(Status.CANCEL_STATUS);
					}
					final long remaining= (timeoutTime - System.nanoTime());
					if (remaining <= 0) {
						break;
					}
					synchronized (listener) {
						try {
							listener.wait(Math.min(remaining / MILLI_NANO, 50));
						}
						catch (final InterruptedException e) {}
					}
				}
				return future.verify(0);
			}
			finally {
				f.removeListener(listener);
			}
		}
		else {
			final long remaining= (timeoutTime - System.nanoTime());
			return future.verify(remaining, TimeUnit.NANOSECONDS);
		}
	}
	
	public static <T extends SshFuture<T>> T verify(final CallableE<VerifiableFuture<T>, IOException> op,
			final @Nullable Duration timeout, final ProgressMonitor m) throws IOException, StatusException {
		final long startTime= System.nanoTime();
		final var future= op.get();
		return verify(future, startTime, timeout, m);
	}
	
	
	private SshdUtils() {
	}
	
}
