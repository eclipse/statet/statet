/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.net.core.sshd.agent;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import org.eclipse.jgit.internal.transport.sshd.JGitClientSession;
import org.eclipse.jgit.internal.transport.sshd.agent.SshAgentClient;
import org.eclipse.jgit.transport.SshConstants;
import org.eclipse.jgit.transport.sshd.agent.ConnectorFactory;

import org.apache.sshd.agent.SshAgent;
import org.apache.sshd.agent.SshAgentFactory;
import org.apache.sshd.agent.SshAgentServer;
import org.apache.sshd.common.FactoryManager;
import org.apache.sshd.common.channel.ChannelFactory;
import org.apache.sshd.common.session.ConnectionService;
import org.apache.sshd.common.session.Session;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.core.ssh.OpenSshConfigUtils;


@NonNullByDefault
public class StatetSshAgentFactory implements SshAgentFactory {
	
	
	private final ConnectorFactory factory;
	
	private final Path homeDir;
	
	
	public StatetSshAgentFactory(final ConnectorFactory factory, final Path homeDir) {
		this.factory= factory;
		this.homeDir= homeDir;
	}
	
	
	@Override
	public List<ChannelFactory> getChannelForwardingFactories(final FactoryManager manager) {
		return Collections.emptyList();
	}
	
	@Override
	public @Nullable SshAgent createClient(final @Nullable Session session,
			final FactoryManager manager) throws IOException {
		String identityAgent= null;
		if (session instanceof final JGitClientSession jgitSession) {
			final var hostConfig= jgitSession.getHostConfigEntry();
			if (hostConfig != null) {
				identityAgent= hostConfig.getProperty(SshConstants.IDENTITY_AGENT, null);
			}
		}
		if (identityAgent == null) {
			final var defaultConnector= this.factory.getDefaultConnector();
			if (defaultConnector != null) {
				identityAgent= defaultConnector.getIdentityAgent();
			}
		}
		if (OpenSshConfigUtils.parseNone(identityAgent)) {
			return null;
		}
		return new SshAgentClient(this.factory.create(identityAgent, this.homeDir.toFile()));
	}
	
	@Override
	public @Nullable SshAgentServer createServer(final ConnectionService service) throws IOException {
		return null;
	}
	
}
