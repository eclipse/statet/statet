/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core.ssh.sshd;

import static org.apache.sshd.common.SshConstants.SSH2_DISCONNECT_NO_MORE_AUTH_METHODS_AVAILABLE;

import static org.eclipse.statet.internal.jcommons.net.core.sshd.StatetSshdInternals.BUNDLE_ID;
import static org.eclipse.statet.internal.jcommons.net.core.sshd.StatetSshdInternals.isDebugEnabled;
import static org.eclipse.statet.internal.jcommons.net.core.sshd.StatetSshdInternals.logDebug;
import static org.eclipse.statet.internal.jcommons.net.core.sshd.StatetSshdInternals.logError;
import static org.eclipse.statet.jcommons.io.IOUtils.close;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketOptions;
import java.net.URI;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.eclipse.jgit.internal.transport.sshd.AuthenticationCanceledException;
import org.eclipse.jgit.internal.transport.sshd.AuthenticationLogger;
import org.eclipse.jgit.internal.transport.sshd.JGitSshClient;
import org.eclipse.jgit.transport.SshConstants;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.channel.ChannelDirectTcpip;
import org.apache.sshd.client.channel.ChannelExec;
import org.apache.sshd.client.channel.ClientChannelEvent;
import org.apache.sshd.client.config.hosts.HostConfigEntry;
import org.apache.sshd.client.session.ClientSession;
import org.apache.sshd.client.session.forward.PortForwardingTracker;
import org.apache.sshd.common.AttributeRepository;
import org.apache.sshd.common.SshException;
import org.apache.sshd.common.channel.ChannelOutputStream;
import org.apache.sshd.common.future.CloseFuture;
import org.apache.sshd.common.future.SshFutureListener;
import org.apache.sshd.common.util.io.IoUtils;
import org.apache.sshd.common.util.net.SshdSocketAddress;

import org.eclipse.statet.internal.jcommons.net.core.sshd.Messages;
import org.eclipse.statet.internal.jcommons.net.core.sshd.SshdUtils;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.io.IOUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.CommonsNet;
import org.eclipse.statet.jcommons.net.core.RemoteProcess;
import org.eclipse.statet.jcommons.net.core.TunnelClientSocketImpl;
import org.eclipse.statet.jcommons.net.core.ssh.BasicSshClientSession;
import org.eclipse.statet.jcommons.net.core.ssh.OpenSshConfigUtils;
import org.eclipse.statet.jcommons.net.core.ssh.SshTarget;
import org.eclipse.statet.jcommons.runtime.ProcessConfig;
import org.eclipse.statet.jcommons.status.CancelStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public class SshdClientSession extends BasicSshClientSession<ClientSession> {
	
	
	private static final SshdSocketAddress LOCAL_LOCALHOST_ADDRESS= new SshdSocketAddress(
			CommonsNet.LOCAL_LOOPBACK_STRING, 0);
	
	private static final int MAX_DEPTH= 10;
	
	
	private final SshClient client;
	
	
	public SshdClientSession(final SshTarget target, final @Nullable Duration timeout,
			final SshClient client) {
		super(target, timeout);
		this.client= client;
		this.client.start();
	}
	
	
	@Override
	protected ClientSession connect(
			final @Nullable Duration timeout, final ProgressMonitor m) throws Exception {
		final SshTarget target= getTarget();
		return connect(target.getUri(), ImCollections.emptyList(),
				(future) -> disconnect(),
				timeout, MAX_DEPTH, m );
	}
	
	private ClientSession connect(final URI target, List<URI> jumps,
			final @Nullable SshFutureListener<CloseFuture> listener,
			final @Nullable Duration timeout, int depth, final ProgressMonitor m) throws IOException, StatusException {
		if (--depth < 0) {
			throw new IOException(MessageFormat.format(Messages.get().proxyJumpAbort, target));
		}
		final HostConfigEntry hostConfig;
		{	final String host= target.getHost();
			if (host == null) {
				throw new IOException("The host component is missing: " + target);
			}
			hostConfig= getHostConfig(target.getUserInfo(), host, target.getPort());
			jumps= SshdUtils.determineHops(jumps, hostConfig, host);
		}
		m.setWorkRemaining((jumps.size() + 1) * 2);
		
		ClientSession resultSession= null;
		ClientSession proxySession= null;
		PortForwardingTracker portForward= null;
		try {
			if (!jumps.isEmpty()) {
				final var hop= jumps.remove(0);
				if (isDebugEnabled()) {
					logDebug(String.format("Connecting to jump host: %1$s", hop)); //$NON-NLS-1$
				}
				proxySession= connect(hop, jumps, null, timeout, depth, m);
			}
			
			AttributeRepository context= null;
			if (proxySession != null) {
				final SshdSocketAddress remoteAddress= new SshdSocketAddress(
						hostConfig.getHostName(), hostConfig.getPort() );
				portForward= proxySession.createLocalPortForwardingTracker(
						SshdSocketAddress.LOCALHOST_ADDRESS, remoteAddress );
				// We must connect to the locally bound address, not the one from the host config.
				context= AttributeRepository.ofKeyValuePair(
						JGitSshClient.LOCAL_FORWARD_ADDRESS,
						portForward.getBoundAddress() );
			}
			final var hostTimeout= OpenSshConfigUtils.parseDuration(
					hostConfig.getProperty(SshConstants.CONNECT_TIMEOUT) );
			resultSession= connect(hostConfig, context,
					(hostTimeout != null) ? hostTimeout : timeout, m );
			m.addWorked(1);
			
			if (proxySession != null) {
				final PortForwardingTracker tracker= portForward;
				final ClientSession pSession = proxySession;
				resultSession.addCloseFutureListener(future -> {
					IoUtils.closeQuietly(tracker);
					final String sessionName= pSession.toString();
					try {
						pSession.close();
					}
					catch (final IOException e) {
						logError(MessageFormat.format(Messages.get().sshProxySessionCloseFailed,
								sessionName ), e);
					}
				});
				portForward= null;
				proxySession= null;
			}
			if (listener != null) {
				resultSession.addCloseFutureListener(listener);
			}
			
			auth(resultSession, hostConfig, m);
			m.addWorked(1);
			
			return resultSession;
		}
		catch (final Exception e) {
			close(portForward, e);
			close(proxySession, e);
			close(resultSession, e);
			throw e;
		}
	}
	
	private ClientSession connect(final HostConfigEntry hostConfig,
			final @Nullable AttributeRepository context,
			final @Nullable Duration timeout, final ProgressMonitor m) throws IOException, StatusException {
		m.beginSubTask(String.format("Connecting to '%1$s'...", hostConfig.getHostName()));
		
		final var connect= SshdUtils.verify(() -> this.client.connect(hostConfig, context, null),
				timeout, m );
		return connect.getClientSession();
	}
	
	private void auth(final ClientSession session, final HostConfigEntry hostConfig,
			final ProgressMonitor m) throws IOException, StatusException {
		m.beginSubTask(String.format("Connecting to '%1$s' • Authenticate...", hostConfig.getHostName()));
		AuthenticationLogger authLog= null;
		try {
			authLog= new AuthenticationLogger(session);
			final long authStartTime= System.nanoTime();
			SshdUtils.verify(session.auth(),
					authStartTime, session.getAuthTimeout(), m );
			m.beginSubTask(String.format("Connecting to '%1$s' • Authenticate \u2713", hostConfig.getHostName()));
		}
		catch (final SshException e) {
			final String host= hostConfig.getHostName();
			final int port= hostConfig.getPort();
			if (e.getDisconnectCode() == SSH2_DISCONNECT_NO_MORE_AUTH_METHODS_AVAILABLE) {
				final String message= MessageFormat.format(Messages.get().loginDenied,
						host, Integer.toString(port));
				throw new IOException(SshdUtils.attachAuthLog(message, authLog), e);
			}
			else if (e.getCause() instanceof AuthenticationCanceledException) {
				final String message= nonNullElse(e.getCause().getMessage(), "Canceled");
				throw new StatusException(new CancelStatus(BUNDLE_ID,
						SshdUtils.attachAuthLog(message, authLog),
						e.getCause() ));
			}
			throw e;
		}
		finally {
			if (authLog != null) {
				authLog.clear();
			}
		}
	}
	
	
	@Override
	protected void disconnect(final @Nullable ClientSession session) throws Exception {
		try {
			if (session != null) {
				session.close();
			}
		}
		finally {
			this.client.close();
		}
	}
	
	
	private HostConfigEntry getHostConfig(final String username, final String host, final int port)
			throws IOException {
		final HostConfigEntry entry= this.client.getHostConfigEntryResolver()
				.resolveEffectiveHost(host, port, null, username, null, null);
		if (entry == null) {
			if (SshdSocketAddress.isIPv6Address(host)) {
				return new HostConfigEntry("", host, port, username); //$NON-NLS-1$
			}
			return new HostConfigEntry(host, host, port, username);
		}
		return entry;
	}
	
	
	@Override
	public RemoteProcess exec(final ClientSession session,
			final ProcessConfig processConfig,
			final @Nullable Duration timeout, final ProgressMonitor m) throws Exception {
		final var channel= session.createExecChannel(processConfig.getCommandString(), null,
				processConfig.getEnvironmentVars() );
		try {
			final InputStream in= processConfig.getInputStream();
			final OutputStream out= IOUtils.orNullStream(processConfig.getOutputStream());
			channel.setIn(in);
			channel.setOut(out);
			channel.setRedirectErrorStream(true);
			
			SshdUtils.verify(() -> channel.open(),
					timeout, m );
			return new SshdExecProcess(processConfig, channel);
		}
		catch (final Exception e) {
			channel.close(true);
			throw e;
		}
	}
	
	@Override
	public TunnelClientSocketImpl createDirectTcpIpSocketImpl(final ClientSession session
			) throws Exception {
		return new SshdDirectClientSocketImpl(session);
	}
	
	@Override
	protected InetSocketAddress startPortForwardingL(final ClientSession session,
			final InetSocketAddress targetAddress,
			final @Nullable Duration timeout) throws Exception {
		final var localAddress= session.startLocalPortForwarding(LOCAL_LOCALHOST_ADDRESS,
				new SshdSocketAddress(targetAddress) );
		return localAddress.toInetSocketAddress();
	}
	
	@Override
	protected void stopPortForwardingL(final ClientSession session,
			final InetSocketAddress targetAddress, final InetSocketAddress localAddress,
			final @Nullable Duration timeout) throws Exception {
		session.stopLocalPortForwarding(new SshdSocketAddress(localAddress));
	}
	
	
	private static class SshdExecProcess extends RemoteProcess {
		
		
		private static final Set<ClientChannelEvent> EXIT_EVENTS= EnumSet.of(ClientChannelEvent.EXIT_STATUS, ClientChannelEvent.CLOSED);
		
		
		private final ChannelExec channel;
		
		
		public SshdExecProcess(final ProcessConfig processConfig, final ChannelExec channel) {
			super(processConfig.getCommandString());
			this.channel= channel;
		}
		
		
		@Override
		public boolean isAlive() {
			return (!this.channel.isClosed()
					&& this.channel.getExitStatus() == null );
		}
		
		@Override
		public boolean waitFor(final @Nullable Duration timeout) throws InterruptedException {
			this.channel.waitFor(EXIT_EVENTS, timeout);
			return isAlive();
		}
		
		@Override
		public int waitFor(final @Nullable Duration timeout,
				final ProgressMonitor m) throws StatusException {
			final Object futureLock= this.channel.getFutureLock();
			if (timeout != null) {
				final long timeoutTime= System.nanoTime() + timeout.toNanos();
				synchronized (futureLock) {
					while (isAlive()) {
						if (m.isCanceled()) {
							throw newCancelException();
						}
						final long remainingNanos= timeoutTime - System.nanoTime();
						if (remainingNanos <= 0) {
							throw newTimeoutException(timeout);
						}
						try {
							futureLock.wait(Math.min(TimeUnit.NANOSECONDS.toMillis(remainingNanos), 100));
						}
						catch (final InterruptedException e) {}
					}
				}
				return getExitStatusCode();
			}
			else {
				while (isAlive()) {
					if (m.isCanceled()) {
						throw newCancelException();
					}
					synchronized (futureLock) {
						if (!isAlive()) {
							break;
						}
						try {
							futureLock.wait(100);
						}
						catch (final InterruptedException e) {}
					}
				}
				return getExitStatusCode();
			}
		}
		
		@Override
		public int getExitStatusCode() {
			final boolean closed= this.channel.isClosed();
			final Integer exitCode= this.channel.getExitStatus();
			if (exitCode == null) {
				if (closed) {
					return MISSING_EXIT_STATUS_CODE;
				}
				throw new IllegalThreadStateException("The process has not terminated");
			}
			return exitCode.intValue();
		}
		
		@Override
		public void close() {
			if (this.channel.isOpen()) {
				this.channel.close(false);
			}
		}
		
	}
	
	private static class SshdDirectClientSocketImpl extends TunnelClientSocketImpl {
		
		
		private final ClientSession session;
		
		private @Nullable ChannelDirectTcpip channel;
		
		
		public SshdDirectClientSocketImpl(final ClientSession session) throws IOException {
			this.session= session;
		}
		
		
		@Override
		protected void connectRemote(final InetSocketAddress targetAddress,
				final @Nullable Duration timeout, final ProgressMonitor m) throws IOException, StatusException {
			final var channel= this.session.createDirectTcpipChannel(LOCAL_LOCALHOST_ADDRESS,
					new SshdSocketAddress(targetAddress) );
			this.channel= channel;
			SshdUtils.verify(() -> channel.open(),
					timeout, m );
			
			final var in= channel.getInvertedOut();
			setInputStream(in);
			final var out= (ChannelOutputStream)channel.getInvertedIn();
			out.setNoDelay(this.tcpNoDelay);
			setOutputStream(out);
		}
		
		@Override
		protected void close() throws IOException {
			super.close();
			final var channel= this.channel;
			if (channel == null) {
				return;
			}
			
			channel.close();
		}
		
		
		@Override
		public void setOption(final int optID, final Object value) throws SocketException {
			switch (optID) {
			case SocketOptions.TCP_NODELAY:
				{	final boolean tcpNoDelay= ((Boolean)value).booleanValue();
					if (tcpNoDelay != this.tcpNoDelay) {
						this.tcpNoDelay= tcpNoDelay;
						try {
							((ChannelOutputStream)getOutputStream()).setNoDelay(tcpNoDelay);
						}
						catch (final Exception e) {}
					}
					return;
				}
			default:
				break;
			}
			super.setOption(optID, value);
		}
		
		
	}
	
}
