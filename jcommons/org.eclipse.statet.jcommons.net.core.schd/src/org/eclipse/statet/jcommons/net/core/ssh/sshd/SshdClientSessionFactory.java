/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core.ssh.sshd;

import static org.eclipse.statet.internal.jcommons.net.core.sshd.SshdUtils.toKeyIdentityProvider;
import static org.eclipse.statet.jcommons.lang.SystemUtils.getLocalUserName;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.security.KeyPair;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import org.eclipse.jgit.internal.transport.ssh.OpenSshConfigFile;
import org.eclipse.jgit.internal.transport.sshd.CachingKeyPairProvider;
import org.eclipse.jgit.internal.transport.sshd.GssApiWithMicAuthFactory;
import org.eclipse.jgit.internal.transport.sshd.JGitPublicKeyAuthFactory;
import org.eclipse.jgit.internal.transport.sshd.JGitServerKeyVerifier;
import org.eclipse.jgit.internal.transport.sshd.JGitSshClient;
import org.eclipse.jgit.internal.transport.sshd.JGitSshConfig;
import org.eclipse.jgit.internal.transport.sshd.JGitUserInteraction;
import org.eclipse.jgit.internal.transport.sshd.OpenSshServerKeyDatabase;
import org.eclipse.jgit.internal.transport.sshd.PasswordProviderWrapper;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.SshConfigStore;
import org.eclipse.jgit.transport.SshConstants;
import org.eclipse.jgit.transport.sshd.DefaultProxyDataFactory;
import org.eclipse.jgit.transport.sshd.KeyPasswordProvider;
import org.eclipse.jgit.transport.sshd.KeyPasswordProviderFactory;
import org.eclipse.jgit.transport.sshd.ProxyDataFactory;
import org.eclipse.jgit.transport.sshd.ServerKeyDatabase;
import org.eclipse.jgit.transport.sshd.agent.ConnectorFactory;

import org.apache.sshd.client.ClientBuilder;
import org.apache.sshd.client.auth.UserAuthFactory;
import org.apache.sshd.client.auth.keyboard.UserAuthKeyboardInteractiveFactory;
import org.apache.sshd.client.auth.password.UserAuthPasswordFactory;
import org.apache.sshd.client.config.hosts.HostConfigEntryResolver;
import org.apache.sshd.common.compression.BuiltinCompressions;
import org.apache.sshd.common.config.keys.FilePasswordProvider;
import org.apache.sshd.common.keyprovider.KeyIdentityProvider;

import org.eclipse.statet.internal.jcommons.net.core.sshd.agent.StatetSshAgentFactory;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.io.FileUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.core.ssh.BasicSshClientSessionFactory;
import org.eclipse.statet.jcommons.net.core.ssh.OpenSshConfigUtils;
import org.eclipse.statet.jcommons.net.core.ssh.SshClientSession;
import org.eclipse.statet.jcommons.net.core.ssh.SshTarget;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public class SshdClientSessionFactory extends BasicSshClientSessionFactory {
	
	
	private static final ImList<UserAuthFactory> USER_AUTH_FACTORIES= ImCollections.newList(
			// About the order of password and keyboard-interactive, see upstream
			// bug https://issues.apache.org/jira/projects/SSHD/issues/SSHD-866 .
			// Password auth doesn't have this problem.
			GssApiWithMicAuthFactory.INSTANCE,
			JGitPublicKeyAuthFactory.FACTORY,
			UserAuthPasswordFactory.INSTANCE,
			UserAuthKeyboardInteractiveFactory.INSTANCE );
	
	private static final ImList<String> DEFAULT_IDENTITY_NAMES= ImCollections.newList(
			SshConstants.DEFAULT_IDENTITIES );
	
	
	protected static class ConfigBase {
		
		private final Path homeDir;
		
		private final Path sshDataDir;
		
		public ConfigBase(final Path homeDir, final Path sshDir) {
			this.homeDir= homeDir.toAbsolutePath().normalize();
			this.sshDataDir= sshDir.toAbsolutePath().normalize();
		}
		
		
		public final Path getHomeDir() {
			return this.homeDir;
		}
		
		public final Path getSshDataDir() {
			return this.sshDataDir;
		}
		
		
		@Override
		public int hashCode() {
			return 31 * this.homeDir.hashCode() + this.homeDir.hashCode();
		}
		
		@Override
		public boolean equals(final @Nullable Object obj) {
			return (obj == this
					|| (obj instanceof final ConfigBase other
							&& this.homeDir.equals(other.homeDir)
							&& this.sshDataDir.equals(other.sshDataDir) ));
		}
		
	}
	
	
	private final Map<ConfigBase, HostConfigEntryResolver> defaultHostConfigEntryResolver= new ConcurrentHashMap<>();
		
	private final Map<ConfigBase, ServerKeyDatabase> defaultServerKeyDatabase= new ConcurrentHashMap<>();
	
	private final Map<ImList<Path>, Iterable<KeyPair>> defaultKeys= new ConcurrentHashMap<>();
	
	private final @Nullable ProxyDataFactory proxies;
	
	
	public SshdClientSessionFactory(final @Nullable ProxyDataFactory proxies) {
		this.proxies= proxies;
	}
	
	public SshdClientSessionFactory() {
		this(new DefaultProxyDataFactory());
	}
	
	
	protected Path getSshDataDirectory() {
		return getUserHomeDirectory().resolve(OpenSshConfigUtils.DATA_DIR_NAME);
	}
	
	private HostConfigEntryResolver getHostConfigEntryResolver(final ConfigBase configBase) {
		return this.defaultHostConfigEntryResolver.computeIfAbsent(configBase,
				(t) -> new JGitSshConfig(
						createSshConfigStore(configBase, getLocalUserName()) ));
	}
	
	protected @Nullable SshConfigStore createSshConfigStore(final ConfigBase configBase,
			final String localUserName) {
		final Path configFile= configBase.getSshDataDir().resolve(OpenSshConfigUtils.CONFIG_FILE_NAME);
		return new OpenSshConfigFile(configBase.getHomeDir().toFile(), configFile.toFile(),
				localUserName );
	}
	
	protected List<Path> getDefaultIdentities(final ConfigBase configBase) {
		return FileUtils.getRegularFiles(configBase.getSshDataDir(), DEFAULT_IDENTITY_NAMES);
	}
	
	protected Iterable<KeyPair> getDefaultKeys(final ConfigBase configBase) {
		final List<Path> defaultIdentities= getDefaultIdentities(configBase);
		return this.defaultKeys.computeIfAbsent(ImCollections.toList(defaultIdentities),
				(t) -> new CachingKeyPairProvider(defaultIdentities, null) );
	}
	
	
	protected ServerKeyDatabase getServerKeyDatabase(final ConfigBase configBase) {
		return this.defaultServerKeyDatabase.computeIfAbsent(configBase, this::createServerKeyDatabase);
	}
	
	protected ServerKeyDatabase createServerKeyDatabase(final ConfigBase configBase) {
		return new OpenSshServerKeyDatabase(true, getDefaultKnownHostsFiles(configBase));
	}
	
	protected List<Path> getDefaultKnownHostsFiles(final ConfigBase configBase) {
		return ImCollections.newList(
				configBase.getSshDataDir().resolve(SshConstants.KNOWN_HOSTS),
				configBase.getSshDataDir().resolve(SshConstants.KNOWN_HOSTS + '2') );
	}
	
	
	/**
	 * Gets the list of default preferred authentication mechanisms. If
	 * {@code null} is returned the openssh default list will be in effect. If
	 * the ssh config defines {@code PreferredAuthentications} the value from
	 * the ssh config takes precedence.
	 *
	 * @return a comma-separated list of mechanism names, or {@code null} if none
	 */
	protected @Nullable String getDefaultPreferredAuthenticationMethodsString() {
		return null;
	}
	
	protected @Nullable KeyPasswordProvider createKeyPasswordProvider(final @Nullable CredentialsProvider credentialsProvider) {
		return null;
	}
	
	private FilePasswordProvider createFilePasswordProvider(final Supplier<KeyPasswordProvider> providerFactory) {
		return new PasswordProviderWrapper(providerFactory);
	}
	
	protected @Nullable ConnectorFactory getConnectorFactory() {
		return ConnectorFactory.getDefault();
	}
	
	
	@Override
	protected SshClientSession createSession(final SshTarget target,
			final @Nullable Duration timeout, final ProgressMonitor m) throws StatusException {
		m.setWorkRemaining(1 + 3);
		
		final var client= createClient(target, getCreditalsProvider());
		final var session= new SshdClientSession(target, timeout, client);
		m.addWorked(1);
		
		session.connect(m);
		return session;
	}
	
	protected @Nullable CredentialsProvider getCreditalsProvider() {
		return CredentialsProvider.getDefault();
	}
	
	protected JGitSshClient createClient(final SshTarget target,
			final @Nullable CredentialsProvider credentialsProvider) throws StatusException {
		final ConfigBase configBase= new ConfigBase(getUserHomeDirectory(), getSshDataDirectory());
		final HostConfigEntryResolver configFile= getHostConfigEntryResolver(configBase);
		final KeyIdentityProvider defaultKeysProvider= toKeyIdentityProvider(getDefaultKeys(configBase));
		final Supplier<KeyPasswordProvider> passwordProviderFactory= () -> {
					final var provider= createKeyPasswordProvider(credentialsProvider);
					return (provider != null) ?
							provider :
							KeyPasswordProviderFactory.getInstance().apply(credentialsProvider);
				};
		final var client= (JGitSshClient)ClientBuilder.builder()
				.factory(JGitSshClient::new)
				.filePasswordProvider(createFilePasswordProvider(passwordProviderFactory))
				.hostConfigEntryResolver(configFile)
				.serverKeyVerifier(new JGitServerKeyVerifier(getServerKeyDatabase(configBase)))
				.compressionFactories(ImCollections.toList(BuiltinCompressions.VALUES))
				.build();
		client.setUserInteraction(
				new JGitUserInteraction(credentialsProvider));
		client.setUserAuthFactories(USER_AUTH_FACTORIES);
		client.setKeyIdentityProvider(defaultKeysProvider);
		client.setKeyPasswordProviderFactory(passwordProviderFactory);
		final var connectorFactory= getConnectorFactory();
		if (connectorFactory != null) {
			client.setAgentFactory(
					new StatetSshAgentFactory(connectorFactory, configBase.getHomeDir()) );
		}
		client.setProxyDatabase(this.proxies);
		client.setCredentialsProvider(credentialsProvider);
		final String defaultAuths= getDefaultPreferredAuthenticationMethodsString();
		if (defaultAuths != null) {
			client.setAttribute(JGitSshClient.PREFERRED_AUTHENTICATIONS, defaultAuths);
		}
		try {
			client.setAttribute(JGitSshClient.HOME_DIRECTORY, configBase.getHomeDir());
		}
		catch (final SecurityException | InvalidPathException e) {
			// Ignore
		}
		return client;
	}
	
}
