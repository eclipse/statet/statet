/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.runtime.eplatform;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Platform;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.BasicAppEnvironment;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.eplatform.EStatusUtils;


@NonNullByDefault
public final class EPlatformAppEnvironment extends BasicAppEnvironment implements BundleActivator {
	
	
	public static final String ENV_ID= "org.eclipse.statet.jcommons.runtime.appEnvironments.EPlatform"; //$NON-NLS-1$
	
	
	private static EPlatformAppEnvironment instance= nonNullLateInit();
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static EPlatformAppEnvironment getInstance() {
		return instance;
	}
	
	
	private Bundle bundle= nonNullLateInit();
	
	
	@SuppressWarnings("null")
	public EPlatformAppEnvironment() {
		super(ENV_ID, null, new EPlatformBundleResolver());
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		this.bundle= context.getBundle();
		
		EPlatformAppEnvironment.instance= this;
		CommonsRuntime.init(this);
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		onAppStopping();
	}
	
	
	@Override
	public void log(final Status status) {
		final ILog log= Platform.getLog(this.bundle);
		log.log(EStatusUtils.convert(status));
	}
	
}
