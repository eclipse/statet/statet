/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class AbstractImIntList implements ImIntList {
	
	
	public AbstractImIntList() {
	}
	
	
	@Override
	public boolean add(final int e) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void addAt(final int index, final int e) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public int setAt(final int index, final int e) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean remove(final int e) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public int removeAt(final int index) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}
	
	
	public abstract void copyTo(int srcIndex, int[] dest, int destIndex, int length);
	
	public abstract void copyTo(int[] dest, int destIndex);
	
	
}
