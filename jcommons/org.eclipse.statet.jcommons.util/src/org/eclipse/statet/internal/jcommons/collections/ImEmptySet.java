/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.ImSequencedSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Empty set implementation.
 * <p>
 * Comparable to <code>Collections.emptySet()</code>.</p>
 * 
 * @since 1.5
 */
@NonNullByDefault
public final class ImEmptySet<E> extends AbstractImList<E> implements ImSequencedSet<E>,
		RandomAccess {
	
	
	@SuppressWarnings("rawtypes")
	public static final ImEmptySet INSTANCE= new ImEmptySet();
	
	
	private final ListIterator<E> iterator= new AbstractImListIter<>() {
		
		@Override
		public boolean hasNext() {
			return false;
		}
		
		@Override
		public int nextIndex() {
			return 0;
		}
		
		@Override
		public E next() {
			throw new NoSuchElementException();
		}
		
		@Override
		public boolean hasPrevious() {
			return false;
		}
		
		@Override
		public int previousIndex() {
			return -1;
		}
		
		@Override
		public E previous() {
			throw new NoSuchElementException();
		}
		
	};
	
	
	public ImEmptySet() {
	}
	
	
	@Override
	public int size() {
		return 0;
	}
	
	@Override
	public boolean isEmpty() {
		return true;
	}
	
	@Override
	public boolean contains(final @Nullable Object e) {
		return false;
	}
	
	@Override
	public boolean containsAll(final Collection<?> c) {
		return c.isEmpty();
	}
	
	@Override
	public E get(final int index) {
		throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
	}
	
	@Override
	public E getFirst() {
		throw new NoSuchElementException();
	}
	
	@Override
	public E getLast() {
		throw new NoSuchElementException();
	}
	
	@Override
	public int indexOf(final @Nullable Object e) {
		return -1;
	}
	
	@Override
	public int lastIndexOf(final @Nullable Object e) {
		return -1;
	}
	
	
	@Override
	public Iterator<E> iterator() {
		return this.iterator;
	}
	
	@Override
	public Spliterator<E> spliterator() {
		return Spliterators.emptySpliterator();
	}
	
	
	@Override
	public Object[] toArray() {
		return ArrayUtils.EMPTY_OBJ;
	}
	
	@Override
	@SuppressWarnings("null")
	public <T> T[] toArray(final T[] dest) {
		if (dest.length > 0) {
			dest[0]= null;
		}
		return dest;
	}
	
	@Override
	public <T> T[] toArray(final IntFunction<T[]> generator) {
		return generator.apply(0);
	}
	
	@Override
	public void copyTo(final int srcIndex, final Object[] dest, final int destIndex, final int length) {
		assert (false);
	}
	
	@Override
	public void copyTo(final Object[] dest, final int destIndex) {
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public ImList<E> toList() {
		return ImEmptyList.INSTANCE;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public ImIdentityList<E> toIdentityList() {
		return ImEmptyIdentityList.INSTANCE;
	}
	
	@Override
	public ImSequencedSet<E> reversed() {
		return this;
	}
	
	@Override
	@SuppressWarnings({ "unchecked" })
	public <R> MappingResult<R> map(final Function<E, R> mapper) {
		nonNullAssert(mapper);
		return ImEmptyList.INSTANCE;
	}
	
	@Override
	public @Nullable E findFirst(final Predicate<E> predicate) {
		nonNullAssert(predicate);
		return null;
	}
	
	@Override
	public boolean anyMatch(final Predicate<? super E> predicate) {
		nonNullAssert(predicate);
		return false;
	}
	
	@Override
	public boolean allMatch(final Predicate<? super E> predicate) {
		nonNullAssert(predicate);
		return true;
	}
	
	@Override
	public boolean noneMatch(final Predicate<? super E> predicate) {
		nonNullAssert(predicate);
		return true;
	}
	
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof final Set<?> other) {
			return (other.isEmpty());
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return "[]"; //$NON-NLS-1$
	}
	
}
