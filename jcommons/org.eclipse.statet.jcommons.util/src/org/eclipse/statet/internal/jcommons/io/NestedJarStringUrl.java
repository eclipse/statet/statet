/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.JAR_NESTED_SEPARATOR;
import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.NESTED_SCHEME;
import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.isSchemeUrl;
import static org.eclipse.statet.jcommons.io.UriUtils.FILE_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SEPARATOR;

import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * {@code jar:nested:/rhelpserver-4/org.eclipse.statet.rhelp.server.jar/!BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/}
 * {@code jar:nested:/rhelpserver-4/org.eclipse.statet.rhelp.server.jar/!BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/META-INF/MANIFEST.MF}
 */
@NonNullByDefault
public final class NestedJarStringUrl extends AbstractNestedArchiveStringUrl {
	
	
	private final int nestSepIndex;
	private final int jarSepIndex;
	private final int fragSepIndex;
	
	
	public NestedJarStringUrl(final String urlString, final int jarSepIndex)
			throws URISyntaxException {
		super(urlString);
		final int nestSepIdx= urlString.indexOf(JAR_NESTED_SEPARATOR, 11);
		if (nestSepIdx == -1 || nestSepIdx >= jarSepIndex - 1) {
			throw new URISyntaxException(urlString, "Nested JAR separator '/!' is missing");
		}
		final int fragmentIdx= urlString.indexOf('#', nestSepIdx);
		
		this.nestSepIndex= nestSepIdx;
		this.jarSepIndex= jarSepIndex;
		this.fragSepIndex= ((fragmentIdx != -1) ? fragmentIdx : urlString.length());
	}
	
	public NestedJarStringUrl(final String urlString)
			throws URISyntaxException {
		super(urlString);
		if (!isSchemeUrl(urlString, JAR_SCHEME + ':' + NESTED_SCHEME)) {
			throw new IllegalArgumentException("url= " + urlString);
		}
		final int jarSepIdx= urlString.indexOf(JAR_SEPARATOR, 11);
		if (jarSepIdx == -1) {
			throw new URISyntaxException(urlString, "JAR content separator '!/' is missing");
		}
		final int nestSepIdx= urlString.indexOf(JAR_NESTED_SEPARATOR, 11);
		if (nestSepIdx == -1 || nestSepIdx >= jarSepIdx - 1) {
			throw new URISyntaxException(urlString, "Nested JAR separator '/!' is missing");
		}
		final int fragmentIdx= urlString.indexOf('#', jarSepIdx);
		
		this.nestSepIndex= nestSepIdx;
		this.jarSepIndex= jarSepIdx;
		this.fragSepIndex= ((fragmentIdx != -1) ? fragmentIdx : urlString.length());
	}
	
	
	@Override
	public String getOuterArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	protected String createOuterArchiveUrl(final String urlString) {
		final var sb= new StringBuilder(this.nestSepIndex - 6); // 5 - 11 = -6
		sb.append(FILE_SCHEME + ':');
		sb.append(urlString, 11, this.nestSepIndex);
		return sb.toString();
	}
	
	@Override
	public String getInnerArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	protected String createInnerArchiveUrl(final String urlString) throws URISyntaxException {
		final var sb= new StringBuilder(this.jarSepIndex - 2); // 9 - 11 = -2
		sb.append(JAR_SCHEME + ':' + FILE_SCHEME + ':');
		sb.append(urlString, 11, this.jarSepIndex);
		sb.replace(this.nestSepIndex - 2, this.nestSepIndex, JAR_SEPARATOR);
		return sb.toString();
	}
	
	@Override
	protected String createEntryPath(final String urlString) {
		return urlString.substring(this.jarSepIndex + 2, this.fragSepIndex);
	}
	
	
}
