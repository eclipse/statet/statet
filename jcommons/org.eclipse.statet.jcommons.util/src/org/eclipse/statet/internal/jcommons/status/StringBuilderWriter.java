/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.status;

import java.io.IOException;
import java.io.Writer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class StringBuilderWriter extends java.io.Writer {
	
	
	private final StringBuilder sb;
	
	
	public StringBuilderWriter(final StringBuilder sb) {
		this.sb= sb;
	}
	
	public StringBuilderWriter() {
		this(new StringBuilder());
	}
	
	
	@Override
	public void write(final int c) throws IOException {
		this.sb.append(c);
	}
	
	@Override
	public void write(final char[] cbuf, final int off, final int len) throws IOException {
		this.sb.append(cbuf, off, len);
	}
	
	@Override
	public void write(final String str) throws IOException {
		this.sb.append(str);
	}
	
	@Override
	public void write(final String str, final int off, final int len) throws IOException {
		this.sb.append(str, off, off + len);
	}
	
	@Override
	public Writer append(final @Nullable CharSequence csq) throws IOException {
		this.sb.append(csq);
		return this;
	}
	
	@Override
	public Writer append(final @Nullable CharSequence csq, final int start, final int end) throws IOException {
		this.sb.append(csq, start, end);
		return this;
	}
	
	@Override
	public Writer append(final char c) throws IOException {
		this.sb.append(c);
		return this;
	}
	
	@Override
	public void flush() throws IOException {
	}
	
	@Override
	public void close() throws IOException {
	}
	
	
	@Override
	public String toString() {
		return this.sb.toString();
	}
	
}
