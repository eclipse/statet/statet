/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import static org.eclipse.statet.jcommons.io.UriUtils.FILE_SCHEME;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractNestedArchiveStringUrl extends AbstractArchiveStringUrl {
	
	
	private volatile @Nullable URI innerArchiveUrl;
	private volatile @Nullable String innerArchiveUrlString;
	
	
	public AbstractNestedArchiveStringUrl(final String urlString) {
		super(urlString);
	}
	
	
	@Override
	public final boolean isNested() {
		return true;
	}
	
	@Override
	public String getOuterArchiveScheme() {
		return FILE_SCHEME;
	}
	
	@Override
	public final URI getInnerArchiveUrl() throws URISyntaxException {
		var url= this.innerArchiveUrl;
		if (url == null) {
			url= new URI(getInnerArchiveUrlString());
			this.innerArchiveUrl= url;
		}
		return url;
	}
	
	@Override
	public final String getInnerArchiveUrlString() throws URISyntaxException {
		var urlString= this.innerArchiveUrlString;
		if (urlString == null) {
			urlString= createInnerArchiveUrl(getUrlString());
			this.innerArchiveUrlString= urlString;
		}
		return urlString;
	}
	
	protected abstract String createInnerArchiveUrl(String urlString) throws URISyntaxException;
	
	
}
