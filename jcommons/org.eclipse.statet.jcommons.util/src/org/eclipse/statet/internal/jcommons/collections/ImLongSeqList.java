/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.util.stream.LongStream;

import org.eclipse.statet.jcommons.collections.LongList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImLongSeqList extends AbstractImLongList {
	
	
	private final long e0;
	private final int length;
	
	
	public ImLongSeqList(final long e0, final int length) {
		this.e0= e0;
		this.length= length;
	}
	
	
	@Override
	public int size() {
		return this.length;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public boolean contains(final long e) {
		final long index= e - this.e0;
		return (index >= 0 && index < this.length);
	}
	
	@Override
	public long getAt(final int index) {
		if (index < 0 || index >= this.length) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return this.e0 + index;
	}
	
	@Override
	public long getFirst() {
		return this.e0;
	}
	
	@Override
	public long getLast() {
		return this.e0 + this.length - 1;
	}
	
	@Override
	public int indexOf(final long e) {
		final long index= e - this.e0;
		if (index >= 0 && index < this.length) {
			return (int)index;
		}
		return -1;
	}
	
	@Override
	public int lastIndexOf(final long e) {
		final long index= e - this.e0;
		if (index >= 0 && index < this.length) {
			return (int)index;
		}
		return -1;
	}
	
	
	@Override
	public LongStream stream() {
		return LongStream.range(this.e0, this.e0 + this.length);
	}
	
	
	@Override
	public long[] toArray() {
		final long[] dest= new long[this.length];
		long e= this.e0;
		for (int i= 0; i < this.length; i++) {
			dest[i]= e++;
		}
		return dest;
	}
	
	@Override
	public void toArray(final long[] array) {
		long e= this.e0;
		for (int i= 0; i < this.length; i++) {
			array[i]= e++;
		}
	}
	
	@Override
	public void copyTo(final int srcIndex, final long[] dest, int destIndex, final int length) {
		long e= this.e0 + srcIndex;
		for (int i= 0; i < length; i++) {
			dest[destIndex++]= e++;
		}
	}
	
	@Override
	public void copyTo(final long[] dest, int destIndex) {
		long e= this.e0;
		for (int i= 0; i < this.length; i++) {
			dest[destIndex++]= e++;
		}
	}
	
	
	@Override
	public int hashCode() {
		int hashCode= 9;
		for (int i= 0; i < this.length; i++) {
			hashCode= 31 * hashCode + Long.hashCode(this.e0 + i);
		}
		return hashCode;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof final LongList other) {
			if (this.length != other.size()) {
				return false;
			}
			for (int i= 0; i < this.length; i++) {
				if (this.e0 + i != other.getAt(i)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		final var sb = new StringBuilder();
		sb.append('[');
		sb.append(this.e0);
		for (int i= 1; i < this.length; i++) {
			sb.append(", "); //$NON-NLS-1$
			sb.append(this.e0 + i);
		}
		sb.append(']');
		return sb.toString();
	}
	
}
