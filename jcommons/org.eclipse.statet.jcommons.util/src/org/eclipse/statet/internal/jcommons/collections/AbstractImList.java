/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.util.Collection;
import java.util.ListIterator;
import java.util.function.Predicate;

import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractImList<E> {
	
	
	protected static abstract class AbstractImListIter<E> implements ListIterator<E>, Immutable {
		
		
		public AbstractImListIter() {
		}
		
		
		@Override
		public final void set(final E e) {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public final void add(final E e) {
			throw new UnsupportedOperationException();
		}
		
		@Override
		public final void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
	
	
	public AbstractImList() {
	}
	
	
	public final E set(final int index, final E e) {
		throw new UnsupportedOperationException();
	}
	
	public final boolean add(final E e) {
		throw new UnsupportedOperationException();
	}
	
	public final void add(final int index, final E e) {
		throw new UnsupportedOperationException();
	}
	
	public final void addFirst(final E e) {
		throw new UnsupportedOperationException();
	}
	
	public final void addLast(final E e) {
		throw new UnsupportedOperationException();
	}
	
	public final boolean addAll(final Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}
	
	public final boolean addAll(final int index, final Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}
	
	public final boolean remove(final @Nullable Object e) {
		throw new UnsupportedOperationException();
	}
	
	public final E remove(final int index) {
		throw new UnsupportedOperationException();
	}
	
	public final E removeFirst() {
		throw new UnsupportedOperationException();
	}
	
	public final E removeLast() {
		throw new UnsupportedOperationException();
	}
	
	public final boolean removeAll(final Collection<?> c) {
		throw new UnsupportedOperationException();
	}
	
	public final boolean removeIf(final Predicate<? super E> filter) {
		throw new UnsupportedOperationException();
	}
	
	public final void clear() {
		throw new UnsupportedOperationException();
	}
	
	public final boolean retainAll(final Collection<?> c) {
		throw new UnsupportedOperationException();
	}
	
	
	public abstract int size();
	
	
	public abstract E get(final int index);
	
	public abstract int indexOf(final @Nullable Object e);
	
	public abstract int lastIndexOf(final @Nullable Object e);
	
	
	public abstract void copyTo(int srcIndex, Object[] dest, int destIndex, int length);
	
	public abstract void copyTo(Object[] dest, int destIndex);
	
	public abstract ImList<E> toList();
	public abstract ImIdentityList<E> toIdentityList();
	
}
