/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SCHEME;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * {@code jar:file:/rhelpserver-4/org.eclipse.statet.rhelp.server/BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/}
 * {@code jar:file:/rhelpserver-4/org.eclipse.statet.rhelp.server/BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/META-INF/MANIFEST.MF}
 */
@NonNullByDefault
public class JarUriUrl extends AbstractArchiveUriUrl {
	
	
	private final int scheme2SepIndex;
	private final int jarSepIndex;
	
	
	public JarUriUrl(final URI url, final int schemeSep2Index, final int jarSepIndex) {
		super(url);
		
		this.scheme2SepIndex= schemeSep2Index;
		this.jarSepIndex= jarSepIndex;
	}
	
	
	@Override
	public boolean isNested() {
		return false;
	}
	
	
	@Override
	public String getOuterArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	public String getOuterArchiveScheme() {
		return getUrl().getSchemeSpecificPart().substring(0, this.scheme2SepIndex);
	}
	
	@Override
	protected URI createOuterArchiveUrl(final URI url) throws URISyntaxException {
		final String ssp= url.getSchemeSpecificPart();
		return new URI(ssp.substring(0, this.scheme2SepIndex),
				ssp.substring(this.scheme2SepIndex + 1, this.jarSepIndex),
				url.getFragment() );
	}
	
	@Override
	public String getInnerArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	public URI getInnerArchiveUrl() throws URISyntaxException {
		return getOuterArchiveUrl();
	}
	
	@Override
	public String getInnerArchiveUrlString() throws URISyntaxException {
		return getOuterArchiveUrlString();
	}
	
	@Override
	protected String createEntryPath(final URI url) {
		final String ssp= url.getSchemeSpecificPart();
		return ssp.substring(this.jarSepIndex + 2, ssp.length());
	}
	
	
}
