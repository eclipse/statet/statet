/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.NoSuchElementException;
import java.util.Spliterators;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImIntEmptyList extends AbstractImIntList {
	
	
	public static final ImIntEmptyList INSTANCE= new ImIntEmptyList();
	
	
	public ImIntEmptyList() {
	}
	
	
	@Override
	public int size() {
		return 0;
	}
	
	@Override
	public boolean isEmpty() {
		return true;
	}
	
	@Override
	public boolean contains(final int e) {
		return false;
	}
	
	@Override
	public int getAt(final int index) {
		throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
	}
	
	@Override
	public int getFirst() {
		throw new NoSuchElementException();
	}
	
	@Override
	public int getLast() {
		throw new NoSuchElementException();
	}
	
	@Override
	public int indexOf(final int e) {
		return -1;
	}
	
	@Override
	public int lastIndexOf(final int e) {
		return -1;
	}
	
	
	@Override
	public IntStream stream() {
		return StreamSupport.intStream(Spliterators.emptyIntSpliterator(), false);
	}
	
	
	@Override
	public int[] toArray() {
		return ArrayUtils.EMPTY_INT;
	}
	
	@Override
	public void toArray(final int[] array) {
		nonNullAssert(array);
	}
	
	@Override
	public void copyTo(final int srcIndex, final int[] dest, final int destIndex, final int length) {
		assert (false);
	}
	
	@Override
	public void copyTo(final int[] dest, final int destIndex) {
	}
	
	
	@Override
	public String getString() {
		return ""; //$NON-NLS-1$
	}
	
	
	@Override
	public int hashCode() {
		return 9;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof final IntList other) {
			return (other.isEmpty());
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return "[]"; //$NON-NLS-1$
	}
	
}
