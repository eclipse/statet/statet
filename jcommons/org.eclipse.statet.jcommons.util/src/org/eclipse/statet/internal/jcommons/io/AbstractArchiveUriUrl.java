/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.io.ArchiveUrl;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractArchiveUriUrl implements ArchiveUrl {
	
	
	private final URI url;
	
	private volatile @Nullable URI outerArchiveUrl;
	private volatile @Nullable String pathString;
	
	
	public AbstractArchiveUriUrl(final URI url) {
		this.url= url;
	}
	
	
	@Override
	public final URI getUrl() {
		return this.url;
	}
	
	@Override
	public final String getUrlString() {
		return this.url.toString();
	}
	
	
	@Override
	public final URI getOuterArchiveUrl() throws URISyntaxException {
		var url= this.outerArchiveUrl;
		if (url == null) {
			url= createOuterArchiveUrl(this.url);
			this.outerArchiveUrl= url;
		}
		return url;
	}
	
	protected abstract URI createOuterArchiveUrl(URI url) throws URISyntaxException;
	
	@Override
	public final String getOuterArchiveUrlString() throws URISyntaxException {
		return getOuterArchiveUrl().toString();
	}
	
	@Override
	public final String getInnerEntryPathString() {
		var s= this.pathString;
		if (s == null) {
			s= createEntryPath(this.url);
			this.pathString= s;
		}
		return s;
	}
	
	protected abstract String createEntryPath(URI url);
	
	
	@Override
	public String toString() {
		return getUrlString();
	}
	
}
