/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.isSchemeUrl;
import static org.eclipse.statet.jcommons.io.UriUtils.FILE_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SEPARATOR;

import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * {@code jar:file:/rhelpserver-4/org.eclipse.statet.rhelp.server.jar!/BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/}
 * {@code jar:file:/rhelpserver-4/org.eclipse.statet.rhelp.server.jar!/BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/META-INF/MANIFEST.MF}
 */
@NonNullByDefault
public class NestedJarBoot2StringUrl extends AbstractNestedArchiveStringUrl {
	
	
	private final int nestSepIndex;
	private final int jarSepIndex;
	private final int fragSepIndex;
	
	
	public NestedJarBoot2StringUrl(final String urlString, final int nestSepIndex, final int jarSepIndex)
			throws URISyntaxException {
		super(urlString);
		final int fragmentIdx= urlString.indexOf('#', nestSepIndex);
		
		this.nestSepIndex= nestSepIndex;
		this.jarSepIndex= jarSepIndex;
		this.fragSepIndex= ((fragmentIdx != -1) ? fragmentIdx : urlString.length());
	}
	
	public NestedJarBoot2StringUrl(final String urlString)
			throws URISyntaxException {
		super(urlString);
		if (!isSchemeUrl(urlString, JAR_SCHEME + ':' + FILE_SCHEME)) {
			throw new IllegalArgumentException("url= " + urlString);
		}
		final int jarSep1Idx= urlString.indexOf(JAR_SEPARATOR, 9);
		if (jarSep1Idx == -1) {
			throw new URISyntaxException(urlString, "JAR content separator '!/' is missing");
		}
		final int jarSep2Idx= urlString.indexOf(JAR_SEPARATOR, jarSep1Idx + 2);
		if (jarSep2Idx == -1) {
			throw new URISyntaxException(urlString, "Nested JAR separator '!/' is missing");
		}
		final int fragmentIdx= urlString.indexOf('#', jarSep2Idx);
		
		this.nestSepIndex= jarSep1Idx;
		this.jarSepIndex= jarSep2Idx;
		this.fragSepIndex= ((fragmentIdx != -1) ? fragmentIdx : urlString.length());
	}
	
	
	@Override
	public String getOuterArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	protected String createOuterArchiveUrl(final String urlString) {
		return urlString.substring(4, this.nestSepIndex);
	}
	
	@Override
	public String getInnerArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	protected String createInnerArchiveUrl(final String urlString) throws URISyntaxException {
		return urlString.substring(0, this.jarSepIndex);
	}
	
	@Override
	protected String createEntryPath(final String urlString) {
		return urlString.substring(this.jarSepIndex + 2, this.fragSepIndex);
	}
	
	
}
