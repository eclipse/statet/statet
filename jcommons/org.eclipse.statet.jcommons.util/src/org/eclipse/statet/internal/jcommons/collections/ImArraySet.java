/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.collections.ImSequencedSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Immutable set implementation based on an array.
 * <p>
 * Comparable to <code>Collections.unmodifiableList(Array.asList(...))</code>.</p>
 * 
 * @since de.walware.ecommons.coremisc 1.5
 */
@NonNullByDefault
public final class ImArraySet<E> extends AbstractImList<E> implements ImSequencedSet<E>,
		RandomAccess {
	
	
	private class Iter extends AbstractImListIter<E> {
		
		
		private int cursor;
		
		
		Iter(final int index) {
			this.cursor= index;
		}
		
		
		@Override
		public boolean hasNext() {
			return (this.cursor < ImArraySet.this.array.length);
		}
		
		@Override
		public int nextIndex() {
			return this.cursor;
		}
		
		@Override
		public E next() {
			if (this.cursor >= ImArraySet.this.array.length) {
				throw new NoSuchElementException();
			}
			return ImArraySet.this.array[this.cursor++];
		}
		
		@Override
		public boolean hasPrevious() {
			return (this.cursor > 0);
		}
		
		@Override
		public int previousIndex() {
			return this.cursor - 1;
		}
		
		@Override
		public E previous() {
			if (this.cursor <= 0) {
				throw new NoSuchElementException();
			}
			return ImArraySet.this.array[--this.cursor];
		}
		
	}
	
	
	private final E[] array;
	
	
	/**
	 * Create a new constant list backed by the given array (directly used!).
	 * 
	 * If the list is published through an API and should be constant, the
	 * the elements of the given array must not any longer be changed.
	 * 
	 * @param a the array by which the list will be backed.
	 */
	public ImArraySet(final E[] a) {
		this.array= a;
	}
	
	
	@Override
	public int size() {
		return this.array.length;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public boolean contains(final @Nullable Object e) {
		return (indexOf(e) >= 0);
	}
	
	@Override
	public boolean containsAll(final Collection<?> c) {
		final Iterator<?> e= c.iterator();
		while (e.hasNext()) {
			if (indexOf(e.next()) < 0) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public E get(final int index) {
		return this.array[index];
	}
	
	@Override
	public E getFirst() {
		return this.array[0];
	}
	
	@Override
	public E getLast() {
		return this.array[this.array.length - 1];
	}
	
	@Override
	public int indexOf(final @Nullable Object e) {
		if (e == null) {
			for (int i= 0; i < this.array.length; i++) {
				if (null == this.array[i]) {
					return i;
				}
			}
			return -1;
		}
		else {
			for (int i= 0; i < this.array.length; i++) {
				if (e.equals(this.array[i])) {
					return i;
				}
			}
			return -1;
		}
	}
	
	@Override
	public int lastIndexOf(final @Nullable Object e) {
		if (e == null) {
			for (int i= this.array.length - 1; i >= 0; i--) {
				if (null == this.array[i]) {
					return i;
				}
			}
			return -1;
		}
		else {
			for (int i= this.array.length - 1; i >= 0; i--) {
				if (e.equals(this.array[i])) {
					return i;
				}
			}
			return -1;
		}
	}
	
	
	@Override
	public Iterator<E> iterator() {
		return new Iter(0);
	}
	
	@Override
	public Spliterator<E> spliterator() {
		return Spliterators.spliterator(this.array, 0, this.array.length,
				Spliterator.IMMUTABLE | Spliterator.DISTINCT );
	}
	
	
	@Override
	public Object[] toArray() {
		final var dest= new Object[this.array.length];
		System.arraycopy(this.array, 0, dest, 0, this.array.length);
		return dest;
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "null" })
	public <T> T[] toArray(final T[] dest) {
		final int n= this.array.length;
		if (dest.length < n) {
			return Arrays.copyOf(this.array, n, (Class<? extends T[]>)dest.getClass());
		}
		System.arraycopy(this.array, 0, dest, 0, n);
		if (dest.length > n) {
			dest[n]= null;
		}
		return dest;
	}
	
	@Override
	public <T> T[] toArray(final IntFunction<T[]> generator) {
		final var dest= generator.apply(this.array.length);
		System.arraycopy(this.array, 0, dest, 0, this.array.length);
		return dest;
	}
	
	@Override
	public void copyTo(final int srcIndex, final Object[] dest, final int destIndex, final int length) {
		System.arraycopy(this.array, srcIndex, dest, destIndex, length);
	}
	
	@Override
	public void copyTo(final Object[] dest, final int destIndex) {
		System.arraycopy(this.array, 0, dest, destIndex, this.array.length);
	}
	
	@Override
	public ImList<E> toList() {
		return new ImArrayList<>(this.array);
	}
	
	@Override
	public ImIdentityList<E> toIdentityList() {
		return new ImArrayIdentityList<>(this.array);
	}
	
	@Override
	public ImSequencedSet<E> reversed() {
		return new ImArraySet<>(ArrayUtils.copyReverse(this.array, 0, this.array.length));
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <R> MappingResult<R> map(final Function<E, R> mapper) {
		final R[] result= (R[])new Object[this.array.length];
		for (int i= 0; i < this.array.length; i++) {
			result[i]= mapper.apply(this.array[i]);
		}
		return new ImArrayList<>(result);
	}
	
	@Override
	public @Nullable E findFirst(final Predicate<E> predicate) {
		for (int i= 0; i < this.array.length; i++) {
			final E e= this.array[i];
			if (predicate.test(e)) {
				return e;
			}
		}
		return null;
	}
	
	@Override
	public boolean anyMatch(final Predicate<? super E> predicate) {
		for (int i= 0; i < this.array.length; i++) {
			if (predicate.test(this.array[i])) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean allMatch(final Predicate<? super E> predicate) {
		for (int i= 0; i < this.array.length; i++) {
			if (!predicate.test(this.array[i])) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean noneMatch(final Predicate<? super E> predicate) {
		for (int i= 0; i < this.array.length; i++) {
			if (predicate.test(this.array[i])) {
				return false;
			}
		}
		return true;
	}
	
	
	@Override
	public int hashCode() {
		int hashCode= 0;
		for (int i= 0; i < this.array.length; i++) {
			if (this.array[i] != null) {
				hashCode+= this.array[i].hashCode();
			}
		}
		return hashCode;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof final Set<?> other) {
			return (this.array.length == other.size()
					&& containsAll(other) );
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return Arrays.toString(this.array);
	}
	
}
