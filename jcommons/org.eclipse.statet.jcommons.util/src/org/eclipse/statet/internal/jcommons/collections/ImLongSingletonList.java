/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.util.stream.LongStream;

import org.eclipse.statet.jcommons.collections.LongList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImLongSingletonList extends AbstractImLongList {
	
	
	private final long e0;
	
	
	public ImLongSingletonList(final long e) {
		this.e0= e;
	}
	
	
	@Override
	public int size() {
		return 1;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	
	@Override
	public boolean contains(final long e) {
		return (this.e0 == e);
	}
	
	@Override
	public long getAt(final int index) {
		if (index != 0) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return this.e0;
	}
	
	@Override
	public long getFirst() {
		return this.e0;
	}
	
	@Override
	public long getLast() {
		return this.e0;
	}
	
	@Override
	public int indexOf(final long e) {
		return (this.e0 == e) ? 0 : -1;
	}
	
	@Override
	public int lastIndexOf(final long e) {
		return (this.e0 == e) ? 0 : -1;
	}
	
	
	@Override
	public LongStream stream() {
		return LongStream.of(this.e0);
	}
	
	
	@Override
	public long[] toArray() {
		return new long[] { this.e0 };
	}
	
	@Override
	public void toArray(final long[] array) {
		array[0]= this.e0;
	}
	
	@Override
	public void copyTo(final int srcIndex, final long[] dest, final int destIndex, final int length) {
		assert (length == 1);
		dest[destIndex]= this.e0;
	}
	
	@Override
	public void copyTo(final long[] dest, final int destIndex) {
		dest[destIndex]= this.e0;
	}
	
	
	@Override
	public int hashCode() {
		int hashCode= 7;
		hashCode= 31 * hashCode + Long.hashCode(this.e0);
		return hashCode;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof final LongList other) {
			return (1 == other.size()
					&& this.e0 == other.getFirst() );
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append('[');
		sb.append(this.e0);
		sb.append(']');
		return sb.toString();
	}
	
}
