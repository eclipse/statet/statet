/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Spliterator;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import org.eclipse.statet.jcommons.collections.IdentityList;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Immutable list implementation for a single element.
 * <p>
 * Comparable to <code>Collections.unmodifiableList(Collections.singletonList(...))</code>.</p>
 * 
 * @since de.walware.ecommons.coremisc 1.5
 */
@NonNullByDefault
public final class ImSingletonIdentityList<E> extends AbstractImList<E> implements ImIdentityList<E>,
		RandomAccess {
	
	
	private class Iter extends AbstractImListIter<E> {
		
		
		private int cursor;
		
		
		Iter(final int index) {
			this.cursor= index;
		}
		
		
		@Override
		public boolean hasNext() {
			return (this.cursor < 1);
		}
		
		@Override
		public int nextIndex() {
			return this.cursor;
		}
		
		@Override
		public E next() {
			if (this.cursor >= 1) {
				throw new NoSuchElementException();
			}
			this.cursor++;
			return ImSingletonIdentityList.this.e0;
		}
		
		@Override
		public boolean hasPrevious() {
			return (this.cursor > 0);
		}
		
		@Override
		public int previousIndex() {
			return this.cursor - 1;
		}
		
		@Override
		public E previous() {
			if (this.cursor <= 0) {
				throw new NoSuchElementException();
			}
			this.cursor--;
			return ImSingletonIdentityList.this.e0;
		}
		
	}
	
	
	private final E e0;
	
	
	/**
	 * Create a new constant list with the specified element.
	 * 
	 * @param e the element the list will contain.
	 */
	public ImSingletonIdentityList(final E e) {
		this.e0= e;
	}
	
	
	@Override
	public int size() {
		return 1;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public boolean contains(final @Nullable Object e) {
		return (this.e0 == e);
	}
	
	@Override
	public boolean containsAll(final Collection<?> c) {
		final Iterator<?> e= c.iterator();
		while (e.hasNext()) {
			if (!contains(e.next())) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public E get(final int index) {
		if (index != 0) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return this.e0;
	}
	
	@Override
	public E getFirst() {
		return this.e0;
	}
	
	@Override
	public E getLast() {
		return this.e0;
	}
	
	@Override
	public int indexOf(final @Nullable Object e) {
		return (this.e0 == e) ? 0 : -1;
	}
	
	@Override
	public int lastIndexOf(final @Nullable Object e) {
		return (this.e0 == e) ? 0 : -1;
	}
	
	
	@Override
	public Iterator<E> iterator() {
		return new Iter(0);
	}
	
	@Override
	public ListIterator<E> listIterator() {
		return new Iter(0);
	}
	
	@Override
	public ListIterator<E> listIterator(final int index) {
		if (index < 0 || index > 1) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return new Iter(index);
	}
	
	@Override
	public Spliterator<E> spliterator() {
		return new ImSingletonSpliterator<>(this.e0);
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public ImIdentityList<E> subList(final int fromIndex, final int toIndex) {
		if (fromIndex < 0 || toIndex > 1) {
			throw new IndexOutOfBoundsException("fromIndex= " + fromIndex + ", toIndex= " + toIndex + ", size= " + 1); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (fromIndex > toIndex) {
			throw new IllegalArgumentException("fromIndex > toIndex: fromIndex= " + fromIndex + ", toIndex= " + toIndex); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (toIndex - fromIndex == 1) {
			return this;
		}
		else {
			return ImEmptyIdentityList.INSTANCE;
		}
	}
	
	@Override
	public Object[] toArray() {
		return new Object[] { this.e0 };
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "null" })
	public <T> T[] toArray(T[] dest) {
		if (dest.length < 1) {
			dest= (T[])Array.newInstance(((Class<? extends T[]>)dest.getClass()).getComponentType(), 1);
		}
		dest[0]= (T)this.e0;
		if (dest.length > 1) {
			dest[1]= null;
		}
		return dest;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T[] toArray(final IntFunction<T[]> generator) {
		final var dest= generator.apply(1);
		dest[0]= (T)this.e0;
		return dest;
	}
	
	@Override
	public void copyTo(final int srcIndex, final Object[] dest, final int destIndex, final int length) {
		assert (length == 1);
		dest[destIndex]= this.e0;
	}
	
	@Override
	public void copyTo(final Object[] dest, final int destIndex) {
		dest[destIndex]= this.e0;
	}
	
	@Override
	public ImList<E> toList() {
		return new ImSingletonList<>(this.e0);
	}
	
	@Override
	public ImIdentityList<E> toIdentityList() {
		return this;
	}
	
	@Override
	public ImIdentityList<E> reversed() {
		return this;
	}
	
	@Override
	public <R> MappingResult<R> map(final Function<E, R> mapper) {
		return new ImSingletonList<>(
				mapper.apply(this.e0) );
	}
	
	@Override
	public @Nullable E findFirst(final Predicate<E> predicate) {
		return predicate.test(this.e0) ? this.e0 : null;
	}
	
	@Override
	public boolean anyMatch(final Predicate<? super E> predicate) {
		return predicate.test(this.e0);
	}
	
	@Override
	public boolean allMatch(final Predicate<? super E> predicate) {
		return predicate.test(this.e0);
	}
	
	@Override
	public boolean noneMatch(final Predicate<? super E> predicate) {
		return !predicate.test(this.e0);
	}
	
	
	@Override
	public int hashCode() {
		int hashCode= 1;
		hashCode= 31 * hashCode + Objects.hashCode(this.e0);
		return hashCode;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof final IdentityList<?> other) {
			return (1 == other.size()
					&& (this.e0 == other.get(0)) );
		}
		if (obj instanceof final List<?> other) {
			return (1 == other.size()
					&& Objects.equals(this.e0, other.get(0)) );
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return '[' + String.valueOf(this.e0) + ']';
	}
	
}
