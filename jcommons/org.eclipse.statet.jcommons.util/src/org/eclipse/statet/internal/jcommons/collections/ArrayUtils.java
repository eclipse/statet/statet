/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.FIELD;
import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.PARAMETER;
import static org.eclipse.statet.jcommons.lang.NullDefaultLocation.RETURN_TYPE;

import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.collections.LongList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault({ PARAMETER, RETURN_TYPE, FIELD })
public final class ArrayUtils {
	
	
	public static final int[] EMPTY_INT= new int[0];
	public static final long[] EMPTY_LONG= new long[0];
	public static final Object[] EMPTY_OBJ= new Object[0];
	
	
	public static <E> boolean containsEqual(final E[] array, final int startIdx, final int endIdx,
			final E e) {
		if (e == null) {
			for (int idx= 0; idx < endIdx; idx++) {
				if (null == array[idx]) {
					return true;
				}
			}
			return false;
		}
		else {
			for (int idx= 0; idx < endIdx; idx++) {
				if (e.equals(array[idx])) {
					return true;
				}
			}
			return false;
		}
	}
	
	public static <E> boolean containsIdentical(final E[] array, final int startIdx, final int endIdx,
			final E e) {
		for (int idx= 0; idx < endIdx; idx++) {
			if (e == array[idx]) {
				return true;
			}
		}
		return false;
	}
	
	
	public static void copyTo(final Collection<?> src, final Object[] dest, final int destIndex) {
		if (src instanceof AbstractImList<?>) {
			((AbstractImList<?>)src).copyTo(dest, destIndex);
		}
		else if (destIndex == 0) {
			src.toArray(dest);
		}
		else {
			final Object[] a= src.toArray();
			System.arraycopy(a, 0, dest, destIndex, a.length);
		}
	}
	
	
	public static <E> E[] copyAddElement(final List<? extends E> l, final int n, final int index, final E e) {
		@SuppressWarnings("unchecked")
		final E[] a= (E[])new Object[n + 1];
		if (l instanceof AbstractImList<?>) {
			if (index == 0) {
				((AbstractImList<?>)l).copyTo(a, 1);
			}
			else if (index == n) {
				((AbstractImList<?>)l).copyTo(a, 0);
			}
			else {
				((AbstractImList<?>)l).copyTo(0, a, 0, index);
				((AbstractImList<?>)l).copyTo(index, a, index + 1, n - index);
			}
		}
		else {
			if (index == 0) {
				final Object[] srcArray= l.toArray();
				System.arraycopy(srcArray, 0, a, 1, n);
			}
			else if (index == n) {
				l.toArray(a);
			}
			else {
				final Object[] srcArray= l.toArray();
				System.arraycopy(srcArray, 0, a, 0, index);
				System.arraycopy(srcArray, index, a, index + 1, n - index);
			}
		}
		a[index]= e;
		return a;
	}
	
	public static <E> E[] copyAddElement(final Collection<? extends E> l, final int n, final E e) {
		@SuppressWarnings("unchecked")
		final E[] a= (E[])new Object[n + 1];
		if (l instanceof AbstractImList<?>) {
			((AbstractImList<?>)l).copyTo(a, 0);
		}
		else {
			l.toArray(a);
		}
		a[n]= e;
		return a;
	}
	
	public static <E> E[] copyRemoveElement(final List<? extends E> l, final int n, final int index) {
		@SuppressWarnings("unchecked")
		final E[] a= (E[])new Object[n];
		if (l instanceof AbstractImList<?>) {
			if (index > 0) {
				((AbstractImList<?>)l).copyTo(0, a, 0, index);
			}
			if (index < n) {
				((AbstractImList<?>)l).copyTo(index + 1, a, index, n - index);
			}
		}
		else {
			final Object[] src= l.toArray();
			if (index > 0) {
				System.arraycopy(src, 0, a, 0, index);
			}
			if (index < n) {
				System.arraycopy(src, index + 1, a, index, n - index);
			}
		}
		return a;
	}
	
	public static <E> E[] copyReverse(final E[] array, final int startIdx, final int endIdx) {
		@SuppressWarnings("unchecked")
		final E[] reversed= (E[])new Object[endIdx - startIdx];
		for (int idx= 0, idxSrc= endIdx - 1; idxSrc >= startIdx; idx++, idxSrc--) {
			reversed[idx]= array[idxSrc];
		}
		return reversed;
	}
	
	
	public static int[] copyAddElement(final IntList l, final int n, final int index, final int e) {
		final int[] a= new int[n + 1];
		if (l instanceof AbstractImIntList) {
			if (index == 0) {
				((AbstractImIntList)l).copyTo(a, 1);
			}
			else if (index == n) {
				((AbstractImIntList)l).copyTo(a, 0);
			}
			else {
				((AbstractImIntList)l).copyTo(0, a, 0, index);
				((AbstractImIntList)l).copyTo(index, a, index + 1, n - index);
			}
		}
		else {
			if (index == 0) {
				final int[] srcArray= l.toArray();
				System.arraycopy(srcArray, 0, a, 1, n);
			}
			else if (index == n) {
				l.toArray(a);
			}
			else {
				final int[] srcArray= l.toArray();
				System.arraycopy(srcArray, 0, a, 0, index);
				System.arraycopy(srcArray, index, a, index + 1, n - index);
			}
		}
		a[index]= e;
		return a;
	}
	
	public static int[] copyAddElement(final IntList l, final int n, final int e) {
		final int[] a= new int[n + 1];
		if (l instanceof AbstractImIntList) {
			((AbstractImIntList)l).copyTo(a, 0);
		}
		else {
			l.toArray(a);
		}
		a[n]= e;
		return a;
	}
	
	public static int[] repeat(final IntList l, final int times) {
		final int[] srcArray= l.toArray();
		final int[] a= new int[srcArray.length * times];
		for (int i= 0; i < times; i++) {
			System.arraycopy(srcArray, 0, a, i * srcArray.length, srcArray.length);
		}
		return a;
	}
	
	
	public static long[] copyAddElement(final LongList l, final int n, final int index, final long e) {
		final long[] a= new long[n + 1];
		if (l instanceof AbstractImLongList) {
			if (index == 0) {
				((AbstractImLongList)l).copyTo(a, 1);
			}
			else if (index == n) {
				((AbstractImLongList)l).copyTo(a, 0);
			}
			else {
				((AbstractImLongList)l).copyTo(0, a, 0, index);
				((AbstractImLongList)l).copyTo(index, a, index + 1, n - index);
			}
		}
		else {
			if (index == 0) {
				final long[] srcArray= l.toArray();
				System.arraycopy(srcArray, 0, a, 1, n);
			}
			else if (index == n) {
				l.toArray(a);
			}
			else {
				final long[] srcArray= l.toArray();
				System.arraycopy(srcArray, 0, a, 0, index);
				System.arraycopy(srcArray, index, a, index + 1, n - index);
			}
		}
		a[index]= e;
		return a;
	}
	
	public static long[] copyAddElement(final LongList l, final int n, final long e) {
		final long[] a= new long[n + 1];
		if (l instanceof AbstractImLongList) {
			((AbstractImLongList)l).copyTo(a, 0);
		}
		else {
			l.toArray(a);
		}
		a[n]= e;
		return a;
	}
	
	public static long[] repeat(final LongList l, final int times) {
		final long[] srcArray= l.toArray();
		final long[] a= new long[srcArray.length * times];
		for (int i= 0; i < times; i++) {
			System.arraycopy(srcArray, 0, a, i * srcArray.length, srcArray.length);
		}
		return a;
	}
	
	
	public static String toString(final int[] array, int startIdx, final int size) {
		if (size == 0) {
			return "[]"; //$NON-NLS-1$
		}
		final StringBuilder sb= new StringBuilder(8 + size * 4);
		sb.append('[');
		sb.append(array[startIdx++]);
		while (startIdx < size) {
			sb.append(", "); //$NON-NLS-1$
			sb.append(array[startIdx++]);
		}
		sb.append(']');
		return sb.toString();
	}
	
	public static String toString(final long[] array, int startIdx, final int size) {
		if (size == 0) {
			return "[]"; //$NON-NLS-1$
		}
		final StringBuilder sb= new StringBuilder(8 + size * 4);
		sb.append('[');
		sb.append(array[startIdx++]);
		while (startIdx < size) {
			sb.append(", "); //$NON-NLS-1$
			sb.append(array[startIdx++]);
		}
		sb.append(']');
		return sb.toString();
	}
	
	
	private ArrayUtils() {
	}
	
}
