/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import static org.eclipse.statet.jcommons.io.UriUtils.FILE_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SEPARATOR;
import static org.eclipse.statet.jcommons.io.UriUtils.isValidScheme;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.io.ArchiveUrl;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class CommonsUriInternals {
	
	
	public static final String NESTED_SCHEME= "nested"; //$NON-NLS-1$
	public static final String JAR_NESTED_SEPARATOR= "/!"; //$NON-NLS-1$
	
	private static final boolean NESTED_JAR_BOOT2_ENABLED= true;
	
	
	public static boolean startsWithScheme(final String urlString, final String expectedScheme) {
		final int n= expectedScheme.length();
		if (urlString.length() <= n) {
			return false;
		}
		for (int index= 0; index < n; index++) {
			final char c= urlString.charAt(index);
			final char cExpected= expectedScheme.charAt(index);
			if (c != cExpected && Character.toLowerCase(c) != cExpected) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean startsWithScheme(final String urlString, final int offset, final String expectedScheme) {
		final int n= expectedScheme.length();
		if (urlString.length() <= offset + n) {
			return false;
		}
		for (int index= 0; index < n; index++) {
			final char c= urlString.charAt(offset + index);
			final char cExpected= expectedScheme.charAt(index);
			if (c != cExpected && Character.toLowerCase(c) != cExpected) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isSchemeUrl(final String urlString, final String expectedScheme) {
		final int n= expectedScheme.length();
		if (urlString.length() <= n || urlString.charAt(n) != ':') {
			return false;
		}
		for (int index= 0; index < n; index++) {
			final char c= urlString.charAt(index);
			final char cExpected= expectedScheme.charAt(index);
			if (c != cExpected && Character.toLowerCase(c) != cExpected) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isSchemeUrl(final String urlString, final int offset, final String expectedScheme) {
		final int n= expectedScheme.length();
		if (urlString.length() <= offset + n || urlString.charAt(offset + n) != ':') {
			return false;
		}
		for (int index= 0; index < n; index++) {
			final char c= urlString.charAt(offset + index);
			final char cExpected= expectedScheme.charAt(index);
			if (c != cExpected && Character.toLowerCase(c) != cExpected) {
				return false;
			}
		}
		return true;
	}
	
	
	public static ArchiveUrl createJarArchiveUrl(final String urlString) throws URISyntaxException {
		final int schemeSep2Idx= urlString.indexOf(':', 4);
		if (schemeSep2Idx == -1 || !isValidScheme(urlString, 4, schemeSep2Idx)) {
			throw new URISyntaxException(urlString, "Valid scheme for JAR file is missing", 4);
		}
		final int jarSep1Idx= urlString.indexOf(JAR_SEPARATOR, schemeSep2Idx + 1);
		if (jarSep1Idx == -1) {
			throw new URISyntaxException(urlString, "JAR content separator '!/' is missing");
		}
		if (schemeSep2Idx == 10 && startsWithScheme(urlString, 4, NESTED_SCHEME)) {
			return new NestedJarStringUrl(urlString, jarSep1Idx);
		}
		final int jarSep2Idx= urlString.indexOf(JAR_SEPARATOR, jarSep1Idx + 2);
		if (jarSep2Idx != -1) {
			if (NESTED_JAR_BOOT2_ENABLED
					&& schemeSep2Idx == 8 && startsWithScheme(urlString, 4, FILE_SCHEME) ) {
				return new NestedJarBoot2StringUrl(urlString, jarSep1Idx, jarSep2Idx);
			}
			throw new URISyntaxException(urlString, "Unknown/Unsupported JAR URL type", 4);
		}
		return new JarStringUrl(urlString, schemeSep2Idx, jarSep1Idx);
	}
	
	public static ArchiveUrl createJarArchiveUrl(final URI url) throws URISyntaxException {
		final String ssp= url.getSchemeSpecificPart();
		final int schemeSep2Idx= ssp.indexOf(':');
		if (schemeSep2Idx == -1 || !isValidScheme(ssp, 0, schemeSep2Idx)) {
			throw new URISyntaxException(url.toString(), "Valid scheme for JAR file is missing");
		}
		final int jarSep1Idx= ssp.indexOf(JAR_SEPARATOR, schemeSep2Idx + 1);
		if (jarSep1Idx == -1) {
			throw new URISyntaxException(url.toString(), "JAR content separator '!/' is missing");
		}
		if (schemeSep2Idx == 6 && startsWithScheme(ssp, NESTED_SCHEME)) {
			return new NestedJarUriUrl(url, jarSep1Idx);
		}
		final int jarSep2Idx= ssp.indexOf(JAR_SEPARATOR, jarSep1Idx + 2);
		if (jarSep2Idx != -1) {
			if (NESTED_JAR_BOOT2_ENABLED
					&& schemeSep2Idx == 4 && startsWithScheme(ssp, FILE_SCHEME) ) {
				return new NestedJarBoot2UriUrl(url, jarSep1Idx, jarSep2Idx);
			}
			throw new URISyntaxException(url.toString(), "Unknown/Unsupported JAR URL type", 4);
		}
		return new JarUriUrl(url, schemeSep2Idx, jarSep1Idx);
	}
	
	
	private CommonsUriInternals() {
	}
	
}
