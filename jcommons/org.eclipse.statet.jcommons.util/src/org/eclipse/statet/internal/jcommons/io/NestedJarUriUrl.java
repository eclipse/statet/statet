/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.JAR_NESTED_SEPARATOR;
import static org.eclipse.statet.jcommons.io.UriUtils.FILE_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SEPARATOR;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * {@code jar:nested:/rhelpserver-4/org.eclipse.statet.rhelp.server.jar/!BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/}
 * {@code jar:nested:/rhelpserver-4/org.eclipse.statet.rhelp.server.jar/!BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/META-INF/MANIFEST.MF}
 */
@NonNullByDefault
public final class NestedJarUriUrl extends AbstractNestedArchiveUriUrl {
	
	
	private final int nestSepIndex;
	private final int jarSepIndex;
	
	
	public NestedJarUriUrl(final URI url, final int jarSepIndex)
			throws URISyntaxException {
		super(url);
		final String ssp= url.getSchemeSpecificPart();
		final int nestSepIdx= ssp.indexOf(JAR_NESTED_SEPARATOR, 7);
		if (nestSepIdx == -1 || nestSepIdx >= jarSepIndex - 1) {
			throw new URISyntaxException(url.toString(), "Nested JAR separator '/!' is missing");
		}
		
		this.nestSepIndex= nestSepIdx;
		this.jarSepIndex= jarSepIndex;
	}
	
	
	@Override
	public String getOuterArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	protected URI createOuterArchiveUrl(final URI url) throws URISyntaxException {
		final String ssp= url.getSchemeSpecificPart();
		return new URI(FILE_SCHEME,
				ssp.substring(7, this.nestSepIndex),
				url.getFragment() );
	}
	
	@Override
	public String getInnerArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	protected URI createInnerArchiveUrl(final URI url) throws URISyntaxException {
		final String ssp= url.getSchemeSpecificPart();
		final var sb= new StringBuilder(this.jarSepIndex - 2); // 5 - 7 = -2
		sb.append(FILE_SCHEME + ':');
		sb.append(ssp, 7, this.jarSepIndex);
		sb.replace(this.nestSepIndex - 2, this.nestSepIndex, JAR_SEPARATOR);
		return new URI(JAR_SCHEME, sb.toString(), null);
	}
	
	@Override
	protected String createEntryPath(final URI url) {
		final String ssp= url.getSchemeSpecificPart();
		return ssp.substring(this.jarSepIndex + 2, ssp.length());
	}
	
	
}
