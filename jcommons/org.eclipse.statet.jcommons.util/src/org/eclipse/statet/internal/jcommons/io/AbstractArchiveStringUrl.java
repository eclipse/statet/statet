/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.io.ArchiveUrl;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class AbstractArchiveStringUrl implements ArchiveUrl {
	
	
	private volatile @Nullable URI url;
	private final String urlString;
	
	private volatile @Nullable URI outerArchiveUrl;
	private volatile @Nullable String outerArchiveUrlString;
	private volatile @Nullable String pathString;
	
	
	public AbstractArchiveStringUrl(final String urlString) {
		this.urlString= urlString;
	}
	
	
	@Override
	public final URI getUrl() throws URISyntaxException {
		var url= this.url;
		if (url == null) {
			url= new URI(this.urlString);
			this.url= url;
		}
		return url;
	}
	
	@Override
	public final String getUrlString() {
		return this.urlString;
	}
	
	
	@Override
	public final URI getOuterArchiveUrl() throws URISyntaxException {
		var url= this.outerArchiveUrl;
		if (url == null) {
			url= new URI(getOuterArchiveUrlString());
			this.outerArchiveUrl= url;
		}
		return url;
	}
	
	@Override
	public final String getOuterArchiveUrlString() {
		var s= this.outerArchiveUrlString;
		if (s == null) {
			s= createOuterArchiveUrl(this.urlString);
			this.outerArchiveUrlString= s;
		}
		return s;
	}
	
	protected abstract String createOuterArchiveUrl(String urlString);
	
	@Override
	public final String getInnerEntryPathString() {
		var s= this.pathString;
		if (s == null) {
			s= createEntryPath(this.urlString);
			this.pathString= s;
		}
		return s;
	}
	
	protected abstract String createEntryPath(String urlString);
	
	
	@Override
	public String toString() {
		return getUrlString();
	}
	
}
