/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;

import org.eclipse.statet.jcommons.collections.ImCollection.MappingResult;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Immutable empty list implementation.
 * <p>
 * Comparable to <code>Collections.emptyList()</code>.</p>
 * 
 * @since de.walware.ecommons.coremisc 1.2
 */
@NonNullByDefault
public final class ImEmptyList<E> extends AbstractImList<E> implements ImList<E>,
		RandomAccess, MappingResult<E> {
	
	
	@SuppressWarnings("rawtypes")
	public static final ImEmptyList INSTANCE= new ImEmptyList();
	
	
	private final ListIterator<E> iterator= new AbstractImListIter<>() {
		
		@Override
		public boolean hasNext() {
			return false;
		}
		
		@Override
		public int nextIndex() {
			return 0;
		}
		
		@Override
		public E next() {
			throw new NoSuchElementException();
		}
		
		@Override
		public boolean hasPrevious() {
			return false;
		}
		
		@Override
		public int previousIndex() {
			return -1;
		}
		
		@Override
		public E previous() {
			throw new NoSuchElementException();
		}
		
	};
	
	
	public ImEmptyList() {
	}
	
	
	@Override
	public int size() {
		return 0;
	}
	
	@Override
	public boolean isEmpty() {
		return true;
	}
	
	@Override
	public boolean contains(final @Nullable Object e) {
		return false;
	}
	
	@Override
	public boolean containsAll(final Collection<?> c) {
		return c.isEmpty();
	}
	
	@Override
	public E get(final int index) {
		throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
	}
	
	@Override
	public E getFirst() {
		throw new NoSuchElementException();
	}
	
	@Override
	public E getLast() {
		throw new NoSuchElementException();
	}
	
	@Override
	public int indexOf(final @Nullable Object e) {
		return -1;
	}
	
	@Override
	public int lastIndexOf(final @Nullable Object e) {
		return -1;
	}
	
	
	@Override
	public Iterator<E> iterator() {
		return this.iterator;
	}
	
	@Override
	public ListIterator<E> listIterator() {
		return this.iterator;
	}
	
	@Override
	public ListIterator<E> listIterator(final int index) {
		if (index != 0) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return this.iterator;
	}
	
	@Override
	public Spliterator<E> spliterator() {
		return Spliterators.emptySpliterator();
	}
	
	
	@Override
	public ImList<E> subList(final int fromIndex, final int toIndex) {
		if (fromIndex < 0 || toIndex > 0) {
			throw new IndexOutOfBoundsException("fromIndex= " + fromIndex + ", toIndex= " + toIndex + ", size= " + 1); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (fromIndex > toIndex) {
			throw new IllegalArgumentException("fromIndex > toIndex: fromIndex= " + fromIndex + ", toIndex= " + toIndex); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return this;
	}
	
	@Override
	public Object[] toArray() {
		return ArrayUtils.EMPTY_OBJ;
	}
	
	@Override
	@SuppressWarnings("null")
	public <T> T[] toArray(final T[] dest) {
		if (dest.length > 0) {
			dest[0]= null;
		}
		return dest;
	}
	
	@Override
	public <T> T[] toArray(final IntFunction<T[]> generator) {
		return generator.apply(0);
	}
	
	@Override
	public void copyTo(final int srcIndex, final Object[] dest, final int destIndex, final int length) {
		assert (false);
	}
	
	@Override
	public void copyTo(final Object[] dest, final int destIndex) {
	}
	
	@Override
	public ImList<E> toList() {
		return this;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public ImIdentityList<E> toIdentityList() {
		return ImEmptyIdentityList.INSTANCE;
	}
	
	@Override
	public ImList<E> reversed() {
		return this;
	}
	
	@Override
	@SuppressWarnings({ "unchecked" })
	public <R> MappingResult<R> map(final Function<E, R> mapper) {
		nonNullAssert(mapper);
		return INSTANCE;
	}
	
	@Override
	public @Nullable E findFirst(final Predicate<E> predicate) {
		nonNullAssert(predicate);
		return null;
	}
	
	@Override
	public boolean anyMatch(final Predicate<? super E> predicate) {
		nonNullAssert(predicate);
		return false;
	}
	
	@Override
	public boolean allMatch(final Predicate<? super E> predicate) {
		nonNullAssert(predicate);
		return true;
	}
	
	@Override
	public boolean noneMatch(final Predicate<? super E> predicate) {
		nonNullAssert(predicate);
		return true;
	}
	
	
	@Override
	public int hashCode() {
		return 1;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof final List<?> other) {
			return (other.isEmpty());
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return "[]"; //$NON-NLS-1$
	}
	
}
