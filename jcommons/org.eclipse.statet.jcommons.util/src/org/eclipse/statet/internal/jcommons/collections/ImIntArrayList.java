/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.collections;

import java.util.Arrays;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ImIntArrayList extends AbstractImIntList {
	
	
	private final int[] array;
	
	
	public ImIntArrayList(final int[] array) {
		this.array= array;
	}
	
	
	@Override
	public int size() {
		return this.array.length;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public boolean contains(final int e) {
		return (indexOf(e) >= 0);
	}
	
	@Override
	public int getAt(final int index) {
		if (index < 0 || index >= this.array.length) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return this.array[index];
	}
	
	@Override
	public int getFirst() {
		return this.array[0];
	}
	
	@Override
	public int getLast() {
		return this.array[this.array.length - 1];
	}
	
	@Override
	public int indexOf(final int e) {
		for (int i= 0; i < this.array.length; i++) {
			if (this.array[i] == e) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public int lastIndexOf(final int e) {
		for (int i= this.array.length - 1; i >= 0; i--) {
			if (this.array[i] == e) {
				return i;
			}
		}
		return -1;
	}
	
	
	@Override
	public IntStream stream() {
		return StreamSupport.intStream(Spliterators.spliterator(this.array,
				Spliterator.ORDERED | Spliterator.IMMUTABLE ), false );
	}
	
	
	@Override
	public int[] toArray() {
		return Arrays.copyOf(this.array, this.array.length);
	}
	
	@Override
	public void toArray(final int[] array) {
		System.arraycopy(this.array, 0, array, 0, this.array.length);
	}
	
	@Override
	public void copyTo(final int srcIndex, final int[] dest, final int destIndex, final int length) {
		System.arraycopy(this.array, srcIndex, dest, destIndex, length);
	}
	
	@Override
	public void copyTo(final int[] dest, final int destIndex) {
		System.arraycopy(this.array, 0, dest, destIndex, this.array.length);
	}
	
	
	@Override
	public String getString() {
		return new String(this.array, 0, this.array.length);
	}
	
	
	@Override
	public int hashCode() {
		int hashCode= 9;
		for (int i= 0; i < this.array.length; i++) {
			hashCode= 31 * hashCode + this.array[i];
		}
		return hashCode;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof final IntList other) {
			if (this.array.length != other.size()) {
				return false;
			}
			for (int i= 0; i < this.array.length; i++) {
				if (this.array[i] != other.getAt(i)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return ArrayUtils.toString(this.array, 0, this.array.length);
	}
	
}
