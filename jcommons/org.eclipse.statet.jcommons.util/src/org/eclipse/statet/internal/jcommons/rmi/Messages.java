/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.rmi;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class Messages {
	
	
	public static String RMI_status_RegistryAlreadyStarted_message;
	public static String RMI_status_RegistryStartFailed_message;
	public static String RMI_status_RegistryStartFailedPortAlreadyUsed_message;
	public static String RMI_status_RegistryStartFailedWithExitValue_message;
	public static String RMI_status_RegistryStopFailedNotFound_message;
	
	
	static {
		RMI_status_RegistryAlreadyStarted_message= "RMI Registry at port {0} is already started.";
		RMI_status_RegistryStartFailed_message= "RMI Registry at port {0} could not be started.";
		RMI_status_RegistryStartFailedPortAlreadyUsed_message= "RMI Registry at port {0} could not be started, because the port is already used.";
		RMI_status_RegistryStartFailedWithExitValue_message= "RMI Registry at port {0} could not be started. The new process terminated with exit value {1}.";
		RMI_status_RegistryStopFailedNotFound_message= "RMI Registry at port {0} could not be found. Please note: Only registries started within an Eclipse session can be stopped with this feature.";
	}
	
}
