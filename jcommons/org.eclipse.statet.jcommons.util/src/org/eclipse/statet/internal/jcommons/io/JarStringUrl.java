/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.jcommons.io;

import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.JAR_NESTED_SEPARATOR;
import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.NESTED_SCHEME;
import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.isSchemeUrl;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SEPARATOR;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.io.UriUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * {@code jar:file:/rhelpserver-4/org.eclipse.statet.rhelp.server/BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/}
 * {@code jar:file:/rhelpserver-4/org.eclipse.statet.rhelp.server/BOOT-INF/lib/org.eclipse.statet.jcommons.util-4.9.0-SNAPSHOT.jar!/META-INF/MANIFEST.MF}
 */
@NonNullByDefault
public final class JarStringUrl extends AbstractArchiveStringUrl {
	
	
	private final int scheme2SepIndex;
	private final int jarSepIndex;
	private final int fragSepIndex;
	
	
	public JarStringUrl(final String urlString, final int scheme2SepIndex, final int jarSepIndex) {
		super(urlString);
		final int fragmentIdx= urlString.indexOf('#', jarSepIndex);
		
		this.scheme2SepIndex= scheme2SepIndex;
		this.jarSepIndex= jarSepIndex;
		this.fragSepIndex= ((fragmentIdx != -1) ? fragmentIdx : urlString.length());
	}
	
	public JarStringUrl(final String urlString)
			throws URISyntaxException {
		super(urlString);
		if (!isSchemeUrl(urlString, JAR_SCHEME)) {
			throw new IllegalArgumentException("url= " + urlString);
		}
		final int schemeSep2Idx= urlString.indexOf(':', 4);
		if (schemeSep2Idx == -1 || !UriUtils.isValidScheme(urlString, 4, schemeSep2Idx)) {
			throw new URISyntaxException(urlString, "Valid scheme for JAR file is missing", 4);
		}
		final int jarSepIdx= urlString.indexOf(JAR_SEPARATOR, schemeSep2Idx + 1);
		if (jarSepIdx == -1) {
			throw new URISyntaxException(urlString, "JAR content separator '!/' is missing", jarSepIdx);
		}
		final int fragmentIdx= urlString.indexOf('#', jarSepIdx);
		
		this.scheme2SepIndex= schemeSep2Idx;
		this.jarSepIndex= jarSepIdx;
		this.fragSepIndex= ((fragmentIdx != -1) ? fragmentIdx : urlString.length());
	}
	
	
	@Override
	public boolean isNested() {
		return false;
	}
	
	
	@Override
	public String getOuterArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	public String getOuterArchiveScheme() {
		return getUrlString().substring(4, this.scheme2SepIndex);
	}
	
	@Override
	protected String createOuterArchiveUrl(final String urlString) {
		return urlString.substring(4, this.jarSepIndex);
	}
	
	@Override
	public String getInnerArchiveType() {
		return JAR_SCHEME;
	}
	
	@Override
	public URI getInnerArchiveUrl() throws URISyntaxException {
		return getOuterArchiveUrl();
	}
	
	@Override
	public String getInnerArchiveUrlString() {
		return getOuterArchiveUrlString();
	}
	
	@Override
	protected String createEntryPath(final String urlString) {
		return urlString.substring(this.jarSepIndex + 2, this.fragSepIndex);
	}
	
	
	public String createInnerEntryUrlString(final String entryPath) {
		final String urlString= getUrlString();
		final var sb= new StringBuilder(urlString.length() + entryPath.length() + 2); // 11 - 9 = 2
		sb.append(JAR_SCHEME + ':' + NESTED_SCHEME + ':');
		sb.append(urlString, 9, this.jarSepIndex);
		sb.append(JAR_NESTED_SEPARATOR);
		sb.append(urlString, this.jarSepIndex + 2, this.fragSepIndex);
		sb.append(JAR_SEPARATOR);
		sb.append(entryPath);
		sb.append(urlString, this.fragSepIndex, urlString.length());
		return sb.toString();
	}
	
	
}
