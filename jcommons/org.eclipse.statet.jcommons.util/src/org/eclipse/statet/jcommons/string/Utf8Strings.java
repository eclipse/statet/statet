/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class Utf8Strings {
	
	
	public static boolean isValidCodepoint(final int cp) {
		return (cp >= 0x0000 && cp <= 0x10FFFF
				&& (cp & 0x1FF800) != 0xD800 );
	}
	
	private static int getCodepoint0(final byte[] bytes, final int offset, final int length) {
		if (length > 0) {
			final byte byte0= bytes[offset];
			if ((byte0 & (byte)0x80) != 0) {
				if ((byte0 & (byte)0x40) != 0) {
					if ((byte0 & (byte)0x20) != 0) {
						if ((byte0 & (byte)0x10) != 0) {
							if ((byte0 & (byte)0x08) == 0 && length >= 4 // 11110xxx
									&& (bytes[offset + 1] & (byte)0xC0) == (byte)0x80
									&& (bytes[offset + 2] & (byte)0xC0) == (byte)0x80
									&& (bytes[offset + 3] & (byte)0xC0) == (byte)0x80 ) {
								return (((byte0 << 6
										^ bytes[offset + 1]) << 6
										^ bytes[offset + 2]) << 6
										^ bytes[offset + 3])
										^ ((((byte)0xF0 << 6 ^ (byte)0x80) << 6 ^ (byte)0x80) << 6 ^ (byte)0x80);
							}
						}
						else if (length >= 3  // 1110xxxx
								&& (bytes[offset + 1] & (byte)0xC0) == (byte)0x80
								&& (bytes[offset + 2] & (byte)0xC0) == (byte)0x80 ) {
							final int cp= ((byte0 << 6
									^ bytes[offset + 1]) << 6
									^ bytes[offset + 2])
									^ (((byte)0xE0 << 6 ^ (byte)0x80) << 6 ^ (byte)0x80);
							if ((cp & 0xF800) != 0xD800) {
								return cp;
							}
						}
					}
					else if (length >= 2  // 110xxxxx
							&& (bytes[offset + 1] & (byte)0xC0) == (byte)0x80 ) {
						return (byte0 << 6
								^ bytes[offset + 1])
								^ ((byte)0xC0 << 6 ^ (byte)0x80);
					}
				}
			}
			else { // 0xxxxxxx
				return byte0;
			}
		}
		return -1;
	}
	
	public static int getCodepoint(final byte[] bytes, final int offset, final int endOffset) {
		if (endOffset > bytes.length || offset < 0 || offset > endOffset) {
			throw new IllegalArgumentException();
		}
		return getCodepoint0(bytes, offset, endOffset - offset);
	}
	
	public static int getCodepoint(final byte[] bytes, final int offset) {
		if (offset > bytes.length || offset < 0) {
			throw new IllegalArgumentException();
		}
		return getCodepoint0(bytes, offset, bytes.length - offset);
	}
	
	public static int getCodepointLength(final int cp) {
		if (cp < 0) {
			return 0;
		}
		else if (cp <= 0x007F) {
			return 1;
		}
		else if (cp <= 0x07FF) {
			return 2;
		}
		else if (cp <= 0xFFFF) {
			return 3;
		}
		else {
			return 4;
		}
	}
	
	
	private Utf8Strings() {
	}
	
}
