/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.util.StringUtils.insertSpaces;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Utils for multiline string considering the ASCII line separators
 * {@code "\n"} (U+000A), {@code "\r"} (U+000D), {@code "\r\n"} (U+000D U+000A).
 */
@NonNullByDefault
public final class MStringAUtils {
	
	
	public static final boolean isLineSeparator(final char c) {
		return (c == '\n' || c == '\r');
	}
	
	
	public static final void replaceTabsWithSpaces(final StringBuilder sb, final int tabWidth) {
		int column= 0;
		for (int index= 0; index < sb.length();) {
			final char c= sb.charAt(index);
			switch (c) {
			case '\r':
				if (++index < sb.length() && sb.charAt(index++) != '\n') {
					index--;
				}
				column= 0;
				continue;
			case '\n':
				index++;
				column= 0;
				continue;
			case '\t':
				final int numSpaces= tabWidth - column % tabWidth;
				sb.setCharAt(index++, ' ');
				if (numSpaces > 1) {
					insertSpaces(sb, index, numSpaces - 1);
					index+= numSpaces - 1;
				}
				column+= numSpaces;
				continue;
			default:
				index++;
				column++;
				continue;
			}
		}
	}
	
	/**
	 * Computes the minimal indent of all lines in the specified region.
	 * 
	 * An empty last line is ignore.
	 * 
	 * @param s
	 * @param startIndex start index of the region in {@code s}, inclusive.
	 * @param endIndex end index of region in {@code s}, exclusive.
	 * @param tabWidth width of tabs
	 * @return the indent
	 */
	public static final int computeIndent(final String s, final int startIndex, final int endIndex,
			final int tabWidth) {
		if (startIndex == endIndex) {
			return 0;
		}
		int minColumn= Integer.MAX_VALUE;
		int columns= 0;
		COUNT_COLUMNS: for (int index= startIndex; index < endIndex; ) {
			char c= s.charAt(index++);
			switch (c) {
			case ' ':
				columns++;
				continue COUNT_COLUMNS;
			case '\t':
				columns+= tabWidth - columns % tabWidth;
				continue COUNT_COLUMNS;
			default:
				if (columns < minColumn) {
					minColumn= columns;
				}
				columns= 0;
				
				while (index < endIndex) {
					switch (c) {
					case '\r':
						if (s.charAt(index++) != '\n') {
							index--;
						}
						continue COUNT_COLUMNS;
					case '\n':
						continue COUNT_COLUMNS;
					default:
						c= s.charAt(index++);
						continue;
					}
				}
				return minColumn;
			}
		}
		return (columns > 0 && columns < minColumn) ? columns : minColumn;
	}
	
	public static final int computeIndent(final String s,
			final int tabWidth) {
		return computeIndent(s, 0, s.length(), tabWidth);
	}
	
	/**
	 * Computes the minimal indent of all lines in the specified region.
	 * 
	 * An empty last line is ignore.
	 * 
	 * @param s
	 * @param startIndex start index of the region in {@code s}, inclusive.
	 * @param endIndex end index of region in {@code s}, exclusive.
	 * @param tabWidth width of tabs
	 * @return the indent
	 */
	public static final int computeIndent(final StringBuilder s, final int startIndex, final int endIndex,
			final int tabWidth) {
		if (startIndex == endIndex) {
			return 0;
		}
		int minColumns= Integer.MAX_VALUE;
		int column= 0;
		COUNT_COLUMNS: for (int index= startIndex; index < endIndex; ) {
			char c= s.charAt(index++);
			switch (c) {
			case ' ':
				column++;
				continue COUNT_COLUMNS;
			case '\t':
				column+= tabWidth - column % tabWidth;
				continue COUNT_COLUMNS;
			default:
				if (column < minColumns) {
					minColumns= column;
				}
				column= 0;
				
				while (index < endIndex) {
					switch (c) {
					case '\r':
						if (s.charAt(index++) != '\n') {
							index--;
						}
						continue COUNT_COLUMNS;
					case '\n':
						continue COUNT_COLUMNS;
					default:
						c= s.charAt(index++);
						continue;
					}
				}
				return minColumns;
			}
		}
		return (column > 0 && column < minColumns) ? column : minColumns;
	}
	
	public static final int computeIndent(final StringBuilder s,
			final int tabWidth) {
		return computeIndent(s, 0, s.length(), tabWidth);
	}
	
	
	/**
	 * Adds the lines of a string without its line delimiters to the specified list.
	 * 
	 * @param s the text
	 * @param lines list the lines are added to
	 */
	public static void collectLines(final String s, final int startIndex, final int endIndex,
			final Collection<String> lines) {
		int index= startIndex;
		int lineStartIndex= startIndex;
		while (index < endIndex) {
			switch (s.charAt(index)) {
			case '\r':
				lines.add(s.substring(lineStartIndex, index++));
				if (index < endIndex && s.charAt(index++) != '\n') {
					index--;
				}
				lineStartIndex= index;
				continue;
			case '\n':
				lines.add(s.substring(lineStartIndex, index++));
				lineStartIndex= index;
				continue;
			default:
				index++;
				continue;
			}
		}
		if (lineStartIndex < endIndex) {
			lines.add(s.substring(lineStartIndex, endIndex));
		}
	}
	
	public static void collectLines(final String s, final Collection<String> lines) {
		collectLines(s, 0, s.length(), lines);
	}
	
	public static List<String> linesToList(final String s) {
		final List<String> lines= new ArrayList<>(2 + s.length() / 20);
		collectLines(s, lines);
		return lines;
	}
	
	public static String getLastLine(final String s) {
		int start= s.length() - 1;
		if (start > 1) {
			if (s.charAt(start) == '\n') {
				start--;
			}
			start= s.lastIndexOf('\n', start);
			if (start != -1) {
				return s.substring(start + 1, s.length());
			}
		}
		return s;
	}
	
	
	/**
	 * Strips the first characters of each line.
	 * 
	 * An empty last line is ignored (not changed).
	 * 
	 * Note: the lines are not checked!
	 * 
	 * @param sb
	 * @param numChars
	 */
	public static final void stripLineStart(final StringBuilder sb, final int numChars) {
		if (numChars == 0 || sb.length() < numChars) {
			return;
		}
		int index= 0;
		sb.delete(0, numChars);
		while (index < sb.length() - numChars - 1) {
			final char c= sb.charAt(index++);
			switch (c) {
			case '\r':
				if (sb.charAt(index++) != '\n') {
					index--;
				}
				sb.delete(index, index + numChars);
				continue;
			case '\n':
				sb.delete(index, index + numChars);
				continue;
			default:
				continue;
			}
		}
	}
	
	public static final void appendWithLineStart(final StringBuilder sb,
			final String s, final int startIndex, final int endIndex,
			final String prefix,
			final boolean applyToFirstLine) {
		nonNullAssert(s);
		nonNullAssert(prefix);
		if (startIndex == endIndex) {
			return;
		}
		int index= startIndex;
		int lineStartIndex;
		if (applyToFirstLine) {
			lineStartIndex= startIndex;
		}
		else {
			CONSUME_LINE: while (index < endIndex) {
				switch (s.charAt(index++)) {
				case '\r':
					if (index < endIndex && s.charAt(index++) != '\n') {
						index--;
					}
					break CONSUME_LINE;
				case '\n':
					break CONSUME_LINE;
				default:
					continue;
				}
			}
			sb.append(s, startIndex, index);
			lineStartIndex= index;
		}
		while (index < endIndex) {
			switch (s.charAt(index++)) {
			case '\r':
				if (index < endIndex && s.charAt(index++) != '\n') {
					index--;
				}
				sb.append(prefix);
				sb.append(s, lineStartIndex, index);
				lineStartIndex= index;
				continue;
			case '\n':
				sb.append(prefix);
				sb.append(s, lineStartIndex, index);
				lineStartIndex= index;
				continue;
			default:
				continue;
			}
		}
		if (lineStartIndex < endIndex) {
			sb.append(prefix);
			sb.append(s, lineStartIndex, endIndex);
		}
	}
	
	public static final void appendWithLineStart(final StringBuilder sb,
			final String s,
			final String prefix,
			final boolean applyToFirstLine) {
		appendWithLineStart(sb, s, 0, s.length(), prefix, applyToFirstLine);
	}
	
	public static final void appendWithLineStart(final StringBuilder sb,
			final String s,
			final String prefix) {
		appendWithLineStart(sb, s, 0, s.length(), prefix, true);
	}
	
	
	private MStringAUtils() {
	}
	
}
