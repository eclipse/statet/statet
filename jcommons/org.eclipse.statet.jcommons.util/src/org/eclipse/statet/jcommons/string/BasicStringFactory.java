/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import java.util.concurrent.ForkJoinPool;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class BasicStringFactory implements StringFactory {
	
	
	public static final BasicStringFactory INSTANCE= new BasicStringFactory();
	
	
	static final int CHARTABLE_SIZE= 0x100;
	private static final @NonNull String[] CHARTABLE= new @NonNull String[CHARTABLE_SIZE];
	private static final @Nullable String[] PAIRTABLE= new @Nullable String[CHARTABLE_SIZE * CHARTABLE_SIZE]; 
	static {
		final var bytes= new byte[1];
		for (char c0= 0; c0 < CHARTABLE_SIZE; c0++) {
			bytes[0]= (byte)(c0 & 0xFF);
			CHARTABLE[c0]= new String(bytes, 0, 0, 1).intern();
		}
		
		ForkJoinPool.commonPool().execute(() -> {
			for (char c0= 0x20; c0 < 0x7F; c0++) {
				for (char c1= 0x20; c1 < 0x7F; c1++) {
					getPairString(c0 * CHARTABLE_SIZE + c1);
				}
			}
		});
	}
	
	static final String getCharString(final int c) {
		return CHARTABLE[c];
	}
	
	static final String getPairString(final int i) {
		String s= PAIRTABLE[i];
		if (s == null) {
			s= new String(new byte[] { (byte)(i / CHARTABLE_SIZE & 0xFF), (byte)(i % CHARTABLE_SIZE & 0xFF) }, 0, 0, 2).intern();
			PAIRTABLE[i]= s;
		}
		return s;
	}
	
	
	public BasicStringFactory() {
	}
	
	
	@Override
	public String get(final CharSequence s) {
		final char c0, c1;
		switch (s.length()) {
		case 0:
			return ""; //$NON-NLS-1$
		case 1:
			if ((c0= s.charAt(0)) < CHARTABLE_SIZE) {
				return getCharString(c0);
			}
			break;
		case 2:
			if ((c0= s.charAt(0)) < CHARTABLE_SIZE && (c1= s.charAt(1)) < CHARTABLE_SIZE) {
				return getPairString(c0 * CHARTABLE_SIZE + c1);
			}
			break;
		default:
			break;
		}
		return s.toString();
	}
	
	@Override
	public String get(final CharArrayString s) {
		final char c0, c1;
		switch (s.length()) {
		case 0:
			return ""; //$NON-NLS-1$
		case 1:
			if ((c0= s.charAt(0)) < CHARTABLE_SIZE) {
				return getCharString(c0);
			}
			break;
		case 2:
			if ((c0= s.charAt(0)) < CHARTABLE_SIZE && (c1= s.charAt(1)) < CHARTABLE_SIZE) {
				return getPairString(c0 * CHARTABLE_SIZE + c1);
			}
			break;
		default:
			break;
		}
		return s.toString();
	}
	
	@Override
	public String get(final String s) {
		final char c0, c1;
		switch (s.length()) {
		case 0:
			return ""; //$NON-NLS-1$
		case 1:
			if ((c0= s.charAt(0)) < CHARTABLE_SIZE) {
				return getCharString(c0);
			}
			break;
		case 2:
			if ((c0= s.charAt(0)) < CHARTABLE_SIZE && (c1= s.charAt(1)) < CHARTABLE_SIZE) {
				return getPairString(c0 * CHARTABLE_SIZE + c1);
			}
			break;
		default:
			break;
		}
		return s;
	}
	
	@Override
	public String get(final char c) {
		if (c < CHARTABLE_SIZE) {
			return getCharString(c);
		}
		return String.valueOf(c);
	}
	
	@Override
	public String get(final int codepoint) {
		if (codepoint >= 0 && codepoint < CHARTABLE_SIZE) {
			return getCharString(codepoint);
		}
		return Character.toString(codepoint);
	}
	
	
}
