/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class CharPair implements Immutable {
	
	
	public final char opening;
	
	public final char closing;
	
	
	public CharPair(final char left, final char right) {
		this.opening= left;
		this.closing= right;
	}
	
	
	public char[] toArray() {
		return new char[] { this.opening, this.closing };
	}
	
	
	@Override
	public int hashCode() {
		return (this.opening << 16 | this.closing);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final CharPair other
						&& this.opening == other.opening
						&& this.closing == other.closing ));
	}
	
	
	@Override
	public String toString() {
		return new String(new char[] { '[', this.opening, ',', ' ', this.closing, ']' });
	}
	
}
