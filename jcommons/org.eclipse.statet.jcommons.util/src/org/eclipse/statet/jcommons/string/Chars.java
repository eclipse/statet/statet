/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.string;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class Chars {
	
	
	private static final byte[] U_DIGITS= {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'A', 'B', 'C', 'D', 'E', 'F'
	};
	
	
	public static final String getAsciiControlAbbr(final char c) {
		return switch (c) {
		case 0x00 -> "NUL";
		case 0x01 -> "SOH";
		case 0x02 -> "STX";
		case 0x03 -> "ETX";
		case 0x04 -> "EOT";
		case 0x05 -> "ENQ";
		case 0x06 -> "ACK";
		case 0x07 -> "BEL";
		case 0x08 -> "BS";
		case 0x09 -> "HT";
		case 0x0A -> "LF";
		case 0x0B -> "VT";
		case 0x0C -> "FF";
		case 0x0D -> "CR";
		case 0x0E -> "SO";
		case 0x0F -> "SI";
		case 0x10 -> "DLE";
		case 0x11 -> "DC1";
		case 0x12 -> "DC2";
		case 0x13 -> "DC3";
		case 0x14 -> "DC4";
		case 0x15 -> "NAX";
		case 0x16 -> "SYN";
		case 0x17 -> "ETB";
		case 0x18 -> "CAN";
		case 0x19 -> "EM";
		case 0x1A -> "SUB";
		case 0x1B -> "ESC";
		case 0x1C -> "FS";
		case 0x1D -> "GS";
		case 0x1E -> "RS";
		case 0x1F -> "US";
		case 0x20 -> "SP";
		case 0x7F -> "DEL";
		default ->
				throw new IllegalArgumentException(String.format("c= 0x%1$04X", (int)c));
		};
	}
	
	public static final char getAsciiControlSymbol(final char c) {
		return switch (c) {
		case 0x00 -> '\u2400';
		case 0x01 -> '\u2401';
		case 0x02 -> '\u2402';
		case 0x03 -> '\u2403';
		case 0x04 -> '\u2404';
		case 0x05 -> '\u2405';
		case 0x06 -> '\u2406';
		case 0x07 -> '\u2407';
		case 0x08 -> '\u2408';
		case 0x09 -> '\u2409';
		case 0x0A -> '\u240A';
		case 0x0B -> '\u240B';
		case 0x0C -> '\u240C';
		case 0x0D -> '\u240D';
		case 0x0E -> '\u240E';
		case 0x0F -> '\u240F';
		case 0x10 -> '\u2410';
		case 0x11 -> '\u2411';
		case 0x12 -> '\u2412';
		case 0x13 -> '\u2413';
		case 0x14 -> '\u2414';
		case 0x15 -> '\u2415';
		case 0x16 -> '\u2416';
		case 0x17 -> '\u2417';
		case 0x18 -> '\u2418';
		case 0x19 -> '\u2419';
		case 0x1A -> '\u241A';
		case 0x1B -> '\u241B';
		case 0x1C -> '\u241C';
		case 0x1D -> '\u241D';
		case 0x1E -> '\u241E';
		case 0x1F -> '\u241F';
		case 0x20 -> '\u2420';
		case 0x7F -> '\u2421';
		default ->
				throw new IllegalArgumentException(String.format("c= 0x%1$04X", (int)c));
		};
	}
	
	/**
	 * Formats the Unicode code points in the standard U+XXXX notation.
	 * 
	 * @param cp the code point
	 * @return a string with the formatted code point
	 */
	@SuppressWarnings("deprecation")
	public static String formatCodePoint(int cp) {
		final int nDigits= Math.max(((Integer.SIZE + 7 - Integer.numberOfLeadingZeros(cp)) / 8) * 2, 4);
		final byte[] latin1= new byte[2 + nDigits];
		latin1[0]= 'U';
		latin1[1]= '+';
		int i= latin1.length;
		do {
			latin1[--i]= U_DIGITS[cp & 0xF];
			cp >>>= 4;
		} while (i > 2);
		return new String(latin1, 0, 0, latin1.length);
	}
	
	
	public static final CharPair CURLY_BRACKETS= new CharPair('{', '}');
	
	public static final CharPair ROUND_BRACKETS= new CharPair('(', ')');
	
	public static final CharPair SQUARE_BRACKETS= new CharPair('[', ']');
	
	
	private Chars() {
	}
	
}
