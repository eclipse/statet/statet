/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;


@NonNullByDefault
public abstract class AbstractStatus implements Status {
	
	
	private final String bundleId;
	
	private final int code;
	
	private final String message;
	
	private final @Nullable Throwable exception;
	
	
	public AbstractStatus(final String bundleId, final int code,
			final String message, final @Nullable Throwable exception) {
		if (bundleId.isEmpty()) {
			throw new IllegalArgumentException();
		}
		
		this.bundleId= bundleId;
		this.code= code;
		this.message= nonNullElse(message, ""); //$NON-NLS-1$
		this.exception= exception;
	}
	
	
	@Override
	public final String getBundleId() {
		return this.bundleId;
	}
	
	@Override
	public final int getCode() {
		return this.code;
	}
	
	
	@Override
	public String getMessage() {
		return this.message;
	}
	
	@Override
	public @Nullable Throwable getException() {
		return this.exception;
	}
	
	
	@Override
	public boolean isMultiStatus() {
		return false;
	}
	
	@Override
	public ImList<Status> getChildren() {
		return ImCollections.emptyList();
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder("Status"); //$NON-NLS-1$
		sb.append(' ', Statuses.getSeverityString(getSeverity()));
		sb.append(" ["); //$NON-NLS-1$
		sb.append(this.bundleId);
		sb.append(':');
		sb.append(this.code);
		sb.append(']');
		sb.addProp("message", this.message); //$NON-NLS-1$
		sb.addProp("exception", this.exception); //$NON-NLS-1$
		if (isMultiStatus()) {
			sb.addProp("children", getChildren());
		}
		return sb.toString();
	}
	
}
