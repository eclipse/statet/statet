/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class Statuses {
	
	static final String UNKNOWN_ID= "unknown"; //$NON-NLS-1$
	
	
	/**
	 * A standard OK status.
	 */
	@Deprecated
	public static final Status OK_STATUS= Status.OK_STATUS;
	
	/**
	 * A standard CANCEL status.
	 */
	@Deprecated
	public static final Status CANCEL_STATUS= Status.CANCEL_STATUS;
	
	
	@Deprecated
	public static Status newStatus(final int severity, final String bundleId, final int code,
			final String message, final @Nullable Throwable exception) {
		return Status.newStatus(severity, bundleId, code, message, exception);
	}
	
	@Deprecated
	public static Status newStatus(final int severity, final String bundleId,
			final String message, final @Nullable Throwable exception) {
		return Status.newStatus(severity, bundleId, message, exception);
	}
	
	@Deprecated
	public static Status newStatus(final int severity, final String bundleId, final String message) {
		return Status.newStatus(severity, bundleId, message);
	}
	
	/**
	 * Returns the more severe of two status.
	 * 
	 * Severity order is: ERROR > WARNING > INFO > OK.
	 * If the two statuses have the same severity, the second is returned.
	 */
	public static Status getMoreSevere(final Status status1, final Status status2) {
		if (status1.getSeverity() > status2.getSeverity()) {
			return status1;
		}
		else {
			return status2;
		}
	}
	
	/**
	 * Returns the most severe status from a list of statuses.
	 * 
	 * Severity order is: ERROR > WARNING > INFO > OK.
	 * If the two statuses have the same severity, the second is returned.
	 */
	public static @Nullable Status getMostSevere(final List<? extends Status> statuses) {
		Status max= null;
		for (final Status status : statuses) {
			if (status.getSeverity() == Status.ERROR) {
				return status;
			}
			if (max == null || status.getSeverity() >= max.getSeverity()) {
				max= status;
			}
		}
		return max;
	}
	
	
	public static String getSeverityString(final byte severity) {
		switch (severity) {
		case Status.OK:
			return "OK"; //$NON-NLS-1$
		case Status.INFO:
			return "INFO"; //$NON-NLS-1$
		case Status.WARNING:
			return "WARNING"; //$NON-NLS-1$
		case Status.ERROR:
			return "ERROR"; //$NON-NLS-1$
		case Status.CANCEL:
			return "CANCEL"; //$NON-NLS-1$
		default:
			throw new IllegalArgumentException("severity= " + severity); //$NON-NLS-1$
		}
	}
	
	
	private Statuses() {}
	
}
