/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class NullProgressMonitor implements ProgressMonitor {
	
	
	private final class SuppressIsCancelled implements ProgressMonitor {
		
		
		public SuppressIsCancelled() {
		}
		
		
		@Override
		public void beginTask(final String name, final int totalWork) {
		}
		
		@Override
		public void beginSubTask(final @Nullable String name) {
		}
		
		@Override
		public ProgressMonitor setWorkRemaining(final int work) {
			return this;
		}
		
		@Override
		public void addWorked(final int work) {
		}
		
		@Override
		public ProgressMonitor newSubMonitor(final int work) {
			return this;
		}
		
		@Override
		public ProgressMonitor newSubMonitor(final int work, final int flags) {
			return this;
		}
		
		@Override
		public void done() {
		}
		
		
		@Override
		public boolean isCanceled() {
			return false;
		}
		
		@Override
		public void setCanceled(final boolean state) {
			NullProgressMonitor.this.setCanceled(state);
		}
		
	}
	
	
	private volatile boolean isCanceled;
	
	
	public NullProgressMonitor() {
	}
	
	
	@Override
	public void beginTask(final String name, final int totalWork) {
	}
	
	@Override
	public void beginSubTask(final @Nullable String name) {
	}
	
	@Override
	public ProgressMonitor setWorkRemaining(final int work) {
		return this;
	}
	
	@Override
	public void addWorked(final int work) {
	}
	
	@Override
	public ProgressMonitor newSubMonitor(final int work) {
		return this;
	}
	
	@Override
	public ProgressMonitor newSubMonitor(final int work, final int flags) {
		return ((flags & SUPPRESS_ISCANCELED) != 0) ? new SuppressIsCancelled() : this;
	}
	
	@Override
	public void done() {
	}
	
	
	@Override
	public boolean isCanceled() {
		return this.isCanceled;
	}
	
	@Override
	public void setCanceled(final boolean state) {
		this.isCanceled= state;
	}
	
}
