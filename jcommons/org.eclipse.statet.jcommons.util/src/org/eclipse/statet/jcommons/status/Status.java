/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * A status object represents the outcome of an operation.
 * 
 * <p>A status carries the following properties:
 * <ul>
 * <li>{@link #getSeverity() severity}</li>
 * <li>{@link #getBundleId() bundle identifier}</li>
 * <li>{@link #getCode() status code}</li>
 * <li>{@link #getMessage() message}</li>
 * <li>{@link #getException() exception (optional)}</li>
 * </ul>
 * A {@link #isMultiStatus() multi-status} can contain other status objects as children.</p>
 * 
 * @see Statuses
 * @see OkStatus
 * @see InfoStatus
 * @see WarningStatus
 * @see ErrorStatus
 * @see CancelStatus
 * @see MultiStatus
 * @see StatusException
 */
@NonNullByDefault
public interface Status {
	
	
	/** Status severity indicating the status represents the normal case ("everything is fine"). */
	byte OK=         0;
	
	/** Status severity indicating the status is informational ("fyi"). */
	byte INFO=       1 << 0;
	
	/** Status severity indicating the status represents a warning. */
	byte WARNING=    1 << 1;
	
	/** Status severity indicating the status represents an error. */
	byte ERROR=      1 << 2;
	
	/** Status severity indicating the status represents a cancelation */
	byte CANCEL=     1 << 3;
	
	
	/**
	 * Returns the severity.
	 * 
	 * <p>Severity is one value of:
	 * <ul>
	 * <li><code>{@link #OK}</code></li>
	 * <li><code>{@link #INFO}</code></li>
	 * <li><code>{@link #WARNING}</code></li>
	 * <li><code>{@link #ERROR}</code></li>
	 * <li><code>{@link #CANCEL}</code></li>
	 * </ul></p>
	 * <p>The severity of a multi-status is defined to be the maximum severity of any of its
	 * children, or <code>OK</code> if it has no children.</p>
	 *
	 * @return the severity
	 */
	byte getSeverity();
	
	
	/**
	 * Returns the unique identifier of the bundle associated with this status.
	 *
	 * @return the unique identifier of the relevant bundle
	 */
	String getBundleId();
	
	/**
	 * Returns the bundle specific status code.
	 *
	 * @return the status code
	 */
	int getCode();
	
	
	/**
	 * Returns the message describing the outcome.
	 * 
	 * <p>The message is localized to the current locale.</p>
	 * 
	 * @return a localized message
	 */
	String getMessage();
	
	/**
	 * Returns the relevant low-level exception.
	 * 
	 * @return the relevant exception, or <code>null</code> if none
	 */
	@Nullable Throwable getException();
	
	
	/**
	 * Returns whether this status is a multi-status which can contain other status objects as
	 * children.
	 * 
	 * @return <code>true</code> if the status is a multi-status, otherwise <code>false</code>
	 * 
	 * @see #getChildren()
	 */
	boolean isMultiStatus();
	
	/**
	 * Returns a list of status objects contained in this status.
	 * 
	 * @return the children
	 * 
	 * @see #isMultiStatus()
	 */
	ImList<Status> getChildren();
	
	
	/**
	 * A standard OK status.
	 */
	static final Status OK_STATUS= new OkStatus(Statuses.UNKNOWN_ID, 0, "OK"); //$NON-NLS-1$
	
	/**
	 * A standard CANCEL status.
	 */
	static final Status CANCEL_STATUS= new CancelStatus(Statuses.UNKNOWN_ID, 1, "Canceled"); //$NON-NLS-1$
	
	
	/**
	 * Creates a new status with the specified properties.
	 * 
	 * @return the new status
	 */
	static Status newStatus(final int severity, final String bundleId, final int code,
			final String message, final @Nullable Throwable exception) {
		switch (severity) {
		case Status.OK:
			return new OkStatus(bundleId, code, message);
		case Status.INFO:
			return new InfoStatus(bundleId, code, message, exception);
		case Status.WARNING:
			return new WarningStatus(bundleId, code, message, exception);
		case Status.ERROR:
			return new ErrorStatus(bundleId, code, message, exception);
		case Status.CANCEL:
			return new CancelStatus(bundleId, code, message, exception);
		default:
			throw new IllegalArgumentException("severity= " + severity); //$NON-NLS-1$
		}
	}
	
	/**
	 * Creates a new status with the specified properties.
	 * 
	 * @return the new status
	 */
	static Status newStatus(final int severity, final String bundleId,
			final String message, final @Nullable Throwable exception) {
		switch (severity) {
		case Status.OK:
			return new OkStatus(bundleId, message);
		case Status.INFO:
			return new InfoStatus(bundleId, message, exception);
		case Status.WARNING:
			return new WarningStatus(bundleId, message, exception);
		case Status.ERROR:
			return new ErrorStatus(bundleId, message, exception);
		case Status.CANCEL:
			return new CancelStatus(bundleId, message, exception);
		default:
			throw new IllegalArgumentException("severity= " + severity); //$NON-NLS-1$
		}
	}
	
	/**
	 * Creates a new status with the specified properties.
	 * 
	 * @return the new status
	 */
	static Status newStatus(final int severity, final String bundleId, final String message) {
		switch (severity) {
		case Status.OK:
			return new OkStatus(bundleId, message);
		case Status.INFO:
			return new InfoStatus(bundleId, message);
		case Status.WARNING:
			return new WarningStatus(bundleId, message);
		case Status.ERROR:
			return new ErrorStatus(bundleId, message);
		case Status.CANCEL:
			return new CancelStatus(bundleId, message);
		default:
			throw new IllegalArgumentException("severity= " + severity); //$NON-NLS-1$
		}
	}
	
}
