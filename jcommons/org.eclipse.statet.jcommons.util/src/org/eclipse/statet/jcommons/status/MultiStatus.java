/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class MultiStatus extends AbstractStatus {
	
	
	private static byte computeSeverity(final ImList<Status> children) {
		byte severity= OK;
		for (final Status child : children) {
			if (child.getSeverity() > severity) {
				severity= child.getSeverity();
			}
		}
		return severity;
	}
	
	
	private byte severity;
	
	private ImList<Status> children;
	
	
	public MultiStatus(final String bundleId, final int code,
			final String message, final @Nullable Throwable exception,
			final ImList<Status> children) {
		super(bundleId, code, message, exception);
		this.severity= computeSeverity(children);
		this.children= children;
	}
	
	public MultiStatus(final String bundleId, final int code,
			final String message, final @Nullable Throwable exception) {
		super(bundleId, code, message, exception);
		this.severity= OK;
		this.children= ImCollections.newList();
	}
	
	public MultiStatus(final String bundleId, final int code,
			final String message) {
		super(bundleId, code, message, null);
		this.severity= OK;
		this.children= ImCollections.newList();
	}
	
	public MultiStatus(final String bundleId,
			final String message, final @Nullable Throwable exception,
			final ImList<Status> children) {
		super(bundleId, 0, message, exception);
		this.severity= computeSeverity(children);
		this.children= children;
	}
	
	public MultiStatus(final String bundleId,
			final String message, final @Nullable Throwable exception) {
		super(bundleId, 0, message, exception);
		this.severity= OK;
		this.children= ImCollections.newList();
	}
	
	public MultiStatus(final String bundleId, final String message) {
		super(bundleId, 0, message, null);
		this.severity= OK;
		this.children= ImCollections.newList();
	}
	
	
	@Override
	public byte getSeverity() {
		return this.severity;
	}
	
	
	@Override
	public final boolean isMultiStatus() {
		return true;
	}
	
	public void add(final Status status) {
		this.children= ImCollections.addElement(this.children, nonNullAssert(status));
		if (status.getSeverity() > this.severity) {
			this.severity= status.getSeverity();
		}
	}
	
	@Override
	public ImList<Status> getChildren() {
		return this.children;
	}
	
}
