/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status.util;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;

import org.eclipse.statet.internal.jcommons.status.StringBuilderWriter;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.Status;


@NonNullByDefault
public class StatusPrinter {
	
	
	public static String getSeverityFixWidthString(final byte severity) {
		switch (severity) {
		case Status.OK:
			return "OK     "; //$NON-NLS-1$
		case Status.INFO:
			return "INFO   "; //$NON-NLS-1$
		case Status.WARNING:
			return "WARNING"; //$NON-NLS-1$
		case Status.ERROR:
			return "ERROR  "; //$NON-NLS-1$
		case Status.CANCEL:
			return "CANCEL "; //$NON-NLS-1$
		default:
			throw new IllegalArgumentException("severity= " + severity); //$NON-NLS-1$
		}
	}
	
	
	public StatusPrinter() {
	}
	
	
	public void print(final List<Status> statusList, final PrintStream output) {
		print(statusList, null, output);
	}
	
	public void print(final List<Status> statusList, final PrintWriter output) {
		print(statusList, null, output);
	}
	
	public void print(final List<Status> statusList, final StringBuilder output) {
		final PrintWriter writer= new PrintWriter(new StringBuilderWriter(output));
		print(statusList, null, writer);
		writer.flush();
	}
	
	private void print(final List<Status> children, @Nullable StringBuilder text,
			final PrintStream output) {
		if (children == null || children.isEmpty()) {
			return;
		}
		if (text == null) {
			text= new StringBuilder("("); //$NON-NLS-1$
		}
		else {
			text.append('.');
		}
		final int baseTextLength= text.length();
		for (int i= 0; i < children.size(); i++) {
			final Status child= children.get(i);
			text.setLength(baseTextLength);
			text.append(i + 1);
			final int iTextLength= text.length();
			text.append(") "); //$NON-NLS-1$
			text.append(getSeverityFixWidthString(child.getSeverity()));
			text.append(" ["); //$NON-NLS-1$
			text.append(child.getBundleId());
			text.append(':');
			text.append(child.getCode());
			text.append("] ");
			text.append(child.getMessage());
			output.println(text);
			final Throwable exception= child.getException();
			if (exception != null) {
				exception.printStackTrace(output);
			}
			text.setLength(iTextLength);
			print(child.getChildren(), text, output);
		}
	}
	
	private void print(final List<Status> children, @Nullable StringBuilder text,
			final PrintWriter output) {
		if (children == null || children.isEmpty()) {
			return;
		}
		if (text == null) {
			text= new StringBuilder("("); //$NON-NLS-1$
		}
		else {
			text.append('.');
		}
		final int baseTextLength= text.length();
		for (int i= 0; i < children.size(); i++) {
			final Status child= children.get(i);
			text.setLength(baseTextLength);
			text.append(i + 1);
			final int iTextLength= text.length();
			text.append(") "); //$NON-NLS-1$
			text.append(getSeverityFixWidthString(child.getSeverity()));
			text.append(" ["); //$NON-NLS-1$
			text.append(child.getBundleId());
			text.append(':');
			text.append(child.getCode());
			text.append("] ");
			text.append(child.getMessage());
			output.println(text);
			final Throwable exception= child.getException();
			if (exception != null) {
				exception.printStackTrace(output);
			}
			text.setLength(iTextLength);
			print(child.getChildren(), text, output);
		}
	}
	
}
