/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status;

import java.io.PrintStream;
import java.io.PrintWriter;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.util.StatusPrinter;


@NonNullByDefault
public class StatusException extends Exception {
	
	
	private static final long serialVersionUID= 1L;
	
	private static final StatusPrinter PRINTER= new StatusPrinter();
	
	
	private final Status status;
	
	
	public StatusException(final Status status) {
		super(status.getMessage(), status.getException());
		this.status= status;
	}
	
	
	public Status getStatus() {
		return this.status;
	}
	
	
	@Override
	public void printStackTrace(final PrintStream output) {
		synchronized (output) {
			super.printStackTrace(output);
			if (this.status.isMultiStatus()) {
				output.println("Contains:"); //$NON-NLS-1$
				PRINTER.print(this.status.getChildren(), output);
			}
		}
	}
	
	@Override
	public void printStackTrace(final PrintWriter output) {
		synchronized (output) {
			super.printStackTrace(output);
			if (this.status.isMultiStatus()) {
				output.println("Contains:"); //$NON-NLS-1$
				PRINTER.print(this.status.getChildren(), output);
			}
		}
	}
	
}
