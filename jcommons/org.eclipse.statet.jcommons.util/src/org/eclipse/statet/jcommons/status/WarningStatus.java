/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.status;

import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class WarningStatus extends AbstractStatus implements Immutable {
	
	
	public WarningStatus(final String bundleId, final int code,
			final String message, final @Nullable Throwable exception) {
		super(bundleId, code, message, exception);
	}
	
	public WarningStatus(final String bundleId, final int code,
			final String message) {
		super(bundleId, code, message, null);
	}
	
	public WarningStatus(final String bundleId,
			final String message, final @Nullable Throwable exception) {
		super(bundleId, 0, message, exception);
	}
	
	public WarningStatus(final String bundleId, final String message) {
		super(bundleId, 0, message, null);
	}
	
	
	@Override
	public final byte getSeverity() {
		return WARNING;
	}
	
	
}
