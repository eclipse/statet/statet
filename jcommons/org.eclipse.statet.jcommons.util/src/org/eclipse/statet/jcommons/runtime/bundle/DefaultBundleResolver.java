/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime.bundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.status.MultiStatus;


/**
 * Bundle resolver based on a BundleEntryProvider.
 */
@NonNullByDefault
public class DefaultBundleResolver implements BundleResolver {
	
	
	public static final String ID= "BundleEntryProvider"; //$NON-NLS-1$
	
	
	private final BundleEntryProvider entryProvider;
	
	private @Nullable ImList<? extends BundleEntry> knownEntries;
	
	
	public DefaultBundleResolver(final BundleEntryProvider entryProvider) {
		this.entryProvider= entryProvider;
	}
	
	
	protected ImList<? extends BundleEntry> gatherKnownEntries() {
		final List<BundleEntry> entries= new ArrayList<>();
		this.entryProvider.getEntries(entries);
		return ImCollections.toList(entries);
	}
	
	
	@Override
	public boolean resolveBundle(final BundleSpec bundleSpec,
			final Set<BundleEntry> resolved, final MultiStatus status) {
		ImList<? extends BundleEntry> knownEntries= this.knownEntries;
		if (knownEntries == null) {
			knownEntries= gatherKnownEntries();
			this.knownEntries= knownEntries;
		}
		for (final BundleEntry entry : knownEntries) {
			if (entry.getBundleId().equals(bundleSpec.getId())) {
				resolved.add(entry);
				return true;
			}
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder("BundleResolver", getClass()); //$NON-NLS-1$
		sb.append(' ', ID);
		sb.addProp("entryProvider", this.entryProvider); //$NON-NLS-1$
		return sb.toString();
	}
	
}
