/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime.bundle;

import java.util.Set;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.status.MultiStatus;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * Bundle resolver using the reference class specified in the bundle spec.
 */
@NonNullByDefault
public class RefClassBundleResolver implements BundleResolver {
	
	
	public static final String ID= "RefClass"; //$NON-NLS-1$
	
	
	public RefClassBundleResolver() {
	}
	
	
	@Override
	public boolean resolveBundle(final BundleSpec bundleSpec,
			final Set<BundleEntry> resolved, final MultiStatus status) {
		final Class<?> refClass;
		if ((refClass= bundleSpec.getRefClass()) != null) {
			try {
				resolved.add(Bundles.detectEntry(refClass));
				return true;
			}
			catch (final StatusException e) {
			}
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder("BundleResolver", getClass()); //$NON-NLS-1$
		sb.append(' ', ID);
		return sb.toString();
	}
	
}
