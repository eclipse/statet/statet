/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime.bundle;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.ProviderNotFoundException;

import org.eclipse.statet.internal.jcommons.runtime.CommonsRuntimeInternals;
import org.eclipse.statet.jcommons.io.UriUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;


@NonNullByDefault
public abstract class BundleEntry implements Comparable<BundleEntry> {
	
	
	/**
	 * Entry for bundles distributed as jar file.
	 */
	public static class Jar extends BundleEntry {
		
		
		private @Nullable Path contentRoot;
		
		
		public Jar(final String bundleId, final Path path) {
			super(bundleId, path);
		}
		
		
		@Override
		public String getJClassPathUrlString() {
			String urlString= super.getJClassPathUrlString();
			// if it is a jar url, a fully qualified url with path the root directory inside the
			// jar (append "!/") is required
			if (UriUtils.isJarUrl(urlString)) {
				try {
					urlString= UriUtils.toJarUrlString(urlString);
				}
				catch (final URISyntaxException e) {
					throw new RuntimeException(e);
				}
			}
			return urlString;
		}
		
		private synchronized Path getContentRootPath() throws IOException, URISyntaxException, ProviderNotFoundException {
			Path root= this.contentRoot;
			if (root == null) {
				final FileSystem jarFS= FileSystems.newFileSystem(getPath());
				root= jarFS.getPath("/");
				this.contentRoot= root;
			}
			return root;
		}
		
		@Override
		public @Nullable Path getResourcePath(final String resource) {
			try {
				return getContentRootPath().resolve(resource);
			}
			catch (final IOException | ProviderNotFoundException e) {
				CommonsRuntime.log(new ErrorStatus(CommonsRuntimeInternals.BUNDLE_ID,
						String.format("An error occured while resolving resource path '%1$s' in '%2$s'.",
								resource, this ),
						e ));
				return null;
			}
			catch (final URISyntaxException e) {
				throw new RuntimeException(e);
			}
		}
		
		@Override
		public @Nullable String getResourceUrlString(final String resource) {
			try {
				final String urlString= UriUtils.toJarUrlString(getUrlString());
				if (urlString.endsWith("/")) { //$NON-NLS-1$
					return urlString + resource;
				}
			}
			catch (final URISyntaxException e) {
				throw new RuntimeException(e);
			}
			return null;
		}
		
	}
	
	/**
	 * Entry for bundles extracted in a file system directory.
	 */
	public static class Dir extends BundleEntry {
		
		
		private final Path jClassPath;
		
		
		public Dir(final String bundleId, final Path path,
				final Path jClassPath) {
			super(bundleId, path);
			
			this.jClassPath= jClassPath;
		}
		
		
		@Override
		public Path getJClassPath() {
			return this.jClassPath;
		}
		
	}
	
	
	protected final String bundleId;
	
	private final Path path;
	
	
	public BundleEntry(final String bundleId, final Path path) {
		this.bundleId= bundleId;
		this.path= path;
	}
	
	
	public String getBundleId() {
		return this.bundleId;
	}
	
	@Override
	public int compareTo(final BundleEntry o) {
		return this.bundleId.compareTo((o).bundleId);
	}
	
	
	public final Path getPath() {
		return this.path;
	}
	
	public String getUrlString() {
		return this.path.toUri().toString();
	}
	
	public Path getJClassPath() {
		return this.path;
	}
	
	public String getJClassPathString() {
		return getJClassPath().toString();
	}
	
	public String getJClassPathUrlString() {
		return getJClassPath().toUri().toString();
	}
	
	@Deprecated
	public String getCodebaseString() {
		return getJClassPath().toUri().toString();
	}
	
	
	public @Nullable Path getResourcePath(final String resource) {
		return this.path.resolve(resource);
	}
	
	public @Nullable String getResourceUrlString(final String resource) {
		final Path path= getResourcePath(resource);
		if (path != null) {
			return path.toUri().toString();
		}
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return this.bundleId.hashCode() + getPath().hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof final BundleEntry other) {
			return (this.bundleId.equals(other.bundleId)
					&& getPath().equals(other.getPath()) );
		}
		return super.equals(obj);
	}
	
	
	@Override
	public String toString() {
		return String.format("'%1$s' -> '%2$s'", this.bundleId, getUrlString()); //$NON-NLS-1$
	}
	
}
