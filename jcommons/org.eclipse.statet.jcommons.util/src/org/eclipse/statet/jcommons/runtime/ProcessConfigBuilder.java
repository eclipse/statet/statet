/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.Builder;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ProcessConfigBuilder implements Builder<ProcessConfig> {
	
	
	private String commandString;
	
	private final Map<String, String> environmentVars;
	
	private @Nullable OutputStream ioOutputStream;
	private @Nullable InputStream ioInputStream;
	
	
	public ProcessConfigBuilder() {
		this.commandString= ""; //$NON-NLS-1$
		this.environmentVars= new HashMap<>();
	}
	
	
	public String getCommandString() {
		return this.commandString;
	}
	
	public ProcessConfigBuilder setCommandString(final String command) {
		this.commandString= nonNullAssert(command);
		return this;
	}
	
	public Map<String, String> getEnvironmentVars() {
		return this.environmentVars;
	}
	
	public ProcessConfigBuilder addEnvironmentVar(final String name, final String value) {
		this.environmentVars.put(name, value);
		return this;
	}
	
	public ProcessConfigBuilder addEnvironmentVars(final Map<String, String> vars) {
		this.environmentVars.putAll(vars);
		return this;
	}
	
	public @Nullable InputStream getInputStream() {
		return this.ioInputStream;
	}
	
	public ProcessConfigBuilder setInputStream(final @Nullable InputStream in) {
		this.ioInputStream= in;
		return this;
	}
	
	public @Nullable OutputStream getOutputStream() {
		return this.ioOutputStream;
	}
	
	public ProcessConfigBuilder setOutputStream(final @Nullable OutputStream out) {
		this.ioOutputStream= out;
		return this;
	}
	
	
	@Override
	public ProcessConfig build() {
		return new ProcessConfig(this.commandString, this.environmentVars,
				this.ioInputStream, this.ioOutputStream );
	}
	
}
