/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime.bundle;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BundleSpec {
	
	
	private final String id;
	
	private final @Nullable Class<?> refClass;
	
	
	public BundleSpec(final String id, final @Nullable Class<?> refClass) {
		this.id= id;
		this.refClass= refClass;
	}
	
	public BundleSpec(final String id) {
		this(id, null);
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public @Nullable Class<?> getRefClass() {
		return this.refClass;
	}
	
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	
	@Override
	public @NonNull String toString() {
		return this.id;
	}
	
}
