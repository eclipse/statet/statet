/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime;

import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SCHEME;
import static org.eclipse.statet.jcommons.io.UriUtils.JAR_SEPARATOR;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.security.CodeSource;

import org.eclipse.statet.jcommons.io.UriUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ClassLoaderUtils {
	
	
	public static String getClassLocationUrlString(final Class<?> refClass) {
		String urlString;
		urlString= getUrlStringByClassloader(refClass);
//		if (urlString.startsWith("bundleresource://")) { // in osgi //$NON-NLS-1$
//			urlString= getUrlStringByDomain(refClass);
//		}
		return urlString;
	}
	
	private static String getUrlStringByDomain(final Class<?> refClass) {
		try {
			final CodeSource source= refClass.getProtectionDomain().getCodeSource();
			if (source == null) {
				throw new UnsupportedOperationException("CodeSource not available"); //$NON-NLS-1$
			}
			final URI url= source.getLocation().toURI();
			final String s= url.toString();
			if (s.endsWith(".jar")) { //$NON-NLS-1$
				final StringBuilder sb= new StringBuilder(s.length());
				if (!UriUtils.isJarUrl(s)) {
					sb.append(JAR_SCHEME + ':');
				}
				sb.append(s);
				sb.append(JAR_SEPARATOR);
				return sb.toString();
			}
			return s;
		}
		catch (final Exception e) {
			throw new UnsupportedOperationException(
					String.format("getUrlStringByDomain failed (refClass= %1$s)", refClass.getName()), //$NON-NLS-1$
					e );
		}
	}
	
	private static String getUrlStringByClassloader(final Class<?> refClass) {
		try {
			final String resourceName= refClass.getName().replace('.', '/') + ".class"; //$NON-NLS-1$
			final URI url= refClass.getClassLoader().getResource(resourceName).toURI();
			final String s= url.toString();
			if (s.endsWith(resourceName)) {
				return s.substring(0, s.length() - resourceName.length());
			}
			throw new UnsupportedOperationException("url= " + url); //$NON-NLS-1$
		}
		catch (final Exception e) {
			throw new UnsupportedOperationException(
					String.format("getUrlStringByClassloader failed (refClass= %1$s)", refClass.getName()), //$NON-NLS-1$
					e );
		}
	}
	
	
	public static String toJClassPathEntryString(final String urlString) {
		try {
			final URI fileUrl;
			final var archiveUrl= UriUtils.getArchiveUrl(urlString);
			if (archiveUrl != null && !archiveUrl.isNested()) {
				fileUrl= archiveUrl.getOuterArchiveUrl();
			}
			else if (UriUtils.isFileUrl(urlString)) {
				fileUrl= new URI(urlString);
			}
			else {
				throw new UnsupportedOperationException("url= " + urlString); //$NON-NLS-1$
			}
			
			final Path path= Path.of(fileUrl);
			return path.toString();
		}
		catch (final URISyntaxException e) {
			throw new UnsupportedOperationException("url= " + urlString, e); //$NON-NLS-1$
		}
	}
	
	
	private ClassLoaderUtils() {}
	
}
