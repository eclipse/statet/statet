/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.runtime;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class ProcessConfig implements Immutable {
	
	
	private final String commandString;
	
	private final Map<String, String> environmentVars;
	
	private final @Nullable InputStream ioInputStream;
	private final @Nullable OutputStream ioOutputStream;
	
	
	public ProcessConfig(final String commandString, final Map<String, String> environmentVars,
			final @Nullable InputStream ioInputStream, final @Nullable OutputStream ioOutputStream) {
		this.commandString= commandString;
		this.environmentVars= environmentVars;
		this.ioInputStream= ioInputStream;
		this.ioOutputStream= ioOutputStream;
	}
	
	
	public String getCommandString() {
		return this.commandString;
	}
	
	public Map<String, String> getEnvironmentVars() {
		return this.environmentVars;
	}
	
	public @Nullable InputStream getInputStream() {
		return this.ioInputStream;
	}
	
	public @Nullable OutputStream getOutputStream() {
		return this.ioOutputStream;
	}
	
}
