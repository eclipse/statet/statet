/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core;

import static org.eclipse.statet.internal.jcommons.net.CommonsNetInternals.BUNDLE_ID;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.runtime.CommonsRuntime.log;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;

import org.eclipse.statet.jcommons.collections.CopyOnWriteIdentityListSet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.net.Port;
import org.eclipse.statet.jcommons.runtime.ProcessConfig;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.InfoStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * Basic implementation of remote system access client session.
 */
@NonNullByDefault
public abstract class BasicRSAccessClientSession<TSession> implements RSAccessClientSession {
	
	
	private static final byte S_CONNECTING= 1;
	private static final byte S_CONNECTED= 2;
	private static final byte S_DISCONNECTED= 3;
	
	
	private static class SessionPortForwarding {
		
		
		private final InetSocketAddress targetAddress;
		private final InetSocketAddress localAddress;
		
		private final List<PortForwardingL> handles= new ArrayList<>();
		
		
		public SessionPortForwarding(final InetSocketAddress targetAddress, final InetSocketAddress localAddress) {
			this.targetAddress= targetAddress;
			this.localAddress= localAddress;
		}
		
		
		public InetSocketAddress getTargetAddress() {
			return this.targetAddress;
		}
		
		public InetSocketAddress getLocalAddress() {
			return this.localAddress;
		}
		
		
		public PortForwardingL createHandle() {
			final var handle= new PortForwardingL(this.targetAddress, this.localAddress);
			this.handles.add(handle);
			return handle;
		}
		
		public boolean removeHandle(final PortForwardingL handle) {
			return this.handles.remove(handle);
		}
		
		public boolean hasHandles() {
			return !this.handles.isEmpty();
		}
		
		
	}
	
	
	private volatile byte state;
	
	private volatile @Nullable TSession session;
	
	private final CopyOnWriteIdentityListSet<Listener> listeners= new CopyOnWriteIdentityListSet<>();
	
	private final @Nullable Duration timeout;
	
	private final Map<InetSocketAddress, SessionPortForwarding> managedPortForwardings= new HashMap<>();
	
	
	protected BasicRSAccessClientSession(final @Nullable Duration timeout) {
		this.timeout= timeout;
	}
	
	
	@Override
	public void addListener(final Listener listener) {
		this.listeners.add(nonNullAssert(listener));
	}
	
	@Override
	public void removeListener(final Listener listener) {
		this.listeners.remove(listener);
	}
	
	protected void notifyListeners(final Event event) {
		for (final var listener : this.listeners) {
			try {
				listener.onSessionChanged(event);
			}
			catch (final RuntimeException e) {
				final ToStringBuilder sb= buildToString();
				sb.addProp("listener", listener); //$NON-NLS-1$
				sb.addProp("event", event); //$NON-NLS-1$
				sb.getStringBuilder().insert(0, "An error occurred while notifying a listener of: ");
				log(new ErrorStatus(BUNDLE_ID, sb.toString(), e));
			}
		}
	}
	
	
	@Override
	public boolean isConnected() {
		if (this.state == S_CONNECTED) {
			final var session= this.session;
			if (session != null) {
				if (isConnected(session)) {
					return true;
				}
				else {
					disconnect();
				}
			}
		}
		return false;
	}
	
	private void setConnected(final TSession session) {
		synchronized (this) {
			if (this.state != S_CONNECTING) {
				throw new IllegalStateException();
			}
			this.state= S_CONNECTED;
			this.session= session;
		}
	}
	
	protected boolean isConnected(final TSession session) {
		return true;
	}
	
	public void connect(final ProgressMonitor m) throws StatusException {
		try {
			synchronized (this) {
				this.state= S_CONNECTING;
			}
			
			final var session= connect(this.timeout, m);
			setConnected(session);
		}
		catch (final Exception e) {
			disconnect(e);
			
			if (e instanceof final StatusException se) {
				if (se.getStatus().getSeverity() == IStatus.CANCEL) {
					throw se;
				}
			}
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"Failed to connect to remote host.",
					e ));
		}
	}
	
	@Override
	public void disconnect() {
		disconnect((Exception)null);
	}
	
	protected void disconnect(final @Nullable Exception reason) {
		final @Nullable TSession session;
		synchronized (this) {
			if (this.state == S_DISCONNECTED) {
				return;
			}
			session= this.session;
			this.state= S_DISCONNECTED;
			this.session= null;
		}
		try {
			disconnect(session);
		}
		catch (final Exception e) {
			if (reason != null) {
				reason.addSuppressed(e);
			}
			else {
				log(new ErrorStatus(BUNDLE_ID,
						String.format("Failed to close %1$s session.", getTarget().getType()),
						e ));
			}
		}
		finally {
			synchronized (this.managedPortForwardings) {
				this.managedPortForwardings.clear();
			}
			notifyListeners(new Event(Event.Type.DISCONNECT, this));
		}
	}
	
	protected abstract TSession connect(
			final @Nullable Duration timeout, final ProgressMonitor monitor) throws Exception;
	
	protected abstract void disconnect(final @Nullable TSession session) throws Exception;
	
	
	protected final @Nullable TSession getSession() {
		return this.session;
	}
	
	protected final TSession checkSession() throws StatusException {
		final var session= this.session;
		if (session == null) {
			throw new StatusException(new InfoStatus(BUNDLE_ID,
					String.format("The %1$s session is closed.", getTarget().getType()) ));
		}
		return session;
	}
	
	
	protected InetSocketAddress createTargetLocalhostAddress(final Port port) {
		return InetSocketAddress.createUnresolved(getTargetLocalhostString(), port.get());
	}
	
	protected @Nullable Duration getTimeout() {
		return this.timeout;
	}
	
	
	@Override
	public RemoteProcess exec(final ProcessConfig processConfig,
			final ProgressMonitor m) throws StatusException {
		try {
			final var session= checkSession();
			return exec(session, processConfig, getTimeout(), m);
		}
		catch (final Exception e) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"Failed to execute remote command.",
					e ));
		}
	}
	
	protected abstract RemoteProcess exec(TSession session,
			ProcessConfig processConfig,
			@Nullable Duration timeout, ProgressMonitor m) throws Exception;
	
	
	@Override
	public Socket createDirectTcpIpSocket(final InetSocketAddress targetAddress,
			final ProgressMonitor m) throws StatusException {
		try {
			final var session= checkSession();
			final var socketImpl= createDirectTcpIpSocketImpl(session);
			final var socket= new RemoteSocket(socketImpl);
			socket.connect(targetAddress, getTimeout(), m);
			return socket;
		}
		catch (final Exception e) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"Failed to create remote socket.",
					e ));
		}
	}
	
	@Override
	public Socket createDirectTcpIpSocket(final Port targetPort,
			final ProgressMonitor m) throws StatusException {
		return createDirectTcpIpSocket(createTargetLocalhostAddress(targetPort), m);
	}
	
	protected abstract TunnelClientSocketImpl createDirectTcpIpSocketImpl(final TSession session
			) throws Exception;
	
	
	@Override
	public PortForwardingL allocatePortForwardingL(final InetSocketAddress targetAddress) throws StatusException {
		try {
			final var session= checkSession();
			
			SessionPortForwarding managed;
			synchronized (this.managedPortForwardings) {
				managed= this.managedPortForwardings.get(targetAddress);
				if (managed != null) {
					return managed.createHandle();
				}
			}
			
			final var localAddress= startPortForwardingL(session, targetAddress, getTimeout());
			managed= new SessionPortForwarding(targetAddress, localAddress);
			
			synchronized (this.managedPortForwardings) {
				checkSession();
				this.managedPortForwardings.put(targetAddress, managed);
				return managed.createHandle();
			}
		}
		catch (final Exception e) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"Failed to start local port forwarding.",
					e ));
		}
	}
	
	@Override
	public PortForwardingL allocatePortForwardingL(final Port targetPort) throws StatusException {
		return allocatePortForwardingL(createTargetLocalhostAddress(targetPort));
	}
	
	@Override
	public void releasePortForwarding(final PortForwardingL handle) throws StatusException {
		try {
			final var session= getSession();
			if (session == null) {
				return;
			}
			
			final SessionPortForwarding managed;
			synchronized (this.managedPortForwardings) {
				managed= this.managedPortForwardings.get(handle.getTargetAddress());
				if (managed == null || !managed.removeHandle(handle)) {
					throw new IllegalArgumentException();
				}
				if (managed.hasHandles()) {
					return;
				}
				this.managedPortForwardings.remove(handle.getTargetAddress());
			}
			
			stopPortForwardingL(session, managed.getTargetAddress(), managed.getLocalAddress(), getTimeout());
		}
		catch (final Exception e) {
			if (this.state != S_CONNECTED) {
				return;
			}
			throw new StatusException(new ErrorStatus(BUNDLE_ID,
					"Failed to stop local port forwarding.",
					e ));
		}
	}
	
	protected abstract InetSocketAddress startPortForwardingL(TSession session,
			InetSocketAddress targetAddress,
			@Nullable Duration timeout) throws Exception;
	
	protected abstract void stopPortForwardingL(TSession session,
			InetSocketAddress targetAddress, InetSocketAddress localAddress,
			@Nullable Duration timeout) throws Exception;
	
	
	@Override
	public String toString() {
		return buildToString().toString();
	}
	
	protected abstract ToStringBuilder buildToString();
	
}
