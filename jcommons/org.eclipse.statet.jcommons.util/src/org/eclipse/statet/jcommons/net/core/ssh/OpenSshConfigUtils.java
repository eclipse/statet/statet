/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core.ssh;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.io.UriUtils;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.CommonsNet;


@NonNullByDefault
public class OpenSshConfigUtils {
	
	
	public static final String DATA_DIR_NAME= ".ssh"; //$NON-NLS-1$
	
	public static final String CONFIG_FILE_NAME= "config"; //$NON-NLS-1$
	public static final String KNOWN_HOSTS_FILE_NAME= "known_hosts"; //$NON-NLS-1$
	
	
	private static final Pattern SHORT_SSH_FORMAT= Pattern.compile("[\\w-.]+(?:@[\\w-.]+)?(?::\\d+)?"); //$NON-NLS-1$
	
	public static URI createSshUri(String s) throws URISyntaxException {
		s= s.trim();
		if (SHORT_SSH_FORMAT.matcher(s).matches()) {
			s= CommonsNet.SSH_SCHEME + "://" + s; //$NON-NLS-1$
		}
		final var uri= new URI(s);
		if (!UriUtils.isScheme(uri.getScheme(), CommonsNet.SSH_SCHEME)) {
			throw new URISyntaxException(s, "URI with scheme 'ssh' expected");
		}
		return uri;
	}
	
	
	/**
	 * Parses a ssh config flag value (yes/true/on - no/false/off).
	 *
	 * @param value the value string to convert
	 * @return {@code true} if {@code value} is "yes", "on", or "true";
	 *         {@code false} otherwise
	 */
	public static boolean parseTrue(@Nullable String value) {
		if (value == null) {
			return false;
		}
		value= value.trim();
		switch (value) {
		case "yes":
		case "true":
		case "on":
			return true;
		default:
			return false;
		}
	}
	
	public static boolean parseNone(final @Nullable String value) {
		if (value == null) {
			return false;
		}
		switch (value) {
		case "none":
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * Parses a positive integer value.
	 *
	 * @param value the value string to convert
	 * @return the value, or -1 if it wasn't a positive integer value
	 */
	public static int parsePositiveInt(@Nullable String value) {
		if (value == null) {
			return -1;
		}
		value= value.trim();
		try {
			return Integer.parseUnsignedInt(value);
		}
		catch (final NumberFormatException e) {
			return -1;
		}
	}
	
	/**
	 * Parses an OpenSSH time value as duration.
	 *
	 * @param value the value string to convert
	 * @return the parsed value as duration, or {@code null} if the value string is not valid
	 * @see <a href="https://man.openbsd.org/sshd_config.5#TIME_FORMATS">OpenBSD
	 *      man 5 sshd_config, section TIME FORMATS</a>
	 */
	public static @Nullable Duration parseDuration(@Nullable String value) {
		if (value == null) {
			return null;
		}
		@Nullable Duration duration= null;
		value= value.trim();
		final int length= value.length();
		int idx= 0;
		while (idx < length) {
			final int tOffset= idx;
			final int tValue;
			{	char c= value.charAt(idx);
				if (c == '+') {
					idx++;
				}
				while (idx < length) {
					c= value.charAt(idx++);
					if (c >= '0' && c <= '9') {
						continue;
					}
					else {
						idx--;
						break;
					}
				}
				try {
					tValue= Integer.parseInt(value, tOffset, idx, 10);
				}
				catch (final NumberFormatException e) {
					return duration;
				}
			}
			char tUnit= 's';
			if (idx < length) {
				tUnit= value.charAt(idx++);
				if (tUnit == ' ') {
					tUnit= 's';
				}
				else if (idx < length && value.charAt(idx) == ' ') {
					idx++;
				}
			}
			if (duration == null) {
				duration= Duration.ZERO;
			}
			switch (tUnit) {
			case 's':
			case 'S':
				duration= duration.plusSeconds(tValue);
				continue;
			case 'm':
			case 'M':
				duration= duration.plusMinutes(tValue);
				continue;
			case 'h':
			case 'H':
				duration= duration.plusHours(tValue);
				continue;
			case 'd':
			case 'D':
				duration= duration.plusDays(tValue);
				continue;
			case 'w':
			case 'W':
				duration= duration.plusDays(tValue * 7);
				continue;
			default:
				return null; // invalid spec
			}
		}
		return duration;
	}
	
	public static List<URI> parseProxyJump(final String value)
			throws URISyntaxException {
		final @NonNull String[] hops= value.split(","); //$NON-NLS-1$
		final var result= new ArrayList<URI>();
		for (final String hop : hops) {
			final URI to= createSshUri(hop);
			String path;
			if ((path= to.getPath()) != null && !path.isEmpty()) {
				throw new URISyntaxException(hop, "URI without path component expected");
			}
			result.add(to);
		}
		return result;
	}
	
	
	private OpenSshConfigUtils() {
	}
	
}
