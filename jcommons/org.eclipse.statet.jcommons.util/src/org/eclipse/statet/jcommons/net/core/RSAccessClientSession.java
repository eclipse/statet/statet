/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core;

import java.net.InetSocketAddress;
import java.net.Socket;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.net.CommonsNet;
import org.eclipse.statet.jcommons.net.Port;
import org.eclipse.statet.jcommons.runtime.ProcessConfig;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * Remote system access client session.
 */
@NonNullByDefault
public interface RSAccessClientSession {
	
	
	public static class Event {
		
		public static enum Type {
			
			DISCONNECT;
		}
		
		
		private final Type type;
		
		private final RSAccessClientSession session;
		
		
		public Event(final Type type, final RSAccessClientSession session) {
			this.type= type;
			this.session= session;
		}
		
		
		public Type getType() {
			return this.type;
		}
		
		public RSAccessClientSession getSession() {
			return this.session;
		}
		
		
		@Override
		public String toString() {
			return this.type.toString();
		}
		
	}
	
	public static interface Listener {
		
		void onSessionChanged(Event event);
		
	}
	
	
	public RemoteTarget getTarget();
	
	
	public void addListener(Listener listener);
	public void removeListener(Listener listener);
	
	
	public default String getTargetLocalhostString() {
		return CommonsNet.LOOPBACK_STRING;
	}
	
	
	public boolean isConnected();
	
	public void disconnect();
	
	
	public RemoteProcess exec(ProcessConfig processConfig,
			ProgressMonitor m) throws StatusException;
	
	public Socket createDirectTcpIpSocket(InetSocketAddress targetAddress,
			ProgressMonitor m) throws StatusException;
	
	public Socket createDirectTcpIpSocket(Port targetPort,
			ProgressMonitor m) throws StatusException;
	
	public PortForwardingL allocatePortForwardingL(InetSocketAddress targetAddress)
			throws StatusException;
	
	public PortForwardingL allocatePortForwardingL(Port remotePort)
			throws StatusException;
	
	public void releasePortForwarding(PortForwardingL handle)
			throws StatusException;
	
	
}
