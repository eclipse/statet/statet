/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core;

import java.net.InetSocketAddress;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class PortForwardingL {
	
	
	private final InetSocketAddress localAddress;
	
	private final InetSocketAddress targetAddress;
	
	
	public PortForwardingL(final InetSocketAddress targetAddress, final InetSocketAddress localAddress) {
		this.targetAddress= targetAddress;
		this.localAddress= localAddress;
	}
	
	
	public InetSocketAddress getTargetAddress() {
		return this.targetAddress;
	}
	
	public InetSocketAddress getLocalAddress() {
		return this.localAddress;
	}
	
}
