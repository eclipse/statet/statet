/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.net;

import java.io.Serializable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class PortRange implements Serializable {
	
	
	private static final long serialVersionUID= 6035156412290812307L;
	
	
	public static PortRange valueOf(final String s) throws IllegalArgumentException {
		if (s.length() >= 3) {
			final int idx= s.indexOf('-', 1);
			if (idx != -1) {
				return new PortRange(
						Integer.parseInt(s.substring(0, idx)),
						Integer.parseInt(s.substring(idx + 1, s.length())) );
			}
		}
		if (s.length() > 0) {
			return new PortRange(
					Integer.parseInt(s) );
		}
		throw new NumberFormatException("s= <empty>");
	}
	
	
	private final int min;
	private final int max;
	
	
	public PortRange(final int min, final int max) {
		if (!Port.isValidPortNum(min)) {
			throw new IllegalArgumentException("min= " + min); //$NON-NLS-1$
		}
		if (!Port.isValidPortNum(max)) {
			throw new IllegalArgumentException("max= " + max); //$NON-NLS-1$
		}
		if (min > max) {
			throw new IllegalArgumentException("min > max"); //$NON-NLS-1$
		}
		
		this.min= min;
		this.max= max;
	}
	
	public PortRange(final int num) {
		if (num < 0) {
			throw new IllegalArgumentException("num= " + num); //$NON-NLS-1$
		}
		this.min= num;
		this.max= num;
	}
	
	public PortRange(final Port from, final Port to) {
		this(from.get(), to.get());
	}
	
	
	public int getMin() {
		return this.min;
	}
	
	public int getMax() {
		return this.max;
	}
	
	public int getLength() {
		return this.max - this.min + 1;
	}
	
	public boolean contains(final Port port) {
		final int num= port.get();
		return (num >= this.min && num <= this.max);
	}
	
	public String getText() {
		final StringBuilder sb= new StringBuilder();
		sb.append(this.min);
		sb.append('-');
		sb.append(this.max);
		return sb.toString();
	}
	
	
	@Override
	public int hashCode() {
		return this.min + this.max + (this.max << 7);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final PortRange other
						&& this.min == other.min
						&& this.max == other.max ));
	}
	
	/**
	 * Compatible to {@link #valueOf(String)}
	 */
	@Override
	public String toString() {
		if (this.min == this.max) {
			return Integer.toString(this.min);
		}
		final StringBuilder sb= new StringBuilder();
		sb.append(this.min);
		sb.append('-');
		sb.append(this.max);
		return sb.toString();
	}
	
}
