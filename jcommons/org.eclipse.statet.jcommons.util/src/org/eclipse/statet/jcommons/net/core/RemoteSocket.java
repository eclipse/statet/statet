/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core;

import static org.eclipse.statet.jcommons.util.TimeUtils.toMillisInt;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.time.Duration;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public class RemoteSocket extends Socket {
	
	
	private class SocketOutputStream extends OutputStream {
		
		private final OutputStream delegate;
		
		SocketOutputStream(final OutputStream delegate) {
			this.delegate= delegate;
		}
		
		@Override
		public void close() throws IOException {
			RemoteSocket.this.close();
		}
		
		@Override
		public void write(final byte b[], final int off, final int len) throws IOException {
			this.delegate.write(b, off, len);
		}
		
		@Override
		public void write(final int b) throws IOException {
			write(new byte[] { (byte)b }, 0, 1);
		}
		
		@Override
		public void flush() throws IOException {
			this.delegate.flush();
		}
		
	}
	
	
	private final TunnelClientSocketImpl impl;
	
	private volatile @Nullable OutputStream out;
	
	
	public RemoteSocket(final TunnelClientSocketImpl impl) throws SocketException, IOException {
		super(impl);
		this.impl= impl;
	}
	
	
	public void connect(final InetSocketAddress targetAddress,
			final @Nullable Duration timeout, final ProgressMonitor m) throws IOException, StatusException {
		this.impl.connectMonitor= m;
		try {
			super.connect(targetAddress, toMillisInt(timeout, 0));
		}
		catch (final IOException e) {
			if (e.getCause() instanceof StatusException) {
				throw (StatusException)e.getCause();
			}
		}
		finally {
			this.impl.connectMonitor= null;
		}
	}
	
	
	@Override
	public OutputStream getOutputStream() throws IOException {
		if (isClosed()) {
			throw new SocketException("Socket is closed");
		}
		if (!isConnected()) {
			throw new SocketException("Socket is not connected");
		}
		if (isOutputShutdown()) {
			throw new SocketException("Socket output is shutdown");
		}
		var out= this.out;
		if (out == null) {
			synchronized (this) {
				if ((out= this.out) == null) {
					out= new SocketOutputStream(this.impl.getOutputStream());
					this.out= out;
				}
			}
		}
		return out;
	}
	
	
}
