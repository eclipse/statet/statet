/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core;

import static org.eclipse.statet.internal.jcommons.net.CommonsNetInternals.BUNDLE_ID;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.net.Port;
import org.eclipse.statet.jcommons.net.core.RSAccessClientSession.Event;
import org.eclipse.statet.jcommons.runtime.ProcessConfig;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * Basic implementation of remote system access session service.
 * 
 * Don't forget to dispose to close the open connections.
 */
@NonNullByDefault
public class BasicRSAccessSessionService implements RSAccessSessionService, Disposable {
	
	
	private static final byte STARTED= 1;
	private static final byte STOPPED= 2;
	
	private static final RSAccessClientSession CONNECTING= new SessionDummy();
	
	
	private final Map<RemoteTarget, RSAccessClientSession> pool;
	
	private final RSAccessClientSessionFactory<?> clientSessionFactory;
	
	private final RSAccessClientSession.Listener clientSessionListener;
	
	private byte state;
	
	
	public BasicRSAccessSessionService(final RSAccessClientSessionFactory<?> clientSessionFactory) {
		this.pool= new HashMap<>();
		this.state= STARTED;
		this.clientSessionFactory= clientSessionFactory;
		this.clientSessionListener= this::onSessionChanged;
	}
	
	@Override
	public void dispose() {
		final ImList<RSAccessClientSession> sessions;
		synchronized (this) {
			if (this.state == STOPPED) {
				return;
			}
			this.state= STOPPED;
			sessions= ImCollections.toList(this.pool.values());
			this.pool.clear();
		}
		for (final var session : sessions) {
			session.disconnect();
		}
		if (this.clientSessionFactory instanceof Disposable) {
			((Disposable)this.clientSessionFactory).dispose();
		}
	}
	
	
	protected void checkLive() throws StatusException {
		if (this.state != STARTED) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID, "The remote session service is shutdown."));
		}
	}
	
	@Override
	public RSAccessClientSession getClientSession(final RemoteTarget target,
			final ProgressMonitor m) throws StatusException {
		m.beginTask(String.format("Getting %1$s connection to '%2$s'", target.getType(), target.getHost()), 10);
		
		final var session= getClientSession0(target, m);
		
		m.done();
		return session;
	}
	
	protected RSAccessClientSession getClientSession0(final RemoteTarget target,
			final ProgressMonitor m) throws StatusException {
		RSAccessClientSession session;
		boolean ok= false;
		synchronized (this) {
			while (true) {
				checkLive();
				if (m.isCanceled()) {
					throw new StatusException(Status.CANCEL_STATUS);
				}
				
				session= this.pool.get(target);
				if (session == null) {
					this.pool.put(target, CONNECTING);
					break;
				}
				if (session != CONNECTING) {
					break;
				}
				try {
					wait();
				}
				catch (final InterruptedException e) {
				}
			}
		}
		m.addWorked(1);
		
		try {
			if (session == null) {
				session= createClientSession(target, m.newSubMonitor(8));
				session.addListener(this.clientSessionListener);
				synchronized (this) {
					checkLive();
					notifyAll();
					this.pool.put(target, session);
					ok= true;
					return session;
				}
			}
			else if (session.isConnected()) {
				ok= true;
				return session;
			}
		}
		finally {
			if (!ok) {
				synchronized (this) {
					notifyAll();
					this.pool.remove(target, (session != null) ? session : CONNECTING);
				}
				if (session != null) {
					session.disconnect();
				}
			}
		}
		
		m.setWorkRemaining(10);
		return getClientSession0(target, m);
	}
	
	protected RSAccessClientSession createClientSession(final RemoteTarget target,
			final ProgressMonitor m) throws StatusException {
		return this.clientSessionFactory.createSession(target, m);
	}
	
	protected void onSessionChanged(final Event event) {
		switch (event.getType()) {
		case DISCONNECT:
			synchronized (this) {
				notifyAll();
				this.pool.remove(event.getSession().getTarget());
			}
			return;
		default:
			return;
		}
	}
	
	
	@SuppressWarnings("null")
	private static class SessionDummy implements RSAccessClientSession {
		
		@Override
		public RemoteTarget getTarget() {
			return null;
		}
		
		@Override
		public void addListener(final Listener listener) {
		}
		
		@Override
		public void removeListener(final Listener listener) {
		}
		
		@Override
		public boolean isConnected() {
			return false;
		}
		
		@Override
		public void disconnect() {
		}
		
		@Override
		public RemoteProcess exec(final ProcessConfig processConfig, final ProgressMonitor m) throws StatusException {
			return null;
		}
		
		@Override
		public Socket createDirectTcpIpSocket(final InetSocketAddress targetAddress, final ProgressMonitor m) throws StatusException {
			return null;
		}
		
		@Override
		public Socket createDirectTcpIpSocket(final Port targetPort, final ProgressMonitor m) throws StatusException {
			return null;
		}
		
		@Override
		public PortForwardingL allocatePortForwardingL(final InetSocketAddress targetAddress) throws StatusException {
			return null;
		}
		
		@Override
		public PortForwardingL allocatePortForwardingL(final Port remotePort) throws StatusException {
			return null;
		}
		
		@Override
		public void releasePortForwarding(final PortForwardingL handle) throws StatusException {
		}
		
	}
	
}
