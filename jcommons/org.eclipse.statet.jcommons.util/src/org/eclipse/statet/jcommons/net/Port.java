/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.net;

import java.io.Serializable;

import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class Port implements Immutable, Serializable {
	
	
	private static final long serialVersionUID= -3612703291136601800L;
	
	
	public static Port valueOf(final String s) throws IllegalArgumentException {
		return new Port(Integer.parseInt(s));
	}
	
	public static Port valueOf(final Integer num) throws IllegalArgumentException {
		return new Port(num.intValue());
	}
	
	static final boolean isValidPortNum(final int num) {
		return (num >= 0 && num <= 65535);
	}
	
	
	private final int num;
	
	
	public Port(final int num) {
		if (!isValidPortNum(num)) {
			throw new IllegalArgumentException("num= " + num); //$NON-NLS-1$
		}
		this.num= num;
	}
	
	
	public int get() {
		return this.num;
	}
	
	public String getText() {
		return Integer.toString(this.num);
	}
	
	
	@Override
	public int hashCode() {
		return this.num ^ (this.num << 8);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| ((obj instanceof Port)
						&& this.num == ((Port) obj).num ));
	}
	
	/**
	 * Returns the port number as string.
	 * 
	 * Compatible to {@link #valueOf(String)}.
	 */
	@Override
	public String toString() {
		return Integer.toString(this.num);
	}
	
}
