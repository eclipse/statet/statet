/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core;

import static org.eclipse.statet.jcommons.util.Units.MILLI_NANO;

import java.time.Duration;

import org.eclipse.statet.internal.jcommons.net.CommonsNetInternals;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.CancelStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public abstract class RemoteProcess implements AutoCloseable {
	
	
	public static final int MISSING_EXIT_STATUS_CODE= -1;
	
	
	private final String command;
	
	
	public RemoteProcess(final String command) {
		this.command= command;
	}
	
	
	/**
	 * 
	 * @return {@code true} if the process has not yet exited
	 */
	public abstract boolean isAlive();
	
	/**
	 * 
	 * @param timeout the maximum time to wait or {@code null} for no timeout
	 * @return {@code true} if the process has exited
	 */
	public boolean waitFor(final @Nullable Duration timeout)
			throws InterruptedException {
		if (timeout == null) {
			while (isAlive()) {
				Thread.sleep(50);
			}
			return true;
		}
		else {
			final long timeoutTime= System.nanoTime() + timeout.toNanos();
			while (isAlive()) {
				final long remaining= timeoutTime - System.nanoTime();
				if (remaining <= 0) {
					return false;
				}
				Thread.sleep(Math.min(remaining / MILLI_NANO, 50));
			}
			return true;
		}
	}
	
	/**
	 * 
	 * @param timeout the maximum time to wait or {@code null} for no timeout
	 * @param m progress monitor checked for cancellation
	 * @return {@code true} if the process has exited
	 * @throws StatusException 
	 */
	public int waitFor(final @Nullable Duration timeout,
			final ProgressMonitor m) throws StatusException {
		if (timeout != null) {
			final long timeoutTime= System.nanoTime() + timeout.toNanos();
			while (isAlive()) {
				if (m.isCanceled()) {
					throw newCancelException();
				}
				final long remaining= timeoutTime - System.nanoTime();
				if (remaining <= 0) {
					throw newTimeoutException(timeout);
				}
				try {
					Thread.sleep(Math.min(remaining / MILLI_NANO, 50));
				}
				catch (final InterruptedException e) {}
			}
			return getExitStatusCode();
		}
		else {
			while (isAlive()) {
				if (m.isCanceled()) {
					throw newCancelException();
				}
				try {
					Thread.sleep(50);
				}
				catch (final InterruptedException e) {}
			}
			return getExitStatusCode();
		}
	}
	
	public abstract int getExitStatusCode();
	
	@Override
	public abstract void close();
	
	
	protected StatusException newCancelException() {
		return new StatusException(Status.CANCEL_STATUS);
	}
	
	protected StatusException newTimeoutException(final Duration timeout) {
		return new StatusException(new CancelStatus(CommonsNetInternals.BUNDLE_ID,
				String.format("The process has not exited within the specified timeout (%1$s).",
						timeout )));
	}
	
}
