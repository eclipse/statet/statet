/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core.ssh;

import java.time.Duration;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.net.core.BasicRSAccessClientSession;


@NonNullByDefault
public abstract class BasicSshClientSession<TSession> extends BasicRSAccessClientSession<TSession>
		implements SshClientSession {
	
	
	private final SshTarget target;
	
	
	public BasicSshClientSession(final SshTarget target, final @Nullable Duration timeout) {
		super(timeout);
		this.target= target;
	}
	
	
	@Override
	public SshTarget getTarget() {
		return this.target;
	}
	
	
	@Override
	protected ToStringBuilder buildToString() {
		final var sb= new ToStringBuilder(SshClientSession.class, getClass());
		sb.addProp("target", this.target); //$NON-NLS-1$
		return sb;
	}
	
}
