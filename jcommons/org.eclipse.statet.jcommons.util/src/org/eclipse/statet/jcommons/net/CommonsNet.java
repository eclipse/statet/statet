/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CommonsNet {
	
	
	public static final String LOCALHOST_NAME= "localhost"; //$NON-NLS-1$
	public static final String LOOPBACK_IPV4= "127.0.0.1"; //$NON-NLS-1$
	public static final String LOOPBACK_IPV6= "0:0:0:0:0:0:0:1"; //$NON-NLS-1$
	public static final String LOOPBACK_IPV6_SHORT= "::1"; //$NON-NLS-1$
	public static final String LOOPBACK_STRING;
	public static final String LOCAL_LOOPBACK_STRING;
	private static final InetAddress LOCAL_LOOPBACK_INET_ADDRESS;
	
	public static InetAddress getLocalLoopbackInetAddress() {
		return LOCAL_LOOPBACK_INET_ADDRESS;
	}
	
	public static boolean isCommonLoopback(final String host) {
		return (host.equals(LOCAL_LOOPBACK_STRING)
				|| host.equals(LOOPBACK_IPV4)
				|| host.equals(LOOPBACK_IPV6)
				|| host.equals('[' + LOOPBACK_IPV6 + ']')
				|| host.equals(LOOPBACK_IPV6_SHORT)
				|| host.equals('[' + LOOPBACK_IPV6_SHORT + ']')
				|| host.equals(LOCALHOST_NAME) );
	}
	
	public static boolean isLocalLocalhost(final String host) {
		if (isCommonLoopback(host)
				|| host.equals(LOCAL_LOOPBACK_INET_ADDRESS.getHostName()) ) {
			return true;
		}
		try {
			final InetAddress localhost= InetAddress.getLocalHost();
			if (!isCommonLoopback(localhost.getHostName())) {
				for (final var inetAddress : InetAddress.getAllByName(localhost.getHostName())) {
					if (host.equals(inetAddress.getHostName())
							|| host.equals(inetAddress.getHostAddress()) ) {
						return true;
					}
				}
			}
		}
		catch (final UnknownHostException e) {}
		catch (final ArrayIndexOutOfBoundsException e) { /* JVM bug */ }
		return false;
	}
	
	
	public static final String ANYADDRESS_IPV4= "0.0.0.0"; //$NON-NLS-1$
	public static final String ANYADDRESS_IPV6= "0:0:0:0:0:0:0:0"; //$NON-NLS-1$
	public static final String ANYADDRESS_IPV6_SHORT= "::"; //$NON-NLS-1$
	public static final String ANYADDRESS_STRING;
	public static final String LOCAL_ANYADDRESS_STRING;
	
	/** URI scheme for HTTP. */
	public static final String HTTP_SCHEME= "http"; //$NON-NLS-1$
	/** IANA assigned default port number for HTTP. */
	public static final int HTTP_DEFAULT_PORT_NUM= 80;
	/** IANA assigned default port for HTTP. */
	public static final Port HTTP_DEFAULT_PORT= new Port(HTTP_DEFAULT_PORT_NUM);
	
	/** URI scheme for HTTPS. */
	public static final String HTTPS_SCHEME= "https"; //$NON-NLS-1$
	/** IANA assigned default port number for HTTPS. */
	public static final int HTTPS_DEFAULT_PORT_NUM= 443;
	/** IANA assigned default port for HTTPS. */
	public static final Port HTTPS_DEFAULT_PORT= new Port(HTTPS_DEFAULT_PORT_NUM);
	
	/** URI scheme for SSH. */
	public static final String SSH_SCHEME= "ssh"; //$NON-NLS-1$
	/** URI scheme for SFTP. */
	public static final String SFTP_SCHEME= "sftp"; //$NON-NLS-1$
	/** IANA assigned default port number for SSH. */
	public static final int SSH_DEFAULT_PORT_NUM= 22;
	/** IANA assigned default port for SSH. */
	public static final Port SSH_DEFAULT_PORT= new Port(SSH_DEFAULT_PORT_NUM);
	
	
	static {
		LOOPBACK_STRING= LOOPBACK_IPV4;
		LOCAL_LOOPBACK_INET_ADDRESS= InetAddress.getLoopbackAddress();
		LOCAL_LOOPBACK_STRING= LOCAL_LOOPBACK_INET_ADDRESS.getHostAddress();
		
		ANYADDRESS_STRING= ANYADDRESS_IPV4;
		LOCAL_ANYADDRESS_STRING= ANYADDRESS_STRING;
	}
	
	
	public static boolean isIpV4Address(final String host) {
		int offset= 0;
		final int l= host.length();
		int nGroups= 0;
		while (nGroups < 4) {
			int nDigits= 0;
			ITER_CHAR: while (offset < l) {
				switch (host.charAt(offset)) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					nDigits++;
					offset++;
					continue ITER_CHAR;
				case '.':
					if (nGroups < 3) {
						break ITER_CHAR;
					}
					return false;
				default:
					return false;
				}
			}
			if (isDecByte(host, offset - nDigits, nDigits)) {
				nGroups++;
				offset++;
				continue;
			}
			return false;
		}
		return true;
	}
	
	public static boolean isIpV6Address(final String host) {
		try {
			if (!host.isEmpty()) {
				switch (host.charAt(0)) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case '[':
				case ':':
					if (host.indexOf(':') >= 0) {
						InetAddress.getByName(host);
						return true;
					}
					break;
				default:
					break;
				}
			}
		}
		catch (final UnknownHostException e) {}
		return false;
	}
	
	public static boolean isIpAddress(final String host) {
		return (isIpV4Address(host) || isIpV6Address(host));
	}
	
	
	public static Port getDefaultPort(final String scheme) throws UnknownSchemeException {
		return switch (scheme) {
		case HTTP_SCHEME -> HTTP_DEFAULT_PORT;
		case HTTPS_SCHEME -> HTTPS_DEFAULT_PORT;
		case SSH_SCHEME -> SSH_DEFAULT_PORT;
		default -> throw new UnknownSchemeException(scheme);
		};
	}
	
	public static int getDefaultPortNum(final String scheme) throws UnknownSchemeException {
		return switch (scheme) {
		case HTTP_SCHEME -> HTTP_DEFAULT_PORT_NUM;
		case HTTPS_SCHEME -> HTTPS_DEFAULT_PORT_NUM;
		case SSH_SCHEME -> SSH_DEFAULT_PORT_NUM;
		default -> throw new UnknownSchemeException(scheme);
		};
	}
	
	
	private static boolean isDecByte(final String s, final int offset, final int n) {
		switch (n) {
		case 0:
			return false;
		case 1:
			return true;
		case 2:
			if (s.charAt(offset) == '0') {
				return false;
			}
			return true;
		case 3:
			switch (s.charAt(offset)) {
			case '0':
				return false;
			case '1':
				return true;
			case '2':
				switch (s.charAt(offset + 1)) {
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
					return true;
				case '5':
					switch (s.charAt(offset + 2)) {
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
						return true;
					default:
						return false;
					}
				default:
					return false;
				}
			default:
				return false;
			}
		default:
			return false;
		}
	}
	
	
	private CommonsNet() {
	}
	
}
