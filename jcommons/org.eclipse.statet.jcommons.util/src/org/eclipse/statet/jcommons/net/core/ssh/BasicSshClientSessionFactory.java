/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core.ssh;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.nio.file.Path;
import java.time.Duration;

import org.eclipse.statet.jcommons.io.FileUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.core.RSAccessClientSessionFactory;
import org.eclipse.statet.jcommons.net.core.RemoteTarget;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public abstract class BasicSshClientSessionFactory implements RSAccessClientSessionFactory<SshClientSession> {
	
	
	private static final Duration SSH_TIMEOUT_DEFAULT;
	
	static {
		{	Duration timeout= null;
			final String value= System.getProperty("org.eclipse.statet.jcommons.net.core.ssh.Timeout.millis"); //$NON-NLS-1$
			if (value != null) {
				try {
					timeout= Duration.ofMillis(Integer.parseUnsignedInt(value));
				}
				catch (final NumberFormatException e) {}
			}
			if (timeout == null) {
				timeout= Duration.ofSeconds(30);
			}
			SSH_TIMEOUT_DEFAULT= timeout;
		}
	}
	
	
	private final @Nullable Duration timeoutDefault;
	
	
	public BasicSshClientSessionFactory() {
		this.timeoutDefault= SSH_TIMEOUT_DEFAULT;
	}
	
	
	protected Path getUserHomeDirectory() {
		return FileUtils.getUserHomeDirectory();
	}
	
	
	@Override
	public SshClientSession createSession(final RemoteTarget target,
			final ProgressMonitor m) throws StatusException {
		nonNullAssert(target);
		if (!(target instanceof SshTarget)) {
			throw new IllegalArgumentException("targetType= " + target.getClass().getName()); //$NON-NLS-1$
		}
		final var sshTarget= (SshTarget)target;
		return createSession(sshTarget, getTimeout(sshTarget), m);
	}
	
	protected @Nullable Duration getTimeout(final SshTarget target) {
		return this.timeoutDefault;
	}
	
	protected abstract SshClientSession createSession(final SshTarget target,
			final @Nullable Duration timeout, final ProgressMonitor m) throws StatusException;
	
	
}
