/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core.ssh;

import static org.eclipse.statet.internal.jcommons.net.CommonsNetInternals.BUNDLE_ID;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.net.CommonsNet.SSH_DEFAULT_PORT;
import static org.eclipse.statet.jcommons.net.CommonsNet.SSH_SCHEME;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.Port;
import org.eclipse.statet.jcommons.net.core.RemoteTarget;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public class SshTarget implements RemoteTarget {
	
	
	public static final String TYPE= "SSH"; //$NON-NLS-1$
	
	
	private final String host;
	
	private final Port port;
	
	private final String username;
	
	private final String targetId;
	
	private volatile URI uri;
	
	
	public SshTarget(final String host, final @Nullable Port port, final String username) throws StatusException {
		try {
			this.host= nonNullAssert(host);
			this.port= (port != null) ? port : SSH_DEFAULT_PORT;
			this.username= nonNullAssert(username);
			
			if (this.host.isEmpty()) {
				throw new IllegalArgumentException("host (hostname/IP) is missing");
			}
			if (this.username.isEmpty()) {
				throw new IllegalArgumentException("username is missing");
			}
			this.uri= new URI(SSH_SCHEME, this.username, this.host, this.port.get(), null, null, null);
			this.targetId= this.uri.toString();
		}
		catch (final URISyntaxException | IllegalArgumentException e) {
			throw new StatusException(new ErrorStatus(BUNDLE_ID, "Invalid SSH target.", e));
		}
	}
	
	
	@Override
	public String getType() {
		return TYPE;
	}
	
	
	@Override
	public String getHost() {
		return this.host;
	}
	
	public Port getPort() {
		return this.port;
	}
	
	public int getPortNum() {
		return this.port.get();
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public URI getUri() {
		return this.uri;
	}
	
	
	@Override
	public int hashCode() {
		return this.targetId.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj == this
				|| (obj != null && obj.getClass() == getClass()
						&& this.targetId.equals(((SshTarget)obj).targetId) ));
	}
	
	
	@Override
	public String toString() {
		return this.targetId;
	}
	
}
