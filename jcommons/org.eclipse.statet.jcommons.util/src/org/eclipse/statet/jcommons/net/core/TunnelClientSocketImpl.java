/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.net.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketImpl;
import java.net.SocketOptions;
import java.time.Duration;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public abstract class TunnelClientSocketImpl extends SocketImpl {
	
	
	protected static final int RCVBUF_DEFAULT= 32 * 1024;
	
	
	private @Nullable InetSocketAddress targetAddress;
	
	private @Nullable InputStream inputStream;
	private @Nullable OutputStream outputStream;
	
	protected boolean tcpNoDelay;
	
	protected @Nullable ProgressMonitor connectMonitor;
	
	
	public TunnelClientSocketImpl() throws IOException {
		this.localport= 0;
	}
	
	
	@Override
	protected void create(final boolean stream) throws IOException {
		if (!stream) {
			throw new IOException("Not supported");
		}
	}
	
	@Override
	protected void connect(final @Nullable String host, final int port) throws IOException {
		connect(InetAddress.getByName(host), port);
	}
	
	@Override
	protected void connect(final InetAddress address, final int port) throws IOException {
		connect(new InetSocketAddress(address, port), 0);
	}
	
	@Override
	protected void connect(final SocketAddress address, final int timeout) throws IOException {
		connect(address, (timeout != 0) ? Duration.ofMillis(timeout) : null);
	}
	
	protected void connect(final SocketAddress address,
			final @Nullable Duration timeout) throws IOException {
		nonNullAssert(address);
		if (!(address instanceof InetSocketAddress)) {
			throw new UnsupportedOperationException("addressType= " + address.getClass().toString());
		}
		final InetSocketAddress targetAddress= (InetSocketAddress)address;
		if (this.localport != 0) {
			throw new IOException("Not supported: reconnect to " + address.toString());
		}
		var m= this.connectMonitor;
		if (m == null) {
			m= new NullProgressMonitor();
		}
		try {
			connectRemote(targetAddress, timeout, m);
			assert(this.inputStream != null && this.outputStream != null);
			
			this.targetAddress= targetAddress;
			this.address= targetAddress.getAddress();
			this.port= targetAddress.getPort();
			this.localport= -1;
		}
		catch (final IOException | StatusException e) {
			try {
				close();
			}
			catch (final IOException ignore) {}
			throw (e instanceof IOException) ? (IOException)e : new IOException(e);
		}
	}
	
	protected @Nullable InetSocketAddress getTargetAddress() {
		return this.targetAddress;
	}
	
	protected void setInputStream(final InputStream inputStream) {
		this.inputStream= nonNullAssert(inputStream);
	}
	
	protected void setOutputStream(final OutputStream outputStream) {
		this.outputStream= nonNullAssert(outputStream);
	}
	
	protected abstract void connectRemote(final InetSocketAddress targetAddress,
			final @Nullable Duration timeout, final ProgressMonitor m) throws IOException, StatusException;
	
	
	@Override
	protected void bind(final InetAddress host, final int port) throws IOException {
		throw new IOException("Not supported");
	}
	
	@Override
	protected void listen(final int backlog) throws IOException {
		throw new IOException("Not supported");
	}
	
	@Override
	protected void accept(final SocketImpl s) throws IOException {
		throw new IOException("Not supported");
	}
	
	
	@Override
	protected InputStream getInputStream() throws IOException {
		final var inputStream= this.inputStream;
		if (inputStream == null) {
			throw new IOException("Not yet initialized");
		}
		return inputStream;
	}
	
	@Override
	protected OutputStream getOutputStream() throws IOException {
		final var outputStream= this.outputStream;
		if (outputStream == null) {
			throw new IOException("Not yet initialized");
		}
		return outputStream;
	}
	
	@Override
	protected int available() throws IOException {
		final var inputStream= this.inputStream;
		return (inputStream != null) ? inputStream.available() : 0;
	}
	
	@Override
	protected void close() throws IOException {
		this.localport= -1;
	}
	
	@Override
	protected void shutdownInput() throws IOException {
		final var inputStream= this.inputStream;
		if (inputStream == null) {
			return;
		}
		inputStream.close();
	}
	
	@Override
	protected void shutdownOutput() throws IOException {
		final var outputStream= this.outputStream;
		if (outputStream == null) {
			return;
		}
		outputStream.close();
	}
	
	@Override
	protected void sendUrgentData(final int data) throws IOException {
		throw new IOException("Not supported");
	}
	
	
	@Override
	public void setOption(final int optID, final Object value) throws SocketException {
		switch (optID) {
		case SocketOptions.TCP_NODELAY:
			if (((Boolean)value).booleanValue() == this.tcpNoDelay) {
				return;
			}
			break;
		case SocketOptions.SO_KEEPALIVE:
			if (((Boolean)value).booleanValue() == false) {
				return;
			}
			break;
		case SocketOptions.SO_TIMEOUT:
			return;
		case SocketOptions.SO_SNDBUF:
		case SocketOptions.SO_RCVBUF:
			return;
		default:
//			System.out.println("SshSocket setOption " + optID + "= " + value);
			break;
		}
		throw new SocketException("Not supported: option= " + optID + " with value= " + value);
	}
	
	@Override
	public Object getOption(final int optID) throws SocketException {
		switch (optID) {
		case SocketOptions.TCP_NODELAY:
			return Boolean.valueOf(this.tcpNoDelay);
		case SocketOptions.SO_KEEPALIVE:
			return Boolean.FALSE;
		case SocketOptions.SO_TIMEOUT:
			return Integer.valueOf(0);
		case SocketOptions.SO_SNDBUF:
			return Integer.valueOf(1024);
		case SocketOptions.SO_RCVBUF:
			return Integer.valueOf(RCVBUF_DEFAULT);
		default:
//			System.out.println("SshSocket getOption " + optID);
			break;
		}
		throw new SocketException("Not supported: option= " + optID);
	}
	
}
