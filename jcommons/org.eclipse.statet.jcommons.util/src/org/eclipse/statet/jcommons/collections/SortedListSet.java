/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.Comparator;
import java.util.Spliterator;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Sorted set, which also implements the list interface.
 */
@NonNullByDefault
public interface SortedListSet<E extends @NonNull Object> extends NonNullSet<E>, NonNullList<E> {
	
	
	/**
	 * Comparator used to compare the elements.
	 * 
	 * If the elements implements {@link Comparable}, the comparator can be unset.
	 * 
	 * @return the comparator or <code>null</code>
	 */
	@Nullable Comparator<? super E> getComparator();
	
	
	int indexOfE(@NonNull E element);
	
	/**
	 * 
	 * @param element element to add
	 * @return index of element (>= 0, if element was not yet in the list)
	 */
	int addE(@NonNull E element);
	
	/**
	 * 
	 * @param element element to remove
	 * @return index of element (>= 0, if element was in the list)
	 */
	int removeE(@NonNull E element);
	
	@Override
	SortedListSet<E> subList(int fromIndex, int toIndex);
	
	
	@Override
	default Spliterator<E> spliterator() {
		return NonNullList.super.spliterator();
	}
	
}
