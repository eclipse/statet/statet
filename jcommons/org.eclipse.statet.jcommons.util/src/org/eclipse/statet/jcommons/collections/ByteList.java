/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.nio.charset.Charset;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface ByteList {
	
	
	public int size();
	
	public boolean isEmpty();
	
	public boolean contains(byte e);
	
	public byte getAt(int index);
	
	public int indexOf(byte e);
	public int lastIndexOf(byte e);
	
	
	public boolean add(byte e);
	public void addAt(int index, byte e);
	
	public boolean add(byte[] e, int start, int end);
	
	public byte setAt(int index, byte e);
	
	public boolean remove(byte e);
	public int removeAt(int index);
	
	public void clear();
	
	
	public byte[] toArray();
	
	
	public String getString(Charset charset);
	
}
