/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.NoSuchElementException;
import java.util.PrimitiveIterator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@Deprecated(since= "4.10")
@NonNullByDefault
public interface IntIterator extends PrimitiveIterator.OfInt {
	
	
	@Override
	public default int nextInt() {
		return nextValue();
	}
	
	/**
	 * Returns the next value in the iteration.
	 * <p>
	 * Like {@link #next()}, but returns directly the primitiv value.
	 * 
	 * @return the next value in the iteration
	 * @throws NoSuchElementException if the iteration has no more elements
	 */
	@Deprecated(since= "4.10")
	public int nextValue();
	
	
}
