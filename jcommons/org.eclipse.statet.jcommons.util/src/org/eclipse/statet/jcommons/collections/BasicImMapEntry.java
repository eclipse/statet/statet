/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.Map;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicImMapEntry<@NonNull K, V> implements ImMapEntry<K, V> {
	
	
	private final K key;
	
	private final V value;
	
	
	public BasicImMapEntry(final K key, final V value) {
		this.key= key;
		this.value= value;
	}
	
	
	@Override
	public final K getKey() {
		return this.key;
	}
	
	@Override
	public final V getValue() {
		return this.value;
	}
	
	
	@Override
	public int hashCode() {
		return this.key.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final Map.Entry<?, ?> other
						&& this.key.equals(other.getKey())
						&& Objects.equals(this.value, other.getValue()) ));
	}
	
	
	@Override
	public String toString() {
		final V value= this.value;
		return String.format("%1$s= %2$s", this.key, ((value != null) ? value : "<null>")); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
