/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.stream.LongStream;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface LongList {
	
	
	public int size();
	
	public boolean isEmpty();
	
	
	public int indexOf(long e);
	public int lastIndexOf(long e);
	
	public boolean contains(long e);
	
	public long getAt(int index);
	public long getFirst();
	public long getLast();
	
	
	public boolean add(long e);
	public void addAt(int index, long e);
	
	public long setAt(int index, long e);
	
	public boolean remove(long e);
	public long removeAt(int index);
	
	public void clear();
	
	
	public LongStream stream();
	
	
	public long[] toArray();
	public void toArray(long[] array);
	
}
