/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;

import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Immutable collection.
 * 
 * <p>The collection is unmodifiable by clients and, if not otherwise documented, the client can 
 * assume that the elements of the set do not change.
 * </p>
 */
@NonNullByDefault
public interface ImCollection<E> extends Collection<E>, Immutable {
	
	
	interface MappingResult<R> extends ImCollection<R> {
		
		
		ImList<R> toList();
		
		ImIdentityList<R> toIdentityList();
		
	}
	
	
	<R> MappingResult<R> map(Function<E, R> mapper);
	
	@Nullable E findFirst(Predicate<E> predicate);
	
	boolean anyMatch(Predicate<? super E> predicate);
	boolean allMatch(Predicate<? super E> predicate);
	boolean noneMatch(Predicate<? super E> predicate);
	
}
