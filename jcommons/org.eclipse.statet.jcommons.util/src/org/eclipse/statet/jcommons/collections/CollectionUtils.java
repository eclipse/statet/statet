/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import java.util.function.Function;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class CollectionUtils {
	
	
	public static final <K, V> void putAll(final Map<K, V> map, final ImList<K> keys, final V value) {
		for (final var key : keys) {
			map.put(key, value);
		}
	}
	
	public static final <E> void addNonNull(final Collection<E> c, final @Nullable E e) {
		if (e != null) {
			c.add(e);
		}
	}
	
	
	public static String toString(final List<@NonNull ?> list, final String sep) {
		final int n= list.size();
		if (n <= 0) {
			return ""; //$NON-NLS-1$
		}
		else if (n == 1) {
			return list.get(0).toString();
		}
		else if (list instanceof RandomAccess) {
			final StringBuilder sb= new StringBuilder(list.get(0).toString());
			for (int i= 1; i < n; i++) {
				sb.append(sep);
				sb.append(list.get(i).toString());
			}
			return sb.toString();
		}
		else {
			final Iterator<@NonNull ?> iter= list.iterator();
			final StringBuilder sb= new StringBuilder(iter.next().toString());
			for (int i= 1; i < n; i++) {
				sb.append(sep);
				sb.append(iter.next().toString());
			}
			return sb.toString();
		}
	}
	
	public static String toString(final Collection<@NonNull ?> c, final String sep) {
		if (c instanceof List) {
			return toString((List<@NonNull ?>)c, sep);
		}
		final int n= c.size();
		if (n <= 0) {
			return ""; //$NON-NLS-1$
		}
		else if (n == 1) {
			return c.iterator().next().toString();
		}
		else {
			final Iterator<@NonNull ?> iter= c.iterator();
			final StringBuilder sb= new StringBuilder(iter.next().toString());
			for (int i= 1; i < n; i++) {
				sb.append(sep);
				sb.append(iter.next().toString());
			}
			return sb.toString();
		}
	}
	
	public static <E> String toString(final ImList<@NonNull E> list, final Function<E, String> formatter,
			final char sep) {
		final int n= list.size();
		return switch (n) {
		case 0 ->
				""; //$NON-NLS-1$
		case 1 ->
				formatter.apply(list.getFirst());
		default -> {
				final StringBuilder sb= new StringBuilder(formatter.apply(list.getFirst()));
				for (int i= 1; i < n; i++) {
					sb.append(sep);
					sb.append(formatter.apply(list.get(i)));
				}
				yield sb.toString();
			}
		};
	}
	
	public static <E> String toString(final ImList<@NonNull E> list, final Function<E, String> formatter,
			final String sep) {
		final int n= list.size();
		return switch (n) {
		case 0 ->
				""; //$NON-NLS-1$
		case 1 ->
				formatter.apply(list.getFirst());
		default -> {
				final StringBuilder sb= new StringBuilder(formatter.apply(list.getFirst()));
				for (int i= 1; i < n; i++) {
					sb.append(sep);
					sb.append(formatter.apply(list.get(i)));
				}
				yield sb.toString();
			}
		};
	}
	
	
	@SuppressWarnings("null")
	public static ImIdentityList<String> toIdentifierList(final String[] array) {
		int count= 0;
		for (int i= 0; i < array.length; i++) {
			final String s= array[i];
			if (s != null && !s.isEmpty()) {
				array[count++]= s.intern();
			}
		}
		return ImCollections.newIdentityList(array, 0, count);
	}
	
	@SuppressWarnings("null")
	public static ImIdentitySet<String> toIdentifierSet(final String[] array) {
		int count= 0;
		for (int i= 0; i < array.length; i++) {
			final String s= array[i];
			if (s != null && !s.isEmpty()) {
				array[count++]= s.intern();
			}
		}
		return ImCollections.newIdentitySet(array, 0, count);
	}
	
	
	public static void clear(final @Nullable Collection<?> c) {
		if (c != null) {
			c.clear();
		}
	}
	
}
