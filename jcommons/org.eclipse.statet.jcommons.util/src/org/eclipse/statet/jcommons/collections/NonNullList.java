/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public interface NonNullList<E extends @NonNull Object> extends List<E> {
	
	
	@Override
	boolean contains(@NonNull Object e);
	
	@Override
	int indexOf(@NonNull Object e);
	@Override
	int lastIndexOf(@NonNull Object e);
	
	@Override
	@NonNull E get(int index);
	
	@Override
	boolean add(@NonNull E e);
	@Override
	void add(int index, @NonNull E e);
	
	@Override
	@NonNull E set(int index, @NonNull E e);
	
	@Override
	boolean remove(@NonNull Object e);
	@Override
	@NonNull E remove(int index);
	
	@Override
	@NonNull Object [] toArray();
	
}
