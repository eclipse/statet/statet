/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.stream.IntStream;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface IntList {
	
	
	public int size();
	
	public boolean isEmpty();
	
	public boolean contains(int e);
	
	public int getAt(int index);
	public int getFirst();
	public int getLast();
	
	public int indexOf(int e);
	public int lastIndexOf(int e);
	
	
	public boolean add(int e);
	public void addAt(int index, int e);
	
	public int setAt(int index, int e);
	
	public boolean remove(int e);
	public int removeAt(int index);
	
	public void clear();
	
	
	public IntStream stream();
	
	
	public int[] toArray();
	public void toArray(int[] array);
	
	
	public String getString();
	
}
