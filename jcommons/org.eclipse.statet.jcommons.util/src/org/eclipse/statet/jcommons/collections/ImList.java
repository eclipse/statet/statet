/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Immutable list.
 * 
 * <p>The list is unmodifiable by clients and, if not otherwise documented, the client can assume
 * that the elements of the list do not change.
 * </p>
 */
@NonNullByDefault
public interface ImList<E> extends List<E>, ImSequencedCollection<E> {
	
	
	@Override
	public E getFirst();
	
	@Override
	public E getLast();
	
	
	@Override
	public ImList<E> subList(int fromIndex, int toIndex);
	
	@Override
	public ImList<E> reversed();
	
}
