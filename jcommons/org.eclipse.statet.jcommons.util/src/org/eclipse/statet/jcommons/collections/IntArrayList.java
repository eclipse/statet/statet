/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.eclipse.statet.internal.jcommons.collections.ArrayUtils;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class IntArrayList implements IntList {
	
	
	private static final int[] EMPTY_ARRAY= new int[0];
	
	
	private int[] array;
	
	private int size;
	
	
	public IntArrayList() {
		this.array= EMPTY_ARRAY;
	}
	
	public IntArrayList(final int initialCapacity) {
		this.array= new int[initialCapacity];
	}
	
	public IntArrayList(final int[] initialElements) {
		final int size= initialElements.length;
		if (size != 0) {
			this.array= Arrays.copyOf(initialElements, size);
			this.size= size;
		}
		else {
			this.array= EMPTY_ARRAY;
		}
	}
	
	
	@Override
	public int size() {
		return this.size;
	}
	
	@Override
	public boolean isEmpty() {
		return (this.size == 0);
	}
	
	
	@Override
	public int indexOf(final int e) {
		final int[] array= this.array;
		final int size= this.size;
		for (int i= 0; i < size; i++) {
			if (array[i] == e) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public int lastIndexOf(final int e) {
		final int[] array= this.array;
		for (int i= this.size - 1; i >= 0; i--) {
			if (array[i] == e) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public boolean contains(final int e) {
		return (indexOf(e) >= 0);
	}
	
	
	@Override
	public int getAt(final int index) {
		if (index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		return this.array[index];
	}
	
	@Override
	public int getFirst() {
		if (this.size == 0) {
			throw new NoSuchElementException();
		}
		return this.array[0];
	}
	
	@Override
	public int getLast() {
		final int size= this.size;
		if (size == 0) {
			throw new NoSuchElementException();
		}
		return this.array[size - 1];
	}
	
	
	protected final void ensureCapacity(int min) {
		if (min > this.array.length) {
			if (this.array == EMPTY_ARRAY) {
				min= 8;
			}
			int newCapacity= Math.max(this.array.length + (this.array.length >> 1), min);
			if (newCapacity < 0) {
				newCapacity= Integer.MAX_VALUE;
			}
			final int[] newArray= new int[newCapacity];
			System.arraycopy(this.array, 0, newArray, 0, this.size);
			this.array= newArray;
		}
	}
	
	@Override
	public boolean add(final int e) {
		ensureCapacity(this.size + 1);
		this.array[this.size++]= e;
		return true;
	}
	
	@Override
	public void addAt(final int index, final int e) {
		if (index < 0 || index > this.size) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		ensureCapacity(this.size + 1);
		if (index < this.size) {
			System.arraycopy(this.array, index, this.array, index + 1, this.size - index);
		}
		this.array[index]= e;
		this.size++;
	}
	
	@Override
	public int setAt(final int index, final int e) {
		if (index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		final int oldElement= this.array[index];
		this.array[index]= e;
		return oldElement;
	}
	
	private void doRemoveElementBefore(final int index) {
		if (index != this.size) {
			System.arraycopy(this.array, index, this.array, index - 1, this.size - index);
		}
	}
	
	@Override
	public boolean remove(final int e) {
		final int index= indexOf(e);
		if (index >= 0) {
			doRemoveElementBefore(index + 1);
			this.size--;
			return true;
		}
		return false;
	}
	
	@Override
	public int removeAt(final int index) {
		if (index < 0 || index >= this.size) {
			throw new IndexOutOfBoundsException("index= " + index); //$NON-NLS-1$
		}
		final int oldElement= this.array[index];
		doRemoveElementBefore(index + 1);
		this.size--;
		return oldElement;
	}
	
	@Override
	public void clear() {
		this.size= 0;
	}
	
	
	@Override
	public IntStream stream() {
		return StreamSupport.intStream(Spliterators.spliterator(this.array,
				Spliterator.ORDERED ), false );
	}
	
	
	@Override
	public int[] toArray() {
		return Arrays.copyOf(this.array, this.size);
	}
	
	@Override
	public void toArray(final int[] array) {
		System.arraycopy(this.array, 0, array, 0, this.size);
	}
	
	
	@Override
	public String getString() {
		return new String(this.array, 0, this.size);
	}
	
	
	@Override
	public String toString() {
		return ArrayUtils.toString(this.array, 0, this.size);
	}
	
}
