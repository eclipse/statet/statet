/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.collections;

import java.util.ArrayList;

import org.eclipse.statet.jcommons.lang.Builder;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ImListBuilder<E> extends ArrayList<E> implements Builder<ImList<E>> {
	
	private static final long serialVersionUID= 1L;
	
	
	/**
	 * Constructs an empty list with the specified initial capacity.
	 *
	 * @param  initialCapacity  the initial capacity of the list
	 */
	public ImListBuilder(final int initialCapacity) {
		super(initialCapacity);
	}
	
	public ImListBuilder() {
		super();
	}
	
	
	@Override
	public ImList<E> build() {
		return ImCollections.toList(this);
	}
	
}
