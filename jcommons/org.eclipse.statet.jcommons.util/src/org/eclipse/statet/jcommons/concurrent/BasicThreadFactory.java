/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.concurrent;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicThreadFactory implements ThreadFactory {
	
	
	private static String buildNamePrefix(final @Nullable String s1, final @Nullable String s2) {
		final StringBuilder sb= new StringBuilder();
		if (s1 != null) {
			sb.append(s1);
			sb.append(' ');
		}
		if (s2 != null) {
			sb.append(s2);
			sb.append(' ');
		}
		sb.append("[Thread-"); //$NON-NLS-1$
		return sb.toString();
	}
	
	private static final String NAME_SUFFIX= "]"; //$NON-NLS-1$
	
	@SuppressWarnings("null")
	private static ThreadGroup getCurrentThreadGroup() {
		return Thread.currentThread().getThreadGroup();
	}
	
	
	private final ThreadGroup threadGroup;
	
	private final String threadNamePrefix;
	private final String threadNameSuffix;
	
	private final AtomicInteger threadNumber= new AtomicInteger(1);
	
	
	public BasicThreadFactory(final ThreadGroup threadGroup,
			final String threadNamePrefix, final String threadNameSuffix) {
		this.threadGroup= nonNullAssert(threadGroup);
		this.threadNamePrefix= nonNullAssert(threadNamePrefix);
		this.threadNameSuffix= nonNullAssert(threadNameSuffix);
	}
	
	public BasicThreadFactory(final String threadBaseName) {
		this(getCurrentThreadGroup(),
				buildNamePrefix(threadBaseName, null), NAME_SUFFIX );
	}
	
	public BasicThreadFactory(final ThreadGroup threadGroup,
			final @Nullable String threadSubGroupName) {
		this(threadGroup,
				buildNamePrefix(threadGroup.getName(), threadSubGroupName), NAME_SUFFIX );
	}
	
	public BasicThreadFactory(final ThreadGroup threadGroup) {
		this(threadGroup, null);
	}
	
	
	protected boolean getDaemon() {
		return false;
	}
	
	protected int getPriority() {
		return Thread.NORM_PRIORITY;
	}
	
	@Override
	public Thread newThread(final Runnable r) {
		final Thread thread= new Thread(this.threadGroup, r,
				this.threadNamePrefix + this.threadNumber.getAndIncrement() +  this.threadNameSuffix);
		
		thread.setDaemon(getDaemon());
		thread.setPriority(getPriority());
		return thread;
	}
	
}
