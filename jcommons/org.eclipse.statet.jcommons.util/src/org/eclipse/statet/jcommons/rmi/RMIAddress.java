/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.rmi;

import static org.eclipse.statet.jcommons.net.CommonsNet.LOCAL_LOOPBACK_STRING;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.CommonsNet;
import org.eclipse.statet.jcommons.net.Port;


/**
 * Address for RMI naming
 */
@NonNullByDefault
public class RMIAddress {
	
	
	public static class Sec {
		
		private final String scheme;
		
		public Sec(final String scheme) {
			this.scheme= scheme;
		}
		
		
		@Override
		public String toString() {
			return this.scheme;
		}
		
	}
	
	
	public static final Sec SSH= new Sec("ssh"); //$NON-NLS-1$
	public static final Sec SSL= new Sec("ssl"); //$NON-NLS-1$
	
	
	private static final int DEFAULT_PORT_NUM= Registry.REGISTRY_PORT;
	public static final Port DEFAULT_PORT= new Port(DEFAULT_PORT_NUM);
	
	public static final String REGISTRY_NAME= ""; //$NON-NLS-1$
	
	
	private static final byte RESOLVE= 1 << 0;
	
	/**
	 * Flag to parse explicitly a registry address.
	 * 
	 * <ul>
	 *   <li>if present, the URL path / RMI service name is discarded</li>
	 *   <li>a missing path / path separator is not an error</li>
	 * </ul>
	 * 
	 * @see #parse(String, byte)
	 * @see #parseUnresolved(String, byte)
	 */
	public static final byte PARSE_REGISTRY= 1 << 1;
	
	
	public static void validate(final String address) throws MalformedURLException {
		try {
			new RMIAddress(address, 0);
		}
		catch (final UnknownHostException e) {
		}
	}
	
	/**
	 * Creates a RMI address from string.
	 * 
	 * @param address the address string
	 * @return the RMI registry address
	 * @throws MalformedURLException
	 * @throws UnknownHostException
	 */
	public static RMIAddress parse(final String address, final byte flags)
			throws MalformedURLException, UnknownHostException {
		return new RMIAddress(address, flags | RESOLVE);
	}
	
	/**
	 * Creates a RMI address from string.
	 * 
	 * @param address the address string
	 * @return the RMI registry address
	 * @throws MalformedURLException
	 * @throws UnknownHostException
	 */
	public static RMIAddress parse(final String address)
			throws MalformedURLException, UnknownHostException {
		return parse(address, (byte)0);
	}
	
	public static RMIAddress parseUnresolved(final String address, final byte flags)
			throws MalformedURLException {
		try {
			return new RMIAddress(address, flags);
		}
		catch (final UnknownHostException e) {
			throw new IllegalStateException(e);
		}
	}
	
	public static RMIAddress parseUnresolved(final String address)
			throws MalformedURLException {
		return parseUnresolved(address, (byte)0);
	}
	
	
	private static @Nullable String getHostString(final @Nullable InetAddress hostInetAddress) {
		if (hostInetAddress == null) {
			return null;
		}
		final String s= hostInetAddress.getHostAddress();
		if (hostInetAddress instanceof Inet6Address && s.charAt(0) != '[') {
			return '[' + s + ']';
		}
		return s;
	}
	
	private static @Nullable String checkHostString(final @Nullable String host) {
		if (host != null) {
			if (host.isEmpty()) {
				return null;
			}
			if (host.charAt(0) != '[' && CommonsNet.isIpV6Address(host)) {
				return '[' + host + ']';
			}
		}
		return host;
	}
	
	private static InetAddress resolveHostInetAddress(final @Nullable String host) throws UnknownHostException {
		return (host == null || CommonsNet.isCommonLoopback(host)) ?
				CommonsNet.getLocalLoopbackInetAddress() :
				InetAddress.getByName(host);
	}
	
	private static Port parsePortUrl(final String port) throws MalformedURLException {
		try {
			return Port.valueOf(port);
		}
		catch (final IllegalArgumentException e) {
			throw new MalformedURLException("Invalid port: " + e.getLocalizedMessage());
		}
	}
	
	private static Port checkPort(final Port port) {
		return (port.get() == DEFAULT_PORT_NUM) ? DEFAULT_PORT : port;
	}
	
	private static boolean isNameChar(final char c) {
		return (Character.isLetterOrDigit(c) || c == '-' || c == '_' || c == '.' || c == '/');
	}
	
	private static String checkNameUrl(final String s) throws MalformedURLException {
		for (int i= 0; i < s.length(); i++) {
			final char c= s.charAt(i);
			if (!isNameChar(c)) {
				throw new MalformedURLException("Invalid path component (RMI service name): character '" + c +"' is not allowed.");
			}
		}
		return s;
	}
	
	private static String checkNameArg(final String s) throws IllegalArgumentException {
		for (int i= 0; i < s.length(); i++) {
			final char c= s.charAt(i);
			if (!isNameChar(c)) {
				throw new IllegalArgumentException("Invalid name: character '" + c + "' is not allowed.");
			}
		}
		return s;
	}
	
	
	private final @Nullable String host;
	private @Nullable InetAddress hostInetAddress;
	private final Port port;
	private final @Nullable Sec sec;
	private final String path;
	
	private @Nullable String address;
	private volatile @Nullable String ser;
	
	
	@Deprecated
	public RMIAddress(final String address)
			throws UnknownHostException, MalformedURLException {
		this(address, RESOLVE);
	}
	
	public RMIAddress(final @Nullable String host, final int portNum,
			final @Nullable String name) throws IllegalArgumentException, UnknownHostException {
		this(checkHostString(host),
				resolveHostInetAddress(host),
				(portNum < 0) ? DEFAULT_PORT : checkPort(new Port(portNum)),
				null,
				(name == null) ? REGISTRY_NAME : checkNameArg(name));
	}
	
	public RMIAddress(final @Nullable InetAddress address, final Port port, final @Nullable Sec sec,
			final String name) {
		this(getHostString(address),
				(address == null) ? CommonsNet.getLocalLoopbackInetAddress() : address,
				checkPort(port),
				sec,
				checkNameArg(name) );
	}
	
	@Deprecated
	public RMIAddress(final InetAddress address, final Port port, final boolean isSsl,
			final String name) {
		this(getHostString(address), address, checkPort(port), (isSsl) ? SSL : null,
				checkNameArg(name) );
	}
	
	public RMIAddress(final @Nullable InetAddress address, final Port port,
			final String name) {
		this(address, port, null, name);
	}
	
	public RMIAddress(final RMIAddress registry,
			final String name) {
		this(registry.host, registry.hostInetAddress, registry.port, registry.sec,
				checkNameArg(name) );
	}
	
	private RMIAddress(final String address, final int flags)
			throws UnknownHostException, MalformedURLException {
		final int l= address.length();
		int idx;
		if (address.startsWith("ssh:")) { //$NON-NLS-1$
			idx= 4;
			this.sec= SSH;
		}
		else if (address.startsWith("ssl:")) { //$NON-NLS-1$
			idx= 4;
			this.sec= SSL;
		}
		else {
			idx= 0;
			this.sec= null;
		}
		if (address.startsWith("rmi:", idx)) { //$NON-NLS-1$
			idx+= 4;
		}
		
		if (address.startsWith("//", idx)) { //$NON-NLS-1$
			idx+= 2;
		}
		{	int idxEndAuth= address.indexOf('/', idx);
			if (idxEndAuth == -1) {
				if ((flags & PARSE_REGISTRY) == 0) {
					throw new MalformedURLException("Missing path component (RMI service name)");
				}
				idxEndAuth= l;
			}
			final int idxPort;
			if (idx < idxEndAuth && address.charAt(idx) == '[') {
				final int idxEnd= indexOf(address, ']', idx + 1, idxEndAuth);
				if (idxEnd == -1 || !CommonsNet.isIpV6Address(
						this.host= address.substring(idx, idxEnd + 1) )) {
					throw new MalformedURLException("Invalid authority: invalid IPv6 address");
				}
				idx= idxEnd + 1;
				if (idx < idxEndAuth) {
					if (address.charAt(idx) != ':') {
						throw new MalformedURLException("Invalid authority");
					}
					idxPort= idx;
				}
				else {
					idxPort= -1;
				}
			}
			else {
				idxPort= indexOf(address, ':', idx, idxEndAuth);
				final int idxEnd= (idxPort != -1) ? idxPort : idxEndAuth;
				this.host= (idx < idxEnd) ? address.substring(idx, idxEnd) : null;
			}
			if (idxPort != -1) {
				this.port= checkPort(parsePortUrl(address.substring(idxPort + 1, idxEndAuth)));
			}
			else {
				this.port= DEFAULT_PORT;
			}
			idx= idxEndAuth + 1;
		}
		this.path= (idx >= l || (flags & PARSE_REGISTRY) != 0) ?
				REGISTRY_NAME : 
				checkNameUrl(address.substring(idx, l));
		
		if ((flags & RESOLVE) != 0) {
			this.hostInetAddress= resolveHostInetAddress(this.host);
		}
	}
	
	private RMIAddress(final @Nullable String host, final @Nullable InetAddress hostInetAddress,
			final Port port,
			final @Nullable Sec sec, final String path) {
		this.host= host;
		this.hostInetAddress= hostInetAddress;
		this.port= port;
		this.sec= sec;
		this.path= path;
	}
	
	
	public boolean isResolved() {
		return (this.hostInetAddress != null);
	}
	
	/**
	 * Returns a RMI address with resolved properties.
	 * <ul>
	 *   <li>{@link #getHostInetAddress()}</li>
	 * </ul>
	 * 
	 * @return the RMI address with resolved values.
	 * @throws UnknownHostException
	 */
	public RMIAddress resolve() throws UnknownHostException {
		if (this.hostInetAddress != null) {
			return this;
		}
		return new RMIAddress(this.host, resolveHostInetAddress(this.host), this.port,
				this.sec, this.path );
	}
	
	
	/**
	 * @return the host as specified when creating the address.
	 */
	public String getHost() {
		final String host= this.host;
		return (host == null) ? LOCAL_LOOPBACK_STRING : host;
	}
	
	public InetAddress getHostInetAddress() {
		final var hostInetAddress= this.hostInetAddress;
		if (hostInetAddress == null) {
			throw new UnsupportedOperationException("address is unresolved");
		}
		return hostInetAddress;
	}
	
	public boolean isLocalHost() {
		final String host= this.host;
		if (host == null) {
			return true;
		}
		final var hostInetAddress= this.hostInetAddress;
		if (hostInetAddress != null && hostInetAddress.isLoopbackAddress()) {
			return true;
		}
		return CommonsNet.isLocalLocalhost(host);
	}
	
	public Port getPort() {
		return this.port;
	}
	
	public int getPortNum() {
		return this.port.get();
	}
	
	public @Nullable Sec getSec() {
		return this.sec;
	}
	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return this.path;
	}
	
	/**
	 * Standard string presentation to use for rmi
	 * @return
	 */
	public String getAddress() {
		String address= this.address;
		if (address == null) {
			final StringBuilder sb= new StringBuilder(32);
			sb.append("rmi://"); //$NON-NLS-1$
			if (this.host != null) {
				sb.append(this.host);
			}
			if (this.port != DEFAULT_PORT) {
				sb.append(':');
				sb.append(this.port);
			}
			sb.append('/');
			sb.append(this.path);
			address= sb.toString();
			this.address= address;
		}
		return address;
	}
	
	public RMIAddress getRegistryAddress() {
		return new RMIAddress(this.host, this.hostInetAddress, this.port, this.sec, REGISTRY_NAME);
	}
	
	public Remote lookup() throws RemoteException, NotBoundException, AccessException {
		final RMIRegistry rmiRegistry= new RMIRegistry(getRegistryAddress(), false);
		return rmiRegistry.getRegistry().lookup(getName());
	}
	
	/**
	 * 
	 * @return if SSL is enabled
	 * 
	 * @see #getSec()
	 * @since 1.4
	 */
	public boolean isSsl() {
		return (this.sec == SSL);
	}
	
	
	@Override
	public String toString() {
		String ser= this.ser;
		if (ser == null) {
			ser= getAddress();
			final var sec= this.sec;
			if (sec != null) {
				ser= sec.scheme + ':' + ser;
			}
			this.ser= ser;
		}
		return ser;
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final RMIAddress other
						&& Objects.equals(this.host, other.host)
						&& this.port.equals(other.port)
						&& this.sec == other.sec
						&& this.path.equals(other.path)
						&& Objects.equals(this.hostInetAddress, other.hostInetAddress) ));
	}
	
	
	private static int indexOf(final String s, final char c, final int fromIndex, final int endIndex) {
		final int idx= s.indexOf(c, fromIndex);
		return (idx < endIndex) ? idx : -1;
	}
	
}
