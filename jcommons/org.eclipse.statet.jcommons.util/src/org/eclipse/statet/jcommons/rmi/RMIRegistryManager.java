/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.rmi;

import static org.eclipse.statet.internal.jcommons.rmi.CommonsRmiInternals.BUNDLE_ID;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.File;
import java.lang.ProcessBuilder.Redirect;
import java.net.BindException;
import java.net.InetAddress;
import java.net.URI;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import org.eclipse.statet.internal.jcommons.rmi.Messages;
import org.eclipse.statet.internal.jcommons.rmi.eplatform.EPlatformContributor;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.net.Port;
import org.eclipse.statet.jcommons.net.PortRange;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.runtime.ProcessUtils;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.status.InfoStatus;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * Utility managing local RMI registries:
 * <ul>
 *   <li>Embedded private RMI registry: A single registry
 *     intended to use for the current application only.</li>
 *   <li>Separate RMI registries: Started in a separate
 *     process at a specified port. Multiple registries and
 *     shutdown behaviour possible.</li>
 * </ul>
 * 
 * Note: stop modes are not applied if the registry is already started. 
 */
@NonNullByDefault
public class RMIRegistryManager {
	
	
	public static enum StopRule {
		
		/**
		 * Mode to stop the registry always automatically.
		 */
		ALWAYS,
		
		/**
		 * Mode to stop the registry never automatically.
		 */
		NEVER,
		
		/**
		 * Mode to stop the registry if empty.
		 */
		IF_EMPTY,
		
	}
	
	
	private static final class ManagedRegistry {
		
		public static final int SEPERATE=         1 << 0;
		public static final int EMBEDDED=         1 << 1;
		public static final int FOREIGN=          1 << 2;
		
		
		private final RMIRegistry registry;
		private final int type;
		
		private final StopRule stopRule;
		private @Nullable Process process;
		
		
		public ManagedRegistry(final RMIRegistry registry, final int type,
				final StopRule stopRule) {
			this.registry= registry;
			this.type= type;
			this.stopRule= stopRule;
		}
		
		
		@Override
		public String toString() {
			final ToStringBuilder sb= new ToStringBuilder(ManagedRegistry.class);
			sb.append(' '); sb.append(this.registry.getAddress().toString());
			sb.addProp("type", this.type);
			sb.addProp("stopRule", this.stopRule);
			return sb.toString();
		}
		
	}
	
	
	private static final String EMBEDDED_PORT_RANGE_KEY= "org.eclipse.statet.jcommons.rmi.registry.TcpPort"; //$NON-NLS-1$
	
	private static final PortRange EMBEDDED_PORT_RANGE_DEFAULT= new PortRange(51100, 51199);
	
	public static final RMIRegistryManager INSTANCE= new RMIRegistryManager();
	
	
	private final Map<Port, ManagedRegistry> registries= new HashMap<>();
	
	private final Object embeddedLock= new Object();
	private PortRange embeddedPortRange;
	private boolean embeddedStartSeparate= true;
	private boolean embeddedSsl= false;
	private @Nullable ManagedRegistry embeddedRegistry;
	private final List<ManagedRegistry> embeddedRegistries= new ArrayList<>(4);
	private @Nullable List<URI> embeddedCodebaseEntries;
	private boolean embeddedCodebaseLoadContrib;
	
	
	private RMIRegistryManager() {
		initDispose();
		
		this.embeddedPortRange= EMBEDDED_PORT_RANGE_DEFAULT;
		final String s= System.getProperty(EMBEDDED_PORT_RANGE_KEY);
		if (s != null && s.length() > 0) {
			try {
				this.embeddedPortRange= PortRange.valueOf(s);
			}
			catch (final IllegalArgumentException e) {
				CommonsRuntime.log(new ErrorStatus(BUNDLE_ID,
						"The value of the Java property '" + EMBEDDED_PORT_RANGE_KEY + "' is invalid.",
						e ));
			}
		}
		this.embeddedCodebaseLoadContrib= true;
	}
	
	protected void initDispose() {
		CommonsRuntime.getEnvironment().addStoppingListener(this::dispose);
	}
	
	
	/**
	 * Returns a handler for the RMI registry at the local host and the given port.
	 * 
	 * The registry must be started by this util instance.
	 * 
	 * @param port the registry port
	 * @return the registry handler or <code>null</code>
	 */
	public @Nullable RMIRegistry getRegistry(int port) {
		if (port <= 0) {
			port= Registry.REGISTRY_PORT;
		}
		
		final Port key= new Port(port);
		final ManagedRegistry r;
		synchronized (this) {
			r= this.registries.get(key);
		}
		return (r != null) ? r.registry : null;
	}
	
	/**
	 * Sets the port for the managed embedded private RMI registry.
	 * 
	 * @param port the registry port
	 */
	public void setEmbeddedPrivatePort(final int port) {
		setEmbeddedPrivatePortDynamic(port, port);
	}
	
	/**
	 * Sets the valid port range for the managed embedded private RMI registry.
	 * An unused port for the registry is search inside this range.
	 * 
	 * @param min lowest valid registry port
	 * @param max highest valid the registry port
	 */
	public void setEmbeddedPrivatePortDynamic(final int min, final int max) {
		if (min > 0 && min > max) {
			throw new IllegalArgumentException("min > max");
		}
		synchronized (this.embeddedLock) {
			this.embeddedRegistry= null;
			this.embeddedPortRange= (min > 0) ? new PortRange(min, max) : EMBEDDED_PORT_RANGE_DEFAULT;
		}
	}
	
	/**
	 * Sets the start mode for the managed embedded private RMI registry.
	 * 
	 * @param separate start registry in separate process
	 */
	public void setEmbeddedPrivateMode(final boolean separate) {
		this.setEmbeddedPrivateMode(separate, this.embeddedSsl);
	}
	
	/**
	 * Sets the start mode for the managed embedded private RMI registry.
	 * 
	 * @param separate start registry in separate process
	 * @param ssl start using SSL sockets
	 * 
	 * @since 1.4
	 */
	public void setEmbeddedPrivateMode(final boolean separate, final boolean ssl) {
		if (separate && ssl) {
			throw new IllegalArgumentException("ssl is only supported if separate is not enabled");
		}
		synchronized (this.embeddedLock) {
			this.embeddedRegistry= null;
			this.embeddedStartSeparate= separate;
			this.embeddedSsl= ssl;
		}
	}
	
	public void addEmbeddedCodebaseEntry(final URI entry) {
		if (entry.getScheme() == null) {
			throw new IllegalArgumentException("entry: missing scheme"); //$NON-NLS-1$
		}
		
		synchronized (this.embeddedLock) {
			List<URI> embeddedCodebaseEntries= this.embeddedCodebaseEntries;
			if (embeddedCodebaseEntries == null) {
				embeddedCodebaseEntries= new ArrayList<>();
				this.embeddedCodebaseEntries= embeddedCodebaseEntries;
			}
			if (!embeddedCodebaseEntries.contains(entry)) {
				this.embeddedRegistry= null;
				embeddedCodebaseEntries.add(entry);
			}
		}
	}
	
	
	/**
	 * Returns the managed embedded private RMI registry.
	 * 
	 * @return the registry
	 */
	public RMIRegistry getEmbeddedPrivateRegistry(final ProgressMonitor m)
			throws StatusException {
		ManagedRegistry r= null;
		Status status= null;
		synchronized (this.embeddedLock) {
			r= this.embeddedRegistry;
			if (r != null) {
				return r.registry;
			}
			for (final Iterator<ManagedRegistry> iter= this.embeddedRegistries.iterator(); iter.hasNext();) {
				r= iter.next();
				final RMIAddress address= r.registry.getAddress();
				if (this.embeddedPortRange.contains(address.getPort())
						&& address.isSsl() == this.embeddedSsl
						&& this.embeddedStartSeparate == (r.process != null) ) {
					this.embeddedRegistry= r;
					return r.registry;
				}
			}
			r= null;
			
			m.beginTask("Starting embedded service registry", ProgressMonitor.UNKNOWN);
			m.beginSubTask((this.embeddedStartSeparate) ?
					"Starting service registry (RMI) process." : "Starting service registry (RMI)." );
			
			final List<URI> embeddedCodebaseEntries= (this.embeddedStartSeparate) ?
					getEmbeddedCodebaseEntries() : null;
			
			int loop= 1;
			BindException bindException= null;
			for (int portNum= this.embeddedPortRange.getMin(); ; ) {
				m.checkCanceled();
				
				try {
					final RMIAddress rmiAddress= new RMIAddress(
							InetAddress.getLoopbackAddress(), new Port(portNum),
							(this.embeddedSsl) ? RMIAddress.SSL : null,
							RMIAddress.REGISTRY_NAME );
					if (this.embeddedStartSeparate) {
						status= startSeparateRegistry(rmiAddress, false,
								(this.embeddedPortRange.getLength() == 1),
								ManagedRegistry.EMBEDDED, StopRule.ALWAYS,
								embeddedCodebaseEntries );
						if (status.getSeverity() < Status.ERROR) {
							r= this.registries.get(rmiAddress.getPort());
						}
					}
					else {
						final Registry javaRegistry;
						if (rmiAddress.isSsl()) {
							javaRegistry= LocateRegistry.createRegistry(portNum,
									new SslRMIClientSocketFactory(),
									new SslRMIServerSocketFactory(null, null, true) );
						}
						else {
							javaRegistry= LocateRegistry.createRegistry(portNum);
						}
						r= new ManagedRegistry(
								new RMIRegistry(rmiAddress, javaRegistry, false),
								ManagedRegistry.EMBEDDED, StopRule.ALWAYS );
					}
					if (r != null) {
						this.embeddedRegistry= r;
						this.embeddedRegistries.add(r);
						break;
					}
				}
				catch (final Exception e) {
					if (e.getCause() instanceof BindException) {
						bindException= (BindException)e;
					}
					else {
						status= new ErrorStatus(BUNDLE_ID,
								"An exception was thrown when starting the embedded registry.",
								e );
					}
				}
				
				portNum++;
				if (portNum % 10 == 0) {
					portNum+= 10;
				}
				if (portNum > this.embeddedPortRange.getMax()) {
					if (loop == 1) {
						loop= 2;
						portNum= this.embeddedPortRange.getMin() + 10;
						if (portNum <= this.embeddedPortRange.getMax()) {
							continue;
						}
					}
					if (status == null) {
						status= new ErrorStatus(BUNDLE_ID,
								String.format("Failed to bind the embedded RMI registry to a TCP port in the specified range (%1$s).",
										this.embeddedPortRange ),
								bindException );
					}
					break;
				}
			}
		}
		if (r != null) {
			synchronized (this) {
				this.registries.put(r.registry.getAddress().getPort(), r);
			}
			return r.registry;
		}
		throw new StatusException(nonNullAssert(status));
	}
	
	private @Nullable List<URI> getEmbeddedCodebaseEntries() {
		List<URI> embeddedCodebaseEntries= this.embeddedCodebaseEntries;
		if (this.embeddedCodebaseLoadContrib) {
			if (embeddedCodebaseEntries == null) {
				embeddedCodebaseEntries= new ArrayList<>();
				this.embeddedCodebaseEntries= embeddedCodebaseEntries;
			}
			try {
				this.embeddedCodebaseLoadContrib= false;
				new EPlatformContributor().addCodebaseEntries(embeddedCodebaseEntries);
			}
			catch (final Exception e) {}
		}
		return embeddedCodebaseEntries;
	}
	
	
	public Status startSeparateRegistry(final RMIAddress address, final boolean allowExisting,
			final @Nullable StopRule stopRule, final @Nullable List<URI> codebaseEntries) {
		return startSeparateRegistry(address, allowExisting, false, 0, stopRule, codebaseEntries);
	}
	
	private Status startSeparateRegistry(RMIAddress address,
			final boolean allowExisting, final boolean noCheck,
			final int addType, final @Nullable StopRule stopRule,
			final @Nullable List<URI> codebaseEntries) {
		if (allowExisting && codebaseEntries != null) {
			throw new IllegalArgumentException("allow existing not valid in combination with codebase entries");
		}
		if (!address.isResolved()) {
			throw new IllegalArgumentException("address: not resolved");
		}
		final InetAddress hostInetAddress= address.getHostInetAddress();
		if (!(hostInetAddress.isLinkLocalAddress() || hostInetAddress.isLoopbackAddress())) {
			throw new IllegalArgumentException("address: not local"); //$NON-NLS-1$
		}
		if (address.getName() != null) {
			address= new RMIAddress(address, RMIAddress.REGISTRY_NAME);
		}
		if (!noCheck) {
			final Status status= checkRegistryAddress(address, allowExisting);
			if (status != null) {
				return status;
			}
		}
		
		final Process process;
		final ProcessBuilder processBuilder= new ProcessBuilder();
		try {
			final StringBuilder sb= new StringBuilder();
			sb.setLength(0);
			sb.append(System.getProperty("java.home")); //$NON-NLS-1$
			sb.append(File.separator).append("bin"); //$NON-NLS-1$
			sb.append(File.separator).append("rmiregistry"); //$NON-NLS-1$
			processBuilder.command().add(sb.toString());
			processBuilder.command().add(address.getPort().toString());
			if (codebaseEntries != null && !codebaseEntries.isEmpty()) {
				sb.setLength(0);
				sb.append("-J-Djava.rmi.server.codebase="); //$NON-NLS-1$
				sb.append(codebaseEntries.get(0));
				for (int i= 1; i < codebaseEntries.size(); i++) {
					sb.append(' ');
					sb.append(codebaseEntries.get(i));
				}
				processBuilder.command().add(sb.toString());
			}
			if (address.getHost() != null) {
				sb.setLength(0);
				sb.append("-J-Djava.rmi.server.hostname="); //$NON-NLS-1$
				sb.append(address.getHost());
				processBuilder.command().add(sb.toString());
			}
			processBuilder.redirectOutput(Redirect.DISCARD);
			processBuilder.redirectErrorStream(true);
			process= processBuilder.start();
		}
		catch (final Exception e) {
			return new ErrorStatus(BUNDLE_ID,
					MessageFormat.format(Messages.RMI_status_RegistryStartFailed_message,
							address.getPort() ),
					e );
		}
		
		RemoteException lastException= null;
		for (int i= 1; ; i++) {
			try {
				final int exit= process.exitValue();
				return new ErrorStatus(BUNDLE_ID,
						MessageFormat.format(Messages.RMI_status_RegistryStartFailedWithExitValue_message,
										address.getPort(), exit )
								+ "\n\tcommandLine= " + ProcessUtils.generateCommandLine(processBuilder.command()) );
			}
			catch (final IllegalThreadStateException e) {
			}
			if (i > 1) {
				try {
					RMIUtils.checkRegistryAccess(address);
					final Registry registry= LocateRegistry.getRegistry(address.getHost(), address.getPort().get());
					registry.list();
					final ManagedRegistry r= new ManagedRegistry(
							new RMIRegistry(address, registry, true),
							ManagedRegistry.SEPERATE | addType,
							(stopRule != null) ? stopRule : StopRule.IF_EMPTY );
					r.process= process;
					synchronized (this) {
						this.registries.put(address.getPort(), r);
					}
					return Status.OK_STATUS;
				}
				catch (final RemoteException e) {
					lastException= e;
				}
			}
			
			if (Thread.interrupted()) {
				process.destroy();
				return Status.CANCEL_STATUS;
			}
			if (i >= 25) {
				process.destroy();
				return new ErrorStatus(BUNDLE_ID,
						MessageFormat.format(Messages.RMI_status_RegistryStartFailed_message,
										address.getPort() )
								+ "\n\tcommandLine= " + ProcessUtils.generateCommandLine(processBuilder.command()),
						lastException );
			}
			try {
				Thread.sleep(50);
				continue;
			}
			catch (final InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
	
	private @Nullable Status checkRegistryAddress(final RMIAddress address, final boolean allowExisting) {
		try {
			RMIUtils.checkRegistryAccess(address);
		}
		catch (final RemoteException e) {
			return null;
		}
		if (allowExisting) {
			try {
				final Registry registry= LocateRegistry.getRegistry(address.getHost(), address.getPortNum());
				registry.list();
				synchronized (this) {
					if (!this.registries.containsKey(address.getPort())) {
						final ManagedRegistry r= new ManagedRegistry(
								new RMIRegistry(address, registry, false),
								ManagedRegistry.FOREIGN,
								StopRule.NEVER );
						this.registries.put(address.getPort(), r);
					}
				}
				return new InfoStatus(BUNDLE_ID,
						MessageFormat.format(Messages.RMI_status_RegistryAlreadyStarted_message,
								address.getPort() ));
			}
			catch (final RemoteException e) {}
		}
		return new ErrorStatus(BUNDLE_ID,
				MessageFormat.format(Messages.RMI_status_RegistryStartFailedPortAlreadyUsed_message,
						address.getPort() ));
	}
	
	public Status stopSeparateRegistry(final RMIAddress address) {
		return stopSeparateRegistry(address.getPort().get());
	}
	
	public Status stopSeparateRegistry(int port) {
		if (port <= 0) {
			port= Registry.REGISTRY_PORT;
		}
		
		final Port key= new Port(port);
		final ManagedRegistry r;
		final Process process;
		synchronized (this) {
			r= this.registries.get(key);
			if (r == null || (process= r.process) == null) {
				return new ErrorStatus(BUNDLE_ID,
						MessageFormat.format(Messages.RMI_status_RegistryStopFailedNotFound_message,
								port ));
			}
			this.registries.remove(key);
		}
		
		process.destroy();
		return Status.OK_STATUS;
	}
	
	
	protected void dispose() {
		synchronized(this) {
			for (final ManagedRegistry r : this.registries.values()) {
				final Process process= r.process;
				if (process == null) {
					continue;
				}
				switch (r.stopRule) {
				case ALWAYS:
					break;
				case NEVER:
					continue;
				case IF_EMPTY:
					try {
						final Registry registry= r.registry.getRegistry();
						if (registry.list().length > 0) {
							continue;
						}
					}
					catch (final RemoteException e) {}
					break;
				}
				process.destroy();
				r.process= null;
			}
			this.registries.clear();
		}
	}
	
}
