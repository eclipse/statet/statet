/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.rmi;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.rmi.RemoteException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RMIUtils {
	
	
	private static final int ACCESS_TIMEOUT;
	
	static {
		int timeout= 15000;
		final String s= System.getProperty("org.eclipse.statet.jcommons.rmi.AccessTimeout");
		if (s != null) {
			try {
				timeout= Integer.parseInt(s);
			} catch (final Exception e) {}
		}
		ACCESS_TIMEOUT= timeout;
	}
	
	
	public static void checkRegistryAccess(final RMIAddress rmiAddress) throws RemoteException {
		final var socketAddress= new InetSocketAddress(rmiAddress.getHostInetAddress(), rmiAddress.getPortNum());
		try (final Socket socket= new Socket()) {
			try {
				socket.setSoTimeout(ACCESS_TIMEOUT);
			}
			catch (final SocketException e) {}
			socket.connect(socketAddress, ACCESS_TIMEOUT);
		}
		catch (final IOException e) {
			throw new RemoteException("Cannot connect to RMI registry.", e);
		}
	}
	
	
}
