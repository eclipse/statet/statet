/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.rmi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.rmi.ssl.SslRMIClientSocketFactory;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils;


@NonNullByDefault
public class RMIRegistry {
	
	
	private final RMIAddress address;
	
	private final Registry registry;
	
	
	public RMIRegistry(final RMIAddress address, final Registry registry,
			final boolean validate) throws RemoteException {
		if (address.getName().length() > 0) {
			throw new IllegalArgumentException();
		}
		this.address= address;
		this.registry= ObjectUtils.nonNullAssert(registry);
		
		if (validate) {
			this.registry.list();
		}
	}
	
	public RMIRegistry(final RMIAddress address, final boolean validate) throws RemoteException {
		if (address.getName().length() > 0) {
			throw new IllegalArgumentException();
		}
		this.address= address;
		this.registry= LocateRegistry.getRegistry(address.getHost(), address.getPort().get(),
				(address.isSsl()) ? new SslRMIClientSocketFactory() : null );
		
		if (validate) {
			this.registry.list();
		}
	}
	
	
	public RMIAddress getAddress() {
		return this.address;
	}
	
	public Registry getRegistry() {
		return this.registry;
	}
	
}
