/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.util;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class Units {
	
	
	/** Factor of deci: 1 deci = 10<sup>1</sup> = 10 */
	public static final int DECI_FACTOR= 10;
	/** Factor of centi: 1 centi = 10<sup>2</sup> = 100 */
	public static final int CENTI_FACTOR= 100;
	/** Factor of milli: 1 milli = 10<sup>3</sup> = 1 000 */
	public static final int MILLI_FACTOR= 1_000;
	/** Factor of micro: 1 micro = 10<sup>6</sup> = 1 000 000 */
	public static final int MIRCO_FACTOR= 1_000_000;
	/** Factor of nano: 1 nano = 10<sup>9</sup> = 1 000 000 000 */
	public static final int NANO_FACTOR= 1_000_000_000;
	
	/** 1 milli in nano: 1 000 000 */
	public static final int MILLI_NANO= NANO_FACTOR / MILLI_FACTOR;
	
	/** 1 minute in second: 60 */
	public static final int MINUTE_SECOND= 60;
	/** 1 hour in minute: 60 */
	public static final int HOUR_MINUTE= 60;
	/** 1 hour in second: 3600 */
	public static final int HOUR_SECOND= HOUR_MINUTE * MINUTE_SECOND;
	
	
	private Units() {
	}
	
}
