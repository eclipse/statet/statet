/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class Version implements Comparable<Version> {
	
	private static int parseInt(final String value, final String componentName) {
		try {
			return Integer.parseInt(value);
		}
		catch (final NumberFormatException e) {
			throw new IllegalArgumentException(componentName + "= '" + value + "'", e);
		}
	}
	
	private static void validateInt(final int value, final String componentName) {
		if (value < 0) {
			throw new IllegalArgumentException(componentName + "= " + value);
		}
	}
	
	
	private final int major;
	private final int minor;
	private final int micro;
	private final String build;
	
	private transient @Nullable String versionString;
	private transient int hash;
	
	
	public Version(final int major, final int minor, final int micro,
			final @Nullable String qualifier) {
		this.major= major;
		this.minor= minor;
		this.micro= micro;
		this.build= nonNullElse(qualifier, ""); //$NON-NLS-1$
		validate();
	}
	
	public Version(final int major, final int minor, final int micro) {
		this(major, minor, micro, null);
	}
	
	public Version(final Version version) {
		this(version.getMajor(), version.getMinor(), version.getMicro(), version.getBuild());
	}
	
	public Version(final String version) {
		int major= 0;
		int minor= 0;
		int micro= 0;
		String build= "";
		{	final var components= version.split("\\.", 4);
			if (components.length == 0 || components[0].isEmpty()) {
				throw new IllegalArgumentException("version= '" +  version + "'");
			}
			major= parseInt(components[0], "major");
			if (components.length >= 2) {
				minor= parseInt(components[1], "minor");
				if (components.length >= 3) {
					micro= parseInt(components[2], "micro");
					if (components.length >= 4) {
						build= components[3];
					}
				}
			}
		}
		
		this.major= major;
		this.minor= minor;
		this.micro= micro;
		this.build= build;
		validate();
	}
	
	private void validate() {
		validateInt(this.major, "major");
		validateInt(this.minor, "minor");
		validateInt(this.micro, "micro");
		for (int i= 0; i < this.build.length(); i++) {
			final char c= this.build.charAt(i);
			if (!Character.isDefined(c) || c == ' ') {
				throw new IllegalArgumentException("build= " + this.build);
			}
		}
	}
	
	
	public int getMajor() {
		return this.major;
	}
	
	public int getMinor() {
		return this.minor;
	}
	
	public int getMicro() {
		return this.micro;
	}
	
	public String getBuild() {
		return this.build;
	}
	
	
	@Override
	public int hashCode() {
		int h= this.hash;
		if (h != 0) {
			return h;
		}
		h= 31 * 17;
		h= 31 * h + this.major;
		h= 31 * h + this.minor;
		h= 31 * h + this.micro;
		h= 31 * h + this.build.hashCode();
		return this.hash= h;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final Version other
						&& this.major == other.major
						&& this.minor == other.minor
						&& this.micro == other.micro
						&& this.build.equals(other.build) ));
	}
	
	@Override
	public int compareTo(final Version other) {
		if (this == other) {
			return 0;
		}
		int result= this.major - other.major;
		if (result != 0) {
			return result;
		}
		result= this.minor - other.minor;
		if (result != 0) {
			return result;
		}
		result= this.micro - other.micro;
		if (result != 0) {
			return result;
		}
		return this.build.compareTo(other.build);
	}
	
	
	@Override
	public String toString() {
		final String s= this.versionString;
		if (s != null) {
			return s;
		}
		final StringBuilder sb= new StringBuilder(20 + this.build.length());
		sb.append(this.major);
		sb.append('.');
		sb.append(this.minor);
		sb.append('.');
		sb.append(this.micro);
		if (!this.build.isEmpty()) {
			sb.append('.');
			sb.append(this.build);
		}
		return this.versionString= sb.toString();
	}
	
}
