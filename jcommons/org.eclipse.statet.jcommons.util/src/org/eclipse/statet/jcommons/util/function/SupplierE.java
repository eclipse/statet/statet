/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.util.function;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Supplier with typed exception.
 *
 * @param <T> the return type which is expected from {@link #get()}
 * @param <E> the exception type which can be thrown by {@link #get()}
 */
@FunctionalInterface
@NonNullByDefault
public interface SupplierE<T, E extends Exception> {
	
	
	T get() throws E;
	
}
