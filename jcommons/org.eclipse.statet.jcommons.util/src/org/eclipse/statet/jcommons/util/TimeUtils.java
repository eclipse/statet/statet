/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.util;

import java.time.Duration;
import java.time.ZoneId;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class TimeUtils {
	
	
	public static final ZoneId UTC_ZONE_ID= ZoneId.of("UTC"); //$NON-NLS-1$
	
	
	public static final long toMillisLong(final @Nullable Duration duration, final long nullValue) {
		if (duration == null) {
			return nullValue;
		}
		final long millis= duration.toMillis();
		if (millis <= 0) {
			return 1;
		}
		return millis;
	}
	
	public static final int toMillisInt(final @Nullable Duration duration, final int nullValue) {
		if (duration == null) {
			return nullValue;
		}
		final long millis= duration.toMillis();
		if (millis <= 0) {
			return 1;
		}
		if (millis >= Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		}
		return (int)millis;
	}
	
	
	private TimeUtils() {
	}
	
}
