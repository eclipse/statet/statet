/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class ArrayUtils {
	
	
	public static int indexOf(final byte[] a, final byte[] b) {
		nonNullAssert(a);
		final int l= b.length;
		if (a == b || l == 0) {
			return 0;
		}
		if (a.length < l) {
			return -1;
		}
		final byte b0= b[0];
		if (l == 1) {
			return indexOf(a, b0);
		}
		final int lastStart= a.length - l;
		ITER_START: for (int start= 0; start <= lastStart; start++) {
			if (a[start] == b0 && a[start + 1] == b[1]) {
				for (int i= start + 2, j= 2; j < l; i++, j++) {
					if (a[i] != b[j]) {
						continue ITER_START;
					}
				}
				return start;
			}
		}
		return -1;
	}
	
	public static int indexOf(final byte[] a, final byte b) {
		for (int i= 0; i < a.length; i++) {
			if (a[i] == b) {
				return i;
			}
		}
		return -1;
	}
	
	public static boolean contains(final byte[] a, final byte[] b) {
		return (indexOf(a, b) != -1);
	}
	
	public static boolean contains(final byte[] a, final byte b) {
		for (int i= 0; i < a.length; i++) {
			if (a[i] == b) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean startsWith(final byte[] a, final byte[] b) {
		nonNullAssert(a);
		final int l= b.length;
		if (a == b || l == 0) {
			return true;
		}
		if (a.length < l) {
			return false;
		}
		if (l < 16) {
			for (int i= 0; i < l; i++) {
				if (a[i] != b[i]) {
					return false;
				}
			}
			return true;
		}
		else {
			return Arrays.equals(a, 0, l, b, 0, l);
		}
	}
	
	public static boolean startsWith(final byte[] a, final byte b) {
		return (a.length > 0 && a[0] == b);
	}
	
	public static byte[] concat(final byte[] a1, final byte[] a2) {
		final int n1= a1.length;
		final byte[] a= new byte[n1 + a2.length];
		System.arraycopy(a1, 0, a, 0, n1);
		System.arraycopy(a2, 0, a, n1, a2.length);
		return a;
	}
	
	public static byte[] concat(final byte[] a1, final byte[] a2, final byte[] a3) {
		final int n1= a1.length;
		final int n12= n1 + a2.length;
		final byte[] a= new byte[n12 + a3.length];
		System.arraycopy(a1, 0, a, 0, n1);
		System.arraycopy(a2, 0, a, n1, a2.length);
		System.arraycopy(a3, 0, a, n12, a3.length);
		return a;
	}
	
	
	public static int indexOf(final int[] a, final int[] b) {
		nonNullAssert(a);
		final int l= b.length;
		if (a == b || l == 0) {
			return 0;
		}
		if (a.length < l) {
			return -1;
		}
		final int b0= b[0];
		if (l == 1) {
			return indexOf(a, b0);
		}
		final int lastStart= a.length - l;
		ITER_START: for (int start= 0; start <= lastStart; start++) {
			if (a[start] == b0 && a[start + 1] == b[1]) {
				for (int i= start + 2, j= 2; j < l; i++, j++) {
					if (a[i] != b[j]) {
						continue ITER_START;
					}
				}
				return start;
			}
		}
		return -1;
	}
	
	public static int indexOf(final int[] a, final int b) {
		for (int i= 0; i < a.length; i++) {
			if (a[i] == b) {
				return i;
			}
		}
		return -1;
	}
	
	public static boolean contains(final int[] a, final int[] b) {
		return (indexOf(a, b) != -1);
	}
	
	public static boolean contains(final int[] a, final int b) {
		for (int i= 0; i < a.length; i++) {
			if (a[i] == b) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean startsWith(final int[] a, final int[] b) {
		nonNullAssert(a);
		final int l= b.length;
		if (a == b || l == 0) {
			return true;
		}
		if (a.length < l) {
			return false;
		}
		if (l < 16) {
			for (int i= 0; i < l; i++) {
				if (a[i] != b[i]) {
					return false;
				}
			}
			return true;
		}
		else {
			return Arrays.equals(a, 0, l, b, 0, l);
		}
	}
	
	public static boolean startsWith(final int[] a, final int b) {
		return (a.length > 0 && a[0] == b);
	}
	
	public static int[] concat(final int[] a1, final int[] a2) {
		final int n1= a1.length;
		final int[] a= new int[n1 + a2.length];
		System.arraycopy(a1, 0, a, 0, n1);
		System.arraycopy(a2, 0, a, n1, a2.length);
		return a;
	}
	
	public static int[] concat(final int[] a1, final int[] a2, final int[] a3) {
		final int n1= a1.length;
		final int n12= n1 + a2.length;
		final int[] a= new int[n12 + a3.length];
		System.arraycopy(a1, 0, a, 0, n1);
		System.arraycopy(a2, 0, a, n1, a2.length);
		System.arraycopy(a3, 0, a, n12, a3.length);
		return a;
	}
	
	
	public static int[] toInt(final byte[] a) {
		final int[] ia= new int[a.length];
		for (int i= 0; i < a.length; i++) {
			ia[i]= a[i];
		}
		return ia;
	}
	
	public static int[] toIntUnsigned(final byte[] a) {
		final int[] ia= new int[a.length];
		for (int i= 0; i < a.length; i++) {
			ia[i]= (a[i] & 0xFF);
		}
		return ia;
	}
	
	
	private ArrayUtils() {
	}
	
}
