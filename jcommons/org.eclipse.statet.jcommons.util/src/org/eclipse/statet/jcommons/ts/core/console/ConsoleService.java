/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.ts.core.console;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.ts.core.ToolService;


/**
 * The basic service interface of tools offering console-like IO.
 */
@NonNullByDefault
public interface ConsoleService extends ToolService {
	
	
	boolean acceptNewConsoleCommand();
	
	/**
	 * Submits the text to the tool console.
	 * 
	 * @param input the text to submit
	 * @param m the progress monitor of the current run (or a child)
	 * @throws StatusException if an error occurred or the operation was canceled
	 */
	void submitToConsole(final String input,
			final ProgressMonitor m) throws StatusException;
	
	
	/*---- Status ----*/
	
	/**
	 * Reports a status to the user
	 * 
	 * @param status the status to handle
	 * @param m the progress monitor of the current run (or a child)
	 */
	void handleStatus(final Status status, final ProgressMonitor m);
	
	
}
