/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * A handler is plugged into a tool and is called by its tool service on special
 * events or tool commands.
 * For example a tool handler can enable the interaction with the GUI for the tool.
 * 
 * A single command handler can support multiple commands.
 * 
 * @since 1.1
 */
@NonNullByDefault
public interface ToolCommandHandler {
	
	
	/**
	 * Executes the specified command
	 * 
	 * @param id the ID of the command
	 * @param service the tool service calling the command
	 * @param data the command data for input (parameters) and output (return values)
	 * @param m the progress monitor
	 * 
	 * @return the status
	 */
	Status execute(final String id, final ToolService service, final ToolCommandData data,
			final ProgressMonitor m) throws StatusException;
	
}
