/*=============================================================================#
 # Copyright (c) 2006, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Central handler for a tool.
 * It provides access to its properties and the queue.
 */
@NonNullByDefault
public interface Tool {
	
	/**
	 * Constant for {@link #getLabel(int)} for the default label
	 */
	int DEFAULT_LABEL=                                     0x00000000;
	
	/**
	 * Constant for {@link #getLabel(int)} for a longer label with more information
	 */
	int LONG_LABEL=                                        0x00000001;
	
	
	/**
	 * Returns the main type of the tool.
	 * 
	 * @return id of the main tool type
	 */
	String getMainType();
	
	/**
	 * Returns if the tool provides the given feature set.
	 * 
	 * @param featureId id of the feature set
	 * @return <code>true</code> if the features are supported, otherwise <code>false</code>
	 */
	boolean isProvidingFeatureSet(final String featureId);
	
	/**
	 * Returns the runnable queue of the tool
	 * 
	 * @return the queue
	 */
	ToolQueue getQueue();
	
	/**
	 * Returns whether the tool is terminated.
	 * 
	 * @return <code>true</code> if terminated or disconnected, otherwise <code>false</code>
	 */
	boolean isTerminated();
	
	/**
	 * Adds the specified listener to the collection of listeners which will be notified when the
	 * tool has been terminated.
	 * 
	 * <p>If the identical listener is already registered, the method does not do anything.</p>
	 * 
	 * @param listener the listener to add
	 * 
	 * @since 4.10
	 */
	void addTerminationListener(Runnable listener);
	
	/**
	 * Removes the specified listener of the collection of listeners which will be notified when the
	 * tool has been terminated.
	 * 
	 * @param listener the listener to remove
	 * 
	 * @since 4.10
	 */
	void removeTerminationListener(Runnable listener);
	
	/**
	 * Returns a label for the tool.
	 * 
	 * Global config options: {@link #DEFAULT_LABEL}, {@link #LONG_LABEL}
	 * 
	 * @param config allows to configure the information to include in the label
	 * @return the label
	 */
	String getLabel(final int config);
	
	
	/**
	 * Returns the workspace.
	 * 
	 * @return the workspace
	 */
	ToolWorkspace getWorkspace();
	
	
}
