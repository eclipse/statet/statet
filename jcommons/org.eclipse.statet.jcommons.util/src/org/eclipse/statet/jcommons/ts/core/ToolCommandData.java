/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.ts.core;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * The data for execution of tool commands by a {@link ToolCommandHandler}.
 */
@NonNullByDefault
public interface ToolCommandData {
	
	
	public static class IllegalDataException extends RuntimeException {
		
		private static final long serialVersionUID= 1L;
		
		
		public IllegalDataException(final String message) {
			super(message);
		}
		
		
	}
	
	
	/**
	 * Returns the raw data as provided by the tool, if available.
	 * 
	 * @param key the key of the data entry.
	 * @return the data or {@code null} if not available.
	 */
	public @Nullable Object getRawData(final String key);
	
	/**
	 * Returns the data, if available.
	 * 
	 * <p>If the {@link #getRawData(String) raw data} of the data entry is not available, the method
	 * returns {@code null}.
	 * If the raw data is available and not an instance of the requested type, the raw data is
	 * converted to the requested type. If the conversion fails, the method throws an exception.</p>
	 * 
	 * @param key the key of the data entry
	 * @param type the type of the data
	 * @return the data or {@code null}
	 * @throws IllegalDataException if the data is incompatible to the required type
	 */
	public <TData> @Nullable TData get(final String key, final Class<TData> type)
			throws IllegalDataException;
	
	/**
	 * Returns the data or the specified default data.
	 * 
	 * <p>If the {@link #getRawData(String) raw data} of the data entry is not available, the method
	 * returns the specified default data.
	 * If the raw data is available and not an instance of the requested type, the raw data is
	 * converted to the requested type. If the conversion fails, the method throws an exception.</p>
	 * 
	 * @param key the key of the data entry
	 * @param type the type of the data
	 * @param defaultData the data to return if the data entry is missing
	 * @return the data
	 * @throws IllegalDataException if the data is incompatible to the required type
	 */
	public default <TData> TData get(final String key, final Class<TData> type,
			final @NonNull TData defaultData)
			throws IllegalDataException {
		final var data= get(key, type);
		if (data == null) {
			return defaultData;
		}
		return data;
	}
	
	/**
	 * Returns the data.
	 * 
	 * <p>If the {@link #getRawData(String) raw data} of the data entry is not available, the method
	 * throws an exception.
	 * If the raw data is available and not an instance of the requested type, the raw data is
	 * converted to the requested type. If the conversion fails, the method throws an exception.</p>
	 * 
	 * @param key the key of the data entry
	 * @param type the type of the data
	 * @return the data
	 * @throws IllegalDataException if the data is missing or incompatible to the required type
	 */
	public default <TData> TData getRequired(final String key, final Class<TData> type)
			throws IllegalDataException {
		final var data= get(key, type);
		if (data == null) {
			throw new IllegalDataException(String.format("Data entry '%1$s' is missing.",
					key ));
		}
		return data;
	}
	
	
	public default @Nullable String getString(final String key)
			throws IllegalDataException {
		return get(key, String.class);
	}
	
	public default String getString(final String key, final String defaultData)
			throws IllegalDataException {
		return get(key, String.class, defaultData);
	}
	
	public default String getStringRequired(final String key)
			throws IllegalDataException {
		return getRequired(key, String.class);
	}
	
	public default boolean getBoolean(final String key, final boolean defaultData)
			throws IllegalDataException {
		return get(key, Boolean.class, defaultData);
	}
	
	public default boolean getBooleanRequired(final String key)
			throws IllegalDataException {
		return getRequired(key, Boolean.class);
	}
	
	public default int getInt(final String key, final int defaultData)
			throws IllegalDataException {
		return get(key, Integer.class, defaultData);
	}
	
	public default int getIntRequired(final String key)
			throws IllegalDataException {
		return getRequired(key, Integer.class);
	}
	
	
	public void setReturnData(final String key, @Nullable Object value);
	
	public void removeReturnData(final String key);
	
	
}
