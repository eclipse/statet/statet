/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.CommonsNet;
import org.eclipse.statet.jcommons.net.core.RSAccessClientSession;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


/**
 * The workspace of a {@link Tool tool}.
 * 
 * @see Tool#getWorkspace()
 */
@NonNullByDefault
public interface ToolWorkspace {
	
	
	/**
	 * Returns the host (host name or IP address) of this workspace.
	 * 
	 * For local workspaces the method should return '{@link CommonsNet#LOCALHOST_NAME localhost}'.
	 * 
	 * @return the host
	 */
	public String getHost();
	
	/**
	 * Returns if this workspace is local.
	 * 
	 * @return {@code true} if the workspace is local, otherwise {@code false}
	 */
	public boolean isLocal();
	
	/**
	 * Returns if this workspace is remote (not local).
	 * 
	 * @return {@code true} if the workspace is remote, otherwise {@code false}
	 */
	public boolean isRemote();
	
	/**
	 * Returns the network client session of this remote workspace.
	 * 
	 * @return the client session if available or {@code null}
	 */
	public @Nullable RSAccessClientSession getRemoteNetSession(ProgressMonitor m) throws StatusException;
	
	
}
