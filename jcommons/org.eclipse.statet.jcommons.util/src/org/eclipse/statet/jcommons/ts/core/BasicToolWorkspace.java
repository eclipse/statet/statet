/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.ts.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.net.CommonsNet;
import org.eclipse.statet.jcommons.net.core.RSAccessClientSession;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;


@NonNullByDefault
public class BasicToolWorkspace implements ToolWorkspace {
	
	
	private final String host;
	
	
	public BasicToolWorkspace(final String host) {
		this.host= CommonsNet.isLocalLocalhost(host) ? CommonsNet.LOCALHOST_NAME : host;
	}
	
	
	@Override
	public String getHost() {
		return this.host;
	}
	
	@Override
	public boolean isLocal() {
		return (this.host == CommonsNet.LOCALHOST_NAME);
	}
	
	@Override
	public boolean isRemote() {
		return (this.host != CommonsNet.LOCALHOST_NAME);
	}
	
	@Override
	public @Nullable RSAccessClientSession getRemoteNetSession(final ProgressMonitor m) throws StatusException {
		return null;
	}
	
	
}
