/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.createJarArchiveUrl;
import static org.eclipse.statet.internal.jcommons.io.CommonsUriInternals.isSchemeUrl;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.eclipse.statet.internal.jcommons.io.JarStringUrl;
import org.eclipse.statet.jcommons.collections.ByteArrayList;
import org.eclipse.statet.jcommons.collections.ByteList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class UriUtils {
	
	
	public static final String HTTP_SCHEME= "http"; //$NON-NLS-1$
	public static final String HTTPS_SCHEME= "https"; //$NON-NLS-1$
	public static final String FILE_SCHEME= "file"; //$NON-NLS-1$
	public static final String JAR_SCHEME= "jar"; //$NON-NLS-1$
	
	public static final String JAR_SEPARATOR= "!/"; //$NON-NLS-1$
	
	
	public static final URI toUri(final URL url) throws URISyntaxException {
		final String s= url.toExternalForm();
		final String scheme= url.getProtocol();
		final String fragment= url.getRef();
		final String spp= s.substring(scheme.length() + 1,
				(fragment != null) ? s.length() - fragment.length() - 1 : s.length() );
		return new URI(scheme, spp, fragment);
	}
	
	
	public static boolean isValidScheme(final String s, int startIndex, final int endIndex) {
		if (endIndex > s.length() || startIndex < 0 || startIndex > endIndex) {
			throw new IllegalArgumentException();
		}
		if (startIndex == endIndex) {
			return false;
		}
		switch (s.charAt(startIndex++)) {
		case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
		case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
			break;
		default:
			return false;
		}
		while (startIndex < endIndex) {
			switch (s.charAt(startIndex++)) {
			case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
			case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			case '+', '-', '.':
				continue;
			default:
				return false;
			}
		}
		return true;
	}
	
	public static boolean isValidScheme(final String s) {
		return isValidScheme(s, 0, s.length());
	}
	
	public static boolean isScheme(final @Nullable String scheme, final String expectedScheme) {
		final int n= expectedScheme.length();
		if (scheme == null || scheme.length() != n) {
			return false;
		}
		for (int index= 0; index < n; index++) {
			final char c= scheme.charAt(index);
			final char cExpected= expectedScheme.charAt(index);
			if (c != cExpected && Character.toLowerCase(c) != cExpected) {
				return false;
			}
		}
		return true;
	}
	
	public static String normalizeScheme(final String scheme) throws URISyntaxException {
		final int n= scheme.length();
		{	// check
			if (n == 0) {
				throw new URISyntaxException(scheme, "Missing scheme name", 0);
			}
			boolean norm= true;
			switch (scheme.charAt(0)) {
			case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
				norm= false;
				break;
			case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
				break;
			default:
				throw new URISyntaxException(scheme, "Illegal character in scheme name", 0);
			}
			for (int index= 1; index < n; index++) {
				switch (scheme.charAt(index)) {
				case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
					norm= false;
					continue;
				case 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
				case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
				case '+', '-', '.':
					continue;
				default:
					throw new URISyntaxException(scheme, "Illegal character in scheme name", index);
				}
			}
			if (norm) {
				return scheme;
			}
		}
		
		final var sb= new StringBuilder(n);
		for (int index= 0; index < n; ) {
			final char c= scheme.charAt(index++);
			switch (c) {
			case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
				sb.append((char)(c + 32));
				continue;
			default:
				sb.append(c);
				continue;
			}
		}
		return sb.toString();
	}
	
	
	/**
	 * Returns if the specified URL is a file URL.
	 * 
	 * <p>The function does not validate the URL.</p>
	 * 
	 * @param url the URL
	 * @return <code>true</code> if it is a file URL, otherwise <code>false</code>
	 */
	public static final boolean isFileUrl(final URI url) {
		return isScheme(url.getScheme(), FILE_SCHEME);
	}
	
	/**
	 * Returns if the specified URL is a file URL.
	 * 
	 * <p>The function does not validate the URL.</p>
	 * 
	 * @param urlString the URL
	 * @return <code>true</code> if it is a file URL, otherwise <code>false</code>
	 */
	public static final boolean isFileUrl(final String urlString) {
		return isSchemeUrl(urlString, FILE_SCHEME);
	}
	
	/**
	 * Returns if the specified URL is a JAR URL.
	 * 
	 * <p>The function does not validate the URL.</p>
	 * 
	 * @param url the URL
	 * @return <code>true</code> if it is a JAR URL, otherwise <code>false</code>
	 */
	public static final boolean isJarUrl(final URI url) {
		return isScheme(url.getScheme(), JAR_SCHEME);
	}
	
	private static URI assertJarUrlArgument(final URI url) {
		if (!isJarUrl(url)) {
			throw new IllegalArgumentException("no JAR url");
		}
		return url;
	}
	
	private static String assertJarUrlArgument(final String urlString) {
		if (!isJarUrl(urlString)) {
			throw new IllegalArgumentException("no JAR url");
		}
		return urlString;
	}
	
	/**
	 * Returns if the specified URL is a JAR URL.
	 * 
	 * <p>The function does not validate the URL.</p>
	 * 
	 * @param urlString the URL
	 * @return <code>true</code> if it is a JAR URL, otherwise <code>false</code>
	 */
	public static final boolean isJarUrl(final String urlString) {
		return isSchemeUrl(urlString, JAR_SCHEME);
	}
	
	public static @Nullable ArchiveUrl getArchiveUrl(final URI url) throws URISyntaxException {
		final String scheme= url.getScheme();
		if (isScheme(scheme, JAR_SCHEME)) {
			return createJarArchiveUrl(url);
		}
		return null;
	}
	
	public static @Nullable ArchiveUrl getArchiveUrl(final String urlString) throws URISyntaxException {
		if (isSchemeUrl(urlString, JAR_SCHEME)) {
			return createJarArchiveUrl(urlString);
		}
		return null;
	}
	
	public static ArchiveUrl getJarArchiveUrl(final URI url) throws URISyntaxException {
		assertJarUrlArgument(url);
		return createJarArchiveUrl(url);
	}
	
	public static ArchiveUrl getJarArchiveUrl(final String urlString) throws URISyntaxException {
		assertJarUrlArgument(urlString);
		return createJarArchiveUrl(urlString);
	}
	
	/**
	 * Return the URL of the JAR archive file.
	 * 
	 * @param url the JAR URL
	 * @return the URL of the archive file
	 * @throws IllegalArgumentException if the specified URL is not a JAR URL
	 * @throws URISyntaxException if the specified URL is invalid
	 * 
	 * @see {@link #getJarEntryPath(URI)} for the other part of the URL
	 */
	public static final URI getJarFileUrl(final URI url) throws URISyntaxException {
		assertJarUrlArgument(url);
		final var archiveUrl= createJarArchiveUrl(url);
		return archiveUrl.getInnerArchiveUrl();
	}
	
	/**
	 * Return the path of the entry inside the JAR archive.
	 * 
	 * @param url the JAR URL
	 * @return the path of the entry as String (without leading slash)
	 * @throws IllegalArgumentException if the specified URL is not a JAR URL
	 * @throws URISyntaxException if the specified URL is invalid
	 * 
	 * @see {@link #getJarFileUrl(URI)} for the other part of the URL
	 */
	public static final String getJarEntryPath(final URI url) throws URISyntaxException {
		assertJarUrlArgument(url);
		final var archiveUrl= createJarArchiveUrl(url);
		return archiveUrl.getInnerEntryPathString();
	}
	
	/**
	 * Returns the JAR URL pointing to the root inside the specified JAR file.
	 * 
	 * @param jarFileUrlString URL as String pointing to the JAR file itself
	 * @return the JAR URL as String
	 * @throws URISyntaxException
	 */
	public static final URI toJarUrl(final String jarFileUrlString) throws URISyntaxException {
		return new URI(toJarUrlString(jarFileUrlString));
	}
	
	/**
	 * Returns the JAR URL as String pointing to the root inside the specified JAR file.
	 * 
	 * @param jarFileUrlString URL as String pointing to the JAR file itself
	 * @return the JAR URL as String
	 * @throws URISyntaxException
	 */
	public static final String toJarUrlString(final String jarFileUrlString) throws URISyntaxException {
		if (isSchemeUrl(jarFileUrlString, JAR_SCHEME)) {
			final var archiveUrl= createJarArchiveUrl(jarFileUrlString);
			if (archiveUrl instanceof final JarStringUrl jarUrl && archiveUrl.getOuterArchiveScheme().equals(FILE_SCHEME)) {
				return jarUrl.createInnerEntryUrlString("");
			}
			throw new IllegalArgumentException("url= " + jarFileUrlString); //$NON-NLS-1$
		}
		else {
			final StringBuilder sb= new StringBuilder(jarFileUrlString.length() + 6); // 4 + 2 = 6
			sb.append(UriUtils.JAR_SCHEME + ':');
			sb.append(jarFileUrlString);
			sb.append(UriUtils.JAR_SEPARATOR);
			return sb.toString();
		}
	}
	
	
	private static final boolean[] PATH_SEGMENT_IS_ALLOWED;
	private static final boolean[] FRAGMENT_IS_ALLOWED;
	
	private static final byte PERCENT_CODE= '%';
	private static final byte[] HEX_DIGIT_CODES= {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'A', 'B', 'C', 'D', 'E', 'F'
	};
	
	
	private static void add(final boolean[] is, final char... chars) {
		for (int i= 0; i < chars.length; i++) {
			is[chars[i]]= true;
		}
	}
	static {
		final char[] alpha= new char[] {
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
				'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
				'U', 'V', 'W', 'X', 'Y', 'Z',
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
				'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
				'u', 'v', 'w', 'x', 'y', 'z' };
		final char[] digit= new char[] {
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		final char[] subDelim= new char[] {
				'!', '$', '&', '\'', '(', ')', '*', '+', ',', ';', '=' };
		final char[] unreservedAdd= new char[] {
				'-', '.', '_', '~' };
		
		final boolean[] pcharAllowed= new boolean[0x100];
		add(pcharAllowed, alpha);
		add(pcharAllowed, digit);
		add(pcharAllowed, unreservedAdd);
		add(pcharAllowed, subDelim);
		add(pcharAllowed, ':', '@');
		
		{	final boolean[] isAllowed= pcharAllowed.clone();
			PATH_SEGMENT_IS_ALLOWED= isAllowed;
		}
		{	final boolean[] isAllowed= pcharAllowed.clone();
			add(isAllowed, '/', '?');
			FRAGMENT_IS_ALLOWED= isAllowed;
		}
	}
	
	
	private static String encodePercent(final String s, final boolean[] isAllowed) {
		int idx= 0;
		
		for (final int n= s.length(); ; ) {
			if (idx == n) {
				return s;
			}
			final char c= s.charAt(idx);
			if (c < 0x7F && isAllowed[c]) {
				idx++;
				continue;
			}
			else {
				break;
			}
		}
		
		final byte[] rawBytes= s.getBytes(StandardCharsets.UTF_8);
		final ByteList latinBytes= new ByteArrayList(rawBytes.length + ((3 + rawBytes.length / 10) * 2));
		if (idx > 0) {
			latinBytes.add(rawBytes, 0, idx);
		}
		do {
			final byte raw= rawBytes[idx++];
			if (isAllowed[(raw & 0xFF)]) {
				latinBytes.add(raw);
				continue;
			}
			else {
				latinBytes.add(PERCENT_CODE);
				latinBytes.add(HEX_DIGIT_CODES[(raw & 0xF0) >>> 4]);
				latinBytes.add(HEX_DIGIT_CODES[(raw & 0x0F)]);
			}
		} while (idx < rawBytes.length);
		
		return latinBytes.getString(StandardCharsets.ISO_8859_1);
	}
	
	/**
	 * Encodes the specified string as URI path segment.
	 * 
	 * @param segment the segment to encode
	 * @return the encoded segment
	 */
	public static String encodePathSegment(final String segment) {
		return encodePercent(segment, PATH_SEGMENT_IS_ALLOWED);
	}
	
	/**
	 * Encodes the specified string as URI fragment.
	 * 
	 * @param fragment the fragment to encode
	 * @return the encoded fragment
	 */
	public static String encodeFragment(final String fragment) {
		return encodePercent(fragment, FRAGMENT_IS_ALLOWED);
	}
	
	
	private UriUtils() {
	}
	
}
