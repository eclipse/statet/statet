/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface ArchiveUrl {
	
	
	public URI getUrl() throws URISyntaxException;
	public String getUrlString();
	
	public boolean isNested();
	
	public String getOuterArchiveType();
	public String getOuterArchiveScheme();
	public URI getOuterArchiveUrl() throws URISyntaxException;
	public String getOuterArchiveUrlString() throws URISyntaxException;
	
	public String getInnerArchiveType();
	public URI getInnerArchiveUrl() throws URISyntaxException;
	public String getInnerArchiveUrlString() throws URISyntaxException;
	public String getInnerEntryPathString();
	
}
