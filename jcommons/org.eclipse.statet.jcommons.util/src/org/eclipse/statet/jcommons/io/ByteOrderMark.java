/*=============================================================================#
 # Copyright (c) 2024, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import static org.eclipse.statet.jcommons.io.IOUtils.UTF_16BE_BOM;
import static org.eclipse.statet.jcommons.io.IOUtils.UTF_16LE_BOM;
import static org.eclipse.statet.jcommons.io.IOUtils.UTF_8_BOM;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public enum ByteOrderMark {
	
	
	UTF_8 (UTF_8_BOM) {
		
		@Override
		public boolean matchesStart(final byte[] bytes) {
			return (bytes.length >= 3
					&& bytes[0] == (byte)0xEF && bytes[1] == (byte)0xBB && bytes[2] == (byte)0xBF );
		}
		
		@Override
		public Charset getCharset() {
			return StandardCharsets.UTF_8;
		}
		
	},
	
	UTF_16BE (UTF_16BE_BOM) {
		
		@Override
		public boolean matchesStart(final byte[] bytes) {
			return (bytes.length >= 2
					&& bytes[0] == (byte)0xFE && bytes[1] == (byte)0xFF );
		}
		
		@Override
		public Charset getCharset() {
			return StandardCharsets.UTF_16BE;
		}
		
	},
	
	UTF_16LE (UTF_16LE_BOM) {
		
		@Override
		public boolean matchesStart(final byte[] bytes) {
			return (bytes.length >= 2
					&& bytes[0] == (byte)0xFF && bytes[1] == (byte)0xFE );
		}
		
		@Override
		public Charset getCharset() {
			return StandardCharsets.UTF_16LE;
		}
		
	};
	
	
	private ByteOrderMark(final byte[] bytes) {
		this.bytes= bytes;
	}
	
	
	private final byte[] bytes;
	
	
	protected byte[] getBytes() {
		return this.bytes;
	}
	
	public int getLength() {
		return this.bytes.length;
	}
	
	public abstract boolean matchesStart(final byte[] bytes);
	
	public void writeTo(final OutputStream out) throws IOException {
		out.write(this.bytes);
	}
	
	public abstract Charset getCharset();
	
	
	public static @Nullable ByteOrderMark of(final Charset charset) {
		if (charset.name().startsWith("UTF-")) {
			if (charset.equals(StandardCharsets.UTF_8)) {
				return UTF_8;
			}
			else if (charset.equals(StandardCharsets.UTF_16BE)) {
				return UTF_16BE;
			}
			else if (charset.equals(StandardCharsets.UTF_16LE)) {
				return UTF_16LE;
			}
		}
		return null;
	}
	
	public static @Nullable ByteOrderMark of(final byte[] bom) {
		return switch (bom.length) {
		case 2 ->
			switch (bom[0]) {
			case (byte)0xFE -> (bom[1] == (byte)0xFF) ? UTF_16BE : null;
			case (byte)0xFF -> (bom[1] == (byte)0xFE) ? UTF_16LE : null;
			default -> null;
			};
		case 3 -> (bom[0] == (byte)0xEF && bom[1] == (byte)0xBB && bom[2] == (byte)0xBF) ? UTF_8 : null;
		default -> null;
		};
	}
	
	public static @Nullable ByteOrderMark ofStart(final byte[] bom) {
		if (bom.length >= 2) {
			return switch (bom[0]) {
			case (byte)0xEF -> (bom.length >= 3 && bom[1] == (byte)0xBB && bom[2] == (byte)0xBF) ? UTF_8 : null;
			case (byte)0xFE -> (bom[1] == (byte)0xFF) ? UTF_16BE : null;
			case (byte)0xFF -> (bom[1] == (byte)0xFE) ? UTF_16LE : null;
			default -> null;
			};
		}
		return null;
	}
	
	public static @Nullable ByteOrderMark read(final InputStream in) throws IOException {
		return switch (in.read()) {
		case 0xEF -> (in.read() == 0xBB && in.read() == 0xBF) ? UTF_8 : null;
		case 0xFE -> (in.read() == 0xFF) ? UTF_16BE : null;
		case 0xFF -> (in.read() == 0xFE) ? UTF_16LE : null;
		default -> null;
		};
	}
	
	
}
