/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.file.Files;
import java.nio.file.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class IOUtils {
	
	static final char BOM_CHAR= 0xFEFF;
	
	static final byte[] UTF_8_BOM=    new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF };
	static final byte[] UTF_16BE_BOM= new byte[] { (byte)0xFE, (byte)0xFF };
	static final byte[] UTF_16LE_BOM= new byte[] { (byte)0xFF, (byte)0xFE };
	
	
	public static InputStream orNullStream(final @Nullable InputStream stream) {
		return (stream != null) ? stream : InputStream.nullInputStream();
	}
	
	public static OutputStream orNullStream(final @Nullable OutputStream stream) {
		return (stream != null) ? stream : OutputStream.nullOutputStream();
	}
	
	
	public static InputStreamReader newStreamContentReader(final InputStream in,
			final @Nullable ByteOrderMark bom, Charset defaultCharset) throws IOException {
		if (bom != null) {
			in.skipNBytes(bom.getLength());
			defaultCharset= bom.getCharset();
		}
		return new InputStreamReader(in, defaultCharset);
	}
	
	/**
	 * Skips the BOM in the input if present.
	 * 
	 * @param reader the input reader.
	 */
	public static void skipBOM(final BufferedReader reader) throws IOException {
		reader.mark(1);
		if (reader.read() != BOM_CHAR) {
			reader.reset();
		}
	}
	
	public static BufferedReader newBufferedStreamContentReader(final InputStream in,
			final CharsetDecoder decoder) throws IOException {
		final var reader= new BufferedReader(new InputStreamReader(in, decoder));
		if (decoder.charset().name().startsWith("UTF-")) { //$NON-NLS-1$
			skipBOM(reader);
		}
		return reader;
	}
	
	public static BufferedReader newBufferedStreamContentReader(final InputStream in,
			final Charset charset, final CodingErrorAction errorAction) throws IOException {
		CharsetDecoder decoder= charset.newDecoder();
		if (errorAction != CodingErrorAction.REPORT) {
			decoder= decoder.onMalformedInput(errorAction).onUnmappableCharacter(errorAction);
		}
		return newBufferedStreamContentReader(in, decoder);
	}
	
	public static BufferedReader newBufferedStreamContentReader(final InputStream in,
			final Charset charset) throws IOException {
		final CharsetDecoder decoder= charset.newDecoder();
		return newBufferedStreamContentReader(in, decoder);
	}
	
	/**
	 * Strips the BOM of the specified string if present.
	 * 
	 * @param s the string.
	 * @return the content of the string (string without BOM).
	 */
	public static String stripBOM(final String s) throws IOException {
		final int l= s.length();
		return (l > 0 && s.charAt(0) == BOM_CHAR) ?
				s.substring(1, l) : s;
	}
	
	/**
	 * Reads the character content from the specified input stream.
	 * 
	 * @param in the stream to read.
	 * @param charset the charset to use to decode the stream.
	 * @return the content of the stream (string without BOM).
	 */
	public static String readContentString(final InputStream in,
			final Charset charset) throws IOException {
		final byte[] bytes= in.readAllBytes();
		final var bom= ByteOrderMark.of(charset);
		final int startIndex= (bom != null && bom.matchesStart(bytes)) ? bom.getLength() : 0;
		return new String(bytes, startIndex, bytes.length - startIndex, charset);
	}
	
	/**
	 * Reads the character content of the specified file.
	 * 
	 * @param path the path of the file to read.
	 * @param charset the charset to use to decode the file.
	 * @return the content of the file (string without BOM).
	 */
	public static String readContentString(final Path path,
			final Charset charset) throws IOException {
		String s= Files.readString(path, charset);
		if (charset.name().startsWith("UTF-")) { //$NON-NLS-1$
			s= stripBOM(s);
		}
		return s;
	}
	
	
	public static void writeBOM(final OutputStream out, final Charset charset) throws IOException {
		final var bom= ByteOrderMark.of(charset);
		if (bom != null) {
			bom.writeTo(out);
		}
	}
	
	
	public static void close(final @Nullable Closeable toClose, final Throwable exception) {
		if (toClose != null) {
			try {
				toClose.close();
			}
			catch (final IOException | RuntimeException e) {
				exception.addSuppressed(e);
			}
		}
	}
	
	
	private IOUtils() {
	}
	
}
