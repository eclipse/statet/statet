/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.jcommons.io;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class FileUtils {
	
	
	private static final Path USER_HOME_DIRECTORY_PATH= Path.of(
			nonNullAssert(System.getProperty("user.home")) ).toAbsolutePath(); //$NON-NLS-1$
	
	private static final Path USER_WORKING_DIRECTORY_REF_PATH= Path.of(""); //$NON-NLS-1$
	
	/**
	 * Returns the user home directory (= system property {@code user.home}).
	 * 
	 * @return the path of the user home directory
	 * 
	 * @see System#getProperties()
	 */
	public static final Path getUserHomeDirectory() {
		return USER_HOME_DIRECTORY_PATH;
	}
	
	/**
	 * Returns the current working directory (= system property {@code user.dir}).
	 * 
	 * @return the path of the working directory
	 * 
	 * @see System#getProperties()
	 */
	public static final Path getUserWorkingDirectory() {
		return USER_WORKING_DIRECTORY_REF_PATH.toAbsolutePath();
	}
	
	
	/**
	 * Checks if the specified path has a {@link Path#getParent() parent} and returns it.
	 * 
	 * @param path the path
	 * @return the parent of the path
	 * @throws IllegalArgumentException if the path does not have a parent
	 */
	public static final Path requireParent(final Path path) {
		final Path parent= path.getParent();
		if (parent == null) {
			throw new IllegalArgumentException("path does not have a parent"); //$NON-NLS-1$
		}
		return parent;
	}
	
	/**
	 * Checks if the specified path is not empty (= has a {@link Path#getFileName() file name}) and
	 * returns its file name.
	 * 
	 * @param path the path
	 * @return the file name segment of the path
	 * @throws IllegalArgumentException if the path is empty
	 */
	public static final Path requireFileName(final Path path) {
		final Path fileName= path.getFileName();
		if (fileName == null) {
			throw new IllegalArgumentException("path is empty"); //$NON-NLS-1$
		}
		return fileName;
	}
	
	
	/**
	 * Returns a list of existing regular files within a directory.
	 * 
	 * @param dir the path of the directory
	 * @param fileNames the file names to look for
	 * @return a list with path of the matching files
	 */
	public static final List<Path> getRegularFiles(final Path dir, final ImList<String> fileNames) {
		final List<Path> files= new ArrayList<>(fileNames.size());
		for (final String name : fileNames) {
			final Path file= dir.resolve(name);
			if (Files.isRegularFile(file)) {
				files.add(file);
			}
		}
		return files;
	}
	
	
	private static final FileVisitor<Path> DELETE_R_VISITOR = new FileVisitor<>() {
		
		@Override
		public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs)
				throws IOException {
			if (attrs.isSymbolicLink() || attrs.isOther()) {
				Files.delete(dir);
				return FileVisitResult.SKIP_SUBTREE;
			}
			return FileVisitResult.CONTINUE;
		}
		
		@Override
		public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs)
				throws IOException {
			Files.delete(file);
			return FileVisitResult.CONTINUE;
		}
		
		@Override
		public FileVisitResult visitFileFailed(final Path file, final IOException exc)
				throws IOException {
			throw exc;
		}
		
		@Override
		public FileVisitResult postVisitDirectory(final Path dir, final @Nullable IOException exc)
				throws IOException {
			Files.delete(dir);
			return FileVisitResult.CONTINUE;
		}
		
	};
	
	
	/**
	 * Deletes the specified file if it exists. If the file is a directory, its entries are deleted
	 * recursively.
	 * 
	 * @param path the path of the file to delete
	 * @return {@code true} if the file was deleted, {@code false} if the file does not exists
	 * @throws IOException if an I/O error occurs
	 */
	public static final boolean deleteRecursively(final Path path) throws IOException {
		if (Files.notExists(path)) {
			return false;
		}
		Files.walkFileTree(path, DELETE_R_VISITOR);
		return true;
	}
	
	/**
	 * Cleans the specified file directory.
	 * 
	 * @param path the path of the directory to clean
	 * @param filter to select entries of the directory to delete
	 * @throws NotDirectoryException if the file is not a directory
	 * @throws IOException if an I/O error occurs
	 */
	public static final void cleanDirectory(final Path path,
			final DirectoryStream.Filter<? super Path> filter) throws IOException {
		try (final var stream= Files.newDirectoryStream(path, filter)) {
			for (final Path entry : stream) {
				deleteRecursively(entry);
			}
		}
	}
	
	/**
	 * Cleans the specified file directory.
	 * 
	 * @param path the path of the directory to clean
	 * @throws NotDirectoryException if the file is not a directory
	 * @throws IOException if an I/O error occurs
	 */
	public static final void cleanDirectory(final Path path) throws IOException {
		try (final var entries= Files.newDirectoryStream(path)) {
			for (final Path entry : entries) {
				deleteRecursively(entry);
			}
		}
	}
	
	
	private static final Comparator<Path> LAST_MODIFIED_TIME_COMPARATOR= new Comparator<>() {
		@Override
		public int compare(final Path o1, final Path o2) {
			FileTime time1= null;
			FileTime time2= null;
			try {
				time1= Files.getLastModifiedTime(o1);
			}
			catch (final IOException e) {}
			try {
				time2= Files.getLastModifiedTime(o2);
			}
			catch (final IOException e) {}
			
			if (time1 == null) {
				return (time2 == null) ? 0 : 1;
			}
			if (time2 == null) {
				return -1;
			}
			return time1.compareTo(time2);
		}
	};
	
	public static final Comparator<Path> getLastModifiedTimeComparator() {
		return LAST_MODIFIED_TIME_COMPARATOR;
	}
	
	
	private FileUtils() {
	}
	
}
