/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.CTX1_NAME;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_NODE_MISSING;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;


@NonNullByDefault
public class DcfLexerTest {
	
	
	private final StringParserInput input= new StringParserInput();
	
	
	public DcfLexerTest() {
	}
	
	
	static ImList<String> lineEndings() {
		return ImCollections.newList("", "\n", "\r", "\r\n");
	}
	
	
	@Test
	public void getInput() {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset("Name: Value").init());
		assertSame(this.input, lexer.getInput());
	}
	
	
	@Test
	public void empty() {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset("").init());
		assertNextToken(DcfLexer.EOF, 0, 0, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void fieldNameValue(final String lineEnd) {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset("Name:Value" + lineEnd).init());
		assertNextToken(DcfLexer.FIELD_NAME, 0, 5, lexer);
		assertNextToken(DcfLexer.VALUE, 5, 5, lexer);
		assertEnd(lineEnd, 10, lexer);
		
		lexer.reset(this.input.reset("Name: Value 123" + lineEnd).init());
		assertNextToken(DcfLexer.FIELD_NAME, 0, 5, lexer);
		assertNextToken(DcfLexer.VALUE, 6, 9, lexer);
		assertEnd(lineEnd, 15, lexer);
		
		lexer.reset(this.input.reset("Name:\tValue " + lineEnd).init());
		assertNextToken(DcfLexer.FIELD_NAME, 0, 5, lexer);
		assertNextToken(DcfLexer.VALUE, 6, 5, lexer);
		assertEnd(lineEnd, 12, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void fieldName_withoutValue(final String lineEnd) {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset("Name:" + lineEnd).init());
		assertNextToken(DcfLexer.FIELD_NAME, 0, 5, lexer);
		assertEnd(lineEnd, 5, lexer);
		
		lexer.reset(this.input.reset("Name: \t" + lineEnd).init());
		assertNextToken(DcfLexer.FIELD_NAME, 0, 5, lexer);
		assertEnd(lineEnd, 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void fieldName_invalid(final String lineEnd) {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset(":" + lineEnd).init());
		assertNextToken(DcfLexer.FIELD_NAME, TYPE12_SYNTAX_NODE_MISSING | CTX1_NAME,
				0, 1,
				lexer );
		assertEnd(lineEnd, 1, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void continueValue(final String lineEnd) {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset(" 123" + lineEnd).init());
		assertNextToken(DcfLexer.VALUE, 1, 3, lexer);
		assertEnd(lineEnd, 4, lexer);
		
		lexer.reset(this.input.reset("\t\t 123\t" + lineEnd).init());
		assertNextToken(DcfLexer.VALUE, 3, 3, lexer);
		assertEnd(lineEnd, 7, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void continueValue_EmptyLine(final String lineEnd) {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset(" ." + lineEnd).init());
		assertNextToken(DcfLexer.VALUE_EMPTY_LINE, 1, 1, lexer);
		assertEnd(lineEnd, 2, lexer);
		
		lexer.reset(this.input.reset("\t\t \f\u000B.\t\f\u000B" + lineEnd).init());
		assertNextToken(DcfLexer.VALUE_EMPTY_LINE, 5, 1, lexer);
		assertEnd(lineEnd, 9, lexer);
		
		lexer.reset(this.input.reset("\t\t \f\u000B..\t\f\u000B" + lineEnd).init());
		assertNextToken(DcfLexer.VALUE, 3, 7, lexer);
		assertEnd(lineEnd, 10, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void blankLine(final String lineEnd) {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset(lineEnd).init());
		if (!lineEnd.isEmpty()) {
			assertNextToken(DcfLexer.BLANK_LINE, 0, lineEnd.length(), lexer);
		}
		assertNextToken(DcfLexer.EOF, lineEnd.length(), 0, lexer);
		
		lexer.reset(this.input.reset("  \t" + lineEnd).init());
		if (!lineEnd.isEmpty()) {
			assertNextToken(DcfLexer.BLANK_LINE, 0, 3 + lineEnd.length(), lexer);
		}
		assertNextToken(DcfLexer.EOF, 3 + lineEnd.length(), 0, lexer);
	}
	
	@ParameterizedTest
	@MethodSource("lineEndings")
	public void unknown(final String lineEnd) {
		final DcfLexer lexer= new DcfLexer();
		
		lexer.reset(this.input.reset("Foo" + lineEnd).init());
		assertNextToken(DcfLexer.UNKNOWN, 0, 3, lexer);
		assertEnd(lineEnd, 3, lexer);
	}
	
	
	static  void assertNextToken(final byte expectedType, final int expectedFlags,
			final int expectedOffset, final int expectedLength,
			final DcfLexer lexer) {
		assertEquals(expectedType, lexer.next(), "type (next)");
		assertEquals(expectedType, lexer.getType(), "type");
		assertEquals(expectedFlags, lexer.getFlags(), "flags");
		assertEquals(expectedOffset, lexer.getOffset(), "offset");
		assertEquals(expectedLength, lexer.getLength(), "length");
		assertNull(lexer.getStatusDetail(), "statusDetail");
	}
	
	static void assertNextToken(final byte expectedType,
			final int expectedOffset, final int expectedLength,
			final DcfLexer lexer) {
		assertNextToken(expectedType, 0, expectedOffset, expectedLength, lexer);
	}
	
	static void assertEnd(final String lineEnd, int offset,
			final DcfLexer lexer) {
		if (!lineEnd.isEmpty()) {
			assertNextToken(DcfLexer.LINEBREAK, 0, offset, lineEnd.length(), lexer);
			offset+= lineEnd.length();
		}
		assertNextToken(DcfLexer.EOF, offset, 0, lexer);
	}
	
}
