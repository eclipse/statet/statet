/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.CTX12_VALUE_CONT;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.CTX1_NAME;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_NODE_MISSING;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_TOKEN_UNKNOWN;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.Dummy;
import org.eclipse.statet.dsl.core.source.ast.NodeType;
import org.eclipse.statet.dsl.core.source.ast.Record;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.ltk.core.StatusCodes;


@NonNullByDefault
public class DcfRParserTest {
	
	
	private final StringParserInput input= new StringParserInput();
	
	private final DcfRParser parser;
	
	
	public DcfRParserTest() {
		this.parser= createParser();
	}
	
	protected DcfRParser createParser() {
		return new DcfRParser();
	}
	
	protected boolean assertText() {
		return true;
	}
	
	
	static ImList<String> lineSeparators() {
		return ImCollections.newList("\n", "\r\n");
	}
	
	
	@Test
	public void getSyntaxLabel() {
		assertEquals("DCF (R)", this.parser.getSyntaxLabel());
	}
	
	
	@ParameterizedTest
	@MethodSource("lineSeparators")
	public void empty(final String lineEnd) {
		SourceComponent sourceComponent;
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"").init() );
		assertSourceComponent(   0,                          0,
				0, sourceComponent );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"" + lineEnd).init() );
		assertSourceComponent(   0,                          0,
				0, sourceComponent );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"" + lineEnd + lineEnd).init() );
		assertSourceComponent(   0,                          0,
				0, sourceComponent );
	}
	
	@ParameterizedTest
	@MethodSource("lineSeparators")
	public void Field_simpleValue(final String lineEnd) {
		SourceComponent sourceComponent;
		Record record;
		Field field;
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1:Value1" + lineEnd).init() );
		assertSourceComponent(   0,                         13 + 1 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                         13 + 1 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0, "Field1", 0, "Value1", 0,
				record, 0 );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1: Value 2. " + lineEnd).init() );
		assertSourceComponent(   0,                         17 + 1 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                         17 + 1 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0, "Field1", 1, "Value 2.", 1,
				record, 0 );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1: . " + lineEnd).init() );
		assertSourceComponent(   0,                         10 + 1 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                         10 + 1 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0, "Field1", 1, ".", 1,
				record, 0 );
	}
	
	@ParameterizedTest
	@MethodSource("lineSeparators")
	public void Field_empty(final String lineEnd) {
		SourceComponent sourceComponent;
		Record record;
		Field field;
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1:" + lineEnd).init() );
		assertSourceComponent(   0,                          7 + 1 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                          7 + 1 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0, "Field1", 0, "", 0,
				record, 0 );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1:  " + lineEnd).init() );
		assertSourceComponent(   0,                          9 + 1 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                          9 + 1 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0, "Field1", 0, "", 2,
				record, 0 );
	}
	
	@ParameterizedTest
	@MethodSource("lineSeparators")
	public void Field_multilineValue(final String lineEnd) {
		SourceComponent sourceComponent;
		Record record;
		Field field;
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1:Line1" + lineEnd +
				" Line2" + lineEnd).init() );
		assertSourceComponent(   0,                         18 + 2 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                         18 + 2 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0,                         18 + 1 * lineEnd.length(),
				"Field1", record, 0 );
		assertValue(0,           7,                         18 + 1 * lineEnd.length(),
				"Line1\nLine2", field );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1: Line 1 " + lineEnd +
				" Line 2\t " + lineEnd).init() );
		assertSourceComponent(   0,                         24 + 2 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                         24 + 2 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0,                         24 + 1 * lineEnd.length(),
				"Field1", record, 0 );
		assertValue(0,           7,                         24 + 1 * lineEnd.length(),
				"Line 1\nLine 2", field );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1:" + lineEnd +
				"  Line 1 " + lineEnd +
				"  Line 2. " + lineEnd).init() );
		assertSourceComponent(   0,                         26 + 3 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                         26 + 3 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0,                         26 + 2 * lineEnd.length(),
				"Field1", record, 0 );
		assertValue(0,           7,                         26 + 2 * lineEnd.length(),
				"Line 1\nLine 2.", field );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1:" + lineEnd +
				"\t." + lineEnd +
				"\tLine 2" + lineEnd +
				"\t." + lineEnd +
				"\tPara 2" + lineEnd +
				lineEnd).init() );
		assertSourceComponent(   0,                         25 + 5 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                         25 + 5 * lineEnd.length(),
				1, sourceComponent, 0 );
		field= assertField(      0,                         25 + 4 * lineEnd.length(),
				"Field1", record, 0 );
		assertValue(0,           7,                         25 + 4 * lineEnd.length(),
				"\nLine 2\n\nPara 2", field );
	}
	
	@ParameterizedTest
	@MethodSource("lineSeparators")
	public void Record(final String lineEnd) {
		SourceComponent sourceComponent;
		Record record;
		Field field;
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1: Value1" + lineEnd +
				"Field2: Value 2 line 1 ..." + lineEnd +
				"  ... line2" + lineEnd +
				"Field3:" + lineEnd).init() );
		assertSourceComponent(   0,                         58 + 4 * lineEnd.length(),
				1, sourceComponent );
		record= assertRecord(    0,                         58 + 4 * lineEnd.length(),
				3, sourceComponent, 0 );
		field= assertField(      0,                         14,
				"Field1", record, 0 );
		assertValue(0,           7,                         14,
				"Value1", field );
		field= assertField(     14 + 1 * lineEnd.length(),  51 + 2 * lineEnd.length(),
				"Field2", record, 1 );
		assertValue(0,          21 + 1 * lineEnd.length(),  51 + 2 * lineEnd.length(),
				"Value 2 line 1 ...\n... line2", field );
		field= assertField(     51 + 3 * lineEnd.length(),  58 + 3 * lineEnd.length(),
				"Field3", record, 2 );
		assertValue(0,          58 + 3 * lineEnd.length(),  58 + 3 * lineEnd.length(),
				"", field );
	}
	
	@Test
	public void Record_multiple() {
		SourceComponent sourceComponent;
		int start;
		Record record;
		Field field;
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1: ValueA\n" +
				"Field2: Value 2A line 1 ...\n" +
				"  ... line2\n" +
				"Field3:\n" +
				"\n" +
				"Field1: ValueB\n" +
				"Field2: Value 2B line 1 ...\n" +
				"  ... line2\n" +
				"Field4:END\n" ).init() );
		assertSourceComponent(0, 130, 2, sourceComponent);
		start= 0;
		record= assertRecord(start, start + 63, 3, sourceComponent, 0);
		field= assertField(start + 0, start + 14, "Field1", record, 0);
		field= assertField(start + 15, start + 54, "Field2", record, 1);
		field= assertField(start + 55, start + 62, "Field3", record, 2);
		start= record.getEndOffset() + 1;
		record= assertRecord(start, start + 66, 3, sourceComponent, 1);
		field= assertField(start + 0, start + 14, "Field1", record, 0);
		field= assertField(start + 15, start + 54, "Field2", record, 1);
		field= assertField(start + 55, start + 65, "Field4", record, 2);
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"\n" +
				"Field1: ValueA\n" +
				"Field2: Value 2 line 1 ...\n" +
				"  ... line2\n" +
				"\n" +
				"Field1: ValueB\n" +
				"\n" + 
				"\n" + 
				"Field1: ValueC\n" +
				"Field5: XXX\n" +
				"\n" +
				"\n" ).init() );
		assertSourceComponent(1, 100, 3, sourceComponent);
		start= 1;
		record= assertRecord(start, start + 54, 2, sourceComponent, 0);
		field= assertField(start + 0, start + 14, "Field1", record, 0);
		field= assertField(start + 15, start + 53, "Field2", record, 1);
		start= record.getEndOffset() + 1;
		record= assertRecord(56, 71, 1, sourceComponent, 1);
		field= assertField(start + 0, start + 14, "Field1", record, 0);
		start= record.getEndOffset() + 2;
		record= assertRecord(start, 100, 2, sourceComponent, 2);
		field= assertField(start + 0, start + 14, "Field1", record, 0);
		field= assertField(start + 15, start + 26, "Field5", record, 1);
	}
	
	@ParameterizedTest
	@MethodSource("lineSeparators")
	public void invalid(final String lineEnd) {
		SourceComponent sourceComponent;
		Record record;
		Field field;
		DslAstNode node;
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				"Field1" + lineEnd).init() );
		assertSourceComponent(StatusCodes.ERROR_IN_CHILD,
				0, 6 + lineEnd.length(), 1, sourceComponent);
		record= assertRecord(StatusCodes.ERROR_IN_CHILD,
				0, 6 + lineEnd.length(), 1, sourceComponent, 0 );
		node= record.getChild(0);
		assertSame(record, node.getParent(), "parent");
		assertNode(NodeType.ERROR, Dummy.class, TYPE12_SYNTAX_TOKEN_UNKNOWN,
				0, 6, null, 0, node );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				": Value" + lineEnd).init() );
		assertSourceComponent(StatusCodes.ERROR_IN_CHILD,
				0, 7 + lineEnd.length(), 1, sourceComponent );
		record= assertRecord(StatusCodes.ERROR_IN_CHILD,
				0, 7 + lineEnd.length(), 1, sourceComponent, 0 );
		field= assertField(StatusCodes.ERROR_IN_CHILD,
				0, 7, record, 0 );
		assertFieldName(TYPE12_SYNTAX_NODE_MISSING | CTX1_NAME,
				0, null, field );
		
		sourceComponent= this.parser.parseSourceUnit(this.input.reset(
				" Value" + lineEnd).init() );
		assertSourceComponent(StatusCodes.ERROR_IN_CHILD,
				0, 6 + lineEnd.length(), 1, sourceComponent);
		record= assertRecord(StatusCodes.ERROR_IN_CHILD,
				0, 6 + lineEnd.length(), 1, sourceComponent, 0);
		node= record.getChild(0);
		assertSame(record, node.getParent(), "parent");
		assertNode(NodeType.SCALAR, Value.class, TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_VALUE_CONT,
				1, 6, null, 0, node );
	}
	
	
	protected SourceComponent assertSourceComponent(
			final int expectedStatusCode,
			final int expectedStartOffset, final int expectedEndOffset,
			final int expectedChildCount,
			final DslAstNode actual) {
		assertNode(NodeType.SOURCELINES, SourceComponent.class, expectedStatusCode,
				expectedStartOffset, expectedEndOffset,
				null, expectedChildCount,
				actual );
		
		return (SourceComponent)actual;
	}
	
	protected SourceComponent assertSourceComponent(
			final int expectedStartOffset, final int expectedEndOffset,
			final int expectedChildCount,
			final DslAstNode actual) {
		return assertSourceComponent(0, expectedStartOffset, expectedEndOffset,
				expectedChildCount,
				actual );
	}
	
	protected Record assertRecord(
			final int expectedStatusCode,
			final int expectedStartOffset, final int expectedEndOffset,
			final int expectedChildCount,
			final DslAstNode parent, final int index) {
		final var actual= parent.getChild(index);
		assertSame(parent, actual.getParent(), "parent");
		assertNode(NodeType.RECORD, Record.class, expectedStatusCode,
				expectedStartOffset, expectedEndOffset,
				null, expectedChildCount,
				actual );
		
		return (Record)actual;
	}
	
	protected Record assertRecord(
			final int expectedStartOffset, final int expectedEndOffset,
			final int expectedChildCount,
			final DslAstNode parent, final int index) {
		return assertRecord(0, expectedStartOffset, expectedEndOffset, expectedChildCount,
				parent, index );
	}
	
	protected Field assertField(
			final int expectedStatusCode,
			final int expectedStartOffset, final int expectedEndOffset,
			final DslAstNode parent, final int index) {
		final var actual= parent.getChild(index);
		assertSame(parent, actual.getParent(), "parent");
		assertNode(NodeType.KEY_VALUE_ENTRY, Field.class, expectedStatusCode,
				expectedStartOffset, expectedEndOffset,
				null, 2,
				actual );
		
		return (Field)actual;
	}
	
	protected Field assertField(
			final int expectedStartOffset,
			final String expectedName, final int space, final String expectedValue, final int trailing,
			final DslAstNode parent, final int index) {
		final int expectedValueStartOffset= expectedStartOffset + expectedName.length() + 1;
		final int expectedEndOffset= expectedValueStartOffset + space + expectedValue.length() + trailing;
		final Field field= assertField(0, expectedStartOffset, expectedEndOffset, parent, index);
		assertFieldName(0, expectedStartOffset, expectedName, field);
		assertValue(0, expectedValueStartOffset, expectedEndOffset,
				expectedValue, field );
		return field;
	}
	
	protected Field assertField(
			final int expectedStartOffset, final int expectedEndOffset,
			final String expectedName,
			final DslAstNode parent, final int index) {
		final Field field= assertField(0, expectedStartOffset, expectedEndOffset, parent, index);
		assertFieldName(0, expectedStartOffset, expectedName, field);
		return field;
	}
	
	protected Name assertFieldName(
			final int expectedStatusCode,
			final int expectedStartOffset, final @Nullable String expectedName,
			final Field parent) {
		final var actual= parent.getKey();
		assertSame(actual, parent.getChild(0));
		assertSame(parent, actual.getParent(), "parent");
		assertNode(NodeType.SCALAR, Name.class, expectedStatusCode,
				expectedStartOffset, expectedStartOffset + ((expectedName != null) ? expectedName.length() : 0),
				expectedName, 0,
				actual );
		
		return (Name)actual;
	}
	
	protected Value assertValue(
			final int expectedStatusCode,
			final int expectedStartOffset, final int expectedEndOffset,
			final String text,
			final Field parent) {
		final var actual= parent.getValue();
		assertSame(actual, parent.getChild(1));
		assertSame(parent, actual.getParent(), "parent");
		assertNode(NodeType.SCALAR, Value.class, 0,
				expectedStartOffset, expectedEndOffset,
				text, 0,
				actual );
		
		return (Value)actual;
	}
	
	
	protected void assertNode(
			final NodeType expectedNodeType, final Class<?> jType,
			final int expectedStatusCode,
			final int expectedStartOffset, final int expectedEndOffset,
			final @Nullable String expectedText,
			final int expectedChildCount,
			final DslAstNode actual) {
		assertEquals(expectedNodeType, actual.getNodeType(), "nodeType");
		assertTrue(jType.isInstance(actual));
		assertEquals(expectedStatusCode, actual.getStatusCode());
		assertEquals(expectedStartOffset, actual.getStartOffset(), "startOffset");
		assertEquals(expectedEndOffset, actual.getEndOffset(), "endOffset");
		if (assertText() || Name.class.isAssignableFrom(jType)) {
			assertEquals(expectedText, actual.getText(), "text");
		}
		else if (expectedText != null && !expectedText.isEmpty()) {
			assertNull(actual.getText(), "text");
		}
		assertEquals(expectedChildCount, actual.getChildCount(), "childCount");
	}
	
}