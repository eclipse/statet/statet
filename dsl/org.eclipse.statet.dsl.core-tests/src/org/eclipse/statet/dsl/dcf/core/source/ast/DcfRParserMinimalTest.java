/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.dcf.core.source.DcfLexer;
import org.eclipse.statet.ltk.ast.core.AstInfo;


@NonNullByDefault
public class DcfRParserMinimalTest extends DcfRParserTest {
	
	
	public DcfRParserMinimalTest() {
	}
	
	@Override
	protected DcfRParser createParser() {
		return new DcfRParser(AstInfo.LEVEL_MINIMAL, new DcfLexer());
	}
	
	@Override
	protected boolean assertText() {
		return false;
	}
	
}