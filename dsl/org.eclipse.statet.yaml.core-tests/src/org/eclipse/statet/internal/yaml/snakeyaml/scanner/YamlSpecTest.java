/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.snakeyaml.scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeFalse;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.opentest4j.AssertionFailedError;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.yaml.core.source.ast.YamlParser;


@NonNullByDefault
public class YamlSpecTest {
	
	
	private static final String TEST_SUITE_VERSION= "2022-01-17";
	
	private static final URI TEST_SUITE_ARCHIVE_URI= URI.create(String.format(
			"https://github.com/yaml/yaml-test-suite/archive/refs/tags/data-%s.zip", TEST_SUITE_VERSION ));
	
	private static final Set<String> IGNORE_CASE_IDS= Set.of(
			"5T43", "58MP", "DBG4", "HM87.00", "YJV2", // syntax char meets plain scalar
			"H7J7",
			"DK95.06", // indent
			"VJP3.00",
			"Y79Y.000", "Y79Y.003", "Y79Y.004", "Y79Y.005", "Y79Y.006", "Y79Y.007", "Y79Y.008", "Y79Y.009",
			"-" );
	
	private static final Set<String> IGNORE_ERROR_CASE_IDS= Set.of(
			"QLJ7", "SF5V", // validation of directives / tags
			"9JBA", "CVW2", // comment (?)
			"4EJS", "4HVU", "9C9N", "DK95.01", "DMG6", "G9HC", "N4JP", "QB6E", "U44R", "ZVH3", // indent
			"-" );
	
	
	public static class SpecTestCase {
		
		private final String id;
		private final String label;
		
		final String input;
		final boolean error;
		final List<YamlEvent> expected;
		
		public SpecTestCase(final String id, final String label,
				final String input,
				final boolean error, final List<YamlEvent> expected) {
			this.id= id;
			this.label= label;
			
			this.input= input;
			this.error= error;
			this.expected= expected;
		}
		
		@Override
		public String toString() {
			return String.format("%1$s (label= %2$s)", this.id, this.label);
		}
		
	}
	
	static class YamlEvent {
		
		final char op;
		final String type;
		final @Nullable String sub;
		final ImList<String> properties;
		final @Nullable String value;
		final int status;
		
		
		public YamlEvent(final char op, final String type, final @Nullable String sub,
				final ImList<String> properties, final @Nullable String value,
				final int status) {
			this.op= op;
			this.type= type;
			this.sub= sub;
			this.properties= properties;
			this.value= value;
			this.status= status;
		}
		
		public YamlEvent(final char op, final String type, final @Nullable String sub,
				final ImList<String> properties,
				final int status) {
			this(op, type, sub, properties, "", status);
		}
		
		public YamlEvent(final char op, final String type,
				final ImList<String> properties, final String value,
				final int status) {
			this(op, type, null, properties, value, status);
		}
		
		public YamlEvent(final char op, final String type,
				final ImList<String> properties,
				final int status) {
			this(op, type, null, ImCollections.newList(), "", status);
		}
		
		public YamlEvent(final String s) {
			this.op= s.charAt(0);
			this.type= s.substring(1, 4);
			String sub= null;
			final List<String> properties= new ArrayList<>();
			String value= "";
			if (s.length() >= 5) {
				int sIdx= 5;
				CHECK_PROPS: while (sIdx < s.length()) {
					int pEnd= s.indexOf(' ', sIdx);
					if (pEnd < 0) {
						pEnd= s.length();
					}
					switch (s.charAt(sIdx)) {
					case '[':
					case '{':
						sub= s.substring(sIdx, pEnd);
						sIdx= pEnd + 1;
						continue;
					case '&':
					case '<':
						properties.add(s.substring(sIdx, pEnd));
						sIdx= pEnd + 1;
						continue CHECK_PROPS;
					case ':':
					case '"':
					case '\'':
					default:
						value= s.substring(sIdx);
						break CHECK_PROPS;
					}
				}
			}
			this.sub= sub;
			this.properties= ImCollections.toList(properties);
			this.value= decodeEscapes(value);
			this.status= 0;
		}
		
		
		public @Nullable String getValue() {
			return this.value;
		}
		
		public boolean isError() {
			return ((this.status & ERROR) != 0);
		}
		
		public boolean hasError() {
			return ((this.status & (ERROR | ERROR_IN_CHILD)) != 0);
		}
		
		
		@Override
		public String toString() {
			final StringBuilder sb= new StringBuilder();
			sb.append(this.op);
			sb.append(this.type);
			if (this.sub != null) {
				sb.append(' ').append(this.sub);
			}
			for (final String property : this.properties) {
				sb.append(' ').append(property);
			}
			if (this.value != null && !this.value.isEmpty()) {
				sb.append(' ').append(escapeString(this.value));
			}
			if (this.status != 0) {
				sb.append(String.format("    0x%1$08X", this.status));
			}
			return sb.toString();
		}
		
	}
	
	
	public static List<SpecTestCase> testCases() {
		final List<SpecTestCase> parameters= new ArrayList<>();
		
		loadTestCases(parameters);
		
		return ImCollections.toList(parameters);
	}
	
	
	public YamlSpecTest() {
	}
	
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void testEvents(final SpecTestCase testCase) throws Throwable {
		assumeFalse(IGNORE_CASE_IDS.contains(testCase.id));
		
		List<YamlEvent> actual= ImCollections.emptyList();
		try {
			final YamlParser parser= new YamlParser(AstInfo.LEVEL_MODEL_DEFAULT);
			parser.setScalarText(true);
			final SourceComponent sourceComponent= parser.parseSourceUnit(testCase.input);
			actual= Ast2EventConverter.collect(sourceComponent);
			assertEventsEquals(testCase, actual);
		}
		catch (final Error | RuntimeException | InvocationTargetException e) {
			System.out.println(testCase.id);
			
			final StringBuilder msg= new StringBuilder();
			msg.append("\n").append(testCase.label);
			if (testCase.error) {
				msg.append(" (error= true)");
			}
			msg.append("\n========== input ==========\n");
			msg.append(testCase.input);
			msg.append("\n========== events \n");
			final int count= Math.max(testCase.expected.size(), actual.size());
			for (int i= 0; i < count; i++) {
				msg.append(String.format("%1$-50s %2$s\n",
						(i < testCase.expected.size()) ? testCase.expected.get(i) : "",
						(i < actual.size()) ? actual.get(i) : "" ));
			}
			msg.append("==========\\n");
			e.addSuppressed(new Exception(msg.toString()));
			throw e;
		}
	}
	
	private void assertEventsEquals(final SpecTestCase testCase, final List<YamlEvent> actual) {
		int idx= 0;
		final boolean[] wasError= new boolean[100];
		int depth= 0;
		try {
			while (idx < testCase.expected.size()) {
				final int idx0= idx;
				final YamlEvent expectedEvent= testCase.expected.get(idx);
				assertTrue(idx < actual.size());
				final YamlEvent actualEvent= actual.get(idx);
				
				assertEquals(expectedEvent.op, actualEvent.op,
						() -> String.format("[%1$s].op", idx0) );
				assertEquals(expectedEvent.type, actualEvent.type,
						() -> String.format("[%1$s].type", idx0) );
				
				if (!testCase.error || expectedEvent.sub != null) {
					assertEquals(expectedEvent.sub, actualEvent.sub,
							() -> String.format("[%1$s].sub", idx0) );
				}
				
				final List<String> actProperties= new ArrayList<>(actualEvent.properties);
				assertEquals(expectedEvent.properties.size(), actProperties.size(),
						() -> String.format("[%1$s].properties.size", idx0) );
				for (int expPropIdx= 0; expPropIdx < expectedEvent.properties.size(); expPropIdx++) {
					final int propIdx0= expPropIdx++;
					String expProp= expectedEvent.properties.get(propIdx0);
					if (expProp != null && expProp.startsWith("<")) {
						expProp= "<";
					}
					final int actParamIdx= actProperties.indexOf(expProp);
					if (actParamIdx == -1) {
						assertEquals(expProp, actProperties.get(0),
								() -> String.format("[%1$s].params[%2$s]", idx0, propIdx0) );
					}
				}
				{	final String expValue= expectedEvent.value;
					assertEquals(expValue, actualEvent.value,
							() -> String.format("[%1$s].value", idx0) );
				}
				
				if (!testCase.error) {
					assertEquals(0, actualEvent.status & ERROR,
							() -> String.format("[%1$s].status", idx0 ) );
					assertEquals(0, actualEvent.status & ERROR_IN_CHILD,
							() -> String.format("[%1$s].status", idx0 ) );
				}
				else {
					switch (actualEvent.op) {
					case '+':
						wasError[++depth]= actualEvent.isError();
						break;
					case '=':
						wasError[depth]|= actualEvent.isError();
						break;
					case '-':
						depth--;
					}
				}
				
				idx++;
			}
			
			if (!testCase.error) {
				final int idx0= idx;
				assertEquals(idx0, actual.size(),
						() -> String.format("[%1$s]", idx0) );
				return;
			}
			else {
				final int idx0= idx;
				if (IGNORE_ERROR_CASE_IDS.contains(testCase.id)) {
					return;
				}
				if (wasError[depth]) {
					return;
				}
				while (idx < actual.size()) {
					final YamlEvent actualEvent= actual.get(idx);
					if (actualEvent.hasError()) {
						return;
					}
					if (actualEvent.op == '+' && !actualEvent.type.equals("DOC")) {
						fail();
					}
					idx++;
				}
				fail();
			}
		}
		catch (final AssertionFailedError e) {
			if (testCase.error) {
				if (idx < actual.size()) {
					final YamlEvent actualEvent= actual.get(idx);
					if (idx + 1 == testCase.expected.size()) {
						assertTrue(actualEvent.hasError());
						return;
					}
				}
			}
			throw e;
		}
	}
	
	
	private static @Nullable String escapeString(final String s) {
		int idx= 0;
		CHECK: while (true) {
			if (idx == s.length()) {
				return s;
			}
			final char c= s.charAt(idx);
			switch (c) {
			case '\n':
			case '\r':
			case '\\':
			case '\t':
			case '\b':
				break CHECK;
			default:
				idx++;
				continue;
			}
		}
		final StringBuilder sb= new StringBuilder(s.length() + 4);
		sb.append(s, 0, idx);
		for (; idx < s.length(); idx++) {
			final char c= s.charAt(idx);
			switch (c) {
			case '\n':
				sb.append("\\n");
				continue;
			case '\r':
				sb.append("\\r");
				continue;
			case '\\':
				sb.append("\\\\");
				continue;
			case '\t':
				sb.append("\\t");
				continue;
			case '\b':
				sb.append("\\b");
				continue;
			default:
				sb.append(c);
				continue;
			}
		}
		return sb.toString();
	}
	
	private static String decodeEscapes(final String s) {
		int idx= s.indexOf('\\');
		if (idx == -1) {
			return s;
		}
		final StringBuilder sb= new StringBuilder(s.length());
		int idxWritten= 0;
		do {
			if (idxWritten < idx) {
				sb.append(s, idxWritten, idx);
			}
			idx++;
			if (idx < s.length()) {
				final char c= s.charAt(idx++);
				switch (c) {
				case '\\':
					sb.append(c);
					break;
				case 'r':
					sb.append('\r');
					break;
				case 'n':
					sb.append('\n');
					break;
				case 't':
					sb.append('\t');
					break;
				case 'b':
					sb.append('\b');
					break;
				default:
					break;
				}
			}
			idxWritten= idx;
			idx= s.indexOf('\\', idxWritten);
		} while (idx != -1);
		if (idxWritten < s.length()) {
			sb.append(s, idxWritten, s.length());
		}
		return sb.toString();
	}
	
	
	private static void loadTestCases(final List<SpecTestCase> parameters) {
		try {
			try (final var testCases= Files.newDirectoryStream(provideYamlSpecTestSuite())) {
				for (final Path testPath : testCases) {
					final String id= testPath.getFileName().toString();
					loadTestCase(parameters, testPath, id);
					try (final var subCases= Files.newDirectoryStream(testPath)) {
						for (final Path subPath : subCases) {
							loadTestCase(parameters, subPath,
									id + '.' + subPath.getFileName().toString() );
						}
					}
				}
			}
		}
		catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static void loadTestCase(final List<SpecTestCase> parameters, final Path path,
			final String id) throws IOException {
		if (!Files.exists(path.resolve("==="))) {
			return;
		}
		final String label= Files.readString(path.resolve("===")).trim();
		final String input= Files.readString(path.resolve("in.yaml"));
		final boolean error= Files.exists(path.resolve("error"));
		final List<YamlEvent> expected= Files.readAllLines(path.resolve("test.event"))
				.stream().map((s) -> new YamlEvent(s))
				.collect(Collectors.toList());
		parameters.add(new SpecTestCase(id, label, input, error, expected));
	}
	
	
	private static Path provideYamlSpecTestSuite() throws IOException {
		final Path tmpDir= Paths.get("./tmp");
		if (!Files.isDirectory(tmpDir)) {
			Files.createDirectory(tmpDir);
		}
		final Path specDir= tmpDir.resolve(String.format("test-suite-%s", TEST_SUITE_VERSION));
//		Files.delete(specPath);
		if (!Files.isDirectory(specDir)) {
			final Path archiveFile= specDir.resolveSibling(specDir.getFileName().toString() + ".zip");
			Files.copy(TEST_SUITE_ARCHIVE_URI.toURL().openStream(), archiveFile,
					StandardCopyOption.REPLACE_EXISTING );
			final FileSystem fs= FileSystems.newFileSystem(
					URI.create("jar:" + archiveFile.toUri().toString()), Map.of());
			try (fs) {
				final Matcher caseIdMatcher= Pattern.compile("[\\p{Upper}\\p{Digit}]{4}").matcher("");
				final Matcher subIdMatcher= Pattern.compile("[\\p{Digit}]{1,}").matcher("");
				final Set<String> caseTestFiles= Set.of("===", "in.yaml", "error", "test.event");
				Files.walkFileTree(fs.getPath("/"), new SimpleFileVisitor<>() {
					@Nullable Path targetDir;
					public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
						String id;
						switch (dir.getNameCount()) {
						case 0:
							return FileVisitResult.CONTINUE;
						case 1:
							this.targetDir= specDir;
							return FileVisitResult.CONTINUE;
						case 2:
							id= dir.getFileName().toString();
							if (caseIdMatcher.reset(id).matches()) {
								this.targetDir= Files.createDirectories(this.targetDir.resolve(id));
								return FileVisitResult.CONTINUE;
							}
							return FileVisitResult.SKIP_SUBTREE;
						case 3:
							id= dir.getFileName().toString();
							if (subIdMatcher.reset(id).matches()) {
								this.targetDir= Files.createDirectories(this.targetDir.resolve(id));
								return FileVisitResult.CONTINUE;
							}
							return FileVisitResult.SKIP_SUBTREE;
						default:
							throw new IllegalStateException();
						}
					}
					public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
						this.targetDir= (dir.getNameCount() > 1) ? this.targetDir.getParent() : null;
						return FileVisitResult.CONTINUE;
					}
					public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
						Files.copy(file, this.targetDir.resolve(file.getFileName().toString()));
						return FileVisitResult.CONTINUE;
					}
				});
			}
		}
		return specDir;
	}
	
}
