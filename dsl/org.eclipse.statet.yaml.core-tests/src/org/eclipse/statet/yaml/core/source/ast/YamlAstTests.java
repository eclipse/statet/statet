/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;


@NonNullByDefault
public class YamlAstTests {
	
	
	public static void assertNoChildren(final DslAstNode node) {
		assertEquals(0, node.getChildCount(), "childCount");
		assertFalse(node.hasChildren(), "hasChildren");
	}
	
	public static void assertChildren(final int expectedCount, final DslAstNode node) {
		assertEquals(expectedCount, node.getChildCount(), "childCount");
		assertTrue(node.hasChildren(), "hasChildren");
	}
	
	public static void assertRegion(final int expectedOffset, final int expectedLength,
			final TextRegion actual) {
		assertEquals(expectedOffset, actual.getStartOffset(), "startOffset");
		assertEquals(expectedOffset +  expectedLength, actual.getEndOffset(), "endOffset");
	}
	
	
	private YamlAstTests() {
	}
	
}
