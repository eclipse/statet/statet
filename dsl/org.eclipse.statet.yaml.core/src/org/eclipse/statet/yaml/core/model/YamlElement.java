/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.model.DslElementName;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;


@NonNullByDefault
public interface YamlElement<TModelChild extends YamlElement<?>>
		extends LtkModelElement<TModelChild> {
	
	
	static int C12_DOC=             C1_CLASS | 0x1 << SHIFT_C2;
	
	static int C1_COLLECTION=   0xA << SHIFT_C1;
	static int C12_SEQ=             C1_COLLECTION | 0x0 << SHIFT_C2;
	static int C12_MAP=             C1_COLLECTION | 0x1 << SHIFT_C2;
	
	static int C1_SCALAR=       0xB << SHIFT_C1;
	
	static int C1_ALIAS=        0xE << SHIFT_C1;
	
	static int C1_DUMMY=        0xF << SHIFT_C1;
	
	
	@Override
	DslElementName getElementName();
	
	
}
