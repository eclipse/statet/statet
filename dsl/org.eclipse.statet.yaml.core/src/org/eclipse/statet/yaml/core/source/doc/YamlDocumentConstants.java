/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.doc;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;


@NonNullByDefault
public interface YamlDocumentConstants {
	
	
	/**
	 * The id of partitioning of YAML documents.
	 */
	String YAML_PARTITIONING= "org.eclipse.statet.Yaml"; //$NON-NLS-1$
	
	String YAML_DEFAULT_CONTENT_TYPE= "Yaml.Default"; //$NON-NLS-1$
	String YAML_COMMENT_CONTENT_TYPE= "Yaml.Comment"; //$NON-NLS-1$
	String YAML_DIRECTIVE_CONTENT_TYPE= "Yaml.Directive"; //$NON-NLS-1$
	String YAML_KEY_CONTENT_TYPE= "Yaml.Key"; //$NON-NLS-1$
	String YAML_TAG_CONTENT_TYPE= "Yaml.Tag"; //$NON-NLS-1$
	String YAML_VALUE_CONTENT_TYPE= "Yaml.Value"; //$NON-NLS-1$
	
	
	/**
	 * List with all partition content types of YAML documents.
	 */
	ImList<String> YAML_CONTENT_TYPES= ImCollections.newList(
			YAML_DEFAULT_CONTENT_TYPE,
			YAML_COMMENT_CONTENT_TYPE,
			YAML_DIRECTIVE_CONTENT_TYPE,
			YAML_KEY_CONTENT_TYPE,
			YAML_TAG_CONTENT_TYPE,
			YAML_VALUE_CONTENT_TYPE );
	
	
	PartitionConstraint YAML_DEFAULT_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
			return (contentType == YAML_DEFAULT_CONTENT_TYPE);
		}
	};
	
	PartitionConstraint YAML_ANY_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
			return (contentType.startsWith("Yaml.")); //$NON-NLS-1$
		}
	};
	
}
