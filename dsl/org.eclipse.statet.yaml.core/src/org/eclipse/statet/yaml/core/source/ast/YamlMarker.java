/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.Marker;


@NonNullByDefault
public class YamlMarker extends Marker {
	
	
	static final class DirectivesEnd extends YamlMarker {
		
		DirectivesEnd(final YamlDocument parent, final int startOffset, final int endOffset) {
			super(parent);
			doSetStartEndOffset(startOffset, endOffset);
		}
		
		
		@Override
		public String getText() {
			return "---"; //$NON-NLS-1$
		}
		
	}
	
	static final class DocumentEnd extends YamlMarker {
		
		DocumentEnd(final DslAstNode parent, final int startOffset, final int endOffset) {
			super(parent);
			doSetStartEndOffset(startOffset, endOffset);
		}
		
		
		@Override
		public String getText() {
			return "..."; //$NON-NLS-1$
		}
		
	}
	
	
	YamlMarker(final DslAstNode parent) {
		super(parent);
	}
	
	
}
