/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.model.DslElementName;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.internal.yaml.core.model.ContainerSourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public class YamlChunkElement extends ContainerSourceElement implements YamlSourceElement {
	
	
	private final SourceStructElement<?, ?> parent;
	
	
	public YamlChunkElement(final SourceStructElement<?, ?> parent,
			final SourceComponent astNode,
			final DslElementName name, final int occurrenceCount) {
		super(astNode);
		this.parent= parent;
		this.name= name;
		this.occurrenceCount= occurrenceCount;
	}
	
	
	@Override
	public int getElementType() {
		return YamlElement.C12_SOURCE_CHUNK;
	}
	
	@Override
	public SourceUnit getSourceUnit() {
		return this.parent.getSourceUnit();
	}
	
	@Override
	public boolean isReadOnly() {
		return this.parent.isReadOnly();
	}
	
	@Override
	public boolean exists() {
		return this.parent.exists();
	}
	
	@Override
	public @Nullable ContainerSourceElement getModelParent() {
		return null;
	}
	
	@Override
	public SourceStructElement<?, ?> getSourceParent() {
		return this.parent;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == SourceComponent.class) {
			return (T)this.astNode;
		}
		return super.getAdapter(adapterType);
	}
	
}
