/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model;

import org.eclipse.core.resources.IFile;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.impl.GenericResourceSourceUnit;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentContentInfo;


@NonNullByDefault
public class YamlResourceSourceUnit extends GenericResourceSourceUnit implements SourceUnit {
	
	
	public YamlResourceSourceUnit(final String id, final IFile file) {
		super(id, file);
	}
	
	
	@Override
	public String getModelTypeId() {
		return YamlModel.YAML_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return YamlDocumentContentInfo.INSTANCE;
	}
	
}
