/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.Tag;


@NonNullByDefault
public final class YamlTag extends Tag {
	
	
	private final @Nullable String handle;
	
	
	YamlTag(final DslAstNode parent, final int startOffset, final int endOffset,
			final @Nullable String handle, final String suffix) {
		super(parent, suffix);
		doSetStartEndOffset(startOffset, endOffset);
		
		this.handle= handle;
	}
	
	
	public @Nullable String getHandle() {
		return this.handle;
	}
	
	@SuppressWarnings("null")
	public String getSuffix() {
		return getText();
	}
	
	
}
