/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.util;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.CharPairSet;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.text.core.HeuristicTokenScanner;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentConstants;


@NonNullByDefault
public class YamlHeuristicTokenScanner extends HeuristicTokenScanner {
	
	
	public static final CharPairSet YAML_BRACKETS= new CharPairSet(
			ImCollections.newIdentityList(CURLY_BRACKETS, SQUARE_BRACKETS),
			'\\' );
	
	
	public static YamlHeuristicTokenScanner create(final DocContentSections documentContentInfo) {
		return (documentContentInfo.getPrimaryType() == YamlDocumentConstants.YAML_PARTITIONING) ?
				new YamlHeuristicTokenScanner(documentContentInfo) :
				new YamlChunkHeuristicTokenScanner(documentContentInfo);
	}
	
	
	protected YamlHeuristicTokenScanner(final DocContentSections documentContentInfo) {
		super(documentContentInfo, YamlDocumentConstants.YAML_DEFAULT_CONTENT_CONSTRAINT);
	}
	
	
	@Override
	public CharPairSet getDefaultBrackets() {
		return YAML_BRACKETS;
	}
	
}
