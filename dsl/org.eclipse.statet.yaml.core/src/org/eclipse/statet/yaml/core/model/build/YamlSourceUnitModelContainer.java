/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model.build;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;


@NonNullByDefault
public class YamlSourceUnitModelContainer
		extends SourceUnitModelContainer<SourceUnit, YamlSourceUnitModelInfo> {
	
	
	public YamlSourceUnitModelContainer(final SourceUnit unit,
			final @Nullable SourceUnitIssueSupport issueSupport) {
		super(unit, issueSupport);
	}
	
	
	@Override
	public Class<?> getAdapterClass() {
		return YamlSourceUnitModelContainer.class;
	}
	
	@Override
	public boolean isContainerFor(final String modelTypeId) {
		return (modelTypeId == YamlModel.YAML_TYPE_ID);
	}
	
	@Override
	protected ModelManager getModelManager() {
		return YamlModel.getYamlModelManager();
	}
	
}
