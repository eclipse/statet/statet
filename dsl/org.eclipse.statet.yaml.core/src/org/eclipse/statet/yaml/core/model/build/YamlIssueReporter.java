/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model.build;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.internal.yaml.core.model.AstProblemReporter;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.model.core.build.BasicIssueReporter;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.yaml.core.model.YamlCompositeSourceElement;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.YamlSourceElement;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;


@NonNullByDefault
public class YamlIssueReporter extends BasicIssueReporter<SourceUnit, YamlSourceUnitModelInfo> {
	
	
	private final AstProblemReporter syntaxProblemReporter= new AstProblemReporter();
	
	private final YamlTaskTagReporter taskReporter= new YamlTaskTagReporter();
	
	
	public YamlIssueReporter() {
		super(YamlModel.YAML_TYPE_ID);
	}
	
	
	@Override
	protected YamlTaskTagReporter getTaskReporter() {
		return this.taskReporter;
	}
	
	
	@Override
	protected void runReporters(final SourceUnit sourceUnit,
			final YamlSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		final YamlSourceElement element= modelInfo.getSourceElement();
		if (element instanceof YamlCompositeSourceElement) {
			final var elements= ((YamlCompositeSourceElement)element).getCompositeElements();
			for (final var yamlChunk : elements) {
				runReporters(sourceUnit, yamlChunk.getAdapter(AstNode.class),
						modelInfo, content, requestor, level );
			}
		}
		else {
			runReporters(sourceUnit, element.getAdapter(AstNode.class),
					modelInfo, content, requestor, level );
		}
	}
	
	
	@Override
	protected void runReporters(final SourceUnit sourceUnit, final @Nullable AstNode node,
			final @Nullable YamlSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (node == null) {
			return;
		}
		if (node instanceof DslAstNode) {
			runReporters(sourceUnit, (DslAstNode)node, modelInfo,
					content, requestor, level );
		}
	}
	
	private void runReporters(final SourceUnit sourceUnit, final DslAstNode node,
			final @Nullable YamlSourceUnitModelInfo modelInfo, final SourceContent content,
			final IssueRequestor requestor, final int level) {
		if (shouldReportProblems()) {
			this.syntaxProblemReporter.run(node, content, requestor);
		}
		if (shouldReportTasks()) {
			this.taskReporter.run(sourceUnit, node, content, requestor);
		}
	}
	
}
