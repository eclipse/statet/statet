/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.doc;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.BasicDocContentSections;


@NonNullByDefault
public class YamlDocumentContentInfo extends BasicDocContentSections {
	
	
	public static final String YAML=                        YamlDocumentConstants.YAML_PARTITIONING;
	
	
	public static final YamlDocumentContentInfo INSTANCE= new YamlDocumentContentInfo();
	
	
	public YamlDocumentContentInfo() {
		super(YamlDocumentConstants.YAML_PARTITIONING, YAML);
	}
	
	
	@Override
	public String getTypeByPartition(final String contentType) {
		return YAML;
	}
	
}
