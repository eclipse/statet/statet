/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.refactoring;

import static org.eclipse.statet.ltk.model.core.element.LtkModelElement.MASK_C12;

import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.yaml.core.YamlCore;
import org.eclipse.statet.yaml.core.model.YamlElement;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentConstants;
import org.eclipse.statet.yaml.core.source.util.YamlHeuristicTokenScanner;


public class YamlRefactoringAdapter extends RefactoringAdapter {
	
	
	public YamlRefactoringAdapter() {
		super(YamlModel.YAML_TYPE_ID);
	}
	
	
	@Override
	public String getPluginIdentifier() {
		return YamlCore.BUNDLE_ID;
	}
	
	@Override
	public YamlHeuristicTokenScanner getScanner(final SourceUnit su) {
		return YamlHeuristicTokenScanner.create(su.getDocumentContentInfo());
	}
	
	@Override
	public boolean isCommentContent(final ITypedRegion partition) {
		return (partition != null
				&& partition.getType() == YamlDocumentConstants.YAML_COMMENT_CONTENT_TYPE );
	}
	
	@Override
	protected boolean shouldAppendEmptyLine(final SourceElement element) {
		return (element.getModelTypeId() == YamlModel.YAML_TYPE_ID
				&& (element.getElementType() & MASK_C12) == YamlElement.C12_DOC);
	}
	
}
