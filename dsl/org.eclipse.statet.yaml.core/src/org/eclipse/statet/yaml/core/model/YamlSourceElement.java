/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;


@NonNullByDefault
public interface YamlSourceElement
		extends YamlElement<YamlSourceElement>,
				SourceStructElement<YamlSourceElement, SourceStructElement<?, ?>> {
	
	
	@Override
	@Nullable YamlElement<?> getModelParent();
	@Override
	boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super YamlSourceElement> filter);
	@Override
	List<? extends YamlSourceElement> getModelChildren(final @Nullable LtkModelElementFilter<? super YamlSourceElement> filter);
	
	
}
