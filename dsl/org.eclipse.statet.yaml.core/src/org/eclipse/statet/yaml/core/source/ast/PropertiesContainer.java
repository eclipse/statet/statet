/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nullable;

import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;

import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;


@NonNullByDefault
final class PropertiesContainer extends org.eclipse.statet.dsl.core.source.ast.PropertiesContainer {
	
	
	public PropertiesContainer(final DslAstNode parent, final int startOffset, final int endOffset) {
		super(parent, startOffset, endOffset);
	}
	
	@Override
	protected void finish(final int endOffset) {
		final ImIdentityList<DslAstNode> properties= getProperties();
		boolean hasAnchor= false;
		boolean hasTag= false;
		for (final DslAstNode property : properties) {
			switch (property.getNodeType()) {
			case ANCHOR:
				if (!hasAnchor) {
					hasAnchor= true;
				}
				else {
					setStatusThis(property, TYPE12_SYNTAX_TOKEN_UNEXPECTED);
				}
				continue;
			case TAG:
				if (!hasTag) {
					hasTag= true;
				}
				else {
					setStatusThis(property, TYPE12_SYNTAX_TOKEN_UNEXPECTED);
				}
				continue;
			default:
				throw new RuntimeException();
			}
		}
		
		DslAstNode nodeChild= nullable(getNode());
		if (nodeChild == null) {
			nodeChild= new YamlScalar.Plain(this,
					(!properties.isEmpty()) ?
							properties.getLast().getEndOffset() :
							getEndOffset() );
			doSetNodeChild(nodeChild);
		}
		else if (nodeChild.getStatusCode() == 0) {
			switch (nodeChild.getNodeType()) {
			case ALIAS:
				if (hasAnchor) {
					setStatusThis(nodeChild, TYPE12_SYNTAX_TOKEN_UNEXPECTED);
				}
				break;
			default:
				break;
			}
		}
		
		super.finish(endOffset);
	}
	
	
}
