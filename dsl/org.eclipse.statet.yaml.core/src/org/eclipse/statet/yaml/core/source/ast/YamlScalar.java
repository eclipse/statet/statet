/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.Scalar;


@NonNullByDefault
abstract class YamlScalar extends Scalar {
	
	
	static class DQuoted extends YamlScalar {
		
		DQuoted(final DslAstNode parent, final int startOffset, final int endOffset,
				final @Nullable String value) {
			super(parent);
			doSetStartEndOffset(startOffset, endOffset);
			doSetText(value);
		}
		
		@Override
		public char getOperator() {
			return '\"';
		}
		
	}
	
	static class SQuoated extends YamlScalar {
		
		SQuoated(final DslAstNode parent, final int startOffset, final int endOffset,
				final @Nullable String value) {
			super(parent);
			doSetStartEndOffset(startOffset, endOffset);
			doSetText(value);
		}
		
		@Override
		public char getOperator() {
			return '\'';
		}
		
	}
	
	static class Literal extends YamlScalar {
		
		Literal(final DslAstNode parent, final int startOffset, final int endOffset,
				final @Nullable String value) {
			super(parent);
			doSetStartEndOffset(startOffset, endOffset);
			doSetText(value);
		}
		
		@Override
		public char getOperator() {
			return '|';
		}
		
	}
	
	static class Folded extends YamlScalar {
		
		Folded(final DslAstNode parent, final int startOffset, final int endOffset,
				final @Nullable String value) {
			super(parent);
			doSetStartEndOffset(startOffset, endOffset);
			doSetText(value);
		}
		
		@Override
		public char getOperator() {
			return '>';
		}
		
	}
	
	static class Plain extends YamlScalar {
		
		Plain(final DslAstNode parent, final int startOffset, final int endOffset,
				final @Nullable String value) {
			super(parent);
			doSetStartEndOffset(startOffset, endOffset);
			doSetText(value);
		}
		
		Plain(final DslAstNode parent, final int offset) {
			super(parent);
			doSetStartEndOffset(offset);
		}
		
		@Override
		public char getOperator() {
			return 0;
		}
		
	}
	
	
	YamlScalar(final DslAstNode parent) {
		super(parent);
	}
	
	
}
