/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_DOC_CONTENT;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_NODE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;

import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.DslParser;
import org.eclipse.statet.dsl.core.source.ast.Dummy;
import org.eclipse.statet.dsl.core.source.ast.NodeType;
import org.eclipse.statet.dsl.core.source.ast.Record;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;


@NonNullByDefault
public class YamlDocument extends Record {
	
	
	ImIdentityList<DslAstNode> directiveChildren= NO_CHILDREN;
	@Nullable DslAstNode directivesEndChild;
	
	ImIdentityList<DslAstNode> contentChildren= NO_CHILDREN;
	
	@Nullable DslAstNode endChild;
	
	
	YamlDocument(final SourceComponent parent, final int offset) {
		super(parent);
		doSetStartEndOffset(offset);
	}
	
	@Override
	protected void finish(final int endOffset, final DslParser.NContainerBuilder containerBuilder) {
		int directiveEndIdx= 0;
		int contentStartIdx;
		int contentEndIdx;
		{	final var children= containerBuilder.children;
			while (directiveEndIdx < children.size()
					&& children.get(directiveEndIdx).getNodeType() == NodeType.DIRECTIVE) {
				directiveEndIdx++;
			}
			
			contentStartIdx= directiveEndIdx;
			if (this.directivesEndChild != null) {
				contentStartIdx++;
			}
			
			contentEndIdx= children.size();
			if (this.endChild != null) {
				contentEndIdx--;
			}
			
			if (contentStartIdx == contentEndIdx && this.directivesEndChild == null) {
				final Dummy dummy= new Dummy(TYPE12_SYNTAX_MISSING_NODE | CTX12_DOC_CONTENT,
						this, (this.endChild != null) ?
								this.endChild.getStartOffset() : getEndOffset() );
				children.add(contentStartIdx, dummy);
				contentEndIdx++;
			}
		}
		
		super.finish(endOffset, containerBuilder);
		final ImIdentityList<DslAstNode> children= getChildren();
		this.directiveChildren= children.subList(0, directiveEndIdx);
		this.contentChildren= children.subList(contentStartIdx, contentEndIdx);
		
		if (contentEndIdx - contentStartIdx > 1) {
			final var child= children.get(contentStartIdx + 1);
			setStatusThis(child, TYPE12_SYNTAX_TOKEN_UNEXPECTED);
		}
	}
	
	
	@Override
	public final ImIdentityList<DslAstNode> getDirectives() {
		return this.directiveChildren;
	}
	
	@Override
	public final @Nullable DslAstNode getDirectivesEndMarker() {
		return this.directivesEndChild;
	}
	
	@Override
	public final ImIdentityList<DslAstNode> getContentNodes() {
		return this.contentChildren;
	}
	
	@Override
	public @Nullable DslAstNode getEndMarker() {
		return this.endChild;
	}
	
}
