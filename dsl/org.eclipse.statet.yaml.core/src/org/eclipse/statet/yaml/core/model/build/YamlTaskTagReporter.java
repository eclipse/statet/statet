/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model.build;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.Comment;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.impl.TaskTagReporter;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public class YamlTaskTagReporter extends TaskTagReporter {
	
	
	public YamlTaskTagReporter() {
	}
	
	
	public void run(final SourceUnit sourceUnit, final DslAstNode node,
			final SourceContent sourceContent,
			final IssueRequestor requestor) {
		if (node instanceof SourceComponent) {
			setup(sourceContent, requestor);
			final var comments= nonNullAssert(((SourceComponent)node).getComments());
			for (final Comment comment : comments) {
				checkForTasks(comment.getStartOffset() + 1, comment.getEndOffset());
			}
		}
	}
	
}
