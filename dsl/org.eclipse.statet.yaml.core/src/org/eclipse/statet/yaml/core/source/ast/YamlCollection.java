/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_BLOCK;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_FLOW;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SEQ_BLOCK;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SEQ_FLOW;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_COLLECTION_NOT_CLOSED;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_SEP_COMMA_MISSING;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.DslParser;
import org.eclipse.statet.dsl.core.source.ast.NodeType;


@NonNullByDefault
public abstract class YamlCollection extends org.eclipse.statet.dsl.core.source.ast.Collection {
	
	
	public static abstract class FlowCollection extends YamlCollection {
		
		
		private ImIntList sepOffsets;
		
		int closeIndicatorOffset= NA_OFFSET;
		
		
		@SuppressWarnings("null")
		FlowCollection(final DslAstNode parent) {
			super(parent);
		}
		
		@Override
		protected void add(final DslParser.NContainerBuilder containerBuilder, final DslAstNode child) {
			if (containerBuilder.sepOffsets.size() < containerBuilder.children.size()) {
				containerBuilder.sepOffsets.add(NA_OFFSET);
				if (getStatusCode() == 0) {
					doSetStatusCode(TYPE12_SYNTAX_SEP_COMMA_MISSING | getNodeCode());
				}
			}
			super.add(containerBuilder, child);
		}
		
		@Override
		protected void finish(final int endOffset, final DslParser.NContainerBuilder containerBuilder) {
			if (getStatusCode() == 0 && this.closeIndicatorOffset == NA_OFFSET) {
				doSetStatusCode(TYPE12_SYNTAX_COLLECTION_NOT_CLOSED | getNodeCode());
			}
			
			super.finish(endOffset, containerBuilder);
			
			this.sepOffsets= ImCollections.toIntList(containerBuilder.sepOffsets);
			if (!this.sepOffsets.isEmpty()) {
				doSetEndOffsetMin(this.sepOffsets.getLast() + 1);
			}
		}
		
		
		@Override
		public int getOpenIndicatorOffset() {
			return getStartOffset();
		}
		
		public ImIntList getSepOffsets() {
			return this.sepOffsets;
		}
		
		@Override
		public int getCloseIndicatorOffset() {
			return this.closeIndicatorOffset;
		}
		
		
	}
	
	static final class FlowSeq extends FlowCollection {
		
		
		FlowSeq(final DslAstNode parent, final int offset, final int endOffset) {
			super(parent);
			doSetStartEndOffset(offset, endOffset);
		}
		
		
		@Override
		public NodeType getNodeType() {
			return NodeType.SEQ;
		}
		
		@Override
		public char getOperator() {
			return '[';
		}
		
		@Override
		int getNodeCode() {
			return CTX12_SEQ_FLOW;
		}
		
	}
	
	static final class FlowMap extends FlowCollection {
		
		
		FlowMap(final DslAstNode parent, final int offset, final int endOffset) {
			super(parent);
			doSetStartEndOffset(offset, endOffset);
		}
		
		
		@Override
		public NodeType getNodeType() {
			return NodeType.MAP;
		}
		
		@Override
		public char getOperator() {
			return '{';
		}
		
		@Override
		int getNodeCode() {
			return CTX12_MAP_FLOW;
		}
		
	}
	
	public static abstract class BlockCollection extends YamlCollection {
		
		
		BlockCollection(final DslAstNode parent) {
			super(parent);
		}
		
		
		@Override
		public int getOpenIndicatorOffset() {
			return NA_OFFSET;
		}
		
		@Override
		public int getCloseIndicatorOffset() {
			return NA_OFFSET;
		}
		
		
	}
	
	static final class BlockSeq extends BlockCollection {
		
		
		BlockSeq(final DslAstNode parent, final int offset, final int endOffset) {
			super(parent);
			doSetStartEndOffset(offset, endOffset);
		}
		
		
		@Override
		public NodeType getNodeType() {
			return NodeType.SEQ;
		}
		
		@Override
		public char getOperator() {
			return '-';
		}
		
		@Override
		int getNodeCode() {
			return CTX12_SEQ_BLOCK;
		}
		
	}
	
	static final class BlockMap extends BlockCollection {
		
		
		BlockMap(final DslAstNode parent, final int offset, final int endOffset) {
			super(parent);
			doSetStartEndOffset(offset, endOffset);
		}
		
		
		@Override
		public NodeType getNodeType() {
			return NodeType.MAP;
		}
		
		@Override
		public char getOperator() {
			return '?'; // symbolic
		}
		
		@Override
		int getNodeCode() {
			return CTX12_MAP_BLOCK;
		}
		
	}
	
	
	private YamlCollection(final DslAstNode parent) {
		super(parent);
	}
	
	
	abstract int getNodeCode();
	
	
}
