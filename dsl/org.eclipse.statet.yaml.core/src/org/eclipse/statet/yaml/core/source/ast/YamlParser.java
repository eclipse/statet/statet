/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_DIRECTIVES_END_MARKER;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_ENTRY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_KEY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_VALUE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SEQ_ENTRY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_INDICATOR;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_MARKER;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_NODE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.Comment;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.DslParser;
import org.eclipse.statet.dsl.core.source.ast.Dummy;
import org.eclipse.statet.dsl.core.source.ast.NodeType;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.internal.yaml.snakeyaml.scanner.ScannerImpl;
import org.eclipse.statet.internal.yaml.snakeyaml.scanner.SyntaxProblem;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.AliasToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.AnchorToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.CommentType;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.DirectiveToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.ScalarToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.TagToken;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.TagTuple;
import org.eclipse.statet.internal.yaml.snakeyaml.tokens.Token;
import org.eclipse.statet.ltk.core.StatusCodes;


@NonNullByDefault
public class YamlParser extends DslParser {
	
	
	public static final int COLLECT_COMMENTS= 1 << 0;
	
	
	private final ScannerImpl lexer= new ScannerImpl(true, false, false) {
		@Override
		protected void handleComment(final CommentType type, final int startIndex, final int endIndex) {
			if (getCommentLevel() != 0) {
				addComment(new Comment(startIndex, endIndex));
			}
		}
	};
	
	
	public YamlParser(final int level) {
		super(level);
	}
	
	
	@Override
	public String getSyntaxLabel() {
		return "YAML"; //$NON-NLS-1$
	}
	
	
	public void setScalarText(final boolean create) {
		this.lexer.setCreateContentText(create);
	}
	
	
	@Override
	protected void initTask() {
		super.initTask();
		this.lexer.reset(getParseInput());
	}
	
	
	@Override
	protected boolean isNodeProperty(final DslAstNode node) {
		return YamlAsts.PROPERTY_TYPES.contains(node.getNodeType());
	}
	
	
	@Override
	protected void exit(final int offset) {
		super.exit(offset);
		
		if (getDepth() == 1) {
			this.lexer.resetToRoot();
		}
	}
	
	private boolean exitTo1(final Class<?> type1, final Class<?> type2) {
		while (getDepth() > 1) {
			final DslAstNode currentNode= getCurrentNode();
			if (currentNode.getClass() == type1 || currentNode.getClass() == type2) {
				return true;
			}
			if (currentNode.getNodeType() == NodeType.KEY_VALUE_ENTRY) {
				exit(NA_OFFSET);
				continue;
			}
			break;
		}
		int checkDepth= getDepth() - 1;
		DslAstNode node= getCurrentNode().getDslParent();
		while (checkDepth > 1) {
			if (node.getClass() == type1 || node.getClass() == type2) {
				while (getDepth() > checkDepth) {
					exit(NA_OFFSET);
				}
				return true;
			}
			checkDepth--;
			node= node.getDslParent();
		}
		return false;
	}
	
	private void ensureRecordContent(final int startOffset) {
		YamlDocument recordNode;
		switch (getRecordState()) {
		case RECORD_NONE:
			recordNode= new YamlDocument((SourceComponent)getCurrentNode(), startOffset);
			enterNode(recordNode);
			beginRecordContent(recordNode);
			return;
		case RECORD_DIRECTIVES:
			recordNode= (YamlDocument)getCurrentNode();
			if (!getCurrentContainerBuilder().children.isEmpty()) {
				final var marker= new Dummy(TYPE12_SYNTAX_MISSING_MARKER | CTX12_DIRECTIVES_END_MARKER,
						recordNode, startOffset );
				addChildTerm(marker);
				recordNode.directivesEndChild= marker;
			}
			beginRecordContent(recordNode);
			return;
		default:
			return;
		}
	}
	
	private DslAstNode checkParent(final Token token) {
		ensureRecordContent(token.getStartIndex());
		final DslAstNode currentNode= getCurrentNode();
		switch (currentNode.getNodeType()) {
		case SEQ: if (currentNode.getOperator() == '-') {
				final var seqEntry= new SeqEntry(
						TYPE12_SYNTAX_MISSING_INDICATOR | CTX12_SEQ_ENTRY,
						currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(seqEntry);
				return seqEntry;
			}
			return currentNode;
		case MAP: {
				final var mapEntry= new MapEntry(
						(currentNode.getOperator() == '?') ? (TYPE12_SYNTAX_MISSING_INDICATOR | CTX12_MAP_KEY) : 0,
						currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(mapEntry);
				return mapEntry;
			}
		default:
			return currentNode;
		}
	}
	
	private void addChildTerm(final DslAstNode node) {
		addChild(node);
		
		checkExit();
	}
	
	
	@Override
	protected void parseInput(final SourceComponent sourceNode) {
		while (true) {
			final Token token= this.lexer.nextToken();
			if (token == null) {
				break;
			}
			
			switch (token.getTokenId()) {
			case StreamStart:
			case StreamEnd:
				continue;
			
			case Directive: {
				DslAstNode currentNode= getCurrentNode();
				YamlDocument docNode;
				switch (getRecordState()) {
				case RECORD_NONE:
					docNode= new YamlDocument((SourceComponent)currentNode, token.getStartIndex());
					enterNode(docNode);
					beginRecordDirectives(docNode);
					break;
				case RECORD_DIRECTIVES:
					docNode= (YamlDocument)currentNode;
					break;
				default:
					currentNode= exitToSourceComponent(token.getStartIndex());
					docNode= null;
					break;
				}
				final YamlDirective node= new YamlDirective(currentNode,
						token.getStartIndex(), token.getEndIndex(),
						((DirectiveToken<?>)token).getName() );
				addChildTerm(node);
				if (docNode != null) {
					attachProblem(node, token);
				}
				else {
					setStatus(node, TYPE12_SYNTAX_TOKEN_UNEXPECTED, null);
				}
				continue;
			}
			case DocumentStart: {
				DslAstNode currentNode= getCurrentNode();
				YamlDocument docNode;
				switch (getRecordState()) {
				case RECORD_DIRECTIVES:
					docNode= (YamlDocument)currentNode;
					break;
				default:
					currentNode= exitToSourceComponent(token.getStartIndex());
					docNode= new YamlDocument((SourceComponent)currentNode, token.getStartIndex());
					enterNode(docNode);
					break;
				}
				final var marker= new YamlMarker.DirectivesEnd(docNode,
						token.getStartIndex(), token.getEndIndex() );
				addChildTerm(marker);
				attachProblem(marker, token);
				docNode.directivesEndChild= marker;
				beginRecordContent(docNode);
				continue;
			}
			case DocumentEnd: {
				DslAstNode currentNode= getCurrentNode();
				YamlDocument docNode;
				switch (getRecordState()) {
				case RECORD_NONE:
					docNode= null;
					break;
				case RECORD_DIRECTIVES:
					docNode= (YamlDocument)currentNode;
					break;
				default:
					currentNode= exitToRecord();
					docNode= (YamlDocument)currentNode;
					break;
				}
				final var marker= new YamlMarker.DocumentEnd(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				addChildTerm(marker);
				attachProblem(marker, token);
				if (docNode != null) {
					docNode.endChild= marker;
					endRecord(docNode, marker.getEndOffset());
				}
				continue;
			}
			
			case BlockSequenceStart: {
				ensureRecordContent(token.getStartIndex());
				final DslAstNode currentNode= getCurrentNode();
				final YamlCollection.BlockSeq node= new YamlCollection.BlockSeq(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case BlockMappingStart: {
				ensureRecordContent(token.getStartIndex());
				final DslAstNode currentNode= getCurrentNode();
				final YamlCollection.BlockMap node= new YamlCollection.BlockMap(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case BlockEnd: {
				final boolean found= exitTo(YamlCollection.BlockSeq.class, YamlCollection.BlockMap.class);
				if (found) {
					exit(token.getEndIndex());
				}
				continue;
			}
			case FlowSequenceStart: {
				ensureRecordContent(token.getStartIndex());
				final DslAstNode currentNode= getCurrentNode();
				final YamlCollection.FlowCollection node= new YamlCollection.FlowSeq(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case FlowSequenceEnd: {
				final boolean found= exitTo(YamlCollection.FlowSeq.class);
				final DslAstNode currentNode= getCurrentNode();
				final YamlCollection.FlowSeq yamlCollection;
				if (found) {
					yamlCollection= (YamlCollection.FlowSeq) currentNode;
					yamlCollection.closeIndicatorOffset= token.getStartIndex();
					exit(token.getEndIndex());
				}
				else {
					addChildTerm(new Dummy(TYPE12_SYNTAX_TOKEN_UNEXPECTED, currentNode,
							token.getStartIndex(), token.getEndIndex() ));
				}
				continue;
			}
			case FlowMappingStart: {
				ensureRecordContent(token.getStartIndex());
				final DslAstNode currentNode= getCurrentNode();
				final YamlCollection.FlowCollection node= new YamlCollection.FlowMap(currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case FlowMappingEnd: {
				final boolean found= exitTo(YamlCollection.FlowMap.class);
				final DslAstNode currentNode= getCurrentNode();
				final YamlCollection.FlowMap node;
				if (found) {
					node= (YamlCollection.FlowMap) currentNode;
					node.closeIndicatorOffset= token.getStartIndex();
					exit(token.getEndIndex());
				}
				else {
					addChildTerm(new Dummy(TYPE12_SYNTAX_TOKEN_UNEXPECTED, currentNode,
							token.getStartIndex(), token.getEndIndex() ));
				}
				continue;
			}
			
			case BlockEntry: {
				ensureRecordContent(token.getStartIndex());
				DslAstNode currentNode= getCurrentNode();
				final YamlCollection.BlockSeq seqNode;
				if (currentNode instanceof PropertiesContainer
						&& currentNode.getDslParent() instanceof SeqEntry) {
					exit(NA_OFFSET);
					currentNode= getCurrentNode();
				}
				if (currentNode instanceof YamlCollection.BlockSeq) {
					seqNode= (YamlCollection.BlockSeq)currentNode;
				}
				else if (currentNode instanceof SeqEntry) {
					exit(NA_OFFSET);
					seqNode= (YamlCollection.BlockSeq)getCurrentNode();
				}
				else {
					seqNode= new YamlCollection.BlockSeq(currentNode,
							token.getStartIndex(), token.getEndIndex() );
					enterNode(seqNode);
				}
				final SeqEntry node= new SeqEntry(seqNode, token.getStartIndex(), token.getEndIndex());
				enterNode(node);
				attachProblem(node, token);
				continue;
			}
			case FlowEntry: {
				ensureRecordContent(token.getStartIndex());
				final boolean found= exitTo1(YamlCollection.FlowSeq.class, YamlCollection.FlowMap.class);
				final DslAstNode currentNode= getCurrentNode();
				if (found) {
					final var builder= getCurrentContainerBuilder();
					if (currentNode.getStatusCode() == 0
							&& builder.sepOffsets.size() == builder.children.size() ) {
						final DslAstNode node= switch (currentNode.getNodeType()) {
								case SEQ ->
										new Dummy(TYPE12_SYNTAX_MISSING_NODE | CTX12_SEQ_ENTRY,
												currentNode, token.getStartIndex() );
								case MAP ->
										new Dummy(TYPE12_SYNTAX_MISSING_NODE | CTX12_MAP_ENTRY,
												currentNode, token.getStartIndex() );
								default ->
										throw new RuntimeException();
								};
						addChildTerm(node);
					}
					builder.sepOffsets.add(token.getStartIndex());
				}
				else {
					addChildTerm(new Dummy(TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_SEQ_ENTRY,
							currentNode,
							token.getStartIndex(), token.getEndIndex() ));
				}
				continue;
			}
			
			case Anchor: {
				final DslAstNode currentNode= checkParent(token);
				
				final PropertiesContainer propertiesNode;
				if (currentNode.getNodeType() == NodeType.PROPERTIES_CONTAINER) {
					propertiesNode= (PropertiesContainer)currentNode;
				}
				else {
					propertiesNode= new PropertiesContainer(currentNode,
							token.getStartIndex(), token.getEndIndex() );
					enterNode(propertiesNode);
				}
				final Anchor node= new Anchor(propertiesNode,
						token.getStartIndex(), token.getEndIndex(),
						((AnchorToken)token).getValue() );
				addChildTerm(node);
				attachProblem(node, token);
				continue;
			}
			case Alias: {
				final DslAstNode currentNode= checkParent(token);
				
				final Alias node= new Alias(currentNode,
						token.getStartIndex(), token.getEndIndex(),
						((AliasToken)token).getValue() );
				addChildTerm(node);
				attachProblem(node, token);
				continue;
			}
			
			case Tag: {
				final TagTuple tagTuple= ((TagToken)token).getValue();
				final DslAstNode currentNode= checkParent(token);
				
				final PropertiesContainer propertiesNode;
				if (currentNode instanceof PropertiesContainer) {
					propertiesNode= (PropertiesContainer)currentNode;
				}
				else {
					propertiesNode= new PropertiesContainer(currentNode,
							token.getStartIndex(), token.getEndIndex() );
					enterNode(propertiesNode);
				}
				final YamlTag node= new YamlTag(propertiesNode,
						token.getStartIndex(), token.getEndIndex(),
						tagTuple.getHandle(), tagTuple.getSuffix() );
				addChildTerm(node);
				attachProblem(node, token);
				continue;
			}
			
			case Key: {
				ensureRecordContent(token.getStartIndex());
				DslAstNode currentNode= getCurrentNode();
				
				final boolean found;
				if (currentNode instanceof YamlCollection.FlowSeq) {
					found= true;
				}
				else {
					found= exitTo(NodeType.MAP);
					currentNode= getCurrentNode();
				}
				final var entryNode= new MapEntry(
						(!found) ? (TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_MAP_KEY) : 0,
						currentNode,
						token.getStartIndex(), token.getEndIndex() );
				enterNode(entryNode);
				attachProblem(entryNode, token);
				if (token.getLength() > 0) { // explicite
					entryNode.keyIndicatorOffset= entryNode.getStartOffset();
				}
				continue;
			}
			case Value: {
				ensureRecordContent(token.getStartIndex());
				DslAstNode currentNode= getCurrentNode();
				
				final boolean found;
				if (currentNode instanceof YamlCollection.FlowSeq) {
					found= true;
				}
				else {
					found= exitTo(NodeType.KEY_VALUE_ENTRY, NodeType.MAP);
					currentNode= getCurrentNode();
				}
				final MapEntry entryNode;
				if (currentNode.getNodeType() == NodeType.KEY_VALUE_ENTRY) {
					entryNode= (MapEntry)currentNode;
					entryNode.setEndOffset(token.getEndIndex());
				}
				else {
					entryNode= new MapEntry(
							(!found) ? (TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_MAP_VALUE) : 0,
							currentNode,
							token.getStartIndex(), token.getEndIndex() );
					enterNode(entryNode);
				}
				attachProblem(entryNode, token);
				if (token.getLength() > 0) {
					entryNode.valueIndicatorOffset= token.getStartIndex();
				}
				continue;
			}
			
			case Scalar: {
				final ScalarToken scalarToken= (ScalarToken)token;
				final DslAstNode currentNode= checkParent(token);
				final YamlScalar node= switch (scalarToken.getStyle()) {
						case DOUBLE_QUOTED ->
								new YamlScalar.DQuoted(currentNode,
										token.getStartIndex(), token.getEndIndex(),
										scalarToken.getValue() );
						case SINGLE_QUOTED ->
								new YamlScalar.SQuoated(currentNode,
										token.getStartIndex(), token.getEndIndex(),
										scalarToken.getValue() );
						case LITERAL ->
								new YamlScalar.Literal(currentNode,
										token.getStartIndex(), token.getEndIndex(),
										scalarToken.getValue() );
						case FOLDED ->
								new YamlScalar.Folded(currentNode,
										token.getStartIndex(), token.getEndIndex(),
										scalarToken.getValue() );
						default ->
								new YamlScalar.Plain(currentNode,
										token.getStartIndex(), token.getEndIndex(),
										scalarToken.getValue() );
						};
				addChildTerm(node);
				attachProblem(node, token);
				continue;
			}
			
			default:
				continue;
			}
		}
	}
	
	private void attachProblem(final DslAstNode node, final Token token) {
		if ((node.getStatusCode() & StatusCodes.ERROR) != 0) {
			return;
		}
		final SyntaxProblem problem= token.getProblem();
		if (problem != null) {
			setStatus(node, problem.getStatusCode(), problem.getStatusDetail());
		}
	}
	
}
