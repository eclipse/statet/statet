/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.isNull;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;


@NonNullByDefault
final class MapEntry extends org.eclipse.statet.dsl.core.source.ast.KeyValuePair {
	
	
	int keyIndicatorOffset= NA_OFFSET;
	
	int valueIndicatorOffset= NA_OFFSET;
	
	
	MapEntry(final int statusCode, final DslAstNode parent,
			final int startOffset, final int endOffset) {
		super(statusCode, parent);
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	MapEntry(final DslAstNode parent, final int startOffset, final int endOffset) {
		super(parent);
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	MapEntry(final DslAstNode parent, final int startOffset) {
		super(parent);
		doSetStartEndOffset(startOffset);
	}
	
	@Override
	protected void finish(final int endOffset) {
		if (isNull(getKey())) {
			doSetKeyChild(new YamlScalar.Plain(this, // empty node
					(this.keyIndicatorOffset != NA_OFFSET) ?
							this.keyIndicatorOffset : getStartOffset() ));
		}
		if (isNull(getValue())) {
			doSetValueChild(new YamlScalar.Plain(this, // empty node
					(this.valueIndicatorOffset != NA_OFFSET) ?
							this.valueIndicatorOffset :
							getKey().getEndOffset() ));
		}
		
		super.finish(endOffset);
	}
	
	void setEndOffset(final int offset) {
		doSetEndOffset(offset);
	}
	
	
	@Override
	public final int getKeyIndicatorOffset() {
		return this.keyIndicatorOffset;
	}
	
	@Override
	public final int getValueIndicatorOffset() {
		return this.valueIndicatorOffset;
	}
	
	
}
