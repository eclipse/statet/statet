/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.refactoring;

import org.eclipse.statet.ltk.refactoring.core.CommonPasteCodeProcessor;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination;


public class PasteYamlCodeProcessor extends CommonPasteCodeProcessor {
	
	
	public PasteYamlCodeProcessor(final String code,
			final RefactoringDestination destination, final RefactoringAdapter adapter) {
		super(code, destination, adapter);
	}
	
	
	@Override
	public String getIdentifier() {
		return YamlRefactoring.PASTE_YAML_CODE_PROCESSOR_ID;
	}
	
	@Override
	protected String getRefactoringIdentifier() {
		return YamlRefactoring.PASTE_YAML_CODE_REFACTORING_ID;
	}
	
}
