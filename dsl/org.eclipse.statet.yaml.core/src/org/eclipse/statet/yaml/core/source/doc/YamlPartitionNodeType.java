/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.doc;

import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.treepartitioner.BasicPartitionNodeType;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;


@NonNullByDefault
public abstract class YamlPartitionNodeType extends BasicPartitionNodeType {
	
	
	public static final YamlPartitionNodeType DEFAULT_ROOT= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE;
		}
		
		@Override
		public boolean prefereAtBegin(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
		@Override
		public boolean prefereAtEnd(final TreePartitionNode node, final IDocument document) {
			return true;
		}
		
	};
	
	public static final YamlPartitionNodeType DIRECTIVE= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_DIRECTIVE_CONTENT_TYPE;
		}
		
	};
	
	public static final YamlPartitionNodeType FLOAT_MAPPING= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE;
		}
		
	};
	
	public static final YamlPartitionNodeType FLOAT_SEQUENCE= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE;
		}
		
	};
	
	public static final YamlPartitionNodeType BLOCK_MAPPING= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE;
		}
		
	};
	
	public static final YamlPartitionNodeType BLOCK_SEQUENCE= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE;
		}
		
	};
	
	public static final YamlPartitionNodeType KEY= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_KEY_CONTENT_TYPE;
		}
		
	};
	
	public static final YamlPartitionNodeType TAG= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_TAG_CONTENT_TYPE;
		}
		
	};
	
	public static final YamlPartitionNodeType VALUE= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_VALUE_CONTENT_TYPE;
		}
		
	};
	
	public static final YamlPartitionNodeType COMMENT_LINE= new YamlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return YamlDocumentConstants.YAML_COMMENT_CONTENT_TYPE;
		}
		
	};
	
	
	protected YamlPartitionNodeType() {
	}
	
	
}
