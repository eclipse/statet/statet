/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.core.YamlCorePlugin;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


@NonNullByDefault
public final class YamlModel {
	
	
	public static final String YAML_TYPE_ID= "Yaml"; //$NON-NLS-1$
	
	
	public static YamlModelManager getYamlModelManager() {
		return YamlCorePlugin.getInstance().getYamlModelManager();
	}
	
	public static @Nullable YamlSourceUnitModelInfo getYamlModelInfo(final SourceUnitModelInfo modelInfo) {
		if (modelInfo != null) {
			if (modelInfo instanceof YamlSourceUnitModelInfo) {
				return (YamlSourceUnitModelInfo)modelInfo;
			}
			for (final Object aAttachment : modelInfo.getAttachments()) {
				if (aAttachment instanceof YamlSourceUnitModelInfo) {
					return (YamlSourceUnitModelInfo)aAttachment;
				}
			}
		}
		return null;
	}
	
	
	private YamlModel() {}
	
}
