/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;


@NonNullByDefault
final class SeqEntry extends org.eclipse.statet.dsl.core.source.ast.SeqEntry {
	
	
	int valueIndicatorOffset= NA_OFFSET;
	
	
	SeqEntry(final int statusCode, final DslAstNode parent,
			final int startOffset, final int endOffset) {
		super(statusCode, parent);
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	SeqEntry(final DslAstNode parent, final int startOffset, final int endOffset) {
		super(parent);
		doSetStartEndOffset(startOffset, endOffset);
		this.valueIndicatorOffset= startOffset;
	}
	
	@Override
	protected DslAstNode createAbsentValueNode(final int endOffset) {
		return new YamlScalar.Plain(this, // empty node
				(this.valueIndicatorOffset != NA_OFFSET) ?
						this.valueIndicatorOffset + 1 :
						endOffset );
	}
	
	
	@Override
	public final int getValueIndicatorOffset() {
		return this.valueIndicatorOffset;
	}
	
	
}
