/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.yaml.core.model.YamlCompositeSourceElement;
import org.eclipse.statet.yaml.core.model.YamlSourceElement;


@NonNullByDefault
public class CompositeSourceElement extends ContainerSourceElement.SourceContainer
		implements YamlCompositeSourceElement {
	
	
	private final ImList<? extends YamlSourceElement> compositeElements;
	
	private final TextRegion sourceRange;
	
	private volatile @Nullable List<SourceStructElement<?, ?>> allSourceChildren;
	
	
	public CompositeSourceElement(final SourceUnit sourceUnit,
			final SourceModelStamp stamp,
			final List<? extends YamlSourceElement> elements,
			final TextRegion sourceRange) {
		super(sourceUnit, stamp, null);
		
		this.compositeElements= ImCollections.toList(elements);
		this.sourceRange= sourceRange;
	}
	
	
	@Override
	public ImList<? extends YamlSourceElement> getCompositeElements() {
		return this.compositeElements;
	}
	
	@Override
	public TextRegion getSourceRange() {
		return this.sourceRange;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		for (final YamlSourceElement element : this.compositeElements) {
			if (element.hasSourceChildren(filter)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public List<SourceStructElement<?, ?>> getSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		if (filter == null) {
			List<SourceStructElement<?, ?>> children= this.allSourceChildren;
			if (children == null) {
				final @NonNull List<? extends SourceStructElement<?, ?>>[] compositeLists= new List[this.compositeElements.size()];
				for (int i= 0; i < compositeLists.length; i++) {
					compositeLists[i]= this.compositeElements.get(i).getSourceChildren(null);
				}
				children= this.allSourceChildren= ImCollections.concatList(compositeLists);
			}
			return children;
		}
		else {
			final List<SourceStructElement<?, ?>> children= new ArrayList<>();
			for (final YamlSourceElement element : this.compositeElements) {
				final List<? extends SourceStructElement<?, ?>> list= element.getSourceChildren(null);
				for (final SourceStructElement<?, ?> child : list) {
					if (filter.include(child)) {
						children.add(child);
					}
				}
			}
			return children;
		}
	}
	
}
