/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class ProblemMessages extends NLS {
	
	
	public static String Syntax_Gen_TokenUnexpected_message;
//	public static String Syntax_Gen_NodeMissing_message;
	public static String Syntax_Gen_TokenUnknown_message;
	public static String Syntax_Gen_LineBreakMissing_message;
	
	public static String Syntax_Doc_DirectiveEndMarkerMissing_message;
	public static String Syntax_Doc_ContentMissing_message;
	public static String Syntax_Doc_DirectiveUnexpected_message;
	
	public static String Syntax_FlowSeq_NotClosed_message;
	public static String Syntax_FlowMap_NotClosed_message;
	public static String Syntax_FlowSeq_SeqEntrySeparatorMissing_message;
	public static String Syntax_FlowSeq_SeqEntryMissing_message;
	public static String Syntax_FlowMap_MapEntrySeparatorMissing_message;
	public static String Syntax_FlowMap_MapEntryMissing_message;
	public static String Syntax_Map_MapKeyIndicatorMissing_messsage;
	public static String Syntax_Map_MapValueIndicatorMissing_message;
	public static String Syntax_Seq_SeqEntryIndicatorMissing_message;
	
	public static String Syntax_Scalar_BlockScalar_IndentIndicatorInvalid_message;
	public static String Syntax_Scalar_BlockScalar_ChompingIndicatorMultiple_message;
	public static String Syntax_Scalar_QuotedScalar_NotClosed_message;
	public static String Syntax_Scalar_QuotedScalar_EscapeSequenceInvalid_messsage;
	
	public static String Syntax_Tag_VerbatimTag_NotClosed_message;
	
	public static String Syntax_Comment_SpaceBeforeMissing_message;
	
	
	static {
		NLS.initializeMessages(ProblemMessages.class.getName(), ProblemMessages.class);
	}
	private ProblemMessages() {}
	
}
