/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.yaml.core.model.YamlSourceElement;


@NonNullByDefault
public class TermElement extends BasicYamlSourceElement {
	
	
	private final int type;
	
	private final ContainerSourceElement parent;
	
	
	public TermElement(final int type, final ContainerSourceElement parent,
			final DslAstNode astNode) {
		super(astNode);
		this.type= type;
		this.parent= parent;
	}
	
	
	@Override
	public int getElementType() {
		return this.type;
	}
	
	@Override
	public SourceUnit getSourceUnit() {
		return this.parent.getSourceUnit();
	}
	
	@Override
	public boolean exists() {
		return this.parent.exists();
	}
	
	@Override
	public boolean isReadOnly() {
		return this.parent.isReadOnly();
	}
	
	
	@Override
	public ContainerSourceElement getSourceParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return false;
	}
	
	@Override
	public List<? extends SourceStructElement<?, ?>> getSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return NO_CHILDREN;
	}
	
	@Override
	public ContainerSourceElement getModelParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super YamlSourceElement> filter) {
		return false;
	}
	
	@Override
	public List<? extends YamlSourceElement> getModelChildren(final @Nullable LtkModelElementFilter<? super YamlSourceElement> filter) {
		return NO_CHILDREN;
	}
	
	
}
