/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceUnitModelInfo;
import org.eclipse.statet.yaml.core.model.YamlSourceElement;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;


@NonNullByDefault
public class YamlSourceUnitModelInfoImpl extends BasicSourceUnitModelInfo implements YamlSourceUnitModelInfo {
	
	
	private final YamlSourceElement sourceElement;
	
	
	public YamlSourceUnitModelInfoImpl(final AstInfo ast, final YamlSourceElement unitElement) {
		super(ast);
		this.sourceElement= unitElement;
	}
	
	
	@Override
	public YamlSourceElement getSourceElement() {
		return this.sourceElement;
	}
	
	
}
