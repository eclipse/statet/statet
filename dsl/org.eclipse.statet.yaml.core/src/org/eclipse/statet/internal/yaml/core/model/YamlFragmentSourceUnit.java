/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.core.input.SourceFragment;
import org.eclipse.statet.ltk.model.core.impl.GenericFragmentSourceUnit2;
import org.eclipse.statet.yaml.core.YamlCore;
import org.eclipse.statet.yaml.core.YamlCoreAccess;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.build.YamlSourceUnitModelContainer;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentContentInfo;


@NonNullByDefault
public abstract class YamlFragmentSourceUnit
		extends GenericFragmentSourceUnit2<YamlSourceUnitModelContainer> {
	
	
	public YamlFragmentSourceUnit(final String id, final SourceFragment fragment) {
		super(id, fragment);
	}
	
	@Override
	protected YamlSourceUnitModelContainer createModelContainer() {
		return new YamlSourceUnitModelContainer(this, null);
	}
	
	
	@Override
	public String getModelTypeId() {
		return YamlModel.YAML_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return YamlDocumentContentInfo.INSTANCE;
	}
	
	
	@Override
	protected void register() {
		super.register();
	}
	
	@Override
	protected void unregister() {
		super.unregister();
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == YamlCoreAccess.class) {
			return (T)YamlCore.getWorkbenchAccess();
		}
		if (adapterType == PreferenceAccess.class) {
			return (T)EPreferences.getInstancePrefs();
		}
		return super.getAdapter(adapterType);
	}
	
	
}
