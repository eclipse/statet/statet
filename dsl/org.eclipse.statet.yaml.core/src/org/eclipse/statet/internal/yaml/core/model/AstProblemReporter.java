/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;
import static org.eclipse.statet.ltk.core.StatusCodes.CTX12;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE12;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE123;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_DIRECTIVES_END_MARKER;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_DOC_CONTENT;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_ENTRY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_FLOW;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_KEY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_MAP_VALUE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SCALAR_FLOW;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SEQ_ENTRY;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_SEQ_FLOW;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.CTX12_TAG_VERBATIM;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE123_SYNTAX_CHOMPING_INDICATOR_MULTIPLE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE123_SYNTAX_INDENTATION_INDICATOR_INVALID;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE123_SYNTAX_LINE_BREAK_MISSING;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE123_SYNTAX_SPACE_BEFORE_COMMENT_MISSING;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_CHAR_INVALID;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_COLLECTION_NOT_CLOSED;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_ESCAPE_INVALID;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_INDICATOR;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_MARKER;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_MISSING_NODE;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_SEP_COMMA_MISSING;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_NOT_CLOSED;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;
import static org.eclipse.statet.yaml.core.source.YamlSourceConstants.TYPE12_SYNTAX_TOKEN_UNKNOWN;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.Alias;
import org.eclipse.statet.dsl.core.source.ast.Anchor;
import org.eclipse.statet.dsl.core.source.ast.Collection;
import org.eclipse.statet.dsl.core.source.ast.Directive;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.DslAstVisitor;
import org.eclipse.statet.dsl.core.source.ast.Dummy;
import org.eclipse.statet.dsl.core.source.ast.KeyValuePair;
import org.eclipse.statet.dsl.core.source.ast.Marker;
import org.eclipse.statet.dsl.core.source.ast.Scalar;
import org.eclipse.statet.dsl.core.source.ast.SeqEntry;
import org.eclipse.statet.dsl.core.source.ast.Tag;
import org.eclipse.statet.ltk.ast.core.util.AbstractAstProblemReporter;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.core.source.StatusDetail;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.source.ast.YamlCollection.FlowCollection;


@NonNullByDefault
public class AstProblemReporter extends AbstractAstProblemReporter {
	
	
	private final Visitor visitor= new Visitor();
	
	
	public AstProblemReporter() {
		super(YamlModel.YAML_TYPE_ID);
	}
	
	
	public void run(final DslAstNode node,
			final SourceContent content,
			final IssueRequestor requestor) {
		try {
			init(content, requestor);
			
			node.acceptInDsl(this.visitor);
			
			flush();
		}
		catch (final OperationCanceledException | InvocationTargetException e) {}
		finally {
			clear();
		}
	}
	
	
	protected void handleCommonCodes(final DslAstNode node, final int code)
			throws BadLocationException, InvocationTargetException {
		final StatusDetail detail;
		TYPE12: switch (code & TYPE12) {
		
		case TYPE12_SYNTAX_TOKEN_UNKNOWN:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
					ProblemMessages.Syntax_Gen_TokenUnknown_message,
							getMessageUtil().getShortQuoteText(node, 0) ),
					node.getStartOffset(), node.getEndOffset() );
			return;
			
		case TYPE12_SYNTAX_TOKEN_UNEXPECTED:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
					ProblemMessages.Syntax_Gen_TokenUnexpected_message,
							getMessageUtil().getMidQuoteText(node, 0) ),
					node.getStartOffset(), node.getEndOffset() );
			return;
			
		case TYPE12_SYNTAX_MISSING_NODE:
//			addProblem(Problem.SEVERITY_ERROR, code,
//					ProblemMessages.Syntax_Gen_NodeMissing_message,
//					node.getStartOffset(), node.getEndOffset() );
			return;
			
		case TYPE12_SYNTAX_CHAR_INVALID:
			detail= StatusDetail.get(node);
			if (detail != null) {
				switch (code & TYPE123) {
				case TYPE123_SYNTAX_SPACE_BEFORE_COMMENT_MISSING:
					addProblem(Problem.SEVERITY_ERROR, code,
							ProblemMessages.Syntax_Comment_SpaceBeforeMissing_message,
							detail.getStartOffset(), detail.getEndOffset() );
					return;
				case TYPE123_SYNTAX_LINE_BREAK_MISSING:
					addProblem(Problem.SEVERITY_ERROR, code,
							ProblemMessages.Syntax_Gen_LineBreakMissing_message,
							detail.getStartOffset(), detail.getEndOffset() );
					return;
				default:
					addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
							ProblemMessages.Syntax_Gen_TokenUnexpected_message,
							getMessageUtil().getShortQuoteText(node, 0) ),
							node.getStartOffset(), node.getEndOffset() );
					return;
				}
			}
			break TYPE12;
		
		default:
			break TYPE12;
		}
		
		super.handleCommonCodes(node, code);
	}
	
	
	private class Visitor extends DslAstVisitor {
		
		
		//	@Override
		//	public void visit(SourceComponent node) throws InvocationTargetException {
		//		super.visit(node);
		//	}
			
		//	@Override
		//	public void visit(final DocumentNode node) throws InvocationTargetException {
		//		super.visit(node);
		//	}
		
		@Override
		public void visit(final Marker node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					{	handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Directive node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & (TYPE12)) {
					case TYPE12_SYNTAX_TOKEN_UNEXPECTED:
						addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
								ProblemMessages.Syntax_Doc_DirectiveUnexpected_message,
										getMessageUtil().getShortQuoteText(node, 0) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
	//	@Override
	//	public void visit(final PropertiesContainer node) throws InvocationTargetException {
	//		super.visit(node);
	//	}
		
		@Override
		public void visit(final Tag node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & (TYPE12 | CTX12)) {
					case TYPE12_SYNTAX_TOKEN_NOT_CLOSED | CTX12_TAG_VERBATIM:
						addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
								ProblemMessages.Syntax_Tag_VerbatimTag_NotClosed_message,
										getMessageUtil().getShortQuoteText(node, 0) ),
								node.getEndOffset(), node.getEndOffset() + 1);
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Anchor node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					{	handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Collection node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					final int offset;
					switch (code & (TYPE12 | CTX12)) {
					case TYPE12_SYNTAX_COLLECTION_NOT_CLOSED | CTX12_SEQ_FLOW:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FlowSeq_NotClosed_message,
								offset= node.getStartOffset(), offset + 1 );
						break STATUS;
					case TYPE12_SYNTAX_COLLECTION_NOT_CLOSED | CTX12_MAP_FLOW:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FlowMap_NotClosed_message,
								offset= node.getStartOffset(), offset + 1 );
						break STATUS;
					
					case TYPE12_SYNTAX_SEP_COMMA_MISSING | CTX12_SEQ_FLOW:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FlowSeq_SeqEntrySeparatorMissing_message,
								offset= getMissingSepOffset((FlowCollection)node), offset + 1 );
						break STATUS;
					case TYPE12_SYNTAX_SEP_COMMA_MISSING | CTX12_MAP_FLOW:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FlowMap_MapEntrySeparatorMissing_message,
								offset= getMissingSepOffset((FlowCollection)node), offset + 1 );
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInDslChildren(this);
		}
		
		@Override
		public void visit(final SeqEntry node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & (TYPE12 | CTX12)) {
					case TYPE12_SYNTAX_MISSING_INDICATOR | CTX12_SEQ_ENTRY:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_Seq_SeqEntryIndicatorMissing_message,
								node.getStartOffset(), node.getStartOffset() + 1 );
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInDslChildren(this);
		}
		
		@Override
		public void visit(final KeyValuePair node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				try {
					final int offset;
					TYPE: switch (code & (TYPE12 | CTX12)) {
					case TYPE12_SYNTAX_MISSING_INDICATOR | CTX12_MAP_KEY:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_Map_MapKeyIndicatorMissing_messsage,
								offset= node.getStartOffset(), offset + 1 );
						break TYPE;
					case TYPE12_SYNTAX_MISSING_INDICATOR | CTX12_MAP_VALUE:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_Map_MapValueIndicatorMissing_message,
								node.getKey().getEndOffset() - 1, node.getKey().getEndOffset() + 1 );
						break TYPE;
						
					default:
						handleCommonCodes(node, code);
						break TYPE;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			node.acceptInDslChildren(this);
		}
		
		@Override
		public void visit(final Scalar node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					final StatusDetail detail;
					switch (code & (TYPE123 | CTX12)) {
					case TYPE12_SYNTAX_TOKEN_NOT_CLOSED | CTX12_SCALAR_FLOW:
						addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
								ProblemMessages.Syntax_Scalar_QuotedScalar_NotClosed_message,
										getMessageUtil().getShortQuoteText(node, 1),
										String.valueOf(node.getOperator()) ),
								node.getEndOffset() - 1, node.getEndOffset() + 1 );
						break STATUS;
					
					case TYPE123_SYNTAX_INDENTATION_INDICATOR_INVALID:
						detail= StatusDetail.get(node);
						if (detail != null) {
							addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
									ProblemMessages.Syntax_Scalar_BlockScalar_IndentIndicatorInvalid_message,
									detail.getText() ),
									detail.getStartOffset(), detail.getEndOffset() );
							break STATUS;
						}
						handleCommonCodes(node, code);
						break STATUS;
					case TYPE123_SYNTAX_CHOMPING_INDICATOR_MULTIPLE:
						detail= StatusDetail.get(node);
						if (detail != null) {
							addProblem(Problem.SEVERITY_ERROR, code,
									ProblemMessages.Syntax_Scalar_BlockScalar_ChompingIndicatorMultiple_message,
									detail.getStartOffset(), detail.getEndOffset() );
							break STATUS;
						}
						handleCommonCodes(node, code);
						break STATUS;
					
					case TYPE12_SYNTAX_ESCAPE_INVALID | CTX12_SCALAR_FLOW:
						detail= StatusDetail.get(node);
						if (detail != null) {
							addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
									ProblemMessages.Syntax_Scalar_QuotedScalar_EscapeSequenceInvalid_messsage,
											detail.getText() ),
									detail.getStartOffset(), detail.getEndOffset() );
							break STATUS;
						}
						handleCommonCodes(node, code);
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Dummy node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & (TYPE12 | CTX12)) {
					case TYPE12_SYNTAX_MISSING_MARKER | CTX12_DIRECTIVES_END_MARKER:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_Doc_DirectiveEndMarkerMissing_message,
								node.getStartOffset(), getMessageUtil().expandSpaceEnd(node.getEndOffset()) );
						break STATUS;
					
					case TYPE12_SYNTAX_MISSING_NODE | CTX12_DOC_CONTENT:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_Doc_ContentMissing_message,
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE12_SYNTAX_MISSING_NODE | CTX12_SEQ_ENTRY:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FlowSeq_SeqEntryMissing_message,
								node.getStartOffset(), getMessageUtil().expandSpaceEnd(node.getEndOffset()) );
						break STATUS;
					case TYPE12_SYNTAX_MISSING_NODE | CTX12_MAP_ENTRY:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_FlowMap_MapEntryMissing_message,
								node.getStartOffset(), getMessageUtil().expandSpaceEnd(node.getEndOffset()) );
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Alias node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					{	handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		
		
	}
	
	private static int getMissingSepOffset(final FlowCollection node) {
		final int idx= node.getSepOffsets().indexOf(NA_OFFSET);
		final DslAstNode child= node.getChild(idx + 1);
		return child.getStartOffset();
	}
	
}
