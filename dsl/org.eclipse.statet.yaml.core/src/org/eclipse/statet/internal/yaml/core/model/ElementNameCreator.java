/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.util.AbortException;

import org.eclipse.statet.dsl.core.model.DslElementName;
import org.eclipse.statet.dsl.core.source.ast.Alias;
import org.eclipse.statet.dsl.core.source.ast.Collection;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.DslAstVisitor;
import org.eclipse.statet.dsl.core.source.ast.KeyValuePair;
import org.eclipse.statet.dsl.core.source.ast.PropertiesContainer;
import org.eclipse.statet.dsl.core.source.ast.Scalar;


@NonNullByDefault
public class ElementNameCreator extends DslAstVisitor {
	
	
	private final StringBuilder sb= new StringBuilder();
	
	private int maxLength= 100;
	
	
	public ElementNameCreator() {
	}
	
	
	public void setLengthSoftLimit(final int length) {
		this.maxLength= length;
	}
	
	
	public DslElementName createSeqEntry(final int index) {
		return DslElementName.create(DslElementName.SEQ_NUM, Integer.toString(index + 1));
	}
	
	public DslElementName createMapKey(final @Nullable DslAstNode node) {
		if (node == null) {
			return DslElementName.create(0, "<?>"); //$NON-NLS-1$
		}
		
		switch (node.getNodeType()) {
		case SCALAR:
			return DslElementName.create(DslElementName.SCALAR, node.getText());
		case ALIAS:
			return DslElementName.create(DslElementName.ANCHOR, node.getText());
		default:
			break;
		}
		
		this.sb.setLength(0);
		try {
			node.acceptInDsl(this);
		}
		catch (final AbortException | InvocationTargetException e) {
		}
		return DslElementName.create(DslElementName.COMPLEX,
				(this.sb.length() > 1) ? this.sb.substring(1) : "" ); //$NON-NLS-1$
	}
	
	
	@Override
	public void visit(final PropertiesContainer node) throws InvocationTargetException {
		node.getNode().acceptInDsl(this);
	}
	
	@Override
	public void visit(final Collection node) throws InvocationTargetException {
		final String open;
		final String close;
		switch (node.getNodeType()) {
		case SEQ:
			open= " ["; //$NON-NLS-1$
			close= " ]"; //$NON-NLS-1$
			break;
		case MAP:
			open= " {"; //$NON-NLS-1$
			close= " }"; //$NON-NLS-1$
			break;
		default:
			throw new RuntimeException();
		}
		
		this.sb.append(open);
		try {
			if (node.hasChildren()) {
				node.getChild(0).acceptInDsl(this);
				for (int i= 1; i < node.getChildCount(); i++) {
					this.sb.append(',');
					node.getChild(i).acceptInDsl(this);
				}
			}
		}
		finally {
			this.sb.append(close);
		}
	}
	
	@Override
	public void visit(final KeyValuePair node) throws InvocationTargetException {
		node.getKey().acceptInDsl(this);
		this.sb.append(" :"); //$NON-NLS-1$
		node.getValue().acceptInDsl(this);
	}
	
	private void checkLength() {
		if (this.sb.length() >= this.maxLength) {
			this.sb.append(" …"); //$NON-NLS-1$
			throw new AbortException();
		}
	}
	
	@Override
	public void visit(final Scalar node) throws InvocationTargetException {
		checkLength();
		final String text= node.getText();
		if (text == null) {
			this.sb.append("<null>"); //$NON-NLS-1$
		}
		else {
			this.sb.append(" \""); //$NON-NLS-1$
			final int max= Math.max(10, this.maxLength - this.sb.length());
			if (text.length() > max) {
				this.sb.append(text, 0, max);
			}
			else {
				this.sb.append(text);
			}
			this.sb.append('"');
		}
	}
	
	@Override
	public void visit(final Alias node) throws InvocationTargetException {
		checkLength();
		final String text= node.getText();
		this.sb.append(" *"); //$NON-NLS-1$
		if (text == null) {
			this.sb.append("<missing>"); //$NON-NLS-1$
		}
		else {
			this.sb.append(text);
		}
	}
	
}
