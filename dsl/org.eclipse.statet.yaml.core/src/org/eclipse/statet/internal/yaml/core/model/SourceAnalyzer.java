/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.dsl.core.model.DslElementName;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.KeyValuePair;
import org.eclipse.statet.dsl.core.source.ast.NodeType;
import org.eclipse.statet.dsl.core.source.ast.PropertiesContainer;
import org.eclipse.statet.dsl.core.source.ast.SeqEntry;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.internal.yaml.core.model.ContainerSourceElement.CollectionContainer;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.yaml.core.model.YamlChunkElement;
import org.eclipse.statet.yaml.core.model.YamlElement;
import org.eclipse.statet.yaml.core.model.YamlSourceElement;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;
import org.eclipse.statet.yaml.core.source.ast.YamlDocument;


@NonNullByDefault
public class SourceAnalyzer {
	
	
	private SourceUnit sourceUnit= nonNullLateInit();
	private AstInfo ast= nonNullLateInit();
	private @Nullable List<YamlChunkElement> chunkElements;
	
	private final ElementNameCreator nameCreator= new ElementNameCreator();
	
	private final List<ArrayList<YamlSourceElement>> containerBuilderStack= new ArrayList<>();
	private int depth= -1;
	
	
	public SourceAnalyzer() {
	}
	
	
	public YamlSourceUnitModelInfoImpl createModel(final SourceUnit sourceUnit, final AstInfo ast) {
		try {
			this.sourceUnit= sourceUnit;
			this.ast= ast;
			
			final var rootNode= (SourceComponent)ast.getRoot();
			final var element= new ContainerSourceElement.SourceContainer(
					this.sourceUnit, ast.getStamp(),
					rootNode );
			processSourceLines(element, rootNode);
			
			final var modelInfo= new YamlSourceUnitModelInfoImpl(ast, element);
			return modelInfo;
		}
		finally {
			while (this.depth >= 0) {
				this.containerBuilderStack.get(this.depth--).clear();
			}
		}
	}
	
	
	public void beginChunkSession(final SourceUnit sourceUnit, final AstInfo ast) {
		this.sourceUnit= sourceUnit;
		this.ast= ast;
		if (this.chunkElements == null) {
			this.chunkElements= new ArrayList<>();
		}
	}
	
	public void processChunk(final YamlChunkElement element,
			final SourceComponent sourceComponent) {
		final var chunkElements= nonNullAssert(this.chunkElements);
		chunkElements.add(element);
//		for (final SourceComponent sourceComponent : sourceComponents) {
			processSourceLines(element, sourceComponent);
//		}
	}
	
	public YamlSourceUnitModelInfo stopChunkSession() {
		final var chunkElements= nonNullAssert(this.chunkElements);
		try {
			final var compositeElement= new CompositeSourceElement(
					this.sourceUnit, this.ast.getStamp(),
					chunkElements, this.ast.getRoot() );
			return new YamlSourceUnitModelInfoImpl(this.ast, compositeElement);
		}
		finally {
			this.sourceUnit= null;
			chunkElements.clear();
		}
	}
	
	
	private List<YamlSourceElement> enter(final ContainerSourceElement element) {
		final int depth= ++this.depth;
		final ArrayList<YamlSourceElement> containerBuilder;
		if (depth < this.containerBuilderStack.size()) {
			containerBuilder= this.containerBuilderStack.get(depth);
		}
		else {
			containerBuilder= new ArrayList<>();
			this.containerBuilderStack.add(containerBuilder);
		}
		return containerBuilder;
	}
	
	private void exit(final ContainerSourceElement element, final List<YamlSourceElement> builder) {
		element.children= ImCollections.concatList(element.children, builder);
		builder.clear();
		this.depth--;
	}
	
	
	private void processSourceLines(final ContainerSourceElement element,
			final SourceComponent sourceComponent) {
		final var builder= enter(element);
		int docCount= element.children.size();
		for (int childIndex= 0; childIndex < sourceComponent.getChildCount(); childIndex++) {
			final var childNode= sourceComponent.getChild(childIndex);
			if (childNode instanceof YamlDocument) {
				final var childElement= createDocumentElement(element, (YamlDocument)childNode);
				childElement.occurrenceCount= docCount++;
				childElement.name= DslElementName.create(DslElementName.RECORD_NUM,
						Integer.toString(childElement.occurrenceCount + 1) );
				builder.add(childElement);
			}
		}
		exit(element, builder);
	}
	
	private BasicYamlSourceElement createDocumentElement(final ContainerSourceElement parent,
			final YamlDocument node) {
		final var element= new CollectionContainer(YamlElement.C12_DOC, parent, node,
				node );
		final var builder= enter(element);
		final var contentNodes= node.getContentNodes();
		for (int childIndex= 0; childIndex < contentNodes.size(); childIndex++) {
			final var childNode= contentNodes.get(childIndex);
			final var childElement= createContentElement(parent, childNode);
			childElement.occurrenceCount= childIndex;
			childElement.name= DslElementName.create(DslElementName.OTHER, ""); //$NON-NLS-1$
			builder.add(childElement);
		}
		exit(element, builder);
		return element;
	}
	
	private BasicYamlSourceElement createContentElement(final ContainerSourceElement parent,
			final DslAstNode node) {
		DslAstNode contentNode= node;
		switch (node.getNodeType()) {
		case SEQ_ENTRY:
			if (node.getDslParent().getNodeType() == NodeType.SEQ) {
				contentNode= ((SeqEntry)node).getValue();
			}
			break;
		case KEY_VALUE_ENTRY:
			if (node.getDslParent().getNodeType() == NodeType.MAP) {
				contentNode= ((KeyValuePair)node).getValue();
			}
			break;
		default:
			break;
		}
		if (contentNode.getNodeType() == NodeType.PROPERTIES_CONTAINER) {
			contentNode= ((PropertiesContainer)contentNode).getNode();
		}
		
		final ContainerSourceElement element;
		final List<YamlSourceElement> builder;
		switch (contentNode.getNodeType()) {
		case SEQ:
			element= new CollectionContainer(YamlElement.C12_SEQ, parent, node, contentNode);
			if (this.depth <= 2 || hasNonScalarChild(contentNode)) {
				builder= enter(element);
				for (int childIndex= 0; childIndex < contentNode.getChildCount(); childIndex++) {
					final var childNode= contentNode.getChild(childIndex);
					final BasicYamlSourceElement childElement;
					if (childNode instanceof KeyValuePair) {
						childElement= createContentElement(element, childNode);
						childElement.name= this.nameCreator.createSeqEntry(childIndex);
					}
					else {
						childElement= createContentElement(element, childNode);
						childElement.name= this.nameCreator.createSeqEntry(childIndex);
					}
					builder.add(childElement);
				}
				exit(element, builder);
			}
			return element;
		case MAP:
			element= new CollectionContainer(YamlElement.C12_MAP, parent, node, contentNode);
			builder= enter(element);
			for (int childIndex= 0; childIndex < contentNode.getChildCount(); childIndex++) {
				final var childNode= contentNode.getChild(childIndex);
				final BasicYamlSourceElement childElement;
				if (childNode instanceof final KeyValuePair entry) {
					childElement= createContentElement(element, childNode);
					childElement.name= this.nameCreator.createMapKey(entry.getKey());
					childElement.nameRegion= entry.getKey();
				}
				else {
					childElement= createContentElement(element, childNode);
					childElement.name= this.nameCreator.createMapKey(null);
				}
				builder.add(childElement);
			}
			exit(element, builder);
			return element;
		case SCALAR:
			return new TermElement(YamlElement.C1_SCALAR, parent, node);
		case ALIAS:
			return new TermElement(YamlElement.C1_ALIAS, parent, node);
		case ERROR:
			return new TermElement(YamlElement.C1_DUMMY, parent, node);
		case KEY_VALUE_ENTRY: // compact singleton map in seq
		default:
			return new TermElement(0, parent, node);
		}
	}
	
	
	private boolean hasNonScalarChild(final DslAstNode node) {
		for (int childIndex= 0; childIndex < node.getChildCount(); childIndex++) {
			final DslAstNode child= node.getChild(childIndex);
			switch (child.getNodeType()) {
			case MAP:
			case PROPERTIES_CONTAINER:
				return true;
			case SEQ:
			case SEQ_ENTRY:
			case KEY_VALUE_ENTRY:
				if (hasNonScalarChild(child)) {
					return true;
				}
				continue;
			default:
				continue;
			}
		}
		return false;
	}
	
}
