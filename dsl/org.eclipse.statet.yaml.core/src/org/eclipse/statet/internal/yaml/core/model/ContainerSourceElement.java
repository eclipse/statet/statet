/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.core.model;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.model.DslElementName;
import org.eclipse.statet.dsl.core.source.ast.Collection;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceContainerElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.YamlSourceElement;


@NonNullByDefault
public abstract class ContainerSourceElement extends BasicYamlSourceElement {
	
	
	ImList<YamlSourceElement> children= NO_CHILDREN;
	
	
	public ContainerSourceElement(final DslAstNode astNode) {
		super(astNode);
	}
	
	
	@Override
	public TextRegion getNameSourceRange() {
		return this.nameRegion;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return LtkModelUtils.<SourceStructElement<?, ?>>hasChildren(this.children, filter);
	}
	
	@Override
	public List<? extends SourceStructElement<?, ?>> getSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return LtkModelUtils.<SourceStructElement<?, ?>>getChildren(this.children, filter);
	}
	
	@Override
	public abstract @Nullable ContainerSourceElement getModelParent();
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super YamlSourceElement> filter) {
		return LtkModelUtils.<YamlSourceElement>hasChildren(this.children, filter);
	}
	
	@Override
	public List<? extends YamlSourceElement> getModelChildren(final @Nullable LtkModelElementFilter<? super YamlSourceElement> filter) {
		return LtkModelUtils.<YamlSourceElement>getChildren(this.children, filter);
	}
	
	
	public static class SourceContainer extends ContainerSourceElement
			implements SourceContainerElement<YamlSourceElement> {
		
		
		private final SourceUnit sourceUnit;
		
		private final SourceModelStamp stamp;
		
		
		public SourceContainer(final SourceUnit sourceUnit,
				final SourceModelStamp stamp,
				final DslAstNode astNode) {
			super(astNode);
			this.sourceUnit= sourceUnit;
			this.stamp= stamp;
			
			final ElementName elementName= sourceUnit.getElementName();
			this.name= (elementName instanceof DslElementName) ?
					(DslElementName)elementName :
					DslElementName.create(DslElementName.RESOURCE, elementName.getSegmentName());
		}
		
		
		@Override
		public int getElementType() {
			return LtkModelElement.C12_SOURCE_FILE;
		}
		
		@Override
		public String getId() {
			return this.sourceUnit.getId();
		}
		
		@Override
		public SourceUnit getSourceUnit() {
			return this.sourceUnit;
		}
		
		@Override
		public SourceModelStamp getStamp() {
			return this.stamp;
		}
		
		@Override
		public boolean exists() {
			final SourceUnitModelInfo modelInfo= getSourceUnit().getModelInfo(YamlModel.YAML_TYPE_ID, 0, null);
			return (modelInfo != null && modelInfo.getSourceElement() == this);
		}
		
		@Override
		public boolean isReadOnly() {
			return this.sourceUnit.isReadOnly();
		}
		
		@Override
		public @Nullable SourceStructElement<?, ?> getSourceParent() {
			return null;
		}
		
		@Override
		public @Nullable ContainerSourceElement getModelParent() {
			return null;
		}
		
	}
	
	public static class CollectionContainer extends ContainerSourceElement {
		
		
		private final int type;
		
		private final ContainerSourceElement parent;
		
		private final DslAstNode contentNode;
		
		
		public CollectionContainer(final int type, final ContainerSourceElement parent,
				final DslAstNode astNode, final DslAstNode contentNode) {
			super(astNode);
			this.type= type;
			this.parent= parent;
			this.contentNode= contentNode;
		}
		
		
		@Override
		public final int getElementType() {
			return this.type;
		}
		
		@Override
		public SourceUnit getSourceUnit() {
			return this.parent.getSourceUnit();
		}
		
		@Override
		public boolean exists() {
			return this.parent.exists();
		}
		
		@Override
		public boolean isReadOnly() {
			return this.parent.isReadOnly();
		}
		
		@Override
		public ContainerSourceElement getSourceParent() {
			return this.parent;
		}
		
		@Override
		public ContainerSourceElement getModelParent() {
			return this.parent;
		}
		
		
		@Override
		public <T> @Nullable T getAdapter(final Class<T> adapterType) {
			if (adapterType == Collection.class) {
				return (T)this.contentNode;
			}
			return super.getAdapter(adapterType);
		}
		
	}
	
}
