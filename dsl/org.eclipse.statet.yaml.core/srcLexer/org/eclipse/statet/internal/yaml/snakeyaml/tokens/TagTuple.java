/*
 * Copyright (c) 2008, 2025 http://www.snakeyaml.org and others.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eclipse.statet.internal.yaml.snakeyaml.tokens;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class TagTuple {
	
	
	private final @Nullable String handle;
	private final String suffix;
	
	
	public TagTuple(final @Nullable String handle, final String suffix) {
		this.handle = handle;
		this.suffix = suffix;
	}
	
	
	public @Nullable String getHandle() {
		return this.handle;
	}
	
	public String getSuffix() {
		return this.suffix;
	}
	
	
}
