/*
 * Copyright (c) 2018, 2025 http://www.snakeyaml.org and others.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eclipse.statet.internal.yaml.snakeyaml.tokens;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * YAML provides a rich set of scalar styles. Block scalar styles include
 * the literal style and the folded style; flow scalar styles include the
 * plain style and two quoted styles, the single-quoted style and the
 * double-quoted style. These styles offer a range of trade-offs between
 * expressive power and readability.
 */
@NonNullByDefault
public enum ScalarStyle {
	
	DOUBLE_QUOTED('"'),
	SINGLE_QUOTED('\''),
	LITERAL('|'),
	FOLDED('>'),
	PLAIN(null);
	
	
	private final @Nullable Character styleOpt;
	
	
	ScalarStyle(final @Nullable Character style) {
		this.styleOpt = style;
	}
	
	
	@Override
	public String toString() {
		if (this.styleOpt != null) {
			return String.valueOf(this.styleOpt);
		}
		return ":";
	}
	
}
