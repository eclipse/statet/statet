/*
 * Copyright (c) 2008, 2025 http://www.snakeyaml.org and others.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eclipse.statet.internal.yaml.snakeyaml.scanner;

import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;


/**
 * Reader: checks if code points are in allowed range. Returns {@link #EOF} when end of data has
 * been reached.
 */
@NonNullByDefault
public final class StreamReader {
	
	
	public static final int EOF= 0;
	
	
	private final TextParserInput input;
	
	/**
	 * Read data (as a moving window for input stream)
	 */
	private int[] codePointsWindow;
	
	/**
	 * Real length of the data in dataWindow
	 */
	private int dataLength;
	
	/**
	 * The variable points to the current position in the data array
	 */
	private int pointer = 0;
	private boolean eof;
	/**
	 * index is only required to implement 1024 key length restriction
	 * It must count code points, but it counts characters (to be fixed)
	 */
	private int index = 0; // in code points
	private int line = 0;
	private int column = 0; //in code points
	private final int bufferSize;
	
	
	public StreamReader(final TextParserInput input,
			final int bufferSize) {
		this.codePointsWindow = new int[0];
		this.dataLength = 0;
		this.input = input;
		this.eof = false;
		this.index= input.getStartIndex();
		this.bufferSize = bufferSize;
	}
	
	
	public static final boolean isPrintable(final String data) {
		final int length = data.length();
		int offset = 0;
		while (offset < length) {
			final int codePoint = data.codePointAt(offset);
			if (!isPrintable(codePoint)) {
				return false;
			}
			offset += Character.charCount(codePoint);
		}
		return true;
	}
	
	public static boolean isPrintable(final int c) {
		return (c >= 0x20 && c <= 0x7E) || c == 0x9 || c == 0xA || c == 0xD || c == 0x85
				|| (c >= 0xA0 && c <= 0xD7FF) || (c >= 0xE000 && c <= 0xFFFD)
				|| (c >= 0x10000 && c <= 0x10FFFF);
	}
	
	
	public int getMark() {
		return this.index;
	}
	
	public void forward() {
		forward(1);
	}
	
	/**
	 * read the next length characters and move the pointer.
	 * if the last character is high surrogate one more character will be read
	 * 
	 * @param length amount of characters to move forward
	 */
	public void forward(final int length) {
		for (int i = 0; i < length && ensureEnoughData(); i++) {
			final int c = this.codePointsWindow[this.pointer++];
			this.index++;
			if (c == '\n'
					// do not count CR if it is followed by LF
					|| (c == '\r' && (ensureEnoughData() && this.codePointsWindow[this.pointer] != '\n')) ) {
				this.line++;
				this.column = 0;
			} else if (c != 0xFEFF) {
				this.column++;
			}
		}
	}
	
	public int peek() {
		return (ensureEnoughData()) ? this.codePointsWindow[this.pointer] : EOF;
	}
	
	/**
	 * Peek the next index-th code point
	 * 
	 * @param index to peek
	 * @return the next index-th code point
	 */
	public int peek(final int index) {
		return (ensureEnoughData(index)) ? this.codePointsWindow[this.pointer + index] : EOF;
	}
	
	/**
	 * peek the next length code points
	 * 
	 * @param length amount of the characters to peek
	 * @return the next length code points
	 */
	public String prefix(final int length) {
		if (length == 0) {
			return "";
		} else if (ensureEnoughData(length)) {
			return new String(this.codePointsWindow, this.pointer, length);
		} else {
			return new String(this.codePointsWindow, this.pointer,
					Math.min(length, this.dataLength - this.pointer));
		}
	}
	
	/**
	 * prefix(length) immediately followed by forward(length)
	 * 
	 * @param length amount of characters to get
	 * @return the next length code points
	 */
	public String prefixForward(final int length) {
		final String prefix = prefix(length);
		this.pointer += length;
		this.index += length;
		// prefix never contains new line characters
		this.column += length;
		return prefix;
	}
	
	private boolean ensureEnoughData() {
		return ensureEnoughData(0);
	}
	
	private boolean ensureEnoughData(final int size) {
		if (!this.eof && this.pointer + size >= this.dataLength) {
			update();
		}
		return (this.pointer + size) < this.dataLength;
	}
	
	private void update() {
		int c= this.input.get(0);
		if (c != TextParserInput.EOF) {
			int cpIndex= (this.dataLength - this.pointer);
			this.codePointsWindow= Arrays.copyOfRange(this.codePointsWindow, this.pointer, this.dataLength + this.bufferSize);
			
			while (cpIndex < this.codePointsWindow.length) {
				if (c != TextParserInput.EOF) {
					final char c0= (char)c;
					c= this.input.get(1);
					if (c != TextParserInput.EOF
							&& Character.isHighSurrogate(c0)
							&& Character.isLowSurrogate((char)c)) {
						this.codePointsWindow[cpIndex++]= Character.toCodePoint(c0, (char)c);
						this.input.consume(2);
						c= this.input.get(0);
						continue;
					}
					this.codePointsWindow[cpIndex++]= c0;
					this.input.consume(1);
					continue;
				}
				else {
					this.eof= true;
					break;
				}
			}
			
			this.dataLength= cpIndex;
			this.pointer= 0;
		}
		else {
			this.eof = true;
		}
	}
	
	
	public int getColumn() {
		return this.column;
	}
	
	/**
	 * @return current position as number (in characters) from the beginning of the stream
	 */
	public int getIndex() {
		return this.index;
	}
	
	public int getLine() {
		return this.line;
	}
	
}
