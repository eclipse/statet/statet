/*
 * Copyright (c) 2008, 2025 http://www.snakeyaml.org and others.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eclipse.statet.internal.yaml.snakeyaml.tokens;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.snakeyaml.scanner.SyntaxProblem;


@NonNullByDefault
public final class ScalarToken extends Token {
	
	
	private final ScalarStyle style;
	
	private final @Nullable String value;
	
	
	public ScalarToken(final @Nullable String value, final ScalarStyle style,
			final int startIndex, final int endIndex, final @Nullable SyntaxProblem problem) {
		super(startIndex, endIndex, problem);
		this.value = value;
		this.style = style;
	}
	
	
	@Override
	public Token.ID getTokenId() {
		return ID.Scalar;
	}
	
	public ScalarStyle getStyle() {
		return this.style;
	}
	
	
	public @Nullable String getValue() {
		return this.value;
	}
	
}
