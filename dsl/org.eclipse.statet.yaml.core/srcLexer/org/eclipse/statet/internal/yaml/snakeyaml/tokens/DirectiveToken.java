/*
 * Copyright (c) 2008, 2025 http://www.snakeyaml.org and others.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eclipse.statet.internal.yaml.snakeyaml.tokens;

import java.util.List;
import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.snakeyaml.scanner.SyntaxProblem;


@NonNullByDefault
public final class DirectiveToken<T> extends Token {
	
	public static final String YAML_DIRECTIVE = "YAML";
	public static final String TAG_DIRECTIVE = "TAG";
	
	
	private final String name;
	private final @Nullable List<T> value;
	
	
	public DirectiveToken(final String name, final @Nullable List<T> value,
			final int startIndex, final int endIndex, final @Nullable SyntaxProblem problem) {
		super(startIndex, endIndex, problem);
		Objects.requireNonNull(name);
		this.name = name;
		if (value != null && value.size() != 2) {
			throw new IllegalArgumentException("Two strings/integers must be provided instead of " + value.size());
		}
		this.value = value;
	}
	
	
	@Override
	public Token.ID getTokenId() {
		return ID.Directive;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	public @Nullable List<T> getValue() {
		return this.value;
	}
	
}
