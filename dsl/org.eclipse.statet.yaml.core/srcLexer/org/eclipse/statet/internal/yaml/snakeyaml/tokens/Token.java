/*
 * Copyright (c) 2008, 2025 http://www.snakeyaml.org and others.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eclipse.statet.internal.yaml.snakeyaml.tokens;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.snakeyaml.scanner.SyntaxProblem;


@NonNullByDefault
public abstract class Token {
	
	public enum ID {
		
		Alias("<alias>"), //NOSONAR
		Anchor("<anchor>"), //NOSONAR
		BlockEnd("<block end>"), //NOSONAR
		BlockEntry("-"), //NOSONAR
		BlockMappingStart("<block mapping start>"), //NOSONAR
		BlockSequenceStart("<block sequence start>"), //NOSONAR
		Directive("<directive>"), //NOSONAR
		DocumentEnd("<document end>"), //NOSONAR
		DocumentStart("<document start>"), //NOSONAR
		FlowEntry(","), //NOSONAR
		FlowMappingEnd("}"), //NOSONAR
		FlowMappingStart("{"), //NOSONAR
		FlowSequenceEnd("]"), //NOSONAR
		FlowSequenceStart("["), //NOSONAR
		Key("?"), //NOSONAR
		Scalar("<scalar>"), //NOSONAR
		StreamEnd("<stream end>"), //NOSONAR
		StreamStart("<stream start>"), //NOSONAR
		Tag("<tag>"), //NOSONAR
		Comment("#"),
		Value(":"), //NOSONAR
		
		ERROR("<ERROR>");
		
		
		private final String description;
		
		ID(final String s) {
			this.description = s;
		}
		
		@Override
		public String toString() {
			return this.description;
		}
	}
	
	
	private final int startIndex;
	private final int endIndex;
	
	private final @Nullable SyntaxProblem problem;
	
	
	public Token(final int startIndex, final int endIndex, final @Nullable SyntaxProblem problem) {
		this.startIndex= startIndex;
		this.endIndex= endIndex;
		this.problem= problem;
	}
	
	
	/**
	 * For error reporting.
	 * 
	 * @return ID of this token
	 */
	public abstract Token.ID getTokenId();
	
	
	public int getStartIndex() {
		return this.startIndex;
	}
	
	public int getEndIndex() {
		return this.endIndex;
	}
	
	public int getLength() {
		return this.endIndex - this.startIndex;
	}
	
	
	public @Nullable SyntaxProblem getProblem() {
		return this.problem;
	}
	
	
	@Override
	public String toString() {
		return getTokenId().toString();
	}
	
}
