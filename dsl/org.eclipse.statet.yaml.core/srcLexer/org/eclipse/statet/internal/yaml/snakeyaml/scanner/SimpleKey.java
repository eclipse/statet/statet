/*
 * Copyright (c) 2008, 2025 http://www.snakeyaml.org and others.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.eclipse.statet.internal.yaml.snakeyaml.scanner;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


/**
 * Simple keys treatment.
 * <p>
 * Helper class for {@link ScannerImpl}.
 * </p>
 * 
 * @see ScannerImpl
 */
@NonNullByDefault
final class SimpleKey {
	
	
	private final int tokenNumber;
	private final boolean required;
	private final int index;
	private final int line;
	private final int column;
	private final int mark;
	
	
	public SimpleKey(final int tokenNumber, final boolean required, final int index, final int line, final int column, final int mark) {
		this.tokenNumber = tokenNumber;
		this.required = required;
		this.index = index;
		this.line = line;
		this.column = column;
		this.mark = mark;
	}
	
	
	public int getTokenNumber() {
		return this.tokenNumber;
	}
	
	public int getColumn() {
		return this.column;
	}
	
	public int getMark() {
		return this.mark;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public int getLine() {
		return this.line;
	}
	
	public boolean isRequired() {
		return this.required;
	}
	
	
	@Override
	public String toString() {
		return "SimpleKey - tokenNumber=" + this.tokenNumber + " required=" + this.required + " index="
				+ this.index + " line=" + this.line + " column=" + this.column;
	}
	
}
