/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.snakeyaml.tokens;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.yaml.snakeyaml.scanner.SyntaxProblem;


@NonNullByDefault
public class Dummy extends Token {
	
	
	public Dummy(final int startIndex, final int endIndex, final @Nullable SyntaxProblem problem) {
		super(startIndex, endIndex, problem);
	}
	
	
	@Override
	public ID getTokenId() {
		return ID.ERROR;
	}
	
	
}
