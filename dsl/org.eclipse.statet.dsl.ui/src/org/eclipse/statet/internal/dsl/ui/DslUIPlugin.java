/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.dsl.ui;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.text.ui.settings.JFaceTextStyleManager;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;
import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.dsl.ui.DslUI;
import org.eclipse.statet.dsl.ui.DslUIResources;
import org.eclipse.statet.dsl.ui.text.DslTextStyles;


@NonNullByDefault
public class DslUIPlugin extends AbstractUIPlugin {
	
	
	private static @Nullable DslUIPlugin instance;
	
	/**
	 * Returns the plug-in instance
	 *
	 * @return the shared instance
	 */
	public static DslUIPlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private final List<Disposable> disposables= new ArrayList<>();
	
	private volatile @Nullable JFaceTextStyleManager dslTextStyleManager;
	
	
	public DslUIPlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					log(new Status(IStatus.ERROR, DslUI.BUNDLE_ID,
							"Error occured when dispose module", //$NON-NLS-1$
							e ));
				}
			}
			this.disposables.clear();
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(DslUIResources.OBJ_MAP_ELEMENT_IMAGE_ID, ImageRegistryUtil.T_OBJ, "dsl-map.png");
		util.register(DslUIResources.OBJ_SEQ_ELEMENT_IMAGE_ID, ImageRegistryUtil.T_OBJ, "dsl-seq.png");
		util.register(DslUIResources.OBJ_SCALAR_ELEMENT_IMAGE_ID, ImageRegistryUtil.T_OBJ, "dsl-scalar.png");
		
		util.register(DslUIResources.OBJ_LABEL_IMAGE_ID, ImageRegistryUtil.T_OBJ, "label.png");
	}
	
	
	public PreferenceStoreTextStyleManager<TextAttribute> getDslTextStyleManager() {
		var textStyleManager= this.dslTextStyleManager;
		if (textStyleManager == null) {
			synchronized (this) {
				if ((textStyleManager= this.dslTextStyleManager) == null) {
					if (!this.started) {
						throw new IllegalStateException("Plug-in is not started.");
					}
					textStyleManager= new JFaceTextStyleManager(getPreferenceStore(),
							DslTextStyles.COMMON_DSL_TEXTSTYLE_CONFIG_QUALIFIER );
					PreferencesUtil.getSettingsChangeNotifier().addManageListener(textStyleManager);
					this.dslTextStyleManager= textStyleManager;
				}
			}
		}
		return textStyleManager;
	}
	
}
