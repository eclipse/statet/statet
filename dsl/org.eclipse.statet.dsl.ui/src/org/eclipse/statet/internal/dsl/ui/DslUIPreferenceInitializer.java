/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.dsl.ui;

import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_BOLD_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_COLOR_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_ITALIC_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_STRIKETHROUGH_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_UNDERLINE_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_USE_SUFFIX;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.workbench.ui.IWaThemeConstants;
import org.eclipse.statet.ecommons.workbench.ui.util.ThemeUtil;

import org.eclipse.statet.dsl.ui.DslUI;
import org.eclipse.statet.dsl.ui.text.DslTextStyles;


@NonNullByDefault
public class DslUIPreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public DslUIPreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store= DslUIPlugin.getInstance().getPreferenceStore();
		final IScopeContext scope= DefaultScope.INSTANCE;
		final IEclipsePreferences pref= scope.getNode(DslUI.BUNDLE_ID);
		final ThemeUtil theme= new ThemeUtil();
		
		EditorsUI.useAnnotationsPreferencePage(store);
		EditorsUI.useQuickDiffPreferencePage(store);
		
		pref.put(DslTextStyles.TS_DEFAULT + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_DEFAULT_COLOR));
		pref.putBoolean(DslTextStyles.TS_DEFAULT + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DEFAULT + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DEFAULT + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DEFAULT + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(DslTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_UNDEFINED_COLOR));
		pref.putBoolean(DslTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DOCUMENT_MARKER + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(DslTextStyles.TS_DIRECTIVE + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_PREPROCESSOR_COLOR));
		pref.putBoolean(DslTextStyles.TS_DIRECTIVE + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DIRECTIVE + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DIRECTIVE + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_DIRECTIVE + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(DslTextStyles.TS_INDICATOR + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_OPERATOR_COLOR));
		pref.putBoolean(DslTextStyles.TS_INDICATOR + TEXTSTYLE_BOLD_SUFFIX, true);
		pref.putBoolean(DslTextStyles.TS_INDICATOR + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_INDICATOR + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_INDICATOR + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(DslTextStyles.TS_BRACKET + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_DEFAULT_COLOR));
		pref.putBoolean(DslTextStyles.TS_BRACKET + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_BRACKET + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_BRACKET + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_BRACKET + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		pref.put(DslTextStyles.TS_BRACKET + TEXTSTYLE_USE_SUFFIX, DslTextStyles.TS_INDICATOR);
		
		pref.put(DslTextStyles.TS_KEY + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_STRING_COLOR));
		pref.putBoolean(DslTextStyles.TS_KEY + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_KEY + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_KEY + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_KEY + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(DslTextStyles.TS_TAG + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_DOCU_TAG_COLOR));
		pref.putBoolean(DslTextStyles.TS_TAG + TEXTSTYLE_BOLD_SUFFIX, true);
		pref.putBoolean(DslTextStyles.TS_TAG + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_TAG + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_TAG + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(DslTextStyles.TS_COMMENT + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_COMMENT_COLOR));
		pref.putBoolean(DslTextStyles.TS_COMMENT + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_COMMENT + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_COMMENT + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_COMMENT + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(DslTextStyles.TS_TASK_TAG + TEXTSTYLE_COLOR_SUFFIX, theme.getColorPrefValue(IWaThemeConstants.CODE_COMMENT_TASKTAG_COLOR));
		pref.putBoolean(DslTextStyles.TS_TASK_TAG + TEXTSTYLE_BOLD_SUFFIX, true);
		pref.putBoolean(DslTextStyles.TS_TASK_TAG + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_TASK_TAG + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(DslTextStyles.TS_TASK_TAG + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
	}
	
}
