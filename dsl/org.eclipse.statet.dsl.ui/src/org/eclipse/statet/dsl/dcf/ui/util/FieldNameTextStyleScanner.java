/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.ui.util;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.rules.Token;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.ui.presentation.TextStyleManager;
import org.eclipse.statet.ecommons.text.ui.presentation.TextStyleManager.TextStyleToken;

import org.eclipse.statet.dsl.ui.text.DslTextStyles;


@NonNullByDefault
public class FieldNameTextStyleScanner implements ITokenScanner {
	
	
	private final TextStyleManager<?> textStyles;
	
	private final TextStyleToken<?> keyToken;
	private final TextStyleToken<?> separatorToken;
	
	private IDocument document;
	private int parseStartOffset;
	private int parseEndOffset;
	
	private int separatorOffset;
	
	private int currentOffset;
	private int currentLength;
	
	
	public FieldNameTextStyleScanner(final TextStyleManager<?> textStyles) {
		this.textStyles= textStyles;
		
		this.keyToken= getToken(DslTextStyles.TS_KEY);
		this.separatorToken= getToken(DslTextStyles.TS_INDICATOR);
	}
	
	
	protected TextStyleToken<?> getToken(final String key) {
		return this.textStyles.getToken(key);
	}
	
	
	@Override
	public void setRange(final IDocument document, final int offset, final int length) {
		this.document= document;
		this.parseStartOffset= offset;
		this.parseEndOffset= offset + length;
		
		this.separatorOffset= Integer.MIN_VALUE;
		this.currentOffset= offset;
		this.currentLength= 0;
	}
	
	@Override
	public IToken nextToken() {
		final int currentOffset= this.currentOffset+= this.currentLength;
		this.currentLength= 0;
		if (currentOffset >= this.parseEndOffset) {
			return Token.EOF;
		}
		final int separatorOffset= checkSeparator();
		if (separatorOffset == currentOffset) {
			this.currentLength= 1;
			return this.separatorToken;
		}
		else {
			this.currentLength= ((separatorOffset > currentOffset) ? separatorOffset : this.parseEndOffset)
					- this.currentOffset;
			return this.keyToken;
		}
	}
	
	@Override
	public int getTokenOffset() {
		return this.currentOffset;
	}
	
	@Override
	public int getTokenLength() {
		return this.currentLength;
	}
	
	
	private int checkSeparator() {
		int separatorOffset= this.separatorOffset;
		if (separatorOffset == Integer.MIN_VALUE) {
			separatorOffset= -1;
			try {
				if (this.parseStartOffset < this.parseEndOffset
						&& this.document.getChar(this.parseEndOffset - 1) == ':') {
					separatorOffset= this.parseEndOffset - 1;
				}
			}
			catch (final BadLocationException e) {
			}
			this.separatorOffset= separatorOffset;
		}
		return separatorOffset;
	}
	
}
