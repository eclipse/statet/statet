/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.UIResources;

import org.eclipse.statet.internal.dsl.ui.DslUIPlugin;


@NonNullByDefault
public class DslUIResources extends UIResources {
	
	
	private static final String NS= "org.eclipse.statet.dsl"; //$NON-NLS-1$
	
	public static final String OBJ_SEQ_ELEMENT_IMAGE_ID=        NS + "/images/obj/Seq"; //$NON-NLS-1$
	public static final String OBJ_MAP_ELEMENT_IMAGE_ID=        NS + "/images/obj/Map"; //$NON-NLS-1$
	public static final String OBJ_SCALAR_ELEMENT_IMAGE_ID=     NS + "/images/obj/Scalar"; //$NON-NLS-1$
	
	public static final String OBJ_LABEL_IMAGE_ID=              NS + "/images/obj/Label"; //$NON-NLS-1$
	
	
	static final DslUIResources INSTANCE= new DslUIResources();
	
	
	private DslUIResources() {
		super(DslUIPlugin.getInstance().getImageRegistry());
	}
	
}
