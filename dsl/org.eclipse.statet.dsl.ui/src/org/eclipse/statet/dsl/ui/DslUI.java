/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.ui;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.internal.dsl.ui.DslUIPlugin;


@NonNullByDefault
public class DslUI {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.dsl.ui"; //$NON-NLS-1$
	
	
	public static DslUIResources getUIResources() {
		return DslUIResources.INSTANCE;
	}
	
	public static IPreferenceStore getPreferenceStore() {
		return DslUIPlugin.getInstance().getPreferenceStore();
	}
	
	public static PreferenceStoreTextStyleManager<TextAttribute> getTextStyleManager() {
		return DslUIPlugin.getInstance().getDslTextStyleManager();
	}
	
	
	private DslUI() {
	}
	
}
