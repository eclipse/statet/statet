/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.ui.text;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.ui.DslUI;


@NonNullByDefault
public class DslTextStyles {
	
	
	public static final String COMMON_DSL_TEXTSTYLE_CONFIG_QUALIFIER= DslUI.BUNDLE_ID + "/textstyle/CommonDsl"; //$NON-NLS-1$
	
	
	public static final String PREFIX= "dsl_ts_"; //$NON-NLS-1$
	
	public static final String TS_DEFAULT= PREFIX + "Default"; //$NON-NLS-1$
	
	public static final String TS_DOCUMENT_MARKER= PREFIX + "DocumentMarker"; //$NON-NLS-1$
	public static final String TS_DIRECTIVE= PREFIX + "Directive"; //$NON-NLS-1$
	
	public static final String TS_INDICATOR= PREFIX + "Indicator"; //$NON-NLS-1$
	public static final String TS_BRACKET= PREFIX + "Indicator.Bracket"; //$NON-NLS-1$
	
	public static final String TS_COMMENT= PREFIX + "Comment"; //$NON-NLS-1$
	public static final String TS_TASK_TAG= PREFIX + "TaskTag"; //$NON-NLS-1$
	
	public static final String TS_KEY= PREFIX + "Key"; //$NON-NLS-1$
//	public static final String TS_QUOTED_KEY= PREFIX + "Key.Quoted"; //$NON-NLS-1$
	public static final String TS_TAG= PREFIX + "Tag"; //$NON-NLS-1$
//	public static final String TS_VALUE= PREFIX + "Scalar"; //$NON-NLS-1$
//	public static final String TS_QUOTED_VALUE= PREFIX + "Scalar.Quoted"; //$NON-NLS-1$
	
}
