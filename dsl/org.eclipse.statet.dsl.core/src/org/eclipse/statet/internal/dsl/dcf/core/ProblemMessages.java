/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.dsl.dcf.core;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class ProblemMessages extends NLS {
	
	
	public static String Syntax_Gen_TokenUnknown_message;
	
	public static String Syntax_Field_NameMissing_message;
	public static String Syntax_Field_ValueUnexpected_message;
	
	
	static {
		NLS.initializeMessages(ProblemMessages.class.getName(), ProblemMessages.class);
	}
	private ProblemMessages() {}
	
}
