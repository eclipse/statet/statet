/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.DslParser.NContainerBuilder;
import org.eclipse.statet.dsl.core.source.ast.Record;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;


@NonNullByDefault
public class DcfRecord extends Record {
	
	
	private ImIdentityList<DslAstNode> contentChildren= NO_CHILDREN;
	
	
	DcfRecord(final SourceComponent parent, final int offset) {
		super(parent);
		doSetStartEndOffset(offset);
	}
	
	@Override
	protected void finish(final int endOffset, final NContainerBuilder containerBuilder) {
		super.finish(endOffset, containerBuilder);
		
		this.contentChildren= getChildren();
	}
	
	
	@Override
	public final ImIdentityList<DslAstNode> getContentNodes() {
		return this.contentChildren;
	}
	
}
