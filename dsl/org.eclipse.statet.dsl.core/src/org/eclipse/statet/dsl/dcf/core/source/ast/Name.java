/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.Scalar;


@NonNullByDefault
public class Name extends Scalar {
	
	
	Name(final int statusCode, final DslAstNode parent, final int startOffset, final int endOffset,
			final @Nullable String text) {
		super(statusCode, parent);
		doSetStartEndOffset(startOffset, endOffset);
		doSetText(text);
	}
	
	Name(final int statusCode, final DslAstNode parent, final int startOffset, final int endOffset) {
		super(statusCode, parent);
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	
	@Override
	public @Nullable TextRegion getTextRegion() {
		return this;
	}
	
}
