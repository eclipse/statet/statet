/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.KeyValuePair;


@NonNullByDefault
public class Field extends KeyValuePair {
	
	
	private final int valueIndicatorOffset;
	
	
	public Field(final DslAstNode parent, final int startOffset, final int indicatorOffset) {
		super(parent);
		doSetStartEndOffset(startOffset, indicatorOffset + 1);
		this.valueIndicatorOffset= indicatorOffset;
	}
	
	void finish(final Name nameNode, final Value valueNode, final int endOffset) {
		doSetKeyChild(nameNode);
		doSetValueChild(valueNode);
		finish(endOffset);
	}
	
	
	@Override
	public final int getKeyIndicatorOffset() {
		return NA_OFFSET;
	}
	
	@Override
	public final int getValueIndicatorOffset() {
		return this.valueIndicatorOffset;
	}
	
}
