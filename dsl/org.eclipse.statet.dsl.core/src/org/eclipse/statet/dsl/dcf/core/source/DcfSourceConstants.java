/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_CTX1;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_CTX2;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE2;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.StatusCodes;


@NonNullByDefault
public interface DcfSourceConstants {
	
	
	static int CTX1_RECORD=                     0x1 << SHIFT_CTX1;
	static int CTX1_NAME=                       0x7 << SHIFT_CTX1;
	static int CTX1_VALUE=                      0x8 << SHIFT_CTX1;
	static int CTX12_VALUE_CONT=                    CTX1_VALUE | 0x1 << SHIFT_CTX2;
	
	
	static int TYPE1_SYNTAX_TOKEN_INCORRECT=                StatusCodes.TYPE1_SYNTAX_TOKEN_INCORRECT;
	static int TYPE12_SYNTAX_TOKEN_UNKNOWN=                     TYPE1_SYNTAX_TOKEN_INCORRECT | 0xE << SHIFT_TYPE2 | ERROR;
	
	static int TYPE1_SYNTAX_TOKEN_UNEXPECTED=               StatusCodes.TYPE1_SYNTAX_TOKEN_UNEXPECTED;
	static int TYPE12_SYNTAX_TOKEN_UNEXPECTED=                  TYPE1_SYNTAX_TOKEN_UNEXPECTED | 0x0 << SHIFT_TYPE2 | ERROR;
	
	static int TYPE1_SYNTAX_TOKEN_MISSING=                  StatusCodes.TYPE1_SYNTAX_TOKEN_MISSING;
	static int TYPE12_SYNTAX_NODE_MISSING=                      StatusCodes.TYPE12_SYNTAX_NODE_MISSING | ERROR;
	
}
