/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.CTX12_VALUE_CONT;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_TOKEN_UNKNOWN;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.source.ast.DslParser;
import org.eclipse.statet.dsl.core.source.ast.Dummy;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.dsl.dcf.core.source.DcfLexer;
import org.eclipse.statet.dsl.dcf.core.source.ast.Value.EmptyLineTextRegion;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.ModelManager;


@NonNullByDefault
public class DcfRParser extends DslParser {
	
	
	private final DcfLexer lexer;
	
	private final boolean createText;
	
	private byte nextType;
	private final List<TextRegion> regionsBuilder= new ArrayList<>();
	private final StringBuilder tmpTextBuilder= new StringBuilder();
	
	private @Nullable List<EmbeddingValue> embeddingList;
	
	
	public DcfRParser(final int level, final DcfLexer lexer) {
		super(level);
		this.lexer= lexer;
		this.createText= ((level & AstInfo.DEFAULT_LEVEL_MASK) > AstInfo.LEVEL_MINIMAL);
	}
	
	public DcfRParser() {
		this(ModelManager.MODEL_FILE, new DcfLexer());
	}
	
	
	@Override
	public String getSyntaxLabel() {
		return "DCF (R)";
	}
	
	
	public void setCollectEmebeddedNodes(final boolean enable) {
		this.embeddingList= (enable) ? new ArrayList<>(32) : null;
	}
	
	@SuppressWarnings("null")
	public List<EmbeddingValue> getEmbeddingNodes() {
		return this.embeddingList;
	}
	
	
	@Override
	protected void clearData() {
		super.clearData();
		CollectionUtils.clear(this.embeddingList);
	}
	
	@Override
	protected void initTask() {
		super.initTask();
		this.lexer.reset(getParseInput());
		
		consumeToken();
	}
	
	protected final void consumeToken() {
		this.nextType= this.lexer.next();
	}
	
	
	private void ensureRecordContent(final int startOffset) {
		DcfRecord recordNode;
		switch (getRecordState()) {
		case RECORD_NONE:
			recordNode= new DcfRecord((SourceComponent)getCurrentNode(), startOffset);
			enterNode(recordNode);
			beginRecordContent(recordNode);
			return;
		case RECORD_DIRECTIVES:
			throw new IllegalStateException();
		default:
			return;
		}
	}
	
	
	@Override
	protected void parseInput(final SourceComponent sourceNode) {
		while (true) {
			switch (this.nextType) {
			case DcfLexer.EOF:
				exitToSourceComponent(this.lexer.getOffset());
				return;
			case DcfLexer.LINEBREAK:
				consumeToken();
				continue;
			case DcfLexer.UNKNOWN: {
					ensureRecordContent(this.lexer.getLineStartOffset());
					final Dummy node= new Dummy(TYPE12_SYNTAX_TOKEN_UNKNOWN, getCurrentNode(),
							this.lexer.getOffset(), this.lexer.getEndOffset() );
					consumeToken();
					addChild(node);
					continue;
				}
			case DcfLexer.BLANK_LINE:
				exitToSourceComponent(this.lexer.getOffset());
				consumeToken();
				continue;
			case DcfLexer.FIELD_NAME:
				parseField();
				continue;
			case DcfLexer.VALUE:
			case DcfLexer.VALUE_EMPTY_LINE: {
					ensureRecordContent(this.lexer.getLineStartOffset());
					final int startOffset= this.lexer.getOffset();
					final int textEndOffset= this.lexer.getEndOffset();
					consumeToken();
					final Value node= new Value.Simple(TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX12_VALUE_CONT,
							getCurrentNode(),
							startOffset, this.lexer.getOffset(), textEndOffset );
					consumeToken();
					addChild(node);
					continue;
				}
			default:
				throw new IllegalStateException("type= " + this.nextType); //$NON-NLS-1$
			}
		}
	}
	
	private void parseField() {
//		assert (this.lexer.getType() == DcfLexer.FIELD_NAME);
		final int offset= this.lexer.getOffset();
		ensureRecordContent(offset);
		final var fieldNode= new Field(getCurrentNode(),
				offset, this.lexer.getEndOffset() - 1 );
		final Name nameNode= new Name(this.lexer.getFlags(), fieldNode,
				offset, this.lexer.getEndOffset() - 1,
				this.lexer.getText() );
		consumeToken();
		final var valueRegions= this.regionsBuilder;
		final var text= (this.createText) ? this.tmpTextBuilder : null;
		String firstText= null;
		try {
			int endOffset= this.lexer.getOffset();
			READ_VALUE: while (true) {
				switch (this.nextType) {
				case DcfLexer.LINEBREAK:
					consumeToken();
					continue READ_VALUE;
				case DcfLexer.VALUE:
					valueRegions.add(new BasicTextRegion(this.lexer.getOffset(), this.lexer.getEndOffset()));
					if (text != null) {
						switch (valueRegions.size()) {
						case 1:
							firstText= this.lexer.getText();
							break;
						case 2:
							text.append(firstText);
							text.append('\n');
							text.append(this.lexer.getText());
							break;
						default:
							text.append('\n');
							text.append(this.lexer.getText());
							break;
						}
					}
					consumeToken();
					endOffset= this.lexer.getOffset();
					continue READ_VALUE;
				case DcfLexer.VALUE_EMPTY_LINE:
					valueRegions.add(new EmptyLineTextRegion(this.lexer.getOffset(), this.lexer.getEndOffset()));
					if (text != null) {
						switch (valueRegions.size()) {
						case 1:
							firstText= ""; //$NON-NLS-1$
							break;
						case 2:
							text.append(firstText);
							text.append('\n');
							break;
						default:
							text.append('\n');
							break;
						}
					}
					consumeToken();
					endOffset= this.lexer.getOffset();
					continue READ_VALUE;
				case DcfLexer.EOF:
					break READ_VALUE;
				default:
					break READ_VALUE;
				}
			}
			final var foreignType= getEmbeddingType(nameNode.getText());
			if (foreignType != null) {
				final EmbeddingValue valueNode= switch (valueRegions.size()) {
						case 0 ->	new EmbeddingValue.Empty(fieldNode, endOffset,
										foreignType );
						case 1 ->	new EmbeddingValue.Simple(fieldNode, endOffset,
										foreignType,
										valueRegions.getFirst(), firstText );
						default ->	new EmbeddingValue.Multiline(fieldNode, endOffset,
										foreignType,
										ImCollections.toList(valueRegions),
										(text != null) ? text.toString() : null );
						};
				if (this.embeddingList != null) {
					this.embeddingList.add(valueNode);
				}
				fieldNode.finish(nameNode, valueNode, endOffset);
			}
			else {
				final Value valueNode= switch (valueRegions.size()) {
						case 0 ->	new Value.Empty(fieldNode, endOffset);
						case 1 ->	new Value.Simple(fieldNode, endOffset,
										valueRegions.getFirst(), firstText );
						default ->	new Value.Multiline(fieldNode, endOffset,
										ImCollections.toList(valueRegions),
										(text != null) ? text.toString() : null );
						};
				fieldNode.finish(nameNode, valueNode, endOffset);
			}
			addChild(fieldNode);
		}
		finally {
			valueRegions.clear();
			if (text != null) {
				text.setLength(0);
			}
		}
	}
	
	
	protected @Nullable String getEmbeddingType(final @Nullable String name) {
		return null;
	}
	
}
