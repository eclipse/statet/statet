/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source;

import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.CTX1_NAME;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_NODE_MISSING;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE12;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.ltk.core.source.StatusDetail;


@NonNullByDefault
public class DcfLexer {
	
	
/*[ Types ]====================================================================*/
	
	public static final byte EOF=                           -1;
	
	protected static final byte NONE=                       0x00;
	
	public static final byte LINEBREAK=                     0x01;
	
	public static final byte UNKNOWN=                       0x02;
	
	public static final byte BLANK_LINE=                    0x03;
	
	public static final byte FIELD_NAME=                    0x04;
	public static final byte VALUE=                         0x05;
	public static final byte VALUE_EMPTY_LINE=              0x06;
	
	
/*[ Flags ]====================================================================*/
	
	
	private TextParserInput input;
	
	private byte foundType;
	private int foundFlags;
	private @Nullable StatusDetail foundDetail;
	private int foundOffset;
	private int foundNum;
	private int foundTextNum;
	private int foundLength;
	
	private boolean isCol0;
	private int col0Offset;
	
	
	public DcfLexer(final TextParserInput input) {
		this();
		
		reset(input);
	}
	
	@SuppressWarnings("null")
	public DcfLexer() {
	}
	
	
	public void reset() {
		this.foundType= NONE;
		this.foundFlags= 0;
		this.foundDetail= null;
		this.foundOffset= this.input.getIndex();
		this.foundNum= 0;
		this.foundLength= 0;
		
		this.isCol0= true;
	}
	
	public void reset(final TextParserInput input) {
		this.input= input;
		reset();
	}
	
	public final TextParserInput getInput() {
		return this.input;
	}
	
	
	private void foundEOF(final TextParserInput in,
			final int startOffset) {
		this.foundType= EOF;
		this.foundFlags= 0;
		this.foundDetail= null;
		if (startOffset > 0) {
			this.input.consume(startOffset);
			this.foundOffset= this.input.getIndex();
		}
		this.foundLength= in.getLengthInSource(this.foundNum= 0);
	}
	
	private void foundLinebreak(final TextParserInput in,
			final int startOffset, final int n) {
		this.foundType= LINEBREAK;
		this.foundFlags= 0;
		this.foundDetail= null;
		if (startOffset > 0) {
			this.input.consume(startOffset);
			this.foundOffset= this.input.getIndex();
		}
		this.foundLength= in.getLengthInSource(this.foundNum= n);
		this.isCol0= true;
	}
	
	private void foundBlankLine(final TextParserInput in, final int n) {
		this.foundType= BLANK_LINE;
		this.foundFlags= 0;
		this.foundDetail= null;
		this.foundLength= in.getLengthInSource(this.foundNum= n);
		this.isCol0= true;
	}
	
	private void foundUnknown(final TextParserInput in, final int n) {
		this.foundType= UNKNOWN;
		this.foundFlags= 0;
		this.foundDetail= null;
		this.foundLength= in.getLengthInSource(this.foundNum= n);
	}
	
	private void foundName(final TextParserInput in, final int n) {
		this.foundType= FIELD_NAME;
		this.foundFlags= 0;
		this.foundDetail= null;
		this.foundLength= in.getLengthInSource(this.foundNum= n);
	}
	
	private void foundName(final TextParserInput in, final int flags, final int n) {
		this.foundType= FIELD_NAME;
		this.foundFlags= flags;
		this.foundDetail= null;
		this.foundLength= in.getLengthInSource(this.foundNum= n);
	}
	
	private void foundValue(final TextParserInput in, final byte type,
			final int startOffset, int endOffset, int n) {
		this.foundType= type;
		this.foundFlags= 0;
		this.foundDetail= null;
		if (startOffset > 0) {
			in.consume(startOffset);
			final int diff= in.getIndex() - this.foundOffset;
			this.foundOffset+= diff;
			endOffset-= diff;
			n-= diff;
		}
		this.foundNum= n;
		this.foundLength= in.getLengthInSource(this.foundTextNum= endOffset);
	}
	
	
	public byte next() {
		this.foundType= NONE;
		while (this.foundType == NONE) {
			this.input.consume(this.foundNum);
			this.foundOffset= this.input.getIndex();
			if (this.isCol0) {
				searchRoot0();
			}
			else {
				searchCont();
			}
		}
		return this.foundType;
	}
	
	public final int getType() {
		return this.foundType;
	}
	
	public final int getFlags() {
		return this.foundFlags;
	}
	
	public final @Nullable StatusDetail getStatusDetail() {
		return this.foundDetail;
	}
	
	public final int getOffset() {
		return this.foundOffset;
	}
	
	public final int getLength() {
		return this.foundLength;
	}
	
	public final int getEndOffset() {
		return this.foundOffset + this.foundLength;
	}
	
	public final int getLineStartOffset() {
		return this.col0Offset;
	}
	
	public final @Nullable String getText() {
		switch (this.foundType) {
		case LINEBREAK:
			return "\n"; //$NON-NLS-1$
		case FIELD_NAME:
			return switch (this.foundFlags & TYPE12) {
			case TYPE12_SYNTAX_NODE_MISSING ->
					null;
			default ->
					this.input.getString(0, this.foundNum - 1);
			};
		case VALUE:
			return this.input.getString(0, this.foundTextNum);
		case VALUE_EMPTY_LINE:
			return ""; //$NON-NLS-1$
		case UNKNOWN:
			return this.input.getString(0, this.foundNum);
		default:
			return null;
		}
	}
	
	
	private final void searchRoot0() {
		final TextParserInput in= this.input;
		this.isCol0= false;
		this.col0Offset= in.getIndex();
		int n= 0;
		while (true) {
			switch (this.input.get(n)) {
			case TextParserInput.EOF:
				foundEOF(in, n);
				return;
			case '\r':
				if (this.input.get(n + 1) == '\n') {
					foundBlankLine(in, n + 2);
					return;
				}
				//$FALL-THROUGH$
			case '\n':
				foundBlankLine(in, n + 1);
				return;
			case ' ':
			case '\t':
				n++;
				continue;
			default:
				if (n == 0) {
					readFieldName(in);
					return;
				}
				else {
					readFieldValue(in, n, true);
					return;
				}
			}
		}
	}
	
	private void searchCont() {
		final TextParserInput in= this.input;
		int n= 0;
		while (true) {
			switch (in.get(n)) {
			case TextParserInput.EOF:
				foundEOF(in, n);
				return;
			case '\r':
				if (in.get(n + 1) == '\n') {
					foundLinebreak(in, n, 2);
					return;
				}
				//$FALL-THROUGH$
			case '\n':
				foundLinebreak(in, n, 1);
				return;
			case ' ':
			case '\t':
				n++;
				continue;
			default:
				readFieldValue(in, n, false);
				return;
			}
		}
	}
	
	private void readFieldName(final TextParserInput in) {
		int n= 0;
		while (true) {
			switch (this.input.get(n++)) {
			case TextParserInput.EOF:
			case '\r':
			case '\n':
				foundUnknown(in, n - 1);
				return;
			case ':':
				if (n == 1) {
					foundName(in, TYPE12_SYNTAX_NODE_MISSING | CTX1_NAME, 1);
					return;
				}
				foundName(in, n);
				return;
			default:
				continue;
			}
		}
	}
	
	private void readFieldValue(final TextParserInput in, final int start, final boolean contLine) {
		int n= start;
		int end= n;
		if (contLine) {
			CHECK_EMPTY: while (true) {
				switch (this.input.get(n)) {
				case ' ':
				case '\t':
					n++;
					continue CHECK_EMPTY;
				case 0x0B:
				case 0x0C:
					end= ++n;
					continue CHECK_EMPTY;
				case '.':
					final int dot= n++;
					end= n;
					while (true) {
						switch (this.input.get(n++)) {
						case TextParserInput.EOF:
						case '\r':
						case '\n':
							foundValue(in, VALUE_EMPTY_LINE, dot, dot + 1, n - 1);
							return;
						case ' ':
						case '\t':
							continue;
						case 0x0B:
						case 0x0C:
							end= n;
							continue;
						default:
							end= n;
							break CHECK_EMPTY;
						}
					}
				default:
					break CHECK_EMPTY;
				}
			}
		}
		while (true) {
			switch (this.input.get(n++)) {
			case TextParserInput.EOF:
			case '\r':
			case '\n':
				foundValue(in, VALUE, start, end, n - 1);
				return;
			case ' ':
			case '\t':
				continue;
			default:
				end= n;
				continue;
			}
		}
	}
	
}
