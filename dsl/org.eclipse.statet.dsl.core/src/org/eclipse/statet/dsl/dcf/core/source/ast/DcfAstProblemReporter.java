/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.CTX1_NAME;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.CTX1_VALUE;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_NODE_MISSING;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_TOKEN_UNEXPECTED;
import static org.eclipse.statet.dsl.dcf.core.source.DcfSourceConstants.TYPE12_SYNTAX_TOKEN_UNKNOWN;
import static org.eclipse.statet.ltk.core.StatusCodes.CTX1;
import static org.eclipse.statet.ltk.core.StatusCodes.SEVERITY_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE12;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.DslAstVisitor;
import org.eclipse.statet.dsl.core.source.ast.Dummy;
import org.eclipse.statet.dsl.core.source.ast.Scalar;
import org.eclipse.statet.internal.dsl.dcf.core.ProblemMessages;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;
import org.eclipse.statet.ltk.ast.core.util.AbstractAstProblemReporter;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;


@NonNullByDefault
public class DcfAstProblemReporter extends AbstractAstProblemReporter {
	
	
	private final Visitor visitor= new Visitor();
	
	
	public DcfAstProblemReporter(final String modelTypeId) {
		super(modelTypeId);
	}
	
	
	public void run(final DslAstNode node,
			final SourceContent content,
			final IssueRequestor requestor) {
		try {
			init(content, requestor);
			
			node.acceptInDsl(this.visitor);
			
			flush();
		}
		catch (final OperationCanceledException | InvocationTargetException e) {}
		finally {
			clear();
		}
	}
	
	
	protected void handleCommonCodes(final DslAstNode node, final int code)
			throws BadLocationException, InvocationTargetException {
		TYPE12: switch (code & TYPE12) {
		
		case TYPE12_SYNTAX_TOKEN_UNKNOWN:
			addProblem(Problem.SEVERITY_ERROR, code, getMessageBuilder().bind(
					ProblemMessages.Syntax_Gen_TokenUnknown_message,
							getMessageUtil().getShortQuoteText(node, 0) ),
					node.getStartOffset(), node.getEndOffset() );
			return;
			
		default:
			break TYPE12;
		}
		
		super.handleCommonCodes(node, code);
	}
	
	
	private class Visitor extends DslAstVisitor {
		
		
		@Override
		public void visit(final Scalar node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & (TYPE12 | CTX1)) {
					case TYPE12_SYNTAX_NODE_MISSING | CTX1_NAME:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_Field_NameMissing_message,
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE12_SYNTAX_TOKEN_UNEXPECTED | CTX1_VALUE:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Syntax_Field_ValueUnexpected_message,
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			else if ((node.getStatusCode() & SEVERITY_IN_CHILD) != 0
					&& node instanceof EmbeddingAstNode ) {
				handleEmbeddedNode((EmbeddingAstNode)node);
			}
		}
		
		@Override
		public void visit(final Dummy node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					handleCommonCodes(node, code);
					break STATUS;
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
	}
	
	
	protected void handleEmbeddedNode(final EmbeddingAstNode embeddingNode) {
	}
	
}
