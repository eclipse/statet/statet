/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.Scalar;


@NonNullByDefault
public abstract class Value extends Scalar {
	
	
	public static interface EmptyLine extends TextRegion {
	}
	
	static class EmptyLineTextRegion extends BasicTextRegion implements EmptyLine {
		
		
		public EmptyLineTextRegion(final int startOffset, final int endOffset) {
			super(startOffset, endOffset);
		}
		
	}
	
	
	static class Simple extends Value {
		
		
		private final ImList<TextRegion> fragments;
		
		
		Simple(final int statusCode, final DslAstNode parent,
				final int startOffset, final int endOffset, final int textEndOffset) {
			super(statusCode, parent);
			doSetStartEndOffset(startOffset, endOffset);
			this.fragments= ImCollections.newList((endOffset == textEndOffset) ?
					this : new BasicTextRegion(startOffset, textEndOffset) );
		}
		
		Simple(final DslAstNode parent, final int endOffset,
				final TextRegion region, final @Nullable String text) {
			super(parent);
			doSetStartEndOffset(parent.getEndOffset(), endOffset);
			doSetText(text);
			this.fragments= ImCollections.newList(region);
		}
		
		
		@Override
		public ImList<TextRegion> getTextRegions() {
			return this.fragments;
		}
		
		@Override
		public @Nullable TextRegion getTextRegion() {
			return this.fragments.getFirst();
		}
		
	}
	
	static class Empty extends Value {
		
		private static final ImList<TextRegion> FRAGMENTS= ImCollections.emptyList();
		
		
		Empty(final DslAstNode parent, final int endOffset) {
			super(parent);
			doSetStartEndOffset(parent.getEndOffset(), endOffset);
			doSetText(""); //$NON-NLS-1$
		}
		
		
		@Override
		public ImList<TextRegion> getTextRegions() {
			return FRAGMENTS;
		}
		
	}
	
	static class Multiline extends Value {
		
		
		private final ImList<TextRegion> fragments;
		
		
		Multiline(final DslAstNode parent, final int endOffset,
				final ImList<TextRegion> fragments, final @Nullable String text) {
			super(parent);
			doSetStartEndOffset(parent.getEndOffset(), endOffset);
			doSetText(text);
			this.fragments= fragments;
		}
		
		
		@Override
		public ImList<TextRegion> getTextRegions() {
			return this.fragments;
		}
		
	}
	
	
	Value(final int statusCode, final DslAstNode parent) {
		super(statusCode, parent);
	}
	
	Value(final DslAstNode parent) {
		super(parent);
	}
	
	
	public abstract ImList<TextRegion> getTextRegions();
	
	
}
