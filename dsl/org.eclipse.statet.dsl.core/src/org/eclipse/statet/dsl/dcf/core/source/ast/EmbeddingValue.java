/*=============================================================================#
 # Copyright (c) 2023, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.dcf.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;


@NonNullByDefault
public abstract class EmbeddingValue extends Value implements EmbeddingAstNode {
	
	
	static class Simple extends EmbeddingValue {
		
		
		private final ImList<TextRegion> fragments;
		
		
		Simple(final DslAstNode parent, final int endOffset,
				final String foreignType,
				final TextRegion region, final @Nullable String text) {
			super(parent, foreignType);
			doSetStartEndOffset(parent.getEndOffset(), endOffset);
			doSetText(text);
			this.fragments= ImCollections.newList(region);
		}
		
		
		@Override
		public ImList<TextRegion> getTextRegions() {
			return this.fragments;
		}
		
		@Override
		public @Nullable TextRegion getTextRegion() {
			return this.fragments.getFirst();
		}
		
	}
	
	static class Empty extends EmbeddingValue {
		
		private static final ImList<TextRegion> FRAGMENTS= ImCollections.emptyList();
		
		
		Empty(final DslAstNode parent, final int endOffset,
				final String foreignType) {
			super(parent, foreignType);
			doSetStartEndOffset(parent.getEndOffset(), endOffset);
			doSetText(""); //$NON-NLS-1$
		}
		
		
		@Override
		public ImList<TextRegion> getTextRegions() {
			return FRAGMENTS;
		}
		
	}
	
	static class Multiline extends EmbeddingValue {
		
		
		private final ImList<TextRegion> fragments;
		
		
		Multiline(final DslAstNode parent, final int endOffset,
				final String foreignType,
				final ImList<TextRegion> fragments, final @Nullable String text) {
			super(parent, foreignType);
			doSetStartEndOffset(parent.getEndOffset(), endOffset);
			doSetText(text);
			this.fragments= fragments;
		}
		
		
		@Override
		public ImList<TextRegion> getTextRegions() {
			return this.fragments;
		}
		
	}
	
	
	private final String foreignType;
	
	private @Nullable AstNode foreignNode;
	
	
	EmbeddingValue(final int statusCode, final DslAstNode parent,
			final String foreignType) {
		super(statusCode, parent);
		this.foreignType= foreignType;
	}
	
	EmbeddingValue(final DslAstNode parent,
			final String foreignType) {
		super(parent);
		this.foreignType= foreignType;
	}
	
	
	@Override
	public String getForeignTypeId() {
		return this.foreignType;
	}
	
	@Override
	public int getEmbedDescr() {
		return EMBED_CHUNK;
	}
	
	@Override
	public void setForeignNode(final @Nullable AstNode node) {
		this.foreignNode= node;
		if (node != null) {
			doSetStatusSeverityOfChild(node.getStatusCode());
		}
	}
	
	@Override
	public @Nullable AstNode getForeignNode() {
		return this.foreignNode;
	}
	
	
	@Override
	@SuppressWarnings("null")
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		if (this.foreignNode != null) {
			this.foreignNode.accept(visitor);
		}
	}
	
}
