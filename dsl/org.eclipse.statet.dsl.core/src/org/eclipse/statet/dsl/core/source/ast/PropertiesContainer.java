/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.Asts;


@NonNullByDefault
public class PropertiesContainer extends DslAstNode {
	
	
	ImIdentityList<DslAstNode> properties= ImCollections.emptyIdentityList();
	
	DslAstNode nodeChild;
	
	
	@SuppressWarnings("null")
	public PropertiesContainer(final DslAstNode parent,
			final int startOffset, final int endOffset) {
		super(parent);
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	protected void doSetNodeChild(final DslAstNode node) {
		this.nodeChild= node;
	}
	
	@Override
	protected void finish(final int endOffset) {
		super.finish(endOffset);
		doSetEndOffsetMin(this.nodeChild.getEndOffset());
	}
	
	@Override
	protected boolean hasErrorInChild() {
		for (final DslAstNode property : this.properties) {
			if (Asts.hasErrors(property)) {
				return true;
			}
		}
		return (Asts.hasErrors(this.nodeChild));
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.PROPERTIES_CONTAINER;
	}
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return this.properties.size() + 1;
	}
	
	public final ImIdentityList<DslAstNode> getProperties() {
		return this.properties;
	}
	
	public final DslAstNode getNode() {
		return this.nodeChild;
	}
	
	@Override
	public final DslAstNode getChild(final int index) {
		if (index == this.properties.size()) {
			return this.nodeChild;
		}
		return this.properties.get(index);
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (this.nodeChild == child) {
			return this.properties.size();
		}
		return this.properties.indexOf(child);
	}
	
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		for (final DslAstNode property : this.properties) {
			visitor.visit(property);
		}
		visitor.visit(this.nodeChild);
	}
	
	@Override
	public final void acceptInDsl(final DslAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInDslChildren(final DslAstVisitor visitor) throws InvocationTargetException {
		for (final DslAstNode property : this.properties) {
			property.acceptInDsl(visitor);
		}
		this.nodeChild.acceptInDsl(visitor);
	}
	
}
