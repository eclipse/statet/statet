/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;


/**
 * Definitions of config file AST node types
 */
public enum NodeType {
	
	
	SOURCELINES,
	RECORD,
	MARKER,
	DIRECTIVE,
	COMMENT,
	ERROR,
	
	PROPERTIES_CONTAINER,
	TAG,
	ANCHOR,
	
	SEQ,
	MAP,
	SEQ_ENTRY,
	KEY_VALUE_ENTRY,
	
	SCALAR,
	
	ALIAS;
	
}
