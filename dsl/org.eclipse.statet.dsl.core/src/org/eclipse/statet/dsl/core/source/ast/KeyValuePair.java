/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.Asts;


@NonNullByDefault
public abstract class KeyValuePair extends DslAstNode {
	
	
	DslAstNode keyChild;
	
	DslAstNode valueChild;
	
	
	@SuppressWarnings("null")
	protected KeyValuePair(final int statusCode, final DslAstNode parent) {
		super(statusCode, parent);
	}
	
	@SuppressWarnings("null")
	protected KeyValuePair(final DslAstNode parent) {
		super(parent);
	}
	
	protected final void doSetKeyChild(final DslAstNode node) {
		this.keyChild= node;
	}
	
	protected final void doSetValueChild(final DslAstNode node) {
		this.valueChild= node;
	}
	
	@Override
	protected void finish(final int endOffset) {
		super.finish(endOffset);
		doSetEndOffsetMin(Math.max(endOffset, this.valueChild.getEndOffset()));
	}
	
	@Override
	protected boolean hasErrorInChild() {
		return (Asts.hasErrors(this.keyChild)
				|| Asts.hasErrors(this.valueChild) );
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.KEY_VALUE_ENTRY;
	}
	
	
	public abstract int getKeyIndicatorOffset();
	
	public abstract int getValueIndicatorOffset();
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 2;
	}
	
	public final DslAstNode getKey() {
		return this.keyChild;
	}
	
	public final DslAstNode getValue() {
		return this.valueChild;
	}
	
	@Override
	public final DslAstNode getChild(final int index) {
		return switch (index) {
		case 0 -> this.keyChild;
		case 1 -> this.valueChild;
		default ->
				throw new IndexOutOfBoundsException(Integer.toString(index));
		};
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (child == this.keyChild) {
			return 0;
		}
		if (child == this.valueChild) {
			return 1;
		}
		return -1;
	}
	
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.keyChild);
		visitor.visit(this.valueChild);
	}
	
	@Override
	public final void acceptInDsl(final DslAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInDslChildren(final DslAstVisitor visitor) throws InvocationTargetException {
		this.keyChild.acceptInDsl(visitor);
		this.valueChild.acceptInDsl(visitor);
	}
	
}
