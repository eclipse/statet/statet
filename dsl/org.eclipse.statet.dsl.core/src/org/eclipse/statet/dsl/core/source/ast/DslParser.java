/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.ltk.ast.core.AstNode.NA_OFFSET;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE1_RUNTIME_ERROR;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.IntArrayList;
import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.ErrorStatus;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.dsl.core.DslCore;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.source.StatusDetail;


@NonNullByDefault
public abstract class DslParser {
	
	
	public static final int COLLECT_COMMENTS= 1 << 0;
	
	protected static final byte RECORD_NONE= 0;
	protected static final byte RECORD_DIRECTIVES= 1;
	protected static final byte RECORD_CONTENT= 2;
	
	
	public static final class NContainerBuilder {
		
		public final List<DslAstNode> children= new ArrayList<>(8);
		
		public final IntList sepOffsets= new IntArrayList(8);
		
		
		void clear() {
			this.children.clear();
			this.sepOffsets.clear();
		}
		
	}
	
	
	private final int level;
	
	private TextParserInput parseInput;
	
	private @Nullable DslAstNode currentNode;
	
	private int depth;
	private final List<NContainerBuilder> containerStack= new ArrayList<>();
	private byte recordState;
	
	private int commentsLevel;
	private final List<Comment> comments= new ArrayList<>();
	
	
	public DslParser(final int level) {
		this.level= level;
	}
	
	
	public abstract String getSyntaxLabel();
	
	
	public final int getCommentLevel() {
		return this.commentsLevel;
	}
	
	public void setCommentLevel(int level) {
		if ((level & COLLECT_COMMENTS) == 0) {
			level= 0;
		}
		else {
			level= (level & (COLLECT_COMMENTS));
		}
		this.commentsLevel= level;
	}
	
	
	protected final TextParserInput getParseInput() {
		return this.parseInput;
	}
	
	protected void initInput(final TextParserInput input, final @Nullable AstNode parent) {
		this.parseInput= nonNullAssert(input);
	}
	
	@SuppressWarnings("null")
	protected void clearInput() {
		this.parseInput= null;
		
		while (this.depth >= 0) {
			if (this.depth < this.containerStack.size()) {
				final var builder= this.containerStack.get(this.depth);
				builder.clear();
			}
			this.depth--;
		}
	}
	
	protected void clearData() {
		if (this.commentsLevel != 0) {
			this.comments.clear();
		}
	}
	
	protected void handleParseError(final Exception e) {
		CommonsRuntime.log(new ErrorStatus(DslCore.BUNDLE_ID,
				String.format("An error occured while parsing %1$s source code. Input:\n%2$s",
						getSyntaxLabel(), this.parseInput.toString() ),
				e ));
		
		clearData();
	}
	
	
	protected final int getDepth() {
		return this.depth;
	}
	
	protected final DslAstNode getCurrentNode() {
		return this.currentNode;
	}
	
	protected final NContainerBuilder getCurrentContainerBuilder() {
		return this.containerStack.get(this.depth);
	}
	
	protected final byte getRecordState() {
		return this.recordState;
	}
	
	protected boolean isNodeProperty(final DslAstNode node) {
		return switch (node.getNodeType()) {
				case TAG -> true;
				default -> false;
				};
	}
	
	protected void enterNode(final NContainer node) {
		if (this.depth >= 0) {
			addChild(node);
		}
		
		this.depth++;
		this.currentNode= node;
		
		while (this.depth >= this.containerStack.size()) {
			this.containerStack.add(new NContainerBuilder());
		}
	}
	
	protected void enterNode(final PropertiesContainer node) {
		addChild(node);
		
		this.depth++;
		this.currentNode= node;
	}
	
	protected void enterNode(final KeyValuePair node) {
		addChild(node);
		
		this.depth++;
		this.currentNode= node;
	}
	
	protected void enterNode(final SeqEntry node) {
		addChild(node);
		
		this.depth++;
		this.currentNode= node;
	}
	
	protected void addChild(final DslAstNode node) {
		final DslAstNode currentNode= nonNullAssert(this.currentNode);
		switch (currentNode.getNodeType()) {
		case KEY_VALUE_ENTRY: {
				final var entryNode= (KeyValuePair)currentNode;
				if (entryNode.keyChild == null && entryNode.getValueIndicatorOffset() == NA_OFFSET) {
					entryNode.keyChild= node;
				}
				else {
					entryNode.valueChild= node;
				}
				return;
			}
		case SEQ_ENTRY: {
				final var entryNode= (SeqEntry)currentNode;
				entryNode.valueChild= node;
				return;
			}
		case PROPERTIES_CONTAINER: {
				final var propertiesNode= (PropertiesContainer)currentNode;
				if (isNodeProperty(node)) {
					propertiesNode.properties= ImCollections.addElement(propertiesNode.properties, node);
				}
				else {
					propertiesNode.nodeChild= node;
				}
				return;
			}
		default: {
				final var container= (NContainer)currentNode;
				final var builder= this.containerStack.get(this.depth);
				container.add(builder, node);
				return;
			}
		}
	}
	
	protected void exit(final int offset) {
		finish(offset);
		this.currentNode= this.currentNode.dslParent;
		this.depth--;
		
		checkExit();
	}
	
	protected final boolean exitTo(final NodeType type1) {
		while (this.depth > 1) {
			final var currentType= this.currentNode.getNodeType();
			if (currentType == type1) {
				return true;
			}
			exit(NA_OFFSET);
		}
		return false;
	}
	
	protected final boolean exitTo(final NodeType type1, final NodeType type2) {
		while (this.depth > 1) {
			final var currentType= this.currentNode.getNodeType();
			if (currentType == type1 || currentType == type2) {
				return true;
			}
			exit(NA_OFFSET);
		}
		return false;
	}
	
	protected final boolean exitTo(final Class<?> type1) {
		while (this.depth > 1) {
			final var currentType= this.currentNode.getClass();
			if (currentType == type1) {
				return true;
			}
			exit(NA_OFFSET);
		}
		return false;
	}
	
	protected final boolean exitTo(final Class<?> type1, final Class<?> type2) {
		while (this.depth > 1) {
			final var currentType= this.currentNode.getClass();
			if (currentType == type1 || currentType == type2) {
				return true;
			}
			exit(NA_OFFSET);
		}
		return false;
	}
	
	protected void checkExit() {
		if (this.depth > 1) {
			switch (this.currentNode.getNodeType()) {
			case PROPERTIES_CONTAINER:
				if (((PropertiesContainer)this.currentNode).nodeChild != null) {
					exit(NA_OFFSET);
				}
				break;
			case KEY_VALUE_ENTRY:
				if (((KeyValuePair)this.currentNode).valueChild != null) {
					exit(NA_OFFSET);
				}
				break;
			case SEQ_ENTRY:
				if (((SeqEntry)this.currentNode).valueChild != null) {
					exit(NA_OFFSET);
				}
				break;
			default:
				break;
			}
		}
	}
	
	private void finish(final int endOffset) {
		final DslAstNode currentNode= nonNullAssert(this.currentNode);
		switch (currentNode.getNodeType()) {
		case KEY_VALUE_ENTRY:
		case SEQ_ENTRY:
		case PROPERTIES_CONTAINER:
				currentNode.finish(endOffset);
				return;
		case RECORD:
			this.recordState= RECORD_NONE;
			//$FALL-THROUGH$
		default: {
				final var container= (NContainer)currentNode;
				if (endOffset != NA_OFFSET) {
					container.setEndOffset(endOffset);
				}
				
				final var builder= this.containerStack.get(this.depth);
				container.finish(endOffset, builder);
				builder.clear();
			}
		}
	}
	
	
	@SuppressWarnings("null")
	protected DslAstNode exitToRecord() {
		while (this.depth > 1) {
			exit(NA_OFFSET);
		}
		return this.currentNode;
	}
	
	@SuppressWarnings("null")
	protected DslAstNode exitToSourceComponent(final int offset) {
		while (this.depth > 1) {
			exit(NA_OFFSET);
		}
		if (this.depth > 0) {
			exit(offset);
		}
		return this.currentNode;
	}
	
	
	public SourceComponent parseSourceUnit(final TextParserInput input) {
		initInput(input, null);
		try {
			initTask();
			
			final SourceComponent sourceNode= parseSourceUnit(null, false);
			
			return sourceNode;
		}
		catch (final Exception e) {
			handleParseError(e);
			return createErrorSourceComponent(null);
		}
		finally {
			clearInput();
		}
	}
	
	public SourceComponent parseSourceUnit(final String input) {
		return parseSourceUnit(new StringParserInput(input).init());
	}
	
	public SourceComponent parseSourceFragment(final TextParserInput input,
			final @Nullable AstNode parent, final boolean expand) {
		initInput(input, parent);
		try {
			initTask();
			
			final SourceComponent sourceNode= parseSourceUnit(parent, expand);
			
			return sourceNode;
		}
		catch (final Exception e) {
			handleParseError(e);
			return createErrorSourceComponent(parent);
		}
		finally {
			clearInput();
		}
	}
	
	
	protected void initTask() {
		clearData();
		this.depth= -1;
		this.recordState= RECORD_NONE;
	}
	
	protected SourceComponent parseSourceUnit(final @Nullable AstNode parent, final boolean expand) {
		final SourceComponent node= new SourceComponent(parent);
		
		enterNode(node);
		parseInput(node);
		while (this.depth >= 0) {
			exit(NA_OFFSET);
		}
		
		if (this.commentsLevel != 0) {
			node.comments= ImCollections.toList(this.comments);
		}
		
		if (expand) {
			node.setStartEndOffset(this.parseInput.getStartIndex(), this.parseInput.getStopIndex());
		}
		else if (node.getChildCount() > 0) {
			node.setStartEndOffset(node.getChild(0).getStartOffset(), node.getChild(node.getChildCount() - 1).getEndOffset());
		}
		else {
			node.setStartEndOffset(this.parseInput.getStartIndex());
		}
		return node;
	}
	
	private SourceComponent createErrorSourceComponent(final @Nullable AstNode parent) {
		final int startOffset= this.parseInput.getStartIndex();
		int endOffset= this.parseInput.getStopIndex();
		if (endOffset < startOffset) {
			endOffset= startOffset;
		}
		final SourceComponent node= new SourceComponent(TYPE1_RUNTIME_ERROR, parent,
				startOffset, endOffset );
		if (this.commentsLevel != 0) {
			node.comments= ImCollections.emptyList();
		}
		return node;
	}
	
	
	protected abstract void parseInput(SourceComponent sourceComponent);
	
	
	protected void beginRecordDirectives(final Record recordNode) {
		this.recordState= RECORD_DIRECTIVES;
	}
	
	protected void beginRecordContent(final Record recordNode) {
		this.recordState= RECORD_CONTENT;
	}
	
	protected void endRecord(final Record recordNode, final int offset) {
		exit(offset);
//		this.recordState= RECORD_NONE;
	}
	
	protected void addComment(final Comment comment) {
		this.comments.add(comment);
	}
	
	protected void setStatus(final DslAstNode node, final int statusCode,
			final @Nullable StatusDetail statusDetail) {
		node.setStatus(statusCode);
		if (statusDetail != null) {
			node.addAttachment(statusDetail);
		}
	}
	
	protected void setStatus(final DslAstNode node, final int statusCode) {
		node.setStatus(statusCode);
	}
	
}
