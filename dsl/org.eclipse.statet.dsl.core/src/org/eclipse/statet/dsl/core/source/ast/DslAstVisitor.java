/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class DslAstVisitor {
	
	
	public void visit(final SourceComponent node) throws InvocationTargetException {
		node.acceptInDslChildren(this);
	}
	
	public void visit(final Record node) throws InvocationTargetException {
		node.acceptInDslChildren(this);
	}
	
	public void visit(final Marker node) throws InvocationTargetException {
	}
	
	public void visit(final Directive node) throws InvocationTargetException {
	}
	
	public void visit(final PropertiesContainer node) throws InvocationTargetException {
		node.acceptInDslChildren(this);
	}
	
	public void visit(final Tag node) throws InvocationTargetException {
	}
	
	public void visit(final Anchor node) throws InvocationTargetException {
	}
	
	public void visit(final Collection node) throws InvocationTargetException {
		node.acceptInDslChildren(this);
	}
	
	public void visit(final SeqEntry node) throws InvocationTargetException {
		node.acceptInDslChildren(this);
	}
	
	public void visit(final KeyValuePair node) throws InvocationTargetException {
		node.acceptInDslChildren(this);
	}
	
	public void visit(final Scalar node) throws InvocationTargetException {
	}
	
	public void visit(final Alias node) throws InvocationTargetException {
	}
	
	public void visit(final Comment node) throws InvocationTargetException {
	}
	
	public void visit(final Dummy node) throws InvocationTargetException {
	}
	
}
