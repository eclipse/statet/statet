/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public class Directive extends DslAstNode {
	
	
	private final String name;
	
	
	public Directive(final DslAstNode parent, final int startOffset, final int endOffset,
			final String name) {
		super(parent);
		doSetStartEndOffset(startOffset, endOffset);
		this.name= name;
	}
	
	protected Directive(final DslAstNode parent,
			final String name) {
		super(parent);
		this.name= name;
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.DIRECTIVE;
	}
	
	
	@Override
	public final @Nullable String getText() {
		return this.name;
	}
	
	
	@Override
	public final boolean hasChildren() {
		return false;
	}
	
	@Override
	public final int getChildCount() {
		return 0;
	}
	
	@Override
	public final DslAstNode getChild(final int index) {
		throw new IndexOutOfBoundsException();
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		return -1;
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
	}
	
	@Override
	public final void acceptInDsl(final DslAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInDslChildren(final DslAstVisitor visitor) throws InvocationTargetException {
	}
	
}
