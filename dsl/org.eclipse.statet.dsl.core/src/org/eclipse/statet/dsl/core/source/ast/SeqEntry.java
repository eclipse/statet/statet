/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nullable;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.Asts;


@NonNullByDefault
public abstract class SeqEntry extends DslAstNode {
	
	
	DslAstNode valueChild;
	
	
	@SuppressWarnings("null")
	protected SeqEntry(final int statusCode, final DslAstNode parent) {
		super(statusCode, parent);
	}
	
	@SuppressWarnings("null")
	protected SeqEntry(final DslAstNode parent) {
		super(parent);
	}
	
	@Override
	protected void finish(final int endOffset) {
		if (nullable(this.valueChild) == null) {
			this.valueChild= createAbsentValueNode(endOffset);
		}
		
		super.finish(endOffset);
		doSetEndOffsetMin(this.valueChild.getEndOffset());
	}
	
	protected abstract DslAstNode createAbsentValueNode(int endOffset);
	
	@Override
	protected boolean hasErrorInChild() {
		return Asts.hasErrors(this.valueChild);
	}
	
	
	@Override
	public final NodeType getNodeType() {
		return NodeType.SEQ_ENTRY;
	}
	
	
	public abstract int getValueIndicatorOffset();
	
	
	@Override
	public final boolean hasChildren() {
		return true;
	}
	
	@Override
	public final int getChildCount() {
		return 1;
	}
	
	public final DslAstNode getValue() {
		return this.valueChild;
	}
	
	@Override
	public final DslAstNode getChild(final int index) {
		return switch (index) {
		case 0 -> this.valueChild;
		default ->
				throw new IndexOutOfBoundsException(Integer.toString(index));
		};
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		if (child == this.valueChild) {
			return 0;
		}
		return -1;
	}
	
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.valueChild);
	}
	
	@Override
	public final void acceptInDsl(final DslAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInDslChildren(final DslAstVisitor visitor) throws InvocationTargetException {
		this.valueChild.acceptInDsl(visitor);
	}
	
}
