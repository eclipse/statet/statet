/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.model;

import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.text.Position;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.dsl.core.source.ast.DslAstNode;


@NonNullByDefault
public abstract class DslLabelAccess extends DslElementName {
	
	
	public static final Comparator<DslLabelAccess> NAME_POSITION_COMPARATOR= new Comparator<>() {
		@Override
		public int compare(final DslLabelAccess o1, final DslLabelAccess o2) {
			return o1.getNameNode().getStartOffset() - o2.getNameNode().getStartOffset();
		}
	};
	
	
	public static Position getTextPosition(final DslAstNode node) {
		return new Position(node.getStartOffset(), node.getLength());
	}
	
	public static TextRegion getTextRegion(final DslAstNode node) {
		return new BasicTextRegion(node);
	}
	
	
	protected DslLabelAccess() {
	}
	
	
	public abstract DslAstNode getNode();
	
	public abstract DslAstNode getNameNode();
	
	public abstract List<? extends DslLabelAccess> getAllInUnit();
	
	
	public abstract boolean isWriteAccess();
	
}
