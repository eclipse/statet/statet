/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR_IN_CHILD;
import static org.eclipse.statet.ltk.core.StatusCodes.SUBSEQUENT;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.impl.AbstractAstNode;


@NonNullByDefault
public abstract class DslAstNode extends AbstractAstNode
		implements AstNode {
	
	
	protected static final ImIdentityList<DslAstNode> NO_CHILDREN= ImCollections.emptyIdentityList();
	
	
	@Nullable DslAstNode dslParent;
	
	
	DslAstNode(final int statusCode, final @Nullable DslAstNode parent) {
		super(statusCode);
		this.dslParent= parent;
	}
	
	DslAstNode(final @Nullable DslAstNode parent) {
		super();
		this.dslParent= parent;
	}
	
	DslAstNode() {
		super();
	}
	
	
	public abstract NodeType getNodeType();
	
	public char getOperator() {
		return 0;
	}
	
	
	@Override
	public @Nullable AstNode getParent() {
		return this.dslParent;
	}
	
	public final @Nullable DslAstNode getDslParent() {
		return this.dslParent;
	}
	
	@Override
	public abstract boolean hasChildren();
	@Override
	public abstract int getChildCount();
	@Override
	public abstract DslAstNode getChild(final int index);
	@Override
	public abstract int getChildIndex(AstNode child);
	
	
	public abstract void acceptInDsl(DslAstVisitor visitor) throws InvocationTargetException;
	
	public abstract void acceptInDslChildren(DslAstVisitor visitor) throws InvocationTargetException;
	
	
	protected void finish(final int endOffset) {
		if (hasErrorInChild()) {
			doSetStatusSeverityInChild(ERROR_IN_CHILD);
		}
	}
	
	protected boolean hasErrorInChild() {
		return false;
	}
	
	final void setStatus(final int statusCode) {
		doSetStatusCode(statusCode);
	}
	
	final void setStatusThis(final int statusCode) {
		doSetStatusThis(statusCode);
	}
	
	final void clearStatusSeverityInChild() {
		doClearStatusSeverityInChild();
	}
	
	final void setStatusSeverityInChild(final int statusCode) {
		doSetStatusSeverityInChild(statusCode);
	}
	
	final void setStatusSubsequent() {
		doAddStatusFlag(SUBSEQUENT);
	}
	
	final void setStartOffset(final int offset) {
		doSetStartOffset(offset);
	}
	
	final void setEndOffset(final int offset) {
		doSetEndOffset(offset);
	}
	
	final void setStartEndOffset(final int startOffset, final int endOffset) {
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	final void setStartEndOffset(final int offset) {
		doSetStartEndOffset(offset);
	}
	
	
	static void acceptInDsl(final DslAstVisitor visitor, final @Nullable DslAstNode node) throws InvocationTargetException {
		if (node != null) {
			node.acceptInDsl(visitor);
		}
	}
	
	
	protected final static void setStatusThis(final DslAstNode node, final int statusCode) {
		node.setStatusThis(statusCode);
	}
	
}
