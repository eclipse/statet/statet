/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.dsl.core.source.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.Asts;


@NonNullByDefault
abstract class NContainer extends DslAstNode {
	
	
	ImIdentityList<DslAstNode> children= NO_CHILDREN;
	
	
	NContainer(final int statusCode, final @Nullable DslAstNode parent) {
		super(statusCode, parent);
	}
	
	NContainer(final @Nullable DslAstNode parent) {
		super(parent);
	}
	
	NContainer() {
		super();
	}
	
	protected void add(final DslParser.NContainerBuilder containerBuilder, final DslAstNode child) {
		containerBuilder.children.add(child);
	}
	
	protected void finish(final int endOffset, final DslParser.NContainerBuilder containerBuilder) {
		this.children= ImCollections.toIdentityList(containerBuilder.children);
		
		super.finish(endOffset);
		if (!this.children.isEmpty()) {
			doSetEndOffsetMin(this.children.getLast().getEndOffset());
		}
	}
	
	@Override
	protected boolean hasErrorInChild() {
		for (final var child : this.children) {
			if (Asts.hasErrors(child)) {
				return true;
			}
		}
		return false;
	}
	
	
	@Override
	public final boolean hasChildren() {
		return !this.children.isEmpty();
	}
	
	@Override
	public final int getChildCount() {
		return this.children.size();
	}
	
	protected final ImIdentityList<DslAstNode> getChildren() {
		return this.children;
	}
	
	@Override
	public final DslAstNode getChild(final int index) {
		return this.children.get(index);
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		return this.children.indexOf(child);
	}
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		for (final DslAstNode child : this.children) {
			visitor.visit(child);
		}
	}
	
	
	@Override
	public final void acceptInDslChildren(final DslAstVisitor visitor) throws InvocationTargetException {
		for (final DslAstNode child : this.children) {
			child.acceptInDsl(visitor);
		}
	}
	
}
