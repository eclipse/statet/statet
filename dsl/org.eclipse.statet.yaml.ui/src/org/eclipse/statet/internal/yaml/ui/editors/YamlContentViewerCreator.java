/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.editors;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.IViewerCreator;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ltk.ui.compare.CompareTextViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.yaml.ui.sourceediting.YamlSourceViewerConfiguration;
import org.eclipse.statet.yaml.ui.sourceediting.YamlSourceViewerConfigurator;


public class YamlContentViewerCreator implements IViewerCreator {
	
	
	public YamlContentViewerCreator() {
	}
	
	
	@Override
	public Viewer createViewer(final Composite parent, final CompareConfiguration config) {
		final YamlSourceViewerConfigurator viewerConfigurator=
				new YamlSourceViewerConfigurator(null,
						new YamlSourceViewerConfiguration(SourceEditorViewerConfiguration.COMPARE_MODE) );
		return new CompareTextViewer(parent, config, viewerConfigurator);
	}
	
}
