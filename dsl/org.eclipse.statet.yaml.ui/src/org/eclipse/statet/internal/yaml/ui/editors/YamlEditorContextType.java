/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.editors;

import org.eclipse.statet.ltk.ui.templates.SourceEditorContextType;


public class YamlEditorContextType extends SourceEditorContextType {
	
	
	//-- Context Types --// (starts with TEMPLATES_ID)
	
	public static final String DEFAULT_CONTEXT_TYPE_ID= "org.eclipse.statet.yaml.templates.YamlEditorDefaultContextType"; //$NON-NLS-1$
	
	
	public YamlEditorContextType() {
		super();
		
		addCommonVariables();
		addEditorVariables();
	}
	
}
