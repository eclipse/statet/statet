/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.ui.SourceStructContentProvider;
import org.eclipse.statet.yaml.core.model.YamlElement;


@NonNullByDefault
public class YamlSourceStructContentProvider implements SourceStructContentProvider {
	
	
	public YamlSourceStructContentProvider() {
	}
	
	
	private List<? extends SourceStructElement<?, ?>> filterDocs(final SourceStructElement<?, ?> sourceElement,
			final @Nullable List<SourceStructElement<?, ?>> tmpList) {
		final List<? extends SourceStructElement<?, ?>> docs= sourceElement.getSourceChildren(null);
		return LtkModelUtils.filter(docs, (doc) -> doc.hasSourceChildren(null), tmpList);
	}
	
	
	@Override
	public @Nullable SourceStructElement<?, ?> getSourceParent(
			final SourceStructElement<?, ?> element) {
		SourceStructElement<?, ?> parent= element.getSourceParent();
		if (parent == null) {
			return null;
		}
		if ((parent.getElementType() & LtkModelElement.MASK_C1) == YamlElement.C1_SOURCE) {
			final List<? extends SourceStructElement<?, ?>> children= filterDocs(element, null);
			if (children.size() == 1) {
				parent= parent.getSourceParent();
			}
		}
		return parent;
	}
	
	@Override
	public boolean hasSourceChildren(
			final SourceStructElement<?, ?> element,
			final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		if ((element.getElementType() & LtkModelElement.MASK_C1) == YamlElement.C1_SOURCE) {
			final List<? extends SourceStructElement<?, ?>> children= filterDocs(element, null);
			return !children.isEmpty();
		}
		return element.hasSourceChildren(filter);
	}
	
	@Override
	public List<? extends SourceStructElement<?, ?>> getSourceChildren(
			SourceStructElement<?, ?> element,
			final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter,
			@Nullable List<SourceStructElement<?, ?>> tmpList) {
		if ((element.getElementType() & LtkModelElement.MASK_C1) == YamlElement.C1_SOURCE) {
			if (tmpList == null) {
				tmpList= new ArrayList<>();
			}
			final List<? extends SourceStructElement<?, ?>> children= filterDocs(element, tmpList);
			if (children.size() == 1) {
				element= children.get(0);
				children.clear();
			}
			else {
				return children;
			}
		}
		return LtkModelUtils.filter(element.getSourceChildren(null), filter, tmpList);
	}
	
}
