/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.config;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String TextStyles_DefaultCodeCategory_label;
	public static String TextStyles_DefaultCodeCategory_short;
	public static String TextStyles_Default_label;
	public static String TextStyles_Default_description;
	public static String TextStyles_Indicators_label;
	public static String TextStyles_Indicators_description;
	public static String TextStyles_SeqMapBrackets_label;
	public static String TextStyles_SeqMapBrackets_description;
	public static String TextStyles_Keys_label;
	public static String TextStyles_Keys_description;
	public static String TextStyles_Tags_label;
	public static String TextStyles_Tags_description;
	
	public static String TextStyles_ProcessorCategory_label;
	public static String TextStyles_ProcessorCategory_short;
	public static String TextStyles_DocumentMarkers_label;
	public static String TextStyles_DocumentMarkers_description;
	public static String TextStyles_Directives_label;
	public static String TextStyles_Directives_description;
	
	public static String TextStyles_CommentCategory_label;
	public static String TextStyles_Comments_label;
	public static String TextStyles_Comments_description;
	public static String TextStyles_TaskTags_label;
	public static String TextStyles_TaskTags_description;
	
	public static String EditorOptions_SmartInsert_label;
	public static String EditorOptions_SmartInsert_AsDefault_label;
	public static String EditorOptions_SmartInsert_description;
	public static String EditorOptions_SmartInsert_TabAction_label;
	public static String EditorOptions_SmartInsert_CloseAuto_label;
	public static String EditorOptions_SmartInsert_CloseBrackets_label;
	public static String EditorOptions_SmartInsert_CloseQuotes_label;
	
	public static String EditorOptions_Folding_Enable_label;
	public static String EditorOptions_Folding_RestoreState_Enable_label;
	public static String EditorOptions_MarkOccurrences_Enable_label;
	public static String EditorOptions_ProblemChecking_Enable_label;
	public static String EditorOptions_AnnotationAppearance_info;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
