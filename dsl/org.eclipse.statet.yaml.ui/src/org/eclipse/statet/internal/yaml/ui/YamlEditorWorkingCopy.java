/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.ui.impl.GenericEditorWorkspaceSourceUnitWorkingCopy2;
import org.eclipse.statet.yaml.core.model.build.YamlSourceUnitModelContainer;


@NonNullByDefault
public final class YamlEditorWorkingCopy
		extends GenericEditorWorkspaceSourceUnitWorkingCopy2<YamlSourceUnitModelContainer> {
	
	
	public YamlEditorWorkingCopy(final WorkspaceSourceUnit from) {
		super(from);
	}
	
	@Override
	protected YamlSourceUnitModelContainer createModelContainer() {
		return new YamlSourceUnitModelContainer(this,
				YamlUIPlugin.getInstance().getYamlDocumentProvider() );
	}
	
	
}
