/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.workbench.ui.util.ThemeUtil;

import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings.TabAction;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.yaml.ui.YamlUI;
import org.eclipse.statet.yaml.ui.editors.YamlEditorBuild;
import org.eclipse.statet.yaml.ui.sourceediting.YamlEditingSettings;


public class YamlUIPreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public YamlUIPreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store= YamlUIPlugin.getInstance().getPreferenceStore();
		final IScopeContext scope= DefaultScope.INSTANCE;
		final IEclipsePreferences pref= scope.getNode(YamlUI.BUNDLE_ID);
		final ThemeUtil theme= new ThemeUtil();
		
		EditorsUI.useAnnotationsPreferencePage(store);
		EditorsUI.useQuickDiffPreferencePage(store);
		
		{	final IEclipsePreferences node= scope.getNode(YamlEditingSettings.ASSIST_YAML_PREF_QUALIFIER);
			node.put(ContentAssistComputerRegistry.CIRCLING_ORDERED, "yaml-elements:false,templates:true,paths:true"); //$NON-NLS-1$
			node.put(ContentAssistComputerRegistry.DEFAULT_DISABLED, ""); //$NON-NLS-1$
		}
		// EditorPreferences
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.FOLDING_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.FOLDING_RESTORE_STATE_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.MARKOCCURRENCES_ENABLED_PREF, Boolean.TRUE);
		
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.SMARTINSERT_BYDEFAULT_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.SMARTINSERT_TAB_ACTION_PREF, TabAction.INSERT_INDENT_LEVEL);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.SMARTINSERT_CLOSEBRACKETS_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, YamlEditingSettings.SMARTINSERT_CLOSEQUOTES_ENABLED_PREF, Boolean.TRUE);
		
		PreferenceUtils.setPrefValue(scope, YamlEditorBuild.PROBLEMCHECKING_ENABLED_PREF, Boolean.TRUE);
		
	}
	
}
