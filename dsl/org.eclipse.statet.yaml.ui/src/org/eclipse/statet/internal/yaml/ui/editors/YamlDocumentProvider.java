/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.editors;

import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.source.IAnnotationModel;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.preferences.SettingsChangeNotifier;
import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceAnnotationModel;
import org.eclipse.statet.ltk.ui.sourceediting.SourceDocumentProvider;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentSetupParticipant;
import org.eclipse.statet.yaml.ui.editors.YamlEditorBuild;


@NonNullByDefault
public class YamlDocumentProvider extends SourceDocumentProvider<SourceUnit>
		implements Disposable {
	
	
	private class ThisAnnotationModel extends SourceAnnotationModel {
		
		public ThisAnnotationModel(final IResource resource) {
			super(resource, YamlDocumentProvider.this.getIssueTypeSet());
		}
		
		@Override
		protected boolean isHandlingTemporaryProblems(final IssueTypeSet.ProblemCategory issueCategory) {
			return YamlDocumentProvider.this.handleTemporaryProblems;
		}
		
	}
	
	
	private SettingsChangeNotifier. @Nullable ChangeListener editorPrefListener;
	
	private boolean handleTemporaryProblems;
	
	
	public YamlDocumentProvider() {
		super(YamlModel.YAML_TYPE_ID, new YamlDocumentSetupParticipant(),
				YamlEditorBuild.YAML_ISSUE_TYPE_SET );
		
		{	final var editorPrefListener= new SettingsChangeNotifier.ChangeListener() {
				@Override
				public void settingsChanged(final Set<String> groupIds) {
					if (groupIds.contains(YamlEditorBuild.GROUP_ID)) {
						updateEditorPrefs();
					}
				}
			};
			this.editorPrefListener= editorPrefListener;
			PreferencesUtil.getSettingsChangeNotifier().addChangeListener(editorPrefListener);
		}
		final PreferenceAccess access= EPreferences.getInstancePrefs();
		this.handleTemporaryProblems= access.getPreferenceValue(YamlEditorBuild.PROBLEMCHECKING_ENABLED_PREF);
	}
	
	
	@Override
	public void dispose() {
		{	final var editorPrefListener= this.editorPrefListener;
			if (editorPrefListener != null) {
				this.editorPrefListener= null;
				PreferencesUtil.getSettingsChangeNotifier().removeChangeListener(editorPrefListener);
			}
		}
	}
	
	private void updateEditorPrefs() {
		final PreferenceAccess access= EPreferences.getInstancePrefs();
		final boolean newHandleTemporaryProblems= access.getPreferenceValue(YamlEditorBuild.PROBLEMCHECKING_ENABLED_PREF);
		if (this.handleTemporaryProblems != newHandleTemporaryProblems) {
			this.handleTemporaryProblems= newHandleTemporaryProblems;
			YamlModel.getYamlModelManager().refresh(Ltk.EDITOR_CONTEXT);
		}
	}
	
	@Override
	protected IAnnotationModel createAnnotationModel(final IFile file) {
		return new ThisAnnotationModel(file);
	}
	
}
