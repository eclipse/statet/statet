/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.editors;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;

import org.eclipse.statet.ltk.ui.compare.CompareMergeTextViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentSetupParticipant;
import org.eclipse.statet.yaml.ui.sourceediting.YamlSourceViewerConfiguration;
import org.eclipse.statet.yaml.ui.sourceediting.YamlSourceViewerConfigurator;


@NonNullByDefault
public class YamlMergeViewer extends CompareMergeTextViewer {
	
	
	public YamlMergeViewer(final Composite parent, final CompareConfiguration configuration) {
		super(parent, configuration);
	}
	
	
	@Override
	protected PartitionerDocumentSetupParticipant createDocumentSetupParticipant() {
		return new YamlDocumentSetupParticipant();
	}
	
	@Override
	protected SourceEditorViewerConfigurator createConfigurator(final SourceViewer sourceViewer) {
		return new YamlSourceViewerConfigurator(null,
				new YamlSourceViewerConfiguration(SourceEditorViewerConfiguration.COMPARE_MODE) );
	}
	
}
