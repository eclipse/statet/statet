/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.config;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.templates.TemplateContextType;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitioner;

import org.eclipse.statet.internal.yaml.ui.YamlUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.templates.config.AbstractTemplatesPreferencePage;
import org.eclipse.statet.yaml.core.source.doc.YamlPartitionNodeType;
import org.eclipse.statet.yaml.ui.sourceediting.YamlTemplateSourceViewerConfigurator;


public class YamlEditorTemplatesPreferencePage extends AbstractTemplatesPreferencePage {
	
	
	public YamlEditorTemplatesPreferencePage() {
		setPreferenceStore(YamlUIPlugin.getInstance().getPreferenceStore());
		setTemplateStore(YamlUIPlugin.getInstance().getYamlEditorTemplateStore());
		setContextTypeRegistry(YamlUIPlugin.getInstance().getYamlEditorTemplateContextTypeRegistry());
	}
	
	@Override
	protected SourceEditorViewerConfigurator createSourceViewerConfigurator(
			final TemplateVariableProcessor templateProcessor) {
		return new YamlTemplateSourceViewerConfigurator(null, templateProcessor);
	}
	
	
	@Override
	protected void configureContext(final AbstractDocument document,
			final TemplateContextType contextType, final SourceEditorViewerConfigurator configurator) {
		final String partitioning= configurator.getDocumentContentInfo().getPartitioning();
		final TreePartitioner partitioner= (TreePartitioner) document.getDocumentPartitioner(partitioning);
//		if (contextType.getId().equals(YamlEditorContextType.)) {
//			partitioner.setStartType(YamlPartitionNodeType.);
//		}
//		else {
			partitioner.setStartType(YamlPartitionNodeType.DEFAULT_ROOT);
//		}
		partitioner.disconnect();
		partitioner.connect(document);
		document.setDocumentPartitioner(partitioning, partitioner);
	}
	
}
