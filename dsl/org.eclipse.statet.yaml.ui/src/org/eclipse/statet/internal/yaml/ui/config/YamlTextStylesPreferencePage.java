/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.config;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.dsl.ui.DslUI;
import org.eclipse.statet.dsl.ui.text.DslTextStyles;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.presentation.AbstractTextStylesConfigurationBlock;
import org.eclipse.statet.ltk.ui.util.CombinedPreferenceStore;
import org.eclipse.statet.yaml.core.YamlCore;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentContentInfo;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentSetupParticipant;
import org.eclipse.statet.yaml.ui.sourceediting.YamlSourceViewerConfiguration;


@NonNullByDefault
public class YamlTextStylesPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public YamlTextStylesPreferencePage() {
		setPreferenceStore(DslUI.getPreferenceStore());
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() throws CoreException {
		return new YamlTextStylesConfigurationBlock();
	}
	
}


@NonNullByDefault
class YamlTextStylesConfigurationBlock extends AbstractTextStylesConfigurationBlock {
	
	
	public YamlTextStylesConfigurationBlock() {
	}
	
	
	@Override
	protected String getSettingsGroup() {
		return DslTextStyles.COMMON_DSL_TEXTSTYLE_CONFIG_QUALIFIER;
	}
	
	@Override
	protected ImList<CategoryNode> createItems() {
		return ImCollections.newList(
			new CategoryNode(Messages.TextStyles_DefaultCodeCategory_label,
				new StyleNode(Messages.TextStyles_Default_label, Messages.TextStyles_Default_description,
						DslTextStyles.TS_DEFAULT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.TextStyles_Indicators_label, Messages.TextStyles_Indicators_description,
						DslTextStyles.TS_INDICATOR, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ),
					new StyleNode(Messages.TextStyles_SeqMapBrackets_label, Messages.TextStyles_SeqMapBrackets_description,
							DslTextStyles.TS_BRACKET, ImCollections.newList(
								SyntaxNode.createUseNoExtraStyle(DslTextStyles.TS_INDICATOR),
								SyntaxNode.createUseCustomStyle() ))),
				new StyleNode(Messages.TextStyles_Keys_label, Messages.TextStyles_Keys_description,
						DslTextStyles.TS_KEY, ImCollections.newList(
							SyntaxNode.createUseCustomStyle(),
							SyntaxNode.createUseOtherStyle(DslTextStyles.TS_DEFAULT, Messages.TextStyles_Default_label) )),
				new StyleNode(Messages.TextStyles_Tags_label, Messages.TextStyles_Tags_description,
						DslTextStyles.TS_TAG, ImCollections.newList(
							SyntaxNode.createUseCustomStyle(),
							SyntaxNode.createUseOtherStyle(DslTextStyles.TS_DEFAULT, Messages.TextStyles_Default_label) ))
			),
			new CategoryNode(Messages.TextStyles_ProcessorCategory_label,
				new StyleNode(Messages.TextStyles_DocumentMarkers_label, Messages.TextStyles_DocumentMarkers_description,
						DslTextStyles.TS_DOCUMENT_MARKER, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.TextStyles_Directives_label, Messages.TextStyles_Directives_description,
						DslTextStyles.TS_DIRECTIVE, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ))
			),
			new CategoryNode(Messages.TextStyles_CommentCategory_label,
				new StyleNode(Messages.TextStyles_Comments_label, Messages.TextStyles_Comments_description,
						DslTextStyles.TS_COMMENT, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() )),
				new StyleNode(Messages.TextStyles_TaskTags_label, Messages.TextStyles_TaskTags_description,
						DslTextStyles.TS_TASK_TAG, ImCollections.newList(
							SyntaxNode.createUseCustomStyle() ))
			)
		);
	}
	
	@Override
	protected String getPreviewFileName() {
		return "YamlTextStylesPreviewCode.txt"; //$NON-NLS-1$
	}
	
	@Override
	protected IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new YamlDocumentSetupParticipant();
	}
	
	@Override
	protected SourceEditorViewerConfiguration getSourceEditorViewerConfiguration(
			final IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		return new YamlSourceViewerConfiguration(YamlDocumentContentInfo.INSTANCE, 0,
				null,
				YamlCore.getDefaultsAccess(), null,
				CombinedPreferenceStore.createStore(
						preferenceStore,
						EditorsUI.getPreferenceStore() ),
				textStyles );
	}
	
}
