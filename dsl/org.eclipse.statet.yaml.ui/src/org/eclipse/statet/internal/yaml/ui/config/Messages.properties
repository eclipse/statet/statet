 #=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#

TextStyles_DefaultCodeCategory_label= Default Code
TextStyles_DefaultCodeCategory_short= Default
TextStyles_Default_label= Default
TextStyles_Default_description= The style used for text not highlighted by another rule.
TextStyles_Indicators_label= Indicators
TextStyles_Indicators_description= The style used for indicator characters \nlike \u203A-\u2039, \u203A?\u2039 and \u203A:\u2039
TextStyles_SeqMapBrackets_label= Brackets (Flow Sequences/Maps)
TextStyles_SeqMapBrackets_description= The style used for squared brackets \u203A[\u2026]\u2039 and curly brackets \u203A{\u2026}\u2039
TextStyles_Keys_label= Keys
TextStyles_Keys_description= The style used for keys of map entries
TextStyles_Tags_label= Tags
TextStyles_Tags_description= The style used for data type tags

TextStyles_ProcessorCategory_label= Processor Instructions
TextStyles_ProcessorCategory_short= Processor (Directives)
TextStyles_DocumentMarkers_label= Document Markers
TextStyles_DocumentMarkers_description= The style used for document markers \u203A---\u2039 and \u203A...\u2039
TextStyles_Directives_label= Directives
TextStyles_Directives_description= The style for directives \u203A%\u2026 \u2039

TextStyles_CommentCategory_label= Comments
TextStyles_Comments_label= Comments
TextStyles_Comments_description= The style used for comments
TextStyles_TaskTags_label= Task Tags
TextStyles_TaskTags_description= The style used for defined task tags \ninside comments

EditorOptions_SmartInsert_label= 'Smart Insert'
EditorOptions_SmartInsert_AsDefault_label= &Enable by default
EditorOptions_SmartInsert_description= The 'Smart Insert' mode enables automatic <a href=\"org.eclipse.statet.yaml.preferencePages.YamlCodeStyle\">indentation</a> on new lines and brackets, and:
EditorOptions_SmartInsert_TabAction_label= &Tab key:
EditorOptions_SmartInsert_CloseAuto_label= Automatically close
EditorOptions_SmartInsert_CloseBrackets_label= &brackets\u2004\u203A\u200A[\u2004]\u200A\u2039 and \u203A\u200A{\u2004}\u200A\u2039
EditorOptions_SmartInsert_CloseQuotes_label= &quotes\u2004\u203A\u200A"\u2026"\u200A\u2039 and \u203A\u200A\'\u2026'\u200A\u2039

EditorOptions_Folding_Enable_label= Enable code &folding
EditorOptions_Folding_RestoreState_Enable_label= Restore expand/collapse states when opening file
EditorOptions_MarkOccurrences_Enable_label= Enable mark &occurrences
EditorOptions_ProblemChecking_Enable_label= Enable s&yntax checking as you type
EditorOptions_AnnotationAppearance_info= The appearance of occurrences, error and other indicators can be configured on the <a href=\"org.eclipse.ui.editors.preferencePages.Annotations\">Annotation</a> preference page.
