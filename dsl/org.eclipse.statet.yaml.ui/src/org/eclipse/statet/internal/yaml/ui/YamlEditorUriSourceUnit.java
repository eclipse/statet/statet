/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.element.SourceDocumentRunnable;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.impl.GenericUriSourceUnit2;
import org.eclipse.statet.ltk.model.core.impl.WorkingBuffer;
import org.eclipse.statet.ltk.model.ui.impl.FileBufferWorkingBuffer;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.build.YamlSourceUnitModelContainer;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentContentInfo;


@NonNullByDefault
public final class YamlEditorUriSourceUnit
		extends GenericUriSourceUnit2<YamlSourceUnitModelContainer>
		implements SourceUnit {
	
	
	public YamlEditorUriSourceUnit(final String id, final IFileStore store) {
		super(id, store);
	}
	
	
	@Override
	protected YamlSourceUnitModelContainer createModelContainer() {
		return new YamlSourceUnitModelContainer(this,
				YamlUIPlugin.getInstance().getYamlDocumentProvider() );
	}
	
	
	@Override
	public String getModelTypeId() {
		return YamlModel.YAML_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return YamlDocumentContentInfo.INSTANCE;
	}
	
	@Override
	public WorkingContext getWorkingContext() {
		return Ltk.EDITOR_CONTEXT;
	}
	
	
	@Override
	public void syncExec(final SourceDocumentRunnable runnable) throws InvocationTargetException {
		FileBufferWorkingBuffer.syncExec(runnable);
	}
	
	
	@Override
	protected WorkingBuffer createWorkingBuffer(final SubMonitor m) {
		return new FileBufferWorkingBuffer(this);
	}
	
}
