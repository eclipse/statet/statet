/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui.editors;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.templates.TemplateContextType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.yaml.ui.YamlUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;


@NonNullByDefault
public class YamlEditorTemplateCompletionComputer extends TemplateCompletionComputer {
	
	
	public YamlEditorTemplateCompletionComputer() {
		super(YamlUIPlugin.getInstance().getYamlEditorTemplateStore(),
				YamlUIPlugin.getInstance().getYamlEditorTemplateContextTypeRegistry() );
	}
	
	
	@Override
	protected @Nullable TemplateContextType getContextType(final AssistInvocationContext context,
			final TextRegion region) {
		try {
			final SourceEditor editor= context.getEditor();
			final AbstractDocument document= (AbstractDocument) context.getSourceViewer().getDocument();
			final ITypedRegion partition= document.getPartition(
					editor.getDocumentContentInfo().getPartitioning(), region.getStartOffset(), true );
			
			return getTypeRegistry().getContextType(YamlEditorContextType.DEFAULT_CONTEXT_TYPE_ID);
		}
		catch (final BadPartitioningException | BadLocationException e) {}
		return null;
	}
	
}
