/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.yaml.ui;

import org.eclipse.core.filesystem.IFileStore;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.core.impl.AbstractEditorSourceUnitFactory;


/**
 * Source unit factory for YAML editor context
 */
@NonNullByDefault
public final class YamlEditorWorkingCopyFactory extends AbstractEditorSourceUnitFactory {
	
	
	public YamlEditorWorkingCopyFactory() {
	}
	
	
	@Override
	protected SourceUnit createSourceUnit(final String id, final WorkspaceSourceUnit su) {
		return new YamlEditorWorkingCopy(su);
	}
	
	@Override
	protected SourceUnit createSourceUnit(final String id, final IFileStore file) {
		return new YamlEditorUriSourceUnit(id, file);
	}
	
}
