/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.UIResources;

import org.eclipse.statet.internal.yaml.ui.YamlUIPlugin;


@NonNullByDefault
public class YamlUIResources extends UIResources {
	
	
	private static final String NS= "org.eclipse.statet.yaml"; //$NON-NLS-1$
	
	public static final String OBJ_DOC_ELEMENT_IMAGE_ID=        NS + "/images/obj/YamlDoc"; //$NON-NLS-1$
	public static final String OBJ_ALIAS_IMAGE_ID=              NS + "/images/obj/Alias"; //$NON-NLS-1$
	
	
	static final YamlUIResources INSTANCE= new YamlUIResources();
	
	
	private YamlUIResources() {
		super(YamlUIPlugin.getInstance().getImageRegistry());
	}
	
}
