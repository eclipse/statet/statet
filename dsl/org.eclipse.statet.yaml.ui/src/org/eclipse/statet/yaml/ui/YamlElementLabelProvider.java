/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.ui;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.SharedUIResources;

import org.eclipse.statet.dsl.core.source.ast.Collection;
import org.eclipse.statet.dsl.ui.DslUI;
import org.eclipse.statet.dsl.ui.DslUIResources;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.ui.ElementLabelProvider;
import org.eclipse.statet.yaml.core.model.YamlElement;
import org.eclipse.statet.yaml.core.model.YamlModel;


@NonNullByDefault
public class YamlElementLabelProvider implements ElementLabelProvider {
	
	
	private final StringBuilder textBuilder= new StringBuilder(100);
	
	
	public YamlElementLabelProvider() {
	}
	
	
	protected final StringBuilder getTextBuilder() {
		this.textBuilder.setLength(0);
		return this.textBuilder;
	}
	
	
	@Override
	public @Nullable Image getImage(final LtkModelElement<?> element) {
		if (element.getModelTypeId() == YamlModel.YAML_TYPE_ID) {
			return switch (element.getElementType() & LtkModelElement.MASK_C123) {
			case YamlElement.C12_DOC ->
					YamlUI.getUIResources().getImage(YamlUIResources.OBJ_DOC_ELEMENT_IMAGE_ID);
			case YamlElement.C12_SEQ ->
					DslUI.getUIResources().getImage(DslUIResources.OBJ_SEQ_ELEMENT_IMAGE_ID);
			case YamlElement.C12_MAP ->
					DslUI.getUIResources().getImage(DslUIResources.OBJ_MAP_ELEMENT_IMAGE_ID);
			case YamlElement.C1_SCALAR ->
					DslUI.getUIResources().getImage(DslUIResources.OBJ_SCALAR_ELEMENT_IMAGE_ID);
			case YamlElement.C1_ALIAS ->
					YamlUI.getUIResources().getImage(YamlUIResources.OBJ_ALIAS_IMAGE_ID);
			case YamlElement.C1_DUMMY ->
					SharedUIResources.getInstance().getImage(SharedUIResources.PLACEHOLDER_IMAGE_ID);
			default ->
					null;
			};
		}
		return null;
	}
	
	@Override
	public @Nullable String getText(final LtkModelElement<?> element) {
		if (element.getModelTypeId() == YamlModel.YAML_TYPE_ID) {
			return element.getElementName().getDisplayName();
		}
		return null;
	}
	
	@Override
	public @Nullable StyledString getStyledText(final LtkModelElement<?> element) {
		if (element.getModelTypeId() == YamlModel.YAML_TYPE_ID) {
			final var text= new StyledString(element.getElementName().getDisplayName());
			final AstNode astNode;
			switch (element.getElementType() & LtkModelElement.MASK_C123) {
			case YamlElement.C12_MAP:
			case YamlElement.C12_SEQ:
				astNode= element.getAdapter(Collection.class);
				if (astNode != null) {
					final var textBuilder= getTextBuilder();
					textBuilder.append(" ("); //$NON-NLS-1$
					textBuilder.append(astNode.getChildCount());
					textBuilder.append(")"); //$NON-NLS-1$
					text.append(textBuilder.toString(), StyledString.DECORATIONS_STYLER);
				}
				break;
			}
			return text;
		}
		return null;
	}
	
}
