/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.ui.actions;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.actions.ToggleBooleanPreferenceHandler;

import org.eclipse.statet.yaml.ui.sourceediting.YamlEditingSettings;


/**
 * Toggles enablement of mark occurrences in YAML editors.
 */
@NonNullByDefault
public class YamlToggleMarkOccurrencesHandler extends ToggleBooleanPreferenceHandler {
	
	
	public YamlToggleMarkOccurrencesHandler() {
		super(	YamlEditingSettings.MARKOCCURRENCES_ENABLED_PREF,
				"org.eclipse.jdt.ui.edit.text.java.toggleMarkOccurrences"); //$NON-NLS-1$
	}
	
}
