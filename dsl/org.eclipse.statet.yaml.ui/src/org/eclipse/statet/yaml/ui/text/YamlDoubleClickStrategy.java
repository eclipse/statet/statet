/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.ui.text;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultTextDoubleClickStrategy;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.TextUtilities;

import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartition;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;

import org.eclipse.statet.ltk.text.core.CharPairMatcher;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentConstants;
import org.eclipse.statet.yaml.core.source.util.YamlBracketPairMatcher;
import org.eclipse.statet.yaml.core.source.util.YamlHeuristicTokenScanner;


/**
 * If matching pairs found, selection of content inside matching brackets,
 * otherwise default word selection.
 */
public class YamlDoubleClickStrategy extends DefaultTextDoubleClickStrategy {
	
	
	private final YamlHeuristicTokenScanner scanner;
	private final CharPairMatcher pairMatcher;
	
	
	public YamlDoubleClickStrategy(final YamlHeuristicTokenScanner scanner) {
		this.scanner= scanner;
		this.pairMatcher= new YamlBracketPairMatcher(this.scanner);
	}
	
	
	@Override
	public void doubleClicked(final ITextViewer textViewer) {
		final int offset= textViewer.getSelectedRange().x;
		
		if (offset < 0) {
			return;
		}
		
		final IDocument document= textViewer.getDocument();
		try {
			TreePartition partition= (TreePartition) TextUtilities.getPartition(document,
					this.scanner.getDocumentPartitioning(), offset, true );
			String type= partition.getType();
			
			// Bracket-Pair-Matching in Code-Partitions
			if (type == YamlDocumentConstants.YAML_DEFAULT_CONTENT_TYPE) {
				final IRegion region= this.pairMatcher.match(document, offset);
				if (region != null && region.getLength() >= 2) {
					textViewer.setSelectedRange(region.getOffset() + 1, region.getLength() - 2);
					return;
				}
			}
			
			// For other partitions, use prefere new partitions (instead opened)
			partition= (TreePartition) TextUtilities.getPartition(document,
					this.scanner.getDocumentPartitioning(), offset, false );
			type= partition.getType();
			final TreePartitionNode treeNode= partition.getTreeNode();
			
			switch (type) {
			case YamlDocumentConstants.YAML_KEY_CONTENT_TYPE:
			case YamlDocumentConstants.YAML_TAG_CONTENT_TYPE:
			case YamlDocumentConstants.YAML_VALUE_CONTENT_TYPE:
				if (offset == treeNode.getStartOffset()) {
					textViewer.setSelectedRange(treeNode.getStartOffset(), treeNode.getLength());
					return;
				}
				break;
			case YamlDocumentConstants.YAML_COMMENT_CONTENT_TYPE:
				if (offset == treeNode.getStartOffset() || offset == treeNode.getStartOffset() + 1) {
					textViewer.setSelectedRange(treeNode.getStartOffset(), treeNode.getLength());
					return;
				}
				break;
			default:
				break;
			}
			
			super.doubleClicked(textViewer);
			return;
		}
		catch (final BadLocationException | NullPointerException e) {
		}
		// else
		textViewer.setSelectedRange(offset, 0);
	}
	
}
