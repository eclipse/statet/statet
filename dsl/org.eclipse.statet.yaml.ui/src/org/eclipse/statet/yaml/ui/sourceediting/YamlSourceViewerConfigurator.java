/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.yaml.ui.sourceediting;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.yaml.core.YamlCodeStyleSettings;
import org.eclipse.statet.yaml.core.YamlCore;
import org.eclipse.statet.yaml.core.YamlCoreAccess;
import org.eclipse.statet.yaml.core.source.doc.YamlDocumentSetupParticipant;


/**
 * Configurator for YAML code source viewers.
 */
@NonNullByDefault
public class YamlSourceViewerConfigurator extends SourceEditorViewerConfigurator
		implements YamlCoreAccess, PropertyChangeListener {
	
	
	private static final Set<String> RESET_GROUP_IDS=
			ImCollections.newSet(YamlCodeStyleSettings.INDENT_GROUP_ID);
	//			TaskTagsPreferences.GROUP_ID );
	
	
	private YamlCoreAccess sourceCoreAccess= nonNullLateInit();
	
	private final YamlCodeStyleSettings yamlCodeStyleCopy;
	
	
	public YamlSourceViewerConfigurator(final @Nullable YamlCoreAccess coreAccess,
			final YamlSourceViewerConfiguration config) {
		super(config);
		this.yamlCodeStyleCopy= new YamlCodeStyleSettings(1);
		config.setCoreAccess(this, null);
		setSource(coreAccess);
		
		this.yamlCodeStyleCopy.load(this.sourceCoreAccess.getYamlCodeStyle());
		this.yamlCodeStyleCopy.resetDirty();
		this.yamlCodeStyleCopy.addPropertyChangeListener(this);
	}
	
	
	@Override
	public IDocumentSetupParticipant getDocumentSetupParticipant() {
		return new YamlDocumentSetupParticipant();
	}
	
	@Override
	protected Set<String> getResetGroupIds() {
		return RESET_GROUP_IDS;
	}
	
	
	public void setSource(@Nullable YamlCoreAccess newAccess) {
		if (newAccess == null) {
			newAccess= YamlCore.getWorkbenchAccess();
		}
		if (this.sourceCoreAccess != newAccess) {
			this.sourceCoreAccess= newAccess;
			handleSettingsChanged(null, null);
		}
	}
	
	
	@Override
	public void handleSettingsChanged(final @Nullable Set<String> groupIds,
			final @Nullable Map<String, Object> options) {
		super.handleSettingsChanged(groupIds, options);
		
		this.yamlCodeStyleCopy.resetDirty();
	}
	
	@Override
	protected void checkSettingsChanges(final Set<String> groupIds,
			final Map<String, Object> options) {
		super.checkSettingsChanges(groupIds, options);
		
		if (groupIds.contains(YamlCodeStyleSettings.INDENT_GROUP_ID)) {
			this.yamlCodeStyleCopy.load(this.sourceCoreAccess.getYamlCodeStyle());
		}
		if (groupIds.contains(YamlEditingSettings.EDITING_PREF_QUALIFIER)) {
			this.updateCompleteConfig= true;
		}
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.sourceCoreAccess.getPrefs();
	}
	
	@Override
	public YamlCodeStyleSettings getYamlCodeStyle() {
		return this.yamlCodeStyleCopy;
	}
	
}
