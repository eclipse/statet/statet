<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.4"?>
<!--
 #=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<plugin>
   
   <extension
         point="org.eclipse.core.runtime.preferences">
      <initializer
            class="org.eclipse.statet.internal.yaml.ui.YamlUIPreferenceInitializer">
      </initializer>
   </extension>
   
   <extension
         point="org.eclipse.statet.ltk.ModelTypes">
      <unitType modelTypeId="Yaml"
            contextKey="editor.default"
            unitFactory="org.eclipse.statet.internal.yaml.ui.YamlEditorWorkingCopyFactory">
      </unitType>
   </extension>
   <extension
         point="org.eclipse.statet.ltk.ModelAdapters">
      <adapterFactory
            modelTypeId="Yaml"
            class="org.eclipse.statet.internal.yaml.ui.YamlAdapterFactory">
         <adapter
               type="org.eclipse.statet.ltk.ui.SourceStructContentProvider"/>
         <adapter
               type="org.eclipse.statet.ltk.ui.ElementLabelProvider"/>
         <adapter
               type="org.eclipse.ui.texteditor.IDocumentProvider"/>
      </adapterFactory>
   </extension>
   
   <extension
         point="org.eclipse.ui.editors">
      <editor
            id="org.eclipse.statet.yaml.editors.Yaml"
            name="%editors_Yaml_name"
            icon="icons/obj_16/yaml_file.png"
            class="org.eclipse.statet.internal.yaml.ui.editors.YamlEditor"
            contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
            default="false">
         <contentTypeBinding
               contentTypeId="org.eclipse.statet.yaml.contentTypes.Yaml">
         </contentTypeBinding>
      </editor>
   </extension>
   
   <extension
         point="org.eclipse.statet.ltk.AdvancedContentAssist">
      <category
            id="templates"
            name="%contentAssist_TemplateCategory_name">
      </category>
      <computer
            id="org.eclipse.statet.yaml.contentAssistComputers.YamlTemplateCompletion"
            contentTypeId="org.eclipse.statet.yaml.contentTypes.Yaml"
            categoryId="templates"
            class="org.eclipse.statet.internal.yaml.ui.editors.YamlEditorTemplateCompletionComputer">
         <partition
               partitionType="Yaml.Default">
         </partition>
      </computer>
   </extension>
   <extension
         point="org.eclipse.ui.editors.templates">
      <contextTypeRegistry
            id="org.eclipse.statet.yaml.templates.YamlEditor">
      </contextTypeRegistry>
      <contextType
            id="org.eclipse.statet.yaml.templates.YamlEditorDefaultContextType"
            registryId="org.eclipse.statet.yaml.templates.YamlEditor"
            name="%templates_YamlEditorDefaultContextType_name"
            class="org.eclipse.statet.internal.yaml.ui.editors.YamlEditorContextType">
      </contextType>
      <include
            file="templates/default-yamleditor-templates.xml"
            translations="templates/default-yamleditor-templates.properties">
      </include>
   </extension>
   
   <extension
         point="org.eclipse.ui.workbench.texteditor.hyperlinkDetectorTargets">
      <target
            id="org.eclipse.statet.yaml.editorHyperlinks.YamlEditorTarget"
            name="%editorHyperlinks_YamlEditorTarget_name">
         <context
               type="org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1">
         </context>
      </target>
   </extension>

   <extension
         point="org.eclipse.ui.handlers">
      <handler
            commandId="org.eclipse.ui.edit.text.folding.toggle"
            class="org.eclipse.statet.yaml.ui.actions.YamlToggleFoldingHandler">
         <activeWhen>
            <with variable="activePartId">
               <equals
                     value="org.eclipse.statet.yaml.editors.Yaml">
               </equals>
            </with>
         </activeWhen>
      </handler>
   </extension>
   
   <extension
         point="org.eclipse.ui.actionSetPartAssociations">
      <actionSetPartAssociation
            targetID="org.eclipse.ui.edit.text.actionSet.presentation">
         <part
               id="org.eclipse.statet.yaml.editors.Yaml">
         </part>
      </actionSetPartAssociation>
   </extension>
   
   <extension
         point="org.eclipse.ui.preferencePages">
      <page
            id="org.eclipse.statet.yaml.preferencePages.Yaml"
            name="%preferencePages_Yaml_name"
            class="org.eclipse.statet.internal.yaml.ui.config.YamlBasePreferencePage">
      </page>
      <page
            id="org.eclipse.statet.yaml.preferencePages.YamlCodeStyle"
            category="org.eclipse.statet.yaml.preferencePages.Yaml"
            name="%preferencePages_YamlCodeStyle_name"
            class="org.eclipse.statet.internal.yaml.ui.config.YamlCodeStylePreferencePage">
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceAppearance"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceFormatting"/>
      </page>
      <page
            id="org.eclipse.statet.yaml.preferencePages.YamlEditor"
            category="org.eclipse.statet.yaml.preferencePages.Yaml"
            name="%preferencePages_YamlEditor_name"
            class="org.eclipse.statet.internal.yaml.ui.config.YamlEditorPreferencePage">
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceEditor"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceAppearance"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.AutoBracket"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.AutoIndent"/>
      </page>
      <page
            id="org.eclipse.statet.yaml.preferencePages.YamlTextStyles"
            category="org.eclipse.statet.yaml.preferencePages.Yaml"
            name="%preferencePages_YamlTextStyles_name"
            class="org.eclipse.statet.internal.yaml.ui.config.YamlTextStylesPreferencePage">
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceEditor"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceAppearance"/>
      </page>
      <page
            id="org.eclipse.statet.yaml.preferencePages.YamlEditorContentAssist"
            category="org.eclipse.statet.yaml.preferencePages.YamlEditor"
            name="%preferencePages_YamlEditorContentAssist_name"
            class="org.eclipse.statet.internal.yaml.ui.config.YamlContentAssistConfigurationPage">
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceEditor"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceContentAssist"/>
      </page>
      <page
            id="org.eclipse.statet.yaml.preferencePages.YamlEditorTemplates"
            category="org.eclipse.statet.yaml.preferencePages.Yaml"
            name="%preferencePages_YamlEditorTemplates_name"
            class="org.eclipse.statet.internal.yaml.ui.config.YamlEditorTemplatesPreferencePage">
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceEditor"/>
         <keywordReference id="org.eclipse.statet.ltk.keywords.SourceTemplates"/>
      </page>
   </extension>
   
   <extension
         point="org.eclipse.compare.contentViewers">
      <viewer
            id="org.eclipse.statet.yaml.compareContentViewers.Yaml"
            class="org.eclipse.statet.internal.yaml.ui.editors.YamlContentViewerCreator">
      </viewer>
      <contentTypeBinding
            contentViewerId="org.eclipse.statet.yaml.compareContentViewers.Yaml"
            contentTypeId="org.eclipse.statet.yaml.contentTypes.Yaml"/>
   </extension>
   <extension
         point="org.eclipse.compare.contentMergeViewers">
      <viewer
            id="org.eclipse.statet.yaml.compareContentViewers.YamlMergeViewer"
            class="org.eclipse.statet.internal.yaml.ui.editors.YamlMergeViewerCreator">
      </viewer>
      <contentTypeBinding
            contentMergeViewerId="org.eclipse.statet.yaml.compareContentViewers.YamlMergeViewer"
            contentTypeId="org.eclipse.statet.yaml.contentTypes.Yaml"/>
   </extension>
   
   <extension
         point="org.eclipse.ui.editors.annotationTypes">
      <type
         name="org.eclipse.statet.yaml.editorAnnotations.ErrorProblem"
         super="org.eclipse.statet.ltk.editorAnnotations.ErrorProblem"
         markerSeverity="2">
      </type>
      <type
         name="org.eclipse.statet.yaml.editorAnnotations.WarningProblem"
         super="org.eclipse.statet.ltk.editorAnnotations.WarningProblem"
         markerSeverity="1">
      </type>
      <type
         name="org.eclipse.statet.yaml.editorAnnotations.InfoProblem"
         super="org.eclipse.statet.ltk.editorAnnotations.InfoProblem"
         markerSeverity="0">
      </type>
   </extension>
   
</plugin>
