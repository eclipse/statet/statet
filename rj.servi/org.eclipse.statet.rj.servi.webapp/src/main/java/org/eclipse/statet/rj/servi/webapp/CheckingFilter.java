/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Checks http request:
 *  - session timeout (to avoid JSF message)
 */
public class CheckingFilter implements Filter {
	
	
	public CheckingFilter() {
	}
	
	
	@Override
	public void init(final FilterConfig fConfig) throws ServletException {
	}
	
	@Override
	public void destroy() {
	}
	
	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
		final HttpServletRequest httpRequest= (HttpServletRequest) request;
		final HttpServletResponse httpResponse= (HttpServletResponse) response;
		final HttpSession session= httpRequest.getSession(false);
		if (!httpRequest.getParameterMap().isEmpty()
				&& httpRequest.getRequestedSessionId() != null
				&& ((session == null) || session.isNew() || !session.getId().equals(httpRequest.getRequestedSessionId()))
				) {
			httpResponse.sendRedirect(httpRequest.getContextPath()+"/faces/resources/sessionexpired.jsp");
			return;
		}
		chain.doFilter(request, response);
		return;
	}
	
}
