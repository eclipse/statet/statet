/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.servlet.ServletContext;

import org.eclipse.statet.rj.RjInitFailedException;
import org.eclipse.statet.rj.RjInvalidConfigurationException;
import org.eclipse.statet.rj.server.util.RJContext;


public class ServletRJContext extends RJContext {
	
	
	private final ServletContext servletContext;
	
	
	public ServletRJContext(final ServletContext context) throws RjInitFailedException {
		this.servletContext= context;
	}
	
	
	@Deprecated
	@Override
	public String getServerPolicyFilePath() throws RjInvalidConfigurationException {
		String path= this.servletContext.getRealPath("WEB-INF/lib");
		final int length= path.length();
		if (length == 0 || (path.charAt(length-1) != '/' && path.charAt(length-1) != File.separatorChar)) {
			path+= File.separatorChar;
		}
		return path + "security.policy";
	}
	
	
	@Override
	protected String getPropertiesDirPath() {
		return "/WEB-INF/";
	}
	
	@Override
	protected InputStream getInputStream(final String path) throws IOException {
		return this.servletContext.getResourceAsStream(path);
	}
	
	@Override
	protected OutputStream getOutputStream(final String path) throws IOException {
		Path file= null;
		{	String s;
			int idx;
			s= this.servletContext.getRealPath(path);
			if (s != null) {
				file= Path.of(s);
			}
			else if ((idx= path.lastIndexOf('/')) >= 0 && idx < path.length() - 1) {
				s= this.servletContext.getRealPath(path.substring(0, idx + 1));
				if (s != null && !s.isEmpty()
						&& (s.charAt(s.length() - 1) == File.separatorChar || s.charAt(s.length() - 1) == '/') ) {
					file= Path.of(s, path.substring(idx + 1, path.length()));
				}
			}
			if (file == null) {
				throw new IOException("Writing to '" + path + "' not supported.");
			}
		}
		try {
			return Files.newOutputStream(file);
		}
		catch (final IOException e) {
			throw new IOException("Failed to get output stream for '" + path + "'.", e);
		}
	}
	
}
