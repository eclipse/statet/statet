/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.runtime.BasicAppEnvironment;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.runtime.bundle.Bundles;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.util.JUtilLoggingStatusLogger;


@NonNullByDefault
public class AppEnv extends BasicAppEnvironment implements ServletContextListener {
	
	
	public static final String ENV_ID= "org.eclipse.statet.rj.servi.pool"; //$NON-NLS-1$
	
	
	private @Nullable ServletContext context;
	
	
	public AppEnv() throws StatusException {
		super(ENV_ID, new JUtilLoggingStatusLogger(), Bundles.createResolver());
	}
	
	
	@Override
	public void contextInitialized(final ServletContextEvent sce) {
		this.context= sce.getServletContext();
		
		CommonsRuntime.init(this);
	}
	
	@Override
	public void contextDestroyed(final ServletContextEvent sce) {
		onAppStopping();
		
		this.context= null;
	}
	
	
}
