/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import java.time.Instant;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.rmi.RMIAddress;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.pool.PoolConfig;
import org.eclipse.statet.rj.servi.pool.PoolNodeItem;
import org.eclipse.statet.rj.servi.pool.PoolNodeObject;
import org.eclipse.statet.rj.servi.pool.PoolServer;


@NonNullByDefault
public class PoolItemBean extends PoolNodeItem {
	
	
	public PoolItemBean(final PoolNodeObject poolObj, final Instant stamp) {
		super(poolObj, stamp);
	}
	
	
	public @Nullable String getRMIAddress() {
		final RMIAddress address= getAddress();
		return (address != null) ? address.getAddress() : null;
	}
	
	
	public @Nullable String actionEnableConsole() {
		try {
			super.enableConsole("none");
		}
		catch (final RjException e) {
			FacesUtils.addErrorMessage(null, e.getMessage());
		}
		return null;
	}
	
	public @Nullable String actionDisableConsole() {
		try {
			super.disableConsole();
		}
		catch (final RjException e) {
			FacesUtils.addErrorMessage(null, e.getMessage());
		}
		return null;
	}
	
	public void actionStop() {
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		final PoolConfig config= new PoolConfig();
		poolServer.getPoolConfig(config);
		
		evict(config.getEvictionTimeout());
	}
	
	public void actionKill() {
		evict(null);
	}
	
}
