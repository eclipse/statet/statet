/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.node.RServiNodeConfig;
import org.eclipse.statet.rj.servi.pool.PoolServer;


public class NodeConfigBean extends RServiNodeConfig {
	
	
	public NodeConfigBean() {
	}
	
	
	@PostConstruct
	public void init() {
		actionLoadCurrent();
	}
	
	
	public synchronized String getRLibsVariable() {
		return getEnvironmentVariables().get("R_LIBS");
	}
	
	public synchronized void setRLibsVariable(final String value) {
		if (value != null && value.length() > 0) {
			getEnvironmentVariables().put("R_LIBS", value);
		}
		else {
			getEnvironmentVariables().remove("R_LIBS");
		}
	}
	
	
	public String actionLoadCurrent() {
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		synchronized (this) {
			poolServer.getNodeConfig(this);
			FacesUtils.validate(this);
		}
		
		return RJWeb.RCONFIG_NAV;
	}
	
	public String actionLoadDefaults() {
		synchronized (this) {
			load(new RServiNodeConfig());
			FacesUtils.validate(this);
		}
		
		return RJWeb.RCONFIG_NAV;
	}
	
	public String actionApply() {
		final RServiNodeConfig config= new RServiNodeConfig(this);
		
		if (!FacesUtils.validate(config)) {
			return null;
		}
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		try {
			poolServer.setNodeConfig(config);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Configuration applied.", null));
		}
		catch (final RjException e) {
			FacesUtils.addErrorMessage(null, e.getMessage());
		}
		return null;
	}
	
	public synchronized String actionSaveAndApply() {
		final RServiNodeConfig config= new RServiNodeConfig(this);
		
		if (!FacesUtils.validate(config)) {
			return null;
		}
		final PoolServer poolServer= FacesUtils.getPoolServer();
		
		try {
			poolServer.setNodeConfig(config);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Configuration applied.", null));
		}
		catch (final RjException e) {
			FacesUtils.addErrorMessage(null, e.getMessage());
			return null;
		}
		FacesUtils.saveToFile(config);
		return null;
	}
	
}
