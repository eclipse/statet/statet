/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;


public class RJWeb {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.rj.servi.webapp";
	
	public static final String RJ_POOLSERVER_KEY= "rj.pool.server";
	public static final String POOLID_KEY= "pool.id";
	public static final String RJCONTEXT_KEY= "rj.context";
	
	public static final String POOLCONFIG_NAV= "conf-pool";
	public static final String POOLSTATUS_NAV= "status-pool";
	public static final String RCONFIG_NAV= "conf-r";
	public static final String NETCONFIG_NAV= "conf-net";
	
	
	private RJWeb() {}
	
}
