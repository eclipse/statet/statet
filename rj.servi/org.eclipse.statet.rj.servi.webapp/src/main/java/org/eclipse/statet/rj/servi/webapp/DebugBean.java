/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.webapp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.el.ELContext;
import javax.faces.context.FacesContext;

import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.servi.node.RServiPool;
import org.eclipse.statet.rj.servi.pool.PoolServer;
import org.eclipse.statet.rj.servi.pool.RServiPoolManager;


public class DebugBean {
	
	
	private final List<RServi> nodes= new ArrayList<>();
	
	
	public DebugBean() {
	}
	
	
	@PreDestroy
	public void destroy() {
		actionCloseAllNodes();
	}
	
	
	public String actionNewNode() {
		final PoolServer poolServer= FacesUtils.getPoolServer();
		final RServiPoolManager poolManager= poolServer.getManager();
		
		if (poolManager == null) {
			FacesUtils.addErrorMessage(null, "The pool is currently not available.");
			return null;
		}
		try {
			final RServi rservi= ((RServiPool) poolManager).getRServi("control-web-app", null);
			synchronized(this) {
				this.nodes.add(rservi);
			}
		}
		catch (final Exception e) {
			FacesUtils.addErrorMessage(null, "An error occurred getting a node: " + e.getMessage());
		}
		refreshPoolStatus();
		return null;
	}
	
	public String actionCloseAllNodes() {
		synchronized (this) {
			for (final Iterator<RServi> iter= this.nodes.iterator(); iter.hasNext(); ) {
				final RServi rservi= iter.next();
				iter.remove();
				try {
					rservi.close();
				}
				catch (final Exception e) {
					FacesUtils.addErrorMessage(null, "An error occurred closing a node: " +e.getMessage());
				}
			}
		}
		
		refreshPoolStatus();
		return RJWeb.POOLSTATUS_NAV;
	}
	
	private void refreshPoolStatus() {
		final FacesContext context= FacesContext.getCurrentInstance();
		if (context != null) {
			try {
				final ELContext elContext= context.getELContext();
				final PoolStatusBean poolStatus= (PoolStatusBean) elContext.getELResolver().getValue(elContext, null, "poolStatus");
				if (poolStatus != null) {
					poolStatus.forceRefresh();
				}
			}
			catch (final Exception exception) {}
		}
	}
	
}
