/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_NUMERIC;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.DOUBLE_DELTA_R;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertAllEqualsInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertNA;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertNum;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.verifyPremise;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RNumericStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StoreNumExchangeTest extends AbstractIntegrationTest {
	
	
	private static final double numMin= Double.MIN_VALUE;
	private static final double numMax= Double.MAX_VALUE;
	private static final double numNaN= Double.NaN;
	private static final double numNInf= Double.NEGATIVE_INFINITY;
	private static final double numPInf= Double.POSITIVE_INFINITY;
	
	protected static class TestArg {
		
		final double[] dataSegValues;
		final boolean[] dataSegNAs;
		final int dataSegRep;
		final int dataLength;
		
		final String label;
		
		public TestArg(final double[] values, final boolean[] nas, final String valuesLabel,
				final int valuesRep) {
			this.dataSegValues= values;
			this.dataSegNAs= nas;
			this.dataSegRep= valuesRep;
			this.dataLength= values.length * valuesRep;
			this.label= valuesLabel + " x" + valuesRep;
		}
		
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	
	private static final List<TestArg> TEST_CASE_DATAS= new ArrayList<>();
	
	private static void addTestArgs(final double[] values, final int @Nullable [] naIdxs, final String valuesLabel,
			final int[] valuesRep) {
		final boolean[] nas= new boolean[values.length];
		if (naIdxs != null) {
			for (final int idx : naIdxs) {
				nas[idx]= true;
			}
		}
		for (final int rep : valuesRep) {
			TEST_CASE_DATAS.add(new TestArg(values, nas, valuesLabel, rep));
		}
	}
	static {
		final int[] singleRep= new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000, 100000 };
		addTestArgs(new double[] { }, null, "Empty", new int[] { 1 });
		addTestArgs(new double[] { 0.0 }, null, "0", singleRep);
		addTestArgs(new double[] { -1.0 }, null, "-1", singleRep);
		addTestArgs(new double[] { 1.0 }, null, "1", singleRep);
		addTestArgs(new double[] { numMin }, null, "Min", singleRep);
		addTestArgs(new double[] { numMax }, null, "Max", singleRep);
		addTestArgs(new double[] { numNaN }, null, "NaN", singleRep);
		addTestArgs(new double[] { numNInf }, null, "-Inf", singleRep);
		addTestArgs(new double[] { numPInf }, null, "+Inf", singleRep);
		addTestArgs(new double[] { 0 }, new int[] { 0 }, "NA", singleRep);
		
		addTestArgs(new double[] { 1.23456789, 2.00000000001, 1e308, 0.0, 0, numMin, numMax, 0.1e-16 },
				new int[] { 4 }, "8", new int[] { 1 });
		
		{	final Random rnd= new Random(68463541);
			final double[] values= new double[1000000];
			for (int i= 0; i < values.length; i++) {
				values[i]= Double.longBitsToDouble(rnd.nextLong());
			}
			values[8683]= numNaN;
			values[8684]= numNInf;
			values[98165]= numPInf;
			values[98166]= numNaN;
			values[values.length - 1]= numNaN;
			addTestArgs(values, null, "Sample " + values.length, new int[] { 1 });
		}
	}
	
	static List<TestArg> provideCaseDatas() {
		return ImCollections.toList(TEST_CASE_DATAS);
	}
	
	
	public StoreNumExchangeTest() throws Exception {
	}
	
	
	protected void createInR_NumVector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder("{ ");
		
		final double[] values= testArg.dataSegValues;
		final boolean[] nas= testArg.dataSegNAs;
		command.append(name).append(" <- double(").append(values.length).append("L); ");
		for (int segIdx= 0; segIdx < values.length; segIdx++) {
			command.append(name).append("[").append((segIdx + 1)).append("] <- ");
			if (nas[segIdx]) {
				command.append("NA");
			}
			else if (Double.isNaN(values[segIdx])) {
				command.append("NaN");
			}
			else if (values[segIdx] == Double.NEGATIVE_INFINITY) {
				command.append("-Inf");
			}
			else if (values[segIdx] == Double.POSITIVE_INFINITY) {
				command.append("Inf");
			}
			else {
				command.append(values[segIdx]);
			}
			command.append("; ");
		}
		if (testArg.dataSegRep > 1) {
			command.append(name).append(" <- rep.int(").append(name)
					.append(", times= ").append(testArg.dataSegRep).append("L); ");
		}
		
		command.append(" }");
		
		r.evalVoid(command.toString(), m);
	}
	
	protected void setData(final RNumericStore data, final TestArg testArg) {
		final double[] values= testArg.dataSegValues;
		final boolean[] nas= testArg.dataSegNAs;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				if (nas[segIdx]) {
					data.setNA(idx);
				}
				else {
					data.setNum(idx, values[segIdx]);
				}
			}
		}
	}
	
	protected void assertNumVector(final RObject rObject, final TestArg testArg,
			final String name) throws UnexpectedRDataException {
		final RVector<?> vector= assertRObject(RObject.TYPE_VECTOR, RVector.class,
				CLASSNAME_NUMERIC, testArg.dataLength,
				rObject, name );
		final RNumericStore data= assertInstanceOf(RNumericStore.class, vector.getData());
		
		final double[] values= testArg.dataSegValues;
		final boolean[] nas= testArg.dataSegNAs;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				if (nas[segIdx]) {
					assertNA(data, idx, name);
				}
				else {
					assertNum(values[segIdx], data, idx, name);
				}
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void fetchData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_NumVector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertNumVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void assignData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RVector<RNumericStore> x= this.rObjectFactory.createNumVector(testArg.dataLength);
			final RNumericStore data= x.getData();
			
			setData(data, testArg);
			verifyPremise(() -> assertNumVector(x, testArg, "x"));
			
			r.assignData("x", x, m);
			
			assertRObjectInR("x", RObject.TYPE_VECTOR, new String[] { CLASSNAME_NUMERIC }, testArg.dataLength,
					r, m, "x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.double(x)", m) ),
					"x" );
			createInR_NumVector("expected", testArg, r, m);
			assertAllEqualsInR(r.evalData("all.equal.numeric(expected, x, scale= abs(expected), "
							+ "tolerance= " + DOUBLE_DELTA_R + ")", m ),
					"x" );
		});
	}
	
	
}
