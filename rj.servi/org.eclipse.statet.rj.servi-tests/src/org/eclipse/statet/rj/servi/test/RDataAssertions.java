/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.function.Supplier;

import org.junit.jupiter.api.function.Executable;
import org.opentest4j.AssertionFailedError;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RComplexStore;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RNumericStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
public class RDataAssertions {
	
	
	public static <T extends RObject> T assertRObject(final byte rObjectType, final Class<T> rObjectJType,
			final String rClassName, final long length,
			final @Nullable RObject obj,
			final String name) {
		final String label= name;
		assertNotNull(obj, label);
		assertEquals(new RObjectType(rObjectType), new RObjectType(obj.getRObjectType()),
				() -> String.format("RObjectType of %1$s", label) );
		assertInstanceOf(rObjectJType, obj, label);
		assertEquals(rClassName, obj.getRClassName(),
				() -> String.format("RClassName of %1$s", label) );
		if (length != Long.MIN_VALUE) {
			assertEquals(length, obj.getLength(),
					() -> String.format("Length of %1$s", label) );
		}
		return rObjectJType.cast(obj);
	}
	
	public static RObject assertRObject(final byte rObjectType,
			final String rClassName, final long length,
			final @Nullable RObject obj,
			final String name) {
		return assertRObject(rObjectType, nonNullAssert(RDataUtils.getObjectTypeJava(rObjectType)),
				rClassName, length,
				obj, name );
	}
	
	public static RObject assertRObject(final byte rObjectType,
			final String rClassName,
			final @Nullable RObject obj,
			final String name) {
		return assertRObject(rObjectType, nonNullAssert(RDataUtils.getObjectTypeJava(rObjectType)),
				rClassName, Long.MIN_VALUE,
				obj, name );
	}
	
	
	public static void assertRObjectInR(final String name, final byte rObjectType,
			final String[] rClassNames, final int length,
			final RService r, final ProgressMonitor m,
			final String objLabel) throws UnexpectedRDataException, StatusException {
		final String label= objLabel + ".store";
		final StringBuilder sb= new StringBuilder();
		
		if (name.indexOf('(') == -1 && name.indexOf('[') == -1) {
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("exists('" + name + "')", m)),
					label );
		}
		
		sb.setLength(0);
		switch (rObjectType) {
		case RObject.TYPE_NULL:
			sb.append("is.null(").append(name).append(")");
			break;
		case RObject.TYPE_VECTOR:
			sb.append("(is.vector(").append(name).append(")");
				sb.append("|| is.factor(").append(name).append("))");
			sb.append("&& !is.array(").append(name).append(")");
			break;
		case RObject.TYPE_DATAFRAME:
			sb.append("is.data.frame(").append(name).append(")");
			break;
		case RObject.TYPE_S4OBJECT:
			sb.append("isS4(").append(name).append(")");
			break;
		default:
			throw new AssertionError(RDataUtils.getObjectTypeName(rObjectType));
		}
		if (sb.length() > 0) {
			assertTrue(RDataUtils.checkSingleLogiValue(
							r.evalData(sb.toString(), m) ),
					() -> String.format("RObjectType of %1$s", label) );
		}
		
		{	final String[] classInR= RDataUtils.checkRCharVector(
					r.evalData("class(" + name + ")", m) ).getData().toArray();
			assertArrayEquals(rClassNames, classInR,
					() -> String.format("RClassName(s) of %1$s", label) );
		}
		
		if (length >= 0) {
			final int lengthInR= RDataUtils.checkSingleIntValue(
					r.evalData("length(" + name + ")", m) );
			assertEquals(length, lengthInR,
					() -> String.format("Length of %1$s", label) );
		}
	}
	
	public static void assertRObjectInR(final String name, final byte rObjectType,
			final String rClassName, final int length,
			final RService r, final ProgressMonitor m) throws StatusException, UnexpectedRDataException {
		assertRObjectInR(name, rObjectType, new String[] { rClassName }, length, r, m, name);
	}
	
	
	public static <T extends RStore<?>> T assertRData(final byte rStoreType, final long length,
			final @Nullable T store,
			final String name) {
		assertNotNull(store, name);
		assertEquals(rStoreType, store.getStoreType(),
				() -> String.format("StoreType of %1$s", name));
		if (length != Long.MIN_VALUE) {
			assertEquals(length, store.getLength(),
					() -> String.format("Length of %1$s", name));
		}
		
		return store;
	}
	
	
	public static void assertNA(final RStore<?> store, final int idx,
			final String label) {
		assertTrue(store.isNA(idx),
				() -> String.format("NA at %1$s of %2$s", idx, label) );
	}
	
	public static void assertLogi(final boolean expected, final RStore<?> store, final int idx,
			final String label) {
		assertFalse(store.isNA(idx),
				() -> String.format("NA at %1$s of %2$s", idx) );
		assertEquals(expected, store.getLogi(idx),
				() -> String.format("int at %1$s of %2$s", idx, label) );
	}
	
	public static void assertInt(final int expected, final RStore<?> store, final int idx,
			final String label) {
		assertFalse(store.isNA(idx),
				() -> String.format("NA at %1$s of %2$s", idx, label) );
		assertEquals(expected, store.getInt(idx),
				() -> String.format("int at %1$s of %2$s", idx, label) );
	}
	
	public static final BigDecimal DOUBLE_DELTA_JAVA= BigDecimal.valueOf(1.6e-16);
	public static final String DOUBLE_DELTA_R= "1.7e-16";
	
	private static void assertDoubleEquals(final double expected, final double actual,
			final Supplier<String> messageSupplier) {
		if (!Double.isFinite(expected) || expected == 0.0) {
			assertEquals(expected, actual, messageSupplier);
			return;
		}
		final BigDecimal bExpected= BigDecimal.valueOf(expected);
		final BigDecimal bDiff= bExpected.subtract(BigDecimal.valueOf(actual)).abs();
		final BigDecimal bDelta= bExpected.abs().multiply(DOUBLE_DELTA_JAVA);
		if (bDiff.compareTo(bDelta) > 0) {
			fail(messageSupplier.get() + " ==> "
					+ String.format("expected: %s but was: %s", expected, actual) );
		}
	}
	
	public static void assertNum(final double expected, final RNumericStore store, final int idx,
			final String label) {
		assertFalse(store.isNA(idx),
				() -> String.format("NA at %1$s of %2$s", idx, label) );
		final boolean nan= Double.isNaN(expected);
		assertEquals(nan, store.isNaN(idx),
				() -> String.format("NaN at %1$s of %2$s", idx, label) );
		if (!nan) {
			assertDoubleEquals(expected, store.getNum(idx),
					() -> String.format("num at %1$s of %2$s", idx, label) );
		}
	}
	
	public static void assertCplx(final double expectedRe, final double expectedIm,
			final RComplexStore store, final int idx,
			final String label) {
		assertFalse(store.isNA(idx),
				() -> String.format("NA at %1$s of %2$s", idx, label) );
		final boolean nan= Double.isNaN(expectedRe) || Double.isNaN(expectedIm);
		assertEquals(nan, store.isNaN(idx),
				() -> String.format("NaN at %1$s of %2$s", idx, label) );
		if (!nan) {
			assertDoubleEquals(expectedRe, store.getCplxRe(idx),
					() -> String.format("cplxRe at %1$s of %2$s", idx, label) );
			assertDoubleEquals(expectedIm, store.getCplxIm(idx),
					() -> String.format("cplxIm at %1$s of %2$s", idx, label) );
		}
	}
	
	public static void assertChar(final @Nullable String expected, final RStore<?> store, final int idx,
			final String label) {
		assertFalse(store.isNA(idx),
				() -> String.format("NA at %1$s of %2$s", idx, label) );
		final String s= store.getChar(idx);
		if (!Objects.equals(expected, s)) {
//			System.out.println(expected.codePoints()
//					.mapToObj((cp) -> String.format("%1$06X", cp))
//					.collect(Collectors.joining(" ")));
			assertArrayEquals(
					(expected != null) ? expected.codePoints().toArray() : null,
					(s != null) ? s.codePoints().toArray() : null,
					() -> String.format("char at %1$s of %2$s", idx, label) );
		}
//		assertEquals(expected, s,
//				() -> String.format("char at %1$s of %2$s", idx, label) );
	}
	
	public static void assertRaw(final byte expected, final RStore<?> store, final int idx,
			final String label) {
		assertFalse(store.isNA(idx),
				() -> String.format("NA at %1$s of %2$s", idx, label) );
		assertEquals(expected, store.getRaw(idx),
				() -> String.format("char at %1$s of %2$s", idx, label) );
	}
	
	
	public static void assertAllEqualsInR(final RObject allEqualResult,
			final String message) throws UnexpectedRDataException {
		if (!RDataUtils.isSingleLogiTrue(allEqualResult)) {
			final StringBuilder sb= new StringBuilder("x ===> not equals in R: ");
			final RCharacterStore detail= RDataUtils.checkRCharVector(allEqualResult).getData();
			for (int i= 0; i < detail.getLength(); i++) {
				if (i > 10) {
					sb.append("  \n");
					break;
				}
				sb.append("  \n[").append(i + 1).append("] ").append(detail.getChar(i));
			}
			fail(sb.toString());
		}
	}
	
	
	public static void verifyPremise(final Executable check) throws Throwable {
		try {
			check.execute();
		}
		catch (final AssertionFailedError e) {	
			throw new AssertionError(e);
		}
	}
	
}
