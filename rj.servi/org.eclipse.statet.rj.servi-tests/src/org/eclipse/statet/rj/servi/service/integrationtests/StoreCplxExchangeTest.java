/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_COMPLEX;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.DOUBLE_DELTA_R;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertAllEqualsInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertCplx;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertNA;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RComplexStore;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.servi.test.RDataAssertions;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StoreCplxExchangeTest extends AbstractIntegrationTest {
	
	
	private static final double numMin= Double.MIN_VALUE;
	private static final double numMax= Double.MAX_VALUE;
	private static final double numNaN= Double.NaN;
	private static final double numNInf= Double.NEGATIVE_INFINITY;
	private static final double numPInf= Double.POSITIVE_INFINITY;
	
	protected static class TestArg {
		
		final double[] dataSegReValues;
		final double[] dataSegImValues;
		final boolean[] dataSegNAs;
		final int dataSegRep;
		final int dataLength;
		
		final String label;
		
		public TestArg(final double[] reValues, final double[] imValues, final boolean[] nas, final String valuesLabel,
				final int valuesRep) {
			this.dataSegReValues= reValues;
			this.dataSegImValues= imValues;
			this.dataSegNAs= nas;
			this.dataSegRep= valuesRep;
			this.dataLength= reValues.length * valuesRep;
			this.label= valuesLabel + " x" + valuesRep;
		}
		
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	
	private static final List<TestArg> TEST_CASE_DATAS= new ArrayList<>();
	
	private static void addTestArgs(final double[] reValues, final double[]  imValues,
			final int @Nullable [] naIdxs,
			final String valuesLabel,
			final int[] valuesRep) {
		final boolean[] nas= new boolean[reValues.length];
		if (naIdxs != null) {
			for (final int idx : naIdxs) {
				nas[idx]= true;
			}
		}
		for (final int rep : valuesRep) {
			TEST_CASE_DATAS.add(new TestArg(reValues, imValues, nas, valuesLabel, rep));
		}
	}
	static {
		addTestArgs(new double[] { }, new double[] { }, null, "Empty", new int[] { 1 });
		addTestArgs(new double[] { 0.0 }, new double[] { 0.0 }, null, "0+0i", new int[] { 1 });
		addTestArgs(new double[] { -1.0 }, new double[] { 0.0 }, null, "-1+0i", new int[] { 1 });
		addTestArgs(new double[] { 1.0 }, new double[] { 0.0 }, null, "1+0i", new int[] { 1 });
		addTestArgs(new double[] { 1.0 }, new double[] { 0.0 }, null, "0+1i", new int[] { 1 });
		addTestArgs(new double[] { 1.0 }, new double[] { 0.0 }, null, "0-1i", new int[] { 1 });
		addTestArgs(new double[] { 1.0 }, new double[] { 0.0 }, null, "1+1i", new int[] { 1 });
		addTestArgs(new double[] { numNaN }, new double[] { numNaN }, null, "NaN", new int[] { 1 });
		addTestArgs(new double[] { numNInf }, new double[] { -1.0 }, null, "-Inf-1i", new int[] { 1 });
		addTestArgs(new double[] { numPInf }, new double[] { 1.0 }, null, "+Inf+1i", new int[] { 1 });
		addTestArgs(new double[] { 0 }, new double[] { 0 }, new int[] { 0 }, "NA", new int[] { 1 });
		
		addTestArgs(new double[] { 1.23456789, 2.00000000001, 1e308, 0.0, 0, numMin, numMax, 0.1e-16 },
				new double[] { -1.0, numPInf, 0.1e-16, 1.0, 0, numMin, numMax, 1.23456789 },
				new int[] { 4 }, "8",
				new int[] { 1, 2, 3, 8, 15, 150, 1500, 50000 } );
		
		{	final Random rnd= new Random(68463542);
			final double[] reValues= new double[500000];
			final double[] imValues= new double[500000];
			for (int i= 0; i < reValues.length; i++) {
				reValues[i]= Double.longBitsToDouble(rnd.nextLong());
				imValues[i]= Double.longBitsToDouble(rnd.nextLong());
			}
			reValues[8683]= numNaN;
			reValues[8684]= numNInf;
			reValues[98165]= numPInf;
			reValues[98166]= numNaN;
			reValues[reValues.length - 1]= numNaN;
			addTestArgs(reValues, imValues, null, "Sample " + reValues.length, new int[] { 1 });
		}
	}
	
	static List<TestArg> provideCaseDatas() {
		return ImCollections.toList(TEST_CASE_DATAS);
	}
	
	
	public StoreCplxExchangeTest() throws Exception {
	}
	
	
	protected void createInR_CplxVector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder("{ ");
		
		final double[] reValues= testArg.dataSegReValues;
		final double[] imValues= testArg.dataSegImValues;
		final boolean[] nas= testArg.dataSegNAs;
		command.append(name).append(" <- complex(").append(reValues.length).append("L); ");
		for (int segIdx= 0; segIdx < reValues.length; segIdx++) {
			command.append(name).append("[").append((segIdx + 1)).append("] <- ");
			if (nas[segIdx]) {
				command.append("NA");
			}
			else if (Double.isNaN(reValues[segIdx]) || Double.isNaN(imValues[segIdx])) {
				command.append("complex(re= NaN, im= NaN)");
			}
			else {
				command.append("complex(re= ");
				if (Double.isNaN(reValues[segIdx])) {
					command.append("NaN");
				}
				else if (reValues[segIdx] == Double.NEGATIVE_INFINITY) {
					command.append("-Inf");
				}
				else if (reValues[segIdx] == Double.POSITIVE_INFINITY) {
					command.append("Inf");
				}
				else {
					command.append(reValues[segIdx]);
				}
				command.append(", im= ");
				if (Double.isNaN(imValues[segIdx])) {
					command.append("NaN");
				}
				else if (imValues[segIdx] == Double.NEGATIVE_INFINITY) {
					command.append("-Inf");
				}
				else if (imValues[segIdx] == Double.POSITIVE_INFINITY) {
					command.append("Inf");
				}
				else {
					command.append(imValues[segIdx]);
				}
				command.append(")");
			}
			command.append("; ");
		}
		if (testArg.dataSegRep > 1) {
			command.append(name).append(" <- rep.int(").append(name)
					.append(", times= ").append(testArg.dataSegRep).append("L); ");
		}
		
		command.append(" }");
		
		r.evalVoid(command.toString(), m);
	}
	
	protected void setData(final RComplexStore data, final TestArg testArg) {
		final double[] reValues= testArg.dataSegReValues;
		final double[] imValues= testArg.dataSegImValues;
		final boolean[] nas= testArg.dataSegNAs;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < reValues.length; idx++, segIdx++) {
				if (nas[segIdx]) {
					data.setNA(idx);
				}
				else {
					data.setCplx(idx, reValues[segIdx], imValues[segIdx]);
				}
			}
		}
	}
	
	protected void assertCplxVector(final RObject rObject, final TestArg testArg,
			final String name) throws UnexpectedRDataException {
		final RVector<?> vector= assertRObject(RObject.TYPE_VECTOR, RVector.class,
				CLASSNAME_COMPLEX, testArg.dataLength,
				rObject, name );
		final RComplexStore data= assertInstanceOf(RComplexStore.class, vector.getData());
		
		final double[] reValues= testArg.dataSegReValues;
		final double[] imValues= testArg.dataSegImValues;
		final boolean[] nas= testArg.dataSegNAs;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < reValues.length; idx++, segIdx++) {
				if (nas[segIdx]) {
					assertNA(data, idx, name);
				}
				else {
					assertCplx(reValues[segIdx], imValues[segIdx], data, idx, name);
				}
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void fetchData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_CplxVector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertCplxVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void assignData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RVector<RComplexStore> x= this.rObjectFactory.createCplxVector(testArg.dataLength);
			final RComplexStore data= x.getData();
			
			setData(data, testArg);
			RDataAssertions.verifyPremise(() -> assertCplxVector(x, testArg, "x"));
			
			r.assignData("x", x, m);
			
			assertRObjectInR("x", RObject.TYPE_VECTOR, new String[] { CLASSNAME_COMPLEX }, testArg.dataLength,
					r, m, "x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.complex(x)", m) ),
					"x" );
			createInR_CplxVector("expected", testArg, r, m);
			assertAllEqualsInR(r.evalData("all.equal.numeric(Re(expected), Re(x), scale= abs(Re(expected)), "
							+ "tolerance= " + DOUBLE_DELTA_R + ")", m ),
					"x" );
			assertAllEqualsInR(r.evalData("all.equal.numeric(Im(expected), Im(x), scale= abs(Im(expected)), "
							+ "tolerance= " + DOUBLE_DELTA_R + ")", m ),
					"x" );
		});
	}
	
	
}
