/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_CHARACTER;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_DATAFRAME;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_FACTOR;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_INTEGER;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_LOGICAL;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_NUMERIC;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertChar;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRData;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.verifyPremise;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataFrame;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class DataFrameExchangeTest extends AbstractIntegrationTest {
	
	
	protected static class TestArg {
		
		
		final int colCount;
		final ImList<String> colNames;
		
		final int rowCount;
		final String rowNamesR;
		
		
		public TestArg(final int rowCount) {
			this.colCount= 4;
			final @NonNull String[] colNames= new @NonNull String[this.colCount];
			for (int i= 0; i < this.colCount; i++) {
				colNames[i]= "Col" + (i + 1);
			}
			this.colNames= ImCollections.newList(colNames);
			
			this.rowCount= rowCount;
			this.rowNamesR= "paste('ID', 1L:" + this.rowCount + "L, sep= '-')";
		}
		
		
	}
	
	
	public DataFrameExchangeTest() throws Exception {
	}
	
	
	protected void createDataFrameInR(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder();
		command.append("{ ");
		command.append(name).append(" <- data.frame(")
					.append("Col1= integer(").append(testArg.rowCount).append("L),")
					.append("Col2= numeric(").append(testArg.rowCount).append("L),")
					.append("Col3= logical(").append(testArg.rowCount).append("L),")
					.append("Col4= factor(integer(").append(testArg.rowCount).append("L), ")
						.append("levels= c(0, 1), labels= c('Class A', 'Class B')),")
					.append("row.names= ").append(testArg.rowNamesR)
				.append("); ");
		command.append("}");
		
		r.evalVoid(command.toString(), m );
	}
	
	private RDataFrame createDataFrame(final TestArg testArg) {
		final RStore<?>[] colStores= new RStore[testArg.colCount];
		colStores[0]= this.rObjectFactory.createIntData(testArg.rowCount);
		colStores[1]= this.rObjectFactory.createNumData(testArg.rowCount);
		colStores[2]= this.rObjectFactory.createLogiData(testArg.rowCount);
		colStores[3]= this.rObjectFactory.createUnorderedFactorData(testArg.rowCount,
				new String[] { "Class A", "Class B" });
		final @NonNull String[] colNames= testArg.colNames.toArray(new @NonNull String[testArg.colCount]);
		final @NonNull String[] rowNames= new String[testArg.rowCount];
		for (int i = 0; i < testArg.rowCount; i++) {
			rowNames[i]= "ID-" + (i + 1);
		}
		return this.rObjectFactory.createDataFrame(colStores, colNames, rowNames);
	}
	
	protected RDataFrame assertRDataFrame(final RObject rObject, final TestArg testArg, final String name)
			throws UnexpectedRDataException {
		final RDataFrame dataFrame= assertRObject(RObject.TYPE_DATAFRAME, RDataFrame.class,
				CLASSNAME_DATAFRAME, testArg.colCount, rObject, name );
		
		assertEquals(testArg.colCount, dataFrame.getColumnCount());
		assertEquals(testArg.rowCount, dataFrame.getRowCount());
		
		return dataFrame;
	}
	
	protected void assertColNames(final RDataFrame dataFrame, final TestArg testArg, final String name) {
		final String label= name + ".columnNames";
		
		final RStore<?> store= assertRData(RStore.CHARACTER, testArg.colCount, dataFrame.getColumnNames(),
				label );
		for (int i= 0; i < testArg.colCount; i++) {
			assertChar(testArg.colNames.get(i), store, i, label);
			assertSame(dataFrame.getColumn(i), dataFrame.getColumn(testArg.colNames.get(i)));
		}
	}
	
	protected void assertRowNames(final RDataFrame dataFrame, final TestArg testArg, final String name) {
		final String label= name + ".rowNames";
		
		final RStore<?> store= assertRData(RStore.CHARACTER, testArg.rowCount, dataFrame.getRowNames(),
				label );
		for (int i= 0; i < testArg.rowCount; i++) {
			final String rowName= "ID-" + (i + 1);
			assertChar(rowName, store, i, label);
		}
	}
	
	
	@Test
	public void fetchDataFrame() throws Throwable {
		final TestArg testArg= new TestArg(1000);
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createDataFrameInR("y", testArg, r, m);
			
			final RObject a= r.evalData("y", m);
			
			final RDataFrame dataFrame= assertRDataFrame(a, testArg, "y");
			
			assertRData(RStore.INTEGER, testArg.rowCount, dataFrame.getColumn(0), "y[[1]]");
			assertRData(RStore.NUMERIC, testArg.rowCount, dataFrame.getColumn(1), "y[[2]]");
			assertRData(RStore.LOGICAL, testArg.rowCount, dataFrame.getColumn(2), "y[[3]]");
			assertRData(RStore.FACTOR, testArg.rowCount, dataFrame.getColumn(3), "y[[4]]");
		});
	}
	
	@Test
	public void fetchDataFrame_ColumnNames() throws Throwable {
		final TestArg testArg= new TestArg(1000);
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createDataFrameInR("y", testArg, r, m);
			
			final RObject a= r.evalData("y", m);
			
			final RDataFrame dataFrame= assertRDataFrame(a, testArg, "y");
			
			assertColNames(dataFrame, testArg, "y");
		});
	}
	
	@Test
	public void fetchDataFrame_RowNames() throws Throwable {
		final TestArg testArg= new TestArg(1000);
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createDataFrameInR("y", testArg, r, m);
			
			final RObject a= r.evalData("y", m);
			
			final RDataFrame dataFrame= assertRDataFrame(a, testArg, "y");
			
			assertRowNames(dataFrame, testArg, "y");
		});
	}
	
	@Test
	public void assignDataFrame() throws Throwable {
		final TestArg testArg= new TestArg(1000);
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RDataFrame y= createDataFrame(testArg);
			verifyPremise(() -> assertRDataFrame(y, testArg, "y"));
			
			r.assignData("y", y, m);
			
			assertRObjectInR("y", RObject.TYPE_DATAFRAME, CLASSNAME_DATAFRAME, testArg.colCount,
					r, m );
			assertTrue(RDataUtils.checkSingleLogiValue(r.evalData(
					"identical(dim(y), c(" + testArg.rowCount + "L, " + testArg.colCount + "L))",
					m )));
			
			assertRObjectInR("y[[1]]", RObject.TYPE_VECTOR, CLASSNAME_INTEGER, testArg.rowCount,
						r, m );
			assertRObjectInR("y[[2]]", RObject.TYPE_VECTOR, CLASSNAME_NUMERIC, testArg.rowCount,
					r, m );
			assertRObjectInR("y[[3]]", RObject.TYPE_VECTOR, CLASSNAME_LOGICAL, testArg.rowCount,
					r, m );
			assertRObjectInR("y[[4]]", RObject.TYPE_VECTOR, CLASSNAME_FACTOR, testArg.rowCount,
					r, m );
		});
	}
	
	@Test
	public void assignDataFrame_ColoumNames() throws Throwable {
		final TestArg testArg= new TestArg(1000);
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RDataFrame y= createDataFrame(testArg);
			verifyPremise(() -> assertColNames(assertRDataFrame(y, testArg, "y"), testArg, "y"));
			
			r.assignData("y", y, m);
			
			assertRObjectInR("y", RObject.TYPE_DATAFRAME, CLASSNAME_DATAFRAME, testArg.colCount,
					r, m );
			
			assertRObjectInR("colnames(y)", RObject.TYPE_VECTOR, CLASSNAME_CHARACTER, testArg.colCount,
						r, m );
			assertTrue(RDataUtils.checkSingleLogiValue(r.evalData(
					"identical("
							+ "c(" + String.join(", ", testArg.colNames.map((s) -> "'" + s + "'").toList()) + "), "
							+ "colnames(y))",
					m )));
		});
	}
	
	@Test
	public void assignDataFrame_RowNames() throws Throwable {
		final TestArg testArg= new TestArg(1000);
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RDataFrame y= createDataFrame(testArg);
			verifyPremise(() -> assertRowNames(assertRDataFrame(y, testArg, "y"), testArg, "y"));
			
			r.assignData("y", y, m);
			
			assertRObjectInR("y", RObject.TYPE_DATAFRAME, CLASSNAME_DATAFRAME, testArg.colCount,
					r, m );
			
			assertRObjectInR("rownames(y)", RObject.TYPE_VECTOR, CLASSNAME_CHARACTER, testArg.rowCount,
					r, m );
			assertTrue(RDataUtils.checkSingleLogiValue(r.evalData(
					"identical(" + testArg.rowNamesR + ", rownames(y))",
					m )));
		});
	}
	
	
}
