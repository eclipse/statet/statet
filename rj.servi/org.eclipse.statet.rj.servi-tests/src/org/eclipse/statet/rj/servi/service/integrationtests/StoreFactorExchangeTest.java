/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_FACTOR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertChar;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertInt;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertNA;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.verifyPremise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RFactorStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StoreFactorExchangeTest extends AbstractIntegrationTest {
	
	
	private static final int intNA= Integer.MIN_VALUE;
	
	protected static class TestArg {
		
		final @Nullable String[] levelLabels;
		final int valueLevels;
		final int naLevelCode;
		
		final int[] dataSegValues;
		final int dataSegRep;
		final int dataLength;
		
		final String label;
		
		public TestArg(final @Nullable String[] levelLabels, final int[] values,
				final String valuesLabel,
				final int valuesRep) {
			this.levelLabels= levelLabels;
			if (levelLabels.length == 0 || levelLabels[levelLabels.length - 1] != null) {
				this.valueLevels= levelLabels.length;
				this.naLevelCode= 0;
			}
			else {
				this.valueLevels= levelLabels.length - 1;
				this.naLevelCode= levelLabels.length;
			}
			this.dataSegValues= values;
			this.dataSegRep= valuesRep;
			this.dataLength= values.length * valuesRep;
			this.label= valuesLabel + " x" + valuesRep;
		}
		
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	
	private static final List<TestArg> TEST_CASE_DATAS= new ArrayList<>();
	
	private static void addTestArgs(final @Nullable String[] levelLabels, final int[] values,
			final String valuesLabel,
			final int[] valuesRep) {
		String valuesLabel0;
		valuesLabel0= "Levels:" + levelLabels.length + " " + valuesLabel;
		for (final int rep : valuesRep) {
			TEST_CASE_DATAS.add(new TestArg(levelLabels, values, valuesLabel0, rep));
		}
		valuesLabel0= "Levels:" + levelLabels.length + "+NA " + valuesLabel;
		for (final int rep : valuesRep) {
			final String[] withNA= Arrays.copyOf(levelLabels, levelLabels.length + 1);
			TEST_CASE_DATAS.add(new TestArg(withNA, values, valuesLabel0, rep));
		}
	}
	static {
		final int[] singleRep= new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000, 100000 };
		addTestArgs(new @Nullable String[] { "A", "B", "C" },
				new int[] { },
				"Empty", new int[] { 1 });
		addTestArgs(new @Nullable String[] { "A", "B", "C" },
				new int[] { 1 },
				"1", singleRep );
		addTestArgs(new @Nullable String[] { "A", "B", "C" },
				new int[] { intNA },
				"NA", singleRep );
		
		addTestArgs(new @Nullable String[] { "A", "B", "C", "D" },
				new int[] { 1, 2, 4, 2, intNA, 3, 1, 1 },
				"8 ABCD", new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000 } );
		addTestArgs(new @Nullable String[] { "A", "AA", "AAA", "AAAA", "AAAAA", "AAAAAA", "AAAAAAA", "AAAAAAAA", "AAAAAAAAA", "AAAAAAAAAA" },
				new int[] { 1, 2, 4, 2, intNA, intNA, 10, 10 },
				"8 A10", new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000 } );
	}
	
	static List<TestArg> provideCaseDatas() {
		return ImCollections.toList(TEST_CASE_DATAS);
	}
	
	
	public StoreFactorExchangeTest() throws Exception {
	}
	
	
	protected void createInR_FactorVector(final String name, final TestArg testArg,
			final String className,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder("{ ");
		
		final int[] values= testArg.dataSegValues;
		command.append(name).append(" <- ").append(className).append("(")
				.append("integer(").append(values.length).append("L), ")
				.append("levels= c(");
		if (testArg.valueLevels > 0) {
			command.append("1L:").append(testArg.valueLevels).append("L, ");
		}
		else {
			command.append("integer(0), ");
		}
		if (testArg.naLevelCode > 0) {
			command.append("NA, ");
		}
		if (command.charAt(command.length() - 1) != '(') {
			command.delete(command.length() - 2, command.length());
		}
		command.append("), ")
				.append("labels= c(");
		if (testArg.valueLevels > 0) {
			for (int i= 0; i < testArg.valueLevels; i++) {
				command.append("'").append(testArg.levelLabels[i]).append("', ");
			}
		}
		if (testArg.naLevelCode > 0) {
			command.append("NA, ");
		}
		if (command.charAt(command.length() - 1) != '(') {
			command.delete(command.length() - 2, command.length());
		}
		command.append("), ")
				.append("exclude= NULL); ");
		for (int segIdx= 0; segIdx < values.length; segIdx++) {
			command.append(name).append("[").append((segIdx + 1)).append("] <- ");
			switch (values[segIdx]) {
			case intNA:
				command.append("NA");
				break;
			default:
				command.append("'").append(testArg.levelLabels[values[segIdx] - 1]).append("'");
				break;
			}
			command.append("; ");
		}
		if (testArg.dataSegRep > 1) {
			command.append(name).append(" <- rep.int(").append(name)
					.append(", times= ").append(testArg.dataSegRep).append("L); ");
		}
		
		command.append(" }");
		
		r.evalVoid(command.toString(), m);
	}
	
	protected void setData(final RFactorStore data, final TestArg testArg) {
		final int[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				switch (values[segIdx]) {
				case intNA:
					if (testArg.naLevelCode == 0) {
						data.setNA(idx);
					}
					else {
						data.setInt(idx, testArg.naLevelCode);
					}
					break;
				default:
					data.setInt(idx, values[segIdx]);
					break;
				}
			}
		}
	}
	
	protected void assertFactorVector(final RObject rObject, final TestArg testArg,
			final String className,
			final String name) throws UnexpectedRDataException {
		final RVector<?> vector= assertRObject(RObject.TYPE_VECTOR, RVector.class,
				className, testArg.dataLength,
				rObject, name );
		final RFactorStore data= assertInstanceOf(RFactorStore.class, vector.getData());
		
		final int[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				switch (values[segIdx]) {
				case intNA:
					if (testArg.naLevelCode == 0) {
						assertNA(data, idx, name);
					}
					else {
						assertInt(testArg.naLevelCode, data, idx, name);
						assertChar(null, data, idx, name);
					}
					break;
				default:
					assertInt(values[segIdx], data, idx, name);
					assertChar(testArg.levelLabels[values[segIdx] - 1], data, idx, name);
					break;
				}
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void fetchData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_FactorVector("x", testArg, CLASSNAME_FACTOR, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertFactorVector(x, testArg, CLASSNAME_FACTOR, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void assignData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RVector<RFactorStore> x= this.rObjectFactory.createUnorderedFactorVector(testArg.dataLength,
					testArg.levelLabels );
			final RFactorStore data= x.getData();
			
			setData(data, testArg);
			verifyPremise(() -> assertFactorVector(x, testArg, CLASSNAME_FACTOR, "x"));
			
			r.assignData("x", x, m);
			
			assertRObjectInR("x", RObject.TYPE_VECTOR, new String[] { CLASSNAME_FACTOR }, testArg.dataLength,
					r, m, "x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.factor(x)", m) ),
					"x" );
			assertFalse(RDataUtils.checkSingleLogiValue(
					r.evalData("is.ordered(x)", m) ),
					"x" );
			createInR_FactorVector("expected", testArg, CLASSNAME_FACTOR, r, m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, x)", m) ),
					"x" );
		});
	}
	
}
