/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import java.util.NoSuchElementException;
import java.util.Random;

import javax.security.auth.login.LoginException;

import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;
import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.servi.pool.AbstractLocalNodeTest;
import org.eclipse.statet.rj.services.RService;


@NonNullByDefault
public abstract class AbstractIntegrationTest extends AbstractLocalNodeTest {
	
	
	private final String fileId;
	
	protected final RObjectFactory rObjectFactory= DefaultRObjectFactory.INSTANCE;
	
	
	public AbstractIntegrationTest() throws Exception {
		super(NODE_PER_ALL);
		this.fileId= this.getClass().getSimpleName();
	}
	
	
	@Override
	protected void initNode() throws Exception {
		super.initNode();
		this.localR.start();
	}
	
	
	public static interface RServiceTestRunnable {
		
		
		void run(RService r, ProgressMonitor m) throws Throwable;
		
	}
	
	protected void runWithRService(final RServiceTestRunnable runnable) throws Throwable {
		final ProgressMonitor m= new NullProgressMonitor();
		final var testId= StackWalker.getInstance().walk(s -> s.skip(1).findFirst()).get().getMethodName();
		final RService r= getServi(this.fileId + '-' + testId);
		try {
			runnable.run(r, m);
		}
		finally {
			closeServi((RServi)r);
		}
	}
	
	
	protected RService getService() throws NoSuchElementException, LoginException, StatusException {
		final var testId= StackWalker.getInstance().walk(s -> s.skip(1).findFirst()).get().getMethodName();
		return getServi(this.fileId + '-' + testId);
	}
	
	protected void closeService(final RService r) throws StatusException {
		closeServi((RServi)r);
	}
	
	
	protected static int[] shuffle(final int[] array, final Random rnd) {
		for (int i= array.length - 1; i > 0; i--) {
			final int j= rnd.nextInt(i + 1);
			final int tmp= array[i];
			array[i]= array[j];
			array[j]= tmp;
		}
		return array;
	}
	
	protected static int[] shuffle(final IntList ints, final Random rnd) {
		return shuffle(ints.toArray(), rnd);
	}
	
	
}
