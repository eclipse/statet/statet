/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RDataUtils;


@NonNullByDefault
public class RObjectType {
	
	
	private final byte type;
	
	
	public RObjectType(final byte type) {
		this.type = type;
	}
	
	
	@Override
	public int hashCode() {
		return this.type;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj instanceof RObjectType
				&& this.type == ((RObjectType)obj).type );
	}
	
	
	@Override
	public String toString() {
		return RDataUtils.getObjectTypeName(this.type) + "(0x" + Integer.toHexString((0xff) & this.type) + ")";
	}
	
}
