/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_RAW;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRaw;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.verifyPremise;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RRawStore;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StoreRawExchangeTest extends AbstractIntegrationTest {
	
	
	protected static class TestArg {
		
		final byte[] dataSegValues;
		final int dataSegRep;
		final int dataLength;
		
		final String label;
		
		public TestArg(final byte[] values, final String valuesLabel, final int valuesRep) {
			this.dataSegValues= values;
			this.dataSegRep= valuesRep;
			this.dataLength= values.length * valuesRep;
			this.label= valuesLabel + " x" + valuesRep;
		}
		
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	
	private static final List<TestArg> TEST_CASE_DATAS= new ArrayList<>();
	
	private static void addTestArgs(final byte[] values, final String valuesLabel,
			final int[] valuesRep) {
		for (final int rep : valuesRep) {
			TEST_CASE_DATAS.add(new TestArg(values, valuesLabel, rep));
		}
	}
	static {
		final int[] singleRep= new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000, 100000 };
		addTestArgs(new byte[] { }, "Empty", new int[] { 1 });
		addTestArgs(new byte[] { 0x00 }, "00", singleRep);
		addTestArgs(new byte[] { 0x01 }, "01", singleRep);
		addTestArgs(new byte[] { 127 }, "7F", singleRep);
		addTestArgs(new byte[] { -128 }, "80", singleRep);
		addTestArgs(new byte[] { -1 }, "FF", singleRep);
		
		addTestArgs(new byte[] { 1, 2, 127, -1, -2, 0, -127, -128 }, "8", new int[] { 1 });
		
		{	final Random rnd= new Random(68463540);
			final byte[] values= new byte[1000000];
			for (int i= 0; i < values.length; i++) {
				values[i]= (byte)(rnd.nextInt(255) & 0xFF);
			}
			addTestArgs(values, "Sample " + values.length, new int[] { 1 });
		}
	}
	
	static List<TestArg> provideCaseDatas() {
		return ImCollections.toList(TEST_CASE_DATAS);
	}
	
	
	public StoreRawExchangeTest() throws Exception {
	}
	
	
	protected void createInR_IntVector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder("{ ");
		
		final byte[] values= testArg.dataSegValues;
		command.append(name).append(" <- raw(").append(values.length).append("L); ");
		for (int segIdx= 0; segIdx < values.length; segIdx++) {
			command.append(name).append("[").append((segIdx + 1)).append("] <- as.raw(");
			command.append(Byte.toUnsignedInt(values[segIdx])).append('L');
			command.append("); ");
		}
		if (testArg.dataSegRep > 1) {
			command.append(name).append(" <- rep.int(").append(name)
					.append(", times= ").append(testArg.dataSegRep).append("L); ");
		}
		
		command.append(" }");
		
		r.evalVoid(command.toString(), m);
	}
	
	protected void setData(final RRawStore data, final TestArg testArg) {
		final byte[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				data.setRaw(idx, values[segIdx]);
			}
		}
	}
	
	protected void assertRawVector(final RObject rObject, final TestArg testArg,
			final String name) throws UnexpectedRDataException {
		final RVector<?> vector= assertRObject(RObject.TYPE_VECTOR, RVector.class,
				CLASSNAME_RAW, testArg.dataLength,
				rObject, name );
		final RRawStore data= assertInstanceOf(RRawStore.class, vector.getData());
		
		final byte[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				assertRaw(values[segIdx], data, idx, name);
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void fetchData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_IntVector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertRawVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void assignData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RVector<RRawStore> x= this.rObjectFactory.createRawVector(testArg.dataLength);
			final RRawStore data= x.getData();
			
			setData(data, testArg);
			verifyPremise(() -> assertRawVector(x, testArg, "x"));
			
			r.assignData("x", x, m);
			
			assertRObjectInR("x", RObject.TYPE_VECTOR, new String[] { CLASSNAME_RAW }, testArg.dataLength,
					r, m, "x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.raw(x)", m) ),
					"x" );
			createInR_IntVector("expected", testArg, r, m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, x)", m) ),
					"x" );
		});
	}
	
	
}
