/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_INTEGER;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertInt;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertNA;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.verifyPremise;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StoreIntExchangeTest extends AbstractIntegrationTest {
	
	
	private static final int intMin= Integer.MIN_VALUE + 1;
	private static final int intMax= Integer.MAX_VALUE;
	private static final int intNA= Integer.MIN_VALUE;
	
	protected static class TestArg {
		
		final int[] dataSegValues;
		final int dataSegRep;
		final int dataLength;
		
		final String label;
		
		public TestArg(final int[] values, final String valuesLabel, final int valuesRep) {
			this.dataSegValues= values;
			this.dataSegRep= valuesRep;
			this.dataLength= values.length * valuesRep;
			this.label= valuesLabel + " x" + valuesRep;
		}
		
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	
	private static final List<TestArg> TEST_CASE_DATAS= new ArrayList<>();
	
	private static void addTestArgs(final int[] values, final String valuesLabel,
			final int[] valuesRep) {
		for (final int rep : valuesRep) {
			TEST_CASE_DATAS.add(new TestArg(values, valuesLabel, rep));
		}
	}
	static {
		final int[] singleRep= new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000, 100000 };
		addTestArgs(new int[] { }, "Empty", new int[] { 1 });
		addTestArgs(new int[] { 0 }, "0", singleRep);
		addTestArgs(new int[] { -1 }, "-1", singleRep);
		addTestArgs(new int[] { 1 }, "1", singleRep);
		addTestArgs(new int[] { intMin }, "Min", singleRep);
		addTestArgs(new int[] { intMax }, "Max", singleRep);
		addTestArgs(new int[] { intNA }, "NA", singleRep);
		
		addTestArgs(new int[] { 1, 2, 3, intNA, 0, intMin, intMax, -1 }, "8", new int[] { 1 });
		
		addTestArgs(new int[] { 0, 1, 2, 3, 4, intNA, 126, 127, 128, 129, intNA, intMax - 2, intMax - 1, intMax, intNA,
							-1, -2, -3, -4, intNA, -126, -127, -128, -129, intNA, intMin + 2, intMin + 1, intMin },
				"selected", new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000 } );
		
		{	final Random rnd= new Random(68463541);
			final int[] values= new int[1000000];
			for (int i= 0; i < values.length; i++) {
				values[i]= rnd.nextInt();
			}
			addTestArgs(values, "sample", new int[] { 1 });
		}
	}
	
	static List<TestArg> provideCaseDatas() {
		return ImCollections.toList(TEST_CASE_DATAS);
	}
	
	
	public StoreIntExchangeTest() throws Exception {
	}
	
	
	protected void createInR_IntVector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder("{ ");
		
		final int[] values= testArg.dataSegValues;
		command.append(name).append(" <- integer(").append(values.length).append("L); ");
		for (int segIdx= 0; segIdx < values.length; segIdx++) {
			command.append(name).append("[").append((segIdx + 1)).append("] <- ");
			switch (values[segIdx]) {
			case intNA:
				command.append("NA");
				break;
			default:
				command.append(values[segIdx]).append('L');
				break;
			}
			command.append("; ");
		}
		if (testArg.dataSegRep > 1) {
			command.append(name).append(" <- rep.int(").append(name)
					.append(", times= ").append(testArg.dataSegRep).append("L); ");
		}
		
		command.append(" }");
		
		r.evalVoid(command.toString(), m);
	}
	
	protected void setData(final RIntegerStore data, final TestArg testArg) {
		final int[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				switch (values[segIdx]) {
				case intNA:
					data.setNA(idx);
					break;
				default:
					data.setInt(idx, values[segIdx]);
					break;
				}
			}
		}
	}
	
	protected void assertIntVector(final RObject rObject, final TestArg testArg,
			final String name) throws UnexpectedRDataException {
		final RVector<?> vector= assertRObject(RObject.TYPE_VECTOR, RVector.class,
				CLASSNAME_INTEGER, testArg.dataLength,
				rObject, name );
		final RIntegerStore data= assertInstanceOf(RIntegerStore.class, vector.getData());
		
		final int[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				switch (values[segIdx]) {
				case intNA:
					assertNA(data, idx, name);
					break;
				default:
					assertInt(values[segIdx], data, idx, name);
					break;
				}
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void fetchData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_IntVector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertIntVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void assignData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RVector<RIntegerStore> x= this.rObjectFactory.createIntVector(testArg.dataLength);
			final RIntegerStore data= x.getData();
			
			setData(data, testArg);
			verifyPremise(() -> assertIntVector(x, testArg, "x"));
			
			r.assignData("x", x, m);
			
			assertRObjectInR("x", RObject.TYPE_VECTOR, new String[] { CLASSNAME_INTEGER }, testArg.dataLength,
					r, m, "x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.integer(x)", m) ),
					"x" );
			createInR_IntVector("expected", testArg, r, m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, x)", m) ),
					"x" );
		});
	}
	
	
}
