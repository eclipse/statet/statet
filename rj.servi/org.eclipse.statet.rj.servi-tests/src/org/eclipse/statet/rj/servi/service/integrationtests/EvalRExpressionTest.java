/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_MATRIX;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class EvalRExpressionTest extends AbstractIntegrationTest {
	
	
	public EvalRExpressionTest() throws Exception {
	}
	
	
	@Test
	public void eval_SyntaxError() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final StatusException exception= assertThrows(StatusException.class, () -> {
				r.evalVoid("x a", m);
			});
//			exception.printStackTrace();
			final Status status= exception.getStatus();
			assertNotNull(status, "status");
			assertNotNull(status.getMessage(), "message");
			assertTrue(status.getMessage().startsWith("Evaluation failed: The specified expression is invalid (syntax error)"),
					"message type" );
		});
	}
	
	@Test
	public void eval_EvalError() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final StatusException exception= assertThrows(StatusException.class, () -> {
				r.evalVoid("x <- a", m);
			});
//			exception.printStackTrace();
			final Status status= exception.getStatus();
			assertNotNull(status, "status");
			assertNotNull(status.getMessage(), "message");
			assertTrue(status.getMessage().startsWith("Evaluation failed: An error occurred when evaluation the specified expression in R"),
					"message type" );
			assertTrue(status.getMessage().contains("not found"),
					"message info" );
		});
	}
	
	
	@Test
	public void setAttr() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			r.assignData("a", this.rObjectFactory.createNumVector(4), m);
			r.assignData("attr(a, 'dim')", this.rObjectFactory.createIntVector(new int[] { 2, 2 }), m);
			
			final RObject a= r.evalData("a", m);
			assertRObject(RObject.TYPE_ARRAY, CLASSNAME_MATRIX, 4, a, "a");
		});
	}
	
	
}
