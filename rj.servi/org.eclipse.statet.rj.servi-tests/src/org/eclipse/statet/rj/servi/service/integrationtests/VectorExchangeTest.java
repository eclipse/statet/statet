/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.jcommons.collections.ImCollections.newIntSequence;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_ARRAY;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_INTEGER;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_MATRIX;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertChar;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertInt;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRData;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.servi.test.RDataAssertions;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class VectorExchangeTest extends AbstractIntegrationTest {
	
	
	protected static class TestArg {
		
		
		final int length;
		
		
		public TestArg(final int length) {
			this.length= length;
		}
		
		
		@Override
		public String toString() {
			return Integer.toString(this.length);
		}
		
	}
	
	protected static class ArrayTestArg extends TestArg {
		
		
		final IntList dim;
		
		
		public ArrayTestArg(final int length, final int rowCount) {
			super(length);
			this.dim= ImCollections.newIntList(rowCount, length / rowCount);
		}
		
		public ArrayTestArg(final int length, final int[] dim) {
			super(length);
			this.dim= ImCollections.newIntList(dim);
		}
		
		
		@Override
		public String toString() {
			return Integer.toString(this.length) + " " + this.dim.toString();
		}
		
	}
	
	
	private static final String[] DIMS= new String[] { "A", "B", "C", "D", "E", "F" };
	
	
	static List<TestArg> provideVectorCaseDatas() {
		return ImCollections.newList(
				new TestArg(1000) );
	}
	
	static List<ArrayTestArg> provideMatrixCaseDatas() {
		return ImCollections.newList(
				new ArrayTestArg(1000, 5) );
	}
	
	static List<ArrayTestArg> provideArrayCaseDatas() {
		return ImCollections.newList(
				new ArrayTestArg(1000, new int[] { 5, 4, 50 }),
				new ArrayTestArg(3600, new int[] { 3, 4, 5, 6, 10 }) );
	}
	
	
	public VectorExchangeTest() throws Exception {
	}
	
	
	protected void assertDim(final RArray<?> rArray, final ArrayTestArg testArg, final String name) {
		final String label= name + ".dim";
		final int dimCount= testArg.dim.size();
		
		final RStore<?> dim= assertRData(RStore.INTEGER, dimCount,
				rArray.getDim(), label );
		for (int idx= 0; idx < dimCount; idx++) {
			assertInt(testArg.dim.getAt(idx), dim, idx, label);
		}
	}
	
	protected void assertDimNames(final RArray<?> rArray, final ArrayTestArg testArg, final String name) {
		final String label= name + ".dimNames";
		final int dimCount= testArg.dim.size();
		
		final RCharacterStore dimNames= assertRData(RStore.CHARACTER, dimCount,
				rArray.getDimNames(), label );
		for (int dim= 0; dim < dimCount; dim++) {
			assertChar(DIMS[dim], dimNames, dim, label);
		}
		
		for (int dim= 0; dim < dimCount; dim++) {
			final String dimLabel= name + ".names[dim= " + dim + "]";
			final int n= testArg.dim.getAt(dim);
			final RStore<?> names= assertRData(RStore.CHARACTER, n,
					rArray.getNames(dim), dimLabel );
			for (int idx= 0; idx < n; idx++) {
				assertChar(DIMS[dim] + (idx + 1), names, idx, dimLabel);
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideVectorCaseDatas")
	public void fetchVector(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append("x <- 1L:").append(testArg.length).append("L; ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RVector<?> rVector= assertRObject(RObject.TYPE_VECTOR, RVector.class, CLASSNAME_INTEGER, testArg.length,
					x, name );
			
			assertNull(rVector.getNames());
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideVectorCaseDatas")
	public void fetchVector_withNames(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append("x <- 1L:").append(testArg.length).append("L; ");
			command.append("names(x) <- paste0('ID', 1L:").append(testArg.length).append("); ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RVector<?> rVector= assertRObject(RObject.TYPE_VECTOR, RVector.class, CLASSNAME_INTEGER, testArg.length,
					x, name );
			
			final RStore<?> names= assertRData(RStore.CHARACTER, testArg.length, rVector.getNames(), "x.names");
			for (int idx= 0; idx < testArg.length; idx++) {
				RDataAssertions.assertChar("ID" + (idx + 1), names, idx, "x.names");
			}
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideVectorCaseDatas")
	public void fetchVector_Data(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append("x <- 1L:").append(testArg.length).append("L; ");
			command.append("names(x) <- paste0('ID', 1L:").append(testArg.length).append("); ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RVector<?> rVector= assertRObject(RObject.TYPE_VECTOR, RVector.class, CLASSNAME_INTEGER, testArg.length,
					x, name );
			
			final RStore<?> data= assertRData(RStore.INTEGER, testArg.length,
					rVector.getData(), "x.data" );
			for (int idx= 0; idx < testArg.length; idx++) {
				assertInt(idx + 1, data, idx, "x.data");
			}
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideMatrixCaseDatas")
	public void fetchMatrix(final ArrayTestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append("x <- matrix(1L:").append(testArg.length).append(", ")
					.append(testArg.dim.getAt(0)).append("L, ")
					.append(testArg.dim.getAt(1)).append("L); ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RArray<?> rArray= assertRObject(RObject.TYPE_ARRAY, RArray.class, CLASSNAME_MATRIX, testArg.length,
					x, name );
			assertDim(rArray, testArg, name);
			
			assertNull(rArray.getDimNames());
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideMatrixCaseDatas")
	public void fetchMatrix_withDimNames(final ArrayTestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append(name).append(" <- matrix(1L:").append(testArg.length).append(", ")
					.append(testArg.dim.getAt(0)).append("L, ")
					.append(testArg.dim.getAt(1)).append("L, ")
					.append("dimnames= list(")
						.append("A= paste0('A', 1L:").append(testArg.dim.getAt(0)).append("L), ")
						.append("B= paste0('B', 1L:").append(testArg.dim.getAt(1)).append("L))")
					.append("); ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RArray<?> rArray= assertRObject(RObject.TYPE_ARRAY, RArray.class, CLASSNAME_MATRIX, testArg.length,
					x, name );
			assertDim(rArray, testArg, name);
			
			assertDimNames(rArray, testArg, name);
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideMatrixCaseDatas")
	public void fetchMatrix_Data(final ArrayTestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append(name).append(" <- matrix(1L:").append(testArg.length).append(", ")
					.append(testArg.dim.getAt(0)).append("L, ")
					.append(testArg.dim.getAt(1)).append("L); ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RArray<?> rArray= assertRObject(RObject.TYPE_ARRAY, RArray.class, CLASSNAME_MATRIX, testArg.length,
					x, name );
			assertDim(rArray, testArg, name);
			
			final RStore<?> data= assertRData(RStore.INTEGER, testArg.length,
					rArray.getData(), "x.data" );
			for (int idx= 0; idx < testArg.length; idx++) {
				assertInt(idx + 1, data, idx, "x.data");
			}
			final RObject x1= r.evalData("x[1, 2]", m);
			assertEquals(RDataUtils.checkSingleIntValue(x1),
					data.getInt(RDataUtils.getDataIdx(rArray.getDim(), 0, 1)) );
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideArrayCaseDatas")
	public void fetchArray(final ArrayTestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append("x <- array(1L:").append(testArg.length).append(", ")
					.append("dim= c(");
			for (int dim= 0; dim < testArg.dim.size(); dim++) {
				command.append(testArg.dim.getAt(dim)).append("L, ");
			}
			command.replace(command.length() - 2, command.length(), ")")
					.append("); ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RArray<?> rArray= assertRObject(RObject.TYPE_ARRAY, RArray.class, CLASSNAME_ARRAY, testArg.length,
					x, name );
			assertDim(rArray, testArg, name);
			
			assertNull(rArray.getDimNames());
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideArrayCaseDatas")
	public void fetchArray_withDimNames(final ArrayTestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append("x <- array(1L:").append(testArg.length).append(", ")
					.append("dim= c(");
			for (int dim= 0; dim < testArg.dim.size(); dim++) {
				command.append(testArg.dim.getAt(dim)).append("L, ");
			}
			command.replace(command.length() - 2, command.length(), "), ")
					.append("dimnames= list(");
			for (int dim= 0; dim < testArg.dim.size(); dim++) {
				command.append(DIMS[dim]).append("= paste0('").append(DIMS[dim]).append("', 1L:").append(testArg.dim.getAt(dim)).append("L), ");
			}
			command.replace(command.length() - 2, command.length(), ")")
					.append("); ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RArray<?> rArray= assertRObject(RObject.TYPE_ARRAY, RArray.class, CLASSNAME_ARRAY, testArg.length,
					x, name );
			assertDim(rArray, testArg, name);
			
			assertDimNames(rArray, testArg, name);
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideArrayCaseDatas")
	public void fetchArray_Data(final ArrayTestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final String name= "x";
			
			final var command= new StringBuilder();
			command.append("{ ");
			command.append("x <- array(1L:").append(testArg.length).append(", ")
					.append("dim= c(");
			for (int dim= 0; dim < testArg.dim.size(); dim++) {
				command.append(testArg.dim.getAt(dim)).append("L, ");
			}
			command.replace(command.length() - 2, command.length(), ")")
					.append("); ");
			command.append("}");
			r.evalVoid(command.toString(), m);
			
			final RObject x= r.evalData(name, m);
			final RArray<?> rArray= assertRObject(RObject.TYPE_ARRAY, RArray.class, CLASSNAME_ARRAY, testArg.length,
					x, name );
			assertDim(rArray, testArg, name);
			
			final RStore<?> data= assertRData(RStore.INTEGER, testArg.length,
					rArray.getData(), "x.data" );
			for (int idx= 0; idx < testArg.length; idx++) {
				assertInt(idx + 1, data, idx, "x.data");
			}
			final ImIntList idxs= newIntSequence(0, testArg.dim.size());
			final RObject x1= r.evalData("x[" + idxs.stream()
					.mapToObj((i) -> Integer.toString(i + 1) + "L")
					.collect(Collectors.joining(", ")) + "]", m );
			assertEquals(RDataUtils.checkSingleIntValue(x1),
					data.getInt(RDataUtils.getDataIdx(rArray.getDim(), idxs.toArray())) );
		});
	}
	
	
}
