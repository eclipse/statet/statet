/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_LOGICAL;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertLogi;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertNA;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.verifyPremise;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RLogicalStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StoreLogiExchangeTest extends AbstractIntegrationTest {
	
	
	private static final byte logiTRUE= 1;
	private static final byte logiFALSE= 2;
	private static final byte logiNA= 3;
	
	protected static class TestArg {
		
		final byte[] dataSegValues;
		final int dataSegRep;
		final int dataLength;
		
		final String label;
		
		public TestArg(final byte[] values, final String valuesLabel, final int valuesRep) {
			this.dataSegValues= values;
			this.dataSegRep= valuesRep;
			this.dataLength= values.length * valuesRep;
			this.label= valuesLabel + " x" + valuesRep;
		}
		
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	
	private static final List<TestArg> TEST_CASE_DATAS= new ArrayList<>();
	
	private static void addTestArgs(final byte[] values, final String valuesLabel,
			final int[] valuesRep) {
		for (final int rep : valuesRep) {
			TEST_CASE_DATAS.add(new TestArg(values, valuesLabel, rep));
		}
	}
	static {
		addTestArgs(new byte[] { }, "Empty", new int[] { 1 });
		final int[] singleRep= new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000, 100000 };
		addTestArgs(new byte[] { logiTRUE }, "True", singleRep);
		addTestArgs(new byte[] { logiFALSE }, "False", singleRep);
		addTestArgs(new byte[] { logiNA }, "NA", singleRep);
		
		addTestArgs(new byte[] { logiTRUE, logiNA, logiFALSE, logiFALSE, logiNA, logiTRUE, logiTRUE, logiFALSE },
				"8", new int[] { 1, 2, 3, 8, 15, 150, 1500, 100000 } );
	}
	
	static List<TestArg> provideCaseDatas() {
		return ImCollections.toList(TEST_CASE_DATAS);
	}
	
	
	public StoreLogiExchangeTest() throws Exception {
	}
	
	
	protected void createInR_LogiVector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder("{ ");
		
		final byte[] values= testArg.dataSegValues;
		command.append(name).append(" <- logical(").append(values.length).append("L); ");
		for (int segIdx= 0; segIdx < values.length; segIdx++) {
			command.append(name).append("[").append((segIdx + 1)).append("] <- ");
			switch (values[segIdx]) {
			case logiTRUE:
				command.append("TRUE");
				break;
			case logiFALSE:
				command.append("FALSE");
				break;
			case logiNA:
				command.append("NA");
				break;
			default:
				throw new IllegalStateException();
			}
			command.append("; ");
		}
		if (testArg.dataSegRep > 1) {
			command.append(name).append(" <- rep.int(").append(name)
					.append(", times= ").append(testArg.dataSegRep).append("L); ");
		}
		
		command.append(" }");
		
		r.evalVoid(command.toString(), m);
	}
	
	protected void setData(final RLogicalStore data, final TestArg testArg) {
		final byte[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				switch (values[segIdx]) {
				case logiTRUE:
					data.setLogi(idx, true);
					break;
				case logiFALSE:
					data.setLogi(idx, false);
					break;
				case logiNA:
					data.setNA(idx);
					break;
				default:
					throw new IllegalStateException();
				}
			}
		}
	}
	
	protected void assertLogiVector(final RObject rObject, final TestArg testArg,
			final String name) throws UnexpectedRDataException {
		final RVector<?> vector= assertRObject(RObject.TYPE_VECTOR, RVector.class,
				CLASSNAME_LOGICAL, testArg.dataLength,
				rObject, name );
		final RLogicalStore data= assertInstanceOf(RLogicalStore.class, vector.getData());
		
		final byte[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				switch (values[segIdx]) {
				case logiTRUE:
					assertLogi(true, data, idx, name);
					break;
				case logiFALSE:
					assertLogi(false, data, idx, name);
					break;
				case logiNA:
					assertNA(data, idx, name);
					break;
				default:
					throw new IllegalStateException();
				}
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	private void fetchData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_LogiVector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertLogiVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void assignData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RVector<RLogicalStore> x= this.rObjectFactory.createLogiVector(testArg.dataLength);
			final RLogicalStore data= x.getData();
			
			setData(data, testArg);
			verifyPremise(() -> assertLogiVector(x, testArg, "x"));
			
			r.assignData("x", x, m);
			
			assertRObjectInR("x", RObject.TYPE_VECTOR, new String[] { CLASSNAME_LOGICAL }, testArg.dataLength,
					r, m, "x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.logical(x)", m) ),
					"x" );
			createInR_LogiVector("expected", testArg, r, m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, x)", m) ),
					"x" );
		});
	}
	
	
}
