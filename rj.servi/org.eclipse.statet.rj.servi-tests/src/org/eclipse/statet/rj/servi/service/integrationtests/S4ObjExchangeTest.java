/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_CHARACTER;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_MATRIX;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_NULL;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_NUMERIC;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRData;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RS4Object;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.RS4ObjectImpl;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class S4ObjExchangeTest extends AbstractIntegrationTest {
	
	
	public S4ObjExchangeTest() throws Exception {
	}
	
	
	protected void createClassInR_TestS4Type(
			final RService r, final ProgressMonitor m) throws StatusException {
		r.evalVoid("{ "
					+ "setClassUnion('nullableCharacter', members= c('character', 'NULL')); "
					+ "setClass('TestS4Type', "
						+ "representation(x= 'numeric', y= 'matrix', color= 'nullableCharacter'), "
						+ "prototype= list(x= numeric(), y= matrix(1:4, 2, 2), color= NULL) ); "
				+ "}", m );
	}
	
	protected void createClassInR_TestS4DataSlot(
			final RService r, final ProgressMonitor m) throws StatusException {
		r.evalVoid("{ " +
					"setClass('TestS4DataSlot', "
							+ "representation(label= 'character'), "
							+ "contains= 'integer' ); " +
				"}", m );
	}
	
	
	@Test
	public void fetchObj_S4Type() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createClassInR_TestS4Type(r, m);
			
			final RObject a= r.evalData("new('TestS4Type')", m);
			
			final RS4Object aObj= assertRObject(RObject.TYPE_S4OBJECT, RS4Object.class,
					"TestS4Type", 3,
					a, "a" );
			
			assertNull(a.getData(), "a.data");
			assertRObject(RObject.TYPE_VECTOR, CLASSNAME_NUMERIC, 0, aObj.get("x"), "a@x");
			assertRObject(RObject.TYPE_ARRAY, CLASSNAME_MATRIX, 4, aObj.get("y"), "a@y");
			assertRObject(RObject.TYPE_NULL, CLASSNAME_NULL, 0, aObj.get("color"), "a@color");
		});
	}
	
	@Test
	public void fetchObj_S4DataSlot() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createClassInR_TestS4DataSlot(r, m);
			
			final RObject a= r.evalData("new('TestS4DataSlot')", m);
			
			final RS4Object aObj= assertRObject(RObject.TYPE_S4OBJECT, RS4Object.class,
					"TestS4DataSlot", 2,
					a, "a" );
			
			assertRData(RStore.INTEGER, 0, a.getData(), "a.data");
			assertRObject(RObject.TYPE_VECTOR, CLASSNAME_CHARACTER, 0, aObj.get("label"), "a@label");
		});
	}
	
	@Test
	public void assignObj_S4Type_allDefault() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createClassInR_TestS4Type(r, m);
			
			final RS4Object aObj= new RS4ObjectImpl("TestS4Type",
					new @NonNull String[] { "x", "y", "color" } );
			r.assignData("a", aObj, m);
			
			assertRObjectInR("a", RObject.TYPE_S4OBJECT, "TestS4Type", 1, r, m);
			r.evalVoid("expected <- new('TestS4Type')", m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, a)", m) ));
		});
	}
	
	@Test
	public void assignObj_S4Type_oneSlotValue() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createClassInR_TestS4Type(r, m);
			
			final RS4ObjectImpl aObj= new RS4ObjectImpl("TestS4Type",
					new @NonNull String[] { "x", "y", "color" } );
			aObj.set("y", this.rObjectFactory.createNumArray(
					new double[] { 1.0, 1.5, 2.0, 2.5 },
					new int[] { 2, 2 } ));
			r.assignData("a", aObj, m);
			
			assertRObjectInR("a", RObject.TYPE_S4OBJECT, "TestS4Type", 1, r, m);
			r.evalVoid("expected <- new('TestS4Type', "
					+ "y= matrix(c(1.0, 1.5, 2.0, 2.5), 2, 2) )", m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, a)", m) ));
		});
	}
	
	@Test
	public void assignObj_S4Type_allSlotValues() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createClassInR_TestS4Type(r, m);
			
			final RS4ObjectImpl aObj= new RS4ObjectImpl("TestS4Type",
					new @NonNull String[] { "x", "y", "color" } );
			aObj.set("x", this.rObjectFactory.createIntVector(
					new int[] { 1, 2 } ));
			aObj.set("y", this.rObjectFactory.createNumArray(
					new double[] { 1.0, 1.5, 2.0, 2.5 },
					new int[] { 2, 2 } ));
			aObj.set("color", this.rObjectFactory.createCharVector(
					new String[] { "red" } ));
			r.assignData("a", aObj, m);
			
			assertRObjectInR("a", RObject.TYPE_S4OBJECT, "TestS4Type", 1, r, m);
			r.evalVoid("expected <- new('TestS4Type', "
					+ "x= 1:2,"
					+ "y= matrix(c(1.0, 1.5, 2.0, 2.5), 2, 2),"
					+ "color= 'red' )", m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, a)", m) ));
		});
	}
	
	@Test
	public void assignObj_S4DataSlot_allDefaults() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createClassInR_TestS4DataSlot(r, m);
			
			// All default
			final RS4Object aObj= new RS4ObjectImpl("TestS4DataSlot",
					new @NonNull String[] { ".Data", "label" } );
			r.assignData("a", aObj, m);
			
			assertRObjectInR("a", RObject.TYPE_S4OBJECT, "TestS4DataSlot", 0, r, m);
			r.evalVoid("expected <- new('TestS4DataSlot')", m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, a)", m) ));
		});
	}
	
	@Test
	public void assignObj_S4DataSlot_dataSlotValue() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createClassInR_TestS4DataSlot(r, m);
			
			final RS4ObjectImpl bObj= new RS4ObjectImpl("TestS4DataSlot",
					new @NonNull String[] { ".Data", "label" } );
			bObj.set(".Data", this.rObjectFactory.createIntVector(
					new int[] { 1, 2, 3, 4 } ));
			r.assignData("b", bObj, m);
			
			assertRObjectInR("b", RObject.TYPE_S4OBJECT, "TestS4DataSlot", 4, r, m);
			r.evalVoid("expected <- new('TestS4DataSlot', "
					+ "1:4 )", m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, b)", m) ));
		});
	}
	
	@Test
	public void assignObj_S4DataSlot_allSlotValues() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createClassInR_TestS4DataSlot(r, m);
			
			final RS4ObjectImpl cObj= new RS4ObjectImpl("TestS4DataSlot",
					new @NonNull String[] { ".Data", "label" } );
			cObj.set(".Data", this.rObjectFactory.createIntVector(
					new int[] { 1, 2, 3 } ));
			cObj.set("label", this.rObjectFactory.createCharVector(
					new String[] { "testId" } ));
			r.assignData("c", cObj, m);
			
			assertRObjectInR("c", RObject.TYPE_S4OBJECT, "TestS4DataSlot", 3, r, m);
			r.evalVoid("expected <- new('TestS4DataSlot', "
					+ "1:3,"
					+ "label= 'testId' )", m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, c)", m) ));
		});
	}
	
	
}
