/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.services.FunctionCall;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class FCallTest extends AbstractIntegrationTest {
	
	
	public FCallTest() throws Exception {
	}
	
	
	@Test
	public void argumentTypes() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			r.evalVoid("x1 <- 1L", m);
			r.evalVoid("testf <- function(a, b, c, d, e, f, g) { "
					+ "if (typeof(a) != 'character' || a != 'ABC') return(paste('a', typeof(a))); "
					+ "if (typeof(b) != 'integer' || b != 1L) return(paste('b', typeof(b)));"
					+ "if (!missing(c)) return(paste('c', str(c))); "
					+ "if (typeof(d) != 'double' || d != 1.5) return(paste('d', typeof(d))); "
					+ "if (typeof(e) != 'logical' || e != FALSE) return(paste('e', typeof(e))); "
					+ "if (typeof(f) != 'integer' || f != 3L) return(paste('f', typeof(f))); "
					+ "if (!is.null(g)) return(paste('g', typeof(g))); "
					+ "return(NULL); "+
					"}", m );
			
			FunctionCall call;
			
			// Named Args
			call= r.createFunctionCall("testf")
					.addChar("a", "ABC")
					.add("b", "x1")
					.addNum("d", 1.5)
					.addLogi("e", false)
					.addInt("f", 3)
					.addNull("g");
			final RObject testResult= call.evalData(m);
			
			if (testResult.getRObjectType() != RObject.TYPE_NULL) {
				fail(RDataUtils.checkSingleCharValue(testResult));
			}
		});
	}
	
	@Test
	public void unnamedArguments() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			r.evalVoid("x1 <- 1L", m);
			r.evalVoid("testf <- function(a, b, c, d, e, f, g) { "
					+ "if (typeof(a) != 'character' || a != 'ABC') return(paste('a', typeof(a))); "
					+ "if (typeof(b) != 'integer' || b != 1L) return(paste('b', typeof(b)));"
					+ "if (!is.null(c)) return(paste('c', typeof(c))); "
					+ "if (typeof(d) != 'double' || d != 1.5) return(paste('d', typeof(d))); "
					+ "if (typeof(e) != 'logical' || e != FALSE) return(paste('e', typeof(e))); "
					+ "if (!missing(f)) return(paste('c', str(f))); "
					+ "return(NULL); "+
					"}", m );
			
			FunctionCall call;
			
			// Named Args
			call= r.createFunctionCall("testf")
					.addNum("d", 1.5)
					.addChar("a", "ABC")
					.add("x1") // b
					.addLogi("e", false)
					.addNull(); // c
			final RObject testResult= call.evalData(m);
			
			if (testResult.getRObjectType() != RObject.TYPE_NULL) {
				fail(RDataUtils.checkSingleCharValue(testResult));
			}
		});
	}
	
	
}
