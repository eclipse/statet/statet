/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class FileExchangeTest extends AbstractIntegrationTest {
	
	
	private static Path tempDir= nonNullLateInit();
	
	
	public FileExchangeTest() throws Exception {
	}
	
	
	@BeforeAll
	public static void initTemp() throws IOException {
		tempDir= Files.createTempDirectory("RService-FileExchangeTest");
	}
	
	
	@Test
	public void uploadFile_Stream() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final byte[] content= "Hello World!".getBytes(StandardCharsets.UTF_8);
			RObject rContentObj;
			
			{	// Relative File Name
				final String rFileName= "relative.txt";
				r.uploadFile(new ByteArrayInputStream(content), content.length, rFileName, 0, m);
				rContentObj= r.createFunctionCall("readBin")
					.addChar("con", rFileName)
					.addInt("n", 100)
					.addChar("what", "raw")
					.evalData(m);
				assertArrayEquals(content, RDataUtils.checkRRawVector(rContentObj).getData().toRawArray());
			}
			{	// Absolute File Name
				final String rFileName= "absolute.txt";
				final var rwd= Path.of(RDataUtils.checkSingleCharValue(
						r.createFunctionCall("getwd").evalData(m) ));
				r.uploadFile(new ByteArrayInputStream(content), content.length,
						rwd.resolve(rFileName).toString(), 0, m );
				rContentObj= r.createFunctionCall("readBin")
					.addChar("con", rFileName)
					.addInt("n", 100)
					.addChar("what", "raw")
					.evalData(m);
				assertArrayEquals(content, RDataUtils.checkRRawVector(rContentObj).getData().toRawArray());
			}
			
			assertThrows(StatusException.class, () -> {
				r.uploadFile(new ByteArrayInputStream(content), content.length,
						"invalid://text.txt", 0, m );
			});
			
			assertThrows(StatusException.class, () -> {
				r.uploadFile(new ByteArrayInputStream(content), content.length + 1,
						"toosmall.txt", 0, m );
			});
		});
	}
	
	@Test
	public void uploadFile_Example1() throws Exception {
		final ProgressMonitor m= new NullProgressMonitor();
		final RService r= getService();
		
		final byte[] content= "Hello World! - example1".getBytes(StandardCharsets.UTF_8);
		final Path testFile= tempDir.resolve("uploadFile_Example1.txt");
		final String rFileName= "test.txt";
		
		Files.write(testFile, content);
		try (final var in= Files.newInputStream(testFile)) {
			r.uploadFile(in, Files.size(testFile), rFileName, 0, m);
		}
		final var rContent= r.createFunctionCall("readBin")
			.addChar("con", rFileName)
			.addInt("n", 100)
			.addChar("what", "raw")
			.evalData(m);
		closeService(r);
		
		assertArrayEquals(content, RDataUtils.checkRRawVector(rContent).getData().toRawArray());
	}
	
	@Test
	public void downloadFile_Stream() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final byte[] content= "Hello World! - example1".getBytes(StandardCharsets.UTF_8);
			final ByteArrayOutputStream out= new ByteArrayOutputStream();
			
			{	// Relative File Name
				final String rFileName= "relative.txt";
				r.createFunctionCall("writeBin")
					.add(this.rObjectFactory.createRawVector(content))
					.addChar("con", rFileName).addLogi("useBytes", true)
					.evalVoid(m);
				out.reset();
				r.downloadFile(out, rFileName, 0, m);
				assertArrayEquals(content, out.toByteArray());
			}
			{	// Absolute File Name
				final String rFileName= "absolute.txt";
				final var rwd= Path.of(RDataUtils.checkSingleCharValue(
						r.createFunctionCall("getwd").evalData(m) ));
				r.createFunctionCall("writeBin")
					.add(this.rObjectFactory.createRawVector(content))
					.addChar("con", rFileName).addLogi("useBytes", true)
					.evalVoid(m);
				out.reset();
				r.downloadFile(out, rwd.resolve(rFileName).toString(), 0, m);
				assertArrayEquals(content, out.toByteArray());
			}
			
			assertThrows(StatusException.class, () -> {
				out.reset();
				r.downloadFile(out, "invalid://text.txt", 0, m);
			});
			
			assertThrows(StatusException.class, () -> {
				out.reset();
				r.downloadFile(out, "missing.txt", 0, m);
			});
		});
	}
	
	@Test
	public void downloadFile_Array() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			final byte[] content= "Hello World! - example1".getBytes(StandardCharsets.UTF_8);
			
			{	// Relative File Name
				final String rFileName= "relative.txt";
				r.createFunctionCall("writeBin")
					.add(this.rObjectFactory.createRawVector(content))
					.addChar("con", rFileName).addLogi("useBytes", true)
					.evalVoid(m);
				final byte[] rContent= r.downloadFile(rFileName, 0, m);
				assertArrayEquals(content, rContent);
			}
			{	// Absolute File Name
				final String rFileName= "absolute.txt";
				final var rwd= Path.of(RDataUtils.checkSingleCharValue(
						r.createFunctionCall("getwd").evalData(m) ));
				r.createFunctionCall("writeBin")
					.add(this.rObjectFactory.createRawVector(content))
					.addChar("con", rFileName).addLogi("useBytes", true)
					.evalVoid(m);
				final byte[] rContent= r.downloadFile(rwd.resolve(rFileName).toString(), 0, m);
				assertArrayEquals(content, rContent);
			}
			{	// File Name with Blank and Unicode-SMP
				final String rFileName= "unicode \uD83E\uDC7D.txt";
				r.createFunctionCall("writeBin")
					.add(this.rObjectFactory.createRawVector(content))
					.addChar("con", rFileName).addLogi("useBytes", true)
					.evalVoid(m);
				final byte[] rContent= r.downloadFile(rFileName, 0, m);
				assertArrayEquals(content, rContent);
			}
			
			assertThrows(StatusException.class, () -> {
				r.downloadFile("invalid://text.txt", 0, m);
			});
			
			assertThrows(StatusException.class, () -> {
				r.downloadFile("missing.txt", 0, m);
			});
		});
	}
	
	@Test
	public void downloadFile_Example1() throws Exception {
		final ProgressMonitor m= new NullProgressMonitor();
		final RService r= getService();
		
		final byte[] content= "Hello World! - example1".getBytes(StandardCharsets.UTF_8);
		final Path testFile= tempDir.resolve("downloadFile_Example1.txt");
		final String rFileName= "test.txt";
		
		r.createFunctionCall("writeBin")
			.add(this.rObjectFactory.createRawVector(content))
			.addChar("con", rFileName).addLogi("useBytes", true)
			.evalVoid(m);
		try (final var out= Files.newOutputStream(testFile)) {
			r.downloadFile(out, rFileName, 0, m);
		}
		closeService(r);
		final var fileText= Files.readAllBytes(testFile);
		
		assertArrayEquals(content, fileText);
	}
	
	
}
