/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.NoSuchElementException;

import javax.management.OperationsException;
import javax.security.auth.login.LoginException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.NullProgressMonitor;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.servi.RServi;
import org.eclipse.statet.rj.servi.node.RServiNodeConfig;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class LocalNodeTest extends AbstractLocalNodeTest {
	
	
	public LocalNodeTest() throws Exception {
		super(NODE_PER_EACH);
	}
	
	
	@Test
	public void test1() throws RjException, OperationsException,
			NoSuchElementException, LoginException, StatusException, UnexpectedRDataException {
		this.localR.start();
		
		final RServi servi1= getServi("test1");
		assertNodeOperative(servi1);
		closeServi(servi1);
	}
	
	@Test
	public void closed() throws RjException, OperationsException,
			NoSuchElementException, LoginException, StatusException, UnexpectedRDataException {
		this.localR.start();
		
		final RServi servi1= getServi("test1");
		assertNodeOperative(servi1);
		assertFalse(servi1.isClosed());
		closeServi(servi1);
		
		assertTrue(servi1.isClosed());
		try {
			assertNodeOperative(servi1);
			throw new AssertionError("not closed");
		}
		catch (final StatusException e) {
			final Status eStatus= e.getStatus();
			assertEquals(Status.ERROR, eStatus.getSeverity());
			assertTrue(eStatus.getMessage().contains("closed"));
		}
	}
	
	@Test
	public void cleanup_R() throws RjException, OperationsException,
			NoSuchElementException, LoginException, StatusException, UnexpectedRDataException {
		final ProgressMonitor m= new NullProgressMonitor();
		
		this.localR.start();
		
		final RServi servi1= getServi("test1");
		
		servi1.evalVoid("x <- 123L", m);
		servi1.evalVoid(".x <- 123L", m);
		assertTrue(RDataUtils.checkSingleLogiValue(servi1.evalData("exists(\"x\")", m)));
		assertTrue(RDataUtils.checkSingleLogiValue(servi1.evalData("exists(\".x\")", m)));
		
		closeServi(servi1);
		
		final RServi servi2= getServi("test1");
		assertFalse(RDataUtils.checkSingleLogiValue(servi2.evalData("exists(\"x\")", m)));
		assertFalse(RDataUtils.checkSingleLogiValue(servi2.evalData("exists(\".x\")", m)));
		
		closeServi(servi2);
	}
	
	@Test
	public void cleanup_WorkingDir() throws RjException, OperationsException,
	NoSuchElementException, LoginException, StatusException, UnexpectedRDataException {
		final ProgressMonitor m= new NullProgressMonitor();
		
		this.localR.start();
		
		final RServi servi1= getServi("test1");
		
		final var wd= Path.of(RDataUtils.checkSingleCharValue(servi1.evalData("getwd()", m)));
		servi1.evalVoid("writeLines(\"Hello\", \"test.txt\")", m);
		final var file= wd.resolve("test.txt");
		assertTrue(Files.isRegularFile(file));
		
		closeServi(servi1);
		
		final RServi servi2= getServi("test1");
		assertTrue(Files.notExists(file));
		
		closeServi(servi2);
	}
	
	@Test
	public void cleanup_afterStop() throws RjException, OperationsException,
	NoSuchElementException, LoginException, StatusException, UnexpectedRDataException {
		final ProgressMonitor m= new NullProgressMonitor();
		
		this.localR.start();
		
		final RServi servi1= getServi("test1");
		
		final var wd= Path.of(RDataUtils.checkSingleCharValue(servi1.evalData("getwd()", m)));
		servi1.evalVoid("writeLines(\"Hello\", \"test.txt\")", m);
		final var wdFile= wd.resolve("test.txt");
		assertTrue(Files.isRegularFile(wdFile));
		
		final var tempDir= Path.of(RDataUtils.checkSingleCharValue(servi1.evalData("tempdir()", m)));
		servi1.evalVoid("writeLines(\"Hello\", paste(tempdir(), \"test.txt\", sep=\"/\"))", m);
		final var tempFile= tempDir.resolve("test.txt");
		assertTrue(Files.isRegularFile(tempFile));
		
		closeServi(servi1);
		
		this.localR.stop();
		
		for (int i= 0; i < 5 && !Files.notExists(wd); i++) {
			try {
				Thread.sleep(200);
			}
			catch (final InterruptedException e) {}
		}
		
		assertTrue(Files.notExists(wdFile));
		assertTrue(Files.notExists(wd));
		assertTrue(Files.notExists(tempFile));
		assertTrue(Files.notExists(tempDir));
	}
	
	@Test
	public void timeout_stop() throws RjException, OperationsException,
			NoSuchElementException, LoginException, StatusException, UnexpectedRDataException {
		final ProgressMonitor m= new NullProgressMonitor();
		
		this.localR.start();
		
		final RServi servi1= getServi("test1");
		
		final Thread blocker= new Thread() {
			@Override
			public void run() {
				try {
					servi1.evalVoid("Sys.sleep(20L)", m);
				}
				catch (final Exception e) {
				}
			}
		};
		blocker.start();
		try {
			Thread.sleep(500);
		}
		catch (final InterruptedException e) {}
		
		final long t1= System.nanoTime();
		this.localR.stop();
		final Duration d1= Duration.ofNanos(System.nanoTime() - t1);
		final Duration dMax= new RServiNodeConfig().getStartStopTimeout().plusMillis(1500);
		assertTrue(d1.compareTo(dMax) < 0,
				() -> String.format("duration expected: < %1$sms, actual: %2$sms", dMax, d1) );
		
		try {
			blocker.join(5000);
		}
		catch (final InterruptedException e) {
		}
		assertTrue(servi1.isClosed());
	}
	
}
