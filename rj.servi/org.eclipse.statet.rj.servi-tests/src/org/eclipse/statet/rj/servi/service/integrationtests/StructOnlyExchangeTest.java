/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_CHARACTER;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_COMPLEX;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_INTEGER;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_LOGICAL;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_NUMERIC;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRData;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StructOnlyExchangeTest extends AbstractIntegrationTest {
	
	
	private final int length= 10000;
	
	
	public StructOnlyExchangeTest() throws Exception {
	}
	
	
	@Test
	public void fetchLogical() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RObject x= r.evalData("logical(" + this.length + "L)",
					null, RObjectFactory.F_ONLY_STRUCT, RService.DEPTH_INFINITE, m );
			
			assertRObject(RObject.TYPE_VECTOR, CLASSNAME_LOGICAL, this.length, x, "x");
			
			final var store= assertRData(RStore.LOGICAL, -1, x.getData(), "x.data");
			
			assertThrows(UnsupportedOperationException.class, () -> {
				store.isNA(0);
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				store.getLogi(0);
			});
		});
	}
	
	@Test
	public void fetchInteger() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RObject x= r.evalData("integer(" + this.length + "L)",
					null, RObjectFactory.F_ONLY_STRUCT, RService.DEPTH_INFINITE, m );
			
			assertRObject(RObject.TYPE_VECTOR, CLASSNAME_INTEGER, this.length, x, "x");
			
			final var store= assertRData(RStore.INTEGER, -1, x.getData(), "x.data");
			
			assertThrows(UnsupportedOperationException.class, () -> {
				store.isNA(0);
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				store.getInt(0);
			});
		});
	}
	
	@Test
	public void fetchNumeric() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RObject x= r.evalData("numeric(" + this.length + "L)",
					null, RObjectFactory.F_ONLY_STRUCT, RService.DEPTH_INFINITE, m );
			
			assertRObject(RObject.TYPE_VECTOR, CLASSNAME_NUMERIC, this.length, x, "x");
			
			final var store= assertRData(RStore.NUMERIC, -1, x.getData(), "x.data");
			
			assertThrows(UnsupportedOperationException.class, () -> {
				store.isNA(0);
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				store.getNum(0);
			});
		});
	}
	
	@Test
	public void fetchComplex() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RObject x= r.evalData("complex(" + this.length + "L)",
					null, RObjectFactory.F_ONLY_STRUCT, RService.DEPTH_INFINITE, m );
			
			assertRObject(RObject.TYPE_VECTOR, CLASSNAME_COMPLEX, this.length, x, "x");
			
			final var store= assertRData(RStore.COMPLEX, -1, x.getData(), "x.data");
			
			assertThrows(UnsupportedOperationException.class, () -> {
				store.isNA(0);
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				store.getCplxRe(0);
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				store.getCplxIm(0);
			});
		});
	}
	
	@Test
	public void fetchChar() throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RObject x= r.evalData("character(" + this.length + "L)",
					null, RObjectFactory.F_ONLY_STRUCT, RService.DEPTH_INFINITE, m );
			
			assertRObject(RObject.TYPE_VECTOR, CLASSNAME_CHARACTER, this.length, x, "x");
			
			final var store= assertRData(RStore.CHARACTER, -1, x.getData(), "x.data");
			
			assertThrows(UnsupportedOperationException.class, () -> {
				store.isNA(0);
			});
			assertThrows(UnsupportedOperationException.class, () -> {
				store.getChar(0);
			});
		});
	}
	
	
}
