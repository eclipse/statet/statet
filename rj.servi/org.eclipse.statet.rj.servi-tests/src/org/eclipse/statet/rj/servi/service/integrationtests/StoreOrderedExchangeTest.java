/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_FACTOR;
import static org.eclipse.statet.rj.data.RObject.CLASSNAME_ORDERED;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.verifyPremise;

import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.ProgressMonitor;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RFactorStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StoreOrderedExchangeTest extends StoreFactorExchangeTest {
	
	
	private static final int intNA= Integer.MIN_VALUE;
	
	
	public StoreOrderedExchangeTest() throws Exception {
	}
	
	
	@Override
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void fetchData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_FactorVector("x", testArg, CLASSNAME_ORDERED, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertFactorVector(x, testArg, CLASSNAME_ORDERED, "x");
		});
	}
	
	@Override
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void assignData(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RVector<RFactorStore> x= this.rObjectFactory.createOrderedFactorVector(testArg.dataLength,
					testArg.levelLabels );
			final RFactorStore data= x.getData();
			
			setData(data, testArg);
			verifyPremise(() -> assertFactorVector(x, testArg, CLASSNAME_ORDERED, "x"));
			
			r.assignData("x", x, m);
			
			assertRObjectInR("x", RObject.TYPE_VECTOR, new String[] { CLASSNAME_ORDERED, CLASSNAME_FACTOR }, testArg.dataLength,
					r, m, "x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.factor(x)", m) ),
					"x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.ordered(x)", m) ),
					"x" );
			createInR_FactorVector("expected", testArg, CLASSNAME_ORDERED, r, m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, x)", m) ),
					"x" );
		});
	}
	
	
}
