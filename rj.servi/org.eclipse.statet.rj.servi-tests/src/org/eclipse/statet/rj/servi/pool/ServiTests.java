/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.rmi.RMIRegistryManager;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;

import org.eclipse.statet.rj.RjInitFailedException;
import org.eclipse.statet.rj.RjInvalidConfigurationException;
import org.eclipse.statet.rj.server.util.RJContext;


@NonNullByDefault
public class ServiTests {
	
	
	private static boolean isInit= false;
	
	public static synchronized void initEnv() throws Exception {
		if (!isInit) {
			CommonsRuntime.check(true);
			
			RMIRegistryManager.INSTANCE.setEmbeddedPrivateMode(false);
			RMIRegistryManager.INSTANCE.setEmbeddedPrivatePort(getRegistryPort());
			
			isInit= true;
		}
	}
	
	public static RJContext getRJContext() throws RjInitFailedException {
		return new RJContext() {
			@Deprecated
			@Override
			public String getServerPolicyFilePath() throws RjInvalidConfigurationException {
				throw new AssertionError();
			}
		};
	}
	
	public static int getRegistryPort() {
		return 8099;
	}
	
}
