/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.service.integrationtests;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.collections.ImCollections.newIntSequence;
import static org.eclipse.statet.jcommons.collections.ImCollections.repeat;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.rj.data.RObject.CLASSNAME_CHARACTER;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertChar;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertNA;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObject;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.assertRObjectInR;
import static org.eclipse.statet.rj.servi.test.RDataAssertions.verifyPremise;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.condition.EnabledIf;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIntList;
import org.eclipse.statet.jcommons.collections.IntArrayList;
import org.eclipse.statet.jcommons.collections.IntList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.services.RService;


@EnabledIfEnvironmentVariable(named= "STATET_TEST_FILES", matches= ".+")
@NonNullByDefault
public class StoreCharExchangeTest extends AbstractIntegrationTest {
	
	
	protected static final @Nullable Charset NATIVE_CHARSET;
	static {
		final String rNativeCharsetName= System.getenv("R_NATIVE_CHARSET");
		NATIVE_CHARSET= (rNativeCharsetName != null) ? Charset.forName(rNativeCharsetName) : null;
	}
	
	private static final int SAMPLE_LENGTH;
	private static final int[] SAMPLE_REPS;
	private static final String UTF8_METHOD;
	static {
		SAMPLE_LENGTH= 1500;
		SAMPLE_REPS= new int[] { 1, 2/*10*/ };
		UTF8_METHOD= "Cp2Utf8"; // raw, Cp2Utf8
	}
	
	
	
	protected static final String[] U_PLANES= new String[17];
	static {
		Arrays.fill(U_PLANES, "(unassigned)");
		U_PLANES[0x00]= "BMP";
		U_PLANES[0x01]= "SMP";
		U_PLANES[0x02]= "SIP";
		U_PLANES[0x03]= "TIP";
		U_PLANES[0x0E]= "SSP";
		U_PLANES[0x0F]= "SPU-A";
		U_PLANES[0x10]= "SPU-B";
	}
	
	private static boolean isValidCodepoint(final int cp) {
		return (cp >= 0x0001 && cp <= 0x10FFFF
				&& !(cp >= 0xD800 && cp <= 0xDFFF) );
	}
	
	private static boolean isLatin1Codepoint(final int cp) {
		return (cp >= 0x0001 && cp <= 0x00FF
				&& !(cp >= 0x0080 && cp <= 0x009F) );
	}
	
	private static final ImIntList ASCII_CODEPOINTS;
	private static final ImIntList LATIN1_CODEPOINTS;
	private static final ImIntList NATIVE_CODEPOINTS;
	static {
		final IntList cps= new IntArrayList();
		{	cps.clear();
			for (int cp= 0x0001; cp <= 0x007F; cp++) {
				cps.add(cp);
			}
			ASCII_CODEPOINTS= ImCollections.toIntList(cps);
		}
		{	cps.clear();
			for (int cp= 0x0001; cp <= 0x00FF; cp++) {
				if (isLatin1Codepoint(cp)) {
					cps.add(cp);
				}
			}
			LATIN1_CODEPOINTS= ImCollections.toIntList(cps);
		}
		if (NATIVE_CHARSET != null) {
			cps.clear();
			final CharsetEncoder encoder= NATIVE_CHARSET.newEncoder();
			for (int cp= 0x0001; cp <= 0xFFFF; cp++) {
				final String s= Character.toString(cp);
				if (encoder.canEncode(s)) {
					cps.add(cp);
				}
			}
			NATIVE_CODEPOINTS= ImCollections.toIntList(cps);
		}
		else {
			NATIVE_CODEPOINTS= null;
		}
	}
	
	
	protected static final int ASCII=   1 << 0;
	protected static final int LATIN1=  1 << 1;
	protected static final int BMP=     1 << 2;
	protected static final int ALL=     1 << 4;
	protected static final int NATIVE=  1 << 5;
	
	protected static String getLabel(final int flags) {
		switch (flags & 0xFF) {
		case ASCII:
			return "U+0001-U+007F (ASCII)";
		case LATIN1:
			return "U+0001-U+00FF \\ U+0080-U+009F (Latin-1)";
		case BMP:
			return "U+0001-U+FFFF (BMP)";
		case ALL:
			return "U+0001-U+10FFFF";
		case NATIVE:
			return "Native (" + NATIVE_CHARSET + ")";
		default:
			throw new IllegalArgumentException(String.format("%1$08X", flags));
		}
	}
	
	protected static IntSupplier createCodepointGen(final int flags, final Random rnd) {
		final ImIntList cps;
		switch (flags & 0xFF) {
		case ASCII:
			cps= ASCII_CODEPOINTS;
			break;
		case LATIN1:
			cps= LATIN1_CODEPOINTS;
			break;
		case BMP:
			return new IntSupplier() {
				@Override
				public int getAsInt() {
					while (true) {
						final int cp= 1 + rnd.nextInt(0xFFFF);
						if (isValidCodepoint(cp)) {
							return cp;
						}
					}
				}
			};
		case ALL:
			return new IntSupplier() {
				@Override
				public int getAsInt() {
					while (true) {
						final int cp= 1 + rnd.nextInt(0x10FFFF);
						if (isValidCodepoint(cp)) {
							return cp;
						}
					}
				}
			};
		case NATIVE:
			cps= NATIVE_CODEPOINTS;
			break;
		default:
			throw new IllegalArgumentException(String.format("%1$08X", flags));
		}
		
		return new IntSupplier() {
			@Override
			public int getAsInt() {
				return cps.getAt(rnd.nextInt(cps.size()));
			}
		};
	}
	
	
	protected static class TestArg {
		
		final int flags;
		
		final @Nullable String[] dataSegValues;
		final int dataSegValuesMult;
		final int dataSegRep;
		final int dataLength;
		
		final String label;
		
		public TestArg(final @Nullable String[] values, final int valueMult, final int valuesRep,
				final int flags, final String valuesLabel) {
			this.dataSegValues= values;
			this.dataSegValuesMult= valueMult;
			this.dataSegRep= valuesRep;
			this.dataLength= values.length * valuesRep;
			
			this.flags= flags;
			
			final StringBuilder sb= new StringBuilder(valuesLabel);
			sb.append(" x").append(valuesRep);
			if (values.length == 1) {
				final String s= values[0];
				sb.append(String.format(" { s[0].length.java= %1$s, s[0].length.utf8= %2$s }",
						(s != null) ? s.length() : "NA",
						(s != null) ? s.getBytes(StandardCharsets.UTF_8).length : "NA" ));
			}
			this.label= sb.toString();
		}
		
		public TestArg(final @Nullable String[] values, final int valuesRep,
				final int flags, final String valuesLabel) {
			this(values, 1, valuesRep, flags, valuesLabel);
		}
		
		public TestArg(final String value, final int valueMult,
				final int flags, final String valuesLabel) {
			this(new @Nullable String[] { value }, valueMult, 1, flags, valuesLabel);
		}
		
		
		@Override
		public String toString() {
			return this.label;
		}
		
	}
	
	
	private static final List<TestArg> TEST_CASE_DATAS= new ArrayList<>();
	
	private static int fillChars(final @Nullable String[] values, int idx,
			final IntList cps, final StringBuilder sb) {
		for (int i= 0; i < cps.size(); idx++, i++) {
			sb.setLength(0);
			sb.appendCodePoint(cps.getAt(i));
			values[idx]= sb.toString();
		}
		return idx;
	}
	private static @Nullable String[] createCharStrings(final IntList cps,
			final StringBuilder sb) {
		final var values= new @Nullable String[cps.size()];
		fillChars(values, 0, cps, sb);
		return values;
	}
	
	private static void addTestArgs(final @Nullable String[] values, final int[] valuesReps,
			final int flags, final String valuesLabel) {
		for (final int rep : valuesReps) {
			TEST_CASE_DATAS.add(new TestArg(values, rep, flags, valuesLabel));
		}
	}
	
	private static void addSampleTests(final int flags, final StringBuilder sb) {
		final Random rnd= new Random(782535 + flags);
		final IntSupplier gen= createCodepointGen(flags, rnd);
		final var values= new @Nullable String[SAMPLE_LENGTH];
		sb.setLength(0);
		int idx= 0;
		switch (flags & 0xFF) {
		case ASCII:
			{	final int[] cps= shuffle(repeat(createCodepointSeq(0x01, 0x7F), 2), rnd);
				for (int i= 0; i < cps.length; i++) {
					final int cp= cps[i];
					sb.appendCodePoint(cp);
					values[idx++]= sb.toString();
				}
				break;
			}
		case LATIN1:
			{	final int[] cps= shuffle(createCodepointSeq(0x01, 0xFF), rnd);
				for (int i= 0; i < cps.length; i++) {
					int cp= cps[i];
					if (!isLatin1Codepoint(cp)) {
						cp= gen.getAsInt();
					}
					sb.appendCodePoint(cp);
					values[idx++]= sb.toString();
				}
				break;
			}
		case ALL:
			{	final int[] ps= shuffle(repeat(newIntSequence(0, U_PLANES.length), 10), rnd);
				for (int p= 0; p < ps.length;) {
					final int cp= (ps[p] << 16) + rnd.nextInt(0x10000);
					if (isValidCodepoint(cp)) {
						sb.appendCodePoint(cp);
						values[idx++]= sb.toString();
						p++;
					}
				}
			}
			break;
		default:
			{	for (int i= 0; i < 0xFF; i++) {
					sb.appendCodePoint(gen.getAsInt());
					values[idx++]= sb.toString();
				}
				break;
			}
		}
		values[idx++]= null;
		
		while (idx < values.length) {
			for (int i= 0; i < 10; i++) {
				final int cp= gen.getAsInt();
				sb.appendCodePoint(cp);
			}
			values[idx++]= sb.toString();
		}
		
		addTestArgs(Arrays.copyOfRange(values, 0, 0x100),
				new int[] { 1, 2, 3, 15 },
				flags, getLabel(flags) + " Sample 256" );
		addTestArgs(values,
				SAMPLE_REPS,
				flags, getLabel(flags) + " Sample " + values.length );
	}
	private static void addLargeStringTest(final int flags, final int mult, final StringBuilder sb) {
		final Random rnd= new Random(32535 + flags);
		final IntSupplier gen= createCodepointGen(flags, rnd);
		sb.setLength(0);
		final int fragLength= 200000;
		for (int i= 0; i < fragLength; i++) {
			final int cp= gen.getAsInt();
			sb.appendCodePoint(cp);
		}
		final String fragment= sb.toString();
		for (int i= 1; i < mult; i++) {
			sb.append(fragment);
		}
		TEST_CASE_DATAS.add(new TestArg(sb.toString(), mult, flags, getLabel(flags) + " LargeString"));
	}
	
	static {
		final StringBuilder sb= new StringBuilder(2000000);
		
		final int[] singleRep= new int[] { 1, 2, 3, 8, 15, 150, 1500, 15000, 100000 };
		addTestArgs(new @Nullable String[] { }, new int[] { 1 }, ASCII, "Empty");
		addTestArgs(new @Nullable String[] { "A" }, singleRep, ASCII, "Single Char");
		addTestArgs(createCharStrings(ASCII_CODEPOINTS, sb), new int[] { 1 },
				ASCII, getLabel(ASCII) + " Chars" );
		addTestArgs(createCharStrings(createCodepointSeq(0x00A0, 0x00FF), sb), new int[] { 1 },
				LATIN1, "U+00A0-U+00FF (add Latin-1) Chars" );
		addTestArgs(createCharStrings(createCodepointSeq(0x0800, 0x0FFF), sb), new int[] { 1 },
				BMP, "U+0080-U+0FFF Chars" );
		{	final var values= new @Nullable String[59392];
			int idx= 0;
			idx= fillChars(values, idx, createCodepointSeq(0x1000, 0xD7FF), sb);
			idx= fillChars(values, idx, createCodepointSeq(0xE000, 0xFFFF), sb);
			addTestArgs(values, new int[] { 1 },
					BMP, "U+1000-U+FFFF \\ U+D800-U+DFFF (add-BMP without high/low-surrogates) Chars" );
		}
		for (int p= 1; p < U_PLANES.length; p++) {
			addTestArgs(createCharStrings(createCodepointSeq(p << 16, (p << 16) + 0xFFFF), sb),
					new int[] { 1 },
					ALL, String.format("U+%1$X0000-U+%1$XFFFF (%2$s) Chars", p, U_PLANES[p]) );
		}
		addTestArgs(new @Nullable String[] { null }, singleRep, ASCII, "NA");
		
		addSampleTests(ASCII, sb);
		addSampleTests(LATIN1, sb);
		addSampleTests(BMP, sb);
		addSampleTests(ALL, sb);
		
		final int f= 12;
		addLargeStringTest(ASCII, 4 * f, sb);
		addLargeStringTest(LATIN1, 3 * f, sb);
		addLargeStringTest(BMP, (int)(1.5 * f), sb);
		addLargeStringTest(ALL, 1 * f, sb);
		
		if (NATIVE_CHARSET != null) {
			System.out.println("NATIVE.codepoints.count= " + NATIVE_CODEPOINTS.size() + "\n"
					+ "      .codepoints= " + NATIVE_CODEPOINTS.stream()
							.mapToObj((cp) -> String.format("U+%1$04X", cp))
							.collect(Collectors.joining(", ", "[ ", " ]")) );
			addTestArgs(createCharStrings(NATIVE_CODEPOINTS, sb), new int[] { 1 },
					NATIVE, getLabel(NATIVE) + " Chars" );
			addSampleTests(NATIVE, sb);
		}
	}
	
	static Stream<TestArg> provideCaseDatas() {
		return TEST_CASE_DATAS.stream()
				.filter((testArg) -> ((testArg.flags & (NATIVE)) == 0));
	}
	
	static Stream<TestArg> provideAsciiCaseDatas() {
		return TEST_CASE_DATAS.stream()
				.filter((testArg) -> ((testArg.flags & (ASCII)) != 0));
	}
	
	static Stream<TestArg> provideLatin1CaseDatas() {
		return TEST_CASE_DATAS.stream()
				.filter((testArg) -> ((testArg.flags & (ASCII | LATIN1)) != 0));
	}
	
	static boolean isNativeEnabled() {
		return (NATIVE_CHARSET != null);
	}
	
	static Stream<TestArg> provideNativeCaseDatas() {
		return TEST_CASE_DATAS.stream()
				.filter((testArg) -> ((testArg.flags & (NATIVE)) != 0));
	}
	
	
	public StoreCharExchangeTest() throws Exception {
	}
	
	
	protected void createInR_byRawToChar(final String name, final TestArg testArg,
			final Charset charset, final @Nullable String rEncoding,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder("{ ");
		
		final @Nullable String[] values= testArg.dataSegValues;
		command.append(name).append(" <- character(").append(values.length).append("L); ");
		for (int segIdx= 0; segIdx < values.length; segIdx++) {
			command.append(name).append("[").append((segIdx + 1)).append("] <- ");
			final String value= values[segIdx];
			if (value == null) {
				command.append("NA");
			}
			else if (value.isEmpty()) {
				command.append("''");
			}
			else {
				if (testArg.dataSegValuesMult > 1) {
					command.append("rawToChar(rep.int(as.raw(c(");
					final byte[] bytes= value.substring(0, value.length() / testArg.dataSegValuesMult)
							.getBytes(charset);
					for (int i= 0; i < bytes.length; i++) {
						command.append(Byte.toUnsignedInt(bytes[i])).append("L,");
					}
					command.delete(command.length() - 1, command.length());
					command.append(")), ").append(testArg.dataSegValuesMult).append("L))");
				}
				else {
					command.append("rawToChar(as.raw(c(");
					final byte[] bytes= value.getBytes(charset);
					for (int i= 0; i < bytes.length; i++) {
						command.append(Byte.toUnsignedInt(bytes[i])).append("L,");
					}
					command.delete(command.length() - 1, command.length());
					command.append(")))");
				}
			}
			command.append("; ");
		}
		if (rEncoding != null) {
			command.append("Encoding(").append(name).append(") <- '").append(rEncoding).append("'; ");
		}
		if (testArg.dataSegRep > 1) {
			command.append(name).append(" <- rep.int(").append(name)
					.append(", times= ").append(testArg.dataSegRep).append("L); ");
		}
		
		command.append(" }");
		
		r.evalVoid(command.toString(), m);
	}
	
	protected void createInR_byIntToUtf8(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		final StringBuilder command= new StringBuilder("{ ");
		
		final @Nullable String[] values= testArg.dataSegValues;
		command.append(name).append(" <- character(").append(values.length).append("L); ");
		for (int segIdx= 0; segIdx < values.length; segIdx++) {
			command.append(name).append("[").append((segIdx + 1)).append("] <- ");
			final String value= values[segIdx];
			if (value == null) {
				command.append("NA");
			}
			else if (value.isEmpty()) {
				command.append("''");
			}
			else {
				if (testArg.dataSegValuesMult > 1) {
					command.append("intToUtf8(rep.int(c(");
					value.substring(0, value.length() / testArg.dataSegValuesMult)
							.codePoints().forEachOrdered((cp) -> command.append(cp).append("L,"));
					command.delete(command.length() - 1, command.length());
					command.append("), ").append(testArg.dataSegValuesMult).append("L))");
				}
				else {
					command.append("intToUtf8(c(");
					value.codePoints().forEachOrdered((cp) -> command.append(cp).append("L,"));
					command.delete(command.length() - 1, command.length());
					command.append("))");
				}
			}
			command.append("; ");
		}
		if (testArg.dataSegRep > 1) {
			command.append(name).append(" <- rep.int(").append(name)
					.append(", times= ").append(testArg.dataSegRep).append("L); ");
		}
		
		command.append(" }");
		
		r.evalVoid(command.toString(), m);
	}
	
	protected void createInR_CharUtf8Vector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		if (UTF8_METHOD == "Cp2Utf8") {
			createInR_byIntToUtf8(name, testArg, r, m);
		}
		else {
			createInR_byRawToChar(name, testArg, StandardCharsets.UTF_8, "UTF-8", r, m);
		}
	}
	
	protected void createInR_CharAsciiVector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		createInR_byRawToChar(name, testArg, StandardCharsets.US_ASCII, null, r, m);
	}
	
	protected void createInR_CharLatin1Vector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		createInR_byRawToChar(name, testArg, StandardCharsets.ISO_8859_1, "latin1", r, m);
	}
	
	protected void createInR_CharNativeVector(final String name, final TestArg testArg,
			final RService r, final ProgressMonitor m) throws StatusException {
		createInR_byRawToChar(name, testArg, nonNullAssert(NATIVE_CHARSET), null, r, m);
	}
	
	protected void setData(final RCharacterStore data, final TestArg testArg) {
		final @Nullable String[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				final String value= values[segIdx];
				if (value == null) {
					data.setNA(idx);
				}
				else {
					data.setChar(idx, value);
				}
			}
		}
	}
	
	protected void assertCharVector(final RObject rObject, final TestArg testArg,
			final String name) throws UnexpectedRDataException {
		final RVector<?> vector= assertRObject(RObject.TYPE_VECTOR, RVector.class,
				CLASSNAME_CHARACTER, testArg.dataLength,
				rObject, name );
		final RCharacterStore data= assertInstanceOf(RCharacterStore.class, vector.getData());
		
		final String[] values= testArg.dataSegValues;
		for (int idx= 0, rep= 0; rep < testArg.dataSegRep; rep++) {
			for (int segIdx= 0; segIdx < values.length; idx++, segIdx++) {
				final String value= values[segIdx];
				if (value == null) {
					assertNA(data, idx, name);
				}
				else {
					assertChar(value, data, idx, name);
				}
			}
		}
	}
	
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void fetchData_Utf8(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_CharUtf8Vector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertCharVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideAsciiCaseDatas")
	public void fetchData_Ascii(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_CharAsciiVector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertCharVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideLatin1CaseDatas")
	public void fetchData_Latin1(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_CharLatin1Vector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertCharVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@EnabledIf("isNativeEnabled")
	@MethodSource("provideNativeCaseDatas")
	public void fetchData_Native(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			createInR_CharNativeVector("x", testArg, r, m);
			
			final RObject x= r.evalData("x", m);
			
			assertCharVector(x, testArg, "x");
		});
	}
	
	@ParameterizedTest
	@MethodSource("provideCaseDatas")
	public void assignData_Utf8(final TestArg testArg) throws Throwable {
		runWithRService((final RService r, final ProgressMonitor m) -> {
			
			final RVector<RCharacterStore> x= this.rObjectFactory.createCharVector(testArg.dataLength);
			final RCharacterStore data= x.getData();
			
			setData(data, testArg);
			verifyPremise(() -> assertCharVector(x, testArg, "x"));
			
			r.assignData("x", x, m);
			
			assertRObjectInR("x", RObject.TYPE_VECTOR, new String[] { CLASSNAME_CHARACTER }, testArg.dataLength,
					r, m, "x" );
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("is.character(x)", m) ),
					"x" );
			createInR_CharUtf8Vector("expected", testArg, r, m);
			assertTrue(RDataUtils.checkSingleLogiValue(
					r.evalData("identical(expected, x)", m) ),
					"x" );
		});
	}
	
	
	private static ImIntList createCodepointSeq(final int first, final int last) {
		return ImCollections.newIntSequence(first, last + 1);
	}
	
}
