/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.management.JMException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.servi.pool.PoolNodeObject;
import org.eclipse.statet.rj.servi.pool.PoolServer;


@NonNullByDefault
public class MXNodeManager implements PoolListener {
	
	
	private final PoolServer server;
	
	private final Map<PoolNodeObject, MXNode> nodes= new HashMap<>();
	
	private final PoolManager poolManager;
	
	
	public MXNodeManager(final PoolServer server, final PoolManager poolManager) {
		this.server= server;
		this.poolManager= poolManager;
	}
	
	
	@Override
	public void initializing(final PoolNodeObject nodeObj) {
	}
	
	@Override
	public void initialized(final PoolNodeObject nodeObj) {
		final MXNode node;
		synchronized (this) {
			if (this.nodes.containsKey(nodeObj)) {
				return;
			}
			node= new MXNode(this.server, nodeObj);
			this.nodes.put(nodeObj, node);
		}
		try {
			node.initJM();
		}
		catch (final JMException e) {
			Utils.logError("An error occurred when initializing JMX for R node '" + node.getId() + "'.", e);
		}
	}
	
	@Override
	public void disposed(final PoolNodeObject nodeObj) {
		final MXNode node= this.nodes.remove(nodeObj);
		if (node != null) {
			dispose(node);
		}
	}
	
	private void dispose(final MXNode node) {
		try {
			node.disposeJM();
		}
		catch (final JMException e) {
			Utils.logError("An error occurred when disposing JMX for R node '" + node.getId() + "'.", e);
		}
	}
	
	public void activate() {
		this.poolManager.addPoolListener(this);
		
		final Collection<? extends PoolNodeObject> objects= this.poolManager.getPoolNodeObjects();
		synchronized (this) {
			for (final PoolNodeObject nodeObj : objects) {
				switch (nodeObj.getState()) {
				case DISPOSED:
					break;
				default:
					initialized(nodeObj);
					break;
				}
			}
		}
	}
	
	public void deactivate() {
		this.poolManager.removePoolListener(this);
		
		synchronized (this) {
			try {
				for (final Entry<PoolNodeObject, MXNode> entry : this.nodes.entrySet()) {
					dispose(entry.getValue());
				}
			}
			finally {
				this.nodes.clear();
			}
		}
	}
	
}
