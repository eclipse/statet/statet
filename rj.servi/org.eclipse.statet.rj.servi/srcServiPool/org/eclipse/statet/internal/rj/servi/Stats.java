/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.servi.pool.PoolNodeObject;


@NonNullByDefault
public class Stats implements PoolListener {
	
	
	public final static int MAX_USAGE= 1;
	public final static int VALIDATION_FAILED= 2;
	
	public final static int EXHAUSTED_FAILED= 3;
	public final static int UNEXPECTED_FAILED= 4;
	
	
	static class NodeEntry {
		int shutdownReason;
	}
	
	
//	private final ArrayList<NodeEntry> tempList= new ArrayList<NodeEntry>();
	
	
	@Override
	public void initializing(final PoolNodeObject nodeObj) {
	}
	
	@Override
	public void initialized(final PoolNodeObject nodeObj) {
//		this.tempList.add(p.stats);
	}
	
	@Override
	public void disposed(final PoolNodeObject nodeObj) {
	}
	
	public void logServUsage(final int borrowTime, final int evalTime) {
	}
	
	public void logServRequestFailed(final int reason) {
	}
	
}
