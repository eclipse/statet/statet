/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import java.time.Instant;
import java.util.Date;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.rmi.RMIAddress;

import org.eclipse.statet.rj.servi.jmx.NodeStateMX;
import org.eclipse.statet.rj.servi.pool.PoolNodeItem;
import org.eclipse.statet.rj.servi.pool.PoolNodeState;


@NonNullByDefault
public class MXNodeState implements NodeStateMX {
	
	
	private final PoolNodeItem item;
	
	private final Instant creationTime;
	private final Instant stateTime;
	
	
	public MXNodeState(final PoolNodeItem item) {
		this.item= item;
		
		this.creationTime= this.item.getCreationTime();
		this.stateTime= this.item.getStateTime();
	}
	
	
	@Override
	public PoolNodeState getState() {
		return this.item.getState();
	}
	
	@Override
	public Date getStateBeginTime() {
		return Date.from(this.stateTime);
	}
	
	@Override
	public @Nullable String getCurrentClientLabel() {
		return this.item.getCurrentClientLabel();
	}
	
	
	@Override
	public Date getCreationTime() {
		return Date.from(this.creationTime);
	}
	
	@Override
	public long getUsageCount() {
		return this.item.getUsageCount();
	}
	
	@Override
	public long getUsageDurationMillis() {
		final var duration= this.item.getUsageDuration();
		return (duration != null) ? duration.toMillis() : -1;
	}
	
	@Override
	public @Nullable String getRMIAddress() {
		final RMIAddress address= this.item.getAddress();
		return (address != null) ? address.getAddress() : null;
	}
	
}
