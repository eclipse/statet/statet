/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.lang.management.ManagementFactory;

import javax.management.JMException;
import javax.management.ObjectName;
import javax.management.OperationsException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.servi.jmx.NetConfigMXBean;
import org.eclipse.statet.rj.servi.pool.NetConfig;
import org.eclipse.statet.rj.servi.pool.PoolServer;


@NonNullByDefault
public class MXNetConfig extends NetConfig implements NetConfigMXBean {
	
	
	private final PoolServer server;
	
	private @Nullable ObjectName jmName;
	
	
	public MXNetConfig(final PoolServer server) {
		this.server= nonNullAssert(server);
	}
	
	
	public void initJM() throws JMException {
		final ObjectName jmName= new ObjectName(this.server.getJMBaseName() + "type=Server.NetConfig");
		this.jmName= jmName;
		ManagementFactory.getPlatformMBeanServer().registerMBean(this, jmName);
	}
	
	public void disposeJM() throws JMException {
		final ObjectName jmName= this.jmName;
		if (jmName != null) {
			this.jmName= null;
			ManagementFactory.getPlatformMBeanServer().unregisterMBean(jmName);
		}
	}
	
	
	@Override
	public void loadDefault() throws OperationsException {
		load(new NetConfig());
	}
	
	@Override
	public void loadSaved() throws OperationsException {
		MXUtils.load(this, this.server.getRJContext());
	}
	
	@Override
	public void save() throws OperationsException {
		MXUtils.save(this, this.server.getRJContext());
	}
	
}
