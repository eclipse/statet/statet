/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import java.time.Duration;
import java.time.Instant;

import org.apache.commons.pool2.impl.DefaultPooledObject;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class APool2NodeObject extends DefaultPooledObject<APool2NodeHandler> {
	
	
	private volatile long lastAllocatedBeginNanos;
	private volatile long lastAllocatedEndNanos;
	
	private volatile Instant stateTime;
	
	
	public APool2NodeObject(final APool2NodeHandler object) {
		super(object);
		this.stateTime= getCreateInstant();
	}
	
	
	@Override
	public synchronized boolean allocate() {
		if (super.allocate()) {
			this.lastAllocatedBeginNanos= System.nanoTime();
			this.stateTime= Instant.now();
			return true;
		}
		return false;
	}
	
	@Override
	public synchronized void markReturning() {
		switch (getState()) {
		case RETURNING:
			return;
		case ALLOCATED:
			this.lastAllocatedEndNanos= System.nanoTime();
			//$FALL-THROUGH$
		default:
			this.stateTime= Instant.now();
			super.markReturning();
			return;
		}
	}
	
	@Override
	public synchronized boolean deallocate() {
		switch (getState()) {
		case IDLE:
			return false;
		case ALLOCATED:
			this.lastAllocatedEndNanos= System.nanoTime();
			//$FALL-THROUGH$
		case RETURNING:
			this.stateTime= Instant.now();
			return super.deallocate();
		default:
			return false;
		}
	}
	
	@Override
	public synchronized void invalidate() {
		switch (getState()) {
		case INVALID:
			return;
		case ALLOCATED:
			this.lastAllocatedEndNanos= System.nanoTime();
			//$FALL-THROUGH$
		default:
			this.stateTime= Instant.now();
			super.invalidate();
			return;
		}
	}
	
	
	public Instant getStateTime() {
		return this.stateTime;
	}
	
	@Override
	public @Nullable Duration getActiveDuration() {
		if (getBorrowedCount() <= 0) {
			return null;
		}
		final long end= this.lastAllocatedEndNanos;
		final long begin= this.lastAllocatedBeginNanos;
		long t= end - begin;
		if (t < 0) {
			t= System.nanoTime() - begin;
		}
		return Duration.ofNanos(t);
	}
	
	@Override
	public long getActiveTimeMillis() {
		final var duration= getActiveDuration();
		return (duration != null) ? duration.toMillis() : -1;
	}
	
}
