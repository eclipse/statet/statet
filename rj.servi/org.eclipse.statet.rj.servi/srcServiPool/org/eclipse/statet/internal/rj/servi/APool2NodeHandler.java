/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import static org.eclipse.statet.jcommons.util.Units.MILLI_FACTOR;
import static org.eclipse.statet.jcommons.util.Units.MILLI_NANO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.Duration;
import java.time.Instant;

import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectState;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.RjClosedException;
import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.pool.PoolNodeObject;
import org.eclipse.statet.rj.servi.pool.PoolNodeState;


@NonNullByDefault
public class APool2NodeHandler extends NodeHandler implements PoolNodeObject, RServiImpl.PoolRef {
	
	
	static final long safeNanos(final long nanos) {
		return (nanos != 0) ? nanos : 1;
	}
	
	static final long evictNanos(final @Nullable Duration timeout) {
		long nanos= System.nanoTime();
		if (timeout != null && !timeout.isNegative()) {
			nanos+= timeout.toNanos();
		}
		return safeNanos(nanos);
	}
	
	
	private final APool2 pool;
	private final APool2NodeObject p;
	private volatile long evict;
	
	final Stats.NodeEntry stats= new Stats.NodeEntry();
	
	@Nullable Remote thisRemote;
	
	private volatile long accessId= -1;
	
	private long allocationRenewPeriodNanos;
	private long allocationRenewTime;
	
	
	public APool2NodeHandler(final APool2 pool) {
		this.pool= pool;
		this.p= new APool2NodeObject(this);
	}
	
	
	@Override
	public NodeHandler getNodeHandler() {
		return this;
	}
	
	PooledObject<APool2NodeHandler> getPooledObject() {
		return this.p;
	}
	
	@Override
	public Instant getCreationTime() {
		return this.p.getCreateInstant();
	}
	
	@Override
	public long getAllocationCount() {
		return this.p.getBorrowedCount();
	}
	
	@Override
	public @Nullable Duration getLastestAllocationDuration() {
		return this.p.getActiveDuration();
	}
	
	@Override
	public PoolNodeState getState() {
		switch(this.p.getState()) {
		case IDLE:
			return (this.node == null) ?
					PoolNodeState.INITIALIZING :
					PoolNodeState.IDLING;
		case ALLOCATED:
			return PoolNodeState.ALLOCATED;
		case INVALID:
			return (this.node == null) ?
					PoolNodeState.DISPOSED :
					PoolNodeState.DISPOSING;
		default:
			return PoolNodeState.CHECKING;
		}
	}
	
	@Override
	@SuppressWarnings("null")
	public Instant getStateTime() {
		switch (getState()) {
		case DISPOSED:
			return getShutdownTime();
		default:
			return this.p.getStateTime();
		}
	}
	
	
	@Override
	void bindClient(final String name, final String host) throws RemoteException {
		super.bindClient(name, host);
		synchronized (this) {
			this.accessId= this.p.getBorrowedCount();
			final var renewPeriod= this.pool.getClientAllocationRenewPeriod();
			this.allocationRenewPeriodNanos= (renewPeriod != null) ? renewPeriod.toNanos() : -1;
			this.allocationRenewTime= System.nanoTime();
		}
	}
	
	void invalidateClient() {
		this.accessId= -1;
		setClientLabel(null);
	}
	
	@Override
	void unbindClient() throws RemoteException {
		synchronized (this) {
			invalidateClient();
		}
		super.unbindClient();
	}
	
	@Override
	public void evict(final @Nullable Duration timeout) {
		doEvict(evictNanos(timeout), timeout == null);
	}
	
	void doEvict(final long nanos, final boolean direct) {
		synchronized (this.p) {
			if (this.evict == 0 || this.evict - nanos > 0) {
				this.evict= safeNanos(nanos);
			}
		}
		
		if (direct) {
			try {
				this.pool.invalidateObject(this);
			}
			catch (final Exception e) {}
		}
	}
	
	boolean isEvictRequested(final long nanos) {
		final long evict= this.evict;
		return (evict != 0
				&& (nanos == 0 || nanos - evict >= 0) );
	}
	
	long getAccessId() {
		final long accessId= this.accessId;
		if (accessId == -1) {
			throw new IllegalAccessError();
		}
		return accessId;
	}
	
	@Override
	public long getCheckIntervalMillis() throws RemoteException {
		final long allocationRenewPeriodNanos;
		return ((allocationRenewPeriodNanos= this.allocationRenewPeriodNanos) != -1) ? 
				((allocationRenewPeriodNanos / (long)(2.125 * MILLI_NANO)) - 1 * MILLI_FACTOR) :
				-1;
	}
	
	@Override
	public synchronized void check(final long accessId) throws RjException, RemoteException {
		if (this.accessId != accessId) {
			throw new RjClosedException("RServi instance is no longer valid.");
		}
		this.allocationRenewTime= System.nanoTime();
	}
	
	void checkClientLost(final long nanos) {
		{	final long allocationRenewPeriodNanos;
			if (this.accessId == -1
					|| (allocationRenewPeriodNanos= this.allocationRenewPeriodNanos) == -1
					|| nanos - this.allocationRenewTime - allocationRenewPeriodNanos < 0
					|| getPooledObject().getState() == PooledObjectState.INVALID ) {
				return;
			}
		}
		String clientLabel;
		synchronized (this) {
			final long allocationRenewPeriodNanos;
			if (this.accessId == -1
					|| (allocationRenewPeriodNanos= this.allocationRenewPeriodNanos) == -1
					|| nanos - this.allocationRenewTime - allocationRenewPeriodNanos < 0 ) {
				return;
			}
			clientLabel= getClientLabel();
			doEvict(nanos, false);
		}
		Utils.logInfo(String.format("Abandoned RServi instance (client= %1$s) detected and marked for eviction.",
				(clientLabel != null) ? '\'' + clientLabel + '\'' : "<no label>" ));
	}
	
	@Override
	public void returnObject(final long accessId) throws RjException {
		try {
			synchronized (this) {
				if (this.accessId != accessId) {
					throw new RjClosedException("RServi instance is no longer valid.");
				}
				invalidateClient();
			}
			this.pool.returnObject(this);
		}
		catch (final Exception e) {
			Utils.logError("An unexpected error occurred when returning RServi instance.", e);
			throw new RjException("An unexpected error occurred when closing RServi instance. See server log for detail.");
		}
	}
	
}
