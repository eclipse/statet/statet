/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.RjInvalidConfigurationException;
import org.eclipse.statet.rj.server.util.RJContext;
import org.eclipse.statet.rj.servi.jmx.PoolServerMXBean;
import org.eclipse.statet.rj.servi.node.RServiNodeConfig;


/**
 * @since 2.0
 */
@NonNullByDefault
public interface PoolServer extends PoolServerMXBean {
	
	
	RJContext getRJContext();
	String getJMBaseName();
	
	void getNetConfig(NetConfig config);
	void setNetConfig(NetConfig config);
	
	void getPoolConfig(PoolConfig config);
	void setPoolConfig(PoolConfig config);
	
	void getNodeConfig(RServiNodeConfig config);
	void setNodeConfig(RServiNodeConfig config) throws RjInvalidConfigurationException;
	
	@Nullable RServiPoolManager getManager();
	
	
}
