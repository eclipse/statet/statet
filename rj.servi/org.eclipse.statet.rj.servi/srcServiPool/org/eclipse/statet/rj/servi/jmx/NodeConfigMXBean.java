/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.jmx;

import java.util.Map;

import javax.management.OperationsException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface NodeConfigMXBean {
	
	
	@DisplayName("R home / R_HOME (path)")
	@Nullable String getRHome();
	void setRHome(@Nullable String path);
	
	@DisplayName("Architecture of binaries / R_ARCH (empty \u21d2 autodetection)")
	@Nullable String getRArch();
	void setRArch(@Nullable String code);
	
	@DisplayName("Java home (path; empty \u21d2 same as the server)")
	@Nullable String getJavaHome();
	void setJavaHome(@Nullable String javaHome);
	
	@DisplayName("Java arguments")
	String getJavaArgs();
	void setJavaArgs(@Nullable String args);
	
	@DisplayName("Environment variables (like R_LIBS)")
	Map<String, String> getEnvironmentVariables();
	@DisplayName("Add/Set/Remove an environment variable (like R_LIBS)")
	void setEnvironmentVariable(String name, @Nullable String value);
	
	@DisplayName("Working directory (path; empty \u21d2 default temp dir)")
	@Nullable String getBaseWorkingDirectory();
	void setBaseWorkingDirectory(@Nullable String path);
	
	@DisplayName("R startup snippet (complete R command per line)")
	String getRStartupSnippet();
	void setRStartupSnippet(@Nullable String code);
	
	@DisplayName("Timeout when starting/stopping node (millisec)")
	long getStartStopTimeoutMillis();
	void setStartStopTimeoutMillis(long milliseconds);
	
	@DisplayName("Enable debug console")
	boolean getEnableConsole();
	void setEnableConsole(boolean enable);
	
	@DisplayName("Enable verbose logging")
	boolean getEnableVerbose();
	void setEnableVerbose(boolean enable);
	
	
	@DisplayName("Apply the current configuration")
	void apply() throws OperationsException;
	
	@DisplayName("Reset current changes / load actual configuration")
	void loadActual() throws OperationsException;
	@DisplayName("Load the default configuration")
	void loadDefault() throws OperationsException;
	@DisplayName("Load the saved configuration")
	void loadSaved() throws OperationsException;
	@DisplayName("Save the current configuration")
	void save() throws OperationsException;
	
}
