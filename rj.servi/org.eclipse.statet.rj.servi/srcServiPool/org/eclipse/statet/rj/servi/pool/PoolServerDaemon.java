/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * This class allows to run the {@link StandalonePoolServer} as daemon using JSVC of Apache
 * Commons-Daemon (http://commons.apache.org/proper/commons-daemon/).
 * 
 * Not tested, feedback welcome.
 */
@NonNullByDefault
public class PoolServerDaemon {
	
	
	private @Nullable StandalonePoolServer poolServer;
	
	
	public PoolServerDaemon() {
	}
	
	
	public synchronized void init(final String[] args) throws Exception {
		final StandalonePoolServer poolServer= this.poolServer;
		if (poolServer != null) {
			throw new IllegalStateException();
		}
		this.poolServer= StandalonePoolServer.initServer(args);
	}
	
	public void start() throws Exception {
		final StandalonePoolServer poolServer= this.poolServer;
		if (poolServer == null) {
			throw new IllegalStateException();
		}
		poolServer.start();
	}
	
	public void stop() throws Exception {
		final StandalonePoolServer poolServer= this.poolServer;
		if (poolServer == null) {
			throw new IllegalStateException();
		}
		poolServer.stop();
	}
	
	public synchronized void destroy() {
		final StandalonePoolServer poolServer= this.poolServer;
		if (poolServer != null) {
			this.poolServer= null;
			poolServer.shutdown();
		}
	}
	
}
