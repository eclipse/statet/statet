/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import java.time.Duration;
import java.time.Instant;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.rmi.RMIAddress;

import org.eclipse.statet.internal.rj.servi.NodeHandler;
import org.eclipse.statet.rj.RjException;


@NonNullByDefault
public class PoolNodeItem {
	
	
	private final PoolNodeObject nodeObj;
	
	private Instant creationTime;
	
	private PoolNodeState state;
	private Instant stateTime;
	
	private long usageCount;
	private @Nullable Duration usageDuration;
	
	private @Nullable RMIAddress address;
	private @Nullable String clientLabel;
	
	
	public PoolNodeItem(final PoolNodeObject nodeObj, final Instant stamp) {
		synchronized(nodeObj) {
			this.nodeObj= nodeObj;
			
			this.creationTime= nodeObj.getCreationTime();
			this.state= nodeObj.getState();
			this.stateTime= nodeObj.getStateTime();
			
			this.usageCount= nodeObj.getAllocationCount();
			this.usageDuration= nodeObj.getLastestAllocationDuration();
			
			final NodeHandler nodeHandler= nodeObj.getNodeHandler();
			this.address= (nodeHandler != null) ? nodeHandler.getAddress() : null;
			this.clientLabel= nodeObj.getClientLabel();
		}
	}
	
	
	/**
	 * @since 4.5
	 */
	public Instant getCreationTime() {
		return this.creationTime;
	}
	
	public PoolNodeState getState() {
		return this.state;
	}
	
	/**
	 * @since 4.5
	 */
	public Instant getStateTime() {
		return this.stateTime;
	}
	
	public @Nullable String getCurrentClientLabel() {
		return this.clientLabel;
	}
	
	public long getUsageCount() {
		return this.usageCount;
	}
	
	/**
	 * @since 4.5
	 */
	public @Nullable Duration getUsageDuration() {
		return this.usageDuration;
	}
	
	public boolean isConsoleEnabled() {
		final NodeHandler nodeHandler= this.nodeObj.getNodeHandler();
		return (nodeHandler != null && nodeHandler.isConsoleEnabled());
	}
	
	public void enableConsole(final String authConfig) throws RjException {
		final NodeHandler nodeHandler= this.nodeObj.getNodeHandler();
		if (nodeHandler != null) {
			nodeHandler.enableConsole(authConfig);
		}
	}
	
	public void disableConsole() throws RjException {
		final NodeHandler nodeHandler= this.nodeObj.getNodeHandler();
		if (nodeHandler != null) {
			nodeHandler.disableConsole();
		}
	}
	
	/**
	 * Returns the RMI address of the node.
	 * 
	 * @return the address of the node if available, otherwise <code>null</code>
	 * 
	 * @since 2.0
	 */
	public @Nullable RMIAddress getAddress() {
		return this.address;
	}
	
	/**
	 * Evicts the pool item.
	 * 
	 * The specified timeout is used when the node is in use.
	 * 
	 * @param timeout the timeout
	 * 
	 * @since 4.5
	 */
	public void evict(final @Nullable Duration timeout) {
		if (this.nodeObj != null) {
			this.nodeObj.evict(timeout);
		}
	}
	
}
