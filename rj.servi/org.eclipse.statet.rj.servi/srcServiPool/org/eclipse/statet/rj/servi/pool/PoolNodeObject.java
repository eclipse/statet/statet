/*=============================================================================#
 # Copyright (c) 2016, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import java.time.Duration;
import java.time.Instant;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.rj.servi.NodeHandler;


@NonNullByDefault
public interface PoolNodeObject {
	
	
	@Nullable NodeHandler getNodeHandler();
	
	/**
	 * Returns the time of the creation of the node.
	 * 
	 * @return the time in milliseconds (as specified by {@link System#currentTimeMillis()})
	 * 
	 * @since 4.5
	 */
	Instant getCreationTime();
	
	/**
	 * Returns the count of allocation of this node.
	 * 
	 * If the node is currently allocated, the current allocation is included in the returned
	 * number.
	 * 
	 * @return the count
	 */
	long getAllocationCount();
	
	/**
	 * Returns the duration of the latest allocation, if available.
	 * 
	 * If the node is currently allocated, it is the duration of the current allocation at this
	 * time.
	 * 
	 * @return the duration, or {@code null} if not available
	 * 
	 * @since 4.5
	 */
	@Nullable Duration getLastestAllocationDuration();
	
	/**
	 * Returns the current state.
	 * 
	 * @return the current state
	 */
	PoolNodeState getState();
	
	/**
	 * @since 4.5
	 */
	Instant getStateTime();
	
	@Nullable String getClientLabel();
	
	/**
	 * Marks this node for eviction.
	 * 
	 * @param timeout the timeout for eviction, or {@code null} for direct eviction in current
	 *     thread
	 * 
	 * @since 4.5
	 */
	void evict(@Nullable Duration timeout);
	
	@Deprecated(since= "4.5")
	default void evict(final long timeoutMillis) {
		evict((timeoutMillis == 0) ? null : Duration.ofMillis(timeoutMillis));
	}
	
	
}
