/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import java.nio.file.Files;
import java.nio.file.Path;

import javax.management.OperationsException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.runtime.CommonsRuntime;
import org.eclipse.statet.jcommons.status.StatusException;
import org.eclipse.statet.jcommons.status.WarningStatus;

import org.eclipse.statet.rj.RjInitFailedException;
import org.eclipse.statet.rj.RjInvalidConfigurationException;
import org.eclipse.statet.rj.server.util.RJContext;
import org.eclipse.statet.rj.servi.RServiUtils;
import org.eclipse.statet.rj.servi.jmx.StandalonePoolServerMXBean;


/**
 * Standalone version of pool server managed by JMX
 * 
 * <p>Required bundles (libraries): <code>org.eclipse.statet.rj.data</code>,
 * <code>org.eclipse.statet.rj.server</code>, <code>org.eclipse.statet.rj.client</code>,
 * <code>org.eclipse.statet.rj.servi</code>, <code>org.eclipse.statet.rj.services.eruntime</code>
 * </p>
 * <p>The configuration is loaded from / saved to the current directory.
 * </p>
 * <p>To start the server use e.g.:
 * <pre>
 * java -cp "*" org.eclipse.statet.rj.servi.pool.StandalonePoolServer &lt;id&gt;
 * </pre></p>
 * <p>By default the libraries are expected in the current directory. If they are located
 * in another directory, it must be specified in the java property <code>org.eclipse.statet.rj.path</code>:
 * <pre>
 * java -cp "/path/to/rjlibs/*" -Dorg.eclipse.statet.rj.path=/path/to/rjlibs/ org.eclipse.statet.rj.servi.pool.StandalonePoolServer &lt;id&gt;
 * </pre></p>
 * </p>
 */
@NonNullByDefault
public class StandalonePoolServer extends JMPoolServer implements StandalonePoolServerMXBean {
	
	
	protected StandalonePoolServer(final String id, final RJContext context) throws RjInitFailedException {
		super(id, context);
	}
	
	
	public static void main(final String[] args) throws Exception {
		final StandalonePoolServer server= initServer(args);
		
		try {
			server.start();
		}
		catch (final OperationsException e) {
			CommonsRuntime.log(new WarningStatus(RServiUtils.RJ_SERVI_ID,
					"The server is started, but the pool could not be started.",
					e ));
		}
	}
	
	static StandalonePoolServer initServer(final String[] args) throws StatusException, RjInitFailedException {
		final String id= (args.length > 0) ? args[0] : null;
		if (id == null || id.isEmpty()) {
			throw new IllegalArgumentException("No pool id specified.");
		}
		
		CommonsRuntime.check(true);
		
		final RJContext context= new RJContext() {
			@Deprecated
			@Override
			public String getServerPolicyFilePath() throws RjInvalidConfigurationException {
				final String path= getPropertiesDirPath() + "security.policy";
				if (Files.isRegularFile(Path.of(path))) {
					return path;
				}
				return super.getServerPolicyFilePath();
			}
		};
		
		return new StandalonePoolServer(id, context);
	}
	
	
}
