/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.pool;

import java.util.Collection;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.node.RServiNodeFactory;
import org.eclipse.statet.rj.servi.node.RServiPool;


@NonNullByDefault
public interface RServiPoolManager {
	
	
	class Counter {
		public int numIdling;
		public int numInUse;
		public int numTotal;
		public int maxIdling;
		public int maxInUse;
		public int maxTotal;
		
		public Counter() {}
	}
	
	
	String getId();
	
	PoolConfig getConfig();
	void setConfig(PoolConfig config);
	void addNodeFactory(RServiNodeFactory nodeFactory);
	ImCollection<? extends RServiNodeFactory> getFactories();
	
	void init() throws RjException;
	void stop(int mode) throws RjException;
	
	RServiPool getPool();
	
	RServiPoolManager.Counter getCounter();
	Collection<? extends PoolNodeObject> getPoolNodeObjects();
	
}
