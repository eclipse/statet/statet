/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.nio.file.Path;
import java.rmi.RemoteException;
import java.time.Instant;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.rmi.RMIAddress;

import org.eclipse.statet.internal.rj.servi.server.RServiBackend;
import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.servi.node.RServiNode;


@NonNullByDefault
public abstract class NodeHandler {
	
	
	static final byte INIT_0=   0x0;
	static final byte INIT_1=   0x1;
	static final byte INIT_2=   0x2;
	
	
	private byte handlerState;
	
	/*-- init in ::init1(...) --*/
	String nodeId= nonNullLateInit();
	RMIAddress address= nonNullLateInit();
	Path dir= nonNullLateInit();
	
	protected @Nullable RServiNode node;
	
	@Nullable Process process;
	
	private @Nullable String clientLabel;
	private @Nullable RServiBackend clientHandler;
	
	boolean isConsoleEnabled;
	
	private @Nullable Instant shutdownTime;
	
	
	public NodeHandler() {
		this.handlerState= INIT_0;
	}
	
	
	public boolean isConsoleEnabled() {
		return this.isConsoleEnabled;
	}
	
	public void enableConsole(final @Nullable String authConfig) throws RjException {
		try {
			final RServiNode node= nonNullAssert(this.node);
			this.isConsoleEnabled= node.setConsole(authConfig);
		}
		catch (final Exception e) {
			Utils.logError("An error occurred when configuring the debug console.", e);
			throw new RjException("An error occurred when configuring the debug console. See server log for detail.");
		}
	}
	
	public void disableConsole() throws RjException {
		enableConsole(null);
	}
	
	public @Nullable RMIAddress getAddress() {
		return this.address;
	}
	
	
	final byte getHandlerState() {
		return this.handlerState;
	}
	
	void init1(final String id, final RMIAddress address, final Path dirPath) {
		if (this.handlerState != INIT_0) {
			throw new IllegalStateException();
		}
		this.nodeId= nonNullAssert(id);
		this.address= nonNullAssert(address);
		this.dir= nonNullAssert(dirPath);
		this.handlerState= INIT_1;
	}
	
	void init2(final RServiNode node, final Process process) {
		if (this.handlerState != INIT_1) {
			throw new IllegalStateException();
		}
		this.node= nonNullAssert(node);
		this.process= nonNullAssert(process);
		this.handlerState= INIT_2;
	}
	
	void bindClient(final String name, final String host) throws RemoteException {
		final RServiNode node= nonNullAssert(this.node);
		final StringBuilder sb= new StringBuilder(80);
		if (name != null) {
			sb.append(name);
		}
		sb.append('@');
		sb.append(host);
		final String client= sb.toString();
		this.clientHandler= node.bindClient(client);
		setClientLabel(client);
	}
	
	void unbindClient() throws RemoteException {
		final RServiNode node= nonNullAssert(this.node);
		this.clientHandler= null;
		setClientLabel(null);
		node.unbindClient();
	}
	
	void shutdown() throws RemoteException {
		this.shutdownTime= Instant.now();
		this.clientHandler= null;
		setClientLabel(null);
		final RServiNode node= this.node;
		this.node= null;
		if (node != null) {
			node.shutdown();
		}
	}
	
	RServiBackend getClientHandler() {
		final var clientHandler= this.clientHandler;
		if (clientHandler == null) {
			throw new IllegalStateException();
		}
		return clientHandler;
	}
	
	void setClientLabel(final @Nullable String clientLabel) {
		this.clientLabel= clientLabel;
	}
	
	public @Nullable String getClientLabel() {
		return this.clientLabel;
	}
	
	/**
	 * @since 4.5
	 */
	public @Nullable Instant getShutdownTime() {
		return this.shutdownTime;
	}
	
}
