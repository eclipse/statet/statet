/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rj.servi;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class Messages {
	
	
	public static String BindClient_error_message= "An exception was thrown when trying to bind the client (activate a node).";
	public static String GetRServi_pub_error_message= "Cannot provide RServi instance: Internal error occurred.";
	public static String GetRServi_NoInstance_pub_Single_message= "Cannot provide RServi instance: Single instance is already in use.";
	public static String GetRServi_NoInstance_pub_Pool_message= "Cannot provide RServi instance: No free node available.";
	public static String UnbindClient_error_message= "An exception was thrown when trying to unbind the client (passivate a node).";
	public static String StartNode_error_message= "An exception was thrown when trying to start the node (make a node).";
	public static String StartLocal_pub_error_message= "Cannot start the RServi instance.";
	public static String RmiUnexportNode_error_message= "An exception was thrown when trying to unexport the node (destroy a node).";
	
}
