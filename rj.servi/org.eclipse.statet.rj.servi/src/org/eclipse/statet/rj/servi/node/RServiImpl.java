/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.node;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.rmi.RMIRegistry;
import org.eclipse.statet.jcommons.runtime.bundle.BundleSpec;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.internal.rj.servi.LocalNodeFactory;
import org.eclipse.statet.internal.rj.servi.LocalNodeManager;
import org.eclipse.statet.internal.rj.servi.PoolManager;
import org.eclipse.statet.rj.RjInvalidConfigurationException;
import org.eclipse.statet.rj.server.util.RJContext;
import org.eclipse.statet.rj.server.util.ServerUtils;
import org.eclipse.statet.rj.servi.RServiUtils;
import org.eclipse.statet.rj.servi.pool.RServiPoolManager;


/**
 * Factory for RServi objects
 */
@NonNullByDefault
public class RServiImpl {
	
	
	private static final ImList<BundleSpec> LIB_SPECS= ImCollections.addElement(
			ServerUtils.MIN_CLASSPATH_SPECS,
			0, RServiUtils.RJ_SERVI_SPEC );
	
	
	/**
	 * Creates a node factory establishing RServi nodes on the local system.
	 * 
	 * This method is intended for RServi pools when utilizing the default bundles.
	 * 
	 * @param poolId the id of the pool or application
	 * @param context context to resolve the RJ libraries
	 * @return a node factory
	 * @throws RjInvalidConfigurationException
	 */
	public static RServiNodeFactory createLocalNodeFactory(final String poolId, final RJContext context)
			throws RjInvalidConfigurationException {
		final ImList<BundleSpec> libIds= LIB_SPECS;
		
		try { // check
			context.resolveBundles(libIds);
		}
		catch (final StatusException e) {
			throw new RjInvalidConfigurationException("Failed to resolve the bundle(s) required for RServi nodes.",
					e );
		}
		
		return new LocalNodeFactory(poolId, context, libIds);
	}
	
	/**
	 * Creates an {@link RServiPoolManager RServi pool}.
	 * 
	 * @param poolId the id of the pool
	 * @param registry a handler for the RMI registry to use, or {@code null} to not publish the pool and pool nodes
	 * @return the pool manager
	 */
	public static RServiPoolManager createPool(final String poolId, final @Nullable RMIRegistry registry) {
		return new PoolManager(poolId, registry);
	}
	
	/**
	 * Creates an {@link RServiNodeManager RServi node manager}.
	 * 
	 * @param id the id (like the poolId)
	 * @param registry a handler for the RMI registry to use
	 * @param factory the node factory to use to establish the node
	 * @return the manager for the RServi instance
	 */
	public static RServiNodeManager createNodeManager(final String id, final RMIRegistry registry,
			final RServiNodeFactory factory) {
		if (factory instanceof LocalNodeFactory) {
			return new LocalNodeManager(id, registry, (LocalNodeFactory) factory);
		}
		throw new UnsupportedOperationException(factory.getClass().getName());
	}
	
}
