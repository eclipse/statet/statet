/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rj.servi.node;

import java.util.Collection;
import java.util.Properties;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface PropertiesBean {
	
	
	class ValidationMessage {
		
		
		private final @Nullable String propertyId;
		private final String message;
		
		
		public ValidationMessage(final String message) {
			this.propertyId= null;
			this.message= message;
		}
		
		public ValidationMessage(final String propertyId, final String message) {
			this.propertyId= propertyId;
			this.message= message;
		}
		
		
		public @Nullable String getPropertyId() {
			return this.propertyId;
		}
		
		public String getMessage() {
			return this.message;
		}
		
	}
	
	
	String getBeanId();
	void load(Properties map);
	void save(Properties map);
	
	boolean validate(@Nullable Collection<ValidationMessage> messages);
	
}
