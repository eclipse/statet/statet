<?xml version="1.0" encoding="UTF-8"?>
<!--
 #=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#
-->

<project xmlns="http://maven.apache.org/POM/4.0.0"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>org.eclipse.statet-releng</groupId>
	<artifactId>tycho-1.7-parent</artifactId>
	<version>4.11.0-SNAPSHOT</version>
	<packaging>pom</packaging>
	<name>Eclipse StatET - Maven Parent</name>
	
	<!-- prerequisites>
		<maven>[3.9.6,4.0.0)</maven>
	</prerequisites -->
	
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		
		<tycho.version>1.7.0</tycho.version>
		<tycho-extras.version>${tycho.version}</tycho-extras.version>
		<tycho.scmUrl>${statet.scmUrl}</tycho.scmUrl>
		<maven.compiler.showWarnings>true</maven.compiler.showWarnings>
		<cbi-plugins.version>1.5.0</cbi-plugins.version>
		
		<java.default.version>11</java.default.version>
		<maven.compiler.release>${java.default.version}</maven.compiler.release>
		
		<!-- if working tree is dirty (error|warning|ignore) -->
		<dirtyWorkingTree>error</dirtyWorkingTree>
		
		<maven.build.timestamp.format>yyyyMMddHHmm</maven.build.timestamp.format>
		
		<statet.version>4.11.0</statet.version>
		<statet.target.id>E202412</statet.target.id>
		<statet.build.timestamp>${maven.build.timestamp}</statet.build.timestamp>
		<statet.build.id>${statet.build.timestamp}</statet.build.id>
		<forceContextQualifier>${statet.build.id}</forceContextQualifier>
		<statet.scmUrl>scm:git:https://gitlab.eclipse.org/eclipse/statet/statet.git</statet.scmUrl>
		
		<statet.test.excludes></statet.test.excludes>
		<trimStackTrace>false</trimStackTrace>
		
	</properties>
	
	<pluginRepositories>
		<pluginRepository>
			<id>cbi-releases</id>
			<url>https://repo.eclipse.org/content/repositories/cbi-releases/</url>
		</pluginRepository>
	</pluginRepositories>
	
	<repositories>
		<repository>
			<id>eclipse.license</id>
			<url>https://download.eclipse.org/cbi/updates/license/</url>
			<layout>p2</layout>
		</repository>
	</repositories>
	
	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-resources-plugin</artifactId>
					<version>3.3.1</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-dependency-plugin</artifactId>
					<version>3.8.1</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.13.0</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>3.4.2</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-assembly-plugin</artifactId>
					<version>3.7.1</version>
				</plugin>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>properties-maven-plugin</artifactId>
					<version>1.2.1</version>
					<executions>
						<execution>
							<phase>generate-resources</phase>
							<goals>
								<goal>write-project-properties</goal>
							</goals>
							<configuration>
								<outputFile>${project.build.directory}/mvn-build.properties</outputFile>
							</configuration>
						</execution>
					</executions>
				</plugin>
				
				<plugin>
					<groupId>org.eclipse.cbi.maven.plugins</groupId>
					<artifactId>eclipse-jarsigner-plugin</artifactId>
					<version>${cbi-plugins.version}</version>
				</plugin>
				
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-maven-plugin</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>target-platform-configuration</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-compiler-plugin</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-packaging-plugin</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho.extras</groupId>
					<artifactId>tycho-custom-bundle-plugin</artifactId>
					<version>${tycho-extras.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-source-plugin</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho.extras</groupId>
					<artifactId>tycho-source-feature-plugin</artifactId>
					<version>${tycho-extras.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-p2-plugin</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-p2-publisher-plugin</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-p2-repository-plugin</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-p2-director-plugin</artifactId>
					<version>${tycho.version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho.extras</groupId>
					<artifactId>tycho-p2-extras-plugin</artifactId>
					<version>${tycho-extras.version}</version>
				</plugin>
				
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-surefire-plugin</artifactId>
					<version>${tycho.version}</version>
					<configuration>
						<!-- providerHint></providerHint -->
						<excludes>
							<exclude>**/*$*</exclude>
							<exclude>${statet.test.excludes}</exclude>
						</excludes>
						<!-- https://bugs.eclipse.org/bugs/show_bug.cgi?id=567913 -->
						<dependencies>
							<dependency>
								<artifactId>org.junit</artifactId>
								<type>eclipse-plugin</type>
							</dependency>
						</dependencies>
					</configuration>
				</plugin>
				
			</plugins>
		</pluginManagement>
		
		<plugins>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-maven-plugin</artifactId>
				<extensions>true</extensions>
			</plugin>
			
			<!-- configure the p2 repo/target -->
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>target-platform-configuration</artifactId>
				<configuration>
					<resolver>p2</resolver>
					<environments>
						<environment>
							<os>linux</os>
							<ws>gtk</ws>
							<arch>x86_64</arch>
						</environment>
						<environment>
							<os>linux</os>
							<ws>gtk</ws>
							<arch>aarch64</arch>
						</environment>
						<environment>
							<os>win32</os>
							<ws>win32</ws>
							<arch>x86_64</arch>
						</environment>
						<environment>
							<os>win32</os>
							<ws>win32</ws>
							<arch>aarch64</arch>
						</environment>
						<environment>
							<os>macosx</os>
							<ws>cocoa</ws>
							<arch>x86_64</arch>
						</environment>
						<environment>
							<os>macosx</os>
							<ws>cocoa</ws>
							<arch>aarch64</arch>
						</environment>
					</environments>
					<executionEnvironmentDefault>JavaSE-${java.default.version}</executionEnvironmentDefault>
				</configuration>
			</plugin>
			
			<!-- configure build qualifier generation -->
			<!-- enable source reference generation -->
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-packaging-plugin</artifactId>
				<dependencies>
					<dependency>
						<groupId>org.eclipse.tycho.extras</groupId>
						<artifactId>tycho-sourceref-jgit</artifactId>
						<version>${tycho-extras.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<sourceReferences>
						<generate>true</generate>
					</sourceReferences>
				</configuration>
			</plugin>
			
			<!-- enable source plugin and feature generation -->
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-source-plugin</artifactId>
				<executions>
					<execution>
						<id>plugin-source</id>
						<goals>
							<goal>plugin-source</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho.extras</groupId>
				<artifactId>tycho-source-feature-plugin</artifactId>
				<executions>
					<execution>
						<id>source-feature</id>
						<goals>
							<goal>source-feature</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<labelSuffix xml:space="preserve">  &#x2022;&#x00A0;Sources</labelSuffix>
				</configuration>
			</plugin>
			
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-p2-plugin</artifactId>
				<executions>
					<execution>
						<id>p2-metadata</id>
						<phase>package</phase>
						<goals>
							<goal>p2-metadata</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>properties-maven-plugin</artifactId>
				<inherited>false</inherited>
			</plugin>
			
		</plugins>
	</build>
	
	<profiles>
		<profile>
			<id>eclipse-sign</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.eclipse.cbi.maven.plugins</groupId>
						<artifactId>eclipse-jarsigner-plugin</artifactId>
						<executions>
							<execution>
								<id>sign</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.eclipse.tycho</groupId>
						<artifactId>tycho-p2-plugin</artifactId>
						<executions>
							<execution>
								<id>p2-metadata</id>
								<phase>verify</phase>
								<goals>
									<goal>p2-metadata</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<defaultP2Metadata>false</defaultP2Metadata>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		
	</profiles>
</project>
