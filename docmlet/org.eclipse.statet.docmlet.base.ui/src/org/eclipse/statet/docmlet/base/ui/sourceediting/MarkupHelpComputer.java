/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.sourceediting;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.IEditorStatusLine;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.docmlet.base.ui.markuphelp.MarkupHelpContextProvider;
import org.eclipse.statet.docmlet.base.ui.markuphelp.MarkupHelpView;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistComputer;


@NonNullByDefault
public class MarkupHelpComputer implements QuickAssistComputer {
	
	
	private static class ShowHelpProposal implements AssistProposal {
		
		
		private final SourceEditor editor;
		
		
		public ShowHelpProposal(final SourceEditor editor) {
			this.editor= editor;
		}
		
		@Override
		public int getRelevance() {
			return 50;
		}
		
		@Override
		public Image getImage() {
			return DocmlBaseUIResources.INSTANCE.getImage(DocmlBaseUIResources.VIEW_MARKUP_HELP_IMAGE_ID);
		}
		
		@Override
		public String getDisplayString() {
			return "Show Markup Cheat Sheet";
		}
		
		@Override
		public String getSortingString() {
			return getDisplayString();
		}
		
		@Override
		public @Nullable String getAdditionalProposalInfo() {
			return null;
		}
		
		
		@Override
		public boolean validate(final IDocument document, final int offset, final @Nullable DocumentEvent event) {
			return true;
		}
		
		@Override
		public void apply(final @Nullable IDocument document) {
			final MarkupHelpContextProvider contextProvider= this.editor.getAdapter(MarkupHelpContextProvider.class);
			final String contentId= contextProvider.getHelpContentId();
			if (contentId != null) {
				final IWorkbenchPage page= UIAccess.getActiveWorkbenchPage(true);
				if (page != null) {
					try {
						final MarkupHelpView view= (MarkupHelpView) page.showView(MarkupHelpView.VIEW_ID);
						view.show(contentId);
					}
					catch (final PartInitException e) {
						StatusManager.getManager().handle(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID, 0,
								"An error occurred while opening the Markup Help View to show a cheat sheet.",
								e ));
					}
				}
			}
			else {
				final IEditorStatusLine statusLine= this.editor.getAdapter(IEditorStatusLine.class);
				if (statusLine != null) {
					statusLine.setMessage(true, "No cheat sheet available for the current markup language.", null);
				}
			}
		}
		
		@Override
		public void apply(final ITextViewer viewer, final char trigger, final int stateMask, final int offset) {
			apply(null);
		}
		
		@Override
		public @Nullable Point getSelection(final IDocument document) {
			return null;
		}
		
		@Override
		public @Nullable IContextInformation getContextInformation() {
			return null;
		}
		
	}
	
	
	public MarkupHelpComputer() {
	}
	
	
	@Override
	public void computeAssistProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		final MarkupHelpContextProvider contextProvider= context.getEditor().getAdapter(MarkupHelpContextProvider.class);
		if (contextProvider != null) {
			proposals.add(new ShowHelpProposal(context.getEditor()));
		}
	}
	
}
