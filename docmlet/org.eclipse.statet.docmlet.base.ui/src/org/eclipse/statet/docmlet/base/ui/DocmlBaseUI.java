/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui;



public class DocmlBaseUI {
	
	
	public static final String BUNDLE_ID= "org.eclipse.statet.docmlet.base.ui"; //$NON-NLS-1$
	
	
	public static final String DOC_EDITOR_CONTEXT_ID= "org.eclipse.statet.docmlet.contexts.DocEditor"; //$NON-NLS-1$
	
	
	public static final String CONFIGURE_MARKUP_COMMAND_ID= "org.eclipse.statet.docmlet.commands.ConfigureMarkup"; //$NON-NLS-1$
	
}
