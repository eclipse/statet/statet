/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.menus.IWorkbenchContribution;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.actions.RunConfigsDropdownContribution;

import org.eclipse.statet.internal.docmlet.base.ui.processing.DocActionUtil;


@NonNullByDefault
public class RunDocConfigsDropdownContribution extends RunConfigsDropdownContribution<IFile>
		implements IWorkbenchContribution, IExecutableExtension {
	
	
	/** plugin.xml */
	public RunDocConfigsDropdownContribution() {
		super(new DocActionUtil(DocActionUtil.ACTIVE_EDITOR_MODE));
	}
	
	
	@Override
	protected DocActionUtil getUtil() {
		return (DocActionUtil) super.getUtil();
	}
	
	@Override
	protected void configure(final Map<String, @Nullable String> parameters) {
		super.configure(parameters);
		{	final String s= parameters.get(DocActionUtil.CONTENT_TYPE_PAR_NAME);
			if (s != null) {
				getUtil().setContentType(
						nonNullAssert(Platform.getContentTypeManager().getContentType(s)) );
			}
		}
	}
	
}
