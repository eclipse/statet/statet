/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;
import org.eclipse.debug.ui.DebugUITools;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class DocProcessingToolLaunchDelegate extends LaunchConfigurationDelegate {
	
	
	protected DocProcessingToolLaunchDelegate() {
	}
	
	
	@Override
	protected @NonNull IProject @Nullable [] getBuildOrder(final ILaunchConfiguration configuration, final String mode) throws CoreException {
		final IResource resource= DebugUITools.getSelectedResource();
		final IProject project;
		if (resource != null && (project= resource.getProject()) != null) {
			return computeReferencedBuildOrder(new @NonNull IProject[] { project });
		}
		return null;
	}
	
	
}
