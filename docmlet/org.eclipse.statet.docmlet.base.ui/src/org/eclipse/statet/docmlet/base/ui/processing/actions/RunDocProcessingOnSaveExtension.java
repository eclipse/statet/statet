/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.actions;

import java.util.Collections;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.ui.services.IServiceScopes;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingManager;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;


public class RunDocProcessingOnSaveExtension {
	
	
	private final SourceEditor1 editor;
	
	private boolean isRunEnabled;
	
	
	public RunDocProcessingOnSaveExtension(final SourceEditor1 editor) {
		this.editor= editor;
	}
	
	
	public boolean isAutoRunEnabled() {
		return this.isRunEnabled;
	}
	
	public void setAutoRunEnabled(final boolean enabled) {
		this.isRunEnabled= enabled;
		
		final Map<String, IWorkbenchWindow> filter= Collections.singletonMap(IServiceScopes.WINDOW_SCOPE, this.editor.getSite()
				.getPage().getWorkbenchWindow() );
		WorkbenchUIUtils.refreshCommandElements(DocProcessingUI.TOGGLE_RUN_ON_SAVE_COMMAND_ID, null, filter);
	}
	
	public void onEditorSaved() {
		if (isAutoRunEnabled()) {
			runDocProcessing();
		}
	}
	
	private void runDocProcessing() {
		final IFile file= ResourceUtil.getFile(this.editor.getEditorInput());
		final DocProcessingManager manager= DocProcessingUI.getDocProcessingManager(
				this.editor.getContentType(), true);
		if (file == null || manager == null) {
			return;
		}
		final ILaunchConfiguration config= manager.getActiveConfig();
		if (config != null) {
			manager.launch(config, file, DocProcessingUI.CommonFlags.PROCESS_AND_PREVIEW);
		}
	}
	
}
