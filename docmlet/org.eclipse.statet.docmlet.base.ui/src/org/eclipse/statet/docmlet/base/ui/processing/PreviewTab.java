/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import java.util.Map;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.variables.IStringVariable;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.core.observable.WritableEqualityValue;
import org.eclipse.statet.ecommons.resources.core.variables.ObservableResourcePathVariable;
import org.eclipse.statet.ecommons.resources.core.variables.ResourceVariableResolver;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.variables.core.VariableUtils;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig.Format;
import org.eclipse.statet.docmlet.base.ui.processing.operations.OpenUsingDocViewerOperationSettings;
import org.eclipse.statet.docmlet.base.ui.processing.operations.OpenUsingEclipseOperation;
import org.eclipse.statet.docmlet.base.ui.processing.operations.OpenUsingEclipseOperationSettings;
import org.eclipse.statet.docmlet.base.ui.processing.operations.RunExternalProgramOperationSettings;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public class PreviewTab extends DocProcessingConfigStepTab
		implements DocProcessingConfigStepTab.Listener {
	
	
	private final DocProcessingConfigIOStepTab produceTab;
	
	private final IObservableValue<@Nullable Format> inputFormatValue;
	
	private final IObservableValue<@Nullable IFile> resolvedInputFileValue;
	
	private Label inputControl;
	
	
	public PreviewTab(final DocProcessingConfigMainTab mainTab,
			final DocProcessingConfigIOStepTab produceTab) {
		super(mainTab, DocProcessingConfig.BASE_PREVIEW_ATTR_QUALIFIER);
		
		this.produceTab= produceTab;
		this.produceTab.addListener(this);
		
		final Realm realm= getRealm();
		this.inputFormatValue= new WritableValue<>(realm, null, Format.class);
		this.inputFormatValue.addValueChangeListener(this);
		
		this.resolvedInputFileValue= new WritableEqualityValue<>(realm, null, IFile.class);
		
		setAvailableOperations(ImCollections.newList(
				new OpenUsingEclipseOperationSettings(),
				new OpenUsingDocViewerOperationSettings(),
				new RunExternalProgramOperationSettings() ));
		
		produceTab.getOutputFileValue().addValueChangeListener(this);
		
		changed(this.produceTab);
	}
	
	
	@Override
	public Image getImage() {
		return DocmlBaseUIResources.INSTANCE.getImage(DocmlBaseUIResources.TOOL_PREVIEW_IMAGE_ID);
	}
	
	@Override
	public String getName() {
		return createName(Messages.PreviewTab_name);
	}
	
	@Override
	public String getLabel() {
		return Messages.Preview_label;
	}
	
	
	@Override
	protected void initVariables(final Map<String, IStringVariable> variables) {
		variables.putAll(getMainTab().getSourceFileVariables());
		
		VariableUtils.add(variables, new ObservableResourcePathVariable<>( // required for updates
				DocProcessingConfig.IN_FILE_PATH_VAR,
				this.resolvedInputFileValue ));
		VariableUtils.add(variables, INPUT_RESOURCE_VAR_DEFS,
				new ResourceVariableResolver(new ResourceVariableResolver.Context() {
					@Override
					public @Nullable IResource getResource() {
						return PreviewTab.this.resolvedInputFileValue.getValue();
					}
				}, ResourceVariableResolver.EXISTS_NEVER ));
	}
	
	protected void setInput(final @Nullable Format format, final @Nullable IFile file) {
		if (format != null && format.equals(this.inputFormatValue.getValue())) {
			this.resolvedInputFileValue.setValue(file);
		}
		else if (file == null || !file.equals(this.resolvedInputFileValue.getValue())) {
			this.resolvedInputFileValue.setValue(null);
			this.inputFormatValue.setValue(format);
			this.resolvedInputFileValue.setValue(file);
		}
		else {
			this.inputFormatValue.setValue(format);
		}
	}
	
	public @Nullable Format getInputFormat() {
		return this.inputFormatValue.getValue();
	}
	
	public @Nullable IFile getInputFile() {
		return this.resolvedInputFileValue.getValue();
	}
	
	public IObservableValue<@Nullable IFile> getInputFileValue() {
		return this.resolvedInputFileValue;
	}
	
	@Override
	public String getInfo() {
		final StringBuilder sb= getStringBuilder();
		
		final Format inputFormat= getInputFormat();
		sb.append((inputFormat != null) ? inputFormat.getInfoLabel() : "?"); //$NON-NLS-1$
		
		sb.append('\n');
		final DocProcessingOperationSettings operation= getOperation();
		sb.append((operation != null) ? operation.getInfo() : " "); //$NON-NLS-1$
		
		return sb.toString();
	}
	
	@Override
	public void changed(final DocProcessingConfigStepTab source) {
		if (source == this.produceTab) {
			updateInput();
		}
	}
	
	@Override
	public void handleValueChange(final ValueChangeEvent<?> event) {
		if (event.getObservable() == this.produceTab.getOutputFileValue()) {
			updateInput();
			return;
		}
		if (event.getObservable() == this.inputFormatValue) {
			updateInputText();
		}
		super.handleValueChange(event);
	}
	
	private void updateInput() {
		final Format format= this.produceTab.getOutputFormat();
		setInput((format != null) ?
						DocProcessingConfig.createOutputFormat(format) :
						null,
				this.produceTab.getOutputFile() );
	}
	
	
	@Override
	protected void addControls(final Composite parent) {
		{	final Composite composite= createFormatGroup(parent);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
		{	final Composite composite= createOperationGroup(parent);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		}
	}
	
	protected Composite createFormatGroup(final Composite parent) {
		final Group group= new Group(parent, SWT.NONE);
		group.setLayout(LayoutUtils.newGroupGrid(4));
		group.setText("IO"); //$NON-NLS-1$
		
		{	final Label label= new Label(group, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
			label.setText(Messages.StepTab_In_label);
		}
		{	final Label label= new Label(group, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
			
			this.inputControl= label;
			updateInputText();
		}
		
		LayoutUtils.addSmallFiller(group, false);
		
		return group;
	}
	
	@Override
	protected String getOperationsLabel() {
		return Messages.PreviewTab_Operations_label;
	}
	
	protected void updateInputText() {
		final Format inputFormat= getInputFormat();
		if (!UIAccess.isOkToUse(this.inputControl) || inputFormat == null) {
			return;
		}
		this.inputControl.setText(inputFormat.getInfoLabel());
		this.inputControl.getParent().layout(true);
	}
	
	@Override
	protected void addBindings(final DataBindingContext dbc) {
		super.addBindings(dbc);
	}
	
	
	@Override
	protected String getDefaultOperationId() {
		return OpenUsingEclipseOperation.ID;
	}
	
	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
		super.setDefaults(configuration);
	}
	
	@Override
	protected void doInitialize(final ILaunchConfiguration configuration) {
		super.doInitialize(configuration);
	}
	
	@Override
	protected void doSave(final ILaunchConfigurationWorkingCopy configuration) {
		super.doSave(configuration);
	}
	
}
