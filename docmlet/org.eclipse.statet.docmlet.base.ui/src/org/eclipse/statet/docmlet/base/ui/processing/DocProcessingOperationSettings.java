/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import java.util.Map;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.core.DataBindingSubContext;


@NonNullByDefault
public abstract class DocProcessingOperationSettings {
	
	
	private DocProcessingConfigStepTab tab;
	
	private boolean isSelected;
	
	private @Nullable DataBindingSubContext bindings;
	
	
	public DocProcessingOperationSettings() {
	}
	
	
	public abstract String getId();
	
	public abstract String getLabel();
	
	public String getInfo() {
		return getLabel();
	}
	
	protected String limitInfo(String info) {
		final int idx= info.indexOf('\n');
		if (idx >= 0) {
			info= info.substring(0, idx);
		}
		return (info.length() < 40) ? info : (info.substring(0, 35) + "\u2026"); //$NON-NLS-1$
	}
	
	
	protected void init(final DocProcessingConfigStepTab tab) {
		this.tab= tab;
	}
	
	protected void dispose() {
	}
	
	
	public DocProcessingConfigStepTab getTab() {
		return this.tab;
	}
	
	protected Realm getRealm() {
		return this.tab.getRealm();
	}
	
	protected @Nullable ILaunchConfigurationDialog getLaunchConfigurationDialog() {
		return this.tab.getLaunchConfigurationDialog();
	}
	
	
	Composite createDetailControl(final Composite parent) {
		final Composite composite= createControl(parent);
		
		if (composite != null) {
			initBindings();
		}
		
		return composite;
	}
	
	void initBindings() {
		final DataBindingContext dbc= this.tab.getDataBindingContext();
		final DataBindingSubContext bindings= new DataBindingSubContext(dbc,
				(final ChangeEvent event) -> {
					if (DocProcessingOperationSettings.this.isSelected) {
						DocProcessingOperationSettings.this.tab.scheduleNotifyListeners();
					}
				} );
		bindings.run(() -> addBindings(dbc));
		this.bindings= bindings;
	}
	
	protected Composite createControl(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		return composite;
	}
	
	protected void addBindings(final DataBindingContext dbc) {
	}
	
	protected void setSelected(final boolean selected) {
		if (selected == this.isSelected) {
			return;
		}
		
		this.isSelected= selected;
		
		final DataBindingSubContext bindings= this.bindings;
		if (bindings != null) {
			bindings.setEnabled(selected);
		}
	}
	
	public boolean isSelected() {
		return this.isSelected;
	}
	
	
	protected static void set(final Map<String, String> config, final String attrName,
			final @Nullable String value) {
		if (value != null) {
			config.put(attrName, value);
		}
		else {
			config.remove(attrName);
		}
	}
	
	protected void load(final Map<String, String> config) {
	}
	
	protected void save(final Map<String, String> config) {
	}
	
}
