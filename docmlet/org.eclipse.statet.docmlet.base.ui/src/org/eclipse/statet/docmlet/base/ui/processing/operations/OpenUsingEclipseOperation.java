/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.isNull;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nullable;

import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.ide.IDE;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingOperation;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolConfig.StepConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolProcess;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public class OpenUsingEclipseOperation extends DocProcessingOperation {
	
	
	public static final String ID= "org.eclipse.statet.docmlet.base.docProcessing.OpenUsingEclipseOperation"; //$NON-NLS-1$
	
	
	private IFile file;
	
	private int failSeverity= IStatus.ERROR;
	
	
	public OpenUsingEclipseOperation() {
	}
	
	public OpenUsingEclipseOperation(final IFile file) {
		this.file= file;
	}
	
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_OpenUsingEclipse_label;
	}
	
	
	public void setFile(final IFile file) {
		this.file= file;
	}
	
	public IFile getFile() {
		return this.file;
	}
	
	public void setFailSeverity(final int severity) {
		this.failSeverity= severity;
	}
	
	@Override
	public void init(final StepConfig stepConfig, final Map<String, String> settings,
			final SubMonitor m) throws CoreException {
		super.init(stepConfig, settings, m);
		
		IFile file= nullable(this.file);
		if (file == null) {
			file= stepConfig.getInputFile();
			this.file= file;
		}
	}
	
	
	@Override
	public int getTicks() {
		return 15;
	}
	
	@Override
	public IStatus run(final DocProcessingToolProcess toolProcess,
			final SubMonitor m) throws CoreException {
		final IFile file= this.file;
		if (isNull(file)) {
			throw new IllegalStateException("not initialized");
		}
		
		m.beginTask(NLS.bind(Messages.ProcessingOperation_OpenUsingEclipse_task, file.getName()),
				10 );
		
		class UIRunnable implements Runnable {
			
			private @Nullable Exception error;
			
			@Override
			public void run() {
				try {
					IWorkbenchPage page= getStepConfig().getToolConfig().getWorkbenchPage();
					if (page == null || UIAccess.isOkToUse(page.getWorkbenchWindow().getShell())) {
						page= UIAccess.getActiveWorkbenchPage(true);
					}
					IDE.openEditor(page, file);
				}
				catch (final Exception e) {
					this.error= e;
				}
			}
		}
		final UIRunnable runnable= new UIRunnable();
		UIAccess.getDisplay().syncExec(runnable);
		
		if (runnable.error != null) {
			return new Status(this.failSeverity, DocmlBaseUI.BUNDLE_ID, 0,
					NLS.bind(Messages.ProcessingOperation_OpenUsingEclipse_error_message,
							file.getName(), getStepConfig().getLabel() ),
					runnable.error );
		}
		return Status.OK_STATUS;
	}
	
}
