/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolConfig.StepConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolProcess;
import org.eclipse.statet.docmlet.base.ui.viewer.DocViewerConfig;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;
import org.eclipse.statet.internal.docmlet.base.ui.viewer.DocViewerLaunchDelegate;


@NonNullByDefault
public class OpenUsingDocViewerOperation extends AbstractLaunchConfigOperation {
	
	
	public static final String ID= "org.eclipse.statet.docmlet.base.docProcessing.OpenUsingDocViewerOperation"; //$NON-NLS-1$
	
	public static final String LAUNCH_CONFIG_NAME_ATTR_NAME= ID + '/' + LAUNCH_CONFIG_NAME_ATTR_KEY;
	
	
	public OpenUsingDocViewerOperation() {
		super(DocViewerConfig.TYPE_ID);
	}
	
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_OpenUsingDocViewer_label;
	}
	 
	
	@Override
	protected void launch(final ILaunchConfigurationDelegate delegate,
			final ILaunchConfiguration config, final String launchMode,
			final DocProcessingToolProcess toolProcess,
			final SubMonitor m) throws CoreException {
		final StepConfig stepConfig= getStepConfig();
		
		((DocViewerLaunchDelegate) delegate).launch(stepConfig.getInputFileUtil(),
				stepConfig.getVariableResolver().getExtraVariables(), config,
				launchMode, toolProcess.getLaunch(), m );
	}
	
}
