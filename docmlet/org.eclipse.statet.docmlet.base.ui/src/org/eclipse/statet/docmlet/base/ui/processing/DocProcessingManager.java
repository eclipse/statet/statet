/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import static org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI.PREVIEW_OUTPUT_STEP;
import static org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI.PRODUCE_OUTPUT_STEP;
import static org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI.WEAVE_STEP;

import java.util.IdentityHashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.core.ILaunchConfigurationListener;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.collections.IdentityCollection;
import org.eclipse.statet.jcommons.collections.IdentitySet;
import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.internal.docmlet.base.ui.processing.DocProcessingRegistry;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


/**
 * Manages configuration of a document processing type.
 * 
 * element= doc file
 */
@NonNullByDefault
public class DocProcessingManager extends LaunchConfigManager<IFile>
		implements ILaunchConfigurationListener, Disposable {
	
	
	protected static final byte WEAVE_BIT=                  0b0_00000010;
	protected static final byte PRODUCE_OUTPUT_BIT=         0b0_00001000;
	protected static final byte OPEN_OUTPUT_BIT=            0b0_00100000;
	
	
	public DocProcessingManager() {
	}
	
	public void init(final DocProcessingRegistry.ManagerConfig config) {
		super.init(config.contentTypeId, config.configTypeId);
	}
	
	
	@Override
	protected byte getBits(final IdentityCollection<String> flags) {
		byte bits;
		if (flags.contains(DocProcessingUI.PROCESSING_STEPS_FLAG)) {
			bits= WEAVE_BIT | PRODUCE_OUTPUT_BIT;
		}
		else {
			bits= 0;
			if (flags.contains(WEAVE_STEP)) {
				bits|= WEAVE_BIT;
			}
			if (flags.contains(PRODUCE_OUTPUT_STEP)) {
				bits|= PRODUCE_OUTPUT_BIT;
			}
			if (flags.contains(PREVIEW_OUTPUT_STEP)) {
				bits|= OPEN_OUTPUT_BIT;
			}
		}
		return bits;
	}
	
	
	@Override
	protected boolean getBuildBeforeLaunch(final byte bits) {
		return (bits == 0 || (bits & WEAVE_BIT) != 0);
	}
	
	@Override
	protected Map<String, Object> createRunAttributes(final IFile element,
			final IdentitySet<String> flags) {
		final Map<String, Object> map= new IdentityHashMap<>(4);
		map.put(DocProcessingUI.CONTENT_TYPE_ID_ATTR_NAME, getContentTypeId());
		if (flags != null) {
			map.put(DocProcessingUI.RUN_STEPS_ATTR_NAME, flags);
		}
		if (element != null) {
			map.put(DocProcessingUI.SOURCE_PATH_ATTR_NAME, element.getFullPath().toPortableString());
		}
		return map;
	}
	
	
	@Override
	protected @Nullable Image getActionImage(final byte bits) {
		return switch (bits) {
		case 0, WEAVE_BIT | PRODUCE_OUTPUT_BIT | OPEN_OUTPUT_BIT ->
				DocmlBaseUIResources.INSTANCE.getImage(DocmlBaseUIResources.TOOL_PROCESSANDPREVIEW_IMAGE_ID);
		case WEAVE_BIT | PRODUCE_OUTPUT_BIT ->
				DocmlBaseUIResources.INSTANCE.getImage(DocmlBaseUIResources.TOOL_PROCESS_IMAGE_ID);
		case PRODUCE_OUTPUT_BIT ->
				DocmlBaseUIResources.INSTANCE.getImage(DocmlBaseUIResources.TOOL_PREVIEW_IMAGE_ID);
		default ->
				null;
		};
	}
	
	@Override
	protected String getActionLabel(final byte bits) {
		return switch (bits) {
		case 0, WEAVE_BIT | PRODUCE_OUTPUT_BIT | OPEN_OUTPUT_BIT ->
				Messages.ProcessingAction_ProcessAndPreview_label;
		case WEAVE_BIT | PRODUCE_OUTPUT_BIT ->
				Messages.ProcessingAction_ProcessDoc_label;
		case WEAVE_BIT ->
				Messages.ProcessingAction_Weave_label;
		case PRODUCE_OUTPUT_BIT ->
				Messages.ProcessingAction_ProduceOutput_label;
		case OPEN_OUTPUT_BIT ->
				Messages.ProcessingAction_PreviewOutput_label;
		default ->
				throw new IllegalArgumentException();
		};
	}
	
}
