/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.markuphelp;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public abstract class MarkupHelpContent {
	
	
	private final String id;
	
	private final String title;
	
	
	public MarkupHelpContent(final String id, final String title) {
		this.id= id;
		this.title= title;
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public abstract String getContent() throws IOException;
	
	
	@Override
	public String toString() {
		return this.title + " (" + this.id + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}
