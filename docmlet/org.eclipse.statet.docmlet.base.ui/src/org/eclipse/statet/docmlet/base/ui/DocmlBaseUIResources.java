/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.base.ui.DocmlBaseUIPlugin;


@NonNullByDefault
public class DocmlBaseUIResources {
	
	
	private static final String NS= "org.eclipse.statet.docmlet.base"; //$NON-NLS-1$
	
	
	public static final String OBJ_PREAMBLE_IMAGE_ID=           NS + "/images/obj/Preamble"; //$NON-NLS-1$
	
	public static final String OBJ_HEADING1_IMAGE_ID=           NS + "/images/obj/Sectioning-H1"; //$NON-NLS-1$
	public static final String OBJ_HEADING2_IMAGE_ID=           NS + "/images/obj/Sectioning-H2"; //$NON-NLS-1$
	public static final String OBJ_HEADING3_IMAGE_ID=           NS + "/images/obj/Sectioning-H3"; //$NON-NLS-1$
	public static final String OBJ_HEADING4_IMAGE_ID=           NS + "/images/obj/Sectioning-H4"; //$NON-NLS-1$
	public static final String OBJ_HEADING5_IMAGE_ID=           NS + "/images/obj/Sectioning-H5"; //$NON-NLS-1$
	public static final String OBJ_HEADING6_IMAGE_ID=           NS + "/images/obj/Sectioning-H6"; //$NON-NLS-1$
	
	public static final String OBJ_LABEL_DEF_IMAGE_ID=          NS + "/images/obj/Label-Def"; //$NON-NLS-1$
	public static final String OBJ_LABEL_REF_IMAGE_ID=          NS + "/images/obj/Label-Ref"; //$NON-NLS-1$
	public static final String OBJ_LABEL_TEXT_IMAGE_ID=         NS + "/images/obj/Label-Text"; //$NON-NLS-1$
	
	public static final String VIEW_MARKUP_HELP_IMAGE_ID=       NS + "/images/view/MarkupHelp"; //$NON-NLS-1$
	
	public static final String TOOL_PROCESS_IMAGE_ID=           NS + "/images/tool/Process"; //$NON-NLS-1$
	public static final String TOOL_PROCESSANDPREVIEW_IMAGE_ID= NS + "/images/tool/ProcessAndPreview"; //$NON-NLS-1$
	public static final String TOOL_PREVIEW_IMAGE_ID=           NS + "/images/tool/Preview"; //$NON-NLS-1$
	
	
	public static final DocmlBaseUIResources INSTANCE= new DocmlBaseUIResources();
	
	
	private final ImageRegistry registry;
	
	
	private DocmlBaseUIResources() {
		this.registry= DocmlBaseUIPlugin.getInstance().getImageRegistry();
	}
	
	
	public @Nullable ImageDescriptor getImageDescriptor(final String id) {
		return this.registry.getDescriptor(id);
	}
	
	public @Nullable Image getImage(final String id) {
		return this.registry.get(id);
	}
	
}
