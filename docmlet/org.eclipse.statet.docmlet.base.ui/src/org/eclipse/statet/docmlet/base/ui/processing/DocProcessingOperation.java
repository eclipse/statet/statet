/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing;

import java.util.Collections;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolConfig.StepConfig;


@NonNullByDefault
public abstract class DocProcessingOperation {
	
	
	public static final Map<String, String> NO_SETTINGS= Collections.emptyMap();
	
	
	private StepConfig stepConfig;
	
	
	public DocProcessingOperation() {
	}
	
	
	public abstract String getId();
	
	public abstract String getLabel();
	
	
	protected StepConfig getStepConfig() {
		return this.stepConfig;
	}
	
	
	public void init(final StepConfig stepConfig, final Map<String, String> settings,
			final SubMonitor m) throws CoreException {
		this.stepConfig= stepConfig;
	}
	
	
	public @Nullable String getContextId() {
		return null;
	}
	
	public @Nullable DocProcessingToolOperationContext createContext() {
		return null;
	}
	
	public int getTicks() {
		return 50;
	}
	
	
	public abstract IStatus run(final DocProcessingToolProcess toolProcess,
			final SubMonitor m) throws CoreException, OperationCanceledException;
	
	
}
