/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingOperation;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolConfig.StepConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolProcess;
import org.eclipse.statet.docmlet.base.ui.viewer.DocViewerUI;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public class CloseInDocViewerOperation extends DocProcessingOperation {
	
	
	public static final String ID= "org.eclipse.statet.docmlet.base.docProcessing.CloseInDocViewerOperation"; //$NON-NLS-1$
	
	
	public CloseInDocViewerOperation() {
	}
	
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_CloseInDocViewer_label;
	}
	
	@Override
	public IStatus run(final DocProcessingToolProcess toolProcess, final SubMonitor m)
			throws CoreException, OperationCanceledException {
		final StepConfig previewConfig= getStepConfig().getToolConfig().getStep(DocProcessingConfig.BASE_PREVIEW_ATTR_QUALIFIER);
		
		DocViewerUI.runPreProduceOutputTask(previewConfig.getInputFileUtil(),
				previewConfig.getVariableResolver().getExtraVariables(), m );
		
		return Status.OK_STATUS;
	}
	
}
