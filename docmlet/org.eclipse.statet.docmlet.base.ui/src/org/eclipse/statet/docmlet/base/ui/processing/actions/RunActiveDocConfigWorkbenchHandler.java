/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.actions;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nullable;

import java.util.Collections;
import java.util.Map;

import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.commands.IElementUpdater;

import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.collections.ImIdentitySet;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager.Listener;
import org.eclipse.statet.ecommons.debug.ui.config.actions.ActionUtil;
import org.eclipse.statet.ecommons.debug.ui.config.actions.RunActiveConfigScopeHandler;
import org.eclipse.statet.ecommons.ui.actions.WorkbenchScopingHandler;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingManager;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI.CommonFlags;
import org.eclipse.statet.docmlet.base.ui.sourceediting.DocEditor;
import org.eclipse.statet.internal.docmlet.base.ui.processing.DocActionUtil;


/**
 * Handlers for document output creation toolchain running with the active configuration.
 */
@NonNullByDefault
public class RunActiveDocConfigWorkbenchHandler extends WorkbenchScopingHandler
		implements IExecutableExtension, IElementUpdater {
	
	
	private static class ScopeHandler extends RunActiveConfigScopeHandler<IFile>
			implements Listener {
		
		
		private @Nullable String lastTypeId;
		
		
		public ScopeHandler(final IWorkbenchWindow scope, final @Nullable String commandId,
				final ImIdentitySet<String> launchFlags) {
			super(scope, commandId, new DocActionUtil(ActionUtil.ACTIVE_EDITOR_MODE), launchFlags);
		}
		
		
		private @Nullable IContentType getType() {
			final IWorkbenchWindow window= getWorkbenchWindow();
			final IEditorPart editor= window.getActivePage().getActiveEditor();
			if (editor instanceof DocEditor) {
				return ((DocEditor) editor).getContentType();
			}
			return null;
		}
		
		@Override
		protected synchronized byte updateManager(final IEvaluationContext context) {
			final DocProcessingManager manager;
			final IContentType type= getType();
			if (type != null) {
				if (type.getId() == this.lastTypeId) {
					return UNCHANGED;
				}
				this.lastTypeId= type.getId();
				manager= DocProcessingUI.getDocProcessingManager(type, true);
			}
			else {
				this.lastTypeId= null;
				manager= null;
			}
			
			return updateManager(manager);
		}
	
	}
	
	
	private ImIdentitySet<String> launchFlags;
	
	
	/** For instantiation via plugin.xml */
	public RunActiveDocConfigWorkbenchHandler() {
	}
	
	
	@Override
	public void setInitializationData(final IConfigurationElement config,
			final String propertyName, final @Nullable Object data) throws CoreException {
		super.setInitializationData(config, propertyName, data);
		try {
			final Map<String, String> parameters= (data instanceof Map) ?
					(Map<String, String>)data : Collections.emptyMap();
//			{	final String s= parameters.get(DocProcessingUI.CONTENT_TYPE_PAR_NAME);
//				if (s != null) {
//				}
//			}
			
			ImIdentitySet<String> launchFlags= null;
			{	final String s= parameters.get(DocActionUtil.LAUNCH_FLAGS_PAR_NAME);
				if (s != null) {
					launchFlags= CollectionUtils.toIdentifierSet(s.split(";")); //$NON-NLS-1$
				}
			}
			if (launchFlags == null) {
				launchFlags= nullable(this.launchFlags);
			}
			if (launchFlags == null) {
				final String commandId= getCommandId();
				if (commandId != null) {
					launchFlags= getCommandLaunchFlags(commandId);
				}
			}
			if (launchFlags == null) {
				throw new IllegalArgumentException(DocActionUtil.LAUNCH_FLAGS_PAR_NAME + "= <missing>"); //$NON-NLS-1$
			}
			this.launchFlags= launchFlags;
		}
		catch (final IllegalArgumentException e) {
			throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID, 0,
					NLS.bind("Invalid declaration of contribution by ''{0}''.", //$NON-NLS-1$
							config.getContributor().getName() ),
					e ));
		}
	}
	
	protected ImIdentitySet<String> getLaunchFlags() {
		return this.launchFlags;
	}
	
	protected ImIdentitySet<String> getCommandLaunchFlags(final String commandId) {
		return switch (commandId) {
		case DocProcessingUI.PROCESS_AND_PREVIEW_DOC_DEFAULT_COMMAND_ID ->
				CommonFlags.PROCESS_AND_PREVIEW;
		case DocProcessingUI.PROCESS_DOC_DEFAULT_COMMAND_ID ->
				CommonFlags.PROCESS;
		case DocProcessingUI.PREVIEW_DOC_DEFAULT_COMMAND_ID ->
				CommonFlags.OPEN_OUTPUT;
		default ->
				throw new IllegalArgumentException("commandId= " + commandId); //$NON-NLS-1$
		};
	}
	
	@Override
	protected ScopeHandler createScopeHandler(final Object scope) {
		return new ScopeHandler((IWorkbenchWindow)scope, getCommandId(), getLaunchFlags());
	}
	
}
