/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.viewer;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class DocViewerConfig {
	
	
	public static final String TYPE_ID= "org.eclipse.statet.docmlet.launchConfigurations.DocViewer"; //$NON-NLS-1$
	
	
/*[ Attributes ]===============================================================*/
	
	public static final String BASE_MAIN_ATTR_QUALIFIER= "org.eclipse.statet.docmlet.base/viewer"; //$NON-NLS-1$
	
	public static final String PROGRAM_FILE_ATTR_NAME= BASE_MAIN_ATTR_QUALIFIER + '/' +
			"ProgramFile.path"; //$NON-NLS-1$
	public static final String PROGRAM_ARGUMENTS_ATTR_NAME= BASE_MAIN_ATTR_QUALIFIER + '/' +
			"ProgramArguments.string"; //$NON-NLS-1$
	
	
	public static final String DDE_COMMAND_ATTR_KEY= "DDE.Command.message"; //$NON-NLS-1$
	public static final String DDE_APPLICATION_ATTR_KEY= "DDE.Application.name"; //$NON-NLS-1$
	public static final String DDE_TOPIC_ATTR_KEY= "DDE.Topic.name"; //$NON-NLS-1$
	
	public static final String TASK_VIEW_OUTPUT_ATTR_QUALIFIER= BASE_MAIN_ATTR_QUALIFIER + '/' +
			"ViewOutput"; //$NON-NLS-1$
	public static final String TASK_PRE_PRODUCE_OUTPUT_ATTR_QUALIFIER= BASE_MAIN_ATTR_QUALIFIER + '/' +
			"PreProduceOutput"; //$NON-NLS-1$
	
	
}
