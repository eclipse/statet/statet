/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.core.util.OverlayLaunchConfiguration;
import org.eclipse.statet.ecommons.variables.core.VariableText2;
import org.eclipse.statet.ecommons.variables.core.VariableUtils;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingConfig;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public class RunExternalProgramOperation extends AbstractLaunchConfigOperation {
	
	
	public static interface ExternalProgramLaunchConfig {
		
		String TYPE_ID= "org.eclipse.ui.externaltools.ProgramLaunchConfigurationType"; //$NON-NLS-1$
		
		String WORKING_DIRECTORY_ATTR_NAME=  "org.eclipse.ui.externaltools.ATTR_WORKING_DIRECTORY"; //$NON-NLS-1$
		String ARGUMENTS_ATTR_NAME= "org.eclipse.ui.externaltools.ATTR_TOOL_ARGUMENTS"; //$NON-NLS-1$
		
	}
	
	
	public static final String ID= "org.eclipse.statet.docmlet.base.docProcessing.RunExternalProgramOperation"; //$NON-NLS-1$
	
	public static final String LAUNCH_CONFIG_NAME_ATTR_NAME= ID + '/' + LAUNCH_CONFIG_NAME_ATTR_KEY;
	
	
	public RunExternalProgramOperation() {
		super(RunExternalProgramOperation.ExternalProgramLaunchConfig.TYPE_ID);
	}
	
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_RunExternalProgram_label;
	}
	
	
	@Override
	protected ILaunchConfiguration preprocessConfig(final ILaunchConfiguration config) throws CoreException {
		final Map<String, Object> additionalAttributes= new HashMap<>();
		final VariableText2 variableResolver= createVariableResolver();
		
		additionalAttributes.put(IDebugUIConstants.ATTR_LAUNCH_IN_BACKGROUND, false);
		
		try {
			String value= nonNullAssert(config.getAttribute(
					ExternalProgramLaunchConfig.WORKING_DIRECTORY_ATTR_NAME, "" )); //$NON-NLS-1$
			if (value.isEmpty()) {
				value= VariableUtils.getValue(VariableUtils.getChecked(
						getStepConfig().getToolConfig().getVariables(), DocProcessingConfig.WD_LOC_VAR_NAME ));
				additionalAttributes.put(ExternalProgramLaunchConfig.WORKING_DIRECTORY_ATTR_NAME, value);
			}
			else {
				value= variableResolver.performStringSubstitution(value, null);
				additionalAttributes.put(ExternalProgramLaunchConfig.WORKING_DIRECTORY_ATTR_NAME, value);
			}
		}
		catch (final CoreException e) {
			throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
					NLS.bind(Messages.ProcessingOperation_RunExternalProgram_Wd_error_SpecInvalid_message,
							e.getMessage() )));
		}
		
		try {
			String value= config.getAttribute(ExternalProgramLaunchConfig.ARGUMENTS_ATTR_NAME, ""); //$NON-NLS-1$
			if (!value.isEmpty()) {
				value= variableResolver.performStringSubstitution(value, null);
				additionalAttributes.put(ExternalProgramLaunchConfig.ARGUMENTS_ATTR_NAME, value);
			}
		}
		catch (final CoreException e) {
			throw new CoreException(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
					NLS.bind(Messages.ProcessingOperation_RunExternalProgram_Args_error_SpecInvalid_message,
							e.getMessage() )));
		}
		
		return new OverlayLaunchConfiguration(config, additionalAttributes);
	}
	
}
