/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.operations;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.isNull;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nullable;

import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceRuleFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingOperation;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolConfig.StepConfig;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingToolProcess;
import org.eclipse.statet.internal.docmlet.base.ui.processing.Messages;


@NonNullByDefault
public class CheckOutputOperation extends DocProcessingOperation {
	
	
	public static final byte REFRESH_FILE= 1;
	public static final byte REFRESH_FILE_DIRECTORY= 2;
	public static final byte REFRESH_WORKING_DIRECTORY= 3;
	
	
	public static final String ID= "org.eclipse.statet.docmlet.base.docProcessing.CheckFileOperation"; //$NON-NLS-1$
	
	
	private IFile file;
	
	private byte refeshMode= REFRESH_FILE;
	
	private int notExistsSeverity= IStatus.ERROR;
	
	
	public CheckOutputOperation() {
	}
	
	public CheckOutputOperation(final IFile file) {
		this.file= file;
	}
	
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public String getLabel() {
		return Messages.ProcessingOperation_CheckOutput_label;
	}
	
	
	public void setFile(final IFile file) {
		this.file= file;
	}
	
	public IFile getFile() {
		return this.file;
	}
	
	public void setRefresh(final byte mode) {
		this.refeshMode= mode;
	}
	
	public byte getRefresh() {
		return this.refeshMode;
	}
	
	public void setNotExistsSeverity(final int severity) {
		this.notExistsSeverity= severity;
	}
	
	@Override
	public void init(final StepConfig stepConfig, final Map<String, String> settings,
			final SubMonitor m) throws CoreException {
		super.init(stepConfig, settings, m);
		
		IFile file= nullable(this.file);
		if (file == null) {
			file= stepConfig.getOutputFile();
			this.file= file;
		}
	}
	
	
	@Override
	public int getTicks() {
		switch (this.refeshMode) {
		case REFRESH_WORKING_DIRECTORY:
			return 20;
		case REFRESH_FILE_DIRECTORY:
			return 15;
		default:
			return 10;
		}
	}
	
	@Override
	public IStatus run(final DocProcessingToolProcess toolProcess,
			final SubMonitor m) throws CoreException {
		final IFile file= this.file;
		if (isNull(file)) {
			throw new IllegalStateException("not initialized");
		}
		
		m.beginTask(NLS.bind(Messages.ProcessingOperation_CheckOutput_task,
						getStepConfig().getLabel() ),
				1 + 10 + 2 );
		
		final IResource refreshResource;
		final boolean refreshFile;
		switch (this.refeshMode) {
		case REFRESH_FILE:
			refreshResource= null;
			refreshFile= true;
			break;
		case REFRESH_FILE_DIRECTORY:
			refreshResource= file.getParent();
			refreshFile= false;
			break;
		case REFRESH_WORKING_DIRECTORY:
			refreshResource= getStepConfig().getToolConfig().getWorkingDirectory();
			refreshFile= refreshResource.getFullPath().isPrefixOf(file.getFullPath());
			break;
		default:
			refreshResource= null;
			refreshFile= false;
		}
		
		final ISchedulingRule schedulingRule;
		{	final IResourceRuleFactory ruleFactory= file.getWorkspace().getRuleFactory();
			ISchedulingRule rule= null;
			if (refreshResource != null) {
				rule= ruleFactory.refreshRule(refreshResource);
			}
			if (refreshFile) {
				rule= MultiRule.combine(rule,
						ruleFactory.refreshRule(file) );
			}
			schedulingRule= toolProcess.beginSchedulingRule(rule, m.newChild(1));
		}
		try {
			if (refreshResource != null) {
				refreshResource.refreshLocal(IResource.DEPTH_INFINITE,
						m.newChild((refreshFile) ? 8 : 10) );
			}
			if (refreshFile) {
				file.refreshLocal(IResource.DEPTH_ZERO,
						m.newChild((refreshResource != null) ? 2 : 10) );
			}
			
			if (!file.exists()) {
				return new Status(this.notExistsSeverity, DocmlBaseUI.BUNDLE_ID,
						NLS.bind(Messages.ProcessingOperation_CheckOutput_error_FileNotExists_message,
								file.getName(), getStepConfig().getLabel() ));
			}
			
			return Status.OK_STATUS;
		}
		finally {
			toolProcess.endSchedulingRule(schedulingRule);
		}
	}
	
}
