/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.ui.processing.actions;

import org.eclipse.core.resources.IFile;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.debug.ui.config.actions.ActionUtil;
import org.eclipse.statet.ecommons.debug.ui.config.actions.RunActiveConfigLaunchShortcut;

import org.eclipse.statet.internal.docmlet.base.ui.processing.DocActionUtil;


@NonNullByDefault
public class RunActiveDocConfigLaunchShortcut extends RunActiveConfigLaunchShortcut<IFile> {
	
	
	public RunActiveDocConfigLaunchShortcut() {
		super(new DocActionUtil(ActionUtil.ACTIVE_MENU_SELECTION_MODE));
	}
	
	
}
