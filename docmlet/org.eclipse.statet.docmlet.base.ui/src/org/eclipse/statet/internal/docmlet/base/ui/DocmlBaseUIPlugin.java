/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.base.ui;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUI;
import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.internal.docmlet.base.ui.markuphelp.DocmletHelpManager;
import org.eclipse.statet.internal.docmlet.base.ui.processing.DocProcessingRegistry;
import org.eclipse.statet.internal.docmlet.base.ui.viewer.DocViewerCloseDelegate;


@NonNullByDefault
public class DocmlBaseUIPlugin extends AbstractUIPlugin {
	
	
	private static @Nullable DocmlBaseUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static @Nullable DocmlBaseUIPlugin getInstance() {
		return DocmlBaseUIPlugin.instance;
	}
	
	
	public static void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private List<Disposable> disposables;
	
	private @Nullable DocmletHelpManager markupHelpManager;
	
	private @Nullable DocViewerCloseDelegate docViewerCloseDelegate;
	private @Nullable DocProcessingRegistry docProcessingRegistry;
	
	
	public DocmlBaseUIPlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.disposables= new ArrayList<>();
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
				this.docProcessingRegistry= null;
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					log(new Status(IStatus.ERROR, DocmlBaseUI.BUNDLE_ID,
							"Error occured while dispose module", //$NON-NLS-1$
							e ));
				}
			}
			this.disposables= null;
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(DocmlBaseUIResources.OBJ_PREAMBLE_IMAGE_ID, ImageRegistryUtil.T_OBJ, "preamble.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.OBJ_HEADING1_IMAGE_ID, ImageRegistryUtil.T_OBJ, "sectioning-1.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.OBJ_HEADING2_IMAGE_ID, ImageRegistryUtil.T_OBJ, "sectioning-2.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.OBJ_HEADING3_IMAGE_ID, ImageRegistryUtil.T_OBJ, "sectioning-3.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.OBJ_HEADING4_IMAGE_ID, ImageRegistryUtil.T_OBJ, "sectioning-4.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.OBJ_HEADING5_IMAGE_ID, ImageRegistryUtil.T_OBJ, "sectioning-5.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.OBJ_HEADING6_IMAGE_ID, ImageRegistryUtil.T_OBJ, "sectioning-6.png"); //$NON-NLS-1$
		
		util.register(DocmlBaseUIResources.OBJ_LABEL_DEF_IMAGE_ID, ImageRegistryUtil.T_OBJ, "label-def.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.OBJ_LABEL_REF_IMAGE_ID, ImageRegistryUtil.T_OBJ, "label-ref.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.OBJ_LABEL_TEXT_IMAGE_ID, ImageRegistryUtil.T_OBJ, "label-text.png"); //$NON-NLS-1$
		
		util.register(DocmlBaseUIResources.VIEW_MARKUP_HELP_IMAGE_ID, ImageRegistryUtil.T_VIEW, "markup_help.png"); //$NON-NLS-1$
		
		util.register(DocmlBaseUIResources.TOOL_PROCESS_IMAGE_ID, ImageRegistryUtil.T_TOOL, "process.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.TOOL_PROCESSANDPREVIEW_IMAGE_ID, ImageRegistryUtil.T_TOOL, "process_and_preview.png"); //$NON-NLS-1$
		util.register(DocmlBaseUIResources.TOOL_PREVIEW_IMAGE_ID, ImageRegistryUtil.T_TOOL, "preview.png"); //$NON-NLS-1$
	}
	
	
	public synchronized DocmletHelpManager getMarkupHelpManager() {
		DocmletHelpManager markupHelpManager= this.markupHelpManager;
		if (markupHelpManager == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			markupHelpManager= new DocmletHelpManager();
			this.markupHelpManager= markupHelpManager;
		}
		return markupHelpManager;
	}
	
	public synchronized DocViewerCloseDelegate getDocViewerCloseDelegate() {
		DocViewerCloseDelegate docViewerCloseDelegate= this.docViewerCloseDelegate;
		if (docViewerCloseDelegate == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			docViewerCloseDelegate= new DocViewerCloseDelegate();
			this.disposables.add(docViewerCloseDelegate);
			this.docViewerCloseDelegate= docViewerCloseDelegate;
		}
		return docViewerCloseDelegate;
	}
	
	public synchronized DocProcessingRegistry getDocProcessingRegistry() {
		DocProcessingRegistry docProcessingRegistry= this.docProcessingRegistry;
		if (docProcessingRegistry == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			docProcessingRegistry= new DocProcessingRegistry();
			this.disposables.add(docProcessingRegistry);
			this.docProcessingRegistry= docProcessingRegistry;
		}
		return docProcessingRegistry;
	}
	
}
