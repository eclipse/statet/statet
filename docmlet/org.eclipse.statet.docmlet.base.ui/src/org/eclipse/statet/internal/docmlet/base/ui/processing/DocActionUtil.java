/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.base.ui.processing;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.debug.ui.config.LaunchConfigManager;
import org.eclipse.statet.ecommons.debug.ui.config.actions.ActionUtil;

import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingManager;
import org.eclipse.statet.docmlet.base.ui.processing.DocProcessingUI;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


/**
 * 
 * element= doc file
 */
@NonNullByDefault
public class DocActionUtil extends ActionUtil<IFile> {
	
	
	private @Nullable IContentType contentType; // for fix type
	
	
	public DocActionUtil(final byte initialMode) {
		super(initialMode);
	}
	
	
	public void setContentType(final IContentType contentType) {
		this.contentType= contentType;
	}
	
	
	@Override
	public @Nullable IFile getLaunchElement(final @Nullable IEditorPart editor) {
		IResource resource= null;
		if (editor instanceof SourceEditor) {
			resource= getSingleResource(editor.getEditorInput());
		}
		return (resource instanceof IFile) ? (IFile) resource : null;
	}
	
	@Override
	public @Nullable IFile getLaunchElement(final @Nullable ISelection selection) {
		IResource resource= null;
		if (selection instanceof IStructuredSelection) {
			resource= getSingleResource((IStructuredSelection) selection);
		}
		return (resource instanceof IFile) ? (IFile) resource : null;
	}
	
	@Override
	public @Nullable LaunchConfigManager<IFile> getManager(final IWorkbenchWindow window,
			final IFile element) {
		return getManager(getContentType(window, element));
	}
	
	
	public @Nullable IContentType getContentType(final IWorkbenchWindow window,
			final @Nullable IFile element) {
		IContentType contentType= this.contentType;
		
		if (contentType == null) {
			switch (getMode()) {
			case DocActionUtil.ACTIVE_EDITOR_MODE:
				if (window != null) {
					final IEditorPart activeEditor= window.getActivePage().getActiveEditor();
					if (activeEditor instanceof SourceEditor) {
						contentType= ((SourceEditor) activeEditor).getContentType();
					}
				}
				break;
			case DocActionUtil.ACTIVE_MENU_SELECTION_MODE:
				if (element != null) {
					try {
						final IContentDescription contentDescription= element.getContentDescription();
						if (contentDescription != null) {
							contentType= contentDescription.getContentType();
						}
					}
					catch (final CoreException e) {}
				}
				break;
			default:
				break;
			}
		}
		
		return contentType;
	}
	
	public @Nullable DocProcessingManager getManager(final @Nullable IContentType contentType) {
		return (contentType != null) ?
				DocProcessingUI.getDocProcessingManager(contentType, true) :
				null;
	}
	
}
