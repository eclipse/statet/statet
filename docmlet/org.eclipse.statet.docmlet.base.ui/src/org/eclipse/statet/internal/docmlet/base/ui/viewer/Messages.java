/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.base.ui.viewer;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String MainTab_name;
	public static String MainTab_LoadPreset_label;
	public static String MainTab_Program_label;
	public static String MainTab_ProgramPath_label;
	public static String MainTab_ProgramPath_name;
	public static String MainTab_ProgramArgs_label;
	
	public static String ProgramArgs_error_Other_message;
	
	public static String MainTab_DDE_ViewOutput_label;
	public static String MainTab_DDE_PreProduceOutput_label;
	public static String MainTab_DDECommand_label;
	public static String MainTab_DDEApplication_label;
	public static String MainTab_DDETopic_label;
	
	public static String DDE_ViewOutput_label;
	public static String DDE_PreProduceOutput_label;
	public static String DDECommand_error_Other_message;
	public static String DDEApplication_error_Other_message;
	public static String DDETopic_error_Other_message;
	
	public static String PreProduceOutput_task; 
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
