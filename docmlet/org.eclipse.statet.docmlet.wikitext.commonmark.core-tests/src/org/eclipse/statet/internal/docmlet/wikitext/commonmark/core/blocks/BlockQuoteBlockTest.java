/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCanStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCannotStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class BlockQuoteBlockTest {
	
	
	private final BlockQuoteBlock block= new BlockQuoteBlock();
	
	
	@Test
	public void canStart() {
		assertCanStart(this.block, ">");
		assertCanStart(this.block, "> ");
		assertCanStart(this.block, ">test");
		assertCanStart(this.block, "> test");
		assertCanStart(this.block, " > test");
		assertCanStart(this.block, "  > test");
		assertCanStart(this.block, "   > test");
		assertCannotStart(this.block, "    > test");
		assertCannotStart(this.block, "test");
		assertCannotStart(this.block, " test");
	}
	
	@Test
	public void blockQuoteSimple() {
		assertContent("<p>test</p><blockquote><p>bq one bq two</p></blockquote><p>three</p>",
				"test\n > bq one\n > bq two\n\nthree");
	}
	
	@Test
	public void blockQuoteSimpleWithLazyContinuation() {
		assertContent("<p>test</p><blockquote><p>bq one bq two</p></blockquote><p>three</p>",
				"test\n > bq one\nbq two\n\nthree");
	}
	
	@Test
	public void blockQuoteContainsBlocks() {
		assertContent("<p>test</p><blockquote><ul><li>one</li></ul></blockquote><ul><li>two</li></ul><p>three</p>",
				"test\n > * one\n* two\n\nthree");
	}
	
	@Test
	public void blockQuoteLazyContinuation() {
		assertContent("<blockquote><ul><li>one two</li></ul></blockquote>", "> * one\ntwo\n");
	}
	
	@Test
	public void blockQuoteLazyContinuationNested() {
		assertContent("<blockquote><ul><li><ul><li>one two</li></ul></li></ul></blockquote>", "> * - one\ntwo\n");
		assertContent("<blockquote><blockquote><p>one two</p></blockquote></blockquote>", "> > one\ntwo\n");
		assertContent("<blockquote><ul><li><h3>one</h3</li></ul></blockquote><p>two</p>", "> * ### one\ntwo\n");
		assertContent("<blockquote><blockquote><pre><code> one\n</code></pre></blockquote></blockquote><p>two</p>", "> >      one\ntwo\n");
	}
	
	@Test
	public void blockQuoteLazyContinuationStopped() {
		assertContent("<blockquote><p>one</p></blockquote><hr/>", "> one\n****");
	}
	
	@Test
	public void blockQuoteParagraphNewlines() {
		for (final String newline : ImCollections.newList("\n", "\r", "\r\n")) {
			assertContent(
					"<blockquote><p>p1 first p1 second p1 third</p></blockquote><blockquote><p>p2 first</p></blockquote>",
					"> p1 first" + newline + "> p1 second" + newline + "> p1 third" + newline + newline + "> p2 first");
		}
	}
	
}
