/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.spec;

import static org.junit.jupiter.api.Assumptions.assumeFalse;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.builder.NoOpDocumentBuilder;
import org.eclipse.mylyn.wikitext.util.LocationTrackingReader;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.CommonmarkLanguage;
import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupParser2;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class CommonMarkSpecTest {
	
	
	private static final String SPEC_VERSION= "0.30";
	
	private static final URI COMMONMARK_SPEC_URI= URI.create(
			String.format("https://raw.githubusercontent.com/commonmark/commonmark-spec/%s/spec.txt", SPEC_VERSION) );
	
	private static final String CASE_START= "```````````````````````````````` example";
	private static final String CASE_SEP= ".";
	private static final String CASE_END= "````````````````````````````````";
	
	
	private static final List<String> HEADING_EXCLUSIONS= ImCollections.newList();
	
	private static final List<Integer> LINE_EXCLUSIONS= ImCollections.newList();
	
	
	public static class SpecTestCase {
		
		private final int num;
		private final String heading;
		
		private final int lineNumber;
		
		final String input;
		final String expected;
		
		public SpecTestCase(final int num, final String heading, final int lineNumber,
				final String input, final String expected) {
			this.num= num;
			this.heading= heading;
			this.lineNumber= lineNumber;
			
			this.input= input;
			this.expected= expected;
		}
		
		@Override
		public String toString() {
			return String.format("%1$s (section= %2$s, lineNumber= %3$s)", this.num, this.heading, this.lineNumber);
		}
		
	}
	
	
	public static List<SpecTestCase> testCases() {
		final List<SpecTestCase> parameters= new ArrayList<>();
		
		loadTestCases(parameters);
		
		return ImCollections.toList(parameters);
	}
	
	
	private void preconditions(final SpecTestCase testCase) {
		assumeFalse(HEADING_EXCLUSIONS.contains(testCase.heading));
		assumeFalse(LINE_EXCLUSIONS.contains(Integer.valueOf(testCase.lineNumber)));
	}
	
	@ParameterizedTest
	@MethodSource("testCases")
	public void test(final SpecTestCase testCase) {
		preconditions(testCase);
		try {
			final CommonmarkLanguage language= createCommonmarkLanguage();
			assertContent(language, testCase.expected, testCase.input);
		}
		catch (final Error | RuntimeException e) {
			final String info= testCase.heading + ": lineNumber= " + testCase.lineNumber;
			e.addSuppressed(new Exception(info + "\n===\n" + testCase.input + "\n===\n"));
			throw e;
		}
	}
	
	/** Tests only whether there is no runtime error */
	@ParameterizedTest
	@MethodSource("testCases")
	public void test_SourceStruct(final SpecTestCase testCase) {
		preconditions(testCase);
		try {
			final CommonmarkLanguage language= createCommonmarkLanguage();
			final MarkupParser2 parser2= new MarkupParser2(language, new NoOpDocumentBuilder(),
					ProcessingContext.PARSE_SOURCE_STRUCT );
			language.processContent(parser2, testCase.input, true);
		}
		catch (Error | RuntimeException e) {
			final String info= testCase.heading + ": lineNumber= " + testCase.lineNumber;
			e.addSuppressed(new Exception(info + "\n===\n" + testCase.input + "\n===\n"));
			throw e;
		}
	}
	
	private CommonmarkLanguage createCommonmarkLanguage() {
		final CommonmarkLanguage language= new CommonmarkLanguage();
		return language;
	}
	
	
	private static void loadTestCases(final List<SpecTestCase> parameters) {
		final Matcher headingMatcher= Pattern.compile("#+\\s*(.+)").matcher("");
		try {
			final String spec= Files.readString(provideCommonMarkSpec());
			final LocationTrackingReader reader= new LocationTrackingReader(new StringReader(spec));
			String heading= "unspecified";
			String line;
			int num= 1;
			while ((line= reader.readLine()) != null) {
				line= line.replace('→', '\t');
				if (line.trim().equals(CASE_START)) {
					final int testLineNumber= reader.getLineNumber();
					final String input= readUntilDelimiter(reader, CASE_SEP);
					final String expected= readUntilDelimiter(reader, CASE_END);
					parameters.add(new SpecTestCase(num, heading, testLineNumber + 1,
							input, expected ));
					num++;
				}
				if (headingMatcher.reset(line).matches()) {
					heading= nonNullAssert(headingMatcher.group(1));
				}
			}
		}
		catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static String readUntilDelimiter(final LocationTrackingReader reader, final String end) throws IOException {
		final List<String> lines= new ArrayList<>();
		String line;
		while ((line= reader.readLine()) != null) {
			line= line.replace('→', '\t');
			if (line.trim().equals(end)) {
				break;
			}
			lines.add(line);
		}
		return CollectionUtils.toString(lines, "\n");
	}
	
	private static Path provideCommonMarkSpec() throws IOException {
		final Path tmpPath= Paths.get("./tmp");
		if (!Files.isDirectory(tmpPath)) {
			Files.createDirectory(tmpPath);
		}
		final Path specPath= tmpPath.resolve(String.format("spec-%s.txt", SPEC_VERSION));
//		Files.delete(specPath);
		if (!Files.exists(specPath)) {
			Files.copy(COMMONMARK_SPEC_URI.toURL().openStream(), specPath);
		}
		return specPath;
	}
	
}
