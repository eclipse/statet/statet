/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCanStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCannotStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.eclipse.mylyn.wikitext.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.parser.builder.NoOpDocumentBuilder;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.CommonmarkLanguage;


@NonNullByDefault
public class ParagraphBlockTest {
	
	
	private final ParagraphBlock<?> block= new ParagraphBlock<>();
	
	
	public ParagraphBlockTest() {
	}
	
	
	@Test
	public void canStart() {
		assertCannotStart(this.block, "");
		assertCannotStart(this.block, "\none");
		assertCanStart(this.block, "one");
	}
	
	@Test
	public void assertParagraph() {
		assertContent("<p>first para second line</p><p>second para fourth line here</p>",
				"first para\nsecond line\n\nsecond para\nfourth line here\n\n\n");
		assertContent("<p>first para second line</p>", "first para\n    second line");
	}
	
	@Test
	public void paragraphNewlines() {
		for (final String newline : ImCollections.newList("\n", "\r", "\r\n")) {
			assertContent("<p>p1 first p1 second p1 third</p><p>p2 first</p>",
					"p1 first" + newline + "p1 second" + newline + "p1 third" + newline + newline + "p2 first");
		}
	}
	
	@Test
	public void trim() {
		// remove of indent from content
		new MarkupParser(new CommonmarkLanguage(), new NoOpDocumentBuilder() {
			
			int i;
			String[] expected= { "aaa", "\n", "bbb", "\n", "ccc" };
			
			@Override
			public void characters(final String text) {
				assertEquals(this.expected[this.i++], text);
			}
			
		}).parse("   aaa\n      bbb\n\tccc");
		
		// remove of final spaces from content
		new MarkupParser(new CommonmarkLanguage(), new NoOpDocumentBuilder() {
			
			int i;
			String[] expected= { "aaa", "\n", "bbb" };
			
			@Override
			public void characters(final String text) {
				assertEquals(this.expected[this.i++], text);
			}
			
		}).parse("   aaa\n      bbb   ");
	}
	
}
