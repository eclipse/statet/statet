/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCanStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCannotStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ThematicBreakBlockTest {
	
	
	private final ThematicBreakBlock block= new ThematicBreakBlock();
	
	
	public ThematicBreakBlockTest() {
	}
	
	
	@Test
	public void canStart() {
		assertCannotStart(this.block, "");
		assertCannotStart(this.block, "a");
		assertCannotStart(this.block, "    ***");
		for (final char c : "*_-".toCharArray()) {
			final String hrIndicator= String.valueOf(c).repeat(3);
			assertCanStart(this.block, "   " + hrIndicator);
			assertCanStart(this.block, "  " + hrIndicator);
			assertCanStart(this.block, " " + hrIndicator);
			assertCannotStart(this.block, "    " + hrIndicator);
			assertCanStart(this.block, hrIndicator);
			assertCanStart(this.block, String.valueOf(c).repeat(4));
			assertCanStart(this.block, String.valueOf(c).repeat(14));
		}
		
		// Bug 472390:
		assertCannotStart(this.block, "\t***");
	}
	
	@Test
	public void process() {
		assertContent("<p>one</p><hr/>", "one\n\n------\n");
		assertContent("<p>one</p><hr/>", "one\n\n---\n");
		assertContent("<p>one</p><hr/>", "one\n\n-  - -\n");
		assertContent("<p>one</p><hr/>", "one\n\n   ** *****\n");
	}
	
}
