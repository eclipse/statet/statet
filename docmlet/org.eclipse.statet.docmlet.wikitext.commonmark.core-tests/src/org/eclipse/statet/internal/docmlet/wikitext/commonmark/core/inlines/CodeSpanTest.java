/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Cursors.createCursor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CodeSpanTest extends AbstractSourceSpanTest {
	
	
	public CodeSpanTest() {
		super(new CodeSpan());
	}
	
	
	@Test
	public void createInline() {
		assertNoInline(createCursor("two"));
		assertNoInline(createCursor("`"));
		assertNoInline(createCursor("`one``"));
		assertInline(Characters.class, 0, 3, createCursor("```one``"));
		assertCode(14, "this is code", "`this is code`");
		assertCode(12, "one two ", "``one\ntwo\n``");
		assertCode(14, "one *two", "```one *two```");
		assertCode(14, "one *two` ", "``one *two` ``");
	}
	
	private void assertCode(final int length, final String codeText, final String content) {
		final Code code= assertInline(Code.class, 0, length, createCursor(content));
		assertEquals(codeText, code.getText());
	}
	
}
