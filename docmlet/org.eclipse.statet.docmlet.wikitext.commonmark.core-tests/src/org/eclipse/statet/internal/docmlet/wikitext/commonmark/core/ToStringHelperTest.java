/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class ToStringHelperTest {
	
	
	public ToStringHelperTest() {
	}
	
	
	@Test
	public void toStringValue() {
		assertEquals(null, ToStringHelper.toStringValue(null));
		assertEquals("", ToStringHelper.toStringValue(""));
		assertEquals("abc", ToStringHelper.toStringValue("abc"));
		assertEquals("01234567890123456789...",
				ToStringHelper.toStringValue("0123456789".repeat(10)));
		assertEquals("a\\r\\n\\tb", ToStringHelper.toStringValue("a\r\n\tb"));
	}
	
}
