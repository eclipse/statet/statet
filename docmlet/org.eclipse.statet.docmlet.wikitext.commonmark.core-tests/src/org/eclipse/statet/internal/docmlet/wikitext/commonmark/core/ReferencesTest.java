/*=============================================================================#
 # Copyright (c) 2020, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References.REF_TITLE_REGEX;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References.LinkDestination;


@NonNullByDefault
public class ReferencesTest {
	
	
	public ReferencesTest() {
	}
	
	
	@Test
	public void read_BracketUri() {
		assertNotNull(parseLinkDestination("<one+two>"));
		assertNotNull(parseLinkDestination("<>"));
		assertNull(parseLinkDestination("<one two"));
		assertNull(parseLinkDestination("<one+two"));
		assertLink("one+two", 0, 2, parseLinkDestination("<one+two>>"));
		assertLink("one+two", 1, 2, parseLinkDestination(" <one+two>>"));
		assertLink("one+two\\>", 0, 2, parseLinkDestination("<one+two\\>>"));
	}
	
	@Test
	public void read_NoBracketUri() {
		assertNotNull(parseLinkDestination("onetwo"));
		assertNotNull(parseLinkDestination("/one/two(threefour\\))"));
		assertLink("one", 0, 0, parseLinkDestination("one two"));
		assertLink("one", 1, 0, parseLinkDestination(" one two"));
		assertLink("one(two(three))", 0, 0, parseLinkDestination("one(two(three))"));
		assertLink("/one/two(three\\))", 0, 0, parseLinkDestination("/one/two(three\\))"));
		assertLink("foo(and\\(bar\\))", 0, 0, parseLinkDestination("foo(and\\(bar\\))"));
		assertLink("foo\\)\\:", 0, 0, parseLinkDestination("foo\\)\\:"));
	}
	
	private @Nullable LinkDestination parseLinkDestination(final String markup) {
		return References.readLinkDestination(markup, 0);
	}
	
	private void assertLink(final String expectedUri, final int offset, final int add,
			final @Nullable LinkDestination linkDestination) {
		assertNotNull(linkDestination);
		assertEquals(expectedUri, linkDestination.getEscapedUri());
		assertEquals(offset + expectedUri.length() + add, linkDestination.getEndOffset());
	}
	
	
	@Test
	public void titlePart() {
		final Pattern pattern= Pattern.compile("(" + REF_TITLE_REGEX + ")", Pattern.DOTALL);
		assertMatch(pattern, "\"one two ('\\\" three\"");
		assertMatch(pattern, "'one two \\\"\\' three'");
		assertMatch(pattern, "(one two \"\\'\\) three)");
		assertMatch(pattern, "(one two (three (four\\)\\))");
		assertFalse(pattern.matcher("\"one").matches());
		assertFalse(pattern.matcher("one\"").matches());
		assertFalse(pattern.matcher("one'").matches());
		assertFalse(pattern.matcher("'one").matches());
		assertFalse(pattern.matcher("(one").matches());
		assertFalse(pattern.matcher("one)").matches());
	}
	
	private void assertMatch(final Pattern pattern, final String input) {
		final Matcher matcher= pattern.matcher(input);
		assertTrue(matcher.matches());
		assertEquals(input, matcher.group(1));
	}
	
}
