/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.AbstractSourceSpanTest.assertParse;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.TextSegment;


@NonNullByDefault
public class InlineParserTest {
	
	
	private final Line line= new Line(0, 1, 0, "test", "\n");
	
	
	public InlineParserTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void requiresSpans() {
		Assertions.assertThrows(NullPointerException.class,
				() -> new InlineParser(null) );
	}
	
	@Test
	public void parse() {
		assertParse("", createInlines());
		assertParse("one\ntwo", createInlines(),
				new Characters(this.line, 0, 3, 3, "one"),
				new SoftLineBreak(this.line, 3, 1, 1),
				new Characters(this.line, 4, 3, 3, "two") );
		assertParse("one\ntwo three", createInlines(),
				new Characters(this.line, 0, 3, 3, "one"),
				new SoftLineBreak(this.line, 3, 1, 1),
				new Characters(this.line, 4, 9, 9, "two three") );
	}
	
	@Test
	public void toStringContent() {
		final InlineParser parser= new InlineParser(ImCollections.newList(
				new CodeSpan(), new AllCharactersSpan() ));
		final String stringContent= parser.toStringContent(CommonmarkAsserts.newContext(),
				new TextSegment(ImCollections.newList(new Line(1, 0, 0, "one `two` three", "\n"))));
		assertEquals("one two three", stringContent);
	}
	
	private InlineParser createInlines() {
		return new InlineParser(ImCollections.newList(
				new LineBreakSpan(), new StringCharactersSpan(), new AllCharactersSpan() ));
	}
	
}
