/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCanStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCannotStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class IndentedCodeBlockTest {
	
	
	private final IndentedCodeBlock block= new IndentedCodeBlock();
	
	
	public IndentedCodeBlockTest() {
	}
	
	
	@Test
	public void canStart() {
		assertCannotStart(this.block, "   code");
		assertCannotStart(this.block, "      ");
		assertCanStart(this.block, "    code");
		assertCanStart(this.block, "     code");
		assertCannotStart(this.block, " code");
		assertCannotStart(this.block, "  code");
		assertCannotStart(this.block, "non-blank\n    code");
		assertCanStart(this.block, "\tcode");
		assertCanStart(this.block, "\t code");
		assertCanStart(this.block, " \tcode");
		assertCanStart(this.block, "  \tcode");
		assertCanStart(this.block, "   \tcode");
	}
	
	@Test
	public void process() {
		assertContent("<pre><code>code\n</code></pre>", "    code");
		assertContent("<pre><code>code\n</code></pre>", "\tcode");
		assertContent("<pre><code> code\n</code></pre>", "\t code");
		assertContent("<pre><code>code  \n</code></pre>", "\tcode  ");
		assertContent("<pre><code>\tcode\n</code></pre>", "    \tcode");
		assertContent("<pre><code>one\ntwo\n</code></pre><p>three</p>", "    one\n    two\n three");
		assertContent("<pre><code>one\n\nthree\n</code></pre>", "    one\n\n    three");
		assertContent("<pre><code>one\n  \nthree\n</code></pre>", "    one\n      \n    three");
		
		// Bug 472395:
		assertContent("<pre><code>\t\tcode\n</code></pre>", "\t\t\tcode");
	}
	
}
