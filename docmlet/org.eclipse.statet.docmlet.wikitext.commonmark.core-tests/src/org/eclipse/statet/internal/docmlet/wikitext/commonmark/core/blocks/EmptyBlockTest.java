/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCanStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCannotStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class EmptyBlockTest {
	
	
	private final EmptyBlock block= new EmptyBlock();
	
	
	public EmptyBlockTest() {
	}
	
	
	@Test
	public void canStart() {
		assertCannotStart(this.block, "one");
		assertCanStart(this.block, "\n");
	}
	
	@Test
	public void process() {
		assertContent("<p>2</p>", "\n2");
	}
	
}
