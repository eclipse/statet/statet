/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext.ReferenceDef;


@NonNullByDefault
public class ProcessingContextTest {
	
	
	public ProcessingContextTest() {
	}
	
	
	@Test
	public void empty() {
		final ProcessingContext context= CommonmarkAsserts.newContext();
		assertNotNull(context);
		assertFalse(context.hasReferenceDefs());
	}
	
	@Test
	public void referenceDefinition() {
		final ProcessingContext context= CommonmarkAsserts.newContext();
		context.addReferenceDef(
				nonNullAssert(context.normalizeLabel("onE")), "/uri", "a title" );
		assertNotNull(context);
		assertTrue(context.hasReferenceDefs());
		assertNotNull(context.getReferenceDef(
				nonNullAssert(context.normalizeLabel("one")) ));
		assertNotNull(context.getReferenceDef(
				nonNullAssert(context.normalizeLabel("One")) ));
		final ReferenceDef def= context.getReferenceDef(
				nonNullAssert(context.normalizeLabel("ONE")) );
//		assertEquals("onE", link.getName());
		assertNotNull(def);
		assertEquals("/uri", def.getUri());
		assertEquals("a title", def.getTitle());
		assertNull(context.getReferenceDef("Unknown"));
	}
	
	public void referenceDefinitionEmptyName() {
		final ProcessingContext context= CommonmarkAsserts.newContext();
		context.addReferenceDef("", "one", "two");
		assertFalse(context.hasReferenceDefs());
	}
	
	@Test
	public void referenceDefinitionDuplicate() {
		final ProcessingContext context= CommonmarkAsserts.newContext();
		context.addReferenceDef("a", "/uri", "a title");
		context.addReferenceDef("a", "/uri2", "a title2");
		final ReferenceDef uriWithTitle= context.getReferenceDef("a");
		assertNotNull(uriWithTitle);
		assertEquals("/uri", uriWithTitle.getUri());
	}
	
	@Test
	public void generateHeadingId() {
		final ProcessingContext processingContext= CommonmarkAsserts.newContext();
		processingContext.setMode(ProcessingContext.EMIT_DOCUMENT);
		assertEquals("a", processingContext.generateHeadingId(1, "a"));
		assertEquals("a2", processingContext.generateHeadingId(1, "a"));
		assertEquals("a3", processingContext.generateHeadingId(2, "a"));
		assertEquals("h1-3", processingContext.generateHeadingId(1, null));
		assertEquals("h1-4", processingContext.generateHeadingId(1, ""));
	}
	
}
