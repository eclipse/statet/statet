/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class CommonMarkIdGenerationStrategyTest {
	
	
	private final CommonmarkIdGenerationStrategy strategy= new CommonmarkIdGenerationStrategy();
	
	
	public CommonMarkIdGenerationStrategyTest() {
	}
	
	
	@Test
	public void simple() {
		assertId("abc", "abc");
		assertId("abc123", "abc123");
		assertId("a_bc", "a_bc");
	}
	
	@Test
	public void mixedCase() {
		assertId("abc", "AbC");
	}
	
	@Test
	public void whitespace() {
		assertId("a-bc", "a bc");
		assertId("a-bc", "a  \tbc");
		assertId("abc", " abc");
		assertId("abc", "abc ");
	}
	
	@Test
	public void allWhitespace() {
		assertId("", "   \t");
	}
	
	@Test
	public void hyphenated() {
		assertId("a-b", "a-b");
		assertId("ab", "-ab");
		assertId("ab", "ab-");
	}
	
	@Test
	public void punctuationAndSpecialCharacters() {
		assertId("a-b", "a.b");
		assertId("a-b", "a....b");
		assertId("a-b", "a,b");
		assertId("a-b", "a;b");
		assertId("a-b", "a*b");
		assertId("a-b", "a&b");
		assertId("ab", ".ab");
	}
	
	private void assertId(final String expected, final String headingText) {
		assertEquals(expected, this.strategy.generateId(headingText));
	}
	
}
