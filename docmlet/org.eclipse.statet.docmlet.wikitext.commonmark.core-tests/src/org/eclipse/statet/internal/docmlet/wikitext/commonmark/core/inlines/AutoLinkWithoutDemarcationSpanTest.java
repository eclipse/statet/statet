/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Cursors.createCursor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;


@NonNullByDefault
public class AutoLinkWithoutDemarcationSpanTest extends AbstractSourceSpanTest {
	
	
	public AutoLinkWithoutDemarcationSpanTest() {
		super(new AutoLinkWithoutDemarcationSpan());
	}
	
	
	@Test
	public void createInline() {
		assertNoInline(createCursor("httpx://example.com sdf"));
		assertNoInline(createCursor("http:/"));
		assertLink(0, 28, "http://example.com:8080/#see", "http://example.com:8080/#see",
				createCursor("http://example.com:8080/#see one"));
		assertLink(0, 23, "https://example.com/foo", "https://example.com/foo",
				createCursor("https://example.com/foo\\"));
		assertLink(1, 23, "https://example.com/foo", "https://example.com/foo",
				createCursor("(https://example.com/foo)", 1));
	}
	
	private void assertLink(final int offset, final int length, final String linkHref, final String text, final Cursor cursor) {
		final Link link= assertInline(Link.class, offset, length, cursor);
		assertEquals(linkHref, link.getHref());
		assertEquals(1, link.getContents().size());
		assertEquals(Characters.class, link.getContents().get(0).getClass());
		assertEquals(text, ((Characters)link.getContents().get(0)).getText());
	}
	
}
