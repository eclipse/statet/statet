/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.DefaultCommonmarkConfig;


@NonNullByDefault
public class ParagraphBlockExtTest {
	
	
	public ParagraphBlockExtTest() {
	}
	
	
	@Test
	public void blank_before_header() {
		final String input= "first para\n# second line\n";
		final DefaultCommonmarkConfig config= new DefaultCommonmarkConfig();
		config.setHeaderInterruptParagraphDisabled(true);
		
		assertContent("<p>first para</p><h1>second line</h1>", input);
		assertContent("<p>first para # second line</p>", input, config);
	}
	
	@Test
	public void blank_before_blockquote() {
		final String input= "first para\n> second line\n";
		final DefaultCommonmarkConfig config= new DefaultCommonmarkConfig();
		config.setBlockquoteInterruptParagraphDisabled(true);
		assertContent("<p>first para</p><blockquote><p>second line</p></blockquote>", input);
		assertContent("<p>first para &gt; second line</p>", input, config);
	}
	
}
