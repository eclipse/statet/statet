/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCanStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCannotStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.CommonmarkLanguage;


@NonNullByDefault
public class AtxHeaderBlockTest {
	
	
	AtxHeaderBlock block= new AtxHeaderBlock();
	
	
	@Test
	public void canStart() {
		assertCannotStart(this.block, "");
		assertCanStart(this.block, "#");
		assertCanStart(this.block, "# ");
		assertCanStart(this.block, "# #");
		assertCanStart(this.block, "# Y");
		assertCanStart(this.block, "# Y #");
		assertCanStart(this.block, "## Y");
		assertCanStart(this.block, "### Y");
		assertCanStart(this.block, "#### Y");
		assertCanStart(this.block, "##### Y");
		assertCanStart(this.block, "###### Y");
		assertCannotStart(this.block, "####### Y");
		assertCanStart(this.block, "# Y#");
		assertCannotStart(this.block, "#Y");
		
		// Bug 472386:
		assertCanStart(this.block, "# #Y");
		assertCanStart(this.block, "   # Y");
		assertCannotStart(this.block, "\t# Y");
	}
	
	@Test
	public void basic() {
		assertContent("<h2 id=\"one-two\">One Two</h2>", "## One Two",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h2 id=\"one-two\">One Two</h2>", "## One Two #####   ",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h2 id=\"one-two\">One Two#</h2>", "## One Two#",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h2 id=\"one-two\">#One #Two</h2>", "## #One #Two",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<p>One</p><h1 id=\"two\">two</h1><p>Three</p>", "One\n# two\nThree",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h2></h2>", "##");
		assertContent("<h2></h2>", "## ##");
	}
	
	
	@Test
	public void trimContent() {
		assertContent("<h2 id=\"one-two\">One Two</h2>",
				"## \tOne Two #   ",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h2 id=\"one-two\">One Two</h2>",
				"## \tOne Two \t",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h2 id=\"one-two\">One Two</h2>",
				"## \tOne Two \t#   ",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h2></h2>",
				"## \t#   ",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
	}
	
	@Test
	public void withNestedInlines() {
		assertContent("<h2 id=\"one-two-three\">One <em>Two</em> \\<strong>three</strong></h2>",
				"## One *Two* \\\\__three__ ##",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
	}
	
}
