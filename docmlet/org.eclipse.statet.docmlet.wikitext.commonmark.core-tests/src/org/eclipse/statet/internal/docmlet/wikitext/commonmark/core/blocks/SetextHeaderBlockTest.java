/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertCanStart;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;

import org.eclipse.mylyn.wikitext.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.parser.builder.NoOpDocumentBuilder;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.CommonmarkLanguage;


@NonNullByDefault
public class SetextHeaderBlockTest {
	
	
	private final ParagraphAndTheLikeBlock block= new ParagraphAndTheLikeBlock();
	
	
	@Test
	public void canStart() {
		assertCanStart(this.block, "Heading\n-");
		assertCanStart(this.block, "Heading\n=");
		assertCanStart(this.block, "Heading\n  =");
		assertCanStart(this.block, "Heading\n   =");
//		assertCannotStart(block, "Heading\n    =");
		assertCanStart(this.block, "Heading\n=====");
		assertCanStart(this.block, "Heading Text\n-----");
//		assertCannotStart(block, "Heading\n\n=====");
		assertCanStart(this.block, "   Heading\n=====");
//		assertCannotStart(block, "    Heading\n=====");
		
		// Bug 472404:
//		assertCannotStart(block, "\tHeading\n=");
//		assertCannotStart(block, "Heading\n\t=");
//		assertCannotStart(block, "Heading\n=-");
	}
	
	@Test
	public void process() {
		assertContent("<h2 id=\"heading-text\">Heading Text</h2>", "Heading Text\n-------",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h1 id=\"heading-text\">Heading Text</h1>", "Heading Text\n=",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h1 id=\"heading-text\">Heading Text</h1>", "Heading Text\n====",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h1 id=\"heading-text\">Heading <em>Text</em></h1>", "Heading *Text*\n====",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		
		// Bug 472404 (remove of indent from content):
		new MarkupParser(new CommonmarkLanguage(), new NoOpDocumentBuilder() {
			
			@Override
			public void characters(final String text) {
				assertEquals("Heading Text", text);
			}
			
		}).parse("   Heading Text\n-------");
	}
	
	@Test
	public void trimContent() {
		assertContent("<h2 id=\"one-two\">One Two</h2>", " One Two \t\n---",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
		assertContent("<h2 id=\"one-two-three\">One Two Three</h2>", "  \nOne Two \n   Three\t\n---",
				CommonmarkLanguage.MYLYN_COMPAT_MODE );
	}
	
}
