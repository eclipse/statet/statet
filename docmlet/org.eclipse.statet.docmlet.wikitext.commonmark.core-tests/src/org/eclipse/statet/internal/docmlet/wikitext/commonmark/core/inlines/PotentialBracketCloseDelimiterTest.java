/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.TextSegment;


@NonNullByDefault
public class PotentialBracketCloseDelimiterTest {
	
	private final Line line= new Line(0, 1, 0, "test", "\n");
	
	private final PotentialBracketRegex shared= new PotentialBracketRegex();
	
	
	public PotentialBracketCloseDelimiterTest() {
	}
	
	
	@Test
	public void linkEndPattern() {
		final Matcher matcher= this.shared.getEndMatcher();
		matcher.reset(" \"a title\")");
		assertTrue(matcher.matches());
		assertEquals("\"a title\"", matcher.group(1));
		
		matcher.reset(" (one two (three (four\\)\\)))");
		assertTrue(matcher.matches());
		assertEquals("(one two (three (four\\)\\))", matcher.group(1));
	}
	
	@Test
	public void eligibleForReferenceDefinitionImage() {
		final List<Inline> inlines= new ArrayList<>();
		final PotentialBracketOpenDelimiter openingDelimiter= new PotentialBracketOpenDelimiter(this.line, 0, 2, "![");
		inlines.add(openingDelimiter);
		final PotentialBracketCloseDelimiter delimiter= new PotentialBracketCloseDelimiter(this.line, 4, this.shared);
		inlines.add(delimiter);
		assertFalse(delimiter.isEligibleForReferenceDefinition(inlines, openingDelimiter, 0));
	}
	
	@Test
	public void eligibleForReferenceDefinitionLinkStartOfLine() {
		assertTrue(checkIsEligibleForReferenceDefinition("[", 0));
		assertTrue(checkIsEligibleForReferenceDefinition("a\n[", 2));
	}
	
	@Test
	public void eligibleForReferenceDefinitionLinkIndented() {
		assertTrue(checkIsEligibleForReferenceDefinition("a\n [", 3));
		assertTrue(checkIsEligibleForReferenceDefinition("a\n   [", 5));
		assertFalse(checkIsEligibleForReferenceDefinition("a\n    [", 6));
		assertFalse(checkIsEligibleForReferenceDefinition("    [", 4));
		assertTrue(checkIsEligibleForReferenceDefinition("   [", 3));
		assertFalse(checkIsEligibleForReferenceDefinition("a  [", 3));
	}
	
	private boolean checkIsEligibleForReferenceDefinition(final String content, final int offset) {
		final TextSegment textSegment= new TextSegment(LineSequence.create(content));
		final Line line= textSegment.getLineAtOffset(offset);
		final List<Inline> inlines= new ArrayList<>();
		final PotentialBracketOpenDelimiter openingDelimiter= new PotentialBracketOpenDelimiter(line, offset, 1, "[");
		inlines.add(openingDelimiter);
		final PotentialBracketCloseDelimiter delimiter= new PotentialBracketCloseDelimiter(line, 10, this.shared);
		inlines.add(delimiter);
		return delimiter.isEligibleForReferenceDefinition(inlines, openingDelimiter, 0);
	}
	
}
