/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.assertContent;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.createLineList;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkAsserts.toContent;

import java.util.List;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;


@NonNullByDefault
public class ListBlockTest {
	
	
	public ListBlockTest() {
	}
	
	
	@Test
	public void canStartBulleted() {
		assertCanStart(true, "-");
		assertCanStart(true, "- ");
		assertCanStart(true, "- test");
		assertCanStart(true, " - test");
		assertCanStart(true, "  - test");
		assertCanStart(true, "   - test");
		assertCanStart(false, "    - test");
		assertCanStart(false, "\t- test");
		assertCanStart(true, "* test");
		assertCanStart(true, "+ test");
		assertCanStart(false, "x test");
	}
	
	@Test
	public void canStartOrdered() {
		assertCanStart(true, "1.");
		assertCanStart(true, "1. ");
		assertCanStart(true, "1. test");
		assertCanStart(true, " 1. test");
		assertCanStart(true, "  1. test");
		assertCanStart(true, "   1. test");
		assertCanStart(false, "    1. test");
		assertCanStart(false, "\t1. test");
		assertCanStart(true, "2. test");
		assertCanStart(true, "3. test");
		assertCanStart(true, "1) test");
		assertCanStart(true, "2) test");
		assertCanStart(true, "23) test");
		assertCanStart(true, "23. test");
		assertCanStart(true, "0. test");
		assertCanStart(true, "001. test");
		assertCanStart(true, "123456789. test");
		assertCanStart(false, "1234567890. test");
		assertCanStart(false, "a. test");
	}
	
	@Test
	public void simpleList() {
		assertSimpleBulletedList("*");
		assertSimpleBulletedList("-");
		assertSimpleBulletedList("+");
	}
	
	@Test
	public void listWithNestedBlocks() {
		assertBulletedListWithNestedBlocks("*");
		assertBulletedListWithNestedBlocks("-");
		assertBulletedListWithNestedBlocks("+");
	}
	
	@Test
	public void tightListWithSublist() {
		final List<String> lines= createLineList(
				"* one",
				"* two",
				"    * three",
				"* four" );
		assertContent("<ul><li>one</li><li>two<ul><li>three</li></ul></li><li>four</li></ul>",
				toContent(lines) );
	}
	
	@Test
	public void simpleOrderedList() {
		assertSimpleOrderedList(".");
		assertSimpleOrderedList(")");
	}
	
	@Test
	public void simpleOrderedListWithNestedBlocks() {
		assertOrderedListWithNestedBlocks(".");
		assertOrderedListWithNestedBlocks(")");
	}
	
	@Test
	public void listWithFencedCodeBlock() {
		assertContent("<ul><li><pre><code>a\n\n\nb\n</code></pre></li></ul>", "* ```\n  a\n\n\n  b\n  ```");
	}
	
	
	@Test
	public void orderedListWithStart() {
		final List<String> lines= createLineList(
				"3. one",
				"4. two" );
		assertContent("<ol start=\"3\"><li>one</li><li>two</li></ol>",
				toContent(lines) );
	}
	
	@Test
	public void doubleBlankLineStartingOnListItem() {
		final List<String> lines= createLineList(
				"* one",
				"*",
				"",
				"* three" );
		assertContent("<ul><li><p>one</p></li><li></li><li><p>three</p></li></ul>",
				toContent(lines) );
	}
	
	@Test
	public void start_UnorderedList_beginWithBlankLine() {
		final List<String> lines= createLineList(
				"*      ",
				"  one",
				"     two" );
		assertContent("<ul><li>one two</li></ul>",
				toContent(lines) );
	}
	
	@Test
	public void start_OrderedList_beginWithBlankLine() {
		final List<String> lines= createLineList(
				"1.     ",
				"   one",
				"     two" );
		assertContent("<ol><li>one two</li></ol>",
				toContent(lines) );
	}
	
	@Test
	public void start_interruptingParagraph_UnorderedList() {
		final List<String> lines= createLineList(
				"first para",
				" - list item" );
		assertContent("<p>first para</p><ul><li>list item</li></ul>",
				toContent(lines) );
	}
	
	@Test
	public void start_OrderedList_interruptingParagraph() {
		final List<String> lines= createLineList(
				"first para",
				"1. list item" );
		assertContent("<p>first para</p><ol><li>list item</li></ol>",
				toContent(lines) );
	}
	
	@Test
	public void noStart_OrderedList_interruptingParagraph_beginWithBlankLine() {
		final List<String> lines= createLineList(
				"first para",
				"1. ",
				"third line" );
		assertContent("<p>first para 1. third line</p>",
				toContent(lines) );
	}
	
	@Test
	public void noStart_UnorderedList_interruptingParagraph_beginWithBlankLine() {
		final List<String> lines= createLineList(
				"first para",
				" * ",
				"third line" );
		assertContent("<p>first para * third line</p>",
				toContent(lines) );
	}
	
	@Test
	public void noStart_OrderedList_interruptingParagraph_beginWithNot1() {
		final List<String> lines= createLineList(
				"first para",
				"2. line" );
		assertContent("<p>first para 2. line</p>",
				toContent(lines) );
	}
	
	@Test
	public void start_OrderedList_interruptingList_beginWithNot1() {
		final List<String> lines= createLineList(
				"1. first list",
				"2) second list" );
		assertContent("<ol><li>first list</li></ol><ol start=\"2\"><li>second list</li></ol>",
				toContent(lines) );
	}
	
	
	private void assertSimpleOrderedList(final String delimiter) {
		final List<String> lines= createLineList(
				"1" + delimiter + " one",
				"2" + delimiter + " two",
				"3" + delimiter + " three four" );
		assertContent("<ol><li>one</li><li>two</li><li>three four</li></ol>",
				toContent(lines) );
	}
	
	private void assertBulletedListWithNestedBlocks(final String delimiter) {
		final List<String> lines= createLineList(
				delimiter + " one",
				delimiter + " two\n  two.2\n\n  two.3",
				delimiter + " three four" );
		assertContent("<ul><li><p>one</p></li><li><p>two two.2</p><p>two.3</p></li><li><p>three four</p></li></ul>",
				toContent(lines) );
	}
	
	private void assertOrderedListWithNestedBlocks(final String delimiter) {
		final List<String> lines= createLineList(
				"1" + delimiter + " one",
				"2" + delimiter + " two\n   two.2\n\n   two.3",
				"3" + delimiter + " three four" );
		assertContent("<ol><li><p>one</p></li><li><p>two two.2</p><p>two.3</p></li><li><p>three four</p></li></ol>",
				toContent(lines) );
	}
	
	private void assertSimpleBulletedList(final String delimiter) {
		final List<String> lines= createLineList(
				delimiter + " one",
				delimiter + " two",
				delimiter + " three four" );
		assertContent("<ul><li>one</li><li>two</li><li>three four</li></ul>",
				toContent(lines) );
	}
	
	private void assertCanStart(final boolean expected, final String string) {
		assertEquals(expected, new ListBlock().canStart(LineSequence.create(string), null));
	}
	
}
