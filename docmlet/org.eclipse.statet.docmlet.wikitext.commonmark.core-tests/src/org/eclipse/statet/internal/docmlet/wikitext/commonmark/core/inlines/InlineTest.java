/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class InlineTest {
	
	
	public InlineTest() {
	}
	
	
	@Test
	public void create() {
		final Line line= new Line(3, 5, 0, "text", "\n");
		final Inline inline= new Inline(line, 8, 3, 3) {
			
			@Override
			public void emit(final ProcessingContext context, final CommonmarkLocator locator, final DocumentBuilder builder) {
			}
			
		};
		assertSame(line, inline.getLine());
		assertEquals(8, inline.getStartOffset());
		assertEquals(3, inline.getLength());
		
		final CommonmarkLocator locator= new CommonmarkLocator();
		locator.setInline(inline);
		assertEquals(3, locator.getLineCharacterOffset());
		assertEquals(6, locator.getLineSegmentEndOffset());
		assertEquals(8, locator.getDocumentOffset());
		assertEquals(line.getStartOffset(), locator.getLineDocumentOffset());
		assertEquals(line.getText().length(), locator.getLineLength());
		assertEquals(line.getLineNumber() + 1, locator.getLineNumber());
	}
	
}
