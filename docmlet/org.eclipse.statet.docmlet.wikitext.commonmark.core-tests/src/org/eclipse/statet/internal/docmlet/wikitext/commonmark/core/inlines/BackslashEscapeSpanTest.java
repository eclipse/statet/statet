/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;


@NonNullByDefault
public class BackslashEscapeSpanTest extends AbstractSourceSpanTest {
	
	
	public BackslashEscapeSpanTest() {
		super(new BackslashEscapeSpan());
	}
	
	
	@Test
	public void backslashEscapedChar() {
		assertNoInline(Cursors.createCursor("\\"));
		assertNoInline(Cursors.createCursor("\\a"));
		assertEscapedCharacter('\\', 0, 2, Cursors.createCursor("\\\\"));
		assertEscapedCharacter('\\', 0, 2, Cursors.createCursor("\\\\\\"));
		assertEscapedCharacter('*', 0, 2, Cursors.createCursor("\\*"));
		assertEscapedCharacter('_', 0, 2, Cursors.createCursor("\\_*"));
	}
	
	@Test
	public void backslashEscapedLinebreak() {
		assertHardLineBreak(0, 2, Cursors.createCursor("\\\nabc"));
		assertHardLineBreak(0, 2, Cursors.createCursor("\\\rabc"));
		assertHardLineBreak(0, 3, Cursors.createCursor("\\\r\nabc"));
	}
	
	private void assertEscapedCharacter(final char ch, final int offset, final int length, final Cursor cursor) {
		final EscapedCharacter escapedCharacter= assertInline(EscapedCharacter.class, offset, length, cursor);
		assertEquals(ch, escapedCharacter.getCharacter());
	}
	
	private void assertHardLineBreak(final int offset, final int length, final Cursor cursor) {
		final HardLineBreak escapedCharacter= assertInline(HardLineBreak.class, offset, length, cursor);
		escapedCharacter.getType();
	}
	
}
