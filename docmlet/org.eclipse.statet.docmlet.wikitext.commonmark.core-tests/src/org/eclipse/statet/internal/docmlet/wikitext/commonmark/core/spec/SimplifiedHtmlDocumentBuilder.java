/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.spec;

import java.io.Writer;

import org.eclipse.mylyn.wikitext.parser.Attributes;
import org.eclipse.mylyn.wikitext.parser.ImageAttributes;
import org.eclipse.mylyn.wikitext.parser.builder.HtmlDocumentBuilder;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.util.HtmlUtils;


@NonNullByDefault
public class SimplifiedHtmlDocumentBuilder extends HtmlDocumentBuilder {
	
	
	private boolean isResolveEntityReferencesEnabled;
	
	
	public SimplifiedHtmlDocumentBuilder(final Writer out) {
		super(out, false);
	}
	
	@Override
	public void image(final Attributes attributes, final @Nullable String url) {
		this.writer.writeEmptyElement(getHtmlNsUri(), "img"); //$NON-NLS-1$
		this.writer.writeAttribute("src", makeUrlAbsolute(url)); //$NON-NLS-1$
		if (attributes instanceof final ImageAttributes imageAttributes) {
			{	final String alt= imageAttributes.getAlt();
				this.writer.writeAttribute(getHtmlNsUri(), "alt", (alt != null) ? alt : "");
			}
			if (imageAttributes.getTitle() != null) {
				this.writer.writeAttribute(getHtmlNsUri(), "title", imageAttributes.getTitle());
			}
		}
	}
	
	
	public void setResolveEntityReferences(final boolean enable) {
		this.isResolveEntityReferencesEnabled= enable;
	}
	
	@Override
	public void entityReference(final String entity) {
		if (this.isResolveEntityReferencesEnabled) {
			try {
				final String resolved= HtmlUtils.resolveEntity(entity);
				if (resolved != null) {
					characters(resolved);
					return;
				}
			}
			catch (final IllegalArgumentException e) {
			}
		}
		super.entityReference(entity);
	}
	
}
