/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Cursors.createCursor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class HtmlEntitySpanTest extends AbstractSourceSpanTest {
	
	
	public HtmlEntitySpanTest() {
		super(new HtmlEntitySpan());
	}
	
	
	@Test
	public void createInline() {
		assertNoInline(createCursor("one"));
		assertNoInline(createCursor("&copy"));
		assertEntity(6, "&copy;", "&copy; ayyy");
		assertEntity(5, "&xa0;", "&xa0; ayyy;");
		assertEntity(6, "&#160;", "&#160;");
		assertEntity(6, "&nbsp;", "&nbsp;");
		assertEntity(6, "&nbsp;", "&nbsp; ab\ncd");
		assertEntity(5, "&#x9;", "&#x9;");
		assertEntity(5, "&#X9;", "&#X9;");
		assertEntity(7, "&#x912;", "&#x912;");
		assertEntity(4, "\uFFFD", "&#0;");
		assertEntity(5, "\uFFFD", "&#00;");
		assertEntity(8, "&#65536;", "&#65536;");
		assertEntity(5, "\uFFFD", "&#x0;");
		assertEntity(9, "&#xfffff;", "&#xfffff;");
	}
	
	private void assertEntity(final int length, final String entity, final String content) {
		assertInline(HtmlEntity.class, 0, length, entity, createCursor(content));
	}
	
}
