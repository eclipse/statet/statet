/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Cursors.createCursor;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class HtmlTagSpanTest extends AbstractSourceSpanTest {
	
	
	public HtmlTagSpanTest() {
		super(new HtmlTagSpan());
	}
	
	
	@Test
	public void htmlTags() {
		assertNoInline(createCursor("<one"));
		assertNoInline(createCursor("< one>"));
		assertNoInline(createCursor("<0one>"));
		assertNoInline(createCursor("<!-- -- -->"));
		assertNoInline(createCursor("<!--->"));
		assertNoInline(createCursor("<!--> -->"));
		assertNoInline(createCursor("<!---> -->"));
		assertInline(HtmlTag.class, 0, 5, createCursor("<one>"));
		assertInline(HtmlTag.class, 0, 5, createCursor("<one> two"));
		assertInline(HtmlTag.class, 0, 22, createCursor("<onetwo three=\"four\"/>"));
		assertInline(HtmlTag.class, 0, 25, createCursor("<onetwo three = 'four' />"));
		assertInline(HtmlTag.class, 0, 33, createCursor("<onetwo three = 'four' selected/>"));
		assertInline(HtmlTag.class, 0, 33, createCursor("<onetwo three = 'four' selected/>"));
		assertInline(HtmlTag.class, 0, 4, createCursor("<a/><b2/>"));
		assertInline(HtmlTag.class, 4, 5, createCursor("<a/><b2/>", 4));
		assertInline(HtmlTag.class, 0, 15, createCursor("<a foo=\"bar\" />"));
		assertInline(HtmlTag.class, 0, 27, createCursor("<a bam = 'baz <em>\"</em>'/>"));
		assertInline(HtmlTag.class, 0, 14, createCursor("<a \n_boolean/>"));
		assertInline(HtmlTag.class, 0, 17, createCursor("<a b=\"c\"\n d='e'/>"));
		assertInline(HtmlTag.class, 0, 20, createCursor("<a zoop:33=zoop:33/>"));
		assertInline(HtmlTag.class, 0, 4, createCursor("</a>"));
		assertInline(HtmlTag.class, 0, 11, createCursor("<!-- c> -->"));
		assertInline(HtmlTag.class, 0, 10, createCursor("<!-- - -->"));
		assertInline(HtmlTag.class, 0, 8, createCursor("<? pi ?>"));
		assertInline(HtmlTag.class, 0, 16, createCursor("<!DECL one two >"));
		assertInline(HtmlTag.class, 0, 12, createCursor("<![CDATA[]]>"));
		assertInline(HtmlTag.class, 0, 13, createCursor("<![CDATA[\n]]>"));
		assertInline(HtmlTag.class, 0, 17, createCursor("<![CDATA[<foo>]]>"));
	}
	
}
