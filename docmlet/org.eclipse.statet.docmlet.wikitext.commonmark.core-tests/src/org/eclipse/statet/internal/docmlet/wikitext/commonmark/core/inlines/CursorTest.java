/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.TextSegment;


@NonNullByDefault
public class CursorTest {
	
	
	public CursorTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void requiresTextSegment() {
		Assertions.assertThrows(NullPointerException.class,
				() -> new Cursor(null) );
	}
	
	@Test
	public void empty() {
		final Cursor cursor= new Cursor(createTextSegment(""));
		assertFalse(cursor.hasChar());
		assertEquals(0, cursor.getOffset());
		cursor.advance();
		assertFalse(cursor.hasChar());
		assertEquals(0, cursor.getOffset());
	}
	
	@Test
	public void withContent() {
		final Cursor cursor= new Cursor(createTextSegment("1\n23"));
		assertTrue(cursor.hasChar());
		assertEquals(0, cursor.getOffset());
		assertEquals('1', cursor.getChar());
		assertEquals('1', cursor.getChar(0));
		cursor.advance();
		assertTrue(cursor.hasChar());
		assertEquals(1, cursor.getOffset());
		assertEquals('\n', cursor.getChar());
		assertEquals('1', cursor.getChar(0));
		cursor.advance();
		assertTrue(cursor.hasChar());
		assertEquals(2, cursor.getOffset());
		assertEquals('2', cursor.getChar());
		cursor.advance();
		assertTrue(cursor.hasChar());
		assertEquals(3, cursor.getOffset());
		assertEquals('3', cursor.getChar());
		assertEquals('3', cursor.getChar(3));
		cursor.advance();
		assertFalse(cursor.hasChar());
		assertEquals(4, cursor.getOffset());
	}
	
	
	@Test
	public void advance() {
		final Cursor cursor= new Cursor(createTextSegment("1\n23"));
		cursor.advance(2);
		assertTrue(cursor.hasChar());
		assertEquals(2, cursor.getOffset());
		assertEquals('2', cursor.getChar());
	}
	
	@Test
	public void rewind() {
		final Cursor cursor= new Cursor(createTextSegment("1\n23"));
		cursor.advance(2);
		assertEquals('2', cursor.getChar());
		cursor.rewind();
		assertEquals('\n', cursor.getChar());
		cursor.rewind();
		assertEquals('1', cursor.getChar());
		cursor.advance(3);
		assertEquals('3', cursor.getChar());
		cursor.rewind(3);
		assertEquals('1', cursor.getChar());
	}
	
	
	@Test
	public void getTextAtOffset() {
		final Cursor cursor= new Cursor(createTextSegment("1\n23"));
		assertEquals("1\n23", cursor.getTextAtOffset());
		assertEquals("1\n2", cursor.getTextAtOffset(0, 3));
		cursor.advance(2);
		assertEquals("23", cursor.getTextAtOffset());
		assertEquals("23", cursor.getTextAtOffset(0, 2));
		assertEquals("2", cursor.getTextAtOffset(0, 1));
		assertEquals("3", cursor.getTextAtOffset(1, 2));
	}
	
	@Test
	public void getNext() {
		final Cursor cursor= new Cursor(createTextSegment("123"));
		assertTrue(cursor.hasNext());
		assertTrue(cursor.hasNext(1));
		assertEquals('2', cursor.getNext());
		assertTrue(cursor.hasNext(2));
		assertEquals('3', cursor.getNext(2));
		assertFalse(cursor.hasNext(3));
		cursor.advance();
		assertTrue(cursor.hasNext());
		assertEquals('3', cursor.getNext());
		assertFalse(cursor.hasNext(2));
		cursor.advance();
		assertFalse(cursor.hasNext());
		assertFalse(cursor.hasNext(1));
	}
	
	@Test
	public void getPrevious() {
		final Cursor cursor= new Cursor(createTextSegment("123"));
		assertFalse(cursor.hasPrevious());
		assertFalse(cursor.hasPrevious(2));
		cursor.advance();
		assertTrue(cursor.hasPrevious());
		assertTrue(cursor.hasPrevious(1));
		assertFalse(cursor.hasPrevious(2));
		assertEquals('1', cursor.getPrevious());
		assertEquals('1', cursor.getPrevious(1));
		cursor.advance();
		assertTrue(cursor.hasPrevious());
		assertTrue(cursor.hasPrevious(1));
		assertTrue(cursor.hasPrevious(2));
		assertFalse(cursor.hasPrevious(3));
		assertEquals('2', cursor.getPrevious());
		assertEquals('2', cursor.getPrevious(1));
		assertEquals('1', cursor.getPrevious(2));
		cursor.advance();
		assertEquals('3', cursor.getPrevious());
		cursor.advance();
		assertEquals('3', cursor.getPrevious());
	}
	
	@Test
	public void matcher() {
		final Cursor cursor= new Cursor(createTextSegment("123"));
		Matcher matcher= cursor.setup(Pattern.compile("123").matcher(""));
		assertNotNull(matcher);
		assertTrue(matcher.matches());
		
		matcher= cursor.setup(Pattern.compile("3").matcher(""), 2);
		assertNotNull(matcher);
		assertTrue(matcher.matches());
	}
	
	@Test
	public void getOffset() {
		Cursor cursor= new Cursor(createTextSegment("one\r\ntwo"));
		assertEquals(0, cursor.getOffset(0));
		assertEquals(0, cursor.getOffset());
		assertEquals(1, cursor.getOffset(1));
		assertEquals(5, cursor.getOffset(4));
		
		cursor.advance(2);
		assertEquals(3, cursor.getOffset(1));
		
		cursor.advance(2);
		assertEquals(5, cursor.getOffset());
		
		cursor= new Cursor(new TextSegment(ImCollections.newList(new Line(1, 10, 0, "abc", "\n"))));
		assertEquals(12, cursor.getOffset(2));
	}
	
	@Test
	public void toCursorOffset() {
		final Cursor cursor= new Cursor(new TextSegment(ImCollections.newList(new Line(1, 10, 0, "abc", "\n"))));
		assertEquals(0, cursor.toCursorOffset(10));
		assertEquals(2, cursor.toCursorOffset(12));
		assertThrows(IllegalArgumentException.class,
				() -> cursor.toCursorOffset(9) );
	}
	
	@Test
	public void getText() {
		assertGetText("1\n23", "01\n23", 1, 5);
		assertGetText("\n2", "01\n23", 2, 4);
		assertGetText("1\n2", "01\n23", 1, 4);
		assertGetText("1\n2", "01\r\n23", 1, 5);
	}
	
	private void assertGetText(final String expected, final String document, final int documentStart, final int documentEnd) {
		final Cursor cursor= new Cursor(createTextSegment(document));
		final int start= cursor.toCursorOffset(documentStart);
		final int end= cursor.toCursorOffset(documentEnd);
		assertEquals(expected, cursor.getText(start, end));
	}
	
	private TextSegment createTextSegment(final String content) {
		return new TextSegment(LineSequence.create(content));
	}
	
}
