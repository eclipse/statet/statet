/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.eclipse.mylyn.wikitext.parser.Attributes;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;
import org.eclipse.mylyn.wikitext.parser.builder.EventDocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.builder.event.BeginBlockEvent;
import org.eclipse.mylyn.wikitext.parser.builder.event.CharactersEvent;
import org.eclipse.mylyn.wikitext.parser.builder.event.DocumentBuilderEvent;
import org.eclipse.mylyn.wikitext.parser.builder.event.EndBlockEvent;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.EmptyBlock;


@NonNullByDefault
public class SourceBlocksTest {
	
	
	private final SourceBlockType<?> block1= mockBlock(BlockType.QUOTE, "b1");
	
	private final SourceBlockType<?> block2= mockBlock(BlockType.PARAGRAPH, "b2");
	
	private final SourceBlocks sourceBlocks= new SourceBlocks(ImCollections.newList(
			this.block1, this.block2, new EmptyBlock() ));
	
	
	public SourceBlocksTest() {
	}
	
	
	@Test
	@SuppressWarnings("null")
	public void requiresBlocks() {
		assertThrows(NullPointerException.class, () ->
				new SourceBlocks(null) );
	}
	
	@Test
	public void process() {
		final EventDocumentBuilder builder= new EventDocumentBuilder();
		final ProcessingContext context= CommonmarkAsserts.newContext();
		final List<SourceBlockNode<?>> nodes= this.sourceBlocks.createNodes(
				LineSequence.create("\nb2\nmore\n\nb1 and\n\n\nb2"), context.getHelper() );
		this.sourceBlocks.emit(context, nodes, builder);
		final ImList<DocumentBuilderEvent> expectedEvents= ImCollections.newList(
				new BeginBlockEvent(BlockType.PARAGRAPH, new Attributes()),
				new CharactersEvent("b2"),
				new CharactersEvent("more"),
				new EndBlockEvent(),
				new BeginBlockEvent(BlockType.QUOTE, new Attributes()),
				new CharactersEvent("b1 and"),
				new EndBlockEvent(),
				new BeginBlockEvent(BlockType.PARAGRAPH, new Attributes()),
				new CharactersEvent("b2"),
				new EndBlockEvent() );
		assertArrayEquals(
				expectedEvents.toArray(), builder.getDocumentBuilderEvents().getEvents().toArray());
	}
	
	private SourceBlockType<?> mockBlock(final BlockType blockType, final String startString) {
		return new SourceBlockType<>() {
			
			@Override
			public boolean canStart(final LineSequence lineSequence,
					final @Nullable SourceBlockNode<?> currentNode) {
				final Line currentLine= lineSequence.getCurrentLine();
				return currentLine != null
						&& currentLine.getText().startsWith(startString);
			}
			
			@Override
			public void createNodes(final SourceBlockBuilder builder) {
				@SuppressWarnings("unused")
				final SourceBlockNode<?> node= new SourceBlockNode<>(this, builder);
				
				advanceNonBlankLines(builder.getLineSequence());
			}
			
			@Override
			public void initializeContext(final ProcessingContext context, final SourceBlockNode<?> node) {
			}
			
			@Override
			public void emit(final ProcessingContext context, final SourceBlockNode<?> node,
					final CommonmarkLocator locator, final DocumentBuilder builder) {
				final List<Line> lines= node.getLines();
				
				builder.beginBlock(blockType, new Attributes());
				
				for (final Line line : lines) {
					builder.characters(line.getText());
				}
				
				builder.endBlock();
			}
			
		};
	}
	
}
