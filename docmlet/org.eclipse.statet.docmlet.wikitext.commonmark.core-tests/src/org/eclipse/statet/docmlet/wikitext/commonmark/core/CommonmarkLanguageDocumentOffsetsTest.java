/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.commonmark.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.StringWriter;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.mylyn.wikitext.parser.Attributes;
import org.eclipse.mylyn.wikitext.parser.Locator;
import org.eclipse.mylyn.wikitext.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.parser.builder.HtmlDocumentBuilder;
import org.eclipse.mylyn.wikitext.util.LocatorImpl;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class CommonmarkLanguageDocumentOffsetsTest {
	
	
	public CommonmarkLanguageDocumentOffsetsTest() {
	}
	
	
	@Test
	public void blockQuoteWithBoldSpanUnix() {
		assertSpanOffsets(6, 15, "<blockquote><p>one <strong>two\nthree</strong> four</p></blockquote>",
				"> one **two\n> three** four");
	}
	
	@Test
	public void blockQuoteWithBoldSpanWindows() {
		assertSpanOffsets(6, 16, "<blockquote><p>one <strong>two\nthree</strong> four</p></blockquote>",
				"> one **two\r\n> three** four");
	}
	
	@Test
	public void blockQuoteWithBoldSpanMac() {
		assertSpanOffsets(6, 15, "<blockquote><p>one <strong>two\nthree</strong> four</p></blockquote>",
				"> one **two\r> three** four");
	}
	
	@Test
	public void blockQuoteWithNestedListAndEmphasis() {
		assertSpanOffsets(8, 16, "<blockquote><ul><li>one <em>two\nthree</em></li></ul></blockquote>",
				"> * one *two\r\n>   three*");
	}
	
	private void assertSpanOffsets(final int offset, final int length, final String expectedHtml, final String markup) {
		final AtomicReference<@Nullable Locator> spanLocator= new AtomicReference<>();
		final CommonmarkLanguage language= new CommonmarkLanguage();
		final MarkupParser parser= new MarkupParser(language);
		final StringWriter out= new StringWriter();
		final HtmlDocumentBuilder builder= new HtmlDocumentBuilder(out) {
			
			@Override
			public void beginSpan(final SpanType type, final Attributes attributes) {
				assertNull(spanLocator.get());
				spanLocator.set(new LocatorImpl(getLocator()));
				super.beginSpan(type, attributes);
			}
			
		};
		builder.setEmitAsDocument(false);
		parser.setBuilder(builder);
		parser.parse(markup);
		
		assertEquals(expectedHtml, out.toString());
		
		final Locator locator= spanLocator.get();
		assertNotNull(locator);
		assertEquals(offset, locator.getDocumentOffset());
		final int actualLength= locator.getLineSegmentEndOffset() - locator.getLineCharacterOffset();
		assertEquals(length, actualLength);
	}
	
}
