/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.commonmark.core;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.StringWriter;
import java.util.List;
import java.util.Objects;

import org.eclipse.core.runtime.Platform;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.IdGenerator;
import org.eclipse.mylyn.wikitext.parser.MarkupParser;
import org.eclipse.mylyn.wikitext.parser.builder.MultiplexingDocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.markup.IdGenerationStrategy;
import org.eclipse.mylyn.wikitext.parser.markup.MarkupLanguage;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.WikitextProblemReporter;
import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupConfig;
import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupParser2;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageExtension2;
import org.eclipse.statet.docmlet.wikitext.core.source.MarkupEventPrinter;
import org.eclipse.statet.docmlet.wikitext.core.source.MarkupSourceFormatAdapter;
import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.ExtdocMarkupLanguage;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Commonmark;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkIdGenerationStrategy;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ContentLineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.NullIdGenerator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.InlineParser;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.source.CommonmarkSourceFormatAdapter;
import org.eclipse.statet.ltk.core.source.SourceContent;


@NonNullByDefault
public class CommonmarkLanguage extends MarkupLanguage implements WikitextMarkupLanguage,
		WikitextMarkupLanguageExtension2, ExtdocMarkupLanguage {
	
	
	protected static final String COMMONMARK_LANGUAGE_NAME= "CommonMark\u2002[StatET]"; //$NON-NLS-1$
	
	
	public static final int MARKDOWN_COMPAT_MODE=            1 << 16;
	
	
	private static final boolean DEBUG_LOG_EVENTS= Boolean.parseBoolean(
			Platform.getDebugOption("org.eclipse.statet.docmlet.wikitext.commonmark/debug/Parser/logEvents") ); //$NON-NLS-1$
	
	
	private /*final*/ @Nullable String scope;
	
	private /*final*/ int mode;
	
	private @Nullable CommonmarkConfig config;
	
	private @Nullable SourceBlocks sourceBlocks;
	private @Nullable InlineParser inlineParser;
	
	private @Nullable WikitextProblemReporter validator;
	
	private @Nullable CommonmarkSourceFormatAdapter sourceAdapter;
	
	
	public CommonmarkLanguage() {
		this(null, 0, null);
	}
	
	public CommonmarkLanguage(final String scope, final int mode, final @Nullable MarkupConfig config) {
		this.scope= scope;
		this.mode= mode;
		setName(COMMONMARK_LANGUAGE_NAME);
		
		setMarkupConfig(config);
	}
	
	
	@Override
	public CommonmarkLanguage clone() {
		final CommonmarkLanguage clone= (CommonmarkLanguage)super.clone();
		clone.mode= this.mode;
		clone.config= this.config;
		return clone;
	}
	
	@Override
	public CommonmarkLanguage clone(final String scope, final int mode) {
		final CommonmarkLanguage clone= (CommonmarkLanguage)super.clone();
		clone.scope= scope;
		clone.mode= mode;
		clone.config= this.config;
		return clone;
	}
	
	
	@Override
	public @Nullable String getScope() {
		return this.scope;
	}
	
	
	@Override
	public int getMode() {
		return this.mode;
	}
	
	@Override
	public boolean isModeEnabled(final int modeMask) {
		return ((this.mode & modeMask) != 0);
	}
	
	
	@Override
	public void setMarkupConfig(final @Nullable MarkupConfig config) {
		if (config != null) {
			config.seal();
		}
		if (this.config != config) {
			this.config= (CommonmarkConfig)config;
			
			this.sourceBlocks= null;
			this.inlineParser= null;
		}
	}
	
	@Override
	public @Nullable CommonmarkConfig getMarkupConfig() {
		return this.config;
	}
	
	
	@Override
	public void processContent(final MarkupParser2 parser, final SourceContent content,
			final boolean asDocument) {
		if (parser == null) {
			throw new NullPointerException("parser"); //$NON-NLS-1$
		}
		if (content == null) {
			throw new NullPointerException("content"); //$NON-NLS-1$
		}
		if (parser.getBuilder() == null) {
			throw new NullPointerException("parser.builder"); //$NON-NLS-1$
		}
		
		if (DEBUG_LOG_EVENTS) {
			final StringWriter out= new StringWriter();
			try {
				final MarkupEventPrinter printer= new MarkupEventPrinter(content.getString(), this, out);
				final MarkupParser2 debugParser= new MarkupParser2(this,
						new MultiplexingDocumentBuilder(printer, parser.getBuilder()),
						parser.getFlags() );
				doProcessContent(debugParser, content, asDocument);
				System.out.println(out.toString());
			}
			catch (final Exception e) {
				System.out.println(out.toString());
				e.printStackTrace();
			}
		}
		else {
			doProcessContent(parser, content, asDocument);
		}
	}
	
	protected void doProcessContent(final MarkupParser2 parser, final SourceContent content,
			final boolean asDocument) {
		final DocumentBuilder builder= nonNullAssert(parser.getBuilder());
		final LineSequence lineSequence= new ContentLineSequence(content.getString(), content.getStringLines());
		
		final ProcessingContext context= createContext();
		
		if (asDocument) {
			builder.beginDocument();
		}
		
		if (parser.isEnabled(MarkupParser2.SOURCE_STRUCT)) {
			context.getSourceBlocks().parseSourceStruct(context, lineSequence, builder);
		}
		else {
			final List<SourceBlockNode<?>> nodes= context.getSourceBlocks().createNodes(lineSequence,
					context.getHelper() );
			context.getSourceBlocks().initializeContext(context, nodes);
			
			context.getSourceBlocks().emit(context, nodes, builder);
		}
		
		if (asDocument) {
			builder.endDocument();
		}
	}
	
	@Override
	public void processContent(final MarkupParser parser, final String markupContent, final boolean asDocument) {
		processContent(new MarkupParser2(parser), new SourceContent(0, markupContent), asDocument);
	}
	
	
	@Override
	public WikitextProblemReporter getProblemReporter() {
		WikitextProblemReporter validator= this.validator;
		if (validator == null) {
			validator= new WikitextProblemReporter();
			this.validator= validator;
		}
		return validator;
	}
	
	
	@Override
	public MarkupSourceFormatAdapter getSourceFormatAdapter() {
		CommonmarkSourceFormatAdapter sourceAdapter= this.sourceAdapter;
		if (sourceAdapter == null) {
			sourceAdapter= new CommonmarkSourceFormatAdapter();
			this.sourceAdapter= sourceAdapter;
		}
		return sourceAdapter;
	}
	
	
	@Override
	public @Nullable IdGenerationStrategy getIdGenerationStrategy() {
		return ((this.mode & MYLYN_COMPAT_MODE) != 0) ? new CommonmarkIdGenerationStrategy() : null;
	}
	
	private IdGenerator createIdGenerator() {
		final IdGenerationStrategy idGenerationStrategy= getIdGenerationStrategy();
		if (idGenerationStrategy != null) {
			final IdGenerator generator= new IdGenerator();
			generator.setGenerationStrategy(idGenerationStrategy);
			return generator;
		}
		return new NullIdGenerator();
	}
	
	private ProcessingContext createContext() {
		SourceBlocks sourceBlocks= this.sourceBlocks;
		if (sourceBlocks == null) {
			sourceBlocks= Commonmark.newSourceBlocks(this.config);
			this.sourceBlocks= sourceBlocks;
		}
		InlineParser inlineParser= this.inlineParser;
		if (inlineParser == null) {
			inlineParser= ((this.mode & MARKDOWN_COMPAT_MODE) != 0) ?
						Commonmark.newInlineParserMarkdown() :
						Commonmark.newInlineParserCommonMark(this.config);
			this.inlineParser= inlineParser;
		}
		
		final ProcessingContext context= new ProcessingContext(sourceBlocks, inlineParser,
				createIdGenerator(), ProcessingContext.INITIALIZE_CONTEXT );
		
		return context;
	}
	
	
	@Override
	public int hashCode() {
		return getName().hashCode() + this.mode;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj != null && obj.getClass() == getClass()) {
			final CommonmarkLanguage other= (CommonmarkLanguage)obj;
			
			return (getName().equals(other.getName())
					&& this.mode == other.mode
					&& Objects.equals(this.config, other.getMarkupConfig()) );
		}
		return false;
	}
	
}
