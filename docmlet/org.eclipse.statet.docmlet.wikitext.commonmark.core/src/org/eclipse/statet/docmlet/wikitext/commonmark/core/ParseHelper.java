/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.commonmark.core;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.HTML_ENTITY_PATTERN;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.HTML_ENTITY_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.isWhitespace;

import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.util.HtmlUtils;


@NonNullByDefault
public class ParseHelper {
	
	
	private static final Pattern ESCAPING_PATTERN= Pattern.compile("\\\\(.)" + "|" + HTML_ENTITY_REGEX); //$NON-NLS-1$
	
	
	private final Matcher escapingMatcher= ESCAPING_PATTERN.matcher(""); //$NON-NLS-1$
	private final Matcher htmlEntityMatcher= HTML_ENTITY_PATTERN.matcher(""); //$NON-NLS-1$
	
	private final StringBuilder tmpBuilder= new StringBuilder(0x40);
	
	
	public ParseHelper() {
	}
	
	
	public boolean isAsciiPunctuation(final char ch) {
		switch (ch) {
			case '!':
			case '"':
			case '#':
			case '$':
			case '%':
			case '&':
			case '\'':
			case '(':
			case ')':
			case '*':
			case '+':
			case ',':
			case '-':
			case '.':
			case '/':
			case ':':
			case ';':
			case '<':
			case '=':
			case '>':
			case '?':
			case '@':
			case '[':
			case '\\':
			case ']':
			case '^':
			case '_':
			case '`':
			case '{':
			case '|':
			case '}':
			case '~':
				return true;
			default:
				return false;
		}
	}
	
	public boolean isUnicodePunctuation(final char ch) {
		if (isAsciiPunctuation(ch)) {
			return true;
		}
		final int type= Character.getType(ch);
		switch (type) {
		case Character.DASH_PUNCTUATION:
		case Character.START_PUNCTUATION:
		case Character.END_PUNCTUATION:
		case Character.CONNECTOR_PUNCTUATION:
		case Character.OTHER_PUNCTUATION:
		case Character.INITIAL_QUOTE_PUNCTUATION:
		case Character.FINAL_QUOTE_PUNCTUATION:
			return true;
		default:
			return false;
		}
	}
	
	
	public Matcher getHtmlEntityMatcher() {
		return this.htmlEntityMatcher;
	}
	
	private Matcher getEscapingMatcher() {
		return this.escapingMatcher;
	}
	
	
	private StringBuilder getTmpBuilder() {
		this.tmpBuilder.setLength(0);
		return this.tmpBuilder;
	}
	
	
	public @Nullable String resolveHtmlEntity(final String reference) {
		try {
			final String replacement= HtmlUtils.resolveEntity(reference);
			if (replacement != null && replacement.charAt(0) == 0) {
				throw new IllegalArgumentException();
			}
			return replacement;
		}
		catch (final IllegalArgumentException e) {
			return "\uFFFD"; //$NON-NLS-1$
		}
	}
	
	public String replaceHtmlEntities(final String text, final @Nullable Function<String, @NonNull String> escaper) {
		final StringBuilder sb= getTmpBuilder();
		final Matcher matcher= getHtmlEntityMatcher().reset(text);
		int lastEnd= 0;
		while (matcher.find()) {
			try {
				final int start= matcher.start();
				final String reference= (@NonNull String)matcher.group(1);
				String replacement= resolveHtmlEntity(reference);
				if (replacement != null) {
					if (escaper != null) {
						replacement= escaper.apply(replacement);
					}
					if (lastEnd < start) {
						sb.append(text, lastEnd, start);
					}
					sb.append(replacement);
					lastEnd= matcher.end();
				}
			}
			catch (final IllegalArgumentException e) {
			}
		}
		
		if (lastEnd == 0) {
			return text;
		}
		
		if (lastEnd < text.length()) {
			sb.append(text, lastEnd, text.length());
		}
		return sb.toString();
	}
	
	public String replaceEscaping(final String text) {
		final StringBuilder sb= getTmpBuilder();
		final Matcher matcher= getEscapingMatcher().reset(text);
		int lastEnd= 0;
		while (matcher.find()) {
			final int start= matcher.start();
			{	final int escapedIdx= matcher.start(1);
				if (escapedIdx >= 0) {
					if (isAsciiPunctuation(text.charAt(escapedIdx))) {
						if (lastEnd < start) {
							sb.append(text, lastEnd, start);
						}
						lastEnd= escapedIdx;
					}
					continue;
				}
			}
			try {
				final String reference= (@NonNull String)matcher.group(2);
				final String replacement= resolveHtmlEntity(reference);
				if (replacement != null) {
					if (lastEnd < start) {
						sb.append(text, lastEnd, start);
					}
					sb.append(replacement);
					lastEnd= matcher.end();
				}
			}
			catch (final IllegalArgumentException e) {
				if (lastEnd < start) {
					sb.append(text, lastEnd, start);
				}
				sb.append('\uFFFD');
				lastEnd= matcher.end();
			}
		}
		
		if (lastEnd == 0) {
			return text;
		}
		
		if (lastEnd < text.length()) {
			sb.append(text, lastEnd, text.length());
		}
		return sb.toString();
	}
	
	
	public String trimWhitespace(final String s) {
		int startIndex= 0;
		int endIndex= s.length();
		while (startIndex < endIndex && isWhitespace(s.charAt(startIndex))) {
			startIndex++;
		}
		while (endIndex > startIndex && isWhitespace(s.charAt(endIndex - 1))) {
			endIndex--;
		}
		return (startIndex == 0 && endIndex == s.length()) ? s : s.substring(startIndex, endIndex);
	}
	
	public String collapseWhitespace(final String text) {
		final StringBuilder sb= getTmpBuilder();
		final int l= text.length();
		int index= 0;
		while (index < l && isWhitespace(text.charAt(index))) {
			index++;
		}
		int whitespaceEnd= index;
		while (index < l) {
			if (isWhitespace(text.charAt(index))) {
				final int wordEnd= index++;
				while (index < l && isWhitespace(text.charAt(index))) {
					index++;
				}
				if (sb.length() > 0) {
					sb.append(' ');
				}
				else if (index == l) {
					return text.substring(whitespaceEnd, wordEnd);
				}
				sb.append(text, whitespaceEnd, wordEnd);
				whitespaceEnd= index;
			}
			else {
				index++;
			}
		}
		if (whitespaceEnd == 0) {
			return text;
		}
		if (whitespaceEnd < text.length()) {
			if (sb.length() > 0) {
				sb.append(' ');
				sb.append(text, whitespaceEnd, text.length());
			}
			else {
				return text.substring(whitespaceEnd, text.length());
			}
		}
		return sb.toString();
	}
	
	public String joinLines(final List<String> list) {
		final int n= list.size();
		if (n <= 0) {
			return ""; //$NON-NLS-1$
		}
		else if (n == 1) {
			return list.get(0).toString();
		}
		else {
			final StringBuilder sb= getTmpBuilder();
			sb.append(list.get(0).toString());
			for (int i= 1; i < n; i++) {
				sb.append('\n');
				sb.append(list.get(i).toString());
			}
			return sb.toString();
		}
	}
	
}
