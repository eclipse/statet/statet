/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.commonmark.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.AbstractMarkupConfig;


@NonNullByDefault
public abstract class BasicCommonmarkConfig<T extends BasicCommonmarkConfig<? super T>> extends AbstractMarkupConfig<T>
		implements CommonmarkConfig {
	
	
	public static final String HEADER_INTERRUPT_PARAGRAPH_DISABLED_PROP= "HeaderInterruptParagraphDisabled"; //$NON-NLS-1$
	private static final String HEADER_INTERRUPT_PARAGRAPH_DISABLED_KEY= "Paragraph+Header=Blank"; //$NON-NLS-1$
	
	public static final String BLOCKQUOTE_INTERRUPT_PARAGRAPH_DISABLED_PROP= "BlockquoteInterruptParagraphDisabled"; //$NON-NLS-1$
	private static final String BLOCKQUOTE_INTERRUPT_PARAGRAPH_DISABLED_KEY= "Paragraph+Blockquote=Blank"; //$NON-NLS-1$
	
	public static final String STRIKEOUT_DTILDE_ENABLED_PROP= "StrikeoutByDTildeEnabled"; //$NON-NLS-1$
	private static final String STRIKEOUT_DTILDE_KEY= "Strikeout=~~"; //$NON-NLS-1$
	
	public static final String SUPERSCRIPT_SCIRCUMFLEX_ENABLED_PROP= "SuperscriptBySCircumflexEnabled"; //$NON-NLS-1$
	private static final String SUPERSCRIPT_SCIRCUMFLEX_KEY= "Superscript=^"; //$NON-NLS-1$
	
	public static final String SUBSCRIPT_STILDE_ENABLED_PROP= "SubscriptBySTildeEnabled"; //$NON-NLS-1$
	private static final String SUBSCRIPT_STILDE_KEY= "Subscript=~"; //$NON-NLS-1$
	
	
	private boolean isHeaderInterruptParagraphDisabled;
	
	private boolean isBlockquoteInterruptParagraphDisabled;
	
	private boolean isStrikeoutDTildeEnabled;
	private boolean isSuperscriptSCircumflexEnabled;
	private boolean isSubscriptSTildeEnabled;
	
	
	public BasicCommonmarkConfig() {
	}
	
	
	@Override
	protected String getConfigType() {
		return "Commonmark"; //$NON-NLS-1$
	}
	
	
	@Override
	public boolean isHeaderInterruptParagraphDisabled() {
		return this.isHeaderInterruptParagraphDisabled;
	}
	
	public void setHeaderInterruptParagraphDisabled(final boolean enabled) {
		checkSeal();
		if (this.isHeaderInterruptParagraphDisabled != enabled) {
			this.isHeaderInterruptParagraphDisabled= enabled;
			afterChange(HEADER_INTERRUPT_PARAGRAPH_DISABLED_PROP);
		}
	}
	
	@Override
	public boolean isBlockquoteInterruptParagraphDisabled() {
		return this.isBlockquoteInterruptParagraphDisabled;
	}
	
	public void setBlockquoteInterruptParagraphDisabled(final boolean enabled) {
		checkSeal();
		if (this.isBlockquoteInterruptParagraphDisabled != enabled) {
			this.isBlockquoteInterruptParagraphDisabled= enabled;
			afterChange(BLOCKQUOTE_INTERRUPT_PARAGRAPH_DISABLED_PROP);
		}
	}
	
	
	@Override
	public boolean isStrikeoutByDTildeEnabled() {
		return this.isStrikeoutDTildeEnabled;
	}
	
	public void setStrikeoutDTildeEnabled(final boolean enabled) {
		checkSeal();
		if (this.isStrikeoutDTildeEnabled != enabled) {
			this.isStrikeoutDTildeEnabled= enabled;
			afterChange(STRIKEOUT_DTILDE_ENABLED_PROP);
		}
	}
	
	@Override
	public boolean isSuperscriptBySCircumflexEnabled() {
		return this.isSuperscriptSCircumflexEnabled;
	}
	
	public void setSuperscriptSCircumflexEnabled(final boolean enabled) {
		checkSeal();
		if (this.isSuperscriptSCircumflexEnabled != enabled) {
			this.isSuperscriptSCircumflexEnabled= enabled;
			afterChange(SUPERSCRIPT_SCIRCUMFLEX_ENABLED_PROP);
		}
	}
	
	@Override
	public boolean isSubscriptBySTildeEnabled() {
		return this.isSubscriptSTildeEnabled;
	}
	
	public void setSubscriptSTildeEnabled(final boolean enabled) {
		checkSeal();
		if (this.isSubscriptSTildeEnabled != enabled) {
			this.isSubscriptSTildeEnabled= enabled;
			afterChange(SUBSCRIPT_STILDE_ENABLED_PROP);
		}
	}
	
	
	@Override
	protected void createConfigString(final StringBuilder sb) {
		super.createConfigString(sb);
		
		if (isHeaderInterruptParagraphDisabled()) {
			sb.append(HEADER_INTERRUPT_PARAGRAPH_DISABLED_KEY);
		}
		if (isBlockquoteInterruptParagraphDisabled()) {
			sb.append(BLOCKQUOTE_INTERRUPT_PARAGRAPH_DISABLED_KEY);
		}
		
		if (isStrikeoutByDTildeEnabled()) {
			sb.append(STRIKEOUT_DTILDE_KEY);
		}
		if (isSuperscriptBySCircumflexEnabled()) {
			sb.append(SUPERSCRIPT_SCIRCUMFLEX_KEY);                                                                                                       
		}
		if (isSubscriptBySTildeEnabled()) {
			sb.append(SUBSCRIPT_STILDE_KEY);
		}
	}
	
	@Override
	protected void load(final String configType, final List<String> s) {
		super.load(configType, s);
		
		setHeaderInterruptParagraphDisabled(s.contains(HEADER_INTERRUPT_PARAGRAPH_DISABLED_KEY));
		setBlockquoteInterruptParagraphDisabled(s.contains(BLOCKQUOTE_INTERRUPT_PARAGRAPH_DISABLED_KEY));
		
		setStrikeoutDTildeEnabled(s.contains(STRIKEOUT_DTILDE_KEY));
		setSuperscriptSCircumflexEnabled(s.contains(SUPERSCRIPT_SCIRCUMFLEX_KEY));
		setSubscriptSTildeEnabled(s.contains(SUBSCRIPT_STILDE_KEY));
	}
	
	@Override
	public void load(final T config) {
		super.load(config);
		
		setHeaderInterruptParagraphDisabled(config.isHeaderInterruptParagraphDisabled());
		setBlockquoteInterruptParagraphDisabled(config.isBlockquoteInterruptParagraphDisabled());
		
		setStrikeoutDTildeEnabled(config.isStrikeoutByDTildeEnabled());
		setSuperscriptSCircumflexEnabled(config.isSuperscriptBySCircumflexEnabled());
		setSubscriptSTildeEnabled(config.isSubscriptBySTildeEnabled());
	}
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final BasicCommonmarkConfig<?> other
						&& super.equals(obj)
						&& isHeaderInterruptParagraphDisabled() == other.isHeaderInterruptParagraphDisabled()
						&& isBlockquoteInterruptParagraphDisabled() == other.isBlockquoteInterruptParagraphDisabled()
						&& isStrikeoutByDTildeEnabled() == other.isStrikeoutByDTildeEnabled()
						&& isSuperscriptBySCircumflexEnabled() == other.isSuperscriptBySCircumflexEnabled()
						&& isSubscriptBySTildeEnabled() == other.isSubscriptBySTildeEnabled() ));
	}
	
}
