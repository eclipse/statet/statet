/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class AllCharactersSpan extends SourceSpan {
	
	
	public AllCharactersSpan() {
	}
	
	
	@Override
	public @Nullable Inline createInline(final ProcessingContext context, final Cursor cursor) {
		final int startOffset= cursor.getOffset();
		final int endOffset= cursor.getOffset(1);
		
		return new Characters(cursor.getLineAtOffset(), startOffset, endOffset - startOffset, 1,
				String.valueOf(cursor.getChar()) );
	}
	
}
