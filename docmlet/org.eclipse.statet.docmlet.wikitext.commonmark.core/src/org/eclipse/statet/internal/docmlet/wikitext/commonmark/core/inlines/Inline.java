/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;
import java.util.Objects;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public abstract class Inline implements TextRegion {
	
	
	private final Line line;
	
	private final int startOffset;
	
	private final int length;
	
	private final int cursorLength;
	
	
	public Inline(final Line line, final int offset, final int length, final int cursorLength) {
		if (offset < 0 || length <= 0) {
			throw new IllegalArgumentException();
		}
		this.line= nonNullAssert(line);
		this.startOffset= offset;
		this.length= length;
		this.cursorLength= cursorLength;
	}
	
	
	@Override
	public int getStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public int getEndOffset() {
		return this.startOffset + this.length;
	}
	
	@Override
	public int getLength() {
		return this.length;
	}
	
	public Line getLine() {
		return this.line;
	}
	
	
	protected int getCursorLength() {
		return this.cursorLength;
	}
	
	void apply(final ProcessingContext context, final List<Inline> inlines,
			final Cursor cursor, final boolean inBlock) {
		cursor.advance(getCursorLength());
		inlines.add(this);
	}
	
	@Nullable InlinesSubstitution secondPass(final List<Inline> inlines) {
		return null;
	}
	
	public abstract void emit(final ProcessingContext context,
			final CommonmarkLocator locator, final DocumentBuilder builder);
	
	
	@Override
	public int hashCode() {
		return Objects.hash(this.startOffset, this.length);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj != null && getClass() == obj.getClass()) {
			final Inline other= (Inline)obj;
			return (this.startOffset == other.startOffset
					&& this.length == other.length);
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(getClass(), getClass());
		sb.addProp("startOffset", getStartOffset()); //$NON-NLS-1$
		sb.addProp("length", getLength()); //$NON-NLS-1$
		return sb.build();
	}
	
}
