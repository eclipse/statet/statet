/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.mylyn.wikitext.parser.IdGenerator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.ParseHelper;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.InlineParser;


@NonNullByDefault
public class ProcessingContext {
	
	
	public static final int INITIALIZE_CONTEXT= 1;
	public static final int EMIT_DOCUMENT= 2;
	public static final int PARSE_SOURCE_STRUCT= 3;
	
	
	public static class ReferenceDef {
		
		
		private final String label;
		
		private final String uri;
		
		private final @Nullable String title;
		
		
		public ReferenceDef(final String label, final String uri, final @Nullable String title) {
			this.label= label;
			this.uri= uri;
			this.title= title;
		}
		
		
		/**
		 * Returns the normalized label.
		 * 
		 * @return the normalized label
		 */
		public String getLabel() {
			return this.label;
		}
		
		
		public String getUri() {
			return this.uri;
		}
		
		public @Nullable String getTitle() {
			return this.title;
		}
		
	}
	
	
	private int mode;
	
	private final SourceBlocks sourceBlocks;
	private final InlineParser inlineParser;
	
	private final Map<String, ReferenceDef> referenceDefs;
	
	private final IdGenerator idGenerator;
	
	private @Nullable ParseHelper parseHelper;
	
	
	public ProcessingContext(final SourceBlocks sourceBlocks, final InlineParser inlineParser,
			final IdGenerator idGenerator, final int initialMode) {
		this.sourceBlocks= sourceBlocks;
		this.inlineParser= inlineParser;
		this.referenceDefs= new HashMap<>();
		this.idGenerator= idGenerator;
		
		this.mode= initialMode;
	}
	
	
	public int getMode() {
		return this.mode;
	}
	
	public void setMode(final int mode) {
		this.mode= mode;
	}
	
	
	public @Nullable String normalizeLabel(@Nullable String label) {
		if (label == null || label.isEmpty()) {
			return null;
		}
		label= getHelper().collapseWhitespace(label);
		if (label.isEmpty()) {
			return null;
		}
		return label.toLowerCase(Locale.ROOT).toUpperCase(Locale.ROOT).toLowerCase();
	}
	
	public boolean hasReferenceDefs() {
		return !this.referenceDefs.isEmpty();
	}
	
	/**
	 * Registers a reference definition in the context.
	 * 
	 * @param label the normalized label
	 * @param href the defined target uri
	 * @param title the optional title
	 */
	public void addReferenceDef(final String label, final String href,
			final @Nullable String title) {
		if (this.mode > INITIALIZE_CONTEXT) {
			throw new IllegalStateException("" + this.mode);
		}
		if (label != null && !label.isEmpty()
				&& !this.referenceDefs.containsKey(label)) {
			this.referenceDefs.put(label, new ReferenceDef(label, href, title));
		}
	}
	
	/**
	 * Returns the reference definition.
	 * 
	 * @param label normalized label
	 * @return the definition or {@code null}
	 */
	public @Nullable ReferenceDef getReferenceDef(final String label) {
		return this.referenceDefs.get(label);
	}
	
	
	public @Nullable String generateHeadingId(final int headingLevel, final @Nullable String headingText) {
		if (this.mode <= INITIALIZE_CONTEXT) {
			return "";
		}
		return this.idGenerator.newId("h" + headingLevel, headingText);
	}
	
	public SourceBlocks getSourceBlocks() {
		return this.sourceBlocks;
	}
	
	public InlineParser getInlineParser() {
		return this.inlineParser;
	}
	
	
	public ParseHelper getHelper() {
		ParseHelper helper= this.parseHelper;
		if (helper == null) {
			helper= new ParseHelper();
			this.parseHelper= helper;
		}
		return helper;
	}
	
}
