/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;


/**
 * CM Thematic Break (= Horizontal Rule)
 */
@NonNullByDefault
public class ThematicBreakBlock extends SourceBlockType<ThematicBreakBlock.ThematicBreakNode> {
	
	
	static final class ThematicBreakNode extends SourceBlockNode<ThematicBreakBlock> {
		
		private ThematicBreakNode(final ThematicBreakBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
	}
	
	
	private static final Pattern PATTERN= Pattern.compile(
			"(?:\\*[ \t]*){3,}|(?:-[ \t]*){3,}|(?:_[ \t]*){3,}",
			Pattern.DOTALL );
	
	
	private final Matcher matcher= PATTERN.matcher("");
	
	
	public ThematicBreakBlock() {
	}
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		return canStart(lineSequence.getCurrentLine());
	}
	
	public boolean canStart(final @Nullable Line currentLine) {
		return (currentLine != null
				&& !currentLine.isBlank() && currentLine.getIndent() < 4
				&& currentLine.setupIndent(this.matcher).matches() );
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		@SuppressWarnings("unused")
		final ThematicBreakNode node= new ThematicBreakNode(this, builder);
		
		builder.getLineSequence().advance();
	}
	
	@Override
	public void initializeContext(final ProcessingContext context, final ThematicBreakNode node) {
	}
	
	@Override
	public void emit(final ProcessingContext context, final ThematicBreakNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		locator.setBlockBegin(node);
		builder.horizontalRule();
	}
	
}
