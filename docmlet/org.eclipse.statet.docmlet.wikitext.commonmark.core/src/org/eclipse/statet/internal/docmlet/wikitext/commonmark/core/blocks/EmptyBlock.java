/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;


@NonNullByDefault
public class EmptyBlock extends SourceBlockType<EmptyBlock.EmptyBlockNode> {
	
	
	static final class EmptyBlockNode extends SourceBlockNode<EmptyBlock> {
		
		private EmptyBlockNode(final EmptyBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
		
		@Override
		public boolean isEmpty() {
			return true;
		}
		
	}
	
	
	public EmptyBlock() {
	}
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		final Line startLine= lineSequence.getCurrentLine();
		return (startLine != null
				&& startLine.isBlank() );
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		@SuppressWarnings("unused")
		final EmptyBlockNode node= new EmptyBlockNode(this, builder);
		
		advanceBlankLines(builder.getLineSequence());
	}
	
	@Override
	public void initializeContext(final ProcessingContext context, final EmptyBlockNode node) {
	}
	
	@Override
	public void emit(final ProcessingContext context, final EmptyBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
	}
	
}
