/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
class InlinesSubstitution {
	
	
	private final Inline first;
	
	private final Inline last;
	
	private final ImList<Inline> substitution;
	
	
	public InlinesSubstitution(final Inline first, final Inline last, final List<? extends Inline> substitution) {
		this.first= nonNullAssert(first);
		this.last= nonNullAssert(last);
		this.substitution= ImCollections.toList(substitution);
	}
	
	
	public List<Inline> apply(final List<Inline> inlines) {
		final List<Inline> builder= new ArrayList<>();
		
		boolean inReplacementSegment= false;
		for (final Inline inline : inlines) {
			if (inline == this.first) {
				inReplacementSegment= true;
				builder.addAll(this.substitution);
			}
			if (!inReplacementSegment) {
				builder.add(inline);
			}
			if (inReplacementSegment && inline == this.last) {
				inReplacementSegment= false;
			}
		}
		return builder;
	}
	
}
