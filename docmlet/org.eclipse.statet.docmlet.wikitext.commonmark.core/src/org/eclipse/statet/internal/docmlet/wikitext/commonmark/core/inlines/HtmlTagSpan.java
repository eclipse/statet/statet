/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.CDATA_1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.CLOSE_TAG_1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.COMMENT_1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.DECL_1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.OPEN_TAG_1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.PI_1_REGEX;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class HtmlTagSpan extends SourceSpan {
	
	
	private static final Pattern PATTERN= Pattern.compile("(<(?:" +
				COMMENT_1_REGEX +
				"|" + PI_1_REGEX +
				"|" + DECL_1_REGEX +
				"|" + CDATA_1_REGEX +
				"|" + OPEN_TAG_1_REGEX + "|" + CLOSE_TAG_1_REGEX +
			")).*",
			Pattern.DOTALL );
	
	
	private @Nullable Matcher matcher;
	
	
	public HtmlTagSpan() {
	}
	
	
	@Override
	public @Nullable Inline createInline(final ProcessingContext context, final Cursor cursor) {
		final char c= cursor.getChar();
		if (c == '<') {
			final Matcher matcher= cursor.setup(getMatcher());
			if (matcher.matches()) {
				final int startOffset= cursor.getOffset();
				final int endOffset= cursor.getMatcherOffset(matcher.end(1));
				
				return new HtmlTag(cursor.getLineAtOffset(), startOffset, endOffset - startOffset,
						matcher.group(1) );
			}
		}
		return null;
	}
	
	
	@SuppressWarnings("null")
	private Matcher getMatcher() {
		if (this.matcher == null) {
			this.matcher= PATTERN.matcher("");
		}
		return this.matcher;
	}
	
}
