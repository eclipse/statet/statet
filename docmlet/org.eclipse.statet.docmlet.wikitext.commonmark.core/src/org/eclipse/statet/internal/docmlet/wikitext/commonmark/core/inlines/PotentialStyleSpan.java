/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.isUnicodeWhitespace;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.PotentialStyleDelimiterInfo.FLANKING;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.PotentialStyleDelimiterInfo.FLANKING_UNDERSCORE;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.ParseHelper;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.PotentialStyleDelimiterInfo.ExtDelimiter;


@NonNullByDefault
public class PotentialStyleSpan extends SourceSpan {
	
	private static char charAfter(final Cursor cursor, final int length) {
		return cursor.hasNext(length) ? cursor.getNext(length) : '\n';
	}
	
	private static char charBefore(final Cursor cursor) {
		return cursor.hasPrevious() ? cursor.getPrevious() : '\n';
	}
	
	
	private final @Nullable PotentialStyleDelimiterInfo tilde;
	private final @Nullable PotentialStyleDelimiterInfo circumflex;
	
	
	public PotentialStyleSpan() {
		this.tilde= null;
		this.circumflex= null;
	}
	
	public PotentialStyleSpan(final boolean strikeoutEnabled,
			final boolean superscriptEnabled, final boolean subscriptEnabled) {
		this.tilde= (strikeoutEnabled || subscriptEnabled) ?
				new ExtDelimiter(subscriptEnabled, strikeoutEnabled) {
			@Override
			public char getChar() {
				return '~';
			}
			@Override
			public Inline createStyleInline(final int size, final Line line,
					final int offset, final int length, final List<Inline> contents) {
				return switch (size) {
				case 1 -> new Subscript(line, offset, length, contents);
				case 2 -> new Strikeout(line, offset, length, contents);
				default ->
						throw new IllegalStateException();
				};
			}
		} : null;
		this.circumflex= (superscriptEnabled) ?
				new ExtDelimiter(superscriptEnabled, false) {
			@Override
			public char getChar() {
				return '^';
			}
			@Override
			public Inline createStyleInline(final int size, final Line line,
					final int offset, final int length, final List<Inline> contents) {
				return switch (size) {
				case 1 -> new Superscript(line, offset, length, contents);
				default ->
						throw new IllegalStateException();
				};
			}
		} : null;
	}
	
	
	public String getControlChars() {
		if (this.tilde != null) {
			if (this.circumflex != null) {
				return "*^_~";
			}
			else {
				return "*_~";
			}
		}
		else {
			if (this.circumflex != null) {
				return "*^_";
			}
			else {
				return "*_";
			}
		}
	}
	
	@Override
	public @Nullable Inline createInline(final ProcessingContext context, final Cursor cursor) {
		final PotentialStyleDelimiterInfo info= getInfo(cursor.getChar());
		if (info != null && !currentPositionIsEscaped(cursor)) {
			final int length= lengthMatching(cursor, info.getChar());
			if (info.isPotentialSequence(length)) {
				boolean canOpen= true;
				boolean canClose= true;
				
				if ((info.getRequirements(0) & (FLANKING | FLANKING_UNDERSCORE)) != 0) {
					final boolean leftFlanking= isLeftFlanking(cursor, length, context);
					final boolean rightFlanking= isRightFlanking(cursor, length, context);
					
					if ((info.getRequirements(0) & FLANKING_UNDERSCORE) != 0) {
						final ParseHelper helper= context.getHelper();
						canOpen= leftFlanking && (!rightFlanking || helper.isUnicodePunctuation(charBefore(cursor)));
						canClose= rightFlanking && (!leftFlanking || helper.isUnicodePunctuation(charAfter(cursor, length)));
					}
					else {
						canOpen= leftFlanking;
						canClose= rightFlanking;
					}
				}
				
				return new PotentialStyleDelimiter(info,
						cursor.getLineAtOffset(),
						cursor.getOffset(), length,
						cursor.getTextAtOffset(0, length),
						canOpen, canClose );
			}
		}
		return null;
	}
	
	private @Nullable PotentialStyleDelimiterInfo getInfo(final char c) {
		return switch (c) {
		case '*' -> PotentialStyleDelimiterInfo.DEFAULT_ASTERISK;
		case '_' -> PotentialStyleDelimiterInfo.DEFAULT_UNDERSCORE;
		case '~' -> this.tilde;
		case '^' -> this.circumflex;
		default ->  null;
		};
	}
	
	boolean isLeftFlanking(final Cursor cursor, final int length, final ProcessingContext context) {
		final char charBefore= charBefore(cursor);
		final char charAfter= charAfter(cursor, length);
		final ParseHelper helper= context.getHelper();
		return !isUnicodeWhitespace(charAfter) && !(helper.isUnicodePunctuation(charAfter)
				&& !isUnicodeWhitespace(charBefore) && !helper.isUnicodePunctuation(charBefore));
	}
	
	boolean isRightFlanking(final Cursor cursor, final int length, final ProcessingContext context) {
		final char charBefore= charBefore(cursor);
		final char charAfter= charAfter(cursor, length);
		final ParseHelper helper= context.getHelper();
		return !isUnicodeWhitespace(charBefore) && !(helper.isUnicodePunctuation(charBefore)
				&& !isUnicodeWhitespace(charAfter) && !helper.isUnicodePunctuation(charAfter));
	}
	
	private boolean currentPositionIsEscaped(final Cursor cursor) {
		int backslashCount= 0;
		for (int x= 1; cursor.hasPrevious(x) && cursor.getPrevious(x) == '\\'; ++x) {
			++backslashCount;
		}
		return backslashCount % 2 == 1;
	}
	
	private int lengthMatching(final Cursor cursor, final char c) {
		int x= 1;
		while (cursor.hasNext(x) && cursor.getNext(x) == c) {
			++x;
		}
		return x;
	}
	
}
