/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextLineInformation;


@NonNullByDefault
public class ContentLineSequence extends LineSequence {
	
	
	private final String content;
	private final TextLineInformation lineInfos;
	
	private @Nullable Line currentLine;
	
	private final List<Line> followingLines= new ArrayList<>();
	
	
	public ContentLineSequence(final String content, final TextLineInformation lineInfos) {
		this.content= content;
		this.lineInfos= lineInfos;
		
		this.currentLine= createLine(0);
	}
	
	
	private @Nullable Line createLine(final int lineNumber) {
		if (lineNumber >= this.lineInfos.getNumberOfLines()) {
			return null;
		}
		
		final int startOffset= this.lineInfos.getStartOffset(lineNumber);
		int endOffset= this.lineInfos.getEndOffset(lineNumber);
		final String delimiter;
		switch ((endOffset > startOffset) ? this.content.charAt(endOffset - 1) : 0) {
		case '\n':
			endOffset--;
			if (endOffset > startOffset && this.content.charAt(endOffset - 1) == '\r') {
				endOffset--;
				delimiter= "\r\n";
			}
			else {
				delimiter= "\n";
			}
			break;
		case '\r':
			endOffset--;
			delimiter= "\r";
			break;
		default:
			delimiter= "";
			break;
		}
		return new Line(lineNumber, startOffset, 0, this.content.substring(startOffset, endOffset), delimiter);
	}
	
	@Override
	public @Nullable Line getCurrentLine() {
		return this.currentLine;
	}
	
	@Override
	public @Nullable Line getNextLine() {
		return getNextLine(0);
	}
	
	public @Nullable Line getNextLine(final int index) {
		if (index < 0) {
			throw new IllegalArgumentException();
		}
		if (this.currentLine != null) {
			while (index >= this.followingLines.size()) {
				final Line line= createLine(this.currentLine.getLineNumber() + this.followingLines.size() + 1);
				if (line == null) {
					break;
				}
				this.followingLines.add(line);
			}
		}
		return (index < this.followingLines.size()) ? this.followingLines.get(index) : null;
	}
	
	@Override
	public void advance() {
		this.currentLine= getNextLine(0);
		if (this.currentLine != null) {
			this.followingLines.remove(0);
		}
	}
	
	@Override
	public LineSequence lookAhead() {
		return new LookAheadLineSequence(this);
	}
	
}
