/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
#=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import java.util.regex.Matcher;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;


@NonNullByDefault
public abstract class SourceBlockType<TNode extends SourceBlockNode<?>> {
	
	
	protected static final void advanceLinesUpto(final LineSequence lineSequence, final int endLineNumber) {
		while (true) {
			final Line line= lineSequence.getCurrentLine();
			if (line != null && line.getLineNumber() < endLineNumber) {
				lineSequence.advance();
				continue;
			}
			break;
		}
	}
	
	protected static final void advanceBlankLines(final LineSequence lineSequence) {
		while (true) {
			final Line line= lineSequence.getCurrentLine();
			if (line != null && line.isBlank()) {
				lineSequence.advance();
				continue;
			}
			break;
		}
	}
	
	protected static final void advanceNonBlankLines(final LineSequence lineSequence) {
		while (true) {
			final Line line= lineSequence.getCurrentLine();
			if (line != null && !line.isBlank()) {
				lineSequence.advance();
				continue;
			}
			break;
		}
	}
	
	protected static void assertMatches(final Matcher matcher) {
		if (!matcher.matches()) {
			throw new IllegalStateException();
		}
	}
	
	
	public abstract boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode);
	
	public abstract void createNodes(final SourceBlockBuilder builder);
	
	public abstract void initializeContext(final ProcessingContext context, final TNode node);
	
	public abstract void emit(final ProcessingContext context, final TNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder);
	
	
}
