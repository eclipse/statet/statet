/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import java.util.List;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class Characters extends InlineWithText {
	
	
	static boolean append(final List<Inline> inlines,
			final int newIndex, final InlineWithText newInline) {
		final Inline previous;
		if (newIndex > 0 && (previous= inlines.get(newIndex - 1)) instanceof Characters) {
			final Characters lastCharacters= (Characters)previous;
			
			final Characters substitution= new Characters(lastCharacters.getLine(),
					lastCharacters.getStartOffset(),
					newInline.getEndOffset() - lastCharacters.getStartOffset(),
					lastCharacters.getCursorLength() + newInline.getCursorLength(),
					lastCharacters.getText() + newInline.getText() );
			inlines.set(newIndex - 1, substitution);
			return true;
		}
		return false;
	}
	
	
	public Characters(final Line line, final int offset, final int length, final int cursorLength,
			final String text) {
		super(line, offset, length, cursorLength, text);
	}
	
	
	@Override
	public void apply(final ProcessingContext context, final List<Inline> inlines,
			final Cursor cursor, final boolean inBlock) {
		if (append(inlines, inlines.size(), this)) {
			cursor.advance(getLength());
			return;
		}
		
		super.apply(context, inlines, cursor, inBlock);
	}
	
	@Override
	public void emit(final ProcessingContext context,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		builder.characters(this.text);
	}
	
}
