/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class HtmlEntitySpan extends SourceSpan {
	
	
	private static final Pattern PATTERN= Pattern.compile(
			CommonRegex.HTML_ENTITY_REGEX + ".*",
			Pattern.DOTALL );
	
	
	private @Nullable Matcher matcher;
	
	
	public HtmlEntitySpan() {
	}
	
	
	@Override
	public @Nullable Inline createInline(final ProcessingContext context, final Cursor cursor) {
		final Matcher matcher;
		final char c= cursor.getChar();
		if (c == '&'
				&& (matcher= cursor.setup(getMatcher())).matches() ) {
			final String entity= (@NonNull String)matcher.group(1);
			return new HtmlEntity(
					cursor.getLineAtOffset(), cursor.getOffset(), entity.length() + 2,
					entity );
		}
		return null;
	}
	
	
	@SuppressWarnings("null")
	private Matcher getMatcher() {
		if (this.matcher == null) {
			this.matcher= PATTERN.matcher("");
		}
		return this.matcher;
	}
	
}
