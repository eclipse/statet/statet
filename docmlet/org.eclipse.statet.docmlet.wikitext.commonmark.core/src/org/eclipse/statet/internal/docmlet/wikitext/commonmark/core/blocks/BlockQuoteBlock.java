/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementDetail;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.FilterLineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;


@NonNullByDefault
public class BlockQuoteBlock extends BlockWithNestedBlocks<BlockQuoteBlock.QuoteBlockNode> {
	
	
	static final class QuoteBlockNode extends SourceBlockNode<BlockQuoteBlock> {
		
		private QuoteBlockNode(final BlockQuoteBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
	}
	
	
	private static final Pattern START_PATTERN= Pattern.compile(
			">.*",
			Pattern.DOTALL );
	private static final Pattern PROCESS_PATTERN= Pattern.compile(
			">([ \t])?.*",
			Pattern.DOTALL );
	
	public static final Pattern PATTERN= PROCESS_PATTERN;
	
	public static final int computeContentLineIndent(final Line line, final Matcher matcher) {
		final int spaceOffset= matcher.start(1);
		if (spaceOffset != -1) {
			return line.getColumn(spaceOffset) + 1 - line.getColumn();
		}
		else {
			return line.getColumn(matcher.regionStart()) + 1 - line.getColumn();
		}
	}
	
	
	private static class QuotedBlockLines extends FilterLineSequence {
		
		
		private final SourceBlockBuilder builder;
		private final SourceBlockNode<BlockQuoteBlock> node;
		
		private final Matcher matcher;
		
		
		public QuotedBlockLines(final SourceBlockBuilder builder,
				final SourceBlockNode<BlockQuoteBlock> node,
				final Matcher matcher) {
			super(builder.getLineSequence());
			
			this.builder= builder;
			this.node= node;
			this.matcher= matcher;
		}
		
		protected QuotedBlockLines(final QuotedBlockLines from) {
			super(from.getDelegate().lookAhead());
			
			this.builder= from.builder;
			this.node= from.node;
			this.matcher= from.matcher;
		}
		
		
		@Override
		public LineSequence lookAhead() {
			return new QuotedBlockLines(this);
		}
		
		@Override
		protected @Nullable Line filter(final Line line) {
			if (!line.isBlank()) {
				final Matcher matcher;
				if (line.getIndent() < 4
						&& (matcher= line.setupIndent(this.matcher)).matches() ) {
					return line.segmentByIndent(computeContentLineIndent(line, matcher));
				}
				if (isLazyContinuation(line)) {
					return line.lazy();
				}
			}
			return null;
		}
		
		private boolean isLazyContinuation(final Line line) {
			final SourceBlockNode<?> activeNode= this.builder.getActiveNode();
			if (activeNode != null && activeNode.isParagraph()) {
				final LineSequence lookAhead= getDelegate().lookAhead(line.getLineNumber());
				if (!((ParagraphBlock)activeNode.getType()).isAnotherBlockStart(
						lookAhead, this.builder.getSourceBlocks(), activeNode)) {
					return true;
				}
			}
			return false;
		}
		
	}
	
	
	private final Matcher startMatcher= START_PATTERN.matcher("");
	
	private @Nullable Matcher processMatcher;
	
	
	public BlockQuoteBlock() {
	}
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		final Line currentLine= lineSequence.getCurrentLine();
		return (currentLine != null
				&& !currentLine.isBlank() && currentLine.getIndent() < 4
				&& currentLine.setupIndent(this.startMatcher).matches() );
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		final QuoteBlockNode node= new QuoteBlockNode(this, builder);
		
		final QuotedBlockLines quotedBlock= new QuotedBlockLines(builder, node,
				getProcessMatcher() );
		builder.createNestedNodes(quotedBlock, null);
	}
	
	@Override
	public void emit(final ProcessingContext context, final QuoteBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final SourceElementAttributes attributes= new SourceElementAttributes(
				SourceElementDetail.END_UNCLOSED );
		
		locator.setBlockBegin(node);
		builder.beginBlock(BlockType.QUOTE, attributes);
		
		super.emit(context, node, locator, builder);
		
		locator.setBlockEnd(node);
		builder.endBlock();
	}
	
	
	@SuppressWarnings("null")
	private Matcher getProcessMatcher() {
		if (this.processMatcher == null) {
			this.processMatcher= PROCESS_PATTERN.matcher("");
		}
		return this.processMatcher;
	}
	
}
