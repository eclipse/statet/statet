/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;


@NonNullByDefault
public abstract class BlockWithNestedBlocks<TNode extends SourceBlockNode<?>> extends SourceBlockType<TNode> {
	
	
	public BlockWithNestedBlocks() {
	}
	
	
	@Override
	public void initializeContext(final ProcessingContext context, final TNode node) {
		for (final SourceBlockNode<?> nestedNode : node.getNested()) {
			nestedNode.getType().initializeContext(context, nestedNode);
		}
	}
	
	@Override
	public void emit(final ProcessingContext context, final TNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		for (final SourceBlockNode<?> nestedNode : node.getNested()) {
			nestedNode.getType().emit(context, nestedNode, locator, builder);
		}
	}
	
}
