/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public abstract class FilterLineSequence extends LineSequence {
	
	private static final Line STOP= new Line(0, 0, 0, "<STOP>", "\n");
	
	
	private final LineSequence delegate;
	
	private @Nullable Line filteredLine;
	
	
	public FilterLineSequence(final LineSequence delegate) {
		this.delegate= delegate;
	}
	
	
	protected final LineSequence getDelegate() {
		return this.delegate;
	}
	
	@Override
	public @Nullable Line getCurrentLine() {
		if (this.filteredLine == null) {
			Line line= this.delegate.getCurrentLine();
			if (line != null) {
				line= filter(line);
			}
			this.filteredLine= (line != null) ? line : STOP;
		}
		return (this.filteredLine != STOP) ? this.filteredLine : null;
	}
	
	@Override
	public @Nullable Line getNextLine() {
		final Line line= this.delegate.getNextLine();
		if (line != null) {
			return filter(line);
		}
		return null;
	}
	
	@Override
	public void advance() {
		this.delegate.advance();
		this.filteredLine= null;
	}
	
	
	protected abstract @Nullable Line filter(final Line line);
	
}
