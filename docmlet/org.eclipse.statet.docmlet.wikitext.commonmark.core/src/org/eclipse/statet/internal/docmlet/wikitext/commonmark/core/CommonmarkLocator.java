/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import org.eclipse.mylyn.wikitext.parser.Locator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines.Inline;


@NonNullByDefault
public class CommonmarkLocator implements Locator {
	
	
	private int lineNumber;
	
	private int lineOffset;
	
	private int lineLength;
	
	private int lineSegmentStartOffset;
	
	private int lineSegmentEndOffset;
	
	
	public CommonmarkLocator() {
	}
	
	
	public void setLine(final Line line) {
		this.lineNumber= line.getLineNumber() + 1;
		this.lineOffset= line.getStartOffset();
		this.lineLength= line.getLength();
		this.lineSegmentStartOffset= 0;
		this.lineSegmentEndOffset= this.lineLength;
	}
	
	public void setBlockBegin(final SourceBlockNode<?> node) {
		final var lines= node.getLines();
		setLine(lines.getFirst());
	}
	
	public void setBlockEnd(final SourceBlockNode<?> node) {
		final var lines= node.getLines();
		final Line lastLine= lines.getLast();
		this.lineNumber= lastLine.getLineNumber();
		this.lineOffset= lastLine.getEndOffset();
		this.lineLength= 0;
		this.lineSegmentStartOffset= 0;
		this.lineSegmentEndOffset= 0;
	}
	
	public void setInline(final Inline inline) {
		final Line line= inline.getLine();
		this.lineNumber= line.getLineNumber() + 1;
		this.lineOffset= line.getStartOffset();
		this.lineLength= line.getText().length();
		this.lineSegmentStartOffset= inline.getStartOffset() - this.lineOffset;
		this.lineSegmentEndOffset= this.lineSegmentStartOffset + inline.getLength();
	}
	
	
	@Override
	public int getLineNumber() {
		return this.lineNumber;
	}
	
	@Override
	public int getLineDocumentOffset() {
		return this.lineOffset;
	}
	
	@Override
	public int getDocumentOffset() {
		return getLineDocumentOffset() + getLineCharacterOffset();
	}
	
	@Override
	public int getLineLength() {
		return this.lineLength;
	}
	
	@Override
	public int getLineCharacterOffset() {
		return this.lineSegmentStartOffset;
	}
	
	@Override
	public int getLineSegmentEndOffset() {
		return this.lineSegmentEndOffset;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(Locator.class, getClass());
		sb.addProp("line", this.lineNumber); //$NON-NLS-1$
		sb.append(" [%1$s, %2$s)", this.lineOffset, this.lineOffset + this.lineLength); //$NON-NLS-1$
		sb.addProp("lineSegment", //$NON-NLS-1$
				"[%1$s, %2$s)", this.lineSegmentStartOffset, this.lineSegmentEndOffset); //$NON-NLS-1$
		return sb.build();
	}
	
}
