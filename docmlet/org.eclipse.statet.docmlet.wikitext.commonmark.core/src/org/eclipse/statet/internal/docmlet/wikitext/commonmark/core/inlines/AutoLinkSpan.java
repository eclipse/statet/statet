/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.CTRL_OR_SPACE;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.References;


@NonNullByDefault
public class AutoLinkSpan extends SourceSpan {
	
	private static final String SCHEME_REGEX= "\\p{Alpha}[\\p{Alnum}.+-]{1,31}";
	
	private static final String ABSOLUTE_URI_REGEX= SCHEME_REGEX + ":[^" + CTRL_OR_SPACE + "<>]+";
	
	private static final String EMAIL_DOMAIN_PART= "\\p{Alnum}(?:[\\p{Alnum}-]{0,61}\\p{Alnum})?";
	
	private static final String EMAIL_REGEX= "[\\p{Alnum}.!#$%&'*+/=?^_`{|}~-]+@" +
			EMAIL_DOMAIN_PART + "(?:\\." + EMAIL_DOMAIN_PART + ")*";
	
	private static final Pattern PATTERN= Pattern.compile("(<(" + ABSOLUTE_URI_REGEX + "|(" + EMAIL_REGEX + "))>).*",
			Pattern.DOTALL );
	
	
	private @Nullable Matcher matcher;
	
	
	public AutoLinkSpan() {
	}
	
	
	@Override
	public @Nullable Inline createInline(final ProcessingContext context, final Cursor cursor) {
		final char c= cursor.getChar();
		if (c == '<') {
			final Matcher matcher= cursor.setup(getMatcher());
			if (matcher.matches()) {
				final String link= (@NonNull String)matcher.group(2);
				final String href= (matcher.start(3) != -1) ? ("mailto:" + link) : link;
				
				final int cursorLength= matcher.end(1) - matcher.regionStart();
				final int startOffset= cursor.getOffset();
				// no line break: final int endOffset= startOffset + cursorLength;
				
				return new Link(cursor.getLineAtOffset(), startOffset, cursorLength, cursorLength,
						encodeLinkUri(href), null, ImCollections.newList(
								new Characters(cursor.getLineAtOffset(),
										startOffset + 1, cursorLength - 2, cursorLength - 2,
										link )));
			}
		}
		return null;
	}
	
	
	@SuppressWarnings("null")
	private Matcher getMatcher() {
		if (this.matcher == null) {
			this.matcher= PATTERN.matcher("");
		}
		return this.matcher;
	}
	
	
	static String encodeLinkUri(final String link) {
		return References.encodeUri(link).replace("%25", "%");
	}
	
}
