/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceElementDetail;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceListAttributes;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.FilterLineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.ListBlock.ListBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks.ParagraphBlock.ParagraphBlockNode;


@NonNullByDefault
public class ListBlock extends BlockWithNestedBlocks<ListBlockNode> {
	
	
	static final class ListBlockNode extends SourceBlockNode<ListBlock> {
		
		private char bulletType;
		
		private @Nullable String listStart;
		
		private ListBlockNode(final ListBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
	}
	
	
	public static final Pattern PATTERN= Pattern.compile(
			"([*+-]|([0-9]{1,9})[.)])(?:([ \t]+)(.+)?)?", //$NON-NLS-1$
			Pattern.DOTALL );
	private static final int MARKER_GROUP= 1;
	private static final int ORDERED_NUM_GROUP= 2;
	private static final int WHITESPACE_GROUP= 3;
	private static final int CONTENT_GROUP= 4;
	
	public static int computeItemLineIndent(final Line line, final Matcher matcher) {
		final int markerEndColumn= line.getColumn(matcher.end(MARKER_GROUP));
		if (matcher.start(WHITESPACE_GROUP) != -1 && matcher.start(CONTENT_GROUP) != -1) {
			final int contentStartColumn= line.getColumn(matcher.start(CONTENT_GROUP));
			if (contentStartColumn - markerEndColumn <= 4) {
				return contentStartColumn - line.getColumn();
			}
		}
		return markerEndColumn + 1 - line.getColumn();
	}
	
	
	private static enum ListMode {
		TIGHT, LOOSE, TIGHT_WITH_TRAILING_EMPTY_LINE
	}
	
	
	private static class ListLines extends FilterLineSequence {
		
		
		private final SourceBlockBuilder builder;
		
		private final ListItemBlock type;
		
		
		public ListLines(final LineSequence delegate, final SourceBlockBuilder builder,
				final ListItemBlock type) {
			super(delegate);
			
			this.builder= builder;
			this.type= type;
		}
		
		public ListLines(final ListLines from) {
			super(from.getDelegate().lookAhead());
			
			this.builder= from.builder;
			this.type= from.type;
		}
		
		
		@Override
		public LineSequence lookAhead() {
			return new ListLines(this);
		}
		
		@Override
		protected @Nullable Line filter(final Line line) {
			if (!line.isBlank()) {
				if (line.getIndent() >= this.type.itemIdent
						|| this.type.canStart(line) ) {
					return line;
				}
				if (isLazyContinuation(line)) {
					return line.lazy();
				}
			}
			else {
				if (lookAheadSafeLine(getDelegate().lookAhead(line.getLineNumber())) != Integer.MIN_VALUE) {
					return line;
				}
			}
			return null;
		}
		
		private boolean isLazyContinuation(final Line line) {
			final SourceBlockNode<?> activeNode= this.builder.getActiveNode();
			if (activeNode != null
					&& activeNode.getParent() != this.type.listNode
					&& activeNode.isParagraph()) {
				final ParagraphBlockNode<?> paragraphNode= (ParagraphBlockNode<?>)activeNode;
				if (!(this.type.canStart(line)
						|| paragraphNode.getType().isAnotherBlockStart(
								getDelegate().lookAhead(line.getLineNumber()), this.builder.getSourceBlocks(), paragraphNode ))) {
					return true;
				}
			}
			return false;
		}
		
		private int lookAheadSafeLine(final LineSequence lineSequence) {
			while (true) {
				final Line line= lineSequence.getCurrentLine();
				if (line != null) {
					if (line.isBlank()) {
						lineSequence.advance();
						continue;
					}
					if (line.getIndent() >= this.type.itemIdent
							|| this.type.canStart(line) ) {
						return line.getLineNumber();
					}
				}
				return Integer.MIN_VALUE;
			}
		}
		
	}
	
	
	private static class ListItemBlock extends BlockWithNestedBlocks<SourceBlockNode<ListItemBlock>> {
		
		
		private final ListBlockNode listNode;
		
		private int itemIdent= 4;
		
		
		public ListItemBlock(final ListBlockNode listNode) {
			this.listNode= listNode;
		}
		
		
		@Override
		public boolean canStart(final LineSequence lineSequence,
				final @Nullable SourceBlockNode<?> currentNode) {
			return canStart(lineSequence.getCurrentLine());
		}
		
		public boolean canStart(final @Nullable Line startLine) {
			if (startLine != null
					&& !startLine.isBlank() && startLine.getIndent() < 4) {
				final ListBlock listType= this.listNode.getType();
				final Matcher matcher;
				return ((matcher= startLine.setupIndent(listType.matcher)).matches()
						&& (listType.bulletType(startLine, matcher) == this.listNode.bulletType)
						&& !listType.thematicBreakBlock.canStart(startLine) );
			}
			return false;
		}
		
		@Override
		public void createNodes(final SourceBlockBuilder builder) {
			final SourceBlockNode<ListItemBlock> node= new SourceBlockNode<>(this, builder);
			
			final ListBlock listType= this.listNode.getType();
			final Line startLine= nonNullAssert(builder.getLineSequence().getCurrentLine());
			this.itemIdent= computeItemLineIndent(startLine, listType.matcher);
			final ListItemLines itemLineSequence= new ListItemLines(builder, node,
					startLine.getLineNumber(), this.itemIdent );
			builder.createNestedNodes(itemLineSequence, null);
		}
		
		@Override
		public void emit(final ProcessingContext context, final SourceBlockNode<ListItemBlock> node,
				final CommonmarkLocator locator, final DocumentBuilder builder) {
			throw new UnsupportedOperationException();
		}
		
		public void emit(final ProcessingContext context, final SourceBlockNode<ListItemBlock> node,
				final ListBlock.ListMode listMode,
				final CommonmarkLocator locator, final DocumentBuilder builder) {
			final SourceElementAttributes attributes= new SourceElementAttributes(
					SourceElementDetail.END_UNCLOSED );
			
			locator.setBlockBegin(node);
			builder.beginBlock(BlockType.LIST_ITEM, attributes);
			
			for (final SourceBlockNode<?> contentNode : node.getNested()) {
				if (listMode == ListBlock.ListMode.TIGHT
						&& contentNode.isParagraph()) {
					final ParagraphBlock.ParagraphBlockNode<?> paragraphNode= (ParagraphBlockNode<?>)contentNode;
					paragraphNode.getType()
							.emitParagraph(context, paragraphNode, false, locator, builder);
				}
				else {
					contentNode.getType()
							.emit(context, contentNode, locator, builder);
				}
			}
			
			locator.setBlockEnd(node);
			builder.endBlock();
		}
		
	}
	
	private static class ListItemLines extends FilterLineSequence {
		
		private final SourceBlockBuilder builder;
		private final SourceBlockNode<ListItemBlock> node;
		
		private final int markerLineNumber;
		private final int indent;
		
		
		public ListItemLines(final SourceBlockBuilder builder,
				final SourceBlockNode<ListItemBlock> node,
				final int markerLineNumber, final int indent) {
			super(builder.getLineSequence());
			
			this.builder= builder;
			this.node= node;
			this.markerLineNumber= markerLineNumber;
			this.indent= indent;
		}
		
		protected ListItemLines(final ListItemLines from) {
			super(from.getDelegate().lookAhead());
			
			this.builder= from.builder;
			this.node= from.node;
			this.markerLineNumber= from.markerLineNumber;
			this.indent= from.indent;
		}
		
		
		@Override
		public ListItemLines lookAhead() {
			return new ListItemLines(this);
		}
		
		
		@Override
		protected @Nullable Line filter(final Line line) {
			final List<SourceBlockNode<?>> contentNodes= this.node.getNested();
			if (contentNodes.size() == 1
					&& contentNodes.get(0).isEmpty()
					&& line.getLineNumber() > this.markerLineNumber + 1) {
				return null;
			}
			// validity already checked in ListLines
			if (line.isLazy()) {
				return line;
			}
			if (line.getLineNumber() == this.markerLineNumber
					|| line.isBlank()
					|| line.getIndent() >= this.indent ) {
				return line.segmentByIndent(this.indent);
			}
			return null;
		}
		
	}
	
	
	private final Matcher matcher= PATTERN.matcher("");
	
	private final ThematicBreakBlock thematicBreakBlock= new ThematicBreakBlock();
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		final Line currentLine= lineSequence.getCurrentLine();
		final Matcher matcher;
		return (currentLine != null
				&& !currentLine.isBlank() && currentLine.getIndent() < 4
				&& (matcher= currentLine.setupIndent(this.matcher)).matches()
				&& (currentNode == null
						|| canInterrupt(currentLine, matcher, currentNode)) );
	}
	
	private boolean canInterrupt(final Line startLine, final Matcher matcher,
			final SourceBlockNode<?> currentNode) {
		final SourceBlockNode<?> parent= currentNode.getParent();
		if (parent != null && parent.getType() instanceof ListItemBlock) {
			return true;
		}
		return (listStart(startLine, matcher) == null
				&& matcher.start(CONTENT_GROUP) != -1 );
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		final ListBlockNode node= new ListBlockNode(this, builder);
		final LineSequence lineSequence= builder.getLineSequence();
		
		final Line startLine= (@NonNull Line)lineSequence.getCurrentLine();
		final Matcher matcher= startLine.setupIndent(this.matcher);
		assertMatches(matcher);
		node.bulletType= bulletType(startLine, matcher);
		node.listStart= listStart(startLine, matcher);
		
		final ListItemBlock itemBlock= new ListItemBlock(node);
		
		builder.createNestedNodes(new ListLines(lineSequence, builder, itemBlock),
				ImCollections.newList(itemBlock) );
	}
	
	@Override
	public void emit(final ProcessingContext context, final ListBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final SourceListAttributes listAttributes= new SourceListAttributes(
				SourceElementDetail.END_UNCLOSED );
		listAttributes.setStart(node.listStart);
		
		final ListMode listMode= (context.getMode() == ProcessingContext.PARSE_SOURCE_STRUCT) ?
				ListMode.LOOSE :
				calculateListMode(node);
		
		locator.setBlockBegin(node);
		builder.beginBlock(toBlockType(node.bulletType), listAttributes);
		
		for (final SourceBlockNode<?> nestedNode : node.getNested()) {
			((ListItemBlock)nestedNode.getType()).emit(context, (SourceBlockNode<ListItemBlock>)nestedNode,
					listMode,
					locator, builder );
		}
		
		locator.setBlockEnd(node);
		builder.endBlock();
	}
	
	
	private ListMode calculateListMode(final ListBlockNode listNode) {
		ListMode listMode= ListMode.TIGHT;
		for (final SourceBlockNode<?> itemNode : listNode.getNested()) {
			switch (listMode) {
			case LOOSE:
			case TIGHT_WITH_TRAILING_EMPTY_LINE:
				return ListMode.LOOSE;
			case TIGHT:
				listMode= getListItemListMode(itemNode);
				continue;
			}
		}
		return (listMode == ListMode.TIGHT_WITH_TRAILING_EMPTY_LINE) ? ListMode.TIGHT : listMode;
	}
	
	private ListMode getListItemListMode(final SourceBlockNode<?> itemNode) {
		final List<SourceBlockNode<?>> contentNodes= itemNode.getNested();
		if (contentNodes.isEmpty()) {
			return ListMode.TIGHT;
		}
		{	final SourceBlockNode<?> block= contentNodes.get(0);
			if (block.isEmpty() && block.getLines().size() > 1) {
				return ListMode.LOOSE;
			}
		}
		for (int idx= 1; idx < contentNodes.size() - 1; idx++) {
			final SourceBlockNode<?> block= contentNodes.get(idx);
			if (block.isEmpty()) {
				return ListMode.LOOSE;
			}
		}
		if (contentNodes.size() > 1) {
			final SourceBlockNode<?> block= contentNodes.get(contentNodes.size() - 1);
			if (block.isEmpty()) {
				return ListMode.TIGHT_WITH_TRAILING_EMPTY_LINE;
			}
		}
		return ListMode.TIGHT;
	}
	
	
	private char bulletType(final Line line, final Matcher matcher) {
		return line.getText().charAt(matcher.end(MARKER_GROUP) - 1);
	}
	
	private @Nullable String listStart(final Line line, final Matcher matcher) {
		String number= matcher.group(ORDERED_NUM_GROUP);
		if (number != null) {
			int startIdx= 0;
			while (startIdx < number.length() - 1) {
				if (number.charAt(startIdx) == '0') {
					startIdx++;
					continue;
				}
				else {
					break;
				}
			}
			if (startIdx > 0) {
				number= number.substring(startIdx, number.length());
			}
			if (number.equals("1")) {
				return null;
			}
			return number;
		}
		return null;
	}
	
	private BlockType toBlockType(final char bulletType) {
		switch (bulletType) {
		case '*':
		case '+':
		case '-':
			return BlockType.BULLETED_LIST;
		default:
			return BlockType.NUMERIC_LIST;
		}
	}
	
}
