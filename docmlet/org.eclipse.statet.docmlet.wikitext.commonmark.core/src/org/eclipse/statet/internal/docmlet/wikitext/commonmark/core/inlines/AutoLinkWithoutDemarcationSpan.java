/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class AutoLinkWithoutDemarcationSpan extends SourceSpan {
	
	
	private static final Pattern PATTERN= Pattern.compile(
			"(https?://[\\p{Alnum}%._~!$&?#'()*+,;:@/=-]*[\\p{Alnum}_~!$&?#'(*+@/=-]).*",
			Pattern.DOTALL );
	
	
	private final Matcher matcher= PATTERN.matcher("");
	
	
	public AutoLinkWithoutDemarcationSpan() {
	}
	
	
	@Override
	public @Nullable Inline createInline(final ProcessingContext context, final Cursor cursor) {
		if (cursor.getChar() == 'h') {
			final Matcher matcher= cursor.setup(this.matcher);
			if (matcher.matches()) {
				final String link= (@NonNull String)matcher.group(1);
				final String href= link;
				
				final int cursorLength= matcher.end(1) - matcher.regionStart();
				final int startOffset= cursor.getOffset();
				// no line break: final int endOffset= startOffset + cursorLength;
				
				return new Link(cursor.getLineAtOffset(), startOffset, cursorLength, cursorLength,
						AutoLinkSpan.encodeLinkUri(link), null, ImCollections.newList(
								new Characters(cursor.getLineAtOffset(),
										startOffset, cursorLength, cursorLength,
										href )));
			}
		}
		return null;
	}
	
}
