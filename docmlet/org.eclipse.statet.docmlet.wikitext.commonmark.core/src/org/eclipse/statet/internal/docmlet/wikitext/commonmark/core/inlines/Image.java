/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;
import java.util.Objects;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.ImageAttributes;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.docmlet.wikitext.core.source.ImageByRefAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.LabelInfo;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ToStringHelper;


@NonNullByDefault
public class Image extends InlineWithNestedContents {
	
	
	private final String src;
	private final @Nullable LabelInfo labelReference;
	
	private final @Nullable String title;
	
	
	public Image(final Line line, final int offset, final int length,
			final String src, final @Nullable String title, final List<Inline> contents) {
		super(line, offset, length, -1, contents);
		this.src= nonNullAssert(src);
		this.labelReference= null;
		this.title= title;
	}
	
	public Image(final Line line, final int offset, final int length,
			final LabelInfo labelReference, final List<Inline> contents) {
		super(line, offset, length, -1, contents);
		this.src= ImageByRefAttributes.REF_SCHEME + ':' + labelReference.getLabel();
		this.labelReference= labelReference;
		this.title= null;
	}
	
	
	public String getHref() {
		return this.src;
	}
	
	@Override
	public void emit(final ProcessingContext context,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final ImageAttributes attributes= (this.labelReference != null) ?
				new ImageByRefAttributes(this.labelReference) : new ImageAttributes();
		attributes.setTitle(this.title);
		
		final List<Inline> contents= getContents();
		if (!contents.isEmpty()) {
			attributes.setAlt(InlineParser.toStringContent(context, contents));
		}
		
		builder.image(attributes, this.src);
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(getStartOffset(), getLength(), getContents(), this.src, this.title);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (super.equals(obj)) {
			@SuppressWarnings("null")
			final Image other= (@NonNull Image)obj;
			
			return (this.src.equals(other.src)
					&& Objects.equals(this.labelReference, other.labelReference)
					&& Objects.equals(this.title, other.title) );
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(Image.class, getClass());
		sb.addProp("startOffset", getStartOffset()); //$NON-NLS-1$
		sb.addProp("length", getLength()); //$NON-NLS-1$
		sb.addProp("src", ToStringHelper.toStringValue(this.src)); //$NON-NLS-1$
		sb.addProp("title", this.title); //$NON-NLS-1$
		sb.addProp("contents", getContents()); //$NON-NLS-1$
		return sb.build();
	}
	
}
