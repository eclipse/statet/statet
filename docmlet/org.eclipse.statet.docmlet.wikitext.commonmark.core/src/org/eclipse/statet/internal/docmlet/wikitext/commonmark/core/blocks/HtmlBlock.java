/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.blocks;

import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.CDATA_END_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.CDATA_START1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.CLOSE_TAG_1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.COMMENT_END_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.COMMENT_START1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.DECL_END_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.DECL_START1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.OPEN_TAG_1_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.PI_END_REGEX;
import static org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonRegex.PI_START1_REGEX;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.source.EmbeddingAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.ExtdocMarkupLanguage;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.LineSequence;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockNode;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlockType;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.SourceBlocks.SourceBlockBuilder;


@NonNullByDefault
public class HtmlBlock extends SourceBlockType<HtmlBlock.HtmlBlockNode> {
	
	
	static final class HtmlBlockNode extends SourceBlockNode<HtmlBlock> {
		
		private byte htmlType;
		
		private boolean isClosed;
		
		private HtmlBlockNode(final HtmlBlock type, final SourceBlockBuilder builder) {
			super(type, builder);
		}
		
	}
	
	
	private static final String HTML_1_TAG_NAMES= "pre|script|style|textarea";
	private static final String HTML_BLOCK_TAG_NAMES=
			"address|article|aside|base|basefont|blockquote|body" +
			"|caption|center|col|colgroup" +
			"|dd|details|dialog|dir|div|dl|dt" +
			"|fieldset|figcaption|figure|footer|form|frame|frameset" +
			"|h1|h2|h3|h4|h5|h6|head|header|hr|html" +
			"|iframe|legend|li|link" +
			"|main|menu|menuitem|meta|nav|noframes" +
			"|ol|optgroup|option|p|param" +
			"|section|source|title|summary" +
			"|table|tbody|td|tfoot|th|thead|tr|track" +
			"|ul";
	
	private static final Pattern START_PATTERN= Pattern.compile("<(?:" +
					"((?:(?i)" + HTML_1_TAG_NAMES + ")(?:[ \t>].*)?)" +
					"|(" + COMMENT_START1_REGEX + ".*)" +
					"|(" + PI_START1_REGEX + ".*)" +
					"|(" + DECL_START1_REGEX + ".*)" +
					"|(" + CDATA_START1_REGEX + ".*)" +
					"|(/?(?:(?i)" + HTML_BLOCK_TAG_NAMES + ")(?:(?:[ \t>]|/>).*)?)" +
					"|(" + OPEN_TAG_1_REGEX + "[ \t]*|" + CLOSE_TAG_1_REGEX + "[ \t]*)" +
			")",
			Pattern.DOTALL );
	
	private static final Pattern END_HTML_1_PATTERN= Pattern.compile(
			"</(?:(?i)" + HTML_1_TAG_NAMES + ")>",
			Pattern.DOTALL );
	private static final Pattern END_COMMENT_PATTERN= Pattern.compile(
			COMMENT_END_REGEX,
			Pattern.DOTALL );
	private static final Pattern END_PI_PATTERN= Pattern.compile(
			PI_END_REGEX,
			Pattern.DOTALL );
	private static final Pattern END_DECL_PATTERN= Pattern.compile(
			DECL_END_REGEX,
			Pattern.DOTALL );
	private static final Pattern END_CDATA_PATTERN= Pattern.compile(
			CDATA_END_REGEX,
			Pattern.DOTALL );
	
	
	private final Matcher startMatcher= START_PATTERN.matcher("");
	
	private @Nullable Matcher endHtml1Matcher;
	private @Nullable Matcher endCommentMatcher;
	private @Nullable Matcher endPIMatcher;
	private @Nullable Matcher endDeclMatcher;
	private @Nullable Matcher endCDATAMatcher;
	
	
	public HtmlBlock() {
	}
	
	
	@Override
	public boolean canStart(final LineSequence lineSequence,
			final @Nullable SourceBlockNode<?> currentNode) {
		final Line currentLine= lineSequence.getCurrentLine();
		final Matcher matcher;
		return (currentLine != null
				&& !currentLine.isBlank() && currentLine.getIndent() < 4
				&& (matcher= currentLine.setupIndent(this.startMatcher)).matches()
				&& (currentNode == null || canInterrupt(matcher)) );
	}
	
	private boolean canInterrupt(final Matcher matcher) {
		return (matcher.start(7) == -1);
	}
	
	@Override
	public void createNodes(final SourceBlockBuilder builder) {
		final HtmlBlockNode htmlBlockNode= new HtmlBlockNode(this, builder);
		final LineSequence lineSequence= builder.getLineSequence();
		
		final Line startLine= lineSequence.getCurrentLine();
		
		final Matcher matcher= startLine.setupIndent(this.startMatcher);
		assertMatches(matcher);
		
		htmlBlockNode.htmlType= getType(matcher);
		final Matcher endMatcher= getEndMatcher(htmlBlockNode.htmlType);
		
		if (endMatcher != null) {
			while (true) {
				final Line line= lineSequence.getCurrentLine();
				if (line != null) {
					lineSequence.advance();
					
					if (line.setup(endMatcher).find()) {
						htmlBlockNode.isClosed= true;
						break;
					}
					continue;
				}
				break;
			}
		}
		else {
			advanceNonBlankLines(lineSequence);
		}
	}
	
	@Override
	public void initializeContext(final ProcessingContext context, final HtmlBlockNode node) {
	}
	
	@Override
	public void emit(final ProcessingContext context, final HtmlBlockNode node,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		final HtmlBlockNode htmlBlockNode= node;
		final ImList<Line> lines= node.getLines();
		
		if (context.getMode() == ProcessingContext.PARSE_SOURCE_STRUCT) {
			final int descr;
			if (htmlBlockNode.htmlType == 2) {
				descr= ExtdocMarkupLanguage.EMBEDDED_HTML_COMMENT_BLOCK_DESCR;
			}
			else {
				descr= ExtdocMarkupLanguage.EMBEDDED_HTML_OTHER_BLOCK_DESCR
						| ((htmlBlockNode.htmlType << ExtdocMarkupLanguage.EMBEDDED_HTML_DISTINCT_SHIFT) & ExtdocMarkupLanguage.EMBEDDED_HTML_DISTINCT_MASK);
			}
			
			locator.setBlockBegin(node);
			builder.beginBlock(BlockType.CODE, new EmbeddingAttributes(
					ExtdocMarkupLanguage.EMBEDDED_HTML, descr) );
		}
		
		for (final Line line : lines) {
			locator.setLine(line);
			builder.charactersUnescaped(line.getText());
			builder.charactersUnescaped("\n");
		}
		
		if (context.getMode() == ProcessingContext.PARSE_SOURCE_STRUCT) {
			locator.setBlockEnd(node);
			builder.endBlock();
		}
	}
	
	
	private byte getType(final Matcher matcher) {
		if (matcher.start(1) != -1) {
			return 1;
		}
		if (matcher.start(2) != -1) {
			return 2;
		}
		if (matcher.start(3) != -1) {
			return 3;
		}
		if (matcher.start(4) != -1) {
			return 4;
		}
		if (matcher.start(5) != -1) {
			return 5;
		}
		if (matcher.start(6) != -1) {
			return 6;
		}
		return 7;
	}
	
	private @Nullable Matcher getEndMatcher(final byte type) {
		switch (type) {
		case 1:
			if (this.endHtml1Matcher == null) {
				this.endHtml1Matcher= END_HTML_1_PATTERN.matcher("");
			}
			return this.endHtml1Matcher;
		case 2:
			if (this.endCommentMatcher == null) {
				this.endCommentMatcher= END_COMMENT_PATTERN.matcher("");
			}
			return this.endCommentMatcher;
		case 3:
			if (this.endPIMatcher == null) {
				this.endPIMatcher= END_PI_PATTERN.matcher("");
			}
			return this.endPIMatcher;
		case 4:
			if (this.endDeclMatcher == null) {
				this.endDeclMatcher= END_DECL_PATTERN.matcher("");
			}
			return this.endDeclMatcher;
		case 5:
			if (this.endCDATAMatcher == null) {
				this.endCDATAMatcher= END_CDATA_PATTERN.matcher("");
			}
			return this.endCDATAMatcher;
		default:
			return null; // blank line
		}
	}
	
}
