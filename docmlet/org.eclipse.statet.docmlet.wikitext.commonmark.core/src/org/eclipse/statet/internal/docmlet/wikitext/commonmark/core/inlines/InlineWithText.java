/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.lang.ObjectUtils.ToStringBuilder;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;


@NonNullByDefault
abstract class InlineWithText extends Inline {
	
	
	protected final String text;
	
	
	public InlineWithText(final Line line, final int offset, final int length, final int cursorLength,
			final String text) {
		super(line, offset, length, cursorLength);
		this.text= nonNullAssert(text);
	}
	
	
	public String getText() {
		return this.text;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(getStartOffset(), getLength(), this.text);
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (super.equals(obj)) {
			@SuppressWarnings("null")
			final InlineWithText other= (@NonNull InlineWithText)obj;
			
			return (this.text.equals(other.text));
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		final ToStringBuilder sb= new ToStringBuilder(getClass(), getClass());
		sb.addProp("startOffset", getStartOffset()); //$NON-NLS-1$
		sb.addProp("length", getLength()); //$NON-NLS-1$
		sb.addProp("text", getText()); //$NON-NLS-1$
		return sb.build();
	}
	
}
