/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.mylyn.wikitext.parser.markup.IdGenerationStrategy;


public class CommonmarkIdGenerationStrategy extends IdGenerationStrategy {
	
	
	private static final Pattern PATTERN= Pattern.compile("[^a-z0-9_]+");
	
	
	private final Matcher matcher= PATTERN.matcher("");
	
	
	@Override
	public String generateId(final String headingText) {
		final String id= headingText.toLowerCase(Locale.ENGLISH);
		final Matcher matcher= this.matcher.reset(id);
		
		boolean result= matcher.find();
		if (result) {
			final StringBuffer sb= new StringBuffer();
			int idx= 0;
			if (matcher.start() == 0) {
				idx= matcher.end();
				result= matcher.find();
			}
			while (result) {
				sb.append(id, idx, matcher.start());
				sb.append("-");
				idx= matcher.end();
				result= matcher.find();
			}
			if (idx < id.length()) {
				sb.append(id, idx, id.length());
			}
			else if (sb.length() == 0) {
				return "";
			}
			else {
				sb.deleteCharAt(sb.length() - 1);
			}
			return sb.toString();
		}
		return id;
	}
	
}
