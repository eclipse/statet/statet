/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class LineBreakSpan extends SourceSpan {
	
	
	private final static Pattern PATTERN= Pattern.compile("( *(\\\\)?\n).*",
			Pattern.DOTALL );
	
	
	private final Matcher matcher= PATTERN.matcher("");
	
	
	public LineBreakSpan() {
	}
	
	
	@Override
	public @Nullable Inline createInline(final ProcessingContext context, final Cursor cursor) {
		final char c= cursor.getChar();
		if (c == '\n' || c == ' ') {
			final Matcher matcher= cursor.setup(this.matcher);
			if (matcher.matches()) {
				final int cursorLength= matcher.end(1) - matcher.regionStart();
				final int startOffset= cursor.getOffset();
				final int endOffset= cursor.getMatcherOffset(matcher.end(1));
				
				if (cursorLength > 2 || matcher.start(2) != -1) {
					return new HardLineBreak(cursor.getLineAtOffset(),
							startOffset, endOffset - startOffset, cursorLength );
				}
				return new SoftLineBreak(cursor.getLineAtOffset(),
						startOffset, endOffset - startOffset, cursorLength );
			}
		}
		return null;
	}
	
}
