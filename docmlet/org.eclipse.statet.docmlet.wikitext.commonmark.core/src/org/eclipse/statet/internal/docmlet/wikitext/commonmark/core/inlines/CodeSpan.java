/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Cursor;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class CodeSpan extends SourceSpan {
	
	
	private static final Pattern OPEN_PATTERN= Pattern.compile("(`+).*",
			Pattern.DOTALL );
	
	private static final Pattern CLOSE_PATTERN= Pattern.compile("`+",
			Pattern.DOTALL );
	
	private static final Pattern LINE_BREAK_PATTERN= Pattern.compile("\\n",
			Pattern.DOTALL );
	
	
	private @Nullable Matcher openMatcher;
	private @Nullable Matcher closeMatcher;
	private @Nullable Matcher lineBreakMatcher;
	
	
	public CodeSpan() {
	}
	
	
	@Override
	public @Nullable Inline createInline(final ProcessingContext context, final Cursor cursor) {
		final char c= cursor.getChar();
		if (c == '`') {
			final Matcher openMatcher= cursor.setup(getOpenMatcher());
			if (openMatcher.matches()) {
				final int startOffset= cursor.getOffset();
				final int backtickCount= openMatcher.end(1) - openMatcher.start(1);
				
				final Matcher closeMatcher= cursor.setup(getCloseMatcher(), backtickCount);
				
				while (closeMatcher.find()) {
					if (closeMatcher.end() - closeMatcher.start() == backtickCount) {
						final String codeText= getCodeText(
								cursor.getText(openMatcher.end(1), closeMatcher.start()) );
						final int cursorLength= closeMatcher.end() - openMatcher.regionStart();
						final int endOffset= cursor.getMatcherOffset(closeMatcher.end());
						
						return new Code(cursor.getLineAtOffset(),
								startOffset, endOffset - startOffset, cursorLength,
								codeText );
					}
				}
				
				if (backtickCount > 1) {
						return new Characters(cursor.getLineAtOffset(),
								startOffset, backtickCount, backtickCount,
								openMatcher.group(1) );
				}
			}
		}
		return null;
	}
	
	
	private Matcher getOpenMatcher() {
		Matcher matcher= this.openMatcher;
		if (matcher == null) {
			matcher= OPEN_PATTERN.matcher("");
			this.openMatcher= matcher;
		}
		return matcher;
	}
	
	private Matcher getCloseMatcher() {
		Matcher matcher= this.closeMatcher;
		if (matcher == null) {
			matcher= CLOSE_PATTERN.matcher("");
			this.closeMatcher= matcher;
		}
		return matcher;
	}
	
	private Matcher getLineBreakMatcher() {
		Matcher matcher= this.lineBreakMatcher;
		if (matcher == null) {
			matcher= LINE_BREAK_PATTERN.matcher("");
			this.lineBreakMatcher= matcher;
		}
		return matcher;
	}
	
	
	private String getCodeText(final String text) {
		final String codeText= getLineBreakMatcher().reset(text).replaceAll(" "); //$NON-NLS-1$
		final int l= codeText.length();
		if (l >= 3 && codeText.charAt(0) == ' ' && codeText.charAt(l - 1) == ' ') {
			return codeText.substring(1, l - 1);
		}
		return codeText;
	}
	
}
