/*=============================================================================#
 # Copyright (c) 2015, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.inlines;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.SpanType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.source.EmbeddingAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.ExtdocMarkupLanguage;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.CommonmarkLocator;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.Line;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.core.ProcessingContext;


@NonNullByDefault
public class HtmlTag extends InlineWithText {
	
	
	public HtmlTag(final Line line, final int offset, final int length, final String content) {
		super(line, offset, length, content.length(), content);
	}
	
	
	@Override
	public void emit(final ProcessingContext context,
			final CommonmarkLocator locator, final DocumentBuilder builder) {
		if (context.getMode() == ProcessingContext.PARSE_SOURCE_STRUCT) {
			final int descr= (getText().startsWith("<!--")) ?
					ExtdocMarkupLanguage.EMBEDDED_HTML_COMMENT_INLINE_DESCR :
					ExtdocMarkupLanguage.EMBEDDED_HTML_OTHER_INLINE_DESCR;
			
			builder.beginSpan(SpanType.CODE, new EmbeddingAttributes(
					ExtdocMarkupLanguage.EMBEDDED_HTML, descr ));
		}
		
		builder.charactersUnescaped(getText());
		
		if (context.getMode() == ProcessingContext.PARSE_SOURCE_STRUCT) {
			builder.endSpan();
		}
	}
	
}
