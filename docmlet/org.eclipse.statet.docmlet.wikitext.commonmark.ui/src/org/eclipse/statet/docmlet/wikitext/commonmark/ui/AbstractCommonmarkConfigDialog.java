/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.commonmark.ui;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.docmlet.wikitext.commonmark.core.BasicCommonmarkConfig;
import org.eclipse.statet.docmlet.wikitext.ui.config.AbstractMarkupConfigDialog;
import org.eclipse.statet.internal.docmlet.wikitext.commonmark.ui.Messages;


public class AbstractCommonmarkConfigDialog<T extends BasicCommonmarkConfig<? super T>> extends AbstractMarkupConfigDialog<T> {
	
	
	public AbstractCommonmarkConfigDialog(final Shell parent,
			final String contextLabel, final boolean isContextEnabled, final T customConfig) {
		super(parent, contextLabel, isContextEnabled, customConfig);
	}
	
	
	@Override
	protected void addProperty(final Composite parent, final String propertyName) {
		switch (propertyName) {
		case BasicCommonmarkConfig.HEADER_INTERRUPT_PARAGRAPH_DISABLED_PROP:
			addProperty(parent, propertyName,
					Messages.MarkupConfig_BlankBeforeHeader_Enable_label );
			return;
		case BasicCommonmarkConfig.BLOCKQUOTE_INTERRUPT_PARAGRAPH_DISABLED_PROP:
			addProperty(parent, propertyName,
					Messages.MarkupConfig_BlankBeforeBlockquote_Enable_label );
			return;
		case BasicCommonmarkConfig.STRIKEOUT_DTILDE_ENABLED_PROP:
			addProperty(parent, propertyName,
					Messages.MarkupConfig_StrikeoutByDTilde_Enable_label );
			return;
		case BasicCommonmarkConfig.SUPERSCRIPT_SCIRCUMFLEX_ENABLED_PROP:
			addProperty(parent, propertyName,
					Messages.MarkupConfig_SuperscriptBySCircumflex_Enable_label );
			return;
		case BasicCommonmarkConfig.SUBSCRIPT_STILDE_ENABLED_PROP:
			addProperty(parent, propertyName,
					Messages.MarkupConfig_SubscriptBySTilde_Enable_label );
			return;
		}
		super.addProperty(parent, propertyName);
	}
	
}
