/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.commonmark.ui;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String MarkupConfig_BlankBeforeHeader_Enable_label;
	public static String MarkupConfig_BlankBeforeBlockquote_Enable_label;
	public static String MarkupConfig_StrikeoutByDTilde_Enable_label;
	public static String MarkupConfig_SuperscriptBySCircumflex_Enable_label;
	public static String MarkupConfig_SubscriptBySTilde_Enable_label;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
