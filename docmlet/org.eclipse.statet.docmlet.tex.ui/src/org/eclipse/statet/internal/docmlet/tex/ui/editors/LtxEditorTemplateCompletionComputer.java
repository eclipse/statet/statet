/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.editors;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.templates.TemplateContextType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;
import org.eclipse.statet.internal.docmlet.tex.ui.TexUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;


@NonNullByDefault
public class LtxEditorTemplateCompletionComputer extends TemplateCompletionComputer {
	
	
	public LtxEditorTemplateCompletionComputer() {
		super(TexUIPlugin.getInstance().getLtxEditorTemplateStore(),
				TexUIPlugin.getInstance().getLtxEditorTemplateContextTypeRegistry() );
	}
	
	
	@Override
	protected String extractPrefix(final AssistInvocationContext context) {
		try {
			final IDocument document= context.getSourceViewer().getDocument();
			final int end= context.getInvocationOffset();
			final int start= Math.max(end-50, 0);
			final String text= document.get(start, end-start);
			int i= text.length()-1;
			while (i >= 0) {
				final char c= text.charAt(i);
				if (Character.isLetterOrDigit(c) || c == '.' || c == '_') {
					i--;
					continue;
				}
				if (c == '\\') {
					return text.substring(i, text.length());
				}
				return text.substring(i + 1, text.length());
			}
			return ""; //$NON-NLS-1$
		}
		catch (final BadLocationException e) {
			return ""; //$NON-NLS-1$
		}
	}
	
	@Override
	protected @Nullable TemplateContextType getContextType(
			final AssistInvocationContext context, final TextRegion region) {
		try {
			final SourceEditor editor= context.getEditor();
			final AbstractDocument document= (AbstractDocument) context.getSourceViewer().getDocument();
			final ITypedRegion partition= document.getPartition(
					editor.getDocumentContentInfo().getPartitioning(), region.getStartOffset(), true );
			if (partition.getType() == TexDocumentConstants.LTX_MATH_CONTENT_TYPE) {
				return getTypeRegistry().getContextType(LtxEditorContextType.LTX_MATH_CONTEXT_TYPE_ID);
			}
			else {
				return getTypeRegistry().getContextType(LtxEditorContextType.LTX_DEFAULT_CONTEXT_TYPE_ID);
			}
		}
		catch (final BadPartitioningException | BadLocationException e) {}
		return null;
	}
	
}
