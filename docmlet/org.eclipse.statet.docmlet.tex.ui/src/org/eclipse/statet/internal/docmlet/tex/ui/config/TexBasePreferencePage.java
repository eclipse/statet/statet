/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Link;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.PreferenceUIUtils;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;


@NonNullByDefault
public class TexBasePreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public TexBasePreferencePage() {
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() {
		return new TexBaseConfigurationBlock(createStatusChangedListener());
	}
	
}


@NonNullByDefault
class TexBaseConfigurationBlock extends ManagedConfigurationBlock {
	
	
	public TexBaseConfigurationBlock(final StatusChangeListener statusListener) {
		super(null, statusListener);
	}
	
	
	@Override
	public void createBlockArea(final Composite pageComposite) {
		{	final Composite composite= createEditorInfo(pageComposite);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		}
	}
	
	private Composite createEditorInfo(final Composite composite) {
		final Group group= new Group(composite, SWT.NONE);
		group.setLayout(LayoutUtils.newGroupGrid(1));
		group.setText(Messages.Base_Editors_label + ':');
		
		final Link link= addLinkControl(group, PreferenceUIUtils.composeSeeAlsoPreferencePages()
				.add("All <a>Text Editors</a> in Eclipse", "org.eclipse.ui.preferencePages.GeneralTextEditor+")
				.add("All <a>Source Editors of StatET</a>", "org.eclipse.statet.ltk.preferencePages.SourceEditors")
				.toString() );
		final GridData gd= new GridData(SWT.FILL, SWT.FILL, true, false);
		applyWrapWidth(gd);
		link.setLayoutData(gd);
		
		return group;
	}
	
}
