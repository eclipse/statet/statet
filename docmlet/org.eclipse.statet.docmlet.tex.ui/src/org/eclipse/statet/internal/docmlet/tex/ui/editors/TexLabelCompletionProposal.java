/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.SearchPattern;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.internal.docmlet.tex.ui.sourceediting.LtxAssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SourceProposal;


@NonNullByDefault
public class TexLabelCompletionProposal extends SourceProposal<LtxAssistInvocationContext> {
	
	
	public static class TexLabelProposalParameters extends ProposalParameters<LtxAssistInvocationContext> {
		
		
		public TexNameAccess access;
		
		
		@SuppressWarnings("null")
		public TexLabelProposalParameters(
				final LtxAssistInvocationContext context, final int replacementOffset,
				final SearchPattern namePattern) {
			super(context, replacementOffset, namePattern);
		}
		
	}
	
	
	protected final TexNameAccess access;
	
	
	protected TexLabelCompletionProposal(final TexLabelProposalParameters parameters) {
		super(parameters);
		
		this.access= nonNullAssert(parameters.access);
	}
	
	
	@Override
	protected String getName() {
		return this.access.getDisplayName();
	}
	
	@Override
	protected int computeReplacementLength(final int replacementOffset, final Point selection, final int caretOffset, final boolean overwrite) throws BadLocationException {
		int end= Math.max(caretOffset, selection.x + selection.y);
		if (overwrite) {
			final LtxAssistInvocationContext context= getInvocationContext();
			final IDocument document= context.getDocument();
			end--;
			SEARCH_END: while (++end < document.getLength()) {
				final char c= document.getChar(end);
				if (c <= 0x20 || c == '\\' || c == '{' || c == '}' || c == '%'
						|| Character.isWhitespace(c)) {
					break SEARCH_END;
				}
				continue SEARCH_END;
			}
		}
		return (end - replacementOffset);
	}
	
	
	@Override
	public String getSortingString() {
		return this.access.getSegmentName();
	}
	
	
	@Override
	public Image getImage() {
		return DocmlBaseUIResources.INSTANCE.getImage(DocmlBaseUIResources.OBJ_LABEL_TEXT_IMAGE_ID);
	}
	
	
	@Override
	public boolean isAutoInsertable() {
		return true;
	}
	
}
