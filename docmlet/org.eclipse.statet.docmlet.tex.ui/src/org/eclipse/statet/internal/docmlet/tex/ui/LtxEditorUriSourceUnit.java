/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitModelContainer;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxDocumentContentInfo;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.element.SourceDocumentRunnable;
import org.eclipse.statet.ltk.model.core.impl.GenericUriSourceUnit2;
import org.eclipse.statet.ltk.model.core.impl.WorkingBuffer;
import org.eclipse.statet.ltk.model.ui.impl.FileBufferWorkingBuffer;


@NonNullByDefault
public final class LtxEditorUriSourceUnit
		extends GenericUriSourceUnit2<LtxSourceUnitModelContainer<TexSourceUnit>>
		implements TexSourceUnit {
	
	
	public LtxEditorUriSourceUnit(final String id, final IFileStore store) {
		super(id, store);
	}
	
	@Override
	protected LtxSourceUnitModelContainer<TexSourceUnit> createModelContainer() {
		return new LtxSourceUnitModelContainer<>(this,
				TexUIPlugin.getInstance().getTexDocumentProvider() );
	}
	
	
	@Override
	public String getModelTypeId() {
		return TexModel.LTX_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return LtxDocumentContentInfo.INSTANCE;
	}
	
	@Override
	public WorkingContext getWorkingContext() {
		return Ltk.EDITOR_CONTEXT;
	}
	
	
	@Override
	public void syncExec(final SourceDocumentRunnable runnable) throws InvocationTargetException {
		FileBufferWorkingBuffer.syncExec(runnable);
	}
	
	
	@Override
	protected WorkingBuffer createWorkingBuffer(final SubMonitor m) {
		return new FileBufferWorkingBuffer(this);
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == TexCoreAccess.class) {
			return (T)TexCore.getWorkbenchAccess();
		}
		return super.getAdapter(adapterType);
	}
	
	
}
