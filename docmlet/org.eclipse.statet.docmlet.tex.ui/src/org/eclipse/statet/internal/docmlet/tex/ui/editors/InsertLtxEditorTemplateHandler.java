/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.editors;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.docmlet.tex.ui.TexUIPlugin;
import org.eclipse.statet.ltk.ui.sourceediting.assist.InsertEditorTemplateHandler;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;


@NonNullByDefault
public class InsertLtxEditorTemplateHandler extends InsertEditorTemplateHandler {
	
	
	/** plugin.xml */
	public InsertLtxEditorTemplateHandler() {
	}
	
	
	@Override
	protected TemplateCompletionComputer getComputer() {
		return (TemplateCompletionComputer) TexUIPlugin.getInstance().getLtxEditorContentAssistRegistry()
				.getComputer("org.eclipse.statet.docmlet.contentAssistComputers.TexTemplateCompletion"); //$NON-NLS-1$
	}
	
}
