/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.config;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils;

import org.eclipse.statet.docmlet.tex.core.commands.LtxCommandCategories;
import org.eclipse.statet.docmlet.tex.core.commands.LtxCommandCategories.Category;
import org.eclipse.statet.docmlet.tex.core.commands.LtxCommandDefinitions;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet;
import org.eclipse.statet.docmlet.tex.ui.TexCommandLabelProvider;


public class LtxCommandsPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public LtxCommandsPreferencePage() {
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() {
		return new TexCommandsConfigurationBlock(createStatusChangedListener());
	}
	
}


class TexCommandsConfigurationBlock extends ManagedConfigurationBlock {
	
	
	private static class CatContentProvider implements ITreeContentProvider {
		
		private LtxCommandCategories categories;
		
		@Override
		public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
			this.categories= (LtxCommandCategories) newInput;
		}
		
		@Override
		public Object[] getElements(final Object inputElement) {
			final List<Category> categories= this.categories.getCategories();
			return categories.toArray(new Category[categories.size()]);
		}
		
		@Override
		public boolean hasChildren(final Object element) {
			return (element instanceof Category);
		}
		
		@Override
		public Object[] getChildren(final Object parentElement) {
			final List<TexCommand> commands= ((Category) parentElement).getCommands();
			return commands.toArray(new TexCommand[commands.size()]);
		}
		
		@Override
		public Object getParent(final Object element) {
			if (element instanceof TexCommand) {
				return this.categories.getCategory((TexCommand) element);
			}
			return null;
		}
		
		@Override
		public void dispose() {
			this.categories= null;
		}
		
	}
	
	
	private static final int DETAIL_CHECK_ASSIST_TEXT= 0;
	private static final int DETAIL_CHECK_ASSIST_MATH= 1;
	private static final int DETAIL_CHECK_SIZE= 2;
	
	private CheckboxTreeViewer treeViewer;
	private final Button[] detailCheckControls= new Button[DETAIL_CHECK_SIZE];
	
	private final Preference<Set<String>>[] detailCheckPrefs= new Preference[DETAIL_CHECK_SIZE];
	
	private final Set<String> enabled= new HashSet<>();
	private final Set<String>[] detailCheckEnabled= new HashSet[DETAIL_CHECK_SIZE];
	
	
	public TexCommandsConfigurationBlock(final StatusChangeListener statusListener) {
		super(null, statusListener);
		
		for (int i= 0; i < DETAIL_CHECK_SIZE; i++) {
			this.detailCheckEnabled[i]= new HashSet<>();
		}
	}
	
	
	@Override
	public void createBlockArea(final Composite pageComposite) {
		final Map<Preference<?>, String> prefs= new HashMap<>();
		
		prefs.put(TexCommandSet.MASTER_COMMANDS_INCLUDE_PREF, null);
		prefs.put(this.detailCheckPrefs[DETAIL_CHECK_ASSIST_TEXT]= TexCommandSet.TEXT_COMMANDS_INCLUDE_PREF, null);
		prefs.put(this.detailCheckPrefs[DETAIL_CHECK_ASSIST_MATH]= TexCommandSet.MATH_COMMANDS_INCLUDE_PREF, null);
		
		setupPreferenceManager(prefs);
		
		final LtxCommandCategories categories= new LtxCommandCategories(LtxCommandDefinitions.getAllCommands());
		
		final Composite composite= new Composite(pageComposite, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(LayoutUtils.newCompositeGrid(2));
		
		{	final Control tree= createTree(categories, composite);
			tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		}
		{	final Control detail= createDetailComposite(composite);
			detail.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		}
		
		// Binding
		updateControls();
		
		this.treeViewer.getTree().select(this.treeViewer.getTree().getItem(0));
	}
	
	private Control createTree(final LtxCommandCategories categories, final Composite composite) {
		this.treeViewer= new CheckboxTreeViewer(composite, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		this.treeViewer.setContentProvider(new CatContentProvider());
		this.treeViewer.addCheckStateListener(
				(final CheckStateChangedEvent event) -> {
					final Object element= event.getElement();
					Category category= null;
					if (element instanceof Category) {
						category= (Category) element;
						final List<TexCommand> commands= category.getCommands();
						if (event.getChecked()) {
							for (final TexCommand command : commands) {
								TexCommandsConfigurationBlock.this.enabled.add(command.getControlWord());
							}
						}
						else {
							for (final TexCommand command : commands) {
								TexCommandsConfigurationBlock.this.enabled.remove(command.getControlWord());
							}
						}
					}
					else if (element instanceof final TexCommand command) {
						if (event.getChecked()) {
							TexCommandsConfigurationBlock.this.enabled.add(command.getControlWord());
						}
						else {
							TexCommandsConfigurationBlock.this.enabled.remove(command.getControlWord());
						}
						category= categories.getCategory(command);
					}
					if (category != null) {
						TexCommandsConfigurationBlock.this.treeViewer.refresh(category, true);
					}
				} );
		this.treeViewer.setCheckStateProvider(new ICheckStateProvider() {
			@Override
			public boolean isChecked(final Object element) {
				if (element instanceof Category) {
					final List<TexCommand> commands= ((Category) element).getCommands();
					for (final TexCommand command : commands) {
						if (TexCommandsConfigurationBlock.this.enabled.contains(command.getControlWord())) {
							return true;
						}
					}
					return false;
				}
				if (element instanceof TexCommand) {
					return (TexCommandsConfigurationBlock.this.enabled.contains(((TexCommand) element).getControlWord()));
				}
				return false;
			}
			@Override
			public boolean isGrayed(final Object element) {
				if (element instanceof Category) {
					int check= 0x0;
					final List<TexCommand> commands= ((Category) element).getCommands();
					for (final TexCommand command : commands) {
						check |= TexCommandsConfigurationBlock.this.enabled.contains(command.getControlWord()) ? 0x1 : 0x2;
						if (check == (0x1 | 0x2)) {
							return true;
						}
					}
				}
				return false;
			}
		});
		this.treeViewer.setLabelProvider(new TexCommandLabelProvider());
		this.treeViewer.setInput(categories);
		
		ViewerUtils.addDoubleClickExpansion(this.treeViewer);
		
		return this.treeViewer.getControl();
	}
	
	protected Object[] getSelectedElements() {
		return ((IStructuredSelection) this.treeViewer.getSelection()).toArray();
	}
	
	protected Control createDetailComposite(final Composite parent) {
		final Composite composite= new Composite(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newCompositeGrid(1));
		
		{	final Label label= new Label(composite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
			label.setText("Region:");
		}
		{	final Button button= new Button(composite, SWT.CHECK);
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, true, false);
			gd.horizontalIndent= LayoutUtils.defaultSmallIndent();
			button.setLayoutData(gd);
			button.setText("&Text");
			
			this.detailCheckControls[DETAIL_CHECK_ASSIST_TEXT]= button;
			registerDetailCheck(DETAIL_CHECK_ASSIST_TEXT);
		}
		{	final Button button= new Button(composite, SWT.CHECK);
			final GridData gd= new GridData(SWT.FILL, SWT.CENTER, true, false);
			gd.horizontalIndent= LayoutUtils.defaultSmallIndent();
			button.setLayoutData(gd);
			button.setText("&Math");
			
			this.detailCheckControls[DETAIL_CHECK_ASSIST_MATH]= button;
			registerDetailCheck(DETAIL_CHECK_ASSIST_MATH);
		}
		
		LayoutUtils.addSmallFiller(composite, true);
		
		return composite;
	}
	
	private void registerDetailCheck(final int id) {
		final Button button= this.detailCheckControls[id];
		final Set<String> enabled= this.detailCheckEnabled[id];
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				button.setGrayed(false);
				final Object[] elements= getSelectedElements();
				if (button.getSelection()) {
					for (final Object element : elements) {
						if (element instanceof Category) {
							final List<TexCommand> commands= ((Category) element).getCommands();
							for (final TexCommand command : commands) {
								enabled.add(command.getControlWord());
							}
						}
						else if (element instanceof TexCommand) {
							enabled.add(((TexCommand) element).getControlWord());
						}
					}
				}
				else {
					for (final Object element : elements) {
						if (element instanceof Category) {
							final List<TexCommand> commands= ((Category) element).getCommands();
							for (final TexCommand command : commands) {
								enabled.remove(command.getControlWord());
							}
						}
						else if (element instanceof TexCommand) {
							enabled.remove(((TexCommand) element).getControlWord());
						}
					}
				}
			}
		});
		
		this.treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				updateDetail();
			}
		});
	}
	
	private void updateDetail() {
		final Object[] elements= getSelectedElements();
		if (elements.length == 0) {
			for (int i= 0; i < DETAIL_CHECK_SIZE; i++) {
				this.detailCheckControls[i].setSelection(false);
				this.detailCheckControls[i].setGrayed(false);
				this.detailCheckControls[i].setEnabled(false);
			}
		}
		
		final int[] state= new int[DETAIL_CHECK_SIZE];
		for (final Object element : elements) {
			if (element instanceof Category) {
				final List<TexCommand> commands= ((Category) element).getCommands();
				for (final TexCommand command : commands) {
					for (int i= 0; i < DETAIL_CHECK_SIZE; i++) {
						state[i]= this.detailCheckEnabled[i].contains(command.getControlWord()) ? 0x1 : 0x2;
					}
				}
			}
			else if (element instanceof final TexCommand command) {
				for (int i= 0; i < DETAIL_CHECK_SIZE; i++) {
					state[i]= this.detailCheckEnabled[i].contains(command.getControlWord()) ? 0x1 : 0x2;
				}
			}
		}
		
		for (int i= 0; i < DETAIL_CHECK_SIZE; i++) {
			this.detailCheckControls[i].setEnabled(true);
			if (state[i] == 0x3) {
				this.detailCheckControls[i].setSelection(true);
				this.detailCheckControls[i].setGrayed(true);
			}
			else {
				this.detailCheckControls[i].setSelection(state[i] == 0x1);
				this.detailCheckControls[i].setGrayed(false);
			}
		}
	}
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
	}
	
	
	@Override
	protected void updateControls() {
		super.updateControls();
		this.enabled.clear();
		this.enabled.addAll(getPreferenceValue(TexCommandSet.MASTER_COMMANDS_INCLUDE_PREF));
		for (int i= 0; i < DETAIL_CHECK_SIZE; i++) {
			this.detailCheckEnabled[i].clear();
			this.detailCheckEnabled[i].addAll(getPreferenceValue(this.detailCheckPrefs[i]));
		}
		this.treeViewer.refresh();
		updateDetail();
	}
	
	private void save() {
		setPrefValue(TexCommandSet.MASTER_COMMANDS_INCLUDE_PREF, this.enabled);
		for (int i= 0; i < DETAIL_CHECK_SIZE; i++) {
			setPrefValue(this.detailCheckPrefs[i], this.detailCheckEnabled[i]);
		}
	}
	
	@Override
	public boolean performOk(final int flags) {
		save();
		return super.performOk(flags);
	}
	
}
