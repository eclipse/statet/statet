/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.editors;

import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.source.IAnnotationModel;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.PreferencesUtil;
import org.eclipse.statet.ecommons.preferences.SettingsChangeNotifier;
import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxDocumentSetupParticipant;
import org.eclipse.statet.docmlet.tex.ui.editors.TexEditorBuild;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.ui.sourceediting.SourceAnnotationModel;
import org.eclipse.statet.ltk.ui.sourceediting.SourceDocumentProvider;


@NonNullByDefault
public class LtxDocumentProvider extends SourceDocumentProvider<TexSourceUnit>
		implements Disposable {
	
	
	private class ThisAnnotationModel extends SourceAnnotationModel {
		
		public ThisAnnotationModel(final IResource resource) {
			super(resource, LtxDocumentProvider.this.getIssueTypeSet());
		}
		
		@Override
		protected boolean isHandlingTemporaryProblems(final IssueTypeSet.ProblemCategory issueCategory) {
			return LtxDocumentProvider.this.handleTemporaryProblems;
		}
		
	}
	
	
	private SettingsChangeNotifier. @Nullable ChangeListener editorPrefListener;
	
	private boolean handleTemporaryProblems;
	
	
	public LtxDocumentProvider() {
		super(TexModel.LTX_TYPE_ID, new LtxDocumentSetupParticipant(),
				TexEditorBuild.LTX_ISSUE_TYPE_SET );
		
		{	final var editorPrefListener= new SettingsChangeNotifier.ChangeListener() {
				@Override
				public void settingsChanged(final Set<String> groupIds) {
					if (groupIds.contains(TexEditorBuild.GROUP_ID)) {
						updateEditorPrefs();
					}
				}
			};
			this.editorPrefListener= editorPrefListener;
			PreferencesUtil.getSettingsChangeNotifier().addChangeListener(editorPrefListener);
		}
		final PreferenceAccess access= EPreferences.getInstancePrefs();
		this.handleTemporaryProblems= access.getPreferenceValue(TexEditorBuild.PROBLEMCHECKING_ENABLED_PREF);
	}
	
	
	@Override
	public void dispose() {
		{	final var editorPrefListener= this.editorPrefListener;
			if (editorPrefListener != null) {
				this.editorPrefListener= null;
				PreferencesUtil.getSettingsChangeNotifier().removeChangeListener(editorPrefListener);
			}
		}
	}
	
	private void updateEditorPrefs() {
		final PreferenceAccess access= EPreferences.getInstancePrefs();
		final boolean newHandleTemporaryProblems= access.getPreferenceValue(TexEditorBuild.PROBLEMCHECKING_ENABLED_PREF);
		if (this.handleTemporaryProblems != newHandleTemporaryProblems) {
			this.handleTemporaryProblems= newHandleTemporaryProblems;
			TexModel.getLtxModelManager().refresh(Ltk.EDITOR_CONTEXT);
		}
	}
	
	@Override
	protected IAnnotationModel createAnnotationModel(final IFile file) {
		return new ThisAnnotationModel(file);
	}
	
	
}
