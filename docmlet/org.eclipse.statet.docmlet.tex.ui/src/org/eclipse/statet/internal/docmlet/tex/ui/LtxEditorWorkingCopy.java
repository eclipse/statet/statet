/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.docmlet.tex.core.model.TexWorkspaceSourceUnit;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.ui.impl.GenericEditorWorkspaceSourceUnitWorkingCopy2;


@NonNullByDefault
public final class LtxEditorWorkingCopy
		extends GenericEditorWorkspaceSourceUnitWorkingCopy2<LtxSourceUnitModelContainer<TexSourceUnit>>
		implements TexWorkspaceSourceUnit {
	
	
	public LtxEditorWorkingCopy(final WorkspaceSourceUnit from) {
		super(from);
	}
	
	@Override
	protected LtxSourceUnitModelContainer<TexSourceUnit> createModelContainer() {
		return new LtxSourceUnitModelContainer<>(this,
				TexUIPlugin.getInstance().getTexDocumentProvider() );
	}
	
	
}
