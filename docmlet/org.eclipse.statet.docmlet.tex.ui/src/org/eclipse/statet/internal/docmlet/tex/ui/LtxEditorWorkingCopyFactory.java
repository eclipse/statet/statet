/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui;

import org.eclipse.core.filesystem.IFileStore;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.core.impl.AbstractEditorSourceUnitFactory;


/**
 * Source unit factory for LaTeX editor context
 */
@NonNullByDefault
public final class LtxEditorWorkingCopyFactory extends AbstractEditorSourceUnitFactory {
	
	
	public LtxEditorWorkingCopyFactory() {
	}
	
	
	@Override
	protected SourceUnit createSourceUnit(final String id, final WorkspaceSourceUnit su) {
		return new LtxEditorWorkingCopy(su);
	}
	
	@Override
	protected SourceUnit createSourceUnit(final String id, final IFileStore file) {
		return new LtxEditorUriSourceUnit(id, file);
	}
	
}
