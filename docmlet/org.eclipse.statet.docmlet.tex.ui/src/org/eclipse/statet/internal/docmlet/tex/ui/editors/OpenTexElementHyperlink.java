/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.editors;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;

import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.actions.OpenDeclaration;


public class OpenTexElementHyperlink implements IHyperlink {
	
	
	private final SourceEditor editor;
	private final IRegion region;
	
	private final TexSourceUnit sourceUnit;
	private final TexNameAccess access;
	
	
	public OpenTexElementHyperlink(final SourceEditor editor, final TexSourceUnit sourceUnit,
			final TexNameAccess access) {
		assert (sourceUnit != null);
		assert (access != null);
		
		this.editor= editor;
		this.region= JFaceTextRegion.toJFaceRegion(
				TexNameAccess.getTextRegion(access.getNameNode()) );
		this.sourceUnit= sourceUnit;
		this.access= access;
	}
	
	
	@Override
	public String getTypeLabel() {
		return null;
	}
	
	@Override
	public IRegion getHyperlinkRegion() {
		return this.region;
	}
	
	@Override
	public String getHyperlinkText() {
		return NLS.bind(Messages.Hyperlinks_OpenDeclaration_label, this.access.getDisplayName());
	}
	
	@Override
	public void open() {
//		try {
			final OpenDeclaration open= new OpenDeclaration();
			final TexNameAccess declAccess= open.selectAccess(this.access.getAllInUnit());
			if (declAccess != null) {
				open.open(this.editor, declAccess);
				return;
			}
			Display.getCurrent().beep();
//		}
//		catch (final PartInitException e) {
//			Display.getCurrent().beep();
//			StatusManager.getManager().handle(new Status(IStatus.INFO, SharedUIResources.BUNDLE_ID, -1,
//					NLS.bind("An error occurred when following the hyperlink and opening the editor for the declaration of ''{0}''", access.getDisplayName()), e));
//		}
	}
	
}
