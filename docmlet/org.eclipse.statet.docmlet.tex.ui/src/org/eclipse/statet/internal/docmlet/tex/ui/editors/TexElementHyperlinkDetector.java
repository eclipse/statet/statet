/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.hyperlink.AbstractHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;

import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.docmlet.tex.ui.actions.LtxOpenDeclarationHandler;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public class TexElementHyperlinkDetector extends AbstractHyperlinkDetector {
	
	
	public TexElementHyperlinkDetector() {
	}
	
	
	@Override
	public @NonNull IHyperlink @Nullable [] detectHyperlinks(final ITextViewer textViewer,
			final IRegion region, final boolean canShowMultipleHyperlinks) {
		final SourceEditor editor= getAdapter(SourceEditor.class);
		if (editor == null) {
			return null;
		}
		
		final List<IHyperlink> hyperlinks= new ArrayList<>(4);
		
		{	final TexNameAccess access= LtxOpenDeclarationHandler.searchAccess(editor,
					JFaceTextRegion.toTextRegion(region) );
			if (access != null) {
				hyperlinks.add(new OpenTexElementHyperlink(editor,
						(TexSourceUnit) editor.getSourceUnit(), access));
			}
		}
		
		if (!hyperlinks.isEmpty()) {
			return hyperlinks.toArray(new @NonNull IHyperlink[hyperlinks.size()]);
		}
		return null;
	}
	
}
