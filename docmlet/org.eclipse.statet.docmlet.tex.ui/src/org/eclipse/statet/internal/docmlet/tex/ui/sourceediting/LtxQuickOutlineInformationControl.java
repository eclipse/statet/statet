/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.sourceediting;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SearchPattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.content.ITextElementFilter;
import org.eclipse.statet.ecommons.ui.content.TextElementFilter;

import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.ui.util.TexNameSearchPattern;
import org.eclipse.statet.ltk.ui.sourceediting.QuickOutlineInformationControl;
import org.eclipse.statet.ltk.ui.sourceediting.actions.OpenDeclaration;


@NonNullByDefault
public class LtxQuickOutlineInformationControl extends QuickOutlineInformationControl {
	
	
	public LtxQuickOutlineInformationControl(final Shell parent, final String commandId) {
		super(parent, commandId, 1, new OpenDeclaration());
	}
	
	
	@Override
	public String getModelTypeId(final int page) {
		return TexModel.LTX_TYPE_ID;
	}
	
	@Override
	protected ITextElementFilter createNameFilter() {
		return new TextElementFilter() {
			@Override
			protected SearchPattern createSearchPattern() {
				return new TexNameSearchPattern();
			}
		};
	}
	
}
