/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui;

import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_BOLD_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_COLOR_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_ITALIC_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_STRIKETHROUGH_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_UNDERLINE_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_USE_SUFFIX;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;
import org.eclipse.statet.ecommons.workbench.ui.IWaThemeConstants;
import org.eclipse.statet.ecommons.workbench.ui.util.ThemeUtil;

import org.eclipse.statet.docmlet.tex.ui.TexUI;
import org.eclipse.statet.docmlet.tex.ui.editors.TexEditorBuild;
import org.eclipse.statet.docmlet.tex.ui.sourceediting.TexEditingSettings;
import org.eclipse.statet.docmlet.tex.ui.text.TexTextStyles;
import org.eclipse.statet.ltk.ui.sourceediting.SmartInsertSettings.TabAction;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;


public class TexUIPreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	public TexUIPreferenceInitializer() {
	}
	
	
	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store= TexUIPlugin.getInstance().getPreferenceStore();
		final IScopeContext scope= DefaultScope.INSTANCE;
		final IEclipsePreferences pref= scope.getNode(TexUI.BUNDLE_ID);
		final ThemeUtil theme= new ThemeUtil();
		
		EditorsUI.useAnnotationsPreferencePage(store);
		EditorsUI.useQuickDiffPreferencePage(store);
		
		String color;
		pref.put(TexTextStyles.TS_DEFAULT + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_DEFAULT_COLOR) );
		pref.putBoolean(TexTextStyles.TS_DEFAULT + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_DEFAULT + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_DEFAULT + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_DEFAULT + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(TexTextStyles.TS_CONTROL_WORD + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_DOC_COMMAND_COLOR) );
		pref.putBoolean(TexTextStyles.TS_CONTROL_WORD + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CONTROL_WORD + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CONTROL_WORD + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CONTROL_WORD + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(TexTextStyles.TS_CONTROL_WORD_SUB_SECTIONING + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue("org.eclipse.statet.docmlet.themes.SectionColor") ); //$NON-NLS-1$
		pref.putBoolean(TexTextStyles.TS_CONTROL_WORD_SUB_SECTIONING + TEXTSTYLE_BOLD_SUFFIX, true);
		pref.putBoolean(TexTextStyles.TS_CONTROL_WORD_SUB_SECTIONING + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CONTROL_WORD_SUB_SECTIONING + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CONTROL_WORD_SUB_SECTIONING + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(TexTextStyles.TS_CONTROL_CHAR + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_DOC_COMMAND_SPECIAL_COLOR) );
		pref.putBoolean(TexTextStyles.TS_CONTROL_CHAR + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CONTROL_CHAR + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CONTROL_CHAR + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CONTROL_CHAR + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		color= theme.getColorPrefValue(IWaThemeConstants.CODE_SUB_COLOR);
		pref.put(TexTextStyles.TS_CURLY_BRACKETS + TEXTSTYLE_COLOR_SUFFIX, color);
		pref.putBoolean(TexTextStyles.TS_CURLY_BRACKETS + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CURLY_BRACKETS + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CURLY_BRACKETS + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_CURLY_BRACKETS + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
//		pref.put(TexTextStyles.TS_SQUARED_BRACKETS + TEXTSTYLE_COLOR_SUFFIX, color);
//		pref.putBoolean(TexTextStyles.TS_SQUARED_BRACKETS + TEXTSTYLE_BOLD_SUFFIX, false);
//		pref.putBoolean(TexTextStyles.TS_SQUARED_BRACKETS + TEXTSTYLE_ITALIC_SUFFIX, false);
//		pref.putBoolean(TexTextStyles.TS_SQUARED_BRACKETS + TEXTSTYLE_UNDERLINE_SUFFIX, false);
//		pref.putBoolean(TexTextStyles.TS_SQUARED_BRACKETS + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
//		
		pref.put(TexTextStyles.TS_MATH + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_DOC_2ND_COLOR) );
		pref.putBoolean(TexTextStyles.TS_MATH + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(TexTextStyles.TS_MATH_CONTROL_WORD + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_DOC_2ND_COMMAND_COLOR) );
		pref.putBoolean(TexTextStyles.TS_MATH_CONTROL_WORD + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH_CONTROL_WORD + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH_CONTROL_WORD + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH_CONTROL_WORD + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(TexTextStyles.TS_MATH_CONTROL_CHAR + TEXTSTYLE_USE_SUFFIX, TexTextStyles.TS_MATH_CONTROL_WORD);
		
		pref.put(TexTextStyles.TS_MATH_CURLY_BRACKETS + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_DOC_2ND_SUB_COLOR) );
		pref.putBoolean(TexTextStyles.TS_MATH_CURLY_BRACKETS + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH_CURLY_BRACKETS + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH_CURLY_BRACKETS + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_MATH_CURLY_BRACKETS + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(TexTextStyles.TS_VERBATIM + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_VERBATIM_COLOR) );
		pref.putBoolean(TexTextStyles.TS_VERBATIM + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_VERBATIM + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_VERBATIM + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_VERBATIM + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(TexTextStyles.TS_COMMENT + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_COMMENT_COLOR) );
		pref.putBoolean(TexTextStyles.TS_COMMENT + TEXTSTYLE_BOLD_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_COMMENT + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_COMMENT + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_COMMENT + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		pref.put(TexTextStyles.TS_TASK_TAG + TEXTSTYLE_COLOR_SUFFIX,
				theme.getColorPrefValue(IWaThemeConstants.CODE_COMMENT_TASKTAG_COLOR) );
		pref.putBoolean(TexTextStyles.TS_TASK_TAG + TEXTSTYLE_BOLD_SUFFIX, true);
		pref.putBoolean(TexTextStyles.TS_TASK_TAG + TEXTSTYLE_ITALIC_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_TASK_TAG + TEXTSTYLE_UNDERLINE_SUFFIX, false);
		pref.putBoolean(TexTextStyles.TS_TASK_TAG + TEXTSTYLE_STRIKETHROUGH_SUFFIX, false);
		
		{	final IEclipsePreferences node= scope.getNode(TexEditingSettings.ASSIST_LTX_PREF_QUALIFIER);
			node.put(ContentAssistComputerRegistry.CIRCLING_ORDERED, "tex-elements:false,templates:true,paths:true"); //$NON-NLS-1$
			node.put(ContentAssistComputerRegistry.DEFAULT_DISABLED, ""); //$NON-NLS-1$
		}
		// EditorPreferences
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.FOLDING_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.FOLDING_RESTORE_STATE_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.MARKOCCURRENCES_ENABLED_PREF, Boolean.TRUE);
		
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.SMARTINSERT_BYDEFAULT_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.SMARTINSERT_TAB_ACTION_PREF, TabAction.INSERT_INDENT_LEVEL);
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.SMARTINSERT_CLOSEBRACKETS_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.SMARTINSERT_CLOSEPARENTHESIS_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.SMARTINSERT_CLOSEMATHDOLLAR_ENABLED_PREF, Boolean.TRUE);
		PreferenceUtils.setPrefValue(scope, TexEditingSettings.SMARTINSERT_HARDWRAP_TEXT_ENABLED_PREF, Boolean.TRUE);
		
		PreferenceUtils.setPrefValue(scope, TexEditorBuild.PROBLEMCHECKING_ENABLED_PREF, Boolean.TRUE);
		
	}
	
}
