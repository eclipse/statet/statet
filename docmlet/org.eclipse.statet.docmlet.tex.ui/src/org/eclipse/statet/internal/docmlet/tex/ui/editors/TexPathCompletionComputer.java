/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.editors;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.ast.TexAsts;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;
import org.eclipse.statet.internal.docmlet.tex.ui.sourceediting.LtxAssistInvocationContext;
import org.eclipse.statet.internal.docmlet.tex.ui.sourceediting.LtxAssistInvocationContext.CommandCall;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.PathCompletionComputor;


/**
 * Completion computer for path in R code.
 * 
 * Supports workspace properties of a R tool process
 */
@NonNullByDefault
public class TexPathCompletionComputer extends PathCompletionComputor {
	
	
	private @Nullable IContainer baseResource;
	private @Nullable IFileStore baseFileStore;
	
	
	public TexPathCompletionComputer() {
	}
	
	
	@Override
	public void onSessionStarted(final SourceEditor editor, final ContentAssist assist) {
		this.baseResource= null;
		this.baseFileStore= null;
		{
			final SourceUnit su= editor.getSourceUnit();
			if (su instanceof WorkspaceSourceUnit) {
				final IResource resource= ((WorkspaceSourceUnit) su).getResource();
				if (this.baseResource == null) {
					this.baseResource= resource.getParent();
				}
				if (this.baseResource != null) {
					try {
						this.baseFileStore= EFS.getStore(this.baseResource.getLocationURI());
					}
					catch (final CoreException e) {
					}
				}
			}
		}
		
		super.onSessionStarted(editor, assist);
	}
	
	@Override
	public void onSessionEnded() {
		super.onSessionEnded();
	}
	
	@Override
	protected char getDefaultFileSeparator() {
		return '/';
	}
	
	@Override
	protected @Nullable TextRegion getContentRegion(final AssistInvocationContext context, final int mode)
			throws BadLocationException {
		if (context instanceof final LtxAssistInvocationContext texContext) {
			final CommandCall commandCall= texContext.getCommandCall(true);
			final int argParameterIndex;
			final TexAstNode argNode;
			if (commandCall != null
					&& (argParameterIndex= commandCall.getInvocationArgParameterIndex()) >= 0
					&& (argNode= commandCall.getArgNode(argParameterIndex)) != null) {
				final TexCommand command= commandCall.getCommand();
				final Parameter param= command.getParameters().get(argParameterIndex);
				final int offset= texContext.getInvocationOffset();
				if (mode == ContentAssistComputer.SPECIFIC_MODE
								|| (param.getContent() & 0xf0) == Parameter.RESOURCE ) {
					final TextRegion region= TexAsts.getInnerRegion(argNode);
					if (region != null
							&& region.getStartOffset() >= offset && offset <= region.getEndOffset() ) {
						return region;
					}
				}
			}
		}
		return null;
	}
	
	@Override
	protected @Nullable IPath getRelativeBasePath() {
		final IContainer baseResource= this.baseResource;
		if (baseResource != null) {
			return baseResource.getLocation();
		}
		return null;
	}
	
	@Override
	protected @Nullable IFileStore getRelativeBaseStore() {
		final IFileStore baseFileStore= this.baseFileStore;
		if (baseFileStore != null) {
			return baseFileStore;
		}
		return null;
	}
	
}
