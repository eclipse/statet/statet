/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.config;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.templates.TemplateContextType;

import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitioner;

import org.eclipse.statet.docmlet.tex.core.source.doc.LtxPartitionNodeType;
import org.eclipse.statet.docmlet.tex.ui.sourceediting.LtxTemplateSourceViewerConfigurator;
import org.eclipse.statet.internal.docmlet.tex.ui.TexUIPlugin;
import org.eclipse.statet.internal.docmlet.tex.ui.editors.LtxEditorContextType;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.templates.config.AbstractTemplatesPreferencePage;


public class LtxEditorTemplatesPreferencePage extends AbstractTemplatesPreferencePage {
	
	
	public LtxEditorTemplatesPreferencePage() {
		super(TexUIPlugin.getInstance().getPreferenceStore(),
				TexUIPlugin.getInstance().getLtxEditorTemplateStore() );
	}
	
	
	@Override
	protected SourceEditorViewerConfigurator createSourceViewerConfigurator(
			final TemplateVariableProcessor templateProcessor) {
		return new LtxTemplateSourceViewerConfigurator(null, templateProcessor);
	}
	
	
	@Override
	protected void configureContext(final AbstractDocument document,
			final TemplateContextType contextType, final SourceEditorViewerConfigurator configurator) {
		final String partitioning= configurator.getDocumentContentInfo().getPartitioning();
		final TreePartitioner partitioner= (TreePartitioner) document.getDocumentPartitioner(partitioning);
		if (contextType.getId().equals(LtxEditorContextType.LTX_MATH_CONTEXT_TYPE_ID)) {
			partitioner.setStartType(LtxPartitionNodeType.MATH_ENV_xxx);
		}
		else {
			partitioner.setStartType(LtxPartitionNodeType.DEFAULT_ROOT);
		}
		partitioner.disconnect();
		partitioner.connect(document);
		document.setDocumentPartitioner(partitioning, partitioner);
	}
	
}
