/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.ui.sourceediting;

import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.link.LinkedModeModel;
import org.eclipse.jface.text.link.LinkedPosition;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.assist.LinkedModeBracketLevel;

import org.eclipse.statet.docmlet.tex.core.source.util.LtxHeuristicTokenScanner;


@NonNullByDefault
public class TexBracketLevel extends LinkedModeBracketLevel {
	
	
	public static final class CurlyBracketPosition extends InBracketPosition {
		
		public CurlyBracketPosition(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '{';
		}
		
		@Override
		public char getCloseChar() {
			return '}';
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			return LtxHeuristicTokenScanner.isEscaped(getDocument(), offset);
		}
		
	}
	
	public static final class SquareBracketPosition extends InBracketPosition {
		
		public SquareBracketPosition(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '[';
		}
		
		@Override
		public char getCloseChar() {
			return ']';
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			return LtxHeuristicTokenScanner.isEscaped(getDocument(), offset);
		}
		
	}
	
	public static final class ParanthesisBracket extends InBracketPosition {
		
		public ParanthesisBracket(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '(';
		}
		
		@Override
		public char getCloseChar() {
			return ')';
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			return LtxHeuristicTokenScanner.isEscaped(getDocument(), offset);
		}
		
	}
	
	public static final class MathDollarBracket extends InBracketPosition {
		
		public MathDollarBracket(final IDocument document, final int offset, final int length,
				final int sequence) {
			super(document, offset, length, sequence);
		}
		
		@Override
		public char getOpenChar() {
			return '$';
		}
		
		@Override
		public char getCloseChar() {
			return '$';
		}
		
		@Override
		protected boolean isEscaped(final int offset) throws BadLocationException {
			return LtxHeuristicTokenScanner.isEscaped(getDocument(), offset);
		}
		
		private int countBackward(int offset) throws BadLocationException {
			final IDocument document= getDocument();
			int count= 0;
			while (offset > 0) {
				if (document.getChar(offset--) == '$') {
					count++;
				}
			}
			return count;
		}
		
		@Override
		public boolean matchesClose(final LinkedModeBracketLevel level, final int offset, final char character)
				throws BadLocationException {
			return (super.matchesClose(level, offset, character)
					&& (getLength() > 0 || countBackward(offset-1) == 2) );
		}
		
	}
	
	
	public static InBracketPosition createPosition(final char c, final IDocument document,
			final int offset, final int length, final int sequence) {
		return switch (c) {
		case '{' -> new CurlyBracketPosition(document, offset, length, sequence);
		case '[' -> new SquareBracketPosition(document, offset, length, sequence);
		case '(' -> new ParanthesisBracket(document, offset, length, sequence);
		case '$' -> new MathDollarBracket(document, offset, length, sequence);
		default ->
				throw new IllegalArgumentException("Invalid position type: " + c);
		};
	}
	
	
	public TexBracketLevel(final LinkedModeModel model,
			final IDocument document, final DocContentSections docContentSections,
			final List<? extends LinkedPosition> positions, final int mode) {
		super(model, document, docContentSections, positions, mode);
	}
	
}
