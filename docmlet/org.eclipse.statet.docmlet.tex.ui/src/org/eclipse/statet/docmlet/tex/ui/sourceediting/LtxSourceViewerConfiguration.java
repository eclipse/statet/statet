/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui.sourceediting;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.information.IInformationProvider;
import org.eclipse.jface.text.quickassist.IQuickAssistProcessor;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.source.ISourceViewer;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.text.IIndentSettings;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.presentation.SingleTokenScanner;
import org.eclipse.statet.ecommons.text.ui.settings.PreferenceStoreTextStyleManager;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxDocumentContentInfo;
import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;
import org.eclipse.statet.docmlet.tex.core.source.util.LtxBracketPairMatcher;
import org.eclipse.statet.docmlet.tex.core.source.util.LtxHeuristicTokenScanner;
import org.eclipse.statet.docmlet.tex.ui.text.LtxDefaultTextStyleScanner;
import org.eclipse.statet.docmlet.tex.ui.text.LtxDoubleClickStrategy;
import org.eclipse.statet.docmlet.tex.ui.text.LtxMathTextStyleScanner;
import org.eclipse.statet.docmlet.tex.ui.text.TexTextStyles;
import org.eclipse.statet.internal.docmlet.tex.ui.TexUIPlugin;
import org.eclipse.statet.internal.docmlet.tex.ui.sourceediting.LtxAutoEditStrategy;
import org.eclipse.statet.internal.docmlet.tex.ui.sourceediting.LtxContentAssistProcessor;
import org.eclipse.statet.internal.docmlet.tex.ui.sourceediting.LtxQuickOutlineInformationProvider;
import org.eclipse.statet.ltk.text.core.CharPairMatcher;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.EcoReconciler2;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorAddon;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceUnitReconcilingStrategy;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistProcessor;
import org.eclipse.statet.ltk.ui.sourceediting.presentation.CommentScanner;


/**
 * Configuration for TeX source editors.
 */
public class LtxSourceViewerConfiguration extends SourceEditorViewerConfiguration {
	
	
	private static final String[] CONTENT_TYPES= TexDocumentConstants.LTX_CONTENT_TYPES.toArray(
			new String[TexDocumentConstants.LTX_CONTENT_TYPES.size()] );
	
	
	protected ITextDoubleClickStrategy doubleClickStrategy;
	
	private LtxAutoEditStrategy autoEditStrategy;
	
	private TexCoreAccess coreAccess;
	
	
	public LtxSourceViewerConfiguration(final int flags) {
		this(LtxDocumentContentInfo.INSTANCE, flags, null, null, null, null);
	}
	
	public LtxSourceViewerConfiguration(final DocContentSections documentContentInfo, final int flags,
			final SourceEditor editor,
			final TexCoreAccess access, final IPreferenceStore preferenceStore,
			final PreferenceStoreTextStyleManager<TextAttribute> textStyles) {
		super(documentContentInfo, flags, editor);
		setCoreAccess(access);
		
		setup((preferenceStore != null) ? preferenceStore : TexUIPlugin.getInstance().getEditorPreferenceStore(),
				LtkUIPreferences.getEditorDecorationPreferences(),
				TexEditingSettings.getAssistPreferences() );
		setTextStyles(textStyles);
	}
	
	protected void setCoreAccess(final TexCoreAccess access) {
		this.coreAccess= (access != null) ? access : TexCore.getWorkbenchAccess();
	}
	
	
	@Override
	protected void initTextStyles() {
		setTextStyles(TexUIPlugin.getInstance().getLtxTextStyles());
	}
	
	@Override
	protected void initScanners() {
		final var textStyles= getTextStyles();
		
		addScanner(TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE,
				new LtxDefaultTextStyleScanner(textStyles) );
		addScanner(TexDocumentConstants.LTX_MATH_CONTENT_TYPE,
				new LtxMathTextStyleScanner(textStyles) );
		addScanner(TexDocumentConstants.LTX_COMMENT_CONTENT_TYPE,
				new CommentScanner(textStyles, TexTextStyles.TS_COMMENT, TexTextStyles.TS_TASK_TAG,
						this.coreAccess.getPrefs() ));
		addScanner(TexDocumentConstants.LTX_MATHCOMMENT_CONTENT_TYPE,
				new CommentScanner(textStyles, TexTextStyles.TS_COMMENT, TexTextStyles.TS_TASK_TAG,
						this.coreAccess.getPrefs() ));
		addScanner(TexDocumentConstants.LTX_VERBATIM_CONTENT_TYPE,
				new SingleTokenScanner(textStyles, TexTextStyles.TS_VERBATIM) );
	}
	
	
	@Override
	public List<SourceEditorAddon> getAddOns() {
		final List<SourceEditorAddon> addons= super.getAddOns();
		if (this.autoEditStrategy != null) {
			addons.add(this.autoEditStrategy);
		}
		return addons;
	}
	
	@Override
	public void handleSettingsChanged(final Set<String> groupIds, final Map<String, Object> options) {
		super.handleSettingsChanged(groupIds, options);
		if (this.autoEditStrategy != null) {
			this.autoEditStrategy.getSettings().handleSettingsChanged(groupIds, options);
		}
	}
	
	
	@Override
	public String[] getConfiguredContentTypes(final ISourceViewer sourceViewer) {
		return CONTENT_TYPES;
	}
	
	
	@Override
	public CharPairMatcher createPairMatcher() {
		return new LtxBracketPairMatcher(
				LtxHeuristicTokenScanner.create(getDocumentContentInfo()) );
	}
	
	@Override
	public ITextDoubleClickStrategy getDoubleClickStrategy(final ISourceViewer sourceViewer, final String contentType) {
		if (this.doubleClickStrategy == null) {
			this.doubleClickStrategy= new LtxDoubleClickStrategy(
					LtxHeuristicTokenScanner.create(getDocumentContentInfo()) );
		}
		return this.doubleClickStrategy;
	}
	
	
	@Override
	protected IIndentSettings getIndentSettings() {
		return this.coreAccess.getTexCodeStyle();
	}
	
	@Override
	public String[] getDefaultPrefixes(final ISourceViewer sourceViewer, final String contentType) {
		return new String[] { "%", "" }; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	
	@Override
	public boolean isSmartInsertSupported() {
		return true;
	}
	
	@Override
	public boolean isSmartInsertByDefault() {
		return EPreferences.getInstancePrefs()
				.getPreferenceValue(TexEditingSettings.SMARTINSERT_BYDEFAULT_ENABLED_PREF);
	}
	
	
	@Override
	public IAutoEditStrategy[] getAutoEditStrategies(final ISourceViewer sourceViewer, final String contentType) {
		if (getSourceEditor() == null) {
			return super.getAutoEditStrategies(sourceViewer, contentType);
		}
		if (this.autoEditStrategy == null) {
			this.autoEditStrategy= createTexAutoEditStrategy();
		}
		return new IAutoEditStrategy[] { this.autoEditStrategy };
	}
	
	protected LtxAutoEditStrategy createTexAutoEditStrategy() {
		return new LtxAutoEditStrategy(this.coreAccess, getSourceEditor());
	}
	
	
	@Override
	public IReconciler getReconciler(final ISourceViewer sourceViewer) {
		final SourceEditor editor= getSourceEditor();
		if (!(editor instanceof SourceEditor1)) {
			return null;
		}
		final EcoReconciler2 reconciler= new EcoReconciler2(editor);
		reconciler.setDelay(500);
		reconciler.addReconcilingStrategy(new SourceUnitReconcilingStrategy());
		
//		final IReconcilingStrategy spellingStrategy= getSpellingStrategy(sourceViewer);
//		if (spellingStrategy != null) {
//			reconciler.addReconcilingStrategy(spellingStrategy);
//		}
		
		return reconciler;
	}
	
	
	@Override
	public void initContentAssist(final ContentAssist assistant) {
		final ContentAssistComputerRegistry registry= TexUIPlugin.getInstance().getLtxEditorContentAssistRegistry();
		
		{	final ContentAssistProcessor processor= new LtxContentAssistProcessor(assistant,
					TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE, registry, getSourceEditor() );
			processor.setCompletionProposalAutoActivationCharacters(new char[] { '\\' });
			assistant.setContentAssistProcessor(processor, TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new LtxContentAssistProcessor(assistant,
					TexDocumentConstants.LTX_MATH_CONTENT_TYPE, registry, getSourceEditor() );
			processor.setCompletionProposalAutoActivationCharacters(new char[] { '\\' });
			assistant.setContentAssistProcessor(processor, TexDocumentConstants.LTX_MATH_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new LtxContentAssistProcessor(assistant,
					TexDocumentConstants.LTX_COMMENT_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, TexDocumentConstants.LTX_COMMENT_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new LtxContentAssistProcessor(assistant,
					TexDocumentConstants.LTX_MATHCOMMENT_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, TexDocumentConstants.LTX_MATHCOMMENT_CONTENT_TYPE);
		}
		{	final ContentAssistProcessor processor= new LtxContentAssistProcessor(assistant,
					TexDocumentConstants.LTX_VERBATIM_CONTENT_TYPE, registry, getSourceEditor() );
			assistant.setContentAssistProcessor(processor, TexDocumentConstants.LTX_VERBATIM_CONTENT_TYPE);
		}
	}
	
	@Override
	protected IQuickAssistProcessor createQuickAssistProcessor() {
		final SourceEditor editor= getSourceEditor();
		if (editor != null) {
			return new LtxQuickAssistProcessor(editor);
		}
		return null;
	}
	
	
	@Override
	protected void collectHyperlinkDetectorTargets(final Map<String, IAdaptable> targets,
			final ISourceViewer sourceViewer) {
		targets.put("org.eclipse.statet.docmlet.editorHyperlinks.TexEditorTarget", getSourceEditor()); //$NON-NLS-1$
	}
	
	
	@Override
	protected IInformationProvider getQuickInformationProvider(final ISourceViewer sourceViewer,
			final int operation) {
		final SourceEditor editor= getSourceEditor();
		if (editor == null) {
			return null;
		}
		return switch (operation) {
		case SourceEditorViewer.SHOW_SOURCE_OUTLINE ->
				new LtxQuickOutlineInformationProvider(editor, operation);
		default ->
				null;
		};
	}
	
}
