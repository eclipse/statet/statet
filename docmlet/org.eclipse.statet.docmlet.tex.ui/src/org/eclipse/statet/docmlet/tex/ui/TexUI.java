/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui;


public class TexUI {
	
	public static final String BUNDLE_ID= "org.eclipse.statet.docmlet.tex.ui"; //$NON-NLS-1$
	
	
	public static final String EDITOR_CONTEXT_ID= "org.eclipse.statet.docmlet.contexts.TexEditor"; //$NON-NLS-1$
	
	
	public static final String BASE_PREF_PAGE_ID= "org.eclipse.statet.docmlet.preferencePages.Tex"; //$NON-NLS-1$
	
	public static final String EDITOR_PREF_PAGE_ID= "org.eclipse.statet.docmlet.preferencePages.TexEditor"; //$NON-NLS-1$
	
}
