/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.docmlet.tex.core.commands.LtxCommandCategories;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;


public class TexCommandLabelProvider extends LabelProvider {
	
	
	@Override
	public Image getImage(final Object element) {
		if (element instanceof TexCommand) {
			return TexUIResources.INSTANCE.getCommandImage((TexCommand) element);
		}
		return null;
	}
	
	@Override
	public String getText(final Object element) {
		if (element instanceof LtxCommandCategories.Category) {
			return ((LtxCommandCategories.Category) element).getLabel();
		}
		if (element instanceof final TexCommand command) {
			return command.getControlWord() + " – " + command.getDescription(); //$NON-NLS-1$
		}
		return super.getText(element);
	}
	
}
