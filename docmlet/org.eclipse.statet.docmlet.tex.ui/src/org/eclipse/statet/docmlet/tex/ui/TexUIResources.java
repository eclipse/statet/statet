/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui;

import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.internal.docmlet.tex.ui.TexUIPlugin;


@NonNullByDefault
public class TexUIResources {
	
	
	private static final String NS= "org.eclipse.statet.docmlet.tex"; //$NON-NLS-1$
	
	
	public static final String OBJ_PART_IMAGE_ID=               NS + "/images/obj/sectioning-part"; //$NON-NLS-1$
	public static final String OBJ_CHAPTER_IMAGE_ID=            NS + "/images/obj/sectioning-chapter"; //$NON-NLS-1$
	public static final String OBJ_SECTION_IMAGE_ID=            NS + "/images/obj/sectioning-section"; //$NON-NLS-1$
	public static final String OBJ_SUBSECTION_IMAGE_ID=         NS + "/images/obj/sectioning-subsection"; //$NON-NLS-1$
	public static final String OBJ_SUBSUBSECTION_IMAGE_ID=      NS + "/images/obj/sectioning-subsubsection"; //$NON-NLS-1$
	
	
	public static final TexUIResources INSTANCE= new TexUIResources();
	
	
	private final ImageRegistry registry;
	
	private Map<String, String> commandImages;
	
	
	private TexUIResources() {
		this.registry= TexUIPlugin.getInstance().getImageRegistry();
	}
	
	public @Nullable ImageDescriptor getImageDescriptor(final String id) {
		return this.registry.getDescriptor(id);
	}
	
	public @Nullable Image getImage(final String id) {
		return this.registry.get(id);
	}
	
	public @Nullable String getCommandImageId(final TexCommand command) {
		if (this.commandImages == null) {
			this.commandImages= TexUIPlugin.getInstance().getCommandImages();
		}
		return this.commandImages.get(command.getControlWord());
	}
	
	public @Nullable Image getCommandImage(final TexCommand command) {
		Image image= null;
		final String id= getCommandImageId(command);
		if (id != null) {
			image= this.registry.get(id);
			if (image == null) {
				image= DocmlBaseUIResources.INSTANCE.getImage(id);
			}
		}
		return image;
	}
	
}
