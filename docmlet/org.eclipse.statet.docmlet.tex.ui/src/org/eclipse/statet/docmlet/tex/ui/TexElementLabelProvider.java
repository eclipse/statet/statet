/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.model.TexElement;
import org.eclipse.statet.docmlet.tex.core.model.TexElementName;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.ui.ElementLabelProvider;


@NonNullByDefault
public class TexElementLabelProvider implements ElementLabelProvider {
	
	
	private final DocmlBaseUIResources docBaseResources;
	private final TexUIResources texResources;
	
	
	public TexElementLabelProvider() {
		this.docBaseResources= DocmlBaseUIResources.INSTANCE;
		this.texResources= TexUIResources.INSTANCE;
	}
	
	
	protected @Nullable Image getDocBaseImage(final String imageId) {
		return this.docBaseResources.getImage(imageId);
	}
	
	protected @Nullable Image getTexImage(final String imageId) {
		return this.texResources.getImage(imageId);
	}
	
	
	@Override
	public @Nullable Image getImage(final LtkModelElement<?> element) {
		if (element.getModelTypeId() == TexModel.LTX_TYPE_ID) {
			return switch (element.getElementType() & LtkModelElement.MASK_C123) {
			case TexElement.C12_PREAMBLE ->
					getDocBaseImage(DocmlBaseUIResources.OBJ_PREAMBLE_IMAGE_ID);
			case TexElement.C12_SECTIONING | TexCommand.PART_LEVEL ->
					getTexImage(TexUIResources.OBJ_PART_IMAGE_ID);
			case TexElement.C12_SECTIONING | TexCommand.CHAPTER_LEVEL ->
					getTexImage(TexUIResources.OBJ_CHAPTER_IMAGE_ID);
			case TexElement.C12_SECTIONING | TexCommand.SECTION_LEVEL ->
					getTexImage(TexUIResources.OBJ_SECTION_IMAGE_ID);
			case TexElement.C12_SECTIONING | TexCommand.SUBSECTION_LEVEL ->
					getTexImage(TexUIResources.OBJ_SUBSECTION_IMAGE_ID);
			case TexElement.C12_SECTIONING | TexCommand.SUBSUBSECTION_LEVEL ->
					getTexImage(TexUIResources.OBJ_SUBSUBSECTION_IMAGE_ID);
			default ->
					null;
			};
		}
		return null;
	}
	
	public @Nullable Image getImage(final TexNameAccess access) {
		if (access.getType() == TexElementName.LABEL) {
			return getTexImage(DocmlBaseUIResources.OBJ_LABEL_REF_IMAGE_ID);
		}
		return null;
	}
	
	@Override
	public @Nullable String getText(final LtkModelElement<?> element) {
		if (element.getModelTypeId() == TexModel.LTX_TYPE_ID) {
			return element.getElementName().getDisplayName();
		}
		return null;
	}
	
}
