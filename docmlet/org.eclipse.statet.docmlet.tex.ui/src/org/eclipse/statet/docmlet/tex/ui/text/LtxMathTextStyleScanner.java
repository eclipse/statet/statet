/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui.text;

import java.util.Map;

import org.eclipse.jface.text.rules.IToken;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.string.CharArrayString;

import org.eclipse.statet.ecommons.collections.IntArrayMap;
import org.eclipse.statet.ecommons.text.ui.presentation.TextStyleManager;

import org.eclipse.statet.docmlet.tex.core.parser.LtxLexer;


@NonNullByDefault
public class LtxMathTextStyleScanner extends LtxDefaultTextStyleScanner {
	
	
	public LtxMathTextStyleScanner(final TextStyleManager<?> textStyles) {
		super(textStyles);
	}
	
	
	@Override
	protected void registerTokens(final IntArrayMap<IToken> map) {
		map.put(LtxLexer.DEFAULT_TEXT, getToken(TexTextStyles.TS_MATH));
		map.put(LtxLexer.CONTROL_WORD, getToken(TexTextStyles.TS_MATH_CONTROL_WORD));
		map.put(LtxLexer.CONTROL_CHAR, getToken(TexTextStyles.TS_MATH_CONTROL_CHAR));
		map.put(LtxLexer.CONTROL_NONE, getToken(TexTextStyles.TS_MATH_CONTROL_WORD));
		map.put(LtxLexer.CURLY_BRACKET_OPEN, getToken(TexTextStyles.TS_MATH_CURLY_BRACKETS));
		map.put(LtxLexer.CURLY_BRACKET_CLOSE, getToken(TexTextStyles.TS_MATH_CURLY_BRACKETS));
		map.put(LtxLexer.SQUARED_BRACKET_OPEN, getToken(TexTextStyles.TS_MATH_CURLY_BRACKETS));
		map.put(LtxLexer.SQUARED_BRACKET_CLOSE, getToken(TexTextStyles.TS_MATH_CURLY_BRACKETS));
		
		map.put(LtxLexer.LINE_COMMENT, getToken(TexTextStyles.TS_COMMENT));
	}
	
	
	@Override
	protected void updateWords(final Map<CharArrayString, IToken> map) {
	}
	
}
