/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui.actions;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.actions.ToggleBooleanPreferenceHandler;

import org.eclipse.statet.docmlet.tex.ui.sourceediting.TexEditingSettings;


/**
 * Toggles enablement of mark occurrences in Tex editors.
 */
@NonNullByDefault
public class TexToggleMarkOccurrencesHandler extends ToggleBooleanPreferenceHandler {
	
	
	public TexToggleMarkOccurrencesHandler() {
		super(	TexEditingSettings.MARKOCCURRENCES_ENABLED_PREF,
				"org.eclipse.jdt.ui.edit.text.java.toggleMarkOccurrences"); //$NON-NLS-1$
	}
	
}
