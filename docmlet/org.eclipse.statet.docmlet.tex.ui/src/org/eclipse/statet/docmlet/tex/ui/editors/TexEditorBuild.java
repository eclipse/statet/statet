/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.ui.editors;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;

import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.project.TexIssues;
import org.eclipse.statet.docmlet.tex.ui.TexUI;
import org.eclipse.statet.docmlet.tex.ui.sourceediting.TexEditingSettings;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;


@NonNullByDefault
public class TexEditorBuild {
	
	
	public static final String GROUP_ID= "Tex/editor/build"; //$NON-NLS-1$
	
	
	public static final BooleanPref PROBLEMCHECKING_ENABLED_PREF= new BooleanPref(
			TexEditingSettings.EDITING_PREF_QUALIFIER, "ProblemChecking.enabled"); //$NON-NLS-1$
	
	
	public static final String ERROR_ANNOTATION_TYPE=       "org.eclipse.statet.docmlet.tex.editorAnnotations.ErrorProblem"; //$NON-NLS-1$
	public static final String WARNING_ANNOTATION_TYPE=     "org.eclipse.statet.docmlet.tex.editorAnnotations.WarningProblem"; //$NON-NLS-1$
	public static final String INFO_ANNOTATION_TYPE=        "org.eclipse.statet.docmlet.tex.editorAnnotations.InfoProblem"; //$NON-NLS-1$
	
	private static final IssueTypeSet.ProblemTypes PROBLEM_ANNOTATION_TYPES= new IssueTypeSet.ProblemTypes(
			ERROR_ANNOTATION_TYPE, WARNING_ANNOTATION_TYPE, INFO_ANNOTATION_TYPE );
	
	public static final IssueTypeSet.ProblemCategory LTX_MODEL_PROBLEM_CATEGORY= new IssueTypeSet.ProblemCategory(
			TexModel.LTX_TYPE_ID,
			null, PROBLEM_ANNOTATION_TYPES );
	
	public static final IssueTypeSet LTX_ISSUE_TYPE_SET= new IssueTypeSet(TexUI.BUNDLE_ID,
			TexIssues.TASK_CATEGORY,
			ImCollections.newList(
					TexEditorBuild.LTX_MODEL_PROBLEM_CATEGORY ));
	
}
