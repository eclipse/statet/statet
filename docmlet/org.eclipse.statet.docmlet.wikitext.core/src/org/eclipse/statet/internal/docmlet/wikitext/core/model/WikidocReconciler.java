/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.AbstractDocument;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CacheStringFactory;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCoreAccess;
import org.eclipse.statet.docmlet.wikitext.core.ast.Embedded;
import org.eclipse.statet.docmlet.wikitext.core.ast.SourceComponent;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikidocParser;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstInfo;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager1;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitEmbeddedModelReconciler;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProjects;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguageDocumentSetupParticipant;
import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.ExtdocMarkupLanguage;
import org.eclipse.statet.internal.docmlet.wikitext.core.WikitextCorePlugin;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.ExtensibleReconciler;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;
import org.eclipse.statet.yaml.core.model.YamlModel;


@NonNullByDefault
public class WikidocReconciler extends ExtensibleReconciler<WikitextProject,
		WikidocSourceUnitModelContainer<?>, WikidocSourceUnitEmbeddedModelReconciler<?>> {
	
	
	private static final ImIdentityList<String> DEFAULT_EMBEDDED_MODEL_IDS= ImCollections.newIdentityList(
			YamlModel.YAML_TYPE_ID, TexModel.LTX_TYPE_ID );
	
	
	protected static final class Data {
		
		public final WikidocSourceUnitModelContainer<?> adapter;
		public final ImList<ExtensionData<WikitextProject, ?, WikidocSourceUnitEmbeddedModelReconciler<?>>> extensions;
		public final @Nullable String nowebTypeId;
		
		public final WikitextSourceUnit sourceUnit;
		public final WikitextCoreAccess coreAccess;
		
		public final SourceContent content;
		
		@Nullable WikitextAstInfo ast;
		
		@Nullable WikidocSourceUnitModelInfo oldModel;
		@Nullable WikidocSourceUnitModelInfo newModel;
		
		
		public Data(final WikidocSourceUnitModelContainer<?> adapter,
				final ImList<ExtensionData<WikitextProject, ?, WikidocSourceUnitEmbeddedModelReconciler<?>>> extensions,
				final @Nullable String nowebTypeId,
				final SubMonitor monitor) {
			this.adapter= adapter;
			this.extensions= extensions;
			this.nowebTypeId= nowebTypeId;
			
			this.sourceUnit= adapter.getSourceUnit();
			this.content= adapter.getParseContent(monitor);
			this.coreAccess= WikitextCore.getContextAccess(adapter.getSourceUnit());
		}
		
		
		boolean isOK() {
			return (this.content != null);
		}
		
		@SuppressWarnings("null")
		public WikitextAstInfo getAst() {
			return this.ast;
		}
		
		@SuppressWarnings("null")
		public WikidocSourceUnitModelInfo getModel() {
			return this.newModel;
		}
		
	}
	
	private static final boolean DEBUG_LOG_AST= Boolean.parseBoolean(
			Platform.getDebugOption("org.eclipse.statet.docmlet.wikitext/debug/Reconciler/logAst") ); //$NON-NLS-1$
	
	
	private final Map<WikitextMarkupLanguage, WikitextMarkupLanguage> languages= new HashMap<>();
	
	private final WikitextModelManagerImpl modelManager;
	protected boolean stop= false;
	
	private final WikitextMarkupLanguageManager1 markupLanguageManager;
	
	private final Object raLock= new Object();
	private final WikidocParser raParser= new WikidocParser(new CacheStringFactory(0x20));
	
	private final Object rmLock= new Object();
	private final SourceAnalyzer rmSourceAnalyzer= new SourceAnalyzer();
	
	private final Object riLock= new Object();
//	private final WikidocProblemReporter riProblemReporter= new WikidocProblemReporter();
	
	
	public WikidocReconciler(final WikitextModelManagerImpl manager) {
		this.modelManager= manager;
		this.markupLanguageManager= WikitextCore.getMarkupLanguageManager();
		
		this.raParser.setCollectHeadingText(true);
	}
	
	
	@Override
	public void init(final WikitextProject project, final MultiStatus statusCollector) {
		super.init(project, statusCollector);
	}
	
	void stop() {
		this.stop= true;
	}
	
	
	@Override
	protected @Nullable WikitextProject getProject(final IProject wsProject) {
		return WikitextProjects.getWikitextProject(wsProject);
	}
	
	@Override
	protected @Nullable WikidocSourceUnitEmbeddedModelReconciler<?> createEmbeddedModelReconciler(
			final String modelTypeId) {
		if (modelTypeId == YamlModel.YAML_TYPE_ID) {
			return new YamlEmbeddedModelReconciler();
		}
		if (modelTypeId == TexModel.LTX_TYPE_ID) {
			return new LtxEmbeddedModelReconciler();
		}
		return LtkModels.getModelAdapter(modelTypeId, WikidocSourceUnitEmbeddedModelReconciler.class);
	}
	
	
	public void reconcile(final WikidocSourceUnitModelContainer<?> adapter, final int flags,
			final IProgressMonitor monitor) {
		final var m= SubMonitor.convert(monitor);
		
		final Data data;
		{	ImIdentityList<String> embeddedModelIds= DEFAULT_EMBEDDED_MODEL_IDS;
			String nowebTypeId= adapter.getNowebType();
			if (nowebTypeId != null) {
				embeddedModelIds= ImCollections.addElementIfAbsent(embeddedModelIds, nowebTypeId);
			}
			final var extensions= initExtensions(embeddedModelIds, adapter, flags);
			if (nowebTypeId != null && !ExtensionData.contains(extensions, nowebTypeId)) {
				nowebTypeId= null;
			}
			data= new Data(adapter, extensions, nowebTypeId, m);
			if (data == null || !data.isOK()) {
				adapter.clear();
				return;
			}
		}
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		
		synchronized (this.raLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			
			updateAst(data, flags, m);
		}
		
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		if ((flags & 0xf) < ModelManager.MODEL_FILE) {
			return;
		}
		
		synchronized (this.rmLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			
			final boolean updated= updateModel(data, flags, m);
			if (updated) {
				this.modelManager.getEventJob().addUpdate(data.sourceUnit,
						data.oldModel, data.newModel );
			}
		}
		
		if ((flags & ModelManager.RECONCILE) != 0 && data.newModel != null) {
			synchronized (this.riLock) {
				if (this.stop || m.isCanceled()
						|| data.newModel != data.adapter.getCurrentModel() ) {
					return;
				}
				
				reportIssues(data, flags, m);
			}
		}
	}
	
	protected final @Nullable WikitextMarkupLanguage getMarkupLanguage(final Data data,
			final SubMonitor m) {
		final WikitextSourceUnit su= data.adapter.getSourceUnit();
		
		WikitextMarkupLanguage markupLanguage= null;
		if (su.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
			final AbstractDocument document= su.getDocument(m);
			markupLanguage= MarkupLanguageDocumentSetupParticipant.getMarkupLanguage(document, su.getDocumentContentInfo().getPartitioning());
		}
		if (markupLanguage == null && su instanceof WorkspaceSourceUnit) {
			markupLanguage= this.markupLanguageManager.getLanguage((IFile) su.getResource(),
					null, true );
		}
		if (markupLanguage == null) {
			return null;
		}
		synchronized (this.languages) {
			WikitextMarkupLanguage internal= this.languages.get(markupLanguage);
			if (internal == null) {
				internal= markupLanguage.clone("Reconciler", markupLanguage.getMode());
				this.languages.put(internal, internal);
			}
			return internal;
		}
	}
	
	protected final void updateAst(final Data data, final int flags, final SubMonitor m) {
		final WikitextMarkupLanguage markupLanguage= getMarkupLanguage(data, m);
		if (markupLanguage == null) {
			throw new UnsupportedOperationException("Markup language is missing.");
		}
		
		final SourceModelStamp stamp= new BasicSourceModelStamp(data.content.getStamp(),
					createSourceConfig(markupLanguage, data.extensions) );
		
		WikitextAstInfo ast= (WikitextAstInfo)data.adapter.getCurrentAst();
		if (ast != null && !stamp.equals(ast.getStamp())) {
			ast= null;
		}
		if (ast != null) {
			data.ast= ast;
		}
		else {
			final SourceComponent sourceNode;
//			final StringParseInput input= new StringParseInput(data.content.text);
//			
			this.raParser.setMarkupLanguage(markupLanguage);
			this.raParser.setCommentLevel(WikidocParser.COLLECT_COMMENTS);
			this.raParser.setCollectEmebeddedNodes(true);
			
			sourceNode= this.raParser.parse(data.content);
			
			final List<Embedded> embeddedNodes= this.raParser.getEmbeddedNodes();
			final var embeddedTypeIds= collectEmbeddedTypeIds(embeddedNodes);
			for (final var extensionData : data.extensions) {
				if (embeddedTypeIds.contains(extensionData.modelTypeId)) {
					((WikidocSourceUnitEmbeddedModelReconciler)extensionData.reconciler)
							.reconcileAst(data.content, embeddedNodes,
									markupLanguage,
									data.adapter, extensionData.config, flags );
				}
			}
			
			ast= new WikitextAstInfo(1, stamp, sourceNode, markupLanguage, embeddedTypeIds);
			if (DEBUG_LOG_AST) {
				logAst(ast, data.content);
			}
			
			synchronized (data.adapter) {
				data.adapter.setAst(ast);
			}
			data.ast= ast;
		}
	}
	
	protected final boolean updateModel(final Data data, final int flags,
			final SubMonitor m) {
		WikidocSourceUnitModelInfo model= data.adapter.getCurrentModel();
		if (model != null && !data.getAst().getStamp().equals(model.getStamp())) {
			model= null;
		}
		if (model != null) {
			data.newModel= model;
			return false;
		}
		else {
			model= this.rmSourceAnalyzer.createModel(data.adapter.getSourceUnit(),
					data.content.getString(), data.getAst() );
			final boolean isOK= (model != null);
			
			final var embeddedTypes= data.getAst().getEmbeddedTypes();
			final var embeddedItems= this.rmSourceAnalyzer.getEmbeddedItems();
			for (final var extensionData : data.extensions) {
				if (embeddedTypes.contains(extensionData.modelTypeId)
						|| data.nowebTypeId == extensionData.modelTypeId) {
					((WikidocSourceUnitEmbeddedModelReconciler)extensionData.reconciler)
							.reconcileModel(model, data.content, embeddedItems,
									data.adapter, extensionData.config, flags,
									m );
				}
			}
			
			if (isOK) {
				synchronized (data.adapter) {
					data.oldModel= data.adapter.getCurrentModel();
					data.adapter.setModel(model);
				}
				data.newModel= model;
				return true;
			}
			return false;
		}
	}
	
	protected void reportIssues(final Data data, final int flags, final SubMonitor m) {
		try {
			final var issueSupport= data.adapter.getIssueSupport();
			if (issueSupport == null) {
				return;
			}
			final var issueRequestor= issueSupport.createIssueRequestor(data.sourceUnit);
			if (issueRequestor != null) {
				try {
					if (data.getAst().getMarkupLanguage() instanceof ExtdocMarkupLanguage) {
						((ExtdocMarkupLanguage)data.getAst().getMarkupLanguage())
								.getProblemReporter().run(data.sourceUnit,
										data.getModel(), data.content,
										issueRequestor, flags );
					}
					
					final var embeddedTypes= data.getAst().getEmbeddedTypes();
					for (final var extensionData : data.extensions) {
						if (embeddedTypes.contains(extensionData.modelTypeId)) {
							((WikidocSourceUnitEmbeddedModelReconciler)extensionData.reconciler)
									.reportIssues(data.getModel(), data.content,
											issueRequestor,
											data.adapter, extensionData.config, flags );
						}
					}
				}
				finally {
					issueRequestor.finish();
				}
			}
		}
		catch (final Exception e) {
			handleStatus(new Status(IStatus.ERROR, TexCore.BUNDLE_ID, 0,
					String.format("An error occurred when reporting issues for source unit %1$s.",
							data.sourceUnit ),
					e ));
		}
	}
	
	
	protected void handleStatus(final IStatus status) {
		final MultiStatus collector= getStatusCollector();
		if (collector != null) {
			collector.add(status);
		}
		else {
			WikitextCorePlugin.log(status);
		}
	}
	
}
