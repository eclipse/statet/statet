/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.builder;

import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.WikitextBuildParticipant;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocWorkspaceSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.internal.docmlet.wikitext.core.WikitextCorePlugin;
import org.eclipse.statet.internal.docmlet.wikitext.core.model.WikidocReconciler;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.project.core.builder.ProjectBuildTask;


@NonNullByDefault
public class WikitextBuildTask extends ProjectBuildTask<WikitextProject, WikidocWorkspaceSourceUnit, WikitextBuildParticipant> {
	
	
	private @Nullable WikidocReconciler reconciler;
	
	
	public WikitextBuildTask(final WikitextProjectBuilder builder) {
		super(builder);
	}
	
	
	protected WikidocReconciler getReconciler() {
		var reconciler= this.reconciler;
		if (reconciler == null) {
			reconciler= new WikidocReconciler(WikitextCorePlugin.getInstance().getWikidocModelManager());
			reconciler.init(getBuilder().getLtkProject(), this.status);
			this.reconciler= reconciler;
		}
		return reconciler;
	}
	
	@Override
	protected void reconcileSourceUnit(final WikidocWorkspaceSourceUnit sourceUnit, final SubMonitor m) {
		final var adapter= sourceUnit.getAdapter(
				WikidocSourceUnitModelContainer.class );
		if (adapter != null) {
			getReconciler().reconcile(adapter,
					ModelManager.MODEL_DEPENDENCIES | ModelManager.RECONCILE,
					m );
		}
	}
	
}
