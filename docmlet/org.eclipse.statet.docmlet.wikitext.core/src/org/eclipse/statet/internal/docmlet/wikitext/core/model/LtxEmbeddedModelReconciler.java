/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CacheStringFactory;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.ast.LtxParser;
import org.eclipse.statet.docmlet.tex.core.ast.SourceComponent;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.source.TexSourceConfig;
import org.eclipse.statet.docmlet.wikitext.core.ast.Embedded;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitEmbeddedModelReconciler;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.model.core.build.EmbeddingForeignReconcileTask;
import org.eclipse.statet.ltk.model.core.build.ReconcileConfig;


@NonNullByDefault
public class LtxEmbeddedModelReconciler implements WikidocSourceUnitEmbeddedModelReconciler<ReconcileConfig<?>> {
	
	
	private final StringParserInput raInput= new StringParserInput(0x1000);
	private @Nullable LtxParser raParser;
	
	
	public LtxEmbeddedModelReconciler() {
	}
	
	
	@Override
	public String getModelTypeId() {
		return TexModel.LTX_TYPE_ID;
	}
	
	@Override
	public ReconcileConfig<?> createConfig(final @Nullable IProject wsProject,
			final @Nullable WikitextProject wikitextProject,
			final int level) {
		return new ReconcileConfig<>(TexSourceConfig.DEFAULT_CONFIG);
	}
	
	
	@Override
	public void reconcileAst(final SourceContent content,
			final List<Embedded> list,
			final WikitextMarkupLanguage markupLanguage,
			final WikidocSourceUnitModelContainer<?> container, final ReconcileConfig<?> config, final int level) {
		this.raInput.reset(content.getString());
		var parser= this.raParser;
		if (parser == null) {
			parser= new LtxParser(null, new CacheStringFactory(0x20));
			this.raParser= parser;
		}
		final TexCommandSet commandSet= TexCore.getWorkbenchAccess().getTexCommandSet();
		for (final EmbeddingAstNode embeddingNode : list) {
			if (embeddingNode.getForeignTypeId() != TexModel.LTX_TYPE_ID) {
				continue;
			}
			
			final SourceComponent component= parser.parseSourceFragment(
					this.raInput.init(embeddingNode.getStartOffset(), embeddingNode.getEndOffset()),
					embeddingNode, commandSet );
			embeddingNode.setForeignNode(component);
		}
	}
	
	@Override
	public void reconcileModel(final WikidocSourceUnitModelInfo wikitextModel, final SourceContent content,
			final List<? extends EmbeddingForeignReconcileTask<Embedded, WikitextSourceElement>> list,
			final WikidocSourceUnitModelContainer<?> container, final ReconcileConfig<?> config, final int level,
			final SubMonitor m) {
	}
	
	@Override
	public void reportIssues(final WikidocSourceUnitModelInfo wikitextModel, final SourceContent content,
			final IssueRequestor issueRequestor,
			final WikidocSourceUnitModelContainer<?> container, final ReconcileConfig<?> config, final int level) {
	}
	
}
