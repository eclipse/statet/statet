/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.input.OffsetStringParserInput;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;
import org.eclipse.statet.jcommons.text.core.input.TextParserInput;

import org.eclipse.statet.docmlet.wikitext.core.ast.Embedded;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitEmbeddedModelReconciler;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.dsl.core.model.DslElementName;
import org.eclipse.statet.dsl.core.source.ast.DslAstNode;
import org.eclipse.statet.dsl.core.source.ast.SourceComponent;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.model.core.build.EmbeddingForeignReconcileTask;
import org.eclipse.statet.ltk.model.core.build.ReconcileConfig;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.yaml.core.model.YamlChunkElement;
import org.eclipse.statet.yaml.core.model.YamlModel;
import org.eclipse.statet.yaml.core.model.YamlSourceUnitModelInfo;
import org.eclipse.statet.yaml.core.model.build.YamlIssueReporter;
import org.eclipse.statet.yaml.core.source.YamlSourceConfig;
import org.eclipse.statet.yaml.core.source.ast.YamlParser;


@NonNullByDefault
public class YamlEmbeddedModelReconciler implements WikidocSourceUnitEmbeddedModelReconciler<ReconcileConfig<?>> {
	
	
	private static final DslElementName METADATA_ELEMENT_NAME= DslElementName.create(
			DslElementName.OTHER, "Metadata (YAML)" );
	
	
	private final StringParserInput raInput= new StringParserInput();
	private @Nullable YamlParser raParser;
	
	private @Nullable YamlIssueReporter riReporter;
	
	
	public YamlEmbeddedModelReconciler() {
	}
	
	
	@Override
	public String getModelTypeId() {
		return YamlModel.YAML_TYPE_ID;
	}
	
	@Override
	public ReconcileConfig<?> createConfig(final @Nullable IProject wsProject,
			final @Nullable WikitextProject wikitextProject,
			final int level) {
		return new ReconcileConfig<>(YamlSourceConfig.DEFAULT_CONFIG);
	}
	
	
	@Override
	public void reconcileAst(final SourceContent content,
			final List<Embedded> list,
			final WikitextMarkupLanguage markupLanguage,
			final WikidocSourceUnitModelContainer<?> container, final ReconcileConfig<?> config, final int level) {
		var parser= this.raParser;
		if (parser == null) {
			parser= new YamlParser(AstInfo.LEVEL_MODEL_DEFAULT);
			
			parser.setScalarText(true);
			parser.setCommentLevel(YamlParser.COLLECT_COMMENTS);
			
			this.raParser= parser;
		}
		
		for (final EmbeddingAstNode embeddingNode : list) {
			if (embeddingNode.getForeignTypeId() != YamlModel.YAML_TYPE_ID) {
				continue;
			}
			
			final String text= content.getString();
			int startOffset= embeddingNode.getStartOffset();
			while (startOffset < text.length() && Character.isWhitespace(text.charAt(startOffset))) {
				startOffset++;
			}
			
			final TextParserInput input= (content.getStartOffset() != 0) ?
					new OffsetStringParserInput(text, content.getStartOffset()) :
					this.raInput.reset(text);
			
			final SourceComponent component= parser.parseSourceFragment(
					input.init(startOffset, embeddingNode.getEndOffset()),
					embeddingNode, true );
			embeddingNode.setForeignNode(component);
		}
	}
	
	@Override
	public void reconcileModel(final WikidocSourceUnitModelInfo wikitextModel, final SourceContent content,
			final List<? extends EmbeddingForeignReconcileTask<Embedded, WikitextSourceElement>> list,
			final WikidocSourceUnitModelContainer<?> container, final ReconcileConfig<?> config, final int level,
			final SubMonitor m) {
		final var sourceElement= wikitextModel.getSourceElement();
		
		int metadataCount= 0;
		final List<YamlChunkElement> chunkElements= new ArrayList<>();
		for (final var task : list) {
			final DslAstNode astNode;
			if (task.getForeignTypeId() != YamlModel.YAML_TYPE_ID
					|| (astNode= (DslAstNode)task.getAstNode().getForeignNode()) == null) {
				continue;
			}
			
			final var element= new YamlChunkElement(task.getEmbeddingElement(),
					(SourceComponent)astNode,
					METADATA_ELEMENT_NAME, metadataCount++ );
			task.setEmbeddedElement(element);
			chunkElements.add(element);
		}
		
		if (metadataCount == 0) {
			return;
		}
		
		final YamlSourceUnitModelInfo modelInfo= YamlModel.getYamlModelManager().reconcile(
				sourceElement.getSourceUnit(), wikitextModel, chunkElements, level, m );
		if (modelInfo != null) {
			wikitextModel.addAttachment(modelInfo);
		}
	}
	
	@Override
	public void reportIssues(final WikidocSourceUnitModelInfo wikitextModel, final SourceContent content,
			final IssueRequestor issueRequestor,
			final WikidocSourceUnitModelContainer<?> container, final ReconcileConfig<?> config, final int level) {
		final YamlSourceUnitModelInfo yamlModel= YamlModel.getYamlModelInfo(wikitextModel);
		if (yamlModel == null) {
			return;
		}
		final SourceUnit sourceUnit= yamlModel.getSourceElement().getSourceUnit();
		
		var reporter= this.riReporter;
		if (reporter == null) {
			reporter= new YamlIssueReporter();
			this.riReporter= reporter;
		}
		reporter.run(sourceUnit, yamlModel, content, issueRequestor, level);
	}
	
}
