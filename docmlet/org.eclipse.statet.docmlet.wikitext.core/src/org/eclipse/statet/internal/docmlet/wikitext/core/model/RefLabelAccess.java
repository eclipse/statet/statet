/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextElementName;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.ltk.model.core.impl.NameAccessAccumulator;


public abstract class RefLabelAccess extends WikitextNameAccess {
	
	
	public final static int A_READ=                        0x00000000;
	public final static int A_WRITE=                       0x00000002;
	
	
	static final class LinkDef extends RefLabelAccess {
		
		protected LinkDef(final NameAccessAccumulator<WikitextNameAccess> shared, final WikitextAstNode node, final WikitextAstNode labelNode) {
			super(shared, node, labelNode);
		}
		
		@Override
		public int getType() {
			return LINK_DEF_LABEL;
		}
		
	}
	
	static final class LinkAnchor extends RefLabelAccess {
		
		protected LinkAnchor(final NameAccessAccumulator<WikitextNameAccess> shared, final WikitextAstNode node, final WikitextAstNode labelNode) {
			super(shared, node, labelNode);
		}
		
		@Override
		public int getType() {
			return LINK_ANCHOR_LABEL;
		}
		
	}
	
	
	private final NameAccessAccumulator<WikitextNameAccess> shared;
	
	private final WikitextAstNode node;
	private final WikitextAstNode nameNode;
	
	int flags;
	
	
	protected RefLabelAccess(final NameAccessAccumulator<WikitextNameAccess> shared, final WikitextAstNode node, final WikitextAstNode labelNode) {
		this.shared= shared;
		shared.getList().add(this);
		this.node= node;
		this.nameNode= labelNode;
	}
	
	
	@Override
	public String getSegmentName() {
		return this.shared.getLabel();
	}
	
	@Override
	public String getDisplayName() {
		return this.shared.getLabel();
	}
	
	@Override
	public WikitextElementName getNextSegment() {
		return null;
	}
	
	
	@Override
	public WikitextAstNode getNode() {
		return this.node;
	}
	
	@Override
	public WikitextAstNode getNameNode() {
		return this.nameNode;
	}
	
	@Override
	public ImList<? extends WikitextNameAccess> getAllInUnit() {
		return ImCollections.toList(this.shared.getList());
	}
	
	
	@Override
	public boolean isWriteAccess() {
		return ((this.flags & A_WRITE) != 0);
	}
	
}
