/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.builder;

import org.eclipse.core.runtime.content.IContentType;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.WikitextBuildParticipant;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocWorkspaceSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProjects;
import org.eclipse.statet.internal.docmlet.wikitext.core.WikitextCorePlugin;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.project.core.builder.ProjectTaskBuilder;


@NonNullByDefault
public class WikitextProjectBuilder extends ProjectTaskBuilder<WikitextProject, WikidocWorkspaceSourceUnit, WikitextBuildParticipant> {
	
	
	public static final String BUILDER_ID= "org.eclipse.statet.docmlet.resourceProjects.WikitextBuilder"; //$NON-NLS-1$
	
	private static final BuilderDefinition<WikitextProject, WikidocWorkspaceSourceUnit, WikitextBuildParticipant> DEFINITION=
			new BuilderDefinition<>(WikitextCore.BUNDLE_ID, WikitextCorePlugin.getInstance(),
					WikitextProjects.WIKITEXT_NATURE_ID, "Wikitext",
					WikidocWorkspaceSourceUnit.class,
					WikitextBuildParticipant.class ) {
				@Override
				public int checkSourceUnitContent(final IContentType contentType) {
					if (contentType.isKindOf(WikitextCore.WIKIDOC_CONTENT_TYPE)) {
						return 0;
					}
					return -1;
				}
			};
	
	
	public WikitextProjectBuilder() {
	}
	
	
	@Override
	public BuilderDefinition<WikitextProject, WikidocWorkspaceSourceUnit, WikitextBuildParticipant> getBuilderDefinition() {
		return DEFINITION;
	}
	
	@Override
	public ImList<IssueTypeSet> getIssueTypeSets() {
		return ImCollections.newList(WikidocSourceUnitModelContainer.ISSUE_TYPE_SET);
	}
	
	
	@Override
	protected WikitextBuildTask createBuildTask() {
		return new WikitextBuildTask(this);
	}
	
	@Override
	protected WikitextCleanTask createCleanTask() {
		return new WikitextCleanTask(this);
	}
	
}
