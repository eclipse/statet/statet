/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String CommandCategory_Sectioning_label;
	public static String CommandCategory_Label_label;
	public static String CommandCategory_WikitexttStyling_label;
	public static String CommandCategory_MathStyling_label;
	public static String CommandCategory_CommonSymbols_label;
	public static String CommandCategory_WikitexttSymbols_label;
	public static String CommandCategory_MathSymbols_GreekUpper_label;
	public static String CommandCategory_MathSymbols_GreekLower_label;
	public static String CommandCategory_MathSymbols_BinOp_label;
	public static String CommandCategory_MathSymbols_RootFracOp_label;
	public static String CommandCategory_MathSymbols_RelStd_label;
	public static String CommandCategory_MathSymbols_RelArrow_label;
	public static String CommandCategory_MathSymbols_RelMisc_label;
	public static String CommandCategory_MathSymbols_LargeOp_label;
	public static String CommandCategory_MathSymbols_NamedOp_label;
	public static String CommandCategory_MathSymbols_MiscAlpha_label;
	public static String CommandCategory_MathSymbols_MiscOrd_label;
	public static String CommandCategory_MathSymbols_Dots_label;
	public static String CommandCategory_MathSymbols_Accents_label;
	public static String CommandCategory_MathSymbols_Brackets_label;
	
	public static String WikitextProject_ConfigureTask_label;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
