/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceUnitModelInfo;


@NonNullByDefault
public class WikidocSourceUnitModelInfoImpl extends BasicSourceUnitModelInfo implements WikidocSourceUnitModelInfo {
	
	
	private final WikitextSourceElement sourceElement;
	
	private final NameAccessSet<WikitextNameAccess> linkAnchorLabels;
	private final NameAccessSet<WikitextNameAccess> linkDefLabels;
	
	private final int minSectionLevel;
	private final int maxSectionLevel;
	
	
	WikidocSourceUnitModelInfoImpl(final AstInfo ast, final WikitextSourceElement unitElement,
			final NameAccessSet<WikitextNameAccess> linkAnchorLabels,
			final NameAccessSet<WikitextNameAccess> linkDefLabels,
			final int minSectionLevel, final int maxSectionLevel) {
		super(ast);
		this.sourceElement= unitElement;
		
		this.linkAnchorLabels= linkAnchorLabels;
		this.linkDefLabels= linkDefLabels;
		
		this.minSectionLevel= minSectionLevel;
		this.maxSectionLevel= maxSectionLevel;
	}
	
	
	@Override
	public WikitextSourceElement getSourceElement() {
		return this.sourceElement;
	}
	
	@Override
	public WikitextAstInfo getAst() {
		return (WikitextAstInfo)super.getAst();
	}
	
	
	@Override
	public NameAccessSet<WikitextNameAccess> getLinkAnchorLabels() {
		return this.linkAnchorLabels;
	}
	
	@Override
	public NameAccessSet<WikitextNameAccess> getLinkRefLabels() {
		return this.linkDefLabels;
	}
	
	@Override
	public int getMinSectionLevel() {
		return this.minSectionLevel;
	}
	
	@Override
	public int getMaxSectionLevel() {
		return this.maxSectionLevel;
	}
	
}
