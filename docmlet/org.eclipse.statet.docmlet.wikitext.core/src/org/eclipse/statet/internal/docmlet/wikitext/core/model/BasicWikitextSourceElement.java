/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.wikitext.core.model.WikitextElementName;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.ltk.core.ElementName;


@NonNullByDefault
public abstract class BasicWikitextSourceElement implements WikitextSourceElement, TextRegion {
	
	
	protected static final ImList<BasicWikitextSourceElement> NO_CHILDREN= ImCollections.emptyList();
	
	
	private final int type;
	WikitextElementName name;
	int occurrenceCount;
	
	int startOffset;
	int length;
	
	
	protected BasicWikitextSourceElement(final int type) {
		this.type= type;
	}
	
	
	@Override
	public final String getModelTypeId() {
		return WikitextModel.WIKIDOC_TYPE_ID;
	}
	
	@Override
	public final int getElementType() {
		return this.type;
	}
	
	@Override
	public ElementName getElementName() {
		return this.name;
	}
	
	@Override
	public String getId() {
		final String name= getElementName().getDisplayName();
		final StringBuilder sb= new StringBuilder(name.length() + 16);
		sb.append(Integer.toHexString(getElementType() & MASK_C12));
		sb.append(':');
		sb.append(name);
		sb.append('#');
		sb.append(this.occurrenceCount);
		return sb.toString();
	}
	
	@Override
	public TextRegion getSourceRange() {
		return this;
	}
	
	@Override
	public int getStartOffset() {
		return this.startOffset;
	}
	
	@Override
	public int getEndOffset() {
		return this.startOffset + this.length;
	}
	
	@Override
	public int getLength() {
		return this.length;
	}
	
	@Override
	public @Nullable TextRegion getDocumentationRange() {
		return null;
	}
	
	
	@Override
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return (this.type & MASK_C12) * getElementName().hashCode() + this.occurrenceCount;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final BasicWikitextSourceElement other
						&& (this.type & MASK_C12) == (other.type & MASK_C12)
						&& this.occurrenceCount == other.occurrenceCount
						&& ((this.type & MASK_C1) == C1_SOURCE || getSourceParent().equals(other.getSourceParent()))
						&& getElementName().equals(other.getElementName()) ));
	}
	
}
