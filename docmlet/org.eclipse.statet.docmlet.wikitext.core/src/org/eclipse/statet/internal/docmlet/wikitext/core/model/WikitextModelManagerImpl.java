/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.model.build.WikidocSourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.impl.AbstractModelEventJob;
import org.eclipse.statet.ltk.model.core.impl.AbstractModelManager;


@NonNullByDefault
public class WikitextModelManagerImpl extends AbstractModelManager implements ModelManager, Disposable {
	
	
	static class EventJob extends AbstractModelEventJob<WikitextSourceUnit, WikidocSourceUnitModelInfo> {
		
		public EventJob(final WikitextModelManagerImpl manager) {
			super(manager);
		}
		
		@Override
		protected void dispose() {
			super.dispose();
		}
		
	}
	
	
	private final EventJob eventJob= new EventJob(this);
	
	private final WikidocReconciler reconciler= new WikidocReconciler(this);
	
	
	public WikitextModelManagerImpl() {
		super(WikitextModel.WIKIDOC_TYPE_ID);
	}
	
	
	@Override
	public void dispose() {
		this.eventJob.dispose();
	}
	
	
	public EventJob getEventJob() {
		return this.eventJob;
	}
	
	@Override
	public void reconcile(final SourceUnitModelContainer<?, ?> adapter,
			final int level, final IProgressMonitor monitor) {
		if (adapter instanceof WikidocSourceUnitModelContainer) {
			this.reconciler.reconcile((WikidocSourceUnitModelContainer<?>) adapter, level, monitor);
		}
	}
	
}
