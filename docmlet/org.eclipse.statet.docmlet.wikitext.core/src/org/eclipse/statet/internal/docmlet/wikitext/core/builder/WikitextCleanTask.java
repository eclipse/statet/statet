/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.builder;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.WikitextBuildParticipant;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocWorkspaceSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProject;
import org.eclipse.statet.ltk.project.core.builder.ProjectCleanTask;


@NonNullByDefault
public class WikitextCleanTask extends ProjectCleanTask<WikitextProject, WikidocWorkspaceSourceUnit, WikitextBuildParticipant> {
	
	
	public WikitextCleanTask(final WikitextProjectBuilder builder) {
		super(builder);
	}
	
	
}
