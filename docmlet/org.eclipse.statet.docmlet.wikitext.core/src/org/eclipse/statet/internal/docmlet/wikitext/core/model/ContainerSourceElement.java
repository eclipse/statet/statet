/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextElementName;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceContainerElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;


@NonNullByDefault
public abstract class ContainerSourceElement extends BasicWikitextSourceElement {
	
	
	List<BasicWikitextSourceElement> children= NO_CHILDREN;
	TextRegion nameRegion;
	
	private final WikitextAstNode astNode;
	
	
	public ContainerSourceElement(final int type, final WikitextAstNode astNode) {
		super(type);
		this.astNode= astNode;
	}
	
	
	@Override
	public TextRegion getNameSourceRange() {
		return this.nameRegion;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return LtkModelUtils.<SourceStructElement<?, ?>>hasChildren(this.children, filter);
	}
	
	@Override
	public List<? extends SourceStructElement<?, ?>> getSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		return LtkModelUtils.<SourceStructElement<?, ?>>getChildren(this.children, filter);
	}
	
	@Override
	public abstract @Nullable ContainerSourceElement getModelParent();
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super WikitextSourceElement> filter) {
		return LtkModelUtils.<WikitextSourceElement>hasChildren(this.children, filter);
	}
	
	@Override
	public List<? extends WikitextSourceElement> getModelChildren(final @Nullable LtkModelElementFilter<? super WikitextSourceElement> filter) {
		return LtkModelUtils.<WikitextSourceElement>getChildren(this.children, filter);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == AstNode.class) {
			return (T)this.astNode;
		}
		return super.getAdapter(adapterType);
	}
	
	
	public static class SourceContainer extends ContainerSourceElement
			implements SourceContainerElement<WikitextSourceElement> {
		
		
		private final SourceUnit sourceUnit;
		
		private final SourceModelStamp stamp;
		
		
		public SourceContainer(final int type, final SourceUnit sourceUnit,
				final SourceModelStamp stamp,
				final WikitextAstNode astNode) {
			super(type, astNode);
			this.sourceUnit= sourceUnit;
			this.stamp= stamp;
		}
		
		
		@Override
		public String getId() {
			return this.sourceUnit.getId();
		}
		
		@Override
		public WikitextElementName getElementName() {
			final ElementName elementName= this.sourceUnit.getElementName();
			if (elementName instanceof WikitextElementName) {
				return (WikitextElementName)elementName;
			}
			return WikitextElementName.create(WikitextElementName.RESOURCE, elementName.getSegmentName());
		}
		
		@Override
		public SourceUnit getSourceUnit() {
			return this.sourceUnit;
		}
		
		@Override
		public SourceModelStamp getStamp() {
			return this.stamp;
		}
		
		@Override
		public boolean exists() {
			final SourceUnitModelInfo modelInfo= getSourceUnit().getModelInfo(WikitextModel.WIKIDOC_TYPE_ID, 0, null);
			return (modelInfo != null && modelInfo.getSourceElement() == this);
		}
		
		@Override
		public boolean isReadOnly() {
			return this.sourceUnit.isReadOnly();
		}
		
		@Override
		public @Nullable SourceStructElement<?, ?> getSourceParent() {
			return null;
		}
		
		@Override
		public @Nullable ContainerSourceElement getModelParent() {
			return null;
		}
		
	}
	
	public static class StructContainer extends ContainerSourceElement {
		
		
		private final ContainerSourceElement parent;
		
		
		public StructContainer(final int type, final ContainerSourceElement parent, final WikitextAstNode astNode) {
			super(type, astNode);
			this.parent= parent;
			
			this.startOffset= astNode.getStartOffset();
			this.length= astNode.getLength();
		}
		
		
		@Override
		public SourceUnit getSourceUnit() {
			return this.parent.getSourceUnit();
		}
		
		@Override
		public boolean exists() {
			return this.parent.exists();
		}
		
		@Override
		public boolean isReadOnly() {
			return this.parent.isReadOnly();
		}
		
		@Override
		public ContainerSourceElement getSourceParent() {
			return this.parent;
		}
		
		@Override
		public ContainerSourceElement getModelParent() {
			return this.parent;
		}
		
	}
	
}
