/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core.model;

import static org.eclipse.statet.docmlet.wikitext.core.model.WikitextElement.C12_SECTIONING;
import static org.eclipse.statet.ltk.model.core.element.LtkModelElement.MASK_C1;
import static org.eclipse.statet.ltk.model.core.element.LtkModelElement.MASK_C12;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.statet.jcommons.text.core.BasicTextRegion;

import org.eclipse.statet.docmlet.wikitext.core.ast.Block;
import org.eclipse.statet.docmlet.wikitext.core.ast.Embedded;
import org.eclipse.statet.docmlet.wikitext.core.ast.Heading;
import org.eclipse.statet.docmlet.wikitext.core.ast.Image;
import org.eclipse.statet.docmlet.wikitext.core.ast.Label;
import org.eclipse.statet.docmlet.wikitext.core.ast.Link;
import org.eclipse.statet.docmlet.wikitext.core.ast.SourceComponent;
import org.eclipse.statet.docmlet.wikitext.core.ast.Span;
import org.eclipse.statet.docmlet.wikitext.core.ast.Text;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstVisitor;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextElement;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextElementName;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;
import org.eclipse.statet.ltk.model.core.impl.BasicNameAccessSet;
import org.eclipse.statet.ltk.model.core.impl.NameAccessAccumulator;


public class SourceAnalyzer extends WikitextAstVisitor {
	
	
	private static final Integer ONE= 1;
	
	
	private String input;
	
	private ContainerSourceElement currentElement;
	
	private final StringBuilder titleBuilder= new StringBuilder();
	private boolean titleDoBuild;
	private ContainerSourceElement titleElement;
	private final Map<String, Integer> structNamesCounter= new HashMap<>();
	
	private final List<EmbeddingReconcileTask> embeddedItems= new ArrayList<>();
	
	private int minSectionLevel;
	private int maxSectionLevel;
	
	private Map<String, NameAccessAccumulator<WikitextNameAccess>> linkAnchorLabels= new HashMap<>();
	private Map<String, NameAccessAccumulator<WikitextNameAccess>> linkDefLabels= new HashMap<>();
	
	
	public void clear() {
		this.input= null;
		this.currentElement= null;
		
		this.titleBuilder.setLength(0);
		this.titleDoBuild= false;
		this.titleElement= null;
		
		this.embeddedItems.clear();
		
		this.minSectionLevel= Integer.MAX_VALUE;
		this.maxSectionLevel= Integer.MIN_VALUE;
	}
	
	public WikidocSourceUnitModelInfoImpl createModel(final WikitextSourceUnit sourceUnit,
			final String input, final AstInfo ast) {
		clear();
		this.input= input;
		if (!(ast.getRoot() instanceof WikitextAstNode)) {
			return null;
		}
		
		if (!this.linkAnchorLabels.isEmpty()) {
			this.linkAnchorLabels.clear();
		}
		if (!this.linkDefLabels.isEmpty()) {
			this.linkDefLabels.clear();
		}
		final WikitextSourceElement root= this.currentElement= new ContainerSourceElement.SourceContainer(
				WikitextSourceElement.C12_SOURCE_FILE, sourceUnit, ast.getStamp(),
				(WikitextAstNode)ast.getRoot() );
		try {
			((WikitextAstNode) ast.getRoot()).acceptInWikitext(this);
			
			if (this.minSectionLevel == Integer.MAX_VALUE) {
				this.minSectionLevel= 0;
				this.maxSectionLevel= 0;
			}
			final NameAccessSet<WikitextNameAccess> linkAnchorLabels;
			final NameAccessSet<WikitextNameAccess> linkDefLabels;
			if (this.linkAnchorLabels.isEmpty()) {
				linkAnchorLabels= BasicNameAccessSet.emptySet();
			}
			else {
				linkAnchorLabels= new BasicNameAccessSet<>(this.linkAnchorLabels);
				this.linkAnchorLabels= new HashMap<>();
			}
			if (this.linkDefLabels.isEmpty()) {
				linkDefLabels= BasicNameAccessSet.emptySet();
			}
			else {
				linkDefLabels= new BasicNameAccessSet<>(this.linkDefLabels);
				this.linkDefLabels= new HashMap<>();
			}
			
			final WikidocSourceUnitModelInfoImpl model= new WikidocSourceUnitModelInfoImpl(ast, root,
					linkAnchorLabels, linkDefLabels,
					this.minSectionLevel, this.maxSectionLevel );
			return model;
		}
		catch (final InvocationTargetException e) {
			throw new IllegalStateException();
		}
	}
	
	public List<EmbeddingReconcileTask> getEmbeddedItems() {
		return this.embeddedItems;
	}
	
	
	private void initElement(final ContainerSourceElement element) {
		if (this.currentElement.children.isEmpty()) {
			this.currentElement.children= new ArrayList<>();
		}
		this.currentElement.children.add(element);
		this.currentElement= element;
	}
	
	private void exitContainer(final int stop, final boolean forward) {
		this.currentElement.length= ((forward) ?
						readLinebreakForward((stop >= 0) ? stop : this.currentElement.startOffset + this.currentElement.length, this.input.length()) :
						readLinebreakBackward((stop >= 0) ? stop : this.currentElement.startOffset + this.currentElement.length, 0) ) -
				this.currentElement.startOffset;
		final List<BasicWikitextSourceElement> children= this.currentElement.children;
		if (!children.isEmpty()) {
			for (final BasicWikitextSourceElement element : children) {
				if ((element.getElementType() & MASK_C12) == C12_SECTIONING) {
					final Map<String, Integer> names= this.structNamesCounter;
					final String name= element.getElementName().getDisplayName();
					final Integer occ= names.get(name);
					if (occ == null) {
						names.put(name, ONE);
					}
					else {
						names.put(name, Integer.valueOf(
								(element.occurrenceCount= occ + 1) ));
					}
				}
			}
			this.structNamesCounter.clear();
		}
		this.currentElement= this.currentElement.getModelParent();
	}
	
	private void finishTitleText() {
		{	boolean wasWhitespace= false;
			int idx= 0;
			while (idx < this.titleBuilder.length()) {
				if (this.titleBuilder.charAt(idx) == ' ') {
					if (wasWhitespace) {
						this.titleBuilder.deleteCharAt(idx);
					}
					else {
						wasWhitespace= true;
						idx++;
					}
				}
				else {
					wasWhitespace= false;
					idx++;
				}
			}
		}
		this.titleElement.name= WikitextElementName.create(WikitextElementName.TITLE, this.titleBuilder.toString());
		this.titleBuilder.setLength(0);
		this.titleElement= null;
		this.titleDoBuild= false;
	}
	
	
	private int readLinebreakForward(int offset, final int limit) {
		if (offset < limit) {
			switch(this.input.charAt(offset)) {
			case '\n':
				if (++offset < limit && this.input.charAt(offset) == '\r') {
					return ++offset;
				}
				return offset;
			case '\r':
				if (++offset < limit && this.input.charAt(offset) == '\n') {
					return ++offset;
				}
				return offset;
			}
		}
		return offset;
	}
	private int readLinebreakBackward(int offset, final int limit) {
		if (offset > limit) {
			switch(this.input.charAt(offset-1)) {
			case '\n':
				if (--offset > limit && this.input.charAt(offset-1) == '\r') {
					return --offset;
				}
				return offset;
			case '\r':
				if (--offset < limit && this.input.charAt(offset-1) == '\n') {
					return --offset;
				}
				return offset;
			}
		}
		return offset;
	}
	
	private RefLabelAccess addLinkAnchorAccess(final WikitextAstNode node) {
		final String label= node.getLabel();
		NameAccessAccumulator<WikitextNameAccess> shared= this.linkAnchorLabels.get(label);
		if (shared == null) {
			shared= new NameAccessAccumulator<>(label);
			this.linkAnchorLabels.put(label, shared);
		}
		final RefLabelAccess access= new RefLabelAccess.LinkAnchor(shared, node, null);
		node.addAttachment(access);
		return access;
	}
	
	private RefLabelAccess addLinkDefAccess(final WikitextAstNode node, final Label labelNode) {
		final String label= labelNode.getText();
		NameAccessAccumulator<WikitextNameAccess> shared= this.linkDefLabels.get(label);
		if (shared == null) {
			shared= new NameAccessAccumulator<>(label);
			this.linkDefLabels.put(label, shared);
		}
		final RefLabelAccess access= new RefLabelAccess.LinkDef(shared, node, labelNode);
		node.addAttachment(access);
		return access;
	}
	
	
	@Override
	public void visit(final SourceComponent node) throws InvocationTargetException {
		this.currentElement.startOffset= node.getStartOffset();
		node.acceptInWikitextChildren(this);
		if (this.titleElement != null) {
			finishTitleText();
		}
		while ((this.currentElement.getElementType() & MASK_C1) != WikitextElement.C1_SOURCE) {
			exitContainer(node.getEndOffset(), true);
		}
		exitContainer(node.getEndOffset(), true);
	}
	
	@Override
	public void visit(final Block node) throws InvocationTargetException {
		if (node.getLabel() != null) {
			final RefLabelAccess access= addLinkAnchorAccess(node);
			access.flags|= RefLabelAccess.A_WRITE;
		}
		
		node.acceptInWikitextChildren(this);
		
		this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
	}
	
	@Override
	public void visit(final Heading node) throws InvocationTargetException {
		if (node.getLabel() != null) {
			final RefLabelAccess access= addLinkAnchorAccess(node);
			access.flags|= RefLabelAccess.A_WRITE;
		}
		
		COMMAND: {
			if ((this.currentElement.getElementType() & MASK_C12) == WikitextElement.C12_SECTIONING
					|| (this.currentElement.getElementType() & MASK_C1) == WikitextElement.C1_SOURCE ) {
				final int level= node.getLevel();
				if (level > 5) {
					break COMMAND;
				}
				if (this.titleElement != null) {
					finishTitleText();
					break COMMAND;
				}
				
				while ((this.currentElement.getElementType() & MASK_C12) == WikitextElement.C12_SECTIONING
						&& (this.currentElement.getElementType() & 0xf) >= level) {
					exitContainer(node.getStartOffset(), false);
				}
				initElement(new ContainerSourceElement.StructContainer(
						WikitextElement.C12_SECTIONING | level, this.currentElement, node ));
				
				node.addAttachment(this.currentElement);
				
				this.minSectionLevel= Math.min(this.minSectionLevel, level);
				this.maxSectionLevel= Math.max(this.maxSectionLevel, level);
				
				final int count= node.getChildCount();
				if (count > 0) {
					this.titleElement= this.currentElement;
					this.titleDoBuild= true;
					
					final int nameStartOffset= node.getChild(0).getStartOffset();
					final int nameEndOffset= readLinebreakBackward(node.getChild(count - 1).getEndOffset(), nameStartOffset);
					this.titleElement.nameRegion= new BasicTextRegion(nameStartOffset, nameEndOffset);
					
					node.acceptInWikitextChildren(this);
					if (this.titleElement != null) {
						finishTitleText();
					}
				}
				else {
					this.currentElement.name= WikitextElementName.create(WikitextElementName.TITLE, ""); //$NON-NLS-1$
					this.currentElement.nameRegion= new BasicTextRegion(node.getStartOffset());
				}
				this.currentElement.length= Math.max(this.currentElement.length, node.getLength());
				return;
			}
		}
		
		node.acceptInWikitextChildren(this);
		
		this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
	}
	
	@Override
	public void visit(final Span node) throws InvocationTargetException {
		if (node.getLabel() != null) {
			final RefLabelAccess access= addLinkAnchorAccess(node);
			access.flags|= RefLabelAccess.A_WRITE;
		}
		
		node.acceptInWikitextChildren(this);
	}
	
	@Override
	public void visit(final Text node) throws InvocationTargetException {
		if (this.titleDoBuild) {
			final String text= node.getText();
			if (text != null) {
				this.titleBuilder.append(text);
				if (this.titleBuilder.length() >= 100) {
					finishTitleText();
				}
			}
		}
		
		this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
	}
	
	@Override
	public void visit(final Link node) throws InvocationTargetException {
		final RefLabelAccess access;
		switch (node.getLinkType()) {
		case Link.LINK_REF_DEFINITION:
			access= addLinkDefAccess(node, node.getReferenceLabel());
			access.flags|= RefLabelAccess.A_WRITE;
			break;
		case Link.LINK_BY_REF:
			access= addLinkDefAccess(node, node.getReferenceLabel());
			break;
		default:
			break;
		}
		super.visit(node);
	}
	
	@Override
	public void visit(final Image node) throws InvocationTargetException {
		final RefLabelAccess access;
		switch (node.getImageType()) {
		case Image.SRC_BY_REF:
			access= addLinkDefAccess(node, node.getReferenceLabel());
			break;
		default:
			break;
		}
		super.visit(node);
	}
	
	@Override
	public void visit(final Embedded node) throws InvocationTargetException {
		if (node.getForeignNode() == null) {
			super.visit(node);
			return;
		}
		if ((node.getEmbedDescr() & 0b0_00000011) == Embedded.EMBED_INLINE) {
			if (this.titleDoBuild) {
				this.titleBuilder.append(this.input, node.getStartOffset(), node.getEndOffset());
				if (this.titleBuilder.length() >= 100) {
					finishTitleText();
				}
			}
			this.embeddedItems.add(new EmbeddingReconcileTask(node, null));
		}
		else {
			if (this.titleElement != null) {
				finishTitleText();
			}
			if (this.currentElement.children.isEmpty()) {
				this.currentElement.children= new ArrayList<>();
			}
			final EmbeddingForeignSourceElement element= new EmbeddingForeignSourceElement(node.getText(),
					this.currentElement, node );
			element.startOffset= node.getStartOffset();
			element.length= node.getLength();
			element.name= WikitextElementName.create(0, ""); //$NON-NLS-1$
			this.currentElement.children.add(element);
			this.embeddedItems.add(new EmbeddingReconcileTask(node, element));
		}
		
		this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
	}
	
}
