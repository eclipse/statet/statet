/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.core;

import org.eclipse.statet.ecommons.preferences.PreferencesManageListener;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCodeStyleSettings;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCoreAccess;


final class BasicWikitextCoreAccess implements WikitextCoreAccess {
	
	
	private boolean isDisposed;
	
	private final PreferenceAccess prefs;
	
	private PreferencesManageListener codeStyleListener;
	private WikitextCodeStyleSettings codeStyle;
	
	
	public BasicWikitextCoreAccess(final PreferenceAccess prefs) {
		this.prefs= prefs;
		
		this.codeStyle= new WikitextCodeStyleSettings(1);
		this.codeStyleListener= new PreferencesManageListener(this.codeStyle, this.prefs, WikitextCodeStyleSettings.ALL_GROUP_IDS);
		this.codeStyle.load(prefs);
		this.codeStyle.resetDirty();
	}
	
	
	@Override
	public PreferenceAccess getPrefs() {
		return this.prefs;
	}
	
	@Override
	public WikitextCodeStyleSettings getWikitextCodeStyle() {
		WikitextCodeStyleSettings codeStyle= this.codeStyle;
		if (codeStyle == null) {
			synchronized (this) {
				codeStyle= this.codeStyle;
				if (codeStyle == null) {
					codeStyle= new WikitextCodeStyleSettings(1);
					if (!this.isDisposed) {
						this.codeStyleListener= new PreferencesManageListener(codeStyle,
								this.prefs, WikitextCodeStyleSettings.ALL_GROUP_IDS );
					}
					codeStyle.load(this.prefs);
					codeStyle.resetDirty();
					this.codeStyle= codeStyle;
				}
			}
		}
		return codeStyle;
	}
	
	
	public synchronized void dispose() {
		this.isDisposed= true;
		
		if (this.codeStyleListener != null) {
			this.codeStyleListener.dispose();
			this.codeStyleListener= null;
		}
	}
	
}
