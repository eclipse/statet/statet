/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.doc;

import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.IDocumentPartitionerExtension3;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;


@NonNullByDefault
public interface MarkupLanguagePartitioner extends IDocumentPartitioner, IDocumentPartitionerExtension3 {
	
	
	WikitextMarkupLanguage getMarkupLanguage();
	
	void setMarkupLanguage(final WikitextMarkupLanguage language);
	
	void reset(); // for changed configuration
	
}
