/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class WikitextAstVisitor {
	
	
	public void visit(final SourceComponent node) throws InvocationTargetException {
		node.acceptInWikitextChildren(this);
	}
	
	public void visit(final Block node) throws InvocationTargetException {
		node.acceptInWikitextChildren(this);
	}
	
	public void visit(final Heading node) throws InvocationTargetException {
		node.acceptInWikitextChildren(this);
	}
	
	public void visit(final Span node) throws InvocationTargetException {
		node.acceptInWikitextChildren(this);
	}
	
	public void visit(final Text node) throws InvocationTargetException {
	}
	
	public void visit(final Link node) throws InvocationTargetException {
		node.acceptInWikitextChildren(this);
	}
	
	public void visit(final Image node) throws InvocationTargetException {
	}
	
	public void visit(final Label node) throws InvocationTargetException {
	}
	
	public void visit(final Control node) throws InvocationTargetException {
	}
	
	
	public void visit(final Verbatim node) throws InvocationTargetException {
	}
	
	public void visit(final Comment node) throws InvocationTargetException {
	}
	
	public void visit(final Dummy node) throws InvocationTargetException {
	}
	
	public void visit(final Embedded node) throws InvocationTargetException {
		node.acceptInWikitextChildren(this);
	}
	
}
