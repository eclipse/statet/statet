/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public abstract class Image extends WikitextAstNode {
	
	
	public static final byte COMMON= 0;
	public static final byte SRC_BY_REF= 2;
	
	
	static final class Ref extends Image {
		
		
		private final Label referenceLabel;
		
		
		Ref(final WikitextAstNode parent, final int startOffset, final int endOffset,
				final byte linkType, final Label referenceLabel) {
			super(parent, startOffset, endOffset, linkType, null);
			
			referenceLabel.parent= this;
			this.referenceLabel= nonNullAssert(referenceLabel);
		}
		
		
		@Override
		public boolean hasChildren() {
			return true;
		}
		
		@Override
		public int getChildCount() {
			return 1;
		}
		
		@Override
		public WikitextAstNode getChild(final int index) {
			if (index == 0) {
				return this.referenceLabel;
			}
			throw new IndexOutOfBoundsException(Integer.toString(index));
		}
		
		@Override
		public int getChildIndex(final AstNode child) {
			if (this.referenceLabel == child) {
				return 0;
			}
			return -1;
		}
		
		@Override
		public Label getReferenceLabel() {
			return this.referenceLabel;
		}
		
		
		@Override
		public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
			this.referenceLabel.accept(visitor);
		}
		
		@Override
		public void acceptInWikitextChildren(final WikitextAstVisitor visitor) throws InvocationTargetException {
			this.referenceLabel.acceptInWikitext(visitor);
		}
		
	}
	
	static final class Common extends Image {
		
		
		Common(final WikitextAstNode parent, final int startOffset, final int endOffset,
				final byte linkType, final String href) {
			super(parent, startOffset, endOffset, linkType, href);
		}
		
		
		@Override
		public @Nullable Label getReferenceLabel() {
			return null;
		}
		
	}
	
	
	private final byte linkType;
	
	private final @Nullable String uri;
	
	
	private Image(final WikitextAstNode parent, final int startOffset, final int endOffset,
			final byte linkType, final @Nullable String uri) {
		super(parent);
		doSetStartEndOffset(startOffset, endOffset);
		
		this.linkType= linkType;
		this.uri= uri;
	}
	
	private Image(final WikitextAstNode parent, final int startOffset,
			final String uri) {
		super(parent);
		doSetStartEndOffset(startOffset);
		
		this.linkType= 0;
		this.uri= uri;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.LINK;
	}
	
	
	@Override
	public boolean hasChildren() {
		return false;
	}
	
	@Override
	public int getChildCount() {
		return 0;
	}
	
	@Override
	public WikitextAstNode getChild(final int index) {
		throw new IndexOutOfBoundsException();
	}
	
	@Override
	public int getChildIndex(final AstNode child) {
		return -1;
	}
	
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
	}
	
	@Override
	public void acceptInWikitext(final WikitextAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public void acceptInWikitextChildren(final WikitextAstVisitor visitor) throws InvocationTargetException {
	}
	
	
	public byte getImageType() {
		return this.linkType;
	}
	
	public String getUri() {
		return this.uri;
	}
	
	public abstract Label getReferenceLabel();
	
}
