/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source;

import java.util.Arrays;

import org.eclipse.statet.ltk.core.source.SourceContent;


public abstract class WeaveParticipant {
	
	
	protected static final char REPLACEMENT_CHAR= '\uFFFC';
	
	protected static final String REPLACEMENT_STRING;
	
	protected static final String REPLACEMENT_LINE;
	static {
		final char[] chars= new char[41];
		Arrays.fill(chars, 'x');
		chars[40]= '\n';
		REPLACEMENT_STRING= new String(chars, 0, 40);
		REPLACEMENT_LINE= new String(chars, 0, 41);
	}
	
	
	protected WeaveParticipant() {
	}
	
	
	public abstract String getForeignTypeId();
	
	public abstract int getEmbedDescr();
	
	public void reset(final SourceContent source) {
	}
	
}
