/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.doc;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;


/**
 * The document setup participant for Wikitext documents.
 */
public class WikidocDocumentSetupParticipant extends MarkupLanguageDocumentSetupParticipant1 {
	
	
	public WikidocDocumentSetupParticipant(final WikitextMarkupLanguage markupLanguage) {
		this(markupLanguage, false);
	}
	
	public WikidocDocumentSetupParticipant(final WikitextMarkupLanguage markupLanguage,
			final boolean templateMode) {
		super(markupLanguage, (templateMode) ? WikitextMarkupLanguage.TEMPLATE_MODE : 0);
	}
	
	
	@Override
	public String getPartitioningId() {
		return WikitextDocumentConstants.WIKIDOC_PARTITIONING;
	}
	
	@Override
	protected MarkupLanguagePartitioner createDocumentPartitioner(final WikitextMarkupLanguage markupLanguage) {
		return new WikitextPartitioner(getPartitioningId(),
				new WikitextPartitionNodeScanner(markupLanguage, getMarkupLanguageMode()),
				WikitextDocumentConstants.WIKIDOC_CONTENT_TYPES );
	}
	
}
