/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.extdoc;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNodeScanner;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNodeType;

import org.eclipse.statet.docmlet.tex.core.source.doc.LtxPartitionNodeScanner;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxPartitionNodeType;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.source.EmbeddingAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextDocumentConstants;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextPartitionNodeType;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextWeavePartitionNodeScanner;
import org.eclipse.statet.yaml.core.source.doc.YamlPartitionNodeScanner;


public class WikidocPartitionNodeScanner extends WikitextWeavePartitionNodeScanner {
	
	
	private static final List<HtmlPartitionNodeType.Default> HTML_DEFAULT_HTML_TYPES= ImCollections.newList(
			new HtmlPartitionNodeType.Default(),
			new HtmlPartitionNodeType.Default(),
			new HtmlPartitionNodeType.Default(),
			new HtmlPartitionNodeType.Default(),
			new HtmlPartitionNodeType.Default(),
			new HtmlPartitionNodeType.Default(),
			new HtmlPartitionNodeType.Default(),
			new HtmlPartitionNodeType.Default() );
	private static final HtmlPartitionNodeType HTML_COMMENT_HTML_TYPE= HtmlPartitionNodeType.COMMENT;
	
	
	private static final WikitextPartitionNodeType YAML_CHUNK_WIKITEXT_TYPE= new WikitextPartitionNodeType();
	
	
	public static final LtxPartitionNodeType TEX_BASE_TEX_TYPE= new LtxPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE;
		}
		
		@Override
		public byte getScannerState() {
			return LtxPartitionNodeType.DEFAULT_ROOT.getScannerState();
		}
		
	};
	public static final WikitextPartitionNodeType TEX_MATH_WIKITEXT_TYPE= new WikitextPartitionNodeType();
	
	
	private YamlPartitionNodeScanner yamlScanner;
	
	private LtxPartitionNodeScanner ltxScanner;
	
	
	public WikidocPartitionNodeScanner(final WikitextMarkupLanguage markupLanguage) {
		this(markupLanguage, 0);
	}
	
	public WikidocPartitionNodeScanner(final WikitextMarkupLanguage markupLanguage,
			final int markupLanguageMode) {
		super(markupLanguage, markupLanguageMode);
	}
	
	
	private TreePartitionNodeScanner getYamlScanner() {
		if (this.yamlScanner == null) {
			this.yamlScanner= new YamlPartitionNodeScanner();
		}
		return this.yamlScanner;
	}
	
	private TreePartitionNodeScanner getLtxScanner() {
		if (this.ltxScanner == null) {
			this.ltxScanner= new LtxPartitionNodeScanner(isTemplateMode());
		}
		return this.ltxScanner;
	}
	
	@Override
	protected void beginEmbeddingBlock(final BlockType type, final Embedding embedding) {
		if (type == BlockType.CODE
				&& embedding.getForeignType() == ExtdocMarkupLanguage.EMBEDDED_HTML) {
			final EmbeddingAttributes attributes= embedding.getAttributes();
			final HtmlPartitionNodeType htmlType= ((attributes.getEmbedDescr() & ExtdocMarkupLanguage.EMBEDDED_HTML_COMMENT_FLAG) != 0) ?
					HTML_COMMENT_HTML_TYPE : HTML_DEFAULT_HTML_TYPES.get(
							(attributes.getEmbedDescr() & ExtdocMarkupLanguage.EMBEDDED_HTML_DISTINCT_MASK) >> ExtdocMarkupLanguage.EMBEDDED_HTML_DISTINCT_SHIFT);
			addNode(htmlType, getEventBeginOffset());
			return;
		}
		if (type == BlockType.CODE
				&& embedding.getForeignType() == ExtdocMarkupLanguage.EMBEDDED_YAML) {
			addNode(YAML_CHUNK_WIKITEXT_TYPE, getEventBeginOffset());
			embedding.init(getNode());
			return;
		}
		super.beginEmbeddingBlock(type, embedding);
	}
	
	
	@Override
	protected void endEmbeddingBlock(final TreePartitionNodeType type, final Embedding embedding) {
		if (type == YAML_CHUNK_WIKITEXT_TYPE) {
			embedding.executeForeignScanner(getYamlScanner());
			exitNode(getEventEndOffset(), getEventFlags(embedding.getAttributes()));
			return;
		}
		if (type instanceof HtmlPartitionNodeType) {
			exitNode(getEventEndOffset(), getEventFlags(embedding.getAttributes()));
			return;
		}
		super.endEmbeddingBlock(type, embedding);
	}
	
	@Override
	protected void beginEmbeddingSpan(final SpanType type, final Embedding embedding) {
		if (type == SpanType.CODE
				&& embedding.getForeignType() == ExtdocMarkupLanguage.EMBEDDED_LTX) {
			addNode(TEX_BASE_TEX_TYPE, getEventBeginOffset());
			embedding.init(getNode());
			return;
		}
		super.beginEmbeddingSpan(type, embedding);
	}
	
	@Override
	protected void endEmbeddingSpan(final TreePartitionNodeType type, final Embedding embedding) {
		if (type == TEX_BASE_TEX_TYPE) {
			embedding.executeForeignScanner(getLtxScanner());
			exitNode(getEventEndOffset(), getEventFlags(embedding.getAttributes()));
			return;
		}
		super.endEmbeddingSpan(type, embedding);
	}
	
}
