/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;


@NonNullByDefault
public abstract class Block extends ContainerNode {
	
	
	static final class TextBlock extends Block {
		
		
		private final ImList<? extends TextRegion> textRegions;
		
		TextBlock(final WikitextAstNode parent, final int offset, final BlockType blockType,
				final @Nullable String label, final ImList<? extends TextRegion> textRegions) {
			super(parent, offset, blockType, label);
			this.textRegions= textRegions;
		}
		
		
		@Override
		public ImList<? extends TextRegion> getTextRegions() {
			return this.textRegions;
		}
		
	}
	
	static final class Common extends Block {
		
		Common(final WikitextAstNode parent, final int offset, final BlockType blockType,
				final @Nullable String label) {
			super(parent, offset, blockType, label);
		}
		
		
		@Override
		public @Nullable ImList<? extends TextRegion> getTextRegions() {
			return null;
		}
		
	}
	
	
	private final BlockType blockType;
	
	private final @Nullable String label;
	
	
	private Block(final WikitextAstNode parent, final int offset, final BlockType blockType,
			final @Nullable String label) {
		super(parent);
		doSetStartEndOffset(offset);
		
		this.blockType= blockType;
		this.label= label;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.BLOCK;
	}
	
	public BlockType getBlockType() {
		return this.blockType;
	}
	
	@Override
	public @Nullable String getLabel() {
		return this.label;
	}
	
	public abstract @Nullable ImList<? extends TextRegion> getTextRegions();
	
	
	@Override
	public void acceptInWikitext(final WikitextAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
}
