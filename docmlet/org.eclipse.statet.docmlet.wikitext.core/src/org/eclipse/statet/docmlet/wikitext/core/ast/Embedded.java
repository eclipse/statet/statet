/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;


@NonNullByDefault
public class Embedded extends ContainerNode implements EmbeddingAstNode {
	
	
	private final String foreignType;
	private final int embedDescr;
	
	private @Nullable AstNode foreignNode;
	
	
	Embedded(final WikitextAstNode parent, final int startOffset, final int endOffset,
			final String foreignType, final int embedDescr) {
		super(parent);
		doSetStartEndOffset(startOffset, endOffset);
		this.foreignType= foreignType;
		this.embedDescr= embedDescr;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.EMBEDDED;
	}
	
	@Override
	public String getForeignTypeId() {
		return this.foreignType;
	}
	
	@Override
	public int getEmbedDescr() {
		return this.embedDescr;
	}
	
	
	@Override
	public void setForeignNode(final @Nullable AstNode node) {
		this.foreignNode= node;
		if (node != null) {
			doAddStatusSeverityOfChild(node.getStatusCode());
		}
	}
	
	@Override
	public @Nullable AstNode getForeignNode() {
		return this.foreignNode;
	}
	
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		// ambiguous! (at moment HTML => no foreignNode)
		if (this.foreignNode != null) {
			this.foreignNode.accept(visitor);
		}
		else {
			super.acceptInChildren(visitor);
		}
	}
	
	@Override
	public void acceptInWikitext(final WikitextAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public String getText() {
		return this.foreignType;
	}
	
}
