/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager1;
import org.eclipse.statet.internal.docmlet.wikitext.core.MarkupLanguageManager1;
import org.eclipse.statet.internal.docmlet.wikitext.core.WikitextCorePlugin;
import org.eclipse.statet.internal.docmlet.wikitext.core.WorkspaceAdapterFactory;


@NonNullByDefault
public class WikitextCore {
	
	public static final String BUNDLE_ID= "org.eclipse.statet.docmlet.wikitext.core"; //$NON-NLS-1$
	
	public static final String WIKIDOC_CONTENT_ID= "org.eclipse.mylyn.wikitext"; //$NON-NLS-1$
	
	public static final IContentType WIKIDOC_CONTENT_TYPE;
	
	/**
	 * Content type id for Wikitext documents
	 */
	public static final String WIKIDOC_CONTENT_ID_NG= "org.eclipse.statet.docmlet.contentTypes.Wikidoc"; //$NON-NLS-1$
	
	
	static {
		final IContentTypeManager contentTypeManager= Platform.getContentTypeManager();
		WIKIDOC_CONTENT_TYPE= contentTypeManager.getContentType(WIKIDOC_CONTENT_ID);
	}
	
	
	private static final WikitextCoreAccess WORKBENCH_ACCESS= WikitextCorePlugin.getInstance().getWorkbenchAccess();
	
	public static WikitextCoreAccess getWorkbenchAccess() {
		return WORKBENCH_ACCESS;
	}
	
	public static WikitextCoreAccess getDefaultsAccess() {
		return WikitextCorePlugin.getInstance().getDefaultsAccess();
	}
	
	public static WikitextCoreAccess getContextAccess(final @Nullable IAdaptable adaptable) {
		if (adaptable != null) {
			WikitextCoreAccess coreAccess= WorkspaceAdapterFactory.getResourceCoreAccess(adaptable);
			if (coreAccess == null) {
				coreAccess= adaptable.getAdapter(WikitextCoreAccess.class);
			}
			if (coreAccess != null) {
				return coreAccess;
			}
		}
		return getWorkbenchAccess();
	}
	
	
	public static WikitextMarkupLanguageManager1 getMarkupLanguageManager() {
		return MarkupLanguageManager1.INSTANCE;
	}
	
}
