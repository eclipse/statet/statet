/*=============================================================================#
 # Copyright (c) 2021, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.project;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.base.core.project.DocTasks;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;


@NonNullByDefault
public final class WikitextIssues {
	
	
	public static final IssueTypeSet.TaskCategory TASK_CATEGORY= new IssueTypeSet.TaskCategory(
			DocTasks.TASK_MARKER_TYPE, null );
	
	
//	public static final String WIKIDOC_MODEL_PROBLEM_MARKER_TYPE= "org.eclipse.statet.docmlet.resourceMarkers.WikidocModelProblem"; //$NON-NLS-1$
//	
//	public static final IssueTypeSet.ProblemTypes WIKIDOC_MODEL_PROBLEM_MARKER_TYPES= new IssueTypeSet.ProblemTypes(
//			WIKIDOC_MODEL_PROBLEM_MARKER_TYPE );
	
	public static final IssueTypeSet.ProblemCategory WIKIDOC_MODEL_PROBLEM_CATEGORY= new IssueTypeSet.ProblemCategory(
			WikitextModel.WIKIDOC_TYPE_ID,
			null, null );
	
	
	private WikitextIssues() {
	}
	
}
