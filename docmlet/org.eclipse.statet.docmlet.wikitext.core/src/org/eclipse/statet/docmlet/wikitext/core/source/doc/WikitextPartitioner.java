/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.doc;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitioner;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;


@NonNullByDefault
public class WikitextPartitioner extends TreePartitioner
		implements MarkupLanguagePartitioner {
	
	
	public WikitextPartitioner(final String partitioningId,
			final WikitextPartitionNodeScanner scanner, final List<String> legalContentTypes) {
		super(partitioningId, scanner, legalContentTypes);
	}
	
	
	@Override
	public WikitextMarkupLanguage getMarkupLanguage() {
		final WikitextPartitionNodeScanner scanner= (WikitextPartitionNodeScanner) this.scanner;
		return scanner.getMarkupLanguage();
	}
	
	@Override
	public void setMarkupLanguage(final WikitextMarkupLanguage markupLanguage) {
		final WikitextPartitionNodeScanner scanner= (WikitextPartitionNodeScanner) this.scanner;
		if (!scanner.getMarkupLanguage().equals(markupLanguage)) {
			scanner.setMarkupLanguage(markupLanguage);
			clear();
		}
	}
	
	@Override
	public void reset() {
		super.clear();
	}
	
}
