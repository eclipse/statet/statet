/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.wikitext.core.WikitextCorePlugin;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public class WikitextModel {
	
	
	public static final String WIKIDOC_TYPE_ID= "Wikidoc"; //$NON-NLS-1$
	
	
	public static ModelManager getWikidocModelManager() {
		return WikitextCorePlugin.getInstance().getWikidocModelManager();
	}
	
	
	public static @Nullable WikitextSourceUnit asWikitextSourceUnit(final @Nullable SourceUnit su) {
		if (su instanceof WikitextSourceUnit) {
			return (WikitextSourceUnit)su;
		}
		return null;
	}
	
}
