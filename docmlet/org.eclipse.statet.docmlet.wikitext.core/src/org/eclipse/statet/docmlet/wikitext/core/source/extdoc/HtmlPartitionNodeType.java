/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.extdoc;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.treepartitioner.BasicPartitionNodeType;

import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextDocumentConstants;


@NonNullByDefault
public abstract class HtmlPartitionNodeType extends BasicPartitionNodeType {
	
	
	public static final class Default extends HtmlPartitionNodeType {
		
		@Override
		public String getPartitionType() {
			return WikitextDocumentConstants.WIKIDOC_HTML_DEFAULT_CONTENT_TYPE;
		}
		
	}
	
	
	public static final HtmlPartitionNodeType COMMENT= new HtmlPartitionNodeType() {
		
		@Override
		public String getPartitionType() {
			return WikitextDocumentConstants.WIKIDOC_HTML_COMMENT_CONTENT_TYPE;
		}
		
	};
	
}
