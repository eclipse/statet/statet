/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.SpanType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class Span extends ContainerNode {
	
	
	private final SpanType spanType;
	
	private final @Nullable String label;
	
	
	Span(final WikitextAstNode parent, final int startOffset, final int endOffset,
			final SpanType spanType, final @Nullable String label) {
		super(parent);
		doSetStartEndOffset(startOffset, endOffset);
		this.spanType= spanType;
		this.label= label;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.SPAN;
	}
	
	
	@Override
	public void acceptInWikitext(final WikitextAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	public SpanType getSpanType() {
		return this.spanType;
	}
	
	@Override
	public @Nullable String getLabel() {
		return this.label;
	}
	
}
