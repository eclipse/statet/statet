/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mylyn.wikitext.parser.Attributes;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.LinkAttributes;
import org.eclipse.mylyn.wikitext.parser.Locator;

import org.eclipse.statet.jcommons.collections.CollectionUtils;
import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.BasicStringFactory;
import org.eclipse.statet.jcommons.string.StringFactory;
import org.eclipse.statet.jcommons.text.core.TextRegion;
import org.eclipse.statet.jcommons.text.core.util.HtmlUtils;

import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupParser2;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextLocator;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.source.EmbeddingAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.ImageByRefAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.LabelInfo;
import org.eclipse.statet.docmlet.wikitext.core.source.LinkByRefAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.LinkRefDefinitionAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.SourceTextBlockAttributes;
import org.eclipse.statet.ltk.core.source.SourceContent;


public class WikidocParser extends DocumentBuilder {
	
	
	public static final int COLLECT_COMMENTS= 1 << 0;
	
	
	private final StringFactory labelFactory;
	
	private WikitextMarkupLanguage markupLanguage;
	
	private SourceContent content;
	
	private WikitextLocator locator2;
	
	private WikitextAstNode currentNode;
	
	private boolean headingText;
	
	private int collectText;
	private Text currentText;
	private final StringBuilder textBuilder= new StringBuilder();
	
	private int depth;
	private final List<List<WikitextAstNode>> childrenStack= new ArrayList<>();
	
	private final List<Comment> comments= new ArrayList<>();
	private int commentsLevel;
	
	private @Nullable List<Embedded> embeddedList;
	
	
	public WikidocParser(final @Nullable StringFactory labelFactory) {
		this.labelFactory= (labelFactory != null) ? labelFactory : BasicStringFactory.INSTANCE;
	}
	
	
	public void setMarkupLanguage(final WikitextMarkupLanguage markupLanguage) {
		this.markupLanguage= markupLanguage;
	}
	
	@Override
	public void setLocator(final Locator locator) {
		super.setLocator(locator);
		this.locator2= (WikitextLocator) locator;
	}
	
	public void setCommentLevel(int level) {
		if ((level & COLLECT_COMMENTS) == 0) {
			level= 0;
		}
		else {
			level= (level & (COLLECT_COMMENTS));
		}
		this.commentsLevel= level;
	}
	
	public void setCollectEmebeddedNodes(final boolean enable) {
		this.embeddedList= (enable) ? new ArrayList<>(32) : null;
	}
	
	public List<Embedded> getEmbeddedNodes() {
		return this.embeddedList;
	}
	
	public void setCollectHeadingText(final boolean enable) {
		this.headingText= enable;
	}
	
	
	protected void clearData() {
		if (this.commentsLevel != 0) {
			this.comments.clear();
		}
		CollectionUtils.clear(this.embeddedList);
	}
	
	private void initInput(final SourceContent content) {
		this.content= content;
		
		clearData();
		this.depth= -1;
	}
	
	public SourceComponent parse(final SourceContent content) {
		try {
			initInput(content);
			
			final MarkupParser2 markupParser= new MarkupParser2(this.markupLanguage, this);
			markupParser.disable(MarkupParser2.GENERATIVE_CONTENT);
			markupParser.enable(MarkupParser2.SOURCE_STRUCT);
			
			markupParser.parse(content, true);
			
			final SourceComponent node= (SourceComponent)this.currentNode;
			if (this.commentsLevel != 0) {
				node.comments= ImCollections.toList(this.comments);
			}
			return node;
		}
		finally {
			while (this.depth >= 0) {
				final List<WikitextAstNode> list= this.childrenStack.get(this.depth);
				list.clear();
				this.depth--;
			}
		}
	}
	
	
	private void addChildNode(final WikitextAstNode node) {
		final List<WikitextAstNode> children= this.childrenStack.get(this.depth);
		children.add(node);
	}
	
	private void finishNode() {
		final List<WikitextAstNode> children= this.childrenStack.get(this.depth);
		if (!children.isEmpty()) {
			((ContainerNode) this.currentNode).children= children.toArray(new WikitextAstNode[children.size()]);
			children.clear();
		}
	}
	
	private void enterNode(final WikitextAstNode node) {
		if (this.depth >= 0) {
			addChildNode(node);
		}
		
		this.depth++;
		this.currentNode= node;
		
		if (this.depth == this.childrenStack.size()) {
			this.childrenStack.add(new ArrayList<>());
		}
	}
	
	private void exitNode(final int offset) {
		finishNode();
		
//		if (offset < this.currentNode.endOffset) {
//			System.out.println("endOffset: " + offset);
//		}
//		if (offset > this.currentNode.endOffset) {
		this.currentNode.setEndOffset(offset);
//		}
//		else {
//			offset= this.currentNode.endOffset;
//		}
		this.currentNode= this.currentNode.parent;
//		if (offset > this.currentNode.endOffset) {
//			this.currentNode.endOffset= offset;
//		}
		this.depth--;
	}
	
	
	private void finishText() {
		if (this.currentText == null) {
			return;
		}
		this.currentText.text= this.textBuilder.toString();
		this.currentText= null;
	}
	
	
	@Override
	public void beginDocument() {
		enterNode(new SourceComponent(0, this.content.getLength()));
	}
	
	@Override
	public void endDocument() {
		assert (this.currentNode instanceof SourceComponent);
		finishText();
		finishNode();
		
		this.depth--;
	}
	
	@Override
	public void beginBlock(final BlockType type, final Attributes attributes) {
		finishText();
		final WikitextAstNode node;
		if (attributes instanceof final EmbeddingAttributes embeddingAttributes) {
			final Embedded embedded;
			node= embedded= new Embedded(this.currentNode,
					this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
					embeddingAttributes.getForeignType(), embeddingAttributes.getEmbedDescr() );
			if (this.embeddedList != null) {
				this.embeddedList.add(embedded);
			}
		}
		else if (attributes instanceof SourceTextBlockAttributes) {
			node= new Block.TextBlock(this.currentNode, this.locator2.getBeginOffset(),
					type, attributes.getId(), ((SourceTextBlockAttributes) attributes).getTextRegions() );
		}
		else {
			node= new Block.Common(this.currentNode, this.locator2.getBeginOffset(),
					type, attributes.getId() );
		}
		enterNode(node);
	}
	
	@Override
	public void endBlock() {
		finishText();
		exitNode(this.locator2.getEndOffset());
	}
	
	@Override
	public void beginSpan(final SpanType type, final Attributes attributes) {
		finishText();
		final WikitextAstNode node;
		if (attributes instanceof final EmbeddingAttributes embeddingAttributes) {
			final Embedded embedded;
			final TextRegion contentRegion= embeddingAttributes.getContentRegion();
			if (contentRegion != null
					&& contentRegion.getStartOffset() > this.locator2.getBeginOffset() ) {
				final Span span= new Span(this.currentNode,
						this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
						type, attributes.getId());
				node= span;
				embedded= new Embedded(span,
						contentRegion.getStartOffset(), contentRegion.getEndOffset(),
						embeddingAttributes.getForeignType(), embeddingAttributes.getEmbedDescr() );
				span.children= new WikitextAstNode[] { embedded };
			}
			else {
				embedded= new Embedded(this.currentNode,
						this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
						embeddingAttributes.getForeignType(), embeddingAttributes.getEmbedDescr() );
				node= embedded;
			}
			if (this.embeddedList != null) {
				this.embeddedList.add(embedded);
			}
		}
		else if (type == SpanType.LINK && attributes instanceof LinkAttributes) {
			node= createLink(attributes, null);
		}
		else {
			node= new Span(this.currentNode,
					this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
					type, attributes.getId());
		}
		enterNode(node);
	}
	
	@Override
	public void endSpan() {
		finishText();
		exitNode(this.locator2.getEndOffset());
	}
	
	@Override
	public void beginHeading(final int level, final Attributes attributes) {
		finishText();
		enterNode(new Heading(this.currentNode, this.locator2.getBeginOffset(), level,
				attributes.getId() ));
		if (this.headingText) {
			this.collectText++;
		}
	}
	
	@Override
	public void endHeading() {
		finishText();
		if (this.headingText) {
			this.collectText--;
		}
		exitNode(this.locator2.getEndOffset());
	}
	
	@Override
	public void characters(final String text) {
		if (this.currentText == null) {
			this.currentText= new Text(this.currentNode,
					this.locator2.getBeginOffset(), this.locator2.getEndOffset() );
			addChildNode(this.currentText);
			this.textBuilder.setLength(0);
		}
		else {
			this.currentText.setEndOffsetMin(this.locator2.getEndOffset());
		}
		if (this.collectText > 0) {
			this.textBuilder.append(text);
		}
	}
	
	@Override
	public void entityReference(final String entity) {
		if (this.collectText > 0) {
			try {
				final String resovled= HtmlUtils.resolveEntity(HtmlUtils.getEntityReference(entity));
				if (resovled != null) {
					characters(resovled);
					return;
				}
			}
			catch (final IllegalArgumentException e) {}
		}
//		characters(null);
	}
	
	@Override
	public void image(final Attributes attributes, final String url) {
		finishText();
		addChildNode(createImage(attributes, url));
	}
	
	private Image createImage(final Attributes attributes, final String src) {
		final byte linkType;
		final LabelInfo referenceInfo;
		if (attributes instanceof ImageByRefAttributes) {
			linkType= Image.SRC_BY_REF;
			referenceInfo= ((ImageByRefAttributes) attributes).getReferenceLabel();
		}
		else {
			linkType= 0;
			referenceInfo= null;
		}
		if (linkType != 0 && referenceInfo != null) {
			final Label referenceNode= createLabel(referenceInfo);
			return new Image.Ref(this.currentNode,
					this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
					linkType, referenceNode );
		}
		return new Image.Common(this.currentNode,
				this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
				Image.COMMON, src );
	}
	
	@Override
	public void link(final Attributes attributes, final String hrefOrHashName, final String text) {
		finishText();
		addChildNode(createLink(attributes, null));
	}
	
	private Link createLink(final Attributes attributes, String href) {
		if (href == null && attributes instanceof LinkAttributes) {
			href= ((LinkAttributes) attributes).getHref();
		}
		final byte linkType;
		final LabelInfo referenceInfo;
		if (attributes instanceof LinkRefDefinitionAttributes) {
			linkType= Link.LINK_REF_DEFINITION;
			referenceInfo= ((LinkRefDefinitionAttributes) attributes).getReferenceLabel();
			if (referenceInfo != null) {
				final Label referenceNode= createLabel(referenceInfo);
				return new Link.Ref(this.currentNode,
						this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
						linkType, referenceNode, href, attributes.getTitle() );
			}
		}
		else if (attributes instanceof LinkByRefAttributes) {
			linkType= Link.LINK_BY_REF;
			referenceInfo= ((LinkByRefAttributes) attributes).getReferenceLabel();
			final Label referenceNode= createLabel(referenceInfo);
			return new Link.Ref(this.currentNode,
					this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
					linkType, referenceNode );
		}
		else {
			linkType= 0;
		}
		return new Link.Common(this.currentNode,
				this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
				Link.COMMON, href, (attributes != null) ? attributes.getTitle() : null );
	}
	
	private Label createLabel(final LabelInfo labelInfo) {
		return new Label(null, labelInfo.getStartOffset(), labelInfo.getEndOffset(), labelInfo.getLabel());
	}
	
	@Override
	public void imageLink(final Attributes linkAttributes, final Attributes imageAttributes, final String href,
			final String imageUrl) {
		finishText();
	}
	
	@Override
	public void acronym(final String text, final String definition) {
		finishText();
	}
	
	@Override
	public void lineBreak() {
		finishText();
		addChildNode(new Control(this.currentNode,
				this.locator2.getBeginOffset(), this.locator2.getEndOffset(),
				Control.LINE_BREAK ));
	}
	
	@Override
	public void charactersUnescaped(final String literal) {
		finishText();
	}
	
}
