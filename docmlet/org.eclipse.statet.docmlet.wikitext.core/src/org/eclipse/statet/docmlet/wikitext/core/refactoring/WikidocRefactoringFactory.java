/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.refactoring;

import org.eclipse.ltk.core.refactoring.participants.CopyProcessor;
import org.eclipse.ltk.core.refactoring.participants.DeleteProcessor;
import org.eclipse.ltk.core.refactoring.participants.MoveProcessor;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination;


public class WikidocRefactoringFactory extends CommonRefactoringFactory {
	
	
	protected WikidocRefactoringFactory() {
	}
	
	
	@Override
	public WikidocRefactoringAdapter createAdapter(final Object elements) {
		return new WikidocRefactoringAdapter();
	}
	
	@Override
	public DeleteProcessor createDeleteProcessor(final Object elementsToDelete, final RefactoringAdapter adapter) {
		return new DeleteWikidocProcessor(createElementSet(elementsToDelete), adapter);
	}
	
	@Override
	public MoveProcessor createMoveProcessor(final Object elementsToMove,
			final RefactoringDestination destination, final RefactoringAdapter adapter) {
		return new MoveWikidocProcessor(createElementSet(elementsToMove), destination, adapter);
	}
	
	@Override
	public CopyProcessor createCopyProcessor(final Object elementsToCopy,
			final RefactoringDestination destination, final RefactoringAdapter adapter) {
		return new CopyWikidocProcessor(createElementSet(elementsToCopy), destination, adapter);
	}
	
	@Override
	public RefactoringProcessor createPasteProcessor(final Object elementsToPaste,
			final RefactoringDestination destination, final RefactoringAdapter adapter) {
		if (elementsToPaste instanceof String) {
			return new PasteWikidocCodeProcessor((String) elementsToPaste, destination, (WikidocRefactoringAdapter) adapter);
		}
		return super.createPasteProcessor(elementsToPaste, destination, adapter);
	}
	
}
