/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.model;

import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.core.util.NameUtils;


@NonNullByDefault
public abstract class WikitextElementName implements ElementName {
	
	
	public static final byte RESOURCE=                      0x0f;
	
	public static final byte TITLE=                         0x11;
	
//	public static final byte LABEL=                         0x40;
	public static final byte LINK_ANCHOR_LABEL=             0x41;
	public static final byte LINK_DEF_LABEL=                0x45;
	
	
	private static class Default extends WikitextElementName {
		
		
		protected final int type;
		protected final String segment;
		
		
		private Default(final int type, final String name) {
			this.type= type;
			this.segment= name;
		}
		
		
		@Override
		public int getType() {
			return this.type;
		}
		
		@Override
		public @Nullable WikitextElementName getNextSegment() {
			return null;
		}
		
		@Override
		public String getSegmentName() {
			return this.segment;
		}
		
		@Override
		public String getDisplayName() {
			return this.segment;
		}
		
	}
	
	
	public static WikitextElementName create(final int type, final String name) {
		return new Default(type, name);
	}
	
	
	@Override
	public abstract @Nullable WikitextElementName getNextSegment();
	
	@Override
	public WikitextElementName getLastSegment() {
		@NonNull WikitextElementName lastSegment;
		WikitextElementName nextSegment= this;
		do {
			lastSegment= nextSegment;
		} while ((nextSegment= nextSegment.getNextSegment()) != null);
		return lastSegment;
	}
	
	
	@Override
	public int hashCode() {
		final String name= getSegmentName();
		final ElementName next= getNextSegment();
		if (next != null) {
			return getType() * ((name != null) ? name.hashCode() : 1) * (next.hashCode()+7);
		}
		else {
			return getType() * ((name != null) ? name.hashCode() : 1);
		}
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final WikitextElementName other
						&& getType() == other.getType()
						&& NameUtils.areEqual(getSegmentName(), other.getSegmentName())
						&& Objects.equals(getNextSegment(), other.getNextSegment()) ));
	}
	
	
	@Override
	public String toString() {
		return getDisplayName();
	}
	
}
