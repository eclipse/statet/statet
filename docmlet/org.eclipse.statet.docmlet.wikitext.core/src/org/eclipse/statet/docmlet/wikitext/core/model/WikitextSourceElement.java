/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.model;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;


@NonNullByDefault
public interface WikitextSourceElement
		extends WikitextElement<WikitextSourceElement>,
				SourceStructElement<WikitextSourceElement, SourceStructElement<?, ?>> {
	
	
	@Override
	@Nullable WikitextSourceElement getModelParent();
	@Override
	boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super WikitextSourceElement> filter);
	@Override
	List<? extends WikitextSourceElement> getModelChildren(final @Nullable LtkModelElementFilter<? super WikitextSourceElement> filter);
	
	
}
