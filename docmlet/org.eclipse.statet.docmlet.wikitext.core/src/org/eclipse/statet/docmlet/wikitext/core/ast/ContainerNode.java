/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
abstract class ContainerNode extends WikitextAstNode {
	
	
	WikitextAstNode[] children= NO_CHILDREN;
	
	
	ContainerNode(final WikitextAstNode parent) {
		super(parent);
	}
	
	ContainerNode() {
		super();
	}
	
	
	@Override
	public boolean hasChildren() {
		return (this.children.length > 0);
	}
	
	@Override
	public int getChildCount() {
		return this.children.length;
	}
	
	@Override
	public WikitextAstNode getChild(final int index) {
		return this.children[index];
	}
	
	@Override
	public int getChildIndex(final AstNode child) {
		for (int i= 0; i < this.children.length; i++) {
			if (this.children[i] == child) {
				return i;
			}
		}
		return -1;
	}
	
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		for (final WikitextAstNode child : this.children) {
			visitor.visit(child);
		}
	}
	
	@Override
	public void acceptInWikitextChildren(final WikitextAstVisitor visitor) throws InvocationTargetException {
		for (final WikitextAstNode child : this.children) {
			child.acceptInWikitext(visitor);
		}
	}
	
}
