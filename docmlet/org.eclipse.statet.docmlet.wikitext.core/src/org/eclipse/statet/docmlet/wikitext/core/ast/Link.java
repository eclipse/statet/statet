/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


@NonNullByDefault
public abstract class Link extends ContainerNode {
	
	
	public static final byte COMMON= 0;
	public static final byte LINK_REF_DEFINITION= 1;
	public static final byte LINK_BY_REF= 2;
	
	
	static final class Ref extends Link {
		
		
		private final Label referenceLabel;
		
		
		Ref(final WikitextAstNode parent, final int startOffset, final int endOffset,
				final byte linkType, final Label referenceLabel,
				final String url, final @Nullable String title) {
			super(parent, linkType, url, title);
			doSetStartEndOffset(startOffset, endOffset);
			
			referenceLabel.parent= this;
			this.referenceLabel= nonNullAssert(referenceLabel);
		}
		
		Ref(final WikitextAstNode parent, final int startOffset, final int endOffset,
				final byte linkType, final Label referenceLabel) {
			super(parent, linkType, null, null);
			doSetStartEndOffset(startOffset, endOffset);
			
			referenceLabel.parent= this;
			this.referenceLabel= nonNullAssert(referenceLabel);
		}
		
		
		@Override
		public boolean hasChildren() {
			return true;
		}
		
		@Override
		public int getChildCount() {
			return this.children.length + 1;
		}
		
		@Override
		public WikitextAstNode getChild(final int index) {
			if (index == 0) {
				return this.referenceLabel;
			}
			return this.children[index - 1];
		}
		
		@Override
		public int getChildIndex(final AstNode child) {
			if (this.referenceLabel == child) {
				return 0;
			}
			for (int i= 0; i < this.children.length; i++) {
				if (this.children[i] == child) {
					return i + 1;
				}
			}
			return -1;
		}
		
		@Override
		public Label getReferenceLabel() {
			return this.referenceLabel;
		}
		
		
		@Override
		public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
			this.referenceLabel.accept(visitor);
			for (final WikitextAstNode child : this.children) {
				child.accept(visitor);
			}
		}
		
		@Override
		public void acceptInWikitextChildren(final WikitextAstVisitor visitor) throws InvocationTargetException {
			this.referenceLabel.acceptInWikitext(visitor);
			for (final WikitextAstNode child : this.children) {
				child.acceptInWikitext(visitor);
			}
		}
		
	}
	
	static final class Common extends Link {
		
		
		Common(final WikitextAstNode parent, final int startOffset, final int endOffset,
				final byte linkType, final String href, final @Nullable String title) {
			super(parent, linkType, href, title);
			doSetStartEndOffset(startOffset, endOffset);
		}
		
		
		@Override
		public @Nullable Label getReferenceLabel() {
			return null;
		}
		
	}
	
	
	private final byte linkType;
	
	private final @Nullable String uri;
	
	private final @Nullable String title;
	
	
	private Link(final WikitextAstNode parent,
			final byte linkType, final @Nullable String uri, final @Nullable String title) {
		super(parent);
		
		this.linkType= linkType;
		this.uri= uri;
		this.title= title;
	}
	
	private Link(final WikitextAstNode parent,
			final String uri, final @Nullable String title) {
		super(parent);
		
		this.linkType= 0;
		this.uri= uri;
		this.title= title;
	}
	
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.LINK;
	}
	
	
	@Override
	public void acceptInWikitext(final WikitextAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	public byte getLinkType() {
		return this.linkType;
	}
	
	public @Nullable String getUri() {
		return this.uri;
	}
	
	public @Nullable String getTitle() {
		return this.title;
	}
	
	public abstract Label getReferenceLabel();
	
}
