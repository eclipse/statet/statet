/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.doc;

import java.util.EnumMap;

import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.collections.IntArrayMap;
import org.eclipse.statet.ecommons.collections.IntMap;
import org.eclipse.statet.ecommons.text.core.treepartitioner.BasicPartitionNodeType;


@NonNullByDefault
public class WikitextPartitionNodeType extends BasicPartitionNodeType {
	
	
	public static final WikitextPartitionNodeType DEFAULT_ROOT= new WikitextPartitionNodeType();
	
	
	public static class Block extends WikitextPartitionNodeType {
		
		
		private final BlockType blockType;
		
		
		public Block(final BlockType blockType) {
			this.blockType= blockType;
		}
		
		
		@Override
		public BlockType getBlockType() {
			return this.blockType;
		}
		
		
		@Override
		public String toString() {
			return getPartitionType() + ":" + this.blockType;
		}
		
	}
	
//	public static class BlockWithStyle extends Block {
//		
//		
//		private final String cssStyle;
//		
//		
//		public BlockWithStyle(BlockType blockType, String cssStyle) {
//			super(blockType);
//			this.cssStyle= cssStyle;
//		}
//		
//		
//		public String getCssStyle() {
//			return this.cssStyle;
//		}
//		
//		
//		@Override
//		public int hashCode() {
//			return getBlockType().hashCode() * getCssStyle().hashCode();
//		}
//		
//		@Override
//		public boolean equals(Object obj) {
//			if (this == obj) {
//				return true;
//			}
//			if (!(obj instanceof BlockWithStyle)) {
//				return false;
//			}
//			final BlockWithStyle other= (BlockWithStyle) obj;
//			return getBlockType() == other.getBlockType()
//					&& getCssStyle().equals(other.getCssStyle() );
//		}
//		
//	}
	
	public static final EnumMap<BlockType, Block> BLOCK_TYPES= new EnumMap<>(BlockType.class);
	static {
		for (final BlockType blockType : BlockType.values()) {
			BLOCK_TYPES.put(blockType, new Block(blockType));
		}
	}
	
	
	public static class Heading extends WikitextPartitionNodeType {
		
		
		private final int level;
		
		
		public Heading(final int level) {
			this.level= level;
		}
		
		
		public int getHeadingLevel() {
			return this.level;
		}
		
		
		@Override
		public String toString() {
			return getPartitionType() + ":HEADING-" + this.level;
		}
		
	}
	
	
	public static final IntMap<Heading> HEADING_TYPES= new IntArrayMap<>();
	static {
		for (int level= 1; level <= 6; level++) {
			HEADING_TYPES.put(level, new Heading(level));
		}
	}
	
	
	public WikitextPartitionNodeType() {
	}
	
	
	@Override
	public String getPartitionType() {
		return WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE;
	}
	
	
	public @Nullable BlockType getBlockType() {
		return null;
	}
	
}
