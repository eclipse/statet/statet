/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.statet.ltk.core.source.SourceContent;


public class RegexBlockWeaveParticipant extends BlockWeaveParticipant {
	
	
	private final String foreignTypeId;
	private final byte embedDescr;
	
	private final Matcher startMatcher;
	private final Matcher endMatcher;
	
	
	public RegexBlockWeaveParticipant(final String foreignTypeId, final byte embedDescr,
			final Pattern startPattern, final Pattern endPattern) {
		this.foreignTypeId= foreignTypeId;
		this.embedDescr= embedDescr;
		
		this.startMatcher= startPattern.matcher(""); //$NON-NLS-1$
		this.endMatcher= endPattern.matcher(""); //$NON-NLS-1$
	}
	
	
	@Override
	public String getForeignTypeId() {
		return this.foreignTypeId;
	}
	
	@Override
	public int getEmbedDescr() {
		return this.embedDescr;
	}
	
	@Override
	public void reset(final SourceContent sourceContent) {
		final String text= (sourceContent != null) ? sourceContent.getString() : ""; //$NON-NLS-1$
		this.startMatcher.reset(text);
		this.endMatcher.reset(text);
	}
	
	@Override
	public boolean checkStartLine(final int startOffset, final int endOffset) {
		this.startMatcher.region(startOffset, endOffset);
		return this.startMatcher.matches();
	}
	
	@Override
	public int getStartOffset() {
		return this.startMatcher.start();
	}
	
	@Override
	public boolean checkEndLine(final int startOffset, final int endOffset) {
		this.endMatcher.region(startOffset, endOffset);
		return this.endMatcher.matches();
	}
	
}
