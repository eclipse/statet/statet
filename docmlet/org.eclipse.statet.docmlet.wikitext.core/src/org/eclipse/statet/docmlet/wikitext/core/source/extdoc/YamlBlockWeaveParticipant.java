/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.extdoc;

import org.eclipse.statet.docmlet.wikitext.core.source.BlockWeaveParticipant;
import org.eclipse.statet.ltk.core.source.SourceContent;


public class YamlBlockWeaveParticipant extends BlockWeaveParticipant {
	
	
	private SourceContent sourceContent;
	
	private int start;
	private int startMarker;
	
	
	public YamlBlockWeaveParticipant() {
	}
	
	
	@Override
	public String getForeignTypeId() {
		return ExtdocMarkupLanguage.EMBEDDED_YAML;
	}
	
	@Override
	public int getEmbedDescr() {
		return ExtdocMarkupLanguage.EMBEDDED_YAML_METADATA_CHUNK_DESCR;
	}
	
	@Override
	public void reset(final SourceContent content) {
		this.sourceContent= content;
		this.start= -1;
	}
	
	@Override
	public boolean checkStartLine(final int startOffset, final int endOffset) {
		final String text= this.sourceContent.getString();
		int offset= startOffset;
		
		switch (endOffset - startOffset) {
		case 1:
			if (text.charAt(offset) == '\n') {
				offset++;
				break;
			}
			return false;
		case 2:
			if (text.charAt(offset) == '\r' && text.charAt(offset + 1) == '\n') {
				offset+= 2;
				break;
			}
			return false;
		default:
			if (startOffset == 0 && this.sourceContent.getStartOffset() == 0) {
				break;
			}
			return false;
		}
		
		if (text.regionMatches(offset, "---", 0, 3)) { //$NON-NLS-1$
			this.start= startOffset;
			this.startMarker= offset;
			return true;
		}
		
		return false;
	}
	
	@Override
	public int getStartOffset() {
		return this.start;
	}
	
	@Override
	public boolean checkEndLine(final int startOffset, final int endOffset) {
		if (startOffset <= this.startMarker) {
			return false;
		}
		final String text= this.sourceContent.getString();
		return text.regionMatches(startOffset, "---", 0, 3) //$NON-NLS-1$
				|| text.regionMatches(startOffset, "...", 0, 3); //$NON-NLS-1$
	}
	
	@Override
	protected void appendReplacement(final StringBuilder sb,
			final String source, final int startOffset, final int endOffset) {
		sb.append("\n\n"); //$NON-NLS-1$
	}
	
	@Override
	protected int getTextLength() {
		return 0;
	}
	
}
