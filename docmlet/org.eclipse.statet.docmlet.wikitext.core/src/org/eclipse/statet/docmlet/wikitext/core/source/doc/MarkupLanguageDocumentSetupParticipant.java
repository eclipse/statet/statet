/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.doc;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.IDocumentPartitioner;

import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;


public abstract class MarkupLanguageDocumentSetupParticipant extends PartitionerDocumentSetupParticipant {
	
	
	public static WikitextMarkupLanguage getMarkupLanguage(final IDocument document, final String partitioning) {
		final MarkupLanguagePartitioner partitioner= (MarkupLanguagePartitioner)
				((IDocumentExtension3) document).getDocumentPartitioner(partitioning);
		return partitioner.getMarkupLanguage();
	}
	
	
	private final WikitextMarkupLanguage markupLanguage;
	
	private final int markupLanguageMode;
	
	
	public MarkupLanguageDocumentSetupParticipant(final WikitextMarkupLanguage markupLanguage,
			final int markupLanguageMode) {
		if (markupLanguage == null) {
			throw new NullPointerException("markupLanguage"); //$NON-NLS-1$
		}
		this.markupLanguage= markupLanguage;
		this.markupLanguageMode= markupLanguageMode;
	}
	
	
	public WikitextMarkupLanguage getMarkupLanguage() {
		return this.markupLanguage;
	}
	
	protected int getMarkupLanguageMode() {
		return this.markupLanguageMode;
	}
	
	
	@Override
	protected void doSetup(final IDocument document) {
		doSetup(document, getMarkupLanguage());
	}
	
	protected void doSetup(final IDocument document, final WikitextMarkupLanguage markupLanguage) {
		final IDocumentExtension3 extension3= (IDocumentExtension3) document;
		MarkupLanguagePartitioner partitioner= (MarkupLanguagePartitioner) extension3.getDocumentPartitioner(getPartitioningId());
		if (partitioner == null) {
			// Setup the document scanner
			partitioner= createDocumentPartitioner(markupLanguage);
			partitioner.connect(document, true);
			extension3.setDocumentPartitioner(getPartitioningId(), partitioner);
		}
		else {
			partitioner.setMarkupLanguage(markupLanguage);
			extension3.setDocumentPartitioner(getPartitioningId(), partitioner);
		}
	}
	
	
	@Override
	protected IDocumentPartitioner createDocumentPartitioner() {
		throw new UnsupportedOperationException();
	}
	
	protected abstract MarkupLanguagePartitioner createDocumentPartitioner(WikitextMarkupLanguage markupLanguage);
	
}
