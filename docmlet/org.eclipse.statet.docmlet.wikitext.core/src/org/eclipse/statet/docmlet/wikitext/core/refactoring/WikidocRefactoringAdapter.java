/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.refactoring;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.source.WikitextHeuristicTokenScanner;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination.Position;
import org.eclipse.statet.ltk.text.core.HeuristicTokenScanner;


public class WikidocRefactoringAdapter extends RefactoringAdapter {
	
	
	public WikidocRefactoringAdapter() {
		super(WikitextModel.WIKIDOC_TYPE_ID);
	}
	
	
	@Override
	public String getPluginIdentifier() {
		return WikitextCore.BUNDLE_ID;
	}
	
	@Override
	public WikitextHeuristicTokenScanner getScanner(final SourceUnit su) {
		return WikitextHeuristicTokenScanner.create(su.getDocumentContentInfo());
	}
	
	@Override
	public boolean canInsert(final ElementSet elements, final SourceElement to,
			final RefactoringDestination.Position pos) {
		if (super.canInsert(elements, to, pos)) {
			if ((to.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_EMBEDDED
					&& pos == RefactoringDestination.Position.INTO ) {
				return false;
			}
			return true;
		}
		return false;
	}
	
	@Override
	public boolean isCommentContent(final ITypedRegion partition) {
		return false;
	}
	
	@Override
	protected int getInsertionOffset(final AbstractDocument document,
			final SourceElement element, final Position pos,
			final HeuristicTokenScanner scanner)
			throws BadLocationException, BadPartitioningException {
		return super.getInsertionOffset(document, element, pos, scanner);
	}
	
}
