/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source;

import org.eclipse.mylyn.wikitext.parser.Attributes;

import org.eclipse.statet.jcommons.text.core.TextRegion;


/**
 * Indicating a block/span element embedding foreign code
 */
public class EmbeddingAttributes extends Attributes {
	
	
	private final String foreignType;
	private final int embedDescr;
	
	private TextRegion contentRegion;
	
	
	public EmbeddingAttributes(final String foreignType, final int embedDescr,
			final TextRegion contentRegion) {
		this.foreignType= foreignType;
		this.embedDescr= embedDescr;
		
		this.contentRegion= contentRegion;
	}
	
	public EmbeddingAttributes(final String foreignType, final int embedDescr) {
		this.foreignType= foreignType;
		this.embedDescr= embedDescr;
	}
	
	
	public String getForeignType() {
		return this.foreignType;
	}
	
	public int getEmbedDescr() {
		return this.embedDescr;
	}
	
	public void setContentRegion(final TextRegion region) {
		this.contentRegion= region;
	}
	
	public TextRegion getContentRegion() {
		return this.contentRegion;
	}
	
}
