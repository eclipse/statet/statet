/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source.doc;

import org.eclipse.core.filebuffers.IDocumentSetupParticipantExtension;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager1;


/**
 * Supports:
 *   <li>Automatic setup of MarkupConfig for documents of workspace resources.</li>
 */
public abstract class MarkupLanguageDocumentSetupParticipant1
		extends MarkupLanguageDocumentSetupParticipant
		implements IDocumentSetupParticipantExtension {
	
	
	private final WikitextMarkupLanguageManager1 markupLanguageManager;
	
	
	public MarkupLanguageDocumentSetupParticipant1(final WikitextMarkupLanguage markupLanguage,
			final int markupLanguageMode) {
		super(markupLanguage, markupLanguageMode);
		
		this.markupLanguageManager= WikitextCore.getMarkupLanguageManager();
	}
	
	
	@Override
	public void setup(final IDocument document, final IPath location, final LocationKind locationKind) {
		WikitextMarkupLanguage markupLanguage= getMarkupLanguage();
		if (locationKind == LocationKind.IFILE) {
			markupLanguage= this.markupLanguageManager.getLanguage(
					ResourcesPlugin.getWorkspace().getRoot().getFile(location),
					markupLanguage.getName(), true );
		}
		doSetup(document, markupLanguage);
	}
	
}
