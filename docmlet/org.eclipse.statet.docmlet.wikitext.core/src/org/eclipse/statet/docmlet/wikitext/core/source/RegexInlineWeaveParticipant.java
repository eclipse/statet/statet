/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source;

import java.util.regex.Pattern;


public class RegexInlineWeaveParticipant extends WeaveParticipant {
	
	
	private final String foreignTypeId;
	private final int embedDescr;
	
	private final Pattern pattern;
	
	
	public RegexInlineWeaveParticipant(final String foreignTypeId, final int embedDescr,
			final Pattern pattern) {
		this.foreignTypeId= foreignTypeId;
		this.embedDescr= embedDescr;
		
		this.pattern= pattern;
	}
	
	
	@Override
	public String getForeignTypeId() {
		return this.foreignTypeId;
	}
	
	@Override
	public int getEmbedDescr() {
		return this.embedDescr;
	}
	
	protected Pattern getPattern() {
		return this.pattern;
	}
	
}
