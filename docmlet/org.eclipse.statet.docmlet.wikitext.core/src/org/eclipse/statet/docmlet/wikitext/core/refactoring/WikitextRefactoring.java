/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.refactoring;

import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;


public class WikitextRefactoring {
	
	
	private static final CommonRefactoringFactory WIKIDOC_FACTORY= new WikidocRefactoringFactory();
	
	public static CommonRefactoringFactory getWikidocFactory() {
		return WIKIDOC_FACTORY;
	}
	
	
	public static final String DELETE_WIKIDOC_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.docmlet.wikitext.refactoring.DeleteWikidocElementsOperation"; //$NON-NLS-1$
	
	public static final String MOVE_WIKIDOC_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.docmlet.wikitext.refactoring.MoveWikidocElementsOperation"; //$NON-NLS-1$
	
	public static final String COPY_WIKIDOC_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.docmlet.wikitext.refactoring.CopyWikidocElementsOperation"; //$NON-NLS-1$
	
	public static final String PASTE_WIKIDOC_CODE_REFACTORING_ID= "org.eclipse.statet.docmlet.wikitext.refactoring.PasteWikidocCodeOperation"; //$NON-NLS-1$
	
	
	public static final String DELETE_WIKIDOC_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.docmlet.wikitext.refactoring.DeleteWikidocElementsProcessor"; //$NON-NLS-1$
	
	public static final String MOVE_WIKIDOC_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.docmlet.wikitext.refactoring.MoveWikidocElementsProcessor"; //$NON-NLS-1$
	
	public static final String COPY_WIKIDOC_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.docmlet.wikitext.refactoring.CopyWikidocElementsProcessor"; //$NON-NLS-1$
	
	public static final String PASTE_WIKIDOC_CODE_PROCESSOR_ID= "org.eclipse.statet.docmlet.wikitext.refactoring.PasteWikidocCodeProcessor"; //$NON-NLS-1$
	
}
