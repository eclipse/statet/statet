/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public final class SourceComponent extends ContainerNode {
	
	
	@Nullable ImList<Comment> comments;
	
	
	SourceComponent(final int startOffset, final int endOffset) {
		super();
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.SOURCELINES;
	}
	
	
	/**
	 * The comment nodes in this source component
	 * 
	 * @return the comments or <code>null</code>, if disabled
	 */
	public @Nullable ImList<Comment> getComments() {
		return this.comments;
	}
	
	
	@Override
	public void acceptInWikitext(final WikitextAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
}
