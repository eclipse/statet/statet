/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.core.source;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.ROUND_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.CharPairSet;

import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextDocumentConstants;
import org.eclipse.statet.ltk.text.core.HeuristicTokenScanner;


@NonNullByDefault
public class WikitextHeuristicTokenScanner extends HeuristicTokenScanner {
	
	
	public static final CharPairSet MARKUP_BRACKETS= new CharPairSet(
			ImCollections.newIdentityList(CURLY_BRACKETS, SQUARE_BRACKETS, ROUND_BRACKETS),
			'\\' );
	
	
	public static boolean isEscaped(final IDocument document, int offset)
			throws BadLocationException {
		boolean escaped= false;
		while (offset > 0 && document.getChar(--offset) == '\\') {
			escaped= !escaped;
		}
		return escaped;
	}
	
	
	public static WikitextHeuristicTokenScanner create(final DocContentSections documentContentInfo) {
		return new WikitextHeuristicTokenScanner(documentContentInfo);
	}
	
	
	protected WikitextHeuristicTokenScanner(final DocContentSections documentContentInfo) {
		super(documentContentInfo, WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_CONSTRAINT);
	}
	
	
	@Override
	public CharPairSet getDefaultBrackets() {
		return MARKUP_BRACKETS;
	}
	
}
