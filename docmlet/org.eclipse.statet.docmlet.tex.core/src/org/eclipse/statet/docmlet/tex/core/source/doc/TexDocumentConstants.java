/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.source.doc;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;


@NonNullByDefault
public interface TexDocumentConstants {
	
	
	/**
	 * The id of partitioning of LaTeX documents.
	 */
	String LTX_PARTITIONING= "org.eclipse.statet.Ltx"; //$NON-NLS-1$
	
	String LTX_DEFAULT_CONTENT_TYPE= "Ltx.Default"; //$NON-NLS-1$
	String LTX_COMMENT_CONTENT_TYPE= "Ltx.Comment"; //$NON-NLS-1$
	String LTX_MATH_CONTENT_TYPE= "Ltx.Math"; //$NON-NLS-1$
	String LTX_MATHCOMMENT_CONTENT_TYPE= "Ltx.MathComment"; //$NON-NLS-1$
	String LTX_VERBATIM_CONTENT_TYPE= "Ltx.Verbatim"; //$NON-NLS-1$
	
	
	/**
	 * List with all partition content types of LaTeX documents.
	 */
	ImList<String> LTX_CONTENT_TYPES= ImCollections.newList(
			LTX_DEFAULT_CONTENT_TYPE,
			LTX_COMMENT_CONTENT_TYPE,
			LTX_MATH_CONTENT_TYPE,
			LTX_MATHCOMMENT_CONTENT_TYPE,
			LTX_VERBATIM_CONTENT_TYPE );
	
	/**
	 * List with all partition content types of LaTeX math regions (e.g. in other documents).
	 */
	ImList<String> LTX_MATH_CONTENT_TYPES= ImCollections.newList(
			LTX_MATH_CONTENT_TYPE,
			LTX_MATHCOMMENT_CONTENT_TYPE );
	
	
	PartitionConstraint LTX_DEFAULT_CONTENT_CONSTRAINT= new PartitionConstraint() {
				@Override
				public boolean matches(final String contentType) {
					return (contentType == LTX_DEFAULT_CONTENT_TYPE);
				}
	};
	
	PartitionConstraint LTX_DEFAULT_OR_MATH_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
			return (contentType == LTX_DEFAULT_CONTENT_TYPE
					|| contentType == LTX_MATH_CONTENT_TYPE );
		}
	};
	
	PartitionConstraint LTX_ANY_CONTENT_CONSTRAINT= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
//			return (partitionType.startsWith("Ltx.")); //$NON-NLS-1$
			return (contentType == LTX_DEFAULT_CONTENT_TYPE
					|| contentType == LTX_MATH_CONTENT_TYPE
					|| contentType == LTX_COMMENT_CONTENT_TYPE
					|| contentType == LTX_MATHCOMMENT_CONTENT_TYPE
					|| contentType == LTX_VERBATIM_CONTENT_TYPE );
		}
	};
	
}
