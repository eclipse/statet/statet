/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_DOCUMENT_INCLUDE;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_DOCUMENT_LAYOUT;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_DOCUMENT_CONTENTLISTS_DEF;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_DOCUMENT_CONTENTLISTS_GEN;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_DOCUMENT_ELEMENT_IMAGES;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_DOCUMENT_ELEMENT_LISTS;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_DOCUMENT_ELEMENT_TABLES;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_DOCUMENT_INDEX_DEF;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_DOCUMENT_INDEX_GEN;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;


@NonNullByDefault
public interface DivDocDefinitions {
	
	
	TexCommand DOCUMENT_input_COMMAND= new TexCommand(C2_DOCUMENT_INCLUDE,
			"input", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("file", Parameter.REQUIRED, Parameter.RESOURCE_SINGLE)
			), "Insert the content of the given file into the document");
	
	TexCommand DOCUMENT_insert_COMMAND= new TexCommand(C2_DOCUMENT_INCLUDE,
			"insert", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("file", Parameter.REQUIRED, Parameter.RESOURCE_SINGLE)
			), "Includes the content of the given file with page feed into the document");
	
	TexCommand DOCUMENT_includegraphics_COMMAND= new TexCommand(C3_DOCUMENT_ELEMENT_IMAGES,
			"includegraphics", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("options", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("file", Parameter.REQUIRED, Parameter.RESOURCE_SINGLE)
			), "Includes the graphic of the given file into the document");
	
	TexCommand DOCUMENT_item_COMMAND= new TexCommand(C3_DOCUMENT_ELEMENT_LISTS,
			"item", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("symbol/term", Parameter.OPTIONAL, Parameter.NONE)
			), "Adds a new item to the list");
	
	TexCommand DOCUMENT_hline_COMMAND= new TexCommand(C3_DOCUMENT_ELEMENT_TABLES,
			"hline", "Draws a horizontal Line below the current row");
	
	
	TexCommand DOCUMENT_maketitle_COMMAND= new TexCommand(TexCommand.DOCUMENT,
			"maketitle", "Inserts a Title page in the document"); //$NON-NLS-1$
	
	TexCommand DOCUMENT_addcontentsline_COMMAND= new TexCommand(C3_DOCUMENT_CONTENTLISTS_DEF,
			"addcontentsline", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("content list", Parameter.REQUIRED, Parameter.NONE),
					new Parameter("type of entry", Parameter.REQUIRED, Parameter.NONE),
					new Parameter("entry", Parameter.REQUIRED, Parameter.NONE)
			), "Adds an extra entry to a content list (toc, lof, lot, ...)");
	
	TexCommand DOCUMENT_caption_COMMAND= new TexCommand(C3_DOCUMENT_CONTENTLISTS_DEF,
			"caption", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("short caption", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("caption", Parameter.REQUIRED, Parameter.NONE)
			), "Adds a caption for the surrounding element");
	
	TexCommand DOCUMENT_tableofcontents_COMMAND= new TexCommand(C3_DOCUMENT_CONTENTLISTS_GEN,
			"tableofcontents", "Inserts a Table of Content (toc) for the document"); //$NON-NLS-1$
	
	TexCommand DOCUMENT_listoffigures_COMMAND= new TexCommand(C3_DOCUMENT_CONTENTLISTS_GEN,
			"listoffigures", "Inserts a List of Figures (lof) in the document"); //$NON-NLS-1$
	
	TexCommand DOCUMENT_listoftables_COMMAND= new TexCommand(C3_DOCUMENT_CONTENTLISTS_GEN,
			"listoftables", "Inserts a List of Tables (lot) in the document"); //$NON-NLS-1$
	
	
	TexCommand DOCUMENT_index_COMMAND= new TexCommand(C3_DOCUMENT_INDEX_DEF,
			"index", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("keyword", Parameter.REQUIRED, Parameter.NONE)
			), "Adds an entry to the index referring to the current position");
	
	TexCommand DOCUMENT_printindex_COMMAND= new TexCommand(C3_DOCUMENT_INDEX_GEN,
			"printindex", "Inserts the Index for the document"); //$NON-NLS-1$
	
	
	TexCommand DOCUMENT_vspace_COMMAND= new TexCommand(C2_DOCUMENT_LAYOUT,
			"vspace", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter("length", Parameter.REQUIRED, Parameter.NONE)
			), "Adds vertical space at the current positition");
	
	TexCommand DOCUMENT_hspace_COMMAND= new TexCommand(C2_DOCUMENT_LAYOUT,
			"hspace", true, ImCollections.newList( //$NON-NLS-1$
					new Parameter("length", Parameter.REQUIRED, Parameter.NONE)
			), "Adds horizontal space at the current position");
	
	
}
