/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.refactoring;

import java.util.Arrays;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEdit;

import org.eclipse.statet.ecommons.text.IndentUtil;
import org.eclipse.statet.ecommons.text.IndentUtil.IndentEditAction;

import org.eclipse.statet.docmlet.tex.core.TexCodeStyleSettings;
import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.ast.NodeType;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;


public class LtxSourceIndenter {
	
	private TexCodeStyleSettings codeStyle;
	
	private AbstractDocument document;
	private TexAstNode rootNode;
	
	private int refLine;
	private int firstLine;
	private int lastLine;
	
	private IndentUtil util;
	
	private int[] lineColumns;
	
	
	public void setup(final TexCoreAccess coreAccess) {
		this.codeStyle= coreAccess.getTexCodeStyle();
	}
	
	private void init(final AbstractDocument document, final TexAstNode node,
			final int firstLine, final int lastLine) throws BadLocationException {
		if (document == null) {
			throw new NullPointerException("document");
		}
		if (node == null) {
			throw new NullPointerException("node");
		}
		if (firstLine < 0 || lastLine < 0 || firstLine > lastLine) {
			throw new IllegalArgumentException("line index");
		}
		this.document= document;
		this.rootNode= node;
		this.firstLine= firstLine;
		this.lastLine= lastLine;
		
		final int count= this.document.getNumberOfLines(0, this.document.getLineOffset(this.lastLine));
		this.lineColumns= new int[count + 2];
		Arrays.fill(this.lineColumns, -1);
		
		this.util= new IndentUtil(document, this.codeStyle);
	}
	
	public void clear() {
		this.document= null;
		this.rootNode= null;
		this.codeStyle= null;
		this.util= null;
		this.lineColumns= null;
	}
	
	
	public int getNewIndentColumn(final int line) throws BadLocationException {
		return this.lineColumns[line];
	}
	
	public int getNewIndentOffset(final int line) {
		try {
			return this.util.getIndentedOffsetAt(line, this.lineColumns[line]);
		} catch (final BadLocationException e) {
			return -1;
		}
	}
	
	public TextEdit getIndentEdits(final AbstractDocument document, final TexAstNode node,
			final int codeOffset, final int firstLine, final int lastLine) throws CoreException {
		try {
			init(document, node, firstLine, lastLine);
			computeIndent(codeOffset);
			return createEdits();
		}
		catch (final BadLocationException e) {
			throw createFailedException(e);
		}
	}
	
	protected void computeIndent(final int codeOffset)
			throws BadLocationException {
		this.refLine= this.firstLine - 1;
		if (this.refLine >= 0) {
			this.lineColumns[this.refLine]= this.util.getLineIndent(this.refLine, false)[IndentUtil.COLUMN_IDX];
			for (int i= this.refLine + 1; i < this.firstLine; i++) {
				this.lineColumns[i]= this.lineColumns[this.refLine];
			}
		}
		
		int line= this.firstLine;
		while (line <= this.lastLine) {
			boolean addEnv= false;
			if (line > 0 && this.codeStyle.getIndentEnvDepth() > 0) {
				this.lineColumns[line]= this.lineColumns[line - 1];
				final IRegion prevLine= this.document.getLineInformation(line - 1);
				final IRegion currentLine= this.document.getLineInformation(line);
				final int currentLineStop= currentLine.getOffset() + currentLine.getLength();
				AstNode node= AstSelection.search(this.rootNode,
						prevLine.getOffset()+prevLine.getLength(),
						prevLine.getOffset()+prevLine.getLength(),
						AstSelection.MODE_COVERING_SAME_LAST ).getCovering();
				while (node != null && !(node instanceof TexAstNode)) {
					node= node.getParent();
				}
				if (node instanceof TexAstNode texNode) {
					final Set<String> envs= this.codeStyle.getIndentEnvLabels();
					while (texNode != null
							&& (texNode.getStartOffset() >= prevLine.getOffset()
									|| texNode.getEndOffset() <= currentLineStop )) {
						if (texNode.getNodeType() == NodeType.ENVIRONMENT) {
							if (envs == null || envs.contains(texNode.getText())) {
								if (texNode.getStartOffset() >= prevLine.getOffset()
										&& texNode.getEndOffset() > currentLineStop ) {
									addEnv= true;
								}
								else if (texNode.getStartOffset() < prevLine.getOffset()
										&& texNode.getEndOffset() <= currentLineStop ) {
									final int beginLine= this.document.getLineOfOffset(texNode.getStartOffset());
									if (this.lineColumns[beginLine] < 0) {
										this.lineColumns[beginLine]= this.util.getLineIndent(beginLine, false)[IndentUtil.COLUMN_IDX];
									}
									this.lineColumns[line]= this.lineColumns[beginLine];
								}
								break;
							}
						}
						texNode= texNode.getTexParent();
					}
				}
				if (addEnv) {
					this.lineColumns[line]+= this.codeStyle.getIndentEnvDepth() * this.util.getLevelColumns();
				}
				line ++;
			}
		}
	}
	
	protected MultiTextEdit createEdits() throws BadLocationException, CoreException {
		final MultiTextEdit edits= new MultiTextEdit();
		final IndentEditAction action= new IndentEditAction() {
			@Override
			public int getIndentColumn(final int line, final int lineOffset) throws BadLocationException {
				return getNewIndentColumn(line);
			}
			@Override
			public void doEdit(final int line, final int offset, final int length, final StringBuilder text)
					throws BadLocationException {
				if (text != null) {
					edits.addChild(new ReplaceEdit(offset, length, text.toString()));
				}
			}
		};
		this.util.changeIndent(this.firstLine, this.lastLine, action);
		return edits;
	}
	
	protected CoreException createFailedException(final Throwable e) {
		return new CoreException(new Status(Status.ERROR, TexCore.BUNDLE_ID, -1, "Indentation failed", e));
	}
	
}
