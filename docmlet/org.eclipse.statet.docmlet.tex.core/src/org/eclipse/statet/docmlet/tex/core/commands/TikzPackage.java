/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import static org.eclipse.statet.docmlet.tex.core.commands.EnvDefinitions.GENERICENV_ENVLABEL_ARGUMENT;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_ELEMENT_GRAPHIC_PICTURE;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet.LTX_TEXT_CONTEXT_ID;

import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;


@NonNullByDefault
public class TikzPackage extends TexPackage {
	
	
	public static final String PACKAGE_NAME= "tikz"; //$NON-NLS-1$
	
	
	private final ImList<TexCommand> commands;
	private final ImList<TexCommand> envs;
	
	
	public TikzPackage() {
		super(PACKAGE_NAME, "TikZ"); //$NON-NLS-1$
		
		this.commands= ImCollections.newList(
				new TexCommand(C3_ELEMENT_GRAPHIC_PICTURE,
						"tikz", false, ImCollections.newList( //$NON-NLS-1$
								new Parameter("options", Parameter.OPTIONAL, Parameter.NONE)
						), this, "TikZ Picture (single command)" )
				);
		this.envs= ImCollections.newList(
				new TexCommand(TexCommand.C3_ENV_ELEMENT_GRAPHICS_BEGIN,
						"tikzpicture", false, ImCollections.newList( //$NON-NLS-1$
								GENERICENV_ENVLABEL_ARGUMENT,
								new Parameter("options", Parameter.OPTIONAL, Parameter.NONE)
						), this, "TikZ Picture" )
				);
	}
	
	
	@Override
	public List<TexCommand> getCommands(final String contextId) {
		if (contextId == LTX_TEXT_CONTEXT_ID) {
			return this.commands;
		}
		return NO_COMMANDS;
	}
	
	@Override
	public List<TexCommand> getEnvs(final String contextId) {
		if (contextId == LTX_TEXT_CONTEXT_ID) {
			return this.envs;
		}
		return NO_COMMANDS;
	}
	
}
