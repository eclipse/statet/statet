/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_GROUP_NOT_CLOSED;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.tex.core.commands.EnvDefinitions;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;
import org.eclipse.statet.ltk.ast.core.Asts;
import org.eclipse.statet.ltk.core.StatusCodes;


@NonNullByDefault
public class TexAsts extends Asts {
	
	
	/**
	 * Resolves the argument values (AST nodes) to the arguments defined by the command.
	 * The command have to be provided by the control node ({@link ControlNode#getCommand()}).
	 * 
	 * @param node the control node
	 * @return array with the resolved arguments (items can be <code>null</code>)
	 */
	public static @Nullable TexAstNode[] resolveArguments(final ControlNode node) {
		final var parameters= nonNullAssert(node.getCommand()).getParameters();
		final @Nullable TexAstNode[] resolved= new @Nullable TexAstNode[parameters.size()];
		int idxArgs= 0, idxValues= 0;
		while (idxArgs < resolved.length && idxValues < node.getChildCount()) {
			final TexAstNode child= node.getChild(idxValues);
			if ((parameters.get(idxArgs).getType() & Parameter.OPTIONAL) != 0) {
				if (child.getText() == "[") { //$NON-NLS-1$
					idxValues++;
					resolved[idxArgs++]= child;
					continue;
				}
				else {
					idxArgs++;
					continue;
				}
			}
			else {
				if (child.getText() == "[") { //$NON-NLS-1$
					idxValues++;
					continue;
				}
				else {
					idxValues++;
					resolved[idxArgs++]= child;
				}
			}
		}
		return resolved;
	}
	
	/**
	 * Returns the index in the array for the node at the specified offset.
	 * 
	 * @param nodes array with nodes (items can be <code>null</code>) ordered by offset
	 * @param offset the offset of the searched node
	 * @return the index in the array if found, otherwise, <tt>(-(<i>insertion index</i>) - 1)</tt>
	 */
	public static int getIndexAt(final @Nullable TexAstNode[] nodes, final int offset) {
		int insert= 0;
		for (int i= 0; i < nodes.length; i++) {
			final var node= nodes[i];
			if (node != null) {
				if (offset < node.getStartOffset()) {
					break;
				}
				if (offset <= node.getEndOffset()) {
					return i;
				}
				insert= i + 1;
			}
		}
		return -(insert + 1);
	}
	
	/**
	 * At moment only for groups
	 * 
	 * @param node
	 * @return
	 */
	public static TextRegion getInnerRegion(final TexAstNode node) {
		if ((node.getStatusCode() & StatusCodes.TYPE123) == TYPE123_GROUP_NOT_CLOSED) {
			return new BasicTextRegion(node.getStartOffset() + 1, node.getEndOffset());
		}
		else {
			return new BasicTextRegion(node.getStartOffset() + 1, node.getEndOffset() - 1);
		}
	}
	
	public static @Nullable Environment getDocumentNode(final TexAstNode node) {
		final int count= node.getChildCount();
		for (int i= 0; i < count; i++) {
			final TexAstNode child= node.getChild(i);
			if (child.getNodeType() == NodeType.ENVIRONMENT
					&& ((Environment) node).getBeginNode().getCommand() == EnvDefinitions.ENV_document_BEGIN) {
				return (Environment) child;
			}
		}
		return null;
	}
	
	
}
