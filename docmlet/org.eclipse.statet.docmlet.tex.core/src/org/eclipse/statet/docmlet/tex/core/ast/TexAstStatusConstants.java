/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import static org.eclipse.statet.ltk.core.StatusCodes.ERROR;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE1;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE2;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE3;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface TexAstStatusConstants {
	
	
	static int TYPE1_INCOMPLETE_ELEMENT=                    0x5 << SHIFT_TYPE1;
	
	static int TYPE12_INCOMPLETE_ENV=                           TYPE1_INCOMPLETE_ELEMENT | 0x1 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_ENV_NOT_OPENED=                              TYPE12_INCOMPLETE_ENV | 0x1 << SHIFT_TYPE3;
	static int TYPE123_ENV_NOT_CLOSED=                              TYPE12_INCOMPLETE_ENV | 0x2 << SHIFT_TYPE3;
	static int TYPE123_ENV_MISSING_NAME=                            TYPE12_INCOMPLETE_ENV | 0x3 << SHIFT_TYPE3;
	static int TYPE123_ENV_UNKNOWN_ENV=                             TYPE12_INCOMPLETE_ENV | 0x4 << SHIFT_TYPE3;
	
	static int TYPE12_INCOMPLETE_VERBATIM=                      TYPE1_INCOMPLETE_ELEMENT | 0x2 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_VERBATIM_INLINE_C_MISSING=                   TYPE12_INCOMPLETE_VERBATIM | 0x1 << SHIFT_TYPE3;
	static int TYPE123_VERBATIM_INLINE_NOT_CLOSED=                  TYPE12_INCOMPLETE_VERBATIM | 0x2 << SHIFT_TYPE3;
	
	static int TYPE12_INCOMPLETE_MATH=                          TYPE1_INCOMPLETE_ELEMENT | 0x3 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_MATH_NOT_CLOSED=                             TYPE12_INCOMPLETE_MATH | 0x2 << SHIFT_TYPE3;
	
	static int TYPE12_INCOMPLETE_GROUP=                         TYPE1_INCOMPLETE_ELEMENT | 0x4 << SHIFT_TYPE2 | ERROR;
	static int TYPE123_GROUP_NOT_OPENED=                            TYPE12_INCOMPLETE_GROUP | 0x1 << SHIFT_TYPE3;
	static int TYPE123_GROUP_NOT_CLOSED=                            TYPE12_INCOMPLETE_GROUP | 0x2 << SHIFT_TYPE3;
	
	
}
