/*=============================================================================#
 # Copyright (c) 2007, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.source.util;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextUtilities;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionUtils;

import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;


@NonNullByDefault
public class LtxChunkHeuristicTokenScanner extends LtxHeuristicTokenScanner {
	
	
	public LtxChunkHeuristicTokenScanner(final DocContentSections documentContentInfo) {
		super(documentContentInfo);
	}
	
	
	@Override
	protected int createForwardBound(final int start) throws BadLocationException {
		final PartitionConstraint matcher= getPartitionConstraint();
		if (matcher.matches(TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE)) {
			final TextRegion rCodeRegion= getTexCodeRegion(start);
			if (rCodeRegion != null) {
				return rCodeRegion.getEndOffset();
			}
		}
		final ITypedRegion partition= TextUtilities.getPartition(getDocument(), getDocumentPartitioning(), start, false);
		return partition.getOffset() + partition.getLength();
	}
	
	@Override
	protected int createBackwardBound(final int start) throws BadLocationException {
		final PartitionConstraint matcher= getPartitionConstraint();
		if (matcher.matches(TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE)) {
			final TextRegion rCodeRegion= getTexCodeRegion(start);
			if (rCodeRegion != null) {
				return rCodeRegion.getStartOffset();
			}
		}
		final ITypedRegion partition= TextUtilities.getPartition(getDocument(), getDocumentPartitioning(), start, false);
		return partition.getOffset();
	}
	
	private TextRegion getTexCodeRegion(final int offset) throws BadLocationException {
		return TreePartitionUtils.searchNode(getDocument(), getDocumentPartitioning(), offset, true,
				TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE );
	}
	
}
