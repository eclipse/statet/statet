/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.source.util;

import static org.eclipse.statet.jcommons.string.Chars.CURLY_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.ROUND_BRACKETS;
import static org.eclipse.statet.jcommons.string.Chars.SQUARE_BRACKETS;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.CharPairSet;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;
import org.eclipse.statet.ltk.text.core.HeuristicTokenScanner;


@NonNullByDefault
public class LtxHeuristicTokenScanner extends HeuristicTokenScanner {
	
	
	public static final CharPairSet LTX_BRACKETS= new CharPairSet(
			ImCollections.newIdentityList(CURLY_BRACKETS, ROUND_BRACKETS, SQUARE_BRACKETS),
			'\\' );
	
	
	public static boolean isEscaped(final IDocument document, int offset)
			throws BadLocationException {
		boolean escaped= false;
		while (offset > 0 && document.getChar(--offset) == '\\') {
			escaped= !escaped;
		}
		return escaped;
	}
	
	public static int getSafeMathPartitionOffset(final IDocumentPartitioner partitioner, int offset) throws BadLocationException, BadPartitioningException {
		int startOffset= offset;
		while (offset > 0) {
			final ITypedRegion partition= partitioner.getPartition(offset - 1);
			final String partitionType= partition.getType();
			if (partitionType == TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE
					|| partitionType == TexDocumentConstants.LTX_COMMENT_CONTENT_TYPE
					|| partitionType == TexDocumentConstants.LTX_VERBATIM_CONTENT_TYPE) {
				return startOffset;
			}
			if (partitionType == TexDocumentConstants.LTX_MATH_CONTENT_TYPE) {
				offset= startOffset= partition.getOffset();
				continue;
			}
			offset= partition.getOffset();
			continue;
		}
		return startOffset;
	}
	
	
	public static LtxHeuristicTokenScanner create(final DocContentSections documentContentInfo) {
		return (documentContentInfo.getPrimaryType() == TexDocumentConstants.LTX_PARTITIONING) ?
				new LtxHeuristicTokenScanner(documentContentInfo) :
				new LtxChunkHeuristicTokenScanner(documentContentInfo);
	}
	
	
	protected LtxHeuristicTokenScanner(final DocContentSections documentContentInfo) {
		super(documentContentInfo, TexDocumentConstants.LTX_DEFAULT_CONTENT_CONSTRAINT);
	}
	
	
	@Override
	public void configure(final IDocument document, final String partitionType) {
		if (partitionType == TexDocumentConstants.LTX_MATH_CONTENT_TYPE) {
			super.configure(document, new PartitionConstraint() {
				private boolean never;
				@Override
				public boolean matches(final String contentType) {
					if (this.never) {
						return false;
					}
					if (contentType == TexDocumentConstants.LTX_MATH_CONTENT_TYPE) {
						return true;
					}
					if (getDefaultPartitionConstraint().matches(contentType)) {
						this.never= true;
					}
					return false;
				}
			});
			return;
		}
		super.configure(document, partitionType);
	}
	
	@Override
	public @NonNull CharPairSet getDefaultBrackets() {
		return LTX_BRACKETS;
	}
	
//	@Override
//	protected int createForwardBound(final int start) throws BadLocationException {
//		final PartitionConstraint matcher= getPartitionConstraint();
//		if (matcher.matches(TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE)) {
//			return UNBOUND;
//		}
//		final ITypedRegion partition= TextUtilities.getPartition(document, getPartitioning(), start, false);
//		return partition.getOffset()+partition.getLength();
//	}
//	
//	@Override
//	protected int createBackwardBound(final int start) throws BadLocationException {
//		final PartitionConstraint matcher= getPartitionConstraint();
//		if (matcher.matches(TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE)) {
//			return -1;
//		}
//		final ITypedRegion partition= TextUtilities.getPartition(document, getPartitioning(), start, false);
//		return partition.getOffset();
//	}
	
}
