/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


/**
 * \...
 */
@NonNullByDefault
public abstract class ControlNode extends TexAstNode {
	
	
	static final class Char extends ControlNode {
		
		
		Char(final int startOffset, final int endOffset,
				final String word) {
			super(word);
			doSetStartEndOffset(startOffset, endOffset);
		}
		
		
		@Override
		public boolean hasChildren() {
			return false;
		}
		
		@Override
		public int getChildCount() {
			return 0;
		}
		
		@Override
		public TexAstNode getChild(final int index) {
			throw new IndexOutOfBoundsException();
		}
		
		@Override
		public int getChildIndex(final AstNode child) {
			return -1;
		}
		
		@Override
		public int getArgsStartOffset() {
			return AstNode.NA_OFFSET;
		}
		
		
		@Override
		public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		}
		
		@Override
		public void acceptInTexChildren(final TexAstVisitor visitor) throws InvocationTargetException {
		}
		
	}
	
	static final class Word extends ControlNode {
		
		
		TexAstNode[] arguments= NO_CHILDREN;
		
		
		Word(final int startOffset, final int endOffset,
				final String word) {
			super(word);
			doSetStartEndOffset(startOffset, endOffset);
		}
		
		
		@Override
		public boolean hasChildren() {
			return (this.arguments.length > 0);
		}
		
		@Override
		public int getChildCount() {
			return this.arguments.length;
		}
		
		@Override
		public TexAstNode getChild(final int index) {
			return this.arguments[index];
		}
		
		@Override
		public int getChildIndex(final AstNode child) {
			for (int i= 0; i < this.arguments.length; i++) {
				if (this.arguments[i] == child) {
					return i;
				}
			}
			return -1;
		}
		
		@Override
		public int getArgsStartOffset() {
			return getStartOffset() + 1 + getText().length();
		}
		
		
		@Override
		public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
			for (final TexAstNode child : this.arguments) {
				visitor.visit(child);
			}
		}
		
		@Override
		public void acceptInTexChildren(final TexAstVisitor visitor) throws InvocationTargetException {
			for (final TexAstNode child : this.arguments) {
				child.acceptInTex(visitor);
			}
		}
		
	}
	
	
	private final String word;
	
	@Nullable TexCommand command;
	
	
	private ControlNode(final String word) {
		this.word= word;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.CONTROL;
	}
	
	
	@Override
	public final void acceptInTex(final TexAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	
	@Override
	public final String getText() {
		return this.word;
	}
	
	public final @Nullable TexCommand getCommand() {
		return this.command;
	}
	
	/**
	 * Returns the first possible offset the arguments can start (end of control word).
	 * @return the start offset
	 */
	public abstract int getArgsStartOffset();
	
}
