/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.model;

import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE1;
import static org.eclipse.statet.ltk.core.StatusCodes.SHIFT_TYPE2;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface LtxModelProblemConstants {
	
	
	int TYPE1_RESOLVE=                                      0x8 << SHIFT_TYPE1;
	
	int STATUS12_LABEL_UNDEFINED=                               TYPE1_RESOLVE | 0x2 << SHIFT_TYPE2;
	
	
}
