/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.model.build;

import java.util.List;

import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.ast.Embedded;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceElement;
import org.eclipse.statet.docmlet.tex.core.project.TexProject;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.model.core.build.EmbeddingForeignReconcileTask;
import org.eclipse.statet.ltk.model.core.build.ReconcileConfig;
import org.eclipse.statet.ltk.model.core.build.SourceUnitEmbeddedModelReconciler;


@NonNullByDefault
public interface LtxSourceUnitEmbeddedModelReconciler<TConfig extends ReconcileConfig<?>>
		extends SourceUnitEmbeddedModelReconciler<TexProject, TConfig> {
	
	
	void reconcileAst(SourceContent content,
			List<Embedded> list,
			LtxSourceUnitModelContainer<?> container, @NonNull TConfig config, int level);
	
	void reconcileModel(LtxSourceUnitModelInfo texModel, SourceContent content,
			List<? extends EmbeddingForeignReconcileTask<Embedded, TexSourceElement>> list,
			LtxSourceUnitModelContainer<?> container, @NonNull TConfig config, int level,
			SubMonitor m);
	
	void reportIssues(LtxSourceUnitModelInfo texModel, SourceContent content,
			IssueRequestor issueRequestor,
			LtxSourceUnitModelContainer<?> container, @NonNull TConfig config, int level);
	
}
