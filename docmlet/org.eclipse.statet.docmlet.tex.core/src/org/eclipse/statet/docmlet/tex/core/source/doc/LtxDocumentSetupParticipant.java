/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.source.doc;

import org.eclipse.jface.text.IDocumentPartitioner;

import org.eclipse.statet.ecommons.text.PartitionerDocumentSetupParticipant;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitioner;


/**
 * The document setup participant for TeX.
 */
public class LtxDocumentSetupParticipant extends PartitionerDocumentSetupParticipant {
	
	
	private final boolean templateMode;
	
	
	public LtxDocumentSetupParticipant() {
		this.templateMode= false;
	}
	
	public LtxDocumentSetupParticipant(final boolean enableTemplateMode) {
		this.templateMode= enableTemplateMode;
	}
	
	
	@Override
	public String getPartitioningId() {
		return TexDocumentConstants.LTX_PARTITIONING;
	}
	
	@Override
	protected IDocumentPartitioner createDocumentPartitioner() {
		return new TreePartitioner(getPartitioningId(),
				new LtxPartitionNodeScanner(this.templateMode),
				TexDocumentConstants.LTX_CONTENT_TYPES );
	}
	
}
