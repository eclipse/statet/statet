/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_GROUP_NOT_CLOSED;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


/**
 * {...}
 */
@NonNullByDefault
public class Group extends ContainerNode {
	
	
	static final class Bracket extends Group {
		
		
		Bracket(final TexAstNode group, final int offset, final int endOffset) {
			super(group);
			doSetStartEndOffset(offset, endOffset);
		}
		
		
		@Override
		public String getText() {
			return "{"; //$NON-NLS-1$
		}
		
	}
	
	static final class Square extends Group {
		
		
		Square(final TexAstNode group, final int offset, final int endOffset) {
			super(group);
			doSetStartEndOffset(offset, endOffset);
		}
		
		
		@Override
		public String getText() {
			return "["; //$NON-NLS-1$
		}
		
	}
	
	
	private Group(final TexAstNode parent) {
		super(parent);
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.GROUP;
	}
	
	
	@Override
	public final boolean hasChildren() {
		return (this.children.length > 0);
	}
	
	@Override
	public final int getChildCount() {
		return this.children.length;
	}
	
	@Override
	public final TexAstNode getChild(final int index) {
		return this.children[index];
	}
	
	@Override
	public final int getChildIndex(final AstNode child) {
		for (int i= 0; i < this.children.length; i++) {
			if (this.children[i] == child) {
				return i;
			}
		}
		return -1;
	}
	
	
	@Override
	public final void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		for (final TexAstNode child : this.children) {
			visitor.visit(child);
		}
	}
	
	@Override
	public void acceptInTex(final TexAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public final void acceptInTexChildren(final TexAstVisitor visitor) throws InvocationTargetException {
		for (final TexAstNode child : this.children) {
			child.acceptInTex(visitor);
		}
	}
	
	
	@Override
	void setEndNode(final int endOffset, final TexAstNode endNode) {
		doSetEndOffset(endOffset);
	}
	
	@Override
	void setMissingEnd() {
		doSetStatusCode(TYPE123_GROUP_NOT_CLOSED);
		if (this.children.length > 0) {
			doSetEndOffset(this.children[this.children.length - 1].getEndOffset());
		}
	}
	
}
