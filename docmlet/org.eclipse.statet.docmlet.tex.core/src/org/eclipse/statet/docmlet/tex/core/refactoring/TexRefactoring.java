/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.refactoring;

import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;


public class TexRefactoring {
	
	
	private static final CommonRefactoringFactory LTX_FACTORY= new LtxRefactoringFactory();
	
	public static CommonRefactoringFactory getLtxFactory() {
		return LTX_FACTORY;
	}
	
	
	public static final String DELETE_LTX_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.docmlet.tex.refactoring.DeleteLtxElementsOperation"; //$NON-NLS-1$
	
	public static final String MOVE_LTX_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.docmlet.tex.refactoring.MoveLtxElementsOperation"; //$NON-NLS-1$
	
	public static final String COPY_LTX_ELEMENTS_REFACTORING_ID= "org.eclipse.statet.docmlet.tex.refactoring.CopyLtxElementsOperation"; //$NON-NLS-1$
	
	public static final String PASTE_LTX_CODE_REFACTORING_ID= "org.eclipse.statet.docmlet.tex.refactoring.PasteLtxCodeOperation"; //$NON-NLS-1$
	
	
	public static final String DELETE_LTX_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.docmlet.tex.refactoring.DeleteLtxElementsProcessor"; //$NON-NLS-1$
	
	public static final String MOVE_LTX_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.docmlet.tex.refactoring.MoveLtxElementsProcessor"; //$NON-NLS-1$
	
	public static final String COPY_LTX_ELEMENTS_PROCESSOR_ID= "org.eclipse.statet.docmlet.tex.refactoring.CopyLtxElementsProcessor"; //$NON-NLS-1$
	
	public static final String PASTE_LTX_CODE_PROCESSOR_ID= "org.eclipse.statet.docmlet.tex.refactoring.PasteLtxCodeProcessor"; //$NON-NLS-1$
	
}
