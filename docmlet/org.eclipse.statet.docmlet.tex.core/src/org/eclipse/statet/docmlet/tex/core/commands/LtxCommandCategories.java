/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.internal.docmlet.tex.core.Messages;


@NonNullByDefault
public class LtxCommandCategories {
	
	
	public static class Category {
		
		
		private final Cat cat;
		
		private final ImList<TexCommand> commands;
		
		
		private Category(final Cat cat, final ImList<TexCommand> commands) {
			this.cat= cat;
			this.commands= commands;
		}
		
		
		public String getLabel() {
			return this.cat.label;
		}
		
		public ImList<TexCommand> getCommands() {
			return this.commands;
		}
		
		
		@Override
		public String toString() {
			return this.cat.label + " [" + this.commands.size() + "]"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		
	}
	
	
	private static enum Cat {
		
		SECTIONING(Messages.CommandCategory_Sectioning_label),
		LABEL(Messages.CommandCategory_Label_label),
		TEXT_STYLING(Messages.CommandCategory_TextStyling_label),
		MATH_STYLING(Messages.CommandCategory_MathStyling_label),
		COMMON_SYMBOLS(Messages.CommandCategory_CommonSymbols_label),
		TEXT_SYMBOLS(Messages.CommandCategory_TextSymbols_label),
		MATHSYMBOLS_GREEK_UPPER(Messages.CommandCategory_MathSymbols_GreekUpper_label),
		MATHSYMBOLS_GREEK_LOWER(Messages.CommandCategory_MathSymbols_GreekLower_label),
		MATHSYMBOLS_BIN_OP(Messages.CommandCategory_MathSymbols_BinOp_label),
		MATHSYMBOLS_ROOTFRAC_OP(Messages.CommandCategory_MathSymbols_RootFracOp_label),
		MATHSYMBOLS_REL_STD(Messages.CommandCategory_MathSymbols_RelStd_label),
		MATHSYMBOLS_REL_ARROW(Messages.CommandCategory_MathSymbols_RelArrow_label),
		MATHSYMBOLS_REL_MISC(Messages.CommandCategory_MathSymbols_RelMisc_label),
		MATHSYMBOLS_LARGE_OP(Messages.CommandCategory_MathSymbols_LargeOp_label),
		MATHSYMBOLS_NAMED_OP(Messages.CommandCategory_MathSymbols_NamedOp_label),
		MATHSYMBOLS_MISC_ALPHA(Messages.CommandCategory_MathSymbols_MiscAlpha_label),
		MATHSYMBOLS_MISC_ORD(Messages.CommandCategory_MathSymbols_MiscOrd_label),
		MATHSYMBOLS_DOTS(Messages.CommandCategory_MathSymbols_Dots_label),
		MATHSYMBOLS_ACCENTS(Messages.CommandCategory_MathSymbols_Accents_label),
		MATHSYMBOLS_BRACKETS(Messages.CommandCategory_MathSymbols_Brackets_label);
		
		
		private final String label;
		
		Cat(final String label) {
			this.label= label;
		}
		
	}
	
	
	private final List<Category> categories;
	
	
	public LtxCommandCategories(final List<TexCommand> list) {
		final Cat[] cats= Cat.values();
		
		final @Nullable List<TexCommand>[] lists= new List[cats.length];
		for (final TexCommand command : list) {
			if (include(command)) {
				final Cat cat= getCat(command);
				if (cat != null) {
					if (lists[cat.ordinal()] == null) {
						lists[cat.ordinal()]= new ArrayList<>();
					}
					lists[cat.ordinal()].add(command);
				}
//				else {
//					System.out.println("" + command.getType() + " " + command.getControlWord());
//				}
			}
		}
		
		final List<Category> categories= new ArrayList<>(lists.length);
		for (int i= 0; i < lists.length; i++) {
			if (lists[i] != null) {
				categories.add(new Category(cats[i], ImCollections.toList(lists[i])));
			}
		}
		this.categories= ImCollections.toList(categories);
	}
	
	
	private @Nullable Cat getCat(final TexCommand command) {
		return switch (command.getType() & TexCommand.MASK_MAIN) {
		case TexCommand.SECTIONING ->						Cat.SECTIONING;
		case TexCommand.LABEL ->							Cat.LABEL;
		case TexCommand.STYLE ->
			switch (command.getType() & TexCommand.MASK_C2) {
			case TexCommand.C2_STYLE_TEXT ->				Cat.TEXT_STYLING;
			case TexCommand.C2_STYLE_MATH ->				Cat.MATH_STYLING;
			default ->										null;
			};
		case TexCommand.SYMBOL ->
			switch (command.getType() & TexCommand.MASK_C2) {
			case TexCommand.C2_SYMBOL_COMMON ->				Cat.COMMON_SYMBOLS;
			case TexCommand.C2_SYMBOL_TEXT ->				Cat.TEXT_SYMBOLS;
			default ->										null;
			};
		case TexCommand.MATHSYMBOL ->
			switch (command.getType() & TexCommand.MASK_C3) {
			case TexCommand.C3_MATHSYMBOL_GREEK_UPPER ->	Cat.MATHSYMBOLS_GREEK_UPPER;
			case TexCommand.C3_MATHSYMBOL_GREEK_LOWER ->	Cat.MATHSYMBOLS_GREEK_LOWER;
			case TexCommand.C3_MATHSYMBOL_OP_BIN ->			Cat.MATHSYMBOLS_BIN_OP;
			case TexCommand.C3_MATHSYMBOL_OP_ROOTFRAC ->	Cat.MATHSYMBOLS_ROOTFRAC_OP;
			case TexCommand.C3_MATHSYMBOL_OP_RELSTD ->		Cat.MATHSYMBOLS_REL_STD;
			case TexCommand.C3_MATHSYMBOL_OP_RELARROW ->	Cat.MATHSYMBOLS_REL_ARROW;
			case TexCommand.C3_MATHSYMBOL_OP_RELMISC ->		Cat.MATHSYMBOLS_REL_MISC;
			case TexCommand.C3_MATHSYMBOL_OP_LARGE ->		Cat.MATHSYMBOLS_LARGE_OP;
			case TexCommand.C3_MATHSYMBOL_OP_NAMED ->		Cat.MATHSYMBOLS_NAMED_OP;
			case TexCommand.C3_MATHSYMBOL_MISC_ALPHA ->		Cat.MATHSYMBOLS_MISC_ALPHA;
			case TexCommand.C3_MATHSYMBOL_MISC_ORD ->		Cat.MATHSYMBOLS_MISC_ORD;
			case TexCommand.C3_MATHSYMBOL_DOTS ->			Cat.MATHSYMBOLS_DOTS;
			case TexCommand.C3_MATHSYMBOL_ACCENTS_ ->		Cat.MATHSYMBOLS_ACCENTS;
			case TexCommand.C3_MATHSYMBOL_BRACKETS_ ->		Cat.MATHSYMBOLS_BRACKETS;
			default ->										null;
			};
		default ->											null;
		};
	}
	
	
	protected boolean include(final TexCommand command) {
		return true;
	}
	
	
	public List<Category> getCategories() {
		return this.categories;
	}
	
	public @Nullable Category getCategory(final TexCommand command) {
		final Cat cat= getCat(command);
		if (cat != null) {
			for (final Category category : this.categories) {
				if (category.cat == cat) {
					return category;
				}
			}
		}
		return null;
	}
	
}
