/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.model.build;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.docmlet.tex.core.project.TexIssues;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;


@NonNullByDefault
public class LtxSourceUnitModelContainer<TSourceUnit extends TexSourceUnit>
		extends SourceUnitModelContainer<TSourceUnit, LtxSourceUnitModelInfo> {
	
	
	public static final IssueTypeSet ISSUE_TYPE_SET= new IssueTypeSet(TexCore.BUNDLE_ID,
			TexIssues.TASK_CATEGORY,
			ImCollections.newList(
					TexIssues.LTX_MODEL_PROBLEM_CATEGORY ));
	
	
	public LtxSourceUnitModelContainer(final TSourceUnit unit,
			final @Nullable SourceUnitIssueSupport issueSupport) {
		super(unit, issueSupport);
	}
	
	
	@Override
	public Class<?> getAdapterClass() {
		return LtxSourceUnitModelContainer.class;
	}
	
	@Override
	public boolean isContainerFor(final String modelTypeId) {
		return (modelTypeId == TexModel.LTX_TYPE_ID);
	}
	
	public @Nullable String getNowebType() {
		return null;
	}
	
	@Override
	protected ModelManager getModelManager() {
		return TexModel.getLtxModelManager();
	}
	
}
