/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.util;

import org.eclipse.core.runtime.preferences.IScopeContext;

import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.preferences.core.util.PreferenceAccessWrapper;

import org.eclipse.statet.docmlet.tex.core.TexCodeStyleSettings;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet;


public class TexCoreAccessWrapper extends PreferenceAccessWrapper
		implements TexCoreAccess {
	
	
	private TexCoreAccess parent;
	
	
	public TexCoreAccessWrapper(final TexCoreAccess texCoreAccess) {
		if (texCoreAccess == null) {
			throw new NullPointerException("texCoreAccess"); //$NON-NLS-1$
		}
		
		updateParent(null, texCoreAccess);
	}
	
	
	public synchronized TexCoreAccess getParent() {
		return this.parent;
	}
	
	public final synchronized boolean setParent(final TexCoreAccess texCoreAccess) {
		if (texCoreAccess == null) {
			throw new NullPointerException("texCoreAccess"); //$NON-NLS-1$
		}
		if (texCoreAccess != this.parent) {
			updateParent(this.parent, texCoreAccess);
			return true;
		}
		return false;
	}
	
	protected void updateParent(final TexCoreAccess oldParent, final TexCoreAccess newParent) {
		this.parent= newParent;
		
		super.setPreferenceContexts(newParent.getPrefs().getPreferenceContexts());
	}
	
	@Override
	public void setPreferenceContexts(final ImList<IScopeContext> contexts) {
		throw new UnsupportedOperationException();
	}
	
	
	@Override
	public TexCommandSet getTexCommandSet() {
		return this.parent.getTexCommandSet();
	}
	
	@Override
	public TexCodeStyleSettings getTexCodeStyle() {
		return this.parent.getTexCodeStyle();
	}
	
}
