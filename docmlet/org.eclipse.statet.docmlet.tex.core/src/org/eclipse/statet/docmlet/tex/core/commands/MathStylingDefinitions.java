/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_STYLE_MATH;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;


@NonNullByDefault
public interface MathStylingDefinitions {
	
	
	TexCommand STYLE_mathnormal_COMMAND= new LtxFontCommand(C2_STYLE_MATH,
			"mathnormal", ImCollections.newList(
					new Parameter(Parameter.REQUIRED, Parameter.NONE)
			), "Prints given text using Normal Math font" ); // 2e
	TexCommand STYLE_mathrm_COMMAND= new LtxFontCommand(C2_STYLE_MATH,
			"mathrm", ImCollections.newList(
					new Parameter(Parameter.REQUIRED, Parameter.NONE)
			), "Prints given text using Roman Typeface font family" ); // 2e
	TexCommand STYLE_mathsf_COMMAND= new LtxFontCommand(C2_STYLE_MATH,
			"mathsf", ImCollections.newList(
					new Parameter(Parameter.REQUIRED, Parameter.NONE)
			), "Prints given text using Sans-Serif Typeface font family" ); // 2e
	TexCommand STYLE_mathtt_COMMAND= new LtxFontCommand(C2_STYLE_MATH,
			"mathtt", ImCollections.newList(
					new Parameter(Parameter.REQUIRED, Parameter.NONE)
			), "Prints given text using Typewriter-like Face font family" ); // 2e
	TexCommand STYLE_mathcal_COMMAND= new LtxFontCommand(C2_STYLE_MATH,
			"mathcal", ImCollections.newList(
					new Parameter(Parameter.REQUIRED, Parameter.NONE)
			), "Prints given text using Calligraphic font family" ); // 2e
	TexCommand STYLE_mathbf_COMMAND= new LtxFontCommand(C2_STYLE_MATH,
			"mathbf", ImCollections.newList(
					new Parameter(Parameter.REQUIRED, Parameter.NONE)
			), "Prints given text using Bold Weight font series" ); // 2e
	TexCommand STYLE_mathit_COMMAND= new LtxFontCommand(C2_STYLE_MATH,
			"mathit", ImCollections.newList(
					new Parameter(Parameter.REQUIRED, Parameter.NONE)
			), "Prints given text using Italic font shape" ); // 2e
	
	
	TexCommand MISC_nonumber_COMMAND= new TexCommand(0,
			"nonumber", "Disables numbering of the current equation" );
	
}
