/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class LtxPrintCommand extends TexCommand {
	
	
	private final String unicode;
	
	
	public LtxPrintCommand(final int type,
			final String word,
			final String description,
			final String unicode) {
		super(type, word, description);
		this.unicode= unicode;
	}
	
	public LtxPrintCommand(final int type,
			final String word, final ImList<? extends Parameter> parameters,
			final String description,
			final String unicode) {
		super(type, word, false, parameters, description);
		this.unicode= unicode;
	}
	
	
	public String getText() {
		return this.unicode;
	}
	
}
