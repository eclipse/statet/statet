/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.parser;

import org.eclipse.statet.jcommons.text.core.input.TextParserInput;


public class NowebLtxLexer extends LtxLexer {
	
	
	private String nowebType;
	
	
	public NowebLtxLexer() {
		super();
	}
	
	
	public void setNowebType(final String modelTypeId) {
		this.nowebType= modelTypeId;
	}
	
	
	@Override
	protected void handleNewLine(final int offset, int n) {
		super.handleNewLine(offset, n);
		
		final TextParserInput in= getInput();
		if (this.nowebType != null
				&& in.get(n++) == '<' && in.get(n++) == '<') {
			setEmbeddedBegin();
		}
	}
	
	@Override
	protected void searchEmbedded() {
		final TextParserInput in= getInput();
		int n= 2;
		CHUNK_CONTENT: while (true) {
			switch (in.get(n++)) {
			case TextParserInput.EOF:
				setEmbeddedEnd(n - 1, this.nowebType);
				return;
			case '\r':
				if (in.get(n) == '\n') {
					n++;
				}
				//$FALL-THROUGH$
			case '\n':
				super.handleNewLine(in.getIndex() + in.getLengthInSource(n), n);
				if (in.get(n) == '@') {
					n++;
					CHUNK_END: while (true) {
						switch (in.get(n++)) {
						case TextParserInput.EOF:
							setEmbeddedEnd(n - 1, this.nowebType);
							return;
						case '\r':
							if (in.get(n) == '\n') {
								n++;
							}
							//$FALL-THROUGH$
						case '\n':
							setEmbeddedEnd(n, this.nowebType);
							return;
						default:
							continue CHUNK_END;
						}
					}
				}
				continue CHUNK_CONTENT;
			default:
				continue CHUNK_CONTENT;
			}
		}
	}
	
}
