/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;

import org.eclipse.statet.ecommons.preferences.AbstractPreferencesModelObject;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.EnumPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.IntPref;
import org.eclipse.statet.ecommons.preferences.core.Preference.StringSetPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.IIndentSettings;


/**
 * Settings for style of R code.
 */
public class TexCodeStyleSettings extends AbstractPreferencesModelObject
		implements IIndentSettings {
	
	
	public static final String QUALIFIER= TexCore.BUNDLE_ID + "/codestyle/tex"; //$NON-NLS-1$
	public static final String INDENT_GROUP_ID= "tex/tex.codestyle/indent"; //$NON-NLS-1$
	
	
	public static final String[] ALL_GROUP_IDS= new String[] { INDENT_GROUP_ID };
	
	
	public static final IntPref TAB_WIDTH_PREF= new IntPref(
			QUALIFIER, "Tab.size"); //$NON-NLS-1$
	
	public static final EnumPref<IndentationType> INDENT_DEFAULT_TYPE_PREF= new EnumPref<>(
			QUALIFIER, "Indent.type", IndentationType.class, //$NON-NLS-1$
			IndentationType.TAB );
	
	public static final IntPref INDENT_SPACES_COUNT_PREF= new IntPref(
			QUALIFIER, "Indent.Level.spaces_count"); //$NON-NLS-1$
	
	public static final BooleanPref REPLACE_CONVERSATIVE_PREF= new BooleanPref(
			QUALIFIER, "Indent.ReplaceConservativ.enabled"); //$NON-NLS-1$
	
	public static final BooleanPref REPLACE_TABS_WITH_SPACES_PREF= new BooleanPref(
			QUALIFIER, "Indent.ReplaceOtherTabs.enabled"); //$NON-NLS-1$
	
	
	public static final IntPref INDENT_BLOCK_DEPTH_PREF= new IntPref(
			QUALIFIER, "IndentBlockDepth.level"); //$NON-NLS-1$
	public static final String INDENT_BLOCK_DEPTH_PROP= "indentBlockDepth"; //$NON-NLS-1$
	
	public static final IntPref INDENT_ENV_DEPTH_PREF= new IntPref(
			QUALIFIER, "IndentEnvDepth.level"); //$NON-NLS-1$
	public static final String INDENT_ENV_DEPTH_PROP= "indentEnvDepth"; //$NON-NLS-1$
	
	public static final Preference<Set<String>> INDENT_ENV_LABELS_PREF= new StringSetPref(
			QUALIFIER, "IndentEnvFor.labels"); //$NON-NLS-1$
	public static final String INDENT_ENV_LABELS_PROP= "indentEnvLabels"; //$NON-NLS-1$
	
	
	public static final Preference<Integer> WRAP_LINE_WIDTH_PREF= new IntPref(
			QUALIFIER, "Wrap.LineWidth.max"); //$NON-NLS-1$
	
	
	private int tabWidth;
	private IndentationType indentDefaultType;
	private int indentSpacesCount;
	private boolean replaceOtherTabsWithSpaces;
	private boolean replaceConservative;
	
	private int lineWidth;
	
	private int indentBlockDepth;
	private int indentEnvDepth;
	private Set<String> indentEnvLabels;
	
	
	/**
	 * Creates an instance with default settings.
	 */
	public TexCodeStyleSettings(final int mode) {
		if (mode >= 1) {
			installLock();
		}
		loadDefaults();
		resetDirty();
	}
	
	
	@Override
	public String[] getNodeQualifiers() {
		return new String[] { QUALIFIER };
	}
	
	@Override
	public void loadDefaults() {
		setTabWidth(4);
		setIndentDefaultType(IndentationType.SPACES);
		setIndentSpacesCount(2);
		setReplaceConservative(false);
		setReplaceOtherTabsWithSpaces(false);
		setIndentBlockDepth(1);
		setIndentEnvDepth(1);
		setIndentEnvLabels(Set.of("itemize", "enumerate", "description"));
		setLineWidth(80);
	}
	
	@Override
	public void load(final PreferenceAccess prefs) {
		setTabWidth(prefs.getPreferenceValue(TAB_WIDTH_PREF));
		setIndentDefaultType(prefs.getPreferenceValue(INDENT_DEFAULT_TYPE_PREF));
		setIndentSpacesCount(prefs.getPreferenceValue(INDENT_SPACES_COUNT_PREF));
		setReplaceConservative(prefs.getPreferenceValue(REPLACE_CONVERSATIVE_PREF));
		setReplaceOtherTabsWithSpaces(prefs.getPreferenceValue(REPLACE_TABS_WITH_SPACES_PREF));
		setIndentBlockDepth(prefs.getPreferenceValue(INDENT_BLOCK_DEPTH_PREF));
		setIndentEnvDepth(prefs.getPreferenceValue(INDENT_ENV_DEPTH_PREF));
		setIndentEnvLabels(prefs.getPreferenceValue(INDENT_ENV_LABELS_PREF));
		setLineWidth(prefs.getPreferenceValue(WRAP_LINE_WIDTH_PREF));
	}
	
	public void load(final TexCodeStyleSettings source) {
		final Lock writeLock= getWriteLock();
		final Lock sourceLock= source.getReadLock();
		try {
			sourceLock.lock();
			writeLock.lock();
			
			setTabWidth(source.tabWidth);
			setIndentDefaultType(source.indentDefaultType);
			setIndentSpacesCount(source.indentSpacesCount);
			setReplaceConservative(source.replaceConservative);
			setReplaceOtherTabsWithSpaces(source.replaceOtherTabsWithSpaces);
			setIndentBlockDepth(source.indentBlockDepth);
			setIndentEnvDepth(source.indentEnvDepth);
			setIndentEnvLabels(source.indentEnvLabels);
			setLineWidth(source.lineWidth);
		}
		finally {
			sourceLock.unlock();
			writeLock.unlock();
		}
	}
	
	@Override
	public Map<Preference<?>, Object> deliverToPreferencesMap(final Map<Preference<?>, Object> map) {
		map.put(TAB_WIDTH_PREF, getTabWidth());
		map.put(INDENT_DEFAULT_TYPE_PREF, getIndentDefaultType());
		map.put(INDENT_SPACES_COUNT_PREF, getIndentSpacesCount());
		map.put(REPLACE_CONVERSATIVE_PREF, getReplaceConservative());
		map.put(REPLACE_TABS_WITH_SPACES_PREF, getReplaceOtherTabsWithSpaces());
		map.put(WRAP_LINE_WIDTH_PREF, getLineWidth());
		map.put(INDENT_BLOCK_DEPTH_PREF, getIndentBlockDepth());
		map.put(INDENT_ENV_DEPTH_PREF, getIndentEnvDepth());
		map.put(INDENT_ENV_LABELS_PREF, getIndentEnvLabels());
		return map;
	}
	
	
/*[ Properties ]===============================================================*/
	
	public void setTabWidth(final int size) {
		final int oldValue= this.tabWidth;
		this.tabWidth= size;
		firePropertyChange(TAB_WIDTH_PROP, oldValue, size);
	}
	@Override
	public int getTabWidth() {
		return this.tabWidth;
	}
	
	public void setIndentDefaultType(final IndentationType type) {
		final IndentationType oldValue= this.indentDefaultType;
		this.indentDefaultType= type;
		firePropertyChange(INDENT_DEFAULT_TYPE_PROP, oldValue, type);
	}
	@Override
	public IndentationType getIndentDefaultType() {
		return this.indentDefaultType;
	}
	
	public void setIndentSpacesCount(final int count) {
		final int oldValue= this.indentSpacesCount;
		this.indentSpacesCount= count;
		firePropertyChange(INDENT_SPACES_COUNT_PROP, oldValue, count);
	}
	@Override
	public int getIndentSpacesCount() {
		return this.indentSpacesCount;
	}
	
	public void setReplaceConservative(final boolean enable) {
		final boolean oldValue= this.replaceConservative;
		this.replaceConservative= enable;
		firePropertyChange(REPLACE_CONSERVATIVE_PROP, oldValue, enable);
	}
	@Override
	public boolean getReplaceConservative() {
		return this.replaceConservative;
	}
	
	public void setReplaceOtherTabsWithSpaces(final boolean enable) {
		final boolean oldValue= this.replaceOtherTabsWithSpaces;
		this.replaceOtherTabsWithSpaces= enable;
		firePropertyChange(REPLACE_TABS_WITH_SPACES_PROP, oldValue, getReplaceOtherTabsWithSpaces());
	}
	@Override
	public boolean getReplaceOtherTabsWithSpaces() {
		return this.replaceOtherTabsWithSpaces;
	}
	
	
	public final void setIndentBlockDepth(final int depth) {
		final int oldValue= this.indentBlockDepth;
		this.indentBlockDepth= depth;
		firePropertyChange(INDENT_BLOCK_DEPTH_PROP, oldValue, depth);
	}
	public final int getIndentBlockDepth() {
		return this.indentBlockDepth;
	}
	
	public final void setIndentEnvDepth(final int depth) {
		final int oldValue= this.indentEnvDepth;
		this.indentEnvDepth= depth;
		firePropertyChange(INDENT_ENV_DEPTH_PROP, oldValue, depth);
	}
	public final int getIndentEnvDepth() {
		return this.indentEnvDepth;
	}
	
	public void setIndentEnvLabels(final Set<String> labels) {
		this.indentEnvLabels= labels;
	}
	public Set<String> getIndentEnvLabels() {
		return this.indentEnvLabels;
	}
	
	
	public void setLineWidth(final int max) {
		final int oldValue= this.lineWidth;
		this.lineWidth= max;
		firePropertyChange(WRAP_LINE_WIDTH_PROP, oldValue, max);
	}
	@Override
	public int getLineWidth() {
		return this.lineWidth;
	}
	
}
