/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_BIB_INCLUDE;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C2_BIB_REF;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_LABEL_COUNTER_DEF;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_LABEL_COUNTER_REF;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_LABEL_REFLABEL_DEF;
import static org.eclipse.statet.docmlet.tex.core.commands.TexCommand.C3_LABEL_REFLABEL_REF;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.commands.TexCommand.Parameter;


@NonNullByDefault
public interface RefDefinitions {
	
	
	TexCommand LABEL_label_COMMAND= new TexCommand(C3_LABEL_REFLABEL_DEF,
			"label", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("marker", Parameter.REQUIRED, Parameter.LABEL_REFLABEL_DEF)
			), "Marks the current element/line with the given label" );
	TexCommand LABEL_ref_COMMAND= new TexCommand(C3_LABEL_REFLABEL_REF,
			"ref", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("marker", Parameter.REQUIRED, Parameter.LABEL_REFLABEL_REF)
			), "Prints a Reference (number) to the given label" );
	TexCommand LABEL_pageref_COMMAND= new TexCommand(C3_LABEL_REFLABEL_REF,
			"pageref", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("marker", Parameter.REQUIRED, Parameter.LABEL_REFLABEL_REF)
			), "Prints a Page Reference (page number) to the given label" );
	
	TexCommand LABEL_eqref_COMMAND= new TexCommand(C3_LABEL_REFLABEL_REF,
			"eqref", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("marker", Parameter.REQUIRED, Parameter.LABEL_REFLABEL_REF)
			), "Prints a Reference (number) to the given equation label" );
	
	
	TexCommand LABEL_newcounter_COMMAND= new TexCommand(C3_LABEL_COUNTER_DEF,
			"newcounter", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_DEF),
					new Parameter("superordinated counter", Parameter.OPTIONAL, Parameter.LABEL_COUNTER_REF)
			), "Defines a new counter" );
	
	TexCommand LABEL_setcounter_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"setcounter", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_SET),
					new Parameter("value", Parameter.REQUIRED, Parameter.NONE)
			), "Sets the counter to the given value" );
	
	TexCommand LABEL_addtocounter_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"addtocounter", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_SET),
					new Parameter("value", Parameter.REQUIRED, Parameter.NONE)
			), "Increments the counter by the given value" );
	
	TexCommand LABEL_stepcounter_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"stepcounter", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_SET)
			), "Increments the counter by one" );
	
	TexCommand LABEL_Alph_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"Alph", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_REF)
			), "Prints the current value of the counter in alphabetic uppercase letters (A, B, C,...)" );
	
	TexCommand LABEL_alph_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"alph", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_REF)
			), "Prints the current value of the counter in alphabetic lowercase letters (a, b, c,...)" );
	
	TexCommand LABEL_Roman_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"Roman", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_REF)
			), "Prints the current value of the counter in uppercase roman numbers (I, II, III,...)" );
	
	TexCommand LABEL_roman_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"roman", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_REF)
			), "Prints the current value of the counter in lowercase roman numbers (i, ii, iii,...)" );
	
	TexCommand LABEL_arabic_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"arabic", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_REF)
			), "Prints the current value of the counter in arabic numbers (1, 2, 3,...)" );
	
	TexCommand LABEL_value_COMMAND= new TexCommand(C3_LABEL_COUNTER_REF,
			"value", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("counter", Parameter.REQUIRED, Parameter.LABEL_COUNTER_REF)
			), "Returns the current value of the counter as number" );
	
	
	TexCommand BIB_bibitem_COMMAND= new TexCommand(TexCommand.C2_BIB_DEF,
			"bibitem", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("label to print", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("key", Parameter.REQUIRED, Parameter.LABEL_BIB_DEF)
			), "Adds an entry to the bibliography" );
	
	TexCommand BIB_cite_COMMAND= new TexCommand(C2_BIB_REF,
			"cite", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("annotation", Parameter.OPTIONAL, Parameter.NONE),
					new Parameter("key", Parameter.REQUIRED, Parameter.LABEL_BIB_REF)
			), "Prints a literature reference to the given bibliography entry" );
	
	TexCommand BIB_nocite_COMMAND= new TexCommand(C2_BIB_REF,
			"nocite", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("key", Parameter.REQUIRED, Parameter.LABEL_BIB_REF)
			), "Ensures that the given literature reference appears in the bibliography of the document" );
	
	TexCommand BIB_bibliography_COMMAND= new TexCommand(C2_BIB_INCLUDE,
			"bibliography", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("files", Parameter.REQUIRED, Parameter.RESOURCE_LIST)
			), "Includes the given bibliography(s)" );
	
	TexCommand BIB_bibliographystyle_COMMAND= new TexCommand(TexCommand.BIB,
			"bibliographystyle", false, ImCollections.newList( //$NON-NLS-1$
					new Parameter("style", Parameter.REQUIRED, Parameter.NONE)
			), "Includes the given bibliography(s)" );
	
	
}
