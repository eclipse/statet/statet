/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;
import org.eclipse.statet.ltk.ast.core.EmbeddingAstNode;


@NonNullByDefault
public class Embedded extends TexAstNode implements EmbeddingAstNode {
	
	
	static class Inline extends Embedded {
		
		
		Inline(final TexAstNode parent, final int startOffset, final String type) {
			super(parent, startOffset, startOffset, type);
		}
		
		
		@Override
		public int getEmbedDescr() {
			return EMBED_INLINE;
		}
		
	}
	
	
	private final String foreignType;
	
	private @Nullable AstNode foreignNode;
	
	
	Embedded(final TexAstNode parent, final int startOffset, final int endOffset,
			final String foreignType) {
		super(parent);
		doSetStartEndOffset(startOffset, endOffset);
		this.foreignType= foreignType;
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.EMBEDDED;
	}
	
	@Override
	public String getForeignTypeId() {
		return this.foreignType;
	}
	
	@Override
	public int getEmbedDescr() {
		return EMBED_CHUNK;
	}
	
	@Override
	public void setForeignNode(final @Nullable AstNode node) {
		this.foreignNode= node;
		if (node != null) {
			doSetStatusSeverityOfChild(node.getStatusCode());
		}
	}
	
	@Override
	public @Nullable AstNode getForeignNode() {
		return this.foreignNode;
	}
	
	@Override
	public boolean hasChildren() {
		return false;
	}
	
	@Override
	public int getChildCount() {
		return 0;
	}
	
	@Override
	public TexAstNode getChild(final int index) {
		throw new IndexOutOfBoundsException();
	}
	
	@Override
	public int getChildIndex(final AstNode child) {
		return -1;
	}
	
	
	@Override
	@SuppressWarnings("null")
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		if (this.foreignNode != null) {
			this.foreignNode.accept(visitor);
		}
	}
	
	@Override
	public void acceptInTex(final TexAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public void acceptInTexChildren(final TexAstVisitor visitor) throws InvocationTargetException {
	}
	
	
	@Override
	public String getText() {
		return this.foreignType;
	}
	
}
