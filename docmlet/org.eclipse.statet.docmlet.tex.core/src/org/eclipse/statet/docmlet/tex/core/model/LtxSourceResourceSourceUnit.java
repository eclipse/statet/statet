/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.model;

import org.eclipse.core.resources.IFile;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;

import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitModelContainer;
import org.eclipse.statet.docmlet.tex.core.source.doc.LtxDocumentContentInfo;
import org.eclipse.statet.internal.docmlet.tex.core.WorkspaceAdapterFactory;
import org.eclipse.statet.ltk.model.core.build.SourceUnitIssueSupport;
import org.eclipse.statet.ltk.model.core.impl.GenericResourceSourceUnit2;
import org.eclipse.statet.ltk.model.core.impl.ResourceIssueSupport;


@NonNullByDefault
public class LtxSourceResourceSourceUnit
		extends GenericResourceSourceUnit2<LtxSourceUnitModelContainer<TexSourceUnit>>
		implements TexWorkspaceSourceUnit {
	
	
	private static final SourceUnitIssueSupport ISSUE_SUPPORT=
			new ResourceIssueSupport(LtxSourceUnitModelContainer.ISSUE_TYPE_SET);
	
	
	public LtxSourceResourceSourceUnit(final String id, final IFile file) {
		super(id, file);
	}
	
	@Override
	protected LtxSourceUnitModelContainer<TexSourceUnit> createModelContainer() {
		return new LtxSourceUnitModelContainer<>(this, null); // marker disabled
	}
	
	
	@Override
	public String getModelTypeId() {
		return TexModel.LTX_TYPE_ID;
	}
	
	@Override
	public DocContentSections getDocumentContentInfo() {
		return LtxDocumentContentInfo.INSTANCE;
	}
	
	public TexCoreAccess getTexCoreAccess() {
		return WorkspaceAdapterFactory.getResourceCoreAccess(getResource());
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == TexCoreAccess.class) {
			return (T)getTexCoreAccess();
		}
		if (adapterType == PreferenceAccess.class) {
			return (T)getTexCoreAccess().getPrefs();
		}
		return super.getAdapter(adapterType);
	}
	
	
}
