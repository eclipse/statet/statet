/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.model;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;


@NonNullByDefault
public interface TexElement<TModelChild extends TexElement<?>>
		extends LtkModelElement<TModelChild> {
	
	
	static int C12_PREAMBLE=        C1_CLASS | 0x1 << SHIFT_C2;
	
	static int C12_SECTIONING=      C1_CLASS | 0x2 << SHIFT_C2;
	
	
}
