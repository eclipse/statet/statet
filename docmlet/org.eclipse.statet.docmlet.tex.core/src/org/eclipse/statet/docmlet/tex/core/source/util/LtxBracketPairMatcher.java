/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.source.util;

import static org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants.LTX_DEFAULT_CONTENT_TYPE;
import static org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants.LTX_MATH_CONTENT_TYPE;
import static org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants.LTX_VERBATIM_CONTENT_TYPE;
import static org.eclipse.statet.docmlet.tex.core.source.util.LtxHeuristicTokenScanner.LTX_BRACKETS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.text.core.PartitionConstraint;
import org.eclipse.statet.ecommons.text.core.TextTokenScanner;

import org.eclipse.statet.ltk.text.core.BasicCharPairMatcher;


/**
 * A pair finder class for implementing the pair matching.
 */
@NonNullByDefault
public class LtxBracketPairMatcher extends BasicCharPairMatcher {
	
	
	private static final PartitionConstraint CONTENT_TYPES= new PartitionConstraint() {
		@Override
		public boolean matches(final String contentType) {
			return (contentType == LTX_DEFAULT_CONTENT_TYPE
					|| contentType == LTX_MATH_CONTENT_TYPE
					|| contentType == LTX_VERBATIM_CONTENT_TYPE );
		}
	};
	
	
	public LtxBracketPairMatcher(final String partitioning, final TextTokenScanner scanner) {
		super(LTX_BRACKETS, partitioning, CONTENT_TYPES, scanner);
	}
	
	public LtxBracketPairMatcher(final LtxHeuristicTokenScanner scanner) {
		super(scanner.getDefaultBrackets(), scanner.getDocumentPartitioning(), CONTENT_TYPES, scanner);
	}
	
	
}
