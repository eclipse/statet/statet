/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.impl.AbstractAstNode;


@NonNullByDefault
public abstract class TexAstNode extends AbstractAstNode
		implements AstNode {
	
	
	protected static final TexAstNode[] NO_CHILDREN= new TexAstNode[0];
	
	
	@Nullable TexAstNode texParent;
	
	
	TexAstNode(final int statusCode, final @Nullable TexAstNode parent) {
		super(statusCode);
		this.texParent= parent;
	}
	
	TexAstNode(final @Nullable TexAstNode parent) {
		super();
		this.texParent= parent;
	}
	
	TexAstNode() {
		super();
	}
	
	
	public abstract NodeType getNodeType();
	
	
	@Override
	public @Nullable AstNode getParent() {
		return this.texParent;
	}
	
	public final @Nullable TexAstNode getTexParent() {
		return this.texParent;
	}
	
	@Override
	public abstract boolean hasChildren();
	@Override
	public abstract int getChildCount();
	@Override
	public abstract TexAstNode getChild(final int index);
	@Override
	public abstract int getChildIndex(AstNode child);
	
	
	public abstract void acceptInTex(final TexAstVisitor visitor) throws InvocationTargetException;
	
	public abstract void acceptInTexChildren(final TexAstVisitor visitor) throws InvocationTargetException;
	
	
	final void setStatus(final int statusCode) {
		doSetStatusCode(statusCode);
	}
	
	final void setStartOffset(final int offset) {
		doSetStartOffset(offset);
	}
	
	final void setEndOffset(final int offset) {
		doSetEndOffset(offset);
	}
	
	final void setStartEndOffset(final int startOffset, final int endOffset) {
		doSetStartEndOffset(startOffset, endOffset);
	}
	
	final void setStartEndOffset(final int offset) {
		doSetStartEndOffset(offset);
	}
	
}
