/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.refactoring;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.ITypedRegion;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.ast.Environment;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.ast.TexAsts;
import org.eclipse.statet.docmlet.tex.core.model.TexElement;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceElement;
import org.eclipse.statet.docmlet.tex.core.source.doc.TexDocumentConstants;
import org.eclipse.statet.docmlet.tex.core.source.util.LtxHeuristicTokenScanner;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination.Position;
import org.eclipse.statet.ltk.text.core.HeuristicTokenScanner;


public class LtxRefactoringAdapter extends RefactoringAdapter {
	
	
	public LtxRefactoringAdapter() {
		super(TexModel.LTX_TYPE_ID);
	}
	
	
	@Override
	public String getPluginIdentifier() {
		return TexCore.BUNDLE_ID;
	}
	
	@Override
	public LtxHeuristicTokenScanner getScanner(final SourceUnit su) {
		return LtxHeuristicTokenScanner.create(su.getDocumentContentInfo());
	}
	
	@Override
	public boolean canInsert(final ElementSet elements, final SourceElement to,
			final RefactoringDestination.Position pos) {
		if (super.canInsert(elements, to, pos)) {
			if ((to.getElementType() & LtkModelElement.MASK_C12) == TexElement.C12_PREAMBLE) {
				for (final var element : elements.getModelElements()) {
					if ((element.getElementType() & LtkModelElement.MASK_C12) == TexElement.C12_SECTIONING) {
						return false;
					}
				}
			}
			if ((to.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_EMBEDDED
					&& pos == RefactoringDestination.Position.INTO ) {
				return false;
			}
			return true;
		}
		return false;
	}
	
	@Override
	public boolean isCommentContent(final ITypedRegion partition) {
		return (partition != null
				&& (partition.getType() == TexDocumentConstants.LTX_COMMENT_CONTENT_TYPE
						|| partition.getType() == TexDocumentConstants.LTX_MATHCOMMENT_CONTENT_TYPE ));
	}
	
	@Override
	protected int getInsertionOffset(final AbstractDocument document,
			final SourceElement element, final Position pos,
			final HeuristicTokenScanner scanner)
			throws BadLocationException, BadPartitioningException {
		if ((element.getElementType() & LtkModelElement.MASK_C12) == TexSourceElement.C12_SOURCE_FILE
				&& pos == Position.INTO) {
			final TexAstNode astNode= element.getAdapter(TexAstNode.class);
			if (astNode != null) {
				final Environment environment= TexAsts.getDocumentNode(astNode);
				if (environment != null && environment.getEndNode() != null) {
					return environment.getEndNode().getStartOffset();
				}
			}
		}
		return super.getInsertionOffset(document, element, pos, scanner);
	}
	
}
