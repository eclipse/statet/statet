/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.ast;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.AstVisitor;


/**
 * \begin{name} ... \end{name}
 */
@NonNullByDefault
public abstract class Environment extends ContainerNode {
	
	
	static final class MathLatexShorthand extends Environment{
		
		
		MathLatexShorthand(final TexAstNode parent, final ControlNode beginNode) {
			super(parent, beginNode);
		}
		
		
		@Override
		public String getText() {
			return this.beginNode.getText();
		}
		
		@Override
		void setMissingEnd() {
			doSetStatusCode(TexAstStatusConstants.TYPE123_MATH_NOT_CLOSED);
			this.endNode= new Dummy(this, getChild(this.children.length).getEndOffset());
			doSetEndOffset(this.endNode.getEndOffset());
		}
		
	}
	
	static final class Word extends Environment {
		
		Word(final TexAstNode parent, final ControlNode beginNode) {
			super(parent, beginNode);
		}
		
		
		@Override
		public @Nullable String getText() {
			if (this.beginNode.hasChildren()) {
				return this.beginNode.getChild(0).getChild(0).getText();
			}
			return null;
		}
		
		@Override
		void setMissingEnd() {
			doSetStatusCode(TexAstStatusConstants.TYPE123_ENV_NOT_CLOSED);
			this.endNode= new Dummy(this, getChild(this.children.length).getEndOffset());
			doSetEndOffset(this.endNode.getEndOffset());
		}
		
	}
	
	
	final ControlNode beginNode;
	TexAstNode endNode;
	
	
	Environment(final TexAstNode parent, final ControlNode beginNode) {
		super(parent);
		this.beginNode= beginNode;
		doSetStartOffset(beginNode.getStartOffset());
	}
	
	
	@Override
	public NodeType getNodeType() {
		return NodeType.ENVIRONMENT;
	}
	
	
	@Override
	public boolean hasChildren() {
		return true;
	}
	
	@Override
	public int getChildCount() {
		return this.children.length + 2;
	}
	
	@Override
	public TexAstNode getChild(final int index) {
		if (index == 0) {
			return this.beginNode;
		}
		if (index <= this.children.length) {
			return this.children[index - 1];
		}
		if (index == this.children.length + 1) {
			return this.endNode;
		}
		throw new IndexOutOfBoundsException();
	}
	
	@Override
	public int getChildIndex(final AstNode child) {
		if (this.beginNode == child) {
			return 0;
		}
		for (int i= 0; i < this.children.length; i++) {
			if (this.children[i] == child) {
				return i + 1;
			}
		}
		if (this.endNode == child) {
			return this.children.length + 1;
		}
		return -1;
	}
	
	public ControlNode getBeginNode() {
		return this.beginNode;
	}
	
	public TexAstNode getEndNode() {
		return this.endNode;
	}
	
	
	@Override
	public void acceptInChildren(final AstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this.beginNode);
		for (final TexAstNode child : this.children) {
			visitor.visit(child);
		}
		visitor.visit(this.endNode);
	}
	
	@Override
	public void acceptInTex(final TexAstVisitor visitor) throws InvocationTargetException {
		visitor.visit(this);
	}
	
	@Override
	public void acceptInTexChildren(final TexAstVisitor visitor) throws InvocationTargetException {
		this.beginNode.acceptInTex(visitor);
		for (final TexAstNode child : this.children) {
			child.acceptInTex(visitor);
		}
		this.endNode.acceptInTex(visitor);
	}
	
	
	@Override
	void setEndNode(final int endOffset, final TexAstNode endNode) {
		this.endNode= endNode;
		doSetEndOffset(endNode.getEndOffset());
	}
	
}
