/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.tex.core.commands;

import static org.eclipse.statet.docmlet.tex.core.commands.LtxCommandDefinitions.add;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.core.Preference.StringSetPref;
import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;

import org.eclipse.statet.docmlet.tex.core.TexCore;


@NonNullByDefault
public class TexCommandSet {
	
	
	public static final String LTX_TEXT_CONTEXT_ID= "ltx:text"; //$NON-NLS-1$
	public static final String LTX_PREAMBLE_CONTEXT_ID= "ltx:preamble"; //$NON-NLS-1$
	public static final String LTX_MATH_CONTEXT_ID= "ltx:math"; //$NON-NLS-1$
	
	
	public static final String QUALIFIER= TexCore.BUNDLE_ID + "/tex.commands"; //$NON-NLS-1$
	
	public static final Preference<Set<String>> MASTER_COMMANDS_INCLUDE_PREF= new StringSetPref(
			QUALIFIER, "LtxCommands.CommandsMaster.include"); //$NON-NLS-1$
	public static final Preference<Set<String>> PREAMBLE_INCLUDE_PREF= new StringSetPref(
			QUALIFIER, "LtxCommands.PreambleCommands.include"); //$NON-NLS-1$
	public static final Preference<Set<String>> TEXT_COMMANDS_INCLUDE_PREF= new StringSetPref(
			QUALIFIER, "LtxCommands.TextCommands.include"); //$NON-NLS-1$
	public static final Preference<Set<String>> MATH_COMMANDS_INCLUDE_PREF= new StringSetPref(
			QUALIFIER, "LtxCommands.MathCommands.include"); //$NON-NLS-1$
	
	public static final Preference<Set<String>> TEXT_ENVS_INCLUDE_PREF= new StringSetPref(
			QUALIFIER, "LtxCommands.TextEnvs.include"); //$NON-NLS-1$
	public static final Preference<Set<String>> MATH_ENVS_INCLUDE_PREF= new StringSetPref(
			QUALIFIER, "LtxCommands.MathEnvs.include"); //$NON-NLS-1$
	
	private static final Map<String, TexCommand> LTX_INTERN_ENVS= new HashMap<>();
	
	static {
		add(LTX_INTERN_ENVS, EnvDefinitions.ENV_Sinput_BEGIN);
		add(LTX_INTERN_ENVS, EnvDefinitions.ENV_Souput_BEGIN);
	}
	
	private static ImList<TexCommand> toListASorted(final Collection<TexCommand> commands) {
		final var array= commands.toArray(new @NonNull TexCommand[commands.size()]);
		return ImCollections.newList(array, (Comparator<TexCommand>)null);
	}
	
	
	private static final class ContextSet {
		
		private final String contextId;
		
		private final @Nullable Map<String, TexCommand> commandMap;
		private final @Nullable ImList<TexCommand> commandListASorted;
		
		private final @Nullable Map<String, TexCommand> envMap;
		private final @Nullable ImList<TexCommand> envListASorted;
		
		public ContextSet(final String contextId,
				final @Nullable Map<String, TexCommand> commandMap, final @Nullable List<TexCommand> commandListASorted,
				final @Nullable Map<String, TexCommand> envMap, final @Nullable List<TexCommand> envListASorted) {
			this.contextId= contextId;
			if (commandMap != null) {
				this.commandMap= commandMap;
				this.commandListASorted= (commandListASorted != null) ?
						ImCollections.toList(commandListASorted) : toListASorted(commandMap.values());
			}
			else {
				this.commandMap= null;
				this.commandListASorted= null;
			}
			if (envMap != null) {
				this.envMap= envMap;
				this.envListASorted= (envListASorted != null) ?
						ImCollections.toList(envListASorted) : toListASorted(envMap.values());
			}
			else {
				this.envMap= null;
				this.envListASorted= null;
			}
		}
		
		public ContextSet(final String contextId,
				final @Nullable List<TexCommand> commandDefs, final @Nullable Set<String> commandFilter,
				final @Nullable List<TexCommand> envDefs, final @Nullable Set<String> envFilter) {
			this.contextId= contextId;
			if (commandDefs != null) {
				final List<TexCommand> list= new ArrayList<>(commandFilter.size() + 16);
				final HashMap<String, TexCommand> map= new HashMap<>(commandFilter.size());
				for (final TexCommand command : commandDefs) {
					if (commandFilter.contains(command.getControlWord())) {
						map.put(command.getControlWord(), command);
						list.add(command);
					}
				}
				this.commandMap= map;
				this.commandListASorted= toListASorted(list);
			}
			else {
				this.commandMap= null;
				this.commandListASorted= null;
			}
			if (envDefs != null) {
				final List<TexCommand> list= new ArrayList<>(envFilter.size() + 16);
				final HashMap<String, TexCommand> map= new HashMap<>(envFilter.size());
				for (final TexCommand command : envDefs) {
					if (envFilter.contains(command.getControlWord())) {
						map.put(command.getControlWord(), command);
						list.add(command);
					}
				}
				this.envMap= map;
				this.envListASorted= toListASorted(list);
			}
			else {
				this.envMap= null;
				this.envListASorted= null;
			}
		}
		
	}
	
	
	private final List<TexCommand> allCommands;
	
	private final List<TexCommand> allEnvs;
	
	private final ContextSet ltxText;
	private final ContextSet ltxPreamble;
	private final ContextSet ltxMath;
	
	private final Map<String, TexCommand> internEnvMap;
	
	
	public TexCommandSet(final PreferenceAccess prefs) {
		final ImList<TexCommand> sortedCommands;
		final ImList<TexCommand> sortedEnvs;
		{	final Set<String> master= prefs.getPreferenceValue(MASTER_COMMANDS_INCLUDE_PREF);
			final List<TexCommand> filteredCommands= new ArrayList<>(master.size());
			final List<TexCommand> commonCommands= LtxCommandDefinitions.getAllCommands(); // defined order
			for (int i= 0; i < commonCommands.size(); i++) {
				final TexCommand command= commonCommands.get(i);
				if (master.contains(command.getControlWord())) {
					filteredCommands.add(command);
				}
			}
			this.allCommands= ImCollections.toList(filteredCommands);
			sortedCommands= toListASorted(this.allCommands);
		}
		{	final List<TexCommand> commonEnvs= LtxCommandDefinitions.getAllEnvs();
			this.allEnvs= ImCollections.toList(commonEnvs);
			sortedEnvs= toListASorted(this.allEnvs);
		}
		
		this.ltxText= new ContextSet(LTX_TEXT_CONTEXT_ID,
				sortedCommands, prefs.getPreferenceValue(TEXT_COMMANDS_INCLUDE_PREF),
				sortedEnvs, prefs.getPreferenceValue(TEXT_ENVS_INCLUDE_PREF) );
		this.ltxPreamble= new ContextSet(LTX_PREAMBLE_CONTEXT_ID,
				sortedCommands, prefs.getPreferenceValue(PREAMBLE_INCLUDE_PREF),
				null, null );
		this.ltxMath= new ContextSet(LTX_MATH_CONTEXT_ID,
				sortedCommands, prefs.getPreferenceValue(MATH_COMMANDS_INCLUDE_PREF),
				sortedEnvs, prefs.getPreferenceValue(MATH_ENVS_INCLUDE_PREF) );
		
		this.internEnvMap= LTX_INTERN_ENVS;
	}
	
	public TexCommandSet(final List<TexCommand> allCommands, final List<TexCommand> allEnvs,
			final Map<String, TexCommand> textCommandMap, final List<TexCommand> textCommandList,
			final Map<String, TexCommand> textEnvMap, final List<TexCommand> textEnvList,
			final Map<String, TexCommand> preambleCommandMap, final List<TexCommand> preambleCommandList,
			final Map<String, TexCommand> mathCommandMap, final List<TexCommand> mathCommandList,
			final Map<String, TexCommand> mathEnvMap, final List<TexCommand> mathEnvList,
			final Map<String, TexCommand> internEnvMap) {
		this.allCommands= allCommands;
		this.allEnvs= allEnvs;
		this.ltxText= new ContextSet(LTX_TEXT_CONTEXT_ID,
				textCommandMap, textCommandList, textEnvMap, textEnvList );
		this.ltxPreamble= new ContextSet(LTX_PREAMBLE_CONTEXT_ID,
				preambleCommandMap, preambleCommandList, null, null );
		this.ltxMath= new ContextSet(LTX_MATH_CONTEXT_ID,
				mathCommandMap, mathCommandList, mathEnvMap, mathEnvList );
		this.internEnvMap= internEnvMap;
	}
	
	
	public List<TexCommand> getAllLtxCommands() {
		return this.allCommands;
	}
	
	public List<TexCommand> getAllLtxEnvs() {
		return this.allEnvs;
	}
	
	
	@SuppressWarnings("null")
	public Map<String, TexCommand> getLtxTextCommandMap() {
		return this.ltxText.commandMap;
	}
	
	@SuppressWarnings("null")
	public List<TexCommand> getLtxTextCommandsASorted() {
		return this.ltxText.commandListASorted;
	}
	
	@SuppressWarnings("null")
	public Map<String, TexCommand> getLtxTextEnvMap() {
		return this.ltxText.envMap;
	}
	
	@SuppressWarnings("null")
	public List<TexCommand> getLtxTextEnvsASorted() {
		return this.ltxText.envListASorted;
	}
	
	
	@SuppressWarnings("null")
	public Map<String, TexCommand> getLtxPreambleCommandMap() {
		return this.ltxPreamble.commandMap;
	}
	
	@SuppressWarnings("null")
	public List<TexCommand> getLtxPreambleCommandsASorted() {
		return this.ltxPreamble.commandListASorted;
	}
	
	
	@SuppressWarnings("null")
	public Map<String, TexCommand> getLtxMathCommandMap() {
		return this.ltxMath.commandMap;
	}
	
	@SuppressWarnings("null")
	public List<TexCommand> getLtxMathCommandsASorted() {
		return this.ltxMath.commandListASorted;
	}
	
	@SuppressWarnings("null")
	public Map<String, TexCommand> getLtxMathEnvMap() {
		return this.ltxMath.envMap;
	}
	
	@SuppressWarnings("null")
	public List<TexCommand> getLtxMathEnvsASorted() {
		return this.ltxMath.envListASorted;
	}
	
	
	private @Nullable ContextSet getContextSet(final String contextId) {
		if (contextId == LTX_TEXT_CONTEXT_ID) {
			return this.ltxText;
		}
		if (contextId == LTX_PREAMBLE_CONTEXT_ID) {
			return this.ltxPreamble;
		}
		if (contextId == LTX_MATH_CONTEXT_ID) {
			return this.ltxMath;
		}
		return null;
	}
	
	public @Nullable List<TexCommand> getCommandsASorted(final String contextId) {
		final ContextSet contextSet= getContextSet(contextId);
		if (contextSet != null) {
			return contextSet.commandListASorted;
		}
		return null;
	}
	
	public @Nullable List<TexCommand> getEnvsASorted(final String contextId) {
		final ContextSet contextSet= getContextSet(contextId);
		if (contextSet != null) {
			return contextSet.commandListASorted;
		}
		return null;
	}
	
	
	public Map<String, TexCommand> getLtxInternEnvMap() {
		return this.internEnvMap;
	}
	
}
