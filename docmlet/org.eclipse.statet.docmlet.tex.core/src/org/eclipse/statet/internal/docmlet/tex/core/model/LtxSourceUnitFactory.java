/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import org.eclipse.core.resources.IFile;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.model.LtxSourceResourceSourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.impl.AbstractFilePersistenceSourceUnitFactory;


/**
 * Factory for common LaTeX files
 */
@NonNullByDefault
public class LtxSourceUnitFactory extends AbstractFilePersistenceSourceUnitFactory {
	
	
	public LtxSourceUnitFactory() {
	}
	
	
	@Override
	protected SourceUnit createSourceUnit(final String id, final IFile file) {
		return new LtxSourceResourceSourceUnit(id, file);
	}
	
}
