/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.builder;

import org.eclipse.core.runtime.content.IContentType;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.TexBuildParticipant;
import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.model.TexWorkspaceSourceUnit;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitModelContainer;
import org.eclipse.statet.docmlet.tex.core.project.TexProject;
import org.eclipse.statet.docmlet.tex.core.project.TexProjects;
import org.eclipse.statet.internal.docmlet.tex.core.TexCorePlugin;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.ltk.project.core.builder.ProjectTaskBuilder;


@NonNullByDefault
public final class TexProjectBuilder extends ProjectTaskBuilder<TexProject, TexWorkspaceSourceUnit, TexBuildParticipant> {
	
	
	public static final String BUILDER_ID= "org.eclipse.statet.docmlet.resourceProjects.TexBuilder"; //$NON-NLS-1$
	
	private static final BuilderDefinition<TexProject, TexWorkspaceSourceUnit, TexBuildParticipant> DEFINITION=
			new BuilderDefinition<>(TexCore.BUNDLE_ID, TexCorePlugin.getInstance(),
					TexProjects.TEX_NATURE_ID, "TeX", //$NON-NLS-1$
					TexWorkspaceSourceUnit.class,
					TexBuildParticipant.class ) {
				public int checkSourceUnitContent(final IContentType contentType) {
					if (contentType.isKindOf(TexCore.LTX_CONTENT_TYPE)) {
						return 0;
					}
					return -1;
				}
			};
	
	
	public TexProjectBuilder() {
	}
	
	
	@Override
	public BuilderDefinition<TexProject, TexWorkspaceSourceUnit, TexBuildParticipant> getBuilderDefinition() {
		return DEFINITION;
	}
	
	@Override
	public ImList<IssueTypeSet> getIssueTypeSets() {
		return ImCollections.newList(LtxSourceUnitModelContainer.ISSUE_TYPE_SET);
	}
	
	
	@Override
	protected TexBuildTask createBuildTask() {
		return new TexBuildTask(this);
	}
	
	@Override
	protected TexCleanTask createCleanTask() {
		return new TexCleanTask(this);
	}
	
}
