/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;

import org.eclipse.core.runtime.OperationCanceledException;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.model.LtxModelProblemConstants;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.issues.core.impl.AbstractSourceIssueReporter;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;


@NonNullByDefault
public class LtxModelInspector extends AbstractSourceIssueReporter {
	
	
	private final int levelRefUndefined= Problem.SEVERITY_WARNING;
	
	
	public LtxModelInspector() {
		super(TexModel.LTX_TYPE_ID);
	}
	
	
	public void run(final LtxSourceUnitModelInfo modelInfo,
			final SourceContent content,
			final IssueRequestor requestor) {
		try {
			init(content, requestor);
			
			checkLabels(modelInfo);
			
			flush();
		}
		catch (final OperationCanceledException e) {}
		finally {
			clear();
		}
	}
	
	
	private void checkLabels(final LtxSourceUnitModelInfo model) {
		final NameAccessSet<TexNameAccess> labelSet= model.getLabels();
		final List<String> labels= labelSet.getNames();
		ITER_LABELS: for (final String label : labels) {
			if (label != null && label.length() > 0) {
				final ImList<TexNameAccess> accessList= nonNullAssert(labelSet.getAllInUnit(label));
				for (final TexNameAccess access : accessList) {
					if (access.isWriteAccess()) {
						continue ITER_LABELS;
					}
				}
				for (final TexNameAccess access : accessList) {
					final TexAstNode nameNode= access.getNameNode();
					addProblem(this.levelRefUndefined, LtxModelProblemConstants.STATUS12_LABEL_UNDEFINED,
							getMessageBuilder().bind(ProblemMessages.Labels_UndefinedRef_message,
									access.getDisplayName() ),
							nameNode.getStartOffset(), nameNode.getEndOffset() );
				}
			}
		}
	}
	
}
