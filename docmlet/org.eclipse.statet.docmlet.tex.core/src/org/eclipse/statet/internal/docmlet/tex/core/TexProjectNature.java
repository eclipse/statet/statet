/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.resources.core.AbstractProjectNature;
import org.eclipse.statet.ecommons.resources.core.ProjectUtils;

import org.eclipse.statet.docmlet.tex.core.TexCodeStyleSettings;
import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommandSet;
import org.eclipse.statet.docmlet.tex.core.project.TexProject;
import org.eclipse.statet.docmlet.tex.core.project.TexProjects;
import org.eclipse.statet.internal.docmlet.tex.core.builder.TexProjectBuilder;


@NonNullByDefault
public class TexProjectNature extends AbstractProjectNature implements TexProject {
	
	
	public static @Nullable TexProjectNature getTexProject(final @Nullable IProject project) {
		try {
			return (project != null) ? (TexProjectNature) project.getNature(TexProjects.TEX_NATURE_ID) : null;
		}
		catch (final CoreException e) {
			TexCorePlugin.log(e.getStatus());
			return null;
		}
	}
	
	
	public TexProjectNature() {
	}
	
	
	@Override
	public void addBuilders() throws CoreException {
		final IProject project= getProject();
		final IProjectDescription description= project.getDescription();
		boolean changed= false;
		changed|= ProjectUtils.addBuilder(description, TexProjectBuilder.BUILDER_ID);
		
		if (changed) {
			project.setDescription(description, null);
		}
	}
	
	@Override
	public void removeBuilders() throws CoreException {
		final IProject project= getProject();
		final IProjectDescription description= project.getDescription();
		boolean changed= false;
		changed|= ProjectUtils.removeBuilder(description, TexProjectBuilder.BUILDER_ID);
		
		if (changed) {
			project.setDescription(description, null);
		}
	}
	
	
	@Override
	public TexCommandSet getTexCommandSet() {
		return TexCore.getWorkbenchAccess().getTexCommandSet();
	}
	
	@Override
	public TexCodeStyleSettings getTexCodeStyle() {
		return TexCore.getWorkbenchAccess().getTexCodeStyle();
	}
	
}
