/*=============================================================================#
 # Copyright (c) 2005, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.builder;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.tex.core.ast.Comment;
import org.eclipse.statet.docmlet.tex.core.ast.SourceComponent;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.impl.TaskTagReporter;


@NonNullByDefault
public class TexTaskTagReporter extends TaskTagReporter {
	
	
	public TexTaskTagReporter() {
	}
	
	
	public void run(final TexSourceUnit sourceUnit, final TexAstNode node,
			final SourceContent sourceContent,
			final IssueRequestor requestor) {
		if (node instanceof SourceComponent) {
			setup(sourceContent, requestor);
			final var comments= nonNullAssert(((SourceComponent)node).getComments());
			for (final Comment comment : comments) {
				checkForTasks(comment.getStartOffset() + 1, comment.getEndOffset());
			}
		}
	}
	
}
