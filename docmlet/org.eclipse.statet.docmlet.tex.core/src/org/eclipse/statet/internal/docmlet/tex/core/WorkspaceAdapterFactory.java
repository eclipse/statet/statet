/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdapterFactory;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.project.TexProjects;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;


@NonNullByDefault
public class WorkspaceAdapterFactory implements IAdapterFactory {
	
	
	private static final @NonNull Class<?>[] ADAPTERS= new @NonNull Class<?>[] {
		TexCoreAccess.class,
	};
	
	
	public static TexCoreAccess getResourceCoreAccess(final IResource resource) {
		final var texProject= TexProjects.getTexProject(resource.getProject());
		if (texProject != null) {
			return texProject;
		}
		return TexCore.getWorkbenchAccess();
	}
	
	public static @Nullable TexCoreAccess getResourceCoreAccess(final @Nullable Object resource) {
		if (resource instanceof IResource) {
			return getResourceCoreAccess((IResource)resource);
		}
		return null;
	}
	
	
	public WorkspaceAdapterFactory() {
	}
	
	
	@Override
	public @NonNull Class<?>[] getAdapterList() {
		return ADAPTERS;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Object adaptableObject, final Class<T> adapterType) {
		if (adapterType == TexCoreAccess.class) {
			if (adaptableObject instanceof WorkspaceSourceUnit) {
				return (T)getResourceCoreAccess(((WorkspaceSourceUnit)adaptableObject).getResource());
			}
			else if (adaptableObject instanceof IResource) {
				return (T)getResourceCoreAccess((IResource)adaptableObject);
			}
			return (T)TexCore.getWorkbenchAccess();
		}
		return null;
	}
	
}
