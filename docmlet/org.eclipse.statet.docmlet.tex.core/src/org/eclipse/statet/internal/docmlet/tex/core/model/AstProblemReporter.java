/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_ENV_MISSING_NAME;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_ENV_NOT_CLOSED;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_ENV_NOT_OPENED;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_GROUP_NOT_CLOSED;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_GROUP_NOT_OPENED;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_MATH_NOT_CLOSED;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_VERBATIM_INLINE_C_MISSING;
import static org.eclipse.statet.docmlet.tex.core.ast.TexAstStatusConstants.TYPE123_VERBATIM_INLINE_NOT_CLOSED;
import static org.eclipse.statet.ltk.core.StatusCodes.TYPE123;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.ast.Comment;
import org.eclipse.statet.docmlet.tex.core.ast.ControlNode;
import org.eclipse.statet.docmlet.tex.core.ast.Dummy;
import org.eclipse.statet.docmlet.tex.core.ast.Embedded;
import org.eclipse.statet.docmlet.tex.core.ast.Environment;
import org.eclipse.statet.docmlet.tex.core.ast.Group;
import org.eclipse.statet.docmlet.tex.core.ast.Label;
import org.eclipse.statet.docmlet.tex.core.ast.Math;
import org.eclipse.statet.docmlet.tex.core.ast.SourceComponent;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstVisitor;
import org.eclipse.statet.docmlet.tex.core.ast.Text;
import org.eclipse.statet.docmlet.tex.core.ast.Verbatim;
import org.eclipse.statet.docmlet.tex.core.model.TexModel;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.ltk.ast.core.util.AbstractAstProblemReporter;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.core.source.StatusDetail;
import org.eclipse.statet.ltk.issues.core.IssueRequestor;
import org.eclipse.statet.ltk.issues.core.Problem;


@NonNullByDefault
public class AstProblemReporter extends AbstractAstProblemReporter {
	
	
	private static final int ENV_LABEL_LIMIT= 20;
	
	
	private final Visitor visitor= new Visitor();
	
	
	public AstProblemReporter() {
		super(TexModel.LTX_TYPE_ID);
	}
	
	
	public void run(final TexSourceUnit sourceUnit, final TexAstNode node,
			final SourceContent content,
			final IssueRequestor requestor) {
		try {
			init(content, requestor);
			
			node.acceptInTex(this.visitor);
			
			flush();
		}
		catch (final InvocationTargetException e) {}
		finally {
			clear();
		}
	}
	
	
	protected void handleCommonCodes(final TexAstNode node, final int code)
			throws BadLocationException, InvocationTargetException {
		super.handleCommonCodes(node, code);
	}
	
	
	private class Visitor extends TexAstVisitor {
		
		
		@Override
		public void visit(final SourceComponent node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					handleCommonCodes(node, code);
					break STATUS;
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			
			node.acceptInTexChildren(this);
		}
		
		@Override
		public void visit(final Group node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					
					case TYPE123_GROUP_NOT_CLOSED:
						if (node.getParent() instanceof ControlNode) {
							addProblem(Problem.SEVERITY_ERROR, code,
									(node.getText() == "{") ? //$NON-NLS-1$
											ProblemMessages.Ast_ReqArgument_NotClosed_message :
											ProblemMessages.Ast_OptArgument_NotClosed_Opt_message,
									node.getStartOffset(), node.getStartOffset() + 1 );
							break STATUS;
						}
						else {
							addProblem(Problem.SEVERITY_ERROR, code,
									(node.getText() == "{") ? //$NON-NLS-1$
											ProblemMessages.Ast_CurlyBracket_NotClosed_message :
											ProblemMessages.Ast_SquareBracket_NotClosed_message,
									node.getStartOffset(), node.getStartOffset() + 1 );
							break STATUS;
						}
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			
			node.acceptInTexChildren(this);
		}
		
		@Override
		public void visit(final Environment node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					
					case TYPE123_ENV_NOT_CLOSED: {
							final TexAstNode beginNode= node.getBeginNode();
							addProblem(Problem.SEVERITY_ERROR, code,
									getMessageBuilder().bind(ProblemMessages.Ast_Env_NotClosed_message,
											limit(node.getText(), ENV_LABEL_LIMIT) ),
									beginNode.getStartOffset(), beginNode.getEndOffset() );
							break STATUS;
						}
					case TYPE123_MATH_NOT_CLOSED: {
							final TexAstNode beginNode= node.getBeginNode();
							String c= node.getText();
							if (c == "[") { //$NON-NLS-1$
								c= "\\]"; //$NON-NLS-1$
							}
							else if (c == "(") { //$NON-NLS-1$
								c= "\\)"; //$NON-NLS-1$
							}
							else {
								c= null;
							}
							if (c != null) {
								addProblem(Problem.SEVERITY_ERROR, code,
										getMessageBuilder().bind(
												ProblemMessages.Ast_Math_NotClosed_message,
												c ),
										beginNode.getStartOffset(), beginNode.getEndOffset() );
							}
							break STATUS;
						}
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			
			node.acceptInTexChildren(this);
		}
		
		@Override
		public void visit(final ControlNode node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					
					case TYPE123_ENV_MISSING_NAME:
						addProblem(Problem.SEVERITY_ERROR, code,
								(node.getText().equals("begin")) ? //$NON-NLS-1$
										ProblemMessages.Ast_Env_MissingName_Begin_message :
										ProblemMessages.Ast_Env_MissingName_End_message,
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					case TYPE123_ENV_NOT_OPENED:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Ast_Env_NotOpened_message,
										limit(node.getChild(0).getChild(0).getText(), ENV_LABEL_LIMIT) ),
								node.getStartOffset(), node.getEndOffset() );
						break STATUS;
					
					case TYPE123_VERBATIM_INLINE_C_MISSING:
						addProblem(Problem.SEVERITY_ERROR, code,
								ProblemMessages.Ast_Verbatim_MissingSep_message,
								node.getEndOffset() - 1, node.getEndOffset() );
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			
			node.acceptInTexChildren(this);
		}
		
		@Override
		public void visit(final Text node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					handleCommonCodes(node, code);
					break STATUS;
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Label node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					handleCommonCodes(node, code);
					break STATUS;
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Math node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					
					case TYPE123_MATH_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Ast_Math_NotClosed_message,
										node.getText() ),
								node.getStartOffset(), node.getStartOffset() + node.getText().length() );
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			
			node.acceptInTexChildren(this);
		}
		
		@Override
		public void visit(final Verbatim node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					
					case TYPE123_VERBATIM_INLINE_NOT_CLOSED:
						addProblem(Problem.SEVERITY_ERROR, code,
								getMessageBuilder().bind(
										ProblemMessages.Ast_Verbatim_NotClosed_message,
										StatusDetail.get(node).getText() ),
								node.getEndOffset() - 1, node.getEndOffset() );
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
		@Override
		public void visit(final Comment node) throws InvocationTargetException {
		}
		
		@Override
		public void visit(final Dummy node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					switch (code & TYPE123) {
					
					case TYPE123_GROUP_NOT_OPENED:
						addProblem(Problem.SEVERITY_ERROR, code,
								(node.getText() == "{") ? //$NON-NLS-1$
										ProblemMessages.Ast_CurlyBracket_NotOpened_message :
										ProblemMessages.Ast_SquareBracket_NotOpened_message,
								node.getStartOffset(), node.getStartOffset() + 1 );
						break STATUS;
					
					default:
						handleCommonCodes(node, code);
						break STATUS;
					}
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
			
			node.acceptInTexChildren(this);
		}
		
		@Override
		public void visit(final Embedded node) throws InvocationTargetException {
			final int code= (node.getStatusCode() & MASK);
			if (requiredCheck(code)) {
				STATUS: try {
					handleCommonCodes(node, code);
					break STATUS;
				}
				catch (final BadLocationException e) {
					throw new InvocationTargetException(e);
				}
			}
		}
		
	}
	
	
	protected static final @Nullable String limit(final @Nullable String label, final int limit) {
		if (label != null && label.length() > limit) {
			return label.substring(0, limit) + '…';
		}
		return label;
	}
	
}
