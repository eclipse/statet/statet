/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import java.util.List;
import java.util.Objects;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.tex.core.ast.Embedded;
import org.eclipse.statet.docmlet.tex.core.model.TexElementName;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceElement;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.model.core.element.EmbeddingForeignSrcStrElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElementFilter;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


@NonNullByDefault
public class EmbeddingForeignSourceElement extends BasicLtxSourceElement
		implements EmbeddingForeignSrcStrElement<TexSourceElement, SourceStructElement<?, ?>> {
	
	
	private final ContainerSourceElement parent;
	private final String externType;
	private @Nullable SourceStructElement<?, ?> foreign;
	
	private final Embedded astNode;
	
	
	protected EmbeddingForeignSourceElement(final String externType, final ContainerSourceElement parent,
			final Embedded astNode) {
		super(LtkModelElement.C1_EMBEDDED);
		this.externType= externType;
		this.parent= parent;
		
		this.astNode= astNode;
	}
	
	
	@Override
	public String getId() {
		final String name= getElementName().getDisplayName();
		final StringBuilder sb= new StringBuilder(name.length() + 32);
		sb.append(Integer.toHexString(getElementType() & MASK_C12));
		sb.append("<noweb:");
		sb.append(this.externType);
		sb.append(">:");
		sb.append(name);
		sb.append('#');
		sb.append(this.occurrenceCount);
		return sb.toString();
	}
	
	@Override
	public ElementName getElementName() {
		final SourceStructElement<?, ?> foreign= this.foreign;
		return (foreign != null) ? foreign.getElementName() : TexElementName.create(0, "");
	}
	
	@Override
	public @Nullable TextRegion getNameSourceRange() {
		final SourceStructElement<?, ?> foreign= this.foreign;
		return (foreign != null) ? foreign.getNameSourceRange() : null;
	}
	
	@Override
	public SourceUnit getSourceUnit() {
		return this.parent.getSourceUnit();
	}
	
	@Override
	public boolean exists() {
		return this.parent.exists();
	}
	
	@Override
	public boolean isReadOnly() {
		return this.parent.isReadOnly();
	}
	
	@Override
	public TexSourceElement getModelParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasModelChildren(final @Nullable LtkModelElementFilter<? super TexSourceElement> filter) {
		return false;
	}
	
	@Override
	public List<? extends TexSourceElement> getModelChildren(final @Nullable LtkModelElementFilter<? super TexSourceElement> filter) {
		return NO_CHILDREN;
	}
	
	@Override
	public @Nullable SourceStructElement<?, ?> getForeignElement() {
		return this.foreign;
	}
	
	@Override
	public SourceStructElement<?, ?> getSourceParent() {
		return this.parent;
	}
	
	@Override
	public boolean hasSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		final SourceStructElement<?, ?> foreign= this.foreign;
		return (foreign != null && (filter == null || filter.include(foreign)));
	}
	
	@Override
	public List<? extends SourceStructElement<?, ?>> getSourceChildren(final @Nullable LtkModelElementFilter<? super SourceStructElement<?, ?>> filter) {
		final SourceStructElement<?, ?> foreign= this.foreign;
		return (foreign != null && (filter == null || filter.include(foreign))) ?
				ImCollections.newList(foreign) : NO_CHILDREN;
	}
	
	public void setForeign(final SourceStructElement<?, ?> foreign) {
		this.foreign= foreign;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		if (adapterType == AstNode.class) {
			return (T) this.astNode;
		}
		{	final @Nullable T adapter= super.getAdapter(adapterType);
			if (adapter != null) {
				return adapter;
			}
		}
		final SourceStructElement<?, ?> foreign= this.foreign;
		if (foreign != null) {
			return foreign.getAdapter(adapterType);
		}
		return null;
	}
	
	
	@Override
	public int hashCode() {
		int h= (LtkModelElement.C1_EMBEDDED & MASK_C12) * this.externType.hashCode() + this.occurrenceCount;
		final SourceStructElement<?, ?> foreign= this.foreign;
		if (foreign != null) {
			h += foreign.hashCode() * 23917;
		}
		return h;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (this == obj
				|| (obj instanceof final EmbeddingForeignSourceElement other
						&& getSourceParent().equals(other.getSourceParent())
						&& Objects.equals(this.foreign, other.foreign) ));
	}
	
}
