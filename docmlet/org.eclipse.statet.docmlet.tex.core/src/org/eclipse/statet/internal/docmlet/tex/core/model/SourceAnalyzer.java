/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import static org.eclipse.statet.docmlet.tex.core.model.TexElement.C12_PREAMBLE;
import static org.eclipse.statet.docmlet.tex.core.model.TexElement.C12_SECTIONING;
import static org.eclipse.statet.ltk.model.core.element.LtkModelElement.C1_SOURCE;
import static org.eclipse.statet.ltk.model.core.element.LtkModelElement.MASK_C1;
import static org.eclipse.statet.ltk.model.core.element.LtkModelElement.MASK_C12;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.docmlet.tex.core.ast.ControlNode;
import org.eclipse.statet.docmlet.tex.core.ast.Embedded;
import org.eclipse.statet.docmlet.tex.core.ast.Environment;
import org.eclipse.statet.docmlet.tex.core.ast.NodeType;
import org.eclipse.statet.docmlet.tex.core.ast.SourceComponent;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstInfo;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstVisitor;
import org.eclipse.statet.docmlet.tex.core.ast.TexAsts;
import org.eclipse.statet.docmlet.tex.core.ast.Text;
import org.eclipse.statet.docmlet.tex.core.commands.LtxPrintCommand;
import org.eclipse.statet.docmlet.tex.core.commands.PreambleDefinitions;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.model.TexElement;
import org.eclipse.statet.docmlet.tex.core.model.TexElementName;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceElement;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;
import org.eclipse.statet.ltk.model.core.impl.BasicNameAccessSet;
import org.eclipse.statet.ltk.model.core.impl.NameAccessAccumulator;


public class SourceAnalyzer extends TexAstVisitor {
	
	
	private static final Integer ONE= 1;
	
	
	private String input;
	
	private ContainerSourceElement currentElement;
	
	private final StringBuilder titleBuilder= new StringBuilder();
	private boolean titleDoBuild;
	private ContainerSourceElement titleElement;
	private final Map<String, Integer> structNamesCounter= new HashMap<>();
	
	private Map<String, NameAccessAccumulator<TexNameAccess>> labels= new HashMap<>();
	private final List<EmbeddingReconcileTask> embeddedItems= new ArrayList<>();
	
	private int minSectionLevel;
	private int maxSectionLevel;
	
	
	public void clear() {
		this.input= null;
		this.currentElement= null;
		
		this.titleBuilder.setLength(0);
		this.titleDoBuild= false;
		this.titleElement= null;
		
		if (this.labels == null || !this.labels.isEmpty()) {
			this.labels= new HashMap<>();
		}
		this.embeddedItems.clear();
		
		this.minSectionLevel= Integer.MAX_VALUE;
		this.maxSectionLevel= Integer.MIN_VALUE;
	}
	
	public LtxSourceUnitModelInfoImpl createModel(final TexSourceUnit sourceUnit,
			final String input, final TexAstInfo ast,
			Map<String, TexCommand> customCommands, Map<String, TexCommand> customEnvs) {
		clear();
		this.input= input;
		if (!(ast.getRoot() instanceof TexAstNode)) {
			return null;
		}
		final TexSourceElement root= this.currentElement= new ContainerSourceElement.SourceContainer(
				TexElement.C12_SOURCE_FILE, sourceUnit, ast.getStamp(),
				(TexAstNode)ast.getRoot() );
		try {
			((TexAstNode)ast.getRoot()).acceptInTex(this);
			
			final NameAccessSet<TexNameAccess> labels;
			if (this.labels.isEmpty()) {
				labels= BasicNameAccessSet.emptySet();
			}
			else {
				labels= new BasicNameAccessSet<>(this.labels);
				this.labels= null;
			}
			
			if (this.minSectionLevel == Integer.MAX_VALUE) {
				this.minSectionLevel= 0;
				this.maxSectionLevel= 0;
			}
			
			if (customCommands != null) {
				customCommands= Collections.unmodifiableMap(customCommands);
			}
			else {
				customCommands= Collections.emptyMap();
			}
			if (customEnvs != null) {
				customEnvs= Collections.unmodifiableMap(customEnvs);
			}
			else {
				customEnvs= Collections.emptyMap();
			}
			final LtxSourceUnitModelInfoImpl model= new LtxSourceUnitModelInfoImpl(ast, root,
					this.minSectionLevel, this.maxSectionLevel, labels, customCommands, customEnvs );
			return model;
		}
		catch (final InvocationTargetException e) {
			throw new IllegalStateException();
		}
	}
	
	public List<EmbeddingReconcileTask> getEmbeddedItems() {
		return this.embeddedItems;
	}
	
	
	private void initElement(final ContainerSourceElement element) {
		if (this.currentElement.children.isEmpty()) {
			this.currentElement.children= new ArrayList<>();
		}
		this.currentElement.children.add(element);
		this.currentElement= element;
	}
	
	private void exitContainer(final int stop, final boolean forward) {
		this.currentElement.length= ((forward) ?
						readLinebreakForward((stop >= 0) ? stop : this.currentElement.startOffset + this.currentElement.length, this.input.length()) :
						readLinebreakBackward((stop >= 0) ? stop : this.currentElement.startOffset + this.currentElement.length, 0) ) -
				this.currentElement.startOffset;
		final List<BasicLtxSourceElement> children= this.currentElement.children;
		if (!children.isEmpty()) {
			for (final BasicLtxSourceElement element : children) {
				if ((element.getElementType() & MASK_C12) == C12_SECTIONING) {
					final Map<String, Integer> names= this.structNamesCounter;
					final String name= element.getElementName().getDisplayName();
					final Integer occ= names.get(name);
					if (occ == null) {
						names.put(name, ONE);
					}
					else {
						names.put(name, Integer.valueOf(
								(element.occurrenceCount= occ + 1) ));
					}
				}
			}
			this.structNamesCounter.clear();
		}
		this.currentElement= this.currentElement.getModelParent();
	}
	
	private int readLinebreakForward(int offset, final int limit) {
		if (offset < limit) {
			switch(this.input.charAt(offset)) {
			case '\n':
				if (++offset < limit && this.input.charAt(offset) == '\r') {
					return ++offset;
				}
				return offset;
			case '\r':
				if (++offset < limit && this.input.charAt(offset) == '\n') {
					return ++offset;
				}
				return offset;
			}
		}
		return offset;
	}
	private int readLinebreakBackward(int offset, final int limit) {
		if (offset > limit) {
			switch(this.input.charAt(offset-1)) {
			case '\n':
				if (--offset > limit && this.input.charAt(offset-1) == '\r') {
					return --offset;
				}
				return offset;
			case '\r':
				if (--offset < limit && this.input.charAt(offset-1) == '\n') {
					return --offset;
				}
				return offset;
			}
		}
		return offset;
	}
	
	private void finishTitleText() {
		{	boolean wasWhitespace= false;
			int idx= 0;
			while (idx < this.titleBuilder.length()) {
				if (this.titleBuilder.charAt(idx) == ' ') {
					if (wasWhitespace) {
						this.titleBuilder.deleteCharAt(idx);
					}
					else {
						wasWhitespace= true;
						idx++;
					}
				}
				else {
					wasWhitespace= false;
					idx++;
				}
			}
		}
		this.titleElement.name= TexElementName.create(TexElementName.TITLE, this.titleBuilder.toString());
		this.titleBuilder.setLength(0);
		this.titleElement= null;
		this.titleDoBuild= false;
	}
	
	
	@Override
	public void visit(final SourceComponent node) throws InvocationTargetException {
		this.currentElement.startOffset= node.getStartOffset();
		node.acceptInTexChildren(this);
		if (this.titleElement != null) {
			finishTitleText();
		}
		while ((this.currentElement.getElementType() & MASK_C1) != C1_SOURCE) {
			exitContainer(node.getEndOffset(), true);
		}
		exitContainer(node.getEndOffset(), true);
	}
	
	@Override
	public void visit(final Environment node) throws InvocationTargetException {
		final TexCommand command= node.getBeginNode().getCommand();
		
		if ((command.getType() & TexCommand.MASK_C2) == TexCommand.C2_ENV_DOCUMENT_BEGIN) {
			if (this.titleElement != null) {
				finishTitleText();
			}
			while ((this.currentElement.getElementType() & MASK_C1) != C1_SOURCE) {
				exitContainer(node.getStartOffset(), false);
			}
		}
		
		node.acceptInTexChildren(this);
		
		if ((command.getType() & TexCommand.MASK_C2) == TexCommand.C2_ENV_DOCUMENT_BEGIN) {
			if (this.titleElement != null) {
				finishTitleText();
			}
			while ((this.currentElement.getElementType() & MASK_C1) != C1_SOURCE) {
				exitContainer((node.getEndNode() != null) ?
						node.getEndNode().getStartOffset() : node.getEndOffset(), false );
			}
		}
		
		{	final TexAstNode beginLabel= getLabelNode(node.getBeginNode());
			if (beginLabel != null) {
				final ImList<EnvLabelAccess> accessList;
				final TexAstNode endLabel= getLabelNode(node.getEndNode());
				if (endLabel != null) {
					accessList= ImCollections.newList(
							new EnvLabelAccess(node.getBeginNode(), beginLabel),
							new EnvLabelAccess(node.getEndNode(), endLabel) );
				}
				else {
					accessList= ImCollections.newList(
							new EnvLabelAccess(node.getBeginNode(), endLabel) );
				}
				for (final EnvLabelAccess access : accessList) {
					access.all= accessList;
					access.getNode().addAttachment(access);
				}
			}
		}
	}
	
	@Override
	public void visit(final ControlNode node) throws InvocationTargetException {
		final TexCommand command= node.getCommand();
		COMMAND: if (command != null) {
			switch (command.getType() & TexCommand.MASK_MAIN) {
			case TexCommand.PREAMBLE:
				if (command == PreambleDefinitions.PREAMBLE_documentclass_COMMAND) {
					if (this.titleElement != null) {
						finishTitleText();
					}
					while ((this.currentElement.getElementType() & MASK_C1) != C1_SOURCE) {
						exitContainer(node.getStartOffset(), false);
					}
					initElement(new ContainerSourceElement.StructContainer(
							C12_PREAMBLE, this.currentElement, node ));
					this.currentElement.name= TexElementName.create(TexElementName.TITLE, "Preamble");
				}
				break;
			case TexCommand.SECTIONING:
				if ((this.currentElement.getElementType() & MASK_C12) == C12_PREAMBLE) {
					exitContainer(node.getStartOffset(), false);
				}
				if ((this.currentElement.getElementType() & MASK_C12) == C12_SECTIONING
						|| (this.currentElement.getElementType() & MASK_C1) == C1_SOURCE ) {
					final int level= (command.getType() & 0xf0) >> 4;
					if (level > 5) {
						break COMMAND;
					}
					if (this.titleElement != null) {
						finishTitleText();
						break COMMAND;
					}
					
					while ((this.currentElement.getElementType() & MASK_C12) == C12_SECTIONING
							&& (this.currentElement.getElementType() & 0xf) >= level) {
						exitContainer(node.getStartOffset(), false);
					}
					initElement(new ContainerSourceElement.StructContainer(
							C12_SECTIONING | level, this.currentElement, node ));
					
					this.minSectionLevel= Math.min(this.minSectionLevel, level);
					this.maxSectionLevel= Math.max(this.maxSectionLevel, level);
					
					final int count= node.getChildCount();
					if (count > 0) {
						this.titleElement= this.currentElement;
						this.titleDoBuild= true;
						final TexAstNode titleNode= node.getChild(0);
						this.titleElement.nameRegion= TexAsts.getInnerRegion(titleNode);
						node.getChild(0).acceptInTex(this);
						if (this.titleElement != null) {
							finishTitleText();
						}
						for (int i= 1; i < count; i++) {
							node.getChild(i).acceptInTex(this);
						}
					}
					else {
						this.currentElement.name= TexElementName.create(TexElementName.TITLE, ""); //$NON-NLS-1$
					}
					this.currentElement.length= Math.max(this.currentElement.length, node.getLength());
					return;
				}
				break;
			case TexCommand.LABEL:
				if ((command.getType() & TexCommand.MASK_C2) == TexCommand.C2_LABEL_REFLABEL) {
					final TexAstNode nameNode= getLabelNode(node);
					if (nameNode != null) {
						final String label= nameNode.getText();
						NameAccessAccumulator<TexNameAccess> shared= this.labels.get(label);
						if (shared == null) {
							shared= new NameAccessAccumulator<>(label);
							this.labels.put(label, shared);
						}
						final RefLabelAccess access= new RefLabelAccess(shared, node, nameNode);
						if ((command.getType() & TexCommand.MASK_C3) == TexCommand.C3_LABEL_REFLABEL_DEF) {
							access.flags |= RefLabelAccess.A_WRITE;
						}
						node.addAttachment(access);
					}
					final boolean prevDoBuild= this.titleDoBuild;
					this.titleDoBuild= false;
					node.acceptInTexChildren(this);
					if (prevDoBuild && this.titleElement != null) {
						this.titleDoBuild= true;
					}
					
					this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
					return;
				}
				break;
			case TexCommand.SYMBOL:
			case TexCommand.MATHSYMBOL:
				if (command instanceof LtxPrintCommand
						&& command.getParameters().isEmpty()
						&& this.titleDoBuild) {
					final String text= ((LtxPrintCommand) command).getText();
					if (text != null) {
						if (text.length() == 1 && Character.getType(text.charAt(0)) == Character.NON_SPACING_MARK) {
							final int size= this.titleBuilder.length();
							node.acceptInTexChildren(this);
							if (this.titleElement != null && this.titleBuilder.length() == size + 1) {
								this.titleBuilder.append(text);
							}
							
							this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
							return;
						}
						this.titleBuilder.append(text);
					}
				}
				break;
			}
		}
		
		node.acceptInTexChildren(this);
		
		this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
	}
	
	@Override
	public void visit(final Text node) throws InvocationTargetException {
		if (this.titleDoBuild) {
			this.titleBuilder.append(this.input, node.getStartOffset(), node.getEndOffset());
			if (this.titleBuilder.length() >= 100) {
				finishTitleText();
			}
		}
		
		this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
	}
	
	@Override
	public void visit(final Embedded node) throws InvocationTargetException {
		if ((node.getEmbedDescr() & 0xF) == Embedded.EMBED_INLINE) {
			if (this.titleDoBuild) {
				this.titleBuilder.append(this.input, node.getStartOffset(), node.getEndOffset());
				if (this.titleBuilder.length() >= 100) {
					finishTitleText();
				}
			}
			this.embeddedItems.add(new EmbeddingReconcileTask(node));
		}
		else {
			if (this.titleElement != null) {
				finishTitleText();
			}
			if (this.currentElement.children.isEmpty()) {
				this.currentElement.children= new ArrayList<>();
			}
			final EmbeddingForeignSourceElement element= new EmbeddingForeignSourceElement(node.getText(),
					this.currentElement, node );
			element.startOffset= node.getStartOffset();
			element.length= node.getLength();
			element.name= TexElementName.create(0, ""); //$NON-NLS-1$
			this.currentElement.children.add(element);
			this.embeddedItems.add(new EmbeddingReconcileTask(node, element));
		}
		
		this.currentElement.length= node.getEndOffset() - this.currentElement.getStartOffset();
	}
	
	
	private TexAstNode getLabelNode(TexAstNode node) {
		if (node != null && node.getNodeType() == NodeType.CONTROL && node.getChildCount() > 0) {
			node= node.getChild(0);
			if (node.getNodeType() == NodeType.LABEL) {
				return node;
			}
			if (node.getNodeType() == NodeType.GROUP && node.getChildCount() > 0) {
				node= node.getChild(0);
				if (node.getNodeType() == NodeType.LABEL) {
					return node;
				}
			}
		}
		return null;
	}
	
}
