/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class ProblemMessages extends NLS {
	
	
	public static String Ast_CurlyBracket_NotClosed_message;
	public static String Ast_CurlyBracket_NotOpened_message;
	public static String Ast_SquareBracket_NotClosed_message;
	public static String Ast_SquareBracket_NotOpened_message;
	public static String Ast_OptArgument_NotClosed_Opt_message;
	public static String Ast_ReqArgument_NotClosed_message;
	
	public static String Ast_Env_MissingName_Begin_message;
	public static String Ast_Env_MissingName_End_message;
	public static String Ast_Env_NotClosed_message;
	public static String Ast_Env_NotOpened_message;
	public static String Ast_Math_NotClosed_message;
	public static String Ast_Verbatim_MissingSep_message;
	public static String Ast_Verbatim_NotClosed_message;
	
	public static String Labels_UndefinedRef_message;
	
	
	static {
		NLS.initializeMessages(ProblemMessages.class.getName(), ProblemMessages.class);
	}
	private ProblemMessages() {}
	
}
