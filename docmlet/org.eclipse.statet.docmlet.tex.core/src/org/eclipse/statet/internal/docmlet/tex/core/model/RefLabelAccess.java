/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.docmlet.tex.core.ast.TexAstNode;
import org.eclipse.statet.docmlet.tex.core.model.TexElementName;
import org.eclipse.statet.docmlet.tex.core.model.TexNameAccess;
import org.eclipse.statet.ltk.model.core.impl.NameAccessAccumulator;


public class RefLabelAccess extends TexNameAccess {
	
	
	public final static int A_READ=                         0x00000000;
	public final static int A_WRITE=                        0x00000002;
	
	
	private final NameAccessAccumulator<TexNameAccess> shared;
	
	private final TexAstNode node;
	private final TexAstNode nameNode;
	
	int flags;
	
	
	protected RefLabelAccess(final NameAccessAccumulator<TexNameAccess> shared, final TexAstNode node, final TexAstNode labelNode) {
		this.shared= shared;
		shared.getList().add(this);
		this.node= node;
		this.nameNode= labelNode;
	}
	
	
	@Override
	public int getType() {
		return LABEL;
	}
	
	@Override
	public String getSegmentName() {
		return this.shared.getLabel();
	}
	
	@Override
	public String getDisplayName() {
		return this.shared.getLabel();
	}
	
	@Override
	public TexElementName getNextSegment() {
		return null;
	}
	
	
	@Override
	public TexAstNode getNode() {
		return this.node;
	}
	
	@Override
	public TexAstNode getNameNode() {
		return this.nameNode;
	}
	
	@Override
	public ImList<? extends TexNameAccess> getAllInUnit() {
		return ImCollections.toList(this.shared.getList());
	}
	
	
	@Override
	public boolean isWriteAccess() {
		return ((this.flags & A_WRITE) != 0);
	}
	
}
