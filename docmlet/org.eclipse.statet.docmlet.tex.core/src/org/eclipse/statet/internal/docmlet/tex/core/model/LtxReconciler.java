/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.string.CacheStringFactory;
import org.eclipse.statet.jcommons.text.core.input.StringParserInput;

import org.eclipse.statet.docmlet.tex.core.TexCore;
import org.eclipse.statet.docmlet.tex.core.TexCoreAccess;
import org.eclipse.statet.docmlet.tex.core.ast.Embedded;
import org.eclipse.statet.docmlet.tex.core.ast.LtxParser;
import org.eclipse.statet.docmlet.tex.core.ast.SourceComponent;
import org.eclipse.statet.docmlet.tex.core.ast.TexAstInfo;
import org.eclipse.statet.docmlet.tex.core.commands.TexCommand;
import org.eclipse.statet.docmlet.tex.core.model.LtxSourceUnitModelInfo;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceUnit;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxIssueReporter;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitEmbeddedModelReconciler;
import org.eclipse.statet.docmlet.tex.core.model.build.LtxSourceUnitModelContainer;
import org.eclipse.statet.docmlet.tex.core.parser.NowebLtxLexer;
import org.eclipse.statet.docmlet.tex.core.project.TexProject;
import org.eclipse.statet.docmlet.tex.core.project.TexProjects;
import org.eclipse.statet.docmlet.tex.core.source.TexSourceConfig;
import org.eclipse.statet.internal.docmlet.tex.core.TexCorePlugin;
import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.build.ExtensibleReconciler;
import org.eclipse.statet.ltk.model.core.impl.BasicSourceModelStamp;


@NonNullByDefault
public class LtxReconciler extends ExtensibleReconciler<TexProject,
		LtxSourceUnitModelContainer<?>, LtxSourceUnitEmbeddedModelReconciler<?>> {
	
	
	private static final ImIdentityList<String> DEFAULT_EMBEDDED_MODEL_IDS= ImCollections.newIdentityList();
	
	
	protected static class Data {
		
		public final LtxSourceUnitModelContainer<?> adapter;
		public final ImList<ExtensionData<TexProject, ?, LtxSourceUnitEmbeddedModelReconciler<?>>> extensions;
		public final @Nullable String nowebTypeId;
		
		public final TexSourceUnit sourceUnit;
		public final TexCoreAccess texCoreAccess;
		
		public final SourceContent content;
		
		@Nullable TexAstInfo ast;
		
		@Nullable Map<String, TexCommand> customCommands;
		@Nullable Map<String, TexCommand> customEnvs;
		
		@Nullable LtxSourceUnitModelInfo oldModel;
		@Nullable LtxSourceUnitModelInfo newModel;
		
		
		public Data(final LtxSourceUnitModelContainer<?> adapter,
				final ImList<ExtensionData<TexProject, ?, LtxSourceUnitEmbeddedModelReconciler<?>>> extensions,
				final @Nullable String nowebTypeId,
				final SubMonitor m) {
			this.adapter= adapter;
			this.extensions= extensions;
			this.nowebTypeId= nowebTypeId;
			
			this.sourceUnit= adapter.getSourceUnit();
			this.texCoreAccess= TexCore.getContextAccess(adapter.getSourceUnit());
			this.content= adapter.getParseContent(m);
		}
		
		
		boolean isOK() {
			return (this.content != null);
		}
		
		@SuppressWarnings("null")
		public TexAstInfo getAst() {
			return this.ast;
		}
		
		@SuppressWarnings("null")
		public LtxSourceUnitModelInfo getModel() {
			return this.newModel;
		}
		
	}
	
	
	private static final boolean DEBUG_LOG_AST= Boolean.parseBoolean(
			Platform.getDebugOption("org.eclipse.statet.docmlet.tex/debug/Reconciler/logAst") ); //$NON-NLS-1$
	
	
	private final LtxModelManagerImpl texManager;
	
	private final Object raLock= new Object();
	private final StringParserInput raInput= new StringParserInput(0x1000);
	private final NowebLtxLexer raLexer= new NowebLtxLexer();
	private final LtxParser raParser= new LtxParser(this.raLexer, new CacheStringFactory(0x20));
	
	private final Object rmLock= new Object();
	private final SourceAnalyzer rmSourceAnalyzer= new SourceAnalyzer();
	
	private final Object riLock= new Object();
	private final LtxIssueReporter riReporter= new LtxIssueReporter();
	
	
	public LtxReconciler(final LtxModelManagerImpl manager) {
		this.texManager= manager;
	}
	
	
	@Override
	public void init(final TexProject project, final MultiStatus statusCollector) {
		super.init(project, statusCollector);
		
		this.riReporter.configure(project.getPrefs(), project);
	}
	
	void stop() {
		this.stop= true;
	}
	
	
	@Override
	protected @Nullable TexProject getProject(final IProject project) {
		return TexProjects.getTexProject(project);
	}
	@Override
	protected @Nullable LtxSourceUnitEmbeddedModelReconciler<?> createEmbeddedModelReconciler(
			final String modelTypeId) {
		return LtkModels.getModelAdapter(modelTypeId, LtxSourceUnitEmbeddedModelReconciler.class);
	}
	
	
	public void reconcile(final LtxSourceUnitModelContainer<?> adapter, final int flags,
			final IProgressMonitor monitor) {
		final var m= SubMonitor.convert(monitor);
		
		final Data data;
		{	ImIdentityList<String> embeddedModelIds= DEFAULT_EMBEDDED_MODEL_IDS;
			String nowebTypeId= adapter.getNowebType();
			if (nowebTypeId != null) {
				embeddedModelIds= ImCollections.addElementIfAbsent(embeddedModelIds, nowebTypeId);
			}
			final var extensions= initExtensions(embeddedModelIds, adapter, flags);
			if (nowebTypeId != null && !ExtensionData.contains(extensions, nowebTypeId)) {
				nowebTypeId= null;
			}
			data= new Data(adapter, extensions, nowebTypeId, m);
			if (data == null || !data.isOK()) {
				adapter.clear();
				return;
			}
		}
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		
		synchronized (this.raLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			updateAst(data, flags, m);
		}
		
		if (this.stop || m.isCanceled()) {
			throw new CancellationException();
		}
		if ((flags & 0xf) < ModelManager.MODEL_FILE) {
			return;
		}
		
		synchronized (this.rmLock) {
			if (this.stop || m.isCanceled()) {
				throw new CancellationException();
			}
			final boolean updated= updateModel(data, flags, m);
			
			if (updated) {
				this.texManager.getEventJob().addUpdate(data.sourceUnit,
						data.oldModel, data.newModel );
			}
		}
		
		if ((flags & ModelManager.RECONCILE) != 0 && data.newModel != null) {
			synchronized (this.riLock) {
				if (this.stop || m.isCanceled()
						|| data.newModel != data.adapter.getCurrentModel() ) {
					return;
				}
				
				reportIssues(data, flags);
			}
		}
	}
	
	protected final void updateAst(final Data data, final int flags,
			final SubMonitor m) {
		final SourceModelStamp stamp= new BasicSourceModelStamp(data.content.getStamp(),
				createSourceConfig(TexSourceConfig.DEFAULT_CONFIG, data.extensions) );
		
		TexAstInfo ast= (TexAstInfo)data.adapter.getCurrentAst();
		if (ast != null && !stamp.equals(ast.getStamp())) {
			ast= null;
		}
		if (ast != null) {
			data.ast= ast;
		}
		else {
			final SourceComponent sourceNode;
			this.raInput.reset(data.content.getString());
			
			this.raParser.setCommentLevel(LtxParser.COLLECT_COMMENTS);
			List<Embedded> embeddedNodes;
			if (data.nowebTypeId != null) {
				final var extensionData= nonNullAssert(
						ExtensionData.get(data.extensions, data.nowebTypeId) );
				this.raLexer.setNowebType(extensionData.modelTypeId);
				this.raParser.setCollectEmebeddedNodes(true);
				
				sourceNode= this.raParser.parseSourceUnit(this.raInput.init(), data.texCoreAccess.getTexCommandSet());
				data.customCommands= this.raParser.getCustomCommandMap();
				data.customEnvs= this.raParser.getCustomEnvMap();
				
				embeddedNodes= this.raParser.getEmbeddedNodes();
			}
			else {
				this.raLexer.setNowebType(null);
				this.raParser.setCollectEmebeddedNodes(false);
				
				sourceNode= this.raParser.parseSourceUnit(this.raInput.init(), data.texCoreAccess.getTexCommandSet());
				
				embeddedNodes= ImCollections.emptyList();
			}
			
			final var embeddedTypeIds= collectEmbeddedTypeIds(embeddedNodes);
			for (final var extensionData : data.extensions) {
				if (embeddedTypeIds.contains(extensionData.modelTypeId)) {
					((LtxSourceUnitEmbeddedModelReconciler)extensionData.reconciler).reconcileAst(
							data.content, embeddedNodes,
							data.adapter, extensionData.config, flags );
				}
			}
			
			ast= new TexAstInfo(1, stamp, sourceNode, embeddedTypeIds);
			if (DEBUG_LOG_AST) {
				logAst(ast, data.content);
			}
			
			synchronized (data.adapter) {
				data.adapter.setAst(ast);
			}
			data.ast= ast;
		}
	}
	
	protected final boolean updateModel(final Data data, final int flags,
			final SubMonitor m) {
		LtxSourceUnitModelInfo model= data.adapter.getCurrentModel();
		if (model != null && !data.getAst().getStamp().equals(model.getStamp())) {
			model= null;
		}
		if (model != null) {
			data.newModel= model;
			return false;
		}
		else {
			model= this.rmSourceAnalyzer.createModel(
					data.adapter.getSourceUnit(), data.content.getString(), data.getAst(),
					data.customCommands, data.customEnvs );
			final boolean isOK= (model != null);
			
			for (final var extensionData : data.extensions) {
				((LtxSourceUnitEmbeddedModelReconciler)extensionData.reconciler).reconcileModel(
						model, data.content,
						this.rmSourceAnalyzer.getEmbeddedItems(),
						data.adapter, extensionData.config, flags,
						m );
			}
			
			if (isOK) {
				synchronized (data.adapter) {
					data.oldModel= data.adapter.getCurrentModel();
					data.adapter.setModel(model);
				}
				data.newModel= model;
				return true;
			}
			return false;
		}
	}
	
	protected final void reportIssues(final Data data, final int flags) {
		try {
			final var issueSupport= data.adapter.getIssueSupport();
			if (issueSupport == null) {
				return;
			}
			final var issueRequestor= issueSupport.createIssueRequestor(data.sourceUnit);
			if (issueRequestor != null) {
				try {
					this.riReporter.run(data.sourceUnit, data.getModel(), data.content,
							issueRequestor, flags );
					
					for (final var extensionData : data.extensions) {
						((LtxSourceUnitEmbeddedModelReconciler)extensionData.reconciler).reportIssues(
								data.getModel(), data.content,
								issueRequestor,
								data.adapter, extensionData.config, flags );
					}
				}
				finally {
					issueRequestor.finish();
				}
			}
		}
		catch (final Exception e) {
			handleStatus(new Status(IStatus.ERROR, TexCore.BUNDLE_ID, 0,
					String.format("An error occurred when reporting issues for source unit %1$s.",
							data.sourceUnit ),
					e ));
		}
	}
	
	
	protected void handleStatus(final IStatus status) {
		final MultiStatus collector= getStatusCollector();
		if (collector != null) {
			collector.add(status);
		}
		else {
			TexCorePlugin.log(status);
		}
	}
	
	
}
