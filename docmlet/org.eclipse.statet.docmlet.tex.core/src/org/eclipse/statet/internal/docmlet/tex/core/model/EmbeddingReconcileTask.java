/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.tex.core.model;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.tex.core.ast.Embedded;
import org.eclipse.statet.docmlet.tex.core.model.TexSourceElement;
import org.eclipse.statet.ltk.model.core.build.EmbeddingForeignReconcileTask;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;


@NonNullByDefault
public class EmbeddingReconcileTask implements EmbeddingForeignReconcileTask<Embedded, TexSourceElement> {
	
	
	private final Embedded node;
	private final @Nullable EmbeddingForeignSourceElement element;
	
	
	public EmbeddingReconcileTask(final Embedded node,
			final EmbeddingForeignSourceElement element) {
		this.node= node;
		this.element= nonNullAssert(element);
	}
	
	public EmbeddingReconcileTask(final Embedded node) {
		assert ((node.getEmbedDescr() & 0xF) == Embedded.EMBED_INLINE);
		this.node= node;
		this.element= null;
	}
	
	
	@Override
	public String getForeignTypeId() {
		return this.node.getForeignTypeId();
	}
	
	@Override
	public Embedded getAstNode() {
		return this.node;
	}
	
	@Override
	public TexSourceElement getEmbeddingElement() {
		return this.element;
	}
	
	@Override
	public void setEmbeddedElement(final SourceStructElement element) {
		if (element != null) {
			this.element.setForeign(element);
		}
	}
	
}
