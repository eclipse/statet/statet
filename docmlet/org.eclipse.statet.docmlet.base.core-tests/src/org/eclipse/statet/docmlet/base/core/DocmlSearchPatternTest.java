/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.eclipse.statet.jcommons.text.core.SearchPattern.PREFIX_MATCH;
import static org.eclipse.statet.jcommons.text.core.SearchPattern.SUBSTRING_MATCH;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.SearchPattern;


@NonNullByDefault
public class DocmlSearchPatternTest {
	// Copy of org.eclipse.statet.jcommons.text.core.SearchPatternTest
	
	
	public DocmlSearchPatternTest() {
	}
	
	
	protected DocmlSearchPattern createPattern(final int rules) {
		return new DocmlSearchPattern(rules, "");
	}
	
	
	@Test
	public void matches_Prefix() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("a"));
		assertEquals(0, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
	}
	
	@Test
	public void matches_Prefix_CaseInsensitive() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(0, pattern.matches("xAbcxabc"));
		
		pattern.setPattern("A");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(0, pattern.matches("xAbcxAbc"));
		assertEquals(0, pattern.matches("xABcxABc"));
		assertEquals(0, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("Ab");
		assertEquals(0, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xAbcxAbc"));
		assertEquals(0, pattern.matches("xABcxABc"));
		assertEquals(0, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("aB");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(0, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xAbcxAbc"));
		assertEquals(0, pattern.matches("xABcxABc"));
		assertEquals(0, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("Abc"));
		assertEquals(0, pattern.matches("aBc"));
		assertEquals(0, pattern.matches("abC"));
		assertEquals(0, pattern.matches("xAbcxAbc"));
		assertEquals(0, pattern.matches("xaBcxaBc"));
		assertEquals(0, pattern.matches("xabCxabC"));
	}
	
	@Test
	public void matches_Substring() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(0, pattern.matches("xbc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xbc"));
		assertEquals(0, pattern.matches("xbxc"));
	}
	
	@Test
	public void matches_Substring_CaseInsensitive() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertEquals(PREFIX_MATCH, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxabc"));
		
		pattern.setPattern("A");
		assertEquals(PREFIX_MATCH, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxAbc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xABcxABc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("Ab");
		assertEquals(0, pattern.matches("A"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxAbc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xABcxABc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("aB");
		assertEquals(0, pattern.matches("a"));
		assertEquals(PREFIX_MATCH, pattern.matches("abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("Abc"));
		assertEquals(PREFIX_MATCH, pattern.matches("ABc"));
		assertEquals(PREFIX_MATCH, pattern.matches("aBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabcxabc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxAbc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xABcxABc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xaBcxaBc"));
		
		pattern.setPattern("bc");
		assertEquals(SUBSTRING_MATCH, pattern.matches("Abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("aBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("abC"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xAbcxAbc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xaBcxaBc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("xabCxabC"));
	}
	
	
	@Test
	public void getMatchingRegions_Prefix() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("");
		assertNull(
				pattern.getMatchingRegions("a", PREFIX_MATCH) );
		assertNull(
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Prefix_CaseInsensitive() {
		final SearchPattern pattern= createPattern(PREFIX_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("A", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("Abc", PREFIX_MATCH) );
		
		pattern.setPattern("A");
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("a", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 1 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("Abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("aBc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("ABc", PREFIX_MATCH) );
		
		pattern.setPattern("Ab");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("aBc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("ABc", PREFIX_MATCH) );
		
		pattern.setPattern("aB");
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("Abc", PREFIX_MATCH) );
		assertArrayEquals(new int[] { 0, 2 },
				pattern.getMatchingRegions("ABc", PREFIX_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Substring() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("bc");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 6, 8 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xbc", SUBSTRING_MATCH) );
	}
	
	@Test
	public void getMatchingRegions_Substring_CaseInsensitive() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("a");
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xAbcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxAbc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xAbcxAbc", SUBSTRING_MATCH) );
		
		pattern.setPattern("A");
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xAbcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 2, 5, 6 },
				pattern.getMatchingRegions("xabcxAbc", SUBSTRING_MATCH) );
		
		pattern.setPattern("ab");
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xAbcxaBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xABcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("Ab");
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xAbcxaBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xABcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("aB");
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xAbcxaBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3, 5, 7 },
				pattern.getMatchingRegions("xABcxabc", SUBSTRING_MATCH) );
		
		pattern.setPattern("bc");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("abc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 6, 8 },
				pattern.getMatchingRegions("xabcxabc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xbc", SUBSTRING_MATCH) );
		
		pattern.setPattern("Bc");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("abC", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 6, 8 },
				pattern.getMatchingRegions("xabcxabC", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xbC", SUBSTRING_MATCH) );
		
		pattern.setPattern("bC");
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("aBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 2, 4, 6, 8 },
				pattern.getMatchingRegions("xabcxaBc", SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 1, 3 },
				pattern.getMatchingRegions("xBc", SUBSTRING_MATCH) );
	}
	
}
