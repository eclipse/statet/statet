/*=============================================================================#
 # Copyright (c) 2019, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.base.core;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.eclipse.statet.jcommons.text.core.SearchPattern.PREFIX_MATCH;
import static org.eclipse.statet.jcommons.text.core.SearchPattern.SUBSTRING_MATCH;

import static org.eclipse.statet.docmlet.base.core.DocmlSearchPattern.LABEL_SUBSTRING_MATCH;

import org.junit.jupiter.api.Test;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.text.core.SearchPattern;


@NonNullByDefault
public class DocmlSearchPatternLabelTest extends DocmlSearchPatternTest {
	
	
	public DocmlSearchPatternLabelTest() {
	}
	
	
	@Override
	protected DocmlSearchPattern createPattern(int rules) {
		if ((rules & SUBSTRING_MATCH) != 0) {
			rules|= LABEL_SUBSTRING_MATCH;
		}
		return super.createPattern(rules);
	}
	
	
	@Test
	public void matches_Substring_Prefix_0() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("");
		assertEquals(PREFIX_MATCH, pattern.matches("sec:a"));
		assertEquals(PREFIX_MATCH, pattern.matches("sec:abc"));
		
		pattern.setPattern("a");
		assertEquals(SUBSTRING_MATCH, pattern.matches("sec:a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("sec:abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("sec:xabcxabc"));
		assertEquals(0, pattern.matches("sec:xbc"));
		
		pattern.setPattern("ab");
		assertEquals(0, pattern.matches("sec:a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("sec:abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("sec:xabcxabc"));
		assertEquals(0, pattern.matches("sec:xbc"));
		
		pattern.setPattern("bc");
		assertEquals(0, pattern.matches("sec:a"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("sec:abc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("sec:xabcxabc"));
		assertEquals(SUBSTRING_MATCH, pattern.matches("sec:xbc"));
		assertEquals(0, pattern.matches("sec:xbxc"));
	}
	
	@Test
	public void matches_Substring_Prefix_PatternWithPrefix() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("sec:");
		assertEquals(PREFIX_MATCH, pattern.matches("sec:a"));
		assertEquals(PREFIX_MATCH, pattern.matches("sec:abc"));
		assertEquals(0, pattern.matches("secx:abc"));
		
		pattern.setPattern("sec:a");
		assertEquals(PREFIX_MATCH, pattern.matches("sec:a"));
		assertEquals(PREFIX_MATCH, pattern.matches("sec:abc"));
		assertEquals(LABEL_SUBSTRING_MATCH, pattern.matches("sec:xabcxabc"));
		assertEquals(0, pattern.matches("sec:xbc"));
		assertEquals(0, pattern.matches("secx:xabc"));
		
		pattern.setPattern("sec:ab");
		assertEquals(0, pattern.matches("sec:a"));
		assertEquals(PREFIX_MATCH, pattern.matches("sec:abc"));
		assertEquals(LABEL_SUBSTRING_MATCH, pattern.matches("sec:xabcxabc"));
		assertEquals(0, pattern.matches("sec:xbc"));
		assertEquals(0, pattern.matches("secx:xabc"));
		
		pattern.setPattern("sec:bc");
		assertEquals(0, pattern.matches("sec:a"));
		assertEquals(LABEL_SUBSTRING_MATCH, pattern.matches("sec:abc"));
		assertEquals(LABEL_SUBSTRING_MATCH, pattern.matches("sec:xabcxabc"));
		assertEquals(0, pattern.matches("sec:xbxc"));
		assertEquals(0, pattern.matches("secx:xabc"));
	}
	
	
	@Test
	public void getMatchingRegions_Substring_PatternWithPrefix() {
		final SearchPattern pattern= createPattern(SUBSTRING_MATCH);
		
		pattern.setPattern("sec:a");
		assertArrayEquals(new int[] { 0, 4, 5, 6, 9, 10 },
				pattern.getMatchingRegions("sec:xabcxabc", LABEL_SUBSTRING_MATCH) );
		
		pattern.setPattern("sec:ab");
		assertArrayEquals(new int[] { 0, 4, 5, 7, 9, 11 },
				pattern.getMatchingRegions("sec:xabcxabc", LABEL_SUBSTRING_MATCH) );
		
		pattern.setPattern("sec:bc");
		assertArrayEquals(new int[] { 0, 4, 5, 7 },
				pattern.getMatchingRegions("sec:abc", LABEL_SUBSTRING_MATCH) );
		assertArrayEquals(new int[] { 0, 4, 6, 8, 10, 12 },
				pattern.getMatchingRegions("sec:xabcxabc", LABEL_SUBSTRING_MATCH) );
	}
	
}
