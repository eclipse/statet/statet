/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentRewriteSession;
import org.eclipse.jface.text.DocumentRewriteSessionType;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEdit;

import org.eclipse.statet.jcommons.text.core.BasicTextRegion;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.IndentUtil;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.source.HardLineWrap;
import org.eclipse.statet.internal.docmlet.wikitext.ui.editors.Messages;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.model.core.ElementSet;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.util.LtkModelElementComparator;
import org.eclipse.statet.ltk.ui.sourceediting.actions.AbstractSourceDocumentHandler;


public class WikidocCorrectLineWrapHandler extends AbstractSourceDocumentHandler<WikitextSourceUnit> {
	
	
	public WikidocCorrectLineWrapHandler() {
	}
	
	
	@Override
	protected String getTaskLabel() {
		return Messages.CorrectLineWrap_task;
	}
	
	@Override
	protected boolean isEditTask() {
		return true;
	}
	
	@Override
	protected boolean isSourceUnitSupported(final SourceUnit sourceUnit) {
		return (sourceUnit instanceof WikitextSourceUnit);
	}
	
	@Override
	protected void doExecute(final ExecData data,
			final IProgressMonitor monitor) throws Exception {
		final List<WikitextSourceUnit> connectedSourceUnits= new ArrayList<>();
		try {
			final byte mode= HardLineWrap.SELECTION_MERGE;
			if (data.getTextSelection() != null) {
				final ITextSelection selection= data.getTextSelection();
				final WikitextSourceUnit sourceUnit= data.getSourceUnits().getFirst();
				sourceUnit.connect(monitor);
				connectedSourceUnits.add(sourceUnit);
				
				final AstInfo ast= sourceUnit.getAstInfo(null, true, monitor);
				if (ast == null) {
					return;
				}
				
				final AbstractDocument doc= sourceUnit.getDocument(monitor);
				final IRegion lineInfo= doc.getLineInformationOfOffset(selection.getOffset());
				final TextRegion region= new BasicTextRegion(lineInfo.getOffset(),
						(selection.getLength() == 0) ?
								lineInfo.getOffset() + lineInfo.getLength() :
								selection.getOffset() + selection.getLength() );
				
				final HardLineWrap hardLineWrap= new HardLineWrap(sourceUnit.getDocumentContentInfo(),
						WikitextCore.getContextAccess(sourceUnit) );
				final TextEdit textEdit= hardLineWrap.createTextEdit(doc, ast, region, mode, null);
				
				apply(doc, textEdit);
			}
			else if (data.getSourceEditor() != null) {
				final ElementSet sourceElements= data.getElementSelection();
				sourceElements.removeElementsWithAncestorsOnList();
				Collections.sort(sourceElements.getModelElements(), new LtkModelElementComparator());
				final WikitextSourceUnit sourceUnit= data.getSourceUnits().getFirst();
				sourceUnit.connect(monitor);
				connectedSourceUnits.add(sourceUnit);
				
				final AstInfo ast= sourceUnit.getAstInfo(null, true, monitor);
				if (ast == null) {
					return;
				}
				
				final AbstractDocument doc= sourceUnit.getDocument(monitor);
				final HardLineWrap hardLineWrap= new HardLineWrap(sourceUnit.getDocumentContentInfo(),
						WikitextCore.getContextAccess(sourceUnit) );
				final IndentUtil indentUtil= new IndentUtil(doc,
						hardLineWrap.getWikitextCoreAccess().getWikitextCodeStyle() );
				
				final List<LtkModelElement<?>> modelElements= sourceElements.getModelElements();
				final MultiTextEdit textEdit= new MultiTextEdit();
				for (int i= 0; i < modelElements.size(); i++) {
					final SourceStructElement element= (SourceStructElement)modelElements.get(i);
					hardLineWrap.addTextEdits(doc, ast, element.getSourceRange(), mode,
							textEdit, indentUtil );
				}
				
				apply(doc, textEdit);
			}
			
		}
		finally {
			for (final WikitextSourceUnit sourceUnit : connectedSourceUnits) {
				sourceUnit.disconnect(monitor);
			}
		}
	}
	
	private void apply(final AbstractDocument doc, final TextEdit textEdit)
			throws MalformedTreeException, BadLocationException {
		if (textEdit != null && textEdit.getChildrenSize() > 0) {
			final DocumentRewriteSession rewriteSession= doc.startRewriteSession(
					DocumentRewriteSessionType.STRICTLY_SEQUENTIAL );
			try {
				textEdit.apply(doc, TextEdit.NONE);
			}
			finally {
				doc.stopRewriteSession(rewriteSession);
			}
		}
	}
	
}
