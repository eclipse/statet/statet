/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.sourceediting;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.base.ui.sourceediting.MarkupHelpComputer;
import org.eclipse.statet.internal.docmlet.wikitext.ui.editors.WikitextQuickRenameComputer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistProcessor;


@NonNullByDefault
public class WikidocQuickAssistProcessor extends QuickAssistProcessor {
	
	
	private final QuickAssistComputer refactoringComputer= new WikitextQuickRenameComputer();
	
	private final QuickAssistComputer helpComputer= new MarkupHelpComputer();
	
	
	public WikidocQuickAssistProcessor(final SourceEditor editor) {
		super(editor);
	}
	
	
	@Override
	protected void addModelAssistProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		this.refactoringComputer.computeAssistProposals(context, proposals, monitor);
		this.helpComputer.computeAssistProposals(context, proposals, monitor);
	}
	
}
