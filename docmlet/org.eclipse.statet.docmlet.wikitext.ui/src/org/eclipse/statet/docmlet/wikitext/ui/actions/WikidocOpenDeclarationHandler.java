/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.actions;

import org.eclipse.core.runtime.NullProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.wikitext.core.ast.NodeType;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.actions.AbstractOpenDeclarationHandler;
import org.eclipse.statet.ltk.ui.sourceediting.actions.OpenDeclaration;


@NonNullByDefault
public class WikidocOpenDeclarationHandler extends AbstractOpenDeclarationHandler {
	
	
//	private static final class WikitextOpenDeclaration extends OpenDeclaration {
//		
//		@Override
//		public ILabelProvider createLabelProvider() {
//			return new WikitextLabelProvider();
//		}
//	}
	
	
	public static @Nullable WikitextNameAccess searchAccess(final SourceEditor editor,
			final TextRegion region) {
		final SourceUnit sourceUnit= editor.getSourceUnit();
		if (sourceUnit instanceof WikitextSourceUnit) {
			final WikidocSourceUnitModelInfo info= (WikidocSourceUnitModelInfo)sourceUnit.getModelInfo(
					WikitextModel.WIKIDOC_TYPE_ID, ModelManager.MODEL_FILE, new NullProgressMonitor());
			if (info != null) {
				final AstInfo astInfo= info.getAst();
				final AstSelection selection= AstSelection.search(astInfo.getRoot(),
						region.getStartOffset(), region.getEndOffset(),
						AstSelection.MODE_COVERING_SAME_LAST );
				final AstNode covering= selection.getCovering();
				if (covering instanceof final WikitextAstNode node) {
					if (node.getNodeType() == NodeType.LABEL) {
						WikitextAstNode current= node;
						do {
							for (final Object attachment : current.getAttachments()) {
								if (attachment instanceof final WikitextNameAccess access) {
									if (access.getNameNode() == node) {
										return access;
									}
								}
							}
							current= current.getWikitextParent();
						} while (current != null);
					}
				}
			}
		}
		return null;
	}
	
	
	@Override
	public boolean execute(final SourceEditor editor, final TextRegion selection) {
		final WikitextNameAccess access= searchAccess(editor, selection);
		if (access != null) {
			final OpenDeclaration open= new OpenDeclaration();
			final WikitextNameAccess declAccess= open.selectAccess(access.getAllInUnit());
			if (declAccess != null) {
				open.open(editor, declAccess);
				return true;
			}
		}
		return false;
	}
	
}
