/*=============================================================================#
 # Copyright (c) 2007, 2025 David Green and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     David Green - org.eclipse.mylyn.docs: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.sourceediting;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.mylyn.internal.wikitext.ui.WikiTextUiPlugin;
import org.eclipse.mylyn.internal.wikitext.ui.editor.preferences.Preferences;
import org.eclipse.mylyn.internal.wikitext.ui.viewer.FontState;
import org.eclipse.mylyn.wikitext.parser.Attributes;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.BlockType;
import org.eclipse.mylyn.wikitext.parser.DocumentBuilder.SpanType;
import org.eclipse.mylyn.wikitext.parser.Locator;
import org.eclipse.mylyn.wikitext.parser.css.CssParser;
import org.eclipse.mylyn.wikitext.parser.css.CssRule;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.RGB;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionUtils;

import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupParser2;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.source.EmbeddingAttributes;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguageDocumentSetupParticipant;
import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.ExtdocMarkupLanguage;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.EmbeddedHtml;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.MarkupCssStyleManager;
import org.eclipse.statet.ltk.core.source.SourceContent;


@SuppressWarnings("restriction")
public class MarkupTokenScanner implements ITokenScanner {
	
	
	private static class PositionToken extends Token {
		
		private final int offset;
		
		private final int length;
		
		
		public PositionToken(final TextAttribute attribute, final int offset, final int length) {
			super(attribute);
			this.offset= offset;
			this.length= length;
		}
		
		public int getOffset() {
			return this.offset;
		}
		
		public int getLength() {
			return this.length;
		}
		
		
		@Override
		public String toString() {
			return "Token [offset=" + this.offset + ", length=" + this.length + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ 
		}
		
	}
	
	private static class BreakException extends RuntimeException {
		
		private static final long serialVersionUID= 1L;
		
		
		public BreakException() {
			super("BreakScan", null, true, false);
		}
		
	}
	
	private class Builder extends DocumentBuilder {
		
		
		private int startOffset;
		private int endOffset;
		
		private int scanRestartOffset;
		
		private int scanCurrentOffset;
		
		private final ArrayDeque<FontState> scanFontStateStack= new ArrayDeque<>();
		
		private FontState nestedCodeFontState;
		
		
		public Builder() {
		}
		
		
		private void clearBuilder() {
			this.scanFontStateStack.clear();
			this.nestedCodeFontState= null;
		}
		
		public void scan(final IDocument document, final int startOffset, final int length)
				throws BadLocationException, BreakException {
			this.startOffset= startOffset;
			this.endOffset= startOffset + length;
			
			this.scanRestartOffset= this.startOffset;
			final TreePartitionNode rootNode= TreePartitionUtils.getRootNode(document, MarkupTokenScanner.this.partitioning);
			TreePartitionNode node= TreePartitionUtils.getNode(document, MarkupTokenScanner.this.partitioning, startOffset, false);
			if (node != rootNode) {
				while (node.getParent() != rootNode) {
					node= node.getParent();
				}
				this.scanRestartOffset= node.getStartOffset();
			}
			
			final WikitextMarkupLanguage markupLanguage= MarkupLanguageDocumentSetupParticipant.getMarkupLanguage(document, MarkupTokenScanner.this.partitioning);
			
			final MarkupParser2 markupParser= new MarkupParser2(markupLanguage, this);
			markupParser.disable(MarkupParser2.GENERATIVE_CONTENT);
			markupParser.enable(MarkupParser2.SOURCE_STRUCT);
			markupParser.enable(MarkupParser2.INLINE_ALL);
			
			final SourceContent content= new SourceContent(0,
					document.get(this.scanRestartOffset, Math.min(this.endOffset + 100, document.getLength()) - this.scanRestartOffset),
					this.scanRestartOffset );
			markupParser.parse(content, true);
		}
		
		private void updateOffset() {
			updateOffset(getLocator().getDocumentOffset());
		}
		
		private void updateOffset(final int locatorOffset) {
			final int offset= this.scanRestartOffset + locatorOffset;
			if (offset > this.scanCurrentOffset) {
				addToken(this.scanFontStateStack.getLast(),
						this.scanCurrentOffset, Math.min(this.endOffset, offset) );
				this.scanCurrentOffset= offset;
			}
			
			if (offset >= this.endOffset) {
				throw new BreakException();
			}
		}
		
		
		@Override
		public void beginDocument() {
			this.scanCurrentOffset= this.startOffset;
			this.scanFontStateStack.addLast(MarkupTokenScanner.this.styleManager.createDefaultFontState());
		}
		
		@Override
		public void endDocument() {
			updateOffset();
		}
		
		@Override
		public void beginBlock(final BlockType type, final Attributes attributes) {
			final FontState fontState;
			if (type == BlockType.CODE && attributes instanceof EmbeddingAttributes
					&& ((EmbeddingAttributes) attributes).getForeignType() == ExtdocMarkupLanguage.EMBEDDED_HTML) {
				fontState= createHtmlFontState(
						this.scanFontStateStack.getLast(),
						(EmbeddingAttributes) attributes );
			}
			else {
				fontState= createFontState(
						this.scanFontStateStack.getLast(),
						getPrefCssStyles(type),
						(attributes != null) ? attributes.getCssStyle() : null );
			}
			
			if (type == BlockType.CODE && this.scanFontStateStack.size() > 1) {
				this.nestedCodeFontState= fontState;
				return;
			}
			
			updateOffset();
			this.scanFontStateStack.addLast(fontState);
		}
		
		@Override
		public void endBlock() {
			if (this.nestedCodeFontState != null) {
				this.nestedCodeFontState= null;
				return;
			}
			
			updateOffset();
			this.scanFontStateStack.removeLast();
		}
		
		@Override
		public void beginSpan(final SpanType type, final Attributes attributes) {
			final FontState fontState;
			if (type == SpanType.CODE && attributes instanceof EmbeddingAttributes
					&& ((EmbeddingAttributes) attributes).getForeignType() == ExtdocMarkupLanguage.EMBEDDED_HTML) {
				fontState= createHtmlFontState(
						this.scanFontStateStack.getLast(),
						(EmbeddingAttributes) attributes );
			}
			else {
				fontState= createFontState(
						this.scanFontStateStack.getLast(),
						getPrefCssStyles(type),
						(attributes != null) ? attributes.getCssStyle() : null );
			}
			
			updateOffset();
			this.scanFontStateStack.addLast(fontState);
		}
		
		@Override
		public void endSpan() {
			updateOffset();
			this.scanFontStateStack.removeLast();
		}
		
		@Override
		public void beginHeading(final int level, final Attributes attributes) {
			final FontState fontState= createFontState(
					this.scanFontStateStack.getLast(),
					getPrefCssStyles(level),
					(attributes != null) ? attributes.getCssStyle() : null );
			
			updateOffset();
			this.scanFontStateStack.addLast(fontState);
		}
		
		@Override
		public void endHeading() {
			updateOffset();
			this.scanFontStateStack.removeLast();
		}
		
		@Override
		public void characters(final String text) {
			if (this.nestedCodeFontState != null) {
				updateOffset();
				this.scanFontStateStack.addLast(this.nestedCodeFontState);
				final Locator locator= getLocator();
				updateOffset(locator.getLineDocumentOffset() + locator.getLineLength());
				this.scanFontStateStack.removeLast();
			}
		}
		
		@Override
		public void charactersUnescaped(final String literal) {
			characters(literal);
		}
		
		@Override
		public void entityReference(final String entity) {
			characters(entity);
		}
		
		@Override
		public void image(final Attributes attributes, final String url) {
		}
		
		@Override
		public void link(final Attributes attributes, final String hrefOrHashName, final String text) {
		}
		
		@Override
		public void imageLink(final Attributes linkAttributes, final Attributes imageAttributes, final String href,
				final String imageUrl) {
		}
		
		@Override
		public void acronym(final String text, final String definition) {
		}
		
		@Override
		public void lineBreak() {
		}
		
	}
	
	
	private final String partitioning;
	
	private final List<PositionToken> tokens= new ArrayList<>();
	
	private Iterator<PositionToken> tokenIter= null;
	
	private PositionToken currentToken= null;
	
	
	private MarkupCssStyleManager styleManager;
	
	private Preferences preferences;
	
	private final CssParser cssParser= new CssParser();
	
	private final Builder builder= new Builder();
	
	
	public MarkupTokenScanner(final String partitioning, final StyleConfig config) {
		this.partitioning= partitioning;
		setStyleConfig(config);
		reloadPreferences();
	}
	
	
	/**
	 * Sets the fonts used by this token scanner.
	 */
	public void setStyleConfig(final StyleConfig config) {
		this.styleManager= new MarkupCssStyleManager(config);
	}
	
	public void reloadPreferences() {
		this.preferences= WikiTextUiPlugin.getDefault().getPreferences();
	}
	
	@Override
	public IToken nextToken() {
		if (this.tokenIter != null && this.tokenIter.hasNext()) {
			this.currentToken= this.tokenIter.next();
		}
		else {
			this.currentToken= null;
			this.tokenIter= null;
			return Token.EOF;
		}
		return this.currentToken;
	}
	
	@Override
	public int getTokenOffset() {
		return this.currentToken.getOffset();
	}
	
	@Override
	public int getTokenLength() {
		return this.currentToken.getLength();
	}
	
	@Override
	public void setRange(final IDocument document, final int offset, final int length) {
		this.tokens.clear();
		this.tokenIter= null;
		this.currentToken= null;
		
		try {
			this.builder.scan(document, offset, length);
		}
		catch (final BreakException e) {}
		catch (final BadLocationException e) {
			throw new RuntimeException(e);
		}
		finally {
			this.builder.clearBuilder();
		}
		
		this.tokenIter= this.tokens.iterator();
	}
	
	protected TextAttribute createTextAttribute(final StyleRange styleRange) {
		int fontStyle= styleRange.fontStyle;
		if (styleRange.strikeout) {
			fontStyle |= TextAttribute.STRIKETHROUGH;
		}
		if (styleRange.underline) {
			fontStyle |= TextAttribute.UNDERLINE;
		}
		return new TextAttribute(styleRange.foreground, styleRange.background, fontStyle, styleRange.font);
	}
	
	protected final String getPrefCssStyles(final SpanType spanType) {
		final String key= switch (spanType) {
				case BOLD ->			Preferences.PHRASE_BOLD;
				case CITATION ->		Preferences.PHRASE_CITATION;
				case CODE ->			Preferences.PHRASE_CODE;
				case DELETED ->			Preferences.PHRASE_DELETED_TEXT;
				case EMPHASIS ->		Preferences.PHRASE_EMPHASIS;
				case INSERTED ->		Preferences.PHRASE_INSERTED_TEXT;
				case ITALIC ->			Preferences.PHRASE_ITALIC;
				case MONOSPACE ->		Preferences.PHRASE_MONOSPACE;
				case QUOTE ->			Preferences.PHRASE_QUOTE;
				case SPAN ->			Preferences.PHRASE_SPAN;
				case STRONG ->			Preferences.PHRASE_STRONG;
				case SUBSCRIPT ->		Preferences.PHRASE_SUBSCRIPT;
				case SUPERSCRIPT ->		Preferences.PHRASE_SUPERSCRIPT;
				case UNDERLINED ->		Preferences.PHRASE_UNDERLINED;
				default ->				null;
				};
		return this.preferences.getCssByPhraseModifierType().get(key);
	}
	
	protected final String getPrefCssStyles(final BlockType blockType) {
		final String key= switch (blockType) {
				case CODE ->			Preferences.BLOCK_BC;
				case QUOTE ->			Preferences.BLOCK_QUOTE;
				case PREFORMATTED ->	Preferences.BLOCK_PRE;
				case DEFINITION_TERM ->	Preferences.BLOCK_DT;
				default -> 				null;
				};
		return this.preferences.getCssByBlockModifierType().get(key);
	}
	
	protected final String getPrefCssStyles(final int headingLevel) {
		final String key= Preferences.HEADING_PREFERENCES[headingLevel];
		return this.preferences.getCssByBlockModifierType().get(key);
	}
	
	private FontState createFontState(final FontState parentState,
			final String prefCssStyles, final String explCssStyle) {
		if (prefCssStyles == null && explCssStyle == null) {
			return parentState;
		}
		
		final FontState fontState= new FontState(parentState);
		if (prefCssStyles != null) {
			processCssStyles(fontState, parentState, prefCssStyles);
		}
		if (explCssStyle != null) {
			processCssStyles(fontState, parentState, explCssStyle);
		}
		return fontState;
	}
	
	
	private final RGB htmlCommentColor= EPreferences.getInstancePrefs()
			.getPreferenceValue(EmbeddedHtml.HTML_COMMENT_COLOR_PREF);
	private final RGB htmlBackgroundColor= EPreferences.getInstancePrefs()
			.getPreferenceValue(EmbeddedHtml.HTML_BACKGROUND_COLOR_PREF);
	
	private FontState createHtmlFontState(final FontState parentState,
			final EmbeddingAttributes attributes) {
		final FontState fontState= new FontState(parentState);
		
		if ((attributes.getEmbedDescr() & ExtdocMarkupLanguage.EMBEDDED_HTML_COMMENT_FLAG) != 0) {
			if (this.htmlCommentColor != null) {
				fontState.setForeground(this.htmlCommentColor);
			}
		}
		else {
			if (this.htmlBackgroundColor != null) {
				fontState.setBackground(this.htmlBackgroundColor);
			}
		}
		
		return fontState;
	}
	
	private void processCssStyles(final FontState fontState, final FontState parentState,
			final String cssStyles) {
		final Iterator<CssRule> ruleIterator= this.cssParser.createRuleIterator(cssStyles);
		while (ruleIterator.hasNext()) {
			this.styleManager.processCssStyles(fontState, parentState, ruleIterator.next());
		}
	}
	
	private void addToken(final FontState fontState, final int startOffset, final int endOffset) {
		final StyleRange styleRange= this.styleManager.createStyleRange(fontState, startOffset, endOffset - startOffset);
		final TextAttribute textAttribute= createTextAttribute(styleRange);
		this.tokens.add(new PositionToken(textAttribute, startOffset, endOffset - startOffset));
	}
	
}
