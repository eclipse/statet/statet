/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.editors;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;

import org.eclipse.statet.docmlet.tex.ui.editors.TexEditorBuild;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.project.WikitextIssues;
import org.eclipse.statet.docmlet.wikitext.ui.WikitextUI;
import org.eclipse.statet.docmlet.wikitext.ui.sourceediting.WikitextEditingSettings;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;
import org.eclipse.statet.yaml.ui.editors.YamlEditorBuild;


@NonNullByDefault
public class WikitextEditorBuild {
	
	
	public static final String GROUP_ID= "Wikitext/editor/build"; //$NON-NLS-1$
	
	
	public static final BooleanPref PROBLEMCHECKING_ENABLED_PREF= new BooleanPref(
			WikitextEditingSettings.EDITING_PREF_QUALIFIER, "ProblemChecking.enabled"); //$NON-NLS-1$
	
	
	public static final String ERROR_ANNOTATION_TYPE=       "org.eclipse.statet.docmlet.wikitext.editorAnnotations.ErrorProblem"; //$NON-NLS-1$
	public static final String WARNING_ANNOTATION_TYPE=     "org.eclipse.statet.docmlet.wikitext.editorAnnotations.WarningProblem"; //$NON-NLS-1$
	public static final String INFO_ANNOTATION_TYPE=        "org.eclipse.statet.docmlet.wikitext.editorAnnotations.InfoProblem"; //$NON-NLS-1$
	
	private static final IssueTypeSet.ProblemTypes PROBLEM_ANNOTATION_TYPES= new IssueTypeSet.ProblemTypes(
			ERROR_ANNOTATION_TYPE, WARNING_ANNOTATION_TYPE, INFO_ANNOTATION_TYPE );
	
	public static final IssueTypeSet.ProblemCategory WIKIDOC_MODEL_PROBLEM_CATEGORY= new IssueTypeSet.ProblemCategory(
			WikitextModel.WIKIDOC_TYPE_ID,
			null, PROBLEM_ANNOTATION_TYPES );
	
	public static final IssueTypeSet WIKIDOC_ISSUE_TYPE_SET= new IssueTypeSet(WikitextUI.BUNDLE_ID,
			WikitextIssues.TASK_CATEGORY,
			ImCollections.newList(
					WikitextEditorBuild.WIKIDOC_MODEL_PROBLEM_CATEGORY,
					YamlEditorBuild.YAML_MODEL_PROBLEM_CATEGORY,
					TexEditorBuild.LTX_MODEL_PROBLEM_CATEGORY ));
	
}
