/*=============================================================================#
 # Copyright (c) 2015, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.sourceediting;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;


@NonNullByDefault
public interface MarkupCompletionExtension {
	
	
	static class CompletionType {
		
		private final String sourcePrefix;
		
		private final String lookupPrefix;
		
		
		public CompletionType(final String prefix) {
			this(prefix, prefix);
		}
		
		public CompletionType(final String sourcePrefix, final String lookupPrefix) {
			this.sourcePrefix= sourcePrefix;
			this.lookupPrefix= lookupPrefix;
		}
		
		
		public String getSourcePrefix() {
			return this.sourcePrefix;
		}
		
		/**
		 * Normalized prefix for lookup
		 * @return the prefix
		 */
		public String getLookupPrefix() {
			return this.lookupPrefix;
		}
		
	}
	
	
	@Nullable CompletionType getLinkAnchorLabel(
			final AssistInvocationContext context, final WikitextMarkupLanguage markupLanguage);
	
	@Nullable CompletionType getLinkRefLabel(
			final AssistInvocationContext context, final WikitextMarkupLanguage markupLanguage);
	
}
