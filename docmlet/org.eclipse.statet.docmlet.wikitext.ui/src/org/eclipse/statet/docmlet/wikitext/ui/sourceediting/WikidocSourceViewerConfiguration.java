/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.sourceediting;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.information.IInformationProvider;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.quickassist.IQuickAssistProcessor;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.mylyn.internal.wikitext.ui.util.WikiTextUiResources;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.themes.IThemeManager;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.text.IIndentSettings;
import org.eclipse.statet.ecommons.text.core.sections.DocContentSections;
import org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.WikitextCoreAccess;
import org.eclipse.statet.docmlet.wikitext.core.source.MarkupBracketPairMatcher;
import org.eclipse.statet.docmlet.wikitext.core.source.WikitextHeuristicTokenScanner;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikidocDocumentContentInfo;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextDocumentConstants;
import org.eclipse.statet.internal.docmlet.wikitext.ui.WikitextUIPlugin;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.DocQuickOutlineInformationProvider;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.MarkupAutoEditStrategy;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.MarkupDamagerRepairer;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.MarkupDoubleClickStrategy;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.WikitextContentAssistProcessor;
import org.eclipse.statet.ltk.text.core.CharPairMatcher;
import org.eclipse.statet.ltk.ui.LtkUIPreferences;
import org.eclipse.statet.ltk.ui.sourceediting.EcoReconciler2;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorAddon;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfiguration;
import org.eclipse.statet.ltk.ui.sourceediting.SourceUnitReconcilingStrategy;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputerRegistry;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistProcessor;


/**
 * Configuration for Wikitext document source editors.
 */
public class WikidocSourceViewerConfiguration extends SourceEditorViewerConfiguration {
	
	
	public static final int FIXED_LINE_HEIGHT_STYLE=        1 << 20;
	
	
	private static final int checkFlags(int flags) {
		if ((flags & SourceEditorViewerConfiguration.COMPARE_MODE) != 0) {
			flags|= FIXED_LINE_HEIGHT_STYLE;
		}
		return flags;
	}
	
	private static final String[] CONTENT_TYPES= WikitextDocumentConstants.WIKIDOC_CONTENT_TYPES.toArray(
			new String[WikitextDocumentConstants.WIKIDOC_CONTENT_TYPES.size()] );
	
	
	protected ITextDoubleClickStrategy doubleClickStrategy;
	
	private MarkupAutoEditStrategy autoEditStrategy;
	
	private WikitextCoreAccess coreAccess;
	
	
	public WikidocSourceViewerConfiguration(final int flags) {
		this(WikidocDocumentContentInfo.INSTANCE, flags, null, null, null);
	}
	
	public WikidocSourceViewerConfiguration(final DocContentSections documentContentInfo, final int flags,
			final SourceEditor editor,
			final WikitextCoreAccess access,
			final IPreferenceStore preferenceStore) {
		super(documentContentInfo, checkFlags(flags), editor);
		setCoreAccess(access);
		
		setup((preferenceStore != null) ? preferenceStore : WikitextEditingSettings.getPreferenceStore(),
				LtkUIPreferences.getEditorDecorationPreferences(),
				WikitextEditingSettings.getAssistPreferences() );
	}
	
	protected void setCoreAccess(final WikitextCoreAccess access) {
		this.coreAccess= (access != null) ? access : WikitextCore.getWorkbenchAccess();
	}
	
	protected StyleConfig createStyleConfig() {
		final IThemeManager themeManager= PlatformUI.getWorkbench().getThemeManager();
		final FontRegistry fontRegistry= themeManager.getCurrentTheme().getFontRegistry();
		return new StyleConfig(
				fontRegistry.get(WikiTextUiResources.PREFERENCE_TEXT_FONT),
				fontRegistry.get(WikiTextUiResources.PREFERENCE_MONOSPACE_FONT),
				((getFlags() & FIXED_LINE_HEIGHT_STYLE) != 0) );
	}
	
	
	@Override
	protected void initScanners() {
		addScanner(WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE,
				new MarkupTokenScanner(getConfiguredDocumentPartitioning(null),
						createStyleConfig() ));
	}
	
	@Override
	protected ITokenScanner getScanner(String contentType) {
		if (contentType == WikitextDocumentConstants.WIKIDOC_HTML_DEFAULT_CONTENT_TYPE
				|| contentType == WikitextDocumentConstants.WIKIDOC_HTML_COMMENT_CONTENT_TYPE) {
			contentType= WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE;
		}
		return super.getScanner(contentType);
	}
	
	
	@Override
	protected void initPresentationReconciler(final PresentationReconciler reconciler) {
		{	final DefaultDamagerRepairer dr= new MarkupDamagerRepairer(
					getScanner(WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE) );
			reconciler.setDamager(dr, WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE);
			reconciler.setRepairer(dr, WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE);
			reconciler.setDamager(dr, WikitextDocumentConstants.WIKIDOC_HTML_DEFAULT_CONTENT_TYPE);
			reconciler.setRepairer(dr, WikitextDocumentConstants.WIKIDOC_HTML_DEFAULT_CONTENT_TYPE);
			reconciler.setDamager(dr, WikitextDocumentConstants.WIKIDOC_HTML_COMMENT_CONTENT_TYPE);
			reconciler.setRepairer(dr, WikitextDocumentConstants.WIKIDOC_HTML_COMMENT_CONTENT_TYPE);
		}
	}
	
	
	@Override
	public List<SourceEditorAddon> getAddOns() {
		final List<SourceEditorAddon> addons= super.getAddOns();
		if (this.autoEditStrategy != null) {
			addons.add(this.autoEditStrategy);
		}
		return addons;
	}
	
	@Override
	public void handleSettingsChanged(final Set<String> groupIds, final Map<String, Object> options) {
		super.handleSettingsChanged(groupIds, options);
		if (this.autoEditStrategy != null) {
			this.autoEditStrategy.getSettings().handleSettingsChanged(groupIds, options);
		}
		if (groupIds.contains(WikitextEditingSettings.TEXTSTYLE_CONFIG_QUALIFIER)) {
			final MarkupTokenScanner scanner= (MarkupTokenScanner) getScanner(
					WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE );
			scanner.setStyleConfig(createStyleConfig());
			options.put(ITextPresentationConstants.SETTINGSCHANGE_AFFECTSPRESENTATION_KEY, Boolean.TRUE);
		}
	}
	
	
	@Override
	public String[] getConfiguredContentTypes(final ISourceViewer sourceViewer) {
		return CONTENT_TYPES;
	}
	
	
	@Override
	public CharPairMatcher createPairMatcher() {
		return new MarkupBracketPairMatcher(
				WikitextHeuristicTokenScanner.create(getDocumentContentInfo()) );
	}
	
	@Override
	public ITextDoubleClickStrategy getDoubleClickStrategy(final ISourceViewer sourceViewer, final String contentType) {
		if (this.doubleClickStrategy == null) {
			this.doubleClickStrategy= new MarkupDoubleClickStrategy(
					getConfiguredDocumentPartitioning(sourceViewer) );
		}
		return this.doubleClickStrategy;
	}
	
	
	@Override
	protected IIndentSettings getIndentSettings() {
		return this.coreAccess.getWikitextCodeStyle();
	}
	
	@Override
	public String[] getDefaultPrefixes(final ISourceViewer sourceViewer, final String contentType) {
		return new String[] { "<!--", "" }; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	
	@Override
	public boolean isSmartInsertSupported() {
		return true;
	}
	
	@Override
	public boolean isSmartInsertByDefault() {
		return EPreferences.getInstancePrefs()
				.getPreferenceValue(WikitextEditingSettings.SMARTINSERT_BYDEFAULT_ENABLED_PREF);
	}
	
	
	@Override
	public IAutoEditStrategy[] getAutoEditStrategies(final ISourceViewer sourceViewer, final String contentType) {
		if (getSourceEditor() == null) {
			return super.getAutoEditStrategies(sourceViewer, contentType);
		}
		if (this.autoEditStrategy == null) {
			this.autoEditStrategy= createAutoEditStrategy();
		}
		return new IAutoEditStrategy[] { this.autoEditStrategy };
	}
	
	protected MarkupAutoEditStrategy createAutoEditStrategy() {
		return new MarkupAutoEditStrategy(this.coreAccess, getSourceEditor());
	}
	
	
	@Override
	public IReconciler getReconciler(final ISourceViewer sourceViewer) {
		final SourceEditor editor= getSourceEditor();
		if (!(editor instanceof SourceEditor1)) {
			return null;
		}
		final EcoReconciler2 reconciler= new EcoReconciler2(editor);
		reconciler.setDelay(500);
		reconciler.addReconcilingStrategy(new SourceUnitReconcilingStrategy());
		
//		final IReconcilingStrategy spellingStrategy= getSpellingStrategy(sourceViewer);
//		if (spellingStrategy != null) {
//			reconciler.addReconcilingStrategy(spellingStrategy);
//		}
		
		return reconciler;
	}
	
	
	@Override
	public void initContentAssist(final ContentAssist assistant) {
		final ContentAssistComputerRegistry registry= WikitextUIPlugin.getInstance().getWikidocEditorContentAssistRegistry();
		
		{	final ContentAssistProcessor processor= new WikitextContentAssistProcessor(assistant,
					WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE, registry,
					getSourceEditor() );
			assistant.setContentAssistProcessor(processor, WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_TYPE);
		}
	}
	
	@Override
	protected IQuickAssistProcessor createQuickAssistProcessor() {
		final SourceEditor editor= getSourceEditor();
		if (editor != null) {
			return new WikidocQuickAssistProcessor(editor);
		}
		return null;
	}
	
	
	@Override
	protected void collectHyperlinkDetectorTargets(final Map<String, IAdaptable> targets,
			final ISourceViewer sourceViewer) {
		targets.put("org.eclipse.statet.docmlet.editorHyperlinks.WikitextEditorTarget", getSourceEditor()); //$NON-NLS-1$
	}
	
	
	@Override
	protected IInformationProvider getQuickInformationProvider(final ISourceViewer sourceViewer,
			final int operation) {
		final SourceEditor editor= getSourceEditor();
		if (editor == null) {
			return null;
		}
		return switch (operation) {
		case SourceEditorViewer.SHOW_SOURCE_OUTLINE ->
				new DocQuickOutlineInformationProvider(editor, operation);
		default ->
				null;
		};
	}
	
}
