/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.config;

import java.beans.Introspector;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.typed.PojoProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.IValueChangeListener;
import org.eclipse.core.databinding.observable.value.ValueChangeEvent;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.ui.dialogs.DialogUtils;
import org.eclipse.statet.ecommons.ui.dialogs.ExtStatusDialog;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.docmlet.wikitext.core.source.extdoc.AbstractMarkupConfig;
import org.eclipse.statet.internal.docmlet.wikitext.ui.config.Messages;


@NonNullByDefault
public abstract class AbstractMarkupConfigDialog<T extends AbstractMarkupConfig<? super T>> extends ExtStatusDialog {
	
	
	private final String contextLabel;
	
	private final IObservableValue<Boolean> contextValue;
	
	protected final T config;
	
	private Button contextControl;
	
	private @Nullable Composite extensionsGroup;
	private final Map<String, Control> configControls= new LinkedHashMap<>();
	
	
	public AbstractMarkupConfigDialog(final Shell parent, final String contextLabel,
			final boolean isContextEnabled, final T customConfig) {
		super(parent, WITH_DATABINDING_CONTEXT);
		
		this.contextLabel= contextLabel;
		this.contextValue= new WritableValue<>((contextLabel == null || isContextEnabled), Boolean.TYPE);
		this.config= customConfig;
	}
	
	
	public boolean isCustomEnabled() {
		return this.contextValue.getValue();
	}
	
	public T getCustomConfig() {
		return this.config;
	}
	
	
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite area= new Composite(parent, SWT.NONE);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		area.setLayout(LayoutUtils.newDialogGrid(2));
		
		if (this.contextLabel != null) {
			final Button button= new Button(area, SWT.CHECK);
			button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
			button.setText(NLS.bind("Enable {0} specific configuration:", this.contextLabel));
			this.contextControl= button;
		}
		
		this.extensionsGroup= createExtensionGroup(area);
		if (this.extensionsGroup != null) {
			this.extensionsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		}
		
		return area;
	}
	
	protected Composite createExtensionGroup(final Composite parent) {
		final Group composite= new Group(parent, SWT.NONE);
		composite.setLayout(LayoutUtils.newGroupGrid(2));
		composite.setText("Extensions:");
		return composite;
	}
	
	protected @Nullable Composite getExtensionComposite() {
		return this.extensionsGroup;
	}
	
	
	protected void addProperty(final Composite parent, final String propertyName,
			final String label) {
		{	final Button button= new Button(parent, SWT.CHECK);
			button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
			button.setText(label);
			this.configControls.put(propertyName, button);
		}
	}
	
	protected void addProperty(final Composite parent, final String propertyName) {
		switch (propertyName) {
		case AbstractMarkupConfig.YAML_METADATA_ENABLED_PROP:
			addProperty(parent, propertyName,
					Messages.MarkupConfig_YamlMetadata_Enable_label );
			return;
		case AbstractMarkupConfig.TEX_MATH_DOLLARS_ENABLED_PROP:
			addProperty(parent, propertyName,
					Messages.MarkupConfig_TexMathDollars_Enable_label );
			return;
		case AbstractMarkupConfig.TEX_MATH_SBACKSLASH_ENABLED_PROP:
			addProperty(parent, propertyName,
					Messages.MarkupConfig_TexMathSBackslash_Enable_label );
			return;
		default:
			addProperty(parent, propertyName, "Enable " + propertyName);
			return;
		}
	}
	
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		final DataBindingContext dbc= db.getContext();
		
		if (this.contextLabel != null) {
			dbc.bindValue(
					WidgetProperties.buttonSelection()
							.observe(this.contextControl),
					this.contextValue );
			
			this.contextValue.addValueChangeListener(new IValueChangeListener<Boolean>() {
				@Override
				public void handleValueChange(final ValueChangeEvent<? extends Boolean> event) {
					updateContextEnabled(event.diff.getNewValue());
				}
			});
			updateContextEnabled(this.contextValue.getValue());
		}
		
		for (final Entry<String, Control> entry : this.configControls.entrySet()) {
			final Control control= entry.getValue();
			if (control instanceof Button) {
				dbc.bindValue(
						WidgetProperties.buttonSelection()
								.observe((Button)control),
						PojoProperties.value(Introspector.decapitalize(entry.getKey()), Boolean.TYPE)
								.observe(this.config) );
			}
		}
	}
	
	protected void updateContextEnabled(final boolean enabled) {
		if (this.extensionsGroup != null) {
			DialogUtils.setEnabled(this.extensionsGroup, null, enabled);
		}
	}
	
}
