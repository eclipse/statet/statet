/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.sourceediting;

import org.eclipse.swt.graphics.Font;


public class StyleConfig {
	
	
	private final Font defaultFont;
	private final Font monospaceFont;
	
	private final boolean isFixedLineHeight;
	
	
	/**
	 * @param defaultFont the default font, must not be null.
	 * @param defaultMonospaceFont the default monospace font, or null if a suitable default should
	 *     be selected
	 * @param isFixedLineHeight if font size styles should be processed
	 */
	public StyleConfig(final Font defaultFont, final Font monospaceFont,
			final boolean isFixedLineHeight) {
		if (defaultFont == null) {
			throw new NullPointerException("defaultFont"); //$NON-NLS-1$
		}
		this.defaultFont= defaultFont;
		this.monospaceFont= monospaceFont;
		this.isFixedLineHeight= isFixedLineHeight;
	}
	
	
	public Font getDefaultFont() {
		return this.defaultFont;
	}
	
	public Font getMonospaceFont() {
		return this.monospaceFont;
	}
	
	public boolean isFixedLineHeight() {
		return this.isFixedLineHeight;
	}
	
}
