/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.editors;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPartitioningException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants;

import org.eclipse.statet.docmlet.wikitext.core.ast.NodeType;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.ui.sourceediting.AbstractMarkOccurrencesProvider.RunData;


public class WikitextMarkOccurrencesLocator {
	
	
	public void run(final RunData run, final SourceUnitModelInfo info,
			final AstSelection astSelection, final @Nullable ITextSelection orgSelection)
			throws BadLocationException, BadPartitioningException, UnsupportedOperationException {
		final WikitextAstNode node= (WikitextAstNode)astSelection.getCovering();
		if (checkForAccess(run, node)) {
			return;
		}
	}
	
	private boolean checkForAccess(final RunData run, WikitextAstNode node) throws BadLocationException {
		if (node == null || !(node.getNodeType() == NodeType.LABEL)) {
			return false;
		}
		do {
			for (final Object attachment : node.getAttachments()) {
				if (attachment instanceof final WikitextNameAccess access) {
					final Map<Annotation, Position> annotations= checkDefault(run, access);
					
					if (annotations != null) {
						run.set(annotations);
						return true;
					}
				}
			}
			node= node.getWikitextParent();
		} while (node != null);
		
		return false;
	}
	
	private Map<Annotation, Position> checkDefault(final RunData run, WikitextNameAccess access) throws BadLocationException {
		while (access != null) {
			final WikitextAstNode nameNode= access.getNameNode();
			if (nameNode == null) {
				return null;
			}
			if (run.accept(new Point(nameNode.getStartOffset(), nameNode.getEndOffset()))) {
				final ImList<? extends WikitextNameAccess> accessList= access.getAllInUnit();
				final Map<Annotation, Position> annotations= new LinkedHashMap<>(accessList.size());
				for (final WikitextNameAccess occurrence : accessList) {
					final String message= run.doc.get(occurrence.getNode().getStartOffset(), occurrence.getNode().getLength());
					annotations.put(
							new Annotation((occurrence.isWriteAccess()) ?
									ITextPresentationConstants.ANNOTATIONS_WRITE_OCCURRENCES_TYPE:
									ITextPresentationConstants.ANNOTATIONS_COMMON_OCCURRENCES_TYPE,
									false, message),
							WikitextNameAccess.getTextPosition(occurrence.getNameNode()) );
				}
				return annotations;
			}
//			access= access.getNextSegment();
			access= null;
		}
		return null;
	}
	
}
