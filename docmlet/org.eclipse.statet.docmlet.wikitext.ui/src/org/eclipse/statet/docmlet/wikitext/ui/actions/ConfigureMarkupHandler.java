/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.docmlet.wikitext.ui.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupConfig;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager1;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocWorkspaceSourceUnit;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.docmlet.wikitext.ui.WikitextUI;
import org.eclipse.statet.docmlet.wikitext.ui.config.MarkupConfigUIAdapter;
import org.eclipse.statet.docmlet.wikitext.ui.editors.WikidocEditor;


public class ConfigureMarkupHandler extends AbstractHandler {
	
	
	private static class ApplyRunnable implements IRunnableWithProgress {
		
		
		private final WikidocWorkspaceSourceUnit sourceUnit;
		
		private final MarkupConfig markupConfig;
		
		
		public ApplyRunnable(final WikidocWorkspaceSourceUnit su,
				final MarkupConfig markupConfig) {
			this.sourceUnit= su;
			this.markupConfig= markupConfig;
		}
		
		@Override
		public void run(final IProgressMonitor monitor) throws InvocationTargetException,
				InterruptedException {
			final SubMonitor m= SubMonitor.convert(monitor, "Applying markup configuration...",
					20 );
			try {
				
				final IFile file= (IFile) this.sourceUnit.getResource();
				
				WikitextCore.getMarkupLanguageManager().setConfig(file, this.markupConfig);
				m.worked(20);
			}
			finally {
				m.done();
			}
		}
		
	}
	
	
	
	public ConfigureMarkupHandler() {
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPart activePart= WorkbenchUIUtils.getActivePart(event.getApplicationContext());
		
		WikitextSourceUnit su= null;
		if (activePart instanceof final WikidocEditor editor) {
			su= editor.getSourceUnit();
		}
		
		if (!(su instanceof WikidocWorkspaceSourceUnit)) {
			MessageDialog.openInformation(getShell(activePart), "Configure Markup",
					"The operation only supported for Wikitext documents in the workspace." );
			return null;
		}
		
		final IFile file= (IFile) su.getResource();
		
		final WikitextMarkupLanguageManager1 markupLanguageManager= WikitextCore.getMarkupLanguageManager();
		final WikitextMarkupLanguage activeLanguage= markupLanguageManager.getLanguage(file, null, true);
		
		if (activeLanguage != null) {
			final MarkupConfig activeConfig= activeLanguage.getMarkupConfig();
			final MarkupConfig fileConfig= markupLanguageManager.getConfig(file, null);
			
			final MarkupConfigUIAdapter ui= (activeConfig != null) ?
					(MarkupConfigUIAdapter) Platform.getAdapterManager().loadAdapter(
							activeConfig, MarkupConfigUIAdapter.class.getName()) :
					null;
			if (ui == null) {
				MessageDialog.openInformation(getShell(activePart), "Configure Markup",
						NLS.bind("The operation is not supported for {0}.",
								activeLanguage.getName() ));
				return null;
			}
			final MarkupConfig config= activeConfig.clone();
			final AtomicBoolean enabled= new AtomicBoolean(fileConfig != null);
			
			if (ui.edit("document", enabled, config, getShell(activePart))) {
				try {
					activePart.getSite().getWorkbenchWindow().run(true, true,
							new ApplyRunnable((WikidocWorkspaceSourceUnit) su,
							(enabled.get()) ? config : null ));
				}
				catch (final InterruptedException e) {}
				catch (final Exception e) {
					StatusManager.getManager().handle(new Status(IStatus.ERROR, WikitextUI.BUNDLE_ID,
							"An error occurred when setting markup configuration.",
							e ));
				}
			}
		}
		
		return null;
	}
	
	protected Shell getShell(final IWorkbenchPart part) {
		if (part != null) {
			final IWorkbenchPartSite site= part.getSite();
			if (site != null) {
				return site.getShell();
			}
		}
		return null;
	}
	
	
}
