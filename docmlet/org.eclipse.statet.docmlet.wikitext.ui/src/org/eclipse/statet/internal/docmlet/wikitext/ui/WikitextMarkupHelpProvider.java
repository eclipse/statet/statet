/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedMap;

import org.eclipse.mylyn.internal.wikitext.ui.WikiTextUiPlugin;
import org.eclipse.mylyn.internal.wikitext.ui.editor.help.HelpContent;
import org.eclipse.mylyn.wikitext.parser.markup.MarkupLanguage;
import org.eclipse.mylyn.wikitext.ui.WikiText;

import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.base.ui.markuphelp.MarkupHelpContent;
import org.eclipse.statet.docmlet.base.ui.markuphelp.MarkupHelpContentProvider;


@SuppressWarnings("restriction")
public class WikitextMarkupHelpProvider implements MarkupHelpContentProvider {
	
	
	public static final String NS= "wikitext"; //$NON-NLS-1$
	
	private static final String NS_= NS + ':';
	
	
	private static class WikitextHelpTopic extends MarkupHelpContent {
		
		
		private HelpContent mylynHelp;
		
		private String content;
		
		
		public WikitextHelpTopic(final HelpContent mylynHelp) {
			super(NS_ + mylynHelp.getMarkupLanguageName(), mylynHelp.getMarkupLanguageName());
			
			this.mylynHelp= mylynHelp;
		}
		
		@Override
		public String getContent() throws IOException {
			if (this.mylynHelp != null) {
				try {
					this.content= this.mylynHelp.getContent();
				}
				finally {
					this.mylynHelp= null;
				}
			}
			return this.content;
		}
		
	}
	
	public static @Nullable String getContentIdFor(MarkupLanguage markupLanguage) {
		final SortedMap<String, HelpContent> mylynHelps= WikiTextUiPlugin.getDefault().getCheatSheets();
		
		while (markupLanguage != null
				&& !mylynHelps.containsKey(markupLanguage.getName()) ) {
			markupLanguage= WikiText.getMarkupLanguage(markupLanguage.getExtendsLanguage());
		}
		
		return (markupLanguage != null) ? (NS_ + markupLanguage.getName()) : null;
	}
	
	
	public WikitextMarkupHelpProvider() {
	}
	
	
	@Override
	public synchronized Collection<MarkupHelpContent> getHelpTopics() {
		final SortedMap<String, HelpContent> mylynHelps= WikiTextUiPlugin.getDefault().getCheatSheets();
		
		final List<MarkupHelpContent> topics= new ArrayList<>(mylynHelps.size());
		for (final HelpContent mylynHelp : mylynHelps.values()) {
			topics.add(new WikitextHelpTopic(mylynHelp));
		}
		return topics;
	}
	
}
