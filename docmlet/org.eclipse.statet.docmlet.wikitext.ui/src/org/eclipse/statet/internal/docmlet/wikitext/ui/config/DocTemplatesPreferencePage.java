/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.config;

import org.eclipse.core.runtime.CoreException;

import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;

import org.eclipse.statet.ltk.ui.templates.config.CodeTemplateConfigurationBlock;
import org.eclipse.statet.ltk.ui.templates.config.CodeTemplateConfigurationRegistry;


public class DocTemplatesPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	private final static String EXTENSION_POINT_ID= "org.eclipse.statet.docmlet.WikitextDocTemplates"; //$NON-NLS-1$
	
	
	public DocTemplatesPreferencePage() {
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() throws CoreException {
		final CodeTemplateConfigurationRegistry registry= new CodeTemplateConfigurationRegistry(EXTENSION_POINT_ID);
		
		return new CodeTemplateConfigurationBlock(Messages.DocTemplates_title,
				CodeTemplateConfigurationBlock.ADD_ITEM
						| CodeTemplateConfigurationBlock.LAZY_LOADING
						| CodeTemplateConfigurationBlock.DEFAULT_BY_CATEGORY,
				registry.getCategories(),
				null );
	}
	
}
