/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting;

import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.mylyn.internal.wikitext.ui.editor.assist.SourceTemplateContextType;
import org.eclipse.mylyn.internal.wikitext.ui.editor.assist.WikiTextTemplateAccess;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguageDocumentSetupParticipant;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateCompletionComputer;


@NonNullByDefault
public class WikitextTemplateAssistComputer extends TemplateCompletionComputer {
	
	
	private @Nullable WikitextMarkupLanguage markupLanguage;
	
	
	@SuppressWarnings("restriction")
	public WikitextTemplateAssistComputer() {
		super(WikiTextTemplateAccess.getInstance().getTemplateStore(),
				WikiTextTemplateAccess.getInstance().getContextTypeRegistry() );
	}
	
	
	@Override
	public void onSessionStarted(final SourceEditor editor, final ContentAssist assist) {
		super.onSessionStarted(editor, assist);
		
		this.markupLanguage= MarkupLanguageDocumentSetupParticipant.getMarkupLanguage(
				editor.getViewer().getDocument(), editor.getDocumentContentInfo().getPartitioning() );
	}
	
	@Override
	public void onSessionEnded() {
		super.onSessionEnded();
		
		this.markupLanguage= null;
	}
	
	protected WikitextMarkupLanguage getMarkupLanguage() {
		return this.markupLanguage;
	}
	
	
	@Override
	@SuppressWarnings("restriction")
	protected @Nullable TemplateContextType getContextType(
			final AssistInvocationContext context, final TextRegion region) {
		return getTypeRegistry().getContextType(SourceTemplateContextType.ID);
	}
	
}
