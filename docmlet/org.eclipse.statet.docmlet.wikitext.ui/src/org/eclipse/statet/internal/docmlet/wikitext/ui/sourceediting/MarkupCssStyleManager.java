/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting;

import org.eclipse.mylyn.internal.wikitext.ui.viewer.CssStyleManager;
import org.eclipse.mylyn.internal.wikitext.ui.viewer.FontState;
import org.eclipse.mylyn.wikitext.parser.css.CssRule;
import org.eclipse.swt.custom.StyleRange;

import org.eclipse.statet.docmlet.wikitext.ui.sourceediting.StyleConfig;


public class MarkupCssStyleManager extends CssStyleManager {
	
	
	private final boolean disableFontConfig;
	
	
	public MarkupCssStyleManager(final StyleConfig config) {
		super(config.getDefaultFont(), config.getMonospaceFont());
		
		this.disableFontConfig= config.isFixedLineHeight();
	}
	
	
	@Override
	public void processCssStyles(final FontState fontState, final FontState parentFontState, final CssRule rule) {
		if (this.disableFontConfig && 
				(RULE_FONT_SIZE.equals(rule.name) || RULE_FONT_FAMILY.equals(rule.name)) ) {
			return;
		}
		super.processCssStyles(fontState, parentFontState, rule);
	}
	
	@Override
	public StyleRange createStyleRange(final FontState fontState, final int offset, final int length) {
//		if (this.disableFontSize && fontState.sizeFactor != 1.0f) {
//			fontState= new FontState(fontState);
//			fontState.sizeFactor= 1.0f;
//		}
		return super.createStyleRange(fontState, offset, length);
	}
	
}
