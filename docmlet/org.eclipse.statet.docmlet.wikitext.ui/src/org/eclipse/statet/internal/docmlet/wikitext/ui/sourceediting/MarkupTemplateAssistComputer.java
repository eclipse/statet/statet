/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting;

import java.util.List;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.SearchPattern;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.ui.SharedUIResources;

import org.eclipse.statet.docmlet.wikitext.ui.WikitextElementLabelProvider;
import org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting.MarkupTemplates.MarkupInfo;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.TemplateProposal.TemplateProposalParameters;
import org.eclipse.statet.ltk.ui.templates.SourceEditorTemplateContext;


@NonNullByDefault
public class MarkupTemplateAssistComputer extends WikitextTemplateAssistComputer {
	
	
	private MarkupTemplates markupTemplates;
	
	private final WikitextElementLabelProvider labelProvider;

	private @Nullable IRegion startLineInfo;
	
	
	public MarkupTemplateAssistComputer() {
		this.labelProvider= new WikitextElementLabelProvider();
	}
	
	
	@Override
	public void onSessionStarted(final SourceEditor editor, final ContentAssist assist) {
		super.onSessionStarted(editor, assist);
		
		this.markupTemplates= MarkupTemplatesManager.INSTANCE.getTemplates(getMarkupLanguage());
	}
	
	
	@Override
	protected boolean handleRequest(final int mode, final String prefix) {
		return true;
	}
	
	@Override
	protected List<Template> getTemplates(final String contextTypeId) {
		return this.markupTemplates.getTemplates();
	}
	
	private boolean isBlockTemplate(final Template template) {
		return (this.markupTemplates.getMarkupInfo(template) != null);
	}
	
	@Override
	protected boolean include(final Template template, final TemplateProposalParameters<?> parameters) {
		if (super.include(template, parameters)) {
			return true;
		}
		if (!isBlockTemplate(template)) {
			parameters.matchRule= SearchPattern.OTHER_MATCH;
			return true;
		}
		return false;
	}
	
	@Override
	protected @Nullable Image getImage(final Template template) {
		final MarkupInfo markupInfo= this.markupTemplates.getMarkupInfo(template);
		if (markupInfo != null) {
			if (markupInfo.getElementType() != 0) {
				return this.labelProvider.getImage(markupInfo);
			}
		}
		return SharedUIResources.getImages().get(SharedUIResources.PLACEHOLDER_IMAGE_ID);
	}
	
	@Override
	protected @Nullable SourceEditorTemplateContext createTemplateContext(
			final AssistInvocationContext context, final TextRegion region,
			final int flags) {
		try {
			this.startLineInfo= context.getDocument().getLineInformationOfOffset(context.getInvocationOffset());
		}
		catch (final Exception e) {
			this.startLineInfo= null;
		}
		
		return super.createTemplateContext(context, region, flags);
	}
	
	@Override
	protected TemplateProposal createProposal(final TemplateProposalParameters<?> parameters) {
		IRegion lineInfo;
		if (parameters.baseRelevance > 0
				&& (lineInfo= this.startLineInfo) != null
				&& lineInfo.getOffset() == parameters.templateContext.getStart()
				&& isBlockTemplate(parameters.template) ) {
			// block at line start
			parameters.baseRelevance+= 10;
		}
		return super.createProposal(parameters);
	}
	
}
