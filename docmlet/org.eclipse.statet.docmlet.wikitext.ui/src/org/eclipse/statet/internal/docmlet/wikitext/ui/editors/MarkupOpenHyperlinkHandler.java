/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.text.IRegion;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.ast.Link;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.util.AstSelection;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public class MarkupOpenHyperlinkHandler {
	
	
	static @Nullable Link searchLink(final WikitextSourceUnit sourceUnit, final IRegion region) {
		final AstInfo astInfo= sourceUnit.getAstInfo(WikitextModel.WIKIDOC_TYPE_ID,
				true, new NullProgressMonitor() );
		if (astInfo != null) {
			final AstSelection astSelection= AstSelection.search(astInfo.getRoot(),
					region.getOffset(), region.getOffset() + region.getLength(),
					AstSelection.MODE_COVERING_SAME_LAST );
			AstNode node= astSelection.getCovering();
			while (node instanceof WikitextAstNode) {
				if (node instanceof Link) {
					return (Link) node;
				}
				node= node.getParent();
			}
		}
		return null;
	}
	
	static List<IFile> refLocalFile(final SourceEditor editor, final Link link) {
		final String href= link.getUri();
		if (href != null && href.indexOf(':') < 0 && href.length() > 1 && href.charAt(0) != '/') {
			try {
				final Object resource= editor.getSourceUnit().getResource();
				if (resource instanceof final IFile currentFile) {
					final IFile refFile= currentFile.getParent().getFile(new Path(href));
					if (refFile != null) {
						final List<IFile> files= new ArrayList<>();
						if (refFile.exists()) {
							files.add(refFile);
						}
						else {
							final IContainer parent= refFile.getParent();
							final int dotIdx;
							if (parent.exists() && (dotIdx= refFile.getName().lastIndexOf('.')) >= 0) {
								final String mainName= refFile.getName().substring(0, dotIdx + 1);
								final IResource[] children= parent.members();
								for (final IResource child : children) {
									if (child.getType() == IResource.FILE
											&& child.getName().startsWith(mainName)) {
										files.add((IFile) child);
									}
								}
							}
						}
						return files;
					}
				}
			}
			catch (final CoreException e) {}
		}
		return ImCollections.emptyList();
	}
	
	
	public MarkupOpenHyperlinkHandler() {
	}
	
	
}
