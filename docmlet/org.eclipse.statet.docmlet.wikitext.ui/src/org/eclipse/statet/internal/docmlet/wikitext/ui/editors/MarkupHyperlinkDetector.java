/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.hyperlink.AbstractHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.docmlet.wikitext.core.ast.Link;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.util.OpenWorkspaceFileHyperlink;


@NonNullByDefault
public class MarkupHyperlinkDetector extends AbstractHyperlinkDetector {
	
	
	public MarkupHyperlinkDetector() {
	}
	
	
	@Override
	public @NonNull IHyperlink @Nullable [] detectHyperlinks(final ITextViewer textViewer, final IRegion region,
			final boolean canShowMultipleHyperlinks) {
		final SourceEditor editor= getAdapter(SourceEditor.class);
		if (editor == null) {
			return null;
		}
		final WikitextSourceUnit sourceUnit= WikitextModel.asWikitextSourceUnit(editor.getSourceUnit());
		if (sourceUnit == null) {
			return null;
		}
		
		final List<IHyperlink> hyperlinks= new ArrayList<>(4);
		
		final Link link= MarkupOpenHyperlinkHandler.searchLink(sourceUnit, region);
		if (link != null) {
			if (link.getLinkType() == Link.LINK_BY_REF) {
				for (final Object attachment : link.getAttachments()) {
					if (attachment instanceof WikitextNameAccess) {
						hyperlinks.add(new OpenMarkupElementHyperlink(editor, sourceUnit,
								(WikitextNameAccess)attachment ));
						break;
					}
				}
			}
			else {
				final String uri= link.getUri();
				if (uri != null && !uri.isEmpty()) {
					if (uri.charAt(0) == '#') {
						if (uri.length() > 1) {
							hyperlinks.add(new OpenMarkupElementHyperlink(editor, sourceUnit,
									link, uri.substring(1) ));
						}
					}
					else {
						final List<IFile> files= MarkupOpenHyperlinkHandler.refLocalFile(editor, link);
						for (final IFile file : files) {
							hyperlinks.add(new OpenWorkspaceFileHyperlink(link, file));
						}
					}
				}
			}
		}
		
		if (!hyperlinks.isEmpty()) {
			return hyperlinks.toArray(new @NonNull IHyperlink[hyperlinks.size()]);
		}
		return null;
	}
	
}
