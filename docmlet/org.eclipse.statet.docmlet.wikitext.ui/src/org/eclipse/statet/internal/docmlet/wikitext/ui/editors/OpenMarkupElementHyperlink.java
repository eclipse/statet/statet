/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.editors;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;

import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;

import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextModel;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceUnit;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.actions.OpenDeclaration;


public class OpenMarkupElementHyperlink implements IHyperlink {
	
	
	private final SourceEditor editor;
	private final IRegion region;
	
	private final WikitextSourceUnit sourceUnit;
	private final WikitextNameAccess access;
	private final String label;
	
	
	public OpenMarkupElementHyperlink(final SourceEditor editor, final WikitextSourceUnit sourceUnit,
			final TextRegion region, final String label) {
		assert (sourceUnit != null);
		assert (label != null);
		
		this.editor= editor;
		this.region= JFaceTextRegion.toJFaceRegion(region);
		this.sourceUnit= sourceUnit;
		this.access= null;
		this.label= label;
	}
	
	public OpenMarkupElementHyperlink(final SourceEditor editor, final WikitextSourceUnit sourceUnit,
			final WikitextNameAccess access) {
		assert (sourceUnit != null);
		assert (access != null);
		
		this.editor= editor;
		this.region= JFaceTextRegion.toJFaceRegion(
				(access.getNameNode() != null) ? access.getNameNode() : access.getNode() );
		this.sourceUnit= sourceUnit;
		this.access= access;
		this.label= this.access.getSegmentName();
	}
	
	
	@Override
	public String getTypeLabel() {
		return null;
	}
	
	@Override
	public IRegion getHyperlinkRegion() {
		return this.region;
	}
	
	@Override
	public String getHyperlinkText() {
		return NLS.bind(Messages.Hyperlinks_OpenDeclaration_label, this.label);
	}
	
	@Override
	public void open() {
		final WikidocSourceUnitModelInfo modelInfo= (WikidocSourceUnitModelInfo)this.sourceUnit.getModelInfo(
				WikitextModel.WIKIDOC_TYPE_ID, ModelManager.MODEL_FILE, new NullProgressMonitor() );
		if (modelInfo != null) {
			final int type= (this.access != null) ? this.access.getType() : WikitextNameAccess.LINK_ANCHOR_LABEL;
			final NameAccessSet<WikitextNameAccess> labels;
			switch (type) {
			case WikitextNameAccess.LINK_DEF_LABEL:
				labels= modelInfo.getLinkRefLabels();
				break;
			case WikitextNameAccess.LINK_ANCHOR_LABEL:
				labels= modelInfo.getLinkAnchorLabels();
				break;
			default:
				return;
			}
			final OpenDeclaration open= new OpenDeclaration();
			final WikitextNameAccess declAccess= open.selectAccess(labels.getAllInUnit(this.label));
			if (declAccess != null) {
				open.open(this.editor, declAccess);
				return;
			}
			Display.getCurrent().beep();
		}
	}
	
}
