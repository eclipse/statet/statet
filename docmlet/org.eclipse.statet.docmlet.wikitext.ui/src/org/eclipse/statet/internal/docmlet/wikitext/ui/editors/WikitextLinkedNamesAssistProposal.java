/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.editors;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.link.LinkedPositionGroup;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImIdentityList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.ltk.ui.LtkActions;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.LinkedNamesAssistProposal;


@NonNullByDefault
public class WikitextLinkedNamesAssistProposal extends LinkedNamesAssistProposal<AssistInvocationContext> {
	
	
	public static final int IN_FILE= 1;
//	public static final int IN_FILE_PRECEDING= 2;
//	public static final int IN_FILE_FOLLOWING= 3;
	
	
	private final WikitextNameAccess access;
	
	private final int mode;
	
	
	public WikitextLinkedNamesAssistProposal(final int mode,
			final AssistInvocationContext invocationContext, final WikitextNameAccess access) {
		super(init(invocationContext, mode, false));
		this.mode= mode;
		this.access= access;
	}
	
	private static ProposalParameters<AssistInvocationContext> init(
			final AssistInvocationContext invocationContext,
			final int mode, final boolean withRegion) {
		switch (mode) {
		case IN_FILE:
			return new ProposalParameters<>(invocationContext,
					LtkActions.QUICK_ASSIST_RENAME_IN_FILE,
					Messages.Proposal_RenameInFile_label,
					Messages.Proposal_RenameInFile_description,
					90 );
		default:
			throw new IllegalArgumentException();
		}
	}
	
	
	@Override
	protected void collectPositions(final IDocument document, final LinkedPositionGroup group)
			throws BadLocationException {
		final ImIdentityList<? extends WikitextNameAccess> allAccess= ImCollections.toIdentityList(
				this.access.getAllInUnit() );
		final int current= allAccess.indexOf(this.access);
		if (current < 0) {
			return;
		}
		
		int idx= 0;
		{	final var nameNode= nonNullAssert(this.access.getNameNode());
			idx= addPosition(group, document, nameNode, idx);
			if (idx == 0) {
				return;
			}
		}
		if (this.mode == IN_FILE) {
			for (int i= current + 1; i < allAccess.size(); i++) {
				final var nameNode= allAccess.get(i).getNameNode();
				if (nameNode != null) {
					idx= addPosition(group, document, nameNode, idx);
				}
			}
		}
		if (this.mode == IN_FILE) {
			for (int i= 0; i < current; i++) {
				final var nameNode= allAccess.get(i).getNameNode();
				if (nameNode != null) {
					idx= addPosition(group, document, nameNode, idx);
				}
			}
		}
	}
	
	protected int addPosition(final LinkedPositionGroup group, final IDocument document,
			final WikitextAstNode nameNode, final int idx) throws BadLocationException {
		return addPosition(group, document, WikitextNameAccess.getTextPosition(nameNode), idx);
	}
	
}
