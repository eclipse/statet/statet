/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting;

import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.ITokenScanner;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartition;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;


public class MarkupDamagerRepairer extends DefaultDamagerRepairer {
	
	
	public MarkupDamagerRepairer(final ITokenScanner scanner) {
		super(scanner);
	}
	
	
	@Override
	public IRegion getDamageRegion(final ITypedRegion partition, final DocumentEvent e,
			final boolean documentPartitioningChanged) {
		if (partition instanceof TreePartition) {
			final TreePartitionNode treeNode= ((TreePartition) partition).getTreeNode();
			if (treeNode.getParent() != null) {
				return JFaceTextRegion.toJFaceRegion(treeNode);
			}
		}
		return partition;
	}
	
}
