/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting;

import static org.eclipse.statet.docmlet.wikitext.core.ast.NodeType.LINK;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.SearchPattern;

import org.eclipse.statet.docmlet.base.core.DocmlSearchPattern;
import org.eclipse.statet.docmlet.base.ui.DocmlBaseUIResources;
import org.eclipse.statet.docmlet.wikitext.core.ast.Link;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;
import org.eclipse.statet.docmlet.wikitext.core.model.WikidocSourceUnitModelInfo;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextElement;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextSourceElement;
import org.eclipse.statet.docmlet.wikitext.core.source.doc.MarkupLanguageDocumentSetupParticipant;
import org.eclipse.statet.docmlet.wikitext.ui.sourceediting.MarkupCompletionExtension;
import org.eclipse.statet.ltk.model.core.element.NameAccessSet;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssist;
import org.eclipse.statet.ltk.ui.sourceediting.assist.ContentAssistComputer;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SimpleCompletionProposal;
import org.eclipse.statet.ltk.ui.sourceediting.assist.SourceProposal.ProposalParameters;


@NonNullByDefault
public class MarkupLabelCompletionComputer implements ContentAssistComputer, MarkupCompletionExtension {
	
	
	private static class LabelCompletionProposal extends SimpleCompletionProposal</*Wikitext*/AssistInvocationContext> {
		
		
		private final @Nullable WikitextNameAccess defAccess;
		private final @Nullable WikitextSourceElement refElement;
		
		
		public LabelCompletionProposal(final ProposalParameters<?> parameters,
				final String replacementString,
				final @Nullable WikitextNameAccess defAccess, final @Nullable WikitextSourceElement refElement) {
			super(parameters, replacementString);
			
			this.defAccess= defAccess;
			this.refElement= refElement;
		}
		
		
		@Override
		protected int computeReplacementLength(final int replacementOffset, final Point selection,
				final int caretOffset, final boolean overwrite) throws BadLocationException {
			int end= Math.max(caretOffset, selection.x + selection.y);
			if (overwrite) {
				final IDocument document= getInvocationContext().getDocument();
				end--;
				while (++end < document.getLength()) {
					final char c= document.getChar(end);
					if (Character.isLetterOrDigit(c) || c == '-' || c == '_' || c == ':' || c == '.') {
						continue;
					}
					break;
				}
			}
			return (end - replacementOffset);
		}
		
		
		@Override
		protected StyledString computeStyledText() {
			final StyledString styledText= new StyledString(getDisplayString());
			final String title= getTitle();
			if (title != null) {
				styledText.append(QUALIFIER_SEPARATOR, StyledString.QUALIFIER_STYLER);
				styledText.append(title, StyledString.QUALIFIER_STYLER);
			}
			return styledText;
		}
		
		private @Nullable String getTitle() {
			if (this.refElement != null) {
				final String name= this.refElement.getElementName().getDisplayName();
				if (name != null) {
					if ((this.refElement.getElementType() & WikitextElement.MASK_C12) == WikitextElement.C12_SECTIONING) {
						final StringBuilder sb= new StringBuilder();
						sb.append("h:"); //$NON-NLS-1$
						sb.append(Integer.toString((this.refElement.getElementType() & 0xf)));
						sb.append(" "); //$NON-NLS-1$
						sb.append(name);
						return sb.toString();
					}
					return name;
				}
			}
			if (this.defAccess != null) {
				final WikitextAstNode node= this.defAccess.getNode();
				if (node != null && node.getNodeType() == LINK) {
					final Link link= (Link) node;
					if (link.getLinkType() == Link.LINK_REF_DEFINITION) {
						return link.getTitle();
					}
				}
			}
			return null;
		}
		
		@Override
		public Image getImage() {
			return DocmlBaseUIResources.INSTANCE.getImage(DocmlBaseUIResources.OBJ_LABEL_TEXT_IMAGE_ID);
		}
		
		
		@Override
		public boolean isAutoInsertable() {
			return false;
		}
		
		
		@Override
		public @Nullable String getAdditionalProposalInfo() {
			if (this.refElement == null && this.defAccess != null) {
				final WikitextAstNode node= this.defAccess.getNode();
				if (node != null && node.getNodeType() == LINK) {
					final Link link= (Link) node;
					if (link.getLinkType() == Link.LINK_REF_DEFINITION) {
						return link.getUri();
					}
				}
			}
			return null;
		}
		
	}
	
	
	private WikitextMarkupLanguage markupLanguage;
	
	private MarkupCompletionExtension completionExtension;
	
	private int searchMatchRules;
	
	
	@SuppressWarnings("null")
	public MarkupLabelCompletionComputer() {
	}
	
	
	@Override
	public void onSessionStarted(final SourceEditor editor, final ContentAssist assist) {
		this.markupLanguage= MarkupLanguageDocumentSetupParticipant.getMarkupLanguage(
				editor.getViewer().getDocument(), editor.getDocumentContentInfo().getPartitioning() );
		if (this.markupLanguage != null) {
			this.completionExtension= Platform.getAdapterManager().getAdapter(this.markupLanguage, MarkupCompletionExtension.class);
		}
		
		int matchRules= SearchPattern.PREFIX_MATCH;
		if (assist.getShowSubstringMatches()) {
			matchRules |= SearchPattern.SUBSTRING_MATCH;
		}
		this.searchMatchRules= matchRules;
	}
	
	@Override
	public void onSessionEnded() {
		this.markupLanguage= null;
		this.completionExtension= null;
	}
	
	
	protected WikitextMarkupLanguage getMarkupLanguage() {
		return this.markupLanguage;
	}
	
	protected int getSearchMatchRules() {
		return this.searchMatchRules;
	}
	
	protected int getLabelSearchMatchRules() {
		int rules= this.searchMatchRules;
		if ((rules & DocmlSearchPattern.SUBSTRING_MATCH) != 0) {
			rules|= DocmlSearchPattern.LABEL_SUBSTRING_MATCH;
		}
		return rules;
	}
	
	
	@Override
	public void computeCompletionProposals(final AssistInvocationContext context, final int mode,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		final WikitextMarkupLanguage markupLanguage= this.markupLanguage;
		final MarkupCompletionExtension ext= (this.completionExtension != null) ? this.completionExtension : this;
		final WikidocSourceUnitModelInfo modelInfo= (WikidocSourceUnitModelInfo)context.getModelInfo();
		
		{	final CompletionType type= ext.getLinkAnchorLabel(context, markupLanguage);
			if (type != null) {
				addLabelProposals(context, type, modelInfo.getLinkAnchorLabels(), proposals);
			}
		}
		{	final CompletionType type= ext.getLinkRefLabel(context, markupLanguage);
			if (type != null) {
				addLabelProposals(context, type, modelInfo.getLinkRefLabels(), proposals);
			}
		}
	}
	
	@Override
	public void computeInformationProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
	}
	
	
	private void addLabelProposals(final AssistInvocationContext context,
			final CompletionType type, final NameAccessSet<WikitextNameAccess> labels,
			final AssistProposalCollector proposals) {
		final ProposalParameters<?> parameters= new ProposalParameters<>(context,
				context.getInvocationOffset() - type.getSourcePrefix().length(),
				new DocmlSearchPattern(getLabelSearchMatchRules(), type.getLookupPrefix()) );
		
		for (final String label : labels.getNames()) {
			if (parameters.matchesNamePattern(label)) {
				final ImList<WikitextNameAccess> accessList= labels.getAllInUnit(label);
				if (accessList.size() == 1
						&& isCurrent(accessList.getFirst().getNameNode(), context.getInvocationOffset()) ) {
					continue;
				}
				
				WikitextNameAccess defAccess= null;
				WikitextSourceElement defElement= null;
				ITER_ACCESS: for (final WikitextNameAccess access : accessList) {
					if (access.isWriteAccess()) {
						for (final Object attachment : access.getNode().getAttachments()) {
							if (attachment instanceof WikitextSourceElement) {
								defAccess= access;
								defElement= (WikitextSourceElement) attachment;
								break ITER_ACCESS;
							}
						}
						if (defAccess == null) {
							defAccess= access;
						}
					}
				}
				parameters.baseRelevance= (defAccess != null) ? 95 : 94;
				proposals.add(new LabelCompletionProposal(parameters,
						((defAccess != null) ? defAccess : accessList.getFirst()).getDisplayName(),
						defAccess, defElement ));
			}
		}
	}
	
	private boolean isCurrent(final WikitextAstNode node, final int offset) {
		return (node != null
				&& node.getStartOffset() <= offset && node.getEndOffset() >= offset);
	}
	
	
	@Override
	public @Nullable CompletionType getLinkAnchorLabel(
			final AssistInvocationContext context, final WikitextMarkupLanguage markupLanguage) {
		try {
			final IDocument document= context.getDocument();
			final int endOffset= context.getOffset();
			int startOffset= endOffset;
			while (startOffset > 0) {
				final char c= document.getChar(--startOffset);
				if (Character.isLetterOrDigit(c) || c == '-' || c == '_' || c == ':' || c == '.') {
					continue;
				}
				if (c == '#') {
					startOffset++;
					return new CompletionType(document.get(startOffset, endOffset - startOffset));
				}
			}
			return null;
		}
		catch (final BadLocationException e) {
			return null;
		}
	}
	
	@Override
	public @Nullable CompletionType getLinkRefLabel(
			final AssistInvocationContext context, final WikitextMarkupLanguage markupLanguage) {
		return null;
	}
	
}
