/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultTextDoubleClickStrategy;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextUtilities;

import org.eclipse.statet.ecommons.text.core.JFaceTextRegion;
import org.eclipse.statet.ecommons.text.core.PartitionConstraint;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartition;
import org.eclipse.statet.ecommons.text.core.treepartitioner.TreePartitionNode;

import org.eclipse.statet.docmlet.wikitext.core.source.doc.WikitextDocumentConstants;


public class MarkupDoubleClickStrategy extends DefaultTextDoubleClickStrategy {
	
	
	private final String partitioning;
	
	private final PartitionConstraint partitionConstrait;
	
	
	public MarkupDoubleClickStrategy(final String partitioning) {
		if (partitioning == null) {
			throw new NullPointerException("partitioning"); //$NON-NLS-1$
		}
		this.partitioning= partitioning;
		this.partitionConstrait= WikitextDocumentConstants.WIKIDOC_DEFAULT_CONTENT_CONSTRAINT;
	}
	
	
	@Override
	protected IRegion findExtendedDoubleClickSelection(final IDocument document, final int offset) {
		try {
			final ITypedRegion partition= TextUtilities.getPartition(document, this.partitioning,
					offset, false );
			if (partition instanceof TreePartition
					&& this.partitionConstrait.matches(partition.getType()) ) {
				final TreePartitionNode treeNode= ((TreePartition) partition).getTreeNode();
				if (offset == treeNode.getStartOffset()) {
					return JFaceTextRegion.toJFaceRegion(treeNode);
				}
			}
		}
		catch (final BadLocationException e) {
		}
		return null;
	}
	
}
