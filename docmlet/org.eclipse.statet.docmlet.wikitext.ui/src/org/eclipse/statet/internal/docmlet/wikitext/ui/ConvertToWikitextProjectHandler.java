/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.ui.util.UIAccess;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.docmlet.wikitext.core.project.WikitextProjects;
import org.eclipse.statet.docmlet.wikitext.ui.WikitextUI;


public class ConvertToWikitextProjectHandler extends AbstractHandler {
	
	
	public ConvertToWikitextProjectHandler() {
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		final List<IProject> projects;
		if (selection instanceof final IStructuredSelection structuredSelection) {
			projects= new ArrayList<>(structuredSelection.size());
			for (final Iterator<?> iter= structuredSelection.iterator(); iter.hasNext(); ) {
				final Object obj= iter.next();
				if (obj instanceof IAdaptable) {
					final IProject project= ((IAdaptable) obj).getAdapter(IProject.class);
					if (project != null) {
						projects.add(project);
					}
				}
			}
		}
		else {
			return null;
		}
		
		final WorkspaceModifyOperation op= new WorkspaceModifyOperation() {
			
			@Override
			protected void execute(final IProgressMonitor monitor) throws CoreException,
					InvocationTargetException, InterruptedException {
				final SubMonitor m= SubMonitor.convert(monitor,
						Messages.WikitextProject_ConvertTask_label,
						20 );
				try {
					final SubMonitor mProjects= m.newChild(20).setWorkRemaining(projects.size());
					for (final IProject project : projects) {
						if (m.isCanceled()) {
							throw new InterruptedException();
						}
						WikitextProjects.setupWikitextProject(project, mProjects.newChild(1));
					}
				}
				finally {
					m.done();
				}
			}
			
		};
		try {
			UIAccess.getActiveWorkbenchWindow(true).run(true, true, op);
		}
		catch (final InvocationTargetException e) {
			StatusManager.getManager().handle(new Status(IStatus.ERROR, WikitextUI.BUNDLE_ID,
					Messages.WikitextProject_ConvertTask_error_message, e.getTargetException() ));
		}
		catch (final InterruptedException e) {
			// cancelled
		}
		
		return null;
	}
	
}
