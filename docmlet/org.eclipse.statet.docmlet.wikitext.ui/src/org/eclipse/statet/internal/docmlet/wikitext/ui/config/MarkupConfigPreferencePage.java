/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlock;
import org.eclipse.statet.ecommons.preferences.ui.ConfigurationBlockPreferencePage;
import org.eclipse.statet.ecommons.preferences.ui.ManagedConfigurationBlock;
import org.eclipse.statet.ecommons.runtime.core.StatusChangeListener;
import org.eclipse.statet.ecommons.ui.components.ButtonGroup;
import org.eclipse.statet.ecommons.ui.components.ButtonGroup.SelectionHandler;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils;

import org.eclipse.statet.docmlet.wikitext.core.WikitextCore;
import org.eclipse.statet.docmlet.wikitext.core.markup.MarkupConfig;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager.WikitextMarkupLanguageDescriptor;
import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguageManager1;
import org.eclipse.statet.docmlet.wikitext.ui.config.MarkupConfigUIAdapter;


@NonNullByDefault
public class MarkupConfigPreferencePage extends ConfigurationBlockPreferencePage {
	
	
	public MarkupConfigPreferencePage() {
	}
	
	
	@Override
	protected ConfigurationBlock createConfigurationBlock() throws CoreException {
		return new MarkupConfigurationBlock(null, createStatusChangedListener());
	}
	
}


@NonNullByDefault
class MarkupConfigurationBlock extends ManagedConfigurationBlock {
	
	
	private class MarkupEntry {
		
		
		private final WikitextMarkupLanguageDescriptor descriptor;
		
		private final Preference<@Nullable String> pref;
		
		
		public MarkupEntry(final WikitextMarkupLanguageDescriptor descriptor) {
			this.descriptor= descriptor;
			this.pref= new Preference.NullableStringPref(descriptor.getPreferenceQualifier(),
					"MarkupConfig.Workbench.config" ); //$NON-NLS-1$
		}
		
		
		public WikitextMarkupLanguageDescriptor getDescriptor() {
			return this.descriptor;
		}
		
		public String getLabel() {
			return this.descriptor.getLabel();
		}
		
		public Preference<@Nullable String> getPref() {
			return this.pref;
		}
		
	}
	
	
	private WikitextMarkupLanguageManager1 markupLanguageManager;
	
	private List<MarkupEntry> markupEntries;
	
	private TableViewer markupEntriesControl;
	private ButtonGroup<MarkupEntry> markupEntriesButtons;
	
	
	public MarkupConfigurationBlock(final @Nullable IProject project,
			final @Nullable StatusChangeListener statusListener) {
		super(project, statusListener);
	}
	
	
	@Override
	protected void createBlockArea(final Composite pageComposite) {
		this.markupLanguageManager= WikitextCore.getMarkupLanguageManager();
		
		final Map<Preference<?>, @Nullable String> prefs= new HashMap<>();
		
		final List<String> languageNames= this.markupLanguageManager.getLanguageNames();
		this.markupEntries= new ArrayList<>(languageNames.size());
		for (final String languageName : languageNames) {
			final WikitextMarkupLanguageDescriptor languageDescriptor= this.markupLanguageManager
					.getLanguageDescriptor(languageName);
			if (languageDescriptor.isConfigSupported()
					&& languageDescriptor.getPreferenceQualifier() != null) {
				final MarkupEntry entry= new MarkupEntry(languageDescriptor);
				this.markupEntries.add(entry);
				prefs.put(entry.getPref(), null);
			}
		}
		
		setupPreferenceManager(prefs);
		
		final Composite mainComposite= new Composite(pageComposite, SWT.NONE);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		mainComposite.setLayout(LayoutUtils.newCompositeGrid(2));
		
		{	final Label label= new Label(mainComposite, SWT.LEFT);
			label.setText(Messages.MarkupConfigs_label);
			label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		}
		{	final TableViewer viewer= new TableViewer(mainComposite);
			viewer.setContentProvider(new ArrayContentProvider());
			viewer.setLabelProvider(new LabelProvider() {
				@Override
				public String getText(final @Nullable Object element) {
					if (element instanceof MarkupEntry) {
						return ((MarkupEntry) element).getLabel();
					}
					return super.getText(element);
				}
			});
			viewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			this.markupEntriesControl= viewer;
		}
		{	final ButtonGroup<MarkupEntry> buttons= new ButtonGroup<>(mainComposite);
			buttons.addEditButton(new SelectionHandler() {
				@Override
				public boolean run(final IStructuredSelection selection) {
					final Object element= getElement(selection);
					if (element instanceof MarkupEntry) {
						edit((MarkupEntry) element);
						return true;
					}
					return false;
				}
			});
			buttons.connectTo(this.markupEntriesControl, null);
			buttons.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, true));
			this.markupEntriesButtons= buttons;
		}
		initBindings();
		updateControls();
	}
	
	
	@Override
	protected void addBindings(final DataBindingSupport db) {
		this.markupEntriesControl.setInput(this.markupEntries);
	}
	
	@Override
	protected void updateControls() {
		super.updateControls();
		
		ViewerUtils.scheduleStandardSelection(this.markupEntriesControl);
	}
	
	private void edit(final MarkupEntry entry) {
		final MarkupConfig config= entry.getDescriptor().newConfig();
		if (config != null) {
			{	final String configString= getPreferenceValue(entry.getPref());
				if (configString != null) {
					config.load(configString);
				}
			}
			final MarkupConfigUIAdapter ui= (MarkupConfigUIAdapter) Platform.getAdapterManager()
					.loadAdapter(config, MarkupConfigUIAdapter.class.getName());
			if (ui != null) {
				if (ui.edit(null, null, config, getShell())) {
					final String configString= config.getString();
					setPrefValue(entry.getPref(), configString);
				}
				return;
			}
		}
		MessageDialog.openInformation(getShell(), Messages.MarkupConfig_title,
				NLS.bind("Sorry, the configuration of {0} is not supported.",
						entry.getDescriptor().getName() ));
		this.markupEntriesControl.remove(entry);
	}
	
	@Override
	protected String[] getFullBuildDialogStrings(final boolean workspaceSettings) {
		final String title= Messages.MarkupConfig_NeedsBuild_title;
		String message;
		if (workspaceSettings) {
			message= Messages.MarkupConfig_NeedsFullBuild_message;
		} else {
			message= Messages.MarkupConfig_NeedsProjectBuild_message;
		}	
		return new String[] { title, message };
	}
	
}
