/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.sourceediting;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.statet.docmlet.wikitext.core.markup.WikitextMarkupLanguage;


public class MarkupTemplatesManager {
	
	
	public static final MarkupTemplatesManager INSTANCE= new MarkupTemplatesManager();
	
	
	private final Map<String, MarkupTemplates> markupLanguageTemplates= new HashMap<>();
	
	
	private MarkupTemplatesManager() {
	}
	
	
	public synchronized MarkupTemplates getTemplates(final WikitextMarkupLanguage markupLanguage) {
		MarkupTemplates templates= this.markupLanguageTemplates.get(markupLanguage.getName());
		if (templates == null) {
			templates= new MarkupTemplates(markupLanguage);
			this.markupLanguageTemplates.put(markupLanguage.getName(), templates);
		}
		return templates;
	}
	
}
