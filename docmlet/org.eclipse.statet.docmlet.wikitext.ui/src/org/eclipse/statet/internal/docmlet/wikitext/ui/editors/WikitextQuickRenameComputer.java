/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.editors;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.docmlet.wikitext.core.ast.NodeType;
import org.eclipse.statet.docmlet.wikitext.core.ast.WikitextAstNode;
import org.eclipse.statet.docmlet.wikitext.core.model.WikitextNameAccess;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistInvocationContext;
import org.eclipse.statet.ltk.ui.sourceediting.assist.AssistProposalCollector;
import org.eclipse.statet.ltk.ui.sourceediting.assist.QuickAssistComputer;


@NonNullByDefault
public class WikitextQuickRenameComputer implements QuickAssistComputer {
	
	
	public WikitextQuickRenameComputer() {
	}
	
	
	@Override
	public void computeAssistProposals(final AssistInvocationContext context,
			final AssistProposalCollector proposals, final IProgressMonitor monitor) {
		if (!(context.getAstSelection().getCovering() instanceof WikitextAstNode)) {
			return;
		}
		final WikitextAstNode node= (WikitextAstNode) context.getAstSelection().getCovering();
		
		if (node.getNodeType() == NodeType.LABEL) {
			WikitextAstNode candidate= node;
			SEARCH_ACCESS : while (candidate != null) {
				for (final Object attachment : candidate.getAttachments()) {
					if (attachment instanceof WikitextNameAccess access) {
						SUB: while (access != null) {
							if (access.getSegmentName() == null) {
								break SUB;
							}
							if (access.getNameNode() == node) {
								addAccessAssistProposals(context, access, proposals);
								break SEARCH_ACCESS;
							}
//							access= access.getNextSegment();
							access= null;
						}
					}
				}
				candidate= candidate.getWikitextParent();
			}
		}
	}
	
	protected void addAccessAssistProposals(final AssistInvocationContext context,
			final WikitextNameAccess access,
			final AssistProposalCollector proposals) {
		final ImList<? extends WikitextNameAccess> accessList= access.getAllInUnit();
		
		proposals.add(new WikitextLinkedNamesAssistProposal(WikitextLinkedNamesAssistProposal.IN_FILE, context, access));
//		
//		if (accessList.length > 2) {
//			Arrays.sort(accessList, RElementAccess.NAME_POSITION_COMPARATOR);
//			
//			int current= 0;
//			for (; current < accessList.length; current++) {
//				if (access == accessList[current]) {
//					break;
//				}
//			}
//			if (current > 0 && current < accessList.length-1) {
//				proposals.add(new LinkedNamesAssistProposal(LinkedNamesAssistProposal.IN_FILE_PRECEDING, context, access));
//				proposals.add(new LinkedNamesAssistProposal(LinkedNamesAssistProposal.IN_FILE_FOLLOWING, context, access));
//			}
//		}
	}
	
}
