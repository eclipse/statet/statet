/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.docmlet.wikitext.ui.config;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class Messages extends NLS {
	
	
	public static String MarkupConfig_title;
	public static String MarkupConfigs_label;
	
	public static String MarkupConfig_YamlMetadata_Enable_label;
	public static String MarkupConfig_TexMathDollars_Enable_label;
	public static String MarkupConfig_TexMathSBackslash_Enable_label;
	
	public static String MarkupConfig_NeedsBuild_title;
	public static String MarkupConfig_NeedsFullBuild_message;
	public static String MarkupConfig_NeedsProjectBuild_message;
	
	public static String Base_Editors_label;
	
	public static String DocTemplates_title;
	
	public static String EditorOptions_SmartInsert_label;
	public static String EditorOptions_SmartInsert_AsDefault_label;
	public static String EditorOptions_SmartInsert_description;
	public static String EditorOptions_SmartInsert_TabAction_label;
	public static String EditorOptions_SmartInsert_CloseAuto_label;
	public static String EditorOptions_SmartInsert_CloseBrackets_label;
	public static String EditorOptions_SmartInsert_CloseParentheses_label;
	public static String EditorOptions_SmartInsert_CloseMathDollar_label;
	public static String EditorOptions_SmartInsert_HardWrapAuto_label;
	public static String EditorOptions_SmartInsert_HardWrapText_label;
	
	public static String EditorOptions_Folding_Enable_label;
	public static String EditorOptions_Folding_RestoreState_Enable_label;
	public static String EditorOptions_MarkOccurrences_Enable_label;
	public static String EditorOptions_ProblemChecking_Enable_label;
	public static String EditorOptions_AnnotationAppearance_info;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
