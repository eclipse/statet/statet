/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.PropColorProvider;
import org.eclipse.statet.rtm.ggplot.TextStyle;
import org.eclipse.statet.rtm.rtdata.RtDataFactory;
import org.eclipse.statet.rtm.rtdata.RtDataPackage;
import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Text Style</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.TextStyleImpl#getSize <em>Size</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.TextStyleImpl#getColor <em>Color</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.TextStyleImpl#getFontFamily <em>Font Family</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.TextStyleImpl#getFontFace <em>Font Face</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.TextStyleImpl#getHJust <em>HJust</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.TextStyleImpl#getVJust <em>VJust</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.TextStyleImpl#getAngle <em>Angle</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class TextStyleImpl extends EObjectImpl implements TextStyle {
	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr SIZE_EDEFAULT= null; 

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr size= SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr COLOR_EDEFAULT= (RTypedExpr)RtDataFactory.eINSTANCE.createFromString(RtDataPackage.eINSTANCE.getRColor(), ""); //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr color= COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getFontFamily() <em>Font Family</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFontFamily()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr FONT_FAMILY_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getFontFamily() <em>Font Family</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFontFamily()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr fontFamily= FONT_FAMILY_EDEFAULT;

	/**
	 * The default value of the '{@link #getFontFace() <em>Font Face</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFontFace()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr FONT_FACE_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getFontFace() <em>Font Face</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFontFace()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr fontFace= FONT_FACE_EDEFAULT;

	/**
	 * The default value of the '{@link #getHJust() <em>HJust</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHJust()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr HJUST_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getHJust() <em>HJust</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHJust()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr hJust= HJUST_EDEFAULT;

	/**
	 * The default value of the '{@link #getVJust() <em>VJust</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVJust()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr VJUST_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getVJust() <em>VJust</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVJust()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr vJust= VJUST_EDEFAULT;

	/**
	 * The default value of the '{@link #getAngle() <em>Angle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAngle()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr ANGLE_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getAngle() <em>Angle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAngle()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr angle= ANGLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TextStyleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GGPlotPackage.Literals.TEXT_STYLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getSize() {
		return this.size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSize(final RTypedExpr newSize) {
		final RTypedExpr oldSize= this.size;
		this.size= newSize;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.TEXT_STYLE__SIZE, oldSize, this.size));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getColor() {
		return this.color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setColor(final RTypedExpr newColor) {
		final RTypedExpr oldColor= this.color;
		this.color= newColor;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.TEXT_STYLE__COLOR, oldColor, this.color));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getFontFamily() {
		return this.fontFamily;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFontFamily(final RTypedExpr newFontFamily) {
		final RTypedExpr oldFontFamily= this.fontFamily;
		this.fontFamily= newFontFamily;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.TEXT_STYLE__FONT_FAMILY, oldFontFamily, this.fontFamily));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getFontFace() {
		return this.fontFace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFontFace(final RTypedExpr newFontFace) {
		final RTypedExpr oldFontFace= this.fontFace;
		this.fontFace= newFontFace;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.TEXT_STYLE__FONT_FACE, oldFontFace, this.fontFace));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getHJust() {
		return this.hJust;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHJust(final RTypedExpr newHJust) {
		final RTypedExpr oldHJust= this.hJust;
		this.hJust= newHJust;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.TEXT_STYLE__HJUST, oldHJust, this.hJust));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getVJust() {
		return this.vJust;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVJust(final RTypedExpr newVJust) {
		final RTypedExpr oldVJust= this.vJust;
		this.vJust= newVJust;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.TEXT_STYLE__VJUST, oldVJust, this.vJust));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getAngle() {
		return this.angle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAngle(final RTypedExpr newAngle) {
		final RTypedExpr oldAngle= this.angle;
		this.angle= newAngle;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.TEXT_STYLE__ANGLE, oldAngle, this.angle));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
		switch (featureID) {
			case GGPlotPackage.TEXT_STYLE__SIZE:
				return getSize();
			case GGPlotPackage.TEXT_STYLE__COLOR:
				return getColor();
			case GGPlotPackage.TEXT_STYLE__FONT_FAMILY:
				return getFontFamily();
			case GGPlotPackage.TEXT_STYLE__FONT_FACE:
				return getFontFace();
			case GGPlotPackage.TEXT_STYLE__HJUST:
				return getHJust();
			case GGPlotPackage.TEXT_STYLE__VJUST:
				return getVJust();
			case GGPlotPackage.TEXT_STYLE__ANGLE:
				return getAngle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(final int featureID, final Object newValue) {
		switch (featureID) {
			case GGPlotPackage.TEXT_STYLE__SIZE:
				setSize((RTypedExpr)newValue);
				return;
			case GGPlotPackage.TEXT_STYLE__COLOR:
				setColor((RTypedExpr)newValue);
				return;
			case GGPlotPackage.TEXT_STYLE__FONT_FAMILY:
				setFontFamily((RTypedExpr)newValue);
				return;
			case GGPlotPackage.TEXT_STYLE__FONT_FACE:
				setFontFace((RTypedExpr)newValue);
				return;
			case GGPlotPackage.TEXT_STYLE__HJUST:
				setHJust((RTypedExpr)newValue);
				return;
			case GGPlotPackage.TEXT_STYLE__VJUST:
				setVJust((RTypedExpr)newValue);
				return;
			case GGPlotPackage.TEXT_STYLE__ANGLE:
				setAngle((RTypedExpr)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(final int featureID) {
		switch (featureID) {
			case GGPlotPackage.TEXT_STYLE__SIZE:
				setSize(SIZE_EDEFAULT);
				return;
			case GGPlotPackage.TEXT_STYLE__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
			case GGPlotPackage.TEXT_STYLE__FONT_FAMILY:
				setFontFamily(FONT_FAMILY_EDEFAULT);
				return;
			case GGPlotPackage.TEXT_STYLE__FONT_FACE:
				setFontFace(FONT_FACE_EDEFAULT);
				return;
			case GGPlotPackage.TEXT_STYLE__HJUST:
				setHJust(HJUST_EDEFAULT);
				return;
			case GGPlotPackage.TEXT_STYLE__VJUST:
				setVJust(VJUST_EDEFAULT);
				return;
			case GGPlotPackage.TEXT_STYLE__ANGLE:
				setAngle(ANGLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(final int featureID) {
		switch (featureID) {
			case GGPlotPackage.TEXT_STYLE__SIZE:
				return SIZE_EDEFAULT == null ? this.size != null : !SIZE_EDEFAULT.equals(this.size);
			case GGPlotPackage.TEXT_STYLE__COLOR:
				return COLOR_EDEFAULT == null ? this.color != null : !COLOR_EDEFAULT.equals(this.color);
			case GGPlotPackage.TEXT_STYLE__FONT_FAMILY:
				return FONT_FAMILY_EDEFAULT == null ? this.fontFamily != null : !FONT_FAMILY_EDEFAULT.equals(this.fontFamily);
			case GGPlotPackage.TEXT_STYLE__FONT_FACE:
				return FONT_FACE_EDEFAULT == null ? this.fontFace != null : !FONT_FACE_EDEFAULT.equals(this.fontFace);
			case GGPlotPackage.TEXT_STYLE__HJUST:
				return HJUST_EDEFAULT == null ? this.hJust != null : !HJUST_EDEFAULT.equals(this.hJust);
			case GGPlotPackage.TEXT_STYLE__VJUST:
				return VJUST_EDEFAULT == null ? this.vJust != null : !VJUST_EDEFAULT.equals(this.vJust);
			case GGPlotPackage.TEXT_STYLE__ANGLE:
				return ANGLE_EDEFAULT == null ? this.angle != null : !ANGLE_EDEFAULT.equals(this.angle);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(final int derivedFeatureID, final Class<?> baseClass) {
		if (baseClass == PropColorProvider.class) {
			switch (derivedFeatureID) {
				case GGPlotPackage.TEXT_STYLE__COLOR: return GGPlotPackage.PROP_COLOR_PROVIDER__COLOR;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(final int baseFeatureID, final Class<?> baseClass) {
		if (baseClass == PropColorProvider.class) {
			switch (baseFeatureID) {
				case GGPlotPackage.PROP_COLOR_PROVIDER__COLOR: return GGPlotPackage.TEXT_STYLE__COLOR;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		final StringBuffer result= new StringBuffer(super.toString());
		result.append(" (size: "); //$NON-NLS-1$
		result.append(this.size);
		result.append(", color: "); //$NON-NLS-1$
		result.append(this.color);
		result.append(", fontFamily: "); //$NON-NLS-1$
		result.append(this.fontFamily);
		result.append(", fontFace: "); //$NON-NLS-1$
		result.append(this.fontFace);
		result.append(", hJust: "); //$NON-NLS-1$
		result.append(this.hJust);
		result.append(", vJust: "); //$NON-NLS-1$
		result.append(this.vJust);
		result.append(", angle: "); //$NON-NLS-1$
		result.append(this.angle);
		result.append(')');
		return result.toString();
	}

} //TextStyleImpl
