/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.GridFacetLayout;
import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Grid Facet Layout</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GridFacetLayoutImpl#getColVars <em>Col Vars</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GridFacetLayoutImpl#getRowVars <em>Row Vars</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class GridFacetLayoutImpl extends EObjectImpl implements GridFacetLayout {
	/**
	 * The cached value of the '{@link #getColVars() <em>Col Vars</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColVars()
	 * @generated
	 * @ordered
	 */
	protected EList<RTypedExpr> colVars;

	/**
	 * The cached value of the '{@link #getRowVars() <em>Row Vars</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRowVars()
	 * @generated
	 * @ordered
	 */
	protected EList<RTypedExpr> rowVars;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GridFacetLayoutImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GGPlotPackage.Literals.GRID_FACET_LAYOUT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RTypedExpr> getRowVars() {
		if (this.rowVars == null) {
			this.rowVars= new EDataTypeUniqueEList<>(RTypedExpr.class, this, GGPlotPackage.GRID_FACET_LAYOUT__ROW_VARS);
		}
		return this.rowVars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RTypedExpr> getColVars() {
		if (this.colVars == null) {
			this.colVars= new EDataTypeUniqueEList<>(RTypedExpr.class, this, GGPlotPackage.GRID_FACET_LAYOUT__COL_VARS);
		}
		return this.colVars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
		switch (featureID) {
			case GGPlotPackage.GRID_FACET_LAYOUT__COL_VARS:
				return getColVars();
			case GGPlotPackage.GRID_FACET_LAYOUT__ROW_VARS:
				return getRowVars();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(final int featureID, final Object newValue) {
		switch (featureID) {
			case GGPlotPackage.GRID_FACET_LAYOUT__COL_VARS:
				getColVars().clear();
				getColVars().addAll((Collection<? extends RTypedExpr>)newValue);
				return;
			case GGPlotPackage.GRID_FACET_LAYOUT__ROW_VARS:
				getRowVars().clear();
				getRowVars().addAll((Collection<? extends RTypedExpr>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(final int featureID) {
		switch (featureID) {
			case GGPlotPackage.GRID_FACET_LAYOUT__COL_VARS:
				getColVars().clear();
				return;
			case GGPlotPackage.GRID_FACET_LAYOUT__ROW_VARS:
				getRowVars().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(final int featureID) {
		switch (featureID) {
			case GGPlotPackage.GRID_FACET_LAYOUT__COL_VARS:
				return this.colVars != null && !this.colVars.isEmpty();
			case GGPlotPackage.GRID_FACET_LAYOUT__ROW_VARS:
				return this.rowVars != null && !this.rowVars.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		final StringBuffer result= new StringBuffer(super.toString());
		result.append(" (colVars: "); //$NON-NLS-1$
		result.append(this.colVars);
		result.append(", rowVars: "); //$NON-NLS-1$
		result.append(this.rowVars);
		result.append(')');
		return result.toString();
	}

} //GridFacetLayoutImpl
