/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot;

import org.eclipse.emf.common.util.EList;

import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Grid Facet Layout</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.GridFacetLayout#getColVars <em>Col Vars</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.GridFacetLayout#getRowVars <em>Row Vars</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGridFacetLayout()
 * @model
 * @generated
 */
public interface GridFacetLayout extends FacetLayout {
	/**
	 * Returns the value of the '<em><b>Col Vars</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.statet.rtm.rtdata.types.RTypedExpr}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Col Vars</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Col Vars</em>' attribute list.
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGridFacetLayout_ColVars()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RVar"
	 * @generated
	 */
	EList<RTypedExpr> getColVars();

	/**
	 * Returns the value of the '<em><b>Row Vars</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.statet.rtm.rtdata.types.RTypedExpr}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row Vars</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row Vars</em>' attribute list.
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGridFacetLayout_RowVars()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RVar"
	 * @generated
	 */
	EList<RTypedExpr> getRowVars();

} // GridFacetLayout
