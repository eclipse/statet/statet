/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.GeomAblineLayer;
import org.eclipse.statet.rtm.ggplot.PropAlphaProvider;
import org.eclipse.statet.rtm.ggplot.PropColorProvider;
import org.eclipse.statet.rtm.ggplot.PropLineTypeProvider;
import org.eclipse.statet.rtm.ggplot.PropSizeProvider;
import org.eclipse.statet.rtm.rtdata.RtDataFactory;
import org.eclipse.statet.rtm.rtdata.RtDataPackage;
import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Geom Abline Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GeomAblineLayerImpl#getLineType <em>Line Type</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GeomAblineLayerImpl#getSize <em>Size</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GeomAblineLayerImpl#getColor <em>Color</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GeomAblineLayerImpl#getAlpha <em>Alpha</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GeomAblineLayerImpl#getInterceptVar <em>Intercept Var</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.impl.GeomAblineLayerImpl#getSlopeVar <em>Slope Var</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class GeomAblineLayerImpl extends LayerImpl implements GeomAblineLayer {
	/**
	 * The default value of the '{@link #getLineType() <em>Line Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineType()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr LINE_TYPE_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getLineType() <em>Line Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineType()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr lineType= LINE_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr SIZE_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr size= SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr COLOR_EDEFAULT= (RTypedExpr)RtDataFactory.eINSTANCE.createFromString(RtDataPackage.eINSTANCE.getRColor(), ""); //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr color= COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getAlpha() <em>Alpha</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlpha()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr ALPHA_EDEFAULT= (RTypedExpr)RtDataFactory.eINSTANCE.createFromString(RtDataPackage.eINSTANCE.getRAlpha(), ""); //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getAlpha() <em>Alpha</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlpha()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr alpha= ALPHA_EDEFAULT;

	/**
	 * The default value of the '{@link #getInterceptVar() <em>Intercept Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterceptVar()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr INTERCEPT_VAR_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getInterceptVar() <em>Intercept Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterceptVar()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr interceptVar= INTERCEPT_VAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getSlopeVar() <em>Slope Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlopeVar()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr SLOPE_VAR_EDEFAULT= null;

	/**
	 * The cached value of the '{@link #getSlopeVar() <em>Slope Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlopeVar()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr slopeVar= SLOPE_VAR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GeomAblineLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GGPlotPackage.Literals.GEOM_ABLINE_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getLineType() {
		return this.lineType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLineType(final RTypedExpr newLineType) {
		final RTypedExpr oldLineType= this.lineType;
		this.lineType= newLineType;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GEOM_ABLINE_LAYER__LINE_TYPE, oldLineType, this.lineType));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getSize() {
		return this.size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSize(final RTypedExpr newSize) {
		final RTypedExpr oldSize= this.size;
		this.size= newSize;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GEOM_ABLINE_LAYER__SIZE, oldSize, this.size));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getColor() {
		return this.color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setColor(final RTypedExpr newColor) {
		final RTypedExpr oldColor= this.color;
		this.color= newColor;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GEOM_ABLINE_LAYER__COLOR, oldColor, this.color));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getAlpha() {
		return this.alpha;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAlpha(final RTypedExpr newAlpha) {
		final RTypedExpr oldAlpha= this.alpha;
		this.alpha= newAlpha;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GEOM_ABLINE_LAYER__ALPHA, oldAlpha, this.alpha));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getInterceptVar() {
		return this.interceptVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInterceptVar(final RTypedExpr newInterceptVar) {
		final RTypedExpr oldInterceptVar= this.interceptVar;
		this.interceptVar= newInterceptVar;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GEOM_ABLINE_LAYER__INTERCEPT_VAR, oldInterceptVar, this.interceptVar));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getSlopeVar() {
		return this.slopeVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSlopeVar(final RTypedExpr newSlopeVar) {
		final RTypedExpr oldSlopeVar= this.slopeVar;
		this.slopeVar= newSlopeVar;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, GGPlotPackage.GEOM_ABLINE_LAYER__SLOPE_VAR, oldSlopeVar, this.slopeVar));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
		switch (featureID) {
			case GGPlotPackage.GEOM_ABLINE_LAYER__LINE_TYPE:
				return getLineType();
			case GGPlotPackage.GEOM_ABLINE_LAYER__SIZE:
				return getSize();
			case GGPlotPackage.GEOM_ABLINE_LAYER__COLOR:
				return getColor();
			case GGPlotPackage.GEOM_ABLINE_LAYER__ALPHA:
				return getAlpha();
			case GGPlotPackage.GEOM_ABLINE_LAYER__INTERCEPT_VAR:
				return getInterceptVar();
			case GGPlotPackage.GEOM_ABLINE_LAYER__SLOPE_VAR:
				return getSlopeVar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(final int featureID, final Object newValue) {
		switch (featureID) {
			case GGPlotPackage.GEOM_ABLINE_LAYER__LINE_TYPE:
				setLineType((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__SIZE:
				setSize((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__COLOR:
				setColor((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__ALPHA:
				setAlpha((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__INTERCEPT_VAR:
				setInterceptVar((RTypedExpr)newValue);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__SLOPE_VAR:
				setSlopeVar((RTypedExpr)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(final int featureID) {
		switch (featureID) {
			case GGPlotPackage.GEOM_ABLINE_LAYER__LINE_TYPE:
				setLineType(LINE_TYPE_EDEFAULT);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__SIZE:
				setSize(SIZE_EDEFAULT);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__ALPHA:
				setAlpha(ALPHA_EDEFAULT);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__INTERCEPT_VAR:
				setInterceptVar(INTERCEPT_VAR_EDEFAULT);
				return;
			case GGPlotPackage.GEOM_ABLINE_LAYER__SLOPE_VAR:
				setSlopeVar(SLOPE_VAR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(final int featureID) {
		switch (featureID) {
			case GGPlotPackage.GEOM_ABLINE_LAYER__LINE_TYPE:
				return LINE_TYPE_EDEFAULT == null ? this.lineType != null : !LINE_TYPE_EDEFAULT.equals(this.lineType);
			case GGPlotPackage.GEOM_ABLINE_LAYER__SIZE:
				return SIZE_EDEFAULT == null ? this.size != null : !SIZE_EDEFAULT.equals(this.size);
			case GGPlotPackage.GEOM_ABLINE_LAYER__COLOR:
				return COLOR_EDEFAULT == null ? this.color != null : !COLOR_EDEFAULT.equals(this.color);
			case GGPlotPackage.GEOM_ABLINE_LAYER__ALPHA:
				return ALPHA_EDEFAULT == null ? this.alpha != null : !ALPHA_EDEFAULT.equals(this.alpha);
			case GGPlotPackage.GEOM_ABLINE_LAYER__INTERCEPT_VAR:
				return INTERCEPT_VAR_EDEFAULT == null ? this.interceptVar != null : !INTERCEPT_VAR_EDEFAULT.equals(this.interceptVar);
			case GGPlotPackage.GEOM_ABLINE_LAYER__SLOPE_VAR:
				return SLOPE_VAR_EDEFAULT == null ? this.slopeVar != null : !SLOPE_VAR_EDEFAULT.equals(this.slopeVar);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(final int derivedFeatureID, final Class<?> baseClass) {
		if (baseClass == PropLineTypeProvider.class) {
			switch (derivedFeatureID) {
				case GGPlotPackage.GEOM_ABLINE_LAYER__LINE_TYPE: return GGPlotPackage.PROP_LINE_TYPE_PROVIDER__LINE_TYPE;
				default: return -1;
			}
		}
		if (baseClass == PropSizeProvider.class) {
			switch (derivedFeatureID) {
				case GGPlotPackage.GEOM_ABLINE_LAYER__SIZE: return GGPlotPackage.PROP_SIZE_PROVIDER__SIZE;
				default: return -1;
			}
		}
		if (baseClass == PropColorProvider.class) {
			switch (derivedFeatureID) {
				case GGPlotPackage.GEOM_ABLINE_LAYER__COLOR: return GGPlotPackage.PROP_COLOR_PROVIDER__COLOR;
				default: return -1;
			}
		}
		if (baseClass == PropAlphaProvider.class) {
			switch (derivedFeatureID) {
				case GGPlotPackage.GEOM_ABLINE_LAYER__ALPHA: return GGPlotPackage.PROP_ALPHA_PROVIDER__ALPHA;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(final int baseFeatureID, final Class<?> baseClass) {
		if (baseClass == PropLineTypeProvider.class) {
			switch (baseFeatureID) {
				case GGPlotPackage.PROP_LINE_TYPE_PROVIDER__LINE_TYPE: return GGPlotPackage.GEOM_ABLINE_LAYER__LINE_TYPE;
				default: return -1;
			}
		}
		if (baseClass == PropSizeProvider.class) {
			switch (baseFeatureID) {
				case GGPlotPackage.PROP_SIZE_PROVIDER__SIZE: return GGPlotPackage.GEOM_ABLINE_LAYER__SIZE;
				default: return -1;
			}
		}
		if (baseClass == PropColorProvider.class) {
			switch (baseFeatureID) {
				case GGPlotPackage.PROP_COLOR_PROVIDER__COLOR: return GGPlotPackage.GEOM_ABLINE_LAYER__COLOR;
				default: return -1;
			}
		}
		if (baseClass == PropAlphaProvider.class) {
			switch (baseFeatureID) {
				case GGPlotPackage.PROP_ALPHA_PROVIDER__ALPHA: return GGPlotPackage.GEOM_ABLINE_LAYER__ALPHA;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		final StringBuffer result= new StringBuffer(super.toString());
		result.append(" (lineType: "); //$NON-NLS-1$
		result.append(this.lineType);
		result.append(", size: "); //$NON-NLS-1$
		result.append(this.size);
		result.append(", color: "); //$NON-NLS-1$
		result.append(this.color);
		result.append(", alpha: "); //$NON-NLS-1$
		result.append(this.alpha);
		result.append(", interceptVar: "); //$NON-NLS-1$
		result.append(this.interceptVar);
		result.append(", slopeVar: "); //$NON-NLS-1$
		result.append(this.slopeVar);
		result.append(')');
		return result.toString();
	}

} //GeomAblineLayerImpl
