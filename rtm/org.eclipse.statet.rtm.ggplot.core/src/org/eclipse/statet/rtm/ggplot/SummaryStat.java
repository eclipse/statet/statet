/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot;

import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Summary Stat</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.SummaryStat#getYFun <em>YFun</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getSummaryStat()
 * @model
 * @generated
 */
public interface SummaryStat extends Stat {
	/**
	 * Returns the value of the '<em><b>YFun</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>YFun</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>YFun</em>' attribute.
	 * @see #setYFun(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getSummaryStat_YFun()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RFunction"
	 * @generated
	 */
	RTypedExpr getYFun();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.SummaryStat#getYFun <em>YFun</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>YFun</em>' attribute.
	 * @see #getYFun()
	 * @generated
	 */
	void setYFun(RTypedExpr value);

} // SummaryStat
