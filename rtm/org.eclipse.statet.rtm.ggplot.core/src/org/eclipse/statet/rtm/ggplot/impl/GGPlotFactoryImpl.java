/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.statet.rtm.ggplot.GGPlot;
import org.eclipse.statet.rtm.ggplot.GGPlotFactory;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.GeomAblineLayer;
import org.eclipse.statet.rtm.ggplot.GeomBarLayer;
import org.eclipse.statet.rtm.ggplot.GeomBoxplotLayer;
import org.eclipse.statet.rtm.ggplot.GeomHistogramLayer;
import org.eclipse.statet.rtm.ggplot.GeomLineLayer;
import org.eclipse.statet.rtm.ggplot.GeomPointLayer;
import org.eclipse.statet.rtm.ggplot.GeomSmoothLayer;
import org.eclipse.statet.rtm.ggplot.GeomTextLayer;
import org.eclipse.statet.rtm.ggplot.GeomTileLayer;
import org.eclipse.statet.rtm.ggplot.GeomViolinLayer;
import org.eclipse.statet.rtm.ggplot.GridFacetLayout;
import org.eclipse.statet.rtm.ggplot.IdentityStat;
import org.eclipse.statet.rtm.ggplot.SummaryStat;
import org.eclipse.statet.rtm.ggplot.TextStyle;
import org.eclipse.statet.rtm.ggplot.WrapFacetLayout;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GGPlotFactoryImpl extends EFactoryImpl implements GGPlotFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static GGPlotFactory init() {
		try {
			final GGPlotFactory theGGPlotFactory= (GGPlotFactory)EPackage.Registry.INSTANCE.getEFactory("http://walware.de/rtm/Rt-ggplot/1.0"); //$NON-NLS-1$ 
			if (theGGPlotFactory != null) {
				return theGGPlotFactory;
			}
		}
		catch (final Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new GGPlotFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GGPlotFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(final EClass eClass) {
		switch (eClass.getClassifierID()) {
			case GGPlotPackage.GG_PLOT: return createGGPlot();
			case GGPlotPackage.GEOM_ABLINE_LAYER: return createGeomAblineLayer();
			case GGPlotPackage.GEOM_BAR_LAYER: return createGeomBarLayer();
			case GGPlotPackage.GEOM_BOXPLOT_LAYER: return createGeomBoxplotLayer();
			case GGPlotPackage.GEOM_HISTOGRAM_LAYER: return createGeomHistogramLayer();
			case GGPlotPackage.GEOM_LINE_LAYER: return createGeomLineLayer();
			case GGPlotPackage.GEOM_POINT_LAYER: return createGeomPointLayer();
			case GGPlotPackage.GEOM_TEXT_LAYER: return createGeomTextLayer();
			case GGPlotPackage.GEOM_SMOOTH_LAYER: return createGeomSmoothLayer();
			case GGPlotPackage.GEOM_TILE_LAYER: return createGeomTileLayer();
			case GGPlotPackage.GEOM_VIOLIN_LAYER: return createGeomViolinLayer();
			case GGPlotPackage.GRID_FACET_LAYOUT: return createGridFacetLayout();
			case GGPlotPackage.WRAP_FACET_LAYOUT: return createWrapFacetLayout();
			case GGPlotPackage.IDENTITY_STAT: return createIdentityStat();
			case GGPlotPackage.SUMMARY_STAT: return createSummaryStat();
			case GGPlotPackage.TEXT_STYLE: return createTextStyle();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GGPlot createGGPlot() {
		final GGPlotImpl ggPlot= new GGPlotImpl();
		return ggPlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomPointLayer createGeomPointLayer() {
		final GeomPointLayerImpl geomPointLayer= new GeomPointLayerImpl();
		return geomPointLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomBarLayer createGeomBarLayer() {
		final GeomBarLayerImpl geomBarLayer= new GeomBarLayerImpl();
		return geomBarLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomTextLayer createGeomTextLayer() {
		final GeomTextLayerImpl geomTextLayer= new GeomTextLayerImpl();
		return geomTextLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomSmoothLayer createGeomSmoothLayer() {
		final GeomSmoothLayerImpl geomSmoothLayer= new GeomSmoothLayerImpl();
		return geomSmoothLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomViolinLayer createGeomViolinLayer() {
		final GeomViolinLayerImpl geomViolinLayer= new GeomViolinLayerImpl();
		return geomViolinLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GridFacetLayout createGridFacetLayout() {
		final GridFacetLayoutImpl gridFacetLayout= new GridFacetLayoutImpl();
		return gridFacetLayout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WrapFacetLayout createWrapFacetLayout() {
		final WrapFacetLayoutImpl wrapFacetLayout= new WrapFacetLayoutImpl();
		return wrapFacetLayout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TextStyle createTextStyle() {
		final TextStyleImpl textStyle= new TextStyleImpl();
		return textStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IdentityStat createIdentityStat() {
		final IdentityStatImpl identityStat= new IdentityStatImpl();
		return identityStat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SummaryStat createSummaryStat() {
		final SummaryStatImpl summaryStat= new SummaryStatImpl();
		return summaryStat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomBoxplotLayer createGeomBoxplotLayer() {
		final GeomBoxplotLayerImpl geomBoxplotLayer= new GeomBoxplotLayerImpl();
		return geomBoxplotLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomHistogramLayer createGeomHistogramLayer() {
		final GeomHistogramLayerImpl geomHistogramLayer= new GeomHistogramLayerImpl();
		return geomHistogramLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomLineLayer createGeomLineLayer() {
		final GeomLineLayerImpl geomLineLayer= new GeomLineLayerImpl();
		return geomLineLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomAblineLayer createGeomAblineLayer() {
		final GeomAblineLayerImpl geomAblineLayer= new GeomAblineLayerImpl();
		return geomAblineLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeomTileLayer createGeomTileLayer() {
		final GeomTileLayerImpl geomTileLayer= new GeomTileLayerImpl();
		return geomTileLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GGPlotPackage getGGPlotPackage() {
		return (GGPlotPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static GGPlotPackage getPackage() {
		return GGPlotPackage.eINSTANCE;
	}

} //GGPlotFactoryImpl
