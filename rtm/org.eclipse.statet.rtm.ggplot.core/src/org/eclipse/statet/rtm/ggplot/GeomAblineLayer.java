/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot;

import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Geom Abline Layer</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.GeomAblineLayer#getInterceptVar <em>Intercept Var</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.GeomAblineLayer#getSlopeVar <em>Slope Var</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGeomAblineLayer()
 * @model
 * @generated
 */
public interface GeomAblineLayer extends Layer, PropLineTypeProvider, PropSizeProvider, PropColorProvider, PropAlphaProvider {
	/**
	 * Returns the value of the '<em><b>Intercept Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intercept Var</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intercept Var</em>' attribute.
	 * @see #setInterceptVar(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGeomAblineLayer_InterceptVar()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RNum"
	 * @generated
	 */
	RTypedExpr getInterceptVar();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.GeomAblineLayer#getInterceptVar <em>Intercept Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Intercept Var</em>' attribute.
	 * @see #getInterceptVar()
	 * @generated
	 */
	void setInterceptVar(RTypedExpr value);

	/**
	 * Returns the value of the '<em><b>Slope Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slope Var</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slope Var</em>' attribute.
	 * @see #setSlopeVar(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getGeomAblineLayer_SlopeVar()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RNum"
	 * @generated
	 */
	RTypedExpr getSlopeVar();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.GeomAblineLayer#getSlopeVar <em>Slope Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slope Var</em>' attribute.
	 * @see #getSlopeVar()
	 * @generated
	 */
	void setSlopeVar(RTypedExpr value);

} // GeomAblineLayer
