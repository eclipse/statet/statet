/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Prop Group Var Provider</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ggplot.PropGroupVarProvider#getGroupVar <em>Group Var</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getPropGroupVarProvider()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface PropGroupVarProvider extends EObject {
	/**
	 * Returns the value of the '<em><b>Group Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group Var</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Var</em>' attribute.
	 * @see #setGroupVar(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#getPropGroupVarProvider_GroupVar()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RVar"
	 * @generated
	 */
	RTypedExpr getGroupVar();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ggplot.PropGroupVarProvider#getGroupVar <em>Group Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Var</em>' attribute.
	 * @see #getGroupVar()
	 * @generated
	 */
	void setGroupVar(RTypedExpr value);

} // PropGroupVarProvider
