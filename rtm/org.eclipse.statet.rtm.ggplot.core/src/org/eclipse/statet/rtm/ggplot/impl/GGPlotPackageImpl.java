/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.statet.rtm.ggplot.FacetLayout;
import org.eclipse.statet.rtm.ggplot.GGPlot;
import org.eclipse.statet.rtm.ggplot.GGPlotFactory;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.GeomAblineLayer;
import org.eclipse.statet.rtm.ggplot.GeomBarLayer;
import org.eclipse.statet.rtm.ggplot.GeomBoxplotLayer;
import org.eclipse.statet.rtm.ggplot.GeomHistogramLayer;
import org.eclipse.statet.rtm.ggplot.GeomLineLayer;
import org.eclipse.statet.rtm.ggplot.GeomPointLayer;
import org.eclipse.statet.rtm.ggplot.GeomSmoothLayer;
import org.eclipse.statet.rtm.ggplot.GeomTextLayer;
import org.eclipse.statet.rtm.ggplot.GeomTileLayer;
import org.eclipse.statet.rtm.ggplot.GeomViolinLayer;
import org.eclipse.statet.rtm.ggplot.GridFacetLayout;
import org.eclipse.statet.rtm.ggplot.IdentityStat;
import org.eclipse.statet.rtm.ggplot.Layer;
import org.eclipse.statet.rtm.ggplot.PropAlphaProvider;
import org.eclipse.statet.rtm.ggplot.PropColorProvider;
import org.eclipse.statet.rtm.ggplot.PropDataProvider;
import org.eclipse.statet.rtm.ggplot.PropFillProvider;
import org.eclipse.statet.rtm.ggplot.PropGroupVarProvider;
import org.eclipse.statet.rtm.ggplot.PropLineTypeProvider;
import org.eclipse.statet.rtm.ggplot.PropShapeProvider;
import org.eclipse.statet.rtm.ggplot.PropSizeProvider;
import org.eclipse.statet.rtm.ggplot.PropStatProvider;
import org.eclipse.statet.rtm.ggplot.PropXVarProvider;
import org.eclipse.statet.rtm.ggplot.PropYVarProvider;
import org.eclipse.statet.rtm.ggplot.Stat;
import org.eclipse.statet.rtm.ggplot.SummaryStat;
import org.eclipse.statet.rtm.ggplot.TextStyle;
import org.eclipse.statet.rtm.ggplot.WrapFacetLayout;
import org.eclipse.statet.rtm.ggplot.XVarLayer;
import org.eclipse.statet.rtm.ggplot.XYVarLayer;
import org.eclipse.statet.rtm.rtdata.RtDataPackage;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GGPlotPackageImpl extends EPackageImpl implements GGPlotPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ggPlotEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass layerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xVarLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xyVarLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomPointLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomBarLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomTextLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomSmoothLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomViolinLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass facetLayoutEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gridFacetLayoutEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass wrapFacetLayoutEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass textStyleEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identityStatEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass summaryStatEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propDataProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propXVarProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propYVarProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propStatProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propGroupVarProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propColorProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propFillProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propAlphaProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propShapeProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propLineTypeProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propSizeProviderEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomBoxplotLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomHistogramLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomLineLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomAblineLayerEClass= null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass geomTileLayerEClass= null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.statet.rtm.ggplot.GGPlotPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GGPlotPackageImpl() {
		super(eNS_URI, GGPlotFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited= false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GGPlotPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GGPlotPackage init() {
		if (isInited) {
			return (GGPlotPackage)EPackage.Registry.INSTANCE.getEPackage(GGPlotPackage.eNS_URI);
		}

		// Obtain or create and register package
		final GGPlotPackageImpl theGGPlotPackage= (GGPlotPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GGPlotPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GGPlotPackageImpl());

		isInited= true;

		// Initialize simple dependencies
		RtDataPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theGGPlotPackage.createPackageContents();

		// Initialize created meta-data
		theGGPlotPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGGPlotPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GGPlotPackage.eNS_URI, theGGPlotPackage);
		return theGGPlotPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGGPlot() {
		return this.ggPlotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGGPlot_DataFilter() {
		return (EAttribute)this.ggPlotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGGPlot_MainTitle() {
		return (EAttribute)this.ggPlotEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGGPlot_MainTitleStyle() {
		return (EReference)this.ggPlotEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGGPlot_Facet() {
		return (EReference)this.ggPlotEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGGPlot_AxXLim() {
		return (EAttribute)this.ggPlotEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGGPlot_AxYLim() {
		return (EAttribute)this.ggPlotEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGGPlot_AxXLabel() {
		return (EAttribute)this.ggPlotEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGGPlot_AxYLabel() {
		return (EAttribute)this.ggPlotEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGGPlot_AxXLabelStyle() {
		return (EReference)this.ggPlotEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGGPlot_AxYLabelStyle() {
		return (EReference)this.ggPlotEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGGPlot_AxXTextStyle() {
		return (EReference)this.ggPlotEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGGPlot_AxYTextStyle() {
		return (EReference)this.ggPlotEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGGPlot_Layers() {
		return (EReference)this.ggPlotEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLayer() {
		return this.layerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getXVarLayer() {
		return this.xVarLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getXYVarLayer() {
		return this.xyVarLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomPointLayer() {
		return this.geomPointLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeomPointLayer_PositionXJitter() {
		return (EAttribute)this.geomPointLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeomPointLayer_PositionYJitter() {
		return (EAttribute)this.geomPointLayerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomBarLayer() {
		return this.geomBarLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomTextLayer() {
		return this.geomTextLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeomTextLayer_Label() {
		return (EAttribute)this.geomTextLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomSmoothLayer() {
		return this.geomSmoothLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomViolinLayer() {
		return this.geomViolinLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFacetLayout() {
		return this.facetLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGridFacetLayout() {
		return this.gridFacetLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGridFacetLayout_ColVars() {
		return (EAttribute)this.gridFacetLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGridFacetLayout_RowVars() {
		return (EAttribute)this.gridFacetLayoutEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getWrapFacetLayout() {
		return this.wrapFacetLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getWrapFacetLayout_ColVars() {
		return (EAttribute)this.wrapFacetLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getWrapFacetLayout_ColNum() {
		return (EAttribute)this.wrapFacetLayoutEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTextStyle() {
		return this.textStyleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTextStyle_FontFamily() {
		return (EAttribute)this.textStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTextStyle_FontFace() {
		return (EAttribute)this.textStyleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTextStyle_HJust() {
		return (EAttribute)this.textStyleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTextStyle_VJust() {
		return (EAttribute)this.textStyleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTextStyle_Angle() {
		return (EAttribute)this.textStyleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStat() {
		return this.statEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIdentityStat() {
		return this.identityStatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSummaryStat() {
		return this.summaryStatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSummaryStat_YFun() {
		return (EAttribute)this.summaryStatEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropDataProvider() {
		return this.propDataProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropDataProvider_Data() {
		return (EAttribute)this.propDataProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropXVarProvider() {
		return this.propXVarProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropXVarProvider_XVar() {
		return (EAttribute)this.propXVarProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropYVarProvider() {
		return this.propYVarProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropYVarProvider_YVar() {
		return (EAttribute)this.propYVarProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropStatProvider() {
		return this.propStatProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPropStatProvider_Stat() {
		return (EReference)this.propStatProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropGroupVarProvider() {
		return this.propGroupVarProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropGroupVarProvider_GroupVar() {
		return (EAttribute)this.propGroupVarProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropColorProvider() {
		return this.propColorProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropColorProvider_Color() {
		return (EAttribute)this.propColorProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropFillProvider() {
		return this.propFillProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropFillProvider_Fill() {
		return (EAttribute)this.propFillProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropAlphaProvider() {
		return this.propAlphaProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropAlphaProvider_Alpha() {
		return (EAttribute)this.propAlphaProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropShapeProvider() {
		return this.propShapeProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropShapeProvider_Shape() {
		return (EAttribute)this.propShapeProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropLineTypeProvider() {
		return this.propLineTypeProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropLineTypeProvider_LineType() {
		return (EAttribute)this.propLineTypeProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPropSizeProvider() {
		return this.propSizeProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPropSizeProvider_Size() {
		return (EAttribute)this.propSizeProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomBoxplotLayer() {
		return this.geomBoxplotLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomHistogramLayer() {
		return this.geomHistogramLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomLineLayer() {
		return this.geomLineLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomAblineLayer() {
		return this.geomAblineLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeomAblineLayer_InterceptVar() {
		return (EAttribute)this.geomAblineLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeomAblineLayer_SlopeVar() {
		return (EAttribute)this.geomAblineLayerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeomTileLayer() {
		return this.geomTileLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GGPlotFactory getGGPlotFactory() {
		return (GGPlotFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated= false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (this.isCreated) {
			return;
		}
		this.isCreated= true;

		// Create classes and their features
		this.ggPlotEClass= createEClass(GG_PLOT);
		createEAttribute(this.ggPlotEClass, GG_PLOT__DATA_FILTER);
		createEAttribute(this.ggPlotEClass, GG_PLOT__MAIN_TITLE);
		createEReference(this.ggPlotEClass, GG_PLOT__MAIN_TITLE_STYLE);
		createEReference(this.ggPlotEClass, GG_PLOT__FACET);
		createEAttribute(this.ggPlotEClass, GG_PLOT__AX_XLIM);
		createEAttribute(this.ggPlotEClass, GG_PLOT__AX_YLIM);
		createEAttribute(this.ggPlotEClass, GG_PLOT__AX_XLABEL);
		createEAttribute(this.ggPlotEClass, GG_PLOT__AX_YLABEL);
		createEReference(this.ggPlotEClass, GG_PLOT__AX_XLABEL_STYLE);
		createEReference(this.ggPlotEClass, GG_PLOT__AX_YLABEL_STYLE);
		createEReference(this.ggPlotEClass, GG_PLOT__AX_XTEXT_STYLE);
		createEReference(this.ggPlotEClass, GG_PLOT__AX_YTEXT_STYLE);
		createEReference(this.ggPlotEClass, GG_PLOT__LAYERS);

		this.layerEClass= createEClass(LAYER);

		this.xVarLayerEClass= createEClass(XVAR_LAYER);

		this.xyVarLayerEClass= createEClass(XY_VAR_LAYER);

		this.geomAblineLayerEClass= createEClass(GEOM_ABLINE_LAYER);
		createEAttribute(this.geomAblineLayerEClass, GEOM_ABLINE_LAYER__INTERCEPT_VAR);
		createEAttribute(this.geomAblineLayerEClass, GEOM_ABLINE_LAYER__SLOPE_VAR);

		this.geomBarLayerEClass= createEClass(GEOM_BAR_LAYER);

		this.geomBoxplotLayerEClass= createEClass(GEOM_BOXPLOT_LAYER);

		this.geomHistogramLayerEClass= createEClass(GEOM_HISTOGRAM_LAYER);

		this.geomLineLayerEClass= createEClass(GEOM_LINE_LAYER);

		this.geomPointLayerEClass= createEClass(GEOM_POINT_LAYER);
		createEAttribute(this.geomPointLayerEClass, GEOM_POINT_LAYER__POSITION_XJITTER);
		createEAttribute(this.geomPointLayerEClass, GEOM_POINT_LAYER__POSITION_YJITTER);

		this.geomTextLayerEClass= createEClass(GEOM_TEXT_LAYER);
		createEAttribute(this.geomTextLayerEClass, GEOM_TEXT_LAYER__LABEL);

		this.geomSmoothLayerEClass= createEClass(GEOM_SMOOTH_LAYER);

		this.geomTileLayerEClass= createEClass(GEOM_TILE_LAYER);

		this.geomViolinLayerEClass= createEClass(GEOM_VIOLIN_LAYER);

		this.facetLayoutEClass= createEClass(FACET_LAYOUT);

		this.gridFacetLayoutEClass= createEClass(GRID_FACET_LAYOUT);
		createEAttribute(this.gridFacetLayoutEClass, GRID_FACET_LAYOUT__COL_VARS);
		createEAttribute(this.gridFacetLayoutEClass, GRID_FACET_LAYOUT__ROW_VARS);

		this.wrapFacetLayoutEClass= createEClass(WRAP_FACET_LAYOUT);
		createEAttribute(this.wrapFacetLayoutEClass, WRAP_FACET_LAYOUT__COL_VARS);
		createEAttribute(this.wrapFacetLayoutEClass, WRAP_FACET_LAYOUT__COL_NUM);

		this.statEClass= createEClass(STAT);

		this.identityStatEClass= createEClass(IDENTITY_STAT);

		this.summaryStatEClass= createEClass(SUMMARY_STAT);
		createEAttribute(this.summaryStatEClass, SUMMARY_STAT__YFUN);

		this.textStyleEClass= createEClass(TEXT_STYLE);
		createEAttribute(this.textStyleEClass, TEXT_STYLE__FONT_FAMILY);
		createEAttribute(this.textStyleEClass, TEXT_STYLE__FONT_FACE);
		createEAttribute(this.textStyleEClass, TEXT_STYLE__HJUST);
		createEAttribute(this.textStyleEClass, TEXT_STYLE__VJUST);
		createEAttribute(this.textStyleEClass, TEXT_STYLE__ANGLE);

		this.propDataProviderEClass= createEClass(PROP_DATA_PROVIDER);
		createEAttribute(this.propDataProviderEClass, PROP_DATA_PROVIDER__DATA);

		this.propXVarProviderEClass= createEClass(PROP_XVAR_PROVIDER);
		createEAttribute(this.propXVarProviderEClass, PROP_XVAR_PROVIDER__XVAR);

		this.propYVarProviderEClass= createEClass(PROP_YVAR_PROVIDER);
		createEAttribute(this.propYVarProviderEClass, PROP_YVAR_PROVIDER__YVAR);

		this.propStatProviderEClass= createEClass(PROP_STAT_PROVIDER);
		createEReference(this.propStatProviderEClass, PROP_STAT_PROVIDER__STAT);

		this.propGroupVarProviderEClass= createEClass(PROP_GROUP_VAR_PROVIDER);
		createEAttribute(this.propGroupVarProviderEClass, PROP_GROUP_VAR_PROVIDER__GROUP_VAR);

		this.propShapeProviderEClass= createEClass(PROP_SHAPE_PROVIDER);
		createEAttribute(this.propShapeProviderEClass, PROP_SHAPE_PROVIDER__SHAPE);

		this.propLineTypeProviderEClass= createEClass(PROP_LINE_TYPE_PROVIDER);
		createEAttribute(this.propLineTypeProviderEClass, PROP_LINE_TYPE_PROVIDER__LINE_TYPE);

		this.propSizeProviderEClass= createEClass(PROP_SIZE_PROVIDER);
		createEAttribute(this.propSizeProviderEClass, PROP_SIZE_PROVIDER__SIZE);

		this.propColorProviderEClass= createEClass(PROP_COLOR_PROVIDER);
		createEAttribute(this.propColorProviderEClass, PROP_COLOR_PROVIDER__COLOR);

		this.propFillProviderEClass= createEClass(PROP_FILL_PROVIDER);
		createEAttribute(this.propFillProviderEClass, PROP_FILL_PROVIDER__FILL);

		this.propAlphaProviderEClass= createEClass(PROP_ALPHA_PROVIDER);
		createEAttribute(this.propAlphaProviderEClass, PROP_ALPHA_PROVIDER__ALPHA);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized= false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (this.isInitialized) {
			return;
		}
		this.isInitialized= true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		final RtDataPackage theRtDataPackage= (RtDataPackage)EPackage.Registry.INSTANCE.getEPackage(RtDataPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		this.ggPlotEClass.getESuperTypes().add(getPropDataProvider());
		this.ggPlotEClass.getESuperTypes().add(getPropXVarProvider());
		this.ggPlotEClass.getESuperTypes().add(getPropYVarProvider());
		this.layerEClass.getESuperTypes().add(getPropDataProvider());
		this.xVarLayerEClass.getESuperTypes().add(getLayer());
		this.xVarLayerEClass.getESuperTypes().add(getPropXVarProvider());
		this.xVarLayerEClass.getESuperTypes().add(getPropGroupVarProvider());
		this.xyVarLayerEClass.getESuperTypes().add(getLayer());
		this.xyVarLayerEClass.getESuperTypes().add(getPropXVarProvider());
		this.xyVarLayerEClass.getESuperTypes().add(getPropYVarProvider());
		this.xyVarLayerEClass.getESuperTypes().add(getPropGroupVarProvider());
		this.geomAblineLayerEClass.getESuperTypes().add(getLayer());
		this.geomAblineLayerEClass.getESuperTypes().add(getPropLineTypeProvider());
		this.geomAblineLayerEClass.getESuperTypes().add(getPropSizeProvider());
		this.geomAblineLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomAblineLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomBarLayerEClass.getESuperTypes().add(getXYVarLayer());
		this.geomBarLayerEClass.getESuperTypes().add(getPropStatProvider());
		this.geomBarLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomBarLayerEClass.getESuperTypes().add(getPropFillProvider());
		this.geomBarLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomBoxplotLayerEClass.getESuperTypes().add(getXVarLayer());
		this.geomBoxplotLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomBoxplotLayerEClass.getESuperTypes().add(getPropFillProvider());
		this.geomBoxplotLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomHistogramLayerEClass.getESuperTypes().add(getXVarLayer());
		this.geomHistogramLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomHistogramLayerEClass.getESuperTypes().add(getPropFillProvider());
		this.geomHistogramLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomLineLayerEClass.getESuperTypes().add(getXYVarLayer());
		this.geomLineLayerEClass.getESuperTypes().add(getPropStatProvider());
		this.geomLineLayerEClass.getESuperTypes().add(getPropLineTypeProvider());
		this.geomLineLayerEClass.getESuperTypes().add(getPropSizeProvider());
		this.geomLineLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomLineLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomPointLayerEClass.getESuperTypes().add(getXYVarLayer());
		this.geomPointLayerEClass.getESuperTypes().add(getPropShapeProvider());
		this.geomPointLayerEClass.getESuperTypes().add(getPropSizeProvider());
		this.geomPointLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomPointLayerEClass.getESuperTypes().add(getPropFillProvider());
		this.geomPointLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomTextLayerEClass.getESuperTypes().add(getXYVarLayer());
		this.geomTextLayerEClass.getESuperTypes().add(getTextStyle());
		this.geomTextLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomSmoothLayerEClass.getESuperTypes().add(getXYVarLayer());
		this.geomSmoothLayerEClass.getESuperTypes().add(getPropSizeProvider());
		this.geomSmoothLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomSmoothLayerEClass.getESuperTypes().add(getPropFillProvider());
		this.geomSmoothLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomTileLayerEClass.getESuperTypes().add(getXYVarLayer());
		this.geomTileLayerEClass.getESuperTypes().add(getPropLineTypeProvider());
		this.geomTileLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomTileLayerEClass.getESuperTypes().add(getPropFillProvider());
		this.geomTileLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.geomViolinLayerEClass.getESuperTypes().add(getXYVarLayer());
		this.geomViolinLayerEClass.getESuperTypes().add(getPropLineTypeProvider());
		this.geomViolinLayerEClass.getESuperTypes().add(getPropColorProvider());
		this.geomViolinLayerEClass.getESuperTypes().add(getPropFillProvider());
		this.geomViolinLayerEClass.getESuperTypes().add(getPropAlphaProvider());
		this.gridFacetLayoutEClass.getESuperTypes().add(getFacetLayout());
		this.wrapFacetLayoutEClass.getESuperTypes().add(getFacetLayout());
		this.identityStatEClass.getESuperTypes().add(getStat());
		this.summaryStatEClass.getESuperTypes().add(getStat());
		this.textStyleEClass.getESuperTypes().add(getPropSizeProvider());
		this.textStyleEClass.getESuperTypes().add(getPropColorProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(this.ggPlotEClass, GGPlot.class, "GGPlot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getGGPlot_DataFilter(), theRtDataPackage.getRDataFilter(), "dataFilter", null, 0, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getGGPlot_MainTitle(), theRtDataPackage.getRLabel(), "mainTitle", null, 0, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getGGPlot_MainTitleStyle(), getTextStyle(), null, "mainTitleStyle", null, 1, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getGGPlot_Facet(), getFacetLayout(), null, "facet", null, 0, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getGGPlot_AxXLim(), theRtDataPackage.getRNumRange(), "axXLim", null, 0, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getGGPlot_AxYLim(), theRtDataPackage.getRNumRange(), "axYLim", null, 0, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getGGPlot_AxXLabel(), theRtDataPackage.getRLabel(), "axXLabel", null, 0, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getGGPlot_AxYLabel(), theRtDataPackage.getRLabel(), "axYLabel", null, 0, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getGGPlot_AxXLabelStyle(), getTextStyle(), null, "axXLabelStyle", null, 1, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getGGPlot_AxYLabelStyle(), getTextStyle(), null, "axYLabelStyle", null, 1, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getGGPlot_AxXTextStyle(), getTextStyle(), null, "axXTextStyle", null, 1, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getGGPlot_AxYTextStyle(), getTextStyle(), null, "axYTextStyle", null, 1, 1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getGGPlot_Layers(), getLayer(), null, "layers", null, 0, -1, GGPlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.layerEClass, Layer.class, "Layer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.xVarLayerEClass, XVarLayer.class, "XVarLayer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.xyVarLayerEClass, XYVarLayer.class, "XYVarLayer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.geomAblineLayerEClass, GeomAblineLayer.class, "GeomAblineLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getGeomAblineLayer_InterceptVar(), theRtDataPackage.getRNum(), "interceptVar", null, 0, 1, GeomAblineLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getGeomAblineLayer_SlopeVar(), theRtDataPackage.getRNum(), "slopeVar", null, 0, 1, GeomAblineLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.geomBarLayerEClass, GeomBarLayer.class, "GeomBarLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.geomBoxplotLayerEClass, GeomBoxplotLayer.class, "GeomBoxplotLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.geomHistogramLayerEClass, GeomHistogramLayer.class, "GeomHistogramLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.geomLineLayerEClass, GeomLineLayer.class, "GeomLineLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.geomPointLayerEClass, GeomPointLayer.class, "GeomPointLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getGeomPointLayer_PositionXJitter(), theRtDataPackage.getRNum(), "positionXJitter", null, 0, 1, GeomPointLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getGeomPointLayer_PositionYJitter(), theRtDataPackage.getRNum(), "positionYJitter", null, 0, 1, GeomPointLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.geomTextLayerEClass, GeomTextLayer.class, "GeomTextLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getGeomTextLayer_Label(), theRtDataPackage.getRVar(), "label", null, 0, 1, GeomTextLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.geomSmoothLayerEClass, GeomSmoothLayer.class, "GeomSmoothLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.geomTileLayerEClass, GeomTileLayer.class, "GeomTileLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.geomViolinLayerEClass, GeomViolinLayer.class, "GeomViolinLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.facetLayoutEClass, FacetLayout.class, "FacetLayout", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.gridFacetLayoutEClass, GridFacetLayout.class, "GridFacetLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getGridFacetLayout_ColVars(), theRtDataPackage.getRVar(), "colVars", null, 0, -1, GridFacetLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getGridFacetLayout_RowVars(), theRtDataPackage.getRVar(), "rowVars", null, 0, -1, GridFacetLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.wrapFacetLayoutEClass, WrapFacetLayout.class, "WrapFacetLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getWrapFacetLayout_ColVars(), theRtDataPackage.getRVar(), "colVars", null, 0, -1, WrapFacetLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getWrapFacetLayout_ColNum(), theRtDataPackage.getRInt(), "colNum", null, 0, 1, WrapFacetLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.statEClass, Stat.class, "Stat", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.identityStatEClass, IdentityStat.class, "IdentityStat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(this.summaryStatEClass, SummaryStat.class, "SummaryStat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSummaryStat_YFun(), theRtDataPackage.getRFunction(), "yFun", null, 0, 1, SummaryStat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.textStyleEClass, TextStyle.class, "TextStyle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getTextStyle_FontFamily(), theRtDataPackage.getRFontFamily(), "fontFamily", null, 0, 1, TextStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getTextStyle_FontFace(), theRtDataPackage.getRText(), "fontFace", null, 0, 1, TextStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getTextStyle_HJust(), theRtDataPackage.getRNum(), "hJust", null, 0, 1, TextStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getTextStyle_VJust(), theRtDataPackage.getRNum(), "vJust", null, 0, 1, TextStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getTextStyle_Angle(), theRtDataPackage.getRNum(), "angle", null, 0, 1, TextStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propDataProviderEClass, PropDataProvider.class, "PropDataProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropDataProvider_Data(), theRtDataPackage.getRDataFrame(), "data", null, 0, 1, PropDataProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propXVarProviderEClass, PropXVarProvider.class, "PropXVarProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropXVarProvider_XVar(), theRtDataPackage.getRVar(), "xVar", null, 0, 1, PropXVarProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propYVarProviderEClass, PropYVarProvider.class, "PropYVarProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropYVarProvider_YVar(), theRtDataPackage.getRVar(), "yVar", null, 0, 1, PropYVarProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propStatProviderEClass, PropStatProvider.class, "PropStatProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getPropStatProvider_Stat(), getStat(), null, "stat", null, 0, 1, PropStatProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propGroupVarProviderEClass, PropGroupVarProvider.class, "PropGroupVarProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropGroupVarProvider_GroupVar(), theRtDataPackage.getRVar(), "groupVar", null, 0, 1, PropGroupVarProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propShapeProviderEClass, PropShapeProvider.class, "PropShapeProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropShapeProvider_Shape(), theRtDataPackage.getRPlotPointSymbol(), "shape", null, 0, 1, PropShapeProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propLineTypeProviderEClass, PropLineTypeProvider.class, "PropLineTypeProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropLineTypeProvider_LineType(), theRtDataPackage.getRPlotLineType(), "lineType", null, 0, 1, PropLineTypeProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propSizeProviderEClass, PropSizeProvider.class, "PropSizeProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropSizeProvider_Size(), theRtDataPackage.getRSize(), "size", null, 0, 1, PropSizeProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(this.propColorProviderEClass, PropColorProvider.class, "PropColorProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropColorProvider_Color(), theRtDataPackage.getRColor(), "color", "", 0, 1, PropColorProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$

		initEClass(this.propFillProviderEClass, PropFillProvider.class, "PropFillProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropFillProvider_Fill(), theRtDataPackage.getRColor(), "fill", "", 0, 1, PropFillProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$

		initEClass(this.propAlphaProviderEClass, PropAlphaProvider.class, "PropAlphaProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getPropAlphaProvider_Alpha(), theRtDataPackage.getRAlpha(), "alpha", "", 0, 1, PropAlphaProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$

		// Create resource
		createResource(eNS_URI);
	}

} //GGPlotPackageImpl
