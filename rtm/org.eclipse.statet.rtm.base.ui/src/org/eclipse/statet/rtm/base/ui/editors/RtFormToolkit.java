/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.editors;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.FormColors;

import org.eclipse.statet.ecommons.emf.ui.forms.EFColors;
import org.eclipse.statet.ecommons.emf.ui.forms.EFToolkit;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFColors;

import org.eclipse.statet.rtm.base.ui.rexpr.RExprTypeUIAdapter;
import org.eclipse.statet.rtm.base.ui.rexpr.RExprWidget;
import org.eclipse.statet.rtm.base.util.RExprTypes;



public class RtFormToolkit extends EFToolkit {
	
	
	public RtFormToolkit(final EFColors colors) {
		super(colors);
	}
	
	
	public void adapt(final RExprWidget control) {
		final FormColors formColors= getColors();
		
		adapt(control, false, false);
		adapt(control.getText(), true, false);
		
		control.setTypeBackgroundColor(formColors.getColor(IEFColors.TW_TYPE_BACKGROUND));
		control.setTypeBorderColor(formColors.getColor(IEFColors.TB_BORDER));
		control.setTypeBorder2Color(formColors.getColor(IEFColors.TW_TYPE_BORDER2));
		control.setTypeHoverColor(formColors.getColor(IEFColors.TW_TYPE_HOVER));
	}
	
	public RExprWidget createPropRTypedExpr(final Composite parent, final int options,
			final RExprTypes types, final List<RExprTypeUIAdapter> uiAdapters) {
		final RExprWidget widget= new RExprWidget(parent, options, types, uiAdapters);
		adapt(widget);
		widget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		return widget;
	}
	
}
