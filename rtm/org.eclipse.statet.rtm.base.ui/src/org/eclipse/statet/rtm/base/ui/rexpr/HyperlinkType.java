/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.rexpr;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.events.IHyperlinkListener;
import org.eclipse.ui.forms.widgets.Hyperlink;
import org.eclipse.ui.views.properties.PropertySheet;
import org.eclipse.ui.views.properties.tabbed.ITabDescriptor;

import org.eclipse.statet.ecommons.emf.core.IContext;
import org.eclipse.statet.ecommons.emf.core.IEMFPropertyContext;
import org.eclipse.statet.ecommons.emf.ui.forms.EFEditor;
import org.eclipse.statet.ecommons.emf.ui.forms.EFPropertySheetPage;
import org.eclipse.statet.ecommons.emf.ui.forms.EFToolkit;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.rtm.base.ui.rexpr.RExprWidget.TypeDef;


public class HyperlinkType extends TypeDef implements IHyperlinkListener {
	
	
	public static interface IHyperlinkProvider {
		
		PropertyTabLink get(EClass eClass, EStructuralFeature eFeature);
		
	}
	
	public static class PropertyTabLink {
		
		private final String label;
		
		final String propertyTabId;
		
		public PropertyTabLink(final String label, final String propertyTabId) {
			this.label= label;
			this.propertyTabId= propertyTabId;
		}
		
		protected void run(final IContext context) {
			showProperties(context);
		}
		
		protected void showProperties(final IContext context) {
			final EFEditor editor= context.getAdapter(EFEditor.class);
			
			final IWorkbenchPage workbenchPage= UIAccess.getActiveWorkbenchPage(true);
			final IViewReference[] viewReferences= workbenchPage.getViewReferences();
			final IViewReference[] rated= new IViewReference[10];
			for (final IViewReference viewReference : viewReferences) {
				final int rate= ratePropertyView(viewReference, editor);
				if (rate >= 0 && rated[rate] == null) {
					rated[rate]= viewReference;
				}
			}
			for (final IViewReference viewReference : rated) {
				if (viewReference != null) {
					final PropertySheet view= (PropertySheet) viewReference.getView(true);
					workbenchPage.activate(view);
					showPropertyTab(view);
					return;
				}
			}
			try {
				final PropertySheet view= (PropertySheet) workbenchPage.showView("org.eclipse.ui.views.PropertySheet"); //$NON-NLS-1$
				if (!view.isPinned()) {
					showPropertyTab(view);
					return;
				}
			}
			catch (final PartInitException e) {
			}
		}
		
		private int ratePropertyView(final IViewReference viewReference, final EFEditor editor) {
			if (viewReference == null || !viewReference.getId().equals("org.eclipse.ui.views.PropertySheet")) { //$NON-NLS-1$
				return -1;
			}
			final PropertySheet view= (PropertySheet) viewReference.getPart(false);
			if (view != null) {
				if (!(view.getCurrentPage() instanceof EFPropertySheetPage)) {
					return -1;
				}
				final EFPropertySheetPage page= (EFPropertySheetPage) view.getCurrentPage();
				final ITabDescriptor tab= page.getSelectedTab();
				final boolean matches= (page.getEditor() == editor
						&& tab != null && tab.getId().equals(this.propertyTabId));
				if (view.isPinned()) {
					if (matches) {
						return 1;
					}
					return -1;
				}
				final int rate= (view.getViewSite().getPage().isPartVisible(view)) ? 2 : 5;
				if (matches) {
					return rate;
				}
				if (viewReference.getSecondaryId() == null) {
					return rate + 1;
				}
				return rate + 2;
			}
			if (viewReference.getSecondaryId() == null) {
				return 8;
			}
			return 9;
		}
		
		private void showPropertyTab(final PropertySheet view) {
			final EFPropertySheetPage efPage= (EFPropertySheetPage) view.getCurrentPage();
			efPage.setSelectedTab(this.propertyTabId);
		}
		
	}
	
	
	private Hyperlink link;
	
	private PropertyTabLink descriptor;
	
	
	public HyperlinkType(final RExprTypeUIAdapter type) {
		super(type);
	}
	
	
	@Override
	public boolean hasDetail() {
		return true;
	}
	
	@Override
	protected Control createDetailControl(final Composite parent) {
		final IEMFPropertyContext context= (IEMFPropertyContext) getContext();
		final IHyperlinkProvider linkProvider= getContext().getAdapter(IHyperlinkProvider.class);
		
		if (context == null || linkProvider == null) {
			return null;
		}
		this.descriptor= linkProvider.get(context.getEClass(), context.getEFeature());
		
		final EFEditor editor= context.getAdapter(EFEditor.class);
		final EFToolkit toolkit= editor.getToolkit();
		this.link= toolkit.createHyperlink(parent, this.descriptor.label, SWT.NONE);
		this.link.addHyperlinkListener(this);
		return this.link;
	}
	
	@Override
	public void linkEntered(final HyperlinkEvent e) {
	}
	
	@Override
	public void linkExited(final HyperlinkEvent e) {
	}
	
	@Override
	public void linkActivated(final HyperlinkEvent e) {
		if (this.descriptor != null) {
			this.descriptor.run(getContext());
		}
	}
	
}
