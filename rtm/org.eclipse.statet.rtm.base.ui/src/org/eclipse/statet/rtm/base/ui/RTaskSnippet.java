/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui;

import java.util.List;


public class RTaskSnippet {
	
	
	private final IRtDescriptor descriptor;
	
	private final String label;
	
	private final List<String> requiredPkgs;
	
	private final String rCode;
	
	
	public RTaskSnippet(final IRtDescriptor descriptor, final String label,
			final List<String> requiredPkgs, final String code) {
		this.descriptor= descriptor;
		this.label= label;
		this.requiredPkgs= requiredPkgs;
		this.rCode= code;
	}
	
	
	public String getLabel() {
		return this.label;
	}
	
	public IRtDescriptor getDescriptor() {
		return this.descriptor;
	}
	
	public List<String> getRequiredPkgs() {
		return this.requiredPkgs;
	}
	
	public String getRCode() {
		return this.rCode;
	}
	
}
