/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.rexpr;

import static org.eclipse.statet.rtm.base.ui.rexpr.RExprTypeUIAdapter.PRIORITY_INVALID;

import java.util.List;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;

import org.eclipse.statet.ecommons.emf.core.IContext;


class RExprDropAdapter extends DropTargetAdapter {
	
	
	private final List<RExprTypeUIAdapter> uiAdapters;
	
	
	public RExprDropAdapter(final List<RExprTypeUIAdapter> uiAdapters) {
		this.uiAdapters= uiAdapters;
	}
	
	
	@Override
	public void dragEnter(final DropTargetEvent event) {
		dragOperationChanged(event);
		
		if (event.detail == DND.DROP_NONE) {
			return;
		}
		if (LocalSelectionTransfer.getTransfer().isSupportedType(event.currentDataType)) {
			final ISelection selection= LocalSelectionTransfer.getTransfer().getSelection();
			if (selection instanceof IStructuredSelection) {
				if (isValidInput(selection, event)) {
					return;
				}
			}
			
			event.detail= DND.DROP_NONE;
			return;
		}
	}
	
	@Override
	public void dragOperationChanged(final DropTargetEvent event) {
		if (event.detail == DND.DROP_DEFAULT || event.detail == DND.DROP_NONE) {
			if ((event.operations & DND.DROP_COPY) != 0) {
				event.detail= DND.DROP_COPY;
			}
			else {
				event.detail= DND.DROP_NONE;
			}
		}
	}
	
	@Override
	public void drop(final DropTargetEvent event) {
		if (!performDrop(event)) {
			event.detail= DND.DROP_NONE;
		}
	}
	
	protected boolean performDrop(final DropTargetEvent event) {
		if (LocalSelectionTransfer.getTransfer().isSupportedType(event.currentDataType)) {
			final ISelection selection= LocalSelectionTransfer.getTransfer().getSelection();
			if (selection instanceof IStructuredSelection) {
				return setInput(selection, event);
			}
			return false;
		}
		if (TextTransfer.getInstance().isSupportedType(event.currentDataType)) {
			final String text= (String) event.data;
			if (text != null && !text.isEmpty()) {
				return insertText(text);
			}
			return false;
		}
		return false;
	}
	
	
	protected boolean isValidInput(final Object input, final DropTargetEvent event) {
		for (final RExprTypeUIAdapter uiAdapters : this.uiAdapters) {
			if (uiAdapters.isValidInput(input, getContext()) > PRIORITY_INVALID) {
				return true;
			}
		}
		return false;
	}
	
	protected boolean setInput(final Object input, final DropTargetEvent event) {
		int selectedPriority= PRIORITY_INVALID;
		RExprTypeUIAdapter selectedAdapter= null;
		for (final RExprTypeUIAdapter uiAdapters : this.uiAdapters) {
			final int priority= uiAdapters.isValidInput(input, getContext());
			if ((priority > PRIORITY_INVALID) && ((uiAdapters.getType().getTypeKey() == getCurrentTypeKey()) ?
					(priority >= selectedPriority) : (priority > selectedPriority) )) {
				selectedPriority= priority;
				selectedAdapter= uiAdapters;
			}
		}
		if (selectedAdapter != null) {
			final List<String> exprs= selectedAdapter.getInputExprs(input, getContext());
			if (exprs != null && exprs.size() == 1) {
				setExpr(selectedAdapter.getType().getTypeKey(), exprs.get(0), event.time);
				return true;
			}
		}
		return false;
	}
	
	
	protected String getCurrentTypeKey() {
		return null;
	}
	
	protected IContext getContext() {
		return null;
	}
	
	protected boolean setExpr(final String typeKey, final String expr, final int time) {
		return false;
	}
	
	protected boolean insertText(final String text) {
		return false;
	}
	
}