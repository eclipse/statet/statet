/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.base.ui.actions;

import java.util.List;

import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IWorkbenchPage;

import org.eclipse.statet.jcommons.status.ProgressMonitor;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ecommons.ts.ui.ToolRunnableDecorator;

import org.eclipse.statet.internal.rtm.base.ui.actions.Messages;
import org.eclipse.statet.r.core.RCore;
import org.eclipse.statet.r.core.pkgmanager.IRPkgManager;
import org.eclipse.statet.r.core.pkgmanager.RPkgUtils;
import org.eclipse.statet.r.ui.pkgmanager.RPkgManagerUI;
import org.eclipse.statet.rj.renv.core.REnv;
import org.eclipse.statet.rj.ts.core.RTool;
import org.eclipse.statet.rj.ts.core.console.AbstractRConsoleOptRunnable;
import org.eclipse.statet.rj.ts.core.console.RConsoleService;
import org.eclipse.statet.rtm.base.ui.RTaskSnippet;


public class RTaskRunnable extends AbstractRConsoleOptRunnable implements ToolRunnableDecorator {
	
	
	private static final int PACKAGE_CHECK= 1;
	
	protected static String createTypeId(final RTaskSnippet snippet) {
		return "r/rtask/" + snippet.getDescriptor().getTaskId(); //$NON-NLS-1$
	}
	
	protected static String createLabel(final RTaskSnippet snippet) {
		return snippet.getDescriptor().getName();
	}
	
	
	private final RTaskSnippet snippet;
	private final IWorkbenchPage page;
	
	private int state;
	
	
	public RTaskRunnable(final RTaskSnippet snippet, final IWorkbenchPage page) {
		this(createTypeId(snippet), snippet.getLabel(), snippet, page);
	}
	
	public RTaskRunnable(final String typeId, final String label, final RTaskSnippet snippet,
			final IWorkbenchPage page) {
		super(typeId, label);
		
		this.snippet= snippet;
		this.page= page;
	}
	
	
	@Override
	public Image getImage() {
		return this.snippet.getDescriptor().getImage();
	}
	
	@Override
	protected void run(final RConsoleService r, final ProgressMonitor m) throws StatusException {
		checkNewCommand(r, m);
		if (!checkPackages(r, m)) {
			return;
		}
		r.briefAboutToChange();
		try {
			r.submitToConsole(this.snippet.getRCode(), m);
		}
		finally {
			r.briefChanged(RConsoleService.AUTO_CHANGE);
		}
	}
	
	
	protected boolean checkPackages(final RConsoleService r,
			final ProgressMonitor m) throws StatusException {
		final RTool tool= r.getTool();
		final REnv rEnv= tool.getREnv();
		if (rEnv == null) {
			return true;
		}
		final IRPkgManager rPkgMgr= RCore.getRPkgManager(rEnv);
		if (rPkgMgr == null) {
			return true;
		}
		final List<String> requiredPkgs= this.snippet.getRequiredPkgs();
		if (RPkgUtils.areInstalled(rPkgMgr, requiredPkgs, r, m)) {
			return true;
		}
		if (this.state == PACKAGE_CHECK) {
			return false; // still missing
		}
		this.state= PACKAGE_CHECK;
		return RPkgManagerUI.requestRequiredRPkgs((IRPkgManager.Ext) rPkgMgr, requiredPkgs,
				r, m, this.page.getWorkbenchWindow(),
				NLS.bind(Messages.RunTask_RequirePkgs_message, this.snippet.getDescriptor().getName()),
				new Runnable() {
					@Override
					public void run() {
						tool.getQueue().add(RTaskRunnable.this);
					}
				}, null);
	}
	
}
