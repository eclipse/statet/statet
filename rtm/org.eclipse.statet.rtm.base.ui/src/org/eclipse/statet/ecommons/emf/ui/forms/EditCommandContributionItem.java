/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.edit.command.CommandActionDelegate;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;

import org.eclipse.statet.ecommons.ui.actions.SimpleContributionItem;


public class EditCommandContributionItem extends SimpleContributionItem {
	
	
	private final CommandParameter commandParameter;
	private final EditingDomain editingDomain;
	private final IObservableValue observable;
	
	private final Object owner;
	private final Command command;
	
	
	public EditCommandContributionItem(final CommandParameter commandParameter,
			final EditingDomain editingDomain, final IObservableValue observable) {
		super("", null); //$NON-NLS-1$
		
		this.commandParameter= commandParameter;
		this.editingDomain= editingDomain;
		this.observable= observable;
		
		this.owner= this.observable.getValue();
		this.command= createCommand();
		
		update((CommandActionDelegate) this.command);
	}
	
	
	protected Command createCommand() {
		return CreateChildCommand.create(this.editingDomain, this.owner,
				this.commandParameter, Collections.singletonList(this.owner) );
	}
	
	private void update(final CommandActionDelegate delegate) {
		setIcon(ExtendedImageRegistry.getInstance().getImageDescriptor(delegate.getImage()));
		setText(delegate.getText());
		setTooltip(delegate.getToolTipText());
	}
	
	@Override
	public boolean isEnabled() {
		return this.editingDomain != null && this.observable.getValue() == this.owner
				&& this.command.canExecute();
	}
	
	@Override
	protected void execute() throws ExecutionException {
		if (!isEnabled()) {
			return;
		}
		this.editingDomain.getCommandStack().execute(this.command);
		
		executed(this.command.getResult());
	}
	
	protected void executed(final Collection<?> result) {
	}
	
}
