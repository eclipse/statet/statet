/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.ui.forms.FormColors;
import org.eclipse.ui.forms.IFormColors;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.TableWrapData;


public class MasterDetailComposite extends SashForm {
	
	
	private class SashListener implements Listener {
		
		
		private boolean hover;
		
		
		public SashListener(final Sash sash) {
			sash.addListener(SWT.MouseEnter, this);
			sash.addListener(SWT.MouseExit, this);
			sash.addListener(SWT.Paint, this);
			sash.addListener(SWT.Dispose, this);
		}
		
		
		@Override
		public void handleEvent(final Event event) {
			switch (event.type) {
			case SWT.MouseEnter:
				this.hover= true;
				((Sash) event.widget).redraw();
				break;
			case SWT.MouseExit:
				this.hover= false;
				((Sash) event.widget).redraw();
				break;
			case SWT.Paint:
				paintSash(event);
				break;
			case SWT.Dispose:
				MasterDetailComposite.this.sashes.remove(event.widget);
				break;
			}
		}
		
		private void paintSash(final Event event) {
			final Sash sash= (Sash) event.widget;
			final IManagedForm form= MasterDetailComposite.this.form;
			final FormColors colors= form.getToolkit().getColors();
			final GC gc= event.gc;
			gc.setBackground(colors.getColor(IFormColors.TB_BG));
			gc.setForeground(colors.getColor(IFormColors.TB_BORDER));
			final Point size= sash.getSize();
			if ((sash.getStyle() & SWT.VERTICAL) != 0) {
				if (this.hover) {
					gc.fillRectangle(0, 0, size.x, size.y);
				//else
					//gc.drawLine(1, 0, 1, size.y-1);
				}
			}
			else {
				if (this.hover) {
					gc.fillRectangle(0, 0, size.x, size.y);
				//else
					//gc.drawLine(0, 1, size.x-1, 1);
				}
			}
		}
		
	}
	
	private final Composite master;
	private final Composite detail;
	
	private boolean checkLayout;
	private final Set<Sash> sashes= new HashSet<>();
	
	private final IManagedForm form;
	
	
	public MasterDetailComposite(final Composite parent, final IManagedForm managedForm) {
		super(parent, SWT.HORIZONTAL);
		
		this.form= managedForm;
		
		final FormToolkit toolkit= managedForm.getToolkit();
		toolkit.adapt(this, false, false);
		
		this.master= toolkit.createComposite(this);
		this.master.setLayout(EFLayoutUtil.createMainSashLeftLayout(1));
		this.detail= toolkit.createComposite(this);
		this.detail.setLayout(EFLayoutUtil.createMainSashRightLayout(1));
		
		setWeights(40, 60);
		
		this.checkLayout= true;
		
		final Listener listener= new Listener() {
			@Override
			public void handleEvent(final Event event) {
				checkSashes();
			}
		};
		this.master.addListener(SWT.Resize, listener);
		this.detail.addListener(SWT.Resize, listener);
	}
	
	
	public Composite getMasterContainer() {
		return this.master;
	}
	
	public Composite getDetailContainer() {
		return this.detail;
	}
	
	
	@Override
	public Point computeSize(final int wHint, final int hHint, final boolean changed) {
		if (this.checkLayout) {
			checkChildren();
		}
		return super.computeSize(wHint, hHint, changed);
	}
	
	@Override
	public void layout(final boolean changed, final boolean all) {
		if (this.checkLayout) {
			checkChildren();
		}
		super.layout(changed, all);
	}
	
	@Override
	public void layout(final Control[] changed, final int flags) {
		if (this.checkLayout) {
			checkChildren();
		}
		super.layout(changed, flags);
	}
	
	@Override
	public void setBounds(final int x, final int y, final int width, final int height) {
		if (this.checkLayout) {
			checkChildren();
		}
		super.setBounds(x, y, width, height);
	}
	
	
	public void checkChildren() {
		this.checkLayout= false;
		{	final Control[] children= this.master.getChildren();
			for (int i= 0; i < children.length; i++) {
				if (children[i].getLayoutData() == null) {
//					children[i].setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB, TableWrapData.FILL_GRAB));
					children[i].setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				}
			}
		}
		{	final Control[] children= this.detail.getChildren();
			for (int i= 0; i < children.length; i++) {
				if (children[i].getLayoutData() == null) {
					children[i].setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB, TableWrapData.FILL));
//					children[i].setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				}
			}
		}
	}
	
	private void checkSashes() {
		{	final Control [] children= getChildren();
			for (int i= 0; i < children.length; i++) {
				if (children[i] instanceof final Sash sash) {
					if (this.sashes.contains(sash)) {
						continue;
					}
					new SashListener(sash);
					this.sashes.add(sash);
				}
			}
		}
	}
	
}
