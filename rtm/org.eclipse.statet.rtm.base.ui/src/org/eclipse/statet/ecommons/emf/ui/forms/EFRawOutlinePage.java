/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;


public class EFRawOutlinePage extends ContentOutlinePage {
	
	
	private final EFEditor editor;
	
	/**
	 * This is the content outline page's viewer.
	 * @generated
	 */
	private TreeViewer viewer;
	
	private IStatusLineManager contentOutlineStatusLineManager;
	
	
	public EFRawOutlinePage(final EFEditor editor) {
		this.editor= editor;
	}
	
	
	@Override
	public void createControl(final Composite parent) {
		super.createControl(parent);
		this.viewer= getTreeViewer();
		this.viewer.addSelectionChangedListener(this);
		
		final EditingDomain editingDomain= this.editor.getEditingDomain();
		
		// Set up the tree viewer
		this.viewer.setContentProvider(new AdapterFactoryContentProvider(this.editor.getAdapterFactory()));
		this.viewer.setLabelProvider(new AdapterFactoryLabelProvider(this.editor.getAdapterFactory()));
		this.viewer.setInput(editingDomain.getResourceSet());
		
		// Make sure our popups work
		this.editor.createRawContextMenuFor(this.viewer);
		
		if (!editingDomain.getResourceSet().getResources().isEmpty()) {
			// Select the root object in the view
			this.viewer.setSelection(new StructuredSelection(editingDomain.getResourceSet().getResources().get(0)), true);
		}
	}
	
	@Override
	public void makeContributions(final IMenuManager menuManager, final IToolBarManager toolBarManager, final IStatusLineManager statusLineManager) {
		super.makeContributions(menuManager, toolBarManager, statusLineManager);
		this.contentOutlineStatusLineManager= statusLineManager;
	}
	
	@Override
	public void setActionBars(final IActionBars actionBars) {
		super.setActionBars(actionBars);
		this.editor.getActionBarContributor().shareGlobalActions(this, actionBars);
	}
	
}
