/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Layout;
import org.eclipse.ui.forms.widgets.TableWrapData;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;


public abstract class MainDetail extends Detail {
	
	
	private final List<EFFormSection> sections= new ArrayList<>();
	
	
	public MainDetail(final DetailStack parent) {
		super(parent);
	}
	
	
	protected void registerSection(final EFFormSection section) {
		this.sections.add(section);
	}
	
	
	@Override
	protected Layout createContentLayout() {
		return EFLayoutUtil.createMainTableLayout(1);
	}
	
	protected Object createSectionLayoutData() {
		return new TableWrapData(TableWrapData.FILL_GRAB);
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		for (final EFFormSection section : this.sections) {
			section.addBindings(context);
		}
	}
	
	
}
