/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.ecommons.emf.core.util.RuleSet;



public interface IEFModelDescriptor {
	
	
	String getModelPluginID();
	
	String getEditorPluginID();
	String getEditorID();
	
	
	Image getImage();
	
	String getName();
	
	
	String getDefaultContentTypeID();
	
	String getDefaultFileExtension();
	
	List<String> getFileExtensions();
	
	
	AdapterFactory createItemProviderAdapterFactory();
	
	RuleSet getRuleSet();
	
	
}
