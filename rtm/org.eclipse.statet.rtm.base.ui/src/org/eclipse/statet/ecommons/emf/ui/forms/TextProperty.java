/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.databinding.edit.IEMFEditValueProperty;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;


public class TextProperty extends EFProperty {
	
	
	private Text widget;
	
	private IObservableValue<String> modelObservable;
	
	
	public TextProperty(final String label, final String tooltip,
			final EClass eClass, final EStructuralFeature eFeature) {
		super(label, tooltip, eClass, eFeature);
	}
	
	
	@Override
	public void create(final Composite parent, final IEFFormPage page) {
		final EFToolkit toolkit= page.getToolkit();
		
		toolkit.createPropLabel(parent, getLabel(), getTooltip());
		this.widget= toolkit.createPropTextField(parent, 40);
	}
	
	@Override
	public Control getControl() {
		return this.widget;
	}
	
	@Override
	public void bind(final IEMFEditContext context) {
		super.bind(context);
		
		final IEMFEditValueProperty emfProperty= EMFEditProperties.value(getEditingDomain(),
				getEFeature() );
		this.modelObservable= emfProperty.observeDetail(getBaseObservable());
		final IObservableValue<String> swtObservable= WidgetProperties.text(SWT.Modify)
				.observe(getRealm(), this.widget );
		
		getDataBindingContext().bindValue(
				swtObservable,
				this.modelObservable);
	}
	
	@Override
	public IObservableValue<String> getPropertyObservable() {
		return this.modelObservable;
	}
	
}
