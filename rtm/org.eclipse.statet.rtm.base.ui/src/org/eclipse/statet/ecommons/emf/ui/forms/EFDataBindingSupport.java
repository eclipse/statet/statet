/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.EMFDataBindingContext;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.widgets.Control;

import org.eclipse.statet.ecommons.databinding.jface.DataBindingSupport;
import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;


public class EFDataBindingSupport extends DataBindingSupport implements IEMFEditContext {
	
	
	private final EFEditor editor;
	
	private final IObservableValue<EObject> rootObject;
	
	private final IAdapterFactory adapterFactory;
	
	
	public EFDataBindingSupport(final EFEditor editor, final IAdapterFactory adapterFactory) {
		super(editor.getAdapter(Control.class));
		this.editor= editor;
		
		final Resource resource= this.editor.getEditingDomain().getResourceSet().getResources().get(0);
		this.rootObject= new WritableValue<>(getRealm(), getRootObject(resource), EObject.class);
		resource.eAdapters().add(new AdapterImpl() {
			@Override
			public void notifyChanged(final Notification notification) {
				if (notification.getFeatureID(Resource.class) == Resource.RESOURCE__IS_LOADED
						&& notification.getNewBooleanValue()) {
					getRealm().exec(new Runnable() {
						@Override
						public void run() {
							final EObject rootObject= getRootObject((Resource) notification.getNotifier());
							EFDataBindingSupport.this.rootObject.setValue(rootObject);
						}
					});
				}
			}
		});
		
		this.adapterFactory= adapterFactory;
	}
	
	@Override
	protected DataBindingContext createContext(final Realm realm) {
		return new EMFDataBindingContext(realm);
	}
	
	
	protected EObject getRootObject(final Resource resource) {
		return resource.getContents().get(0);
	}
	
	@Override
	public EditingDomain getEditingDomain() {
		return this.editor.getEditingDomain();
	}
	
	public IObservableValue<EObject> getRootObservable() {
		return this.rootObject;
	}
	
	@Override
	public DataBindingContext getDataBindingContext() {
		return getContext();
	}
	
	@Override
	public IObservableValue<EObject> getBaseObservable() {
		return getRootObservable();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getAdapter(final Class<T> adapterType) {
		if (adapterType.isInstance(this)) {
			return (T) this;
		}
		if (adapterType.equals(IShellProvider.class)) {
			return (T) this.editor.getSite();
		}
		if (adapterType.equals(EFEditor.class)) {
			return (T) this.editor;
		}
		if (this.adapterFactory != null) {
			return this.adapterFactory.getAdapter(this, adapterType);
		}
		return null;
	}
	
}
