/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.emf.ui.forms;

import org.eclipse.ui.forms.IFormColors;


public interface IEFColors extends IFormColors {
	
	
	String TW_PREFIX= "org.eclipse.statet.ecommons.TypedWidget"; //$NON-NLS-1$
	
	String TW_TYPE_BACKGROUND= TW_PREFIX + "/TypeBackground"; //$NON-NLS-1$
	String TW_TYPE_BORDER2= TW_PREFIX + "/TypeBorder2"; //$NON-NLS-1$
	String TW_TYPE_HOVER= TW_PREFIX + "/TypeHover"; //$NON-NLS-1$
	
	
}
