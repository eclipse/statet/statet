/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.base.ui.actions;

import org.eclipse.osgi.util.NLS;


public class Messages extends NLS {
	
	
	public static String NewRTaskWizard_title;
	public static String NewRTaskWizard_NewFile_title;
	public static String NewRTaskWizard_NewFile_description;
	public static String NewRTaskWizard_error_CreateFile_message;
	public static String NewRTaskWizard_error_OpenEditor_message;
	
	public static String NewTask_PerspSwitch_title;
	public static String NewTask_PerspSwitch_message;
	public static String NewTask_PerspSwitch_WithDesc_message;
	
	public static String RunTask_RequirePkgs_message;
	
	
	static {
		NLS.initializeMessages(Messages.class.getName(), Messages.class);
	}
	private Messages() {}
	
}
