/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.base.ui.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;

import org.eclipse.statet.jcommons.status.Status;
import org.eclipse.statet.jcommons.ts.core.Tool;

import org.eclipse.statet.ecommons.runtime.core.util.StatusUtils;

import org.eclipse.statet.ltk.ui.util.LTKWorkbenchUIUtil;
import org.eclipse.statet.nico.ui.NicoUITools;
import org.eclipse.statet.rj.ts.core.RTool;
import org.eclipse.statet.rtm.base.ui.RTaskSnippet;
import org.eclipse.statet.rtm.base.ui.actions.RTaskRunnable;


public class RunRTaskHandler extends AbstractHandler {
	
	
	private IWorkbenchPart part;
	
	
	public RunRTaskHandler() {
	}
	
	public RunRTaskHandler(final IWorkbenchPart part) {
		this.part= part;
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPart part= (this.part != null) ? this.part : HandlerUtil.getActivePart(event);
		if (part == null) {
			return null;
		}
		final RTaskSnippet snippet= part.getAdapter(RTaskSnippet.class);
		if (snippet == null) {
			return null;
		}
		try {
			final Tool tool= NicoUITools.getTool(part);
			NicoUITools.accessController(RTool.TYPE, RTool.R_SERVICE_FEATURE_ID, tool);
			final RTaskRunnable runnable= new RTaskRunnable(snippet, part.getSite().getPage());
			final Status status= tool.getQueue().add(runnable);
			if (status.getSeverity() == Status.ERROR) {
				throw new CoreException(StatusUtils.convert(status));
			}
		}
		catch (final CoreException e) {
			LTKWorkbenchUIUtil.indicateStatus(e.getStatus(), event);
		}
		return null;
	}
	
}
