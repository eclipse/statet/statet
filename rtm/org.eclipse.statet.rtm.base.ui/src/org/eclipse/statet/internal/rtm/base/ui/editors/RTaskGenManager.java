/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.base.ui.editors;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.ISynchronizable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.rtm.base.core.AbstractRCodeGenerator;
import org.eclipse.statet.rtm.base.ui.IRtDescriptor;
import org.eclipse.statet.rtm.base.ui.RTaskSnippet;
import org.eclipse.statet.rtm.base.ui.editors.RTaskEditor;


public class RTaskGenManager extends EContentAdapter {
	
	
	private final RTaskEditor editor;
	
	private final AbstractRCodeGenerator codeGenerator;
	
	private final RCodeGenSourceFragment codeFragment;
	
	private volatile boolean runnableScheduled;
	
	private final Runnable runnable= new Runnable() {
		
		@Override
		public void run() {
			RTaskGenManager.this.runnableScheduled= false;
			
			update();
		}
		
	};
	
	private RTaskSnippet currentTaskSnippet;
	
	
	public RTaskGenManager(final RTaskEditor editor) {
		this.editor= editor;
		final IRtDescriptor descriptor= this.editor.getModelDescriptor();
		this.codeGenerator= descriptor.createCodeGenerator();
		this.codeFragment= new RCodeGenSourceFragment(Messages.RTaskEditor_RCodePage_label,
				Messages.RTaskEditor_RCodePage_label + " - " + descriptor.getName()); //$NON-NLS-1$
	}
	
	
	@Override
	public void notifyChanged(final Notification notification) {
		super.notifyChanged(notification);
		
		if (this.runnableScheduled) {
			return;
		}
		UIAccess.getDisplay().asyncExec(this.runnable);
	}
	
	private void update() {
		this.currentTaskSnippet= createRTaskSnippet();
		final String code= this.currentTaskSnippet.getRCode();
		final AbstractDocument document= this.codeFragment.getDocument();
		if (document instanceof ISynchronizable) {
			Object lockObject= ((ISynchronizable) document).getLockObject();
			if (lockObject == null) {
				lockObject= this.codeFragment;
			}
			synchronized (lockObject) {
				document.set(code);
			}
		}
		else {
			document.set(code);
		}
	}
	
	private RTaskSnippet createRTaskSnippet() {
		final EObject root= this.editor.getDataBinding().getRootObservable().getValue();
		this.codeGenerator.generate(root);
		
		final IRtDescriptor descriptor= this.editor.getModelDescriptor();
		final String label= descriptor.getName() + " (" + this.editor.getEditorInput().getName() + ")"; //$NON-NLS-1$ //$NON-NLS-2$
		return new RTaskSnippet(descriptor, label,
				this.codeGenerator.getRequiredPkgs(), this.codeGenerator.getRCode());
	}
	
	public RCodeGenSourceFragment getCodeFragment() {
		return this.codeFragment;
	}
	
	public RTaskSnippet getRTaskSnippet() {
		return this.currentTaskSnippet;
	}
	
}
