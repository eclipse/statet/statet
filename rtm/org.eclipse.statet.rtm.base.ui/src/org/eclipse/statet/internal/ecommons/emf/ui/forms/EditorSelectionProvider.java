/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.emf.ui.forms;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.forms.editor.IFormPage;

import org.eclipse.statet.jcommons.collections.ImCollections;

import org.eclipse.statet.ecommons.emf.ui.forms.EFEditor;
import org.eclipse.statet.ecommons.ui.util.PostSelectionProviderProxy;


public class EditorSelectionProvider extends PostSelectionProviderProxy {
	
	
	private final EFEditor editor;
	
	
	public EditorSelectionProvider(final EFEditor editor) {
		this.editor= editor;
	}
	
	
	public EFEditor getEditor() {
		return this.editor;
	}
	
	private ISelectionProvider getActiveSelectionProvider() {
		final IFormPage page= getEditor().getActivePageInstance();
		{	final ISelectionProvider selectionProvider= page.getAdapter(ISelectionProvider.class);
			if (selectionProvider != null) {
				return selectionProvider;
			}
		}
		{	final IEditorPart activeEditor= this.editor.getActiveEditor();
			if (activeEditor != null) {
				final ISelectionProvider selectionProvider= activeEditor.getSite().getSelectionProvider();
				if (selectionProvider != null) {
					return selectionProvider;
				}
			}
		}
		return null;
	}
	
	public void update() {
		setSelectionProvider(getActiveSelectionProvider());
	}
	
	@Override
	protected ISelection getSelection(final ISelection originalSelection) {
		final ISelection selection= super.getSelection(originalSelection);
		if (selection instanceof IStructuredSelection) {
			final EObject base= getEditor().getDataBinding().getBaseObservable().getValue();
			final List<Object> list= ((IStructuredSelection) selection).toList();
			if (base != null && !list.contains(base)) {
				return new StructuredSelection(ImCollections.addElement(list, 0, base));
			}
		}
		return selection;
	}
	
}
