/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.emf.ui.forms;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.IObserving;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.PasteFromClipboardCommand;
import org.eclipse.jface.viewers.IStructuredSelection;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditPropertyContext;


public class PasteEObjectHandler extends EditEObjectHandler {
	
	
	public PasteEObjectHandler() {
	}
	
	
	@Override
	protected Command createCommand(final IStructuredSelection selection,
			final IEMFEditPropertyContext context) {
		final IObservable observable= context.getPropertyObservable();
		final EObject owner= (EObject) ((IObserving) observable).getObserved();
		final int index= CommandParameter.NO_INDEX;
//		if (observable instanceof IObservableList && selection.size() == 1) {
//			index= ((IObservableList) observable).indexOf(selection.getFirstElement());
//		}
		return PasteFromClipboardCommand.create(context.getEditingDomain(), owner,
				context.getEFeature(), index);
	}
	
}
