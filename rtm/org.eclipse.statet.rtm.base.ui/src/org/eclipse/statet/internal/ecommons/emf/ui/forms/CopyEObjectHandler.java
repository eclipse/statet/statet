/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.emf.ui.forms;

import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.edit.command.CopyToClipboardCommand;
import org.eclipse.jface.viewers.IStructuredSelection;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditPropertyContext;


public class CopyEObjectHandler extends EditEObjectHandler {
	
	
	public CopyEObjectHandler() {
	}
	
	
	@Override
	protected boolean isValidSelection(final IStructuredSelection selection) {
		return (super.isValidSelection(selection) && !selection.isEmpty());
	}
	
	@Override
	protected Command createCommand(final IStructuredSelection selection,
			final IEMFEditPropertyContext context) {
		final ImList<Object> collection= ImCollections.toList((List<Object>) selection.toList());
		return CopyToClipboardCommand.create(context.getEditingDomain(), collection);
	}
	
}
