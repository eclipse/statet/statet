/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.emf.ui.forms;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditPropertyContext;
import org.eclipse.statet.ecommons.models.core.util.ElementSourceProvider;
import org.eclipse.statet.ecommons.ui.viewers.ViewerUtils;
import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;


public abstract class EditEObjectHandler extends AbstractHandler {
	
	
	public EditEObjectHandler() {
	}
	
	
	protected ISelection getSelection(final Object context) {
		final ISelectionProvider selectionProvider= ViewerUtils.getSelectionProvider(
				WorkbenchUIUtils.getActiveFocusControl(context) );
		return (selectionProvider != null) ? selectionProvider.getSelection() : null;
	}
	
	@Override
	public void setEnabled(final Object evaluationContext) {
		final ISelection selection= getSelection(evaluationContext);
		setBaseEnabled(selection instanceof IStructuredSelection
				&& isValidSelection((IStructuredSelection) selection));
	}
	
	protected boolean isValidSelection(final IStructuredSelection selection) {
		if (selection instanceof ElementSourceProvider) {
			final Object elementSource= ((ElementSourceProvider) selection).getElementSource();
			return (elementSource instanceof IEMFEditPropertyContext
					&& ((IEMFEditPropertyContext) elementSource).getEFeature() instanceof EReference);
		}
		return false;
	}
	
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final ISelection selection= getSelection(event.getApplicationContext());
		if (selection instanceof IStructuredSelection && isValidSelection((IStructuredSelection) selection)) {
			final IEMFEditPropertyContext context= (IEMFEditPropertyContext) ((ElementSourceProvider) selection).getElementSource();
			final Command command= createCommand((IStructuredSelection) selection, context);
			if (command.canExecute()) {
				context.getEditingDomain().getCommandStack().execute(command);
			}
		}
		return null;
	}
	
	protected abstract Command createCommand(IStructuredSelection selection,
			IEMFEditPropertyContext context);
	
}
