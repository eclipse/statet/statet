/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ecommons.emf.ui.forms;

import java.util.Collection;
import java.util.HashMap;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;


public class EFEditingDomain extends AdapterFactoryEditingDomain {
	
	
	private static Collection<Object> gClipboard;
	
	
	public EFEditingDomain(final AdapterFactory adapterFactory, final CommandStack commandStack) {
		super(adapterFactory, commandStack, new HashMap<>());
	}
	
	
	@Override
	public Collection<Object> getClipboard() {
		return gClipboard;
	}
	
	@Override
	public void setClipboard(final Collection<Object> clipboard) {
		gClipboard= clipboard;
	}
	
}
