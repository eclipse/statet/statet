/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ftable.ui.editors;

import org.eclipse.ui.PartInitException;

import org.eclipse.statet.rtm.base.ui.editors.RTaskEditor;
import org.eclipse.statet.rtm.ftable.ui.RtFTableDescriptor;


public class FTableEditor extends RTaskEditor {
	
	
	public FTableEditor() {
		super(RtFTableDescriptor.INSTANCE);
	}
	
	
	@Override
	protected void addFormPages() throws PartInitException {
		addPage(new MainPage(this));
	}
	
	
}
