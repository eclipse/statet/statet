/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ftable.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rtm.base.ui.RtModelUIPlugin;
import org.eclipse.statet.rtm.base.ui.rexpr.DefaultRExprTypeUIAdapters;
import org.eclipse.statet.rtm.base.ui.rexpr.IRExprTypesUIProvider;
import org.eclipse.statet.rtm.base.ui.rexpr.RExprTypeUIAdapter;
import org.eclipse.statet.rtm.base.util.RExprType;
import org.eclipse.statet.rtm.base.util.RExprTypes;
import org.eclipse.statet.rtm.ftable.core.FTableExprTypesProvider;


public class FTableExprTypesUIProvider extends FTableExprTypesProvider
		implements IRExprTypesUIProvider {
	
	
	private static final DefaultRExprTypeUIAdapters ADAPTERS= new DefaultRExprTypeUIAdapters();
	
	
	private static final RExprTypeUIAdapter FTABLE_DATA_ADAPTER= new DefaultRExprTypeUIAdapters
			.RObjectTypeAdapter(RExprType.DATAFRAME_TYPE,
					RtModelUIPlugin.getInstance().getImageRegistry().get(RtModelUIPlugin.OBJ_DATAFRAME_TYPE_IMAGE_ID) ) {
		
		@Override
		protected boolean isValidRObject(final RObject rObject) {
			switch (rObject.getRObjectType()) {
			case RObject.TYPE_DATAFRAME:
				return true;
			case RObject.TYPE_ARRAY:
				return (((RArray<?>) rObject).getDim().getLength() >= 2);
			default:
				return false;
			}
		}
		
	};
	
	
	@Override
	public List<RExprTypeUIAdapter> getUIAdapters(final RExprTypes types,
			final EClass eClass, final EStructuralFeature eFeature) {
		final List<RExprTypeUIAdapter> uiAdapters= new ArrayList<>();
		for (final RExprType type : types.getTypes()) {
			RExprTypeUIAdapter uiAdapter= null;
			if (type == RExprType.DATAFRAME_TYPE) {
				uiAdapter= FTABLE_DATA_ADAPTER;
			}
			else {
				uiAdapter= ADAPTERS.getUIAdapter(type, eFeature);
			}
			if (uiAdapter != null) {
				uiAdapters.add(uiAdapter);
			}
		}
		return uiAdapters;
	}
	
}
