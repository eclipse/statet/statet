/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ftable.ui;

import java.util.List;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.emf.core.util.RuleSet;

import org.eclipse.statet.rtm.base.core.AbstractRCodeGenerator;
import org.eclipse.statet.rtm.base.ui.IRtDescriptor;
import org.eclipse.statet.rtm.ftable.FTable;
import org.eclipse.statet.rtm.ftable.FTablePackage;
import org.eclipse.statet.rtm.ftable.core.FTableRCodeGen;
import org.eclipse.statet.rtm.ftable.core.FTableRuleSet;
import org.eclipse.statet.rtm.ftable.provider.FTableItemProviderAdapterFactory;
import org.eclipse.statet.rtm.ftable.util.FTableAdapterFactory;


public class RtFTableDescriptor implements IRtDescriptor {
	
	
	public static final IRtDescriptor INSTANCE= new RtFTableDescriptor();
	
	
	private static final String XMI_FILE_EXTENSION= "Rtx-ftable"; //$NON-NLS-1$
	
	private static final ImList<String> FILE_EXTENSIONS= ImCollections.newList(
			XMI_FILE_EXTENSION );
	
	
	public RtFTableDescriptor() {
	}
	
	
	@Override
	public String getTaskId() {
		return RtFTableEditorPlugin.RT_ID;
	}
	
	@Override
	public String getModelPluginID() {
		return "org.eclipse.statet.rtm.ftable.core"; //$NON-NLS-1$
	}
	
	@Override
	public String getEditorPluginID() {
		return "org.eclipse.statet.rtm.ftable.ui"; //$NON-NLS-1$
	}
	
	@Override
	public String getEditorID() {
		return "org.eclipse.statet.rtm.ftable.editors.FTable"; //$NON-NLS-1$
	}
	
	@Override
	public Image getImage() {
		return RtFTableEditorPlugin.getPlugin().getImageRegistry().get(RtFTableEditorPlugin.IMG_OBJ_FTABLE_TASK);
	}
	
	@Override
	public String getName() {
		return "Contingency Table ('ftable')";
	}
	
	@Override
	public String getDefaultContentTypeID() {
		return FTablePackage.eCONTENT_TYPE;
	}
	
	@Override
	public String getDefaultFileExtension() {
		return XMI_FILE_EXTENSION;
	}
	
	@Override
	public String getAssociatedPerspectiveId() {
		return null;
	}
	
	@Override
	public List<String> getFileExtensions() {
		return FILE_EXTENSIONS;
	}
	
	@Override
	public FTablePackage getEPackage() {
		return FTablePackage.eINSTANCE;
	}
	
	@Override
	public FTable createInitialModelObject() {
		return getEPackage().getFTableFactory().createFTable();
	}
	
	@Override
	public FTableAdapterFactory createItemProviderAdapterFactory() {
		return new FTableItemProviderAdapterFactory();
	}
	
	@Override
	public RuleSet getRuleSet() {
		return FTableRuleSet.INSTANCE;
	}
	
	@Override
	public AbstractRCodeGenerator createCodeGenerator() {
		return new FTableRCodeGen();
	}
	
}
