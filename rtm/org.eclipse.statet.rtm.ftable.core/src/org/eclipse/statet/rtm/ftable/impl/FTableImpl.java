/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ftable.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.eclipse.statet.rtm.ftable.FTable;
import org.eclipse.statet.rtm.ftable.FTablePackage;
import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FTable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ftable.impl.FTableImpl#getData <em>Data</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ftable.impl.FTableImpl#getDataFilter <em>Data Filter</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ftable.impl.FTableImpl#getColVars <em>Col Vars</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ftable.impl.FTableImpl#getRowVars <em>Row Vars</em>}</li>
 * </ul>
 * </p>
 * 
 * @generated
 */
public class FTableImpl extends EObjectImpl implements FTable {
	/**
	 * The default value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr DATA_EDEFAULT= null;
	/**
	 * The cached value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr data= DATA_EDEFAULT;
	/**
	 * The default value of the '{@link #getDataFilter() <em>Data Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataFilter()
	 * @generated
	 * @ordered
	 */
	protected static final RTypedExpr DATA_FILTER_EDEFAULT= null;
	/**
	 * The cached value of the '{@link #getDataFilter() <em>Data Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataFilter()
	 * @generated
	 * @ordered
	 */
	protected RTypedExpr dataFilter= DATA_FILTER_EDEFAULT;
	/**
	 * The cached value of the '{@link #getColVars() <em>Col Vars</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColVars()
	 * @generated
	 * @ordered
	 */
	protected EList<RTypedExpr> colVars;
	/**
	 * The cached value of the '{@link #getRowVars() <em>Row Vars</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRowVars()
	 * @generated
	 * @ordered
	 */
	protected EList<RTypedExpr> rowVars;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FTableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FTablePackage.Literals.FTABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getData() {
		return this.data;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setData(final RTypedExpr newData) {
		final RTypedExpr oldData= this.data;
		this.data= newData;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, FTablePackage.FTABLE__DATA, oldData, this.data));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RTypedExpr getDataFilter() {
		return this.dataFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDataFilter(final RTypedExpr newDataFilter) {
		final RTypedExpr oldDataFilter= this.dataFilter;
		this.dataFilter= newDataFilter;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, FTablePackage.FTABLE__DATA_FILTER, oldDataFilter, this.dataFilter));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RTypedExpr> getColVars() {
		if (this.colVars == null) {
			this.colVars= new EDataTypeUniqueEList<>(RTypedExpr.class, this, FTablePackage.FTABLE__COL_VARS);
		}
		return this.colVars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RTypedExpr> getRowVars() {
		if (this.rowVars == null) {
			this.rowVars= new EDataTypeUniqueEList<>(RTypedExpr.class, this, FTablePackage.FTABLE__ROW_VARS);
		}
		return this.rowVars;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(final int featureID, final boolean resolve, final boolean coreType) {
		switch (featureID) {
			case FTablePackage.FTABLE__DATA:
				return getData();
			case FTablePackage.FTABLE__DATA_FILTER:
				return getDataFilter();
			case FTablePackage.FTABLE__COL_VARS:
				return getColVars();
			case FTablePackage.FTABLE__ROW_VARS:
				return getRowVars();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(final int featureID, final Object newValue) {
		switch (featureID) {
			case FTablePackage.FTABLE__DATA:
				setData((RTypedExpr)newValue);
				return;
			case FTablePackage.FTABLE__DATA_FILTER:
				setDataFilter((RTypedExpr)newValue);
				return;
			case FTablePackage.FTABLE__COL_VARS:
				getColVars().clear();
				getColVars().addAll((Collection<? extends RTypedExpr>)newValue);
				return;
			case FTablePackage.FTABLE__ROW_VARS:
				getRowVars().clear();
				getRowVars().addAll((Collection<? extends RTypedExpr>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(final int featureID) {
		switch (featureID) {
			case FTablePackage.FTABLE__DATA:
				setData(DATA_EDEFAULT);
				return;
			case FTablePackage.FTABLE__DATA_FILTER:
				setDataFilter(DATA_FILTER_EDEFAULT);
				return;
			case FTablePackage.FTABLE__COL_VARS:
				getColVars().clear();
				return;
			case FTablePackage.FTABLE__ROW_VARS:
				getRowVars().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(final int featureID) {
		switch (featureID) {
			case FTablePackage.FTABLE__DATA:
				return DATA_EDEFAULT == null ? this.data != null : !DATA_EDEFAULT.equals(this.data);
			case FTablePackage.FTABLE__DATA_FILTER:
				return DATA_FILTER_EDEFAULT == null ? this.dataFilter != null : !DATA_FILTER_EDEFAULT.equals(this.dataFilter);
			case FTablePackage.FTABLE__COL_VARS:
				return this.colVars != null && !this.colVars.isEmpty();
			case FTablePackage.FTABLE__ROW_VARS:
				return this.rowVars != null && !this.rowVars.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		final StringBuffer result= new StringBuffer(super.toString());
		result.append(" (data: "); //$NON-NLS-1$
		result.append(this.data);
		result.append(", dataFilter: "); //$NON-NLS-1$
		result.append(this.dataFilter);
		result.append(", colVars: "); //$NON-NLS-1$
		result.append(this.colVars);
		result.append(", rowVars: "); //$NON-NLS-1$
		result.append(this.rowVars);
		result.append(')');
		return result.toString();
	}

} //FTableImpl
