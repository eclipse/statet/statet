/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ftable;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FTable</b></em>'.
 * <!-- end-user-doc -->
 * 
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.statet.rtm.ftable.FTable#getData <em>Data</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ftable.FTable#getDataFilter <em>Data Filter</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ftable.FTable#getColVars <em>Col Vars</em>}</li>
 *   <li>{@link org.eclipse.statet.rtm.ftable.FTable#getRowVars <em>Row Vars</em>}</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.statet.rtm.ftable.FTablePackage#getFTable()
 * @model
 * @generated
 */
public interface FTable extends EObject {

	/**
	 * Returns the value of the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' attribute.
	 * @see #setData(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ftable.FTablePackage#getFTable_Data()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RDataFrame"
	 * @generated
	 */
	RTypedExpr getData();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ftable.FTable#getData <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data</em>' attribute.
	 * @see #getData()
	 * @generated
	 */
	void setData(RTypedExpr value);

	/**
	 * Returns the value of the '<em><b>Data Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Filter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Filter</em>' attribute.
	 * @see #setDataFilter(RTypedExpr)
	 * @see org.eclipse.statet.rtm.ftable.FTablePackage#getFTable_DataFilter()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RDataFilter"
	 * @generated
	 */
	RTypedExpr getDataFilter();

	/**
	 * Sets the value of the '{@link org.eclipse.statet.rtm.ftable.FTable#getDataFilter <em>Data Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Filter</em>' attribute.
	 * @see #getDataFilter()
	 * @generated
	 */
	void setDataFilter(RTypedExpr value);

	/**
	 * Returns the value of the '<em><b>Col Vars</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.statet.rtm.rtdata.types.RTypedExpr}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Col Vars</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Col Vars</em>' attribute list.
	 * @see org.eclipse.statet.rtm.ftable.FTablePackage#getFTable_ColVars()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RVar"
	 * @generated
	 */
	EList<RTypedExpr> getColVars();

	/**
	 * Returns the value of the '<em><b>Row Vars</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.statet.rtm.rtdata.types.RTypedExpr}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row Vars</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row Vars</em>' attribute list.
	 * @see org.eclipse.statet.rtm.ftable.FTablePackage#getFTable_RowVars()
	 * @model dataType="org.eclipse.statet.rtm.rtdata.RVar"
	 * @generated
	 */
	EList<RTypedExpr> getRowVars();
} // FTable
