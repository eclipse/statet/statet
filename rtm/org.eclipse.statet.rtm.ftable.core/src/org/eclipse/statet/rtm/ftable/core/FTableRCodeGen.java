/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ftable.core;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.statet.rtm.base.core.AbstractRCodeGenerator;
import org.eclipse.statet.rtm.ftable.FTable;
import org.eclipse.statet.rtm.ftable.FTablePackage.Literals;
import org.eclipse.statet.rtm.rtdata.types.RTypedExpr;


public class FTableRCodeGen extends AbstractRCodeGenerator {
	
	
	private final String tableVar= "ft"; //$NON-NLS-1$
	
	
	@Override
	public void generate(final EObject root) {
		reset();
//		addRequirePackage("stats"); //$NON-NLS-1$
		if (root == null) {
			return;
		}
		if (root.eClass() != Literals.FTABLE) {
			throw new IllegalArgumentException("root: " + root.eClass().getName()); //$NON-NLS-1$
		}
		appendNewLine();
		genRCode((FTable) root);
		
		final FunBuilder printFun= appendFun("openInEditor"); //$NON-NLS-1$
		printFun.append("elementName", this.tableVar); //$NON-NLS-1$
		printFun.close();
		appendNewLine();
	}
	
	public void genRCode(final FTable table) {
		appendAssign(this.tableVar);
		{	
			final FunBuilder fun= appendFun("ftable"); //$NON-NLS-1$
			appendData(fun, table);
			appendVars(fun, "col.vars", table.getColVars()); //$NON-NLS-1$
			appendVars(fun, "row.vars", table.getRowVars()); //$NON-NLS-1$
			fun.close();
			appendNewLine();
		}
	}
	
	private void appendData(final FunBuilder fun, final FTable obj) {
		if (obj.getData() != null) {
			String expr= obj.getData().getExpr();
			final RTypedExpr dataFilter= obj.getDataFilter();
			if (dataFilter != null) {
				expr+= dataFilter.getExpr();
			}
			fun.append("x", expr); //$NON-NLS-1$
		}
	}
	
	private void appendVars(final FunBuilder fun, final String argName, final List<RTypedExpr> exprs) {
		if (exprs.isEmpty()) {
			return;
		}
		fun.appendEmpty(argName);
		appendExprsC(exprs, QUOTE_PROCESSOR);
	}
	
}
