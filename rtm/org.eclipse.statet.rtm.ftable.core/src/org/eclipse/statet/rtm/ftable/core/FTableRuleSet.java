/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ftable.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.statet.ecommons.emf.core.util.EFeatureReference;
import org.eclipse.statet.ecommons.emf.core.util.RuleSet;

import org.eclipse.statet.rtm.ftable.FTable;
import org.eclipse.statet.rtm.ftable.FTablePackage.Literals;


public class FTableRuleSet extends RuleSet {
	
	
	public static final String DATA_PARENT_FEATURES_ID= "data" + PARENT_FEATURES_ID_SUFFIX; //$NON-NLS-1$
	
	public static final RuleSet INSTANCE= new FTableRuleSet();
	
	
	@Override
	public Object get(final EObject eObject, final EStructuralFeature eFeature, final String id) {
		if (id.equals(DISJOINT_FEATURES_ID)) {
			if (eObject != null && eObject.eClass() == Literals.FTABLE
					&& (eFeature == Literals.FTABLE__COL_VARS
							|| eFeature == Literals.FTABLE__ROW_VARS)) {
				
				final List<EFeatureReference> features= new ArrayList<>(2);
				features.add(new EFeatureReference(eObject, Literals.FTABLE__COL_VARS));
				features.add(new EFeatureReference(eObject, Literals.FTABLE__ROW_VARS));
				return features;
			}
			return null;
		}
		if (id.equals(DATA_PARENT_FEATURES_ID)) {
			if (eObject != null) {
				final List<EFeatureReference> features= new ArrayList<>(2);
				EObject obj= eObject;
				do {
					if (obj instanceof FTable) {
						features.add(new EFeatureReference(obj, Literals.FTABLE__DATA));
					}
					obj= obj.eContainer();
				} while (obj != null);
				if (!features.isEmpty()) {
					return features;
				}
			}
			return null;
		}
		return null;
	}
	
}
