/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.swt.widgets.Layout;

import org.eclipse.statet.ecommons.emf.ui.forms.Detail;
import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;
import org.eclipse.statet.ecommons.emf.ui.forms.EFLayoutUtil;


public abstract class FacetDetail extends Detail {
	
	
	public FacetDetail(final DetailStack parent) {
		super(parent);
	}
	
	
	@Override
	protected Layout createContentLayout() {
		return EFLayoutUtil.createCompositeColumnGridLayout(3);
	}
	
}
