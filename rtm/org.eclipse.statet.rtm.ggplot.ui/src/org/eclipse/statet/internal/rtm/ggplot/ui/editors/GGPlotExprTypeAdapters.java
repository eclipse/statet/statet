/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.statet.rtm.base.ui.rexpr.DefaultRExprTypeUIAdapters;
import org.eclipse.statet.rtm.base.ui.rexpr.RExprTypeUIAdapter;
import org.eclipse.statet.rtm.base.util.RExprType;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;


public class GGPlotExprTypeAdapters extends DefaultRExprTypeUIAdapters {
	
	
	public GGPlotExprTypeAdapters() {
	}
	
	
	@Override
	public RExprTypeUIAdapter getUIAdapter(final RExprType type, final EStructuralFeature eFeature) {
		if (eFeature == Literals.GG_PLOT__MAIN_TITLE
				|| eFeature == Literals.GG_PLOT__AX_XLABEL
				|| eFeature == Literals.GG_PLOT__AX_YLABEL
				|| eFeature == Literals.GEOM_TEXT_LAYER__LABEL) {
			if (type == RExprType.TEXT_VALUE_TYPE) {
				return LABEL_TEXT_VALUE_LINK_ADAPTER;
			}
			if (type == RExprType.EXPR_LABEL_VALUE_TYPE) {
				return LABEL_EXPR_VALUE_LINK_ADAPTER;
			}
			if (type == RExprType.DATAFRAME_COLUMN_TYPE) {
				return DATAFRAME_COLUMN_LINK_ADAPTER;
			}
		}
		return super.getUIAdapter(type, eFeature);
	}
	
}
