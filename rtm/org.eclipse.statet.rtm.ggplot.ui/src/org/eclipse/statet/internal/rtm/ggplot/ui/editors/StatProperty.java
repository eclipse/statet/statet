/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.Detail;
import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;
import org.eclipse.statet.ecommons.emf.ui.forms.DropDownProperty;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;
import org.eclipse.statet.ecommons.emf.ui.forms.PropertyDetail;

import org.eclipse.statet.rtm.base.ui.rexpr.RExprValueProperty;
import org.eclipse.statet.rtm.base.ui.rexpr.RExprWidget;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;


public class StatProperty extends DropDownProperty {
	
	
	private static class Details extends DetailStack {
		
		
		public Details(final IEFFormPage page, final Composite parent) {
			super(page, parent);
		}
		
		
		@Override
		protected Detail createDetail(final EObject value) {
			if (value != null) {
				switch (value.eClass().getClassifierID()) {
				case GGPlotPackage.SUMMARY_STAT:
					return new SummaryDetail(this);
				default:
					break;
				}
			}
			return super.createDetail(value);
		}
		
	}
	
	private static class SummaryDetail extends PropertyDetail {
		
		
		private final RExprValueProperty property;
		
		
		public SummaryDetail(final DetailStack parent) {
			super(parent);
			
			final EClass eClass= Literals.SUMMARY_STAT;
			
			this.property= (RExprValueProperty) GGPlotProperties.createProperty(
					eClass, Literals.SUMMARY_STAT__YFUN,
					"f(x):", "Summary function for y" );
			
			createContent();
		}
		
		@Override
		protected void createContent(final Composite composite) {
			final IEFFormPage page= getPage();
			
			this.property.create(composite, page, RExprWidget.MIN_SIZE);
		}
		
		@Override
		public void addBindings(final IEMFEditContext context) {
			this.property.bind(context);
		}
		
	}
	
	
	public StatProperty(final String label, final String tooltip, final EClass eClass, final EStructuralFeature eFeature) {
		super(label, tooltip, eClass, eFeature);
	}
	
	
	@Override
	protected List<Object> createInitialInput() {
		final List<Object> list= new ArrayList<>();
		list.add(""); //$NON-NLS-1$
		list.add(GGPlotPackage.eINSTANCE.getGGPlotFactory().createIdentityStat());
		list.add(GGPlotPackage.eINSTANCE.getGGPlotFactory().createSummaryStat());
		return list;
	}
	
	@Override
	protected ILabelProvider createLabelProvider(final IEFFormPage page) {
		return new AdapterFactoryLabelProvider(page.getEditor().getAdapterFactory());
	}
	
	@Override
	protected DetailStack createDetails(final Composite parent, final IEFFormPage page) {
		return new Details(page, parent);
	}
	
}
