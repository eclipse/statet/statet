/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.jface.viewers.IFilter;

import org.eclipse.statet.rtm.ggplot.GGPlot;
import org.eclipse.statet.rtm.ggplot.Layer;
import org.eclipse.statet.rtm.ggplot.TextStyle;


public class LayerTextStyleFilter implements IFilter {
	
	
	public static final IFilter INSTANCE= new LayerTextStyleFilter();
	
	
	public LayerTextStyleFilter() {
	}
	
	
	@Override
	public boolean select(final Object toTest) {
		return (toTest instanceof GGPlot
				|| (toTest instanceof Layer && toTest instanceof TextStyle) );
	}
	
}
