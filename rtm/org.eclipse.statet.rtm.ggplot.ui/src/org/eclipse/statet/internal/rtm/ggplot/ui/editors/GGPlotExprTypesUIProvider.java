/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.statet.rtm.base.ui.rexpr.IRExprTypesUIProvider;
import org.eclipse.statet.rtm.base.ui.rexpr.RExprTypeUIAdapter;
import org.eclipse.statet.rtm.base.util.RExprType;
import org.eclipse.statet.rtm.base.util.RExprTypes;
import org.eclipse.statet.rtm.ggplot.core.GGPlotExprTypesProvider;


public class GGPlotExprTypesUIProvider extends GGPlotExprTypesProvider
		implements IRExprTypesUIProvider {
	
	
	private static final GGPlotExprTypeAdapters ADAPTERS= new GGPlotExprTypeAdapters();
	
	
	@Override
	public List<RExprTypeUIAdapter> getUIAdapters(final RExprTypes types,
			final EClass eClass, final EStructuralFeature eFeature) {
		final List<RExprTypeUIAdapter> uiAdapters= new ArrayList<>();
		for (final RExprType type : types.getTypes()) {
			RExprTypeUIAdapter uiAdapter= null;
			uiAdapter= ADAPTERS.getUIAdapter(type, eFeature);
			if (uiAdapter != null) {
				uiAdapters.add(uiAdapter);
			}
		}
		return uiAdapters;
	}
	
}
