/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.EFFormSection;
import org.eclipse.statet.ecommons.emf.ui.forms.EFPropertySet;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;


class LayerAestheticsSection extends EFFormSection {
	
	
	private final LayerDetail detail;
	
	private final EFPropertySet properties;
	
	
	public LayerAestheticsSection(final LayerDetail detail, final Composite parent) {
		super(detail.getPage(), parent, (TITLE_DESCRIPTION_STYLE | EXPANDABLE_STYLE));
		
		this.detail= detail;
		
		getSection().setText("Layer Aesthetics");
		getSection().setDescription("Customize the style of the layer.");
		
		final EClass eClass= this.detail.getEClass();
		
		this.properties= new EFPropertySet();
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_SHAPE_PROVIDER__SHAPE,
				"Shape:", null ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_LINE_TYPE_PROVIDER__LINE_TYPE,
				"Linetype:", null ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_SIZE_PROVIDER__SIZE,
				"Size:", null ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_COLOR_PROVIDER__COLOR,
				"Color:", "The color" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_FILL_PROVIDER__FILL,
				"Fill:", "The fill color" ));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_ALPHA_PROVIDER__ALPHA,
				"Alpha:", "The opacity level (in [0, 1]" ));
		
		createClient();
	}
	
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		
		this.properties.createControls(composite, page);
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		this.properties.bind(context);
	}
	
}
