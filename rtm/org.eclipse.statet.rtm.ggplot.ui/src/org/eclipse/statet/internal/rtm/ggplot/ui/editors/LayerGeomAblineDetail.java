/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.ui.forms.DetailStack;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage;


public class LayerGeomAblineDetail extends LayerDetail {
	
	
	private LayerDataSection dataSection;
	
	
	public LayerGeomAblineDetail(final DetailStack parent) {
		super(parent, GGPlotPackage.Literals.GEOM_ABLINE_LAYER);
	}
	
	@Override
	protected void createContent(final Composite composite) {
		this.dataSection= new LayerDataSection(this, composite);
		this.dataSection.getSection().setLayoutData(createSectionLayoutData());
	}
	
}
