/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.rtm.ggplot.ui.editors;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.emf.core.databinding.IEMFEditContext;
import org.eclipse.statet.ecommons.emf.ui.forms.EFFormSection;
import org.eclipse.statet.ecommons.emf.ui.forms.EFPropertySet;
import org.eclipse.statet.ecommons.emf.ui.forms.IEFFormPage;

import org.eclipse.statet.rtm.ggplot.GGPlotPackage.Literals;


public class TextStyleSection extends EFFormSection {
	
	
	private final EFPropertySet properties;
	
	
	public TextStyleSection(final IEFFormPage page, final Composite parent,
			final String label) {
		super(page, parent, "Text Style of " + label, null);
		
		final EClass eClass= Literals.TEXT_STYLE;
		
		this.properties= new EFPropertySet();
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.TEXT_STYLE__FONT_FAMILY, "Family:", "The font family"));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.TEXT_STYLE__FONT_FACE, "Face:", "The font face"));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_SIZE_PROVIDER__SIZE, "Size:", "The font size in pts"));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.PROP_COLOR_PROVIDER__COLOR, "Color:", "The color"));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.TEXT_STYLE__HJUST, "H Just:", "The horizontal justification (in [0, 1])"));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.TEXT_STYLE__HJUST, "V Just:", "The vertical justification (in [0, 1])"));
		this.properties.add(GGPlotProperties.createProperty(
				eClass, Literals.TEXT_STYLE__ANGLE, "Angle:", "The rotation angle (in [0, 360])"));
		
		createClient();
	}
	
	@Override
	protected void createContent(final Composite composite) {
		final IEFFormPage page= getPage();
		
		this.properties.createControls(composite, page);
	}
	
	@Override
	public void addBindings(final IEMFEditContext context) {
		this.properties.bind(context);
	}
	
}
