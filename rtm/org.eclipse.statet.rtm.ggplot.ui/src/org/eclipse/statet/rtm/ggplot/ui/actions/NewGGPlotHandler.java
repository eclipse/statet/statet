/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.ui.actions;

import org.eclipse.statet.rtm.base.ui.actions.AbstractNewRTaskHandler;
import org.eclipse.statet.rtm.ggplot.ui.RtGGPlotDescriptor;


public class NewGGPlotHandler extends AbstractNewRTaskHandler {
	
	
	public NewGGPlotHandler() {
		super(RtGGPlotDescriptor.INSTANCE);
	}
	
	
}
