/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.rtm.ggplot.ui;

import java.util.List;

import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ecommons.emf.core.util.RuleSet;

import org.eclipse.statet.rtm.base.ui.IRtDescriptor;
import org.eclipse.statet.rtm.base.ui.RtModelUIPlugin;
import org.eclipse.statet.rtm.ggplot.GGPlot;
import org.eclipse.statet.rtm.ggplot.GGPlotPackage;
import org.eclipse.statet.rtm.ggplot.core.GGPlotRCodeGen;
import org.eclipse.statet.rtm.ggplot.core.GGPlotRuleSet;
import org.eclipse.statet.rtm.ggplot.provider.GGPlotItemProviderAdapterFactory;
import org.eclipse.statet.rtm.ggplot.util.GGPlotAdapterFactory;


public class RtGGPlotDescriptor implements IRtDescriptor {
	
	
	public static final IRtDescriptor INSTANCE= new RtGGPlotDescriptor();
	
	
	private static final String XMI_FILE_EXTENSION= "Rtx-ggplot"; //$NON-NLS-1$
	
	private static final ImList<String> FILE_EXTENSIONS= ImCollections.newList(
			XMI_FILE_EXTENSION );
	
	
	public RtGGPlotDescriptor() {
	}
	
	
	@Override
	public String getTaskId() {
		return RtGGPlotEditorPlugin.RT_ID;
	}
	
	@Override
	public String getModelPluginID() {
		return "org.eclipse.statet.rtm.ggplot.core"; //$NON-NLS-1$
	}
	
	@Override
	public String getEditorPluginID() {
		return "org.eclipse.statet.rtm.ggplot.ui"; //$NON-NLS-1$
	}
	
	@Override
	public String getEditorID() {
		return "org.eclipse.statet.rtm.ggplot.editors.GGPlot"; //$NON-NLS-1$
	}
	
	@Override
	public Image getImage() {
		return RtGGPlotEditorPlugin.getPlugin().getImageRegistry().get(RtGGPlotEditorPlugin.IMG_OBJ_GGPLOT_TASK);
	}
	
	@Override
	public String getName() {
		return "Graph ('ggplot2')";
	}
	
	@Override
	public String getDefaultContentTypeID() {
		return GGPlotPackage.eCONTENT_TYPE;
	}
	
	@Override
	public String getDefaultFileExtension() {
		return XMI_FILE_EXTENSION;
	}
	
	@Override
	public List<String> getFileExtensions() {
		return FILE_EXTENSIONS;
	}
	
	@Override
	public String getAssociatedPerspectiveId() {
		return RtModelUIPlugin.R_GRAPHICS_PERSPECTIVE_ID;
	}
	
	@Override
	public GGPlotPackage getEPackage() {
		return GGPlotPackage.eINSTANCE;
	}
	
	@Override
	public GGPlot createInitialModelObject() {
		return getEPackage().getGGPlotFactory().createGGPlot();
	}
	
	@Override
	public GGPlotAdapterFactory createItemProviderAdapterFactory() {
		return new GGPlotItemProviderAdapterFactory();
	}
	
	@Override
	public RuleSet getRuleSet() {
		return GGPlotRuleSet.INSTANCE;
	}
	
	@Override
	public GGPlotRCodeGen createCodeGenerator() {
		return new GGPlotRCodeGen();
	}
	
}
